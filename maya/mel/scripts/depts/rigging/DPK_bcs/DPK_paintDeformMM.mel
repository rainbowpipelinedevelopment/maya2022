//----------------------------------------------------------------------------
// Copyright � 2007, Daniel Pook-Kolb
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//   - Redistributions of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//
//   - Redistributions in binary form must reproduce the above copyright
//     notice, this list of conditions and the following disclaimer in the
//     documentation and/or other materials provided with the distribution.
//
//   - Neither the name of the author nor the names of its contributors may be
//     used to endorse or promote products derived from this software without
//     specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//----------------------------------------------------------------------------
//
//
//--------------------------------------------------------------------------//
//  DPK_paintDeformMM.mel
//
//  Author:			Daniel Pook-Kolb
//					daniel@dpk.stargrav.com
//
//  File Version:	5
//  Last Change:	Mar 09, 2007
//
//  Description:
//      Dynamically creates a marking menu with all objects that were set up
//      for this marking menu. Also shows menu items for setting up objects
//      for this marking menu.
//
//  Usage:			...
//
//  Return:			none
//
//  Files Needed:
//					DPK_arrayTools.mel
//					DPK_findInAllNamespaces.mel
//					DPK_generalHotkeys.mel
//					DPK_paintDeform.mel
//					DPK_paintDeformHotkeys.mel
//					DPK_paintDeformMM_addObjects.mel
//					DPK_registerHotkeys.mel
//
//--------------------------------------------------------------------------//


//------------------------------
//------------------------------
//
//  Local Worker Procedures {{{1
//
//------------------------------
//------------------------------

//------------------------------
// DPK_L_isPoly {{{2
//
// This proc checks whether the given $obj is a polygonal object and returns
// true if it is, false otherwise.
//------------------------------
proc int DPK_L_isPoly( string $obj )
//------------------------------
{
//--//
// Check whether this object is a poly shape.
if( size(`ls -type "mesh" $obj`) ) {
	return true;
}

// $obj is not a poly shape. But maybe it is a transform with a poly shape
// beneath it. Get its shape children.
string $allShapes[] = `listRelatives -pa -c -type "mesh" $obj`;

// Iterate through them and get those that are not intermediate objects.
int $x, $shapes;
for( $x = 0; $x < size($allShapes); ++$x ) {
	// Check whether this is an intermediate object.
	if( !`getAttr ($allShapes[$x]+".intermediateObject")` ) {
		// No it is not, so we can store that we've found a shape.
		++$shapes;
	}
}

if( 1 == $shapes ) {
	// We've found exactly one valid shape.
	return true;
} else if( 1 < $shapes ) {
	// This will not work with multiple shapes under one transform.
	error "Multi shape objects not yet supported.";
}

// We didn't find any shapes.
return false;
//--//
}
//---- end "DPK_L_isPoly" ----// }}}2


//------------------------------
// DPK_L_getSelectedPolyObjs {{{2
//
// Returns an array of the selected polygon objects.
//------------------------------
proc string[] DPK_L_getSelectedPolyObjs()
//------------------------------
{
//--//
// Get the currently selected objects.
string $sels[] = `ls -sl`;

// Iterate through them and get a polygonal one.
string $objs[];
int    $x, $inc;
for( $x = 0; $x < size($sels); ++$x ) {
	if( DPK_L_isPoly($sels[$x]) ) {
		// This is a poly object. Add it to the list.
		$objs[ $inc++ ] = $sels[$x];
	}
}

return $objs;
//--//
}
//---- end "DPK_L_getSelectedPolyObjs" ----// }}}2


//------------------------------
// DPK_L_addMenuItems {{{2
//------------------------------
proc DPK_L_addMenuItems()
//------------------------------
{
//--//
// Preparations <
// Get all nodes in the scene that have a "DPK_paintDeformTarget" attribute.
string $objs[] = DPK_findInAllNamespaces( "*.DPK_paintDeformTarget", "-o" );

// Get the number of selected polygon objects.
int $numObjs = size( `DPK_L_getSelectedPolyObjs` );


// Put the set up objects into this MM. <
string $rp, $rps[];
int    $x;
for( $x = 0; $x < size($objs); ++$x ) {
	// Get the preferred radial position of this object.
	$rp = `getAttr ($objs[$x]+".DPK_paintDeformTarget")`;

	// Check whether we have a valid radial position for this object.
	if( size($rp) && -1 == DPK_at_string_eleIndex($rps, $rp) ) {
		// Yes, so we add it at its preferred position to the MM. First, store
		// that this position is now occupied.
		$rps[ size($rps) ] = $rp;

		// Create the menu item.
		menuItem -l `substitute "^_" $objs[$x] ""`
			-rp  $rp
			-c  ("DPK_paintDeformMM_call {\"choose\", \""+$objs[$x]+"\"}")
			-ecr 0
			-i   "commandButton.xpm"
			-bld 0;
	} else {
		// We don't have a valid radial position, so we add this one to the
		// bottom.
		menuItem -l `substitute "^_" $objs[$x] ""`
			-c  ("DPK_paintDeformMM_call {\"choose\", \""+$objs[$x]+"\"}")
			-ecr 0
			-i   "commandButton.xpm"
			-bld 0;
	}
}


// Menu items for adding selected objects to the list. <
// We'll create borders around the configuration items.
menuItem -divider true;
menuItem -divider true;

// This submenu will contain items for adding objects at different positions
// into the list.
menuItem -l "Add Selected Object(s)"
	-sm  true
	-en  ($numObjs)
	-bld 0;

// These are the possible radial positions.
string $rpn[] = { "North", "North East", "East", "South East", "South",
				  "South West", "West", "North West" };
string $rpp[] = { "N", "NE", "E", "SE", "S", "SW", "W", "NW" };

// Iterate through them and add the ones that aren't occupied.
for( $x = 0; $x < size($rpp); ++$x ) {
	if( -1 == DPK_at_string_eleIndex($rps, $rpp[$x]) ) {
		// This position is still free and can be used.
		menuItem -l $rpn[$x]
			-c  ("DPK_paintDeformMM_call {\"add\", \""+$rpp[$x]+"\"}")
			-ecr 0
			-i   "commandButton.xpm"
			-bld 0;
	} else {
		// The position is occupied and can't be used.
		menuItem -l $rpn[$x] -en 0 -bld 0;
	}
}
// Add the bottom list position.
menuItem -divider true;
menuItem -l "Bottom List"
	-c  ("DPK_paintDeformMM_call {\"add\", \"\"}")
	-ecr 0
	-i   "commandButton.xpm"
	-bld 0;
setParent -m "..";


// When there are objects, add menus for removing them. <
menuItem -l "Remove"
	-sm  true
	-en  `size $objs`
	-bld 0;
for( $x = 0; $x < size($objs); ++$x ) {
	menuItem -l $objs[$x]
		-c  ("DPK_paintDeformMM_call {\"remove\", \""+$objs[$x]+"\"}")
		-ecr 0
		-i   "commandButton.xpm"
		-bld 0;
}
setParent -m "..";
//--//
}
//---- end "DPK_L_addMenuItems" ----// }}}2


//------------------------------
// DPK_L_buildMM {{{2
//------------------------------
proc DPK_L_buildMM( int    $button,
					int    $ctrl,
					int    $alt,
					int    $shift )
//------------------------------
{
//--//
// Delete the "dpkMM" when it exists.
string $popup = "dpkMM";
if (`popupMenu -exists $popup`) { deleteUI $popup; }

// Create the popup menu.
popupMenu
	-p   "viewPanes"
	-b   $button
	-ctl $ctrl
	-alt $alt
	-sh  $shift
	-aob false
	-mm  true
	$popup;


// Add the menuItems to this popup menu.
DPK_L_addMenuItems;

// Reset the menu parent.
setParent -m "..";
//--//
}
//---- end "DPK_L_buildMM" ----// }}}2


//------------------------------
// DPK_L_addObject {{{2
//------------------------------
proc DPK_L_addObject( string $rp )
//------------------------------
{
//--//
// Get the selected polygon objects.
string $objs[] = DPK_L_getSelectedPolyObjs();

// Add them to the list.
DPK_paintDeformMM_addObjects( $objs, $rp );
//--//
}
//---- end "DPK_L_addObject" ----// }}}2


//------------------------------
// DPK_L_removeObject {{{2
//
// Removes the specified object from the marking menu.
//------------------------------
proc DPK_L_removeObject( string $obj )
//------------------------------
{
//--//
if( objExists($obj) && `objExists ($obj+".DPK_paintDeformTarget")` ) {
	deleteAttr ($obj+".DPK_paintDeformTarget");
}
//--//
}
//---- end "DPK_L_removeObject" ----// }}}2


//------------------------------
// DPK_L_chooseObject {{{2
//------------------------------
proc DPK_L_chooseObject( string $obj )
//------------------------------
{
//--//
// Make sure the object exists.
if( !objExists($obj) || !DPK_L_isPoly($obj) ) {
	error ("Object does not exist or is not a polygon object: "+$obj);
}

// Make sure the paintDeform UI is opened.
string $win = "DPK_paintDeform_win";
if( !`window -exists $win` ) {
	// The paintDeform window isn't open. Open it now.
	DPK_paintDeform;
}

// Specify the $obj as the current target obj.
eval( ("DPK_paintDeform_call {\"setTarg\", \""+$obj+"\"}") );

// Enter the tool.
eval( "DPK_paintDeform_call {\"enterTool\"}" );
//--//
}
//---- end "DPK_L_chooseObject" ----// }}}2


// }}}1


//------------------------------
//------------------------------
//
//  Global Procedures {{{1
//
//------------------------------
//------------------------------

////////////////////////////////
// DPK_paintDeformMM_call {{{2
//
// This is the "callback" procedure.
//
// It is called by the UI controls with one string array. The first element
// defines the task that should be performed. The other elements are optional
// and can be used as arguments to the called procs.
//
// This construct helps to minimize the use of global procs when you have many
// buttons...
////////////////////////////////
global proc DPK_paintDeformMM_call( string $args[] )
////////////////////////////////
{
//--//
// The first element of the $args array defines the task. We use a "switch"
// construct here because there may be many different tasks.
switch( $args[0] ) {
	// General {{{3
	case "add":
		DPK_L_addObject $args[1];
		break;
	case "remove":
		DPK_L_removeObject $args[1];
		break;
	case "choose":
		DPK_L_chooseObject $args[1];
		break;

	case "rebuildMenuItems":
		DPK_L_addMenuItems;
		break;

	// Issue a warning if the call wasn't implemented. {{{3
	default:
		warning ("Not yet implemented: "+$args[0]);
		break;
	// }}}3
}

// Make sure the MM will be removed.
if( `popupMenu -exists "dpkMM"` ) { deleteUI "dpkMM"; }
//--//
}
//---- end "DPK_paintDeformMM_call" ----// }}}2


////////////////////////////////
//------------------------------
// DPK_paintDeformMM {{{2
//------------------------------
////////////////////////////////
global proc DPK_paintDeformMM( int $button,
							   int $ctrl,
							   int $alt,
							   int $shift )
////////////////////////////////
{
//--//
if( $button ) {
	// Build the marking menu.
	DPK_L_buildMM $button $ctrl $alt $shift;
}
//--//
}
//---- end "DPK_paintDeformMM" ----// }}}2


// }}}1


//------------------------------
// Configure Vim {{{3
//
// If you're wondering why there are places with three curly braces ("{" or
// "}") with a number in comments, these are used by vim to do automatic
// folding (hiding lines).
//
// If you don't know what vim is, check out:
//    http://www.vim.org
//
// These settings will configure vim to display this file correctly.
//    vim:set ts=4 tw=78 fdm=marker fdl=1 fdc=3:
