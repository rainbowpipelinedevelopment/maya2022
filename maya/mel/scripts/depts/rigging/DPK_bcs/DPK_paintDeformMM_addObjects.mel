//----------------------------------------------------------------------------
// Copyright � 2007, Daniel Pook-Kolb
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//   - Redistributions of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//
//   - Redistributions in binary form must reproduce the above copyright
//     notice, this list of conditions and the following disclaimer in the
//     documentation and/or other materials provided with the distribution.
//
//   - Neither the name of the author nor the names of its contributors may be
//     used to endorse or promote products derived from this software without
//     specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//----------------------------------------------------------------------------
//
//
//----------------------------------------------------------------------------
//  DPK_paintDeformMM_addObjects.mel
//
//  Author:			Daniel Pook-Kolb
//					daniel@dpk.stargrav.com
//
//  File Version:	1
//  Last Change:	Mar 08, 2007
//
//  Description:
//      This script adds the given objects to the paintDeform marking menu at
//      the specified radial position.
//
//  Usage:
//      DPK_paintDeformMM_addObjects $objs $rp;
//
//      $objs - A string array containing the (polygon) objects to be added to
//              the marking menu.
//
//      $rp   - A string containing the radial position at which the object
//              should be added. Note that only the first of the specified
//              objects will be added at this position. The others are added
//              to the bottom list. Valid values are:
//              "N"  - for North
//              "NE" - for North East
//              "E"  - for East
//              "SE" - for South East
//              "S"  - for South
//              "SW" - for South West
//              "W"  - for West
//              "NW" - for North West
//              ""   - for the bottom list
//
//  Return:			none
//
//  Files Needed:	none
//
//----------------------------------------------------------------------------


//------------------------------
//------------------------------
//
//  Global Procedures {{{1
//
//------------------------------
//------------------------------

////////////////////////////////
//------------------------------
// DPK_paintDeformMM_addObjects {{{2
//------------------------------
////////////////////////////////
global proc DPK_paintDeformMM_addObjects( string $objs[], string $rp )
////////////////////////////////
{
//--//
// Iterate through the objects and add them add the preferred radial position.
int $x;
for( $x = 0; $x < size($objs); ++$x ) {
	// If it doesn't exist, add the "DPK_paintDeformTarget" attribute.
	if( !`objExists ($objs[$x]+".DPK_paintDeformTarget")` ) {
		addAttr -ln "DPK_paintDeformTarget" -dt "string" -h 1 $objs[$x];
	}

	// We can set the radial position on the first object only.
	if( size($rp) && !$x ) {
		// Set the radial position.
		setAttr ($objs[$x]+".DPK_paintDeformTarget") -type "string" $rp;
	} else {
		if( size($rp) ) {
			// We're trying to add more than one object to a radial position.
			warning ("PaintDeform: Cannot add more than one object to the "
					+"same radial position. Adding object to bottom list: "
					+$objs[$x]);
		}

		// The bottom list can hold any number of objects.
		setAttr ($objs[$x]+".DPK_paintDeformTarget") -type "string" "";
	}
}
//--//
}
//---- end "DPK_paintDeformMM_addObjects" ----// }}}2


// }}}1


//------------------------------
// Configure Vim {{{3
//
// If you're wondering why there are places with three curly braces ("{" or
// "}") with a number in comments, these are used by vim to do automatic
// folding (hiding lines).
//
// If you don't know what vim is, check out:
//    http://www.vim.org
//
// These settings will configure vim to display this file correctly.
// vim:set ts=4 tw=78 fdm=marker fdl=1 fdc=3:
