//----------------------------------------------------------------------------
// Copyright (c) 2015, Daniel Pook-Kolb
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//   - Redistributions of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//
//   - Redistributions in binary form must reproduce the above copyright
//     notice, this list of conditions and the following disclaimer in the
//     documentation and/or other materials provided with the distribution.
//
//   - Neither the name of the author nor the names of its contributors may be
//     used to endorse or promote products derived from this software without
//     specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//----------------------------------------------------------------------------
//
//
//--------------------------------------------------------------------------//
//  DPK_rotateDeltasUI.mel
//
//  Author:			Daniel Pook-Kolb
//					daniel@dpk.stargrav.com
//
//  Version:		1.0.5
//  Last Change:	May 01, 2015
//
//  Description:	This script can "rotate" the deltas between two specified
//                  objects' vertex positions.
//
//  Usage:			Type "DPK_rotateDeltasUI;" in the command line,
//					or create a shelf button with this command.
//
//  Return:			none
//
//  Files Needed:	DPK_rotateDeltas.mel
//
//--------------------------------------------------------------------------//


//------------------------------
//------------------------------
//
//  Includes {{{1
//
//  Source the scripts which contain necessary procedures.
//
//------------------------------
//------------------------------

// }}}1


//------------------------------
//------------------------------
//
//  File Scope Constants {{{1
//
//  Define some local "dummy" procedures which just return a simple value.
//  They can be used as constant variables which are global to this script.
//
//------------------------------
//------------------------------

//------------------------------
// Define the procs. {{{2


//------------------------------
// Returns the current version.
// Please synchronize with the version above.
//------------------------------
proc string DPK_L_version()      { return "1.0.5"; }


//------------------------------
// Set this variable to false for release.
// If we're developing, we want to delete any stored prefs about our windows.
// Default: false
//------------------------------
proc int    DPK_L_isDeveloping() { return false; }


//------------------------------
// This is used as the window name throughout the script.
// Default: DPK_rotateDeltasUI_win
//------------------------------
proc string DPK_L_windowName()   {
	return "DPK_rotateDeltasUI_win";
}
// }}}2


// }}}1


//------------------------------
//------------------------------
//
//  Option Variables {{{1
//
//  These procedures create, edit, and query the option variables, used by
//  this script.
//
//------------------------------
//------------------------------

//------------------------------
// DPK_L_getOption_targObj {{{2
//------------------------------
proc string DPK_L_getOption_targObj()
//------------------------------
{
//--//
// Get the optionVar's name.
string $var = (DPK_L_windowName()+"_targObj");

// Check, whether the optionVar exists.
if( !`optionVar -exists $var` ) {
	// The optionVar doesn't exist, so we return the default.
	return "";
}

// The optionVar exists, so we return its value.
return `optionVar -query $var`;
//--//
}
//---- end "DPK_L_getOption_targObj" ----// }}}2


//------------------------------
// DPK_L_getOption_axis {{{2
//------------------------------
proc vector DPK_L_getOption_axis()
//------------------------------
{
//--//
// Get the optionVar's name.
string $var = (DPK_L_windowName()+"_axis");

// Check, whether the optionVar exists.
if( !`optionVar -exists $var` ) {
	// The optionVar doesn't exist, so we return the default.
	return << 1.0, 0.0, 0.0 >>;
}

// The optionVar exists. Get its value.
float $value[] = `optionVar -query $var`;

// Build a vector from it.
vector $vec = << $value[0], $value[1], $value[2] >>;

// Make sure the vector has a length.
if( 0.001 > mag($vec) ) {
	// No it hasn't, so we return the default.
	return << 1.0, 0.0, 0.0 >>;
}

// Return the vector.
return $vec;
//--//
}
//---- end "DPK_L_getOption_axis" ----// }}}2


//------------------------------
// DPK_L_getOption_angle {{{2
//------------------------------
proc float DPK_L_getOption_angle()
//------------------------------
{
//--//
// Get the optionVar's name.
string $var = (DPK_L_windowName()+"_angle");

// Check, whether the optionVar exists.
if( !`optionVar -exists $var` ) {
	// The optionVar doesn't exist, so we return the default.
	return 45.0;
}

// The optionVar exists, so we return its value.
return `optionVar -query $var`;
//--//
}
//---- end "DPK_L_getOption_angle" ----// }}}2


//------------------------------
// DPK_L_setOption_targObj {{{2
//------------------------------
proc DPK_L_setOption_targObj( string $targObj )
//------------------------------
{
//--//
// Set the optionVar to $targObj.
optionVar -stringValue (DPK_L_windowName()+"_targObj") $targObj;
//--//
}
//---- end "DPK_L_setOption_targObj" ----// }}}2


//------------------------------
// DPK_L_setOption_axis {{{2
//------------------------------
proc DPK_L_setOption_axis( vector $axis )
//------------------------------
{
//--//
// Get the optionVar's name.
string $var = (DPK_L_windowName()+"_axis");

// First, clear the array.
if( `optionVar -exists $var` ) {
	optionVar -clearArray $var;
}

// Add the three floats.
optionVar
	-floatValueAppend $var ($axis.x)
	-floatValueAppend $var ($axis.y)
	-floatValueAppend $var ($axis.z);
//--//
}
//---- end "DPK_L_setOption_axis" ----// }}}2


//------------------------------
// DPK_L_setOption_angle {{{2
//------------------------------
proc DPK_L_setOption_angle( float $angle )
//------------------------------
{
//--//
// Set the optionVar to $angle.
optionVar -floatValue (DPK_L_windowName()+"_angle") $angle;
//--//
}
//---- end "DPK_L_setOption_angle" ----// }}}2


//------------------------------
// DPK_L_clearAllOptionVars {{{2
//------------------------------
proc DPK_L_clearAllOptionVars()
//------------------------------
{
//--//
string $varPrefix = (DPK_L_windowName()+"_");

// Remove all optionVars.
optionVar -remove ($varPrefix+"targObj");
optionVar -remove ($varPrefix+"axis");
optionVar -remove ($varPrefix+"angle");
//--//
}
//---- end "DPK_L_clearAllOptionVars" ----// }}}2


// }}}1


//------------------------------
//------------------------------
//
//  Local Worker Procedures {{{1
//
//  These are the local procedures which do the actual work. They don't deal
//  with UI stuff but work in the actual Maya scene. They're approximately in
//  the order from general tasks (or tasks that are used in many other procs)
//  to very specific tasks.
//
//------------------------------
//------------------------------

//------------------------------
// DPK_L_error {{{2
//
// Use this error proc to issue warnings and errors while informing the user
// in which script the error occurred.
//
// $msg is the actual text message that is printed.
//
// $level defines the way the message is printed.
//   $level == 0
//		The message is just printed.
//   $level == 1
//		The message printed as a warning.
//   $level == 2
//		The message is printed as an error.
//   $level == 3
//		A confirm dialog is created with the message.
//------------------------------
proc DPK_L_error( string $msg, int $level )
//------------------------------
{
//--//
string $win = DPK_L_windowName();

// Add the script's name in front of the message.
string $procMsg = ("["+$win+"] "+$msg);

switch( $level ) {
	case 1:
		// Issue a warning.
		warning $procMsg;
		break;
	case 2:
		// Error out.
		error $procMsg;
		break;
	case 3:
		// Create a confirmDialog and show the message (without the script's
		// name).
		confirmDialog
			-title   $win
			-message $msg
			-button  "Close"
			-db      "Close";
		break;
	default:
		// If $level is neither of the above values, just print the message.
		print ("// "+$procMsg+"\n");
		break;
}
//--//
}
//---- end "DPK_L_error" ----// }}}2


//------------------------------
// DPK_L_convertVersionString {{{2
//
// Converts a string like "1.0.1 x64" into the float "1.01".
//------------------------------
proc float DPK_L_convertVersionString( string $verStr )
//------------------------------
{
//--//
// First, remove any substring after a space (contains e.g. "x64").
$verStr = match( "^[0-9\.]*", $verStr );

// Split the string up at the dots.
string $toks[];
tokenize $verStr "." $toks;

// Reset the version string to the first number. The remaining numbers will be
// added after a dot (if there are any).
$verStr = $toks[0];
if( size($toks) > 1 ) {
	$verStr += ".";
}

// Iterate through the remaining numbers and add them to the $verStr.
int $x;
for( $x = 1; $x < size($toks); ++$x ) {
	$verStr += $toks[$x];
}

// Convert the string into a float and return it.
float $version = $verStr;
return $version;
//--//
}
//---- end "DPK_L_convertVersionString" ----// }}}2


//------------------------------
// _has2011 {{{2
//------------------------------
proc int _has2011()
//------------------------------
{
	return ( DPK_L_convertVersionString(`about -v`) >= 2011 );
}
//---- end "_has2011" ----// }}}2


//------------------------------
// DPK_L_isPoly {{{2
//
// This proc checks whether the given $obj is a polygonal object and returns
// true if it is, false otherwise.
//------------------------------
proc int DPK_L_isPoly( string $obj, int $doError )
//------------------------------
{
//--//
// Check whether this object is a poly shape.
if( size(`ls -type "mesh" $obj`) ) {
	return true;
}

// $obj is not a poly shape. But maybe it is a transform with a poly shape
// beneath it. Get its shape children.
string $allShapes[] = `listRelatives -pa -c -type "mesh" $obj`;

// Iterate through them and get those that are not intermediate objects.
int $x, $shapes;
for( $x = 0; $x < size($allShapes); ++$x ) {
	// Check whether this is an intermediate object.
	if( !`getAttr ($allShapes[$x]+".intermediateObject")` ) {
		// No it is not, so we can store that we've found a shape.
		++$shapes;
	}
}

if( 1 == $shapes ) {
	// We've found exactly one valid shape.
	return true;
} else if( 1 < $shapes ) {
	// This will not work with multiple shapes under one transform.
	if( $doError ) {
		error "Multi shape objects not yet supported.";
	} else {
		return false;
	}
}

// We didn't find any shapes.
return false;
//--//
}
//---- end "DPK_L_isPoly" ----// }}}2


//------------------------------
// DPK_L_getSelectedPoly {{{2
//------------------------------
proc string DPK_L_getSelectedPoly( int $doError )
//------------------------------
{
//--//
// Get the currently selected objects.
string $sels[] = `ls -sl`;

// Iterate through them and get a polygonal one.
int $x;
string $obj;
for( $x = 0; $x < size($sels); ++$x ) {
	if( DPK_L_isPoly($sels[$x], $doError) ) {
		// The $x'th selected object is a poly object. If we already have an
		// object, the user has selected more than one.
		if( size($obj) ) {
			if( $doError ) {
				error "More than one poly object selected.";
			} else {
				return "";
			}
		}

		// Store the found poly object.
		$obj = $sels[$x];
	}
}

return $obj;
//--//
}
//---- end "DPK_L_getSelectedPoly" ----// }}}2


// }}}1


//------------------------------
//------------------------------
//
//  UI Operations {{{1
//
//  These are the procedures that operate in the UI. They set and retrieve
//  values, select items, etc.. They may be called directly by the global
//  callback procedure at the end of this script or by other procs that
//  collect information from the UI to perform their task.
//
//------------------------------
//------------------------------

// }}}1


//------------------------------
//------------------------------
//
//  UI Maintenance {{{1
//
//  These are the local procs directly related to the UI.
//  They will build and update it etc..
//
//------------------------------
//------------------------------

//------------------------------
// DPK_L_enableControls {{{2
//------------------------------
proc DPK_L_enableControls()
//------------------------------
{
//--//
string $win = DPK_L_windowName();

// Get the current targObj.
string $targObj = DPK_L_getOption_targObj();

// Get the selected object if possible.
string $currObj = DPK_L_getSelectedPoly( false );

// When the $targObj exists and there is a valid $currObj, we can enable the
// Apply button.
setParent $win;
if( objExists($targObj) && DPK_L_isPoly($targObj, false) && size($currObj) ) {
	button -edit -en true  "Apply";
} else {
	button -edit -en false "Apply";
}
//--//
}
//---- end "DPK_L_enableControls" ----// }}}2


//------------------------------
// DPK_L_saveSettings {{{2
//------------------------------
proc DPK_L_saveSettings()
//------------------------------
{
//--//
// Set the window as current parent.
setParent `DPK_L_windowName`;

// Get the current angle value.
float $angle = `floatFieldGrp -q -v1 "angleFFG"`;

// Get the axis vector.
float $axis[3];
$axis[0] = `floatFieldGrp -q -v1 "axisFFG"`;
$axis[1] = `floatFieldGrp -q -v2 "axisFFG"`;
$axis[2] = `floatFieldGrp -q -v3 "axisFFG"`;
vector $vec = << $axis[0], $axis[1], $axis[2] >>;

// If the $vec has no length, we'll use the default 1 0 0.
if( 0.001 > mag($vec) ) {
	warning "Specified axis vector is invalid. Using << 1, 0, 0 >>.";
	$vec = << 1.0, 0.0, 0.0 >>;
	floatFieldGrp -edit -v1 1.0 -v2 0.0 -v3 0.0 "axisFFG";
}

// Set the optionVars to the retrieved values.
DPK_L_setOption_angle $angle;
DPK_L_setOption_axis  $vec;
//--//
}
//---- end "DPK_L_saveSettings" ----// }}}2


//------------------------------
// DPK_L_updateUI_settings {{{2
//------------------------------
proc DPK_L_updateUI_settings()
//------------------------------
{
//--//
// Get the window name.
string $win = DPK_L_windowName();

// Get the current settings.
string $targObj = DPK_L_getOption_targObj();
float  $angle   = DPK_L_getOption_angle();
vector $axis    = DPK_L_getOption_axis();

// Set the ui controls.
setParent $win;
textFieldButtonGrp -edit
	-tx (`objExists $targObj` ? $targObj : "<none>")
	"targTFBG";
floatFieldGrp -edit -v1 $angle "angleFFG";
floatFieldGrp -edit
	-v1 ($axis.x)
	-v2 ($axis.y)
	-v3 ($axis.z)
	"axisFFG";

// Enable/Disable the controls.
DPK_L_enableControls;
//--//
}
//---- end "DPK_L_updateUI_settings" ----// }}}2


//------------------------------
// DPK_L_buildUI {{{2
//
// This procedure creates the window and returns it.
//------------------------------
proc string DPK_L_buildUI()
//------------------------------
{
//--//
// Preparations {{{3
// First we get the window name.
string $win = DPK_L_windowName();

// We shall delete it if it exists.
if( `window -ex $win` ) { deleteUI -wnd $win; }

// If we're developing, we want to delete any stored prefs about our window.
if( `DPK_L_isDeveloping` && `windowPref -exists $win` ) {
	windowPref -remove $win;
}


// Now we can create the window. {{{3
// We'll use a version string for the window title.
string $ver = `DPK_L_version`;
window  -t ("DPK_rotateDeltasUI    v"+$ver)
		-tlc 150 500
		-rtf true
		-menuBar true
		$win;


// Define the UI template(s). {{{3
// Make sure the template doesn't exist.
string $uiTemplate = ($win+"_uiTemplate");
if( `uiTemplate -exists $uiTemplate` ) {
	deleteUI -uiTemplate $uiTemplate;
}

// Create the template.
uiTemplate $uiTemplate;


// Define templates for the different types of UI controls and layouts. <
// We'll start with the layouts.
formLayout -defineTemplate $uiTemplate -nd 100;

columnLayout -defineTemplate $uiTemplate
	-adj true
	-cal "center"
	-cat "both" 0
	-rs  2;

// Now the templates for the other UI controls.
button -defineTemplate $uiTemplate
	-width  80
	-height 25
	-align "center";

textFieldButtonGrp -defineTemplate $uiTemplate
	-ad3 2
	-cw3 80      50     50
	-ct3 "right" "both" "both"
	-co3 4       0      0
	-cl3 "right" "left" "center";

floatFieldGrp -defineTemplate $uiTemplate
	-cw2 80 50
	-ct2 "right" "both"
	-co2 4 0
	-cw4 80 50 50 50
	-ct4 "right" "both" "both" "both"
	-co4 4 0 0 0;


// Set the created template to be the current UI template. <
setUITemplate -pst $uiTemplate;


// Add the layouts. {{{3
// The main formLayout will position the bottom line controls and the
// columnLayout.
string $mainFL = `formLayout`;

// The columnLayout will put all the other controls into a single column.
string $mainCL = `columnLayout`;


// Add the UI controls. {{{3
setParent $mainFL;
button -ann "Rotate the deltas of the selected object" "Apply";
button "Close";

// The sep1 separator separates ;) this bottom line from the rest of the UI.
separator "sep1";

// Add the settings controls into the columnLayout.
setParent $mainCL;
textFieldButtonGrp -ed 0 -l "Target Object" -bl "Choose" "targTFBG";
floatFieldGrp
	-ann "Angle of rotation in degree"
	-l   "Angle"
	-nf  1
	"angleFFG";

floatFieldGrp
	-ann "The axis about which to rotate (e.g. 1 0 0 for the x-axis)"
	-l   "Axis"
	-nf  3
	"axisFFG";


// Menus {{{3
// Create the file menu.
menu -label "File" -tearOff false;

	// Create the menuItems.
	menuItem -label "Save Settings"
		-c "DPK_rotateDeltasUI_call {\"saveSettings\"}";
	menuItem -label "Reset Settings"
		-c "DPK_rotateDeltasUI_call {\"resetSettings\"}";

// Create the help menu.
menu -label "Help" -tearOff false;

	// Create the menuItems.
	menuItem -label "DPK Homepage"
			-ann "Visit the DPK homepage in the internet"
			-c "showHelp -absolute \"http://dpk.stargrav.com\"";
	menuItem -label "Online Help"
			-ann "Visit the online manaul"
			-c ("showHelp -absolute \"http://www.stargrav.com/"+
					"bcs/docs/data/lv2-rotate-overview.html\"");
	menuItem -divider true;
	menuItem -label "About..."
			-ann "Display version info about this script"
			-c "DPK_rotateDeltasUI_call {\"about\"}";



// Reset the current UI template. {{{3
setUITemplate -ppt;


// Set up the callbacks. {{{3
setParent $mainFL;
button -e -c ("deleteUI -wnd "+$win) "Close";
button -e
	-c  "DPK_rotateDeltasUI_call {\"apply\"}"
	"Apply";

setParent $mainCL;
textFieldButtonGrp -edit
	-bc "DPK_rotateDeltasUI_call {\"chooseTarg\"}"
	"targTFBG";


// Do the layout. {{{3
if( !`about -li` && !`about -ir`
		&& DPK_L_convertVersionString(`about -v`) < 2011 ) {
	// We're not on a *NIX system, so we do what works on windows. Sizing the
	// window really small before doing the layout. This has the effect that
	// the window is resized during the layout and thus becomes just as big as
	// required to show all controls.
	window -edit -wh 10 10 $win;
}

// Now, lay out the controls.
formLayout -e
    -af		"Close"		"bottom"	4
    -af		"Close"		"right"		4

    -ac		"sep1"		"bottom"	4		"Close"
    -af		"sep1"		"left"		0
    -af		"sep1"		"right"		0

    -af		"Apply"		"bottom"	4
    -af		"Apply"		"left"		4

	-af		$mainCL		"top"		0
	-af		$mainCL		"left"		4
	-ac		$mainCL		"bottom"	4		"sep1"
	-af		$mainCL		"right"		4
$mainFL;

if( !_has2011() && (`about -li` || `about -ir`) ) {
	// We're running linux or irix.
	window -edit -wh 350 140 $win;
}

// Update the UI. {{{3
// Update the UI settings controls so that they display the current options.
DPK_L_updateUI_settings;


// Add scriptJobs that update the UI. {{{3
scriptJob -parent $win
	-event "SelectionChanged" "DPK_rotateDeltasUI_call {\"selChanged\"}";


return $win;
//--//
}
//---- end "DPK_L_buildUI" ----// }}}2


// }}}1


//------------------------------
//------------------------------
//
//  UI Callbacks {{{1
//
//  These are the procedures that are indirectly called by the UI controls
//  through the global callback procedure at the end of this script. They
//  retrieve UI information and call the worker procedures, which perform the
//  actual task within the Maya scene.
//
//------------------------------
//------------------------------

//------------------------------
// DPK_L_apply {{{2
//------------------------------
proc DPK_L_apply()
//------------------------------
{
//--//
// Get the current settings.
string $targObj = DPK_L_getOption_targObj();
float  $angle   = DPK_L_getOption_angle();
vector $axis    = DPK_L_getOption_axis();

// Get the currently selected poly object.
string $obj = DPK_L_getSelectedPoly( true );

// Error checking.
if( !size($obj) ) {
	error "Please select a polygonal object.";
}
if( !size($targObj)               ||
	!objExists($targObj)          ||
	!DPK_L_isPoly($targObj, true) ) {
	error "No valid target object specified.";
}

// Rotate the deltas.
evalEcho( "DPK_rotateDeltas \""+$obj+"\" \""+$targObj+"\" "+$angle+
			" <<"+($axis.x)+","+($axis.y)+","+($axis.z)+">>;");
//--//
}
//---- end "DPK_L_apply" ----// }}}2


//------------------------------
// DPK_L_chooseTarg {{{2
//------------------------------
proc DPK_L_chooseTarg()
//------------------------------
{
//--//
// Get the currently selected poly object.
string $obj = DPK_L_getSelectedPoly( true );

// Set the optionVar to it.
DPK_L_setOption_targObj $obj;

// Enter the object into the TFBG.
setParent `DPK_L_windowName`;
if( size($obj) ) {
	textFieldButtonGrp -edit -tx $obj "targTFBG";
} else {
	textFieldButtonGrp -edit -tx "<none>" "targTFBG";
}

// Enable/Disable the controls.
DPK_L_enableControls;
//--//
}
//---- end "DPK_L_chooseTarg" ----// }}}2


//------------------------------
// DPK_L_about {{{2
//------------------------------
proc DPK_L_about ()
//------------------------------
{
//--//
string $ver = `DPK_L_version`;

// Create a confirm dialog with version info etc..
confirmDialog
	-title "DPK_rotateDeltasUI"
	-m ("DPK_rotateDeltasUI.mel      v"+$ver+"\n\n\n"+
		"Author:  Daniel Pook-Kolb\n"+
		"Email:  daniel@dpk.stargrav.com\n\n\n"+
		"Copyright 2015, Daniel Pook-Kolb\n")
	-ma "left"
	-b  "Close"
	-db "Close";
//--//
}
//---- end "DPK_L_about" ----// }}}2


// }}}1


//------------------------------
//------------------------------
//
//  (Optional) Scripting Interface {{{1
//
//  This section contains global procedures which present an alternate
//  interface to the functionality of this script. They don't use any UI but
//  directly perform their task. These can be called from within other scripts
//  or in batch mode.
//
//------------------------------
//------------------------------

// }}}1


//------------------------------
//------------------------------
//
//  Main Procedures {{{1
//
//  And here are the two most important global procs.
//
//------------------------------
//------------------------------

////////////////////////////////
// DPK_rotateDeltasUI_call {{{2
//
// This is the "callback" procedure.
//
// It is called by the UI controls with one string array. The first element
// defines the task that should be performed. The other elements are optional
// and can be used as arguments to the called procs.
//
// This construct helps to minimize the use of global procs when you have many
// buttons...
////////////////////////////////
global proc DPK_rotateDeltasUI_call( string $args[] )
////////////////////////////////
{
//--//
// The first element of the $args array defines the task. We use a "switch"
// construct here because there may be many different tasks.
switch( $args[0] ) {
    case "apply":
		DPK_L_saveSettings;
		DPK_L_apply;
		break;

	case "chooseTarg":
		DPK_L_chooseTarg;
		break;

	case "saveSettings":
		DPK_L_saveSettings;
		break;

	case "resetSettings":
		DPK_L_clearAllOptionVars;
		DPK_L_updateUI_settings;
		break;

	case "selChanged":
		DPK_L_enableControls;
		break;

	case "about":
		DPK_L_about;
		break;

	// Issue a warning if the call wasn't implemented.
	default:
		DPK_L_error ("Not yet implemented: "+$args[0]) 1;
		break;
}
//--//
}
//---- end "DPK_rotateDeltasUI_call" ----// }}}2


////////////////////////////////
//------------------------------
// DPK_rotateDeltasUI {{{2
//
// This is the main procedure. It really doesn't do more than calling the proc
// that creates the UI and showing the resulting window.
//------------------------------
////////////////////////////////
global proc DPK_rotateDeltasUI()
////////////////////////////////
{
//--//
// Build and show the UI.
showWindow `DPK_L_buildUI`;
//--//
}
//---- end "DPK_rotateDeltasUI" ----// }}}2


// }}}1


//------------------------------
// Configure Vim {{{3
//
// If you're wondering why there are places with three curly braces ("{" or
// "}") with a number in comments, these are used by vim to do automatic
// folding (hiding lines).
//
// If you don't know what vim is, check out:
//    http://www.vim.org
//
// These settings will configure vim to display this file correctly.
//    vim:set ts=4 tw=78 fdm=marker fdl=1 fdc=3:
