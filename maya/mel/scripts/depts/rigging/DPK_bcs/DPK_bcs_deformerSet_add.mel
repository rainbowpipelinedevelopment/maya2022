//----------------------------------------------------------------------------
// Copyright (c) 2017, STARGRAV GmbH
//
//
// The contents of this file are subject to the license "license.txt"
// you received with the BCS package.
// This may be superseded by an applicable license agreement between you and
// Stargrav GmbH.
//----------------------------------------------------------------------------
//
//
//----------------------------------------------------------------------------
//  DPK_bcs_deformerSet_add.mel
//
//  Author:         Daniel Pook-Kolb
//                  daniel@dpk.stargrav.com
//
//  Description:
//      Add selected components to the BCS node's deformer set.
//----------------------------------------------------------------------------

DPK_bcs_utils; // Ensures the BCS utils are loaded.

proc string _first(string $arr[]) {
    return $arr[0];
}

proc string _getBcsDeformerSet(string $bcs) {
    string $future[] = `listHistory -future true $bcs`;
    return _first(`ls -type "objectSet" $future`);
}

proc _set_add(string $set, string $points[]) {
    sets -edit -add $set $points;
}


////////////////////////////////
// DPK_bcs_deformerSet_add <
////////////////////////////////
global proc DPK_bcs_deformerSet_add(string $bcs) {
    string $points[] = `ls -sl -type "float3" -type "double3"`;
    if (!size($points)) {
        error "No vertices/points selected.";
    }
    if (!size($bcs)) {
        $bcs = _first(`ls -sl -o`);
    }
    $bcs = DPK_getBCSnode($bcs);
    string $deformerSet = _getBcsDeformerSet($bcs);
    int $fi = `DPK_bcs -q -freezeInput $bcs`;
    if ($fi) {
        DPK_bcs -edit -freezeInput off $bcs;
    }
    if (catch(_set_add($deformerSet, $points))) {
        if ($fi) {
            DPK_bcs -edit -freezeInput $fi $bcs;
        }
        error "Adding points to set failed.";
    }
    if ($fi) {
        DPK_bcs -edit -freezeInput $fi $bcs;
    }
}
