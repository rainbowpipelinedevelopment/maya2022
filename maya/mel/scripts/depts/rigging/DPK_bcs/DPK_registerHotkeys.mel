//----------------------------------------------------------------------------
// Copyright � 2007, Daniel Pook-Kolb
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//   - Redistributions of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//
//   - Redistributions in binary form must reproduce the above copyright
//     notice, this list of conditions and the following disclaimer in the
//     documentation and/or other materials provided with the distribution.
//
//   - Neither the name of the author nor the names of its contributors may be
//     used to endorse or promote products derived from this software without
//     specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//----------------------------------------------------------------------------
//
//
//----------------------------------------------------------------------------
//  DPK_registerHotkeys.mel
//
//  Author:			Daniel Pook-Kolb
//					daniel@dpk.stargrav.com
//
//  File Version:	1
//  Last Change:	Mar 08, 2007
//
//  Description:
//      Calls all available DPK_.*Hotkey MEL scripts to register hotkey
//      commands without assigning an actual hotkey. You can then manually
//      assign them a key in the Hotkey Editor.
//
//  Usage:
//      Simply type "DPK_registerHotkeys;" on the command line to register the
//      hotkey commands. Note that no hotkeys are assigned, the commands are
//      just made available in the "User" section of the Hotkey Editor.
//
//  Return:			none
//
//  Files Needed:	DPK_generalHotkeys.mel
//
//----------------------------------------------------------------------------


//------------------------------
//------------------------------
//
//  Global Procedures {{{1
//
//------------------------------
//------------------------------

////////////////////////////////
//------------------------------
// DPK_registerHotkeys {{{2
//------------------------------
////////////////////////////////
global proc DPK_registerHotkeys()
////////////////////////////////
{
//--//
// Call all available hotkey commands.
if( exists("DPK_toggleViewHotkeys.mel") ) {
	DPK_toggleViewHotkeys;
}
if( exists("DPK_paintDeformHotkeys.mel") ) {
	DPK_paintDeformHotkeys;
}
if( exists("DPK_generalHotkeys.mel") ) {
	DPK_generalHotkeys;
}
//--//
}
//---- end "DPK_registerHotkeys" ----// }}}2


// }}}1


//------------------------------
// Configure Vim {{{3
//
// If you're wondering why there are places with three curly braces ("{" or
// "}") with a number in comments, these are used by vim to do automatic
// folding (hiding lines).
//
// If you don't know what vim is, check out:
//    http://www.vim.org
//
// These settings will configure vim to display this file correctly.
// vim:set ts=4 tw=78 fdm=marker fdl=1 fdc=3:
