//----------------------------------------------------------------------------
// Copyright (c) 2017, STARGRAV GmbH
//
//
// The contents of this file are subject to the license "license.txt"
// you received with the BCS package.
// This may be superseded by an applicable license agreement between you and
// Stargrav GmbH.
//----------------------------------------------------------------------------
//
//
//----------------------------------------------------------------------------
//  DPK_toggleViewHotkeys.mel
//
//  Author:			Daniel Pook-Kolb
//					daniel@dpk.stargrav.com
//
//  File Version:	13
//  Last Change:	Jan 13, 2009
//
//  Description:
//      Registers two hotkey commands for the ToggleView and ToggleSmoothing
//      setups. The two hotkey commands are:
//        DPK_toggleView
//        DPK_toggleSmoothing
//
//      They work on setups you've created using the DPK_toggleViewSetup.mel
//      script.
//
//  Usage:
//      Simply type "DPK_toggleViewHotkeys;" on the command line to register
//      the hotkey commands. Note that no hotkeys are assigned, the commands
//      are just made available in the "User" section of the Hotkey Editor.
//
//  Return:			none
//
//  Files Needed:
//					DPK_registerHotkeys.mel
//					DPK_generalHotkeys.mel
//					DPK_toggleViewSetup.mel
//					DPK_bcsUI_version.mel
//					DPK_arrayTools.mel
//					DPK_findInAllNamespaces.mel
//
//----------------------------------------------------------------------------


//------------------------------
//------------------------------
//
//  Local Worker Procedures {{{1
//
//------------------------------
//------------------------------

//------------------------------
// DPK_L_registerCommand {{{2
//------------------------------
proc DPK_L_registerCommand( string $name, string $ann, string $cmd )
//------------------------------
{
//--//
// Make sure the command doesn't exist.
if( `runTimeCommand -ex $name` ) {
	return;
}

// Create the runtime command that does the work.
runTimeCommand
	-annotation $ann
	-category   "User"
	-command    $cmd
	$name;

// Create the named command that shows up in the hotkey editor.
nameCommand
	-annotation ($name+"NameCommand")
	-command    ($name)
	($name+"NameCommand");
//--//
}
//---- end "DPK_L_registerCommand" ----// }}}2


// }}}1


//------------------------------
//------------------------------
//
//  Global Procedures {{{1
//
//------------------------------
//------------------------------

////////////////////////////////
//------------------------------
// DPK_toggleViewHotkeys {{{2
//------------------------------
////////////////////////////////
global proc DPK_toggleViewHotkeys()
////////////////////////////////
{
//--//
// Switch between the reference and wip object.
DPK_L_registerCommand "DPK_toggleView"
	"Toggles the view between two positions. Requires setup using: DPK_toggleViewSetup.mel"
	"if( size(`ls -type \"script\" \"DPK_toggleView\"`) ) {\n    if( `pluginInfo -q -loaded \"DPK_bcs\"` ) {\n        eval \"DPK_eval -d \\\"scriptNode -eb DPK_toggleView\\\"\";\n    } else {\n        warning \"Please load the 'DPK_bcs' plugin for nicer undo.\";\n        scriptNode -eb DPK_toggleView;\n    }\n} else {\n    warning \"No \\\"toggleView\\\" setup found. Please use DPK_toggleViewSetup.mel to create such a setup.\";\n}"
;


// Toggle smoothing of a toggleSmoothing setup.
DPK_L_registerCommand "DPK_toggleSmoothing"
	"Toggles the smoothing of a 'toggleView' setup. Requires setup using: DPK_toggleViewSetup.mel"
	"if( size(`ls -type \"script\" \"DPK_toggleSmoothing\"`) ) {\n    if( `pluginInfo -q -loaded \"DPK_bcs\"` ) {\n        eval \"DPK_eval -d \\\"scriptNode -eb DPK_toggleSmoothing\\\"\";\n    } else {\n        warning \"Please load the 'DPK_bcs' plugin for nicer undo.\";\n        scriptNode -eb DPK_toggleSmoothing;\n    }\n} else {\n    warning \"No \\\"toggleSmoothing\\\" setup found. Please use DPK_toggleViewSetup.mel to create such a setup.\";\n}"
;
//--//
}
//---- end "DPK_toggleViewHotkeys" ----// }}}2


// }}}1


//------------------------------
// Configure Vim {{{3
//
// If you're wondering why there are places with three curly braces ("{" or
// "}") with a number in comments, these are used by vim to do automatic
// folding (hiding lines).
//
// If you don't know what vim is, check out:
//    http://www.vim.org
//
// These settings will configure vim to display this file correctly.
// vim:set ts=4 tw=78 fdm=marker fdl=1 fdc=3:
