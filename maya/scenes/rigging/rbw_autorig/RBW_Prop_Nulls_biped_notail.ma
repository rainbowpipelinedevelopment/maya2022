//Maya ASCII 2019 scene
//Name: SKT_Prop_Nulls_biped_notail_NEW.ma
//Last modified: Fri, Aug 13, 2021 02:11:18 PM
//Codeset: 1252
requires maya "2019";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t pal;
fileInfo "application" "maya";
fileInfo "product" "Maya 2019";
fileInfo "version" "2019";
fileInfo "cutIdentifier" "202004141915-92acaa8c08";
fileInfo "osv" "Microsoft Windows 10 Technical Preview  (Build 19043)\n";
createNode transform -n "propNULL_GRP";
	rename -uid "1C5B10F9-4DA0-B81F-89E1-12B3C6408303";
createNode transform -n "L_propNULL_GRP" -p "propNULL_GRP";
	rename -uid "00E71E9D-43EF-52B1-3B8B-F7B3EF7D468B";
	setAttr ".t" -type "double3" 9.4110007341004991 13.205999652041845 0.21404153222568453 ;
	setAttr ".r" -type "double3" 3.7376128631879787e-08 1.1907465328851485e-07 15.000000000065533 ;
createNode transform -n "L_propNULL_Ctrl" -p "L_propNULL_GRP";
	rename -uid "72D5F5B9-4C68-2B92-C01E-F4A133FF4822";
	setAttr -l on -k off ".v";
	setAttr ".ovc" 17;
createNode nurbsCurve -n "L_propNULL_CtrlShape" -p "L_propNULL_Ctrl";
	rename -uid "3BE37A7F-4A37-7B60-2EC3-50BC1D47840F";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 0 no 3
		5 0 1 2 3 4
		5
		-0.89820071273825941 0 5.5733200383037599e-17
		7.0512908272736729e-17 0 0.89820071273825941
		0.89820071273825941 0 -8.5292616162435797e-17
		-7.0512908272736729e-17 0 -0.89820071273825941
		-0.89820071273825941 0 5.5733200383037599e-17
		;
createNode transform -n "L_propOffset_Ctrl" -p "L_propNULL_Ctrl";
	rename -uid "4A6F5EA3-4DB9-CD5E-03C5-4BB783B8DF6D";
	addAttr -ci true -sn "follow" -ln "follow" -min 0 -max 7 -en "Hand L:Hand R:Hip:COG:Transform:Chest L:Head" 
		-at "enum";
	setAttr -l on -k off ".v";
	setAttr -k on ".follow";
createNode nurbsCurve -n "L_propOffset_CtrlShape" -p "L_propOffset_Ctrl";
	rename -uid "9827A46B-4D0F-07C1-1F52-10B870C15416";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.59151707896363581 2.5611389844764424e-17 1.0751931810883966e-15
		0.41826573772284492 3.6219974869690475e-17 -0.41826573772284498
		-5.1903470903718819e-17 2.5611389844764445e-17 -0.59151707896363503
		-0.41826573772284492 1.1828107054477595e-32 -0.41826573772284514
		-0.5915170789636347 -2.5611389844764436e-17 -3.781040042157305e-16
		-0.41826573772284509 -3.6219974869690475e-17 0.41826573772284498
		-3.6332429632603157e-16 -2.5611389844764445e-17 0.5915170789636347
		0.41826573772284492 -1.8121354668597418e-32 0.41826573772284514
		0.59151707896363581 2.5611389844764424e-17 1.0751931810883966e-15
		0.41826573772284492 3.6219974869690475e-17 -0.41826573772284498
		-5.1903470903718819e-17 2.5611389844764445e-17 -0.59151707896363503
		;
createNode transform -n "L_propAnchor_LOC_Ctrl" -p "L_propOffset_Ctrl";
	rename -uid "5C6DAE84-4E06-77F3-FE6B-3DB909F1940C";
createNode locator -n "L_propAnchor_LOC_CtrlShape" -p "L_propAnchor_LOC_Ctrl";
	rename -uid "4E130928-4FD0-8316-8924-039E882144AB";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".los" -type "double3" 0.4 0.4 0.4 ;
createNode transform -n "R_propNULL_GRP" -p "propNULL_GRP";
	rename -uid "792A4289-486F-08DB-E306-B5A8DFDB799B";
	setAttr ".t" -type "double3" -9.4110067261329462 13.206141518691103 0.21404033178934112 ;
	setAttr ".r" -type "double3" -5.2628202876185833e-15 -2.0963632437829894e-15 -14.99999999999998 ;
createNode transform -n "R_propNULL_Ctrl" -p "R_propNULL_GRP";
	rename -uid "302EE374-4BF4-A274-2773-37B149985141";
	setAttr -l on -k off ".v";
	setAttr ".ovc" 17;
createNode nurbsCurve -n "R_propNULL_CtrlShape" -p "R_propNULL_Ctrl";
	rename -uid "9E8A49E8-4684-A346-A392-E1BAFB894D22";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 0 no 3
		5 0 1 2 3 4
		5
		-0.89820071273825941 0 5.5733200383037599e-17
		7.0512908272736729e-17 0 0.89820071273825941
		0.89820071273825941 0 -8.5292616162435797e-17
		-7.0512908272736729e-17 0 -0.89820071273825941
		-0.89820071273825941 0 5.5733200383037599e-17
		;
createNode transform -n "R_propOffset_Ctrl" -p "R_propNULL_Ctrl";
	rename -uid "78E28B6C-4F06-01E4-9EB8-DAA03A04487B";
	addAttr -ci true -sn "follow" -ln "follow" -min 0 -max 7 -en "Hand L:Hand R:Hip:COG:Transform:Chest R:Head" 
		-at "enum";
	setAttr -l on -k off ".v";
	setAttr -k on ".follow" 1;
createNode nurbsCurve -n "R_propOffset_CtrlShape" -p "R_propOffset_Ctrl";
	rename -uid "7980E969-49B8-E91F-0EB3-51BC27AC7CDB";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.59151707896363581 2.5611389844764424e-17 1.0751931810883966e-15
		0.41826573772284492 3.6219974869690475e-17 -0.41826573772284498
		-5.1903470903718819e-17 2.5611389844764445e-17 -0.59151707896363503
		-0.41826573772284492 1.1828107054477595e-32 -0.41826573772284514
		-0.5915170789636347 -2.5611389844764436e-17 -3.781040042157305e-16
		-0.41826573772284509 -3.6219974869690475e-17 0.41826573772284498
		-3.6332429632603157e-16 -2.5611389844764445e-17 0.5915170789636347
		0.41826573772284492 -1.8121354668597418e-32 0.41826573772284514
		0.59151707896363581 2.5611389844764424e-17 1.0751931810883966e-15
		0.41826573772284492 3.6219974869690475e-17 -0.41826573772284498
		-5.1903470903718819e-17 2.5611389844764445e-17 -0.59151707896363503
		;
createNode transform -n "R_propAnchor_LOC_Ctrl" -p "R_propOffset_Ctrl";
	rename -uid "06248FC1-46FD-2644-D79D-23A6A56D22FC";
createNode locator -n "R_propAnchor_LOC_CtrlShape" -p "R_propAnchor_LOC_Ctrl";
	rename -uid "8073FD37-401A-EB3F-472D-09886FA9B5BA";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".los" -type "double3" 0.4 0.4 0.4 ;
createNode transform -n "propLOCS_GRP";
	rename -uid "CBADA49B-4610-78E0-3C62-8981F4442C00";
createNode transform -n "LOC_hand_L" -p "propLOCS_GRP";
	rename -uid "C092203D-4B14-625F-D380-12A92D6D397D";
	setAttr ".t" -type "double3" 12.442364504257799 12.01643150625981 0.21404153222568506 ;
	setAttr ".r" -type "double3" 0 0 15.000000000065533 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
createNode locator -n "LOC_hand_LShape" -p "LOC_hand_L";
	rename -uid "B49CAD43-49BD-7C99-8916-FB9A1C0A301A";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".los" -type "double3" 0.4 0.4 0.4 ;
createNode transform -n "LOC_hip_L" -p "propLOCS_GRP";
	rename -uid "5990D4D0-489C-6944-2548-249800042350";
	setAttr ".t" -type "double3" 7.3932443677849715 8.5 0 ;
createNode locator -n "LOC_hip_LShape" -p "LOC_hip_L";
	rename -uid "D62CE3ED-49A1-27F8-5E93-B980923C2936";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".los" -type "double3" 0.4 0.4 0.4 ;
createNode transform -n "LOC_cog_L" -p "propLOCS_GRP";
	rename -uid "18C71B7B-47BE-28CC-BD7A-26A9F8F1AA90";
	setAttr ".t" -type "double3" 8.6191886740685693 8.4999999999999982 0 ;
createNode locator -n "LOC_cog_LShape" -p "LOC_cog_L";
	rename -uid "63EE569B-4168-DC88-473B-739D2800C9EF";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".los" -type "double3" 0.4 0.4 0.4 ;
createNode transform -n "LOC_transform_L" -p "propLOCS_GRP";
	rename -uid "468BD678-4A3A-C2D6-FA9E-A1B6D32027F1";
	setAttr ".t" -type "double3" 11.010310051838085 0 0 ;
createNode locator -n "LOC_transform_LShape" -p "LOC_transform_L";
	rename -uid "289A42DE-4B59-310A-BCA7-8883995CC983";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".los" -type "double3" 0.4 0.4 0.4 ;
createNode transform -n "LOC_chest_L" -p "propLOCS_GRP";
	rename -uid "3A655207-437D-3C56-CA64-6FB6B73385B5";
	setAttr ".t" -type "double3" 1.1488580535903656 14.194945568989144 5.3464111313510445 ;
	setAttr ".r" -type "double3" 64.999999999999986 0 0 ;
createNode locator -n "LOC_chest_LShape" -p "LOC_chest_L";
	rename -uid "755C4792-4FD1-27FE-54C6-10958C2DB986";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".los" -type "double3" 0.4 0.4 0.4 ;
createNode transform -n "LOC_head_L" -p "propLOCS_GRP";
	rename -uid "23855323-440B-F132-FF17-AB90BC6FCED4";
	setAttr ".t" -type "double3" 7.6520074414122012 21 5.5179548306163353 ;
createNode locator -n "LOC_head_LShape" -p "LOC_head_L";
	rename -uid "4A2AA85A-4BB9-6858-7451-99BB14AF940D";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".los" -type "double3" 0.4 0.4 0.4 ;
createNode transform -n "LOC_hand_R" -p "propLOCS_GRP";
	rename -uid "31A4A35D-41E7-9368-895B-AA92105A7033";
	setAttr ".t" -type "double3" -12.442 12.01643150625981 0.21404153222568506 ;
	setAttr ".r" -type "double3" 0 0 -14.999999999999998 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
createNode locator -n "LOC_hand_RShape" -p "LOC_hand_R";
	rename -uid "779F68B7-4AB4-994C-64AC-2FB01B0D1B9D";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".los" -type "double3" 0.4 0.4 0.4 ;
createNode transform -n "LOC_hip_R" -p "propLOCS_GRP";
	rename -uid "FDD6AA28-4178-7E12-C01A-00B0E7E1CED0";
	setAttr ".t" -type "double3" -7.393 8.5 0 ;
createNode locator -n "LOC_hip_RShape" -p "LOC_hip_R";
	rename -uid "D8D002CE-42EA-4028-1EF4-88803AF58629";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".los" -type "double3" 0.4 0.4 0.4 ;
createNode transform -n "LOC_cog_R" -p "propLOCS_GRP";
	rename -uid "B529BE22-4725-7944-0ECA-9186D59FD99B";
	setAttr ".t" -type "double3" -8.619 8.4999999999999982 0 ;
createNode locator -n "LOC_cog_RShape" -p "LOC_cog_R";
	rename -uid "3514C757-42F1-C065-D6D6-AFA9E4607C37";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".los" -type "double3" 0.4 0.4 0.4 ;
createNode transform -n "LOC_transform_R" -p "propLOCS_GRP";
	rename -uid "1AEC74E2-4988-C374-43EB-87BD67FBD46A";
	setAttr ".t" -type "double3" -11.01 0 0 ;
createNode locator -n "LOC_transform_RShape" -p "LOC_transform_R";
	rename -uid "A70A6C3B-41FF-BC05-3128-B49C90B28DC3";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".los" -type "double3" 0.4 0.4 0.4 ;
createNode transform -n "LOC_chest_R" -p "propLOCS_GRP";
	rename -uid "5B7F62DF-494C-6F4F-411F-F89B4E59181F";
	setAttr ".t" -type "double3" -1.149 14.194945568989144 5.3464111313510445 ;
	setAttr ".r" -type "double3" 64.999999999999986 0 0 ;
createNode locator -n "LOC_chest_RShape" -p "LOC_chest_R";
	rename -uid "C3010DD4-4B32-DCF8-A9DF-EF9E7C43B12F";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".los" -type "double3" 0.4 0.4 0.4 ;
createNode transform -n "LOC_head_R" -p "propLOCS_GRP";
	rename -uid "E67B83F4-4130-129B-0061-5192BFEE66E4";
	setAttr ".t" -type "double3" -7.652 21 5.5179548306163353 ;
createNode locator -n "LOC_head_RShape" -p "LOC_head_R";
	rename -uid "F7BFC8FA-4C73-AAC7-1A91-EFA5C6E9D23F";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".los" -type "double3" 0.4 0.4 0.4 ;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av -k on ".unw" 1;
	setAttr -av -k on ".etw";
	setAttr -av -k on ".tps";
	setAttr -av -k on ".tms";
select -ne :hardwareRenderingGlobals;
	setAttr -av -k on ".ihi";
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr -k on ".hwi";
	setAttr -av ".ta";
	setAttr -av ".tq";
	setAttr -av ".aoam";
	setAttr -av ".aora";
	setAttr -k on ".hff";
	setAttr -av -k on ".hfd";
	setAttr -av ".hfs";
	setAttr -av ".hfe";
	setAttr -av -k on ".hfcr";
	setAttr -av -k on ".hfcg";
	setAttr -av -k on ".hfcb";
	setAttr -av ".hfa";
	setAttr -av ".mbe";
	setAttr -av -k on ".mbsof";
	setAttr -k on ".blen";
	setAttr -k on ".blat";
	setAttr ".msaa" yes;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -k on ".fzn";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".bbx";
	setAttr -k on ".vwm";
	setAttr -k on ".tpv";
	setAttr -k on ".uit";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k on ".macc";
	setAttr -av -k on ".macd";
	setAttr -av -k on ".macq";
	setAttr -av -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -av -k on ".clip";
	setAttr -av -k on ".edm";
	setAttr -av -k on ".edl" no;
	setAttr -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -av -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -av -cb on ".imfkey";
	setAttr -av -k on ".gama";
	setAttr -av -k on ".an";
	setAttr -k on ".ar";
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -av -k on ".be";
	setAttr -av -cb on ".ep";
	setAttr -av -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -av -k on ".pff";
	setAttr -av -cb on ".peie";
	setAttr -av -k on ".ifp";
	setAttr -k on ".rv";
	setAttr -av -k on ".comp";
	setAttr -av -k on ".cth";
	setAttr -av -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -av -k on ".rd";
	setAttr -av -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -av -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -av -k on ".mm";
	setAttr -av -k on ".npu";
	setAttr -av -k on ".itf";
	setAttr -av -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -av -k on ".uf";
	setAttr -av -k on ".oi";
	setAttr -av -k on ".rut";
	setAttr -av -k on ".mot";
	setAttr -av -k on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -av -k on ".mbso";
	setAttr -av -k on ".mbsc";
	setAttr -av -k on ".afp";
	setAttr -av -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -av -k on ".ubc";
	setAttr -av -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -av -k on ".udbx";
	setAttr -av -k on ".smc";
	setAttr -av -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -av -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -av -k on ".tlwd";
	setAttr -av -k on ".tlht";
	setAttr -av -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -av -k on ".ope";
	setAttr -av -k on ".oppf";
	setAttr -av -k on ".rcp";
	setAttr -av -k on ".icp";
	setAttr -av -k on ".ocp";
	setAttr -k on ".hbl";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -av -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w";
	setAttr -av -k on ".h";
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar";
	setAttr -av -k on ".ldar";
	setAttr -av -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -av -k on ".isu";
	setAttr -av -k on ".pdu";
select -ne :hardwareRenderGlobals;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k off -cb on ".ctrs" 256;
	setAttr -av -k off -cb on ".btrs" 512;
	setAttr -av -k off -cb on ".fbfm";
	setAttr -av -k off -cb on ".ehql";
	setAttr -av -k off -cb on ".eams";
	setAttr -av -k off -cb on ".eeaa";
	setAttr -av -k off -cb on ".engm";
	setAttr -av -k off -cb on ".mes";
	setAttr -av -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -av -k off -cb on ".mbs";
	setAttr -av -k off -cb on ".trm";
	setAttr -av -k off -cb on ".tshc";
	setAttr -av -k off -cb on ".enpt";
	setAttr -av -k off -cb on ".clmt";
	setAttr -av -k off -cb on ".tcov";
	setAttr -av -k off -cb on ".lith";
	setAttr -av -k off -cb on ".sobc";
	setAttr -av -k off -cb on ".cuth";
	setAttr -av -k off -cb on ".hgcd";
	setAttr -av -k off -cb on ".hgci";
	setAttr -av -k off -cb on ".mgcs";
	setAttr -av -k off -cb on ".twa";
	setAttr -av -k off -cb on ".twz";
	setAttr -k on ".hwcc";
	setAttr -k on ".hwdp";
	setAttr -k on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
// End of SKT_Prop_Nulls_biped_notail_NEW.ma
