//Maya ASCII 2019 scene
//Name: wheels_base_rigNewScript.ma
//Last modified: Fri, May 24, 2024 05:37:50 PM
//Codeset: 1252
requires maya "2019";
requires -nodeType "VRaySettingsNode" -dataType "VRaySunParams" -dataType "vrayFloatVectorData"
		 -dataType "vrayFloatVectorData" -dataType "vrayIntData" "vrayformaya" "5";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "vrayBuild" "5.00.21 848ea8a";
fileInfo "application" "maya";
fileInfo "product" "Maya 2019";
fileInfo "version" "2019";
fileInfo "cutIdentifier" "202004141915-92acaa8c08";
fileInfo "osv" "Microsoft Windows 10 Technical Preview  (Build 19045)\n";
createNode transform -s -n "persp";
	rename -uid "74656C92-409A-FF7D-1CF4-44849AA0592F";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 28 21 28 ;
	setAttr ".r" -type "double3" -27.938352729602379 44.999999999999972 -5.172681101354183e-14 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "FC5847D3-4539-A06A-3E91-8A8248C4D016";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 44.82186966202994;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "AD7A5F80-4DA1-A759-618F-3490E5FC2B7B";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "7C027676-4CDB-AF1E-1F9D-A9981EE8CABE";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "0C5543B5-47A1-757F-EE52-DE856DC4755F";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "27C6D3BC-4051-1507-03AC-AC9E894D3180";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "F108C067-4D81-0375-4636-B38A49A75B09";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "AE8DD8D1-4A02-1DCB-3F1F-2EA73B6C972C";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "GRP_Drive_Control";
	rename -uid "E130BB25-46C2-7D20-8B88-01893FFD3054";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".t" -type "double3" 7.1054273576010019e-15 -13.348115633643339 35.389190673828125 ;
	setAttr ".r" -type "double3" 0 -90 0 ;
	setAttr ".rp" -type "double3" 35.389190673828132 20.515619352962506 0 ;
	setAttr ".rpt" -type "double3" -35.389190673827038 0 -35.389190673828118 ;
	setAttr ".sp" -type "double3" 35.389190673828132 20.515619352962506 0 ;
createNode transform -n "CNT_drive" -p "GRP_Drive_Control";
	rename -uid "87F72309-430C-95FE-FABE-3CBB9AA658C7";
	addAttr -ci true -sn "Drive" -ln "Drive" -at "double";
	addAttr -ci true -sn "Radius" -ln "Radius" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 64.62727165222168 20.515619352962506 -2.3665827156630354e-30 ;
	setAttr ".sp" -type "double3" 64.62727165222168 20.515619352962506 -2.3665827156630354e-30 ;
createNode nurbsCurve -n "CNT_driveShape" -p "CNT_drive";
	rename -uid "DB510F64-4DAC-ADD2-7D20-0CBFC2C53857";
	setAttr -k off ".v";
	setAttr ".tw" yes;
	setAttr -s 11 ".cp[0:10]" -type "double3" 29.937562973444063 0 1.1111162453402072e-32 
		22.532657678232649 0 7.0413451706701213 22.532657678232649 0 3.5206725853350607 -14.234791059474432 
		0 3.5206725853350607 -14.234791059474432 0 7.0413451706701213 -20.778953870596723 
		0 1.1111162453402072e-32 -14.234791059474432 0 -7.0413451706701213 -14.234791059474432 
		0 -3.5206725853350607 22.532657678232649 0 -3.5206725853350607 22.532657678232649 
		0 -7.0413451706701213 29.937562973444063 0 1.1111162453402072e-32;
createNode nurbsCurve -n "CNT_driveShapeOrig" -p "CNT_drive";
	rename -uid "F4D8B594-47DC-615A-A8F5-4097931F53F5";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 10 0 no 3
		11 0 1 2 3 4 5 6 7 8 9 10
		11
		-31.103782755748988 20.515619352962506 -2.7128552029974017e-32
		-13.024274025619214 20.515619352962506 -17.191855453885719
		-13.024274025619214 20.515619352962506 -8.5959277269428593
		76.74559931332071 20.515619352962506 -8.5959277269428593
		76.74559931332071 20.515619352962506 -17.191855453885719
		92.723555000557909 20.515619352962506 -2.7128552029974017e-32
		76.74559931332071 20.515619352962506 17.191855453885719
		76.74559931332071 20.515619352962506 8.5959277269428593
		-13.024274025619214 20.515619352962506 8.5959277269428593
		-13.024274025619214 20.515619352962506 17.191855453885719
		-31.103782755748988 20.515619352962506 -2.7128552029974017e-32
		;
createNode transform -n "GRP_Anchor_Front_Tire_Control_L" -p "CNT_drive";
	rename -uid "1D623BD3-4F91-E3B8-845A-4FB224D22ADF";
	setAttr ".t" -type "double3" 48.206731796264648 17.507783201924632 -10.712905883787954 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000004 ;
createNode transform -n "GRP_Offset_Front_Tire_Control_L" -p "GRP_Anchor_Front_Tire_Control_L";
	rename -uid "E201A434-4CD9-4851-55B9-048B7FC53792";
createNode transform -n "CNT_Front_Tire_L" -p "GRP_Offset_Front_Tire_Control_L";
	rename -uid "0AD02141-47D5-220D-9B85-B7A4955CCD86";
	addAttr -ci true -sn "Tire_Spin_Rotation" -ln "Tire_Spin_Rotation" -min -1 -max 
		1 -at "double";
	setAttr -l on -k off ".v";
	setAttr -k on ".Tire_Spin_Rotation" 1;
createNode nurbsCurve -n "CNT_Front_Tire_LShape" -p "CNT_Front_Tire_L";
	rename -uid "0E9A0B5C-4F78-8B4C-B2CE-BBACFDC2B85D";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".tw" yes;
	setAttr -s 11 ".cp[0:10]" -type "double3" -6.8837521849358323 -3.6858079926965659 
		3.197944192239262 2.1086845283582741e-15 -1.4165234755955882e-18 4.5225760483770383 
		6.883752184935827 3.685807992696561 3.1979441922392646 9.7350956999516747 5.2125196515746364 
		1.3105293929004545e-15 6.8837521849358279 3.6858079926965628 -3.1979441922392633 
		1.9324154848663007e-15 2.1667365885867912e-15 -4.5225760483770392 -6.8837521849358225 
		-3.685807992696561 -3.1979441922392655 -9.7350956999516747 -5.2125196515746364 -2.4290844179282781e-15 
		0 0 0 0 0 0 0 0 0;
createNode transform -n "GRP_Anchor_Back_Tire_Control_L" -p "CNT_drive";
	rename -uid "5E73C9DD-4E17-AACF-EA49-B3886E9D519F";
	setAttr ".t" -type "double3" 20.153226852416992 17.507782756752441 -11.251556396483279 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1 1.0000000000000004 ;
createNode transform -n "GRP_Offset_Back_Tire_Control_L" -p "GRP_Anchor_Back_Tire_Control_L";
	rename -uid "825007C7-4694-1729-50A5-A6B38A380BCB";
createNode transform -n "CNT_Back_Tire_L" -p "GRP_Offset_Back_Tire_Control_L";
	rename -uid "3AB7E02C-48F7-8874-F1FB-9BAB4EEA3B6C";
	addAttr -ci true -sn "Tire_Spin_Rotation" -ln "Tire_Spin_Rotation" -min -1 -max 
		1 -at "double";
	setAttr -l on -k off ".v";
	setAttr -k on ".Tire_Spin_Rotation" 1;
createNode nurbsCurve -n "CNT_Back_Tire_LShape" -p "CNT_Back_Tire_L";
	rename -uid "3EB4B3A9-4057-B178-FD4B-2A93DDA2C881";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		4.0953539788952538e-15 -3.6858079926965654 -3.6858079926965601
		9.9949267636321841e-16 5.9468616594135344e-16 -5.2125196515746346
		-7.4460981434459239e-16 3.6858079926965619 -3.685807992696561
		-7.4460981434459239e-16 5.2125196515746364 -3.0135702623569669e-16
		-2.2338294430337752e-15 3.6858079926965623 3.6858079926965637
		-9.994926763632188e-16 1.5706338991698408e-15 5.2125196515746381
		1.1169147215168876e-15 -3.6858079926965615 3.6858079926965641
		7.4460981434459239e-16 -5.2125196515746364 4.008755234947802e-15
		4.0953539788952538e-15 -3.6858079926965654 -3.6858079926965601
		9.9949267636321841e-16 5.9468616594135344e-16 -5.2125196515746346
		-7.4460981434459239e-16 3.6858079926965619 -3.685807992696561
		;
createNode transform -n "GRP_Anchor_Front_Tire_Control_R" -p "CNT_drive";
	rename -uid "11BDB012-46D2-C72F-3550-7CAEB842765F";
	setAttr ".t" -type "double3" 48.206731796264641 17.507783201924632 10.712905883790169 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000004 ;
createNode transform -n "GRP_Offset_Front_Tire_Control_R" -p "GRP_Anchor_Front_Tire_Control_R";
	rename -uid "299B5199-48CB-0333-F07F-FF84CB76BA4E";
createNode transform -n "CNT_Front_Tire_R" -p "GRP_Offset_Front_Tire_Control_R";
	rename -uid "552791F1-4B2C-2010-ED80-DD9EA27DDEB0";
	addAttr -ci true -sn "Tire_Spin_Rotation" -ln "Tire_Spin_Rotation" -min -1 -max 
		1 -at "double";
	setAttr -l on -k off ".v";
	setAttr -k on ".Tire_Spin_Rotation" 1;
createNode nurbsCurve -n "CNT_Front_Tire_RShape" -p "CNT_Front_Tire_R";
	rename -uid "5F210556-4C88-6418-AB50-A0B67C2E7749";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.7763568394002505e-15 -3.6858079926965654 -3.6858079926965615
		9.9802651191750452e-16 5.9468616594135344e-16 -5.2125196515746364
		0 3.6858079926965619 -3.6858079926965623
		0 5.2125196515746364 -1.5104577880810204e-15
		-8.8817841970012523e-16 3.6858079926965623 3.6858079926965619
		-1.0009588408089305e-15 1.5706338991698416e-15 5.2125196515746373
		8.8817841970012523e-16 -3.6858079926965615 3.6858079926965623
		0 -5.2125196515746364 2.7996544731024783e-15
		1.7763568394002505e-15 -3.6858079926965654 -3.6858079926965615
		9.9802651191750452e-16 5.9468616594135344e-16 -5.2125196515746364
		0 3.6858079926965619 -3.6858079926965623
		;
createNode transform -n "GRP_Anchor_Back_Tire_Control_R" -p "CNT_drive";
	rename -uid "C4F54925-40F9-2D07-4D61-0F8E91A4B167";
	setAttr ".t" -type "double3" 20.153226852416992 17.507782756752441 11.251556396485471 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1 1.0000000000000004 ;
createNode transform -n "GRP_Offset_Back_Tire_Control_R" -p "GRP_Anchor_Back_Tire_Control_R";
	rename -uid "90141996-42EA-9F65-6186-0F893922D698";
createNode transform -n "CNT_Back_Tire_R" -p "GRP_Offset_Back_Tire_Control_R";
	rename -uid "1DA9C008-4245-7FBE-EB0A-D199328D1569";
	addAttr -ci true -sn "Tire_Spin_Rotation" -ln "Tire_Spin_Rotation" -min -1 -max 
		1 -at "double";
	setAttr -l on -k off ".v";
	setAttr -k on ".Tire_Spin_Rotation" 1;
createNode nurbsCurve -n "CNT_Back_Tire_RShape" -p "CNT_Back_Tire_R";
	rename -uid "9315280E-4E30-A011-0B84-28B3B42A49D8";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		4.0953539788952538e-15 -3.6858079926965654 -3.6858079926965601
		9.9949267636321841e-16 5.9468616594135344e-16 -5.2125196515746346
		-7.4460981434459239e-16 3.6858079926965619 -3.685807992696561
		-7.4460981434459239e-16 5.2125196515746364 -3.0135702623569669e-16
		-2.2338294430337752e-15 3.6858079926965623 3.6858079926965637
		-9.994926763632188e-16 1.5706338991698408e-15 5.2125196515746381
		1.1169147215168876e-15 -3.6858079926965615 3.6858079926965641
		7.4460981434459239e-16 -5.2125196515746364 4.008755234947802e-15
		4.0953539788952538e-15 -3.6858079926965654 -3.6858079926965601
		9.9949267636321841e-16 5.9468616594135344e-16 -5.2125196515746346
		-7.4460981434459239e-16 3.6858079926965619 -3.685807992696561
		;
createNode transform -n "GRP_CNT_Front_Tires" -p "CNT_drive";
	rename -uid "3D15B0EB-42D5-9F61-9B15-E19A7B0E0186";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".t" -type "double3" 64.627271648729618 13.348115633643339 1.1078333985669933e-12 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode transform -n "CNT_Front_Tires" -p "GRP_CNT_Front_Tires";
	rename -uid "75593B76-4969-B2A1-5C81-DA80B7C7A653";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".mntl" -type "double3" -5 -1 -1 ;
	setAttr ".mxtl" -type "double3" 5 1 1 ;
	setAttr ".mtxe" yes;
	setAttr ".xtxe" yes;
createNode nurbsCurve -n "CNT_Front_TiresShape" -p "CNT_Front_Tires";
	rename -uid "CB7ACE48-4615-6B32-B92A-848184D15A8C";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 10 0 no 3
		11 0 1 2 3 4 5 6 7 8 9 10
		11
		-8.2565018500429908 0 0
		-4.953901110025793 0 -3.3026007400171933
		-4.953901110025793 0 -1.6513003700085966
		4.953901110025793 0 -1.6513003700085966
		4.953901110025793 0 -3.3026007400171933
		8.2565018500429908 0 0
		4.953901110025793 0 3.3026007400171933
		4.953901110025793 0 1.6513003700085966
		-4.953901110025793 0 1.6513003700085966
		-4.953901110025793 0 3.3026007400171933
		-8.2565018500429908 0 0
		;
createNode joint -n "JNT_Moto_SideCar_Root";
	rename -uid "0203EDD3-430E-C249-6B9A-93B180360976";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-16 0 1.0000000000000002 0 0 1 0 0
		 -1.0000000000000002 0 2.2204460492503131e-16 0 -2.7755575615628914e-14 0 8.8817841970017462e-15 1;
	setAttr ".radi" 10;
createNode transform -n "GRP_Front_Tire_Joint_L" -p "JNT_Moto_SideCar_Root";
	rename -uid "15395B92-4DE2-1DF5-06F6-078515E6560F";
	setAttr ".rp" -type "double3" 34.020404840988583 22.140929222106934 -1.7377966889361636 ;
	setAttr ".rpt" -type "double3" 1.1128673300319261 0 1.7377966889361627 ;
	setAttr ".sp" -type "double3" 34.020404840988583 22.140929222106934 -1.7377966889361636 ;
createNode joint -n "JNT_Front_Tire_L" -p "GRP_Front_Tire_Joint_L";
	rename -uid "7D0BF772-46DE-61D3-F3BA-30AADD22ACD1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 34.020404840988583 22.140929222106934 -1.7377966889361638 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "none";
	setAttr ".jo" -type "double3" 0 89.999999999999986 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1.0000000000000007 0 -4.9303806576313238e-32 0 0 1.0000000000000002 0 0
		 -4.9303806576313238e-32 0 1.0000000000000004 0 10.712905883789064 4.1596675682812965 12.817541122436527 1;
	setAttr ".radi" 7;
createNode parentConstraint -n "GRP_Front_Tire_Joint_L_parentConstraint1" -p "GRP_Front_Tire_Joint_L";
	rename -uid "E699AA2C-400A-EBD7-0E63-9996A4F08E3D";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "CNT_Front_Tire_LW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 4.4408920985006262e-15 3.5527136788005009e-15 
		0 ;
	setAttr ".tg[0].tor" -type "double3" 0 -89.999999999999986 0 ;
	setAttr ".rst" -type "double3" -22.315731048583995 -17.981261653825637 -10.712905883789084 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "GRP_Front_Tire_Joint_L_scaleConstraint1" -p "GRP_Front_Tire_Joint_L";
	rename -uid "62389D24-452F-01A2-6938-E4BF0D080B8E";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "CNT_Front_Tire_LW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.99999999999999978 1 0.99999999999999978 ;
	setAttr -k on ".w0";
createNode transform -n "GRP_Back_Tire_Joint_L" -p "JNT_Moto_SideCar_Root";
	rename -uid "831C3793-4F84-4020-D473-2396E9490EEE";
	setAttr ".rp" -type "double3" -28.79029464721679 12.609360694885254 7.1054273576010019e-15 ;
	setAttr ".sp" -type "double3" -28.79029464721679 12.609360694885254 7.1054273576010019e-15 ;
createNode joint -n "JNT_Back_Tire_L" -p "GRP_Back_Tire_Joint_L";
	rename -uid "77DECEE0-4C67-2F01-6A69-BD9F176AE2A5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -28.79029464721679 12.609360694885254 6.8368188104665567e-15 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1.0000000000000007 0 0 0 0 1 0 0 0 0 1.0000000000000007 0
		 11.251556396484379 4.1596671231091022 -15.235963821411129 1;
	setAttr ".radi" 7;
createNode parentConstraint -n "GRP_Back_Tire_Joint_L_parentConstraint1" -p "GRP_Back_Tire_Joint_L";
	rename -uid "9589E72C-4329-4DF2-8CE2-68AA3EC60BCF";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "CNT_Back_Tire_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -1.4200079609079887e-15 0 7.1054273576010019e-15 ;
	setAttr ".tg[0].tor" -type "double3" 0 -89.999999999999986 0 ;
	setAttr ".rst" -type "double3" 13.554330825805657 -8.4496935717761517 -11.251556396484412 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "GRP_Back_Tire_Joint_L_scaleConstraint1" -p "GRP_Back_Tire_Joint_L";
	rename -uid "B0E92E33-440C-19CE-7BDA-9AA13F397C6C";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "CNT_Back_Tire_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.99999999999999978 1 0.99999999999999978 ;
	setAttr -k on ".w0";
createNode transform -n "GRP_Front_Tire_Joint_R" -p "JNT_Moto_SideCar_Root";
	rename -uid "E39A7F64-492D-BE1E-0C5B-569D1E5D1A97";
	setAttr ".rp" -type "double3" 34.020404840988583 22.140929222106934 -1.7377966889361636 ;
	setAttr ".rpt" -type "double3" 1.1128673300319261 0 1.7377966889361627 ;
	setAttr ".sp" -type "double3" 34.020404840988583 22.140929222106934 -1.7377966889361636 ;
createNode joint -n "JNT_Front_Tire_R" -p "GRP_Front_Tire_Joint_R";
	rename -uid "5603D4A4-4403-F885-6D33-92B931451C50";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 34.020404840988583 22.140929222106934 -1.7377966889361638 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "none";
	setAttr ".jo" -type "double3" 0 89.999999999999986 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1.0000000000000007 0 -4.9303806576313238e-32 0 0 1.0000000000000002 0 0
		 -4.9303806576313238e-32 0 1.0000000000000004 0 -10.712905883789063 4.1596675682812929 12.81754112243652 1;
	setAttr ".radi" 7;
createNode parentConstraint -n "GRP_Front_Tire_Joint_R_parentConstraint1" -p "GRP_Front_Tire_Joint_R";
	rename -uid "A9E41505-4C08-FF43-465A-DEAD58F2AECA";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "CNT_Front_Tire_RW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 4.6629367034256575e-15 0 0 ;
	setAttr ".tg[0].tor" -type "double3" 0 -89.999999999999986 0 ;
	setAttr ".rst" -type "double3" -22.315731048584006 -17.981261653825641 10.712905883789034 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "GRP_Front_Tire_Joint_R_scaleConstraint1" -p "GRP_Front_Tire_Joint_R";
	rename -uid "EF632300-48E7-86C5-9689-E7B37BBEE756";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "CNT_Front_Tire_RW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.99999999999999978 1 0.99999999999999978 ;
	setAttr -k on ".w0";
createNode transform -n "GRP_Back_Tire_Joint_R" -p "JNT_Moto_SideCar_Root";
	rename -uid "7F26F871-4031-0D46-5F88-34A019138C38";
	setAttr ".rp" -type "double3" -28.79029464721679 12.609360694885254 7.1054273576010019e-15 ;
	setAttr ".sp" -type "double3" -28.79029464721679 12.609360694885254 7.1054273576010019e-15 ;
createNode joint -n "JNT_Back_Tire_R" -p "GRP_Back_Tire_Joint_R";
	rename -uid "37A0F61A-4535-1300-EFA5-55832E16B086";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -28.79029464721679 12.609360694885254 6.8368188104665567e-15 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1.0000000000000007 0 0 0 0 1 0 0 0 0 1.0000000000000007 0
		 -11.251556396484371 4.1596671231091005 -15.235963821411133 1;
	setAttr ".radi" 7;
createNode parentConstraint -n "GRP_Back_Tire_Joint_R_parentConstraint1" -p "GRP_Back_Tire_Joint_R";
	rename -uid "6DB665A8-4501-1EFA-C422-80A10DF706BC";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "CNT_Back_Tire_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -1.0636590824157364e-15 -1.7763568394002505e-15 
		-3.5527136788005009e-15 ;
	setAttr ".tg[0].tor" -type "double3" 0 -89.999999999999986 0 ;
	setAttr ".rst" -type "double3" 13.55433082580565 -8.4496935717761534 11.251556396484334 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "GRP_Back_Tire_Joint_R_scaleConstraint1" -p "GRP_Back_Tire_Joint_R";
	rename -uid "901AE222-4904-54FC-FE10-7CA301ABDE6D";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "CNT_Back_Tire_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.99999999999999978 1 0.99999999999999978 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "JNT_Moto_SideCar_Root_parentConstraint1" -p "JNT_Moto_SideCar_Root";
	rename -uid "8C3221CB-4DD0-F7E2-7AC2-98A76ABA0668";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "drive_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -29.23808097839354 -7.1675037193191669 1.1290968160437842e-12 ;
	setAttr ".lr" -type "double3" 0 -90 0 ;
	setAttr ".rst" -type "double3" -2.7755575615628914e-14 8.8817841970012523e-16 1.4210854715202004e-14 ;
	setAttr ".rsrr" -type "double3" 0 -89.999999999999986 0 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "JNT_Moto_SideCar_Root_scaleConstraint1" -p "JNT_Moto_SideCar_Root";
	rename -uid "E49E998B-4735-2508-8945-3EAA5F29DE0E";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "drive_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode transform -n "GRP_locDrive";
	rename -uid "1989267F-4BEC-5594-7844-20AF076B50E1";
createNode transform -n "LOC_drive" -p "GRP_locDrive";
	rename -uid "E7C2EBB5-424C-A894-15FD-FF8D0EF712AE";
createNode locator -n "LOC_driveShape" -p "LOC_drive";
	rename -uid "3A6F79E7-4FA1-37E0-0986-19ADC51F560F";
	setAttr -k off ".v";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "3F4CE6FE-44F4-355E-528F-ECAE2227D195";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode VRaySettingsNode -s -n "vraySettings";
	rename -uid "505B6A08-4006-50DE-FC2A-978740B28896";
	setAttr ".gi" yes;
	setAttr ".rfc" yes;
	setAttr ".pe" 2;
	setAttr ".se" 3;
	setAttr ".cmph" 60;
	setAttr ".cfile" -type "string" "";
	setAttr ".cfile2" -type "string" "";
	setAttr ".casf" -type "string" "";
	setAttr ".casf2" -type "string" "";
	setAttr ".st" 3;
	setAttr ".msr" 6;
	setAttr ".aaft" 3;
	setAttr ".aafs" 2;
	setAttr ".dma" 24;
	setAttr ".dam" 1;
	setAttr ".pt" 0.0099999997764825821;
	setAttr ".pmt" 0;
	setAttr ".sd" 1000;
	setAttr ".ss" 0.01;
	setAttr ".pfts" 20;
	setAttr ".ufg" yes;
	setAttr ".fnm" -type "string" "";
	setAttr ".lcfnm" -type "string" "";
	setAttr ".asf" -type "string" "";
	setAttr ".lcasf" -type "string" "";
	setAttr ".urtrshd" yes;
	setAttr ".rtrshd" 2;
	setAttr ".lightCacheType" 1;
	setAttr ".ifile" -type "string" "";
	setAttr ".ifile2" -type "string" "";
	setAttr ".iasf" -type "string" "";
	setAttr ".iasf2" -type "string" "";
	setAttr ".pmfile" -type "string" "";
	setAttr ".pmfile2" -type "string" "";
	setAttr ".pmasf" -type "string" "";
	setAttr ".pmasf2" -type "string" "";
	setAttr ".dmcstd" yes;
	setAttr ".dmculs" no;
	setAttr ".dmcsat" 0.004999999888241291;
	setAttr ".cmtp" 6;
	setAttr ".cmao" 2;
	setAttr ".cg" 2.2000000476837158;
	setAttr ".mtah" yes;
	setAttr ".rgbcs" -1;
	setAttr ".suv" 0;
	setAttr ".srflc" 1;
	setAttr ".srdml" 0;
	setAttr ".seu" yes;
	setAttr ".gormio" yes;
	setAttr ".gocle" yes;
	setAttr ".gopl" 2;
	setAttr ".wi" 960;
	setAttr ".he" 540;
	setAttr ".aspr" 1.7777780294418335;
	setAttr ".productionGPUResizeTextures" 0;
	setAttr ".autolt" 0;
	setAttr ".jpegq" 100;
	setAttr ".vfbOn" yes;
	setAttr ".vfbSA" -type "Int32Array" 847 124 20 0 0 0 0
		 0 0 0 0 -1073724351 0 0 0 0 0 0 0
		 0 0 0 -1074790400 0 -1074790400 0 -1074790400 0 -1074790400 1 0
		 0 0 3253 1 3241 0 1 3233 1700143739 1869181810 825893486 1632379436
		 1936876921 578501154 1936876886 577662825 573321530 1935764579 574235251 1953460082 1881287714 1701867378 1701409906 2067407475
		 1919252002 1852795251 741423650 1835101730 574235237 1696738338 1818386798 1949966949 744846706 1886938402 577007201 1818322490
		 573334899 1634760805 1650549870 975332716 1702195828 1931619453 1814913653 1919252833 1530536563 1818436219 577991521 1751327290
		 779317089 1886611812 1132028268 1701999215 1869182051 573317742 1886351984 1769239141 975336293 1702240891 1869181810 825893486
		 1634607660 975332717 1936278562 2036427888 1919894304 1952671090 577662825 1852121644 1701601889 1920219682 573334901 1634760805
		 975332462 1702195828 2019893804 1684955504 1701601889 1920219682 573334901 1718579824 577072233 573321530 1869641829 1701999987
		 774912546 1763847216 1717527395 577072233 740434490 1667459362 1852142175 1953392996 578055781 573321274 1886088290 1852793716
		 1715085942 1702063201 1668227628 1717530473 577072233 740434490 1768124194 1868783471 1936879468 1701011824 741358114 1768124194
		 1768185711 1634496627 1986356345 577069929 573321274 1869177711 1701410399 1634890871 1868985198 975334770 1864510512 1601136995
		 1702257011 1835626089 577070945 1818322490 746415475 1651864354 2036427821 577991269 578509626 1935764579 574235251 1868654691
		 1701981811 1869819494 1701016181 1684828006 740455013 1869770786 1953654128 577987945 1981971258 1769173605 975335023 1847733297
		 577072481 1867719226 1701016181 1196564538 573317698 1650552421 975332716 1702195828 2019893804 1684955504 1634089506 744846188
		 1886938402 1633971809 577072226 1818322490 573334899 1667330159 578385001 808333626 1818370604 1600417381 1701080941 741358114
		 1668444962 1887007839 809116261 1931619453 1814913653 1919252833 1530536563 1818436219 577991521 1751327290 779317089 778462578
		 1751607660 2020175220 1881287714 1701867378 1701409906 2067407475 1919252002 1852795251 741423650 1835101730 574235237 1751607628
		 2020167028 1696738338 1818386798 1715085925 1702063201 2019893804 1684955504 1634089506 744846188 1886938402 1633971809 577072226
		 1970435130 573341029 761427315 1702453612 975336306 746413403 1818436219 577991521 1751327290 779317089 778462578 1886220131
		 1953067887 573317733 1886351984 1769239141 975336293 1702240891 1869181810 825893486 1634607660 975332717 1836008226 1769172848
		 740451700 1634624802 577072226 1818322490 573334899 1634760805 975332462 1936482662 1696738405 1851879544 1818386788 1949966949
		 744846706 1634758434 2037672291 774978082 1646406704 1684956524 1685024095 809116261 1931619453 1814913653 1919252833 1530536563
		 2103278941 1663204140 1936941420 1663187490 1936679272 778399790 1869505892 1919251305 1881287714 1701867378 1701409906 2067407475
		 1919252002 1852795251 741423650 1835101730 574235237 1869505860 1919251305 1853169722 1767994977 1818386796 573317733 1650552421
		 975332716 1936482662 1696738405 1851879544 1715085924 1702063201 2019893804 1684955504 1701601889 1920219682 573334901 1667330159
		 578385001 808333626 1818370604 1600417381 1701080941 741358114 1952669986 577074793 1818322490 573334899 1936028272 975336549
		 1931619378 1852142196 577270887 808333626 1634869804 1937074532 808532514 573321262 1665234792 1701602659 1702125938 1920219682
		 573334901 1869505892 1919251305 1685024095 825893477 1931619453 1814913653 1919252833 1530536563 2066513245 1634493218 975336307
		 1634231074 1882092399 1701588581 2019980142 1881287714 1701867378 1701409906 2067407475 1919252002 1852795251 741423650 1835101730
		 574235237 1936614732 1717978400 1937007461 1696738338 1818386798 1949966949 744846706 1886938402 577007201 1818322490 573334899
		 1634760805 1650549870 975332716 1702195828 1886331436 1953063777 825893497 573321262 1852140642 1869438820 975332708 1730292786
		 1701994860 577662815 1818322490 573334899 1918987367 1769168741 975332730 808333363 1818698284 1600483937 1734960503 975336552
		 741355057 1869373986 2002742639 1751607653 809116276 808465454 808464432 909718832 1818698284 1600483937 1701996660 1819240563
		 825893476 573321262 1953261926 1918857829 1952543855 577662825 808333370 1634935340 1634891124 1852795252 774978082 1747070000
		 2003071585 1600483937 1701012321 1634887020 577004916 1970435130 1663183973 1600416879 1836212599 1634089506 744846188 1953392930
		 1667330661 1702259060 1920219682 573334901 1650552421 1650419052 1701077356 1949966963 744846706 1684632354 975336293 1646406710
		 1701077356 1869766515 1769234804 975335023 808334641 1953702444 1801545074 1970037343 809116274 808464942 808464432 943272496
		 1937056300 1919377253 1852404833 1715085927 1702063201 1919361580 1852404833 1701076839 1953067886 893002361 741355056 1634887458
		 1735289204 1852140639 577270887 774910266 1730292784 1769234802 2053072750 577597295 808334650 1919361580 1852404833 1819500391
		 577073263 808333370 1919361580 1852404833 1953718119 1735288178 975333492 741355057 1702065442 1667460959 1769174380 975335023
		 1936482662 1864510565 1970037603 1852795251 1836675935 1920230765 975332201 1702195828 1668227628 1937075299 1601073001 1668441456
		 578055781 774910522 1864510512 1970037603 1852795251 1953460831 1869182049 809116270 573321262 1818452847 1869181813 1918984046
		 825893475 808333360 1937056300 1668505445 1668571506 1715085928 1702063201 1668489772 2037604210 1952804205 576940402 1970435130
		 1931619429 1885303395 1702130785 975335026 1931619376 1834971747 1769237621 1918987367 1868783461 578055797 573321530 1601332083
		 1936614756 578385001 774911290 1931619376 1818194531 1952935525 893002344 741355056 1919120162 1869378399 1985963376 1634300513
		 577069934 808333370 1668489772 1769430898 1600681060 1769103734 1701015137 774912546 1931619376 1935635043 577004901 573321274
		 1601332083 1836019578 775043618 1931619376 1918857827 1952543855 577662825 808333370 1668489772 1953718130 1735288178 975333492
		 741355057 1702065442 1937073247 1715085940 1702063201 1969496620 1885303923 1702130785 975335026 1679961136 1601467253 1936614756
		 578385001 774911290 1679961136 1601467253 1768186226 1985966965 1634300513 577069934 808333370 1969496620 1784640627 1702130793
		 809116274 573321262 1953723748 1869576799 842670701 573321262 1953723748 1953460831 1869182049 809116270 573321262 1953723748
		 1920234335 1952935525 825893480 573321262 1918987367 1937071973 1651466085 1667331187 1767859564 1701273965 1634089506 744846188
		 1634494242 1868522866 1635021666 1600482403 1734438249 1634754405 975333492 573317666 1953718895 1634560351 2053072231 577597295
		 808333626 1651450412 1767863411 1701273965 1953460831 1869182049 809116270 573321262 1953718895 1634560351 1935631719 1852142196
		 577270887 808333626 1937056300 1768316773 1919251564 1634560351 975332711 1936482662 1730292837 1701994860 1634560351 1885300071
		 577270881 2099388986 1970479660 1634479458 1936876921 1566259746 1568497021 1377971325 1869178725 1852131182 578501154 1936876918
		 577662825 573321530 1937076077 1868980069 2003790956 1634624863 1684368482 1634089506 744846188 1970236706 1717527923 1869376623
		 1869635447 1601465961 1801678700 975332453 1936482662 1830956133 1702065519 1819240031 1601662828 1852403568 578314100 573321274
		 1937076077 1868980069 2003790956 1768910943 2036298862 2100312610 1699881516 1919247470 2003134806 578501154 1936876918 577662825
		 573321530 1650552421 1918854508 1701080677 1701994354 1852795239 1634089506 744846188 1852142114 1601332580 1768383858 2019520111
		 758784560 741355057 1852142114 1601332580 1768383858 2036297327 758784560 741355057 1852142114 1601332580 1768383858 2019520111
		 758784561 741355057 1852142114 1601332580 1768383858 2036297327 758784561 741355057 1701410338 1701994359 1949966948 744846706
		 1701410338 1919377271 577660261 1970435130 1981951077 1601660265 1702194274 1920219682 573334901 2003134838 1852796255 1715085935
		 1702063201 1868767788 1601335148 1835101283 1869438832 975332708 573323574 1869377379 1818451826 1601203553 975335023 1702195828
		 1768956460 1600939384 1868983913 1668246623 577004907 1818322490 573334899 1702390128 1852399468 1667198822 1701999215 1684370531
		 1819239263 577991279 1818322490 746415475 1952661794 1936617321 578501154 1936876918 577662825 573321530 1937072496 1380993381
		 1701339999 1684368227 1634089506 744846188 1702065442 1702390096 1886601580 1601463141 1667590243 577004907 1818322490 573334899
		 1953719668 1601398098 1768186226 893002351 1948396593 1383363429 1667199845 1801676136 975332453 1936482662 1679961189 1735746149
		 1684105299 1600613993 1768186226 893002351 1679961142 1735746149 1684105299 1600613993 1667590243 577004907 1818322490 573334899
		 1919251571 1867345765 1918854500 1869177953 943143458 1953702444 1868919397 1701080909 1701339999 1684368227 1634089506 2103800684
		 125 ;
	setAttr ".mSceneName" -type "string" "D:/IMPORTANTE/wheels_base_rigNewScript.ma";
	setAttr ".rt_cpuRayBundleSize" 4;
	setAttr ".rt_gpuRayBundleSize" 128;
	setAttr ".rt_maxPaths" 10000;
	setAttr ".rt_engineType" 3;
	setAttr ".rt_gpuResizeTextures" 0;
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "4B19C599-4912-6CC6-B921-B49186154E9D";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "B83CDE7C-473C-83E9-2F59-F28D64A2527B";
createNode displayLayerManager -n "layerManager";
	rename -uid "54C963CA-492E-8663-9AFA-4F84BE7A69E0";
createNode displayLayer -n "defaultLayer";
	rename -uid "A4D3DF95-4980-372D-25D1-CCB6F961CE37";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "178765C5-44BA-2E25-811C-6C8CF1B4642E";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "0280973B-4F33-6567-8914-0EB4A7300FF7";
	setAttr ".g" yes;
createNode multiplyDivide -n "MLD_scale_radius";
	rename -uid "22C5C0BE-42C6-7C6F-28DD-6F891DFD6913";
	setAttr ".i1" -type "float3" 4.1500001 0 0 ;
createNode expression -n "EXP_Rotate_Tires";
	rename -uid "84CE6645-49E0-EB6F-46FA-25B7A96BB1E9";
	setAttr -k on ".nds";
	setAttr -s 8 ".in[5:7]"  0 0 0;
	setAttr -s 5 ".in";
	setAttr -s 4 ".out";
	setAttr ".ixp" -type "string" (
		"vector $vPos = << .O[0], .O[1], .O[2] >>;\nfloat $distance = 0.0;\nint $direction = 1;\nvector $vPosChange = `getAttr CNT_drive.translate`;\nfloat $FirstFrame = `findKeyframe -time 0 -which first CNT_drive`;\nfloat $cx = $vPosChange.x - $vPos.x;\nfloat $cy = $vPosChange.y - $vPos.y;\nfloat $cz = $vPosChange.z - $vPos.z;\nfloat $distance = sqrt( `pow $cx 2` + `pow $cy 2` + `pow $cz 2` );\nif ( ( $vPosChange.x == $vPos.x ) && ( $vPosChange.y != $vPos.y ) && ( $vPosChange.z == $vPos.z ) ){}\nelse {\n    float $angle = .I[0]%360;\n    if ( $angle == 0 ){ \n        if ( $vPosChange.x > $vPos.x ) \n            $direction = -1;\n        else \n            $direction=1;}\n\n        if (( $angle > 0 && $angle < 90 ) || ( $angle > 270 && $angle < 360 ) || ( $angle < 0 && $angle > -90 ) || ( $angle < -270 && $angle > -360 )) {\n        if ( $vPosChange.x > $vPos.x )\n            $direction = -1 * $direction;\n        else\n            $direction = 1 * $direction;}\n\n\n    if (( $angle <-90 && $angle > -270 ) || ( $angle > 90 && $angle < 270 ) ) { \n"
		+ "        if ( $vPosChange.x > $vPos.x ) \n            $direction = 1 * $direction;\n        else \n            $direction = -1* $direction; }\n\n\n    if (( $angle==-90) || ( $angle==270) ) { \n        if ( $vPosChange.z > $vPos.z ) \n            $direction = -1 ;\n        else \n            $direction = 1 ; \n        }\n\n\n    if (( $angle==90) || ( $angle==-270) ) { \n        if ( $vPosChange.z > $vPos.z ) \n            $direction = 1 ;\n        else \n            $direction = -1 ; \n        }\n\n    \n    .O[3] = .O[3] + ( ( $direction * ( ( $distance / ( 6.283185 * .I[1] ) ) * -360.0 ) ) ); }\n.O[0] = .I[2];\n.O[1] = .I[3];\n.O[2] = .I[4];\n\nif (frame == $FirstFrame) {\n    .O[3] = 0;\n}\n\nelse if ($FirstFrame == 0) {\n    .O[3] = 0;\n}");
createNode unitConversion -n "unitConversion44";
	rename -uid "F69C9C76-4CF7-D770-4776-C3A875F76381";
	setAttr ".cf" 0.017453292519943295;
createNode setRange -n "STR_Front_Tires";
	rename -uid "02D8BE2B-4907-4749-40CD-1E9C2F850DCD";
	setAttr ".n" -type "float3" -18 0 0 ;
	setAttr ".m" -type "float3" 18 0 0 ;
	setAttr ".on" -type "float3" -5 0 0 ;
	setAttr ".om" -type "float3" 5 0 0 ;
createNode makeNurbCircle -n "makeNurbCircle4";
	rename -uid "60184B15-4D1D-A7FA-BA52-B293F6B51920";
	setAttr ".nr" -type "double3" 0 1 0 ;
	setAttr ".r" 8.7846478616130579;
createNode unitConversion -n "unitConversion45";
	rename -uid "E018D8DC-4EBF-2DD0-5758-4FA26F857EB2";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion39";
	rename -uid "662E9945-47C1-E5C5-785C-4F9F266E1610";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "MLP_FrontTire_Rotation";
	rename -uid "9702EAD2-4EA5-4BF3-42F9-9EB0E2F311FD";
createNode unitConversion -n "unitConversion41";
	rename -uid "4C33AD9F-4356-6287-096B-EDB3C3387EA7";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "MLP_BackTire_Rotation";
	rename -uid "AA4AEC8D-456A-4B28-3619-8C9C35BB2339";
createNode unitConversion -n "unitConversion42";
	rename -uid "614B8E49-4DE2-99C0-0967-AAAAC9DFE11E";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion43";
	rename -uid "B4451D4C-40FE-78BA-0E18-0A940A902A69";
	setAttr ".cf" 0.017453292519943295;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "9C450734-4611-5DD9-973D-AA9CA50BD807";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n"
		+ "            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1534\n            -height 1066\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n"
		+ "            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -selectCommand \"print(\\\"\\\")\" \n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n"
		+ "            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n"
		+ "            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n"
		+ "            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n"
		+ "                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n"
		+ "                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -autoFitTime 0\n"
		+ "                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n"
		+ "                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n"
		+ "                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n"
		+ "\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n"
		+ "                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n"
		+ "                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n{ string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n"
		+ "                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 32768\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n"
		+ "                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n"
		+ "                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName; };\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n"
		+ "\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1534\\n    -height 1066\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1534\\n    -height 1066\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "9C3B15A8-4ADC-D24B-C686-029EE84A8854";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 125 -ast 1 -aet 208.3333333333 ";
	setAttr ".st" 6;
createNode unitConversion -n "unitConversion46";
	rename -uid "19E67233-4463-95C3-3B0C-BC83E638B189";
	setAttr ".cf" 57.295779513082323;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 4 ".u";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "MLD_scale_radius.ox" "CNT_drive.Radius" -l on;
connectAttr "EXP_Rotate_Tires.out[3]" "CNT_drive.Drive";
connectAttr "CNT_driveShapeOrig.ws" "CNT_driveShape.cr";
connectAttr "unitConversion44.o" "GRP_Offset_Front_Tire_Control_L.ry";
connectAttr "makeNurbCircle4.oc" "CNT_Front_Tire_LShape.cr";
connectAttr "unitConversion45.o" "GRP_Offset_Front_Tire_Control_R.ry";
connectAttr "JNT_Moto_SideCar_Root_scaleConstraint1.csx" "JNT_Moto_SideCar_Root.sx"
		;
connectAttr "JNT_Moto_SideCar_Root_scaleConstraint1.csy" "JNT_Moto_SideCar_Root.sy"
		;
connectAttr "JNT_Moto_SideCar_Root_scaleConstraint1.csz" "JNT_Moto_SideCar_Root.sz"
		;
connectAttr "JNT_Moto_SideCar_Root_parentConstraint1.ctx" "JNT_Moto_SideCar_Root.tx"
		;
connectAttr "JNT_Moto_SideCar_Root_parentConstraint1.cty" "JNT_Moto_SideCar_Root.ty"
		;
connectAttr "JNT_Moto_SideCar_Root_parentConstraint1.ctz" "JNT_Moto_SideCar_Root.tz"
		;
connectAttr "JNT_Moto_SideCar_Root_parentConstraint1.crx" "JNT_Moto_SideCar_Root.rx"
		;
connectAttr "JNT_Moto_SideCar_Root_parentConstraint1.cry" "JNT_Moto_SideCar_Root.ry"
		;
connectAttr "JNT_Moto_SideCar_Root_parentConstraint1.crz" "JNT_Moto_SideCar_Root.rz"
		;
connectAttr "GRP_Front_Tire_Joint_L_parentConstraint1.ctx" "GRP_Front_Tire_Joint_L.tx"
		;
connectAttr "GRP_Front_Tire_Joint_L_parentConstraint1.cty" "GRP_Front_Tire_Joint_L.ty"
		;
connectAttr "GRP_Front_Tire_Joint_L_parentConstraint1.ctz" "GRP_Front_Tire_Joint_L.tz"
		;
connectAttr "GRP_Front_Tire_Joint_L_parentConstraint1.crx" "GRP_Front_Tire_Joint_L.rx"
		;
connectAttr "GRP_Front_Tire_Joint_L_parentConstraint1.cry" "GRP_Front_Tire_Joint_L.ry"
		;
connectAttr "GRP_Front_Tire_Joint_L_parentConstraint1.crz" "GRP_Front_Tire_Joint_L.rz"
		;
connectAttr "GRP_Front_Tire_Joint_L_scaleConstraint1.csx" "GRP_Front_Tire_Joint_L.sx"
		;
connectAttr "GRP_Front_Tire_Joint_L_scaleConstraint1.csy" "GRP_Front_Tire_Joint_L.sy"
		;
connectAttr "GRP_Front_Tire_Joint_L_scaleConstraint1.csz" "GRP_Front_Tire_Joint_L.sz"
		;
connectAttr "JNT_Moto_SideCar_Root.s" "JNT_Front_Tire_L.is";
connectAttr "unitConversion39.o" "JNT_Front_Tire_L.rx";
connectAttr "GRP_Front_Tire_Joint_L.ro" "GRP_Front_Tire_Joint_L_parentConstraint1.cro"
		;
connectAttr "GRP_Front_Tire_Joint_L.pim" "GRP_Front_Tire_Joint_L_parentConstraint1.cpim"
		;
connectAttr "GRP_Front_Tire_Joint_L.rp" "GRP_Front_Tire_Joint_L_parentConstraint1.crp"
		;
connectAttr "GRP_Front_Tire_Joint_L.rpt" "GRP_Front_Tire_Joint_L_parentConstraint1.crt"
		;
connectAttr "CNT_Front_Tire_L.t" "GRP_Front_Tire_Joint_L_parentConstraint1.tg[0].tt"
		;
connectAttr "CNT_Front_Tire_L.rp" "GRP_Front_Tire_Joint_L_parentConstraint1.tg[0].trp"
		;
connectAttr "CNT_Front_Tire_L.rpt" "GRP_Front_Tire_Joint_L_parentConstraint1.tg[0].trt"
		;
connectAttr "CNT_Front_Tire_L.r" "GRP_Front_Tire_Joint_L_parentConstraint1.tg[0].tr"
		;
connectAttr "CNT_Front_Tire_L.ro" "GRP_Front_Tire_Joint_L_parentConstraint1.tg[0].tro"
		;
connectAttr "CNT_Front_Tire_L.s" "GRP_Front_Tire_Joint_L_parentConstraint1.tg[0].ts"
		;
connectAttr "CNT_Front_Tire_L.pm" "GRP_Front_Tire_Joint_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "GRP_Front_Tire_Joint_L_parentConstraint1.w0" "GRP_Front_Tire_Joint_L_parentConstraint1.tg[0].tw"
		;
connectAttr "GRP_Front_Tire_Joint_L.pim" "GRP_Front_Tire_Joint_L_scaleConstraint1.cpim"
		;
connectAttr "CNT_Front_Tire_L.s" "GRP_Front_Tire_Joint_L_scaleConstraint1.tg[0].ts"
		;
connectAttr "CNT_Front_Tire_L.pm" "GRP_Front_Tire_Joint_L_scaleConstraint1.tg[0].tpm"
		;
connectAttr "GRP_Front_Tire_Joint_L_scaleConstraint1.w0" "GRP_Front_Tire_Joint_L_scaleConstraint1.tg[0].tw"
		;
connectAttr "GRP_Back_Tire_Joint_L_parentConstraint1.ctx" "GRP_Back_Tire_Joint_L.tx"
		;
connectAttr "GRP_Back_Tire_Joint_L_parentConstraint1.cty" "GRP_Back_Tire_Joint_L.ty"
		;
connectAttr "GRP_Back_Tire_Joint_L_parentConstraint1.ctz" "GRP_Back_Tire_Joint_L.tz"
		;
connectAttr "GRP_Back_Tire_Joint_L_parentConstraint1.crx" "GRP_Back_Tire_Joint_L.rx"
		;
connectAttr "GRP_Back_Tire_Joint_L_parentConstraint1.cry" "GRP_Back_Tire_Joint_L.ry"
		;
connectAttr "GRP_Back_Tire_Joint_L_parentConstraint1.crz" "GRP_Back_Tire_Joint_L.rz"
		;
connectAttr "GRP_Back_Tire_Joint_L_scaleConstraint1.csx" "GRP_Back_Tire_Joint_L.sx"
		;
connectAttr "GRP_Back_Tire_Joint_L_scaleConstraint1.csy" "GRP_Back_Tire_Joint_L.sy"
		;
connectAttr "GRP_Back_Tire_Joint_L_scaleConstraint1.csz" "GRP_Back_Tire_Joint_L.sz"
		;
connectAttr "JNT_Moto_SideCar_Root.s" "JNT_Back_Tire_L.is";
connectAttr "unitConversion41.o" "JNT_Back_Tire_L.rx";
connectAttr "GRP_Back_Tire_Joint_L.ro" "GRP_Back_Tire_Joint_L_parentConstraint1.cro"
		;
connectAttr "GRP_Back_Tire_Joint_L.pim" "GRP_Back_Tire_Joint_L_parentConstraint1.cpim"
		;
connectAttr "GRP_Back_Tire_Joint_L.rp" "GRP_Back_Tire_Joint_L_parentConstraint1.crp"
		;
connectAttr "GRP_Back_Tire_Joint_L.rpt" "GRP_Back_Tire_Joint_L_parentConstraint1.crt"
		;
connectAttr "CNT_Back_Tire_L.t" "GRP_Back_Tire_Joint_L_parentConstraint1.tg[0].tt"
		;
connectAttr "CNT_Back_Tire_L.rp" "GRP_Back_Tire_Joint_L_parentConstraint1.tg[0].trp"
		;
connectAttr "CNT_Back_Tire_L.rpt" "GRP_Back_Tire_Joint_L_parentConstraint1.tg[0].trt"
		;
connectAttr "CNT_Back_Tire_L.r" "GRP_Back_Tire_Joint_L_parentConstraint1.tg[0].tr"
		;
connectAttr "CNT_Back_Tire_L.ro" "GRP_Back_Tire_Joint_L_parentConstraint1.tg[0].tro"
		;
connectAttr "CNT_Back_Tire_L.s" "GRP_Back_Tire_Joint_L_parentConstraint1.tg[0].ts"
		;
connectAttr "CNT_Back_Tire_L.pm" "GRP_Back_Tire_Joint_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "GRP_Back_Tire_Joint_L_parentConstraint1.w0" "GRP_Back_Tire_Joint_L_parentConstraint1.tg[0].tw"
		;
connectAttr "GRP_Back_Tire_Joint_L.pim" "GRP_Back_Tire_Joint_L_scaleConstraint1.cpim"
		;
connectAttr "CNT_Back_Tire_L.s" "GRP_Back_Tire_Joint_L_scaleConstraint1.tg[0].ts"
		;
connectAttr "CNT_Back_Tire_L.pm" "GRP_Back_Tire_Joint_L_scaleConstraint1.tg[0].tpm"
		;
connectAttr "GRP_Back_Tire_Joint_L_scaleConstraint1.w0" "GRP_Back_Tire_Joint_L_scaleConstraint1.tg[0].tw"
		;
connectAttr "GRP_Front_Tire_Joint_R_parentConstraint1.ctx" "GRP_Front_Tire_Joint_R.tx"
		;
connectAttr "GRP_Front_Tire_Joint_R_parentConstraint1.cty" "GRP_Front_Tire_Joint_R.ty"
		;
connectAttr "GRP_Front_Tire_Joint_R_parentConstraint1.ctz" "GRP_Front_Tire_Joint_R.tz"
		;
connectAttr "GRP_Front_Tire_Joint_R_parentConstraint1.crx" "GRP_Front_Tire_Joint_R.rx"
		;
connectAttr "GRP_Front_Tire_Joint_R_parentConstraint1.cry" "GRP_Front_Tire_Joint_R.ry"
		;
connectAttr "GRP_Front_Tire_Joint_R_parentConstraint1.crz" "GRP_Front_Tire_Joint_R.rz"
		;
connectAttr "GRP_Front_Tire_Joint_R_scaleConstraint1.csx" "GRP_Front_Tire_Joint_R.sx"
		;
connectAttr "GRP_Front_Tire_Joint_R_scaleConstraint1.csy" "GRP_Front_Tire_Joint_R.sy"
		;
connectAttr "GRP_Front_Tire_Joint_R_scaleConstraint1.csz" "GRP_Front_Tire_Joint_R.sz"
		;
connectAttr "JNT_Moto_SideCar_Root.s" "JNT_Front_Tire_R.is";
connectAttr "unitConversion42.o" "JNT_Front_Tire_R.rx";
connectAttr "GRP_Front_Tire_Joint_R.ro" "GRP_Front_Tire_Joint_R_parentConstraint1.cro"
		;
connectAttr "GRP_Front_Tire_Joint_R.pim" "GRP_Front_Tire_Joint_R_parentConstraint1.cpim"
		;
connectAttr "GRP_Front_Tire_Joint_R.rp" "GRP_Front_Tire_Joint_R_parentConstraint1.crp"
		;
connectAttr "GRP_Front_Tire_Joint_R.rpt" "GRP_Front_Tire_Joint_R_parentConstraint1.crt"
		;
connectAttr "CNT_Front_Tire_R.t" "GRP_Front_Tire_Joint_R_parentConstraint1.tg[0].tt"
		;
connectAttr "CNT_Front_Tire_R.rp" "GRP_Front_Tire_Joint_R_parentConstraint1.tg[0].trp"
		;
connectAttr "CNT_Front_Tire_R.rpt" "GRP_Front_Tire_Joint_R_parentConstraint1.tg[0].trt"
		;
connectAttr "CNT_Front_Tire_R.r" "GRP_Front_Tire_Joint_R_parentConstraint1.tg[0].tr"
		;
connectAttr "CNT_Front_Tire_R.ro" "GRP_Front_Tire_Joint_R_parentConstraint1.tg[0].tro"
		;
connectAttr "CNT_Front_Tire_R.s" "GRP_Front_Tire_Joint_R_parentConstraint1.tg[0].ts"
		;
connectAttr "CNT_Front_Tire_R.pm" "GRP_Front_Tire_Joint_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "GRP_Front_Tire_Joint_R_parentConstraint1.w0" "GRP_Front_Tire_Joint_R_parentConstraint1.tg[0].tw"
		;
connectAttr "GRP_Front_Tire_Joint_R.pim" "GRP_Front_Tire_Joint_R_scaleConstraint1.cpim"
		;
connectAttr "CNT_Front_Tire_R.s" "GRP_Front_Tire_Joint_R_scaleConstraint1.tg[0].ts"
		;
connectAttr "CNT_Front_Tire_R.pm" "GRP_Front_Tire_Joint_R_scaleConstraint1.tg[0].tpm"
		;
connectAttr "GRP_Front_Tire_Joint_R_scaleConstraint1.w0" "GRP_Front_Tire_Joint_R_scaleConstraint1.tg[0].tw"
		;
connectAttr "GRP_Back_Tire_Joint_R_parentConstraint1.ctx" "GRP_Back_Tire_Joint_R.tx"
		;
connectAttr "GRP_Back_Tire_Joint_R_parentConstraint1.cty" "GRP_Back_Tire_Joint_R.ty"
		;
connectAttr "GRP_Back_Tire_Joint_R_parentConstraint1.ctz" "GRP_Back_Tire_Joint_R.tz"
		;
connectAttr "GRP_Back_Tire_Joint_R_parentConstraint1.crx" "GRP_Back_Tire_Joint_R.rx"
		;
connectAttr "GRP_Back_Tire_Joint_R_parentConstraint1.cry" "GRP_Back_Tire_Joint_R.ry"
		;
connectAttr "GRP_Back_Tire_Joint_R_parentConstraint1.crz" "GRP_Back_Tire_Joint_R.rz"
		;
connectAttr "GRP_Back_Tire_Joint_R_scaleConstraint1.csx" "GRP_Back_Tire_Joint_R.sx"
		;
connectAttr "GRP_Back_Tire_Joint_R_scaleConstraint1.csy" "GRP_Back_Tire_Joint_R.sy"
		;
connectAttr "GRP_Back_Tire_Joint_R_scaleConstraint1.csz" "GRP_Back_Tire_Joint_R.sz"
		;
connectAttr "JNT_Moto_SideCar_Root.s" "JNT_Back_Tire_R.is";
connectAttr "unitConversion43.o" "JNT_Back_Tire_R.rx";
connectAttr "GRP_Back_Tire_Joint_R.ro" "GRP_Back_Tire_Joint_R_parentConstraint1.cro"
		;
connectAttr "GRP_Back_Tire_Joint_R.pim" "GRP_Back_Tire_Joint_R_parentConstraint1.cpim"
		;
connectAttr "GRP_Back_Tire_Joint_R.rp" "GRP_Back_Tire_Joint_R_parentConstraint1.crp"
		;
connectAttr "GRP_Back_Tire_Joint_R.rpt" "GRP_Back_Tire_Joint_R_parentConstraint1.crt"
		;
connectAttr "CNT_Back_Tire_R.t" "GRP_Back_Tire_Joint_R_parentConstraint1.tg[0].tt"
		;
connectAttr "CNT_Back_Tire_R.rp" "GRP_Back_Tire_Joint_R_parentConstraint1.tg[0].trp"
		;
connectAttr "CNT_Back_Tire_R.rpt" "GRP_Back_Tire_Joint_R_parentConstraint1.tg[0].trt"
		;
connectAttr "CNT_Back_Tire_R.r" "GRP_Back_Tire_Joint_R_parentConstraint1.tg[0].tr"
		;
connectAttr "CNT_Back_Tire_R.ro" "GRP_Back_Tire_Joint_R_parentConstraint1.tg[0].tro"
		;
connectAttr "CNT_Back_Tire_R.s" "GRP_Back_Tire_Joint_R_parentConstraint1.tg[0].ts"
		;
connectAttr "CNT_Back_Tire_R.pm" "GRP_Back_Tire_Joint_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "GRP_Back_Tire_Joint_R_parentConstraint1.w0" "GRP_Back_Tire_Joint_R_parentConstraint1.tg[0].tw"
		;
connectAttr "GRP_Back_Tire_Joint_R.pim" "GRP_Back_Tire_Joint_R_scaleConstraint1.cpim"
		;
connectAttr "CNT_Back_Tire_R.s" "GRP_Back_Tire_Joint_R_scaleConstraint1.tg[0].ts"
		;
connectAttr "CNT_Back_Tire_R.pm" "GRP_Back_Tire_Joint_R_scaleConstraint1.tg[0].tpm"
		;
connectAttr "GRP_Back_Tire_Joint_R_scaleConstraint1.w0" "GRP_Back_Tire_Joint_R_scaleConstraint1.tg[0].tw"
		;
connectAttr "JNT_Moto_SideCar_Root.ro" "JNT_Moto_SideCar_Root_parentConstraint1.cro"
		;
connectAttr "JNT_Moto_SideCar_Root.pim" "JNT_Moto_SideCar_Root_parentConstraint1.cpim"
		;
connectAttr "JNT_Moto_SideCar_Root.rp" "JNT_Moto_SideCar_Root_parentConstraint1.crp"
		;
connectAttr "JNT_Moto_SideCar_Root.rpt" "JNT_Moto_SideCar_Root_parentConstraint1.crt"
		;
connectAttr "JNT_Moto_SideCar_Root.jo" "JNT_Moto_SideCar_Root_parentConstraint1.cjo"
		;
connectAttr "CNT_drive.t" "JNT_Moto_SideCar_Root_parentConstraint1.tg[0].tt";
connectAttr "CNT_drive.rp" "JNT_Moto_SideCar_Root_parentConstraint1.tg[0].trp";
connectAttr "CNT_drive.rpt" "JNT_Moto_SideCar_Root_parentConstraint1.tg[0].trt";
connectAttr "CNT_drive.r" "JNT_Moto_SideCar_Root_parentConstraint1.tg[0].tr";
connectAttr "CNT_drive.ro" "JNT_Moto_SideCar_Root_parentConstraint1.tg[0].tro";
connectAttr "CNT_drive.s" "JNT_Moto_SideCar_Root_parentConstraint1.tg[0].ts";
connectAttr "CNT_drive.pm" "JNT_Moto_SideCar_Root_parentConstraint1.tg[0].tpm";
connectAttr "JNT_Moto_SideCar_Root_parentConstraint1.w0" "JNT_Moto_SideCar_Root_parentConstraint1.tg[0].tw"
		;
connectAttr "JNT_Moto_SideCar_Root.pim" "JNT_Moto_SideCar_Root_scaleConstraint1.cpim"
		;
connectAttr "CNT_drive.s" "JNT_Moto_SideCar_Root_scaleConstraint1.tg[0].ts";
connectAttr "CNT_drive.pm" "JNT_Moto_SideCar_Root_scaleConstraint1.tg[0].tpm";
connectAttr "JNT_Moto_SideCar_Root_scaleConstraint1.w0" "JNT_Moto_SideCar_Root_scaleConstraint1.tg[0].tw"
		;
connectAttr "EXP_Rotate_Tires.out[0]" "LOC_drive.tx";
connectAttr "EXP_Rotate_Tires.out[1]" "LOC_drive.ty";
connectAttr "EXP_Rotate_Tires.out[2]" "LOC_drive.tz";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "unitConversion46.o" "EXP_Rotate_Tires.in[0]";
connectAttr "CNT_drive.Radius" "EXP_Rotate_Tires.in[1]";
connectAttr "CNT_drive.tx" "EXP_Rotate_Tires.in[2]";
connectAttr "CNT_drive.ty" "EXP_Rotate_Tires.in[3]";
connectAttr "CNT_drive.tz" "EXP_Rotate_Tires.in[4]";
connectAttr ":time1.o" "EXP_Rotate_Tires.tim";
connectAttr "STR_Front_Tires.ox" "unitConversion44.i";
connectAttr "CNT_Front_Tires.tx" "STR_Front_Tires.vx";
connectAttr "STR_Front_Tires.ox" "unitConversion45.i";
connectAttr "MLP_FrontTire_Rotation.ox" "unitConversion39.i";
connectAttr "CNT_drive.Drive" "MLP_FrontTire_Rotation.i1x";
connectAttr "CNT_drive.Drive" "MLP_FrontTire_Rotation.i1y";
connectAttr "CNT_Front_Tire_L.Tire_Spin_Rotation" "MLP_FrontTire_Rotation.i2x";
connectAttr "CNT_Front_Tire_R.Tire_Spin_Rotation" "MLP_FrontTire_Rotation.i2y";
connectAttr "MLP_BackTire_Rotation.ox" "unitConversion41.i";
connectAttr "CNT_drive.Drive" "MLP_BackTire_Rotation.i1x";
connectAttr "CNT_drive.Drive" "MLP_BackTire_Rotation.i1y";
connectAttr "CNT_Back_Tire_L.Tire_Spin_Rotation" "MLP_BackTire_Rotation.i2x";
connectAttr "CNT_Back_Tire_R.Tire_Spin_Rotation" "MLP_BackTire_Rotation.i2y";
connectAttr "MLP_FrontTire_Rotation.oy" "unitConversion42.i";
connectAttr "MLP_BackTire_Rotation.oy" "unitConversion43.i";
connectAttr "CNT_drive.ry" "unitConversion46.i";
connectAttr "MLD_scale_radius.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "MLP_FrontTire_Rotation.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "MLP_BackTire_Rotation.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "STR_Front_Tires.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of wheels_base_rigNewScript.ma
