//Maya ASCII 2018ff09 scene
//Name: 44Cats_Prop_Ctrls_Default_V002.ma
//Last modified: Fri, Sep 27, 2019 11:25:44 AM
//Codeset: 1252
requires maya "2018ff09";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t pal;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201811122215-49253d42f6";
fileInfo "osv" "Microsoft Windows 7 Business Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode transform -n "CTRL_set";
	rename -uid "47F534E7-4AA2-A93B-B076-A18ACA1E9016";
	addAttr -ci true -sn "Rattr" -ln "Rattr" -nn "reference" -dv 1 -min 0 -max 1 -at "bool";
	setAttr ".ctor" -type "string" "1493386226r";
	setAttr -cb on ".Rattr";
createNode locator -n "CTRL_setShape" -p "CTRL_set";
	rename -uid "2DC30535-43D5-6D9F-15D3-1E8E178F9C5E";
	addAttr -ci true -sn "subC" -ln "subC" -nn "subControl" -dv 1 -min 0 -max 1 -at "bool";
	setAttr -k off ".v";
	setAttr -cb on ".subC" no;
createNode transform -n "CTRL_setsubControl" -p "CTRL_set";
	rename -uid "8E95FB81-4565-3C9C-D7BF-97A29891060B";
createNode locator -n "CTRL_setsubControlShape" -p "CTRL_setsubControl";
	rename -uid "74E3F92B-4F4D-3190-BC65-E4B11A1AF131";
	setAttr -k off ".v";
createNode transform -n "GRP_Name_rig" -p "CTRL_setsubControl";
	rename -uid "691697FA-407D-37C6-11B0-E29DDA1B03CF";
createNode transform -n "GRP_Controls" -p "GRP_Name_rig";
	rename -uid "E6E96290-43B1-665A-78B8-DDA3088C00CF";
createNode transform -n "GRP_constraint" -p "GRP_Controls";
	rename -uid "207A03A8-4A15-CD9D-72F6-43A6FA355D11";
	setAttr ".rp" -type "double3" 0 -2.4651903288156619e-32 4.4408920985006262e-16 ;
	setAttr ".sp" -type "double3" 0 -2.4651903288156619e-32 4.4408920985006262e-16 ;
createNode transform -n "CNT_constraint" -p "GRP_constraint";
	rename -uid "7D0F7FF9-458F-CEBD-E42F-3F822C34D9B9";
	addAttr -ci true -k true -sn "ms" -ln "Master_Scale" -nn "Master Scale" -dv 1 -min 
		0 -max 10 -at "float";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr -k on ".ms";
createNode nurbsCurve -n "CNT_constraintShape" -p "CNT_constraint";
	rename -uid "2845EE6D-4999-9F82-5B7B-DA9243B6018D";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		7.7319069431375382 4.734427544609288e-16 -7.7319069431375222
		-1.2475034251759294e-15 6.6954916436592036e-16 -10.934567661991776
		-7.7319069431375249 4.734427544609289e-16 -7.7319069431375249
		-10.934567661991776 1.8977073696397163e-31 -4.0625642455369259e-15
		-7.7319069431375294 -4.734427544609289e-16 7.7319069431375222
		-3.2947986368746795e-15 -6.6954916436592036e-16 10.934567661991776
		7.7319069431375222 -4.734427544609289e-16 7.7319069431375294
		10.934567661991776 -3.6386402236531741e-31 4.9789775233184057e-15
		7.7319069431375382 4.734427544609288e-16 -7.7319069431375222
		-1.2475034251759294e-15 6.6954916436592036e-16 -10.934567661991776
		-7.7319069431375249 4.734427544609289e-16 -7.7319069431375249
		;
createNode transform -n "GRP_main" -p "CNT_constraint";
	rename -uid "52412C94-4F4F-12A3-E201-2BB55E26B315";
	setAttr ".rp" -type "double3" 0 -2.4651903288156619e-32 4.4408920985006262e-16 ;
	setAttr ".sp" -type "double3" 0 -2.4651903288156619e-32 4.4408920985006262e-16 ;
createNode transform -n "CNT_main" -p "GRP_main";
	rename -uid "8AADD60D-499C-C300-B4D2-ECBA1D08E3CA";
	addAttr -ci true -sn "Pivot_Vis" -ln "Pivot_Vis" -min 0 -max 1 -en "off:on" -at "enum";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".Pivot_Vis" 1;
createNode nurbsCurve -n "CNT_mainShape" -p "CNT_main";
	rename -uid "32D18FD4-493D-820C-EF6B-B6BC9637CA1B";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		5.7989302073531404 3.5508206584569569e-16 -5.7989302073531341
		-9.3562756888194608e-16 5.0216187327443976e-16 -8.2009257464938301
		-5.798930207353135 3.5508206584569583e-16 -5.798930207353135
		-8.2009257464938301 1.4126608770504078e-31 -3.2704232087465436e-15
		-5.798930207353135 -3.5508206584569583e-16 5.7989302073531341
		-2.4710989776560095e-15 -5.0216187327443995e-16 8.2009257464938337
		5.7989302073531341 -3.5508206584569583e-16 5.798930207353135
		8.2009257464938301 -2.7395998179192597e-31 3.5107331178949602e-15
		5.7989302073531404 3.5508206584569569e-16 -5.7989302073531341
		-9.3562756888194608e-16 5.0216187327443976e-16 -8.2009257464938301
		-5.798930207353135 3.5508206584569583e-16 -5.798930207353135
		;
createNode transform -n "GRP_CNT_Transform_Pivot" -p "CNT_main";
	rename -uid "D7276E96-4A8D-255E-C6DD-4DA1783E3FC9";
createNode transform -n "CNT_Transform_Pivot" -p "GRP_CNT_Transform_Pivot";
	rename -uid "5200D996-4A83-5154-1B4B-1896B140064F";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "CNT_Transform_PivotShape" -p "CNT_Transform_Pivot";
	rename -uid "51047CE1-46E0-2B99-9245-A6A9B97FFD5A";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 22;
	setAttr ".cc" -type "nurbsCurve" 
		1 12 0 no 3
		13 0 4.7999999999999998 9.5999999999999996 12.800000000000001 17.600000000000001
		 22.399999999999999 25.600000000000001 30.399999999999999 35.200000000000003 38.399999999999999
		 43.200000000000003 48 51.200000000000003
		13
		-5.2056231652251466 -5.3874834733425982e-32 3.1233738991350859
		-2.0822492660900545 -5.3874834733425982e-32 3.4000587269351416e-17
		-5.2056231652251466 -5.3874834733425982e-32 -3.1233738991350859
		-3.1233738991350859 -5.3874834733425982e-32 -5.2056231652251466
		-1.0216637093086931e-16 -5.3874834733425982e-32 -2.0822492660900545
		3.1233738991350859 -5.3874834733425982e-32 -5.2056231652251466
		5.2056231652251466 -5.3874834733425982e-32 -3.1233738991350859
		2.0822492660900545 -5.3874834733425982e-32 -1.700029363467574e-16
		5.2056231652251466 -5.3874834733425982e-32 3.1233738991350859
		3.1233738991350859 -5.3874834733425982e-32 5.2056231652251466
		1.0216637093086931e-16 -5.3874834733425982e-32 2.0822492660900545
		-3.1233738991350859 -5.3874834733425982e-32 5.2056231652251466
		-5.2056231652251466 -5.3874834733425982e-32 3.1233738991350859
		;
createNode transform -n "main_null" -p "CNT_main";
	rename -uid "A9EF6551-4527-6FE7-8907-ADB9557A644F";
createNode transform -n "GRP_Joints" -p "CTRL_setsubControl";
	rename -uid "E33A0AF3-47A6-7D8D-33F0-D39321191F14";
createNode joint -n "JNT_root" -p "GRP_Joints";
	rename -uid "7A2417E3-40E8-4359-0AD1-83A748B67004";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode parentConstraint -n "JNT_root_parentConstraint1" -p "JNT_root";
	rename -uid "6B9396F3-4888-2FFE-C7BF-F59510CBD1C0";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "main_nullW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode scaleConstraint -n "JNT_root_scaleConstraint1" -p "JNT_root";
	rename -uid "4B5132B9-4F79-33BF-36B4-DA84178941B9";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "main_nullW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode transform -n "GRP_Meshes_grp" -p "CTRL_setsubControl";
	rename -uid "0A8D89D9-4C7F-A32A-DC44-7091B2D1AB1A";
	setAttr ".ovdt" 2;
createNode transform -n "GRP_Meshes_RM" -p "GRP_Meshes_grp";
	rename -uid "783C8A06-4C11-85C1-A06A-41A444CDCC40";
	setAttr ".it" no;
createNode transform -n "GRP_Meshes_RND" -p "GRP_Meshes_grp";
	rename -uid "B8DFBE02-46CF-303D-F0DF-A9AB540FD1B0";
	setAttr ".it" no;
createNode objectSet -n "ALL_CONTROLS";
	rename -uid "78A54F68-4FF3-BAE3-3C6C-819F49402739";
	setAttr ".ihi" 0;
	setAttr -s 5 ".dsm";
createNode objectSet -n "All_Rig_Driver_Meshes_RM";
	rename -uid "E4C67548-4CCF-FCB8-5517-3698759BEA03";
	setAttr ".ihi" 0;
createNode objectSet -n "MSH";
	rename -uid "3B37DBA1-40A2-2276-4CCB-8BB7B96FFD2D";
	setAttr ".ihi" 0;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av -k on ".unw" 1;
	setAttr -av -k on ".etw";
	setAttr -av -k on ".tps";
	setAttr -av -k on ".tms";
select -ne :hardwareRenderingGlobals;
	setAttr -av -k on ".ihi";
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr -av ".ta";
	setAttr -av ".tq";
	setAttr ".etmr" no;
	setAttr ".tmr" 4096;
	setAttr -av ".aoam";
	setAttr -av ".aora";
	setAttr -av ".hfs";
	setAttr -av ".hfe";
	setAttr -av ".mbe";
	setAttr -av -k on ".mbsof";
	setAttr ".msaa" yes;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k on ".macc";
	setAttr -av -k on ".macd";
	setAttr -av -k on ".macq";
	setAttr -av -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -av -k on ".clip";
	setAttr -av -k on ".edm";
	setAttr -av -k on ".edl" no;
	setAttr -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -av -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -av -cb on ".imfkey";
	setAttr -av -k on ".gama";
	setAttr -av -k on ".an";
	setAttr -k on ".ar";
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -av -k on ".be";
	setAttr -av -cb on ".ep";
	setAttr -av -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -av -k on ".pff";
	setAttr -av -cb on ".peie";
	setAttr -av -k on ".ifp";
	setAttr -k on ".rv";
	setAttr -av -k on ".comp";
	setAttr -av -k on ".cth";
	setAttr -av -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -av -k on ".rd";
	setAttr -av -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -av -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -av -k on ".mm";
	setAttr -av -k on ".npu";
	setAttr -av -k on ".itf";
	setAttr -av -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -av -k on ".uf";
	setAttr -av -k on ".oi";
	setAttr -av -k on ".rut";
	setAttr -av -k on ".mot";
	setAttr -av -k on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -av -k on ".mbso";
	setAttr -av -k on ".mbsc";
	setAttr -av -k on ".afp";
	setAttr -av -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -av -k on ".ubc";
	setAttr -av -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -av -k on ".udbx";
	setAttr -av -k on ".smc";
	setAttr -av -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -av -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -av -k on ".tlwd";
	setAttr -av -k on ".tlht";
	setAttr -av -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -av -k on ".ope";
	setAttr -av -k on ".oppf";
	setAttr -av -k on ".rcp";
	setAttr -av -k on ".icp";
	setAttr -av -k on ".ocp";
	setAttr -k on ".hbl";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -av -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w";
	setAttr -av -k on ".h";
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar";
	setAttr -av -k on ".ldar";
	setAttr -av -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -av -k on ".isu";
	setAttr -av -k on ".pdu";
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k off -cb on ".ctrs" 256;
	setAttr -av -k off -cb on ".btrs" 512;
	setAttr -k off -cb on ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off -cb on ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -k on ".hwcc";
	setAttr -k on ".hwdp";
	setAttr -k on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
connectAttr "CTRL_setShape.subC" "CTRL_setsubControlShape.v";
connectAttr "CNT_constraint.ms" "CNT_constraint.sx" -l on;
connectAttr "CNT_constraint.ms" "CNT_constraint.sy" -l on;
connectAttr "CNT_constraint.ms" "CNT_constraint.sz" -l on;
connectAttr "CNT_Transform_Pivot.t" "CNT_main.rp";
connectAttr "CNT_Transform_Pivot.t" "CNT_main.sp";
connectAttr "CNT_main.Pivot_Vis" "GRP_CNT_Transform_Pivot.v";
connectAttr "JNT_root_parentConstraint1.ctx" "JNT_root.tx";
connectAttr "JNT_root_parentConstraint1.cty" "JNT_root.ty";
connectAttr "JNT_root_parentConstraint1.ctz" "JNT_root.tz";
connectAttr "JNT_root_parentConstraint1.crx" "JNT_root.rx";
connectAttr "JNT_root_parentConstraint1.cry" "JNT_root.ry";
connectAttr "JNT_root_parentConstraint1.crz" "JNT_root.rz";
connectAttr "JNT_root_scaleConstraint1.csx" "JNT_root.sx";
connectAttr "JNT_root_scaleConstraint1.csy" "JNT_root.sy";
connectAttr "JNT_root_scaleConstraint1.csz" "JNT_root.sz";
connectAttr "JNT_root.ro" "JNT_root_parentConstraint1.cro";
connectAttr "JNT_root.pim" "JNT_root_parentConstraint1.cpim";
connectAttr "JNT_root.rp" "JNT_root_parentConstraint1.crp";
connectAttr "JNT_root.rpt" "JNT_root_parentConstraint1.crt";
connectAttr "JNT_root.jo" "JNT_root_parentConstraint1.cjo";
connectAttr "main_null.t" "JNT_root_parentConstraint1.tg[0].tt";
connectAttr "main_null.rp" "JNT_root_parentConstraint1.tg[0].trp";
connectAttr "main_null.rpt" "JNT_root_parentConstraint1.tg[0].trt";
connectAttr "main_null.r" "JNT_root_parentConstraint1.tg[0].tr";
connectAttr "main_null.ro" "JNT_root_parentConstraint1.tg[0].tro";
connectAttr "main_null.s" "JNT_root_parentConstraint1.tg[0].ts";
connectAttr "main_null.pm" "JNT_root_parentConstraint1.tg[0].tpm";
connectAttr "JNT_root_parentConstraint1.w0" "JNT_root_parentConstraint1.tg[0].tw"
		;
connectAttr "JNT_root.pim" "JNT_root_scaleConstraint1.cpim";
connectAttr "main_null.s" "JNT_root_scaleConstraint1.tg[0].ts";
connectAttr "main_null.pm" "JNT_root_scaleConstraint1.tg[0].tpm";
connectAttr "JNT_root_scaleConstraint1.w0" "JNT_root_scaleConstraint1.tg[0].tw";
connectAttr "CTRL_set.Rattr" "GRP_Meshes_grp.ove";
connectAttr "CTRL_setsubControl.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_Transform_Pivot.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_main.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_constraint.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CTRL_set.iog" "ALL_CONTROLS.dsm" -na;
// End of 44Cats_Prop_Ctrls_Default_V002.ma
