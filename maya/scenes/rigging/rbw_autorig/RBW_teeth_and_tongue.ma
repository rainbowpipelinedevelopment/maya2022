//Maya ASCII 2019 scene
//Name: MTP_TongueAndTeeth_Rig.ma
//Last modified: Thu, Oct 06, 2022 04:58:02 PM
//Codeset: 1252
requires maya "2019";
requires -nodeType "VRaySettingsNode" -dataType "VRaySunParams" -dataType "vrayFloatVectorData"
		 -dataType "vrayFloatVectorData" -dataType "vrayIntData" "vrayformaya" "5";
requires -nodeType "renderSetup" "renderSetup.py" "1.0";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t pal;
fileInfo "vrayBuild" "5.00.21 848ea8a";
fileInfo "application" "maya";
fileInfo "product" "Maya 2019";
fileInfo "version" "2019";
fileInfo "cutIdentifier" "202004141915-92acaa8c08";
fileInfo "osv" "Microsoft Windows 10 Technical Preview  (Build 19043)\n";
createNode transform -s -n "persp";
	rename -uid "538B8681-42A8-E6E1-0FFC-80B2A8CBB8E2";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 191.32024523181929 341.37318629417484 193.43402063870894 ;
	setAttr ".r" -type "double3" -40.538352729602487 40.199999999999903 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "7CADF5C9-4248-3025-CE9A-84977C9101F2";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 350.05652994957569;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0.015967884017854317 146.32291172045444 3.5281129029106233 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "809F1A39-466E-6B0E-CD1F-AC967CC44777";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "1A666C6A-4472-162D-038E-8888D93811CF";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "ED7E0C6C-42D9-7556-A6B7-3B9A1D74E1B5";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "14E851F6-4D83-DE93-0B64-98A46CABF84E";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "C640F7B0-419A-6AB8-60BF-728212835A96";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "E5084DCF-410C-8F61-CDB1-AA8996FEFD18";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "up_teeth_Ctrl_Grp";
	rename -uid "48CB32C1-4715-307C-838C-D3BC0AB2B2E9";
	setAttr ".t" -type "double3" 0.015850067138671875 147.15875244140628 3.5689961910247767 ;
createNode transform -n "up_teeth_Ctrl_Grp_offset" -p "up_teeth_Ctrl_Grp";
	rename -uid "B8BE4239-471D-1469-B896-B3ABD0EC2A48";
createNode transform -n "up_teeth_Ctrl" -p "up_teeth_Ctrl_Grp_offset";
	rename -uid "A7558920-4547-D490-34FD-33A0E9D2EF3E";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "up_teeth_CtrlShape" -p "up_teeth_Ctrl";
	rename -uid "6628CE80-4D92-B091-6A11-B08CD4630EF4";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-2.1727645390142731 0.6699743243588524 2.8231380995691193
		-4.1175413643194503 0.91247805352175615 -2.9049046757974262
		4.1177769980778152 0.91247805352175615 -2.9049046757974262
		2.1730001727726322 0.6699743243588524 2.8231380995691193
		-2.1727645390142731 0.6699743243588524 2.8231380995691193
		-2.1727645390142731 -0.83136807722350226 2.8231380995691193
		-4.1175413643194503 -0.63658379709611967 -2.9049046757974257
		-4.1175413643194503 0.91247805352175615 -2.9049046757974262
		-2.1727645390142731 0.6699743243588524 2.8231380995691193
		-2.1727645390142731 -0.83136807722350226 2.8231380995691193
		2.1730001727726322 -0.83136807722350226 2.8231380995691193
		2.1730001727726322 0.6699743243588524 2.8231380995691193
		4.1177769980778152 0.91247805352175615 -2.9049046757974262
		4.1177769980778152 -0.63658379709611967 -2.9049046757974257
		2.1730001727726322 -0.83136807722350226 2.8231380995691193
		4.1177769980778152 -0.63658379709611967 -2.9049046757974257
		-4.1175413643194503 -0.63658379709611967 -2.9049046757974257
		;
createNode transform -n "Upper_Teeth_Controls_Grp" -p "up_teeth_Ctrl";
	rename -uid "A8FCA08A-47BD-1F73-32D5-E1BE0A2FFCB8";
	setAttr ".t" -type "double3" 0.04833984375 -0.8857617301483458 -10.552409141540521 ;
createNode transform -n "C_upper_teeth_Ctrl_Grp" -p "Upper_Teeth_Controls_Grp";
	rename -uid "F92D8F41-4FEE-78B5-1F67-C7A0C5C2C779";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".t" -type "double3" -0.048340529253607656 1.0073858862986071 13.080716638495094 ;
	setAttr ".r" -type "double3" 89.999999999999986 0 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000011 1.0000000000000011 ;
createNode transform -n "C_upper_teeth_Ctrl" -p "C_upper_teeth_Ctrl_Grp";
	rename -uid "C9668A66-47F7-0FB2-2424-78BD1E6169EE";
	addAttr -ci true -sn "Mid_Roll" -ln "Mid_Roll" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on ".Mid_Roll";
createNode nurbsCurve -n "C_upper_teeth_CtrlShape" -p "C_upper_teeth_Ctrl";
	rename -uid "5382698E-42AE-FB2F-7F58-4E931181239D";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-2.0825928056288348 0.34082949777472499 1.364244463126189
		-2.0825928056288348 0.34082949777472499 -0.59963325162218761
		1.6152225268910767 0.34082949777472499 -0.59963325162218761
		1.6152225268910767 0.34082949777472499 1.364244463126189
		-2.0825928056288348 0.34082949777472499 1.364244463126189
		-2.0825928056288348 -0.83857759865015968 1.364244463126189
		-2.0825928056288348 -0.83857759865015957 -0.59963325162218761
		-2.0825928056288348 0.34082949777472499 -0.59963325162218761
		-2.0825928056288348 0.34082949777472499 1.364244463126189
		-2.0825928056288348 -0.83857759865015968 1.364244463126189
		1.6152225268910767 -0.83857759865015968 1.364244463126189
		1.6152225268910767 0.34082949777472499 1.364244463126189
		1.6152225268910767 0.34082949777472499 -0.59963325162218761
		1.6152225268910767 -0.83857759865015957 -0.59963325162218761
		1.6152225268910767 -0.83857759865015968 1.364244463126189
		1.6152225268910767 -0.83857759865015957 -0.59963325162218761
		-2.0825928056288348 -0.83857759865015957 -0.59963325162218761
		;
createNode transform -n "L_upper_teeth_Ctrl_Grp" -p "Upper_Teeth_Controls_Grp";
	rename -uid "C69D2952-4378-8907-C122-528EC8D12805";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".t" -type "double3" 2.9221464767796483 1.3967518729706114 10.250945494258925 ;
	setAttr ".r" -type "double3" 90.000000000000156 75.000000000000043 0 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000007 1.0000000000000009 ;
createNode transform -n "L_upper_teeth_Ctrl" -p "L_upper_teeth_Ctrl_Grp";
	rename -uid "B6F8F4FF-44FF-9544-F827-FA85B2ED34F7";
	addAttr -ci true -sn "Left_Roll" -ln "Left_Roll" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on ".Left_Roll";
createNode nurbsCurve -n "L_upper_teeth_CtrlShape" -p "L_upper_teeth_Ctrl";
	rename -uid "0CAA9083-473D-4DB0-BC2A-CF9648455EF6";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-1.2572455625221659 0.51928681029056833 1.5680004679779687
		-1.2572455625221597 0.51928681029056301 -0.21119065157478017
		2.4242922075743936 -0.26563680578483223 -0.21119065157477812
		2.4242922075743927 -0.268731261876264 1.2864052272811501
		-1.2572455625221659 0.51928681029056833 1.5680004679779687
		-1.5083220188466169 -0.31740933931639165 1.5680004679779687
		-1.5083220188466124 -0.3174093393163922 -0.21119065157477984
		-1.2572455625221597 0.51928681029056301 -0.21119065157478017
		-1.2572455625221659 0.51928681029056833 1.5680004679779687
		-1.5083220188466169 -0.31740933931639165 1.5680004679779687
		2.1732157512499426 -1.1054274114832265 1.2864052272811501
		2.4242922075743927 -0.268731261876264 1.2864052272811501
		2.4242922075743936 -0.26563680578483223 -0.21119065157477812
		2.1732157512499346 -1.1023329553917915 -0.21119065157477779
		2.1732157512499426 -1.1054274114832265 1.2864052272811501
		2.1732157512499346 -1.1023329553917915 -0.21119065157477779
		-1.5083220188466124 -0.3174093393163922 -0.21119065157477984
		;
createNode transform -n "R_upper_teeth_Ctrl_Grp" -p "Upper_Teeth_Controls_Grp";
	rename -uid "A49F7BB5-43DC-75DF-30BA-A6AE6FF15F3C";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".t" -type "double3" -2.922 1.3967518729706114 10.250945494258925 ;
	setAttr ".r" -type "double3" 89.999999999999858 -84.734170470027721 3.4655348445738627e-14 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
createNode transform -n "R_upper_teeth_Ctrl" -p "R_upper_teeth_Ctrl_Grp";
	rename -uid "E21683EE-4A5D-A881-FCAF-298FA688B0A9";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "R_upper_teeth_CtrlShape" -p "R_upper_teeth_Ctrl";
	rename -uid "7C5545F9-4CBD-6D7D-CD53-EFB5E7A6C1D1";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-2.3609938047115362 0.30848989071230853 1.1046134237988581
		-2.3609938047115362 0.30848989071230698 -0.17946475466685202
		1.4002352747115667 0.19523643905950835 -0.17946475466685224
		1.4002352747115614 0.19523643905950736 1.6017111475302312
		-2.3609938047115362 0.30848989071230853 1.1046134237988581
		-2.360993804711538 -0.51523268689368273 1.1046134237988581
		-2.3609938047115362 -0.51523268689368051 -0.17946475466685202
		-2.3609938047115362 0.30848989071230698 -0.17946475466685202
		-2.3609938047115362 0.30848989071230853 1.1046134237988581
		-2.360993804711538 -0.51523268689368273 1.1046134237988581
		1.4002352747115572 -0.62848613854647883 1.6017111475302312
		1.4002352747115614 0.19523643905950736 1.6017111475302312
		1.4002352747115667 0.19523643905950835 -0.17946475466685224
		1.400235274711561 -0.62848613854648172 -0.17946475466685224
		1.4002352747115572 -0.62848613854647883 1.6017111475302312
		1.400235274711561 -0.62848613854648172 -0.17946475466685224
		-2.3609938047115362 -0.51523268689368051 -0.17946475466685202
		;
createNode transform -n "low_teeth_Ctrl_Grp";
	rename -uid "9309DCEB-4693-1967-DED2-EB8821E0830B";
	setAttr ".t" -type "double3" -1.1444091796875e-05 145.4883728027344 3.4872614145278895 ;
createNode transform -n "low_teeth_Ctrl_Grp_offset" -p "low_teeth_Ctrl_Grp";
	rename -uid "14586C37-4A8D-5A88-9495-B88AB3E88879";
createNode transform -n "low_teeth_Ctrl" -p "low_teeth_Ctrl_Grp_offset";
	rename -uid "A292B991-4EB6-ECD0-82A0-8E8E33AAF021";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "low_teeth_CtrlShape" -p "low_teeth_Ctrl";
	rename -uid "B334BE10-4C4C-D2AD-AFD3-28AEDC776A94";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".tw" yes;
	setAttr -s 17 ".cp[0:16]" -type "double3" 0.1894642310766792 -0.32223769214748332 
		-0.9019159496810909 0.88329077819914259 0.89322013252654542 1.7533802330141528 -0.96657567038003167 
		0.89322013252654542 1.7533802330141528 -0.1894619774110291 -0.32223769214748332 -0.9019159496810909 
		0.1894642310766792 -0.32223769214748332 -0.9019159496810909 0.1894642310766792 -0.07569959511251878 
		-0.9019159496810909 0.88329077819914259 0.95461400225512327 1.7533802330141528 0.88329077819914259 
		0.89322013252654542 1.7533802330141528 0.1894642310766792 -0.32223769214748332 -0.9019159496810909 
		0.1894642310766792 -0.07569959511251878 -0.9019159496810909 -0.1894619774110291 -0.07569959511251878 
		-0.9019159496810909 -0.1894619774110291 -0.32223769214748332 -0.9019159496810909 
		-0.96657567038003167 0.89322013252654542 1.7533802330141528 -0.96657567038003167 
		0.95461400225512327 1.7533802330141528 -0.1894619774110291 -0.07569959511251878 -0.9019159496810909 
		-0.96657567038003167 0.95461400225512327 1.7533802330141528 0.88329077819914259 0.95461400225512327 
		1.7533802330141528;
createNode nurbsCurve -n "low_teeth_CtrlShapeOrig" -p "low_teeth_Ctrl";
	rename -uid "15D03FF0-4463-4D29-A2E3-CDA301232B5E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-2.3965251176280442 0.4596786144118446 3.4680649657497953
		-4.5414543071774753 0.4596786144118441 -4.3897980508008043
		4.5414258006551362 0.4596786144118441 -4.3897980508008043
		2.3964966111057038 0.4596786144118446 3.4680649657497953
		-2.3965251176280442 0.4596786144118446 3.4680649657497953
		-2.3965251176280442 -0.83808026164101013 3.4680649657497953
		-4.5414543071774753 -0.83808026164101102 -4.3897980508008043
		-4.5414543071774753 0.4596786144118441 -4.3897980508008043
		-2.3965251176280442 0.4596786144118446 3.4680649657497953
		-2.3965251176280442 -0.83808026164101013 3.4680649657497953
		2.3964966111057038 -0.83808026164101013 3.4680649657497953
		2.3964966111057038 0.4596786144118446 3.4680649657497953
		4.5414258006551362 0.4596786144118441 -4.3897980508008043
		4.5414258006551362 -0.83808026164101102 -4.3897980508008043
		2.3964966111057038 -0.83808026164101013 3.4680649657497953
		4.5414258006551362 -0.83808026164101102 -4.3897980508008043
		-4.5414543071774753 -0.83808026164101102 -4.3897980508008043
		;
createNode transform -n "Lower_Teeth_Controls_Grp" -p "low_teeth_Ctrl";
	rename -uid "284DFD28-45DF-E77F-51B6-E2BF18BFCA9B";
	setAttr ".t" -type "double3" 0.04833984375 -0.16149949259951768 -9.8771494950786245 ;
createNode transform -n "C_lower_teeth_Ctrl_Grp" -p "Lower_Teeth_Controls_Grp";
	rename -uid "10802F7B-48F5-3081-24AC-67A46FB8037F";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".t" -type "double3" -0.048340638338572731 -0.49059009202505877 12.188245989217735 ;
	setAttr ".r" -type "double3" 89.999999999999986 7.0622500768802538e-31 180 ;
	setAttr ".s" -type "double3" 1 1.0000000000000009 1.0000000000000009 ;
createNode transform -n "C_lower_teeth_Ctrl" -p "C_lower_teeth_Ctrl_Grp";
	rename -uid "73110395-467E-57BB-EF5F-2BB9DC526BF9";
	addAttr -ci true -sn "Mid_Roll" -ln "Mid_Roll" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on ".Mid_Roll";
createNode nurbsCurve -n "C_lower_teeth_CtrlShape" -p "C_lower_teeth_Ctrl";
	rename -uid "788E7545-47EF-5392-B5B5-7A8C42FA728C";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-1.7185896401394838 0.31255584147262017 1.3708583732141899
		-1.718589640139486 0.31255584147262017 -0.35258476836428215
		1.7185896401394483 0.31255584147262017 -0.35258476836428215
		1.7185896401394483 0.31255584147262017 1.3708583732141899
		-1.7185896401394838 0.31255584147262017 1.3708583732141899
		-1.7185896401394838 -0.61403133734582638 1.3708583732141899
		-1.718589640139486 -0.61403133734582638 -0.35258476836428215
		-1.718589640139486 0.31255584147262017 -0.35258476836428215
		-1.7185896401394838 0.31255584147262017 1.3708583732141899
		-1.7185896401394838 -0.61403133734582638 1.3708583732141899
		1.7185896401394483 -0.61403133734582638 1.3708583732141899
		1.7185896401394483 0.31255584147262017 1.3708583732141899
		1.7185896401394483 0.31255584147262017 -0.35258476836428215
		1.7185896401394483 -0.61403133734582638 -0.35258476836428215
		1.7185896401394483 -0.61403133734582638 1.3708583732141899
		1.7185896401394483 -0.61403133734582638 -0.35258476836428215
		-1.718589640139486 -0.61403133734582638 -0.35258476836428215
		;
createNode transform -n "L_lower_teeth_Ctrl_Grp" -p "Lower_Teeth_Controls_Grp";
	rename -uid "DE485EAE-46AE-02A3-3361-99A7C2CFA39C";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".t" -type "double3" 2.7407989885712025 0.22261465141266984 9.6024778179270367 ;
	setAttr ".r" -type "double3" 90.000000000000384 -86.202805765357837 179.99999999999983 ;
	setAttr ".s" -type "double3" 0.99999999999999978 1.0000000000000004 1.0000000000000004 ;
createNode transform -n "L_lower_teeth_Ctrl" -p "L_lower_teeth_Ctrl_Grp";
	rename -uid "1409EB14-469D-6E52-40D5-3D9F281427E9";
	addAttr -ci true -sn "Left_Roll" -ln "Left_Roll" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on ".Left_Roll";
createNode nurbsCurve -n "L_lower_teeth_CtrlShape" -p "L_lower_teeth_Ctrl";
	rename -uid "E733A733-45A8-0DBD-CF5E-4B820F1FCBBB";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-1.9301495777838023 0.24891307971237597 1.1075940300517506
		-2.1237067087644226 0.26474493490048512 -0.37598738198478898
		1.4171881537227295 -0.024879810078984557 -0.93517227085469923
		1.6738588551532392 -0.045873990740218806 0.8867042170996321
		-1.9301495777838023 0.24891307971237597 1.1075940300517506
		-1.9999356530753356 -0.60427767748774941 1.1075940300517515
		-2.193492784055953 -0.58844582229963938 -0.37598738198478898
		-2.1237067087644226 0.26474493490048512 -0.37598738198478898
		-1.9301495777838023 0.24891307971237597 1.1075940300517506
		-1.9999356530753356 -0.60427767748774941 1.1075940300517515
		1.6040727798617049 -0.89906474794034363 0.88670421709963232
		1.6738588551532392 -0.045873990740218806 0.8867042170996321
		1.4171881537227295 -0.024879810078984557 -0.93517227085469923
		1.3474020784311955 -0.87807056727910393 -0.93517227085469867
		1.6040727798617049 -0.89906474794034363 0.88670421709963232
		1.3474020784311955 -0.87807056727910393 -0.93517227085469867
		-2.193492784055953 -0.58844582229963938 -0.37598738198478898
		;
createNode transform -n "R_lower_teeth_Ctrl_Grp" -p "Lower_Teeth_Controls_Grp";
	rename -uid "761ACE22-4E20-F7F5-9F6E-3196C86EF12F";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".t" -type "double3" -2.741 0.22261465141266967 9.6024778179270367 ;
	setAttr ".r" -type "double3" 90.000000000000156 74.999999999999972 -179.99999999999955 ;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "R_lower_teeth_Ctrl" -p "R_lower_teeth_Ctrl_Grp";
	rename -uid "2FF2CFAC-4E6A-3DF4-19D3-D09830E2C355";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "R_lower_teeth_CtrlShape" -p "R_lower_teeth_Ctrl";
	rename -uid "D6F2F6B1-4557-F848-0029-66A0F0F0C06B";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-1.7034619994945346 0.34172487050070655 0.91207609279884194
		-1.4038556256648216 0.30250583913198409 -0.9402984850041326
		2.4887130630268399 0.10555837762116349 -0.27299649771176432
		2.2627778479096299 0.13513371734346249 1.1238918373522715
		-1.7034619994945346 0.34172487050070655 0.91207609279884194
		-1.8062980922430307 -0.58397239092480713 0.91504234009700491
		-1.5066917184133155 -0.62319142229352464 -0.93733223770596952
		-1.4038556256648216 0.30250583913198409 -0.9402984850041326
		-1.7034619994945346 0.34172487050070655 0.91207609279884194
		-1.8062980922430307 -0.58397239092480713 0.91504234009700491
		2.1599417551611331 -0.79056354408205243 1.1268580846504357
		2.2627778479096299 0.13513371734346249 1.1238918373522715
		2.4887130630268399 0.10555837762116349 -0.27299649771176432
		2.3858769702783467 -0.82013888380435218 -0.27003025041360124
		2.1599417551611331 -0.79056354408205243 1.1268580846504357
		2.3858769702783467 -0.82013888380435218 -0.27003025041360124
		-1.5066917184133155 -0.62319142229352464 -0.93733223770596952
		;
createNode transform -n "Teeth_joints_Grp";
	rename -uid "5E567DD5-4DDA-59AD-3D20-B9A86823E33A";
	setAttr ".t" -type "double3" 8.8595365645274504e-30 2.8421709430404007e-14 -8.8817841970012523e-15 ;
	setAttr ".s" -type "double3" 0.99999999999999967 1 0.99999999999999989 ;
createNode joint -n "L_upper_teeth_jnt" -p "Teeth_joints_Grp";
	rename -uid "90E5D381-4BBF-181F-9BAA-01BCD23D31AB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90.000000000000156 75.000000000000043 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.25881904510252018 -2.3585880124878049e-16 -0.96592582628906853 0
		 0.96592582628906809 -2.36513988710429e-15 0.25881904510252007 0 -2.2266907561572161e-15 -1 -3.8857805861880474e-16 0
		 2.9863363876683189 147.66974258422854 3.2675325437431768 1;
	setAttr ".radi" 0.5;
createNode parentConstraint -n "L_upper_teeth_jnt_parentConstraint1" -p "L_upper_teeth_jnt";
	rename -uid "F1F37D33-43B1-24AA-D7EA-D5A3B81180B3";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "L_upper_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 3.5527136788005009e-15 -1.7763568394002505e-15 
		-3.5527136788005009e-15 ;
	setAttr ".tg[0].tor" -type "double3" 1.4124500153760508e-30 0 1.5902773407317584e-15 ;
	setAttr ".lr" -type "double3" 1.2722218725854064e-14 6.3611093629270304e-15 6.3611093629270335e-15 ;
	setAttr ".rst" -type "double3" 1.5463641881942733 13.801017761230476 12.425179481506346 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "L_upper_teeth_jnt_scaleConstraint1" -p "L_upper_teeth_jnt";
	rename -uid "CE1F94F3-4FA3-42CF-5B2A-BBACB3A4E655";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "L_upper_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.99999999999999978 0.99999999999999933 0.99999999999999911 ;
	setAttr -k on ".w0";
createNode joint -n "C_upper_teeth_jnt" -p "Teeth_joints_Grp";
	rename -uid "30F6369C-4741-ED78-00AD-28A51CF192CA";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99999999999999989 2.2814897236420741e-16 1.8257625707878869e-17 0
		 -5.2289679590516637e-17 5.2735593669694946e-16 1 0 2.1579989801812771e-16 -1.0000000000000002 4.9960036108132044e-16 0
		 0.015849381635064216 147.28037659755654 6.097303687979351 1;
	setAttr ".radi" 0.5;
createNode parentConstraint -n "C_upper_teeth_jnt_parentConstraint1" -p "C_upper_teeth_jnt";
	rename -uid "C7FAF8A2-48D7-4FFE-DF78-AFB207A84C00";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "C_upper_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 0 1.7763568394002505e-15 -3.5527136788005009e-15 ;
	setAttr ".rst" -type "double3" -6.8550360765584628e-07 13.823082923889164 13.922321319580078 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "C_upper_teeth_jnt_scaleConstraint1" -p "C_upper_teeth_jnt";
	rename -uid "37EDBA30-400F-C985-52BE-EAA02E799CDD";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "C_upper_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1 0.99999999999999867 0.99999999999999867 ;
	setAttr -k on ".w0";
createNode joint -n "R_upper_teeth_jnt" -p "Teeth_joints_Grp";
	rename -uid "0375B4AF-46B6-9084-E577-1F919CF460CC";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999943 -75 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.091776733581064571 5.4700617117941932e-16 0.99577960973962043 0
		 -0.99577960973961999 3.5594589440456384e-16 0.091776733581064737 0 -4.5033391675696614e-16 -0.99999999999999978 3.8857805861880469e-16 0
		 -2.8578100891113278 147.66974258422854 3.267532543743179 1;
	setAttr ".radi" 0.5;
createNode parentConstraint -n "R_upper_teeth_jnt_parentConstraint1" -p "R_upper_teeth_jnt";
	rename -uid "3DB4A929-4AA4-E5F2-FA70-E794468278F5";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "R_upper_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -1.7763568394002505e-15 0 -1.7763568394002505e-15 ;
	setAttr ".tg[0].tor" -type "double3" 2.4717875269080888e-30 1.4124500153760508e-30 
		0 ;
	setAttr ".lr" -type "double3" -8.9813825530153438e-14 -5.1204696134978466e-15 9.7341704700277081 ;
	setAttr ".rst" -type "double3" -1.546364545822144 13.801013946533205 12.425180435180661 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "R_upper_teeth_jnt_scaleConstraint1" -p "R_upper_teeth_jnt";
	rename -uid "16871A8F-4D5D-6AF6-37EB-108DBAC12C07";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "R_upper_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
	setAttr -k on ".w0";
createNode joint -n "L_lower_teeth_jnt" -p "Teeth_joints_Grp";
	rename -uid "F86A4640-4EBC-DFD5-A15E-DDBA7266D044";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90 -75 179.99999999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.066225038138776379 4.005542136935994e-16 0.99780471251819502 0
		 0.99780471251819436 -1.9622183547807717e-16 0.066225038138776365 0 3.3931161429445029e-16 0.99999999999999978 -2.7755575615628909e-16 0
		 2.7891273882294061 145.54948796154756 3.212589737376303 1;
	setAttr ".radi" 0.5;
createNode parentConstraint -n "L_lower_teeth_jnt_parentConstraint1" -p "L_lower_teeth_jnt";
	rename -uid "BCB9ADE3-4213-5FB0-9895-F4A91C88A9C5";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "L_lower_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 1.7763568394002505e-15 8.8817841970012523e-16 
		3.5527136788005009e-15 ;
	setAttr ".tg[0].tor" -type "double3" -2.9131781567131048e-30 7.0622500768802538e-31 
		0 ;
	setAttr ".lr" -type "double3" 1.7819191944541933e-13 -7.8888314441182836e-15 11.202805765357859 ;
	setAttr ".rst" -type "double3" 1.5188629627227788 12.104801177978519 12.348910331726074 ;
	setAttr ".rsrr" -type "double3" -8.8278125961003172e-32 -3.1805546814635168e-15 
		3.1805546814635168e-15 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "L_lower_teeth_jnt_scaleConstraint1" -p "L_lower_teeth_jnt";
	rename -uid "D83C9F6A-46CC-9D38-A117-69A2B1664B46";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "L_lower_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1.0000000000000004 0.99999999999999956 0.99999999999999956 ;
	setAttr -k on ".w0";
createNode joint -n "C_lower_teeth_jnt" -p "Teeth_joints_Grp";
	rename -uid "2331ACEB-4366-D0CD-0464-E382D05FEFA9";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999986 7.0622500768802538e-31 180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.99999999999999989 -1.0568429244947204e-16 -1.8257625707878915e-17 0
		 -5.2289679590516526e-17 -1.3877787807814447e-16 1 0 -9.3335218103392424e-17 1.0000000000000002 -5.5511151231257901e-17 0
		 -1.2238680369605626e-05 144.83628321810983 5.7983579086669979 1;
	setAttr ".radi" 0.5;
createNode parentConstraint -n "C_lower_teeth_jnt_parentConstraint1" -p "C_lower_teeth_jnt";
	rename -uid "2D2649A2-4BEE-77FF-D181-31B0D7DE5BC2";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "C_lower_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 0 -1.7763568394002505e-15 0 ;
	setAttr ".tg[0].tor" -type "double3" 0 3.1362690564052943e-46 -1.5681345282026479e-46 ;
	setAttr ".rst" -type "double3" -7.9458857271674788e-07 12.119765281677246 13.597440719604489 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "C_lower_teeth_jnt_scaleConstraint1" -p "C_lower_teeth_jnt";
	rename -uid "15F8DECB-448B-409A-9F15-08B2C2955249";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "C_lower_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1 0.99999999999999889 0.99999999999999889 ;
	setAttr -k on ".w0";
createNode joint -n "R_lower_teeth_jnt" -p "Teeth_joints_Grp";
	rename -uid "2165043C-4496-EAA2-3BC7-C393B6C6EC83";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90.000000000000156 74.999999999999972 -179.99999999999955 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.25881904510252113 -2.3523588439109141e-15 -0.96592582628906853 0
		 -0.96592582628906776 -5.4704251155004637e-15 0.25881904510252118 0 -5.8224261723751672e-15 1.0000000000000002 -7.2164496600635195e-16 0
		 -2.692671600341797 145.54948796154756 3.212589737376303 1;
	setAttr ".radi" 0.5;
createNode parentConstraint -n "R_lower_teeth_jnt_parentConstraint1" -p "R_lower_teeth_jnt";
	rename -uid "A98A4926-475F-3821-DE36-C29DCB0D5431";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "R_lower_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -1.7763568394002505e-15 8.8817841970012523e-16 
		3.5527136788005009e-15 ;
	setAttr ".tg[0].tor" -type "double3" 2.6836550292144963e-29 1.6949400184512606e-29 
		-3.180554681463516e-15 ;
	setAttr ".lr" -type "double3" -1.9083328088781101e-14 3.1805546814635168e-15 -5.2966875576601921e-31 ;
	setAttr ".rst" -type "double3" -1.5188646316528325 12.104801177978519 12.348910331726074 ;
	setAttr ".rsrr" -type "double3" -1.9083328088781101e-14 3.1805546814635168e-15 -5.2966875576601921e-31 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "R_lower_teeth_jnt_scaleConstraint1" -p "R_lower_teeth_jnt";
	rename -uid "D5FA27B3-4226-C7E6-1FE0-4883CB9EBD87";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "R_lower_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
	setAttr -k on ".w0";
createNode transform -n "Tongue_Controls_Grp";
	rename -uid "6D2366AF-4A13-2427-799C-2A84C4A7467A";
	setAttr ".t" -type "double3" 2.3665827039080919e-30 2.8421709430404007e-14 -4.8849813083506888e-15 ;
createNode transform -n "tongue_01_Ctrl_Grp" -p "Tongue_Controls_Grp";
	rename -uid "1828CDB9-4C9E-2A79-EB56-2EA4A5675386";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".t" -type "double3" 1.380102491973467e-19 143.68276977539063 -1.441702008247375 ;
	setAttr ".r" -type "double3" -90 -48.100688621388002 90.000000000000028 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999978 1.0000000000000002 ;
createNode transform -n "tongue_01_Ctrl_Grp_rotate_offset" -p "tongue_01_Ctrl_Grp";
	rename -uid "A67988B8-42B5-CBE5-8521-DCB197F1E9BB";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
createNode transform -n "tongue_01_Ctrl_Grp_translate_offset" -p "tongue_01_Ctrl_Grp_rotate_offset";
	rename -uid "3BA867CD-412A-5B3D-B203-7197DB5B0F47";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 0 -3.7947076036992655e-19 ;
	setAttr ".s" -type "double3" 1 1.0000000000000004 1.0000000000000002 ;
createNode transform -n "tongue_01_Ctrl_Grp_offset" -p "tongue_01_Ctrl_Grp_translate_offset";
	rename -uid "1765A9C9-4158-C11A-6F1F-068AECF7A0AC";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 0 -2.1684043449710089e-19 ;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999989 1 ;
createNode transform -n "tongue_01_Ctrl" -p "tongue_01_Ctrl_Grp_offset";
	rename -uid "D8171B25-41F4-179B-C827-968D6688C24E";
	setAttr -l on -k off ".v";
	setAttr ".rp" -type "double3" 1.4210854715202004e-14 -1.4210854715202016e-14 5.96311192708704e-19 ;
	setAttr ".sp" -type "double3" 1.4210854715202004e-14 -1.4210854715202004e-14 5.96311192708704e-19 ;
	setAttr ".spt" -type "double3" 0 -1.3065508742723067e-29 0 ;
createNode nurbsCurve -n "tongue_01_CtrlShape" -p "tongue_01_Ctrl";
	rename -uid "98250DD1-4E95-4F03-763F-1AB177B88E04";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-0.062457137266068358 0.52397057173747907 1.7052390348361783
		-0.062457137266068358 0.52397057173747907 -1.7046608304477486
		2.2943521663370543 0.64002396840216358 -2.4710336881004564
		2.2943521663370987 0.64002396840217912 2.473312687971585
		-0.062457137266068358 0.52397057173747907 1.7052390348361783
		-0.041693038982734548 -0.70182472450514111 1.7041742318485296
		-0.041693038982734548 -0.70182472450514111 -1.7057256334353972
		-0.062457137266068358 0.52397057173747907 -1.7046608304477486
		-0.062457137266068358 0.52397057173747907 1.7052390348361783
		-0.041693038982734548 -0.70182472450514111 1.7041742318485296
		2.0379180158558841 -0.67699119678894359 2.5583761469076793
		2.2943521663370987 0.64002396840217912 2.473312687971585
		2.2943521663370543 0.64002396840216358 -2.4710336881004564
		2.0379180158559347 -0.67699119678894459 -2.5592640176962527
		2.0379180158558841 -0.67699119678894359 2.5583761469076793
		2.0379180158559347 -0.67699119678894459 -2.5592640176962527
		-0.041693038982734548 -0.70182472450514111 -1.7057256334353972
		;
createNode transform -n "tongue_02_Ctrl_Grp" -p "tongue_01_Ctrl";
	rename -uid "53366673-496B-7DF8-EC5C-358E11673E83";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".t" -type "double3" 2.2141598339521096 -2.8421709430404007e-14 1.1384122811287123e-18 ;
	setAttr ".r" -type "double3" 0 0 -23.679852013670622 ;
	setAttr ".s" -type "double3" 0.99999999999999911 1.0000000000000002 1.0000000000000009 ;
createNode transform -n "tongue_02_Ctrl_Grp_offset" -p "tongue_02_Ctrl_Grp";
	rename -uid "5D974FD7-41C5-15D3-EB73-18B20C1E2D32";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 2.1684043449710089e-19 ;
	setAttr ".s" -type "double3" 1 1.0000000000000004 1.0000000000000002 ;
createNode transform -n "tongue_02_Ctrl" -p "tongue_02_Ctrl_Grp_offset";
	rename -uid "F934AE2C-4D38-67C6-6FFB-6EB093323043";
	setAttr -l on -k off ".v";
	setAttr ".rp" -type "double3" -1.3269527698867023e-09 4.3675640881701818e-10 -1.0839416232143264e-19 ;
	setAttr ".sp" -type "double3" -1.3269527698867023e-09 4.3675640881701838e-10 -1.0839416232143247e-19 ;
	setAttr ".spt" -type "double3" 0 -2.0682039669775495e-25 -1.6851887013104222e-34 ;
createNode nurbsCurve -n "tongue_02_CtrlShape" -p "tongue_02_Ctrl";
	rename -uid "E19A5EA6-480D-5460-1092-47BB61826721";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-0.18360634919471 0.61835386521697122 2.4733125991447764
		-0.18360634919471 0.61835386521697122 -2.4710336863371278
		1.3109548890003322 0.49645275684900275 -2.3801056735544641
		1.3109548890003362 0.49645275684897017 2.3764963738947325
		-0.18360634919471 0.61835386521697122 2.4733125991447764
		0.11049096164595215 -0.69078424175854825 2.5583761664490003
		0.11049096164595215 -0.69078424175854825 -2.5592640204831434
		-0.18360634919471 0.61835386521697122 -2.4710336863371278
		-0.18360634919471 0.61835386521697122 2.4733125991447764
		0.11049096164595215 -0.69078424175854825 2.5583761664490003
		1.0714344951403745 -0.51396158186441687 2.3764963738947329
		1.3109548890003362 0.49645275684897017 2.3764963738947325
		1.3109548890003322 0.49645275684900275 -2.3801056735544641
		1.0714344951403665 -0.51396158186449614 -2.3801056735544641
		1.0714344951403745 -0.51396158186441687 2.3764963738947329
		1.0714344951403665 -0.51396158186449614 -2.3801056735544641
		0.11049096164595215 -0.69078424175854825 -2.5592640204831434
		;
createNode transform -n "tongue_03_Ctrl_Grp" -p "tongue_02_Ctrl";
	rename -uid "F1CC6AE5-49B5-FF4B-4954-CCBA29671ED9";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".t" -type "double3" 1.19327051118605 2.8421709430404007e-14 1.2742086032134629e-16 ;
	setAttr ".r" -type "double3" 0 0 -13.336003514900856 ;
	setAttr ".s" -type "double3" 0.99999999999999745 1 1.0000000000000016 ;
createNode transform -n "tongue_03_Ctrl_Grp_offset" -p "tongue_03_Ctrl_Grp";
	rename -uid "6CA0B267-4B8C-9C21-7DDB-1690B799DD98";
	setAttr ".t" -type "double3" 0 1.7763568394002505e-15 2.1684043449710089e-19 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
createNode transform -n "tongue_03_Ctrl" -p "tongue_03_Ctrl_Grp_offset";
	rename -uid "C8620E01-456A-1449-5AD5-289FD9F79C0E";
	setAttr -l on -k off ".v";
	setAttr ".rp" -type "double3" -6.49484478643103e-07 -7.6016991954475088e-06 2.4038328222708802e-21 ;
	setAttr ".sp" -type "double3" -6.494844786431031e-07 -7.6016991954475088e-06 2.4038328222708802e-21 ;
	setAttr ".spt" -type "double3" 1.0587911820957208e-22 0 0 ;
createNode nurbsCurve -n "tongue_03_CtrlShape" -p "tongue_03_Ctrl";
	rename -uid "E085D9FB-4367-4765-A238-509452E8F6AB";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		5.031531761584979e-15 0.51022796774422674 2.3764963206621279
		5.031531761584979e-15 0.51022796774422674 -2.3801056783185985
		1.625224545380868 0.50482165038840321 -2.1826262791044138
		1.6252756097358827 0.50001388250568324 2.1791011411988208
		5.031531761584979e-15 0.51022796774422674 2.3764963206621279
		-2.1366763747808845e-15 -0.52820951860187448 2.3764963206621279
		-2.1366763747808845e-15 -0.52820951860187448 -2.3801056783185985
		5.031531761584979e-15 0.51022796774422674 -2.3801056783185985
		5.031531761584979e-15 0.51022796774422674 2.3764963206621279
		-2.1366763747808845e-15 -0.52820951860187448 2.3764963206621279
		1.522693116129848 -0.48220996805713212 2.1791011412017927
		1.6251734810260143 0.50962941827123165 2.1791011412047618
		1.6252245453810232 0.50482165038840321 -2.1826262791044138
		1.522693116129848 -0.48220996805715105 -2.1826262791044138
		1.522693116129848 -0.48220996805713212 2.1791011412017927
		1.522693116129848 -0.48220996805715105 -2.1826262791044138
		-2.1366763747808845e-15 -0.52820951860187448 -2.3801056783185985
		;
createNode transform -n "tongue_04_Ctrl_Grp" -p "tongue_03_Ctrl";
	rename -uid "816DA21C-4E52-D4E9-6B48-A3A50D1616BE";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".t" -type "double3" 1.5815476449291666 0 2.702373914920246e-16 ;
	setAttr ".r" -type "double3" 0 0 -5.9305091909821863 ;
	setAttr ".s" -type "double3" 0.99999999999999523 0.99999999999999989 1.0000000000000051 ;
createNode transform -n "tongue_04_Ctrl_Grp_offset" -p "tongue_04_Ctrl_Grp";
	rename -uid "599F3096-4B8C-8790-70E1-1F98A1634619";
	setAttr ".t" -type "double3" 0 0 -5.4210108624275222e-20 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000002 1.0000000000000004 ;
createNode transform -n "tongue_04_Ctrl" -p "tongue_04_Ctrl_Grp_offset";
	rename -uid "4B240CB1-4F93-C825-83E6-B8BCA8730E30";
	setAttr -l on -k off ".v";
	setAttr ".rp" -type "double3" 1.1918937958821369e-07 2.1784103410086744e-09 -5.4015880628239472e-23 ;
	setAttr ".sp" -type "double3" 1.1918937958821374e-07 2.1784103410027456e-09 -2.1689445037181248e-19 ;
	setAttr ".spt" -type "double3" -5.2939556639595453e-23 5.9288170373183815e-21 2.1684043449118424e-19 ;
createNode nurbsCurve -n "tongue_04_CtrlShape" -p "tongue_04_Ctrl";
	rename -uid "4B420E3F-48DF-879C-5A69-148133FAD85A";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-0.008815223561619661 0.5076947954049098 2.1791011450679307
		-0.008815223561619661 0.5076947954049098 -2.1826262044599121
		1.3673975342214248 0.46590935917200238 -1.8972675634712939
		1.3676662404610456 0.4659142701984727 1.8972675634712897
		-0.008815223561619661 0.5076947954049098 2.1791011450679307
		-0.0088152235616330982 -0.4846529866911497 2.1791011450679187
		-0.0088152235616330982 -0.4846529866911497 -2.1826262044599156
		-0.008815223561619661 0.5076947954049098 -2.1826262044599121
		-0.008815223561619661 0.5076947954049098 2.1791011450679307
		-0.0088152235616330982 -0.4846529866911497 2.1791011450679187
		1.3622257773026434 -0.47726718789644024 1.8972675634712894
		1.3676662404610456 0.4659142701984727 1.8972675634712897
		1.3673975342214248 0.46590935917200238 -1.8972675634712939
		1.3619565641183542 -0.47724436150462679 -1.8972675634712945
		1.3622257773026434 -0.47726718789644024 1.8972675634712894
		1.3619565641183542 -0.47724436150462679 -1.8972675634712945
		-0.0088152235616330982 -0.4846529866911497 -2.1826262044599156
		;
createNode transform -n "tongue_05_Ctrl_Grp" -p "tongue_04_Ctrl";
	rename -uid "17C9CE15-4C9E-8BD2-9BB6-BF9367F99FE6";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".t" -type "double3" 1.3648439306084601 2.8421709430404007e-14 2.6690346981160643e-16 ;
	setAttr ".r" -type "double3" -1.1725138118817024e-05 0.0040652135736868843 -0.33051108044916527 ;
	setAttr ".s" -type "double3" 0.99999999999998868 1.0000000000000004 1.0000000000000122 ;
createNode transform -n "tongue_05_Ctrl_Grp_offset" -p "tongue_05_Ctrl_Grp";
	rename -uid "C68C4A69-482D-2609-3731-19B5BF12E2FE";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 0 0 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000002 ;
createNode transform -n "tongue_05_Ctrl" -p "tongue_05_Ctrl_Grp_offset";
	rename -uid "C47AF814-4A05-D781-B414-84B37BE1980E";
	setAttr -l on -k off ".v";
	setAttr ".rp" -type "double3" 1.8341613139405371e-07 -7.6271894329238421e-06 1.1452778599075307e-11 ;
	setAttr ".sp" -type "double3" 1.834161312830318e-07 -7.6271894329238421e-06 1.1452778599075307e-11 ;
	setAttr ".spt" -type "double3" 1.1102190541582244e-16 0 0 ;
createNode nurbsCurve -n "tongue_05_CtrlShape" -p "tongue_05_Ctrl";
	rename -uid "5952EF03-40C6-64E1-F79B-97B5FFC0C2D7";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-1.6732080406848097e-16 0.46593131809691862 1.8972674783649608
		-1.6732080406848097e-16 0.46593131809691862 -1.8972674783649481
		1.4118145728834144 0.39207559596117136 -1.2597368802363429
		1.4118145728834144 0.39207559596117136 1.2595663720166181
		-1.6732080406848097e-16 0.46593131809691862 1.8972674783649608
		-7.0198475837863433e-15 -0.4772614626028106 1.8972674783649521
		-7.0198475837863433e-15 -0.4772614626028106 -1.8972674783649561
		-1.6732080406848097e-16 0.46593131809691862 -1.8972674783649481
		-1.6732080406848097e-16 0.46593131809691862 1.8972674783649608
		-7.0198475837863433e-15 -0.4772614626028106 1.8972674783649521
		1.4118145728834082 -0.38176739500050133 1.2595663720166124
		1.4118145728834144 0.39207559596117136 1.2595663720166181
		1.4118145728834144 0.39207559596117136 -1.2597368802363429
		1.4118145728834082 -0.38176739500050133 -1.2597368802363509
		1.4118145728834082 -0.38176739500050133 1.2595663720166124
		1.4118145728834082 -0.38176739500050133 -1.2597368802363509
		-7.0198475837863433e-15 -0.4772614626028106 -1.8972674783649561
		;
createNode transform -n "Tongue_joints_Grp";
	rename -uid "CD725FCC-4CFA-D611-981A-AD878D1603C8";
	setAttr ".t" -type "double3" 8.8595365645274504e-30 2.8421709430404007e-14 -8.8817841970012523e-15 ;
	setAttr ".s" -type "double3" 0.99999999999999967 1 0.99999999999999989 ;
createNode joint -n "tongue_01_jnt" -p "Tongue_joints_Grp";
	rename -uid "8EC98FAE-4C96-19E2-B700-C6971134633F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 1.3801024918769823e-19 143.68276977539063 -1.4417020082473806 ;
	setAttr ".r" -type "double3" -3.1805546814635168e-15 8.8278125961003172e-32 3.1805546814635168e-15 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -90 -48.100688621388002 90.000000000000028 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -4.0508110377239025e-16 0.66782360975780197 0.74431957266355608 0
		 -3.4774841022150546e-16 0.74431957266355642 -0.66782360975780197 0 -0.99999999999999989 -5.6121587975175438e-16 -1.8257625707878777e-17 0
		 1.3801024919548883e-19 143.68276977539065 -1.4417020082473893 1;
	setAttr ".typ" 1;
	setAttr ".radi" 0.5;
createNode joint -n "tongue_02_jnt" -p "tongue_01_jnt";
	rename -uid "2910957C-466A-F894-2203-9987F865E7FD";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 2.2141598339521096 2.8421709430404007e-14 6.5052130349130266e-19 ;
	setAttr ".r" -type "double3" 0 0 -6.3611093629270335e-15 ;
	setAttr ".s" -type "double3" 1 1.0000000000000009 1.0000000000000007 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -23.679852013670622 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -2.313101273780839e-16 0.31265753948934866 0.9498659184329471 0
		 -4.8116035000546074e-16 0.94986591843294821 -0.3126575394893491 0 -1.0000000000000004 -5.6121587975175477e-16 -1.8257625707878789e-17 0
		 -4.9215475662506614e-16 145.16143798828131 0.206340493168647 1;
	setAttr ".radi" 0.5;
createNode joint -n "tongue_03_jnt" -p "tongue_02_jnt";
	rename -uid "2245ED55-4783-CEB5-8740-0AAB1CD76C67";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 1.1932705111860784 5.6843418860808015e-14 1.2774612097309825e-16 ;
	setAttr ".r" -type "double3" 0 0 -1.2722218725854067e-14 ;
	setAttr ".s" -type "double3" 0.999999999999999 0.99999999999999933 1.0000000000000004 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -13.336003514900856 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -1.1408761444385797e-16 0.085129224987064905 0.9963699187817241 0
		 -5.2153971437998091e-16 0.99636991878172543 -0.085129224987065683 0 -1.0000000000000009 -5.6121587975175507e-16 -1.8257625707878798e-17 0
		 -7.5613714190094772e-16 145.53452301025396 1.3397874832153414 1;
	setAttr ".radi" 0.5;
createNode joint -n "tongue_04_jnt" -p "tongue_03_jnt";
	rename -uid "51686BC4-444A-A01F-59A9-1B90E501C3F6";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 1.5815476449291914 0 2.7018318138338139e-16 ;
	setAttr ".r" -type "double3" 0 0 -2.4649298781342254e-14 ;
	setAttr ".s" -type "double3" 0.99999999999999922 1.0000000000000007 1.0000000000000033 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -5.9305091909821863 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -5.9590386667410244e-17 -0.018273511105107568 0.99983302545569464 0
		 -5.3053618468665393e-16 0.99983302545569741 0.018273511105106693 0 -1.0000000000000042 -5.6121587975175694e-16 -1.825762570787886e-17 0
		 -1.09530248537118e-15 145.66915893554693 2.9155939817428607 1;
	setAttr ".radi" 0.50052768041686158;
createNode joint -n "tongue_05_jnt" -p "tongue_04_jnt";
	rename -uid "F6209C24-4E84-ABCE-AF9D-77A3415E78BE";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 1.3648439306084543 -5.6843418860808015e-14 2.669239693748925e-16 ;
	setAttr ".r" -type "double3" 1.9958543590574262e-18 -2.17394353320093e-17 -2.8724384434524254e-14 ;
	setAttr ".s" -type "double3" 0.99999999999999434 1 1.0000000000000049 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.1725138118817035e-05 0.0040652135736869467 -0.33051108044916527 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 7.0951361598016306e-05 -0.024040718401777295 0.99971097764534711 0
		 2.0464226437848531e-07 0.99971098016200399 0.024040718447772225 0 -0.99999999748294055 -1.5011385895065452e-06 7.0935774994083974e-05 0
		 -1.3775848523579696e-15 145.64421844482416 4.2802100181579528 1;
	setAttr ".radi" 0.50227282651671001;
createNode joint -n "tongue_06_jnt" -p "tongue_05_jnt";
	rename -uid "D5E8587C-4E1E-0828-2334-55954677DEF5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 1.3809673054870384 -9.2148526219454302e-15 -2.4615883883883368e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -8.4082091718014191e-05 -1.8223526324225785e-05 -12.228858466511156 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 6.898002049420542e-05 -0.23525106892065945 0.97193463247968992 0
		 1.6696217647564905e-05 0.97193463492751864 0.23525106832817572 0 -0.99999999748150592 8.6484937003834339e-12 7.0971874084901202e-05 0
		 9.7981510645517674e-05 145.61101899871088 5.6607781932226606 1;
	setAttr ".radi" 0.50227282651671001;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "C2CE5E48-421A-39DE-D71D-E09D829D80B9";
	setAttr -s 6 ".lnk";
	setAttr -s 4 ".slnk";
createNode VRaySettingsNode -s -n "vraySettings";
	rename -uid "6A0F5999-4453-899C-B76F-E2B48BED0D94";
	setAttr ".gi" yes;
	setAttr ".rfc" yes;
	setAttr ".pe" 2;
	setAttr ".se" 3;
	setAttr ".cmph" 60;
	setAttr ".cfile" -type "string" "";
	setAttr ".cfile2" -type "string" "";
	setAttr ".casf" -type "string" "";
	setAttr ".casf2" -type "string" "";
	setAttr ".st" 3;
	setAttr ".msr" 6;
	setAttr ".aaft" 3;
	setAttr ".aafs" 2;
	setAttr ".dma" 24;
	setAttr ".dam" 1;
	setAttr ".pt" 0.0099999997764825821;
	setAttr ".pmt" 0;
	setAttr ".sd" 1000;
	setAttr ".ss" 0.01;
	setAttr ".pfts" 20;
	setAttr ".ufg" yes;
	setAttr ".fnm" -type "string" "";
	setAttr ".lcfnm" -type "string" "";
	setAttr ".asf" -type "string" "";
	setAttr ".lcasf" -type "string" "";
	setAttr ".urtrshd" yes;
	setAttr ".rtrshd" 2;
	setAttr ".lightCacheType" 1;
	setAttr ".ifile" -type "string" "";
	setAttr ".ifile2" -type "string" "";
	setAttr ".iasf" -type "string" "";
	setAttr ".iasf2" -type "string" "";
	setAttr ".pmfile" -type "string" "";
	setAttr ".pmfile2" -type "string" "";
	setAttr ".pmasf" -type "string" "";
	setAttr ".pmasf2" -type "string" "";
	setAttr ".dmcstd" yes;
	setAttr ".dmculs" no;
	setAttr ".dmcsat" 0.004999999888241291;
	setAttr ".cmtp" 6;
	setAttr ".cmao" 2;
	setAttr ".cg" 2.2000000476837158;
	setAttr ".mtah" yes;
	setAttr ".rgbcs" -1;
	setAttr ".suv" 0;
	setAttr ".srflc" 1;
	setAttr ".srdml" 0;
	setAttr ".seu" yes;
	setAttr ".gormio" yes;
	setAttr ".gocle" yes;
	setAttr ".gopl" 2;
	setAttr ".wi" 960;
	setAttr ".he" 540;
	setAttr ".aspr" 1.7777780294418335;
	setAttr ".productionGPUResizeTextures" 0;
	setAttr ".autolt" 0;
	setAttr ".jpegq" 100;
	setAttr ".vfbOn" yes;
	setAttr ".vfbSA" -type "Int32Array" 847 124 20 0 0 0 0
		 0 0 0 0 -1073724351 0 0 0 0 0 0 0
		 0 0 0 -1074790400 0 -1074790400 0 -1074790400 0 -1074790400 1 0
		 0 0 3254 1 3242 0 1 3234 1700143739 1869181810 825893486 1632379436
		 1936876921 578501154 1936876886 577662825 573321530 1935764579 574235251 1953460082 1881287714 1701867378 1701409906 2067407475
		 1919252002 1852795251 741423650 1835101730 574235237 1696738338 1818386798 1949966949 744846706 1886938402 577007201 1818322490
		 573334899 1634760805 1650549870 975332716 1702195828 1931619453 1814913653 1919252833 1530536563 1818436219 577991521 1751327290
		 779317089 1886611812 1132028268 1701999215 1869182051 573317742 1886351984 1769239141 975336293 1702240891 1869181810 825893486
		 1634607660 975332717 1936278562 2036427888 1919894304 1952671090 577662825 1852121644 1701601889 1920219682 573334901 1634760805
		 975332462 1702195828 2019893804 1684955504 1701601889 1920219682 573334901 1718579824 577072233 573321530 1869641829 1701999987
		 774912546 1763847216 1717527395 577072233 740434490 1667459362 1852142175 1953392996 578055781 573321274 1886088290 1852793716
		 1715085942 1702063201 1668227628 1717530473 577072233 740434490 1768124194 1868783471 1936879468 1701011824 741358114 1768124194
		 1768185711 1634496627 1986356345 577069929 573321274 1869177711 1701410399 1634890871 1868985198 975334770 1864510512 1601136995
		 1702257011 1835626089 577070945 1818322490 746415475 1651864354 2036427821 577991269 578509626 1935764579 574235251 1868654691
		 1701981811 1869819494 1701016181 1684828006 740455013 1869770786 1953654128 577987945 1981971258 1769173605 975335023 1847733297
		 577072481 1867719226 1701016181 1196564538 573317698 1650552421 975332716 1702195828 2019893804 1684955504 1634089506 744846188
		 1886938402 1633971809 577072226 1818322490 573334899 1667330159 578385001 808333626 1818370604 1600417381 1701080941 741358114
		 1668444962 1887007839 809116261 1931619453 1814913653 1919252833 1530536563 1818436219 577991521 1751327290 779317089 778462578
		 1751607660 2020175220 1881287714 1701867378 1701409906 2067407475 1919252002 1852795251 741423650 1835101730 574235237 1751607628
		 2020167028 1696738338 1818386798 1715085925 1702063201 2019893804 1684955504 1634089506 744846188 1886938402 1633971809 577072226
		 1970435130 573341029 761427315 1702453612 975336306 746413403 1818436219 577991521 1751327290 779317089 778462578 1886220131
		 1953067887 573317733 1886351984 1769239141 975336293 1702240891 1869181810 825893486 1634607660 975332717 1836008226 1769172848
		 740451700 1634624802 577072226 1818322490 573334899 1634760805 975332462 1936482662 1696738405 1851879544 1818386788 1949966949
		 744846706 1634758434 2037672291 774978082 1646406704 1684956524 1685024095 809116261 1931619453 1814913653 1919252833 1530536563
		 2103278941 1663204140 1936941420 1663187490 1936679272 778399790 1869505892 1919251305 1881287714 1701867378 1701409906 2067407475
		 1919252002 1852795251 741423650 1835101730 574235237 1869505860 1919251305 1853169722 1767994977 1818386796 573317733 1650552421
		 975332716 1936482662 1696738405 1851879544 1715085924 1702063201 2019893804 1684955504 1701601889 1920219682 573334901 1667330159
		 578385001 808333626 1818370604 1600417381 1701080941 741358114 1952669986 577074793 1818322490 573334899 1936028272 975336549
		 1931619378 1852142196 577270887 808333626 1634869804 1937074532 808532514 573321262 1665234792 1701602659 1702125938 1920219682
		 573334901 1869505892 1919251305 1685024095 825893477 1931619453 1814913653 1919252833 1530536563 2066513245 1634493218 975336307
		 1634231074 1882092399 1701588581 2019980142 1881287714 1701867378 1701409906 2067407475 1919252002 1852795251 741423650 1835101730
		 574235237 1936614732 1717978400 1937007461 1696738338 1818386798 1715085925 1702063201 2019893804 1684955504 1634089506 744846188
		 1886938402 1633971809 577072226 1970435130 1864510565 1768120688 975337844 741355057 1701601826 1834968174 577070191 573321786
		 1918987367 1852792677 1634089506 744846188 1634494242 1935631730 577075817 774910778 1730292784 1701994860 1768257375 578054247
		 808333626 1818370604 1601007471 1734960503 975336552 808726064 808464432 959787056 1730292790 1701994860 1919448159 1869116261
		 975332460 741355057 1818846754 1601332596 1635020658 1852795252 774912546 1931619376 1920300129 1869182049 825893486 573321262
		 1685217640 1701994871 1667457375 1919249509 1684370529 1920219682 573334901 1684828003 1918990175 1715085933 1702063201 1852383788
		 1634887028 1986622563 1949966949 744846706 1634624802 1600482402 1684106338 975336293 1702195828 1769153068 577987940 573322810
		 1684106338 1918858085 1952543855 577662825 775237946 1931619376 1634038388 1818386283 975336053 808594992 808464432 959590448
		 1965173816 1734305139 1769234802 975333230 1936482662 1730292837 1769234802 1683973998 1769172581 975337844 808333365 1919361580
		 1852404833 1701601127 1752459118 808532514 573321262 1952543335 1600613993 1836019578 775240226 1730292784 1769234802 1935632238
		 1701867372 774912546 1730292784 1769234802 1935632238 1852142196 577270887 808333626 1937056300 1668243301 1937075299 577662825
		 1818322490 573334899 1818452847 1869181813 2037604206 1952804205 576940402 1970435130 1864510565 1970037603 1852795251 1919250527
		 1953391971 808598050 573321262 1818452847 1869181813 1869766510 1769234804 975335023 741355056 1667460898 1769174380 1633644143
		 975332210 774910001 1965173808 1935631731 1952543331 975333475 1936482662 1931619429 1935635043 1701670265 1667854964 1920219682
		 573334901 1601332083 1953784176 577663589 573321274 1601332083 1953265005 1634494313 1667196274 1953396079 741423650 1919120162
		 1852138591 2037672307 808794658 573321262 1601332083 1735288172 975333492 808333365 1668489772 1819500402 1600483439 1769103734
		 1701015137 774912546 1931619376 2002743907 1752458345 1918989919 1668178281 809116261 573321262 1601332083 1684366707 741358114
		 1919120162 1869576799 842670701 573321262 1601332083 1635020658 1852795252 774912546 1931619376 1935635043 1852142196 577270887
		 808333626 1937056300 1969512293 975336563 1936482662 1679961189 1601467253 1953784176 577663589 573321274 1953723748 1852138591
		 2037672307 808794658 573321262 1953723748 1684107871 1601402217 1769103734 1701015137 774912546 1679961136 1601467253 1953786218
		 975336037 741355056 1937073186 1870290804 975334767 741355058 1937073186 1869766516 1769234804 975335023 741355056 1937073186
		 1953718132 1735288178 975333492 741355057 1634494242 1969186162 1868522867 1635021666 1600482403 1734438249 1715085925 1702063201
		 1818698284 1600483937 1953718895 1701602145 1634560351 1885300071 577270881 740434490 1935830818 1835622260 1600481121 1836019578
		 774978082 1864510512 1601467234 1734438249 1869766501 1769234804 975335023 741355056 1935830818 1835622260 1600481121 1701999731
		 1752459118 774978082 1965173808 1717527923 1702128745 1835622258 577070945 1818322490 573334899 1918987367 1835622245 1600481121
		 1752457584 572668450 1931619453 1814913653 1919252833 1530536563 2103278941 573341021 1768383826 1699180143 2067407470 1919252002
		 1852795251 741423650 1970236706 1717527923 1869376623 1852137335 1701601889 1715085924 1702063201 1869423148 1600484213 1819045734
		 1885304687 1953393007 1668246623 577004907 1818322490 573334899 1937076077 1868980069 2003790956 1768910943 2019521646 741358114
		 1970236706 1717527923 1869376623 1869635447 1601465961 809116281 1377971325 1701080677 1701402226 2067407479 1919252002 1852795251
		 741423650 1634624802 1600482402 1684956530 1918857829 1869178725 1715085934 1702063201 1701978668 1919247470 1734701663 1601073001
		 975319160 808333613 1701978668 1919247470 1734701663 1601073001 975319161 808333613 1701978668 1919247470 1734701663 1601073001
		 975319416 808333613 1701978668 1919247470 1734701663 1601073001 975319417 808333613 1769349676 1918859109 975332453 1702195828
		 1769349676 1734309733 1852138866 1920219682 573334901 2003134838 1970037343 1949966949 744846706 1701410338 1869438839 975335278
		 1936482662 1663183973 1919904879 1634493279 1834971245 577070191 741946938 1819239202 1667199599 1886216556 577662815 1970435130
		 1881287781 1818589289 1718511967 1869373295 1684368227 1634089506 744846188 2020175906 1767861349 1601136238 1920102243 1702126437
		 1868783460 1936879468 1634089506 2103800684 1665212972 1852795252 2067407475 1919252002 1852795251 741423650 1969319970 1346987379
		 1751342930 1701536613 1715085924 1702063201 1937056300 2020167781 1933667429 1952671088 1701339999 1684368227 1634089506 744846188
		 1936028706 1936020084 1684107871 975335273 573321525 1953719668 1601398098 1667590243 577004907 1818322490 573334899 1969382756
		 1634227047 1735289188 1684107871 975335273 573322805 1969382756 1634227047 1735289188 1701339999 1684368227 1634089506 744846188
		 1702130466 1299146098 1600480367 1768186226 926556783 1931619384 1701995892 1685015919 1751342949 1701536613 1715085924 1702063201
		 32125 ;
	setAttr ".mSceneName" -type "string" "D:/Rig/MERTROPOLIS/MTP_TongueAndTeeth_Rig.ma";
	setAttr ".rt_cpuRayBundleSize" 4;
	setAttr ".rt_gpuRayBundleSize" 128;
	setAttr ".rt_maxPaths" 10000;
	setAttr ".rt_engineType" 3;
	setAttr ".rt_gpuResizeTextures" 0;
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "A9AA744B-4350-6131-2BF9-0B8E5EBEF0CD";
	setAttr -s 2 ".bsdt";
	setAttr ".bsdt[0].bscd" -type "Int32Array" 1 -1 ;
	setAttr ".bsdt[1].bscd" -type "Int32Array" 0 ;
	setAttr ".bsdt[1].bsdn" -type "string" "Group 1";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "8EC83D2D-4192-CB3E-3893-3C95F2E74DF4";
createNode displayLayerManager -n "layerManager";
	rename -uid "A108C890-4DFB-5661-3224-6799A0CB1CEF";
	setAttr ".cdl" 2;
	setAttr -s 3 ".dli[1:2]"  2 4;
createNode displayLayer -n "defaultLayer";
	rename -uid "38111566-40B0-8351-E6A6-FDB9F34E59F4";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "E2D31F6D-4351-40D3-5EC0-38B51AE6BAF9";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "5F384F71-4EFB-59BB-4B84-C09611D91F6E";
	setAttr ".g" yes;
createNode renderSetup -n "renderSetup";
	rename -uid "6971BB56-4E3C-84A4-B228-F2A907C435A7";
createNode script -n "uiConfigurationScriptNode";
	rename -uid "C6643C05-494E-F6FC-2A4B-0680E2E734E4";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 0.6\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 0.6\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n"
		+ "            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n"
		+ "            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 0.6\n            -textureAnisotropic 0\n"
		+ "            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n"
		+ "            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n"
		+ "            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 0.6\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n"
		+ "            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n"
		+ "            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1209\n            -height 706\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n"
		+ "            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n"
		+ "            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n"
		+ "            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n"
		+ "            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n"
		+ "                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n"
		+ "                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n"
		+ "                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n"
		+ "                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n"
		+ "                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n"
		+ "                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n"
		+ "                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n"
		+ "                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n"
		+ "                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "{ string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 0.6\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n"
		+ "                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName; };\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 0.6\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1209\\n    -height 706\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 0.6\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1209\\n    -height 706\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "C2C92B42-46AB-E62F-7938-759D9E2E710B";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "9693D2EB-43DE-CE3F-431E-BF80ACE23C4B";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -622.61902287839052 -174.9999930461251 ;
	setAttr ".tgi[0].vh" -type "double2" 592.85711929911758 407.14284096445425 ;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo1";
	rename -uid "91EC58A1-45AD-5079-F089-759B6F097E36";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -622.61902287839052 -174.9999930461251 ;
	setAttr ".tgi[0].vh" -type "double2" 592.85711929911758 407.14284096445425 ;
createNode animCurveTU -n "BS_body_root_MSH_body_squeeze1_Shape";
	rename -uid "7A1DD1B5-43BC-C669-F138-A4B636E13A6B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 17 0 30 1;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo3";
	rename -uid "5BC584DB-461E-36C4-AA34-66B0E2609D69";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -5066.4034829977691 3367.7242350210277 ;
	setAttr ".tgi[0].vh" -type "double2" -770.55877479946446 4351.7579602868564 ;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo4";
	rename -uid "79896E7B-4684-9ED6-EC81-9CA4C2AD4A29";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -511.94655948481727 -100.99472870802012 ;
	setAttr ".tgi[0].vh" -type "double2" 723.05588272111322 338.09522466054091 ;
createNode unitConversion -n "unitConversion380";
	rename -uid "A21C49AC-4F8E-DA8A-D4D7-9CBBA61B3CB1";
	setAttr ".cf" 0.017453292519943295;
createNode expression -n "Face_Expr";
	rename -uid "D39FEA08-46FF-D9C6-962F-F09051831885";
	setAttr -k on ".nds";
	setAttr -s 128 ".in[0:127]"  0 0 0 0 0 0 1 1 1 0 0 0 0 0 0 1 1 1 0 0 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 7 0 7 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 29 ".out";
	setAttr ".ixp" -type "string" (
		"//TEETH_EXP\n\n.O[0] = 2*(.I[0]);\n.O[1] = 2*(.I[1]);\n.O[2] = 2*(.I[2]);\n.O[3] = 2*(.I[3]);\n.O[4] = 2*(.I[4]);\n.O[5] = 2*(.I[5]);\n.O[6] = (.I[6]);\n.O[7] = (.I[7]);\n.O[8] = (.I[8]);\n\n.O[9] = 2*(.I[9]);\n.O[10] = 2*(.I[10]);\n.O[11] = 2*(.I[11]);\n.O[12] = 2*(.I[12]);\n.O[13] = 2*(.I[13]);\n.O[14] = 2*(.I[14]);\n.O[15] = (.I[15]);\n.O[16] = (.I[16]);\n.O[17] = (.I[17]);\n\n//Jaw_Translate\n\n.O[18] = 0\n-1 *.I[18]*(.I[18]<0)\n-1 *.I[18]*(.I[18]>0);\n\n.O[19] = 0\n+1 *.I[19]*(.I[19]<0)\n+1 *.I[19]*(.I[19]>0);\n\n.O[20] = 0\n+1 *.I[20]*(.I[20]<0)\n+1 *.I[20]*(.I[20]>0);\n\n//Jaw_Rotate \n\n.O[21] = 0\n-25*.I[21]*(.I[21]<0)\n-5*.I[21]*(.I[21]>0);\n\n.O[22] = 0\n+20*.I[22]*(.I[22]>0)\n+20*.I[22]*(.I[22]<0);\n\n//Tongue_Rotation\n\n.O[23] = 0\n+20*.I[23]*(.I[23]<0)\n+20*.I[23]*(.I[23]>0);\n\n.O[24] = 0\n+20*.I[24]*(.I[24]<0)\n+20*.I[24]*(.I[24]>0);\n\n.O[25] = 0\n+20*.I[23]*(.I[23]<0)\n+20*.I[23]*(.I[23]>0);\n\n.O[26] = 0\n+20*.I[24]*(.I[24]<0)\n+20*.I[24]*(.I[24]>0);\n\n.O[27] = 0\n+20*.I[23]*(.I[23]<0)\n+20*.I[23]*(.I[23]>0);\n\n.O[28] = 0\n+20*.I[24]*(.I[24]<0)\n+20*.I[24]*(.I[24]>0);\n"
		+ "\n.O[29] = 0\n+20*.I[23]*(.I[23]<0)\n+20*.I[23]*(.I[23]>0);\n\n.O[30] = 0\n+20*.I[24]*(.I[24]<0)\n+20*.I[24]*(.I[24]>0);\n\n.O[31] = 0\n+20*.I[23]*(.I[23]<0)\n+20*.I[23]*(.I[23]>0);\n\n.O[32] = 0\n+20*.I[24]*(.I[24]<0)\n+20*.I[24]*(.I[24]>0);\n\n//Tongue_Up_Down \n\n.O[33] = 0\n+1*.I[25]*(.I[25]>0)\n+1*.I[25]*(.I[25]<0);\n\n// PHONEME - BODY\n\n.O[34] = clamp ( 0, 1, (.I[26] * 0.1)) ;\n.O[35] = clamp ( 0, 1, (.I[27] * 0.1) ) ;\n.O[36] = clamp ( 0, 1, (.I[28] * 0.1)) ; \n.O[37] = clamp ( 0, 1, (.I[29] * 0.1)) ; \n.O[38] = clamp ( 0, 1, (.I[30] * 0.1)) ; \n.O[39] = clamp ( 0, 1, (.I[31] * 0.1)) ;\n.O[40] = clamp ( 0, 1, (.I[32] * 0.1)) ; \n.O[41] = clamp ( 0, 1, (.I[33] * 0.1)) ; \n.O[42] = clamp ( 0, 1, (.I[34] * 0.1)) ; \n.O[43] = clamp ( 0, 1, (.I[35] * 0.1)) ; \n.O[44] = clamp ( 0, 1, (.I[36] * 0.1)) ; \n.O[45] = clamp ( 0, 1, (.I[37] * 0.1)) ; \n.O[46] = clamp ( 0, 1, (.I[38] * 0.1)) ; \n.O[47] = clamp ( 0, 1, (.I[39] * 0.1)) ; \n.O[48] = clamp ( 0, 1, (.I[40] * 0.1)) ; \n.O[49] = clamp ( 0, 1, (.I[41] * 0.1)) ;\n.O[50] = clamp ( 0, 1, (.I[42] * 0.1)) ;\n"
		+ ".O[51] = clamp ( 0, 1, (.I[43] * 0.1)) ;\n.O[52] = clamp ( 0, 1, (.I[44] * 0.1)) ;\n.O[53] = clamp ( 0, 1, (.I[45] * 0.1)) ;\n.O[54] = clamp ( 0, 1, (.I[46] * 0.1)) ;\n\n// MOUTH - BODY\n\n.O[55] = clamp ( -1, 1, (.I[47]+.I[48]) * 0.1 ) ;\n.O[56] = clamp ( -1, 1, (.I[49]+.I[48]) * 0.1 ) ;\n.O[57] = clamp ( -1, 1, (.I[50]+.I[51]) * 0.1) ;\n.O[58] = clamp ( -1, 1, (.I[52]) * 0.1) ;\n.O[59] = clamp ( -1, 1, (.I[53]) * 0.1) ;\n.O[60] = clamp ( -1, 1, (.I[54]) * 0.1) ;\n.O[61] = clamp ( -1, 1, (.I[55]+.I[56]) * 0.1) ;\n.O[62] = clamp ( -1, 1, (.I[57]+.I[56]) * 0.1) ;\n.O[63] = clamp ( 0, 1, (.I[58]+.I[59]) * 0.1) ;\n.O[64] = clamp ( 0, 1, (.I[60] + .I[61] ) * 0.1) ;\n.O[65] = clamp ( 0, 1, (.I[62]+.I[63]) * 0.1) ;\n.O[66] = clamp ( -1, 1, (.I[64] + .I[65]) * 0.1) ;\n.O[67] = clamp ( -1, 1, (.I[66] + .I[65]) * 0.1) ;\n.O[68] = clamp ( -1, 1, (.I[67]+.I[68]) * 0.1) ;\n.O[69] = clamp ( -1, 1, (.I[69] * 0.1) ) ;\n.O[70] = clamp ( -1, 1, (.I[70] * 0.1) ) ;\n.O[71] = clamp ( -1, 1, (.I[71]  * 0.1) ) ;\n.O[72] = clamp ( -1, 1, (.I[72]  * 0.1) ) ;\n"
		+ ".O[73] = clamp ( 0, 1, (.I[73]  * 0.1) ) ;\n.O[74] = clamp ( -1, 1, (.I[74]+.I[75]  * 0.1) ) ;\n\n// BROWS - BODY\n\n.O[75] = clamp ( -1, 1, ((.I[76]+.I[48] + .I[65] + .I[68] + .I[75])*0.1 - .I[77]*-1) ) ;\n.O[76] = clamp ( -1, 1, ((.I[78]+.I[48] + .I[65] + .I[68] + .I[75])*0.1 - .I[79]*-1) ) ;\n.O[77] = clamp ( 0, 1, ((.I[80]+.I[51])*0.1 + .I[81]*-1)) ;\n.O[78] = clamp ( 0, 1, ((.I[82]+.I[51])*0.1 + .I[83]*1)) ;\n.O[79] = clamp ( 0, 1, (.I[84]+.I[56])*0.1+.I[85]*-1 ) ;\n.O[80] = clamp ( 0, 1, (.I[86]+.I[56])*0.1+.I[87]*-1 ) ;\n.O[81] = clamp ( 0, 1, (.I[88]+.I[59] * 0.1) ) ;\n.O[82] = clamp ( 0, 1, (.I[89]+.I[59] * 0.1) ) ;\n.O[83] = clamp ( 0, 1, (.I[90] + .I[61])*0.1+.I[85]*-1) ;\n.O[84] = clamp ( 0, 1, (.I[91] + .I[61])*0.1+.I[87]*-1) ;\n.O[85] = clamp ( 0, 1, ((.I[92] * 0.1) + .I[77]*-1)) ;\n.O[86] = clamp ( 0, 1, ((.I[93] * 0.1) + .I[79]*-1)) ;\n.O[87] = clamp ( 0, 1, (.I[94] * 0.1) ) ;\n.O[88] = clamp ( 0, 1, (.I[95] * 0.1) ) ;\n.O[89] = clamp ( 0, 1, ((.I[96] * 0.1) + .I[97]*-1) ) ;\n.O[90] = clamp ( 0, 1, ((.I[98] * 0.1) + .I[99]*1) ) ;\n"
		+ ".O[91] = clamp ( 0, 1, ((.I[100] * 0.1) + .I[97]*-1) ) ;\n.O[92] = clamp ( 0, 1, ((.I[101] * 0.1) + .I[97]*-1) ) ;\n.O[93] = clamp ( 0, 1, ((.I[102] + .I[63] * 0.1) + .I[97]*-1) ) ;\n.O[94] = clamp ( 0, 1, ((.I[103] + .I[63] * 0.1) + .I[97]*-1) ) ;\n\n// EYES - BODY\n\n.O[95] = clamp ( 0, 1, ((.I[104]* 0.1) * (.I[105] * 0.1)) ) ;\n.O[96] = clamp ( 0, 1, ((.I[106]* 0.1) * (.I[107] * 0.1)) ) ;\n.O[97] = clamp ( 0, 1, ((.I[108]* 0.1) * ( 1 - (.I[105] * 0.1))) ) ;\n.O[98] = clamp ( 0, 1, ((.I[109]* 0.1) * ( 1 - (.I[107] * 0.1))) ) ;\n.O[99] = clamp ( 0, 1, (.I[110]+.I[75])* 0.1 ) ;\n.O[100] = clamp ( 0, 1, (.I[111]+.I[75])* 0.1 ) ;\n.O[101] = clamp ( 0, 1, (.I[112]+.I[51])* 0.1 ) ;\n.O[102] = clamp ( 0, 1, (.I[113]+.I[51])* 0.1 ) ;\n.O[103] = clamp ( 0, 1, (.I[114]+.I[56]+.I[59])* 0.1 ) ;\n.O[104] = clamp ( 0, 1, (.I[115]+.I[56]+.I[59])* 0.1 ) ;\n.O[105] = clamp ( 0, 1, (.I[116]+.I[63] +.I[61])* 0.1 ) ;\n.O[106] = clamp ( 0, 1, (.I[117]+.I[63] +.I[61])* 0.1 ) ;\n.O[107] = clamp ( 0, 1, .I[118] * 0.1 ) ;\n.O[108] = clamp ( 0, 1, .I[119] * 0.1 ) ;\n"
		+ ".O[109] = clamp ( 0, 1, .I[120] * 0.1 ) ;\n.O[110] = clamp ( 0, 1, .I[121] * 0.1 ) ;\n.O[111] = clamp ( 0, 1, .I[122] * 0.1 ) ;\n.O[112] = clamp ( 0, 1, .I[123] * 0.1 ) ;\n\n\n\n// EYES - EYELASHES\n\n.O[113] = clamp ( 0, 1, ((.I[104] * 0.1) * (.I[105] * 0.1)) ) ;\n.O[114] = clamp ( 0, 1, ((.I[106] * 0.1) * (.I[107] * 0.1)) ) ;\n.O[115] = clamp ( 0, 1, (.I[110]+.I[75] )* 0.1 ) ;\n.O[116] = clamp ( 0, 1, (.I[111]+.I[75] )* 0.1 ) ;\n.O[117] = clamp ( 0, 1, (.I[112]+.I[51]) * 0.1) ;\n.O[118] = clamp ( 0, 1, (.I[113]+.I[51]) * 0.1) ;\n.O[119] = clamp ( 0, 1, (.I[114]+.I[56]+.I[59]) * 0.1) ;\n.O[120] = clamp ( 0, 1, (.I[115]+.I[56]+.I[59]) * 0.1) ;\n.O[121] = clamp ( 0, 1, (.I[116]+.I[63] +.I[61]) * 0.1) ;\n.O[122] = clamp ( 0, 1, (.I[117]+.I[63] +.I[61]) * 0.1) ;\n\n// EYES - EYELASHES\n\n.O[123] = clamp ( 0, 1, ((.I[108]* 0.1) * ( 1 - (.I[105] * 0.1))) ) ;\n.O[124] = clamp ( 0, 1, ((.I[109]* 0.1) * ( 1 - (.I[107] * 0.1))) ) ;\n.O[125] = clamp ( 0, 1, (.I[110]+.I[75] )* 0.1 ) ;\n.O[126] = clamp ( 0, 1, (.I[111]+.I[75] )* 0.1 ) ;\n.O[127] = clamp ( 0, 1, (.I[120]) * 0.1) ;\n"
		+ ".O[128] = clamp ( 0, 1, (.I[121]) * 0.1) ;\n.O[129] = clamp ( 0, 1, (.I[118]) * 0.1) ;\n.O[130] = clamp ( 0, 1, (.I[119]) * 0.1) ;\n.O[131] = clamp ( 0, 1, (.I[116]+.I[63] +.I[61]) * 0.1) ;\n.O[132] = clamp ( 0, 1, (.I[117]+.I[63] +.I[61]) * 0.1) ;\n\n// BROWS - BROWS\n\n.O[133] = clamp ( -1, 1, ((.I[76]+.I[48] + .I[65] + .I[68] + .I[75])*0.1 - .I[77]*-1) ) ;\n.O[134] = clamp ( -1, 1, ((.I[78]+.I[48] + .I[65] + .I[68] + .I[75])*0.1 - .I[79]*-1) ) ;\n.O[135] = clamp ( 0, 1, ((.I[80]+.I[51])*0.1 + .I[81]*-1)) ;\n.O[136] = clamp ( 0, 1, ((.I[82]+.I[51])*0.1 + .I[83]*1)) ;\n.O[137] = clamp ( 0, 1, (.I[84]+.I[56])*0.1+.I[85]*-1) ;\n.O[138] = clamp ( 0, 1, (.I[86]+.I[56])*0.1+.I[87]*-1) ;\n.O[139] = clamp ( 0, 1, (.I[88]+.I[59] * 0.1 ) ) ;\n.O[140] = clamp ( 0, 1, (.I[89]+.I[59] * 0.1 ) ) ;\n.O[141] = clamp ( 0, 1, (.I[90] + .I[61])*0.1+.I[85]*-1) ;\n.O[142] = clamp ( 0, 1, (.I[91] + .I[61])*0.1+.I[87]*-1) ;\n.O[143] = clamp ( 0, 1, ((.I[92] * 0.1) + .I[77]*-1)) ;\n.O[144] = clamp ( 0, 1, ((.I[93] * 0.1) + .I[79]*-1)) ;\n.O[145] = clamp ( 0, 1, (.I[94] * 0.1) ) ;\n"
		+ ".O[146] = clamp ( 0, 1, (.I[95] * 0.1) ) ;\n.O[147] = clamp ( 0, 1, ((.I[96] * 0.1) + .I[97]*-1) ) ;\n.O[148] = clamp ( 0, 1, ((.I[98] * 0.1) + .I[99]*1) ) ;\n.O[149] = clamp ( 0, 1, ((.I[100] * 0.1) + .I[97]*-1) ) ;\n.O[150] = clamp ( 0, 1, ((.I[101] * 0.1) + .I[97]*-1) ) ;\n.O[151] = clamp ( 0, 1, ((.I[102] + .I[63] * 0.1) + .I[97]*-1) ) ;\n.O[152] = clamp ( 0, 1, ((.I[103] + .I[63] * 0.1) + .I[97]*-1) ) ;\n\n\n\n// PHONEME - TONGUE\n\n.O[153] = clamp ( 0, 1, (.I[26] * 0.1)) ;\n.O[154] = clamp ( 0, 1, (.I[27] * 0.1) ) ;\n.O[155] = clamp ( 0, 1, (.I[28] * 0.1) ) ;\n.O[156] = clamp ( 0, 1, (.I[29] * 0.1) ) ;\n.O[157] = clamp ( 0, 1, (.I[30] * 0.1) ) ;\n.O[158] = clamp ( 0, 1, (.I[31] * 0.1) ) ;\n.O[159] = clamp ( 0, 1, (.I[33] * 0.1) ) ;\n.O[160] = clamp ( 0, 1, (.I[34] * 0.1) ) ;\n.O[161] = clamp ( 0, 1, (.I[35] * 0.1) ) ;\n.O[162] = clamp ( 0, 1, (.I[36] * 0.1) ) ;\n.O[163] = clamp ( 0, 1, (.I[37] * 0.1) ) ;\n.O[164] = clamp ( 0, 1, (.I[39] * 0.1) ) ;\n.O[165] = clamp ( 0, 1, (.I[38] * 0.1) ) ;\n\n\n// MOUTH - TONGUE\n\n.O[166] = clamp ( 0, 1, (.I[58]+.I[59]) * 0.1) ;\n"
		+ ".O[167] = clamp ( 0, 1, (.I[60] + .I[61] ) * 0.1) ;\n.O[168] = clamp ( 0, 1, (.I[62]+.I[63]) * 0.1) ;\n.O[169] = clamp ( -1, 1, (.I[74] + .I[75] * 0.1) ) ;\n.O[170] = clamp ( -1, 1, (.I[50]+.I[51]) * 0.1) ;\n.O[171] = clamp ( -1, 1, (.I[52]) * 0.1) ;\n.O[172] = clamp ( -1, 1, (.I[53]) * 0.1) ;\n\n\n\n// PHONEME - LOWERTEETH\n\n.O[173] = clamp ( 0, 1, (.I[26] * 0.1)) ;\n.O[174] = clamp ( 0, 1, (.I[27] * 0.1)) ;\n.O[175] = clamp ( 0, 1, (.I[28] * 0.1)) ;\n.O[176] = clamp ( 0, 1, (.I[29] * 0.1)) ;\n.O[177] = clamp ( 0, 1, (.I[30] * 0.1)) ;\n.O[178] = clamp ( 0, 1, (.I[31] * 0.1)) ;\n.O[179] = clamp ( 0, 1, (.I[33] * 0.1)) ;\n.O[180] = clamp ( 0, 1, (.I[34] * 0.1)) ;\n.O[181] = clamp ( 0, 1, (.I[35] * 0.1)) ;\n.O[182] = clamp ( 0, 1, (.I[36] * 0.1)) ;\n.O[183] = clamp ( 0, 1, (.I[37] * 0.1)) ;\n.O[184] = clamp ( 0, 1, (.I[39] * 0.1)) ;\n.O[185] = clamp ( 0, 1, (.I[38] * 0.1)) ;\n\n\n// MOUTH - LOWERTEETH\n\n.O[186] = clamp ( 0, 1, (.I[58] + .I[59])* 0.1 ) ;\n.O[187] = clamp ( 0, 1, (.I[60] + .I[61])* 0.1 ) ;\n.O[188] = clamp ( 0, 1, (.I[62]+.I[63])* 0.1 ) ;\n"
		+ ".O[189] = clamp ( 0, 1, (.I[67]+.I[68])* 0.1 ) ;\n.O[190] = clamp ( 0, 1, .I[74] + .I[75] * 0.1);\n.O[191] = clamp ( -1, 1, (.I[50]+.I[51]) * 0.1) ;\n.O[189] = clamp ( -1, 1, (.I[67]) * 0.1) ;\n.O[192] = clamp ( -1, 1, (.I[52]) * 0.1) ;\n.O[193] = clamp ( -1, 1, (.I[53]) * 0.1) ;\n\n\n// IRIS L\n\n.O[194] = clamp ( 0, 1, (.I[124] * -1)* 0.1) ;\n.O[195] = clamp ( 0, 1, .I[124] * 0.1) ;\n.O[196] = clamp ( 0, 1, .I[125] * 0.1) ;\n.O[197] = clamp ( 0, 1, (.I[125] * -1)* 0.1) ;\n.O[198] = clamp ( 0, 1, (.I[116]+.I[63] +.I[61])* 0.1 ) ;\n\n// IRIS R\n\n.O[199] = clamp ( 0, 1, (.I[126] * -1)* 0.1) ;\n.O[200] = clamp ( 0, 1, .I[126]* 0.1) ;\n.O[201] = clamp ( 0, 1, .I[127]* 0.1) ;\n.O[202] = clamp ( 0, 1, (.I[127] * -1)* 0.1) ;\n.O[203] = clamp ( 0, 1, (.I[117]+.I[63] +.I[61])* 0.1 ) ;\n\n// SPECULAR L\n\n.O[204] = clamp ( 0, 1, .I[125]* 0.1) ;\n//BS_PupilLBS.PUPIL_L_irisClose = clamp ( 0, 1, (eye_blend_Ctrl.LT_IRIS_OPEN_CLOSE * -1)* 0.1) ;\n.O[205] = clamp ( 0, 1, .I[124]* 0.1) ;\n//BS_PupilLBS.PUPIL_L_pupilClose = clamp ( 0, 1, (eye_blend_Ctrl.LT_PUPIL_OPEN_CLOSE * -1)* 0.1) ;\n"
		+ ".O[206] = clamp ( 0, 1, (.I[117]+.I[63] +.I[61])* 0.1 ) ;\n.O[207] = clamp ( 0, 1, .I[125]* 0.1) ;\n.O[208] = clamp ( 0, 1, (.I[125] * -1)* 0.1) ;\n.O[209] = clamp ( 0, 1, .I[124]* 0.1) ;\n.O[210] = clamp ( 0, 1, (.I[116]+.I[63] +.I[61])* 0.1 ) ;\n.O[211] = clamp ( 0, 1, (.I[125] * -1)* 0.1) ;\n\n// SPECULAR R\n\n.O[212] = clamp ( 0, 1, .I[127]* 0.1) ;\n//BS_PupilRBS.PUPIL_R_irisClose = clamp ( 0, 1, (eye_blend_Ctrl.RT_IRIS_OPEN_CLOSE * -1)* 0.1) ;\n.O[213] = clamp ( 0, 1, .I[126]* 0.1) ;\n//BS_PupilRBS.PUPIL_R_pupilClose = clamp ( 0, 1, (eye_blend_Ctrl.RT_PUPIL_OPEN_CLOSE * -1)* 0.1) ;\n.O[214] = clamp ( 0, 1, (.I[117]+.I[63] +.I[61])* 0.1 ) ;\n.O[215] = clamp ( 0, 1, .I[127]* 0.1) ;\n.O[216] = clamp ( 0, 1, (.I[127] * -1)* 0.1) ;\n.O[217] = clamp ( 0, 1, .I[126]* 0.1) ;\n.O[218] = clamp ( 0, 1, (.I[117]+.I[63] +.I[61])* 0.1 ) ;\n.O[219] = clamp ( 0, 1, (.I[127] * -1)* 0.1) ;\n\n// BULB L\n\n.O[220] = clamp ( 0, 1, (.I[116]+.I[63] +.I[61])* 0.1 ) ;\n\n// BULB R\n\n.O[221] = clamp ( 0, 1, (.I[117]+.I[63] +.I[61])* 0.1 ) ;");
createNode unitConversion -n "unitConversion382";
	rename -uid "39F1403D-479C-E5CF-6C8B-94926D3CADCB";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion378";
	rename -uid "F8AA2645-4F82-738E-3155-E2931649719C";
	setAttr ".cf" 0.017453292519943295;
createNode controller -n "C_upper_teeth_Ctrl_tag";
	rename -uid "79656635-45DF-5FAB-9DDB-CE9DBB1EB220";
createNode controller -n "L_upper_teeth_Ctrl_tag";
	rename -uid "2547131A-4B03-459C-6CDF-0DBA9D010277";
createNode controller -n "R_upper_teeth_Ctrl_tag";
	rename -uid "4F0D7476-4C91-61AD-B380-AAB618685323";
createNode unitConversion -n "unitConversion391";
	rename -uid "954B0F8C-4E49-F345-0C7B-A7BF389C837D";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion392";
	rename -uid "0D4CDDDD-4530-E1B8-6D91-B984228DCEA2";
	setAttr ".cf" 0.017453292519943295;
createNode controller -n "tongue_01_Ctrl_tag";
	rename -uid "B2AD8687-4F04-22B4-0C68-CF89D26A0FC4";
createNode unitConversion -n "unitConversion393";
	rename -uid "64A9FDF8-41C7-E335-2C68-A88EA7BA42E1";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion394";
	rename -uid "94D783C3-45EC-B2BA-703F-60BAF48E5F12";
	setAttr ".cf" 0.017453292519943295;
createNode controller -n "tongue_02_Ctrl_tag";
	rename -uid "DAF1DB19-4F22-F3A2-E8FA-05A00256BAE3";
createNode unitConversion -n "unitConversion396";
	rename -uid "A7F03A3F-408C-D913-CCA3-458E3FEF84D8";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion395";
	rename -uid "EF627C5A-4300-4A61-BDB4-B5ABE598DA86";
	setAttr ".cf" 0.017453292519943295;
createNode controller -n "tongue_03_Ctrl_tag";
	rename -uid "3E4B647C-4F6B-5A0F-059F-D99EE698A9F9";
createNode unitConversion -n "unitConversion397";
	rename -uid "4444DE08-429A-8245-999F-B28C5694BAA3";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion398";
	rename -uid "94A19470-44DB-992A-5A3C-9EB178B74E8C";
	setAttr ".cf" 0.017453292519943295;
createNode controller -n "tongue_04_Ctrl_tag";
	rename -uid "7687E164-4905-179F-D968-8CA9927159E1";
createNode unitConversion -n "unitConversion400";
	rename -uid "013BE8E6-4917-0B14-0AE8-53BAB551F30A";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion399";
	rename -uid "F9216924-4B55-0567-E4FB-B490B5E85B3A";
	setAttr ".cf" 0.017453292519943295;
createNode controller -n "tongue_05_Ctrl_tag";
	rename -uid "9BACD1FD-4C49-79C8-1C7C-198DEAE2365A";
createNode unitConversion -n "unitConversion386";
	rename -uid "0AD7D9CB-4761-65B4-64C8-B58B3C7CE01F";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion384";
	rename -uid "D05E3113-4377-AA7C-6466-C9A70B4BFEC5";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion388";
	rename -uid "07570C25-470C-5A3E-E537-E9B9107BD6DA";
	setAttr ".cf" 0.017453292519943295;
createNode controller -n "low_teeth_Ctrl_tag";
	rename -uid "78C45544-449E-A9D7-90F7-5382494A121A";
createNode controller -n "C_lower_teeth_Ctrl_tag";
	rename -uid "F7ACBC21-4DF8-AC3F-7A34-8383A23940DD";
createNode controller -n "L_lower_teeth_Ctrl_tag";
	rename -uid "7B5C255C-4614-C33C-1B7E-C2A619024A81";
createNode controller -n "R_lower_teeth_Ctrl_tag";
	rename -uid "59E9A124-4392-E363-8EDE-2FA4708A58A2";
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".msaa" yes;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".fs" 1;
	setAttr ".ef" 10;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "Face_Expr.out[0]" "up_teeth_Ctrl_Grp_offset.tx";
connectAttr "Face_Expr.out[1]" "up_teeth_Ctrl_Grp_offset.ty";
connectAttr "Face_Expr.out[2]" "up_teeth_Ctrl_Grp_offset.tz";
connectAttr "unitConversion378.o" "up_teeth_Ctrl_Grp_offset.rx";
connectAttr "unitConversion380.o" "up_teeth_Ctrl_Grp_offset.ry";
connectAttr "unitConversion382.o" "up_teeth_Ctrl_Grp_offset.rz";
connectAttr "Face_Expr.out[6]" "up_teeth_Ctrl_Grp_offset.sx";
connectAttr "Face_Expr.out[7]" "up_teeth_Ctrl_Grp_offset.sy";
connectAttr "Face_Expr.out[8]" "up_teeth_Ctrl_Grp_offset.sz";
connectAttr "Face_Expr.out[9]" "low_teeth_Ctrl_Grp_offset.tx";
connectAttr "Face_Expr.out[10]" "low_teeth_Ctrl_Grp_offset.ty";
connectAttr "Face_Expr.out[11]" "low_teeth_Ctrl_Grp_offset.tz";
connectAttr "unitConversion384.o" "low_teeth_Ctrl_Grp_offset.rx";
connectAttr "unitConversion386.o" "low_teeth_Ctrl_Grp_offset.ry";
connectAttr "unitConversion388.o" "low_teeth_Ctrl_Grp_offset.rz";
connectAttr "Face_Expr.out[15]" "low_teeth_Ctrl_Grp_offset.sx";
connectAttr "Face_Expr.out[16]" "low_teeth_Ctrl_Grp_offset.sy";
connectAttr "Face_Expr.out[17]" "low_teeth_Ctrl_Grp_offset.sz";
connectAttr "low_teeth_CtrlShapeOrig.ws" "low_teeth_CtrlShape.cr";
connectAttr "L_upper_teeth_jnt_parentConstraint1.ctx" "L_upper_teeth_jnt.tx";
connectAttr "L_upper_teeth_jnt_parentConstraint1.cty" "L_upper_teeth_jnt.ty";
connectAttr "L_upper_teeth_jnt_parentConstraint1.ctz" "L_upper_teeth_jnt.tz";
connectAttr "L_upper_teeth_jnt_parentConstraint1.crx" "L_upper_teeth_jnt.rx";
connectAttr "L_upper_teeth_jnt_parentConstraint1.cry" "L_upper_teeth_jnt.ry";
connectAttr "L_upper_teeth_jnt_parentConstraint1.crz" "L_upper_teeth_jnt.rz";
connectAttr "L_upper_teeth_jnt_scaleConstraint1.csx" "L_upper_teeth_jnt.sx";
connectAttr "L_upper_teeth_jnt_scaleConstraint1.csy" "L_upper_teeth_jnt.sy";
connectAttr "L_upper_teeth_jnt_scaleConstraint1.csz" "L_upper_teeth_jnt.sz";
connectAttr "L_upper_teeth_jnt.ro" "L_upper_teeth_jnt_parentConstraint1.cro";
connectAttr "L_upper_teeth_jnt.pim" "L_upper_teeth_jnt_parentConstraint1.cpim";
connectAttr "L_upper_teeth_jnt.rp" "L_upper_teeth_jnt_parentConstraint1.crp";
connectAttr "L_upper_teeth_jnt.rpt" "L_upper_teeth_jnt_parentConstraint1.crt";
connectAttr "L_upper_teeth_jnt.jo" "L_upper_teeth_jnt_parentConstraint1.cjo";
connectAttr "L_upper_teeth_Ctrl.t" "L_upper_teeth_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "L_upper_teeth_Ctrl.rp" "L_upper_teeth_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "L_upper_teeth_Ctrl.rpt" "L_upper_teeth_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "L_upper_teeth_Ctrl.r" "L_upper_teeth_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "L_upper_teeth_Ctrl.ro" "L_upper_teeth_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "L_upper_teeth_Ctrl.s" "L_upper_teeth_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "L_upper_teeth_Ctrl.pm" "L_upper_teeth_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "L_upper_teeth_jnt_parentConstraint1.w0" "L_upper_teeth_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "L_upper_teeth_jnt.pim" "L_upper_teeth_jnt_scaleConstraint1.cpim";
connectAttr "L_upper_teeth_Ctrl.s" "L_upper_teeth_jnt_scaleConstraint1.tg[0].ts"
		;
connectAttr "L_upper_teeth_Ctrl.pm" "L_upper_teeth_jnt_scaleConstraint1.tg[0].tpm"
		;
connectAttr "L_upper_teeth_jnt_scaleConstraint1.w0" "L_upper_teeth_jnt_scaleConstraint1.tg[0].tw"
		;
connectAttr "C_upper_teeth_jnt_parentConstraint1.ctx" "C_upper_teeth_jnt.tx";
connectAttr "C_upper_teeth_jnt_parentConstraint1.cty" "C_upper_teeth_jnt.ty";
connectAttr "C_upper_teeth_jnt_parentConstraint1.ctz" "C_upper_teeth_jnt.tz";
connectAttr "C_upper_teeth_jnt_parentConstraint1.crx" "C_upper_teeth_jnt.rx";
connectAttr "C_upper_teeth_jnt_parentConstraint1.cry" "C_upper_teeth_jnt.ry";
connectAttr "C_upper_teeth_jnt_parentConstraint1.crz" "C_upper_teeth_jnt.rz";
connectAttr "C_upper_teeth_jnt_scaleConstraint1.csx" "C_upper_teeth_jnt.sx";
connectAttr "C_upper_teeth_jnt_scaleConstraint1.csy" "C_upper_teeth_jnt.sy";
connectAttr "C_upper_teeth_jnt_scaleConstraint1.csz" "C_upper_teeth_jnt.sz";
connectAttr "C_upper_teeth_jnt.ro" "C_upper_teeth_jnt_parentConstraint1.cro";
connectAttr "C_upper_teeth_jnt.pim" "C_upper_teeth_jnt_parentConstraint1.cpim";
connectAttr "C_upper_teeth_jnt.rp" "C_upper_teeth_jnt_parentConstraint1.crp";
connectAttr "C_upper_teeth_jnt.rpt" "C_upper_teeth_jnt_parentConstraint1.crt";
connectAttr "C_upper_teeth_jnt.jo" "C_upper_teeth_jnt_parentConstraint1.cjo";
connectAttr "C_upper_teeth_Ctrl.t" "C_upper_teeth_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "C_upper_teeth_Ctrl.rp" "C_upper_teeth_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "C_upper_teeth_Ctrl.rpt" "C_upper_teeth_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "C_upper_teeth_Ctrl.r" "C_upper_teeth_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "C_upper_teeth_Ctrl.ro" "C_upper_teeth_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "C_upper_teeth_Ctrl.s" "C_upper_teeth_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "C_upper_teeth_Ctrl.pm" "C_upper_teeth_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "C_upper_teeth_jnt_parentConstraint1.w0" "C_upper_teeth_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "C_upper_teeth_jnt.pim" "C_upper_teeth_jnt_scaleConstraint1.cpim";
connectAttr "C_upper_teeth_Ctrl.s" "C_upper_teeth_jnt_scaleConstraint1.tg[0].ts"
		;
connectAttr "C_upper_teeth_Ctrl.pm" "C_upper_teeth_jnt_scaleConstraint1.tg[0].tpm"
		;
connectAttr "C_upper_teeth_jnt_scaleConstraint1.w0" "C_upper_teeth_jnt_scaleConstraint1.tg[0].tw"
		;
connectAttr "R_upper_teeth_jnt_parentConstraint1.ctx" "R_upper_teeth_jnt.tx";
connectAttr "R_upper_teeth_jnt_parentConstraint1.cty" "R_upper_teeth_jnt.ty";
connectAttr "R_upper_teeth_jnt_parentConstraint1.ctz" "R_upper_teeth_jnt.tz";
connectAttr "R_upper_teeth_jnt_parentConstraint1.crx" "R_upper_teeth_jnt.rx";
connectAttr "R_upper_teeth_jnt_parentConstraint1.cry" "R_upper_teeth_jnt.ry";
connectAttr "R_upper_teeth_jnt_parentConstraint1.crz" "R_upper_teeth_jnt.rz";
connectAttr "R_upper_teeth_jnt_scaleConstraint1.csx" "R_upper_teeth_jnt.sx";
connectAttr "R_upper_teeth_jnt_scaleConstraint1.csy" "R_upper_teeth_jnt.sy";
connectAttr "R_upper_teeth_jnt_scaleConstraint1.csz" "R_upper_teeth_jnt.sz";
connectAttr "R_upper_teeth_jnt.ro" "R_upper_teeth_jnt_parentConstraint1.cro";
connectAttr "R_upper_teeth_jnt.pim" "R_upper_teeth_jnt_parentConstraint1.cpim";
connectAttr "R_upper_teeth_jnt.rp" "R_upper_teeth_jnt_parentConstraint1.crp";
connectAttr "R_upper_teeth_jnt.rpt" "R_upper_teeth_jnt_parentConstraint1.crt";
connectAttr "R_upper_teeth_jnt.jo" "R_upper_teeth_jnt_parentConstraint1.cjo";
connectAttr "R_upper_teeth_Ctrl.t" "R_upper_teeth_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "R_upper_teeth_Ctrl.rp" "R_upper_teeth_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "R_upper_teeth_Ctrl.rpt" "R_upper_teeth_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "R_upper_teeth_Ctrl.r" "R_upper_teeth_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "R_upper_teeth_Ctrl.ro" "R_upper_teeth_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "R_upper_teeth_Ctrl.s" "R_upper_teeth_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "R_upper_teeth_Ctrl.pm" "R_upper_teeth_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "R_upper_teeth_jnt_parentConstraint1.w0" "R_upper_teeth_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "R_upper_teeth_jnt.pim" "R_upper_teeth_jnt_scaleConstraint1.cpim";
connectAttr "R_upper_teeth_Ctrl.s" "R_upper_teeth_jnt_scaleConstraint1.tg[0].ts"
		;
connectAttr "R_upper_teeth_Ctrl.pm" "R_upper_teeth_jnt_scaleConstraint1.tg[0].tpm"
		;
connectAttr "R_upper_teeth_jnt_scaleConstraint1.w0" "R_upper_teeth_jnt_scaleConstraint1.tg[0].tw"
		;
connectAttr "L_lower_teeth_jnt_parentConstraint1.ctx" "L_lower_teeth_jnt.tx";
connectAttr "L_lower_teeth_jnt_parentConstraint1.cty" "L_lower_teeth_jnt.ty";
connectAttr "L_lower_teeth_jnt_parentConstraint1.ctz" "L_lower_teeth_jnt.tz";
connectAttr "L_lower_teeth_jnt_parentConstraint1.crx" "L_lower_teeth_jnt.rx";
connectAttr "L_lower_teeth_jnt_parentConstraint1.cry" "L_lower_teeth_jnt.ry";
connectAttr "L_lower_teeth_jnt_parentConstraint1.crz" "L_lower_teeth_jnt.rz";
connectAttr "L_lower_teeth_jnt_scaleConstraint1.csx" "L_lower_teeth_jnt.sx";
connectAttr "L_lower_teeth_jnt_scaleConstraint1.csy" "L_lower_teeth_jnt.sy";
connectAttr "L_lower_teeth_jnt_scaleConstraint1.csz" "L_lower_teeth_jnt.sz";
connectAttr "L_lower_teeth_jnt.ro" "L_lower_teeth_jnt_parentConstraint1.cro";
connectAttr "L_lower_teeth_jnt.pim" "L_lower_teeth_jnt_parentConstraint1.cpim";
connectAttr "L_lower_teeth_jnt.rp" "L_lower_teeth_jnt_parentConstraint1.crp";
connectAttr "L_lower_teeth_jnt.rpt" "L_lower_teeth_jnt_parentConstraint1.crt";
connectAttr "L_lower_teeth_jnt.jo" "L_lower_teeth_jnt_parentConstraint1.cjo";
connectAttr "L_lower_teeth_Ctrl.t" "L_lower_teeth_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "L_lower_teeth_Ctrl.rp" "L_lower_teeth_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "L_lower_teeth_Ctrl.rpt" "L_lower_teeth_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "L_lower_teeth_Ctrl.r" "L_lower_teeth_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "L_lower_teeth_Ctrl.ro" "L_lower_teeth_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "L_lower_teeth_Ctrl.s" "L_lower_teeth_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "L_lower_teeth_Ctrl.pm" "L_lower_teeth_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "L_lower_teeth_jnt_parentConstraint1.w0" "L_lower_teeth_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "L_lower_teeth_jnt.pim" "L_lower_teeth_jnt_scaleConstraint1.cpim";
connectAttr "L_lower_teeth_Ctrl.s" "L_lower_teeth_jnt_scaleConstraint1.tg[0].ts"
		;
connectAttr "L_lower_teeth_Ctrl.pm" "L_lower_teeth_jnt_scaleConstraint1.tg[0].tpm"
		;
connectAttr "L_lower_teeth_jnt_scaleConstraint1.w0" "L_lower_teeth_jnt_scaleConstraint1.tg[0].tw"
		;
connectAttr "C_lower_teeth_jnt_parentConstraint1.ctx" "C_lower_teeth_jnt.tx";
connectAttr "C_lower_teeth_jnt_parentConstraint1.cty" "C_lower_teeth_jnt.ty";
connectAttr "C_lower_teeth_jnt_parentConstraint1.ctz" "C_lower_teeth_jnt.tz";
connectAttr "C_lower_teeth_jnt_parentConstraint1.crx" "C_lower_teeth_jnt.rx";
connectAttr "C_lower_teeth_jnt_parentConstraint1.cry" "C_lower_teeth_jnt.ry";
connectAttr "C_lower_teeth_jnt_parentConstraint1.crz" "C_lower_teeth_jnt.rz";
connectAttr "C_lower_teeth_jnt_scaleConstraint1.csx" "C_lower_teeth_jnt.sx";
connectAttr "C_lower_teeth_jnt_scaleConstraint1.csy" "C_lower_teeth_jnt.sy";
connectAttr "C_lower_teeth_jnt_scaleConstraint1.csz" "C_lower_teeth_jnt.sz";
connectAttr "C_lower_teeth_jnt.ro" "C_lower_teeth_jnt_parentConstraint1.cro";
connectAttr "C_lower_teeth_jnt.pim" "C_lower_teeth_jnt_parentConstraint1.cpim";
connectAttr "C_lower_teeth_jnt.rp" "C_lower_teeth_jnt_parentConstraint1.crp";
connectAttr "C_lower_teeth_jnt.rpt" "C_lower_teeth_jnt_parentConstraint1.crt";
connectAttr "C_lower_teeth_jnt.jo" "C_lower_teeth_jnt_parentConstraint1.cjo";
connectAttr "C_lower_teeth_Ctrl.t" "C_lower_teeth_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "C_lower_teeth_Ctrl.rp" "C_lower_teeth_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "C_lower_teeth_Ctrl.rpt" "C_lower_teeth_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "C_lower_teeth_Ctrl.r" "C_lower_teeth_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "C_lower_teeth_Ctrl.ro" "C_lower_teeth_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "C_lower_teeth_Ctrl.s" "C_lower_teeth_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "C_lower_teeth_Ctrl.pm" "C_lower_teeth_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "C_lower_teeth_jnt_parentConstraint1.w0" "C_lower_teeth_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "C_lower_teeth_jnt.pim" "C_lower_teeth_jnt_scaleConstraint1.cpim";
connectAttr "C_lower_teeth_Ctrl.s" "C_lower_teeth_jnt_scaleConstraint1.tg[0].ts"
		;
connectAttr "C_lower_teeth_Ctrl.pm" "C_lower_teeth_jnt_scaleConstraint1.tg[0].tpm"
		;
connectAttr "C_lower_teeth_jnt_scaleConstraint1.w0" "C_lower_teeth_jnt_scaleConstraint1.tg[0].tw"
		;
connectAttr "R_lower_teeth_jnt_parentConstraint1.ctx" "R_lower_teeth_jnt.tx";
connectAttr "R_lower_teeth_jnt_parentConstraint1.cty" "R_lower_teeth_jnt.ty";
connectAttr "R_lower_teeth_jnt_parentConstraint1.ctz" "R_lower_teeth_jnt.tz";
connectAttr "R_lower_teeth_jnt_parentConstraint1.crx" "R_lower_teeth_jnt.rx";
connectAttr "R_lower_teeth_jnt_parentConstraint1.cry" "R_lower_teeth_jnt.ry";
connectAttr "R_lower_teeth_jnt_parentConstraint1.crz" "R_lower_teeth_jnt.rz";
connectAttr "R_lower_teeth_jnt_scaleConstraint1.csx" "R_lower_teeth_jnt.sx";
connectAttr "R_lower_teeth_jnt_scaleConstraint1.csy" "R_lower_teeth_jnt.sy";
connectAttr "R_lower_teeth_jnt_scaleConstraint1.csz" "R_lower_teeth_jnt.sz";
connectAttr "R_lower_teeth_jnt.ro" "R_lower_teeth_jnt_parentConstraint1.cro";
connectAttr "R_lower_teeth_jnt.pim" "R_lower_teeth_jnt_parentConstraint1.cpim";
connectAttr "R_lower_teeth_jnt.rp" "R_lower_teeth_jnt_parentConstraint1.crp";
connectAttr "R_lower_teeth_jnt.rpt" "R_lower_teeth_jnt_parentConstraint1.crt";
connectAttr "R_lower_teeth_jnt.jo" "R_lower_teeth_jnt_parentConstraint1.cjo";
connectAttr "R_lower_teeth_Ctrl.t" "R_lower_teeth_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "R_lower_teeth_Ctrl.rp" "R_lower_teeth_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "R_lower_teeth_Ctrl.rpt" "R_lower_teeth_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "R_lower_teeth_Ctrl.r" "R_lower_teeth_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "R_lower_teeth_Ctrl.ro" "R_lower_teeth_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "R_lower_teeth_Ctrl.s" "R_lower_teeth_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "R_lower_teeth_Ctrl.pm" "R_lower_teeth_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "R_lower_teeth_jnt_parentConstraint1.w0" "R_lower_teeth_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "R_lower_teeth_jnt.pim" "R_lower_teeth_jnt_scaleConstraint1.cpim";
connectAttr "R_lower_teeth_Ctrl.s" "R_lower_teeth_jnt_scaleConstraint1.tg[0].ts"
		;
connectAttr "R_lower_teeth_Ctrl.pm" "R_lower_teeth_jnt_scaleConstraint1.tg[0].tpm"
		;
connectAttr "R_lower_teeth_jnt_scaleConstraint1.w0" "R_lower_teeth_jnt_scaleConstraint1.tg[0].tw"
		;
connectAttr "Face_Expr.out[33]" "tongue_01_Ctrl_Grp_translate_offset.ty";
connectAttr "unitConversion391.o" "tongue_01_Ctrl_Grp_offset.rz";
connectAttr "unitConversion392.o" "tongue_01_Ctrl_Grp_offset.ry";
connectAttr "unitConversion393.o" "tongue_02_Ctrl_Grp_offset.rz";
connectAttr "unitConversion394.o" "tongue_02_Ctrl_Grp_offset.ry";
connectAttr "unitConversion395.o" "tongue_03_Ctrl_Grp_offset.rz";
connectAttr "unitConversion396.o" "tongue_03_Ctrl_Grp_offset.ry";
connectAttr "unitConversion397.o" "tongue_04_Ctrl_Grp_offset.rz";
connectAttr "unitConversion398.o" "tongue_04_Ctrl_Grp_offset.ry";
connectAttr "unitConversion399.o" "tongue_05_Ctrl_Grp_offset.rz";
connectAttr "unitConversion400.o" "tongue_05_Ctrl_Grp_offset.ry";
connectAttr "tongue_01_jnt.s" "tongue_02_jnt.is";
connectAttr "tongue_02_jnt.s" "tongue_03_jnt.is";
connectAttr "tongue_03_jnt.s" "tongue_04_jnt.is";
connectAttr "tongue_04_jnt.s" "tongue_05_jnt.is";
connectAttr "tongue_05_jnt.s" "tongue_06_jnt.is";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":hyperGraphLayout.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":defaultObjectSet.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":defaultHardwareRenderGlobals.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":defaultHardwareRenderGlobals.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "Face_Expr.out[4]" "unitConversion380.i";
connectAttr "Face_Expr.out[5]" "unitConversion382.i";
connectAttr "Face_Expr.out[3]" "unitConversion378.i";
connectAttr "C_upper_teeth_Ctrl.msg" "C_upper_teeth_Ctrl_tag.act";
connectAttr "L_upper_teeth_Ctrl.msg" "L_upper_teeth_Ctrl_tag.act";
connectAttr "R_upper_teeth_Ctrl.msg" "R_upper_teeth_Ctrl_tag.act";
connectAttr "Face_Expr.out[23]" "unitConversion391.i";
connectAttr "Face_Expr.out[24]" "unitConversion392.i";
connectAttr "tongue_01_Ctrl.msg" "tongue_01_Ctrl_tag.act";
connectAttr "Face_Expr.out[25]" "unitConversion393.i";
connectAttr "Face_Expr.out[26]" "unitConversion394.i";
connectAttr "tongue_02_Ctrl.msg" "tongue_02_Ctrl_tag.act";
connectAttr "Face_Expr.out[28]" "unitConversion396.i";
connectAttr "Face_Expr.out[27]" "unitConversion395.i";
connectAttr "tongue_03_Ctrl.msg" "tongue_03_Ctrl_tag.act";
connectAttr "Face_Expr.out[29]" "unitConversion397.i";
connectAttr "Face_Expr.out[30]" "unitConversion398.i";
connectAttr "tongue_04_Ctrl.msg" "tongue_04_Ctrl_tag.act";
connectAttr "Face_Expr.out[32]" "unitConversion400.i";
connectAttr "Face_Expr.out[31]" "unitConversion399.i";
connectAttr "tongue_05_Ctrl.msg" "tongue_05_Ctrl_tag.act";
connectAttr "Face_Expr.out[13]" "unitConversion386.i";
connectAttr "Face_Expr.out[12]" "unitConversion384.i";
connectAttr "Face_Expr.out[14]" "unitConversion388.i";
connectAttr "low_teeth_Ctrl.msg" "low_teeth_Ctrl_tag.act";
connectAttr "C_lower_teeth_Ctrl.msg" "C_lower_teeth_Ctrl_tag.act";
connectAttr "L_lower_teeth_Ctrl.msg" "L_lower_teeth_Ctrl_tag.act";
connectAttr "R_lower_teeth_Ctrl.msg" "R_lower_teeth_Ctrl_tag.act";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
dataStructure -fmt "raw" -as "name=faceConnectMarkerStructure:bool=faceConnectMarker:string[200]=faceConnectOutputGroups";
dataStructure -fmt "raw" -as "name=faceConnectOutputStructure:bool=faceConnectOutput:string[200]=faceConnectOutputAttributes:string[200]=faceConnectOutputGroups";
// End of MTP_TongueAndTeeth_Rig.ma
