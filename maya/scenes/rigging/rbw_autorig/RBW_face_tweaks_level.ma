//Maya ASCII 2019 scene
//Name: RBW_face_tweaks_level.ma
//Last modified: Tue, May 28, 2024 10:22:52 AM
//Codeset: 1252
requires maya "2019";
requires -nodeType "VRaySettingsNode" -dataType "VRaySunParams" -dataType "vrayFloatVectorData"
		 -dataType "vrayFloatVectorData" -dataType "vrayIntData" "vrayformaya" "5";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "vrayBuild" "5.00.21 848ea8a";
fileInfo "application" "maya";
fileInfo "product" "Maya 2019";
fileInfo "version" "2019";
fileInfo "cutIdentifier" "202004141915-92acaa8c08";
fileInfo "osv" "Microsoft Windows 10 Technical Preview  (Build 19045)\n";
createNode transform -s -n "persp";
	rename -uid "41195423-428A-A34C-95BC-B1974F77B04C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 28 21 28 ;
	setAttr ".r" -type "double3" -27.938352729602379 44.999999999999972 -5.172681101354183e-14 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "53A96193-4B51-C1C1-6A12-259F48153BEB";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 44.82186966202994;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "4FEAFE48-4306-E12E-843C-A9B4AD4EACE5";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "4FA11862-4826-2D25-C5D7-98B2A6C0E774";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "54504B19-445B-000A-DEED-028966E507E3";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "757734C1-4753-5F2D-81B7-DBBDCE617366";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "E2479137-486B-3313-A50E-FA90191553B6";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "617D205C-490C-0F9C-7211-CCBC3259F21B";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "BodyRM_Rig_Grp";
	rename -uid "5E9FA951-4ECF-3DA0-A71B-2CB4C406C987";
createNode transform -n "BodyRM_Controls_Grp" -p "BodyRM_Rig_Grp";
	rename -uid "039B6209-427A-CC58-3EA5-D586B6918F71";
createNode transform -n "BodyRM_Lip_Controls_Grp" -p "BodyRM_Controls_Grp";
	rename -uid "24B2BB4E-4941-1A36-AE6B-D4B0E96E8B65";
createNode transform -n "R_up_lip_01_Ctrl_Grp" -p "BodyRM_Lip_Controls_Grp";
	rename -uid "73F925F0-4F51-17C6-7E7D-5DB9FF59EF18";
	setAttr ".t" -type "double3" -1.7226123809814453 20.369339678075061 6.7005225066989844 ;
	setAttr ".r" -type "double3" 0 180 0 ;
createNode transform -n "R_up_lip_01_Ctrl" -p "R_up_lip_01_Ctrl_Grp";
	rename -uid "5B089F0C-4956-A98E-0C13-B7AD2E8BDA6A";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "R_up_lip_01_CtrlShape" -p "R_up_lip_01_Ctrl";
	rename -uid "F010EB84-46D4-0BBF-F9EE-7CB3FE9FC68A";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 -2 -1 0 1 2 3 4 5 6
		7
		0.21751232088512285 -0.20028357956462628 -0.78125477664225418
		-1.2559309838012457e-15 0.29392313346802368 -0.78125477664225418
		-0.21751232088512573 -0.20028357956462628 -0.78125477664225418
		-1.2559309838012457e-15 -0.20790776890413909 -0.78125477664225418
		0.21751232088512285 -0.20028357956462628 -0.78125477664225418
		-1.2559309838012457e-15 0.29392313346802368 -0.78125477664225418
		-0.21751232088512573 -0.20028357956462628 -0.78125477664225418
		;
createNode transform -n "R_up_lip_Ctrl_Grp" -p "BodyRM_Lip_Controls_Grp";
	rename -uid "E04C6EB7-47F5-6B31-BF67-36BE704B221C";
	setAttr ".t" -type "double3" -0.93601891398429882 20.516340656167088 7.2393479347229004 ;
	setAttr ".r" -type "double3" 0 180 0 ;
createNode transform -n "R_up_lip_Ctrl" -p "R_up_lip_Ctrl_Grp";
	rename -uid "96F7AF66-4C42-0C26-1523-58809790627F";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "R_up_lip_CtrlShape" -p "R_up_lip_Ctrl";
	rename -uid "020E7A9E-40D3-7A64-6A92-06A839F5D04D";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 -2 -1 0 1 2 3 4 5 6
		7
		0.21751232088512296 -0.20028357956462628 -0.64776841805827967
		-1.1449086813387298e-15 0.29392313346802368 -0.64776841805827967
		-0.21751232088512562 -0.20028357956462628 -0.64776841805827967
		-1.2559309838012457e-15 -0.20790776890413909 -0.64776841805827967
		0.21751232088512296 -0.20028357956462628 -0.64776841805827967
		-1.1449086813387298e-15 0.29392313346802368 -0.64776841805827967
		-0.21751232088512562 -0.20028357956462628 -0.64776841805827967
		;
createNode transform -n "up_lip_Ctrl_Grp" -p "BodyRM_Lip_Controls_Grp";
	rename -uid "8F317DDA-449B-9E17-8BC7-348A14A1C1DA";
	setAttr ".t" -type "double3" 0 20.742899782057606 7.4302005767822257 ;
createNode transform -n "up_lip_Ctrl" -p "up_lip_Ctrl_Grp";
	rename -uid "4C415CED-493F-3C15-D730-5C91456E4E8A";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "up_lip_CtrlShape" -p "up_lip_Ctrl";
	rename -uid "04AB7F4F-4EDA-EB15-D7B8-758A2F0C464B";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 -2 -1 0 1 2 3 4 5 6
		7
		0.21751232088512315 -0.2002835795646255 0.64776841805827701
		-1.1420989954047921e-15 0.29392313346802279 0.64776841805827701
		-0.21751232088512554 -0.2002835795646255 0.64776841805827701
		-1.184703883517219e-15 -0.20790776890413742 0.64776841805827701
		0.21751232088512315 -0.2002835795646255 0.64776841805827701
		-1.1420989954047921e-15 0.29392313346802279 0.64776841805827701
		-0.21751232088512554 -0.2002835795646255 0.64776841805827701
		;
createNode transform -n "L_up_lip_Ctrl_Grp" -p "BodyRM_Lip_Controls_Grp";
	rename -uid "C36BCC9B-4074-082A-9A41-688FEF15C3DB";
	setAttr ".t" -type "double3" 0.93601891398429882 20.516340656167088 7.2393479347229004 ;
createNode transform -n "L_up_lip_Ctrl" -p "L_up_lip_Ctrl_Grp";
	rename -uid "89ED8A27-43C9-A351-CEEA-589EF47E732D";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "L_up_lip_CtrlShape" -p "L_up_lip_Ctrl";
	rename -uid "CC0111D0-4E04-6662-E1A4-2DAD59777B21";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 -2 -1 0 1 2 3 4 5 6
		7
		0.21751232088512315 -0.2002835795646255 0.64776841805827701
		-1.1420989954047921e-15 0.29392313346802279 0.64776841805827701
		-0.21751232088512554 -0.2002835795646255 0.64776841805827701
		-1.184703883517219e-15 -0.20790776890413742 0.64776841805827701
		0.21751232088512315 -0.2002835795646255 0.64776841805827701
		-1.1420989954047921e-15 0.29392313346802279 0.64776841805827701
		-0.21751232088512554 -0.2002835795646255 0.64776841805827701
		;
createNode transform -n "L_up_lip_01_Ctrl_Grp" -p "BodyRM_Lip_Controls_Grp";
	rename -uid "41464B42-4BF4-10FC-1FCD-DC94647DB2F9";
	setAttr ".t" -type "double3" 1.7226123809814453 20.369339678075061 6.7005225066989844 ;
createNode transform -n "L_up_lip_01_Ctrl" -p "L_up_lip_01_Ctrl_Grp";
	rename -uid "1918C498-465B-CBFB-C409-3781CFF1C9AA";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "L_up_lip_01_CtrlShape" -p "L_up_lip_01_Ctrl";
	rename -uid "38F07842-4E32-23E8-D1F1-DEA703C1FC50";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 -2 -1 0 1 2 3 4 5 6
		7
		0.21751232088512293 -0.2002835795646255 0.78125477664225063
		-1.3641436003298234e-15 0.29392313346802279 0.78125477664225063
		-0.21751232088512576 -0.2002835795646255 0.78125477664225063
		-1.4067484884422503e-15 -0.20790776890413742 0.78125477664225063
		0.21751232088512293 -0.2002835795646255 0.78125477664225063
		-1.3641436003298234e-15 0.29392313346802279 0.78125477664225063
		-0.21751232088512576 -0.2002835795646255 0.78125477664225063
		;
createNode transform -n "R_low_lip_01_Ctrl_Grp" -p "BodyRM_Lip_Controls_Grp";
	rename -uid "7361AE6C-45CB-2015-7FBB-BBB5B3A04859";
	setAttr ".t" -type "double3" -1.6951909065246582 19.988830107342835 6.3012366045154504 ;
	setAttr ".r" -type "double3" 0 0 180 ;
createNode transform -n "R_low_lip_01_Ctrl" -p "R_low_lip_01_Ctrl_Grp";
	rename -uid "596886E3-48AB-4262-9482-4E96DFFFBF6A";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "R_low_lip_01_CtrlShape" -p "R_low_lip_01_Ctrl";
	rename -uid "AEB30A53-4D8F-9D87-D101-8EB67988D02A";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 -2 -1 0 1 2 3 4 5 6
		7
		0.21751232088512271 -0.096257860853263044 0.78125477664225151
		-1.5861882052548547e-15 0.39794885217938525 0.78125477664225151
		-0.21751232088512598 -0.096257860853263044 0.78125477664225151
		-1.6287930933672816e-15 -0.10388205019277497 0.78125477664225151
		0.21751232088512271 -0.096257860853263044 0.78125477664225151
		-1.5861882052548547e-15 0.39794885217938525 0.78125477664225151
		-0.21751232088512598 -0.096257860853263044 0.78125477664225151
		;
createNode transform -n "R_low_lip_Ctrl_Grp" -p "BodyRM_Lip_Controls_Grp";
	rename -uid "1477F3F4-423A-1D74-065F-138E9246C722";
	setAttr ".t" -type "double3" -0.91451603174209595 19.726325147751961 6.7682154451131353 ;
	setAttr ".r" -type "double3" 0 0 180 ;
createNode transform -n "R_low_lip_Ctrl" -p "R_low_lip_Ctrl_Grp";
	rename -uid "B2ABD9C5-4CCD-4C00-85BD-5CA5D344497A";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "R_low_lip_CtrlShape" -p "R_low_lip_Ctrl";
	rename -uid "6D2C6BC9-461E-67A2-D8D0-65913C4AEC94";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 -2 -1 0 1 2 3 4 5 6
		7
		0.21751232088512315 -0.2002835795646255 0.64776841805827701
		-1.1420989954047921e-15 0.29392313346802279 0.64776841805827701
		-0.21751232088512554 -0.2002835795646255 0.64776841805827701
		-1.184703883517219e-15 -0.20790776890413742 0.64776841805827701
		0.21751232088512315 -0.2002835795646255 0.64776841805827701
		-1.1420989954047921e-15 0.29392313346802279 0.64776841805827701
		-0.21751232088512554 -0.2002835795646255 0.64776841805827701
		;
createNode transform -n "low_lip_Ctrl_Grp" -p "BodyRM_Lip_Controls_Grp";
	rename -uid "962D6F71-429F-EBC5-7AC6-4EB20A15F91C";
	setAttr ".t" -type "double3" -3.9898639947466563e-17 19.636009311401594 7.0391147409200192 ;
	setAttr ".r" -type "double3" 0 0 180 ;
createNode transform -n "low_lip_Ctrl" -p "low_lip_Ctrl_Grp";
	rename -uid "15B55EC9-4BC5-29CF-6918-88AD503C7FA8";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "low_lip_CtrlShape" -p "low_lip_Ctrl";
	rename -uid "FC0819A0-46A3-6F15-E2A8-94BA36CAE365";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 -2 -1 0 1 2 3 4 5 6
		7
		0.21751232088512315 -0.2002835795646255 0.64776841805827701
		-1.1420989954047921e-15 0.29392313346802279 0.64776841805827701
		-0.21751232088512554 -0.2002835795646255 0.64776841805827701
		-1.184703883517219e-15 -0.20790776890413742 0.64776841805827701
		0.21751232088512315 -0.2002835795646255 0.64776841805827701
		-1.1420989954047921e-15 0.29392313346802279 0.64776841805827701
		-0.21751232088512554 -0.2002835795646255 0.64776841805827701
		;
createNode transform -n "L_low_lip_Ctrl_Grp" -p "BodyRM_Lip_Controls_Grp";
	rename -uid "82235326-4B59-4669-68FA-2B98B32CAEDB";
	setAttr ".t" -type "double3" 0.91451603174209595 19.726325147751961 6.7682154451131353 ;
	setAttr ".r" -type "double3" 180 -3.5083546492674388e-15 -9.4006079503318619e-16 ;
createNode transform -n "L_low_lip_Ctrl" -p "L_low_lip_Ctrl_Grp";
	rename -uid "4584519D-42DF-684C-483C-ED8A51DA0014";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "L_low_lip_CtrlShape" -p "L_low_lip_Ctrl";
	rename -uid "2214F033-46CF-9A09-C8D4-E48996A9A20B";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 -2 -1 0 1 2 3 4 5 6
		7
		0.21751232088512307 -0.20028357956462628 -0.64776841805827878
		-1.1102230246251565e-15 0.29392313346802368 -0.64776841805827878
		-0.21751232088512551 -0.20028357956462628 -0.64776841805827878
		-1.1102230246251565e-15 -0.20790776890413909 -0.64776841805827878
		0.21751232088512307 -0.20028357956462628 -0.64776841805827878
		-1.1102230246251565e-15 0.29392313346802368 -0.64776841805827878
		-0.21751232088512551 -0.20028357956462628 -0.64776841805827878
		;
createNode transform -n "L_low_lip_01_Ctrl_Grp" -p "BodyRM_Lip_Controls_Grp";
	rename -uid "2CE4B9EC-4678-127E-DF4D-F3AFDE627A22";
	setAttr ".t" -type "double3" 1.6951909065246582 19.988830107342835 6.3012366045154504 ;
	setAttr ".r" -type "double3" 180 -3.5083546492674388e-15 -9.4006079503318619e-16 ;
createNode transform -n "L_low_lip_01_Ctrl" -p "L_low_lip_01_Ctrl_Grp";
	rename -uid "2DA8C0EF-449F-FC7D-B9B0-76B990EEFDFA";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "L_low_lip_01_CtrlShape" -p "L_low_lip_01_Ctrl";
	rename -uid "38BE4F27-4131-7C88-9504-62BFA922DC7D";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 -2 -1 0 1 2 3 4 5 6
		7
		0.21751232088512262 -0.096257860853263821 -0.78125477664225507
		-1.5543122344752192e-15 0.39794885217938614 -0.78125477664225507
		-0.21751232088512595 -0.096257860853263821 -0.78125477664225507
		-1.5543122344752192e-15 -0.10388205019277663 -0.78125477664225507
		0.21751232088512262 -0.096257860853263821 -0.78125477664225507
		-1.5543122344752192e-15 0.39794885217938614 -0.78125477664225507
		-0.21751232088512595 -0.096257860853263821 -0.78125477664225507
		;
createNode transform -n "BodyRM_EyeLid_Controls_Grp" -p "BodyRM_Controls_Grp";
	rename -uid "00213C66-44F2-8626-77F3-3CA624991336";
createNode transform -n "L_eyeLidUpInn_Ctrl_Grp" -p "BodyRM_EyeLid_Controls_Grp";
	rename -uid "94FBDC45-4CEB-CB2D-BA41-19A2E8DD49B1";
	setAttr ".t" -type "double3" 1.8802814461050161 24.648844157554102 4.9112160535072409 ;
	setAttr ".r" -type "double3" -3.8315801893510448 -5.0209737434370894 -105.31919189002197 ;
createNode transform -n "L_eyeLidUpInn_Ctrl" -p "L_eyeLidUpInn_Ctrl_Grp";
	rename -uid "A0A918A2-4474-840C-87DB-D1920E18F0BC";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "L_eyeLidUpInn_CtrlShape" -p "L_eyeLidUpInn_Ctrl";
	rename -uid "DB748680-4678-734B-31CF-E7AF8177323F";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 -2 -1 0 1 2 3 4 5 6
		7
		0.2841003765304958 -0.21174500830609483 0.84271685403939856
		0.066588055645371638 0.28246170472655363 0.84271685403939856
		-0.15092426523975289 -0.21174500830609472 0.84271685403939856
		0.066588055645371597 -0.21936919764560669 0.84271685403939856
		0.2841003765304958 -0.21174500830609483 0.84271685403939856
		0.066588055645371638 0.28246170472655363 0.84271685403939856
		-0.15092426523975289 -0.21174500830609472 0.84271685403939856
		;
createNode transform -n "L_eyeLidUpOut_Ctrl_Grp" -p "BodyRM_EyeLid_Controls_Grp";
	rename -uid "101AC64A-41FD-6083-C7F6-D8AB5C42C91F";
	setAttr ".t" -type "double3" 5.1213719731303087 25.172799690262991 4.1653392228522401 ;
	setAttr ".r" -type "double3" 10.750327676813161 -1.5113091818522075 129.71475135551137 ;
createNode transform -n "L_eyeLidUpOut_Ctrl" -p "L_eyeLidUpOut_Ctrl_Grp";
	rename -uid "9DE161EA-436B-99D2-E65F-2F8321208D7C";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "L_eyeLidUpOut_CtrlShape" -p "L_eyeLidUpOut_Ctrl";
	rename -uid "0400CA55-4EB4-9E8A-FF4F-C29C025E8F84";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 -2 -1 0 1 2 3 4 5 6
		7
		0.44472002241976916 -0.20214076217698196 0.81376834154552558
		0.22720770153464512 0.29206595085566667 0.81376834154552558
		0.0096953806495205918 -0.2021407621769819 0.81376834154552558
		0.22720770153464506 -0.20976495151649394 0.81376834154552558
		0.44472002241976916 -0.20214076217698196 0.81376834154552558
		0.22720770153464512 0.29206595085566667 0.81376834154552558
		0.0096953806495205918 -0.2021407621769819 0.81376834154552558
		;
createNode transform -n "L_eyeLidLowOut_Ctrl_Grp" -p "BodyRM_EyeLid_Controls_Grp";
	rename -uid "3AAE333C-41B6-F035-6DA4-58B6204A7E5B";
	setAttr ".t" -type "double3" 5.2545280456542969 22.615339279174805 3.8841893672943115 ;
	setAttr ".r" -type "double3" 6.9935671519798488 27.417591460079883 37.122766051474194 ;
createNode transform -n "L_eyeLidLowOut_Ctrl" -p "L_eyeLidLowOut_Ctrl_Grp";
	rename -uid "B7C05FFE-4E55-DB47-34DC-C09CB2AE2770";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "L_eyeLidLowOut_CtrlShape" -p "L_eyeLidLowOut_Ctrl";
	rename -uid "0478A728-42E6-88E1-59D8-D58AEB579DE1";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 -2 -1 0 1 2 3 4 5 6
		7
		0.2176344511431445 -0.005471549749555933 0.84184363041749499
		-0.07656073754118442 0.44729889528673428 0.84184363041749499
		-0.21172227881497302 -0.075466102301916665 0.84184363041749499
		0.0041828022398051777 -0.047993680227267334 0.84184363041749499
		0.2176344511431445 -0.005471549749555933 0.84184363041749499
		-0.07656073754118442 0.44729889528673428 0.84184363041749499
		-0.21172227881497302 -0.075466102301916665 0.84184363041749499
		;
createNode transform -n "L_eyeLidLowInn_Ctrl_Grp" -p "BodyRM_EyeLid_Controls_Grp";
	rename -uid "D4B8196D-4D4E-4C09-B705-41A100302D02";
	setAttr ".t" -type "double3" 3.0470523834228516 22.286026000976563 4.6612105369567871 ;
	setAttr ".r" -type "double3" -16.248130561871605 14.093457233141033 -10.019944701829251 ;
createNode transform -n "L_eyeLidLowInn_Ctrl" -p "L_eyeLidLowInn_Ctrl_Grp";
	rename -uid "6CEB5E96-40C7-DB98-474C-F38F39605AA2";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "L_eyeLidLowInn_CtrlShape" -p "L_eyeLidLowInn_Ctrl";
	rename -uid "B874DCF8-4C6C-DE09-087B-5EBD04325427";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 -2 -1 0 1 2 3 4 5 6
		7
		0.20794080337867876 -0.25144430590731559 0.80772784273571985
		-0.0095715175064454566 0.24276240712533276 0.80772784273571985
		-0.2270838383915697 -0.25144430590731559 0.80772784273571985
		-0.0095715175064455676 -0.25906849524682751 0.80772784273571985
		0.20794080337867876 -0.25144430590731559 0.80772784273571985
		-0.0095715175064454566 0.24276240712533276 0.80772784273571985
		-0.2270838383915697 -0.25144430590731559 0.80772784273571985
		;
createNode transform -n "R_eyeLidUpInn_Ctrl_Grp" -p "BodyRM_EyeLid_Controls_Grp";
	rename -uid "7ECBA060-4B36-0DD9-333A-4D889F9AE74F";
	setAttr ".t" -type "double3" -1.88028 24.6488 4.91122 ;
	setAttr ".r" -type "double3" -176.16841981064894 354.97902625656292 -74.68080810997813 ;
createNode transform -n "R_eyeLidUpInn_Ctrl" -p "R_eyeLidUpInn_Ctrl_Grp";
	rename -uid "205DD207-4D8C-FE19-D166-D286331E17EE";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "R_eyeLidUpInn_CtrlShape" -p "R_eyeLidUpInn_Ctrl";
	rename -uid "180817D7-4766-04B3-BB9C-0381F8BB6023";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 -2 -1 0 1 2 3 4 5 6
		7
		0.28405722537558376 -0.21175524548211655 -0.84271599749663384
		0.066544904490456247 0.28245146755053163 -0.84271599749663295
		-0.15096741639466416 -0.21175524548211566 -0.84271599749663384
		0.066544904490456247 -0.21937943482162758 -0.84271599749663295
		0.28405722537558376 -0.21175524548211655 -0.84271599749663384
		0.066544904490456247 0.28245146755053163 -0.84271599749663295
		-0.15096741639466416 -0.21175524548211566 -0.84271599749663384
		;
createNode transform -n "R_eyeLidUpOut_Ctrl_Grp" -p "BodyRM_EyeLid_Controls_Grp";
	rename -uid "1968524B-4886-0A83-6000-7DB2CACB17D5";
	setAttr ".t" -type "double3" -5.12137 25.1728 4.16534 ;
	setAttr ".r" -type "double3" 169.24967232318684 358.48869081814775 50.285248644488597 ;
createNode transform -n "R_eyeLidUpOut_Ctrl" -p "R_eyeLidUpOut_Ctrl_Grp";
	rename -uid "DFD628F8-4FFD-34FB-4812-0E8CC360AE7B";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "R_eyeLidUpOut_CtrlShape" -p "R_eyeLidUpOut_Ctrl";
	rename -uid "BEC612A1-4C80-1FFB-6BFE-5D977D08F874";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 -2 -1 0 1 2 3 4 5 6
		7
		0.44471850342100083 -0.2021421964378014 -0.81376786334314932
		0.22720618253587332 0.29206451659484856 -0.81376786334314755
		0.0096938616507529218 -0.2021421964378014 -0.81376786334314932
		0.22720618253587332 -0.20976638577731421 -0.81376786334314932
		0.44471850342100083 -0.2021421964378014 -0.81376786334314932
		0.22720618253587332 0.29206451659484856 -0.81376786334314755
		0.0096938616507529218 -0.2021421964378014 -0.81376786334314932
		;
createNode transform -n "R_eyeLidLowOut_Ctrl_Grp" -p "BodyRM_EyeLid_Controls_Grp";
	rename -uid "47FED676-4AED-07CB-740A-E7B853BEAFDE";
	setAttr ".t" -type "double3" -5.25453 22.6153 3.88419 ;
	setAttr ".r" -type "double3" 173.00643284802021 387.4175914600799 142.87723394852583 ;
createNode transform -n "R_eyeLidLowOut_Ctrl" -p "R_eyeLidLowOut_Ctrl_Grp";
	rename -uid "6574EEEE-45E6-ECDB-53B7-FAB982FA4A19";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "R_eyeLidLowOut_CtrlShape" -p "R_eyeLidLowOut_Ctrl";
	rename -uid "A81DE99A-41E5-8B77-3AD5-D2B5444879CC";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 -2 -1 0 1 2 3 4 5 6
		7
		0.2176544024003455 -0.0054381196752686378 -0.84184923852107296
		-0.076540786283983309 0.44733232536102463 -0.84184923852107474
		-0.2117023275577754 -0.075432672227631059 -0.84184923852107296
		0.004202753497006384 -0.047960250152975448 -0.84184923852107296
		0.2176544024003455 -0.0054381196752686378 -0.84184923852107296
		-0.076540786283983309 0.44733232536102463 -0.84184923852107474
		-0.2117023275577754 -0.075432672227631059 -0.84184923852107296
		;
createNode transform -n "R_eyeLidLowInn_Ctrl_Grp" -p "BodyRM_EyeLid_Controls_Grp";
	rename -uid "B615C504-459A-FF30-BDE3-34AFFBB5A0E3";
	setAttr ".t" -type "double3" -3.04705 22.286 4.66121 ;
	setAttr ".r" -type "double3" 16.248130561871591 525.90654276685893 10.019944701829232 ;
createNode transform -n "R_eyeLidLowInn_Ctrl" -p "R_eyeLidLowInn_Ctrl_Grp";
	rename -uid "DAB53DC2-4662-C639-18A8-F59738B40A5C";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "R_eyeLidLowInn_CtrlShape" -p "R_eyeLidLowInn_Ctrl";
	rename -uid "AB715163-4FC1-E51E-6617-FDB8AE5A82C1";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 4 2 no 3
		9 -2 -1 0 1 2 3 4 5 6
		7
		0.20793856128517585 -0.25141932345476548 -0.80773511390416353
		-0.0095737595999485503 0.24278738957788093 -0.8077351139041653
		-0.22708608048507251 -0.25141932345476903 -0.80773511390416353
		-0.0095737595999489944 -0.25904351279427829 -0.80773511390416353
		0.20793856128517585 -0.25141932345476548 -0.80773511390416353
		-0.0095737595999485503 0.24278738957788093 -0.8077351139041653
		-0.22708608048507251 -0.25141932345476903 -0.80773511390416353
		;
createNode transform -n "BodyRM_Brows_Controls_Grp" -p "BodyRM_Controls_Grp";
	rename -uid "C8EB22EF-4A4B-A5BB-87AF-1FB0D5A34AFB";
createNode transform -n "L_brow_01_Ctrl_Grp" -p "BodyRM_Brows_Controls_Grp";
	rename -uid "9A5DB26D-4740-5CD3-0E03-F2940F873C13";
	setAttr ".t" -type "double3" 1.2225185632705688 27.13810920715332 5.5643825531005859 ;
createNode transform -n "L_brow_01_Ctrl" -p "L_brow_01_Ctrl_Grp";
	rename -uid "49334332-4C3D-5417-78B0-2AAA5F284235";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "L_brow_01_CtrlShape" -p "L_brow_01_Ctrl";
	rename -uid "C84E65A5-417F-8598-386B-53BFC798540E";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.29776539930838547 0.29776539930838486 0.58889833269112424
		-2.7979691249959896e-16 0.42110386610735856 0.58889833269112413
		-0.29776539930838541 0.29776539930838519 0.58889833269112424
		-0.42110386610735884 1.2605073773899358e-16 0.58889833269112424
		-0.29776539930838553 -0.29776539930838486 0.58889833269112424
		-3.4982510013237298e-16 -0.42110386610735867 0.58889833269112424
		0.29776539930838469 -0.2977653993083853 0.58889833269112424
		0.42110386610735856 -2.2409020042487765e-16 0.58889833269112424
		0.29776539930838547 0.29776539930838486 0.58889833269112424
		-2.7979691249959896e-16 0.42110386610735856 0.58889833269112413
		-0.29776539930838541 0.29776539930838519 0.58889833269112424
		;
createNode transform -n "L_brow_02_Ctrl_Grp" -p "BodyRM_Brows_Controls_Grp";
	rename -uid "C3210DC2-45E4-1EB0-4737-EDA863A0D19F";
	setAttr ".t" -type "double3" 3.6255110245018054 27.809473066088177 4.8423030077580522 ;
createNode transform -n "L_brow_02_Ctrl" -p "L_brow_02_Ctrl_Grp";
	rename -uid "26453EA3-4C8C-C9BC-0233-078FE344C691";
	addAttr -ci true -sn "Follow_Brow" -ln "Follow_Brow" -min 0 -max 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr -k on ".Follow_Brow" 1;
createNode nurbsCurve -n "L_brow_02_CtrlShape" -p "L_brow_02_Ctrl";
	rename -uid "795A77DE-499E-B33F-F82F-82B558D8F814";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.49876263736743309 0.49876263736742443 0.59032821844469219
		-1.0760843877152791e-15 0.70535688616998926 0.59032821844469219
		-0.49876263736743365 0.49876263736742471 0.59032821844469219
		-0.70535688616999892 -7.7604980288913966e-15 0.59032821844469219
		-0.49876263736743415 -0.49876263736744059 0.59032821844469219
		-1.2081493966149103e-15 -0.70535688617000658 0.5903282184446923
		0.4987626373674317 -0.49876263736744109 0.59032821844469219
		0.70535688616999737 -8.3437413673933566e-15 0.59032821844469219
		0.49876263736743309 0.49876263736742443 0.59032821844469219
		-1.0760843877152791e-15 0.70535688616998926 0.59032821844469219
		-0.49876263736743365 0.49876263736742471 0.59032821844469219
		;
createNode transform -n "L_brow_03_Ctrl_Grp" -p "BodyRM_Brows_Controls_Grp";
	rename -uid "93AE7240-4910-A2C3-5FA1-9DAAB102FAD2";
	setAttr ".t" -type "double3" 5.8342890739440918 27.045372009277344 3.6995601654052734 ;
createNode transform -n "L_brow_03_Ctrl" -p "L_brow_03_Ctrl_Grp";
	rename -uid "AD7A76C6-4E01-9A69-9957-BB8C797ED175";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "L_brow_03_CtrlShape" -p "L_brow_03_Ctrl";
	rename -uid "2AF67DD9-4058-D813-CD5D-2DBA9906BC73";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.29776539930838619 0.29776539930838486 0.59032821844469219
		4.0013748488246198e-16 0.42110386610735856 0.59032821844469219
		-0.29776539930838469 0.29776539930838519 0.59032821844469219
		-0.42110386610735823 1.2202536521099587e-16 0.59032821844469219
		-0.29776539930838475 -0.29776539930838486 0.59032821844469219
		3.2129358767872121e-16 -0.42110386610735867 0.5903282184446923
		0.29776539930838536 -0.2977653993083853 0.59032821844469219
		0.42110386610735906 -2.2617570794808763e-16 0.59032821844469219
		0.29776539930838619 0.29776539930838486 0.59032821844469219
		4.0013748488246198e-16 0.42110386610735856 0.59032821844469219
		-0.29776539930838469 0.29776539930838519 0.59032821844469219
		;
createNode transform -n "R_brow_01_Ctrl_Grp" -p "BodyRM_Brows_Controls_Grp";
	rename -uid "563078D1-4F94-7083-3C49-C099CC360B91";
	setAttr ".t" -type "double3" -1.2225199999999998 27.1381 5.56438 ;
	setAttr ".r" -type "double3" 0 540 0 ;
createNode transform -n "R_brow_01_Ctrl" -p "R_brow_01_Ctrl_Grp";
	rename -uid "AAED315E-4BA5-D67F-1FC3-1B98AE4B9625";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "R_brow_01_CtrlShape" -p "R_brow_01_Ctrl";
	rename -uid "F8882A1B-463E-A5C9-5C86-609B9CD64B07";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.29776361288205455 0.2977768474633159 -0.59033139296455051
		-1.7864263308615432e-06 0.4211153142622876 -0.59033139296455051
		-0.29776718573471544 0.2977768474633159 -0.59033139296455051
		-0.42110565253368992 1.1448154930138215e-05 -0.59033139296455051
		-0.29776718573471578 -0.29775395115345549 -0.59033139296455051
		-1.7864263308615432e-06 -0.42109241795242769 -0.59033139296455051
		0.29776361288205344 -0.29775395115345549 -0.59033139296455051
		0.42110207968102836 1.1448154930138215e-05 -0.59033139296455051
		0.29776361288205455 0.2977768474633159 -0.59033139296455051
		-1.7864263308615432e-06 0.4211153142622876 -0.59033139296455051
		-0.29776718573471544 0.2977768474633159 -0.59033139296455051
		;
createNode transform -n "R_brow_02_Ctrl_Grp" -p "BodyRM_Brows_Controls_Grp";
	rename -uid "73317B05-464A-56EB-B95F-21BF3B26EE0A";
	setAttr ".t" -type "double3" -3.62551 27.8095 4.8423 ;
	setAttr ".r" -type "double3" 0 540 0 ;
createNode transform -n "R_brow_02_Ctrl" -p "R_brow_02_Ctrl_Grp";
	rename -uid "94CFA00D-4713-621F-B9FF-DFB5917A4DC7";
	addAttr -ci true -sn "Follow_Brow" -ln "Follow_Brow" -min 0 -max 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr -k on ".Follow_Brow" 1;
createNode nurbsCurve -n "R_brow_02_CtrlShape" -p "R_brow_02_Ctrl";
	rename -uid "B127E450-461D-33DF-C5E8-E7AABC47282E";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.49876391123080699 0.49872914779852745 -0.5903319582846851
		1.273863373455737e-06 0.70532339660109433 -0.59033195828468399
		-0.49876136350405903 0.49872914779852745 -0.59033195828468399
		-0.70535561230662513 -3.3489568906115946e-05 -0.59033195828468399
		-0.4987613635040597 -0.49879612693633973 -0.59033195828468399
		1.273863373455737e-06 -0.70539037573890184 -0.59033195828468399
		0.49876391123080543 -0.49879612693633973 -0.5903319582846851
		0.70535816003337071 -3.3489568906115946e-05 -0.5903319582846851
		0.49876391123080699 0.49872914779852745 -0.5903319582846851
		1.273863373455737e-06 0.70532339660109433 -0.59033195828468399
		-0.49876136350405903 0.49872914779852745 -0.59033195828468399
		;
createNode transform -n "R_brow_03_Ctrl_Grp" -p "BodyRM_Brows_Controls_Grp";
	rename -uid "523CFC98-413F-E838-2C57-25B33F162C8A";
	setAttr ".t" -type "double3" -5.83429 27.045400000000004 3.6995600000000004 ;
	setAttr ".r" -type "double3" 0 540 0 ;
createNode transform -n "R_brow_03_Ctrl" -p "R_brow_03_Ctrl_Grp";
	rename -uid "00DCC3B7-44EE-A4C6-7C6E-6AA8165CD283";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "R_brow_03_CtrlShape" -p "R_brow_03_Ctrl";
	rename -uid "D2FFA3C4-4645-822D-5DF5-809D08C4D227";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.29776424785243116 0.29773059570313187 -0.59032842410925979
		-1.1514559545370714e-06 0.4210690625021033 -0.59032842410925979
		-0.29776655076433944 0.29773059570313187 -0.59032842410925979
		-0.42110501756331326 -3.4803605254199907e-05 -0.59032842410925979
		-0.29776655076433944 -0.29780020291364023 -0.59032842410925979
		-1.1514559545370714e-06 -0.42113866971261177 -0.59032842410925979
		0.29776424785243 -0.29780020291364023 -0.59032842410925979
		0.42110271465140525 -3.4803605254199907e-05 -0.59032842410925979
		0.29776424785243116 0.29773059570313187 -0.59032842410925979
		-1.1514559545370714e-06 0.4210690625021033 -0.59032842410925979
		-0.29776655076433944 0.29773059570313187 -0.59032842410925979
		;
createNode transform -n "BodyRM_Joints_Grp" -p "BodyRM_Rig_Grp";
	rename -uid "CECAB721-47F9-682F-077A-45968294F193";
createNode joint -n "zerorm_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "F0ADB74B-4DA7-F38D-F31B-258F57142BA1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 20 0 1;
	setAttr ".radi" 0.5;
createNode joint -n "R_up_lip_01_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "AD4ADE25-4EC8-7EBF-9414-8B9DBBBF4297";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.7226123809814453 20.369339678075061 6.7005225066989844 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 180 0 ;
	setAttr ".bps" -type "matrix" -1 0 -1.2246467991473532e-16 0 0 1 0 0 1.2246467991473532e-16 0 -1 0
		 -1.7226123809814453 20.369339678075061 6.7005225066989844 1;
	setAttr ".radi" 0.5;
createNode joint -n "R_up_lip_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "BF56993F-4607-57D0-E81F-018CDFD1C1D0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.93601891398429882 20.516340656167088 7.2393479347229004 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 180 0 ;
	setAttr ".bps" -type "matrix" -1 0 -1.2246467991473532e-16 0 0 1 0 0 1.2246467991473532e-16 0 -1 0
		 -0.93601891398429882 20.516340656167088 7.2393479347229004 1;
	setAttr ".radi" 0.5;
createNode joint -n "up_lip_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "75D9E908-499D-D0E5-7B5D-27A1D09B65E5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.0160326509054483e-27 20.742899782057606 7.4302005767822257 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 2.0160326509054483e-27 20.742899782057606 7.4302005767822257 1;
	setAttr ".radi" 0.5;
createNode joint -n "L_up_lip_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "15BAF76B-48CE-0970-5920-8891AC823877";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.93601891398429882 20.516340656167088 7.2393479347229004 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.93601891398429882 20.516340656167088 7.2393479347229004 1;
	setAttr ".radi" 0.5;
createNode joint -n "L_up_lip_01_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "4B969D1E-4E2B-F597-4625-50B5C5DB78F0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.7226123809814453 20.369339678075061 6.7005225066989844 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 1.7226123809814453 20.369339678075061 6.7005225066989844 1;
	setAttr ".radi" 0.5;
createNode joint -n "R_low_lip_01_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "91CD737D-4215-B94B-7380-AD83E0250413";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.6951909065246582 19.988830107342835 6.3012366045154504 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 180 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991473532e-16 0 0 -1.2246467991473532e-16 -1 0 0
		 0 0 1 0 -1.6951909065246582 19.988830107342835 6.3012366045154504 1;
	setAttr ".radi" 0.5;
createNode joint -n "R_low_lip_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "4603A4BF-4C36-7CD2-389D-ADA1BC04F8ED";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.91451603174209595 19.726325147751961 6.7682154451131353 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 180 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991473532e-16 0 0 -1.2246467991473532e-16 -1 0 0
		 0 0 1 0 -0.91451603174209595 19.726325147751961 6.7682154451131353 1;
	setAttr ".radi" 0.5;
createNode joint -n "low_lip_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "91E70D54-42FC-0E9A-519C-4F9AA46AC895";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -3.9130767231693188e-17 19.636009311401594 7.0391147409200192 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 180 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991473532e-16 0 0 -1.2246467991473532e-16 -1 0 0
		 0 0 1 0 -3.9130767231693188e-17 19.636009311401594 7.0391147409200192 1;
	setAttr ".radi" 0.5;
createNode joint -n "L_low_lip_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "7DC786E9-4F7A-5590-7B1E-B7A9C1A27C8F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.91451603174209595 19.726325147751961 6.7682154451131353 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 180 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 -1 1.2246467991473532e-16 0 0 -1.2246467991473532e-16 -1 0
		 0.91451603174209595 19.726325147751961 6.7682154451131353 1;
	setAttr ".radi" 0.5;
createNode joint -n "L_low_lip_01_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "C66C0CBC-4F38-73BD-395B-B8A4AAE8093B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.6951909065246582 19.988830107342835 6.3012366045154504 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 180 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 -1 1.2246467991473532e-16 0 0 -1.2246467991473532e-16 -1 0
		 1.6951909065246582 19.988830107342835 6.3012366045154504 1;
	setAttr ".radi" 0.5;
createNode joint -n "L_eyeLidUpInn_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "0A7B10DB-4C9E-E40F-6C42-2F814083DE71";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.8802814461050172 24.648844157554105 4.9112160535072418 ;
	setAttr ".r" -type "double3" -6.7983620541979493e-15 3.9756933518293967e-15 6.7770465774351353e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -3.8315801893510391 -5.020973743437092 -105.31919189002197 ;
	setAttr ".bps" -type "matrix" -0.26318233227390353 -0.96076804626362788 0.087520404807337179 0
		 0.96076804626362788 -0.26924624012151521 -0.066567435426031732 0 0.087520404807337235 0.066567435426031676 0.99393609215238832 0
		 1.8802814461050172 24.648844157554105 4.9112160535072418 1;
	setAttr ".radi" 0.5;
createNode joint -n "L_eyeLidUpOut_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "7A8784D0-4D2A-5E6B-043C-EEA8003B849F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.1213719731303113 25.172799690262995 4.1653392228522392 ;
	setAttr ".r" -type "double3" -1.5902773407317584e-15 7.9513867036587919e-16 -1.1034765745125397e-32 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 10.750327676813161 -1.511309181852208 129.71475135551137 ;
	setAttr ".bps" -type "matrix" -0.63874361419188475 0.76896748539874116 0.026374262617655977 0
		 -0.75259103726650789 -0.63153592205021636 0.18646476821722824 0 0.16004163819209705 0.099254146309963487 0.98210757480281674 0
		 5.1213719731303113 25.172799690262995 4.1653392228522392 1;
	setAttr ".radi" 0.5;
createNode joint -n "L_eyeLidLowOut_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "6D68ED20-44ED-396A-F13B-D881DD2AA72E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.2545280456542978 22.615339279174805 3.8841893672943106 ;
	setAttr ".r" -type "double3" -1.5902773407317588e-15 9.939233379573501e-17 6.1623246953355651e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 6.993567151979847 27.417591460079887 37.122766051474187 ;
	setAttr ".bps" -type "matrix" 0.70778174146570572 0.53573335075565853 -0.46047234806868398 0
		 -0.5543305080517511 0.82524912201396861 0.10808133260772343 0 0.43790717542746449 0.17875587683566777 0.88106960122674272 0
		 5.2545280456542978 22.615339279174805 3.8841893672943106 1;
	setAttr ".radi" 0.5;
createNode joint -n "L_eyeLidLowInn_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "C0756735-4A54-1DEC-B5F3-EC96CE7CFE6A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 3.0470523834228516 22.286026000976555 4.6612105369567871 ;
	setAttr ".r" -type "double3" -2.5842006786891076e-15 3.9756933518293944e-15 -6.361109362927032e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -16.248130561871601 14.093457233141029 -10.019944701829246 ;
	setAttr ".bps" -type "matrix" 0.95510618484860121 -0.16875382102354169 -0.24350425777366624 0
		 0.099948873761017132 0.95726978187130829 -0.27137573095242346 0 0.2788949592550104 0.23485466272997929 0.93116104358812912 0
		 3.0470523834228516 22.286026000976555 4.6612105369567871 1;
	setAttr ".radi" 0.5;
createNode joint -n "R_eyeLidUpInn_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "E6694E59-45DD-B838-86C0-E3A79335F69E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.88028 24.648800000000005 4.9112199999999984 ;
	setAttr ".r" -type "double3" -6.3611093629270209e-15 2.3059021440610494e-14 5.7249984266343308e-14 ;
	setAttr ".s" -type "double3" 1 1.0000000000000004 1.0000000000000002 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -176.16841981064894 -5.020973743437092 -74.680808109978116 ;
	setAttr ".bps" -type "matrix" 0.26318233227390242 -0.96076804626362822 0.087520404807337179 0
		 -0.96076804626362866 -0.26924624012151466 -0.066567435426032009 0 0.087520404807337415 -0.06656743542603169 -0.99393609215238876 0
		 -1.88028 24.648800000000005 4.9112199999999984 1;
	setAttr ".radi" 0.5;
createNode joint -n "R_eyeLidUpOut_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "AE355AFA-4452-D4A0-B3EF-499EDEA34B94";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -5.12137 25.1728 4.1653399999999987 ;
	setAttr ".r" -type "double3" -1.9878466759146905e-15 4.3732626870123355e-14 1.9083328088781101e-14 ;
	setAttr ".s" -type "double3" 1 1.0000000000000004 1 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 169.24967232318684 -1.5113091818521949 50.285248644488625 ;
	setAttr ".bps" -type "matrix" 0.63874361419188519 0.76896748539874105 0.026374262617655755 0
		 0.75259103726650811 -0.63153592205021714 0.18646476821722816 0 0.16004163819209677 -0.099254146309963598 -0.98210757480281696 0
		 -5.1213699999999998 25.172799999999999 4.1653399999999987 1;
	setAttr ".radi" 0.5;
createNode joint -n "R_eyeLidLowOut_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "761BE200-4B9D-6EDF-EF16-DC820B2439A9";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -5.25453 22.6153 3.88419 ;
	setAttr ".r" -type "double3" -8.3489560388417303e-15 1.1330726052713774e-14 -1.2722218725854064e-14 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 173.00643284802021 27.417591460079883 142.8772339485258 ;
	setAttr ".bps" -type "matrix" -0.70778174146570594 0.53573335075565887 -0.4604723480686842 0
		 0.55433050805175188 0.82524912201396827 0.10808133260772257 0 0.43790717542746405 -0.17875587683566879 -0.88106960122674316 0
		 -5.2545299999999999 22.615300000000001 3.8841899999999998 1;
	setAttr ".radi" 0.5;
createNode joint -n "R_eyeLidLowInn_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "F18EFBDA-4739-3956-06A6-28B741CEB85D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -3.04705 22.286 4.6612100000000014 ;
	setAttr ".r" -type "double3" -7.6774382671709954e-30 -5.0292520900641865e-14 1.7493050748049341e-14 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000002 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 16.248130561871591 165.90654276685896 10.019944701829232 ;
	setAttr ".bps" -type "matrix" -0.95510618484860133 -0.16875382102354144 -0.24350425777366652 0
		 -0.099948873761016882 0.95726978187130851 -0.27137573095242334 0 0.27889495925501057 -0.23485466272997921 -0.93116104358812923 0
		 -3.04705 22.286000000000001 4.6612100000000014 1;
	setAttr ".radi" 0.5;
createNode joint -n "L_brow_01_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "EC2C6154-4430-A56C-29BD-9CAE1E972383";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.2225185632705686 27.13810920715332 5.564382553100585 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 1.2225185632705686 27.13810920715332 5.564382553100585 1;
	setAttr ".radi" 0.5;
createNode joint -n "L_brow_02_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "D7BEE5C8-42F2-9169-74A3-169EE6718CFE";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 3.6255110245018058 27.809473066088184 4.8423030077580522 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.6255110245018058 27.809473066088184 4.8423030077580522 1;
	setAttr ".radi" 0.5;
createNode joint -n "L_brow_03_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "91D06631-4D43-E8F8-3500-35A64C318AE5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.8342890739440927 27.045372009277344 3.6995601654052734 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 5.8342890739440927 27.045372009277344 3.6995601654052734 1;
	setAttr ".radi" 0.5;
createNode joint -n "R_brow_01_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "70BFDBDF-4DBE-5B04-C109-FCB8885D189E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.2225199999999998 27.1381 5.56438 ;
	setAttr ".r" -type "double3" 0 -2.5444437451708134e-14 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 180 0 ;
	setAttr ".bps" -type "matrix" -1 0 -1.2246467991473532e-16 0 0 1 0 0 1.2246467991473532e-16 0 -1 0
		 -1.2225199999999998 27.138100000000001 5.5643799999999999 1;
	setAttr ".radi" 0.5;
createNode joint -n "R_brow_02_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "3B9E0074-452F-EA27-D2FB-9FACBA7E0188";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -3.6255100000000002 27.8095 4.8423 ;
	setAttr ".r" -type "double3" 0 -2.5444437451708134e-14 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 180 0 ;
	setAttr ".bps" -type "matrix" -1 0 -1.2246467991473532e-16 0 0 1 0 0 1.2246467991473532e-16 0 -1 0
		 -3.6255100000000002 27.8095 4.8422999999999998 1;
	setAttr ".radi" 0.5;
createNode joint -n "R_brow_03_jnt" -p "BodyRM_Joints_Grp";
	rename -uid "DA4ECC15-41C5-167D-B381-318C44E2C0A7";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -5.83429 27.045400000000008 3.6995600000000004 ;
	setAttr ".r" -type "double3" 0 -2.5444437451708134e-14 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 180 0 ;
	setAttr ".bps" -type "matrix" -1 0 -1.2246467991473532e-16 0 0 1 0 0 1.2246467991473532e-16 0 -1 0
		 -5.8342900000000002 27.045400000000008 3.6995600000000004 1;
	setAttr ".radi" 0.5;
createNode transform -n "BodyRM_Follicles_Grp" -p "BodyRM_Rig_Grp";
	rename -uid "7DFDA983-46AA-1990-DA08-06A4E668A99E";
createNode transform -n "R_up_lip_01_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "27ECA366-409A-A405-0BFF-EDA991AA091B";
	setAttr ".t" -type "double3" -1.7226123809814453 20.369339678075061 6.7005225066989844 ;
createNode locator -n "R_up_lip_01_flcShape" -p "R_up_lip_01_flc";
	rename -uid "EBABD97F-47E2-B34F-9AB9-829C3D1974EE";
	setAttr -k off ".v";
createNode transform -n "R_up_lip_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "8173B1BC-4675-B634-6259-178ED241570A";
	setAttr ".t" -type "double3" -0.93601891398429882 20.516340656167088 7.2393479347229004 ;
createNode locator -n "R_up_lip_flcShape" -p "R_up_lip_flc";
	rename -uid "177CD9A6-47FC-C201-507B-11BF40A56AD8";
	setAttr -k off ".v";
createNode transform -n "up_lip_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "85D9335F-4E97-1F24-6E7B-BD98B05ED7B8";
	setAttr ".t" -type "double3" 2.0160326509054483e-27 20.742899782057606 7.4302005767822257 ;
createNode locator -n "up_lip_flcShape" -p "up_lip_flc";
	rename -uid "9DC3FCED-4089-6256-DD0A-AB85507CDCB0";
	setAttr -k off ".v";
createNode transform -n "L_up_lip_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "17F516C5-44F3-2EC4-CE28-0A85472CACC6";
	setAttr ".t" -type "double3" 0.93601891398429882 20.516340656167088 7.2393479347229004 ;
createNode locator -n "L_up_lip_flcShape" -p "L_up_lip_flc";
	rename -uid "A3E3FC4C-4949-9B5E-F727-19B7D968CF56";
	setAttr -k off ".v";
createNode transform -n "L_up_lip_01_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "0D06D5BC-4780-51AF-EC56-BFAD828A4D34";
	setAttr ".t" -type "double3" 1.7226123809814453 20.369339678075061 6.7005225066989844 ;
createNode locator -n "L_up_lip_01_flcShape" -p "L_up_lip_01_flc";
	rename -uid "DF94E56E-470C-9A1D-136D-1AA1F2B403D8";
	setAttr -k off ".v";
createNode transform -n "R_low_lip_01_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "F2BA2FF5-48EA-D441-32AA-2797E1E7600B";
	setAttr ".t" -type "double3" -1.6951909065246582 19.988830107342835 6.3012366045154504 ;
createNode locator -n "R_low_lip_01_flcShape" -p "R_low_lip_01_flc";
	rename -uid "C03D6F64-47E9-EEDF-D479-FDBD31FFF0FB";
	setAttr -k off ".v";
createNode transform -n "R_low_lip_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "22C71D54-4418-916F-4EB4-7B8A0ACE2E6A";
	setAttr ".t" -type "double3" -0.91451603174209595 19.726325147751961 6.7682154451131353 ;
createNode locator -n "R_low_lip_flcShape" -p "R_low_lip_flc";
	rename -uid "7480C2E0-4FDB-DFBB-4610-1A97742A01B1";
	setAttr -k off ".v";
createNode transform -n "low_lip_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "DC1986FF-49EB-E1CF-AB62-BB8970B2BB82";
	setAttr ".t" -type "double3" -3.9130767231693188e-17 19.636009311401594 7.0391147409200192 ;
createNode locator -n "low_lip_flcShape" -p "low_lip_flc";
	rename -uid "D7C3A8AA-4BB8-6624-B1E5-03BC8B4FB62B";
	setAttr -k off ".v";
createNode transform -n "L_low_lip_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "CF95E398-4633-3CC4-6708-A9AC8E7F74B7";
	setAttr ".t" -type "double3" 0.91451603174209595 19.726325147751961 6.7682154451131353 ;
createNode locator -n "L_low_lip_flcShape" -p "L_low_lip_flc";
	rename -uid "6F3C17FE-4A2A-F950-5627-41B7B190DF9B";
	setAttr -k off ".v";
createNode transform -n "L_low_lip_01_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "F2F8E474-49CD-E7AD-80CB-E1AD30980D8A";
	setAttr ".t" -type "double3" 1.6951909065246582 19.988830107342835 6.3012366045154504 ;
createNode locator -n "L_low_lip_01_flcShape" -p "L_low_lip_01_flc";
	rename -uid "51513F34-43EF-1524-5F60-CCA8043DBE7C";
	setAttr -k off ".v";
createNode transform -n "L_eyeLidUpInn_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "B7B8242F-4C97-7157-9C2A-EDBE927896BD";
	setAttr ".t" -type "double3" 1.8802814461050172 24.648844157554105 4.9112160535072418 ;
createNode locator -n "L_eyeLidUpInn_flcShape" -p "L_eyeLidUpInn_flc";
	rename -uid "D42CEDE9-4890-550D-157C-3D88BA0347C9";
	setAttr -k off ".v";
createNode transform -n "L_eyeLidUpOut_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "9E9E52B0-4442-CFA2-4717-6DB183AF671D";
	setAttr ".t" -type "double3" 5.1213719731303113 25.172799690262995 4.1653392228522392 ;
createNode locator -n "L_eyeLidUpOut_flcShape" -p "L_eyeLidUpOut_flc";
	rename -uid "0C19184B-4E1B-20F4-F2DF-A58BC63FDEA6";
	setAttr -k off ".v";
createNode transform -n "L_eyeLidLowOut_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "CF907A4D-4A9E-5D7C-920D-E4A4F82A3119";
	setAttr ".t" -type "double3" 5.2545280456542978 22.615339279174805 3.8841893672943106 ;
createNode locator -n "L_eyeLidLowOut_flcShape" -p "L_eyeLidLowOut_flc";
	rename -uid "5DF685DD-4C89-2ED4-26C0-72B020DF7784";
	setAttr -k off ".v";
createNode transform -n "L_eyeLidLowInn_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "B1AB2FB4-4FFF-9D66-2DC7-E2ABD1E83374";
	setAttr ".t" -type "double3" 3.0470523834228516 22.286026000976555 4.6612105369567871 ;
createNode locator -n "L_eyeLidLowInn_flcShape" -p "L_eyeLidLowInn_flc";
	rename -uid "00C3726B-4B5A-6C0F-DF69-1C9CBE19A112";
	setAttr -k off ".v";
createNode transform -n "R_eyeLidUpInn_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "AE64B14E-4BDC-82F2-37F7-D8A6D5A7947A";
	setAttr ".t" -type "double3" -1.88028 24.648800000000005 4.9112199999999984 ;
createNode locator -n "R_eyeLidUpInn_flcShape" -p "R_eyeLidUpInn_flc";
	rename -uid "043C1C55-4AA9-FC54-5B91-0D823D92EB1F";
	setAttr -k off ".v";
createNode transform -n "R_eyeLidUpOut_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "79CA245D-4B75-8863-FD61-5998222D8523";
	setAttr ".t" -type "double3" -5.12137 25.1728 4.1653399999999987 ;
createNode locator -n "R_eyeLidUpOut_flcShape" -p "R_eyeLidUpOut_flc";
	rename -uid "1426E9BE-4E45-2C85-3443-CF85BDBD142C";
	setAttr -k off ".v";
createNode transform -n "R_eyeLidLowOut_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "483D82C6-4183-4271-6B3E-DFB8EDAF7ACC";
	setAttr ".t" -type "double3" -5.25453 22.6153 3.88419 ;
createNode locator -n "R_eyeLidLowOut_flcShape" -p "R_eyeLidLowOut_flc";
	rename -uid "D0774029-467B-5244-A3A3-61A338A9C73D";
	setAttr -k off ".v";
createNode transform -n "R_eyeLidLowInn_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "AEB6AE56-4E73-3698-7C30-72BD39F8F921";
	setAttr ".t" -type "double3" -3.04705 22.286 4.6612100000000014 ;
createNode locator -n "R_eyeLidLowInn_flcShape" -p "R_eyeLidLowInn_flc";
	rename -uid "7905875C-46F1-2BD7-3701-A0AF9AB7BEB9";
	setAttr -k off ".v";
createNode transform -n "L_brow_01_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "3DD1478B-47AD-DE3F-1BD3-60B61796B3FE";
	setAttr ".t" -type "double3" 1.2225185632705686 27.13810920715332 5.564382553100585 ;
createNode locator -n "L_brow_01_flcShape" -p "L_brow_01_flc";
	rename -uid "9C41F92B-4E77-8E14-6FC1-2D90DF7ACEB5";
	setAttr -k off ".v";
createNode transform -n "L_brow_02_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "38D9631E-4F68-D661-1363-17AFB7A6EB4B";
	setAttr ".t" -type "double3" 3.6255110245018058 27.809473066088184 4.8423030077580522 ;
createNode locator -n "L_brow_02_flcShape" -p "L_brow_02_flc";
	rename -uid "615A50AE-4484-D448-13F0-12ABAC0018FE";
	setAttr -k off ".v";
createNode transform -n "L_brow_03_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "19688C63-41FF-C3A9-9562-02982B37957F";
	setAttr ".t" -type "double3" 5.8342890739440927 27.045372009277344 3.6995601654052734 ;
createNode locator -n "L_brow_03_flcShape" -p "L_brow_03_flc";
	rename -uid "85F08F45-435C-40FB-BC1F-37ABF93AE200";
	setAttr -k off ".v";
createNode transform -n "R_brow_01_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "C41991BE-45BF-FD4F-09DC-39932B4C72DE";
	setAttr ".t" -type "double3" -1.2225199999999998 27.1381 5.56438 ;
createNode locator -n "R_brow_01_flcShape" -p "R_brow_01_flc";
	rename -uid "AF5CF3E4-446C-744B-E11F-468705C96C61";
	setAttr -k off ".v";
createNode transform -n "R_brow_02_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "2FFA82D4-414D-9960-F3B7-18B98208B58E";
	setAttr ".t" -type "double3" -3.6255100000000002 27.8095 4.8423 ;
createNode locator -n "R_brow_02_flcShape" -p "R_brow_02_flc";
	rename -uid "91C06D28-41E4-842C-FD3C-ED85D26BD219";
	setAttr -k off ".v";
createNode transform -n "R_brow_03_flc" -p "BodyRM_Follicles_Grp";
	rename -uid "1C0AAAF0-40DB-5D51-BB56-1DB03DCC965D";
	setAttr ".t" -type "double3" -5.83429 27.045400000000008 3.6995600000000004 ;
createNode locator -n "R_brow_03_flcShape" -p "R_brow_03_flc";
	rename -uid "5E947C5D-4DF4-F500-C466-808348D648B2";
	setAttr -k off ".v";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "8E54D8EB-4FBD-4FA0-5290-6BA90FE11108";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode VRaySettingsNode -s -n "vraySettings";
	rename -uid "06629C8E-433A-382D-499C-3985F15FC873";
	setAttr ".gi" yes;
	setAttr ".rfc" yes;
	setAttr ".pe" 2;
	setAttr ".se" 3;
	setAttr ".cmph" 60;
	setAttr ".cfile" -type "string" "";
	setAttr ".cfile2" -type "string" "";
	setAttr ".casf" -type "string" "";
	setAttr ".casf2" -type "string" "";
	setAttr ".st" 3;
	setAttr ".msr" 6;
	setAttr ".aaft" 3;
	setAttr ".aafs" 2;
	setAttr ".dma" 24;
	setAttr ".dam" 1;
	setAttr ".pt" 0.0099999997764825821;
	setAttr ".pmt" 0;
	setAttr ".sd" 1000;
	setAttr ".ss" 0.01;
	setAttr ".pfts" 20;
	setAttr ".ufg" yes;
	setAttr ".fnm" -type "string" "";
	setAttr ".lcfnm" -type "string" "";
	setAttr ".asf" -type "string" "";
	setAttr ".lcasf" -type "string" "";
	setAttr ".urtrshd" yes;
	setAttr ".rtrshd" 2;
	setAttr ".lightCacheType" 1;
	setAttr ".ifile" -type "string" "";
	setAttr ".ifile2" -type "string" "";
	setAttr ".iasf" -type "string" "";
	setAttr ".iasf2" -type "string" "";
	setAttr ".pmfile" -type "string" "";
	setAttr ".pmfile2" -type "string" "";
	setAttr ".pmasf" -type "string" "";
	setAttr ".pmasf2" -type "string" "";
	setAttr ".dmcstd" yes;
	setAttr ".dmculs" no;
	setAttr ".dmcsat" 0.004999999888241291;
	setAttr ".cmtp" 6;
	setAttr ".cmao" 2;
	setAttr ".cg" 2.2000000476837158;
	setAttr ".mtah" yes;
	setAttr ".rgbcs" -1;
	setAttr ".suv" 0;
	setAttr ".srflc" 1;
	setAttr ".srdml" 0;
	setAttr ".seu" yes;
	setAttr ".gormio" yes;
	setAttr ".gocle" yes;
	setAttr ".gopl" 2;
	setAttr ".wi" 960;
	setAttr ".he" 540;
	setAttr ".aspr" 1.7777780294418335;
	setAttr ".productionGPUResizeTextures" 0;
	setAttr ".autolt" 0;
	setAttr ".jpegq" 100;
	setAttr ".vfbOn" yes;
	setAttr ".vfbSA" -type "Int32Array" 847 124 20 0 0 0 0
		 0 0 0 0 -1073724351 0 0 0 0 0 0 0
		 0 0 0 -1074790400 0 -1074790400 0 -1074790400 0 -1074790400 1 0
		 0 0 3253 1 3241 0 1 3233 1700143739 1869181810 825893486 1632379436
		 1936876921 578501154 1936876886 577662825 573321530 1935764579 574235251 1953460082 1881287714 1701867378 1701409906 2067407475
		 1919252002 1852795251 741423650 1835101730 574235237 1696738338 1818386798 1949966949 744846706 1886938402 577007201 1818322490
		 573334899 1634760805 1650549870 975332716 1702195828 1931619453 1814913653 1919252833 1530536563 1818436219 577991521 1751327290
		 779317089 1886611812 1132028268 1701999215 1869182051 573317742 1886351984 1769239141 975336293 1702240891 1869181810 825893486
		 1634607660 975332717 1936278562 2036427888 1919894304 1952671090 577662825 1852121644 1701601889 1920219682 573334901 1634760805
		 975332462 1702195828 2019893804 1684955504 1701601889 1920219682 573334901 1718579824 577072233 573321530 1869641829 1701999987
		 774912546 1763847216 1717527395 577072233 740434490 1667459362 1852142175 1953392996 578055781 573321274 1886088290 1852793716
		 1715085942 1702063201 1668227628 1717530473 577072233 740434490 1768124194 1868783471 1936879468 1701011824 741358114 1768124194
		 1768185711 1634496627 1986356345 577069929 573321274 1869177711 1701410399 1634890871 1868985198 975334770 1864510512 1601136995
		 1702257011 1835626089 577070945 1818322490 746415475 1651864354 2036427821 577991269 578509626 1935764579 574235251 1868654691
		 1701981811 1869819494 1701016181 1684828006 740455013 1869770786 1953654128 577987945 1981971258 1769173605 975335023 1847733297
		 577072481 1867719226 1701016181 1196564538 573317698 1650552421 975332716 1702195828 2019893804 1684955504 1634089506 744846188
		 1886938402 1633971809 577072226 1818322490 573334899 1667330159 578385001 808333626 1818370604 1600417381 1701080941 741358114
		 1668444962 1887007839 809116261 1931619453 1814913653 1919252833 1530536563 1818436219 577991521 1751327290 779317089 778462578
		 1751607660 2020175220 1881287714 1701867378 1701409906 2067407475 1919252002 1852795251 741423650 1835101730 574235237 1751607628
		 2020167028 1696738338 1818386798 1715085925 1702063201 2019893804 1684955504 1634089506 744846188 1886938402 1633971809 577072226
		 1970435130 573341029 761427315 1702453612 975336306 746413403 1818436219 577991521 1751327290 779317089 778462578 1886220131
		 1953067887 573317733 1886351984 1769239141 975336293 1702240891 1869181810 825893486 1634607660 975332717 1836008226 1769172848
		 740451700 1634624802 577072226 1818322490 573334899 1634760805 975332462 1936482662 1696738405 1851879544 1818386788 1949966949
		 744846706 1634758434 2037672291 774978082 1646406704 1684956524 1685024095 809116261 1931619453 1814913653 1919252833 1530536563
		 2103278941 1663204140 1936941420 1663187490 1936679272 778399790 1869505892 1919251305 1881287714 1701867378 1701409906 2067407475
		 1919252002 1852795251 741423650 1835101730 574235237 1869505860 1919251305 1853169722 1767994977 1818386796 573317733 1650552421
		 975332716 1936482662 1696738405 1851879544 1715085924 1702063201 2019893804 1684955504 1701601889 1920219682 573334901 1667330159
		 578385001 808333626 1818370604 1600417381 1701080941 741358114 1952669986 577074793 1818322490 573334899 1936028272 975336549
		 1931619378 1852142196 577270887 808333626 1634869804 1937074532 808532514 573321262 1665234792 1701602659 1702125938 1920219682
		 573334901 1869505892 1919251305 1685024095 825893477 1931619453 1814913653 1919252833 1530536563 2066513245 1634493218 975336307
		 1634231074 1882092399 1701588581 2019980142 1881287714 1701867378 1701409906 2067407475 1919252002 1852795251 741423650 1835101730
		 574235237 1936614732 1717978400 1937007461 1696738338 1818386798 1949966949 744846706 1886938402 577007201 1818322490 573334899
		 1634760805 1650549870 975332716 1702195828 1886331436 1953063777 825893497 573321262 1852140642 1869438820 975332708 1730292786
		 1701994860 577662815 1818322490 573334899 1918987367 1769168741 975332730 808333363 1818698284 1600483937 1734960503 975336552
		 741355057 1869373986 2002742639 1751607653 809116276 808465454 808464432 909718832 1818698284 1600483937 1701996660 1819240563
		 825893476 573321262 1953261926 1918857829 1952543855 577662825 808333370 1634935340 1634891124 1852795252 774978082 1747070000
		 2003071585 1600483937 1701012321 1634887020 577004916 1970435130 1663183973 1600416879 1836212599 1634089506 744846188 1953392930
		 1667330661 1702259060 1920219682 573334901 1650552421 1650419052 1701077356 1949966963 744846706 1684632354 975336293 1646406710
		 1701077356 1869766515 1769234804 975335023 808334641 1953702444 1801545074 1970037343 809116274 808464942 808464432 943272496
		 1937056300 1919377253 1852404833 1715085927 1702063201 1919361580 1852404833 1701076839 1953067886 893002361 741355056 1634887458
		 1735289204 1852140639 577270887 774910266 1730292784 1769234802 2053072750 577597295 808334650 1919361580 1852404833 1819500391
		 577073263 808333370 1919361580 1852404833 1953718119 1735288178 975333492 741355057 1702065442 1667460959 1769174380 975335023
		 1936482662 1864510565 1970037603 1852795251 1836675935 1920230765 975332201 1702195828 1668227628 1937075299 1601073001 1668441456
		 578055781 774910522 1864510512 1970037603 1852795251 1953460831 1869182049 809116270 573321262 1818452847 1869181813 1918984046
		 825893475 808333360 1937056300 1668505445 1668571506 1715085928 1702063201 1668489772 2037604210 1952804205 576940402 1970435130
		 1931619429 1885303395 1702130785 975335026 1931619376 1834971747 1769237621 1918987367 1868783461 578055797 573321530 1601332083
		 1936614756 578385001 774911290 1931619376 1818194531 1952935525 893002344 741355056 1919120162 1869378399 1985963376 1634300513
		 577069934 808333370 1668489772 1769430898 1600681060 1769103734 1701015137 774912546 1931619376 1935635043 577004901 573321274
		 1601332083 1836019578 775043618 1931619376 1918857827 1952543855 577662825 808333370 1668489772 1953718130 1735288178 975333492
		 741355057 1702065442 1937073247 1715085940 1702063201 1969496620 1885303923 1702130785 975335026 1679961136 1601467253 1936614756
		 578385001 774911290 1679961136 1601467253 1768186226 1985966965 1634300513 577069934 808333370 1969496620 1784640627 1702130793
		 809116274 573321262 1953723748 1869576799 842670701 573321262 1953723748 1953460831 1869182049 809116270 573321262 1953723748
		 1920234335 1952935525 825893480 573321262 1918987367 1937071973 1651466085 1667331187 1767859564 1701273965 1634089506 744846188
		 1634494242 1868522866 1635021666 1600482403 1734438249 1634754405 975333492 573317666 1953718895 1634560351 2053072231 577597295
		 808333626 1651450412 1767863411 1701273965 1953460831 1869182049 809116270 573321262 1953718895 1634560351 1935631719 1852142196
		 577270887 808333626 1937056300 1768316773 1919251564 1634560351 975332711 1936482662 1730292837 1701994860 1634560351 1885300071
		 577270881 2099388986 1970479660 1634479458 1936876921 1566259746 1568497021 1377971325 1869178725 1852131182 578501154 1936876918
		 577662825 573321530 1937076077 1868980069 2003790956 1634624863 1684368482 1634089506 744846188 1970236706 1717527923 1869376623
		 1869635447 1601465961 1801678700 975332453 1936482662 1830956133 1702065519 1819240031 1601662828 1852403568 578314100 573321274
		 1937076077 1868980069 2003790956 1768910943 2036298862 2100312610 1699881516 1919247470 2003134806 578501154 1936876918 577662825
		 573321530 1650552421 1918854508 1701080677 1701994354 1852795239 1634089506 744846188 1852142114 1601332580 1768383858 2019520111
		 758784560 741355057 1852142114 1601332580 1768383858 2036297327 758784560 741355057 1852142114 1601332580 1768383858 2019520111
		 758784561 741355057 1852142114 1601332580 1768383858 2036297327 758784561 741355057 1701410338 1701994359 1949966948 744846706
		 1701410338 1919377271 577660261 1970435130 1981951077 1601660265 1702194274 1920219682 573334901 2003134838 1852796255 1715085935
		 1702063201 1868767788 1601335148 1835101283 1869438832 975332708 573323574 1869377379 1818451826 1601203553 975335023 1702195828
		 1768956460 1600939384 1868983913 1668246623 577004907 1818322490 573334899 1702390128 1852399468 1667198822 1701999215 1684370531
		 1819239263 577991279 1818322490 746415475 1952661794 1936617321 578501154 1936876918 577662825 573321530 1937072496 1380993381
		 1701339999 1684368227 1634089506 744846188 1702065442 1702390096 1886601580 1601463141 1667590243 577004907 1818322490 573334899
		 1953719668 1601398098 1768186226 893002351 1948396593 1383363429 1667199845 1801676136 975332453 1936482662 1679961189 1735746149
		 1684105299 1600613993 1768186226 893002351 1679961142 1735746149 1684105299 1600613993 1667590243 577004907 1818322490 573334899
		 1919251571 1867345765 1918854500 1869177953 943143458 1953702444 1868919397 1701080909 1701339999 1684368227 1634089506 2103800684
		 125 ;
	setAttr ".mSceneName" -type "string" "C:/Users/s.petracca/Desktop/update script/RBW_face_tweaks_level.ma";
	setAttr ".rt_cpuRayBundleSize" 4;
	setAttr ".rt_gpuRayBundleSize" 128;
	setAttr ".rt_maxPaths" 10000;
	setAttr ".rt_engineType" 3;
	setAttr ".rt_gpuResizeTextures" 0;
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "D67F0F51-48AD-AD97-E578-0EB8744695C9";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "A9526798-4C64-2C12-A241-B6BED4189876";
createNode displayLayerManager -n "layerManager";
	rename -uid "8A7EDD5C-4C24-148C-E676-71B64F76F28C";
createNode displayLayer -n "defaultLayer";
	rename -uid "1E75B442-436D-7CE6-DFE5-89AD32D55009";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "DB7752FF-4F50-0F0F-F18D-D89E650B5F94";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "DA6AFC51-4EF4-5649-5D3C-A6BB528DCAE9";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "8ED125B8-40ED-BB7E-6582-9283DA2B1BC4";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n"
		+ "            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1955\n            -height 1066\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n"
		+ "            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n"
		+ "                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n"
		+ "            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n"
		+ "                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n"
		+ "                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n"
		+ "\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n"
		+ "                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n"
		+ "                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n{ string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n"
		+ "                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 32768\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n"
		+ "                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n"
		+ "                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName; };\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n"
		+ "\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1955\\n    -height 1066\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1955\\n    -height 1066\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "6808EC1C-4DA1-C278-222F-0588529CE2F1";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of RBW_face_tweaks_level.ma
