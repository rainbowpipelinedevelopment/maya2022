import sys
import maya.OpenMaya as OpenMaya
import maya.OpenMayaMPx as OpenMayaMPx
# import maya.OpenMayaRender as OpenMayaRender
# import maya.OpenMayaUI as OpenMayaUI

nodeTypeName = "saveNode"
nodeTypeId = OpenMaya.MTypeId(0x00112E01)


class saveNode(OpenMayaMPx.MPxNode):
    def __init__(self):
        OpenMayaMPx.MPxNode.__init__(self)


def nodeCreator():
    return OpenMayaMPx.asMPxPtr(saveNode())


def nodeInitializer():
    return True


def initializePlugin(obj):
    plugin = OpenMayaMPx.MFnPlugin(obj, "Rainbow CGI", "Any", "Any")
    try:
        plugin.registerNode(nodeTypeName, nodeTypeId, nodeCreator, nodeInitializer)
    except:
        sys.stderr.write("Failed to register node: {}".format(nodeTypeName))


def uninitializePlugin(obj):
    plugin = OpenMayaMPx.MFnPlugin(obj)
    try:
        plugin.deregisterNode(nodeTypeId)
    except:
        sys.stderr.write("Failed to deregister node: {}".format(nodeTypeName))
