class Ordered_dict(dict):

    def __init__(self, *args, **kwargs):
        dict.__init__(self, *args, **kwargs)
        self._order = list(self.keys())

    def __setitem__(self, key, value):
        dict.__setitem__(self, key, value)
        if key in self._order:
            self._order.remove(key)
        self._order.append(key)

    def __delitem__(self, key):
        dict.__delitem__(self, key)
        self._order.remove(key)

    def order(self):
        return self._order[:]

    def ordered_items(self):
        return [(key, self[key]) for key in self._order]


PIPENAME = "arakno"

mc_depts = {
    "Modeling": ["hig", "prx", "bs", "gbs", "utls"],

    "Surfacing": ["rnd", "uvs", "txt"],

    "Grooming": ["hrs", "rnd"],

    "Rigging": ["hig", "crowd", "prx", "lay", "fac", "ass"],

    "Set": ["hig"],

    "Fx": ["source", "ani", "rnd"]
}

sc_depts = {"Lay": [], "Pri": [], "Sec": [], "Cam": [], "Fin": ["msh", "vrscene", "set", "bgasset", "all"], "Fx": [], "Env": [], "Lgt": []}
sc_labels = {'Animation', 'Finishing', 'Fx', 'Lighting'}

finishingFolders = {'vrscene': '02_vrscene', 'msh': '03_msh', 'set': '04_set', 'all': ''}

mcLabels = ["category", "group", "name", "variant", "type", "approvation"]
mcInfoLabels = ["note", "user", "date", "version", "dept", "db_id", "assett_id"]

commonLabels = ["id_mv", "project", "share", "parent"]

saveLabels = mcLabels + mcInfoLabels + commonLabels

eye_locators = ['LOC_eyePivot_L', 'LOC_eyePivot_R']

riggingSets = ["ALL_CONTROLS", "All_Rig_Driver_Meshes_RM", "LOC_TRACKER"]

rigHeadNames1 = ['JNT_root', 'SKN_skullBase', 'head_Jnt']
rigHeadNames2 = ['Head_a_jnt', 'RIG_head_jnt', 'RIG_head_a_jnt', 'Jnt_head', 'Head_jnt']
rigHeadNames3 = ['head_a_jnt']
headNames = rigHeadNames1 + rigHeadNames2 + rigHeadNames3

deptsColors = {
    'modeling': 'rgba(209, 123, 80, 150)',
    'surfacing': 'rgba(209, 150, 80, 150)',
    'grooming': 'rgba(209, 186, 80, 150)',
    'rigging': 'rgba(155, 206, 130, 150)',
    'setdressing': 'rgba(72, 178, 124, 150)',
    'fx': 'rgba(80, 177, 209, 150)',
    'animation': 'rgba(164, 129, 221, 150)',
    'finishing': 'rgba(193, 124, 218, 150)',
    'lighting': 'rgba(221, 128, 218, 150)'
}

lightsType = [
    'VRayLightRectShape',
    'VRayLightDomeShape',
    'VRayLightSphereShape',
    'directionalLight',
    'ambientLight',
    'spotLight',
    'VRayLightMeshLightLinking'
]
