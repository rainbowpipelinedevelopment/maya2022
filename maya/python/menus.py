import importlib
import functools
import os

import maya.cmds as cmd
import maya.mel as mel

import config.dictionaries as dictionary
import utilz.shelfsInspector as shelfsInspector
import api.log
import common.deptInterface as deptInt
import utilz.utilities as utilities

importlib.reload(dictionary)
importlib.reload(shelfsInspector)
importlib.reload(api.log)
importlib.reload(deptInt)
importlib.reload(utilities)

projectName = os.getenv('project')


################################################################################
# @brief      set project to mc folder
#
# @param      args  The arguments
#
def run_setProject(*args):
    mcFolder = os.getenv('MC_FOLDER')
    cmd.workspace(mcFolder, openWorkspace=True)


################################################################################
# @brief      toggle shelf visibility.
#
# @param      args  The arguments
#
# @return     { description_of_the_return_value }
#
def toggleShelf(*args):
    import api.shelf
    importlib.reload(api.shelf)

    shelf_inspector = shelfsInspector.shelfsInspector('shelfs', 'python')
    if args[1]:
        for shelfDict in shelf_inspector.buttonPackages:
            if shelfDict['name'] == args[0]:
                shelfName = shelfDict['name']
                shelf = api.shelf.createShelf(shelfName)
                for shelfButton in shelfDict['buttons']:
                    api.shelf.addButton(
                        label=shelfButton['label'],
                        command=shelfButton['launch'],
                        image=shelfButton['icon'],
                        shelfLayout=shelf
                    )

        return
    else:
        api.shelf.deleteShelf(args[0])


################################################################################
# @brief      create menu in maya.
#
# @param      mainEnv  The main environment
#
def Create(mainEnv):

    gMainWindow = mel.eval('$temp1=$gMainWindow')

    if ".git" in os.getenv("LOCALPIPE"):
        label = "DEV"
    else:
        label = "USER"

    if projectName:
        label = "{} {}".format(label, projectName)

    menuLabel = "| RBW_{} |".format(label)

    RBWMenu = cmd.menu("RBWM", parent=gMainWindow, tearOff=True, label=menuLabel)

    mc_depts = dictionary.mc_depts
    sc_depts = dictionary.sc_labels

    cmd.menuItem(divider=True, label="MAIN CONTENT", parent=RBWMenu)
    for dept in mc_depts:
        if dept == 'Set':
            dept_pack = 'setdressing'
        else:
            dept_pack = dept.lower().replace(" ", "")
        cmd.menuItem(
            label=dept,
            c=functools.partial(deptInt.DeptWindow.showInterface, dept_pack, dept_pack, None),
            # c=Callback(deptInt.DeptWindow.showInterface, dept, dept_pack, None, utilz.getMayaWindow()),
            # c=functools.partial(testCommand, dept),
            parent=RBWMenu
        )

    cmd.menuItem(divider=True, label="SHOT CONTENT", parent=RBWMenu)
    for dept in sc_depts:
        dept_pack = dept.lower().replace(" ", "")
        cmd.menuItem(
            label=dept,
            # c=Callback(deptInt.DeptWindow.showInterface, dept, dept_pack, None, utilz.getMayaWindow()),
            c=functools.partial(deptInt.DeptWindow.showInterface, dept_pack, dept_pack, None),
            parent=RBWMenu
        )

    # shot content utility per animazione
    cmd.menuItem(divider=True, label="SHOT CONTENT UTILITY", parent=RBWMenu)
    cmd.menuItem(l="Set MC content", c=run_setProject, parent=RBWMenu)

    # generic utility
    cmd.menuItem(divider=True, label="UTILITY", parent=RBWMenu)
    # commonM = cmd.menuItem(parent=RBWMenu, subMenu=True, tearOff=True, label="Common_Tools")
    # commontoolsM.createSubCommonTools(commonM)

    # carico l'unica shelf utile con tutte le pulsantiere dei reparti
    shelfMenu = cmd.menuItem(subMenu=True, tearOff=True, label='Shelf', parent=RBWMenu)

    shelfs = []
    shelf_inspector = shelfsInspector.shelfsInspector('shelfs', 'python')
    for shelfDict in shelf_inspector.buttonPackages:
        shelfs.append(shelfDict['name'])

    for shelfName in shelfs:
        cmd.menuItem(label=shelfName, parent=shelfMenu, checkBox=True, command=functools.partial(toggleShelf, shelfName))
