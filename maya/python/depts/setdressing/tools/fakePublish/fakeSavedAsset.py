import importlib
import os
import re
import shutil

import maya.cmds as cmd
import maya.mel as mel

import api.log
import api.scene
import api.savedAsset
import api.widgets.rbw_UI as rbwUI
import common.checkUpdate.checkUpdate as chk
import common.switchAssembly as switchAssembly
import depts.modeling.tools.finalize.finalize as fm

importlib.reload(api.log)
importlib.reload(api.scene)
# importlib.reload(api.savedAsset)
# importlib.reload(rbwUI)
importlib.reload(chk)
importlib.reload(switchAssembly)
importlib.reload(fm)


################################################################################
# @brief      { function_description }
#
# @param      dept  The dept
#
def fakePublish(dept='modeling'):
    api.log.logger().debug('The fake modeling publish procedure has started... ')
    sceneFileName = api.scene.scenename(relative=True)
    exportTempFilePath = os.path.join(os.path.dirname(sceneFileName), 'export_temp_fake{}Publish.mb'.format(dept.capitalize())).replace('\\', '/')

    setSavedAsset = api.savedAsset.SavedAsset(params={'path': sceneFileName})

    # isolate the first assembly reference node
    assemblyList = cmd.ls(type='assemblyReference')
    if len(assemblyList) > 1:
        cmd.parent(assemblyList[1:], world=True)
    cmd.select('SET', replace=True)
    cmd.file(exportTempFilePath, exportSelected=True, type='mayaBinary', preserveReferences=True)
    cmd.file(new=True, force=True)
    cmd.file(exportTempFilePath, open=True, force=True)

    chk.updateAllAssembliesBatch()

    # import assembly and clean file
    assemblies = cmd.ls(type='assemblyReference')
    cmd.select(assemblies, replace=True)

    if dept == 'modeling':
        switchTo = 'cache base'
    else:
        switchTo = 'surfacing'
    switchAssembly.getRepresentationInScene(representationName=switchTo, onlyImport=False, deleteSource=True, world=False, reference=False, approvation='def')

    if cmd.objExists('SET'):
        cmd.delete('SET')

    # extract the meshes, group them and delete the remaining nodes
    meshShapeList = cmd.ls(type='mesh')
    meshList = cmd.listRelatives(meshShapeList, parent=True, fullPath=True)
    topGroupName = 'GRP_{}'.format(setSavedAsset.asset.name)
    topGroup = cmd.group(empty=True, name=topGroupName)
    cmd.parent(meshList, topGroup)

    controlSetList = cmd.ls('CTRL_set*')
    if controlSetList:
        cmd.delete(controlSetList)

    # # modeling finalize
    cmd.select(topGroup, replace=True)
    fm.finalize(character=False)

    saveNodes = cmd.ls('*saveNode')
    if saveNodes:
        cmd.delete(saveNodes)

    # # save
    wipSavedAssetParams = {
        'asset': setSavedAsset.getAsset(),
        'dept': dept,
        'approvation': 'wip',
        'note': 'Pipeline autosave before def save',
        'selection': False,
    }

    if dept == 'modeling':
        wipSavedAssetParams['sgUploadPicture'] = False
        wipSavedAssetParams['deptType'] = 'hig'
    elif dept == 'surfacing':
        wipSavedAssetParams['deptType'] = 'rnd'
        wipSavedAssetParams['vrscene'] = False
        wipSavedAssetParams['exportElements'] = False

    wipSavedAsset = api.savedAsset.SavedAsset(params=wipSavedAssetParams)

    wipSuccess = wipSavedAsset.save()
    if wipSuccess:
        defSavedAssetParams = wipSavedAssetParams.copy()
        defSavedAssetParams['approvation'] = 'def'
        defSavedAssetParams['note'] = ''
        defSavedAssetParams['parent'] = wipSavedAsset.db_id

        defSavedAsset = api.savedAsset.SavedAsset(params=defSavedAssetParams)
        success = defSavedAsset.save()
        if success:
            newNote = 'Pipeline autosave before def save {}'.format(defSavedAsset.version)
            updateQuery = "UPDATE `savedAsset_wip` SET `parent`='{}', `note`='{}' WHERE `id`='{}'".format(defSavedAsset.db_id, newNote, wipSavedAsset.db_id)
            api.database.deleteQuery(updateQuery)

    cmd.file(new=True, force=True)

    # delete the temporary file used for the export
    if os.path.exists(exportTempFilePath):
        os.remove(exportTempFilePath)
        api.log.logger().debug('removing: {}'.format(exportTempFilePath))

    setSavedAsset.load()


################################################################################
# @brief      overwrite the .abc file for modeling save def
#
def overwriteAlembicFile():
    api.log.logger().debug('The alembic overwriting procedure has started...')
    sceneFileName = api.scene.scenename(relative=True)
    setSavedAsset = api.savedAsset.SavedAsset(params={'path': sceneFileName})

    modelingSaveAssetParams = {
        'asset': setSavedAsset.getAsset(),
        'dept': 'modeling',
        'deptType': 'hig',
        'approvation': 'def'
    }

    modelingSaveAsset = api.savedAsset.SavedAsset(params=modelingSaveAssetParams)
    latestModeling = modelingSaveAsset.getLatest()
    if latestModeling:
        latestModelingPath = latestModeling[2]

        alembicFullPath = os.path.join(os.getenv('MC_FOLDER'), latestModelingPath.replace('.mb', '_base.abc')).replace('\\', '/')

        chk.updateAllAssembliesBatch()

        # import assembly and clean file
        assemblies = cmd.ls(type='assemblyReference')
        visibleAssemblies = cmd.ls(type='assemblyReference', visible=True)

        topGroupName = 'GRP_setGeo'
        topGroup = cmd.group(empty=True, name=topGroupName)

        cmd.select(clear=True)
        for assembly in assemblies:
            if assembly in visibleAssemblies:
                cmd.select(assembly, replace=True)
                newItem = switchAssembly.getRepresentationInScene(onlyImport=False, deleteSource=True, world=False, reference=False, approvation='def')
                cmd.parent(newItem, topGroup)
            else:
                cmd.delete(assembly)

        if cmd.objExists('SET'):
            cmd.delete('SET')

        # export the GPU cache
        cmd.displaySmoothness(du=0, dv=0, pw=4, ps=1, po=1)

        alembicDirPath = os.path.dirname(alembicFullPath)
        alembicFileNoExtension = os.path.basename(alembicFullPath)[:-4]

        gpuBase = cmd.gpuCache(
            topGroupName,
            startTime=1,
            endTime=1,
            optimize=True,
            optimizationThreshold=512,
            writeMaterials=True,
            dataFormat='ogawa',
            useBaseTessellation=True,
            directory=alembicDirPath,
            f=alembicFileNoExtension
        )

        cmd.file(new=True, force=True)

        # replace the smooth alembic with the base one (the smooth will be too heavy to use anyway)
        alembicSmoothPath = alembicFullPath.replace('_base.abc', '_smooth.abc')

        if os.path.exists(alembicSmoothPath):
            shutil.copyfile(alembicFullPath, alembicSmoothPath)

        # open the original set
        cmd.file(sceneFileName, open=True, force=True)
        api.log.logger().debug('The alembic overwriting procedure has ended!')

    else:
        rbwUI.RBWError(title="ATTENTION", text='No Modeling save found for this asset.')
        return


################################################################################
# @brief      overwrite the vrmesh and verscene files
#
def overwriteVrFile():
    api.log.logger().debug('The Vrmesh and VrScene overwriting procedure has started...')
    sceneFileName = api.scene.scenename(relative=True)
    setSavedAsset = api.savedAsset.SavedAsset(params={'path': sceneFileName})

    surfacingSaveAssetParams = {
        'asset': setSavedAsset.getAsset(),
        'dept': 'surfacing',
        'deptType': 'rnd',
        'approvation': 'def'
    }

    surfacingSaveAsset = api.savedAsset.SavedAsset(params=surfacingSaveAssetParams)
    latestSurfacing = surfacingSaveAsset.getLatest()
    if latestSurfacing:
        latestSurfacingPath = latestSurfacing[2]

        chk.updateAllAssembliesBatch()

        assemblyList = cmd.ls(type='assemblyReference')
        singleAssList = []

        for assembly in assemblyList:
            parents = cmd.listRelatives(assembly, allParents=True, fullPath=True)
            hidden = True if not cmd.getAttr('{}.visibility'.format(assembly)) else [True for x in parents if not cmd.getAttr('{}.visibility'.format(x))]
            if not hidden:
                assNameTokens = assembly.split('_')
                assetVariant = re.sub('\d', '', assNameTokens[3])
                assemblyName = '{}_{}_{}_{}'.format(assNameTokens[0], assNameTokens[1], assNameTokens[2], assetVariant)

                if assemblyName not in singleAssList:
                    singleAssList.append(assemblyName)

        # store the infos about the instances to create
        instanceDictData = {}

        for singleAss in singleAssList:
            itemsToBeInstanced = cmd.ls('{}*'.format(singleAss), type='assemblyReference')
            assemblyToGetRnd = cmd.ls('{}*'.format(singleAss), type='assemblyReference')[0]
            cmd.select(assemblyToGetRnd, replace=True)
            itemInstancer = switchAssembly.getRepresentationInScene(representationName='vrmesh', onlyImport=False, deleteSource=False, world=False, reference=True, approvation='def')[0]
            cmd.setAttr('{}.visibility'.format(itemInstancer), False)
            cmd.xform(itemInstancer, translation=(0, 0, 0), rotation=(0, 0, 0), scale=(1, 1, 1))
            cmd.select(clear=True)
            instanceDictData[itemInstancer] = itemsToBeInstanced

        # create the instances
        for key in instanceDictData:
            createParticleInstances(key, instanceDictData[key])

        # overwrite the rnd .mb file with the one with particles and instances
        latestSurfacingMbPath = os.path.join(os.getenv('MC_FOLDER'), latestSurfacingPath).replace('\\', '/')
        cmd.delete('SET')
        cmd.file(latestSurfacingMbPath, type='mayaBinary', exportAll=True, force=True, preserveReferences=True)

        # export the vrscene inside set folder
        vrscenefilename = api.vray.createVRayFile()
        cmd.file(new=True, force=True)
        api.vray.addVrscene(os.path.normpath(vrscenefilename))
        basename, ext = os.path.splitext(vrscenefilename)
        vrscenefilename = api.export.export_ma(basename + '_vrscene.ma')
        cmd.file(new=True, force=True)

        # copy the vrscene file from set folder to surfacing folder
        setVrsceneMaPath = vrscenefilename.replace('\\', '/')
        surfVrsceneMaPath = latestSurfacingMbPath.replace('.mb', '_vrscene.ma')

        setVrscenePath = setVrsceneMaPath.replace('_vrscene.ma', '.vrscene')
        surfVrscenePath = surfVrsceneMaPath.replace('_vrscene.ma', '.vrscene')

        if os.path.isfile(setVrsceneMaPath) and os.path.isfile(setVrscenePath):
            if os.path.isfile(surfVrsceneMaPath) and os.path.isfile(surfVrscenePath):
                shutil.copyfile(setVrscenePath, surfVrscenePath)
            else:
                api.log.logger().debug('No surfacing vrscene files found at the following paths, please check:\n{}\n{}'.format(surfVrsceneMaPath, surfVrscenePath))

            # delete the files created inside the set folder
            os.remove(setVrsceneMaPath)
            os.remove(setVrscenePath)
        else:
            api.log.logger().debug('No set vrscene files found at the following paths, please check:\n{}\n{}'.format(setVrsceneMaPath, setVrscenePath))

        # open the original set
        filePath = os.path.join(os.getenv('MC_FOLDER'), sceneFileName).replace('\\', '/')
        cmd.file(filePath, open=True, force=True)
        api.log.logger().debug('The overwrite vrscene procedure has ended!')
    else:
        api.log.logger().debug('No surfacing folder found at the following path: {}'.format(latestSurfacingPath))


################################################################################
# Creates the instances using particles system.
#
# @param      itemToInstance  The instancer item.
# @param      itemsList       The items to be switched with the instances.
#
def createParticleInstances(itemToInstance, itemsList):
    # create particles and per particles attributes needed
    particleSetShape = cmd.createNode('nParticle', name='temp_temp_nPRT_setShape')
    cmd.addAttr(particleSetShape, longName='radiusPP', dataType='doubleArray')
    cmd.addAttr(particleSetShape, longName='radiusPP0', dataType='doubleArray')
    cmd.addAttr(particleSetShape, longName='rotationPP', dataType='vectorArray')
    cmd.addAttr(particleSetShape, longName='rotationPP0', dataType='vectorArray')

    # get rotation and scale coordinate from original selected items
    for i in range(0, len(itemsList), 1):
        translationMatrix = cmd.xform(itemsList[i], query=True, translation=True, worldSpace=True)
        rotationMatrix = cmd.xform(itemsList[i], query=True, rotation=True)
        scaleMatrix = cmd.xform(itemsList[i], query=True, scale=True, relative=True)
        cmd.emit(object=particleSetShape, position=translationMatrix)
        cmd.nParticle(particleSetShape, e=True, id=i, attribute='rotationPP', vv=rotationMatrix)
        cmd.nParticle(particleSetShape, e=True, id=i, attribute='radiusPP', fv=scaleMatrix[0])

    # set initial state on particle system
    mel.eval('setDynStartState;')

    # instance last selected item on instancer node
    cmd.select(particleSetShape)
    cmd.select(itemToInstance, toggle=True)
    partInstancer = cmd.particleInstancer(particleSetShape, name='temp_temp_instanced_SET', addObject=True, object=itemToInstance, cycle='None', cycleStep=1, cycleStepUnits='Frames', levelOfDetail='Geometry', rotationUnits='Degrees', rotationOrder='XYZ', position='worldPosition', age='age')

    # set instancer rotation and scale attribute using radiusPP and rotationPP
    cmd.particleInstancer(particleSetShape, edit=True, name=partInstancer, scale='radiusPP')
    cmd.particleInstancer(particleSetShape, edit=True, name=partInstancer, rotation='rotationPP')

    cmd.setAttr('{}.levelOfDetail'.format(partInstancer), 2)

    for item in itemsList:
        cmd.setAttr('{}.visibility'.format(item), 0)

    # set particle node as intermediate object and group items
    particleSet = cmd.listRelatives(particleSetShape, parent=True, fullPath=True)
    cmd.setAttr('{}.intermediateObject'.format(particleSet[0]), 1)
    instancedSetGRP = cmd.group(empty=True, name='temp_temp_GRP_instancedSet')
    cmd.parent(particleSet, instancedSetGRP)
    cmd.parent(partInstancer, instancedSetGRP)
    mel.eval('searchReplaceNames "temp_temp_" " " "all";')


################################################################################
# @brief      customize the current block file.
#
def customizeBlock():
    scenePath = api.scene.scenename(relative=True)
    currentSavedAsset = api.savedAsset.SavedAsset(params={'path': scenePath})

    if currentSavedAsset.dept == 'surfacing' and currentSavedAsset.approvation == 'def' and currentSavedAsset.getName().endswith('block'):
        api.log.logger().debug('The Vrmesh and VrScene customize procedure has started...')
        cmd.file(save=True, force=True, type='MayaBinary')

        # export the vrscene inside set folder
        vrscenefilename = api.vray.createVRayFile()
        cmd.file(new=True, force=True)
        api.vray.addVrscene(os.path.normpath(vrscenefilename))
        basename, ext = os.path.splitext(vrscenefilename)
        vrscenefilename = api.export.export_ma(basename + '_vrscene.ma')
        cmd.file(new=True, force=True)

        # open the original set
        filePath = os.path.join(os.getenv('MC_FOLDER'), scenePath).replace('\\', '/')
        cmd.file(filePath, open=True, force=True)
        api.log.logger().debug('The customize vrscene procedure has ended!')
    else:
        rbwUI.RBWError(text='Please open a block surfacing def file to run this tool.')
