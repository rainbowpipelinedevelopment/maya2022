import importlib
import os

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'setdressing')


def run_fakePublishModeling(*args):
    import depts.setdressing.tools.fakePublish.fakeSavedAsset as fakePublish
    importlib.reload(fakePublish)

    fakePublish.fakePublish(dept='modeling')


def run_overwriteAlembicFile(*args):
    import depts.setdressing.tools.fakePublish.fakeSavedAsset as fakePublish
    importlib.reload(fakePublish)

    fakePublish.overwriteAlembicFile()


def run_fakePublishSurfacinf(*args):
    import depts.setdressing.tools.fakePublish.fakeSavedAsset as fakePublish
    importlib.reload(fakePublish)

    fakePublish.fakePublish(dept='surfacing')


def run_overwriteVrsceneFile(*args):
    import depts.setdressing.tools.fakePublish.fakeSavedAsset as fakePublish
    importlib.reload(fakePublish)

    fakePublish.overwriteVrFile()


def run_customizeBlock(*args):
    import depts.setdressing.tools.fakePublish.fakeSavedAsset as fakePublish
    importlib.reload(fakePublish)

    fakePublish.customizeBlock()


fakePyblishModelingTool = {
    "name": 'Fake Publish Modeling',
    "launch": run_fakePublishModeling,
    "statustip": "Make a fake publish for Modeling dept",
    "icon": os.path.join(iconDir, "fakePublish.png")
}


overwriteAlembicFileTool = {
    "name": 'Overwrite Alembic',
    "launch": run_overwriteAlembicFile,
    "statustip": "Overwrite the alembic file for modeling save def",
    "icon": os.path.join(iconDir, "overwrite.png")
}


fakePyblishSurfacingTool = {
    "name": 'Fake Publish Surfacing',
    "launch": run_fakePublishSurfacinf,
    "statustip": "Make a fake publish for Surfacing dept",
    "icon": os.path.join(iconDir, "fakePublish.png")
}


overwriteVrsceneFileTool = {
    "name": 'Overwrite Vrscene',
    "launch": run_overwriteVrsceneFile,
    "statustip": "Overwrite the vrscene file for surfacing save def",
    "icon": os.path.join(iconDir, "overwrite.png")
}


customizeBlockTool = {
    "name": 'Customize Block',
    "launch": run_customizeBlock,
    "statustip": "Overwrite the vrscene file for surfacing save def",
    "icon": os.path.join(iconDir, "overwrite.png")
}


tools = [
    fakePyblishModelingTool,
    overwriteAlembicFileTool,
    fakePyblishSurfacingTool,
    overwriteVrsceneFileTool,
    customizeBlockTool
]

PackageTool = {
    "name": "fake publish",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
