import importlib
import os

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'common')


################################################################################
# @brief      run the replace assembly function.
#
# @param      args  The arguments
#
def run_replaceAssembly(*args):
    import depts.setdressing.tools.switches.replaceAssemblyUI as replaceAssemblyUI
    importlib.reload(replaceAssemblyUI)

    win = replaceAssemblyUI.ReplaceAssemblyUI()
    win.show()


################################################################################
# @brief      run switch to proxy function.
#
# @param      args  The arguments
#
def run_switchToProxy(*args):
    import common.switchAssembly as switchAssembly
    importlib.reload(switchAssembly)

    switchAssembly.switchTo('cache proxy')


################################################################################
# @brief      run switch to smooth function.
#
# @param      args  The arguments
#
def run_switchSmooth(*args):
    import common.switchAssembly as switchAssembly
    importlib.reload(switchAssembly)

    switchAssembly.switchTo('cache smooth')


################################################################################
# @brief      run switch to cache base function.
#
# @param      args  The arguments
#
def run_switchToCacheBase(*args):
    import common.switchAssembly as switchAssembly
    importlib.reload(switchAssembly)

    switchAssembly.switchTo('cache base')


################################################################################
# @brief      run switch to txt function.
#
# @param      args  The arguments
#
def run_switchToTxt(*args):
    import common.switchAssembly as switchAssembly
    importlib.reload(switchAssembly)

    switchAssembly.switchTo('animText')


################################################################################
# @brief      give me assembly
#
# @param      args  The arguments
#
def run_giveMeAssembly(*args):
    import common.switchAssembly as switchAssembly
    importlib.reload(switchAssembly)

    switchAssembly.getAssemblyFromReference(deleteSource=False, parentAssembly=False)


################################################################################
# @brief      run import all assemblies surf.
#
# @param      args  The arguments
#
def run_importAllAssembliesSurf(*args):
    import common.switchAssembly as switchAssembly
    importlib.reload(switchAssembly)

    switchAssembly.getRepresentationInScene(representationName='surfacing', onlyImport=False, deleteSource=True, world=False, reference=False, approvation='wip')


################################################################################
# @brief      run import all assemblies modeling.
#
# @param      args  The arguments
#
def run_importAllAssembliesModeling(*args):
    import common.switchAssembly as switchAssembly
    importlib.reload(switchAssembly)

    switchAssembly.getRepresentationInScene(onlyImport=False, deleteSource=True, world=True, reference=False, approvation='def')


################################################################################
# @brief      run reference all modeling assemblies.
#
# @param      args  The arguments
#
def run_referenceAllModelingAssemblies(*args):
    import common.switchAssembly as switchAssembly
    importlib.reload(switchAssembly)

    switchAssembly.getRepresentationInScene(onlyImport=False, deleteSource=False, world=True, reference=True, approvation='def')


switchAssemblyTool = {
    "name": "Replace Assembly",
    "launch": run_replaceAssembly,
    "icon": os.path.join(iconDir, "substitution.png")
}

switchProxyTool = {
    "name": 'Switch To Proxy',
    "launch": run_switchToProxy,
    "statustip": "Switch assemblies in scene with the respective proxy version",
    "icon": os.path.join(iconDir, "switch.png")
}


switchSmoothTool = {
    "name": "Switch To Smooth",
    "launch": run_switchSmooth,
    "icon": os.path.join(iconDir, "switch.png")
}


switchBaseTool = {
    'name': 'Switch To Base',
    'launch': run_switchToCacheBase,
    "icon": os.path.join(iconDir, "switch.png")
}


switchTxtTool = {
    'name': 'Switch To Txt',
    'launch': run_switchToTxt,
    "icon": os.path.join(iconDir, "switch.png")
}


referenceToGpuTool = {
    'name': 'Reference To GPU',
    'launch': run_giveMeAssembly,
    'icon': os.path.join(iconDir, "..", "animation", "refToInstance.png")
}


importAllAssembliesSurf = {
    'name': 'Import Surfacing wip',
    'launch': run_importAllAssembliesSurf,
    'icon': os.path.join(iconDir, "merge.png")
}


importModelingDefTool = {
    'name': 'Import Modeling def',
    'launch': run_importAllAssembliesModeling,
    'icon': os.path.join(iconDir, "merge.png")
}


referenceModelingDefTool = {
    'name': 'Reference Modeling def',
    'launch': run_referenceAllModelingAssemblies,
    'icon': os.path.join(iconDir, "reference.png")
}


tools = [
    switchAssemblyTool,
    switchProxyTool,
    switchSmoothTool,
    switchBaseTool,
    switchTxtTool,
    referenceToGpuTool,
    importAllAssembliesSurf,
    importModelingDefTool,
    referenceModelingDefTool
]

PackageTool = {
    "name": "switches",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
