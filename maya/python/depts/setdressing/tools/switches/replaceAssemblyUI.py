import importlib
import os
from PySide2 import QtGui, QtCore, QtWidgets

import maya.cmds as cmd

import api.log
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.log)
# importlib.reload(rbwUI)

################################################################################
# @brief      This class describes a switch assembly ui.
#
class ReplaceAssemblyUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(ReplaceAssemblyUI, self).__init__()

        if cmd.window('ReplaceAssemblyUI', exists=True):
            cmd.deleteUI('ReplaceAssemblyUI')

        self.setObjectName('ReplaceAssemblyUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topWidget = rbwUI.RBWFrame(layout='V', radius=5, spacing=3)

        self.targetWidget = rbwUI.RBWFrame(layout='G', radius=5, bgColor='rgba(30, 30, 30, 150)')
        self.targetLabel = QtWidgets.QLabel('Please select or type the assembly to replace with and press the button')
        self.targetLine = rbwUI.RBWLineEdit(bgColor='transparent')
        self.setTargetButton = rbwUI.RBWButton(text='Set Target Assembly', size=[120, 35])
        self.setTargetButton.clicked.connect(self.setTarget)

        self.targetWidget.layout.addWidget(self.targetLabel, 0, 0, 1, 2)
        self.targetWidget.layout.addWidget(self.targetLine, 1, 0)
        self.targetWidget.layout.addWidget(self.setTargetButton, 1, 1)

        self.topWidget.addWidget(self.targetWidget)

        self.toReplaceButton = rbwUI.RBWButton(text='Replace Selected Items', size=[160, 35], icon=['switch.png'])
        self.toReplaceButton.clicked.connect(self.replaceSelected)

        self.topWidget.addWidget(self.toReplaceButton, alignment=QtCore.Qt.AlignCenter)

        self.mainLayout.addWidget(self.topWidget)

        self.setMinimumSize(400, 200)
        self.setIcon('switch.png')
        self.setTitle('Replace Assembly UI')
        self.setFocus()

    ############################################################################
    # @brief      Sets the target.
    #
    def setTarget(self):
        targetObjects = cmd.ls(selection=True)

        if targetObjects:
            if len(targetObjects) == 1:
                self.targetLine.setText(targetObjects[0])
            else:
                rbwUI.RBWError(title='ATTENTION', text='I remind you that you must select only one object.')
        else:
            rbwUI.RBWError(title='ATTENTION', text='You must select something to set as target.')

    ############################################################################
    # @brief      replace the selected assembly with the target one.
    #
    def replaceSelected(self):
        selectedObjects = cmd.ls(selection=True)

        if selectedObjects:
            targetAssembly = self.targetLine.text()
            if targetAssembly != '':
                taregtPath = cmd.getAttr('{}.definition'.format(targetAssembly))

                for assembly in selectedObjects:
                    cmd.setAttr('{}.definition'.format(assembly), taregtPath, type='string')
                    assemblyNewName = os.path.basename(taregtPath).split('setEdit')[0]
                    cmd.rename(assembly, assemblyNewName)
            else:
                rbwUI.RBWError(title='ATTENTION', text='You must set somthing as target before the switch.')
        else:
            rbwUI.RBWError(title='ATTENTION', text='You must select something to switch.')
