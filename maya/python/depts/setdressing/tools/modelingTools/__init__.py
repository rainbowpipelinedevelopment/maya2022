import importlib
import os

iconpath = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'common')


def run_fin(*args):
    import depts.modeling.tools.finalize.finalize as fm
    importlib.reload(fm)
    fm.finalize(character=False)


def run_chk(*args):
    import depts.modeling.tools.finalize.integrityCheck as minc
    importlib.reload(minc)
    minc.integrityCheck()


def run_rdo(*args):
    import depts.rigging.tools.utilities.resetDisplayOverrides as rdo
    importlib.reload(rdo)
    rdo.resetDisplayOverridesCmd()


def run_deleteNamespace(*args):
    import utilz.namespaceUtil as nmUtil
    importlib.reload(nmUtil)

    nmUtil.delNameSpaceSelected()


def run_renameUI(*args):
    import common.renameToolUI as renameToolUI
    importlib.reload(renameToolUI)

    win = renameToolUI.RenameToolUI()
    win.show()


def run_restoreCoordinate(*args):
    import depts.setdressing.tools.utilities.restoreOriginalTransforms as restoreOriginalTransforms
    importlib.reload(restoreOriginalTransforms)

    restoreOriginalTransforms.restoreOriginalTransforms()


def run_restoreTranslate(*args):
    import maya.mel as mel

    mel.eval("BakeCustomPivot;")


chkTool = {
    "name": "chkTool",
    "launch": run_chk,
    "icon": os.path.join(iconpath, "check.png")
}


finTool = {
    "name": "finTool",
    "launch": run_fin,
    "icon": os.path.join(iconpath, "check.png")
}


rdoTool = {
    "name": "Reset Display Override",
    "launch": run_rdo,
    "icon": os.path.join(iconpath, '..', 'setdressing', "resetDO.png"),
    "statustip": "Reset Display Override"
}


deleteNamespaceTool = {
    "name": "Delete Namespace",
    "launch": run_deleteNamespace,
    "icon": os.path.join(iconpath, 'remove.png')
}


renameTool = {
    "name": "Rename Tool",
    "launch": run_renameUI,
    "icon": os.path.join(iconpath, "rename.png"),
    "statustip": "Rename Tool"
}


restoreCoordinate = {
    "name": "Restore Coordinate",
    "launch": run_restoreCoordinate,
    "icon": os.path.join(iconpath, '..', 'setdressing', "restoreCoordinate.png")
}


restoreTranslate = {
    "name": "Restore Translate",
    "launch": run_restoreTranslate,
    "icon": os.path.join(iconpath, '..', 'setdressing', "restoreCoordinate.png")
}

tools = [
    finTool,
    chkTool,
    rdoTool,
    deleteNamespaceTool,
    renameTool,
    restoreCoordinate,
    restoreTranslate
]


PackageTool = {
    "name": "modeling tools",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
