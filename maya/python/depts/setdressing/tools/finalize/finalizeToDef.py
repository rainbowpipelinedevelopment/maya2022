import importlib
import os

import maya.cmds as cmd

import api.log
import api.widgets.rbw_UI as rbwUI
import depts.setdressing.set_utilities as setUtilz

importlib.reload(api.log)
# importlib.reload(rbwUI)
importlib.reload(setUtilz)


################################################################################
# @brief      Creates a top group.
#
def createTopGroup():
    # top groups from the scene
    top_groups = setUtilz.topGroups()

    if "SET" in top_groups:

        if len(top_groups) == 1:
            api.log.logger().debug("There is already a top group named SET")
            return
        else:
            top_groups.remove("SET")
            set_topGroup = "SET"

    else:
        set_topGroup = cmd.group(empty=True, name="SET")

    for grp in top_groups:
        cmd.parent(grp, set_topGroup)


################################################################################
# @brief      rename every assembly node with the right name ie: group_name_variant
#
def correctRenameAssemblyNodes():
    sceneAssemblies = cmd.ls(type='assemblyReference')

    for sceneAssembly in sceneAssemblies:
        definition = cmd.getAttr('{}.definition'.format(sceneAssembly))
        assemblyName = os.path.split(definition)[1]
        newTransformName = '{}_'.format(assemblyName.split('_setEdit')[0])

        # If the node's name starts with a number put the project's abbreviation in front of it
        if newTransformName[0].isdigit():
            newTransformName = os.environ['PROJECT'] + newTransformName

        cmd.rename(sceneAssembly, newTransformName)


################################################################################
# @brief      check offset function.
#
# @return     check result.
#
def checkOffset():
    excludeList = ["front", "side", "persp", "top", "left", "bottom", "back", 'IMPORT', 'LIBRARY', 'EXTRA']
    transforms = cmd.ls(exactType='transform')

    offsettedtransforms = []

    for transform in transforms:
        if transform not in excludeList:
            translate = cmd.getAttr('{}.translate'.format(transform))
            rotate = cmd.getAttr('{}.rotate'.format(transform))
            scale = cmd.getAttr('{}.scale'.format(transform))

            if translate != [(0.0, 0.0, 0.0)] or rotate != [(0.0, 0.0, 0.0)] or scale != [(1.0, 1.0, 1.0)]:
                offsettedtransforms.append(transform)

    if len(offsettedtransforms) > 0:
        offset = True
    else:
        offset = False

    return offset, offsettedtransforms


################################################################################
# @brief      finalize the set.
#
def finalize():
    offset, offsetList = checkOffset()
    if not offset:
        createTopGroup()
        correctRenameAssemblyNodes()

        assblies = cmd.ls(type='assembly')
        api.log.logger().debug('assemblies in scene: {}'.format(len(assblies)))

        for assembly in assblies:
            if cmd.assembly(assembly, query=True, active=True) not in ['cache base', 'cache smooth', 'cache proxy']:
                rbwUI.RBWError(title='ATTENZIONE', text='One or more assembly are switch to surfacing representation.\nPlease check.')
                return

    else:
        message = 'ATTENZIONE, alcuni dei sottogruppi hanno un offset:\n'

        for offsetedObject in offsetList:
            message = '{}- {}\n'.format(message, offsetedObject)

        message = '{}Perfavore correggi e riprova a finalizzare'.format(message)
        rbwUI.RBWError(title='ATTENZIONE', text=message)


################################################################################
# @brief      reset the pivot in local space.
#
# @return     { description_of_the_return_value }
#
def resetLocalPivot():
    assemblies = cmd.ls(type='assembly')
    api.log.logger().debug('assemblies in scene: {}'.format(len(assemblies)))

    movedPivotItems = []
    for item in assemblies:
        rotatePivot = cmd.xform(item, query=True, rotatePivot=True)
        scalePivot = cmd.xform(item, query=True, scalePivot=True)
        if rotatePivot != [0.0, 0.0, 0.0]:
            movedPivotItems.append(item)
        elif scalePivot != [0.0, 0.0, 0.0]:
            movedPivotItems.append(item)
    api.log.logger().debug('assemblies to fix: {}'.format(len(movedPivotItems)))
    cmd.select(movedPivotItems)

    for item in movedPivotItems:
        cmd.xform(item, rotatePivot=[0.0, 0.0, 0.0])
        cmd.xform(item, scalePivot=[0.0, 0.0, 0.0])

    return len(movedPivotItems)
