import importlib

import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI

importlib.reload(rbwUI)


def getDict(root):
    sceneDict = {root.split('|')[-1]: {}}
    buildDict(root, sceneDict)

    return sceneDict


def buildDict(node, parentDict):
    sn = cmd.ls(node)[0].split(":")[0].split('|')
    if cmd.objectType(node, i="transform"):
        parentDict[sn[-1]] = {}
        children = cmd.listRelatives(node, children=True, fullPath=True)
        if children:
            for child in children:
                buildDict(child, parentDict[sn[-1]])
    else:
        if cmd.objectType(node, i='assemblyReference'):

            name = node.split('|')[-1]
            translation = cmd.xform(node, query=True, translation=True, worldSpace=True)
            rotation = cmd.xform(node, query=True, rotation=True, worldSpace=True)
            scale = cmd.xform(node, query=True, scale=True, worldSpace=True)
            path = cmd.getAttr('{}.definition'.format(node))
            representation = cmd.assembly(node, query=True, active=True)

            parentDict[name] = {'translation': translation, 'rotation': rotation, 'scale': scale, 'path': path, 'representation': representation}


def build(setDict, parent=None):
    for key in setDict:
        if list(setDict[key].keys()) == ['translation', 'rotation', 'scale', 'path', 'representation']:
            assemblyNode = cmd.assembly(name=key, type='assemblyReference')
            cmd.setAttr('{}.definition'.format(assemblyNode), setDict[key]['path'], type='string')
            cmd.xform(assemblyNode, translation=setDict[key]['translation'])
            cmd.xform(assemblyNode, rotation=setDict[key]['rotation'])
            cmd.xform(assemblyNode, scale=setDict[key]['scale'])

            if parent:
                cmd.parent(assemblyNode, parent)
        else:
            cmd.group(empty=True, n=key)
            if parent:
                cmd.parent(key, parent)
            build(setDict[key], key)


def rebuildSet():
    selection = cmd.ls(selection=True, long=True)
    if selection:
        if len(selection) == 1:
            root = selection[0]
        else:
            rbwUI.RBWError(text='More than one object selected')
            return
    else:
        root = '|WIP'

    if not cmd.objExists(root):
        rbwUI.RBWError(text='No valid top group (WIP) in scene.')
        return

    # get set components
    sceneDict = getDict(root)

    # delete the old set
    cmd.delete(root)

    # rebuild set
    build(sceneDict, parent=None)

    if len(root.split('|')) > 2:
        newRoot = root.split('|')[-1]
        rootParent = '|'.join(root.split('|')[:-1])

        cmd.parent(newRoot, rootParent)
