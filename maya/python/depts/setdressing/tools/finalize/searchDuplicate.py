import importlib
import math

import maya.cmds as cmd

import api.log

importlib.reload(api.log)


################################################################################
# @brief      { function_description }
#
# @param      skipDifferentName  The skip different name
# @param      skipBlock          The skip block
# @param      tolerance          The tolerance
# @param      dialog             The dialog
#
# @return     { description_of_the_return_value }
#
def searchDuplicate(skipDifferentName=False, skipBlock=False, tolerance=2.0, dialog=False):
    setObjects = []
    confrontObjects = []
    doubleObjects = []

    progress = 0

    cmd.select(clear=True)

    if dialog:
        cmd.progressWindow(title='Search Duplicate Bar', progress=progress, status='Progress: 0%%')

    assemblies = cmd.ls(type='assemblyReference', visible=True)

    if skipBlock:
        setObjects = []
        for obj in assemblies:
            if not obj.split('_')[2].endswith('block'):
                setObjects.append(obj)
    else:
        setObjects = assemblies

    confrontObjects = list(setObjects)
    step = 100.0 / len(setObjects)

    for setObject in setObjects:
        progress += step
        if dialog:
            cmd.progressWindow(e=1, progress=progress, status='Progress:')

        setObjectX = cmd.getAttr('{}.translateX'.format(setObject))
        setObjectY = cmd.getAttr('{}.translateY'.format(setObject))
        setObjectZ = cmd.getAttr('{}.translateZ'.format(setObject))

        confrontObjects.remove(setObject)

        for secObject in confrontObjects:
            secObjectX = cmd.getAttr('{}.translateX'.format(secObject))
            secObjectY = cmd.getAttr('{}.translateY'.format(secObject))
            secObjectZ = cmd.getAttr('{}.translateZ'.format(secObject))

            distX2 = (secObjectX - setObjectX) * (secObjectX - setObjectX)
            distY2 = (secObjectY - setObjectY) * (secObjectY - setObjectY)
            distZ2 = (secObjectZ - setObjectZ) * (secObjectZ - setObjectZ)

            dist = math.sqrt(distX2 + distY2 + distZ2)

            nameObject = setObject.split('_')[2]

            # verifico se andranno mostrati tutti gli elementi o solo quelli con il nome diverso
            if skipDifferentName:
                if dist <= tolerance and nameObject == secObject.split('_')[2]:
                    doubleObjects.append(secObject)
                    api.log.logger().debug('{} and {} are near'.format(setObject, secObject))
            else:
                if dist <= tolerance:
                    doubleObjects.append(secObject)
                    api.log.logger().debug('{} and {} are near'.format(setObject, secObject))

    # stampo la lista
    api.log.logger().debug('Lista delle item vicine:')
    for obj in doubleObjects:
        api.log.logger().debug(obj)

    if dialog:
        cmd.progressWindow(endProgress=1)

    return doubleObjects
