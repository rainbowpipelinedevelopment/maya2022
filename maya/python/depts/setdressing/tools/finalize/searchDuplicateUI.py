import importlib
from PySide2 import QtCore

import maya.cmds as cmd

import api.log
import api.widgets.rbw_UI as rbwUI
import depts.setdressing.tools.finalize.searchDuplicate as searchDuplicate

importlib.reload(api.log)
# importlib.reload(rbwUI)
importlib.reload(searchDuplicate)

class SearchDuplicateUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs the object
    #
    def __init__(self):
        super(SearchDuplicateUI, self).__init__()
        if cmd.window('SearchDuplicateUI', exists=True):
            cmd.deleteUI('SearchDuplicateUI')

        self.setObjectName('SearchDuplicateUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      initUI method
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topWidget = rbwUI.RBWFrame(layout='V', radius=5, spacing=3)

        self.optionsGroup = rbwUI.RBWGroupBox('Options:', layout='V', fontSize=12)

        # first row
        self.skipDifferentNameCheckBox = rbwUI.RBWCheckBox(text='Skip different name')

        self.skipBlockCheckBox = rbwUI.RBWCheckBox('Skip Block')

        # second row
        self.toleranceLine = rbwUI.RBWLineEdit(title='Tolerance', bgColor='transparent')
        self.toleranceLine.edit.setText('2.0')

        self.optionsGroup.addWidget(self.skipDifferentNameCheckBox)
        self.optionsGroup.addWidget(self.skipBlockCheckBox)
        self.optionsGroup.addWidget(self.toleranceLine)

        self.topWidget.addWidget(self.optionsGroup)

        # third row
        self.startButton = rbwUI.RBWButton(text='Start', icon=['start.png'], size=[100, 35])
        self.startButton.clicked.connect(self.searchDuplicate)

        self.topWidget.addWidget(self.startButton, alignment=QtCore.Qt.AlignCenter)

        self.mainLayout.addWidget(self.topWidget)

        self.setMinimumSize(350, 205)
        self.setTitle('Search Duplicate')
        self.setFocus()

    ############################################################################
    # @brief      search duplicate.
    #
    def searchDuplicate(self):
        skipDifferentName = self.skipDifferentNameCheckBox.isChecked()
        skipBlock = self.skipBlockCheckBox.isChecked()
        tolerance = float(self.toleranceLine.text())

        doubleObjects = searchDuplicate.searchDuplicate(skipDifferentName=skipDifferentName, skipBlock=skipBlock, tolerance=tolerance, dialog=True)

        if len(doubleObjects) > 0:
            cmd.select(doubleObjects)
            msg = 'Ci sono item nella tolleranza indicata\nPotrebbero essere dei doppioni\nControlla ed in caso cancellale'
            rbwUI.RBWWarning(title='WARNING', text=msg)
        else:
            cmd.select(clear=True)
            msg2 = 'Non ci sono item nella tolleranza indicata\n'
            rbwUI.RBWWarning(title='WARNING', text=msg2)
            api.log.logger().debug('Non ci sono item vicine')
