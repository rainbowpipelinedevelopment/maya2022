import importlib
import os

iconpath = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'common')


def run_finalizeToDef(*args):
    import depts.setdressing.tools.finalize.finalizeToDef as finalizeToDef
    importlib.reload(finalizeToDef)

    finalizeToDef.finalize()


def run_checkToDef(*args):
    import depts.setdressing.tools.finalize.check as check
    importlib.reload(check)

    check.checkAsset()


def run_pluginCheck(*args):
    import common.pluginCheckUI as pluginCheckUI
    import api.scene
    import api.widgets.rbw_UI as rbwUI

    importlib.reload(pluginCheckUI)
    importlib.reload(api.scene)
    # importlib.reload(rbwUI)

    if api.scene.listUnknowPlugin() is None:
        rbwUI.RBWConfirm(title="No bad plugin Found", text="No unknown plugin found in this file", defCancel=None)
        return

    window = pluginCheckUI.PluginCheckUI()
    window.show()


def run_resetLocalPivot(*args):
    import depts.setdressing.tools.finalize.finalizeToDef as finalizeToDef
    importlib.reload(finalizeToDef)

    finalizeToDef.resetLocalPivot()


def run_searchDuplicate(*args):
    import depts.setdressing.tools.finalize.searchDuplicateUI as searchDuplicateUI
    importlib.reload(searchDuplicateUI)

    win = searchDuplicateUI.SearchDuplicateUI()
    win.show()


def run_rebuildSet(*args):
    import depts.setdressing.tools.finalize.rebuildSet as rebuildSet
    importlib.reload(rebuildSet)

    rebuildSet.rebuildSet()


finalizeToDef = {
    'name': 'Finalize Set: DEF',
    'launch': run_finalizeToDef,
    "icon": os.path.join(iconpath, "check.png")
}

checkTool = {
    'name': 'Check Set',
    'launch': run_checkToDef,
    "icon": os.path.join(iconpath, "check.png")
}

pluginCheckTool = {
    "name": "Unknown Plugin Check",
    "launch": run_pluginCheck,
    "icon": os.path.join(iconpath, "check.png"),
    "statustip": "Consente di eliminare i plugin sconosciuti presenti nel file"
}


resetLocalPivot = {
    "name": "Reset Local Pivot",
    "launch": run_resetLocalPivot,
    "icon": os.path.join(iconpath, "..", "setdressing", "restoreCoordinate.png"),
    "statustip": "resetLocalPivot"
}

searchDuplicate = {
    'name': 'searchDuplicate',
    'launch': run_searchDuplicate,
    "icon": os.path.join(iconpath, "search.png"),
    'statustip': 'Search duplicated item UI'
}

rebuildSetTool = {
    'name': 'Rebuild Set',
    'launch': run_rebuildSet,
    'icon': os.path.join(iconpath, "build.png"),
    'statustip': 'Rebuild set for a better clean up'
}

tools = [
    finalizeToDef,
    checkTool,
    pluginCheckTool,
    resetLocalPivot,
    searchDuplicate,
    rebuildSetTool
]

PackageTool = {
    "name": "finalize",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
