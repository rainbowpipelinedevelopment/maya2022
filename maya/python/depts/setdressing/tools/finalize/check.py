import importlib
import re
import os

import maya.cmds as cmd
import maya.mel as mel

import api.widgets.rbw_UI as rbwUI
import api.database
import api.scene
import depts.setdressing.tools.finalize.searchDuplicate as searchDuplicate
import depts.setdressing.tools.finalize.finalizeToDef as finalizeToDef

# importlib.reload(rbwUI)
importlib.reload(api.database)
importlib.reload(api.scene)
importlib.reload(searchDuplicate)
importlib.reload(finalizeToDef)


def checkTopGroup():
    topGroups = api.scene.getGroupName()
    if topGroups:
        if isinstance(topGroups, list):
            rbwUI.RBWError(text='More than one top group.')
            return False
        else:
            if topGroups == 'SET':
                return True
            else:
                rbwUI.RBWError(text='The top group have a bad name.')
                return False
    else:
        rbwUI.RBWError(text='The top group is missing or have a bad name.')
        return False


################################################################################
# @brief      { function_description }
#
# @return     { description_of_the_return_value }
#
def checkTransform():
    badTranforms = []
    transforms = []
    for transform in cmd.ls(type='transform'):
        if not cmd.listRelatives(transform, children=True, shapes=True):
            transforms.append(transform)

    for transform in transforms:
        translation = cmd.xform(transform, query=True, translation=True, worldSpace=True)
        rotation = cmd.xform(transform, query=True, rotation=True, worldSpace=True)
        scale = cmd.xform(transform, query=True, scale=True, worldSpace=True)

        if translation != [0.0, 0.0, 0.0]:
            if transform not in badTranforms:
                badTranforms.append(transform)
        if rotation != [0.0, 0.0, 0.0]:
            if transform not in badTranforms:
                badTranforms.append(transform)
        if scale != [1.0, 1.0, 1.0]:
            if transform not in badTranforms:
                badTranforms.append(transform)

    if len(badTranforms) > 0:
        rbwUI.RBWError(text='One or more transform are not correctly freeze.\nI will select those for you')
        cmd.select(badTranforms)
    else:
        return True


################################################################################
# @brief      { function_description }
#
# @return     { description_of_the_return_value }
#
def checkScale():
    badScale = []

    for assembly in cmd.ls(type='assemblyReference'):
        scale = cmd.xform(assembly, query=True, scale=True, worldSpace=True)

        if scale[0] < 0:
            if assembly not in badScale:
                badScale.append(assembly)
        if scale[1] < 0:
            if assembly not in badScale:
                badScale.append(assembly)
        if scale[2] < 0:
            if assembly not in badScale:
                badScale.append(assembly)

    if len(badScale) > 0:
        rbwUI.RBWError(text='One or more assembly has negative value on scale.\nI will select those for you')
        cmd.select(badScale)
    else:
        return True


################################################################################
# @brief      { function_description }
#
# @return     { description_of_the_return_value }
#
def checkKeyable():
    badKeyable = []

    for assembly in cmd.ls(type='assemblyReference'):
        for attr in cmd.listAttr(assembly):
            try:
                if cmd.keyframe(assembly, attribute=attr, query=True):
                    if assembly not in badKeyable:
                        badKeyable.append(assembly)
            except:
                pass

    if len(badKeyable) > 0:
        rbwUI.RBWError(text='One or more assembly hasone or more animation keys.\nI will select those for you')
        cmd.select(badKeyable)
    else:
        return True


################################################################################
# @brief      { function_description }
#
# @return     { description_of_the_return_value }
#
def checkDuplicate():
    doubleObjects = searchDuplicate.searchDuplicate(skipDifferentName=True, skipBlock=True, tolerance=1.0)

    if len(doubleObjects) > 0:
        rbwUI.RBWError(text='There are some duplicated items.\nI will select those for you')
        cmd.select(doubleObjects)
    else:
        return True


################################################################################
# @brief      { function_description }
#
# @return     { description_of_the_return_value }
#
def checkMesh():
    meshes = cmd.ls(type='mesh')
    badMeshes = []

    testString = r"__NS\d?:"
    for mesh in meshes:
        if not re.search(testString, mesh):
            badMeshes.append(mesh)

    if len(badMeshes) > 0:
        rbwUI.RBWError(text='There are some mesh in scene.\nI will select those for you')
        cmd.select(badMeshes)
    else:
        return True


################################################################################
# @brief      { function_description }
#
# @return     { description_of_the_return_value }
#
def checkUpdate():
    outOfDates = []
    assemblies = cmd.ls(type='assemblyReference')
    for assembly in assemblies:
        definition = '/'.join(cmd.getAttr('{}.definition'.format(assembly)).split('/')[7:])
        definitionForQuery = '/'.join(cmd.getAttr('{}.definition'.format(assembly)).split('/')[7:-2])
        latestAssemblyPathQuery = "SELECT a.path FROM `assembly` AS a JOIN `V_assetList` AS va ON a.assetId=va.variantID WHERE a.path LIKE '%{}%' AND va.projectID={} ORDER BY a.date DESC".format(definitionForQuery, os.getenv('ID_MV'))
        latestAssemblyPath = api.database.selectSingleQuery(latestAssemblyPathQuery)[0]
        if latestAssemblyPath != definition:
            outOfDates.append(assembly)

    if len(outOfDates) > 0:
        rbwUI.RBWError(text='Some assembly is not currently updated.\nI will select those for you')
        cmd.select(outOfDates)
    else:
        return True


################################################################################
# @brief      { function_description }
#
# @return     { description_of_the_return_value }
#
def renameGrp():
    grps = cmd.ls('GRP_*')
    for obj in grps:
        newName = obj[4:]
        cmd.rename(obj, newName)

    return len(grps)


################################################################################
# @brief      { function_description }
#
def deleteUnknownNodes():
    unknownNodes = cmd.ls(type="unknown")
    unknownNodes += cmd.ls(type="unknownDag")
    for item in unknownNodes:
        if cmd.objExists(item):
            cmd.lockNode(item, lock=False)
            cmd.delete(item)


################################################################################
# @brief      { function_description }
#
def checkAsset():
    allReturn = []

    if not checkTopGroup():
        return

    if not checkTransform():
        return

    if not checkScale():
        return

    if not checkDuplicate():
        return

    if not checkKeyable():
        return

    if not checkMesh():
        return

    if not checkUpdate():
        return

    allReturn.append('-' * 150)
    allReturn.append('CHECK TOP GROUP ===> PASSED')

    allReturn.append('-' * 150)
    allReturn.append('CHECK TRANSFORM ===> PASSED')

    allReturn.append('-' * 150)
    allReturn.append('CHECK SCALE ===> PASSED')

    allReturn.append('-' * 150)
    allReturn.append('CHECK DUPLICATE ===> PASSED')

    allReturn.append('-' * 150)
    allReturn.append('CHECK KEYS ===> PASSED')

    allReturn.append('-' * 150)
    allReturn.append('CHECK MESHES ===> PASSED')

    allReturn.append('-' * 150)
    allReturn.append('CHECK ASSEMBLY UPDATE ===> PASSED')

    allReturn.append('-' * 150)
    renamedGroup = renameGrp()
    allReturn.append('REMOVED "GRP_" PREFIX TO ===> {} ITEMS'.format(renamedGroup))

    allReturn.append('-' * 150)
    resetted = finalizeToDef.resetLocalPivot()
    allReturn.append('RESET PIVOT TO ===> {} ITEMS'.format(resetted))

    allReturn.append('-' * 150)
    mel.eval("MLdeleteUnused;")
    allReturn.append('DELETE UNUSED NODES')

    allReturn.append('-' * 150)
    deleteUnknownNodes()
    allReturn.append('DELETE UNKNOWN NODES')

    allReturn.append('-' * 150)
    cmd.delete([layer for layer in cmd.ls(type='displayLayer') if layer != 'defaultLayer'])
    allReturn.append('DELETE DISPLAY LAYERS')

    allReturn.append('-' * 150)
    plugs = api.scene.listUnknowPlugin()
    if plugs:
        for plug in api.scene.listUnknowPlugin():
            try:
                cmd.unknownPlugin(plug, remove=True)
            except:
                pass
    allReturn.append('DELETE BAD PLUGINS')
    allReturn.append('-' * 150)

    rbwUI.RBWConfirm(title='SetDressing Integrity Check', text='\n'.join(allReturn), defCancel=None)
    return True
