import importlib
from PySide2 import QtWidgets

import maya.cmds as cmd

import common.loadAndSaveAssetUI
import api.savedAsset
import api.scene
import api.widgets.rbw_UI as rbwUI
import depts.setdressing.tools.finalize.check as check

importlib.reload(common.loadAndSaveAssetUI)
# importlib.reload(api.savedAsset)
importlib.reload(api.scene)
# importlib.reload(rbwUI)
importlib.reload(check)


################################################################################
# @brief      This class describes a load and save set dressing ui.
#
class LoadAndSaveSetdressingUI(common.loadAndSaveAssetUI.AssetUI):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(LoadAndSaveSetdressingUI, self).__init__('Set')

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        super(LoadAndSaveSetdressingUI, self).initUI()

        self.saveUploadOnSgLabel = rbwUI.RBWLabel(text='Upload On SG', italic=False, size=12)
        self.saveUploadOnSgSwitch = rbwUI.RBWSwitch()
        self.saveUploadOnSgSwitch.setChecked(False)

        self.saveInfoGroup.addRow(self.saveUploadOnSgLabel, self.saveUploadOnSgSwitch)

    ############################################################################
    # @brief      save function.
    #
    def save(self):
        try:
            savedAssetParam = {
                'asset': self.currentAsset,
                'dept': self.deptName.lower(),
                'deptType': self.currentDeptType,
                'approvation': self.currentApprovation,
                'note': self.saveNotesTextEdit.toPlainText(),
                'sgUploadPicture': self.saveUploadOnSgSwitch.isChecked()
            }

            if self.saveExportSelectionSwitch.isChecked():
                savedAssetParam['selection'] = cmd.ls(selection=True)[0]
            else:
                savedAssetParam['selection'] = None

            if savedAssetParam['approvation'] == 'def':
                checkIntegrity = check.checkAsset()
            else:
                checkIntegrity = True

            if checkIntegrity:

                if self.currentApprovation == 'def':
                    wipSavedAssetParam = {
                        'asset': self.currentAsset,
                        'dept': self.deptName.lower(),
                        'deptType': self.currentDeptType,
                        'approvation': 'wip',
                        'note': 'Pipeline autosave before def save',
                        'sgUploadPicture': False,
                        'selection': savedAssetParam['selection']
                    }

                    wipSavedAsset = api.savedAsset.SavedAsset(params=wipSavedAssetParam)
                    wipSuccess = wipSavedAsset.save()

                    if wipSuccess:
                        savedAssetParam['parent'] = wipSavedAsset.db_id
                        savedAsset = api.savedAsset.SavedAsset(params=savedAssetParam)
                        success = savedAsset.save()
                        if success:
                            self.updateWipParentOnDB(wipSavedAsset, savedAsset)
                    else:
                        rbwUI.RBWError(title='ATTENTION', text='Problems during autosave wip.\nPleas control the scene and make a new save.')
                        return

                else:
                    savedAsset = api.savedAsset.SavedAsset(params=savedAssetParam)
                    success = savedAsset.save()

                if success:
                    rbwUI.RBWConfirm(title='Save {} Complete'.format(self.deptName), text='Asset Save Complete', defCancel=None)
                    self.fillSavedAssetsTree()
                else:
                    rbwUI.RBWError(title='ATTENTION', text='Some Error During Save')
                    return
            else:
                rbwUI.RBWError(title='ATTENTION', text='The scene doesn\'t pass the Set dressing Check Integrity, control and make a new save.')
                return

        except AttributeError:
            rbwUI.RBWError(title='ATTENTION', text='One between deptType or approvation are not properly selected.\nSelect those and try again.')
            return
