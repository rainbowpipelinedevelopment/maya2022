import importlib
import os
import functools
from PySide2 import QtGui, QtCore, QtWidgets

import maya.cmds as cmd

import api.log
import api.database
import api.savedAsset
import config.dictionaries
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.log)
importlib.reload(api.database)
# importlib.reload(api.savedAsset)
importlib.reload(config.dictionaries)
# importlib.reload(rbwUI)


availableCategories = ['prop', 'set', 'fx']


################################################################################
# @brief      This class describes an asset loader ui.
#
class AssetLoaderUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super().__init__()

        if cmd.window('AssetLoaderUI', exists=True):
            cmd.deleteUI('AssetLoaderUI')

        self.mcFolder = os.getenv('MC_FOLDER')
        api.log.logger().debug(self.mcFolder)
        self.id_mv = os.getenv('ID_MV')
        api.log.logger().debug(self.id_mv)

        self.assets = api.system.getAssetsFromDB(adds=[['`category`', 'IN', '("set", "prop", "fx")']])

        self.setObjectName('AssetLoaderUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.horizontalSplitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)
        self.horizontalSplitter.setObjectName('splitterH')
        self.horizontalSplitter.setStyleSheet('QSplitter#splitterH{background-color: transparent; border-radius: 6px;}')
        self.mainLayout.addWidget(self.horizontalSplitter)

        ########################################################################
        # LEFT PANEL
        #

        # --------------------------- TOP WIDGET -------------------------- #
        self.leftWidget = rbwUI.RBWFrame(layout='V', radius=5, spacing=10)

        # ------------------------- search widget ------------------------- #
        self.searchGroup = rbwUI.RBWGroupBox(title='Search asset name or a part of this:', layout='V', fontSize=12)

        self.searchWidget = rbwUI.RBWFrame(layout='H', margins=[0, 0, 0, 0], bgColor='transparent')

        for category in availableCategories:
            radioButton = QtWidgets.QRadioButton(category)
            radioButton.setObjectName('{}RadioButton'.format(category))
            radioButton.clicked.connect(functools.partial(self.searchRadioButtonClicked, category))
            self.searchWidget.addWidget(radioButton)

        self.searchLineEdit = rbwUI.RBWLineEdit(title='Type asset name:', stretch=False)
        self.searchLineEdit.returnPressed.connect(self.searchEntry)
        self.searchButton = rbwUI.RBWButton(icon=['search.png'])
        self.searchButton.clicked.connect(self.searchEntry)

        self.searchWidget.addWidget(self.searchLineEdit)
        self.searchWidget.addWidget(self.searchButton)

        self.searchGroup.addWidget(self.searchWidget)

        self.searchResultList = QtWidgets.QListWidget()
        self.searchResultList.itemClicked.connect(self.resultListItemClicked)

        self.searchGroup.addWidget(self.searchResultList)

        self.leftWidget.addWidget(self.searchGroup, alignment=QtCore.Qt.AlignTop)

        # ------------------------- Info widget ------------------------- #
        self.assetInfoGroup = rbwUI.RBWGroupBox(title='Asset Info:', layout='H', fontSize=12)

        self.assetGroupComboBox = rbwUI.RBWComboBox('Group', stretch=False)
        self.assetNameComboBox = rbwUI.RBWComboBox('Name', stretch=False)
        self.assetVariantComboBox = rbwUI.RBWComboBox('Variant', stretch=False)

        self.assetGroupComboBox.activated.connect(self.fillAssetNameComboBox)
        self.assetNameComboBox.activated.connect(self.fillAssetVariantComboBox)
        self.assetVariantComboBox.activated.connect(self.setCurrentAsset)

        self.assetInfoGroup.addWidget(self.assetGroupComboBox)
        self.assetInfoGroup.addWidget(self.assetNameComboBox)
        self.assetInfoGroup.addWidget(self.assetVariantComboBox)

        self.leftWidget.addWidget(self.assetInfoGroup)

        # ------------------------- load options ------------------------- #
        self.loadOptionsGroup = rbwUI.RBWGroupBox(title='Load Options:', layout='H', fontSize=12)

        self.leftWidget.addWidget(self.loadOptionsGroup)

        # ------------------------- Button group ------------------------- #
        self.buttonsLayout = QtWidgets.QHBoxLayout()

        self.pickButton = rbwUI.RBWButton(text='Pick', icon=['add.png'], size=[405, 35])
        self.pickButton.clicked.connect(self.pickButtonClicked)

        self.removeButton = rbwUI.RBWButton(text='Remove', icon=['delete.png'], size=[405, 35])
        self.removeButton.clicked.connect(self.removeButtonClicked)

        self.buttonsLayout.addWidget(self.pickButton)
        self.buttonsLayout.addWidget(self.removeButton)

        self.leftWidget.addLayout(self.buttonsLayout)

        # ------------------------- Picked Assets Tree ------------------------- #
        self.pickedElementTreeWidget = rbwUI.RBWTreeWidget(['type', 'path'])
        self.pickedElementTreeWidget.setFirstColumnSpanned(0, self.pickedElementTreeWidget.rootIndex(), True)
        self.pickedElementTreeWidget.setFirstColumnSpanned(1, self.pickedElementTreeWidget.rootIndex(), True)

        self.leftWidget.addWidget(self.pickedElementTreeWidget)

        # ------------------------- Add to scene button ------------------------- #
        self.addToSceneButton = rbwUI.RBWButton(text='Add to scene', icon=['merge.png'], size=[820, 35])
        self.addToSceneButton.clicked.connect(self.addSceneClicked)

        self.leftWidget.addWidget(self.addToSceneButton)

        self.horizontalSplitter.addWidget(self.leftWidget)

        ########################################################################
        # RIGHT AREA
        #
        self.rightWidget = rbwUI.RBWFrame(layout='V', radius=5)

        # ------------------------- preview panel ------------------------- #
        self.imageListWidget = QtWidgets.QListWidget()
        self.imageListWidget.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.imageListWidget.setViewMode(QtWidgets.QListView.IconMode)
        self.imageListWidget.setResizeMode(QtWidgets.QListView.Adjust)
        self.imageListWidget.setMovement(QtWidgets.QListView.Static)
        self.imageListWidget.setIconSize(QtCore.QSize(150, 150))
        self.imageListWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.imageListWidget.itemClicked.connect(self.previewImage)

        self.rightWidget.addWidget(self.imageListWidget)

        self.horizontalSplitter.addWidget(self.rightWidget)

        self.horizontalSplitter.setStretchFactor(0, 0)

        self.setMinimumSize(1020, 700)
        self.setTitle('Asset Loader UI')
        self.addStatusBar()
        self.setFocus()

    ############################################################################
    # @brief      fill the group combo box.
    #
    def fillAssetGroupComboBox(self):
        self.assetGroupComboBox.clear()
        groups = self.assets[self.currentCategory].keys()

        self.assetGroupComboBox.addItems(groups)

        if len(groups) == 1:
            self.assetGroupComboBox.setCurrentIndex(0)
            self.fillAssetNameComboBox()

    ############################################################################
    # @brief      fill th name combo box.
    #
    def fillAssetNameComboBox(self):
        self.currentGroup = self.assetGroupComboBox.currentText()

        self.assetNameComboBox.clear()
        names = self.assets[self.currentCategory][self.currentGroup].keys()

        self.assetNameComboBox.addItems(names)

        if len(names) == 1:
            self.assetNameComboBox.setCurrentIndex(0)
            self.fillAssetVariantComboBox()

    ############################################################################
    # @brief      fill the variant combo box.
    #
    def fillAssetVariantComboBox(self):
        self.currentName = self.assetNameComboBox.currentText()

        self.assetVariantComboBox.clear()
        variants = self.assets[self.currentCategory][self.currentGroup][self.currentName].keys()

        self.assetVariantComboBox.addItems(variants)

        if len(variants) == 1:
            self.assetVariantComboBox.setCurrentIndex(0)
            self.setCurrentAsset()

    ############################################################################
    # @brief      Sets the current asset.
    #
    def setCurrentAsset(self):
        self.currentVariant = self.assetVariantComboBox.currentText()

        self.currentAsset = api.asset.Asset({
            'category': self.currentCategory,
            'group': self.currentGroup,
            'name': self.currentName,
            'variant': self.currentVariant
        })

        self.populateLoadOptionGroup()
        self.updatePreview()

    ################################################################################
    # @brief      Gets the representation.
    #
    # @return     The representation.
    #
    def getRepresentation(self):
        reps = []
        assemblyObj = api.assembly.Assembly(params={'asset': self.currentAsset})
        latestAssemblyData = assemblyObj.getLatest()

        if latestAssemblyData:
            self.currentAssembly = api.assembly.Assembly(params={'db_id': latestAssemblyData[0]})
            if self.currentAsset.category == 'set':
                if self.currentAsset.name.endswith('block'):
                    return ['hig', 'block']
                else:
                    return ['hig']

            if self.currentAssembly.modHigId:
                reps = reps + ['base', 'smooth']

            if self.currentAssembly.modPrxId:
                reps.append('proxy')

            return reps
        else:
            self.currentAssembly = None
            return None

    ############################################################################
    # @brief      populate load option group.
    #
    def populateLoadOptionGroup(self):
        for widget in self.loadOptionsGroup.children():
            if isinstance(widget, QtWidgets.QRadioButton):
                widget.setParent(None)
                widget.deleteLater()

        reps = self.getRepresentation()

        if reps:
            for rep in reps:
                tmpRadioBtn = QtWidgets.QRadioButton(rep)
                tmpRadioBtn.setObjectName('{}RadioBtn'.format(rep))
                tmpRadioBtn.clicked.connect(functools.partial(self.loadOptionRadioButtonClicked, rep))
                self.loadOptionsGroup.addWidget(tmpRadioBtn)
                if rep == "base" or rep == "hig":
                    tmpRadioBtn.click()

    ############################################################################
    # @brief      search the give asset.
    #
    def searchEntry(self):
        wantedEntryName = self.searchLineEdit.text()
        if wantedEntryName == '':
            self.updateResultList()
        else:
            searchAssetFromNameQuery = "SELECT DISTINCT `category`, `group`, `name` FROM `V_assetList` WHERE `projectId`={} AND `name` LIKE '%{}%' AND (`category` = 'set' OR `category` = 'prop' OR `category` = 'fx')".format(
                self.id_mv,
                wantedEntryName
            )

            results = api.database.selectQuery(searchAssetFromNameQuery)
            if results:
                self.updateResultList(results)

    ############################################################################
    # @brief      update the list result.
    #
    # @param      queryResults  The query results
    #
    def updateResultList(self, queryResults=None):
        self.searchResultList.clear()
        if queryResults:
            self.searchResultList.show()
            for infoTupla in queryResults:
                item = QtWidgets.QListWidgetItem('{} - {} - {}'.format(infoTupla[0], infoTupla[1], infoTupla[2]))
                self.searchResultList.addItem(item)
        else:
            self.searchResultList.hide()

    ############################################################################
    # @brief      clear combo boxes
    #
    def clearCombos(self):
        self.assetGroupComboBox.clear()
        self.assetNameComboBox.clear()
        self.assetVariantComboBox.clear()

    ############################################################################
    # @brief      search radio button clicked
    #
    # @param      category  The category
    #
    def searchRadioButtonClicked(self, category):
        self.clearCombos()
        self.currentCategory = category
        self.fillAssetGroupComboBox()

    ############################################################################
    # @brief      load option radio button clicked.
    #
    def loadOptionRadioButtonClicked(self, loadType):
        self.loadTypeSelected = loadType

    ############################################################################
    # @brief      result list item clicked
    #
    def resultListItemClicked(self):
        self.updateRadioButtons()
        self.updateComboBoxes()

    ############################################################################
    # @brief      update radio buttons
    #
    def updateRadioButtons(self):
        # collecto le info dall'elemento selezionato
        fields = self.searchResultList.currentItem().text().split(' - ')

        # seleziono la categoria
        radioButtonName = '{}RadioButton'.format(fields[0])
        radioButton = self.findChild(QtWidgets.QRadioButton, radioButtonName)
        radioButton.setChecked(True)
        radioButton.click()

    ############################################################################
    # @brief      update combo boxes
    #
    def updateComboBoxes(self):
        # collecto le info dall'elemento selezionato
        fields = self.searchResultList.currentItem().text().split(' - ')

        # seleziono il gruppo
        groupItemIndex = self.assetGroupComboBox.findText(fields[1], QtCore.Qt.MatchExactly)
        if groupItemIndex > -1:
            self.assetGroupComboBox.setCurrentIndex(groupItemIndex)

            self.fillAssetNameComboBox()

            # seleziono il nome
            nameItemIndex = self.assetNameComboBox.findText(fields[2], QtCore.Qt.MatchExactly)

            if nameItemIndex > -1:
                self.assetNameComboBox.setCurrentIndex(nameItemIndex)

                self.fillAssetVariantComboBox()

    ############################################################################
    # @brief      pick function.
    #
    def pickButtonClicked(self):
        self.statusBar.showMessage('Pick')

        if self.pickedElementTreeWidget.findItems(self.currentCategory.upper(), QtCore.Qt.MatchContains | QtCore.Qt.MatchRecursive, 0):
            mainNode = self.pickedElementTreeWidget.findItems(self.currentCategory.upper(), QtCore.Qt.MatchContains | QtCore.Qt.MatchRecursive, 0)[0]
        else:
            mainNode = QtWidgets.QTreeWidgetItem(self.pickedElementTreeWidget)
            mainNode.setText(0, self.currentCategory.upper())

        child = QtWidgets.QTreeWidgetItem(mainNode)
        child.setText(0, self.loadTypeSelected)

        if self.currentCategory == "set" and self.loadTypeSelected == "hig":
            setSavedAsset = api.savedAsset.SavedAsset(params={'asset': self.currentAsset, 'dept': 'set', 'deptType': 'hig', 'approvation': 'def'})
            setPath = setSavedAsset.getLatest()[2]
            child.setText(1, setPath)
        else:
            child.setText(1, self.currentAssembly.definitionPath)

        self.pickedElementTreeWidget.expandAll()

        self.statusBar.showMessage('Ready')

    ############################################################################
    # @brief      Removes a button clicked.
    #
    def removeButtonClicked(self):
        self.statusBar.showMessage('Remove')

        try:
            currentItem = self.pickedElementTreeWidget.selectedItems()[0]
            parentItem = currentItem.parent()

            parentItem.removeChild(currentItem)

            childrenCount = parentItem.childCount()
            if not childrenCount:
                self.pickedElementTreeWidget.invisibleRootItem().removeChild(parentItem)
        except AttributeError:
            pass

        self.statusBar.showMessage('Ready')

    ############################################################################
    # @brief      Adds a scene clicked.
    #
    def addSceneClicked(self):
        root = self.pickedElementTreeWidget.invisibleRootItem()
        for i in range(0, root.childCount()):
            categoryNode = root.child(i)

            for j in range(0, categoryNode.childCount()):
                item = categoryNode.child(j)

                loadType = item.text(0)
                path = item.text(1)

                if loadType != 'hig':
                    node = api.scene.addToAddItemsGroup(path)
                    assembly = api.assembly.Assembly(node=node)
                    assembly.switchRepresentation('cache {}'.format(loadType))
                else:
                    node = api.scene.addToSetGroup(path)

        self.pickedElementTreeWidget.clear()

    ############################################################################
    # @brief      update the previe in the right panel
    #
    # @param      index  The index
    #
    def updatePreview(self):
        self.imageListWidget.clear()

        if self.currentAssembly:
            if self.currentAssembly.modHigId:
                fullPath = os.path.join(self.mcFolder, self.currentAssembly.getRepresentationPathFromId('modHigId')).replace('\\', '/')
            elif self.currentAssembly.modPrxId:
                fullPath = os.path.join(self.mcFolder, self.currentAssembly.getRepresentationPathFromId('modPrxId')).replace('\\', '/')
            else:
                api.log.logger().error('No modeling representation for the current assembly')
                return

            if os.path.exists(fullPath):
                snapshotFolder = os.path.join(os.path.dirname(fullPath), "snapshots").replace('\\', '/')
                baseName = os.path.basename(fullPath).split('.')[0]
                imgList = ["persp", "side", "front", "top"]
                if os.path.exists(snapshotFolder):
                    for image in imgList:
                        imagePath = os.path.join(snapshotFolder, "{}_{}.jpg".format(baseName, image)).replace('\\', '/')

                        pixmap = QtGui.QPixmap(imagePath)
                        icon = QtGui.QIcon()
                        icon.addPixmap(pixmap)

                        imageItem = QtWidgets.QListWidgetItem(icon, image)
                        self.imageListWidget.addItem(imageItem)
        else:
            rbwUI.RBWError(title='ATTENTION', text='No assembly for this asset')

    ############################################################################
    # @brief      preview the selected image
    #
    def previewImage(self):
        if self.currentAssembly.modHigId:
            selectedPath = self.currentAssembly.getRepresentationPathFromId('modHigId')
        elif self.currentAssembly.modPrxId:
            selectedPath = self.currentAssembly.getRepresentationPathFromId('modPrxId')

        api.log.logger().debug('selectedPath: {}'.format(selectedPath))

        curruntRow = self.imageListWidget.currentRow()
        item = self.imageListWidget.item(curruntRow)
        selectedImage = item.text()
        api.log.logger().debug('selectedImage: {}'.format(selectedImage))

        imageName = os.path.basename(selectedPath).replace('.mb', '_{}.jpg'.format(selectedImage))
        imagePath = os.path.join(self.mcFolder, os.path.dirname(selectedPath), 'snapshots', imageName).replace('\\', '/')

        previewPanel = rbwUI.RBWPreviewPanel(self, imagePath)
        previewPanel.show()
        api.log.logger().debug(previewPanel.sizeHint())
