import importlib
import os

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'arakno')


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_loadAndSaveModeling(*args):
    import depts.modeling.tools.main.loadAndSaveModelingUI as loadAndSaveMod
    importlib.reload(loadAndSaveMod)

    win = loadAndSaveMod.LoadAndSaveModelingUI()
    win.show()


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_loadAndSaveSurfacing(*args):
    import depts.surfacing.tools.main.loadAndSaveSurfacingUI as loadAndSaveSurf
    importlib.reload(loadAndSaveSurf)

    win = loadAndSaveSurf.LoadAndSaveSurfacingUI()
    win.show()


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_loadAndSaveRigging(*args):
    import depts.rigging.tools.main.loadAndSaveRiggingUI as loadAndSaveRiggingUI
    importlib.reload(loadAndSaveRiggingUI)

    win = loadAndSaveRiggingUI.LoadAndSaveRiggingUI()
    win.show()


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_loadAndSaveSet(*args):
    import depts.setdressing.tools.main.loadAndSaveSetdressingUI as loadAndSaveSetdressingUI
    importlib.reload(loadAndSaveSetdressingUI)

    win = loadAndSaveSetdressingUI.LoadAndSaveSetdressingUI()
    win.show()


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_assetLoader(*args):
    import depts.setdressing.tools.main.assetLoaderUI as assetLoaderUI
    importlib.reload(assetLoaderUI)

    win = assetLoaderUI.AssetLoaderUI()
    win.show()


################################################################################
# @brief      run checkupdate tool.
#
# @param      args  The arguments
#
def run_checkUpdate(*args):
    import common.checkUpdate.checkUpdateUI as checkUpdateUI
    importlib.reload(checkUpdateUI)

    win = checkUpdateUI.CheckUpdateUI('setdressing')
    win.show()


loadAndSaveModelingTool = {
    "name": "L/S Modeling",
    "launch": run_loadAndSaveModeling,
    "icon": os.path.join(iconDir, "loadAndSaveModeling.png"),
    "statustip": "Load & Save Modeling"
}


loadAndSaveSurfacingTool = {
    "name": "L/S Surfacing",
    "launch": run_loadAndSaveSurfacing,
    "icon": os.path.join(iconDir, "loadAndSaveSurfacing.png"),
    "statustip": "Load & Save Surfacing"
}


loadAndSaveRiggingTool = {
    "name": "L/S Rigging",
    "launch": run_loadAndSaveRigging,
    "icon": os.path.join(iconDir, "loadAndSaveRigging.png"),
    "statustip": "Load & Save Rigging"
}


loadAndSaveSetTool = {
    "name": "L/S Set",
    "launch": run_loadAndSaveSet,
    "icon": os.path.join(iconDir, "loadAndSaveSet.png"),
    "statustip": "Load & Save Set Dressing"
}


assetLoaderTool = {
    "name": 'Asset Loader',
    "launch": run_assetLoader,
    "icon": os.path.join(iconDir, "..", "common", "merge.png"),
    "statustip": "Asset Loader"
}


checkupdateTool = {
    "name": "Check Update",
    "launch": run_checkUpdate,
    "icon": os.path.join(iconDir, '..', 'common', "refresh.png"),
    "statustip": "Check Update"
}

tools = [
    loadAndSaveModelingTool,
    loadAndSaveSurfacingTool,
    loadAndSaveRiggingTool,
    loadAndSaveSetTool,
    assetLoaderTool,
    checkupdateTool
]

PackageTool = {
    "name": "main",
    "tools": tools
}
