import importlib
from PySide2 import QtWidgets, QtCore
import collections

import maya.cmds as cmd

import api.log
import api.asset
import api.exception
import api.widgets.rbw_UI as rbwUI
import depts.modeling.tools.finalize.integrityCheck as integrityCheck

importlib.reload(api.log)
# importlib.reload(api.asset)
importlib.reload(api.exception)
# importlib.reload(rbwUI)
importlib.reload(integrityCheck)


################################################################################
# @brief      This class describes a modeling in scene ui.
#
class ModelingInSceneUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super().__init__()
        if cmd.window('DuplicateItemsUI', exists=True):
            cmd.deleteUI('DuplicateItemsUI')

        self.setObjectName('DuplicateItemsUI')
        check = self.checkItems()
        if check:
            self.initUI()
            self.updateMargins()
        else:
            return

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topWidegt = rbwUI.RBWFrame(layout='V')
        self.mainLayout.addWidget(self.topWidegt)

        self.modelGroup = rbwUI.RBWGroupBox(title='Available items:', fontSize=12, layout='V')
        self.topWidegt.addWidget(self.modelGroup)

        self.modelTree = rbwUI.RBWTreeWidget(['PROP/FX entity:', 'Model Top Group:', 'ISOLATE IT', 'Finalize Model for copies', 'Publish Modeling WIP', 'Save Proxy', 'Proxy Percent', 'Image on SG', 'Publish Modeling DEF'], darkMode=True)
        self.modelGroup.addWidget(self.modelTree)

        self.optionsBox = rbwUI.RBWFrame(layout='H', bgColor='transparent', margins=[0, 0, 0, 0])

        self.selectAllButton = rbwUI.RBWButton(text='Select All', size=[200, 35])
        self.finalizeFirstButton = rbwUI.RBWButton(text='Finalize First', size=[200, 35])
        self.finalizeAgainButton = rbwUI.RBWButton(text='Finalize Again', size=[200, 35])
        self.refreshButton = rbwUI.RBWButton(text='Referesh UI', size=[200, 35])

        self.optionsBox.addWidget(self.selectAllButton)
        self.optionsBox.addWidget(self.finalizeFirstButton)
        self.optionsBox.addWidget(self.finalizeAgainButton)
        self.optionsBox.addWidget(self.refreshButton)

        self.modelGroup.addWidget(self.optionsBox)

        self.otherOptionsGroup = rbwUI.RBWGroupBox(title='Other Options:', layout='H', fontSize=12)

        self.getWipModelingButton = rbwUI.RBWButton(text='Get this Assembly as WIP modeling', size=[200, 35])
        self.editInPositionButton = rbwUI.RBWButton(text='Edit in position', size=[200, 35])
        self.duplicateAsNewModelButton = rbwUI.RBWButton(text='Duplicate as a new model', size=[200, 35])

        self.otherOptionsGroup.addWidget(self.getWipModelingButton)
        self.otherOptionsGroup.addWidget(self.editInPositionButton)
        self.otherOptionsGroup.addWidget(self.duplicateAsNewModelButton)

        self.topWidegt.addWidget(self.otherOptionsGroup)

        self.setMinimumSize(1300, 600)
        self.setTitle('Modeling In Scene')
        self.setFocus()

    def checkDoubleNames(self):
        integrityCheck.lookingForIntermediate()
        items = self.itemNames.keys()
        doubleNameItems = {}
        for item in items:
            children = cmd.listRelatives(item, allDescendents=True)
            childrenFull = cmd.listRelatives(item, allDescendents=True, fullPath=True)

            doubles = [subItem for subItem, count in collections.Counter(children).items() if count > 1]
            for child in childrenFull:
                for double in doubles:
                    if child.endswith(double):
                        if item in doubleNameItems.keys():
                            doubleNameItems[item].append(child)
                        else:
                            doubleNameItems[item] = [child]

        cmd.select(clear=True)
        if len(doubleNameItems) > 0:
            api.log.logger().debug('doubleNameItems: {}'.format(doubleNameItems))
            message = 'One or more items has some sub groups with the same name.\nI will select these items for you.\nPlease fix these.'
            rbwUI.RBWError(text=message)
            for item in doubleNameItems:
                cmd.select(doubleNameItems[item], add=True)

    def checkDataEntry(self, node):
        assetToken = node.split('_')

        try:
            assetParams = {
                'category': assetToken[0],
                'group': assetToken[1],
                'name': assetToken[2],
                'variant': assetToken[3]
            }

            asset = api.asset.Asset(params=assetParams)

            return asset.variantId, asset.category

        except api.exception.InvalidAsset:
            return None, None

    def checkItems(self):
        # creo la lista delle item duplicabili dalla gerarchia dentro WIP
        self.itemNames = {}

        if not cmd.objExists('WIP'):
            previousSelection = cmd.ls(selection=True)
            cmd.group(name='WIP', empty=True)
            cmd.select(previousSelection, replace=True)

        entities = cmd.listRelatives('WIP', children=True, f=True)
        if entities:
            for fullPathGroup in entities:
                # controllo se i gruppi entity dentro WIP rispettano bene la naming convention
                group = fullPathGroup.split('|')[-1]
                if not len(group.split('_')) == 4:
                    message = "The entity: {} doesn't follow the proper naming convention, please check!".format(group)
                    rbwUI.RBWError(text=message)
                    return

                if not group == group.lower():
                    cmd.rename(group, group.lower())
                    message = "The entity: {} didn't follow the proper naming convention, it had some capital letters, now renamed..".format(group)
                    rbwUI.RBWError(text=message)
                    return

                # controllo se sotto al gruppo entity c'e' uno e solo un top-group
                topChildrens = cmd.listRelatives(group, children=True, fullPath=True)

                if topChildrens:
                    if not topChildrens[0].count('ctrlSet_'):
                        if len(topChildrens) > 1:
                            # creo un gruppo dove mettere eventuali WIP models non conformi
                            grpToCheck = 'WIP_TO_BE_CHECKED'
                            if not cmd.objExists(grpToCheck):
                                cmd.group(empty=True, name=grpToCheck)
                            api.log.logger().debug("missing top-gropup here: {}".format(group))
                            api.log.logger().debug("topChildrens: {}".format(topChildrens))
                            cmd.parent(group, grpToCheck)
                            message = "The entity: {} doesn't follow the rules, it should have a single top-group, please check!".format(group)
                            rbwUI.RBWError(text=message)
                            return
                else:
                    # caso di gruppo asset vuoto, lo cancello
                    cmd.delete(fullPathGroup)
                    message = 'The group {} was empty, please check and try again.'.format(fullPathGroup)
                    rbwUI.RBWError(text=message)
                    return

                item = cmd.listRelatives(group, children=True, fullPath=True)
                if item:
                    if len(item) > 1:
                        for subGroup in item:
                            if subGroup.count('_cp000'):
                                self.itemNames[group] = {'modGroup': subGroup.split('|')[-1]}
                    else:
                        self.itemNames[group] = {'modGroup': item[0].split('|')[-1]}

                # controllo se esiste il data-entry
                assetId, assetCategory = self.checkDataEntry(group)
                if assetId:
                    self.itemNames[group]['category'] = assetCategory
                    self.itemNames[group]['assetId'] = assetId
                else:
                    self.itemNames[group]['category'] = None
                    self.itemNames[group]['assetId'] = None

        api.log.logger().debug(self.itemNames)

        # controllo eventuali doppi nomi che bloccherebbero il finalize
        self.checkDoubleNames()
        return True
