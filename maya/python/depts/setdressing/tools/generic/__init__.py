import importlib
import os

iconpath = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'setdressing')


def run_createSurfTxtFromModeling(*args):
    import depts.setdressing.tools.generic.createSurfTxtFromModeling as createSurfTxtFromModeling
    importlib.reload(createSurfTxtFromModeling)

    createSurfTxtFromModeling.createTxtSurfFromModeling()


txtFromModTool = {
    "name": "Create Ground Txt Preview",
    "launch": run_createSurfTxtFromModeling,
    "statustip": "Create a Surfacing txt save def from Modeling"
}

tools = [
    txtFromModTool
]

PackageTool = {
    "name": "generic",
    "tools": tools,
    "icon_size": "32",
    "icon_only": False
}
