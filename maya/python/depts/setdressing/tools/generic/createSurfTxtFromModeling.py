import importlib
import os

import maya.cmds as cmd

import lookdev.lookdevTools as lookdevTools
import api.widgets.rbw_UI as rbwUI
import api.savedAsset
import api.asset
import api.scene
import api.log

importlib.reload(lookdevTools)
# importlib.reload(rbwUI)
# importlib.reload(api.savedAsset)
# importlib.reload(api.asset)
importlib.reload(api.scene)
importlib.reload(api.log)


################################################################################
# @brief      assign to meshes the checker shader and blend diffuse wqith mesh color.
#
# @param      meshes  The meshes
#
def assignCheckerShader(meshes=None):
    shaderAssetParams = {'category': 'shader', 'group': 'generic', 'name': 'checker', 'variant': 'default', 'id_mv': 1}
    shaderAsset = api.asset.Asset(params=shaderAssetParams)

    shaderCheckerParmas = {
        'asset': shaderAsset,
        'dept': 'surfacing',
        'deptType': 'rnd',
        'approvation': 'def'
    }

    shaderSavedAsset = api.savedAsset.SavedAsset(params=shaderCheckerParmas)
    shaderPath = os.path.join(shaderAsset.content, shaderSavedAsset.getLatest()[2]).replace('\\', '/')

    currentSavedAsset = api.savedAsset.SavedAsset(saveNode=cmd.ls('modeling_saveNode')[0])
    assetParams = currentSavedAsset.asset.__dict__

    topGroups = api.scene.getTopGroups()

    if len(topGroups) == 1:
        if topGroups[0] == 'CTRL_set':
            topGroup = cmd.listRelatives('CTRL_setsubControl', children=True)[1]
        else:
            topGroup = topGroups[0]

        if not meshes:
            meshes = [cmd.listRelatives(mesh, parent=True)[0] for mesh in cmd.ls(type='mesh')]

        for mesh in meshes:
            # take values
            shape = cmd.listRelatives(mesh, children=True)[0]
            shadeEng = cmd.listConnections(shape, type='shadingEngine')[0]
            material = cmd.ls(cmd.listConnections(shadeEng), materials=True)[0]
            meshColor = cmd.getAttr('{}.color'.format(material))[0]
            api.log.logger().debug('meshColor: {}'.format(meshColor))

            # smooth the mesh
            nodePath = cmd.ls(mesh, long=True)[0]
            parentNodesList = nodePath.split('|')
            smoothCheck = True

            for parentNode in parentNodesList:
                if parentNode == 'nosmooth':
                    smoothCheck = False
                    break

            if smoothCheck:
                cmd.displaySmoothness(mesh, du=3, dv=3, pw=16, ps=4, po=3)
            else:
                cmd.displaySmoothness(mesh, du=0, dv=0, pw=4, ps=1, po=1)

            # import shader
            cmd.file(shaderPath, i=True, options="v=0;p=17", loadReferenceDepth="all")

            # rename imported shader
            oldMaterial = 'sl_assetName_checker_meshName_shader'
            lookdevTools.renameShadingEngine(oldMaterial, assetParams, mesh)
            newMaterial = lookdevTools.renameMaterial(oldMaterial, assetParams, mesh)
            lookdevTools.renameMaterialSubnode(newMaterial, assetParams, mesh)
            newShading = newMaterial.replace('_shader', '_sg')

            # assign new shader to mesh
            cmd.select(mesh, replace=True)
            cmd.hyperShade(mesh, assign=newShading)
            cmd.sets(e=True, forceElement=newShading)

            cmd.setAttr('{}.colorEntryList[1].color'.format(newMaterial.replace('_shader', '_C_ramp')), meshColor[0], meshColor[1], meshColor[2], type='double3')

        nodes = api.scene.getTopGroups()
        cmd.group(empty=True, n='PROJECTIONS')
        for node in nodes:
            if node not in topGroups:
                cmd.parent(node, 'PROJECTIONS')
        cmd.parent('PROJECTIONS', topGroup)
        cmd.setAttr('PROJECTIONS.visibility', 0)

    else:
        rbwUI.RBWError(text='More than one top group in scene')


################################################################################
# @brief      Creates a txt surfacing from modeling.
#
def createTxtSurfFromModeling():
    meshes = cmd.ls(selection=True)

    assignCheckerShader(meshes)

    currentSavedAsset = api.savedAsset.SavedAsset(saveNode=cmd.ls('modeling_saveNode')[0])

    wipSavedAsset = api.savedAsset.SavedAsset(params={
        'asset': currentSavedAsset.asset,
        'dept': 'surfacing',
        'deptType': 'txt',
        'approvation': 'wip',
        'note': 'Pipeline autosave before def save',
        'selection': False,
        'vrscene': False,
        'exportElements': False
    })

    defSavedAssetParams = {
        'asset': currentSavedAsset.asset,
        'dept': 'surfacing',
        'deptType': 'txt',
        'approvation': 'def',
        'note': 'Pipeline autosave from modeling',
        'selection': None,
        'vrscene': False,
        'exportElements': False
    }

    wipSuccess = wipSavedAsset.save()
    if wipSuccess:
        defSavedAssetParams['parent'] = wipSavedAsset.db_id
        savedAsset = api.savedAsset.SavedAsset(params=defSavedAssetParams)
        success = savedAsset.save()
        if success:
            newNote = 'Pipeline autosave before def save {}'.format(savedAsset.version)
            updateQuery = "UPDATE `savedAsset_wip` SET `parent`='{}', `note`='{}' WHERE `id`='{}'".format(savedAsset.db_id, newNote, wipSavedAsset.db_id)
            api.database.deleteQuery(updateQuery)

    rbwUI.RBWConfirm(title='Save Surf Txt Complete', text='Asset Save Complete', defCancel=None)
