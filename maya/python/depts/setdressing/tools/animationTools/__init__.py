import importlib
import os

iconpath = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'arakno')


def run_loadAndSave(*args):
    import common.loadAndSaveShotUI as lss
    importlib.reload(lss)

    win = lss.ShotUI()
    win.show()


def run_animationAssetLoader(*args):
    import depts.animation.tools.main.animationAssetLoaderUI as animationAssetLoaderUI
    importlib.reload(animationAssetLoaderUI)

    win = animationAssetLoaderUI.AnimationAssetLoaderUI()
    win.show()


def run_giveMeTool(*args):
    import depts.animation.tools.utility.sceneManagement as sceneMan
    importlib.reload(sceneMan)
    sceneMan.checkAvailableStuff()


loadAndSaveTool = {
    "name": "L/S Animation",
    "launch": run_loadAndSave,
    "icon": os.path.join(iconpath, "loadAndSave.png")
}

animationAssetLoaderTool = {
    "name": "Anim Asset Loader",
    "launch": run_animationAssetLoader,
    "icon": os.path.join(iconpath, "..", "common", "merge.png")
}


giveMeTool = {
    "name": "Give Me Tool",
    "launch": run_giveMeTool,
    "icon": os.path.join(iconpath, "..", "animation", "giveMe.png")
}


tools = [
    loadAndSaveTool,
    animationAssetLoaderTool,
    giveMeTool
]


PackageTool = {
    "name": "animation tools",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
