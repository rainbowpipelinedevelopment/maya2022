import importlib

import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI

# importlib.reload(rbwUI)


def redressingMultiItems():
    items = cmd.ls(sl=1)

    locators = []
    assemblies = []

    for item in items:
        if cmd.nodeType(item) == 'assemblyReference':
            assemblies.append(item)
        else:
            locators.append(item)
    if len(assemblies) > 1:
        rbwUI.RBWError(title='Wrong Selection', text='ATTENZIONE: Hai selezionato piu\' di un assembly.\nControlla la selezione e riprova')
    elif len(assemblies) == 1:
        assemblyToCopy = assemblies[0]
        for locator in locators:
            # make a copy
            cmd.select(assemblyToCopy, r=1)
            copy = cmd.duplicate()
            poc = cmd.pointConstraint(locator, copy[0], offset=[0, 0, 0], weight=True)
            orc = cmd.orientConstraint(locator, copy[0], offset=[0, 0, 0], weight=True)
            sca = cmd.scaleConstraint(locator, copy[0], offset=[1, 1, 1], weight=True)
            cmd.delete(poc, orc, sca)
    else:
        rbwUI.RBWError(title='Wrong Selection', text='ATTENZIONE: Controlla la selezione e riprova')
