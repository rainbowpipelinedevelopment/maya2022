import importlib
import math

import maya.cmds as cmd
import maya.mel as mel

import api.matrix
import api.vector

importlib.reload(api.vector)
importlib.reload(api.matrix)


def restoreOriginalTransforms():
    xAxis = [1.0, 0.0, 0.0]
    yAxis = [0.0, 1.0, 0.0]
    zAxis = [0.0, 0.0, 1.0]
    ###############################################################################
    # BUILD NEW COORDINATE SYSTEM
    selectedFace = cmd.ls(selection=True, long=True)[0]
    selectedObject = '|'.join(selectedFace.split('|')[:-1])

    normalString = cmd.polyInfo(selectedFace, fn=True)[0].split(': ')[1].split('\n')[0]

    wAxis = [float(i) for i in normalString.split(' ')]
    wAxis = api.vector.normalize(wAxis)
    uAxis = api.vector.crossProduct(yAxis, wAxis)
    uAxis = api.vector.normalize(uAxis)
    vAxis = api.vector.crossProduct(wAxis, uAxis)
    vAxis = api.vector.normalize(vAxis)

    ###############################################################################
    # X AXIS
    alpha1 = math.acos(uAxis[0])
    degAlpha1 = alpha1 * 180 / math.pi

    if degAlpha1 > 0.0:
        uVectorX = api.vector.crossProduct([uAxis[0], 0.0, uAxis[2]], xAxis)
        uVectorX = api.vector.normalize(uVectorX)

        if uVectorX[1] == 1.0:
            finalAlpha1 = -1 * degAlpha1
        else:
            finalAlpha1 = degAlpha1

        uAxis, vAxis, wAxis = rotateCoordinateSystem(uAxis, vAxis, wAxis, [0.0, -finalAlpha1, 0.0])
    else:
        finalAlpha1 = 0.0

    alpha2 = math.acos(uAxis[0])
    degAlpha2 = alpha2 * 180 / math.pi

    if degAlpha2 > 0.0:
        uVectorX = api.vector.crossProduct([uAxis[0], uAxis[1], 0.0], xAxis)
        uVectorX = api.vector.normalize(uVectorX)

        if uVectorX[2] == 1.0:
            finalAlpha2 = -1 * degAlpha2
        else:
            finalAlpha2 = degAlpha2

        uAxis, vAxis, wAxis = rotateCoordinateSystem(uAxis, vAxis, wAxis, [0.0, 0.0, -finalAlpha2])
    else:
        finalAlpha2 = 0.0

    ##################################################################################################
    # Y AXIS
    beta1 = math.acos(vAxis[1])
    degBeta1 = beta1 * 180 / math.pi

    if degBeta1 > 0.0:
        vVectorY = api.vector.crossProduct([0.0, vAxis[1], vAxis[2]], yAxis)
        vVectorY = api.vector.normalize(vVectorY)

        if vVectorY[0] == 1.0:
            finalBeta1 = -1 * degBeta1
        else:
            finalBeta1 = degBeta1

        uAxis, vAxis, wAxis = rotateCoordinateSystem(uAxis, vAxis, wAxis, [-finalBeta1, 0.0, 0.0])
    else:
        finalBeta1 = 0.0

    beta2 = math.acos(vAxis[1])
    degBeta2 = beta2 * 180 / math.pi

    if degBeta2 > 0.0:
        vVectorY = api.vector.crossProduct([vAxis[0], vAxis[1], 0.0], yAxis)
        vVectorY = api.vector.normalize(vVectorY)

        if vVectorY[2] == 1.0:
            finalBeta2 = -1 * degBeta2
        else:
            finalBeta2 = degBeta2

        uAxis, vAxis, wAxis = rotateCoordinateSystem(uAxis, vAxis, wAxis, [0.0, 0.0, -finalBeta2])
    else:
        finalBeta2 = 0.0

    ##################################################################################################
    # Z AXIS
    gamma1 = math.acos(wAxis[2])
    degGamma1 = gamma1 * 180 / math.pi

    if degGamma1 > 0.0:
        wVectorZ = api.vector.crossProduct([0.0, wAxis[1], wAxis[2]], zAxis)
        wVectorZ = api.vector.normalize(wVectorZ)

        if wVectorZ[0] == 1.0:
            finalGamma1 = -1 * degGamma1
        else:
            finalGamma1 = degGamma1

        uAxis, vAxis, wAxis = rotateCoordinateSystem(uAxis, vAxis, wAxis, [-finalGamma1, 0.0, 0.0])
    else:
        finalGamma1 = 0.0

    gamma2 = math.acos(wAxis[2])
    degGamma2 = gamma2 * 180 / math.pi

    if degGamma2 > 0.0:
        wVectorZ = api.vector.crossProduct([wAxis[0], 0.0, wAxis[2]], zAxis)
        wVectorZ = api.vector.normalize(wVectorZ)

        if wVectorZ[1] == 1.0:
            finalGamma2 = -1 * degGamma2
        else:
            finalGamma2 = degGamma2

        uAxis, vAxis, wAxis = rotateCoordinateSystem(uAxis, vAxis, wAxis, [0.0, -finalGamma2, 0.0])
    else:
        finalGamma2 = 0.0

    ##################################################################################################
    # ROTATE PIVOT AND BAKE IT
    cmd.select(selectedObject, replace=True)
    cmd.manipPivot(o=(finalBeta1 + finalGamma1, finalAlpha1 + finalGamma2, finalAlpha2 + finalBeta2))
    mel.eval("BakeCustomPivot;")


################################################################################
# @brief      rotate the coordinate system by the give rotation vector
#
# @param      uAxis     The x axis
# @param      vAxis     The y axis
# @param      wAxis     The z axis
# @param      rotation  The rotation
#
# @return     the rotate axis
#
def rotateCoordinateSystem(uAxis, vAxis, wAxis, rotation=[0.0, 0.0, 0.0]):
    rotationMatrix = api.matrix.getMatrixFromVectors(rotation=rotation)

    uAxis = api.matrix.multiplyVector(rotationMatrix, uAxis)
    uAxis = api.vector.normalize(uAxis)
    vAxis = api.matrix.multiplyVector(rotationMatrix, vAxis)
    vAxis = api.vector.normalize(vAxis)
    wAxis = api.matrix.multiplyVector(rotationMatrix, wAxis)
    wAxis = api.vector.normalize(wAxis)

    return uAxis, vAxis, wAxis
