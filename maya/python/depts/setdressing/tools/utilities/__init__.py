import importlib
import os


iconDir = os.path.join(os.getenv('LOCALPY'), "depts", "setdressing", "tools", "icons")


################################################################################
# @brief      run create duplicate.
#
# @param      args  The arguments
#
def run_createDuplicate(*args):
    import depts.setdressing.tools.utilities.createDuplicateUI as createDuplicateUI
    importlib.reload(createDuplicateUI)

    win = createDuplicateUI.CreateDuplicateUI()
    win.show()


################################################################################
# @brief      run redress items function.
#
# @param      args  The arguments
#
def run_redressingMultiItems(*args):
    import depts.setdressing.tools.utilities.redressingMultipleItems as redressing
    importlib.reload(redressing)

    redressing.redressingMultiItems()


################################################################################
# @brief      run restore coordinates.
#
# @param      args  The arguments
#
def run_restoreCoordinate(*args):
    import depts.setdressing.tools.utilities.restoreOriginalTransforms as restoreOriginalTransforms
    importlib.reload(restoreOriginalTransforms)

    restoreOriginalTransforms.restoreOriginalTransforms()


################################################################################
# @brief      run restore translate function.
#
# @param      args  The arguments
#
def run_restoreTranslate(*args):
    import maya.mel as mel

    mel.eval("BakeCustomPivot;")


createDuplicate = {
    'name': 'Create Duplicate',
    'launch': run_createDuplicate
}


redressingMultiItems = {
    'name': 'Redressing Multi Items',
    'launch': run_redressingMultiItems
}


restoreCoordinate = {
    "name": "Restore Coordinate",
    "launch": run_restoreCoordinate
}


restoreTranslate = {
    "name": "Restore Translate",
    "launch": run_restoreTranslate
}


tools = [
    createDuplicate,
    redressingMultiItems,
    restoreCoordinate,
    restoreTranslate
]

PackageTool = {
    "name": "utilities",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
