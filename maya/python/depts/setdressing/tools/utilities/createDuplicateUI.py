import importlib
import os
from PySide2 import QtWidgets, QtCore, QtGui

import maya.cmds as cmd

import api.log
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.log)
# importlib.reload(rbwUI)


class CreateDuplicateUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs the object
    #
    # @param      self  The object
    #
    def __init__(self):
        super(CreateDuplicateUI, self).__init__()
        if cmd.window('CreateDuplicateUI', exists=True):
            cmd.deleteUI('CreateDuplicateUI')

        self.setObjectName('CreateDuplicateUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      initUI method
    #
    # @param      self  The object
    #
    # @return     { description_of_the_return_value }
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topWidget = rbwUI.RBWFrame(layout='G', radius=5)

        self.objectsGroup = rbwUI.RBWGroupBox(title='Objects:', layout='G', fontSize=12)

        self.sourceObjectLine = rbwUI.RBWLineEdit(title='Source Object', bgColor='transparent', stretch=False)
        self.sourceObjectSetButton = rbwUI.RBWButton(text='Set', size=[50, 20])

        self.destObjectLine = rbwUI.RBWLineEdit(title='Dest Object', bgColor='transparent', stretch=False)
        self.destObjectSetButton = rbwUI.RBWButton(text='Set', size=[50, 20])

        self.sourceObjectSetButton.clicked.connect(self.setSourceObject)
        self.destObjectSetButton.clicked.connect(self.setDestObject)

        self.objectsGroup.layout.addWidget(self.sourceObjectLine, 0, 0)
        self.objectsGroup.layout.addWidget(self.sourceObjectSetButton, 0, 1)
        self.objectsGroup.layout.addWidget(self.destObjectLine, 1, 0)
        self.objectsGroup.layout.addWidget(self.destObjectSetButton, 1, 1)

        self.topWidget.layout.addWidget(self.objectsGroup, 0, 0, 1, 2)

        self.duplicateOptionsGroup = rbwUI.RBWGroupBox(title='Options:', layout='V', fontSize=12)

        self.objectNameOptionButton = QtWidgets.QRadioButton('Name')
        self.objectNameOptionButton.setChecked(True)
        self.objectSelectionOptionButton = QtWidgets.QRadioButton('Selection (Based on Name)')
        self.selectedObjectButton = QtWidgets.QRadioButton('Selection (Without Specific Dest.)')

        self.duplicateOptionsGroup.addWidget(self.objectNameOptionButton)
        self.duplicateOptionsGroup.addWidget(self.objectSelectionOptionButton)
        self.duplicateOptionsGroup.addWidget(self.selectedObjectButton)

        self.topWidget.layout.addWidget(self.duplicateOptionsGroup, 1, 0)

        self.duplicateButton = rbwUI.RBWButton(text='Duplicate', icon=['copy.png'], size=[100, 35])
        self.duplicateButton.clicked.connect(self.duplicate)
        self.topWidget.layout.addWidget(self.duplicateButton, 1, 1)

        self.mainLayout.addWidget(self.topWidget)

        self.setMinimumSize(400, 220)
        self.setTitle('Create Duplicate UI')
        self.setFocus()

    ############################################################################
    # @brief      Sets the source object.
    #
    def setSourceObject(self):
        selection = cmd.ls(selection=True)
        if len(selection) == 0:
            rbwUI.RBWError(title='ATTENTION', text='You must select something before click the set button.')
        elif len(selection) > 1:
            rbwUI.RBWError(title='ATTENTION', text='You can only select one object to set it.')
        else:
            self.sourceObjectLine.setText(selection[0])

    ############################################################################
    # @brief      Sets the destination object.
    #
    def setDestObject(self):
        selection = cmd.ls(selection=True)
        if len(selection) == 0:
            rbwUI.RBWError(title='ATTENTION', text='You must select something before click the set button.')
        elif len(selection) > 1:
            rbwUI.RBWError(title='ATTENTION', text='You can only select one object to set it.')
        else:
            if selection[0] != self.sourceObjectLine.text():
                self.destObjectLine.setText(selection[0])
            else:
                rbwUI.RBWError(title='ATTENTION', text='You must select an object other than the source.')

    ############################################################################
    # @brief      duplicate function
    #
    def duplicate(self):
        self.currentSource = self.sourceObjectLine.text()

        if self.selectedObjectButton.isChecked():
            objectSelected = cmd.ls(selection=True)
        else:
            self.currentDest = self.destObjectLine.text()

            if self.currentDest != '':
                nameObjSplit = self.currentDest.split("_")
                nameObj = "_".join(nameObjSplit[:4])

            if self.objectNameOptionButton.isChecked():
                objectSelected = cmd.ls('{}*'.format(nameObj), type='assemblyReference')
            else:
                objectSelected = cmd.ls('{}*'.format(nameObj), selection=True)

            if self.currentDest in objectSelected:
                objectSelected.remove(self.currentDest)

            cmd.matchTransform(self.currentSource, self.currentDest, pivots=True)

        for selectObj in objectSelected:
            newObj = cmd.duplicate(self.currentSource)
            cmd.matchTransform(newObj[0], selectObj, position=True, rotation=True, scale=True)
