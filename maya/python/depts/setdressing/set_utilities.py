import importlib

import maya.cmds as cmd


################################################################################
# @brief      get the top groups.
#
# @return     the top group
#
def topGroups():
    top_nodes = cmd.ls(assemblies=True)
    defaultNodes = ['persp', 'top', 'front', 'side']
    custom_nodes = [grp for grp in top_nodes if grp not in defaultNodes]
    top_groups = [grp for grp in custom_nodes if isGroup(grp)]
    return top_groups


def isGroup(node):
    if "transform" not in cmd.nodeType(node):
        return False

    childs = cmd.listRelatives(node, children=True, fullPath=True)

    if childs:
        for c in childs:
            isNotTransform = "transform" not in cmd.nodeType(c)
            isNotXgenPalette = "xgmPalette" not in cmd.nodeType(c)
            isNotAssemblyReference = "assemblyReference" not in cmd.nodeType(c)

            if isNotTransform and isNotXgenPalette and isNotAssemblyReference:
                return False

    return True
