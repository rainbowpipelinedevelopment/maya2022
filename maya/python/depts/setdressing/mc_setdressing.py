import importlib
import os
import xml.etree.cElementTree as ET
import xml.dom.minidom as xdm

import maya.cmds as cmd
import maya.mel as mel

import depts.mc_depts as mcdepts
import api.log
import api.json
import api.scene
import api.asset
import api.database
import api.widgets.rbw_UI as rbwUI
import config.dictionaries as dictionary

importlib.reload(mcdepts)
importlib.reload(api.log)
importlib.reload(api.json)
importlib.reload(api.scene)
# importlib.reload(api.asset)
importlib.reload(api.database)
# importlib.reload(rbwUI)
importlib.reload(dictionary)

################################################################################
# @brief      This class describes a set.
#
class Set(mcdepts.Dept):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, savedAsset):
        super(Set, self).__init__(savedAsset)
        self.deptName = "set"

    def load(self, mode='load', params=None):
        if mode == 'load':
            cmd.file(self.path, open=True, force=True)
        elif mode == 'merge':
            cmd.file(self.path, i=True, type="mayaBinary", ignoreVersion=True, mergeNamespacesOnClash=False, pr=True)
        else:
            pass

    ############################################################################
    # @brief      Perform the actual save
    #
    # @param      self  The object
    #
    # @return     None
    #
    def save(self):
        # CHECK se si vuole salvare una def con il gruppo WIP presente
        if self.isDef():
            if cmd.objExists('WIP'):
                message = "ATTENZIONE, in questa scena e' presente il gruppo 'WIP'\nSi e' scelto pero' di fare un publish def, quindi la procedura cancellera' il gruppo 'WIP'."
                answer = rbwUI.RBWWarning(title='WARNING', text=message)
                if not answer:
                    return
                else:
                    cmd.delete('WIP')

        # CHECK FOR ITEMS WITH PIVOT POINT CAHNGED AND ROTATIONS DONE
        # (it is dangerous for some reasons)
        assemblies = cmd.ls(type='assembly')
        api.log.logger().debug(len(assemblies))
        movedPivotItems = []
        for item in assemblies:
            moved = 0
            pivotRotation = cmd.xform(item, query=True, rotatePivot=True)
            pivotScale = cmd.xform(item, query=True, scalePivot=True)
            if not str(pivotRotation) == "[0.0, 0.0, 0.0]":
                moved = 1
                api.log.logger().debug('item: {}, pivotRotation: {}'.format(item, pivotRotation))
            elif not str(pivotScale) == "[0.0, 0.0, 0.0]":
                moved = 1
                api.log.logger().debug('item: {}, pivotScale: {}'.format(item, pivotScale))
            if moved:
                rotations = cmd.xform(item, query=True, rotateTranslation=True)
                if not str(rotations) == '[0.0, 0.0, 0.0]':
                    movedPivotItems.append(item)
        if len(movedPivotItems):
            cmd.select(movedPivotItems)
            movedPivotMessage = "There are some items with the pivot point changed, and some rotations on top of that.\n\nIt could be a problem for the \"Give me Rig\" tool, please check!\n\nIf you don't know what to do, ask to your supervisor."
            rbwUI.RBWWarning(title="Warning", text=movedPivotMessage)

        api.scene.pluginCleanUp()

        if cmd.objExists('aTools_StoreNode'):
            cmd.delete('aTools_StoreNode')

        self.deleteLegacyRender()
        self.fixModelEditors()

        if self.isDef():
            self.tagAssemblies()
            self.fixModelEditors()

        # === local scene save ===
        self.localSave()

        # === Real Save On Disk!! ===
        self.realSave()

        self.exportXML()

        scenefilename = api.scene.scenename()

        # REWRITE in order to fix the list creation
        if self.isDef():
            self.shotgunLinkPropIntoSet()

            # new for UE
            self.exportJson()

            cmd.file(new=True, force=True)
            cmd.file(scenefilename, open=True, force=True)

        self.publishOnShotgun()

        # leave this!!
        return True

    ############################################################################
    # @brief      delete the bad legacy render layers.
    #
    def deleteLegacyRender(self):
        all = cmd.ls(type="renderLayer")
        cmd.select(all, r=1)
        sel = cmd.ls(sl=True)
        try:
            cmd.delete(sel)
        except:
            pass

    ############################################################################
    # @brief      fix model editors.
    #
    def fixModelEditors(self):
        for item in cmd.lsUI(editors=True):
            try:
                cmd.modelEditor(item, edit=True, editorChanged="")
            except RuntimeError:
                pass

    ############################################################################
    # @brief      tag all assemblies with the asset id.
    #
    def tagAssemblies(self):
        setTopGroup = "SET"
        connections = cmd.listConnections(setTopGroup)
        if str(connections[0]) == "set_saveNode":
            saveNode = connections[0]
            assetID = cmd.getAttr('{}.asset_id'.format(saveNode))

        allSubNodes = cmd.listRelatives(setTopGroup, allDescendents=1, fullPath=1)
        for item in allSubNodes:
            if cmd.objectType(item) == "assemblyReference":
                itemIdToken = item.split('_')[-2]

                # check if it's already tagged
                if not itemIdToken.isdigit():
                    taggedItem = '{}_{}_'.format(item.split("|")[-1], assetID)
                    cmd.rename(item, taggedItem)
                else:
                    if itemIdToken == assetID:
                        pass
                    else:
                        oldName = item.split("|")[-1]
                        oldNameTokens = oldName.split('_')
                        taggedItem = '{}_{}_{}_{}_'.format(oldNameTokens[0], oldNameTokens[1], oldNameTokens[2], assetID)
                        cmd.rename(item, taggedItem)

    ############################################################################
    # @brief      export xml from the current asset.
    #
    # @return     export success.
    #
    def exportXML(self):

        path = os.path.join(self.savedAsset.savedAssetFolder, '{}.xml'.format(self.savedAsset.sceneName)).replace('\\', '/')
        root = ET.Element('assemblies')

        assemblies = self.listAssemblies()

        for assembly in assemblies:
            assemblyNode = assemblies[assembly][0]
            definitionRelPath = '/'.join(cmd.getAttr('{}.definition'.format(assemblyNode)).split('/')[7:])
            ET.SubElement(root, 'assembly', name=assembly, path=definitionRelPath, id=str(self.getAssemblyRecord(definitionRelPath)[0]))

        if root:
            tree = ET.ElementTree(root)
            tree.write(path)

            xml = xdm.parse(path)
            pretty_xml_as_string = xml.toprettyxml(indent='\t')

            with open(path, "w") as f:
                f.write(pretty_xml_as_string)

            return True

    ############################################################################
    # @brief      export json posirion file for UE.
    #
    def exportJson(self):
        data = {}
        assemblies = cmd.ls(type='assemblyReference')
        cmd.xform('|SET', rotation=[90.0, 0.0, 90.0])
        for assembly in assemblies:
            translation = cmd.xform(assembly, query=True, translation=True, worldSpace=True)
            rotation = cmd.xform(assembly, query=True, rotation=True, worldSpace=True)
            scale = cmd.xform(assembly, query=True, scale=True, worldSpace=True)

            data[assembly] = [translation[0], translation[1], translation[2], rotation[0], rotation[1], rotation[2], scale[0], scale[1], scale[2]]

        jsonPath = os.path.join(self.savedAsset.savedAssetFolder, '{}_ue.json'.format(self.savedAsset.sceneName)).replace('\\', '/')
        api.json.json_write(data, jsonPath)

    ############################################################################
    # @brief      link prop in set on SG.
    #
    def shotgunLinkPropIntoSet(self):
        assetIds = []

        assemblies = self.listAssemblies()

        for assembly in assemblies:
            assemblyNode = assemblies[assembly][0]
            definitionRelPath = '/'.join(cmd.getAttr('{}.definition'.format(assemblyNode)).split('/')[7:])
            assetSgId = int(self.getAssemblyRecord(definitionRelPath)[1])
            assetIds.append(assetSgId)

        self.shotgridObj.updateAssetSubAssets(int(self.getSgId()), assetIds)

    ################################################################################
    # @brief      list assemblies
    #
    # @return     a dict with assemblies infos.
    #
    def listAssemblies(self):
        assemblies = {}

        for assembly in cmd.ls(type='assemblyReference'):
            assemblyPath = cmd.getAttr('{}.definition'.format(assembly))
            assetFullName = '_'.join(assemblyPath.split('/')[8:12])
            if assetFullName not in assemblies:
                assemblies[assetFullName] = [assembly]
            else:
                assemblies[assetFullName].append(assembly)

        return assemblies

    ############################################################################
    # @brief      Gets the assembly identifier.
    #
    # @param      assemblyDefinition  The assembly definition
    #
    # @return     The assembly identifier.
    #
    def getAssemblyRecord(self, assemblyDefinition):
        assemblyIdQuery = "SELECT a.id, ast.shotgunId FROM `assembly` AS a JOIN `asset` AS ast ON a.assetId=ast.id WHERE a.path = '{}'".format(assemblyDefinition)
        result = api.database.selectSingleQuery(assemblyIdQuery)

        return result

    ########################################################################
    # @brief      publish on shotgun
    #
    #
    def publishOnShotgun(self):
        if hasattr(self.savedAsset, 'sgUploadPicture') and self.savedAsset.sgUploadPicture:
            try:
                saveNode = cmd.ls('set_saveNode')[0]
            except IndexError:
                rbwUI.RBWError(title='ATTENZIONE', text='Il SaveNode non esiste')
                return

            # variabili create con il SaveNode per l'upload su Shotgun della Version
            shotgunId = cmd.getAttr('{}.shotgun_id'.format(saveNode))
            note = cmd.getAttr('{}.note'.format(saveNode))

            try:
                assetTasks = self.shotgridObj.taskByPipelineStepByAsset('Set Dressing', assetId=int(shotgunId))
                if len(assetTasks) == 1:
                    taskName = assetTasks[0]['cached_display_name']
                else:
                    for task in assetTasks:
                        if task['cached_display_name'].lower() == 'set dressing':
                            taskName = task['cached_display_name']
            except:
                rbwUI.RBWError(title='ATTENTION', text='No Task assigned, upload picture on Shotgun failed.\nAsset saved!')
                api.log.logger().error("No Task assigned, upload picture on Shotgun failed. Asset saved!")
                return

            try:
                taskParam = self.shotgridObj.taskByPipelineStepByAsset('Set Dressing', assetId=int(shotgunId), taskName=taskName)[0]
            except TypeError:
                rbwUI.RBWError(title='ATTENTION', text='Error with Task during upload picture on Shotgun.\nAsset saved!')
                api.log.logger().error("Error with Task during upload picture on Shotgun. Asset saved!")
                return

            entity = {'type': 'Asset', 'id': int(shotgunId)}
            taskId = taskParam['id']
            filename = api.scene.scenename().split('/')[-1]
            filePath = api.scene.scenename().replace(filename, '')
            imagePath = '{}snapshots/{}_persp.jpg'.format(filePath, filename[:-3])

            # upload della nuova Version su Shotgun
            try:
                self.shotgridObj.createAssetVersion(filename[:-3], note, entity, taskId, imagePath, '')
            except:
                rbwUI.RBWError(title='ATTENTION', text='Some errors during upload picture on Shotgun.\nAsset saved!')
                api.log.logger().error("Some errors during upload picture on Shotgun. Asset saved!")
                return

            rbwUI.RBWConfirm(title='SG Upload Picture', text='Correct SG upload picture', defCancel=None)
