import importlib
import os

import maya.cmds as cmd

import depts.sc_depts as scDepts
import api.camera
import api.widgets.rbw_UI as rbwUI
import depts.animation.tools.generic.animCurveExporter as animCurveExporter

importlib.reload(scDepts)
importlib.reload(api.camera)
# importlib.reload(rbwUI)
importlib.reload(animCurveExporter)


################################################################################
# @brief      This class describes a lighting.
#
class Camera(scDepts.Dept):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.name = "Cam"

    ############################################################################
    # @brief      save function.
    #
    def save(self):
        cam = api.camera.getSceneCamera()

        mbPath = os.path.join(self.savedShot.savedShotFolder, '{}.mb'.format(self.savedShot.sceneName)).replace('\\', '/')
        maPath = os.path.join(self.savedShot.savedShotFolder, '{}.ma'.format(self.savedShot.sceneName)).replace('\\', '/')
        xmlPath = os.path.join(self.savedShot.savedShotFolder, '{}.xml'.format(self.savedShot.sceneName)).replace('\\', '/')
        rbwPath = os.path.join(self.savedShot.savedShotFolder, '{}.rbw'.format(self.savedShot.sceneName)).replace('\\', '/')
        fbxPath = os.path.join(self.savedShot.savedShotFolder, '{}.fbx'.format(self.savedShot.sceneName)).replace('\\', '/')
        abcPath = os.path.join(self.savedShot.savedShotFolder, '{}.abc'.format(self.savedShot.sceneName)).replace('\\', '/')
        chanPath = os.path.join(self.savedShot.savedShotFolder, '{}.chan'.format(self.savedShot.sceneName)).replace('\\', '/')
        locatorAbcPath = os.path.join(self.savedShot.savedShotFolder, '{}_locators.abc'.format(self.savedShot.sceneName)).replace('\\', '/')

        # export xml
        api.camera.exportXml(cam, xmlPath)

        # create the BAKED_CAM in scene
        bakedCam = api.camera.bake_camera(selCam=cam)

        # export camera .ma and .mb
        try:
            cmd.file(mbPath, es=1, typ="mayaBinary", pr=1, ch=1, chn=1, con=1, exp=1, f=1)
        except:
            api.log.logger().error('Error in save mb file')

        try:
            cmd.file(maPath, es=1, typ="mayaAscii", pr=1, ch=1, chn=1, con=1, exp=1, f=1)
        except:
            api.log.logger().error('Error in save ma file')

        # camera anim curves
        exportData = animCurveExporter.ExportData()
        exportData.filePath = rbwPath
        exportData.saveNotKeyed = True
        exportData.saveHiddenObjs = True
        exportData.saveHierarchy = True
        exportData.listObjects = bakedCam
        animCurveExporter.exportAnimCurves(exportData)

        # export fbx
        cmd.select(bakedCam[0], replace=True)
        api.camera.exportFbx(fbxPath)

        # export alembic
        api.camera.exportAlembic(abcPath, bakedCam[0])

        # export locators
        api.camera.exportLocators(locatorAbcPath)

        # export .chan
        api.camera.newChannelExporter(chanPath, bakedCam)

        cmd.delete(bakedCam)

        try:
            staticRanges_str, staticRanges_lst = api.camera.detect_cam_movements_from_chan(chanPath)
        except:
            staticRanges_str = 0

        self.shotgridObj.update_frames_with_no_cam_movement(self.getShotIdSg(), staticRanges_str)

        return True

    ############################################################################
    # @brief      load function.
    #
    def load(self, mode='load', confirm=True):
        if mode == 'load':
            # check if there are unsaved changes
            fileCheckState = cmd.file(q=True, modified=True)
            # if there are, save them first ... then we can proceed
            if fileCheckState and confirm:
                api.log.logger().debug('Need to save...')
                # confirm dialog for load a new shot (Rafael request..)
                openScenePath = str(cmd.file(query=True, sceneName=True))
                if openScenePath:
                    answer = rbwUI.RBWWarning(title='Unsave Changes', text='There are unsaved changes.', defOk=['Load', 'ok.png'], defCancel=['Skip', 'cancel.png'])
                    if not answer:
                        api.log.logger().debug("Load action skipped... Please procede to save your opened scene!")
                        return

            cmd.file(new=True, force=True)
            cmd.file(self.path, open=True, force=True)

            # FPS
            self.setFPS()

        elif mode == 'merge':
            node = api.scene.mergeAsset(self.path)

            if cmd.objExists('LIBRARY'):
                libraryGroup = cmd.ls('LIBRARY')[0]
                cmd.rename(libraryGroup, 'LIBRARY_OLD')
            cmd.group(empty=True, n='LIBRARY')

            cmd.parent(node, 'LIBRARY')
