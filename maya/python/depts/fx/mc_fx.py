import importlib

import maya.cmds as cmd

import depts.mc_depts as mc_depts
import api.widgets.rbw_UI as rbwUI

importlib.reload(mc_depts)
# importlib.reload(rbwUI)


class Fx(mc_depts.Dept):

    def save(self):
        self.localSave()

        if self.getDeptType() != 'source':
            # Remove absolute paths
            try:
                self.checkTexturesPath()
            except:
                rbwUI.RBWWarning(title='ATTENTION', text='Error while checking and replacing absolute paths with relative ones.\nThe save will contine but please contact pipeline@rbw-cgi.it')

        # remove Display Layers
        if self.isDef():
            self.cleanupDisplayLayers()

        if self.savedAsset.cacheSet:
            if not cmd.objExists('FX_CACHES'):
                meshes = cmd.ls(type='mesh')
                if meshes:
                    cmd.sets(name='FX_CACHES', empty=True)
                    cmd.sets(meshes, add='FX_CACHES')

        # === Real Save On Disk!! ===
        self.realSave()

        # write the xml file for deliveries..
        self.exportXML()

        # leave this!!
        return True
