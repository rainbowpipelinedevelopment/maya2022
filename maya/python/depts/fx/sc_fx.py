import importlib

import maya.cmds as cmd

import depts.sc_depts as scDepts
import api.log
import api.scene
import api.widgets.rbw_UI as rbwUI
import depts.finishing.tools.generic.cacheManager as cacheManager

importlib.reload(scDepts)
importlib.reload(api.log)
importlib.reload(api.scene)
# importlib.reload(rbwUI)
importlib.reload(cacheManager)


################################################################################
# @brief      This class describes a fx.
#
class Fx(scDepts.Dept):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      savedShot  The saved shot
    #
    def __init__(self, savedShot):
        super(Fx, self).__init__(savedShot)

        self.name = 'Fx'

    ############################################################################
    # @brief      load function.
    #
    def load(self, mode='load', confirm=True):
        if mode == 'load':
            super(Fx, self).load(mode, confirm)

        elif mode == 'merge':
            node = api.scene.mergeAsset(self.path)
            if not cmd.objExists('FX'):
                cmd.group(empty=True, n='FX')

            cmd.parent(node, 'FX')
            api.log.logger().debug('Add {} to FX'.format(node))

            self.attachLgtShaders(node)

        else:
            # merge as reference and change shader
            namespace = '{}_{}_hou_FX'.format(self.savedShot.itemName[0], self.savedShot.itemName)

            api.scene.addToFxGroup(self.path, namespace)

            proxies = cmd.ls('{}:*'.format(namespace), type='VRayProxy')
            proxy2Shader = {}
            for proxy in proxies:
                shaderOverrides = cmd.vrayUpdateProxy(proxy, getShaderSets=True)
                for i in range(0, len(shaderOverrides)):
                    shader = cmd.defaultNavigation(defaultTraversal=True, destination='{}.shaders[{}].shadersConnections'.format(proxy, i))[0]
                    if 'lambert' not in shader:
                        if shader in proxy2Shader:
                            proxy2Shader[shader].append({'proxy': proxy, 'index': i})
                        else:
                            proxy2Shader[shader] = [{'proxy': proxy, 'index': i}]

            cacheManager.changeShaders(namespace[:-2], proxy2Shader)

            self.attachLgtShaders('{}:GRP_FX'.format(namespace))
