import importlib
import os

import maya.cmds as cmd
import maya.mel as mel

import api.log
import api.json
import api.vray
import api.shot
import api.savedShot
import api.database

importlib.reload(api.log)
importlib.reload(api.json)
importlib.reload(api.vray)
# importlib.reload(api.shot)
# importlib.reload(api.savedShot)
importlib.reload(api.database)


################################################################################
# @brief      collect all fx file.
#
# @param      shotObj         The shot object
# @param      includeVbd      The include vdb
# @param      includeAlembic  The include alembic
# @param      save            The save
#
def collect(shotObj, itemName, alembicList=[], vdbList=[], all=False):
    alembics = getAlembics(shotObj, itemName)
    if not all:
        removeList = [key for key in alembics.keys() if key not in alembicList]
        alembics = removeKey(alembics, removeList)

    importAlembics(alembics)

    vdbs = getVdbs(shotObj, itemName)
    if not all:
        removeList = [key for key in vdbs.keys() if key not in vdbList]
        vdbs = removeKey(vdbs, removeList)

    importVdbs(vdbs)


################################################################################
# @brief      Removes some keys.
#
# @param      inputDict  The input dictionary
# @param      keys       The keys
#
# @return     the new dictionary
#
def removeKey(inputDict, keys):
    returnDict = dict(inputDict)
    for key in keys:
        del returnDict[key]

    return returnDict


################################################################################
# @brief      Gets the export folder.
#
# @param      shotObj   The shot object
# @param      itemName  The item name
#
# @return     The export folder.
#
def getExportFolder(shotObj, itemName):
    return os.path.join(shotObj.getShotFolder(), 'Fx', 'PRJ_{}_{}_{}'.format(shotObj.episode, shotObj.sequence, shotObj.shot), 'houdini', 'export', itemName).replace('\\', '/')


################################################################################
# @brief      Gets the materials.
#
# @param      shotObj   The shot object
# @param      itemName  The item name
#
# @return     The materials.
#
def getMaterials(shotObj, itemName):
    exportFolder = getExportFolder(shotObj, itemName)
    if os.path.exists(exportFolder):
        versionJson = os.path.join(exportFolder, 'version.json').replace('\\', '/')

        return api.json.json_read(versionJson)


################################################################################
# @brief      Gets the files.
#
# @param      shotObj   The shot object
# @param      itemName  The item name
# @param      mode      The mode
#
# @return     The files.
#
def getFiles(shotObj, itemName, mode='abc'):
    files = {}

    materials = getMaterials(shotObj, itemName)

    if materials:
        for item in materials:
            if mode in materials[item]:
                version = materials[item][mode]
                alembicFolder = os.path.join(getExportFolder(shotObj, itemName), item, mode, version).replace('\\', '/')
                if os.path.exists(alembicFolder):
                    path = os.path.join(alembicFolder, '{}_{}.{}'.format(item, version, mode)).replace('\\', '/')
                    if mode == 'vdb':
                        path = path.replace('.vdb', '.#####.vdb')
                    files[item] = {'path': path, 'version': version}

    return files


################################################################################
# @brief      Gets the alembics.
#
# @param      shotObj   The shot object
# @param      itemName  The item name
#
# @return     The alembics.
#
def getAlembics(shotObj, itemName):
    return getFiles(shotObj, itemName, 'abc')


################################################################################
# @brief      Gets the vdbs.
#
# @param      shotObj   The shot object
# @param      itemName  The item name
#
# @return     The vdbs.
#
def getVdbs(shotObj, itemName):
    return getFiles(shotObj, itemName, 'vdb')


################################################################################
# @brief      import all the alembic caches.
#
# @param      alembics    The alembics
#
def importAlembics(alembics):
    if not cmd.objExists('GRP_FX'):
        cmd.group(empty=True, n='GRP_FX')

    for alembic in alembics:
        newProxy = api.vray.loadAbcInVrayProxy(alembics[alembic]['path'], 'MSH_{}_FX'.format(alembic))

        cmd.setAttr("{}_vrayproxy.animType".format(newProxy), 1)
        cmd.setAttr("{}_vrayproxy.useAlembicOffset".format(newProxy), 1)
        cmd.setAttr("{}_vrayproxy.tessellateHair".format(newProxy), 1)
        cmd.setAttr("{}_vrayproxy.subdivAllMeshes".format(newProxy), 0)

        objectsToShade = cmd.vrayUpdateProxy('{}_vrayproxy'.format(newProxy), getShaderSets=True)
        for i in range(0, len(objectsToShade)):
            objectToShade = objectsToShade[i]

            vrayMtl = cmd.shadingNode('VRayMtl', asShader=True, name='{}_{}_base{}_{}_shader'.format(os.getenv('PROJECT'), alembic, str(i).zfill(3), alembic))

            cmd.connectAttr('{}.outColor'.format(vrayMtl), '{}_vrayproxy.shaders[0].shadersConnections'.format(newProxy), force=True)
            cmd.setAttr('{}_vrayproxy.shaders[0].shadersNames'.format(newProxy), objectToShade, type='string')

        cmd.parent(newProxy, 'GRP_FX')
        api.log.logger().debug('Abc for cache {} imported'.format(alembic))


################################################################################
# @brief      create a vdb node for the caches.
#
# @param      vdbs        The vdbs
#
def importVdbs(vdbs):
    if not cmd.objExists('GRP_FX'):
        cmd.group(empty=True, n='GRP_FX')

    for vdb in vdbs:
        mel.eval('vrayCreateVolumeGrid')
        newVdb = cmd.rename('VRayVolumeGrid1', 'VDB_{}_FX'.format(vdb))
        cmd.setAttr('{}.inPath'.format(newVdb), vdbs[vdb]['path'], type='string')

        cmd.parent(newVdb, 'GRP_FX')
        api.log.logger().debug('Vdb for cache {} create'.format(vdb))
