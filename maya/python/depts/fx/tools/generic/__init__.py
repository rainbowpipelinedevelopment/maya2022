import importlib
import os

import maya.cmds as cmd

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'common')


def run_collect(*args):
    import depts.fx.tools.generic.collectUI as collectUI
    importlib.reload(collectUI)

    win = collectUI.CollectUI()
    win.show()


collectTool = {
    'name': 'Collect',
    'launch': run_collect,
    'icon': os.path.join(iconDir, "collect.png")
}

tools = [
    collectTool
]

PackageTool = {
    "name": "generic",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
