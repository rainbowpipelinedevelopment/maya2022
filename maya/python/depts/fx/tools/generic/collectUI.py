import importlib
from PySide2 import QtGui, QtCore, QtWidgets
import os
import functools

import maya.cmds as cmd

import api.database
import api.scene
import api.shot
import api.widgets.rbw_UI as rbwUI
import config.dictionaries
import depts.fx.tools.generic.collect as collect

importlib.reload(api.database)
importlib.reload(api.scene)
# importlib.reload(api.shot)
# importlib.reload(rbwUI)
importlib.reload(config.dictionaries)
importlib.reload(collect)


################################################################################
# @brief      This class describes a collect all ui.
#
class CollectUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(CollectUI, self).__init__()
        if cmd.window('CollectUI', exists=True):
            cmd.deleteUI('CollectUI')

        self.mcFolder = os.getenv('MC_FOLDER')
        self.id_mv = os.getenv('ID_MV')
        self.animationFolder = os.path.join(self.mcFolder, 'scenes', 'animation').replace('\\', '/')
        self.shots = api.system.getShotsFromDB()

        self.setObjectName('CollectUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)

        self.mainLayout.addWidget(self.splitter)

        ########################################################################
        # LEFT PANEL
        #
        self.leftWidget = rbwUI.RBWFrame(layout='V', radius=5)

        self.shotGroup = rbwUI.RBWGroupBox(title='Shot:', layout='V', fontSize=12, spacing=1)

        self.seasonComboBox = rbwUI.RBWComboBox(text='Season', bgColor='transparent')
        self.episodeComboBox = rbwUI.RBWComboBox(text='Episode', bgColor='transparent')
        self.sequenceComboBox = rbwUI.RBWComboBox(text='Sequence', bgColor='transparent')
        self.shotComboBox = rbwUI.RBWComboBox(text='Shot', bgColor='transparent')
        self.itemNameComboBox = rbwUI.RBWComboBox(text='ItemName', bgColor='transparent')

        self.seasonComboBox.activated.connect(self.fillEpisodeComboBox)
        self.episodeComboBox.activated.connect(self.fillSequenceComboBox)
        self.sequenceComboBox.activated.connect(self.fillShotComboBox)
        self.shotComboBox.activated.connect(self.fillItemNameComboBox)
        self.itemNameComboBox.activated.connect(self.fillGroupBoxes)

        self.shotGroup.addWidget(self.seasonComboBox)
        self.shotGroup.addWidget(self.episodeComboBox)
        self.shotGroup.addWidget(self.sequenceComboBox)
        self.shotGroup.addWidget(self.shotComboBox)
        self.shotGroup.addWidget(self.itemNameComboBox)

        self.leftWidget.addWidget(self.shotGroup)

        self.collectButton = rbwUI.RBWButton(text='Collect', icon=['collect.png'], size=[None, 35])
        self.collectButton.clicked.connect(self.collect)

        self.leftWidget.layout.addWidget(self.collectButton)

        self.splitter.addWidget(self.leftWidget)

        ########################################################################
        # LEFT PANEL
        #
        self.rightWidget = rbwUI.RBWFrame(layout='V', radius=5)

        # vdbs
        self.vdbsGroupBox = rbwUI.RBWGroupBox(title='Vdbs:', layout='V', fontSize=12, spacing=2)

        self.vdbsScrollWidget = QtWidgets.QScrollArea()
        self.vdbsScrollWidget.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.vdbsScrollWidget.setWidgetResizable(True)
        self.vdbsScrollWidget.setStyleSheet('background-color: transparent; border-color: transparent')

        self.vdbsWidget = rbwUI.RBWFrame(layout='V', margins=[0, 0, 3, 0], bgColor='transparent')
        self.vdbsScrollWidget.setWidget(self.vdbsWidget)

        self.vdbsGroupBox.addWidget(self.vdbsScrollWidget)

        self.rightWidget.addWidget(self.vdbsGroupBox)

        # alembics
        self.alembicsGroupBox = rbwUI.RBWGroupBox(title='Alembics:', layout='V', fontSize=12, spacing=2)

        self.alembicsScrollWidget = QtWidgets.QScrollArea()
        self.alembicsScrollWidget.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.alembicsScrollWidget.setWidgetResizable(True)
        self.alembicsScrollWidget.setStyleSheet('background-color: transparent; border-color: transparent')

        self.alembicsWidget = rbwUI.RBWFrame(layout='V', margins=[0, 0, 3, 0], bgColor='transparent')
        self.alembicsScrollWidget.setWidget(self.alembicsWidget)

        self.alembicsGroupBox.addWidget(self.alembicsScrollWidget)

        self.rightWidget.addWidget(self.alembicsGroupBox)

        self.splitter.addWidget(self.rightWidget)

        self.splitter.setStretchFactor(0, 1)
        self.splitter.setStretchFactor(1, 5)

        self.autoFillCombo()
        self.setMinimumSize(500, 250)

        self.setTitle('Collect UI')
        self.setFocus()

    ############################################################################
    # @brief      fill the shot season combo box.
    #
    def fillSeasonComboBox(self):
        self.seasonComboBox.clear()
        seasons = sorted(self.shots.keys())

        self.seasonComboBox.addItems(seasons)

        if len(seasons) == 1:
            self.seasonComboBox.setCurrentIndex(0)
            self.fillEpisodeComboBox()

    ############################################################################
    # @brief      fill the episode combo box.
    #
    def fillEpisodeComboBox(self):
        self.currentSeason = self.seasonComboBox.currentText()

        self.episodeComboBox.clear()
        episodes = sorted(self.shots[self.currentSeason].keys())

        self.episodeComboBox.addItems(episodes)

        if len(episodes) == 1:
            self.episodeComboBox.setCurrentIndex(0)
            self.fillSequenceComboBox()

    ############################################################################
    # @brief      fill the sequence combo box.
    #
    def fillSequenceComboBox(self):
        self.currentEpisode = self.episodeComboBox.currentText()

        self.sequenceComboBox.clear()
        sequences = sorted(self.shots[self.currentSeason][self.currentEpisode].keys())

        self.sequenceComboBox.addItems(sequences)

        if len(sequences) == 1:
            self.sequenceComboBox.setCurrentIndex(0)
            self.fillShotComboBox()

    ############################################################################
    # @brief      fill the shot combo box.
    #
    def fillShotComboBox(self):
        self.currentSequence = self.sequenceComboBox.currentText()

        self.shotComboBox.clear()
        shots = sorted(self.shots[self.currentSeason][self.currentEpisode][self.currentSequence].keys())

        self.shotComboBox.addItems(shots)

        if len(shots) == 1:
            self.shotComboBox.setCurrentIndex(0)

    ############################################################################
    # @brief      auto fill of all the combos.
    #
    def autoFillCombo(self):
        fileName = api.scene.scenename()
        if self.animationFolder in fileName:
            try:
                relPath = fileName.split(self.animationFolder)[1]
                season = relPath.split('/')[1]
                episode = relPath.split('/')[2]
                sequence = relPath.split('/')[3]
                shot = relPath.split('/')[4]

                self.fillSeasonComboBox()
                seasonIndex = self.seasonComboBox.findText(season, QtCore.Qt.MatchFixedString)
                self.seasonComboBox.setCurrentIndex(seasonIndex)

                self.fillEpisodeComboBox()
                episodeIndex = self.episodeComboBox.findText(episode, QtCore.Qt.MatchFixedString)
                self.episodeComboBox.setCurrentIndex(episodeIndex)

                self.fillSequenceComboBox()
                sequenceIndex = self.sequenceComboBox.findText(sequence, QtCore.Qt.MatchFixedString)
                self.sequenceComboBox.setCurrentIndex(sequenceIndex)

                self.fillShotComboBox()
                shotIndex = self.shotComboBox.findText(shot, QtCore.Qt.MatchFixedString)
                self.shotComboBox.setCurrentIndex(shotIndex)
                self.fillItemNameComboBox()

            except:
                self.fillSeasonComboBox()
        else:
            self.fillSeasonComboBox()

    ############################################################################
    # @brief      fill itemName combo bocx.
    #
    def fillItemNameComboBox(self):
        self.currentShot = self.shotComboBox.currentText()
        self.shotObj = api.shot.Shot(params={'season': self.currentSeason, 'episode': self.currentEpisode, 'sequence': self.currentSequence, 'shot': self.currentShot, 'id_mv': self.id_mv})

        fXTasksJson = os.path.join(self.shotObj.getShotFolder(), 'Fx', 'taskNames.json').replace('\\', '/')

        if os.path.exists(fXTasksJson):
            taskNames = api.json.json_read(fXTasksJson)['taskNames']
            self.itemNameComboBox.clear()
            self.itemNameComboBox.addItems(taskNames)
        else:
            rbwUI.RBWError(text='No Fx found under the selected shot.')

    ############################################################################
    # @brief      fille the group boxes.
    #
    def fillGroupBoxes(self):
        self.currentItemName = self.itemNameComboBox.currentText()

        for widget in self.alembicsWidget.widget.children():
            if isinstance(widget, rbwUI.RBWCheckBox):
                widget.setParent(None)
                widget.deleteLater()

        for widget in self.vdbsWidget.widget.children():
            if isinstance(widget, rbwUI.RBWCheckBox):
                widget.setParent(None)
                widget.deleteLater()

        self.alembics = {}
        for alembic in collect.getAlembics(self.shotObj, self.currentItemName):
            checkBox = rbwUI.RBWCheckBox(alembic)
            checkBox.stateChanged.connect(functools.partial(self.updateAlembics, alembic))
            self.alembicsWidget.addWidget(checkBox)
            self.alembics[alembic] = False

        self.vdbs = {}
        for vdb in collect.getVdbs(self.shotObj, self.currentItemName):
            checkBox = rbwUI.RBWCheckBox(vdb)
            checkBox.stateChanged.connect(functools.partial(self.updateVdbs, vdb))
            self.vdbsWidget.addWidget(checkBox)
            self.vdbs[vdb] = False

    ############################################################################
    # @brief      update alembic seleted status.
    #
    # @param      alembic  The alembic
    # @param      state    The state
    #
    def updateAlembics(self, alembic, state):
        if state == 2:
            self.alembics[alembic] = True
        else:
            self.alembics[alembic] = False

    ############################################################################
    # @brief      update vdb seleted status.
    #
    # @param      alembic  The vdb
    # @param      state    The state
    #
    def updateVdbs(self, vdb, state):
        if state == 2:
            self.vdbs[vdb] = True
        else:
            self.vdbs[vdb] = False

    ############################################################################
    # @brief      collect function
    #
    def collect(self):
        alembics = [alembic for alembic in self.alembics if self.alembics[alembic]]
        vdbs = [vdb for vdb in self.vdbs if self.vdbs[vdb]]

        if not(len(alembics) == 0 and len(vdbs) == 0):
            exportFolder = collect.getExportFolder(self.shotObj, self.currentItemName)
            if os.path.exists(exportFolder):
                collect.collect(shotObj=self.shotObj, itemName=self.currentItemName, alembicList=alembics, vdbList=vdbs)
            else:
                rbwUI.RBWError(text='No pipeline exports find under the given name for selected shot')
                return
