import importlib

import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI
import api.savedAsset
import common.loadAndSaveAssetUI

importlib.reload(rbwUI)
importlib.reload(api.savedAsset)
importlib.reload(common.loadAndSaveAssetUI)

################################################################################
# @brief      This class describes a load and save rigging ui.
#
class LoadAndSaveFxUI(common.loadAndSaveAssetUI.AssetUI):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(LoadAndSaveFxUI, self).__init__('Fx')

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        super(LoadAndSaveFxUI, self).initUI()

        self.saveCreateCachesSetLabel = rbwUI.RBWLabel(text='Create Caches Set', italic=False, size=12)
        self.saveCreateCachesSetSwitch = rbwUI.RBWSwitch()

        self.saveInfoGroup.addRow(self.saveCreateCachesSetLabel, self.saveCreateCachesSetSwitch)

    ############################################################################
    # @brief      save function.
    #
    def save(self):
        try:
            savedAssetParam = {
                'asset': self.currentAsset,
                'dept': self.deptName.lower(),
                'deptType': self.currentDeptType,
                'approvation': self.currentApprovation,
                'note': self.saveNotesTextEdit.toPlainText(),
                'cacheSet': self.saveCreateCachesSetSwitch.isChecked()
            }

            if self.saveExportSelectionSwitch.isChecked():
                savedAssetParam['selection'] = cmd.ls(selection=True)[0]
            else:
                savedAssetParam['selection'] = None

            if self.currentApprovation == 'def':
                if not self.checkToSave():
                    return

                wipSavedAssetParam = {
                    'asset': self.currentAsset,
                    'dept': self.deptName.lower(),
                    'deptType': self.currentDeptType,
                    'approvation': 'wip',
                    'note': 'Pipeline autosave before def save',
                    'cacheSet': self.saveCreateCachesSetSwitch.isChecked(),
                    'selection': savedAssetParam['selection']
                }

                wipSavedAsset = api.savedAsset.SavedAsset(params=wipSavedAssetParam)
                wipSuccess = wipSavedAsset.save()

                if wipSuccess:
                    savedAssetParam['parent'] = wipSavedAsset.db_id
                    savedAsset = api.savedAsset.SavedAsset(params=savedAssetParam)
                    success = savedAsset.save()
                    if success:
                        self.updateWipParentOnDB(wipSavedAsset, savedAsset)
                else:
                    rbwUI.RBWError(title='ATTENTION', text='Problems during autosave wip.\nPleas control the scene and make a new save.')
                    return
            else:
                savedAsset = api.savedAsset.SavedAsset(params=savedAssetParam)
                success = savedAsset.save()

            if success:
                rbwUI.RBWConfirm(title='Save {} Complete'.format(self.deptName), text='Asset Save Complete', defCancel=None)
                self.fillSavedAssetsTree()
            else:
                rbwUI.RBWError(title='ATTENTION', text='Some Error During Save')

        except AttributeError:
            rbwUI.RBWError(title='ATTENTION', text='One between deptType or approvation are not properly selected.\nSelect those and try again.')

    ############################################################################
    # @brief      check function
    #
    def checkToSave(self):
        super(LoadAndSaveFxUI, self).checkToSave()

        if self.currentDeptType == 'ani':

            if not self.checkTextureExt():
                return

        return True
