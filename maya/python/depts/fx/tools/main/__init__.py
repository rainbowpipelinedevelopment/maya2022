import importlib
import os

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'arakno')


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_loadAndSaveMcFx(*args):
    import depts.fx.tools.main.loadAndSaveFxUI as loadAndSaveFxUI
    importlib.reload(loadAndSaveFxUI)

    win = loadAndSaveFxUI.LoadAndSaveFxUI()
    win.show()


################################################################################
# @brief      run load and save UI
#
# @param      args  The arguments
#
def run_loadAndSaveSc(*args):
    import common.loadAndSaveShotUI as lss
    importlib.reload(lss)

    win = lss.ShotUI()
    win.show()


loadAndSaveMCFxTool = {
    "name": "L/S MC Fx",
    "launch": run_loadAndSaveMcFx,
    "icon": os.path.join(iconDir, "loadAndSaveFx.png"),
    "statustip": "Load & Save MC Fx"
}

loadAndSaveScTool = {
    "name": "L/S SC Fx",
    "launch": run_loadAndSaveSc,
    "icon": os.path.join(iconDir, "loadAndSaveFx.png"),
    "statustip": "Load & Save"
}


tools = [
    loadAndSaveMCFxTool,
    loadAndSaveScTool
]

PackageTool = {
    "name": "main",
    "tools": tools
}
