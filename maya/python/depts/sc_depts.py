import importlib
import os
import xml.etree.cElementTree as ET
import xml.dom.minidom as xdm
import time

import maya.cmds as cmd
import maya.mel as mel

import api.log
import api.database
import api.scene
import api.camera
import api.shotgrid
import api.assembly
import api.savedAsset
import api.exception
import api.widgets.rbw_UI as rbwUI
import utilz.snapshot
import config.dictionaries
import common.checkUpdate.checkUpdateUI as checkUpdateUI
import depts.lighting.tools.sceneSetup.sceneSetup as setup
import common.bakeConstraints as bakeConstraints

importlib.reload(api.log)
importlib.reload(api.database)
importlib.reload(api.scene)
importlib.reload(api.camera)
importlib.reload(api.shotgrid)
# importlib.reload(api.assembly)
# importlib.reload(api.savedAsset)
importlib.reload(api.exception)
# importlib.reload(rbwUI)
importlib.reload(utilz.snapshot)
importlib.reload(config.dictionaries)
importlib.reload(checkUpdateUI)
importlib.reload(setup)
importlib.reload(bakeConstraints)


################################################################################
# @brief      This class describes a dept.
#
class Dept(object):

    ############################################################################
    # Constructs a new instance.
    #
    #
    def __init__(self, savedShot):
        self.name = "dept"
        self.savedShot = savedShot

        if cmd.objExists("_UNKNOWN_REF_NODE_"):
            cmd.delete("_UNKNOWN_REF_NODE_")
        if cmd.objExists("*:_UNKNOWN_REF_NODE_"):
            try:
                cmd.delete("*:_UNKNOWN_REF_NODE_")
            except:
                pass

        self.id_mv = os.getenv('ID_MV')
        self.projectName = os.getenv('PROJECT')
        self.projectFolder = os.getenv('MC_FOLDER').replace('\\', '/')
        self.tempFolder = os.path.join(os.getenv('HOMEPATH'), 'Documents', 'RBW_maya_tmp').replace('\\', '/')

        self.getFPS()
        self.getRES()

        try:
            self.path = os.path.join(self.savedShot.savedShotFolder, '{}.ma'.format(self.savedShot.sceneName)).replace('\\', '/')
        except AttributeError:
            self.path = os.path.join(self.projectFolder, self.savedShot.path).replace('\\', '/')

        self.shotgridObj = api.shotgrid.ShotgridObj()

        self.errors = []

    ############################################################################
    # @brief      set the project FPS
    #
    def getFPS(self):
        queryFPS = "SELECT name, fps FROM `p_prj_framerate` AS prjf JOIN `p_framerate` AS fr ON prjf.framerate_id=fr.id WHERE prjf.id_mv={}".format(self.id_mv)
        res = api.database.selectSingleQuery(queryFPS)
        self.fps = res[0].lower()
        self.framerate = int(res[1])

        if self.fps.count('frame'):
            self.fps = self.fps.split('frame')[0]

    ############################################################################
    # @brief      set the project FPS
    #
    def getRES(self):
        queryRES = "SELECT `res_width`, `res_height` FROM `V_projectSettings` WHERE `projectId` = {}".format(self.id_mv)
        res = api.database.selectSingleQuery(queryRES)

        self.prjWidth = res[0]
        self.prjHeight = res[1]

    ############################################################################
    # @brief      set the project FPS
    #
    def setFPS(self):
        cmd.currentUnit(time=self.fps)

    ############################################################################
    # @brief      load function.
    #
    def load(self, mode='load', confirm=True):
        if mode == 'load':
            # check if there are unsaved changes
            fileCheckState = cmd.file(q=True, modified=True)
            # if there are, save them first ... then we can proceed
            if fileCheckState and confirm:
                api.log.logger().debug('Need to save...')
                openScenePath = str(cmd.file(query=True, sceneName=True))
                if openScenePath:
                    answer = rbwUI.RBWWarning(title='Warning', text='There are unsaved changes.', defOk=['Load', 'ok.png'], defCancel=['Skip', 'cancel.png']).result()
                    if not answer:
                        api.log.logger().debug("Load action skipped... Please procede to save your opened scene!")
                        return

            cmd.file(new=True, force=True)
            cmd.file(self.path, open=True, force=True)

            # FPS
            self.setFPS()

        elif mode == 'merge':
            cmd.file(self.path, i=True, options="v=0;p=17", loadReferenceDepth="all")

    ############################################################################
    # @brief      sc save method
    #
    def save(self):
        api.camera.lockUnlockCamera(lock=False)
        self.realSave()
        self.exportXML()
        return True

    ############################################################################
    # @brief      rename the scene and then save.
    #
    # @param      path    The path
    # @param      format  The format
    #
    def renameAndSave(self, path, format):
        self.setFPS()

        remotePath = path
        cmd.file(rename=remotePath)
        api.log.logger().debug("Saving to {}".format(remotePath))
        cmd.file(save=True, force=True, type=format)

    ############################################################################
    # @brief      real save function.
    #
    def realSave(self):
        api.log.logger().debug('realSave maya ascii (default)')
        api.log.logger().debug('server save')

        # some cleanup operations
        api.scene.badPluginCleanUp()

        if cmd.unknownPlugin(query=True, list=True) is not None:
            api.scene.pluginCheck()

        # special clean up
        api.scene.specialCleanup()

        # real save maya binary
        mbPath = os.path.join(self.savedShot.savedShotFolder, '{}.mb'.format(self.savedShot.sceneName)).replace('\\', '/')
        self.renameAndSave(mbPath, 'mayaBinary')
        api.log.logger().debug('mb saved in: {}'.format(mbPath))

        # real save maya ascii
        try:
            maPath = mbPath.replace('.mb', '.ma')
            self.renameAndSave(maPath, 'mayaAscii')
            api.log.logger().debug('ma saved in: {}'.format(maPath))
            cmd.file(rename=maPath)
        except RuntimeError:
            api.log.logger().debug('Error: Cannot save in Maya Ascii!')

    ############################################################################
    # @brief      export the xml file.
    #
    def exportXML(self):
        xmlPath = os.path.join(self.savedShot.savedShotFolder, '{}.xml'.format(self.savedShot.sceneName)).replace('\\', '/')

        sceneRoot = ET.Element('scene')
        assetsRoot = ET.SubElement(sceneRoot, 'assets')

        saveNodes = api.scene.getSaveNodes()
        for saveNode in saveNodes:
            if cmd.listConnections(saveNode) is not None:
                try:
                    savedAsset = api.savedAsset.SavedAsset(saveNode=saveNode)
                    ET.SubElement(assetsRoot, 'asset', name=saveNode, id=str(savedAsset.getAssetId()), dept=savedAsset.dept, savedAssetId=str(savedAsset.db_id))
                except api.exception.InvalidSavedAsset:
                    continue

        assembliesRoot = ET.SubElement(sceneRoot, 'assemblies')
        assemblies = self.listAssemblies()
        for assembly in assemblies:
            assemblyNode = assemblies[assembly][0]
            definitionRelPath = '/'.join(cmd.getAttr('{}.definition'.format(assemblyNode)).split('/')[7:])
            ET.SubElement(assembliesRoot, 'assembly', name=assembly, path=definitionRelPath, id=str(self.getAssemblyRecord(definitionRelPath)[0]))

        ET.SubElement(sceneRoot, 'comment', value=self.savedShot.note)
        ET.SubElement(sceneRoot, 'user', name=os.getenv('USERNAME'))
        ET.SubElement(sceneRoot, 'currentFrame', value=str(cmd.currentTime(query=True)))
        ET.SubElement(sceneRoot, 'shotId', value=str(self.getShotId()))
        ET.SubElement(sceneRoot, 'savedShotId', value=str(self.savedShot.db_id))
        ET.SubElement(sceneRoot, 'saveDate', value=str(self.savedShot.date))

        if sceneRoot:
            tree = ET.ElementTree(sceneRoot)
            tree.write(xmlPath)

            xml = xdm.parse(xmlPath)
            pretty_xml_as_string = xml.toprettyxml(indent='\t')

            with open(xmlPath, "w") as f:
                f.write(pretty_xml_as_string)

            return True

    ################################################################################
    # @brief      list assemblies
    #
    # @return     a dict with assemblies infos.
    #
    def listAssemblies(self):
        assemblies = {}

        for assembly in cmd.ls(type='assemblyReference'):
            assemblyPath = cmd.getAttr('{}.definition'.format(assembly))
            assetFullName = '_'.join(assemblyPath.split('/')[8:12])
            if assetFullName not in assemblies:
                assemblies[assetFullName] = [assembly]
            else:
                assemblies[assetFullName].append(assembly)

        return assemblies

    ############################################################################
    # @brief      { function_description }
    #
    def sanizeVrscene(self):
        vrscenes = cmd.ls(type='VRayScene')

        for vrscene in vrscenes:
            if ':' not in vrscene:
                vrsceneInfo = vrscene.split('_')
                if vrsceneInfo[-1] != 'Shape':
                    vrsceneInfo[-1] = 'Shape'
                    itemName = '_'.join(vrsceneInfo[:-1])
                    rightVrscene = '_'.join(vrsceneInfo)
                    cmd.rename(vrscene, rightVrscene)

                    expressionString = cmd.expression('{}_expression'.format(itemName), string=True, query=True)
                    newExpressionString = expressionString.replace(vrscene, rightVrscene)
                    cmd.expression('{}_expression'.format(itemName), string=newExpressionString, edit=True)

    ############################################################################
    # @brief      Attaches the lgt shaders.
    #
    # @param      node  The node
    #
    def attachLgtShaders(self, node):
        if node.split(':')[0].endswith('RND'):
            namespace = node.split(':')[0][:-3]
        elif node.split(':')[0].endswith('FX'):
            namespace = node.split(':')[0][:-2]

        setup.checkLightingMaterials()

        if namespace.split('_')[3] != '':
            shaderString = '{}_*_switchOvr_{}{}{}_shader'.format(self.projectName, namespace.split('_')[1], namespace.split('_')[2].capitalize(), namespace.split('_')[3])
        else:
            shaderString = '{}_*_switchOvr_{}{}_shader'.format(self.projectName, namespace.split('_')[1], namespace.split('_')[2].capitalize())

        api.log.logger().debug(shaderString)

        for shader in cmd.ls(shaderString):
            cmd.connectAttr('sl_assetName_trace_meshName_shader.outColor', '{}.material_1'.format(shader), force=True)
            cmd.connectAttr('sl_assetName_reflective_meshName_shader.outColor', '{}.material_2'.format(shader), force=True)

    ############################################################################
    # @brief      Gets the assembly identifier.
    #
    # @param      assemblyDefinition  The assembly definition
    #
    # @return     The assembly identifier.
    #
    def getAssemblyRecord(self, assemblyDefinition):
        assemblyIdQuery = "SELECT a.id, ast.shotgunId FROM `assembly` AS a JOIN `asset` AS ast ON a.assetId=ast.id WHERE a.path = '{}'".format(assemblyDefinition)
        result = api.database.selectSingleQuery(assemblyIdQuery)

        return result

    ############################################################################
    # @brief      Gets the shot obj.
    #
    # @return     The shot.
    #
    def getShotObj(self):
        return self.savedShot.getShotObj()

    ############################################################################
    # @brief      Gets the season.
    #
    # @return     The season.
    #
    def getSeason(self):
        return self.savedShot.getSeason()

    ############################################################################
    # @brief      Gets the episode.
    #
    # @return     The episode.
    #
    def getEpisode(self):
        return self.savedShot.getEpisode()

    ############################################################################
    # @brief      Gets the sequence.
    #
    # @return     The sequence.
    #
    def getSequence(self):
        return self.savedShot.getSequence()

    ############################################################################
    # @brief      Gets the shot.
    #
    # @return     The shot.
    #
    def getShot(self):
        return self.savedShot.getShot()

    ############################################################################
    # @brief      Gets the dept.
    #
    # @return     The dept.
    #
    def getDept(self):
        return self.savedShot.dept

    ############################################################################
    # @brief      Gets the depttype.
    #
    # @return     The depttype.
    #
    def getDeptType(self):
        return self.savedShot.deptType

    ############################################################################
    # @brief      Gets the item name.
    #
    # @return     The item name.
    #
    def getItemName(self):
        return self.savedShot.itemName

    # shotIdSg

    ############################################################################
    # @brief      Gets the season identifier sg.
    #
    # @return     The season identifier sg.
    #
    def getSeasonIdSg(self):
        return self.savedShot.getSeasonIdSg()

    ############################################################################
    # @brief      Gets the episode identifier sg.
    #
    # @return     The episode identifier sg.
    #
    def getEpisodeIdSg(self):
        return self.savedShot.getEpisodeIdSg()

    ############################################################################
    # @brief      Gets the sequence identifier sg.
    #
    # @return     The sequence identifier sg.
    #
    def getSequenceIdSg(self):
        return self.savedShot.getSequenceIdSg()

    ############################################################################
    # @brief      Gets the shot identifier sg.
    #
    # @return     The shot identifier sg.
    #
    def getShotIdSg(self):
        return self.savedShot.getShotIdSg()

    ############################################################################
    # @brief      Gets the season identifier.
    #
    # @return     The season identifier.
    #
    def getSeasonId(self):
        return self.savedShot.getSeasonId()

    ############################################################################
    # @brief      Gets the episode identifier.
    #
    # @return     The episode identifier .
    #
    def getEpisodeId(self):
        return self.savedShot.getEpisodeId()

    ############################################################################
    # @brief      Gets the sequence identifier.
    #
    # @return     The sequence identifier.
    #
    def getSequenceId(self):
        return self.savedShot.getSequenceId()

    ############################################################################
    # @brief      Gets the shot identifier.
    #
    # @return     The shot identifier.
    #
    def getShotId(self):
        return self.savedShot.getShotId()


################################################################################
# @brief      This class describes an animation.
#
class Animation(Dept):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      savedShot  The saved shot
    #
    def __init__(self, savedShot, showDialog=True):
        super().__init__(savedShot)

        self.name = 'Anim'
        self.isPromoted = True if self.savedShot.promoted == 1 else False
        self.showDialog = showDialog

    ############################################################################
    # @brief      detect un-wanted top groups in the scene
    #
    # @param      self  The object
    #
    # @return     all the un-wanted top groups
    #
    def unwantedTopGroups(self):
        top_nodes = cmd.ls(assemblies=True)
        defaultNodes = ['persp', 'top', 'front', 'side', 'CHARACTER', 'PROP', 'SET', 'SET_EDIT', 'LIBRARY', 'FX', 'EXTRA', 'SPECIAL']

        unwatedTopGroups = []
        for grp in top_nodes:
            if grp not in defaultNodes:
                if grp == 'PROXY':
                    if not cmd.objExists('EXTRA'):
                        cmd.group(empty=True, name="EXTRA")
                    cmd.parent(grp, "EXTRA")
                else:
                    unwatedTopGroups.append(grp)

        if len(unwatedTopGroups):
            cmd.select(unwatedTopGroups, replace=True)
            mess = 'There are some items in the Outliner out of the category groups, please check!'
            cmd.headsUpMessage(mess, time=1)
            api.log.logger().debug('\n\n{}\n'.format(mess))
            return unwatedTopGroups

    ################################################################################
    # @brief      sets the right settings for the Viewport 2.0
    #
    def setViewportSettings(self):
        mel.eval('generateAllUvTilePreviews')
        mel.eval('setAttr "hardwareRenderingGlobals.alphaCutPrepass" 1;')

    ############################################################################
    # @brief      animation load function
    #
    # @param      mode  The mode
    #
    def load(self, mode='load', confirm=True):
        super().load(mode=mode, confirm=confirm)

        self.checkFunction()

        self.unwantedTopGroups()
        self.setViewportSettings()

        win = checkUpdateUI.CheckUpdateUI('animation')
        win.show()

    def getCharDistance(self):
        # necessary variables names
        cam = '*:Render_Cam'
        allRigNodes = api.scene.getSaveNodes(dept='rigging')
        headNames = config.dictionaries.headNames

        # check necessary elements before to start
        if self.getDept() in ['Pri', 'Lay']:
            api.log.logger().debug("Distance Camera it's not for Layout or Primary...")
            return

        if not cmd.objExists(cam):
            api.log.logger().debug("There is not the camera-rig the procedure cannot work, please check.")
            return
        else:
            cmd.select(cam, replace=True)
            camFound = cmd.ls(selection=True)
            if len(camFound) > 1:
                api.log.logger().debug("There are more than one camera-rig, please check.")
                return

        if not allRigNodes:
            api.log.logger().debug("There aren't rig in this scene, the procedure cannot work, please check.")
            return

        distDict = {}
        distDict['camFocal'] = []

        # detect all the character-rig in the opened scene
        allCharNamespaces = []
        for rig in allRigNodes:
            if cmd.getAttr('{}.genre'.format(rig)) in ['character', 'prop']:
                namspace = rig.split(':')[0]
                allCharNamespaces.append(namspace)

        # detect all the head-joints in the opened scene
        headJoints = []
        for namespace in allCharNamespaces:
            for head in headNames:
                headJoint = '{}:{}'.format(namespace, head)
                if cmd.objExists(headJoint):
                    headJoints.append(str(headJoint))
                    distDict[namespace] = []
                    break

        # get the current frame and the whole time range
        start = cmd.playbackOptions(q=True, min=True)
        end = cmd.playbackOptions(q=True, max=True)

        for i in range(int(start), int(end + 1)):
            # frame by frame detect all distances
            cmd.currentTime(i)
            cameraPosition = cmd.xform(camFound, q=1, ws=1, t=1)
            focal = cmd.getAttr('{}Shape.fl'.format(camFound[0]))
            distDict['camFocal'].append(focal)

            for head in headJoints:
                heads = cmd.ls(head)
                fullHeadNode = heads[0]
                headPosition = cmd.xform(fullHeadNode, q=1, ws=1, t=1)
                melCmd = "mag(" + str(cameraPosition).replace('[', '<<').replace(']', '>>') + " - " + str(headPosition).replace('[', '<<').replace(']', '>>') + ");"
                distance = mel.eval(melCmd)
                distDict[str(head.split(':')[0])].append(distance)

                cmd.select(clear=True)

        minDistDict = {}
        for keys, values in distDict.items():
            if keys == 'camFocal':
                minDistDict[keys] = int(max(values))
            else:
                minDistDict[keys] = int(min(values))

        distanceJsonPath = os.path.join(self.savedShot.savedShotFolder, '{}_distances.json'.format(self.savedShot.sceneName)).replace('\\', '/')
        api.json.json_write(minDistDict, distanceJsonPath)

    ############################################################################
    # @brief      switch to hires rig.
    #
    def switchToHiResRig(self):
        # switch to HiRes all the rig if it's possible
        for ctrl in cmd.ls('*:Transform_Ctrl'):
            try:
                # force to HiRes for the production playblast
                cmd.setAttr('{}.Geo_Res'.format(ctrl), 0)
            except:
                pass

    ##################################################################################
    # @brief      Checks and replaces absolute texture files paths with relative ones
    #
    def checkTexturesPath(self):
        api.log.logger().debug('starts --> checkTexturesPath() function')
        textures = cmd.ls(type='file')
        wrongPaths = []
        if textures:
            for texture in textures:
                texturePath = cmd.getAttr('{}.fileTextureName'.format(texture))
                if not (texturePath.startswith('sourceimages/') or (texturePath.count('sourceimages/') and texturePath.startswith('//speed.nas/'))) and texturePath and (texturePath.count('.png') or texturePath.count('.tiff')):
                    try:
                        rightPath = os.path.join('sourceimages', texturePath.split('sourceimages/')[1]).replace('\\', '/')
                        cmd.setAttr('{}.fileTextureName'.format(texture), rightPath, type='string')
                    except IndexError:
                        wrongPaths.append(texturePath)

            if wrongPaths:
                api.log.logger().debug('Wrong Texture path which cant be automatically fixed:')
                infoMex = "These texture paths cant be fixed automatically:\n"
                for texture in wrongPaths:
                    api.log.logger().debug(' - {}'.format(texture))
                    infoMex = '{}\n - {}'.format(infoMex, texture)
                infoMex = '{}\n\nPlease consider to fix them manually,\nyou can find them in the script editor'.format(infoMex)
                rbwUI.RBWConfirm(title="Info", text=infoMex, icon="information", defCancel=None)

        api.log.logger().debug('ends --> checkTexturesPath() function')

    ############################################################################
    # @brief      updateshot duration record.
    #
    def updateDuration(self):
        api.log.logger().debug('starts --> updateDuration() function')
        selectQuery = "SELECT `duration` FROM `shot` WHERE `id`= '{}'".format(self.getShotId())

        sceneDuration = int(cmd.playbackOptions(query=True, animationEndTime=True))

        dbShotDuration = api.database.selectSingleQuery(selectQuery)[0]

        if dbShotDuration != sceneDuration:
            updateQuery = "UPDATE `shot` SET `duration`={} WHERE `id`={}".format(sceneDuration, self.getShotId())
            api.database.deleteQuery(updateQuery)

        sgDurationInfo = self.shotgridObj.getShotDuration(self.getShotIdSg())

        sgCutDuration = sgDurationInfo['sg_cut_duration']
        sgFrameCount = sgDurationInfo['sg_frame_count']

        if sgCutDuration != sceneDuration or sgFrameCount != sceneDuration:
            self.shotgridObj.updateShotDuration(self.getShotIdSg(), sceneDuration)

        api.log.logger().debug('ends --> updateDuration() function')

    ############################################################################
    # @brief      Links an asset on shotgun.
    #
    def linkAssetOnShotgun(self):
        api.log.logger().debug('starts --> linkAssetOnShotgun() function')
        riggingSaveNodes = api.scene.getSaveNodes(dept='rigging')
        setSaveNodes = api.scene.getSaveNodes(dept='set')
        fxSaveNodes = api.scene.getSaveNodes(dept='fx')

        saveNodes = riggingSaveNodes + setSaveNodes + fxSaveNodes

        assets = []
        for saveNode in saveNodes:
            try:
                savedAsset = api.savedAsset.SavedAsset(saveNode=saveNode)
                assets.append({'type': 'Asset', 'id': savedAsset.getShotgunId()})
            except api.exception.InvalidSavedAsset:
                continue

        self.shotgridObj.updateShotSubAssets(self.getShotIdSg(), assets)
        api.log.logger().debug('ends --> linkAssetOnShotgun() function')

    ############################################################################
    # @brief      save function.
    #
    def save(self):
        if self.checkFunction():
            if self.isPromoted:
                self.exportCam()
                self.getCharDistance()
                self.mergeAllAnimLayer()
                self.bakeAllConstraint()

                # New for UE Projects
                if self.savedShot.shot.isUeProject():
                    self.exportUE()

            self.switchToHiResRig()
            self.checkTexturesPath()

            super().save()

            self.updateDuration()
            self.linkAssetOnShotgun()
            return True

    ############################################################################
    # @brief      merge all anim layer together.
    #
    def mergeAllAnimLayer(self):
        animLayers = cmd.ls(type='animLayer')
        if len(animLayers) > 1:
            cmd.select(animLayers)
            for layer in animLayers:
                mel.eval('animLayerEditorOnSelect "{}" 1;'.format(layer))
            mel.eval('performAnimLayerMerge 0;')

    def bakeAllConstraint(self):
        allConstraints = cmd.ls(type='constraint')
        constraintsNotInReference = []
        for constraint in allConstraints:
            if not constraint.count(':'):
                api.log.logger().debug("Constraint not in reference: {}".format(constraint))
                constraintsNotInReference.append(constraint)
        if len(constraintsNotInReference):
            api.log.logger().debug("Running bake_constraints procedure...")
            bakeConstraints.bakeConstraints()

    ############################################################################
    # @brief      export the camera
    #
    # @param      updateShotgun  bool, toggle the SG data update
    #
    def exportCam(self):
        cameraSavedShotParam = {
            'shot': self.savedShot.shot,
            'dept': 'Cam',
            'version': '{}_{}'.format(self.getDept(), self.savedShot.version),
            'note': 'Export from {}'.format(self.savedShot.sceneName)
        }

        time.sleep(5)
        cameraSavedShot = api.savedShot.SavedShot(params=cameraSavedShotParam)
        cameraSavedShot.save()

    ############################################################################
    # @brief      Gets the real references.
    #
    # @return     The real references.
    #
    def getRealReferences(self):
        referenceNodes = cmd.ls(type='reference')
        referenceNodes.sort()

        realReferences = {}
        unLoadedReferences = []
        for referenceNode in referenceNodes:
            if "sharedReferenceNode" not in referenceNode and 'UNKNOWN_REF_NODE' not in referenceNode:
                try:
                    if cmd.referenceQuery(referenceNode, isLoaded=True):
                        realReferences[referenceNode] = self.getReferenceInfoes(referenceNode)
                    else:
                        unLoadedReferences.append(referenceNode)
                except:
                    pass

        if len(unLoadedReferences) > 0 and self.name == 'Sec':
            refListForWarning = '\n'.join(unLoadedReferences[0:])
            api.log.logger().debug('{}\n\n\n'.format(refListForWarning))

            infoMex = "Questa scena presenta delle reference unloaded.\nSi prega di scegliere se tenerli caricati o rimuoverli per sempre.\nControllare lo script editor per l'elenco dettagliato."
            if self.showDialog:
                choosen = rbwUI.RBWWarning(title="Attention", text=infoMex, defOk=['KEEP (Load all reference)', 'ok.png'], defCancel=['KILL (Remove unloaded-reference)', 'cancel.png']).result()
            else:
                choosen = False

            if choosen:
                for referenceNode in unLoadedReferences:
                    cmd.file(loadReference=referenceNode)
                    realReferences[referenceNode] = self.getReferenceInfoes(referenceNode)
            else:
                for referenceNode in unLoadedReferences:
                    try:
                        referencePath = cmd.referenceQuery(referenceNode, filename=True, unresolvedName=True, withoutCopyNumber=True)
                        api.log.logger().debug('working on referenceNode: {}'.format(referenceNode))
                        cmd.file(referencePath, removeReference=1)
                    except RuntimeError:
                        pass

            # delete fosterParents
            for fosterParent in cmd.ls(type='fosterParent'):
                cmd.delete(fosterParent)

        return realReferences

    ############################################################################
    # @brief      Gets the reference infoes.
    #
    # @param      referenceNode  The reference node
    #
    # @return     The reference infoes.
    #
    def getReferenceInfoes(self, referenceNode):
        referencePath = cmd.referenceQuery(referenceNode, filename=True, unresolvedName=True, withoutCopyNumber=True)
        uniquePath = cmd.referenceQuery(referenceNode, filename=True, unresolvedName=True)
        referenceNamespace = cmd.referenceQuery(referenceNode, namespace=True)
        topGroup = cmd.referenceQuery(referenceNode, nodes=True)[0]
        saveNode = cmd.listConnections(topGroup, destination=False, source=True, type='saveNode')[0]
        return {'path': referencePath, 'uniquePath': uniquePath, 'namespace': referenceNamespace, 'topGroup': topGroup, 'saveNode': saveNode}

    ############################################################################
    # @brief      check the reference status.
    #
    # @param      references  The references
    #
    # @return     the bad references if exists
    #
    def checkBadReferences(self, references):
        api.log.logger().debug('starts --> checkBadReferences() function')
        badReferences = []
        for referenceNode in references:
            saveNode = references[referenceNode]['saveNode']
            try:
                api.savedAsset.SavedAsset(saveNode=saveNode)
            except api.exception.InvalidSavedAsset:
                badReferences.append(referenceNode)

        api.log.logger().debug('ends --> checkBadReferences() function')
        return badReferences

    ############################################################################
    # @brief      check the reference path.
    #
    # @param      references  The references
    #
    def checkReferencePath(self, references):
        api.log.logger().debug('starts --> checkReferencePath() function')

        fixed = []
        for referenceNode in references:
            api.log.logger().debug('work on reference: {}'.format(referenceNode))
            unresolvedPath = references[referenceNode]['path']
            if not unresolvedPath.startswith('scenes/'):
                toTrim = unresolvedPath.find("scenes")
                newUnresolvedPath = unresolvedPath[toTrim:]
                cmd.file(newUnresolvedPath, loadReference=referenceNode)
                api.log.logger().debug('fix path for {}'.format(referenceNode))
                references[referenceNode]['path'] = unresolvedPath

        api.log.logger().debug('ends --> checkReferencePath() function')
        return fixed

    ############################################################################
    # @brief      check unwanted modeling reference.
    #
    def checkUnwantedModelingReferences(self, references):
        api.log.logger().debug('starts --> checkUnwantedModelingReferences() function')
        modelingPublish = []

        for referenceNode in references:
            saveNode = references[referenceNode]['saveNode']
            if cmd.getAttr('{}.dept'.format(saveNode)) == 'modeling':
                modelingPublish.append(referenceNode)

        if modelingPublish:
            message = 'There are some modeling publish in reference, please check:'
            for modelingPub in modelingPublish:
                message = '{}\n- {}'.format(message, modelingPub)
            api.log.logger().debug(message)
            api.log.logger().debug(modelingPublish)
            if self.showDialog:
                rbwUI.RBWError(title='ERROR', text=message)
            api.log.logger().debug('starts --> checkUnwantedModelingReferences() function')
            return False
        else:
            api.log.logger().debug('starts --> checkUnwantedModelingReferences() function')
            return True

    ############################################################################
    # @brief      checks all reference approvation
    #
    # @param      references  The references
    #
    def checkApprovation(self, references):
        api.log.logger().debug('starts --> checkSaveNodes() function')

        wipReferences = []
        for referenceNode in references:
            saveNode = references[referenceNode]['saveNode']
            if cmd.getAttr('{}.approvation'.format(saveNode)) == 'wip':
                wipReferences.append(referenceNode)

        if wipReferences:
            message = 'There are some wip publish in reference, please check:'
            api.log.logger().debug(message)
            api.log.logger().debug(wipReferences)
            if self.showDialog:
                rbwUI.RBWError(title='ERROR', text=message)
            api.log.logger().debug('ends --> checkSaveNodes() function')
            return False
        else:
            api.log.logger().debug('ends --> checkSaveNodes() function')
            return True

    ############################################################################
    # @brief      { function_description }
    #
    # @param      references  The references
    #
    def checkNamespace(self, references):
        api.log.logger().debug('starts --> checkNamespace() function')

        for referenceNode in references:
            currentNamespace = references[referenceNode]['namespace']

            group = cmd.getAttr('{}.group'.format(references[referenceNode]['saveNode']))
            name = cmd.getAttr('{}.name'.format(references[referenceNode]['saveNode']))
            variant = cmd.getAttr('{}.variant'.format(references[referenceNode]['saveNode']))
            rightNamespace = '{}_{}_{}_'.format(group, name, variant)

            if rightNamespace[0].isdigit():
                rightNamespace = '{}{}'.format(self.projectName, rightNamespace)

            if not currentNamespace.startswith(':{}'.format(rightNamespace)) or currentNamespace.endswith('__') or len(currentNamespace.split('_')) > 4:
                rightNamespace = ':{}'.format(rightNamespace)
                cmd.file(references[referenceNode]['uniquePath'], edit=True, namespace=rightNamespace)
                api.log.logger().debug('Fix namespace for ref: {}'.format(referenceNode))

        cmd.namespace(set=':')
        api.log.logger().debug('starts --> checkNamespace() function')

    ############################################################################
    # @brief      check the scene assembly definition path.
    #
    def checkAssemblyDefinition(self):
        api.log.logger().debug('start --> checkAssemblyDefinition() function')
        wrongAssembly = []
        for assemblyNode in cmd.ls(type='assemblyReference'):
            try:
                api.assembly.Assembly(node=assemblyNode)
            except api.exception.InvalidAssembly:
                definitionPath = cmd.getAttr("{}.definition".format(assemblyNode))
                toTrim = definitionPath.find("scenes")
                if toTrim > -1:
                    correctDefinitionPath = definitionPath[toTrim:]
                    cmd.setAttr("{}.definition".format(assemblyNode), correctDefinitionPath, type="string")
                    api.log.logger().debug("Fixed a wrong assembly-reference path from: {}".format(definitionPath))
                    api.log.logger().debug("To the fixed: {}".format(correctDefinitionPath))
                    try:
                        api.assembly.Assembly(node=assemblyNode)
                    except api.exception.InvalidAssembly:
                        wrongAssembly.append(assemblyNode)
                else:
                    wrongAssembly.append(assemblyNode)

        if len(wrongAssembly) > 0:
            rbwUI.RBWError(text='One or more assembly had a bad definition path.\nPlease check the script editor for the list.')
            api.log.logger().debug(60 * '---')
            cmd.select(clear=True)
            for assemblyNode in wrongAssembly:
                api.log.logger().debug(assemblyNode)
                cmd.select(assemblyNode, add=True)
            api.log.logger().debug(60 * '---')
        api.log.logger().debug('end --> checkAssemblyDefinition() function')

    ############################################################################
    # @brief      check resolution
    #
    def checkResolution(self):
        api.log.logger().debug('starts --> checkResolution() function')
        camera = api.camera.getSceneCamera()
        if camera:
            marketingAttr = '{}:Cam_Main_ctrl.marketing'.format(camera.split(':')[0])
            if (cmd.objExists(marketingAttr) and not cmd.getAttr(marketingAttr)) or not (cmd.objExists(marketingAttr)):
                resolutionQuery = "SELECT r.width, r.height FROM `p_prj_resolution` AS pr JOIN `p_resolution` AS r ON pr.resolutionId=r.id WHERE pr.id_mv='{}'".format(self.id_mv)
                resolutionTuple = api.database.selectSingleQuery(resolutionQuery)

                cmd.setAttr('defaultResolution.width', resolutionTuple[0])
                cmd.setAttr('defaultResolution.height', resolutionTuple[1])
                api.log.logger().debug('Scene resolution setted as default show values: {}-{}'.format(resolutionTuple[0], resolutionTuple[1]))
            elif cmd.objExists(marketingAttr) and cmd.getAttr(marketingAttr):
                api.log.logger().debug('CheckResolution value override skipped due to "Marketing" camera attribute turned ON')
        api.log.logger().debug('ends --> checkResolution() function')

    ################################################################################
    # @brief      fix if the local transformation of the camera are 0 or 1
    #
    def fixOnCamera(self):
        api.log.logger().debug('starts --> fixOnCamera() function')
        camera = api.camera.getSceneCamera()

        if camera:
            pivotRotate = cmd.getAttr('{}.rotatePivot'.format(camera))[0]
            pivotScale = cmd.getAttr('{}.scalePivot'.format(camera))[0]

            if pivotRotate != (0.0, 0.0, 0.0):
                cmd.setAttr('{}.rotatePivot'.format(camera), 0.0, 0.0, 0.0)
                api.log.logger().debug('reset camera pivot rotation')

            if pivotScale != (0.0, 0.0, 0.0):
                cmd.setAttr('{}.scalePivot'.format(camera), 0.0, 0.0, 0.0)
                api.log.logger().debug('reset camera pivot scale')

        api.log.logger().debug('ends --> fixOnCamera() function')

    ############################################################################
    # @brief      check funxtion for animation scenes.
    #
    # @return     check result
    #
    def checkFunction(self):
        references = self.getRealReferences()

        self.checkReferencePath(references)

        badReferences = self.checkBadReferences(references)
        if len(badReferences) > 0:
            if self.isPromoted:
                message = 'Those references file load a suppressed save file, save is abort.\nPlease inform your lead to understand how to procede:'
                for item in badReferences:
                    message = '{}\n- {}'.format(message, item)
                rbwUI.RBWError(text=message)
                return False
            else:
                message = 'Those references file load a suppressed save file, please inform your lead to understand how to procede:'
                for item in badReferences:
                    message = '{}\n- {}'.format(message, item)
                rbwUI.RBWWarning(text=message, defCancel=None, resizable=True)

        if not self.checkUnwantedModelingReferences(references):
            return False

        if not self.checkApprovation(references):
            return False

        self.checkNamespace(references)

        self.checkAssemblyDefinition()

        camera = api.camera.getSceneCamera()
        if camera:
            self.checkResolution()
            self.fixOnCamera()

        if self.isPromoted:
            geoC = [c for c in cmd.ls(type="geometryConstraint") if ':' not in c]
            if geoC:
                message = 'The scene containts one ore more geometryConstraint.\nPlease the save is abort'
                rbwUI.RBWError(text=message)
                return False

        return True

    ############################################################################
    # @brief      export json positions file for UE and the animated face values
    #
    def exportUE(self):
        # json of the set items in the shot
        if cmd.objExists('SET'):
            data = {}
            assemblies = cmd.ls(type='assemblyReference')

            cmd.xform('|SET', rotation=[90.0, 0.0, 90.0])
            if cmd.objExists('SET_EDIT'):
                cmd.xform('SET_EDIT', rotation=[90.0, 0.0, 90.0])

            for assembly in assemblies:
                if cmd.getAttr('{}.visibility'.format(assembly)):
                    translation = cmd.xform(assembly, query=True, translation=True, worldSpace=True)
                    rotation = cmd.xform(assembly, query=True, rotation=True, worldSpace=True)
                    scale = cmd.xform(assembly, query=True, scale=True, worldSpace=True)

                    data[assembly] = [translation[0], translation[1], translation[2], rotation[0], rotation[1], rotation[2], scale[0], scale[1], scale[2]]

            cmd.xform('|SET', rotation=[0.0, 0.0, 0.0])
            if cmd.objExists('SET_EDIT'):
                cmd.xform('SET_EDIT', rotation=[0.0, 0.0, 0.0])

            jsonPath = os.path.join(self.savedShot.savedShotDeptFolder, self.savedShot.version, '{}_ue.json'.format(self.savedShot.sceneName)).replace('\\', '/')
            api.json.json_write(data, jsonPath)
            api.log.logger().debug('Saved the set json file for UE.')

        # build and export the animated joints for the multiswitch controls
        inTime = cmd.playbackOptions(query=True, minTime=True)
        outTime = cmd.playbackOptions(query=True, maxTime=True)
        allRigMult = cmd.ls('*:multiswitch')
        scene_name = cmd.file(query=True, sceneName=True)
        finPath = scene_name.split('/Sec/')[0] + '/Fin/'
        xtraGrp = 'EXTRA'
        if not cmd.objExists(xtraGrp):
            cmd.group(empty=True, name=xtraGrp)
        specialOverrides = []
        pupilOverride = []
        for rig in allRigMult:
            nam = rig.split(':')[0]
            rigName = 'GRP_' + rig.replace(':', '')
            if cmd.objExists(rigName):
                cmd.delete(rigName)
                print('deleted!')
            charGrp = cmd.group(empty=True, name=rigName)
            cmd.select(rig, replace=True)
            allCtrls = cmd.ls(selection=True)
            if len(allCtrls):
                for ctrl in allCtrls:
                    if ctrl.count('_ctrl'):
                        jntName = ctrl.replace(':', '')
                        # print(cmd.listAttr(ctrl, keyable=True))
                        poly = cmd.polyPlane(w=1, h=1, sx=10, sy=10, ax=[0, 1, 0], cuv=2, name='MSH_' + jntName.lower())
                        cmd.select(clear=True)
                        parentJnt = cmd.joint(p=[0, 0, 0], name='JNT_' + jntName)
                        valueJnt = cmd.joint(p=[0, 0, 0], name=jntName + '_value')
                        cmd.skinCluster(valueJnt, poly)
                        try:
                            cmd.connectAttr(ctrl + '.multiswitch', valueJnt + '.translateZ')
                        except:
                            cmd.connectAttr(ctrl + '.EyesExpression', valueJnt + '.translateZ')
                        allAttrs = cmd.listAttr(ctrl)
                        for attr in allAttrs:
                            if attr == 'phonemes':
                                specialOverrides.append(ctrl)
                        grp = cmd.group(parentJnt, poly, name='GRP_' + jntName)
                        cmd.parent(grp, charGrp)
                        if ctrl.count('L_Eye_ctrl') or ctrl.count('R_Eye_ctrl'):
                            pupilOverride.append(ctrl)

                cmd.parent(charGrp, xtraGrp)

                # bake results
                cmd.select('*_ctrl_value', replace=True)
                sel = cmd.ls(selection=True)
                cmd.bakeResults(sel, simulation=True, t=(inTime, outTime), at='translateZ', sampleBy=True, disableImplicitControl=True, preserveOutsideKeys=True, sparseAnimCurveBake=False, controlPoints=False, shape=False)

                # fix the pupil for the eyes
                print('pupilOverride: ', pupilOverride)
                for ctrl in pupilOverride:
                    print('Baking special overrides for the control: ', ctrl)
                    side = ctrl.split(':')[1].split('_')[0]
                    ppl = ctrl.split(':')[0] + ':' + side + '_Pupil_ctrl'
                    print('pupil control: ', ppl)
                    for frame in range(1, int(outTime), 1):
                        # print(frame)
                        cmd.currentTime(frame)
                        value = cmd.getAttr(ctrl + '.multiswitch')
                        if value == 2 or value == 8 or value == 10:
                            print('Pupil needs to be hidden at this frame ---> ', value)
                            jntValue = (ppl.replace(':', '') + '_value')
                            cmd.setKeyframe(jntValue, attribute='translateZ', time=frame, value=5)
                            print('New value for blank is: ', 5)

                # fix eventual keys on phonems
                print('specialOverrides: ', specialOverrides)
                for ctrl in specialOverrides:
                    print('Baking special overrides for the control: ', ctrl)
                    for frame in range(1, int(outTime), 1):
                        # print(frame)
                        cmd.currentTime(frame)
                        value = cmd.getAttr(ctrl + '.phonemes')
                        if value:
                            print('Phonem here at this frame !  ---> ', value)
                            jntValue = ctrl.replace(':', '') + '_value'
                            cmd.setKeyframe(jntValue, attribute='translateZ', time=frame, value=value + 12)
                            print('New value is: ', value + 12)
