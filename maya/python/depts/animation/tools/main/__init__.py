import importlib
import os

import api.widgets.rbw_UI as rbwUI
# importlib.reload(rbwUI)

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'arakno')


################################################################################
# @brief      run animation asset loader function.
#
# @param      args  The arguments
#
def run_animationAssetLoader(*args):
    import depts.animation.tools.main.animationAssetLoaderUI as animationAssetLoaderUI
    importlib.reload(animationAssetLoaderUI)

    win = animationAssetLoaderUI.AnimationAssetLoaderUI()
    win.show()


################################################################################
# @brief      run load and save UI
#
# @param      args  The arguments
#
def run_loadAndSave(*args):
    import common.loadAndSaveShotUI as lss
    importlib.reload(lss)

    win = lss.ShotUI()
    win.show()


################################################################################
# @brief      run checkupdate tool.
#
# @param      args  The arguments
#
def run_checkUpdate(*args):
    import common.checkUpdate.checkUpdateUI as checkUpdateUI
    importlib.reload(checkUpdateUI)

    win = checkUpdateUI.CheckUpdateUI('animation')
    win.show()


################################################################################
# @brief      run playblast tool.
#
# @param      args  The arguments
#
def run_playblast(*args):
    import common.playblast as playblastUI
    importlib.reload(playblastUI)

    win = playblastUI.PlayblastTool('animation')
    win.show()


################################################################################
# @brief      run studio library plug-in.
#
def run_studiolibrary():
    import sys
    import shutil

    import maya.cmds as cmd

    import api.log
    importlib.reload(api.log)

    winFail = False
    mayaScriptPath = None
    userName = os.getenv('USER')
    mayaVersion = cmd.about(version=True)

    for env in os.getenv('MAYA_SCRIPT_PATH').split(';'):
        if userName in env and 'maya/{}/scripts'.format(mayaVersion) in env:
            mayaScriptPath = env[1:-1]
            break

    if not mayaScriptPath:
        mayaScriptPath = 'C:/Users/{}/Documents/maya/{}/scripts'.format(userName, mayaVersion)
        if not os.path.isdir(mayaScriptPath):
            os.mkdir(mayaScriptPath)
        os.environ['MAYA_SCRIPT_PATH'] += ';' + mayaScriptPath
    else:
        api.log.logger().debug('mayaScriptPath: {}'.format(mayaScriptPath))

    studioLibraryPath = os.path.join(mayaScriptPath, 'studiolibrary').replace('\\', '/')
    api.log.logger().debug('studioLibraryPath: {}'.format(studioLibraryPath))

    if not os.path.isdir(studioLibraryPath):
        api.log.logger().debug('{} not exist\nDownloading studiolibrary from //speed.nas/arakno/pipeSoftware .....'.format(studioLibraryPath))
        shutil.copytree('//speed.nas/arakno/pipeSoftware/studiolibrary', studioLibraryPath)
        api.log.logger().debug("Done")

    if not os.path.exists(r'C:/Users/{}/Documents/maya/{}/scripts/studiolibrary/src'.format(userName, mayaVersion)):
        # WORKAROUND WINDOWS FAIL
        winFail = True
        if not os.path.exists(r'C:/Users/{}.RBW/Documents/maya/{}/scripts/studiolibrary/src'.format(userName, mayaVersion)):
            raise IOError(r'The source path "C:/Users/{}/Documents/maya/{}/scripts/studiolibrary/src" does not exist!'.format(userName, mayaVersion))

    if not winFail:
        if r'C:/Users/{}/Documents/maya/{}/scripts/studiolibrary/src'.format(userName, mayaVersion) not in sys.path:
            sys.path.insert(0, r'C:/Users/{}/Documents/maya/{}/scripts/studiolibrary/src'.format(userName, mayaVersion))
    else:
        if r'C:/Users/{}.RBW/Documents/maya/{}/scripts/studiolibrary/src'.format(userName, mayaVersion) not in sys.path:
            sys.path.insert(0, r'C:/Users/{}.RBW/Documents/maya/{}/scripts/studiolibrary/src'.format(userName, mayaVersion))

    import studiolibrary
    path = os.path.join(os.getenv('MC_FOLDER'), 'scenes', 'animation_library', 'StudioLibrary').replace('\\', '/')
    studiolibrary.main(name='Library', path=path)


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_toggleAnimBot(*args):
    from animBot._api.core import CORE as ANIMBOT_CORE
    ANIMBOT_CORE.trigger.animBotMenu_toggle()


animationAssetLoaderTool = {
    "name": "Asset Loader",
    "launch": run_animationAssetLoader,
    "icon": os.path.join(iconDir, "..", "common", "merge.png")
}

loadAndSaveTool = {
    "name": "L/S Animation",
    "launch": run_loadAndSave,
    "icon": os.path.join(iconDir, "loadAndSaveAnimation.png"),
    "statustip": "Load & Save"
}

checkupdateTool = {
    "name": "Check Update",
    "launch": run_checkUpdate,
    "icon": os.path.join(iconDir, '..', 'common', "refresh.png"),
    "statustip": "Check Update"
}

playblastTool = {
    "name": "Playblast Tool",
    "launch": run_playblast,
    "icon": os.path.join(iconDir, '..', 'common', "playblast.png"),
    "statustip": "Playblast Tool"
}

studiolibraryTool = {
    "name": "Studio Library",
    "launch": run_studiolibrary,
    "icon": os.path.join(iconDir, '..', 'animation', "studioLibrary.png"),
    "statustip": "Open Studio Library Plug-in"
}

animBotToggle = {
    "name": "AnimBot",
    "launch": run_toggleAnimBot,
    "icon": os.path.join(iconDir, '..', 'animation', "animBot.png"),
    "statustip": "Toggle AnimBot plugin"
}


tools = [
    animationAssetLoaderTool,
    loadAndSaveTool,
    checkupdateTool,
    playblastTool,
    studiolibraryTool,
    animBotToggle
]

PackageTool = {
    "name": "main",
    "tools": tools
}
