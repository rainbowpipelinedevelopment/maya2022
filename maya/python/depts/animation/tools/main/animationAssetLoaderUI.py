import importlib
from PySide2 import QtWidgets, QtCore
import os

import maya.cmds as cmd

import api.asset
import api.savedAsset
import api.assembly
import api.widgets.rbw_UI as rbwUI
import config.dictionaries

# importlib.reload(api.asset)
# importlib.reload(api.savedAsset)
# importlib.reload(api.assembly)
# importlib.reload(rbwUI)
importlib.reload(config.dictionaries)


################################################################################
# @brief      This class describes an animation asset loader ui.
#
class AnimationAssetLoaderUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super().__init__()

        if cmd.window('AnimationAssetLoaderUI', exists=True):
            cmd.deleteUI('AnimationAssetLoaderUI')

        self.mcFolder = os.getenv('MC_FOLDER')
        api.log.logger().debug(self.mcFolder)
        self.id_mv = os.getenv('ID_MV')
        api.log.logger().debug(self.id_mv)

        self.assets = api.system.getAssetsFromDB()

        self.setObjectName('AnimationAssetLoaderUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topFrame = rbwUI.RBWFrame(layout='V')
        self.mainLayout.addWidget(self.topFrame)

        # --------------------------- search widget -------------------------- #
        self.searchWidget = rbwUI.RBWFrame(layout='H', margins=[0, 0, 0, 0], bgColor='transparent')

        self.searchLine = rbwUI.RBWLineEdit(title='Type Asset Name', stretch=False, bgColor='rgba(30, 30, 30, 150)')
        self.searchLine.returnPressed.connect(self.searchEntry)
        self.searchButton = rbwUI.RBWButton(icon=['search.png'])
        self.searchButton.clicked.connect(self.searchEntry)

        self.searchWidget.addWidget(self.searchLine)
        self.searchWidget.addWidget(self.searchButton)

        self.topFrame.addWidget(self.searchWidget)

        self.assetResultsFrame = rbwUI.RBWFrame(layout='G', bgColor='transparent', margins=[0, 0, 0, 0])

        # --------------------------- asset widget -------------------------- #
        self.assetInfoGroup = rbwUI.RBWGroupBox(title='Asset Info:', layout='V', fontSize=12)

        self.assetCategoryComboBox = rbwUI.RBWComboBox('Category', bgColor='transparent', size=200)
        self.assetGroupComboBox = rbwUI.RBWComboBox('Group', bgColor='transparent', size=200)
        self.assetNameComboBox = rbwUI.RBWComboBox('Name', bgColor='transparent', size=200)
        self.assetVariantComboBox = rbwUI.RBWComboBox('Variant', bgColor='transparent', size=200)

        self.assetCategoryComboBox.activated.connect(self.fillAssetGroupComboBox)
        self.assetGroupComboBox.activated.connect(self.fillAssetNameComboBox)
        self.assetNameComboBox.activated.connect(self.fillAssetVariantComboBox)
        self.assetVariantComboBox.activated.connect(self.setCurrentAsset)

        self.assetInfoGroup.addWidget(self.assetCategoryComboBox)
        self.assetInfoGroup.addWidget(self.assetGroupComboBox)
        self.assetInfoGroup.addWidget(self.assetNameComboBox)
        self.assetInfoGroup.addWidget(self.assetVariantComboBox)

        # self.assetInfoGroup.layout.setSizeConstraint(QtWidgets.QLayout.SetFixedSize)

        self.assetResultsFrame.layout.addWidget(self.assetInfoGroup, 0, 0, 1, 1)

        # ------------------------ asset result widget ------------------------#
        self.searchResultList = QtWidgets.QListWidget()
        self.searchResultList.itemClicked.connect(self.matchAsset)

        self.assetResultsFrame.layout.addWidget(self.searchResultList, 0, 1, 2, 1)

        self.topFrame.addWidget(self.assetResultsFrame)

        # ---------------------- available asset widget ----------------------#

        self.availableDeptComboBox = rbwUI.RBWComboBox('Available Depts', bgColor='rgba(30, 30, 30, 150)')
        self.availableDeptComboBox.layout.setContentsMargins(5, 5, 10, 5)
        self.availableDeptComboBox.activated.connect(self.pickAsset)

        self.assetResultsFrame.layout.addWidget(self.availableDeptComboBox, 1, 0, 1, 1)

        # ------------------------- picked asset tree ------------------------ #
        self.pickedAssetTree = rbwUI.RBWTreeWidget(['type', 'namespace', 'item'])
        self.pickedAssetTree.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.pickedAssetTree.setColumnWidth(0, 100)
        self.pickedAssetTree.setColumnWidth(1, 150)
        self.pickedAssetTree.setColumnWidth(2, 650)

        self.topFrame.addWidget(self.pickedAssetTree)

        # --------------------------- buttons widget ------------------------- #
        self.buttonWidget = rbwUI.RBWFrame(layout='H', bgColor='transparent')

        self.removeSeletecButton = rbwUI.RBWButton(text='Remove Selected', icon=['delete.png'], size=[200, 35])
        self.removeSeletecButton.clicked.connect(self.removeSelected)
        self.removeAllButton = rbwUI.RBWButton(text='Remove All', icon=['delete.png'], size=[200, 35])
        self.removeAllButton.clicked.connect(self.removeAll)
        self.addToSceneButton = rbwUI.RBWButton(text='Add To Scene', icon=['merge.png'], size=[200, 35])
        self.addToSceneButton.clicked.connect(self.addToScene)

        self.buttonWidget.addWidget(self.removeSeletecButton)
        self.buttonWidget.addWidget(self.removeAllButton)
        self.buttonWidget.addStretch()
        self.buttonWidget.addWidget(self.addToSceneButton)

        self.topFrame.addWidget(self.buttonWidget)

        self.fillAssetCategoryComboBox()

        self.setMinimumSize(950, 430)
        self.setTitle('Animation Asset Loader')
        self.setIcon('merge.png')
        self.addStatusBar()
        self.setFocus()

    ############################################################################
    # @brief      fill the asset category combo box.
    #
    def fillAssetCategoryComboBox(self):
        self.assetCategoryComboBox.clear()
        categories = self.assets.keys()

        self.assetCategoryComboBox.addItems(sorted(categories))

    ############################################################################
    # @brief      fill the group combo box.
    #
    def fillAssetGroupComboBox(self):
        self.currentCategory = self.assetCategoryComboBox.currentText()

        self.assetGroupComboBox.clear()
        groups = self.assets[self.currentCategory].keys()

        self.assetGroupComboBox.addItems(sorted(groups))

    ############################################################################
    # @brief      fill th name combo box.
    #
    def fillAssetNameComboBox(self):
        self.currentGroup = self.assetGroupComboBox.currentText()

        self.assetNameComboBox.clear()
        names = self.assets[self.currentCategory][self.currentGroup].keys()

        self.assetNameComboBox.addItems(sorted(names))

    ############################################################################
    # @brief      fill the variant combo box.
    #
    def fillAssetVariantComboBox(self):
        self.currentName = self.assetNameComboBox.currentText()

        self.assetVariantComboBox.clear()
        variants = self.assets[self.currentCategory][self.currentGroup][self.currentName].keys()

        self.assetVariantComboBox.addItems(sorted(variants))

        if len(variants) == 1:
            self.assetVariantComboBox.setCurrentIndex(0)
            self.setCurrentAsset()

    ############################################################################
    # @brief      Sets the current asset.
    #
    def setCurrentAsset(self):
        self.currentVariant = self.assetVariantComboBox.currentText()

        self.currentAsset = api.asset.Asset({
            'category': self.currentCategory,
            'group': self.currentGroup,
            'name': self.currentName,
            'variant': self.currentVariant
        })

        self.fillAvailableDeptComboBox()

    ############################################################################
    # @brief      search the give asset.
    #
    def searchEntry(self):
        wantedEntryName = self.searchLine.text()
        if wantedEntryName == '':
            self.updateResultList()
        else:
            searchAssetFromNameQuery = "SELECT DISTINCT `category`, `group`, `name` FROM `V_assetList` WHERE `projectId`={} AND `name` LIKE '%{}%'".format(
                self.id_mv,
                wantedEntryName
            )

            results = api.database.selectQuery(searchAssetFromNameQuery)
            if results:
                self.updateResultList(results)

    ############################################################################
    # @brief      update the list result.
    #
    # @param      queryResults  The query results
    #
    def updateResultList(self, queryResults=None):
        self.searchResultList.clear()
        if queryResults:
            self.searchResultList.show()
            for infoTupla in queryResults:
                item = QtWidgets.QListWidgetItem('{} - {} - {}'.format(infoTupla[0], infoTupla[1], infoTupla[2]))
                self.searchResultList.addItem(item)
        else:
            self.searchResultList.hide()

    ############################################################################
    # @brief      update the combo to match the selected asset.
    #
    def matchAsset(self):
        wantedCategory, wantedGroup, wantedName = self.searchResultList.currentItem().text().split(' - ')

        self.fillAssetCategoryComboBox()
        categoryIndex = self.assetCategoryComboBox.findText(wantedCategory, QtCore.Qt.MatchFixedString)
        self.assetCategoryComboBox.setCurrentIndex(categoryIndex)

        self.fillAssetGroupComboBox()
        groupIndex = self.assetGroupComboBox.findText(wantedGroup, QtCore.Qt.MatchFixedString)
        self.assetGroupComboBox.setCurrentIndex(groupIndex)

        self.fillAssetNameComboBox()
        nameIndex = self.assetNameComboBox.findText(wantedName, QtCore.Qt.MatchFixedString)
        self.assetNameComboBox.setCurrentIndex(nameIndex)

        self.fillAssetVariantComboBox()

    ############################################################################
    # @brief      fill available dept combo box.
    #
    def fillAvailableDeptComboBox(self):
        self.availableDeptComboBox.clear()

        defSaveQuery = "SELECT DISTINCT `dept` " \
            "FROM `savedAsset` " \
            "WHERE `assetId` = '{}' " \
            "AND `visible`= 1 " \
            "AND ((`dept` = 'modeling' AND `deptType` = 'hig') " \
            "OR (`dept` = 'rigging' AND `deptType` = 'hig') " \
            "OR (`dept` = 'modeling' AND `deptType` = 'prx') " \
            "OR (`dept` = 'set' AND `deptType` = 'hig') " \
            "OR (`dept` = 'fx' AND `deptType` = 'ani'))".format(self.currentAsset.variantId)
        results = api.database.selectQuery(defSaveQuery)

        if len(results):
            depts = [res[0] for res in results]
            self.availableDeptComboBox.addItems(depts)
            if 'rigging' in depts:
                self.availableDeptComboBox.setCurrentText('rigging')
        else:
            rbwUI.RBWError(title='No save avalaible', text='No Available Save for the given asset.')

    ############################################################################
    # @brief      pick the asset
    #
    def pickAsset(self):
        self.currentDept = self.availableDeptComboBox.currentText()

        savedAsset = None
        if self.currentDept != 'modeling':
            savedAssetParams = {
                'asset': self.currentAsset,
                'dept': self.currentDept,
                'deptType': 'hig',
                'approvation': 'def'
            }

            if self.currentDept == 'fx':
                savedAssetParams['deptType'] = 'ani'

            savedAsset = api.savedAsset.SavedAsset(params=savedAssetParams)
            path = savedAsset.getLatest()[2]
            deptType = savedAssetParams['deptType']
        else:
            assemblyParams = {
                'asset': self.currentAsset
            }

            assembly = api.assembly.Assembly(params=assemblyParams)
            path = assembly.getLatest()[1]
            deptType = 'assembly'

        if self.pickedAssetTree.findItems(self.currentCategory.upper(), QtCore.Qt.MatchContains | QtCore.Qt.MatchRecursive, 0):
            mainNode = self.pickedAssetTree.findItems(self.currentCategory.upper(), QtCore.Qt.MatchContains | QtCore.Qt.MatchRecursive, 0)[0]
        else:
            mainNode = QtWidgets.QTreeWidgetItem(self.pickedAssetTree)
            mainNode.setText(0, self.currentCategory.upper())

        child = QtWidgets.QTreeWidgetItem(mainNode)
        child.setText(0, deptType)

        namespace = savedAsset.getNamespace() if savedAsset else ' - '
        child.setText(1, namespace)

        child.setText(2, path)

        self.pickedAssetTree.expandAll()

    ############################################################################
    # @brief      Removes a selected.
    #
    def removeSelected(self):
        selectedItems = self.pickedAssetTree.selectedItems()
        for item in selectedItems:
            parentItem = item.parent()
            if parentItem:
                parentItem.removeChild(item)
                if parentItem.childCount() == 0:
                    root = self.pickedAssetTree.invisibleRootItem()
                    root.removeChild(parentItem)

    ############################################################################
    # @brief      Removes all.
    #
    def removeAll(self):
        self.pickedAssetTree.clear()

    ############################################################################
    # @brief      Adds to scene.
    #
    def addToScene(self):
        root = self.pickedAssetTree.invisibleRootItem()
        for i in range(0, root.childCount()):
            categoryNode = root.child(i)
            category = categoryNode.text(0)

            for j in range(0, categoryNode.childCount()):
                item = categoryNode.child(j)

                deptType = item.text(0)
                namespace = item.text(1)
                path = item.text(2)

                if deptType != 'assembly':
                    if category == 'CHARACTER':
                        api.scene.addToCharacterGroup(path, namespace)
                    elif category == 'PROP':
                        api.scene.addToPropGroup(path, namespace)
                    elif category == 'SET':
                        api.scene.addToSetGroup(path)
                    elif category == 'LIBRARY':
                        api.scene.addToLibraryGroup(path, namespace)
                else:
                    api.scene.addToSetEditGroup(path)

        self.pickedAssetTree.clear()
