import os
import importlib
import functools
import maya.cmds as cmd
import imagesize

from PySide2 import QtWidgets, QtGui, QtCore

import api.widgets.rbw_UI as rbwUI
import api.asset
import api.savedAsset
import api.database as db
import api.log

# importlib.reload(rbwUI)
# importlib.reload(api.asset)
# importlib.reload(api.savedAsset)
importlib.reload(db)
importlib.reload(api.log)

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'animation')


class FxLibrary(rbwUI.RBWWindow):

    def __init__(self, *args, **kwargs):
        super(FxLibrary, self).__init__(*args, **kwargs)

        if cmd.window('FxLibrary', exists=True):
            cmd.deleteUI('FxLibrary')
        self.setObjectName('FxLibrary')

        self.id_mv = os.getenv('ID_MV')

        self.getFxs()
        self.initUI()
        self.updateMargins()

    def initUI(self):
        self.setMain(True)
        self.setStyle()

        searchBox = rbwUI.RBWFrame(layout='V')
        self.searchLine = rbwUI.RBWLineEdit(title='Search:', placeHolder='Insert fx name', stretch=False, bgColor='rgba(30, 30, 30, 150)')
        self.searchLine.edit.returnPressed.connect(self.searchFx)
        self.fxList = QtWidgets.QListWidget()
        self.fxList.itemClicked.connect(self.traverseToFx)
        self.fxTree = rbwUI.RBWTreeWidget(['Available Fxs'], decorate=True)
        self.fxTree.itemSelectionChanged.connect(self.fillFxBox)

        self.snapCheck = rbwUI.RBWCheckBox('Snap to selection')
        self.constraintCheck = rbwUI.RBWCheckBox('Constraint to selection')
        self.snapCheck.stateChanged.connect(functools.partial(self.toggleChecks, self.constraintCheck))
        self.constraintCheck.stateChanged.connect(functools.partial(self.toggleChecks, self.snapCheck))
        self.checkLay = rbwUI.RBWFrame('V', bgColor='transparent', margins=[0, 0, 0, 0])
        self.pointCheck = rbwUI.RBWCheckBox('Point')
        self.orientCheck = rbwUI.RBWCheckBox('Orient')
        self.scaleCheck = rbwUI.RBWCheckBox('Scale')
        self.checkLay.addWidget(self.pointCheck)
        self.checkLay.addWidget(self.orientCheck)
        self.checkLay.addWidget(self.scaleCheck)
        self.snapCheck.setChecked(True)

        sizeWidget = rbwUI.RBWFrame(bgColor='transparent', layout='G')
        for i in range(0, 4):
            sizeWidget.layout.setColumnStretch(i, 40)
        self.sizeLabel = rbwUI.RBWLabel(text='Icon Size:', size=12, italic=False)
        self.sizeSlider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        self.sizeSlider.setMinimum(3)
        self.sizeSlider.setMaximum(5)
        self.sizeSlider.setTickInterval(1)
        self.sizeSlider.setSingleStep(1)
        self.sizeSlider.setSliderPosition(3)
        self.sizeSlider.valueChanged.connect(self.changeIconSize)
        self.smallLabel = rbwUI.RBWLabel('S', size=12, italic=False)
        self.smallLabel.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.mediumLabel = rbwUI.RBWLabel('M', size=12, italic=False)
        self.mediumLabel.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.largeLabel = rbwUI.RBWLabel('L', size=12, italic=False)
        self.largeLabel.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        sizeWidget.layout.addWidget(self.sizeLabel, 0, 0, 2, 1)
        sizeWidget.layout.addWidget(self.sizeSlider, 0, 1, 1, 3)
        sizeWidget.layout.addWidget(self.smallLabel, 1, 1)
        sizeWidget.layout.addWidget(self.mediumLabel, 1, 2)
        sizeWidget.layout.addWidget(self.largeLabel, 1, 3)

        searchBox.addWidget(self.searchLine)
        searchBox.addWidget(self.fxList)
        searchBox.addWidget(self.fxTree)
        searchBox.addWidget(self.snapCheck)
        searchBox.addWidget(self.constraintCheck)
        searchBox.addWidget(self.checkLay)
        searchBox.addWidget(sizeWidget)

        self.fxBox = QtWidgets.QListWidget()
        self.fxBox.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.fxBox.setViewMode(QtWidgets.QListView.IconMode)
        self.fxBox.setResizeMode(QtWidgets.QListView.Adjust)
        self.fxBox.setMovement(QtWidgets.QListView.Static)
        self.fxBox.setIconSize(QtCore.QSize(150, 150))
        self.fxBox.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.fxBox.customContextMenuRequested.connect(self.openRightClickMenu)
        self.fxBox.itemClicked.connect(self.previewFx)

        splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)
        splitter.addWidget(searchBox)
        splitter.addWidget(self.fxBox)
        splitter.setStretchFactor(0, 1)
        splitter.setStretchFactor(1, 3)
        self.mainLayout.addWidget(splitter)

        self.fillFxTree()

        self.setMinimumSize(1000, 500)
        self.setTitle('Fx Library')
        self.setIcon('../animation/fx.png')
        self.setFocus()

    def getFxs(self):
        self.fxs = {}
        for fxTupla in db.selectQuery("SELECT `group`, `name`, `variant` FROM `V_assetList` WHERE category='fx' AND projectID={}".format(self.id_mv)):
            group = fxTupla[0]
            name = fxTupla[1]
            var = fxTupla[2]

            if group not in self.fxs.keys():
                self.fxs[group] = {}

            if name not in self.fxs[group].keys():
                self.fxs[group][name] = []

            self.fxs[group][name].append(var)

    def fillFxTree(self):
        for fxGroup in self.fxs:
            child = QtWidgets.QTreeWidgetItem(self.fxTree)
            child.setText(0, fxGroup)

            fxNames = list(self.fxs[fxGroup].keys())
            fxNames.sort()

            for fxName in fxNames:
                subChild = QtWidgets.QTreeWidgetItem(child)
                subChild.setText(0, fxName)

    def searchFx(self):
        self.fxList.clear()
        wantedName = self.searchLine.text()

        if wantedName != '':
            for group in self.fxs:
                for name in self.fxs[group]:
                    if wantedName in name:
                        for variant in self.fxs[group][name]:
                            item = QtWidgets.QListWidgetItem('{}-{}-{}'.format(group, name, variant))
                            self.fxList.addItem(item)

    def traverseToFx(self):
        currentSelection = self.fxList.currentItem().text()

        wantedGroup = currentSelection.split('-')[0]
        wantedName = currentSelection.split('-')[1]
        wantedVariant = currentSelection.split('-')[2]

        groupItem = self.fxTree.findItems(wantedGroup, QtCore.Qt.MatchFixedString)[0]
        groupItem.setExpanded(True)

        try:
            for i in range(0, groupItem.childCount()):
                if groupItem.child(i).text(0) == wantedName:
                    nameItem = groupItem.child(i)
                    self.fxTree.setCurrentItem(nameItem)
                    imageItem = self.fxBox.findItems(wantedVariant, QtCore.Qt.MatchFixedString)[0]
                    self.fxBox.setCurrentItem(imageItem)
                    self.previewFx()
        except IndexError:
            pass

    def previewFx(self):
        self.setCurrentFx()
        previewPanel = PreviewFxPanel(self.currentSavedAsset.getLatest()[0])
        previewPanel.show()

    def setCurrentFx(self):
        item = self.fxBox.currentItem()
        self.currentVariant = item.text()

        currentAssetParams = {
            'category': 'fx',
            'group': self.currentGroup,
            'name': self.currentName,
            'variant': self.currentVariant,
            'id_mv': self.id_mv
        }

        self.currentAsset = api.asset.Asset(params=currentAssetParams)

        currentSavedAssetParams = {
            'asset': self.currentAsset,
            'dept': 'fx',
            'deptType': 'ani',
            'approvation': 'def'
        }

        self.currentSavedAsset = api.savedAsset.SavedAsset(params=currentSavedAssetParams)

    def fillFxBox(self):
        self.fxBox.clear()

        try:
            self.currentCategory = 'fx'
            self.currentName = self.fxTree.selectedItems()[0].text(0)
            self.currentGroup = self.fxTree.selectedItems()[0].parent().text(0)

            allVariant = self.fxs[self.currentGroup][self.currentName]
            allVariant.sort()

            for i in range(0, len(allVariant)):
                variant = allVariant[i]

                assetParams = {
                    'category': self.currentCategory,
                    'group': self.currentGroup,
                    'name': self.currentName,
                    'variant': variant,
                    'id_mv': self.id_mv
                }
                currentAsset = api.asset.Asset(params=assetParams)

                savedAssetParams = {
                    'asset': currentAsset,
                    'dept': 'fx',
                    'deptType': 'ani',
                    'approvation': 'def',
                }
                currentSavedAsset = api.savedAsset.SavedAsset(params=savedAssetParams)
                lastFxSavedAsset = currentSavedAsset.getLatest()
                if lastFxSavedAsset:
                    lastFxFolder = os.path.join(self.mcLibrary, os.path.dirname(lastFxSavedAsset[2])).replace('\\', '/')
                    for file in os.listdir(lastFxFolder):
                        if file.endswith('.jpg'):
                            lastFxPath = os.path.join(lastFxFolder, file).replace('\\', '/')
                            break

                    imageItem = self.createImageItem(lastFxPath, currentAsset.variant)
                    self.fxBox.addItem(imageItem)

        except AttributeError:
            pass

    def createImageItem(self, imageParams):
        pixmap = QtGui.QPixmap(imageParams["imagePath"])
        icon = QtGui.QIcon()
        icon.addPixmap(pixmap)

        imageItem = QtWidgets.QListWidgetItem(icon, imageParams['variant'])

        return imageItem

    def changeIconSize(self):
        scaleFactor = self.sizeSlider.value()
        iconSize = scaleFactor * 50
        self.fxBox.setIconSize(QtCore.QSize(iconSize, iconSize))

    def openRightClickMenu(self, position):
        popMenu = QtWidgets.QMenu()
        importAction = QtWidgets.QAction("Import", self)

        if self.fxBox.itemAt(position):
            popMenu.addAction(importAction)

        importAction.triggered.connect(self.importFxInScene)
        popMenu.exec_(self.fxBox.mapToGlobal(position))

    def toggleChecks(self, check, state):
        check.setChecked(False if state else True)
        self.checkLay.show() if self.constraintCheck.isChecked() else self.checkLay.hide()

    def importFxInScene(self):
        self.setCurrentFx()
        fxNode = api.scene.addToFxGroup(self.currentSavedAsset.getLatest()[2], )

        selection = self.getSelection()
        if self.snapCheck.isChecked():
            if selection:
                constraint = cmd.pointConstraint(selection, fxNode)
                cmd.delete(constraint)

        elif self.constraintCheck.isChecked():
            if selection:
                if self.pointCheck.isChecked():
                    cmd.pointConstraint(selection, fxNode)

                if self.orientCheck.isChecked():
                    cmd.orientConstraint(selection, fxNode)

                if self.scaleCheck.isChecked():
                    cmd.scaleConstraint(selection, fxNode)

    def getSelection(self):
        selectionList = cmd.ls(selection=True)

        if not selectionList or len(selectionList) > 1:
            rbwUI.RBWWarning(text='Select ONE object!', defCancel=None, parent=self)
            return None
        else:
            return selectionList[0]


class PreviewFxPanel(QtWidgets.QWidget):

    def __init__(self, lastPath):
        super(PreviewFxPanel, self).__init__()

        self.lastPath = lastPath

        if cmd.window('PreviewFxPanel', exists=True):
            cmd.deleteUI('PreviewFxPanel')
        self.setObjectName('PreviewFxPanel')

        self.initUI()

    def initUI(self):
        self.layout = QtWidgets.QVBoxLayout()

        self.fxViewer = QtWidgets.QLabel()
        self.path = self.getLastFxImage(self.lastPath)
        self.gif = QtGui.QMovie(self.path)
        newWidth, newHeight = self.smoothGifResize(self.path, 960, 540)
        movieSize = QtCore.QSize(newWidth, newHeight)
        self.gif.setScaledSize(movieSize)
        self.fxViewer.setMovie(self.gif)
        self.gif.start()

        self.layout.addWidget(self.fxViewer)

        self.setLayout(self.layout)
        self.setFixedSize(980, 600)
        self.setWindowTitle('Preview Fx')
        self.setFocus()

    def smoothGifResize(self, gif, frameWidth, frameHeight):
        gifWidth0, gifHeight0 = imagesize.get(gif)

        widthRatio = float(frameWidth) / float(gifWidth0)
        heightRatio = float(frameHeight) / float(gifHeight0)

        if widthRatio >= heightRatio:
            gifWidth1 = gifWidth0 * heightRatio
            gifHeight1 = frameHeight
            return gifWidth1, gifHeight1

        gifWidth1 = frameWidth
        gifHeight1 = gifHeight0 * widthRatio
        return gifWidth1, gifHeight1

    def getLastFxImage(self, lastPath):
        previewDir = os.path.join(os.path.dirname(lastPath), 'sequence').replace('\\', '/')
        if os.path.exists(previewDir):
            return [os.path.join(previewDir, file).replace('\\', '/') for file in os.listdir(previewDir) if file.endswith('.gif')][0]
        else:
            previewDir = os.path.join(os.path.dirname(lastPath), 'snapshots').replace('\\', '/')
            return [os.path.join(previewDir, file).replace('\\', '/') for file in os.listdir(previewDir) if file.endswith('_persp.jpg')][0]
