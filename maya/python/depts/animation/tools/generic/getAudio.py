import os
import importlib
import re

import maya.mel as mel
import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI
import api.log

# importlib.reload(rbwUI)
importlib.reload(api.log)


def getSceneAudioFile(prjCode, episode):
    playBackSliderPython = mel.eval('$tmpVar=$gPlayBackSlider')
    currentAudio = cmd.timeControl(playBackSliderPython, query=True, sound=True)

    if not currentAudio:
        try:
            mel.eval("setSoundDisplay {} 1;".format(cmd.ls(type='audio')[0]))
        except:
            pass
        currentAudio = cmd.timeControl(playBackSliderPython, query=True, sound=True)

    return cmd.getAttr('{}.filename'.format(currentAudio)) if currentAudio else None


def importAudio(prjCode, season, episode, sequence, shot, auto=False):
    audioBasePath = '//speed.nas/{}/01_pre_production/08_animatic/{}/Individual_Shots/AIFF'.format(prjCode, episode)
    audioNamespace = '{}_{}_{}_{}_{}'.format(prjCode.upper(), season, episode, sequence, shot)
    audioTestFile = r"{}_{}_{}_{}_{}\w{{0,9}}?".format(prjCode.upper(), season, episode, sequence, shot)

    audios = []
    if os.path.exists(audioBasePath):
        for file in os.listdir(audioBasePath):
            if file.count('.aif') and file.split('.')[0].split('_')[4] == shot and (re.match(audioTestFile, file.split('.')[0] or file.split('.')[0] == audioNamespace)):
                audios.append(file)
    else:
        rbwUI.RBWError(title="Invalid audio default path", text="The default path for the audio files of this show:\n - {}\ndoes not exists\nNo audio will be imported".format(audioBasePath))
        return

    if len(audios):
        checkToUpdate = True
        updateMex = "There is a newer audio version for this shot,\ndo you want to update it?"

        def myOrder(input):
            if len(input.split('_')) == 5:
                return 1
            else:
                return int(input.split('_')[-1].split('.')[0].split('v')[1])

        audios.sort(key=myOrder)
        latestAudio = audios[-1]
        audioFullPath = os.path.join(audioBasePath, latestAudio).replace('\\', '/')

        # Check if the current audio file in scene (if it exists)
        if cmd.ls(type='audio'):
            currentAudioPath = getSceneAudioFile(prjCode, episode)
            if currentAudioPath:
                currentAudioBasePath = os.path.basename(currentAudioPath)

                if audioFullPath == currentAudioPath:
                    if not auto:
                        rbwUI.RBWDialog(text="You are already using the latest\navailable audio version of this shot")

                    checkToUpdate = False
                    api.log.logger().debug('Current audio file: {}'.format(currentAudioPath))

                elif audioBasePath != currentAudioBasePath:
                    updateMex = "The current audio is not in the content,\ndo you want to import the latest\nofficial audio for this shot?"
                    api.log.logger().warning('Wrong audio file: {}'.format(currentAudioPath))
        else:
            updateMex = 'Do you want to import the latest\nofficial audio for this shot?'

        if checkToUpdate:
            if rbwUI.RBWConfirm(title='Confirm Update', text=updateMex).result():
                cmd.delete(cmd.ls(type='audio'))
                cmd.file(audioFullPath, i=True, type='audio', renameAll=True, mergeNamespacesOnClash=False, namespace=audioNamespace, options='o=1', preserveReferences=True)

                # Sync the audio with the correct ending frame of the animation
                audioNode = cmd.ls(type='audio')[0]
                animEndFrame = int(round(cmd.getAttr('{}.endFrame'.format(audioNode)) - 1))
                cmd.playbackOptions(animationEndTime=animEndFrame)

                if not auto:
                    rbwUI.RBWDialog(text="The audio file has been updated!")

                api.log.logger().debug('Audio file updated: {}'.format(audioFullPath))
    else:
        if not auto:
            rbwUI.RBWWarning(text="There are no audio file\navailable for this shot!")

        api.log.logger().warning('No audio file found for this shot!')


def main(auto=False):
    projectCode = os.getenv('PROJECT')

    sceneName = cmd.file(query=True, expandName=True).split('/')[-1]
    season = sceneName.split('_')[1]
    episode = sceneName.split('_')[2]
    sequence = sceneName.split('_')[3]
    shot = sceneName.split('_')[4]

    importAudio(projectCode, season, episode, sequence, shot, auto)
