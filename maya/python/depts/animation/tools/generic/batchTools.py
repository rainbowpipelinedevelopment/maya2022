import os
import importlib
import shutil
import time

import maya.cmds as cmd
import maya.mel as mel

from datetime import date
from PySide2 import QtWidgets

import api.savedShot
import api.camera
import common.checkUpdate.checkUpdate as checkUpd
import common.playblast as pbTool
import depts.animation.sc_animation as scDepts
import api.widgets.rbw_UI as rbwUI
import api.database as db
import api.log

# importlib.reload(api.savedShot)
importlib.reload(api.camera)
importlib.reload(checkUpd)
importlib.reload(pbTool)
importlib.reload(scDepts)
# importlib.reload(rbwUI)
importlib.reload(db)
importlib.reload(api.log)


################################################################################
# @brief      Class for shot ui
#
class BatchTools(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs the object
    #
    # @param      self  The object
    #
    def __init__(self):
        seasonQuery = "SELECT DISTINCT `season` FROM `V_shotList` WHERE `projectID` = {}".format(os.getenv('ID_MV'))
        self.seasons = [x[0] for x in sorted(db.selectQuery(seasonQuery))]
        if self.seasons:
            super(BatchTools, self).__init__()

            if cmd.window('BatchTools', exists=True):
                cmd.deleteUI('BatchTools')

            self.proj = os.getenv('PROJECT')
            self.id_mv = os.getenv('ID_MV')
            self.mcFolder = os.getenv('MC_FOLDER')

            self.setObjectName('BatchTools')
            self.initUI()
            self.updateMargins()
        else:
            rbwUI.RBWWarning(text='There are no seasons for this show!', defCancel=None)

    ############################################################################
    # @brief      initUI method
    #
    # @param      self  The object
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        frame = rbwUI.RBWFrame('H')

        selectionLay = rbwUI.RBWFrame('V', bgColor='transparent', margins=[0, 0, 0, 0])
        self.toolCombo = rbwUI.RBWComboBox('Tool:', items=['Massive export camera', 'Massive set update', 'Check remote deliveries', 'Massive Autoblast'])
        self.toolCombo.activated.connect(self.checkTool)
        self.seasonCombo = rbwUI.RBWComboBox('Season:', items=self.seasons)
        self.seasonCombo.activated.connect(self.fillCombo)
        self.episodeCombo = rbwUI.RBWComboBox('Episode:')
        self.deptCombo = rbwUI.RBWComboBox('Dept:', items=['Lay', 'Pri', 'Sec'])
        self.deptCombo.activated.connect(self.fillShotTree)
        self.fakeCheck = rbwUI.RBWCheckBox('Fake promoted save')
        self.sprCheck = rbwUI.RBWCheckBox('SPR only')
        self.latestCheck = rbwUI.RBWCheckBox('Latest versions only')
        self.latestCheck.stateChanged.connect(self.fillShotTree)
        selectionLay.addWidget(self.toolCombo)
        selectionLay.addWidget(self.seasonCombo)
        selectionLay.addWidget(self.episodeCombo)
        selectionLay.addWidget(self.deptCombo)
        selectionLay.addWidget(self.fakeCheck)
        selectionLay.addWidget(self.sprCheck)
        selectionLay.addWidget(self.latestCheck)

        self.versionTree = rbwUI.RBWTreeWidget(fields=['Shot name'])
        self.versionTree.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

        frame.addWidget(selectionLay)
        frame.addWidget(self.versionTree)

        buttonsLayout = rbwUI.RBWFrame('H')
        self.toggleButton = rbwUI.RBWButton('Toggle selection', size=[None, 30])
        self.toggleButton.clicked.connect(self.toggleSelection)
        self.runButton = rbwUI.RBWButton('Start batch', size=[None, 30])
        self.runButton.clicked.connect(self.startBatch)
        buttonsLayout.addWidget(self.toggleButton)
        buttonsLayout.addWidget(self.runButton)

        self.mainLayout.addWidget(frame)
        self.mainLayout.addWidget(buttonsLayout)

        self.checkTool()

        self.setMinimumSize(500, 270)
        self.setTitle('Batch tools')
        self.setIcon('../animation/batch.png')
        self.setFocus()

    ############################################################################
    # @brief      toggle the tool's checkboxes
    #
    # @param      self  The object
    #
    def checkTool(self):
        index = self.toolCombo.currentIndex()
        self.versionTree.clear()
        self.fakeCheck.show() if not index else self.fakeCheck.hide()
        self.sprCheck.show() if index == 2 else self.sprCheck.hide()

        self.fakeCheck.setChecked(False)
        self.sprCheck.setChecked(False)
        self.latestCheck.setChecked(True)

    ############################################################################
    # @brief      toggle the selection in the version tree
    #
    # @param      self  The object
    #
    def toggleSelection(self):
        root = self.versionTree.invisibleRootItem()
        value = False if self.versionTree.selectedItems() else True
        for i in range(0, root.childCount()):
            root.child(i).setSelected(value)

    ############################################################################
    # @brief      fills the season and episode combos
    #
    # @param      self  The object
    #
    def fillCombo(self):
        season = self.seasonCombo.currentText()
        episodeQuery = "SELECT DISTINCT `episode` FROM `V_shotList` WHERE `season` = '{}' AND `projectID` = {}".format(season, self.id_mv)
        self.episodeCombo.addItems([x[0] for x in sorted(db.selectQuery(episodeQuery))])

    ############################################################################
    # @brief      fills the versions tree
    #
    # @param      self  The object
    #
    def fillShotTree(self):
        self.versionTree.clear()
        self.dept = self.deptCombo.currentText()
        self.season = self.seasonCombo.currentText()
        self.episode = self.episodeCombo.currentText()
        self.paths = {}

        latest = []
        shotQuery = "SELECT `path` FROM `savedShot` WHERE `dept` = '{}' AND `path` LIKE '%/{}/{}/%' AND `id_mv` = {}".format(self.dept, self.season, self.episode, self.id_mv)
        for shot in sorted([x[0] for x in db.selectQuery(shotQuery)], reverse=True):
            file = os.path.basename(shot)
            shotCode = file.split('_Anim')[0]
            if (self.sprCheck.isChecked() and not os.path.basename().endswith('_SPR.ma')) or (self.latestCheck.isChecked() and shotCode in latest):
                continue

            child = QtWidgets.QTreeWidgetItem(self.versionTree)
            child.setText(0, file)
            latest.append(shotCode)
            self.paths[file] = shot

    ############################################################################
    # @brief      start the rigth batc function
    #
    # @param      self  The object
    #
    def startBatch(self):
        items = [x.text(0) for x in self.versionTree.selectedItems()]
        if items:
            self.progress = rbwUI.RBWProgressBar(len(items))
            index = self.toolCombo.currentIndex()
            if not index:
                self.cameraExporterDev(items)
            elif index == 1:
                self.updateSet(items)
            elif index == 2:
                self.checkRemoteDev(items)
            elif index == 3:
                self.massiveBlast(items)

    ############################################################################
    # @brief      Load, export cameras
    #
    # @param      self  The object
    # @param      self  items  list of otem to be iterated
    #
    def cameraExporterDev(self, items):
        for file in items:
            self.progress.cycles['Main'].setText('Working on: {}'.format(file))

            cmd.file(newFile=True, f=True)
            savedShot = api.savedShot.SavedShot(params={'path': self.paths[file]})
            savedShot.load(confirm=False)

            api.log.logger().debug("Exporting camera for {}...".format(file))
            if self.fakeCheck.isChecked():
                savedShot.getDeptObject().exportCam(updateShotgun=False)
            else:
                formatDate = date.today().strftime("%Y_%m_%d")
                cameraPath = os.path.join(self.mcFolder, 'scenes', 'Animation', self.season, self.episode, '_editing', 'camera_exports', formatDate, 'other_formats').replace('\\', '/')
                if not os.path.exists(cameraPath):
                    os.makedirs(cameraPath)

                mbPath = os.path.join(cameraPath, '{}.mb'.format(self.savedShot.sceneName)).replace('\\', '/')
                maPath = os.path.join(cameraPath, '{}.ma'.format(self.savedShot.sceneName)).replace('\\', '/')
                xmlPath = os.path.join(cameraPath, '{}.xml'.format(self.savedShot.sceneName)).replace('\\', '/')
                chanPath = os.path.join(cameraPath, '{}.chan'.format(self.savedShot.sceneName)).replace('\\', '/')

                # export the camera
                cam = api.camera.getSceneCamera()
                api.camera.exportXml(cam, xmlPath)

                try:
                    cmd.file(mbPath, es=1, typ="mayaBinary", pr=1, ch=1, chn=1, con=1, exp=1, f=1)
                except:
                    api.log.logger().error('Error in save mb file')

                try:
                    cmd.file(maPath, es=1, typ="mayaAscii", pr=1, ch=1, chn=1, con=1, exp=1, f=1)
                except:
                    api.log.logger().error('Error in save ma file')

                api.camera.newChannelExporter(cam, chanPath)

                # move only the maya ascii files in the right place
                source = [os.path.join(cameraPath, x).replace('\\', '/') for x in os.listdir(cameraPath) if x.endswith('.ma')][0]
                dest = os.path.join(self.mcFolder, 'scenes', 'Animation', self.season, self.episode, '_editing', 'camera_exports', formatDate, os.path.basename(source)).replace('\\', '/')
                shutil.move(source, dest)

            self.progress.cycles['Main'].updateProgress()

        api.log.logger().debug("Export cameras done!")

    ############################################################################
    # @brief      Load, updateSet
    #
    # @param      self  The object
    # @param      self  items  list of otem to be iterated
    #
    def updateSet(self, items):
        for file in items:
            self.progress.cycles['Main'].setText('Working on: {}'.format(file))

            cmd.file(newFile=True, f=True)
            savedShot = api.savedShot.SavedShot(params={'path': self.paths[file]})
            savedShot.load(confirm=False)

            if cmd.objExists('SET'):
                sets = cmd.listRelatives('SET', children=True)
                if sets:
                    api.log.logger().debug("Updating set for {}...".format(file))
                    [checkUpd.updateSet(x) for x in sets]

            savedShot.note = "Automatic re-save from batch tool"
            savedShot.promoted = 0
            savedShot.save()

            api.log.logger().debug("Exporting playblast for {}...".format(file))
            playblast = pbTool.PlayblastTool('Anim')
            playblast.getOptions()

            self.progress.cycles['Main'].updateProgress()

        api.log.logger().debug("Set update done and playblast exported!")

    ############################################################################
    # @brief      Load, save
    #
    # @param      self  The object
    # @param      self  items  list of otem to be iterated
    #
    def checkRemoteDev(self, items):
        for file in items:
            self.progress.cycles['Main'].setText('Working on: {}'.format(file))

            savedShot = api.savedShot.SavedShot(params={'path': self.paths[file]})
            dept = scDepts.Secondary(savedShot=savedShot)

            cmd.file(newFile=True, f=True)
            dept.load(confirm=False)

            api.log.logger().debug("Updating set for {}...".format(file))

            savedShot.note = "Automatic re-save from batch tool"
            savedShot.promoted = 0
            savedShot.save()

            self.progress.cycles['Main'].updateProgress()

    ############################################################################
    # @brief      Load, blast
    #
    # @param      self  The object
    # @param      self  items  list of otem to be iterated
    #
    def massiveBlast(self, items):
        for file in items:
            self.progress.cycles['Main'].setText('Working on: {}'.format(file))

            cmd.file(newFile=True, f=True)
            savedShot = api.savedShot.SavedShot(params={'path': self.paths[file]})
            savedShot.load(confirm=False)

            api.log.logger().debug("Exporting playblast for {}...".format(file))
            playblast = pbTool.PlayblastTool('Anim')
            mel.eval('generateAllUvTilePreviews')
            mel.eval('setAttr "hardwareRenderingGlobals.alphaCutPrepass" 1;')
            time.sleep(10)
            playblast.getOptions(True)

            self.progress.cycles['Main'].updateProgress()

        api.log.logger().debug("Playblasts exported!")
