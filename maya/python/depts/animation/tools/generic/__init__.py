import os
import importlib

import api.widgets.rbw_UI as rbwUI
import api.log
# importlib.reload(rbwUI)
importlib.reload(api.log)

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'arakno')


################################################################################
# @brief      run initialize layout.
#
# @param      args  The arguments
#
def run_initLay(*args):
    import maya.cmds as cmd
    import animation.tools.generic.getImagePlane as getImagePlane
    import animation.tools.generic.getAudio as getAudio

    importlib.reload(getImagePlane)
    importlib.reload(getAudio)

    res = rbwUI.RBWInput(text="Insert shot tokens:", mode={'Season': 'string', 'Episode': 'string', 'Sequence': 'string', 'Shot': 'string'}).result()
    projectCode = os.getenv('PROJECT')

    imagePlaneTransform = getImagePlane.setImagePlane([projectCode, res['Season'], res['Episode'], res['sequence'], res['Shot']])
    getAudio.importAudio([projectCode, res['Season'], res['Episode'], res['sequence'], res['Shot']])

    cmd.select(clear=True)
    cmd.select(imagePlaneTransform)
    cmd.currentTime(2)

    api.log.logger().debug("Layout initialized!")


################################################################################
# @brief      run mocap tool.
#
# @param      args  The arguments
#
def run_fxLibrary(*args):
    import depts.animation.tools.generic.fxLibrary as fx
    importlib.reload(fx)

    win = fx.FxLibrary()
    win.show()


################################################################################
# @brief      run get audio.
#
# @param      args  The arguments
#
def run_getAudio(*args):
    import depts.animation.tools.generic.getAudio as getAudio
    importlib.reload(getAudio)

    getAudio.main()


################################################################################
# @brief      run mocap tool.
#
# @param      args  The arguments
#
def run_mocap(*args):
    import depts.animation.tools.generic.mocapTool as mocap
    importlib.reload(mocap)

    win = mocap.MocapTool()
    win.show()


################################################################################
# @brief      run batch tools.
#
# @param      args  The arguments
#
def run_batchTools(*args):
    import depts.animation.tools.generic.batchTools as batch
    importlib.reload(batch)

    win = batch.BatchTools()
    win.show()


################################################################################
# @brief      run scene builder.
#
# @param      args  The arguments
#
def run_sceneBuilder(*args):
    import depts.animation.tools.generic.sceneBuilder as builder
    importlib.reload(builder)

    win = builder.SceneBuilder()
    win.show()


initLayTool = {
    "name": "Initialize Layout",
    "launch": run_initLay,
    "icon": os.path.join(iconDir, "..", "animation", "init.png")
}

fxLibrary = {
    "name": "FX Library",
    "launch": run_fxLibrary,
    "icon": os.path.join(iconDir, "..", "animation", "fx.png")
}

getAudioTool = {
    "name": "Get Audio",
    "launch": run_getAudio,
    "icon": os.path.join(iconDir, "..", "animation", "audio.png")
}

mocapTool = {
    "name": "Mocap Tool",
    "launch": run_mocap,
    "icon": os.path.join(iconDir, "..", "animation", "mocap.png")
}

batchTools = {
    "name": "Batch Tools",
    "launch": run_batchTools,
    "icon": os.path.join(iconDir, "..", "animation", "batch.png")
}

sceneBuilderTool = {
    "name": "Scene Builder Tool",
    "launch": run_sceneBuilder,
    "icon": os.path.join(iconDir, "..", "animation", "builder.png"),
}

tools = [
    initLayTool,
    fxLibrary,
    getAudioTool,
    mocapTool,
    batchTools,
    sceneBuilderTool
]

PackageTool = {
    "name": "generic",
    "tools": tools
}
