import os
import importlib
import math
import maya.cmds as cmd
import maya.mel as mel

from PySide2 import QtWidgets, QtCore

import api.widgets.rbw_UI as rbwUI
# importlib.reload(rbwUI)


class MocapTool(rbwUI.RBWWindow):

    def __init__(self, *args, **kwargs):
        super(MocapTool, self).__init__(*args, **kwargs)

        if cmd.window('MocapTool', exists=True):
            cmd.deleteUI('MocapTool')
        self.setObjectName('MocapTool')

        self.fbxDir = os.path.join(os.getenv('MC_FOLDER'), 'data').replace('\\', '/')
        self.animDir = os.path.join(os.getenv('MC_FOLDER'), 'scenes', 'animation').replace('\\', '/')
        self.iconDir = os.path.join(os.getenv("LOCALPIPE"), "img", "icons", "animation").replace('\\', '/')

        self.initUI()
        self.updateMargins()

    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.tabWidget = rbwUI.RBWTabWidget(12)

        fbxTab = rbwUI.RBWFrame('V')
        self.fbxSeasonCombo = rbwUI.RBWComboBox('Season:', items=sorted([x for x in os.listdir(self.fbxDir) if x.startswith('S0')]))
        self.fbxSeasonCombo.activated.connect(self.fillFbxEpisodes)
        self.fbxEpisodeCombo = rbwUI.RBWComboBox('Episode:')
        self.fbxEpisodeCombo.activated.connect(self.fillFbxTree)
        self.fbxTree = rbwUI.RBWTreeWidget(['Available FBX'], scrollV=False)
        self.fbxTree.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.framerateEdit = rbwUI.RBWLineEdit('Framerate:', text='25')
        self.ingestFbxButton = rbwUI.RBWButton('Ingest', size=[None, 30])
        self.ingestFbxButton.clicked.connect(self.ingestFbx)
        fbxTab.addWidget(self.fbxSeasonCombo)
        fbxTab.addWidget(self.fbxEpisodeCombo)
        fbxTab.addWidget(self.fbxTree)
        fbxTab.addWidget(self.framerateEdit)
        fbxTab.addWidget(self.ingestFbxButton)

        mocapTab = rbwUI.RBWFrame('V')
        rigLay = rbwUI.RBWFrame('H', bgColor='transparent', margins=[0, 0, 0, 0])
        self.rigLabel = rbwUI.RBWLabel('Selected RIG: ', italic=False, size=12)
        self.rigButton = rbwUI.RBWButton('Update selection', size=[100, 25])
        self.rigButton.clicked.connect(self.updateRig)
        rigLay.addWidget(self.rigLabel)
        rigLay.addWidget(self.rigButton, alignment=QtCore.Qt.AlignRight)
        self.mocapTree = rbwUI.RBWTreeWidget(['Available MoCaps'], scrollV=False, decorate=True)
        buttonsLay = rbwUI.RBWFrame('H', bgColor='transparent', margins=[0, 0, 0, 0])
        self.treeButton = rbwUI.RBWButton('Update tree', size=[None, 30])
        self.treeButton.clicked.connect(self.fillMocapTree)
        self.mocapButton = rbwUI.RBWButton('Link MoCap', size=[None, 30])
        self.mocapButton.clicked.connect(self.linkMocap)
        self.bakeButton = rbwUI.RBWButton('Bake selection', size=[None, 30])
        self.bakeButton.clicked.connect(self.bakeConnected)
        buttonsLay.addWidget(self.treeButton)
        buttonsLay.addWidget(self.mocapButton)
        buttonsLay.addWidget(self.bakeButton)
        mocapTab.addWidget(rigLay)
        mocapTab.addWidget(self.mocapTree)
        mocapTab.addWidget(buttonsLay)

        self.tabWidget.addTab(fbxTab, 'FBX Ingest')
        self.tabWidget.addTab(mocapTab, 'Link MoCap')

        self.mainLayout.addWidget(self.tabWidget)

        if cmd.ls(sl=True):
            self.updateRig()

        self.setMinimumSize(500, 400)
        self.setTitle('Mocap tool')
        self.setIcon('../animation/mocap.png')
        self.setFocus()

    # ===============================<| FBX ingest functions |>==================================
    def fillFbxEpisodes(self, index):
        self.fbxEpisodeCombo.clear()
        self.fbxTree.clear()
        self.episodes = []
        self.season = self.fbxSeasonCombo.currentText()

        fbxEpisodesDir = os.path.join(self.fbxDir, self.season).replace('\\', '/')
        for episode in sorted([x for x in os.listdir(fbxEpisodesDir) if x.count('PREP')]):
            if episode == 'PREP100':
                for name in [x for x in os.listdir(os.path.join(fbxEpisodesDir, episode).replace('\\', '/')) if os.path.isdir(os.path.join(fbxEpisodesDir, episode, x).replace('\\', '/'))]:
                    self.episodes.append(name)
            else:
                self.episodes.append(episode)

        nCamEpisodesDir = os.path.join(self.fbxDir, 'nCam', self.season).replace('\\', '/')
        for episode in sorted([x for x in os.listdir(nCamEpisodesDir) if x.count('NCAM')]):
            self.episodes.append(episode)

        self.fbxEpisodeCombo.addItems(self.episodes)

    def fillFbxTree(self, index):
        self.fbxTree.clear()
        self.episode = self.episodes[index]
        self.selectedMocaps = []

        if self.episode.count('PREP'):
            self.fbxMocapsDir = os.path.join(self.fbxDir, self.season, self.episode).replace('\\', '/')
        elif self.episode.count('NCAM'):
            self.fbxMocapsDir = os.path.join(self.fbxDir, 'nCam', self.season, self.episode).replace('\\', '/')
        else:
            self.fbxMocapsDir = os.path.join(self.fbxDir, self.season, 'PREP100', self.episode).replace('\\', '/')

        for mocap in sorted([x for x in os.listdir(self.fbxMocapsDir) if x.count("{}_{}".format(os.getenv('PROJECT').upper(), self.episode if (self.episode.count('PREP') or self.episode.count('NCAM')) else 'PREP100')) and x.count('.fbx')]):
            child = QtWidgets.QTreeWidgetItem(self.fbxTree)
            child.setCheckState(0, QtCore.Qt.Unchecked)
            child.setText(1, mocap)

    def ingestFbx(self):
        if self.selectedMocaps:
            if rbwUI.RBWWarning(text="A new scene will be created. Do you want to proceed?", parent=self).result():
                for mocap in self.selectedMocaps:
                    # create new file
                    cmd.file(newFile=True, force=True)

                    # import fbx
                    fbxPath = os.path.join(self.fbxMocapsDir, mocap).replace('\\', '/')
                    cmd.file(fbxPath, i=True, typ="FBX", renameAll=True, options="fbx", preserveReferences=True, importTimeRange="combine")

                    # set fps & time range values
                    cmd.currentUnit(time='{}fps'.format(self.framerateEdit.text()))
                    animationEndTime = cmd.playbackOptions(query=True, animationEndTime=True)
                    animationEndTime = math.ceil(animationEndTime)
                    cmd.playbackOptions(edit=True, animationEndTime=animationEndTime)
                    playbackEndTime = cmd.playbackOptions(query=True, maxTime=True)
                    playbackEndTime = math.ceil(playbackEndTime)
                    cmd.playbackOptions(edit=True, maxTime=playbackEndTime)

                    # open hik character controls tool tab
                    mel.eval("HIKCharacterControlsTool;")

                    # hik character name
                    character = mocap.split("_")[3]
                    character = ''.join([i for i in character if not i.isdigit()])

                    # delete if hik character already present
                    [cmd.delete(node) for node in cmd.ls(l=True, exactType="HIKCharacterNode")]

                    # create hik character
                    self.hikCharacterName = mel.eval('hikCreateCharacter( "{}" );'.format(character))
                    mel.eval('hikUpdateCharacterList();')
                    mel.eval('hikSelectDefinitionTab();')

                    # T-Pose
                    cmd.currentTime(-5)
                    relatives = cmd.listRelatives('Reference', allDescendents=True)
                    for relative in relatives:
                        cmd.setAttr('{}.rotateX'.format(relative), 0)
                        cmd.setAttr('{}.rotateY'.format(relative), 0)
                        cmd.setAttr('{}.rotateZ'.format(relative), 0)

                    # HIK Retargeting
                    cmd.select('Reference', r=True)
                    mel.eval('hikMapBones')

                    mel.eval('hikToggleLockDefinition();')

                    # SAVE
                    path = os.path.join(self.fbxMocapsDir, mocap.replace('.fbx', '.ma')).replace('/', '\\')
                    cmd.file(rename=path if not path.count('\\nCam') else path.replace('\\nCam', '').replace('NCAM', 'PREP'))
                    cmd.file(save=True, type="mayaAscii")

        else:
            rbwUI.RBWWarning(text="Please select at least 1 fbx file to ingest!", defCancel=None, parent=self)
    # ===========================================================================================

    # ===============================<| Link MoCap functions |>==================================
    def updateRig(self):
        self.rigLabel.setText('Selected RIG: ')

        rigs = cmd.ls(sl=True)
        if rigs:
            if len(rigs) == 1 and rigs[0].count(':Group'):
                self.rigLabel.setText('{}: {}'.format(self.rigLabel.text().split(': ')[0], rigs[0].split(':')[0]))
                self.name = rigs[0].split(':')[0].split('_')[2]
                self.fillMocapTree()
            else:
                rbwUI.RBWWarning(text="Please select only 1 CHAR per time", defCancel=None, parent=self)
        else:
            rbwUI.RBWWarning(text="Please select a CHAR first", defCancel=None, parent=self)

    def fillMocapTree(self):
        self.mocapTree.clear()
        for season in sorted([x for x in os.listdir(self.fbxDir) if x.count('S0')]):
            child = QtWidgets.QTreeWidgetItem(self.mocapTree)
            child.setText(0, season)

            seasonDir = os.path.join(self.fbxDir, season).replace('\\', '/')
            for episode in sorted([x for x in os.listdir(seasonDir) if x.count('PREP')]):
                episodeDir = os.path.join(seasonDir, episode).replace('\\', '/') if episode != 'PREP100' else os.path.join(seasonDir, episode, self.name).replace('\\', '/')
                sequences = []

                if os.path.exists(episodeDir):
                    child1 = QtWidgets.QTreeWidgetItem(child)
                    child1.setText(0, episode if episode != 'PREP100' else self.name)

                    for seq in [x for x in os.listdir(episodeDir) if x.count('.ma')]:
                        if seq not in sequences:
                            sequences.append(seq)

                    for sequence in sorted(sequences):
                        if sequence.count(self.name):
                            child2 = QtWidgets.QTreeWidgetItem(child1)
                            child2.setText(0, sequence)

                    if not child1.childCount():
                        child.removeChild(child1)

            if not child.childCount():
                self.mocapTree.invisibleRootItem().removeChild(child)

    def linkMocap(self):
        item = self.mocapTree.selectedItems()[0]
        filename = item.text(0)
        episode = item.parent().text(0)
        season = item.parent().parent().text(0)

        if episode.count('PREP'):
            self.episodeDir = os.path.join(self.fbxDir, season, episode).replace('\\', '/')
        else:
            self.episodeDir = os.path.join(self.fbxDir, season, 'PREP100', episode).replace('\\', '/')

        if filename:
            mocapFile = os.path.join(self.episodeDir, filename).replace('\\', '/')
            cmd.file(mocapFile, i=True)

            if not cmd.objExists('EXTRA'):
                cmd.group(em=True, name='EXTRA')

            cmd.parent('Reference', 'EXTRA')
            cmd.rename('Reference', '{}_hikMocap'.format(self.name))

            uis = cmd.lsUI(l=True, type='optionMenuGrp')
            for ui in uis:
                if 'hikCharacterList' in ui:
                    characterCombo = ui
                elif 'hikSourceList' in ui:
                    sourceCombo = ui

            characterObj = [cmd.menuItem(x, l=True, q=True) for x in cmd.optionMenuGrp(characterCombo, ill=True, q=True) if self.name in cmd.menuItem(x, l=True, q=True)][0]

            mel.eval('hikUpdateCurrentCharacterFromUI()')
            mel.eval('hikUpdateCurrentSourceFromUI()')
            mel.eval('hikUpdateContextualUI()')

            sourceObj = [cmd.menuItem(x, l=True, q=True) for x in cmd.optionMenuGrp(sourceCombo, ill=True, q=True) if self.name in cmd.menuItem(x, l=True, q=True)][0]

            cmd.optionMenuGrp(characterCombo, e=True, value=characterObj)
            mel.eval('hikUpdateCurrentCharacterFromUI()')
            mel.eval('hikUpdateContextualUI()')
            cmd.optionMenuGrp(sourceCombo, e=True, value=sourceObj)
            mel.eval('hikUpdateCurrentSourceFromUI()')
            mel.eval('hikUpdateContextualUI()')

            mel.eval('hikUpdateContextualUI()')
        else:
            rbwUI.RBWWarning(text="Please select a MoCap file first", defCancel=None, parent=self)

    def bakeConnected(self):
        ctrlSet = cmd.ls(sl=True)
        if ctrlSet:
            if len(ctrlSet) == 1 and ctrlSet[0].count('_rig:ControlSet'):
                # rig controls
                rig_controls = cmd.listRelatives(ctrlSet)

                # hik rig controls
                hik_rig_controls = []
                for rig_control in rig_controls:
                    connections = cmd.listConnections(rig_control)
                    if connections:
                        for connection in connections:
                            if connection.count("CustomRigDefaultMappingNode"):
                                hik_rig_controls.append(rig_control)
                                break

                # time range values
                sf = cmd.playbackOptions(minTime=True, q=True)
                ef = cmd.playbackOptions(maxTime=True, q=True)

                # bake
                if hik_rig_controls:
                    cmd.bakeResults(hik_rig_controls, sm=True, time=(sf, ef), attribute=["tx", "ty", "tz", "rx", "ry", "rz"], sampleBy=1, sb=1, osr=1, dic=True, pok=True, sac=False, rba=False, ral=False, bol=False, mr=True, cp=False, s=True)

            else:
                rbwUI.RBWWarning(text="Please select only 1 ControlSet per time", defCancel=None, parent=self)
        else:
            rbwUI.RBWWarning(text="Please select a ControlSet first", defCancel=None, parent=self)
    # ===========================================================================================
