import os
import re
import importlib
import maya.cmds as cmd

from PySide2 import QtCore, QtGui, QtWidgets

import api.savedAsset
import api.savedShot
import api.shot
import api.scene
import api.widgets.rbw_UI as rbwUI
import api.database as db
import api.json

# importlib.reload(api.savedAsset)
# importlib.reload(api.shot)
importlib.reload(api.scene)
# importlib.reload(rbwUI)
importlib.reload(db)
importlib.reload(api.json)


class SceneBuilder(rbwUI.RBWWindow):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if cmd.window('SceneBuilder', exists=True):
            cmd.deleteUI('SceneBuilder')
        self.setObjectName('SceneBuilder')

        self.id_mv = os.getenv('ID_MV')
        self.animationDir = os.path.join(os.getenv('MC_FOLDER'), 'scenes', 'animation').replace('\\', '/')

        self.initUI()
        self.updateMargins()

    def initUI(self):
        self.setMain(True)
        self.setStyle()

        frame = rbwUI.RBWFrame('H')

        selGroup = rbwUI.RBWGroupBox('Select the shot to be built:', 'V', margins=[6, 22, 6, 6])
        self.selTree = rbwUI.RBWTreeWidget(['Available shots'], scrollV=False, decorate=True)
        self.selTree.itemClicked.connect(self.selectShot)
        self.saveCheck = rbwUI.RBWCheckBox('Save the first scene')
        self.saveCheck.setChecked(True)
        self.runButton = rbwUI.RBWButton('Build', size=[60, 30])
        self.runButton.clicked.connect(self.run)
        selGroup.addWidget(self.selTree)
        selGroup.addWidget(self.saveCheck)
        selGroup.addWidget(self.runButton, alignment=QtCore.Qt.AlignHCenter)
        selGroup.setFixedWidth(300)

        assetGroup = rbwUI.RBWGroupBox('These assets will be in the selected shot', 'V', margins=[6, 22, 6, 6])
        self.assetTree = rbwUI.RBWTreeWidget(['Asset', 'Id', 'Dept'], scrollV=False)
        assetGroup.addWidget(self.assetTree)

        frame.addWidget(selGroup)
        frame.addWidget(assetGroup)

        self.mainLayout.addWidget(frame)

        self.fillSelTree()

        self.setMinimumSize(800, 400)
        self.setTitle('Scene builder')
        self.setIcon('../animation/builder.png')
        self.setFocus()

    def fillSelTree(self):
        for season in db.selectQuery("SELECT DISTINCT `season` FROM `V_shotList` WHERE `projectID` = {}".format(self.id_mv)):
            seasonItem = QtWidgets.QTreeWidgetItem(self.selTree)
            seasonItem.setText(0, season[0])
            for episode in db.selectQuery("SELECT DISTINCT `episode` FROM `V_shotList` WHERE `projectID` = {} AND `season` = '{}'".format(self.id_mv, season[0])):
                episodeItem = QtWidgets.QTreeWidgetItem(seasonItem)
                episodeItem.setText(0, episode[0])
                for sequence in db.selectQuery("SELECT DISTINCT `sequence` FROM `V_shotList` WHERE `projectID` = {} AND `season` = '{}' AND `episode` = '{}'".format(self.id_mv, season[0], episode[0])):
                    sequenceItem = QtWidgets.QTreeWidgetItem(episodeItem)
                    sequenceItem.setText(0, sequence[0])
                    for shot in db.selectQuery("SELECT `season`, `episode`, `sequence`, `shot` FROM `V_shotList` WHERE `projectID` = {} AND `season` = '{}' AND `episode` = '{}' AND `sequence` = '{}'".format(self.id_mv, season[0], episode[0], sequence[0])):
                        shotItem = QtWidgets.QTreeWidgetItem(sequenceItem)
                        shotItem.setText(0, '_'.join([os.getenv('PROJECT').upper()] + list(shot)))

                    if not sequenceItem.childCount():
                        sequenceItem.deleteLater()
                if not episodeItem.childCount():
                    episodeItem.deleteLater()
            if not seasonItem.childCount():
                seasonItem.deleteLater()

    def selectShot(self, item, column):
        if not item.childCount():
            split = item.text(0).split('_')
            self.shotObj = api.shot.Shot(params={'season': split[1], 'episode': split[2], 'sequence': split[3], 'shot': split[4]})
            buildsFolder = os.path.join(self.shotObj.getShotFolder(), 'sys', 'BUILDS').replace('\\', '/')
            if os.path.exists(buildsFolder):
                filterString = r"{}_build_vr\d{{3}}.json".format(self.shotObj.getShotCode())
                files = sorted([os.path.join(buildsFolder, x).replace('\\', '/') for x in os.listdir(buildsFolder) if re.match(filterString, x)])
                if files:
                    self.assetTree.clear()
                    self.assets = {}

                    assets = api.json.json_read(files[-1])['assets']
                    for asset in assets:
                        if asset['dept'] == 'vfx':
                            asset['dept'] = 'fx'
                        code = '_'.join([asset['category'], asset['group'], asset['name'], asset['variant']])
                        item = QtWidgets.QTreeWidgetItem(self.assetTree)
                        item.setText(0, code)
                        item.setText(1, str(asset['assetid']))
                        item.setText(2, asset['dept'])

                        versions = db.selectQuery("SELECT * FROM `savedAsset` WHERE `id_mv` = {} AND `dept` = '{}' AND `path` LIKE '%{}%' AND `visible`= 1 ORDER BY `date` DESC".format(self.id_mv, asset['dept'], code))
                        if versions:
                            self.assets[code] = versions[0]
                        else:
                            [item.setForeground(i, QtGui.QColor('yellow')) for i in range(0, item.columnCount())]
                            [item.setToolTip(i, "This asset doesn't have this dept save yet!") for i in range(0, item.columnCount())]
                else:
                    rbwUI.RBWWarning(text="This shot doesn't have a build file yet!\nCreate it with the Prep tool first", defCancel=None)
            else:
                rbwUI.RBWWarning(text="This shot doesn't have a build file yet!\nCreate it with the Prep tool first", defCancel=None)
        else:
            rbwUI.RBWError(text='{} is not a shot!'.format(item.text(0)))

    def run(self):
        if not self.selTree.selectedItems():
            return

        cmd.file(new=True, force=True)
        savedShot = api.savedShot.SavedShot(params={'shot': self.shotObj, 'dept': 'Lay'})
        cmd.playbackOptions(animationEndTime=self.shotObj.getShotDuration())

        for asset, fields in self.assets.items():
            path = fields[2]
            savedAsset = api.savedAsset.SavedAsset(params={'path': path})
            namespace = savedAsset.getNamespace()

            if savedAsset.asset.category == 'character':
                api.scene.addToCharacterGroup(path, namespace)
            elif savedAsset.asset.category == 'prop':
                api.scene.addToPropGroup(path, namespace)
            elif savedAsset.asset.category == 'library':
                api.scene.addToLibraryGroup(path, namespace)
            elif savedAsset.asset.category == 'fx':
                api.scene.addToFxGroup(path, namespace)
            else:
                api.scene.addToSetGroup(path)

        if self.saveCheck.isChecked():
            savedShot.note = 'First build from Scene Builder'
            savedShot.save()

        rbwUI.RBWDialog(text="Scene built!")
