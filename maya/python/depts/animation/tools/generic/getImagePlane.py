import os
import importlib
import maya.cmds as cmd
import maya.mel as mel

import api.widgets.rbw_UI as rbwUI
import api.log

# importlib.reload(rbwUI)
importlib.reload(api.log)


def setImagePlane(prjCode, season, episode, sequence, shot):
    cameraList = cmd.listCameras()
    cameraToExclude = ['persp', 'top', 'front', 'side']
    cameraList = list(set(cameraList) - set(cameraToExclude))
    try:
        renderCam = cameraList[0]
        api.log.logger().debug('renderCam: {}'.format(renderCam))
    except IndexError:
        rbwUI.RBWError(text="There isn't any RBW Camera in this scene,\nplease import a new RBW Camera first!")
        raise IndexError

    imageBasePath = '//speed.nas/{}/01_pre_production/08_animatic/{}/Individual_Shots/JPG/{}_{}_{}_{}_{}'.format(prjCode, episode, prjCode.upper(), season, episode, sequence, shot)
    imageFirstFile = '{}_{}_{}_{}_{}_0000.jpg'.format(prjCode.upper(), season, episode, sequence, shot)
    imageFirstFilePath = '/'.join((imageBasePath, imageFirstFile))

    if os.path.exists(imageFirstFilePath):
        api.log.logger().debug('imageFirstFilePath: {}'.format(imageFirstFilePath))
        imagePlaneName = 'imagePlane_Layout'
        realNameList = cmd.imagePlane(camera=renderCam, name=imagePlaneName, fileName=imageFirstFilePath)

        realTransformName = realNameList[0]
        realShapeName = realNameList[1]

        cmd.setAttr('{}.alphaGain'.format(realShapeName), 0.4)
        cmd.setAttr('{}.useFrameExtension'.format(realShapeName), 1)
        cmd.setAttr('{}.frameOffset'.format(realShapeName), -1)
        cmd.setAttr('{}.displayOnlyIfCurrent'.format(realShapeName), True)
        cmd.optionMenu('AELookThroughCameraMenu', edit=True, enable=True)
        mel.eval("AEchangeLookThroughCamera {};".format(realShapeName))

        cmd.setAttr('{}.filmFit'.format(renderCam), 1)
        cmd.setAttr('{}.overscan'.format(renderCam), 1)

        return realTransformName


def main():
    projectCode = os.getenv('PROJECT')

    sceneName = cmd.file(query=True, expandName=True).split('/')[-1]
    season = sceneName.split('_')[1]
    episode = sceneName.split('_')[2]
    sequence = sceneName.split('_')[3]
    shot = sceneName.split('_')[4]

    setImagePlane(projectCode, season, episode, sequence, shot)
