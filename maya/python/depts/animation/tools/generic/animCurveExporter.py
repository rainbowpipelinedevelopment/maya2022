import importlib
import os
import datetime

import maya.cmds as cmd

import api.log
import api.json

importlib.reload(api.log)
importlib.reload(api.json)


################################################################################
# @brief      Exception for signaling export errors.
#
class ExportException(Exception):
    pass


################################################################################
# @brief      This class describes an export data.
#
class ExportData:
    filePath = ""
    listObjects = []
    saveHierarchy = False
    saveHiddenObjs = False
    saveNotKeyed = False
    startFrame = None
    endFrame = None

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      filePath  The file path
    # @param      listObjs  The list objects
    # @param      saveH     The save h
    # @param      saveHid   The save hid
    # @param      saveNK    The save nk
    # @param      sFrame    The s frame
    # @param      eFrame    The e frame
    #
    def __init__(self, filePath="", listObjs=[], saveH=False, saveHid=False, saveNK=False, sFrame=None, eFrame=None):
        self.filePath = filePath
        self.listObjects = listObjs
        self.saveHierarchy = saveH
        self.saveHiddenObjs = saveHid
        self.saveNotKeyed = saveNK
        self.startFrame = sFrame
        self.endFrame = eFrame


################################################################################
# @brief      export the amin curves.
#
# @param      exportData  The export data
#
def exportAnimCurves(exportData):
    expandedObjsList = []

    # expand sets
    for obj in exportData.listObjects:

        if cmd.nodeType(obj) == "objectSet":
            cmd.select(clear=True)
            cmd.select(obj, noExpand=False, replace=True)
            childs = cmd.ls(selection=True, long=True)
            cmd.select(clear=True)

            if childs:
                for child in childs:
                    expandedObjsList.append(child)
        else:
            if cmd.listAnimatable(obj):
                expandedObjsList.append(obj)

    # exclude hidden objects
    removedObjs = []
    if not exportData.saveHiddenObjs:
        for obj in expandedObjsList:
            try:
                if cmd.getAttr(obj + ".visibility") == 0:
                    removedObjs.append(obj)
            except:
                pass

    for obj in removedObjs:
        expandedObjsList.remove(obj)

    childsObj = expandedObjsList
    # expand hierarchy
    if exportData.saveHierarchy:
        for obj in childsObj:
            childs = cmd.listRelatives(obj, ad=1, f=1)
            if childs:
                for i in range(0, len(childs)):
                    if expandedObjsList.count(childs[i]) == 0:
                        if cmd.listAnimatable(childs[i]):
                            expandedObjsList.append(childs[i])

    # start create the dictionary
    mainDict = {}
    user = os.getenv("USER")
    time = datetime.datetime.now()
    Tm = time.strftime("%Y.%m.%d-%H.%M.%S")
    filename = cmd.file(q=1, sn=1, shn=1)
    project = os.path.basename(cmd.workspace(fn=1))

    info = {}
    info["USER"] = user
    info["date"] = Tm
    info["filename"] = filename
    info["project"] = project

    mainDict["__info__"] = info
    writeFile = False

    for node in expandedObjsList:
        nodeName = node

        if nodeName not in mainDict.keys():
            mainDict[nodeName] = {}
        else:
            continue

        channels = cmd.listConnections(node, t="animCurve")
        mutes = cmd.listConnections(node, t="mute")
        if mutes:
            for mute in mutes:
                if not channels:
                    channels = []
                channels.append("*mute*" + mute)

        # check if there is some constraint with keyframes
        parentConstraint = False
        orientConstraint = False
        pointConstraint = False
        aimConstraint = False
        normalConstraint = False
        pointOnPolyConstraint = False
        tangentConstraint = False
        posBlendNode = None
        rotBlendNode = None

        if cmd.attributeQuery("blendParent1", node=node, ex=1):
            posBlendNode = cmd.listConnections(node + ".blendParent1", p=0, t="pairBlend")
            if posBlendNode:
                parentConstraint = True
                posBlendNode = posBlendNode[0]
                rotBlendNode = posBlendNode

        if cmd.attributeQuery("blendOrient1", node=node, ex=1):
            rotBlendNode = cmd.listConnections(node + ".blendOrient1", p=0, t="pairBlend")
            if rotBlendNode:
                orientConstraint = True
                rotBlendNode = rotBlendNode[0]

        if cmd.attributeQuery("blendPoint1", node=node, ex=1):
            posBlendNode = cmd.listConnections(node + ".blendPoint1", p=0, t="pairBlend")
            if posBlendNode:
                pointConstraint = True
                posBlendNode = posBlendNode[0]

        if cmd.attributeQuery("blendAim1", node=node, ex=1):
            rotBlendNode = cmd.listConnections(node + ".blendAim1", p=0, t="pairBlend")
            if rotBlendNode:
                aimConstraint = True
                rotBlendNode = rotBlendNode[0]

        if cmd.attributeQuery("blendNormal1", node=node, ex=1):
            rotBlendNode = cmd.listConnections(node + ".blendNormal1", p=0, t="pairBlend")
            if rotBlendNode:
                normalConstraint = True
                rotBlendNode = rotBlendNode[0]

        if cmd.attributeQuery("blendPointOnPoly1", node=node, ex=1):
            posBlendNode = cmd.listConnections(node + ".blendPointOnPoly1", p=0, t="pairBlend")
            if posBlendNode:
                pointOnPolyConstraint = True
                posBlendNode = posBlendNode[0]
                rotBlendNode = posBlendNode

        if cmd.attributeQuery("blendTangent1", node=node, ex=1):
            rotBlendNode = cmd.listConnections(node + ".blendTangent1", p=0, t="pairBlend")
            if rotBlendNode:
                tangentConstraint = True
                rotBlendNode = rotBlendNode[0]

        if channels:

            if parentConstraint or pointConstraint or pointOnPolyConstraint:
                if channels.count("translateX") == 0:
                    channels.append("translateX")
                if channels.count("translateY") == 0:
                    channels.append("translateY")
                if channels.count("translateZ") == 0:
                    channels.append("translateZ")

            if parentConstraint or orientConstraint or aimConstraint or normalConstraint or \
                    pointOnPolyConstraint or tangentConstraint:
                if channels.count("rotateX") == 0:
                    channels.append("rotateX")
                if channels.count("rotateY") == 0:
                    channels.append("rotateY")
                if channels.count("rotateZ") == 0:
                    channels.append("rotateZ")

            for chan in channels:
                channelName = ""
                if (chan == "translateX") or (chan == "translateY") or (chan == "translateZ"):
                    channelName = chan
                    if posBlendNode:
                        if chan == "translateX":
                            chanls = cmd.listConnections(posBlendNode + ".inTranslateX1", p=0)
                            if chanls:
                                chan = chanls[0]
                            else:
                                continue
                        elif chan == "translateY":
                            chanls = cmd.listConnections(posBlendNode + ".inTranslateY1", p=0)
                            if chanls:
                                chan = chanls[0]
                            else:
                                continue
                        elif chan == "translateZ":
                            chanls = cmd.listConnections(posBlendNode + ".inTranslateZ1", p=0)
                            if chanls:
                                chan = chanls[0]
                            else:
                                continue
                elif chan == "rotateX" or chan == "rotateY" or chan == "rotateZ":
                    channelName = chan
                    if rotBlendNode:
                        if chan == "rotateX":
                            chanls = cmd.listConnections(rotBlendNode + ".inRotateX1", p=0)
                            if chanls:
                                chan = chanls[0]
                            else:
                                continue
                        elif chan == "rotateY":
                            chanls = cmd.listConnections(rotBlendNode + ".inRotateY1", p=0)
                            if chanls:
                                chan = chanls[0]
                            else:
                                continue
                        elif chan == "rotateZ":
                            chanls = cmd.listConnections(rotBlendNode + ".inRotateZ1", p=0)
                            if chanls:
                                chan = chanls[0]
                            else:
                                continue

                elif chan.count("*mute*") > 0:
                    chan = chan.replace("*mute*", "")
                    channelName = cmd.listConnections(chan + ".output", p=1)[0].split('.')[-1:][0]

                    try:
                        animNode = cmd.listConnections(chan + ".input")[0]
                        mainDict[nodeName][channelName] = {}
                        mainDict[nodeName][channelName]['type'] = "anim"

                        preInf = cmd.getAttr(animNode + ".preInfinity")
                        mainDict[nodeName][channelName]["preInf"] = preInf

                        postInf = cmd.getAttr(animNode + ".postInfinity")
                        mainDict[nodeName][channelName]["postInf"] = postInf

                        weightTang = cmd.getAttr(animNode + ".weightedTangents")
                        mainDict[nodeName][channelName]["weightTang"] = weightTang
                    except:
                        api.log.logger().error("Error on mute node: {}".format(channelName))
                        continue

                else:
                    channelName = cmd.listConnections(chan, p=1)[0].split('.')[-1:][0]

                    # animData = AnimCurve.AnimCurve(node,chan,channelName,exportData.startFrame,exportData.endFrame)
                    mainDict[nodeName][channelName] = {}
                    mainDict[nodeName][channelName]['type'] = "anim"

                    preInf = cmd.getAttr(chan + ".preInfinity")
                    mainDict[nodeName][channelName]["preInf"] = preInf

                    postInf = cmd.getAttr(chan + ".postInfinity")
                    mainDict[nodeName][channelName]["postInf"] = postInf

                    weightTang = cmd.getAttr(chan + ".weightedTangents")
                    mainDict[nodeName][channelName]["weightTang"] = weightTang

                keys = cmd.keyframe(node, at=channelName, q=1)
                if not keys:
                    continue

                writeFile = True

                time0 = keys[0]
                time1 = keys[-1:][0]

                if exportData.startFrame:
                    time0 = exportData.startFrame
                if exportData.endFrame:
                    time1 = exportData.endFrame

                keys = cmd.keyframe(node, at=channelName, q=1, t=(time0, time1))
                try:
                    mainDict[nodeName][channelName]["keys"] = keys
                except:
                    api.log.logger().error("Error in: {} {}".format(nodeName, channelName))
                    raise ExportException(nodeName)

                values = cmd.keyframe(node, at=channelName, q=1, vc=1, t=(time0, time1))
                mainDict[nodeName][channelName]["values"] = values

                inTan = cmd.keyTangent(node, at=channelName, q=1, itt=1, t=(time0, time1))
                mainDict[nodeName][channelName]["inTan"] = inTan

                outTan = cmd.keyTangent(node, at=channelName, q=1, ott=1, t=(time0, time1))
                mainDict[nodeName][channelName]["outTan"] = outTan

                tanLock = cmd.keyTangent(node, at=channelName, q=1, lock=1, t=(time0, time1))
                mainDict[nodeName][channelName]["tanLock"] = tanLock

                weightLock = cmd.keyTangent(node, at=channelName, q=1, weightLock=1, t=(time0, time1))
                mainDict[nodeName][channelName]["weightLock"] = weightLock

                breakDown = cmd.keyframe(node, at=channelName, q=1, breakdown=1, t=(time0, time1))
                mainDict[nodeName][channelName]["breakDown"] = breakDown

                inAngle = cmd.keyTangent(node, at=channelName, q=1, inAngle=1, t=(time0, time1))
                mainDict[nodeName][channelName]["inAngle"] = inAngle

                outAngle = cmd.keyTangent(node, at=channelName, q=1, outAngle=1, t=(time0, time1))
                mainDict[nodeName][channelName]["outAngle"] = outAngle

                inWeight = cmd.keyTangent(node, at=channelName, q=1, inWeight=1, t=(time0, time1))
                mainDict[nodeName][channelName]["inWeight"] = inWeight

                outWeight = cmd.keyTangent(node, at=channelName, q=1, outWeight=1, t=(time0, time1))
                mainDict[nodeName][channelName]["outWeight"] = outWeight

        if exportData.saveNotKeyed:
            attrs = cmd.listAttr(node, k=1)
            if attrs:
                for attr in attrs:
                    try:
                        animNode = cmd.listConnections(node + "." + attr, s=1, d=0)
                        if not animNode:
                            writeFile = True
                            mainDict[nodeName][attr] = {}
                            mainDict[nodeName][attr]['type'] = "static"
                            mainDict[nodeName][attr]['value'] = cmd.getAttr(node + "." + attr)
                            mainDict[nodeName][attr]['attrType'] = str(cmd.getAttr(node + "." + attr, type=True))
                    except:
                        pass

    if writeFile:
        api.json.json_write(mainDict, exportData.filePath)
        # file = open(exportData.filePath, "w")
        # cPickle.dump(mainDict, file, -1)
        # file.close()
