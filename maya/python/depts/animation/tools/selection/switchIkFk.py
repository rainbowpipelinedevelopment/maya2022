import os
import importlib
import maya.cmds as cmd
import functools

from PySide2 import QtGui, QtWidgets

import api.widgets.rbw_UI as rbwUI
import api.log

# importlib.reload(rbwUI)
importlib.reload(api.log)

data = {
    'hand': {
        'R': {
            'jnt': ["R_upArm_jnt", "R_elbow_jnt", "R_wrist_jnt"],
            '0': ["R_upArm_fk_Ctrl", "R_elbow_fk_Ctrl", "R_wrist_fk_Ctrl"],
            '1': ["R_arm_ik_Ctrl", "R_elbow_pv_Ctrl", "R_hand_Ctrl"]
        },
        'L': {
            'jnt': ["L_upArm_jnt", "L_elbow_jnt", "L_wrist_jnt"],
            '0': ["L_upArm_fk_Ctrl", "L_elbow_fk_Ctrl", "L_wrist_fk_Ctrl"],
            '1': ["L_arm_ik_Ctrl", "L_elbow_pv_Ctrl", "L_hand_Ctrl"]
        }
    },
    'leg': {
        'R': {
            'jnt': ["R_upLeg_jnt", "R_knee_jnt", "R_ankle_jnt", "R_ball_jnt"],
            '0': ["R_upLeg_fk_Ctrl", "R_knee_fk_Ctrl", "R_ankle_fk_Ctrl"],
            '1': ["R_knee_pv_Ctrl", "R_heel_ik_Ctrl"]
        },
        'L': {
            'jnt': ["L_upLeg_jnt", "L_knee_jnt", "L_ankle_jnt", "L_ball_jnt"],
            '0': ["L_upLeg_fk_Ctrl", "L_knee_fk_Ctrl", "L_ankle_fk_Ctrl"],
            '1': ["L_knee_pv_Ctrl", "L_heel_ik_Ctrl"]
        }
    }
}

rotateAttr = [".rotateX", ".rotateY", ".rotateZ"]

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'animation')


class IkFkSwitch(rbwUI.RBWWindow):

    def __init__(self):
        self.nsp = ''
        sel = cmd.ls(sl=1)
        if sel:
            self.nsp = sel[0].split(":")[0]
            for part in data:
                for side in data[part]:
                    for fk in data[part][side]:
                        if fk != 'jnt':
                            for ctrl in data[part][side][fk]:
                                try:
                                    cmd.select('{}:{}'.format(self.nsp, ctrl), r=1)
                                except:
                                    rbwUI.RBWWarning(text="The control: {}:{} does not exists\n\nThis asset is not good for the new IK-FK snap tool, there aren't the arm necessary parts".format(self.nsp, ctrl), defCancel=None)
                                    return

            super(IkFkSwitch, self).__init__()

            cmd.select(clear=1)
            if cmd.window('IkFkSwitch{}'.format(self.nsp), exists=True):
                cmd.deleteUI('IkFkSwitch{}'.format(self.nsp))
            self.setObjectName("IkFkSwitch{}".format(self.nsp))

            self.initUI()
            self.updateMargins()
            self.show()

            cmd.select(sel, replace=True)
        else:
            rbwUI.RBWWarning(text="You didn't select any control, please check", defCancel=None)

    def initUI(self):
        self.setMain(True)
        self.setStyle()

        frame = rbwUI.RBWFrame('G')

        self.armR = ikfkButton('Right', 'hand', self.nsp)
        self.armL = ikfkButton('Left', 'hand', self.nsp)
        self.legR = ikfkButton('Right', 'leg', self.nsp)
        self.legL = ikfkButton('Left', 'leg', self.nsp)

        frame.addWidget(self.armR, 0, 0)
        frame.addWidget(self.armL, 0, 1)
        frame.addWidget(self.legR, 1, 0)
        frame.addWidget(self.legL, 1, 1)

        self.mainLayout.addWidget(frame)

        self.setMinimumSize(330, 80)
        self.setTitle("IK/FK - {}".format(self.nsp))
        self.setIcon('../animation/ikfk.png')
        self.setFocus()


class ikfkButton(QtWidgets.QPushButton):
    def __init__(self, side, part, name):
        super(ikfkButton, self).__init__()

        self.side = side
        self.part = part
        self.name = name

        self.attribute = '{}:{}_{}_Ctrl.Switch_IKFK'.format(self.name, self.side[0], self.part)
        self.fk = 0 if cmd.getAttr(self.attribute) else 1

        self.initUI()
        self.updateButton()
        self.clicked.connect(functools.partial(self.switch, self.part, self.side[0]))

    def initUI(self):
        mainLayout = QtWidgets.QHBoxLayout()
        mainLayout.setContentsMargins(0, 0, 0, 0)

        self.icon = QtWidgets.QLabel()
        self.icon.setStyleSheet('background-color: transparent')
        self.text = QtWidgets.QLabel()
        self.text.setStyleSheet('background-color: transparent')

        if self.side == 'Right':
            mainLayout.addStretch()
            mainLayout.addWidget(self.text)
            mainLayout.addWidget(self.icon)
        else:
            mainLayout.addWidget(self.icon)
            mainLayout.addWidget(self.text)
            mainLayout.addStretch()

        self.setLayout(mainLayout)
        self.setStyleSheet('min-height: 35px')

    def updateButton(self):
        image = QtGui.QPixmap(os.path.join(iconDir, '{}_{}_{}.png'.format(self.side, self.part, 'ik' if self.fk else 'fk')).replace('\\', '/')).scaled(30, 30)
        title = '{} {} to {}'.format(self.side, self.part, 'IK' if self.fk else 'FK')
        self.icon.setPixmap(image)
        self.text.setText(title)

    def getMultAttr(self, joints, attrType):
        attributes = {}
        for joint in joints:
            attributes[joint] = {}
            for attr in attrType:
                attributes[joint][attr] = cmd.getAttr("{}:{}{}".format(self.name, joint, attr))
        return attributes

    def setMultAttr(self, fromJoints, toJoints, fromAttrType, toAttrType, attrDict):
        for fromJoint, toJoint in zip(fromJoints, toJoints):
            for fromAttr, toAttr in zip(fromAttrType, toAttrType):
                value = attrDict[fromJoint][fromAttr]
                cmd.setAttr("{}:{}{}".format(self.name, toJoint, toAttr), value)

    def switch(self, part, side):
        # open the undo chunk
        cmd.undoInfo(openChunk=True)

        ctrl = data[self.part][side]
        switch = cmd.getAttr(self.attribute)

        if part == "hand":
            if switch:  # IK -> FK -----------------------------------------------------------------------------------------------
                fromAngles = self.getMultAttr(ctrl['jnt'][:2], rotateAttr)

                cmd.setAttr(self.attribute, 0)

                self.setMultAttr(ctrl['jnt'][:2], ctrl[str(self.fk)][:2], rotateAttr, rotateAttr, fromAngles)

                api.log.logger().debug("-> The {} IK-Arm now match to the FK-Arm ones.".format(side))
            else:  # FK -> IK ----------------------------------------------------------------------------------------------------
                autoClavicle = cmd.getAttr("{}:{}{}".format(self.name, ctrl[str(self.fk)][0], '.clavicleFollow'))
                if autoClavicle:
                    if cmd.keyframe("{}:{}".format(self.name, ctrl[str(self.fk)][0]), attribute='clavicleFollow', q=True, tc=True):
                        api.log.logger().debug("-> The 'clavicleFollow' attribute of the {}:{}{} control is animated, please remove the animation in order to make the snapping from FK to IK.".format(self.name, side, ctrl[str(self.fk)][0]))
                    else:
                        cmd.setAttr("{}:{}{}".format(self.name, ctrl[str(self.fk)][0], '.clavicleFollow'), 0)

                orientElbow = cmd.spaceLocator(name="tmpElbow_loc")
                poc = cmd.pointConstraint("{}:{}".format(self.name, ctrl['jnt'][1]), orientElbow, mo=0)
                cmd.delete(poc)

                orientHand = cmd.spaceLocator(name="tmpHand_loc")
                poc = cmd.pointConstraint("{}:{}".format(self.name, ctrl['jnt'][2]), orientHand, mo=0)
                cmd.delete(poc)
                orc = cmd.orientConstraint("{}:{}".format(self.name, ctrl['jnt'][2]), orientHand, mo=0)
                cmd.delete(orc)

                cmd.setAttr(self.attribute, 1)

                poc = cmd.pointConstraint(orientHand, "{}:{}".format(self.name, ctrl[str(self.fk)][0]), mo=0)
                cmd.delete(poc)

                poc = cmd.pointConstraint(orientElbow, "{}:{}".format(self.name, ctrl[str(self.fk)][1]), offset=(0, 0, -10))
                cmd.delete(poc)
                cmd.delete(orientElbow)
                if side == "R":
                    orc = cmd.orientConstraint(orientHand, "{}:{}".format(self.name, ctrl[str(self.fk)][0]), offset=(180, 0, 0), w=1)
                else:
                    orc = cmd.orientConstraint(orientHand, "{}:{}".format(self.name, ctrl[str(self.fk)][0]), offset=(0, 0, 0), w=1)
                cmd.delete(orc)
                cmd.delete(orientHand)

                api.log.logger().debug("-> The {} FK-Arm now match the IK-Arm ones".format(side))
        else:
            if switch:  # IK -> FK -----------------------------------------------------------------------------------------------
                posKnee = cmd.spaceLocator(name="tmpIkKnee_loc")
                poc = cmd.pointConstraint("{}:{}".format(self.name, ctrl['jnt'][1]), posKnee, mo=0)
                cmd.delete(poc)

                posAnkle = cmd.spaceLocator(name="tmpIkAnkle_loc")
                poc = cmd.pointConstraint("{}:{}".format(self.name, ctrl['jnt'][2]), posAnkle, mo=0)
                cmd.delete(poc)

                cmd.setAttr(self.attribute, 0)

                if side == 'L':
                    aim1 = cmd.aimConstraint(posKnee[0], "{}:{}".format(self.name, ctrl[str(self.fk)][0]), offset=[0, 0, 0], weight=1, aimVector=[1, 0, 0], upVector=[0, 1, 0], worldUpType="vector", worldUpVector=[0, 1, 0])
                    cmd.delete(aim1)
                    aim2 = cmd.aimConstraint(posAnkle[0], "{}:{}".format(self.name, ctrl[str(self.fk)][1]), offset=[0, 0, 0], weight=1, aimVector=[1, 0, 0], upVector=[0, 1, 0], worldUpType="vector", worldUpVector=[0, 1, 0])
                    cmd.delete(aim2)
                elif side == 'R':
                    aim1 = cmd.aimConstraint(posKnee[0], "{}:{}".format(self.name, ctrl[str(self.fk)][0]), offset=[0, 0, 0], weight=1, aimVector=[-1, 0, 0], upVector=[0, -1, 0], worldUpType="vector", worldUpVector=[0, 1, 0])
                    cmd.delete(aim1)
                    aim2 = cmd.aimConstraint(posAnkle[0], "{}:{}".format(self.name, ctrl[str(self.fk)][1]), offset=[0, 0, 0], weight=1, aimVector=[-1, 0, 0], upVector=[0, -1, 0], worldUpType="vector", worldUpVector=[0, 1, 0])
                    cmd.delete(aim2)

                cmd.delete(posKnee)
                cmd.delete(posAnkle)

                rotX = cmd.getAttr("{}:{}.rotateX".format(self.name, ctrl[str(self.fk)][0]))
                rotY = cmd.getAttr("{}:{}.rotateY".format(self.name, ctrl[str(self.fk)][0]))
                rotZ = cmd.getAttr("{}:{}.rotateZ".format(self.name, ctrl[str(self.fk)][0]))
                if abs(rotX) > 30:
                    cmd.setAttr("{}:{}.rotateX".format(self.name, ctrl[str(self.fk)][0]), 0)
                    x = cmd.getAttr("{}:{}.rotateX".format(self.name, ctrl[str(self.fk)][1]))
                    y = cmd.getAttr("{}:{}.rotateY".format(self.name, ctrl[str(self.fk)][1]))
                    z = cmd.getAttr("{}:{}.rotateZ".format(self.name, ctrl[str(self.fk)][1]))
                    if abs(x) > 60:
                        cmd.setAttr("{}:{}.rotateX".format(self.name, ctrl[str(self.fk)][1]), 0)
                    if abs(y) > 160:
                        cmd.setAttr("{}:{}.rotateY".format(self.name, ctrl[str(self.fk)][1]), 0)
                        cmd.setAttr("{}:{}.rotateZ".format(self.name, ctrl[str(self.fk)][1]), z + 45)
                    if z < 0:
                        cmd.setAttr("{}:{}.rotateZ".format(self.name, ctrl[str(self.fk)][1]), z * -1)
                if abs(rotY) > 45:
                    cmd.setAttr("{}:{}.rotateY".format(self.name, ctrl[str(self.fk)][0]), 0)
                    cmd.setAttr("{}:{}.rotateZ".format(self.name, ctrl[str(self.fk)][0]), rotZ * -1)
                    z = cmd.getAttr("{}:{}.rotateZ".format(self.name, ctrl[str(self.fk)][1]))
                    if z < 0:
                        cmd.setAttr("{}:{}.rotateZ".format(self.name, ctrl[str(self.fk)][1]), z * -1)

                api.log.logger().debug("-> The {} IK-Leg has been snap to the FK-Leg ones".format(side))
            else:  # FK -> IK ----------------------------------------------------------------------------------------------------
                orientKnee = cmd.spaceLocator(name="tmpKnee_loc")
                poc = cmd.pointConstraint("{}:{}".format(self.name, ctrl['jnt'][1]), orientKnee, mo=0)
                cmd.delete(poc)
                orc = cmd.orientConstraint("{}:{}".format(self.name, ctrl['jnt'][1]), orientKnee, offset=(-90, -90, 0))
                cmd.delete(orc)

                orientFoot = cmd.spaceLocator(name="tmpFoot_loc")
                poc = cmd.pointConstraint("{}:{}".format(self.name, ctrl['jnt'][2]), orientFoot, mo=0)
                cmd.delete(poc)
                orc = cmd.orientConstraint("{}:{}".format(self.name, ctrl['jnt'][2]), orientFoot, mo=0)
                cmd.delete(orc)

                cmd.setAttr(self.attribute, 1)

                poc = cmd.pointConstraint(orientFoot, "{}:{}".format(self.name, ctrl[str(self.fk)][1]), mo=0)
                cmd.delete(poc)
                cmd.delete(orientFoot)

                if side == "R":
                    orc = cmd.orientConstraint(orientKnee, "{}:{}".format(self.name, ctrl[str(self.fk)][0]), mo=0, w=1)
                    cmd.delete(orc)
                    poc = cmd.pointConstraint(orientKnee, "{}:{}".format(self.name, ctrl[str(self.fk)][0]), offset=(0, 0, 40))
                    cmd.delete(poc)
                    aim1 = cmd.aimConstraint(orientKnee[0], "{}:{}".format(self.name, ctrl[str(self.fk)][0]), offset=[0, 0, 0], weight=1, aimVector=[0, 0, -1], upVector=[0, 1, 0], worldUpType="vector", worldUpVector=[0, 1, 0])
                    cmd.delete(aim1)
                else:
                    orc = cmd.orientConstraint(orientKnee, "{}:{}".format(self.name, ctrl[str(self.fk)][0]), mo=0, w=1)
                    cmd.delete(orc)
                    poc = cmd.pointConstraint(orientKnee, "{}:{}".format(self.name, ctrl[str(self.fk)][0]), offset=(0, 0, 40))
                    cmd.delete(poc)
                    aim1 = cmd.aimConstraint(orientKnee[0], "{}:{}".format(self.name, ctrl[str(self.fk)][0]), offset=[0, 0, 0], weight=1, aimVector=[0, 0, 1], upVector=[0, -1, 0], worldUpType="vector", worldUpVector=[0, 1, 0])
                    cmd.delete(aim1)
                cmd.delete(orientKnee)

                api.log.logger().debug("-> The {} FK-Leg has been snap to the IK-Leg ones".format(side))

        # close the undo chunk
        cmd.undoInfo(closeChunk=True)

        self.fk = 0 if cmd.getAttr(self.attribute) else 1
        self.updateButton()
