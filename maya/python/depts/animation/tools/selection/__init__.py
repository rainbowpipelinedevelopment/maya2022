import os
import importlib
import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI
import api.log
# importlib.reload(rbwUI)
importlib.reload(api.log)

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'arakno')


################################################################################
# @brief      run select anim curves.
#
# @param      args  The arguments
#
def run_selAnimCurve(*args):
    cmd.select(cmd.ls(type='animCurve'), replace=True)
    cmd.select(cmd.ls(type='animCurve', readOnly=True), deselect=True)


################################################################################
# @brief      run select all controls.
#
# @param      args  The arguments
#
def run_selectAllCtrls(*args):
    obj = cmd.ls(sl=1)
    cmd.select(cl=1)
    for i in range(0, len(obj)):
        namespace = (obj[i].find(':'))
        if namespace:
            try:
                try:
                    cmd.select('{}:ALL_CONTROLS'.format(obj[i][:namespace]), add=1)
                except:
                    cmd.select('{}:controls'.format(obj[i][:namespace]), add=1)
                    try:
                        cmd.select('{}:FaceMachineControls'.format(obj[i][:namespace]), add=1)
                    except:
                        cmd.select('{}:face_controls'.format(obj[i][:namespace]), add=1)
            except:
                rbwUI.RBWError(text="I can't select {}'s controls\nThere are no fixed HIG, please replace them".format(obj[i][:namespace]))


################################################################################
# @brief      run Dino main circle.
#
# @param      args  The arguments
#
def run_mainCircle(*args):
    import maya.mel as mel
    mel.eval("source dinomaincircle.mel; dinoMainCircle();")


################################################################################
# @brief      run aTools.
#
# @param      args  The arguments
#
def run_aTools(*args):
    res = rbwUI.RBWConfirm(icon='', title='A-Tools', text='What do you want to do?', defOk=['Install A-Tools', 'merge.png'], defCancel=['Toggle', '../animation/aTools.png']).result()
    if res:
        scriptFolder = os.path.join(os.getenv('USERPROFILE'), 'Documents', 'maya', 'scripts', 'aTools')

        if not os.path.exists(scriptFolder):
            import depts.animation.tools.selection.aTools_install as aTools
            importlib.reload(aTools)
            aTools.aToolsInstall()
        else:
            rbwUI.RBWError(text="You already have A-Tools installed locally")
    else:
        from aTools.animTools.animBar import animBarUI
        animBarUI.show('toggle')


################################################################################
# @brief      run dreamWall picker.
#
# @param      args  The arguments
#
def run_dwPicker(*args):
    import dwpicker
    dwpicker.show()


################################################################################
# @brief      run ik/fk tool.
#
# @param      args  The arguments
#
def run_ikfk(*args):
    import depts.animation.tools.selection.switchIkFk as ikFk
    importlib.reload(ikFk)

    ikFk.IkFkSwitch()


selAnimCurvesTool = {
    "name": "Select Anim curves",
    "launch": run_selAnimCurve,
    "icon": os.path.join(iconDir, "..", "animation", "selCurves.png")
}

selAllCtrlsTool = {
    "name": "Select ALL Ctrls",
    "launch": run_selectAllCtrls,
    "icon": os.path.join(iconDir, "..", "animation", "selCtrls.png")
}

mainCircleTool = {
    "name": "Main Circle",
    "launch": run_mainCircle,
    "icon": os.path.join(iconDir, "..", "animation", "mainCircle.png"),
    "statustip": "Dino Figuera Main Circle control"
}

aTools = {
    "name": "A-Tools",
    "launch": run_aTools,
    "icon": os.path.join(iconDir, '..', 'animation', "aTools.png"),
    "statustip": "Use A-Tools"
}

dwPickerTool = {
    "name": "Dream Wall Picker",
    "launch": run_dwPicker,
    "icon": os.path.join(iconDir, '..', 'animation', "dwPicker.png"),
    "statustip": "Open Dream Wall Picker"
}

IkFkSnapTool = {
    "name": "IK/FK Tool",
    "launch": run_ikfk,
    "icon": os.path.join(iconDir, "..", "animation", "ikfk.png"),
    "statustip": "Open the IkFk snap panel."
}

tools = [
    selAnimCurvesTool,
    selAllCtrlsTool,
    mainCircleTool,
    aTools,
    dwPickerTool,
    IkFkSnapTool
]

PackageTool = {
    "name": "selection",
    "tools": tools
}
