import os
import importlib
import functools
import maya.cmds as cmd

from PySide2 import QtWidgets

import api.scene
import api.shot
import api.savedShot
import api.database
import api.widgets.rbw_UI as rbwUI
import depts.animation.sc_animation as scDepts
import api.log

importlib.reload(api.scene)
# importlib.reload(api.shot)
# importlib.reload(api.savedShot)
importlib.reload(api.database)
# importlib.reload(rbwUI)
importlib.reload(scDepts)
importlib.reload(api.log)


def fragSequence(shotsInfo):
    scene = api.scene.scenename()

    animCurves = cmd.ls(type=['animCurveTA', 'animCurveTL', 'animCurveTT', 'animCurveTU'])
    minFrame = int(cmd.playbackOptions(q=True, min=0.0))
    astFrame = int(cmd.playbackOptions(q=True, ast=0.0))
    maxFrame = int(cmd.playbackOptions(q=True, max=0.0))
    aetFrame = int(cmd.playbackOptions(q=True, aet=0.0))

    for i, item in enumerate(shotsInfo):
        start = item['start']
        end = item['end']

        startDelta = 1 - start
        endDelta = 1 + (end - start)
        for node in animCurves:
            cmd.keyframe(node, edit=True, relative=True, timeChange=startDelta)

        cmd.playbackOptions(ast=1, min=1)
        cmd.playbackOptions(aet=endDelta, max=endDelta)

        shotObj = api.shot.Shot(params={'season': item['season'], 'episode': item['episode'], 'sequence': item['sequence'], 'shot': item['shot']})
        savedShot = api.savedShot.SavedShot(params={'shot': shotObj, 'dept': 'Lay'})
        savedShot.note = 'First save from Fragment tool'
        savedShot.save()

        # revert back
        for node in animCurves:
            cmd.keyframe(node, edit=True, relative=True, timeChange=-startDelta)
        cmd.playbackOptions(ast=astFrame, min=minFrame)
        cmd.playbackOptions(aet=aetFrame, max=maxFrame)

    cmd.file(scene, o=True, f=True)


class FragSequence(rbwUI.RBWWindow):
    def __init__(self, *args, **kwargs):
        super(FragSequence, self).__init__(*args, **kwargs)

        if cmd.window('FragSequence', exists=True):
            cmd.deleteUI('FragSequence')
        self.setObjectName('FragSequence')

        self.seasons = [x[0] for x in api.database.selectQuery("SELECT DISTINCT `season` FROM `V_shotList` WHERE `projectID` = {}".format(os.getenv('ID_MV')))]

        self.initUI()
        self.updateMargins()

    def initUI(self):
        self.setMain(True)
        self.setStyle()

        frame = rbwUI.RBWFrame('V')

        self.shotsTree = rbwUI.RBWTreeWidget(['Season', 'Episode', 'Sequence', 'Shot', 'Start', 'End', 'Delete'], size=[100, 100, 100, 100, 50, 50, 40], scrollV=False)
        self.root = self.shotsTree.invisibleRootItem()

        buttonBox = rbwUI.RBWFrame('H', bgColor='transparent', margins=[0, 0, 0, 0])
        self.addShotBtn = rbwUI.RBWButton('Add shot', size=[None, 25])
        self.addShotBtn.clicked.connect(self.addShot)
        runButton = rbwUI.RBWButton('Fragment sequence', size=[None, 25])
        runButton.clicked.connect(self.run)
        buttonBox.addWidget(self.addShotBtn)
        buttonBox.addWidget(runButton)

        frame.addWidget(self.shotsTree)
        frame.addWidget(buttonBox)

        self.mainLayout.addWidget(frame)

        self.setMinimumSize(580, 300)
        self.setTitle('Fragment Sequence')
        self.setIcon('../animation/fragSequence.png')
        self.setFocus()

    def addShot(self):
        if self.root.childCount():
            previousItem = self.root.child(self.root.childCount() - 1)
        else:
            previousItem = None

        ShotItem(self.shotsTree, self.seasons, previousItem)
        self.addShotBtn.setFocus()  # Move the focus to save the data in the fields which are being edited

    def run(self):
        self.addShotBtn.setFocus()  # Move the focus to save the data in the fields which are being edited

        if not rbwUI.RBWWarning(text='Have you deleted the bad plugins?', parent=self).result():
            return

        if not self.root.childCount():
            rbwUI.RBWError(text='Add a shot first.', parent=self)
            return

        fragSequence([self.root.child(i).info for i in range(0, self.root.childCount())])
        rbwUI.RBWDialog(text='Shots successfully saved!', parent=self)


class ShotItem(QtWidgets.QTreeWidgetItem):
    def __init__(self, tree, seasons, previousItem=None):
        super(ShotItem, self).__init__(tree)

        self.info = {'season': '', 'episode': '', 'sequence': '', 'shot': '', 'start': 0, 'end': 1}

        self.tree = tree
        self.initUI()
        self.seasonCombo.addItems(seasons)
        self.previousItem = previousItem

        if previousItem:
            newEnd = previousItem.info['end'] + 1
            self.startEdit.setText(str(newEnd))
            self.checkInt(self.startEdit, 'start')
            self.endEdit.setText(str(newEnd + 1))
            self.checkInt(self.endEdit, 'end')

            self.seasonCombo.setCurrentText(previousItem.info['season'])
            self.fillNext(self.seasonCombo, self.episodeCombo, 'episode', ['season'])
            self.episodeCombo.setCurrentText(previousItem.info['episode'])
            self.fillNext(self.episodeCombo, self.sequenceCombo, 'sequence', ['season', 'episode'])
            self.sequenceCombo.setCurrentText(previousItem.info['sequence'])
            self.fillNext(self.sequenceCombo, self.shotCombo, 'shot', ['season', 'episode', 'sequence'])

            try:
                self.shotCombo.setCurrentText('{0:03d}'.format(int(previousItem.info['shot']) + 1))
                self.fillNext(self.shotCombo)
            except:
                pass

    def initUI(self):
        self.seasonCombo = rbwUI.RBWComboBox(bgColor='transparent')
        self.episodeCombo = rbwUI.RBWComboBox(bgColor='transparent')
        self.sequenceCombo = rbwUI.RBWComboBox(bgColor='transparent')
        self.shotCombo = rbwUI.RBWComboBox(bgColor='transparent')
        self.startEdit = rbwUI.RBWLineEdit(bgColor='transparent')
        self.endEdit = rbwUI.RBWLineEdit(bgColor='transparent')
        deleteButton = rbwUI.RBWButton('', icon=['cancel.png'], color='transparent')
        deleteButton.clicked.connect(self.removeShot)

        self.startEdit.setText('0')
        self.endEdit.setText('1')

        self.seasonCombo.activated.connect(functools.partial(self.fillNext, self.seasonCombo, self.episodeCombo, 'episode', ['season']))
        self.episodeCombo.activated.connect(functools.partial(self.fillNext, self.episodeCombo, self.sequenceCombo, 'sequence', ['season', 'episode']))
        self.sequenceCombo.activated.connect(functools.partial(self.fillNext, self.sequenceCombo, self.shotCombo, 'shot', ['season', 'episode', 'sequence']))
        self.sequenceCombo.activated.connect(functools.partial(self.fillNext, self.shotCombo))
        self.startEdit.editingFinished.connect(functools.partial(self.checkInt, self.startEdit, 'start'))
        self.endEdit.editingFinished.connect(functools.partial(self.checkInt, self.endEdit, 'end'))

        self.tree.setItemWidget(self, 0, self.seasonCombo)
        self.tree.setItemWidget(self, 1, self.episodeCombo)
        self.tree.setItemWidget(self, 2, self.sequenceCombo)
        self.tree.setItemWidget(self, 3, self.shotCombo)
        self.tree.setItemWidget(self, 4, self.startEdit)
        self.tree.setItemWidget(self, 5, self.endEdit)
        self.tree.setItemWidget(self, 6, deleteButton)

    def fillNext(self, preCombo, postCombo=None, field='', keys=[], index=None):
        if postCombo:
            postCombo.clear()
            self.info[keys[-1]] = preCombo.currentText()

            query = "SELECT DISTINCT `{}` FROM `V_shotList` WHERE `projectID` = {}".format(field, os.getenv('ID_MV'))
            for key in keys:
                query += " AND `{}` = '{}'".format(key, self.info[key])

            items = [x[0] for x in api.database.selectQuery(query)]
            postCombo.addItems(items)
        else:
            self.info['shot'] = preCombo.currentText()

    def checkInt(self, line, key):
        value = line.text()

        if [x for x in value if not x.isdigit()]:
            line.setText('0')
            rbwUI.RBWError(text='Start and End values must be numbers only!')
        else:
            if self.previousItem and int(value) <= self.previousItem.info['end']:
                line.setText(str(self.previousItem.info['end'] + 1))
                rbwUI.RBWWarning(text="You chose a starting frame for this\nshot that is smaller than the end frame of\nthe previous shot, i fixed it", defCancel=None)
                value = line.text()

            self.info[key] = int(value)

    def removeShot(self):
        self.tree.takeTopLevelItem(self.tree.indexOfTopLevelItem(self))
