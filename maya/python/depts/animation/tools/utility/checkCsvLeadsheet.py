'''
CHECK THE LEADSHEET .csv FILE IN ORDER TO IDENTIFY ALL THE ASSET CODE

import importlib
import animation.tools.utility.checkCsvLeadsheet as check
importlib.reload(check)

check.main('//speed.nas/pun/01_pre_production/08_animatic/302A/CSV/PUN_302A_CSV_RBW.csv')
'''

import os
import csv

import api.database

def main(csvPath):
    # check for multiple variant assets
    exceptionQuery = "SELECT `name` FROM `V_assetList` WHERE `projectName` = 'pun' AND `variant` != 'df'"
    results = api.database.selectQuery(exceptionQuery)
    specials = []
    for i in results:
        specials.append(i[0])
    print('specials(assets with multiple variants): ', len(specials), specials)

    totalEntries = 0
    totalFoundEntries = 0
    totalMissingEntries = 0

    specialException = []
    foundAssets = []
    missingAssets = []
    alreadyCode = []

    replaceNames = {}
    counter = 0
    with open(csvPath, mode='r', encoding="utf-8", errors="ignore") as f:
        csv_reader = csv.reader(f)
        for row in csv_reader:
            counter = counter + 1
            # print(counter)
            assets = row[7].split(',')
            totalEntries = totalEntries + len(assets)
            for asset in assets:
                asset = asset.replace(' ', '')
                if asset in specials:
                    exceptionQuery = "SELECT `category`, `group`, `name`, `variant` FROM `V_assetList` WHERE `projectName` = 'pun' AND `name` = '" + asset + "'"
                    results = api.database.selectQuery(exceptionQuery)
                    if len(results) == 2:
                        for r in results:
                            if not r[0] == 'lightrig':
                                if not asset in foundAssets:
                                    foundAssets.append(asset)
                                totalFoundEntries = totalFoundEntries + 1
                    else:
                        if not asset in specialException:
                            # print('Warning, this is an asset that cannot be "translated" easily, there are assets with the same name with different variant, not only the "df" ones!')
                            specialException.append(asset)
                elif asset.count('prop') or asset.count('character'):
                    # controllo se e' un nome di asset gia' corretto con il code
                    parts = asset.split('_')
                    if len(parts) == 4:
                        if not asset in alreadyCode:
                            alreadyCode.append(asset)
                    else:
                        if not asset == 'Assets':
                            missingAssets.append(asset)

                else:
                    assetQuery = "SELECT `category`, `group`, `name`, `variant` FROM `V_assetList` WHERE `projectName` = 'pun' AND `name` = '" + asset + "'"
                    results = api.database.selectQuery(assetQuery)
                    if results:
                        if len(results) == 1:
                            if not asset in foundAssets:
                                foundAssets.append(asset)
                            totalFoundEntries = totalFoundEntries + 1
                        else:
                            print('Warning, more results for this asset: ', asset)
                    else:
                        if not asset in missingAssets:
                            if len(asset):
                                if not asset == 'Assets':
                                    totalMissingEntries = totalMissingEntries + 1
                                    missingAssets.append(asset)

    print('\ntotalEntries: ', totalEntries)
    print('totalFoundEntries: ', totalFoundEntries)
    print('totalMissingEntries: ', totalMissingEntries)
    if len(specialException):
        print('\nspecialException: ', specialException)
    if len(alreadyCode):
        print('\nalreadyCode: ', alreadyCode)

    print('\nmissingAssets: ', len(missingAssets), missingAssets)
    print('\n\nfoundAssets: ', len(foundAssets))

    for asset in foundAssets:
        assetQuery = "SELECT `category`, `group`, `name`, `variant` FROM `V_assetList` WHERE `projectName` = 'pun' AND `name` = '" + asset + "'"
        results = api.database.selectSingleQuery(assetQuery)
        codeName = str(results).replace("'", "").replace(", ", "_")
        search_text = asset
        replace_text = codeName.replace('(', '').replace(')', '')
        print(asset + ' ------------> ', replace_text)
        replaceNames[asset] = replace_text

    if not len(missingAssets):
        print("\n\nThere aren't missing assets, so the function will write a new fixed .csv file for Production")
        makeFixedCsv(csvPath, replaceNames)


def makeFixedCsv(csvPath, replaceNames):
    # print('csvPath: ', csvPath)
    # print('replaceNames: ', replaceNames)
    output_path = csvPath.replace('.csv', '_FIXED.csv')
    print('output_path: ', output_path)

    # Read the IN and write the OUT .csv file
    with open(csvPath, mode='r', newline='') as fileIn:
        reader = csv.reader(fileIn)
        rows = []
        for row in reader:
            assets = (row[7].split(", "))
            # print('assets: ', assets)
            newAssets = []
            for asset in assets:
                for key in replaceNames:
                    if asset == key:
                        newAssets.append(replaceNames[key])
            # print('newAssets: ', newAssets)
            row[7] = str(newAssets).replace('\'', '').replace('[', '').replace(']', '')
            rows.append(row)
    
        # Write the updated content to a new .csv file
        with open(output_path, mode='w', newline='') as fileOut:
            writer = csv.writer(fileOut)
            writer.writerows(rows)

    print("Search and replace completed.")
