import os
import importlib
import maya.mel as mel
import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI
import api.log
# importlib.reload(rbwUI)
importlib.reload(api.log)

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'arakno')


################################################################################
# @brief      run time shift.
#
# @param      args  The arguments
#
def run_timeShiftTool(*args):
    animCurve = cmd.ls(type='animCurve')
    if animCurve:
        res = rbwUI.RBWInput(title="Shift animation", text="Insert the shift value (only numbers):", mode={'Value': 'float'}).result()
        cmd.select(animCurve, r=True)
        cmd.keyframe(animCurve, edit=True, relative=True, timeChange=float(res['Value']))
    else:
        rbwUI.RBWWarning(text="There are no animation curves to be shifted")


################################################################################
# @brief      run tween machine.
#
# @param      args  The arguments
#
def run_tweenMachine(*args):
    mel.eval("source tween_machine.mel;\rtweenMachine;")


################################################################################
# @brief      run fragment sequence.
#
# @param      args  The arguments
#
def run_fragSequence(*args):
    import depts.animation.tools.utility.fragSequence as fs
    importlib.reload(fs)

    w = fs.FragSequence()
    w.show()


################################################################################
# @brief      run anchor transform.
#
# @param      args  The arguments
#
def run_anchorTransform(*args):
    import depts.animation.tools.utility.anchorTransform as aT
    importlib.reload(aT)

    win = aT.AnchorTransformUI()
    win.show()


################################################################################
# @brief      run dynamics keyers.
#
# @param      args  The arguments
#
def run_dynamics_keyers(*args):
    import depts.animation.tools.utility.dynamicsKeyer.dynamics_keyer_UI as dkUI
    importlib.reload(dkUI)

    dkUI.DK_UI().UI()


################################################################################
# @brief      run create mirror camera.
#
# @param      args  The arguments
#
def run_createMirrorCamera(*args):
    import depts.animation.tools.utility.sceneManagement as sceneMan
    importlib.reload(sceneMan)
    sceneMan.addMirrorCamera()


################################################################################
# @brief      run DK anim tool.
#
# @param      args  The arguments
#
def run_dkAnim(*args):
    mel.eval("dkAnim();")


################################################################################
# @brief      run reference to instance tool.
#
# @param      args  The arguments
#
def run_refToInstance(*args):
    import depts.animation.tools.utility.sceneManagement as sceneMan
    importlib.reload(sceneMan)
    sceneMan.referenceToggle()


################################################################################
# @brief      run kill reference tool.
#
# @param      args  The arguments
#
def run_killAsset(*args):
    import depts.animation.tools.utility.sceneManagement as sceneMan
    importlib.reload(sceneMan)
    sceneMan.killAsset()


################################################################################
# @brief      run give me tool.
#
# @param      args  The arguments
#
def run_giveMeTool(*args):
    import depts.animation.tools.utility.sceneManagement as sceneMan
    importlib.reload(sceneMan)
    sceneMan.checkAvailableStuff()


################################################################################
# @brief      run replace reference.
#
# @param      args  The arguments
#
def run_replaceAsset(*args):
    import depts.animation.tools.utility.sceneManagement as sceneMan
    importlib.reload(sceneMan)

    win = sceneMan.ReplaceAsset()
    win.show()


################################################################################
# @brief      run make colored meshes.
#
# @param      args  The arguments
#
def run_makeColoredMeshes(*args):
    import depts.finishing.tools.generic.makeColored as mkcol
    importlib.reload(mkcol)

    mkcol.main()


################################################################################
# @brief      run attack Train To Rail.
#
# @param      args  The arguments
#
def run_attackTrainToRail(*args):
    import depts.animation.tools.utility.attackTrainToRail as attackTrainToRail
    importlib.reload(attackTrainToRail)

    attackTrainToRail.attackTrainToRail()


timeShiftTool = {
    "name": "Time shift",
    "launch": run_timeShiftTool,
    "icon": os.path.join(iconDir, "..", "animation", "timeShift.png"),
    "statustip": "Time Shift Tool"
}

tweenMachineTool = {
    "name": "Tween Machine Tool",
    "launch": run_tweenMachine,
    "icon": os.path.join(iconDir, "..", "animation", "tweenMachine.png"),
    "statustip": "Tween Machine"
}

fragSequenceTool = {
    "name": "Fragment Sequence",
    "launch": run_fragSequence,
    "icon": os.path.join(iconDir, "..", "animation", "fragSequence.png"),
    "statustip": "Fragment Sequence"
}

anchorTransformTool = {
    "name": "Anchor Transform",
    "launch": run_anchorTransform,
    "icon": os.path.join(iconDir, "..", "animation", "anchor.png"),
    "statustip": "Anchor Transform Tool"
}

dynamicsKeyersTool = {
    "name": "Dynamics Keyers",
    "launch": run_dynamics_keyers,
    "icon": os.path.join(iconDir, "..", "animation", "DK.png"),
    "statustip": "Dynamics Keyers"
}

createMirrorCameraTool = {
    "name": "Create Mirror Camera",
    "launch": run_createMirrorCamera,
    "icon": os.path.join(iconDir, "..", "animation", "mirror.png"),
    "statustip": "Create mirror camera"
}

dkAnimTool = {
    "name": "DK Anim Tool",
    "launch": run_dkAnim,
    "statustip": "DK Anim tool",
    "icon": os.path.join(iconDir, "..", "animation", "DKanim.png")
}

importRefTool = {
    "name": "Import/Reference Tool",
    "launch": run_refToInstance,
    "icon": os.path.join(iconDir, "..", "animation", "refToInstance.png"),
    "statustip": "Convert Reference to Instance and vice-versa"
}

killAssetTool = {
    "name": "Kill Asset Tool",
    "launch": run_killAsset,
    "icon": os.path.join(iconDir, "..", "animation", "kill.png"),
    "statustip": "Removes a reference or a set"
}

giveMeTool = {
    "name": "Give Me Tool",
    "launch": run_giveMeTool,
    "icon": os.path.join(iconDir, "..", "animation", "giveMe.png"),
    "statustip": "Give Me Tool"
}

replaceAssetTool = {
    "name": "Replace Asset Tool",
    "launch": run_replaceAsset,
    "icon": os.path.join(iconDir, "..", "common", "replace.png"),
    "statustip": "Raplace asset"
}

makeColoredMeshesTool = {
    "name": "Make Colored Meshes",
    "launch": run_makeColoredMeshes,
    "icon": os.path.join(iconDir, "..", "animation", "paint.png"),
    "statustip": "Make Colored Meshes"
}

attackTrainToRailTool = {
    "name": "Attack Train To Rail",
    "launch": run_attackTrainToRail,
    "icon": os.path.join(iconDir, "..", "rigging", "trainRail.png"),
    "statustip": "Attack Train To Rail"
}


tools = [
    timeShiftTool,
    tweenMachineTool,
    fragSequenceTool,
    anchorTransformTool,
    dynamicsKeyersTool,
    createMirrorCameraTool,
    dkAnimTool,
    importRefTool,
    killAssetTool,
    giveMeTool,
    replaceAssetTool,
    makeColoredMeshesTool,
    attackTrainToRailTool,
]

PackageTool = {
    "name": "utility",
    "tools": tools
}
