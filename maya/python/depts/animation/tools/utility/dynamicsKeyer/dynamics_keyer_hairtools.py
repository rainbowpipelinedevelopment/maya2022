import importlib
from maya import cmds, mel
from depts.animation.tools.utility.dynamicsKeyer.dynamics_keyer_utils import DK_Utils
import depts.animation.tools.utility.dynamicsKeyer.UI as UI

import api.log
importlib.reload(api.log)


class DK_HairTools:

    def __init__(self):
        self.nucleus_name = 'Dynamics_Keyer_Nucleus'
        self.start_frame = cmds.playbackOptions(q=True, min=True)
        mayaVersion = cmds.about(v=True)
        self.break_con = 'C:/Program Files/Autodesk/Maya{}/scripts/startup/channelBoxCommand.mel'.format(mayaVersion)
        mel.eval('source "{0}";'.format(self.break_con))

    def construct_setup(self, sel, cb_state, ctrl_scale, progress_window):
        if not progress_window:
            progress_window = UI.ProgressWindow(max_value=100, title='Building Setup', status='Constructing Curve Setup...')
        self.setup_name = sel[0].replace('|', '_')
        if len(sel) >= 3:
            self.degree = 2
        elif len(sel) <= 2 and len(sel) >= 1:
            self.degree = 1
        self.original_controls = sel
        progress_window.update(10)
        jnts = self.generate_joint_chain(self.original_controls, self.setup_name)
        progress_window.update(10)
        crv = self.generate_curve(jnts)
        progress_window.update(10)
        clu = self.generate_cluster(crv[0], crv[1])
        progress_window.update(10)
        ctrls = self.build_control_curves(ctrl_scale, clu[0], sel, jnts)
        progress_window.update(10)
        self.bake_anim_to_dyn(sel, ctrls[0], clu[0], cb_state)
        progress_window.update(10)
        self.dyn_to_anim(jnts, sel)
        progress_window.update(10)
        cmds.setAttr(self.setup_name + '_Misc_GRP.visibility', 0)
        cmds.setAttr(self.setup_name + '_Solver_GRP.visibility', 0)
        progress_window.update(10)
        self.set_default_value(self.follicle, self.setup_name + '_hairSystemShape1', self.nucleus_name, len(self.original_controls))
        progress_window.update(10)
        # Tag the Group
        cmds.addAttr(self.setup_name + '_Dynamics_GRP', ln='isDynamicsKeyer_Curve', at='message')
        sel_SA = ''.join('"' + '", "'.join(sel) + '"')
        cmds.addAttr(self.setup_name + '_Dynamics_GRP', ln='animControls', dt='stringArray')
        eval("cmds.setAttr(self.setup_name + '_Dynamics_GRP.animControls', {0}, {1}, type='stringArray')".format(len(sel), sel_SA))
        cmds.addAttr(self.setup_name + '_Dynamics_GRP', ln='hairSystem', dt="string")
        cmds.setAttr(self.setup_name + '_Dynamics_GRP.hairSystem', crv[2], type="string", l=True)
        cmds.addAttr(self.setup_name + '_Dynamics_GRP', ln='follicle', dt="string")
        cmds.setAttr(self.setup_name + '_Dynamics_GRP.follicle', self.follicle, type="string", l=True)
        progress_window.update(10)

    def generate_joint_chain(self, anim_ctrls, jntName):
        '''Create jointChain based on the world space transform value'''
        jnt_list = []
        if len(anim_ctrls) >> 1:
            for i, anim_ctrl in enumerate(anim_ctrls):
                trans = cmds.xform(anim_ctrl, q=True, ws=True, rp=True)
                jnt_list.append(cmds.joint(n=jntName + '_JNT_' + str(i).zfill(2), a=True, p=[trans[0], trans[1], trans[2]]))
        elif len(anim_ctrls) == 1:
            ctrl_parent = cmds.listRelatives(anim_ctrls, f=True, p=True)
            for i, anim_ctrl in enumerate(ctrl_parent + anim_ctrls):
                trans = cmds.xform(anim_ctrl, q=True, ws=True, rp=True)
                jnt_list.append(cmds.joint(n=jntName + '_JNT_' + str(i).zfill(2), a=True, p=[trans[0], trans[1], trans[2]]))
        # Orient Joints
        cmds.joint(jnt_list[0], e=True, oj='xyz', sao='yup', ch=True)
        cmds.setAttr(jnt_list[-1] + '.jointOrientX', 0)
        cmds.setAttr(jnt_list[-1] + '.jointOrientY', 0)
        cmds.setAttr(jnt_list[-1] + '.jointOrientZ', 0)
        DK_Utils.organize(jnt_list[0], 'Dynamics_Main_GRP/Curve_Setups_GRP/' + self.setup_name + '_Dynamics_GRP/' + self.setup_name + '_Misc_GRP')

        return jnt_list

    def generate_curve(self, jnt_list):
        old_nuke = cmds.ls(type='nucleus')
        nucleus = DK_Utils.create_nucleus(self.nucleus_name)
        jnt_positions = []
        for each_jnt in jnt_list:
            jnt_positions.append(cmds.xform(each_jnt, q=True, t=True, ws=True))
        ik_curve = cmds.curve(d=self.degree, p=jnt_positions, ws=True, n=self.setup_name + '_ik_Crv')
        DK_Utils.organize(ik_curve, 'Dynamics_Main_GRP/Curve_Setups_GRP/' + self.setup_name + '_Dynamics_GRP/' + self.setup_name + '_Misc_GRP')
        spans = cmds.getAttr(ik_curve + '.spans')

        man_curve = cmds.duplicate(ik_curve, n=self.setup_name + '_man_Crv')
        dyn_curve = cmds.duplicate(ik_curve, n=self.setup_name + '_dyn_Crv')
        # blendShape manualCrv into DynamicsCurve
        man_to_dyn_bs = cmds.blendShape(man_curve, dyn_curve, n=self.setup_name + '_man_to_dyn_bs', o='world', foc=True)
        self.make_curve_dynamic(dyn_curve)
        new_nuke = cmds.ls(type='nucleus')
        trash = []
        for each in new_nuke:
            if each != nucleus and each not in old_nuke:
                trash.append(each)
        if trash:
            cmds.delete(trash)

        cmds.setAttr(nucleus + '.visibility', 0)

        list_con = cmds.listConnections(dyn_curve)
        list_rel = cmds.listRelatives(list_con)
        list_con = cmds.listConnections(list_rel)

        for x in list_con:
            if 'curve' in x:
                dyn_out_curve = cmds.rename(x, self.setup_name + '_dynOut_Crv')
            elif 'follicle' in x:
                follicle = cmds.listRelatives(x, s=True)
                follicle = cmds.rename(follicle[0], self.setup_name + '_follicle')
                self.follicle = follicle
            else:
                continue

        hair_element = []

        for x in cmds.ls('hairSystem*'):
            if 'Shape' not in x:
                hair_element.append(cmds.rename(x, x.replace('hairSystem', self.setup_name + '_hairSystem')))
        for x in hair_element:
            rel = cmds.listRelatives(x)
            for ea in rel:
                if 'hairSystemShape1' in ea:
                    hsys = ea
        # connect hairsystem to nucleus
        DK_Utils.connect_nucleus(nucleus, hsys)

        hair_element.append(follicle)

        for x in hair_element:
            DK_Utils.organize(x, 'Dynamics_Main_GRP/Curve_Setups_GRP/' + self.setup_name + '_Dynamics_GRP/' + self.setup_name + '_Solver_GRP')

        if len(self.original_controls) != 1:
            guide_circle = cmds.circle(n=self.setup_name + '_solverDisplayCircle1', radius=0.01, ch=True, nrz=False, nry=True)
            make_circle = guide_circle[1]
            # Attributes and Values for extrude node
            extrude_attr_value = {'.useProfileNormal': True, '.extrudeType': 2, '.useComponentPivot': 1, '.fixedPath': True}
            # Get shape nodes
            profile = cmds.listRelatives(guide_circle, s=True, ni=True)[0]
            path = cmds.listRelatives(ik_curve, s=True, ni=True)[0]
            # Build extrusion
            output_shape = cmds.createNode('mesh')
            output_mesh = cmds.listRelatives(output_shape, p=True)
            output_mesh = cmds.rename(output_mesh, self.setup_name + '_col_width_display_GEO')

            extrude_name = self.setup_name + '_extrude'
            if cmds.objExists(extrude_name):
                cmds.delete(extrude_name)
            extrude = cmds.createNode('extrude', n=extrude_name)

            nurbs_tes_name = self.setup_name + '_nurbs_tessellate'
            if cmds.objExists(nurbs_tes_name):
                cmds.delete(nurbs_tes_name)
            nurbs_tes = cmds.createNode('nurbsTessellate', n=self.setup_name + '_nurbs_tessellate')
            cmds.setAttr(nurbs_tes + '.chordHeightRatio', 0.9)
            # Set attributes
            for attr_value in zip(extrude_attr_value.keys(), extrude_attr_value.values()):
                cmds.setAttr(extrude + attr_value[0], attr_value[1])
            cmds.setAttr(nurbs_tes + '.format', 1)
            # Connect Attr
            cmds.connectAttr(profile + '.worldSpace[0]', extrude + '.profile', f=True)
            cmds.connectAttr(path + '.worldSpace[0]', extrude + '.path', f=True)
            cmds.connectAttr(extrude + '.outputSurface', nurbs_tes + '.inputSurface', f=True)
            cmds.connectAttr(nurbs_tes + '.outputPolygon', output_mesh + '.inMesh', f=True)
        elif len(self.original_controls) == 1:
            output_mesh = cmds.polySphere(n=self.setup_name + '_col_width_display_GEO', ch=True, radius=0.01)
            poly_sphere = output_mesh[1]
            output_mesh = output_mesh[0]
            cmds.parentConstraint(jnt_list[-1], output_mesh, mo=False, w=1)
        # Clean up
        mel.eval('CBdeleteConnection "{0}";'.format(output_mesh + '.overrideEnabled'))
        mel.eval('CBdeleteConnection "{0}";'.format(output_mesh + '.overrideDisplayType'))
        cmds.setAttr(output_mesh + '.overrideEnabled', 1)
        cmds.setAttr(output_mesh + '.overrideDisplayType', 2)
        shader = 'dynamics_keyer_blinn'
        if not cmds.objExists(shader):
            shader = cmds.shadingNode('blinn', n=shader, asShader=True)
        cmds.setAttr(shader + '.color', 0.449, 1.0, 0.0, type='double3')
        cmds.setAttr(shader + '.transparency', 0.85, 0.85, 0.85, type='double3')
        SG = shader + 'SG'
        if not cmds.objExists(SG):
            SG = cmds.sets(renderable=True, noSurfaceShader=True, empty=True, name=shader + 'SG')
            cmds.connectAttr(shader + '.outColor', SG + '.surfaceShader', f=True)
        cmds.sets(output_mesh, e=True, forceElement=SG)
        if len(self.original_controls) != 1:
            cmds.connectAttr(hsys + '.collideWidthOffset', make_circle + '.radius', f=True)
        elif len(self.original_controls) == 1:
            cmds.connectAttr(hsys + '.collideWidthOffset', poly_sphere + '.radius', f=True)
        cmds.connectAttr(hsys + '.solverDisplay', output_mesh + '.visibility', f=True)
        if len(self.original_controls) != 1:
            DK_Utils.organize(guide_circle[0], 'Dynamics_Main_GRP/Curve_Setups_GRP/' + self.setup_name + '_Dynamics_GRP/' + self.setup_name + '_Misc_GRP')
        DK_Utils.organize(output_mesh, 'Dynamics_Main_GRP/Curve_Setups_GRP/' + self.setup_name + '_Dynamics_GRP/' + self.setup_name + '_Display_GRP')

        # make ikHandle for jointChain
        ik_spline_handle = cmds.ikHandle(sol='ikSplineSolver', sj=jnt_list[0], ee=jnt_list[-1], ccv=False, c=ik_curve)
        DK_Utils.organize(ik_spline_handle[0], 'Dynamics_Main_GRP/Curve_Setups_GRP/' + self.setup_name + '_Dynamics_GRP/' + self.setup_name + '_Misc_GRP')
        # BlendShape manual and Dynamics Curve into ik_curve

        dyn_to_ik_bs = cmds.blendShape(dyn_out_curve, ik_curve, n=self.setup_name + '_dyn_to_ik_bs', o='world', foc=True)
        cmds.blendShape(man_to_dyn_bs, e=True, w=(0, 1.0))
        cmds.blendShape(dyn_to_ik_bs, e=True, w=(0, 1.0))
        if len(self.original_controls) == 1:
            curve_info = cmds.arclen(ik_curve, ch=True)
            cmds.connectAttr(curve_info + '.arcLength', jnt_list[-1] + '.translateX', force=True)

        return man_curve[0], spans, hsys

    def generate_cluster(self, curve='', spans=0):
        cluster_list = []
        for x in range(spans + self.degree):
            name_index = str(x + 1).zfill(2)
            cluster = cmds.cluster(curve + '.cv[' + str(x) + ']', n=self.setup_name + '_manCrv_cluster_' + name_index, relative=False)
            cluster_list.append(cluster)
            DK_Utils.organize(cluster[1], 'Dynamics_Main_GRP/Curve_Setups_GRP/' + self.setup_name + '_Dynamics_GRP/' + self.setup_name + '_Misc_GRP')
        cluster_handles = [x[1] for x in cluster_list]
        cluster_def = [x[0] for x in cluster_list]
        return cluster_handles, cluster_def

    def build_control_curves(self, ctrl_scale, clusterHandles=[], selected_ctrls=[], joints=[]):
        ctrls = []
        ctrl_grps = []

        if len(selected_ctrls) >> 1:
            parent = ''
            for x, cluster in enumerate(clusterHandles):
                radius = DK_Utils.get_radius(selected_ctrls[x])
                shape_01 = cmds.circle(radius=radius * ctrl_scale, nry=False, nrz=False, nrx=True, ch=False)
                shape_02 = cmds.circle(radius=radius * ctrl_scale, nry=False, nrz=True, nrx=False, ch=False)
                shape_03 = cmds.circle(radius=radius * ctrl_scale, nry=True, nrz=False, nrx=False, ch=False)
                shape_list = []
                for ea in [shape_02, shape_03]:
                    shape_list.append(cmds.listRelatives(ea, s=True)[0])
                cmds.parent(shape_list, shape_01, r=True, s=True)
                cmds.delete(shape_02, shape_03)
                ctrl_curve = cmds.rename(shape_01, cluster.replace('cluster', 'Ctrl'))
                ctrls.append(ctrl_curve)
                cmds.delete(cmds.parentConstraint(selected_ctrls[x], ctrl_curve, mo=False))
                ctrl_curve_grp = cmds.group(em=True, n=ctrl_curve + '_GRP')
                ctrl_grps.append(ctrl_curve_grp)
                cmds.delete(cmds.parentConstraint(ctrl_curve, ctrl_curve_grp, w=1, mo=False))
                cmds.parent(ctrl_curve, ctrl_curve_grp)
                cmds.parentConstraint(ctrl_curve, cluster, mo=True)
                if x == 0:
                    root_parent = cmds.listRelatives(selected_ctrls[0], f=True, p=True)
                    if root_parent:
                        cmds.parentConstraint(root_parent, ctrl_curve_grp, w=1, mo=True)
                else:
                    cmds.parentConstraint(parent, ctrl_curve_grp, w=1, mo=True)
                parent = ctrl_curve
                mel.eval('CBdeleteConnection "{0}";'.format(ctrl_curve + '.overrideEnabled'))
                mel.eval('CBdeleteConnection "{0}";'.format(ctrl_curve + '.overrideColor'))
                cmds.setAttr(ctrl_curve + '.overrideEnabled', 1)
                cmds.setAttr(ctrl_curve + '.overrideColor', 14)
                DK_Utils.organize(ctrl_curve_grp, 'Dynamics_Main_GRP/Curve_Setups_GRP/' + self.setup_name + '_Dynamics_GRP/' + self.setup_name + '_Control_GRP')

        elif len(selected_ctrls) == 1:
            for x, cluster in enumerate(clusterHandles):
                radius = DK_Utils.get_radius(selected_ctrls[x])
                shape_01 = cmds.circle(radius=radius * ctrl_scale, nry=False, nrz=False, nrx=True, ch=False)
                shape_02 = cmds.circle(radius=radius * ctrl_scale, nry=False, nrz=True, nrx=False, ch=False)
                shape_03 = cmds.circle(radius=radius * ctrl_scale, nry=True, nrz=False, nrx=False, ch=False)
                shape_list = []
                for ea in [shape_02, shape_03]:
                    shape_list.append(cmds.listRelatives(ea, s=True)[0])
                cmds.parent(shape_list, shape_01, r=True, s=True)
                cmds.delete(shape_02, shape_03)
                ctrl_curve = cmds.rename(shape_01, cluster.replace('cluster', 'Ctrl'))
                ctrls.append(ctrl_curve)
                cmds.delete(cmds.parentConstraint(selected_ctrls[0], ctrl_curve, mo=False))
                ctrl_curve_grp = cmds.group(em=True, n=ctrl_curve + '_GRP')
                ctrl_grps.append(ctrl_curve_grp)
                cmds.delete(cmds.parentConstraint(ctrl_curve, ctrl_curve_grp, w=1, mo=False))
                cmds.parent(ctrl_curve, ctrl_curve_grp)
                cmds.parentConstraint(ctrl_curve, cluster, mo=True)
                root_parent = cmds.listRelatives(selected_ctrls[0], f=True, p=True)
                if root_parent:
                    cmds.parentConstraint(root_parent, ctrl_curve_grp, w=1, mo=True)
                cmds.setAttr(ctrl_curve + '.overrideEnabled', 1)
                cmds.setAttr(ctrl_curve + '.overrideColor', 14)
                cmds.parentConstraint(cluster, clusterHandles[x + 1])
                DK_Utils.organize(ctrl_curve_grp, 'Dynamics_Main_GRP/Curve_Setups_GRP/' + self.setup_name + '_Dynamics_GRP/' + self.setup_name + '_Control_GRP')
                break

        else:
            api.log.logger().debug('Not enough object selected!')
        return ctrls, ctrl_grps

    def bake_anim_to_dyn(self, anim_ctrl=[], dyn_ctrl=[], clu_handle_list=[], bake=False):
        # Bake animation to ctrls
        if bake:
            if len(anim_ctrl) == len(dyn_ctrl):
                bake_anim_par_con = [cmds.parentConstraint(anim_ctrl[x], dyn_ctrl[x], mo=False, w=1.0) for x in range(len(anim_ctrl))]
                # Get timerange
                start_frame = cmds.playbackOptions(q=True, min=True)
                end_frame = cmds.playbackOptions(q=True, max=True)
                time_range = int(start_frame), int(end_frame)
                # Bake simulation
                cmds.bakeResults(dyn_ctrl, t=time_range, at=['tx', 'ty', 'tz', 'rx', 'ry', 'rz'])
                # Delete constraint
                for par_con in bake_anim_par_con:
                    cmds.delete(par_con)
            else:
                api.log.logger().debug('the number of anim_ctrl do not match the number of dyn_ctrl')
        for x in range(len(dyn_ctrl)):
            cmds.parentConstraint(dyn_ctrl[x], clu_handle_list[x], w=1, mo=True)
        cmds.currentTime(self.start_frame, e=True)

    def dyn_to_anim(self, joints, anim_ctrls):
        # Drive anim_ctrl with dynSetup

        if len(anim_ctrls) >> 1:
            for x in range(len(joints)):
                list_attr = ['tx', 'ty', 'tz', 'rx', 'ry', 'rz']
                skip_rotate = []
                skip_translate = []
                for ea in list_attr:
                    state = cmds.getAttr(anim_ctrls[x] + '.' + ea, se=True)
                    if not state:
                        if 'r' in ea:
                            skip_rotate.append(ea.replace('r', ''))
                        elif 't' in ea:
                            skip_translate.append(ea.replace('t', ''))
                cmds.parentConstraint(joints[x], anim_ctrls[x], w=True, mo=True, st=skip_translate, sr=skip_rotate)

        elif len(anim_ctrls) == 1:
            list_attr = ['tx', 'ty', 'tz']
            skip_translate = []
            for ea in list_attr:
                state = cmds.getAttr(anim_ctrls[0] + '.' + ea, se=True)
                if not state:
                    skip_translate.append(ea.replace('t', ''))
            cmds.pointConstraint(joints[1], anim_ctrls[0], w=True, mo=True, sk=skip_translate)

        else:
            api.log.logger().debug('Not enough object selected!')

    def make_curve_dynamic(self, curve):
        cmds.select(curve)
        mel.eval('MakeCurvesDynamic 2 { "1", "0", "1", "1", "0"};')

    def set_default_value(self, follicle, hairsystem, nucleus, num_of_ctrls):
        if num_of_ctrls == 1:
            cmds.setAttr(hairsystem + '.stretchResistance', 1)
            cmds.setAttr(hairsystem + '.bendResistance', 0.05)
            cmds.setAttr(hairsystem + '.damp', 0.05)
            cmds.setAttr(hairsystem + '.drag', 0.05)
        elif num_of_ctrls == 2:
            cmds.setAttr(hairsystem + '.stretchResistance', 10)
            cmds.setAttr(hairsystem + '.bendResistance', 0.01)
            cmds.setAttr(hairsystem + '.damp', 0.02)
            cmds.setAttr(hairsystem + '.drag', 0.02)
        elif num_of_ctrls >= 3:
            cmds.setAttr(hairsystem + '.stretchResistance', 10)
            cmds.setAttr(hairsystem + '.bendResistance', 0.05)
            cmds.setAttr(hairsystem + '.damp', 0.05)
            cmds.setAttr(hairsystem + '.drag', 0.05)
        # HAIRSYSTEM
        cmds.setAttr(hairsystem + '.friction', 0.1)
        cmds.setAttr(hairsystem + '.startCurveAttract', 0.001)
        cmds.setAttr(hairsystem + '.attractionDamp', 0.01)
        cmds.setAttr(hairsystem + '.collideWidthOffset', 0.01)
        # FOLLICLE
        cmds.setAttr(follicle + '.pointLock', 1)
        # NUCLEUS
        cmds.setAttr(nucleus + '.startFrame', self.start_frame)
