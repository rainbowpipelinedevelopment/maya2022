import os
import importlib
import re
import pickle
import webbrowser

from maya import cmds, mel, OpenMaya
from functools import partial

import api.log
import depts.animation.tools.utility.dynamicsKeyer.dynamics_keyer_hairtools as dynamics_keyer_hairtools
import depts.animation.tools.utility.dynamicsKeyer.dynamics_keyer_utils as dynamics_keyer_utils
import depts.animation.tools.utility.dynamicsKeyer.dynamics_keyer_clothtools as dynamics_keyer_clothtools
import depts.animation.tools.utility.dynamicsKeyer.UI as UI

from depts.animation.tools.utility.dynamicsKeyer.dynamics_keyer_hairtools import DK_HairTools
from depts.animation.tools.utility.dynamicsKeyer.dynamics_keyer_utils import DK_Utils
from depts.animation.tools.utility.dynamicsKeyer.dynamics_keyer_clothtools import DK_ClothTools

importlib.reload(api.log)
importlib.reload(dynamics_keyer_hairtools)
importlib.reload(dynamics_keyer_utils)
importlib.reload(dynamics_keyer_clothtools)


class DK_UI:

    def __init__(self):
        self.start_frame = cmds.playbackOptions(q=True, min=True)
        self.window_name = 'dynamics_keyer_UI'
        self.window_title = 'Dynamics Keyer'
        self.degree = 3
        self.nucleus_name = 'Dynamics_Keyer_Nucleus'
        self.default_preset_path = 'U:/Documents/maya/2013-x64/presets/'
        self.DK_hair = DK_HairTools()
        self.DK_cloth = DK_ClothTools()
        self.tool_doc = 'https://docs.google.com/a/bardel.ca/document/d/1WMn9nECER5QpKslAZVzoVvyMr4_Y6sv8RnPnvm4qIek/edit#heading=h.isszk160nkoe'

    def open_url(self, url, *args):
        webbrowser.open_new_tab(url)

    def UI(self):
        '''Create Main Window'''
        if cmds.window(self.window_name, exists=True):
            cmds.deleteUI(self.window_name)
        self.window = cmds.window(self.window_name, t=self.window_title, s=False, mxb=False)
        self.___form_layout_01 = cmds.formLayout()
        self.___column_layout_01 = cmds.columnLayout()
        # MENU
        self.menu_bar_layout_01 = cmds.menuBarLayout(p=self.___column_layout_01)
        self.menu_01 = cmds.menu(l='File', p=self.menu_bar_layout_01)
        cmds.menuItem(l='Save Preset', c=partial(self.write), p=self.menu_01)
        cmds.menuItem(l='Load Preset', c=partial(self.read), p=self.menu_01)
        self.menu_02 = cmds.menu(l='Edit', p=self.menu_bar_layout_01)
        cmds.menuItem(l='Refresh Lists', c=partial(self.refresh), p=self.menu_02)
        self.menu_03 = cmds.menu(l='Help', helpMenu=True, p=self.menu_bar_layout_01)
        cmds.menuItem(l='Tool Documentation', p=self.menu_03, c=partial(self.open_url, self.tool_doc))
        cmds.separator(w=865, h=2, st='none')

        # MAIN FORM LAYOUT >>>>>>>>>>>
        self.___form_layout_02 = cmds.formLayout(nd=1000, p=self.___column_layout_01)
        # hortizontal separators
        self.separator_01 = cmds.separator(w=890, h=2, st='in', p=self.___form_layout_02)
        self.separator_02 = cmds.separator(w=890, h=2, st='in', p=self.___form_layout_02)
        self.separator_03 = cmds.separator(w=447, h=2, st='in', p=self.___form_layout_02)
        self.separator_04 = cmds.separator(w=447, h=2, st='in', p=self.___form_layout_02)
        self.separator_08 = cmds.separator(w=890, h=2, st='in', p=self.___form_layout_02)
        self.separator_09 = cmds.separator(w=447, h=2, st='in', p=self.___form_layout_02)

        # vertical separators
        self.separator_05 = cmds.separator(w=2, h=477, st='in', hr=False, p=self.___form_layout_02)
        self.separator_06 = cmds.separator(w=2, h=477, st='in', hr=False, p=self.___form_layout_02)
        self.separator_07 = cmds.separator(w=2, h=455, st='in', hr=False, p=self.___form_layout_02)
        # main title
        self.text_02 = cmds.text('Dynamics Control Panel', fn='boldLabelFont', p=self.___form_layout_02)

        # self.tab_layout_02
        self.tab_layout_02 = cmds.tabLayout(p=self.___form_layout_02)
        self.ts_list_03 = cmds.textScrollList(w=210, h=322, fn='tinyBoldLabelFont', ams=True,
                                              sc=partial(self.update_filtered_setups), p=self.tab_layout_02)
        cmds.tabLayout(self.tab_layout_02, e=True, tabLabel=(self.ts_list_03, 'NAMESPACE'))

        # self.__row_layout_04
        self.__row_layout_04 = cmds.rowLayout(nc=3, ct3=['left', 'right', 'left'], co3=[0, 0, 7], p=self.___form_layout_02)
        self.checkbox_01 = cmds.checkBox(l='Bake Animation', p=self.__row_layout_04)
        self.ff_grp_03 = cmds.floatFieldGrp(l='   Control Size', v1=1, pre=2, cal=[1, 'left'], cw2=[67, 40], en1=1, p=self.__row_layout_04)
        cmds.button(l='CREATE CURVE SETUP', w=210, h=18, bgc=[0.4, 0.6, 0.5], c=partial(self.build_curve_setup), p=self.__row_layout_04)

        # self.__row_layout_02
        self.__row_layout_02 = cmds.rowLayout(nc=3, ct3=['left', 'right', 'left'], co3=[0, 0, 7], p=self.___form_layout_02)
        self.checkbox_09 = cmds.checkBox(l='Bake Animation', p=self.__row_layout_02)
        cmds.separator(w=115, h=22, st='none', p=self.__row_layout_02)
        cmds.button(l='CREATE CLOTH SETUP', w=210, h=18, bgc=[0.35, 0.55, 0.7], c=partial(self.build_cloth_setup), p=self.__row_layout_02)

        # self.__row_layout_03
        iconPath = os.path.join(os.getenv('LOCALPY'), 'depts', 'animation', 'icons').replace('\\', '/')
        self.__row_layout_03 = cmds.rowLayout(nc=4, ct4=['left', 'left', 'left', 'left'], co4=[35, 30, 30, 40], p=self.___form_layout_02)
        cmds.iconTextButton(st='iconOnly', i=os.path.join(iconPath, 'rewind_button.png'), c=partial(self.rewind))
        cmds.iconTextButton(st='iconOnly', i=os.path.join(iconPath, 'play_button.png'), c=partial(self.interactive_play))
        cmds.iconTextButton(st='iconOnly', i=os.path.join(iconPath, 'stop_button.png'), c=partial(self.stop))
        cmds.button(l='DELETE SETUP', w=210, h=18, bgc=[0.75, 0.35, 0.35], c=partial(self.delete_setup), p=self.__row_layout_03)

        # self.__row_layout_05
        self.__row_layout_05 = cmds.rowLayout(nc=3, ct3=['left', 'right', 'left'], co3=[0, 0, 7], p=self.___form_layout_02)
        self.checkbox_02 = cmds.checkBox(l='Simplify Curve', cc=partial(self.bake_tolerance_enabled), p=self.__row_layout_05)
        self.ff_grp_02 = cmds.floatFieldGrp(l='     Tolerance', v1=0.05, pre=2, cal=[1, 'left'], cw2=[72, 40], en1=0, p=self.__row_layout_05)
        cmds.button(l='BAKE', w=210, h=18, bgc=[0.4, 0.4, 0.7], c=partial(self.bake_dyn_to_anim), p=self.__row_layout_05)

        # self.tab_layout_01
        self.tab_layout_01 = cmds.tabLayout(cc=partial(self.switch_tabs_setup), p=self.___form_layout_02)
        self.ts_list_01 = cmds.textScrollList(w=210, h=322, fn='tinyBoldLabelFont', ams=True, sc=partial(self.connect_controls), p=self.tab_layout_01)
        self.ts_list_04 = cmds.textScrollList(w=210, h=322, fn='tinyBoldLabelFont', ams=True, sc=partial(self.connect_controls), p=self.tab_layout_01)
        cmds.tabLayout(self.tab_layout_01, e=True, tabLabel=[(self.ts_list_01, 'CURVE SETUP'), (self.ts_list_04, 'CLOTH SETUP')])

        # self.___column_layout_02
        self.___column_layout_02 = cmds.columnLayout(rs=4, p=self.___form_layout_02)
        self._frame_layout_01 = cmds.frameLayout(l='Dynamics Presets', borderStyle='etchedIn', w=210,
                                                 fn='tinyBoldLabelFont', cl=True, p=self.___column_layout_02)
        self.___column_layout_09 = cmds.columnLayout(p=self._frame_layout_01)
        cmds.separator(h=4, st='none', p=self.___column_layout_09)
        self.__row_layout_01 = cmds.rowLayout(nc=3, ct3=['both', 'both', 'both'], co3=[3, 3, 3], p=self.___column_layout_09)
        self.button_01 = cmds.button(l='Save', w=60, h=18, bgc=[0.2, 0.2, 0.2], p=self.__row_layout_01)
        self.pu_menu_01 = cmds.popupMenu(p=self.button_01, b=1)
        self.button_02 = cmds.button(l='Load', w=60, h=18, bgc=[0.2, 0.2, 0.2], p=self.__row_layout_01)
        self.pu_menu_02 = cmds.popupMenu(p=self.button_02, b=1)
        self.button_03 = cmds.button(l='Delete', w=60, h=18, bgc=[0.2, 0.2, 0.2], p=self.__row_layout_01)
        self.pu_menu_03 = cmds.popupMenu(p=self.button_03, b=1)
        cmds.separator(h=4, st='none', p=self.___column_layout_09)

        # CURVE DYNAMICS ATTRIBUTEs >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        self._frame_layout_02 = cmds.frameLayout(l='Curve Dynamics Attributes', borderStyle='etchedIn', w=210,
                                                 fn='tinyBoldLabelFont', cl=True, p=self.___column_layout_02)
        self.___column_layout_10 = cmds.columnLayout(p=self._frame_layout_02)
        cmds.separator(h=2, st='none', p=self.___column_layout_10)
        self.op_menu_grp_03 = cmds.optionMenuGrp(l='Attach Ends', ct2=['left', 'left'], co2=[6, 20], cw2=[70, 137],
                                                 cc=partial(self.set_follicle_attach), p=self.___column_layout_10)
        cmds.menuItem(l='No Attach')
        cmds.menuItem(l='Base')
        cmds.menuItem(l='Tip')
        cmds.menuItem(l='Both Ends')
        self.fs_grp_01 = cmds.floatSliderGrp(l='  Length Preserve', min=0, max=100, fmn=0, fmx=1000, v=20.0, f=True, pre=2,
                                             cal=[1, 'left'], cw3=[90, 40, 65], p=self.___column_layout_10)
        self.pu_menu_04 = cmds.popupMenu(p=self.fs_grp_01)
        self.fs_grp_04 = cmds.floatSliderGrp(l='  Stiffness', min=0, max=100, fmn=0, fmx=1000, v=0.5, f=True, pre=2,
                                             cal=[1, 'left'], cw3=[90, 40, 65], p=self.___column_layout_10)
        self.pu_menu_05 = cmds.popupMenu(p=self.fs_grp_04)
        self.fs_grp_02 = cmds.floatSliderGrp(l='  Damp', min=0, max=10, fmn=0, fmx=1000, v=0.1, f=True, pre=2,
                                             cal=[1, 'left'], cw3=[90, 40, 65], p=self.___column_layout_10)
        self.pu_menu_06 = cmds.popupMenu(p=self.fs_grp_02)
        self.fs_grp_03 = cmds.floatSliderGrp(l='  Drag', min=0, max=10, fmn=0, fmx=1000, v=0.1, f=True, pre=2,
                                             cal=[1, 'left'], cw3=[90, 40, 65], p=self.___column_layout_10)
        self.pu_menu_07 = cmds.popupMenu(p=self.fs_grp_03)
        self.fs_grp_08 = cmds.floatSliderGrp(l='  Anim Attract', min=0, max=1, fmn=0, fmx=1, v=0.001, s=0.001, f=True, pre=3,
                                             cal=[1, 'left'], cw3=[90, 40, 65], p=self.___column_layout_10)
        self.pu_menu_08 = cmds.popupMenu(p=self.fs_grp_08)
        cmds.separator(h=2, st='none', p=self.___column_layout_10)

        self._frame_layout_05 = cmds.frameLayout(l='Curve Anim Attraction Scale', borderStyle='etchedIn', w=210,
                                                 fn='tinyBoldLabelFont', cl=True, p=self.___column_layout_02)
        self.___column_layout_06 = cmds.columnLayout(p=self._frame_layout_05)
        cmds.separator(w=1, h=4, st='none', p=self.___column_layout_06)
        self.text_01 = cmds.text(l='    Root >>>>>>>>>>>>>>>>>>>> Tip ', font='smallObliqueLabelFont', p=self.___column_layout_06)
        cmds.separator(w=210, h=3, st='none', p=self.___column_layout_06)
        self.ramp_01 = cmds.gradientControl(w=204, h=70, p=self.___column_layout_06)
        self.ramp_02 = cmds.gradientControlNoAttr(w=204, h=70, p=self.___column_layout_06)

        self._frame_layout_03 = cmds.frameLayout(l='Curve Collision Attributes', borderStyle='etchedIn', w=210,
                                                 fn='tinyBoldLabelFont', cl=True, p=self.___column_layout_02)
        self.___column_layout_03 = cmds.columnLayout(rs=1, p=self._frame_layout_03)
        cmds.separator(h=2, st='none', p=self.___column_layout_03)
        self.__cl_03_sub_cl_01 = cmds.columnLayout(p=self.___column_layout_03, co=['left', 5])
        self.checkbox_11 = cmds.checkBox(l=' Ignore Solver Gravity', en=False, p=self.__cl_03_sub_cl_01)
        self.checkbox_03 = cmds.checkBox(l=' Collision Enable', cc=partial(self.collision_enabled), p=self.__cl_03_sub_cl_01)
        self.checkbox_04 = cmds.checkBox(l=' Collide Thickness Display', en=False, p=self.__cl_03_sub_cl_01)
        self.fs_grp_09 = cmds.floatSliderGrp(l='  Thickness', min=0.01, max=10,  fmn=0, fmx=1000, v=0, pre=2, f=True,
                                             cal=[1, 'left'], cw3=[90, 40, 65], en=False, p=self.___column_layout_03)
        self.pu_menu_19 = cmds.popupMenu(p=self.fs_grp_09)
        cmds.separator(h=2, st='none', p=self.___column_layout_03)
        # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

        # CLOTH ATTRIBUTES>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        self._frame_layout_08 = cmds.frameLayout(l='Cloth Attributes', borderStyle='etchedIn', w=210,
                                                 fn='tinyBoldLabelFont', cl=True, p=self.___column_layout_02)
        self.___column_layout_12 = cmds.columnLayout(p=self._frame_layout_08)
        cmds.separator(h=2, st='none', p=self.___column_layout_12)
        self.fs_grp_05 = cmds.floatSliderGrp(l='  Stretch Resist', min=0, max=200, fmn=0, fmx=1000, v=120, f=True, pre=2,
                                             cal=[1, 'left'], cw3=[90, 40, 65], p=self.___column_layout_12)
        self.pu_menu_09 = cmds.popupMenu(p=self.fs_grp_05)
        self.fs_grp_07 = cmds.floatSliderGrp(l='  Compress Resist', min=0, max=200, fmn=0, fmx=1000, v=80, f=True, pre=2,
                                             cal=[1, 'left'], cw3=[90, 40, 65], p=self.___column_layout_12)
        self.pu_menu_10 = cmds.popupMenu(p=self.fs_grp_07)
        self.fs_grp_12 = cmds.floatSliderGrp(l='  Bend Resist', min=0, max=200, fmn=0, fmx=1000, v=0.05, f=True, pre=2,
                                             cal=[1, 'left'], cw3=[90, 40, 65], p=self.___column_layout_12)
        self.pu_menu_11 = cmds.popupMenu(p=self.fs_grp_12)
        self.fs_grp_15 = cmds.floatSliderGrp(l='  Damp', min=0, max=2, fmn=0, fmx=1, v=0.05, s=0.001, f=True, pre=3,
                                             cal=[1, 'left'], cw3=[90, 40, 65], p=self.___column_layout_12)
        self.pu_menu_12 = cmds.popupMenu(p=self.fs_grp_15)
        self.fs_grp_16 = cmds.floatSliderGrp(l='  Drag', min=0, max=10, fmn=0, fmx=1, v=0.05, s=0.001, f=True, pre=3,
                                             cal=[1, 'left'], cw3=[90, 40, 65], p=self.___column_layout_12)
        self.pu_menu_13 = cmds.popupMenu(p=self.fs_grp_16)
        self.fs_grp_18 = cmds.floatSliderGrp(l='  Input Attract', min=0, max=1, fmn=0, fmx=1, v=0, s=0.001, f=True, pre=3,
                                             cal=[1, 'left'], cw3=[90, 40, 65], p=self.___column_layout_12)
        self.pu_menu_14 = cmds.popupMenu(p=self.fs_grp_18)
        self.fs_grp_19 = cmds.floatSliderGrp(l='  Attract Damp', min=0, max=1, fmn=0, fmx=1, v=0.001, s=0.001, f=True, pre=3,
                                             cal=[1, 'left'], cw3=[90, 40, 65], p=self.___column_layout_12)
        self.pu_menu_15 = cmds.popupMenu(p=self.fs_grp_19)
        cmds.separator(h=2, st='none', p=self.___column_layout_12)

        self._frame_layout_09 = cmds.frameLayout(l='Cloth Collision Attributes', borderStyle='etchedIn', w=210,
                                                 fn='tinyBoldLabelFont', cl=True, p=self.___column_layout_02)
        self.___column_layout_14 = cmds.columnLayout(rs=1, p=self._frame_layout_09)
        cmds.separator(h=2, st='none', p=self.___column_layout_14)
        self.__cl_14_sub_cl_01 = cmds.columnLayout(p=self.___column_layout_14, co=['left', 5])
        self.checkbox_12 = cmds.checkBox(l=' Ignore Solver Gravity', en=False, p=self.__cl_14_sub_cl_01)
        self.checkbox_07 = cmds.checkBox(l=' Collision Enable', cc=partial(self.collision_enabled), p=self.__cl_14_sub_cl_01)
        self.checkbox_10 = cmds.checkBox(l=' Collide Thickness Display', en=False, p=self.__cl_14_sub_cl_01)
        self.op_menu_grp_01 = cmds.optionMenuGrp(l='Collision Flag', cc=partial(self.set_collision_flag),
                                                 ct2=['left', 'both'], co2=[0, 13], cw2=[70, 137], p=self.__cl_14_sub_cl_01)
        cmds.menuItem(l='Vertex')
        cmds.menuItem(l='Edge')
        cmds.menuItem(l='Face')
        self.fs_grp_24 = cmds.floatSliderGrp(l='  Thickness', min=0.01, max=10,  fmn=0, fmx=1000, v=0, pre=2, f=True,
                                             cal=[1, 'left'], cw3=[90, 40, 65], en=False, p=self.___column_layout_14)
        self.pu_menu_20 = cmds.popupMenu(p=self.fs_grp_24)
        cmds.separator(h=2, st='none', p=self.___column_layout_14)
        # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

        # self.tab_layout_03
        self.tab_layout_03 = cmds.tabLayout(cc=partial(self.switch_tabs_constraint), p=self.___form_layout_02)
        self.ts_list_02 = cmds.textScrollList(w=205, h=157, fn='tinyBoldLabelFont', ams=True, p=self.tab_layout_03)
        cmds.textScrollList(self.ts_list_02, e=True, sc=partial(self.connect_control_collider))
        self.ts_list_05 = cmds.textScrollList(w=205, h=157, fn='tinyBoldLabelFont', ams=True, p=self.tab_layout_03)
        cmds.textScrollList(self.ts_list_05, e=True, sc=partial(self.connect_control_constraint, self.ts_list_05))
        cmds.tabLayout(self.tab_layout_03, e=True, tabLabel=[(self.ts_list_02, 'COLLIDER'), (self.ts_list_05, 'CONSTRAINT')])

        # self.___column_layout_07
        self.___column_layout_07 = cmds.columnLayout(rs=4, p=self.___form_layout_02)
        self.__row_layout_06 = cmds.rowLayout(nc=2, ct2=['both', 'both'], co2=[0, 2], p=self.___column_layout_07)
        self.button_04 = cmds.button(l='ADD COLLIDER', w=101, h=18, bgc=[0.45, 0.55, 0.35],
                                     c=partial(self.add_collider_sel), p=self.__row_layout_06)
        self.button_06 = cmds.button(l='DEL COLLIDER', w=101, h=18, bgc=[0.65, 0.4, 0.35],
                                     c=partial(self.remove_collider), p=self.__row_layout_06)
        self.__row_layout_07 = cmds.rowLayout(nc=2, ct2=['both', 'both'], co2=[0, 2], p=self.___column_layout_07)
        self.button_05 = cmds.button(l='ADD CONSTRAINT', w=101, h=18, bgc=[0.5, 0.45, 0.7],
                                     c=partial(self.add_constraint_sel), p=self.__row_layout_07)
        self.button_07 = cmds.button(l='DEL CONSTRAINT', w=101, h=18, bgc=[0.7, 0.45, 0.45],
                                     c=partial(self.remove_constraint), p=self.__row_layout_07)

        self._frame_layout_07 = cmds.frameLayout(l='Collider Collision Attributes', borderStyle='etchedIn', w=210,
                                                 fn='tinyBoldLabelFont', cl=True, p=self.___column_layout_07)
        self.___column_layout_08 = cmds.columnLayout(rs=1, p=self._frame_layout_07)
        cmds.separator(h=2, st='none', p=self.___column_layout_08)
        self.__cl_08_sub_cl_01 = cmds.columnLayout(p=self.___column_layout_08, co=['left', 5])
        self.checkbox_05 = cmds.checkBox(l=' Collision Enable', cc=partial(self.collision_enabled), p=self.__cl_08_sub_cl_01)
        self.checkbox_06 = cmds.checkBox(l=' Collide Thickness Display', en=False, p=self.__cl_08_sub_cl_01)
        self.fs_grp_11 = cmds.floatSliderGrp(l='  Thickness', min=0.01, max=10,  fmn=0, fmx=1000, v=0, pre=2, f=True,
                                             cal=[1, 'left'], cw3=[90, 40, 65], en=False, p=self.___column_layout_08)
        self.pu_menu_16 = cmds.popupMenu(p=self.fs_grp_11)
        cmds.separator(h=2, st='none', p=self.___column_layout_08)

        self._frame_layout_10 = cmds.frameLayout(l='Constraint Attributes', borderStyle='etchedIn', w=210,
                                                 fn='tinyBoldLabelFont', cl=True, p=self.___column_layout_07)
        self.___column_layout_09 = cmds.columnLayout(rs=1, p=self._frame_layout_10)
        cmds.separator(h=2, st='none', p=self.___column_layout_09)
        self.__cl_09_sub_cl_01 = cmds.columnLayout(p=self.___column_layout_09, co=['left', 5])
        self.checkbox_13 = cmds.checkBox(l=' Constraint Enable', cc=partial(self.collision_enabled), p=self.__cl_09_sub_cl_01)

        self.fs_grp_20 = cmds.floatSliderGrp(l='  Strength', min=0, max=200,  fmn=0, fmx=1000, v=20, pre=2, f=True,
                                             cal=[1, 'left'], cw3=[90, 40, 65], en=False, p=self.___column_layout_09)
        self.pu_menu_17 = cmds.popupMenu(p=self.fs_grp_20)
        self.fs_grp_13 = cmds.floatSliderGrp(l='  Glue Strength', min=0, max=1, v=1, pre=2, f=True, cal=[1, 'left'],
                                             cw3=[90, 40, 65], en=False, p=self.___column_layout_09)
        self.pu_menu_18 = cmds.popupMenu(p=self.fs_grp_13)
        cmds.separator(h=2, st='none', p=self.___column_layout_09)

        self._frame_layout_04 = cmds.frameLayout(l='Global Attributes', borderStyle='etchedIn', w=210,
                                                 fn='tinyBoldLabelFont', cl=True, p=self.___column_layout_07)
        self.___column_layout_04 = cmds.columnLayout(rs=1, p=self._frame_layout_04)
        cmds.separator(h=2, st='none', p=self.___column_layout_04)
        self.fs_grp_06 = cmds.floatSliderGrp(l=' Gravity', min=-9.8, max=9.8, v=0.0, s=1, f=True, pre=2,
                                             cal=[1, 'left'], cw3=[90, 40, 65], p=self.___column_layout_04)
        self.fs_grp_10 = cmds.floatSliderGrp(l=' Space Scale', min=0, max=1, v=0.16, fmn=0, fmx=100, s=1, f=True, pre=2,
                                             cal=[1, 'left'], cw3=[90, 40, 65], p=self.___column_layout_04)
        self.is_grp_01 = cmds.intSliderGrp(l=' Iteration', min=0, max=64,  fmn=0, fmx=256, v=3, s=1, f=True,
                                           cal=[1, 'left'], cw3=[90, 40, 65], p=self.___column_layout_04)
        self.if_grp_01 = cmds.intSliderGrp(l=' Start Frame', fmn=-10000, fmx=10000, s=False, f=True, v=1,
                                           cal=[1, 'left'], cw3=[90, 40, 0], p=self.___column_layout_04)
        cmds.separator(h=2, st='none', p=self.___column_layout_04)

        # Layout form
        cmds.formLayout(self.___form_layout_02, e=True,
                        ap=[(self.text_02, 'top', 0, 11),
                            (self.text_02, 'left', 0, 432)],
                        af=[(self.separator_01, 'left', 5),
                            (self.separator_01, 'top', 0),
                            (self.separator_04, 'left', 5),
                            (self.separator_05, 'left', 5),
                            (self.separator_06, 'right', 5),
                            (self.separator_07, 'left', 443),
                            (self.separator_08, 'left', 5),
                            (self.separator_08, 'bottom', 5),
                            (self.separator_09, 'left', 5)],
                        ac=[(self.tab_layout_02, 'left', 5, self.separator_05),
                            (self.tab_layout_02, 'top', 5, self.separator_02),
                            (self.tab_layout_01, 'left', 5, self.tab_layout_02),
                            (self.tab_layout_01, 'top', 5, self.separator_02),
                            (self.___column_layout_02, 'top', 5, self.separator_02),
                            (self.tab_layout_03, 'top', 5, self.separator_02),
                            (self.separator_02, 'left', 0, self.separator_05),
                            (self.separator_02, 'top', 20, self.separator_01),
                            (self.separator_06, 'left', 0, self.separator_01),
                            (self.separator_07, 'left', 5, self.tab_layout_01),
                            (self.separator_09, 'top', 5, self.tab_layout_02),
                            (self.separator_09, 'left', 0, self.separator_05),
                            (self.___column_layout_02, 'left', 5, self.separator_07),
                            (self.tab_layout_03, 'left', 5, self.___column_layout_02),
                            (self.separator_03, 'left', 0, self.separator_05),
                            (self.separator_04, 'left', 0, self.separator_05),
                            (self.__row_layout_04, 'left', 5, self.separator_05),
                            (self.__row_layout_02, 'left', 5, self.separator_05),
                            (self.__row_layout_05, 'left', 5, self.separator_05),
                            (self.__row_layout_04, 'top', 3, self.separator_09),
                            (self.__row_layout_02, 'top', 3, self.separator_09),
                            (self.separator_03, 'top', 29, self.separator_09),
                            (self.__row_layout_03, 'left', 5, self.separator_05),
                            (self.__row_layout_03, 'top', 5, self.separator_03),
                            (self.separator_04, 'top', 5, self.__row_layout_03),
                            (self.__row_layout_05, 'top', 3, self.separator_04),
                            (self.separator_07, 'top', -2, self.separator_02),
                            (self.separator_08, 'top', 0, self.separator_06),
                            (self.___column_layout_07, 'left', 5, self.___column_layout_02),
                            (self.___column_layout_07, 'top', 2, self.tab_layout_03)])

        # UPDATE LISTS
        cmds.playbackOptions(ps=0, mps=1)
        self.refresh()
        self.switch_tabs_setup()
        self.switch_tabs_constraint()
        cmds.showWindow(self.window)

    def interactive_play(self):
        cmds.InteractivePlayback()

    def rewind(self):
        start_frame = cmds.playbackOptions(q=True, min=True)
        cmds.currentTime(start_frame)

    def stop(self):
        cmds.play(st=False)

    '''Node Presets >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'''
    def save_preset(self, action, preset, *args):
        current_tab_index = cmds.tabLayout(self.tab_layout_01, q=True, sti=True)
        if current_tab_index == 1:
            ts_list = DK_Utils.get_scroll_list(self.ts_list_01)
            solver_attr = '.hairSystem'
        elif current_tab_index == 2:
            ts_list = DK_Utils.get_scroll_list(self.ts_list_04)
            solver_attr = '.nCloth'
        if ts_list:
            solvers = [cmds.getAttr(each + '_Dynamics_GRP' + solver_attr) for each in ts_list][-1]
            if action == 'create_new':
                new_preset = self.get_new_preset_name()
                if new_preset:
                    if cmds.nodePreset(ex=[solvers, new_preset]):
                        result = cmds.confirmDialog(t='Overwrite Preset', m='Do you want to overwrite preset "' + new_preset +
                                                    '" with the current settings?', b=['Overwrite', 'Cancel'])
                        if result == 'Overwrite':
                            cmds.nodePreset(sv=[solvers, new_preset])
                            self.update_preset_list()
                    else:
                        cmds.nodePreset(sv=[solvers, new_preset])
                        self.update_preset_list()
                else:
                    return
            else:
                if cmds.nodePreset(ex=[solvers, preset]):
                    result = cmds.confirmDialog(t='Overwrite Preset', m='Do you want to overwrite preset "' + preset +
                                                '" with the current settings?', b=['Overwrite', 'Cancel'])
                    if result == 'Overwrite':
                        cmds.nodePreset(sv=[solvers, preset])
                        self.update_preset_list()
                    else:
                        return
        else:
            cmds.warning('No setup selected to save preset from.')

    def load_preset(self, preset, *args):
        current_tab_index = cmds.tabLayout(self.tab_layout_01, q=True, sti=True)
        if current_tab_index == 1:
            ts_list = DK_Utils.get_scroll_list(self.ts_list_01)
            solver_attr = '.hairSystem'
        elif current_tab_index == 2:
            ts_list = DK_Utils.get_scroll_list(self.ts_list_04)
            solver_attr = '.nCloth'
        if ts_list:
            solvers = [cmds.getAttr(each + '_Dynamics_GRP' + solver_attr) for each in ts_list]
            cmds.nodePreset(ld=[solvers[-1], preset])
            if len(solvers) >> 1:
                list_attrs = cmds.listAttr(solvers[-1], k=True, multi=False)
                remove_list = []
                for attr in list_attrs:
                    if '.' not in attr:
                        attr_type = cmds.getAttr(solvers[-1] + '.' + attr, type=True)
                        if attr_type == 'TdataCompound' or attr_type == 'message':
                            remove_list.append(attr)
                    else:
                        remove_list.append(attr)
                for each in remove_list:
                    list_attrs.remove(each)
                list_attrs = [attr for attr in list_attrs if not isinstance(cmds.getAttr(solvers[-1] + '.' + attr), list)]
                for solver in solvers[:-1]:
                    for attr in list_attrs:
                        checker = [x for x in ['stiffnessScale', 'clumpWidthScale', 'attractionScale', 'displacementScale',
                                   'hairColorScale', 'clumpCurl', 'hairWidthScale', 'clumpFlatness'] if x in attr]
                        if checker:
                            continue
                        else:
                            if cmds.getAttr(solver + '.' + attr, se=True):
                                value = cmds.getAttr(solvers[-1] + '.' + attr)
                                cmds.setAttr(solver + '.' + attr, value)
                            else:
                                continue
            cmds.currentTime(self.start_frame, e=True)
        else:
            cmds.warning('No setup selected to load preset on.')

    def delete_preset(self, preset, *args):
        current_tab_index = cmds.tabLayout(self.tab_layout_01, q=True, sti=True)
        if current_tab_index == 1:
            ts_list = DK_Utils.get_scroll_list(self.ts_list_01)
            solver_attr = '.hairSystem'
        elif current_tab_index == 2:
            ts_list = DK_Utils.get_scroll_list(self.ts_list_04)
            solver_attr = '.nCloth'
        if ts_list:
            solvers = [cmds.getAttr(each + '_Dynamics_GRP' + solver_attr) for each in ts_list][-1]
            result = cmds.confirmDialog(t='Delete Preset', m='Do you want to delete preset "' + preset + '" ?', b=['Delete', 'Cancel'])
            if result == 'Delete':
                cmds.nodePreset(delete=[solvers, preset])
                self.update_preset_list()
        else:
            cmds.warning('No setup selected to delete preset from.')

    def get_new_preset_name(self):
        get_action = cmds.promptDialog(t='Save Attribute Preset', ma='left', m='Preset Name: ',
                                       tx='new_Dynamics_Keyer_preset_01', b=['OK', 'Cancel'])
        if get_action == 'OK':
            preset_name = re.sub(r'[^\w]', '_', cmds.promptDialog(q=True, tx=True))
            return preset_name
        else:
            return

    def update_preset_list(self):
        current_tab_index = cmds.tabLayout(self.tab_layout_01, q=True, sti=True)
        if current_tab_index == 1:
            ts_list = DK_Utils.get_scroll_list(self.ts_list_01)
            solver_attr = '.hairSystem'
        elif current_tab_index == 2:
            ts_list = DK_Utils.get_scroll_list(self.ts_list_04)
            solver_attr = '.nCloth'
        if ts_list:
            cmds.frameLayout(self._frame_layout_01, e=True, en=True)
            solvers = [cmds.getAttr(each + '_Dynamics_GRP' + solver_attr) for each in ts_list][-1]
            presets = cmds.nodePreset(ls=solvers)
            if presets:
                for each in [self.pu_menu_01, self.pu_menu_02, self.pu_menu_03]:
                    cmds.popupMenu(each, e=True, dai=True)
                    if each == self.pu_menu_01:
                        cmds.menuItem(l='Create New Preset', c=partial(self.save_preset, 'create_new', ''), p=self.pu_menu_01)
                        cmds.menuItem(d=True, p=self.pu_menu_01)
                    for preset in presets:
                        if each == self.pu_menu_01:
                            cmds.menuItem(l=preset, c=partial(self.save_preset, '', preset), p=each)
                        elif each == self.pu_menu_02:
                            cmds.menuItem(l=preset, c=partial(self.load_preset, preset), p=each)
                        elif each == self.pu_menu_03:
                            cmds.menuItem(l=preset, c=partial(self.delete_preset, preset), p=each)
            else:
                for each in [self.pu_menu_01, self.pu_menu_02, self.pu_menu_03]:
                    cmds.popupMenu(each, e=True, dai=True)
                cmds.menuItem(l='Create New Preset', c=partial(self.save_preset, 'create_new', ''), p=self.pu_menu_01)

        else:
            for each in [self.pu_menu_01, self.pu_menu_02, self.pu_menu_03]:
                cmds.popupMenu(each, e=True, dai=True)
            cmds.frameLayout(self._frame_layout_01, e=True, en=False)

    '''Global Presets >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'''
    def save_load_setup(self, mode, *args):
        file_path = cmds.fileDialog(m=mode, dm="U:/Documents/maya/2013-x64/presets/*.dyn;*.py")
        return file_path

    def load_setup_preset(self, setup, attrs, gradientAttr, mode, *args):
        if mode == 'curve':
            setup = DK_Utils.search_namespaces(setup)[0]
            hsys = cmds.getAttr(setup + '_Dynamics_GRP.hairSystem')
            for attr_value in zip(attrs.keys(), attrs.values()):
                cmds.setAttr(hsys + attr_value[0], attr_value[1])
            for i, key_value in enumerate(gradientAttr):
                for attr_value in zip(key_value.keys(), key_value.values()):
                    cmds.setAttr(hsys + '.attractionScale[' + str(i) + ']' + attr_value[0], attr_value[1])
        elif mode == 'cloth':
            setup = DK_Utils.search_namespaces(setup)[0]
            ncloth = cmds.getAttr(setup + '_Dynamics_GRP.nCloth')
            for attr_value in zip(attrs.keys(), attrs.values()):
                cmds.setAttr(ncloth + attr_value[0], attr_value[1])

    def write(self, *args):
        path = self.save_load_setup(1)
        if path:
            data = self.get_all_setups()
            with open(path, 'wb') as fh:
                pickle.dump(data, fh)
        else:
            return

    def read(self, *args):
        path = self.save_load_setup(0)
        if path:
            cmds.select(cl=True)
            with open(path, 'rb') as fh:
                data = pickle.load(fh)
            if 'isCloth' not in data.keys() or not data['isCloth']:
                if 'isCloth' in data.keys():
                    del data['isCloth']
                cmds.tabLayout(self.tab_layout_01, e=True, sti=1)
                cb_state = DK_Utils.checkbox_state(self.checkbox_01)
                ctrl_scale = cmds.floatFieldGrp(self.ff_grp_03, q=True, v1=True) * 1.4
                setup_count = len(data.keys())
                max_value = setup_count * 100 + 10
                self.UI = UI.ProgressWindow(max_value, title='Building Setup', status='Initializing...')
                for each_setup in data.keys():
                    api.log.logger().debug(data[each_setup])
                    self.UI.set_status('Building Curve Setup "' + each_setup + '"...')
                    controls = []
                    for each_control in data[each_setup][0]:
                        check_list = DK_Utils.search_namespaces(each_control)
                        if check_list:
                            controls.extend(check_list)
                        else:
                            controls.extend(each_control)
                    self.DK_hair.construct_setup(controls, cb_state, ctrl_scale, self.UI)
                    self.load_setup_preset(each_setup, data[each_setup][1], data[each_setup][2], 'curve')
                par = DK_Utils.search_namespaces('Dynamics_Main_GRP')
                nuke = cmds.listRelatives(par, c=True, typ='nucleus')[0]
                for attr_value in zip(data[each_setup][3].keys(), data[each_setup][3].values()):
                    cmds.setAttr(nuke + attr_value[0], attr_value[1])
                self.UI.update(10)
            elif 'isCloth' in data.keys() and data['isCloth']:
                cmds.tabLayout(self.tab_layout_01, e=True, sti=2)
                cb_state = DK_Utils.checkbox_state(self.checkbox_09)
                del data['isCloth']
                setup_count = len(data.keys())
                constraint_count = 0
                for each in data.keys():
                    if 'constraintParts' in data[each][2].keys():
                        constraint_count += len(data[each][2]['constraintParts'])
                    else:
                        constraint_count += 0
                max_value = (setup_count + constraint_count) * 100 + 20
                self.UI = UI.ProgressWindow(max_value=max_value, title='Building Setup', status='Initializing...')
                for each_setup in data.keys():
                    self.UI.set_status('Building Cloth Setup "' + each_setup + '"...')
                    controls = []
                    for each_control in data[each_setup][0]:
                        check_list = DK_Utils.search_namespaces(each_control)
                        if check_list:
                            controls.extend(check_list)
                        else:
                            controls.extend(each_control)
                    input_meshes = []
                    for each_mesh in data[each_setup][2]['inputMeshes']:
                        input_meshes.extend(DK_Utils.search_namespaces(each_mesh))
                    if input_meshes:
                        input_meshes = list(set(input_meshes))
                    create = self.DK_cloth.construct_setup(controls + input_meshes, self.UI, cb_state=cb_state)
                    if 'constrainedParts' in data[each_setup][2].keys():
                        shape = ''.join(x for x in cmds.listRelatives(create, s=True, ni=True) if 'outputCloth' in x)
                        for each in data[each_setup][2]['constrainedParts']:
                            verts = [shape + '.vtx[' + str(x) + ']' for x in each if isinstance(x, int)]
                            new_targets = []
                            targets = [x for x in each if not isinstance(x, int)]
                            for target in targets:
                                new_targets.extend(DK_Utils.search_namespaces(target))
                            targets = list(set(new_targets))
                            self.add_constraint(verts + targets)
                            self.UI.update(10)
                self.load_setup_preset(each_setup, data[each_setup][1], '', 'cloth')
                self.UI.update(10)
                par = DK_Utils.search_namespaces('Dynamics_Main_GRP')
                nuke = cmds.listRelatives(par, c=True, typ='nucleus')[0]
                for attr_value in zip(data[each_setup][3].keys(), data[each_setup][3].values()):
                    cmds.setAttr(nuke + attr_value[0], attr_value[1])

            self.refresh()
        else:
            return

    def get_all_setups(self, *args):
        current_tab_index = cmds.tabLayout(self.tab_layout_01, q=True, sti=True)
        nucleus_attrs = ['.gravity', '.subSteps', '.spaceScale']
        if current_tab_index == 1:
            setups = DK_Utils.get_scroll_list(self.ts_list_01)
            attrs = ['.stretchResistance', '.bendResistance', '.damp', '.drag', '.startCurveAttract', '.collide',
                     '.collideWidthOffset', '.maxSelfCollisionIterations', '.solverDisplay']
            output_dict = {}
            output_dict['isCloth'] = False
            for setup in setups:
                controls = cmds.getAttr(setup + '_Dynamics_GRP.animControls')
                hsys = cmds.getAttr(setup + '_Dynamics_GRP.hairSystem')
                value = [cmds.getAttr(hsys + x) for x in attrs]
                indice = cmds.getAttr(hsys + '.attractionScale', mi=True)
                hair_value = {}
                for each in zip(attrs, value):
                    hair_value[each[0]] = each[1]
                attraction_scale = []
                for index in indice:
                    gradient_value = {}
                    value = cmds.getAttr(hsys + '.attractionScale[' + str(index) + ']')[0]
                    gradient_value['.attractionScale_Position'] = value[0]
                    gradient_value['.attractionScale_FloatValue'] = value[1]
                    gradient_value['.attractionScale_Interp'] = value[2]
                    attraction_scale.append(gradient_value)
                par = DK_Utils.search_namespaces('Dynamics_Main_GRP')
                nuke = cmds.listRelatives(par, c=True, typ='nucleus')[0]
                nucleus = {}
                for attr in nucleus_attrs:
                    nucleus[attr] = cmds.getAttr(nuke + attr)
                if ':' in setup:
                    setup = setup.split(':')[-1]
                output_dict[setup] = (controls, hair_value, attraction_scale, nucleus)
        elif current_tab_index == 2:
            setups = DK_Utils.get_scroll_list(self.ts_list_04)
            attrs = ['.stretchResistance', '.compressionResistance', '.bendResistance', '.damp', '.drag',
                     '.inputMeshAttract', '.inputAttractDamp', '.collide', '.thickness', '.ignoreSolverGravity', '.solverDisplay']
            output_dict = {}
            output_dict['isCloth'] = True
            constraints = [x.replace('.isConstraint', '') for x in DK_Utils.search_namespaces('.isConstraint')]
            api.log.logger().debug(constraints)
            for setup in setups:
                api.log.logger().debug(setup)
                if constraints:
                    setup_constraints = [constraint for constraint in constraints if setup in cmds.getAttr(constraint + '.setup')]
                    api.log.logger().debug(setup_constraints)
                controls = cmds.getAttr(setup + '_Dynamics_GRP.animControls')
                ncloth = cmds.getAttr(setup + '_Dynamics_GRP.nCloth')
                value = [cmds.getAttr(ncloth + x) for x in attrs]
                ncloth_value = {}
                for each in zip(attrs, value):
                    ncloth_value[each[0]] = each[1]
                miscellaneous = {}
                miscellaneous['inputMeshes'] = cmds.getAttr(setup + '_Dynamics_GRP.inputMeshes')
                if setup_constraints:
                    miscellaneous['constrainedParts'] = self.get_constraint_components(setup_constraints)
                    api.log.logger().debug(miscellaneous['constrainedParts'])
                par = DK_Utils.search_namespaces('Dynamics_Main_GRP')
                nuke = cmds.listRelatives(par, c=True, typ='nucleus')[0]
                nucleus = {}
                for attr in nucleus_attrs:
                    nucleus[attr] = cmds.getAttr(nuke + attr)
                if ':' in setup:
                    setup = setup.split(':')[-1]
                output_dict[setup] = (controls, ncloth_value, miscellaneous, nucleus)
        api.log.logger().debug(output_dict)
        return output_dict

    def get_constraint_components(self, constraints, *args):
        comp_parts = []
        for constraint in constraints:
            shapes = cmds.listRelatives(constraint, s=True, ni=True)
            shape = ''.join(shape for shape in shapes if cmds.objectType(shape, i='dynamicConstraint'))
            nComps = [con for con in cmds.listConnections(shape, s=True, d=False) if cmds.objectType(con, i='nComponent')]
            indice = []
            target = []
            for nComp in nComps:
                roots = cmds.listConnections(nComp, s=True, d=False)
                for root in roots:
                    root_shape = cmds.listRelatives(root, s=True, ni=True)[0]
                    if cmds.objectType(root_shape, i='nCloth'):
                        indice = cmds.getAttr(nComp + '.componentIndices')
                        indice = [int(x) for x in list(indice[0])]
                    elif cmds.objectType(root_shape, i='nRigid'):
                        target = cmds.listConnections(root_shape + '.inputMesh', s=True, d=False)
                        '''
                        if ':' in target:
                            target = [target.split(':')[-1]]
                        '''
            if indice and target:
                comp_parts.append(indice + target)
            else:
                continue
        if comp_parts:
            return comp_parts
        else:
            return

    '''Edit Menu >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'''
    def refresh(self, *args):
        current_tab_index = cmds.tabLayout(self.tab_layout_01, q=True, sti=True)
        self.connect_control_nucleus()
        self.update_namespaces_scroll_list()
        self.update_curve_setup_scroll_list()
        self.update_cloth_setup_scroll_list()
        self.update_collider_scroll_list()
        if current_tab_index == 2:
            self.update_constraint_scroll_list()
        self.update_keyed_controls(1)
        self.collision_enabled()

    def update_filtered_setups(self, *args):
        self.update_curve_setup_scroll_list()
        self.update_cloth_setup_scroll_list()

    '''Change Command Triggers >>>>>>>>>>>>>>>>>>>>>>>'''
    def switch_tabs_setup(self, *args):
        current_tab_index = cmds.tabLayout(self.tab_layout_01, q=True, sti=True)
        if current_tab_index == 1:
            cmds.frameLayout(self._frame_layout_08, e=True, vis=False)
            cmds.frameLayout(self._frame_layout_09, e=True, vis=False)
            cmds.rowLayout(self.__row_layout_02, e=True, vis=False)
            # ------------------------------------------------------
            cmds.frameLayout(self._frame_layout_02, e=True, vis=True)
            cmds.frameLayout(self._frame_layout_05, e=True, vis=True)
            cmds.frameLayout(self._frame_layout_03, e=True, vis=True)
            cmds.rowLayout(self.__row_layout_04, e=True, vis=True)
            cmds.deleteUI(self.ts_list_05)
            cmds.tabLayout(self.tab_layout_03, e=True, tabLabel=(self.ts_list_02, 'COLLIDER'))
            self.update_namespaces_scroll_list()
            self.update_curve_setup_scroll_list()
        elif current_tab_index == 2:
            cmds.frameLayout(self._frame_layout_02, e=True, vis=False)
            cmds.frameLayout(self._frame_layout_05, e=True, vis=False)
            cmds.frameLayout(self._frame_layout_03, e=True, vis=False)
            cmds.rowLayout(self.__row_layout_04, e=True, vis=False)
            # ------------------------------------------------------
            cmds.frameLayout(self._frame_layout_08, e=True, vis=True)
            cmds.frameLayout(self._frame_layout_09, e=True, vis=True)
            cmds.rowLayout(self.__row_layout_02, e=True, vis=True)
            self.ts_list_05 = cmds.textScrollList(w=205, h=135, fn='tinyBoldLabelFont', ams=True, p=self.tab_layout_03)
            cmds.textScrollList(self.ts_list_05, e=True, sc=partial(self.connect_control_constraint, self.ts_list_05))
            cmds.tabLayout(self.tab_layout_03, e=True, tabLabel=[(self.ts_list_02, 'COLLIDER'), (self.ts_list_05, 'CONSTRAINT')])
            self.update_namespaces_scroll_list()
            self.update_cloth_setup_scroll_list()
            self.update_constraint_scroll_list()
        self.collision_enabled()

    def switch_tabs_constraint(self, *args):
        current_tab_index = cmds.tabLayout(self.tab_layout_03, q=True, sti=True)
        if current_tab_index == 1:
            cmds.rowLayout(self.__row_layout_07, e=True, vis=False, en=False)
            cmds.rowLayout(self.__row_layout_06, e=True, vis=True, en=True)
            cmds.frameLayout(self._frame_layout_10, e=True, vis=False)
            cmds.frameLayout(self._frame_layout_07, e=True, vis=True)
        elif current_tab_index == 2:
            cmds.rowLayout(self.__row_layout_06, e=True, vis=False, en=False)
            cmds.rowLayout(self.__row_layout_07, e=True, vis=True, en=True)
            cmds.frameLayout(self._frame_layout_07, e=True, vis=False)
            cmds.frameLayout(self._frame_layout_10, e=True, vis=True)

    '''Checkbox Query >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'''
    def collision_enabled(self, *args):
        current_tab_index = cmds.tabLayout(self.tab_layout_01, q=True, sti=True)
        if current_tab_index == 1:
            current = cmds.checkBox(self.checkbox_03, q=True, v=True)
            if current:
                cmds.floatSliderGrp(self.fs_grp_09, e=True, en=True)
                cmds.checkBox(self.checkbox_04, e=True, en=True)
            else:
                cmds.floatSliderGrp(self.fs_grp_09, e=True, en=False)
                cmds.checkBox(self.checkbox_04, e=True, en=False)
        elif current_tab_index == 2:
            current = cmds.checkBox(self.checkbox_07, q=True, v=True)
            if current:
                cmds.floatSliderGrp(self.fs_grp_24, e=True, en=True)
                cmds.checkBox(self.checkbox_10, e=True, en=True)
                cmds.optionMenuGrp(self.op_menu_grp_01, e=True, en=True)
            else:
                cmds.floatSliderGrp(self.fs_grp_24, e=True, en=False)
                cmds.checkBox(self.checkbox_10, e=True, en=False)
                cmds.optionMenuGrp(self.op_menu_grp_01, e=True, en=False)

    def bake_tolerance_enabled(self, *args):
        current = cmds.checkBox(self.checkbox_02, q=True, v=True)
        if current:
            cmds.floatFieldGrp(self.ff_grp_02, e=True, en1=True)
        else:
            cmds.floatFieldGrp(self.ff_grp_02, e=True, en1=False)

    '''Update Functions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'''
    def update_namespaces_scroll_list(self, *args):
        current_tab_index = cmds.tabLayout(self.tab_layout_01, q=True, sti=True)
        cmds.textScrollList(self.ts_list_03, e=True, ra=True)
        namespaces = cmds.namespaceInfo(r=True, lon=True)
        if 'UI' in namespaces:
            namespaces.remove('UI')
        if 'shared' in namespaces:
            namespaces.remove('shared')
        if current_tab_index == 1:
            tag = '.isDynamicsKeyer_Curve'
        elif current_tab_index == 2:
            tag = '.isDynamicsKeyer_Cloth'
        setups = DK_Utils.search_namespaces(tag)
        if setups:
            if namespaces != [] and not None:
                for namespace in namespaces:
                    check_list = [setup for setup in setups if namespace in setup]
                    if check_list:
                        cmds.textScrollList(self.ts_list_03, e=True, a=namespace + ':')
                    else:
                        continue
                no_ns_check = [setup for setup in setups if ':' not in setup]
                if no_ns_check:
                    cmds.textScrollList(self.ts_list_03, e=True, a='No_Namespace:')
                cmds.textScrollList(self.ts_list_03, e=True, sii=1)
        else:
            return

    def update_curve_setup_scroll_list(self, *args):
        namespaces = cmds.textScrollList(self.ts_list_03, q=True, si=True)
        if not namespaces:
            namespaces = ['']
        list_old = DK_Utils.search_namespaces('.IsDynamicsKeyer')
        list_all = DK_Utils.search_namespaces('.isDynamicsKeyer_Curve')
        if list_old:
            list_all = list_all + list_old
        if list_all:
            list_new = [each.replace('_Dynamics_GRP.isDynamicsKeyer_Curve', '') for each in list_all]
            cmds.textScrollList(self.ts_list_01, e=True, ra=True)
            for namespace in namespaces:
                for item in list_new:
                    if namespace == 'No_Namespace:':
                        if ':' not in item:
                            cmds.textScrollList(self.ts_list_01, e=True, a=item)
                    elif namespace in item:
                        cmds.textScrollList(self.ts_list_01, e=True, a=item)
            cmds.textScrollList(self.ts_list_01, e=True, sii=1)
            cmds.frameLayout(self._frame_layout_02, e=True, en=True)
            cmds.frameLayout(self._frame_layout_05, e=True, en=True)
            cmds.frameLayout(self._frame_layout_03, e=True, en=True)
            cmds.gradientControlNoAttr(self.ramp_02, e=True, vis=False, en=False)
            cmds.gradientControl(self.ramp_01, e=True, vis=True, en=True)
            self.connect_controls()
            return list_new
        else:
            cmds.textScrollList(self.ts_list_01, e=True, ra=True)
            cmds.frameLayout(self._frame_layout_02, e=True, en=False)
            cmds.frameLayout(self._frame_layout_05, e=True, en=False)
            cmds.frameLayout(self._frame_layout_03, e=True, en=False)
            cmds.gradientControl(self.ramp_01, e=True, vis=False, en=False)
            cmds.gradientControlNoAttr(self.ramp_02, e=True, vis=True, en=False)

        self.update_preset_list()

    def update_cloth_setup_scroll_list(self, *args):
        namespaces = cmds.textScrollList(self.ts_list_03, q=True, si=True)
        if not namespaces:
            namespaces = ['']
        list_all = DK_Utils.search_namespaces('.isDynamicsKeyer_Cloth')
        if list_all:
            list_new = [each.replace('_Dynamics_GRP.isDynamicsKeyer_Cloth', '') for each in list_all]
            cmds.textScrollList(self.ts_list_04, e=True, ra=True)
            for namespace in namespaces:
                for item in list_new:
                    if namespace == 'No_Namespace:':
                        if ':' not in item:
                            cmds.textScrollList(self.ts_list_04, e=True, a=item)
                    elif namespace in item:
                        cmds.textScrollList(self.ts_list_04, e=True, a=item)
            cmds.textScrollList(self.ts_list_04, e=True, sii=1)
            cmds.frameLayout(self._frame_layout_08, e=True, en=True)
            cmds.frameLayout(self._frame_layout_09, e=True, en=True)
            self.connect_controls()
            return list_new
        else:
            cmds.textScrollList(self.ts_list_04, e=True, ra=True)
            cmds.frameLayout(self._frame_layout_08, e=True, en=False)
            cmds.frameLayout(self._frame_layout_09, e=True, en=False)

        self.update_preset_list()

    def update_collider_scroll_list(self, *args):
        list_all = DK_Utils.search_namespaces('.isCollider')
        if list_all:
            cmds.textScrollList(self.ts_list_02, e=True, ra=True)
            list_new = [each.replace('.isCollider', '') for each in list_all]
            for obj in list_new:
                cmds.textScrollList(self.ts_list_02, e=True, a=obj)
            cmds.textScrollList(self.ts_list_02, e=True, da=True)
            cmds.frameLayout(self._frame_layout_07, e=True, en=True)
        else:
            cmds.textScrollList(self.ts_list_02, e=True, ra=True)
            cmds.frameLayout(self._frame_layout_07, e=True, en=False)

    def update_constraint_scroll_list(self, *args):
        list_all = DK_Utils.search_namespaces('.isConstraint')
        if list_all:
            cmds.textScrollList(self.ts_list_05, e=True, ra=True)
            list_new = [each.replace('.isConstraint', '') for each in list_all]
            for obj in list_new:
                cmds.textScrollList(self.ts_list_05, e=True, a=obj)
            cmds.textScrollList(self.ts_list_05, e=True, da=True)
            cmds.frameLayout(self._frame_layout_10, e=True, en=True)
        else:
            cmds.textScrollList(self.ts_list_05, e=True, ra=True)
            cmds.frameLayout(self._frame_layout_10, e=True, en=False)

    def update_keyed_controls(self, tab_layout, *args):
        if tab_layout == 1:
            current_tab_index = cmds.tabLayout(self.tab_layout_01, q=True, sti=True)
            if current_tab_index == 1:
                scroll_list = self.ts_list_01
                solver_attr = '.hairSystem'
                attr_ctrl = {'stretchResistance': self.fs_grp_01, 'bendResistance': self.fs_grp_04,
                             'damp': self.fs_grp_02, 'drag': self.fs_grp_03, 'startCurveAttract': self.fs_grp_08}
            elif current_tab_index == 2:
                scroll_list = self.ts_list_04
                solver_attr = '.nCloth'
                attr_ctrl = {'stretchResistance': self.fs_grp_05, 'compressionResistance': self.fs_grp_07,
                             'bendResistance': self.fs_grp_12, 'damp': self.fs_grp_15, 'drag': self.fs_grp_16,
                             'inputMeshAttract': self.fs_grp_18, 'inputAttractDamp': self.fs_grp_19, 'thickness': self.fs_grp_24}
        elif tab_layout == 2:
            current_tab_index = cmds.tabLayout(self.tab_layout_03, q=True, sti=True)
            if current_tab_index == 1:
                scroll_list = self.ts_list_02
                attr_ctrl = {'thickness': self.fs_grp_11}
            elif current_tab_index == 2:
                scroll_list = self.ts_list_05
                attr_ctrl = {'strength': self.fs_grp_20, 'glueStrength': self.fs_grp_13}
        items = DK_Utils.get_scroll_list(scroll_list)
        if items:
            if tab_layout == 1:
                items = [cmds.getAttr(item + '_Dynamics_GRP' + solver_attr) for item in items]
            elif tab_layout == 2:
                items = [cmds.listRelatives(item, s=True, ni=True)[0] for item in items]
            for attr in attr_ctrl.keys():
                total_count = 0
                for item in items:
                    key_count = cmds.keyframe(item + '.' + attr, q=True, kc=True)
                    if key_count:
                        total_count += key_count
                    else:
                        continue
                if total_count != 0:
                    cmds.floatSliderGrp(attr_ctrl[attr], e=True, bgc=[0.6, 0.4, 0.4])
                else:
                    cmds.floatSliderGrp(attr_ctrl[attr], e=True, bgc=[0.267, 0.267, 0.267])

    def connect_control_nucleus(self):
        if cmds.objExists(self.nucleus_name):
            cmds.frameLayout(self._frame_layout_04, e=True, en=True)
            cmds.connectControl(self.fs_grp_06, self.nucleus_name + '.gravity')
            cmds.connectControl(self.fs_grp_10, self.nucleus_name + '.spaceScale')
            cmds.connectControl(self.is_grp_01, self.nucleus_name + '.subSteps')
            cmds.connectControl(self.if_grp_01, self.nucleus_name + '.startFrame')
        else:
            cmds.frameLayout(self._frame_layout_04, e=True, en=False)

    def set_follicle_attach(self, mode):
        mode_interp = {'No Attach': 0, 'Base': 1, 'Tip': 2, 'Both Ends': 3}
        setups = DK_Utils.get_scroll_list(self.ts_list_01)
        if setups:
            follicles = [cmds.getAttr(setup + '_Dynamics_GRP.follicle') for setup in setups]
            for follicle in follicles:
                cmds.setAttr(follicle + '.pointLock', mode_interp[mode])

    def set_collision_flag(self, mode):
        mode_interp = {'Vertex': 1, 'Edge': 2, 'Face': 3}
        setups = DK_Utils.get_scroll_list(self.ts_list_04)
        if setups:
            ncloths = [cmds.getAttr(setup + '_Dynamics_GRP.nCloth') for setup in setups]
            for ncloth in ncloths:
                cmds.setAttr(ncloth + '.collisionFlag', mode_interp[mode])

    def set_keyframe_popup_command(self, objs, attribute, *args):
        if objs:
            current_time = cmds.currentTime(q=True)
            for obj in objs:
                cmds.setKeyframe(obj, at=attribute, t=current_time)
            self.update_keyed_controls(1)
            self.update_keyed_controls(2)
            OpenMaya.MGlobal.displayInfo('Set Keyframe: %s | Attribute: %s | Frame: %s' % (', '.join(objs), attribute, current_time))

    def break_connection_popup_command(self, objs, attribute, *args):
        if objs:
            for obj in objs:
                source_con = cmds.listConnections(obj + '.' + attribute, s=True, d=False, p=True)
                if source_con:
                    for con in source_con:
                        cmds.disconnectAttr(con, obj + '.' + attribute)
                else:
                    continue
            self.update_keyed_controls(1)
            self.update_keyed_controls(2)
            OpenMaya.MGlobal.displayInfo('Break Connection: %s | Attribute: %s' % (', '.join(objs), attribute))

    def connect_controls(self, *args):
        current_tab_index = cmds.tabLayout(self.tab_layout_01, q=True, sti=True)
        if current_tab_index == 1:
            self.connect_curve_controls()
        elif current_tab_index == 2:
            self.connect_cloth_controls()

    def connect_curve_controls(self, *args):
        setups = DK_Utils.get_scroll_list(self.ts_list_01)
        if setups:
            setups = [setup + '_Dynamics_GRP' for setup in setups]
            hsys = [cmds.getAttr(setup + '.hairSystem') for setup in setups]
            follicles = [cmds.getAttr(setup + '.follicle') for setup in setups]
            attrs = ['.stretchResistance', '.bendResistance', '.damp', '.drag', '.startCurveAttract', '.collide',
                     '.collideWidthOffset', '.ignoreSolverGravity', '.solverDisplay']

            fol_value = cmds.getAttr(follicles[-1] + '.pointLock')
            cmds.optionMenuGrp(self.op_menu_grp_03, e=True, sl=fol_value + 1)

            set_key_pu_dict = {self.pu_menu_04: 'stretchResistance', self.pu_menu_05: 'bendResistance',
                               self.pu_menu_06: 'damp', self.pu_menu_07: 'drag', self.pu_menu_08: 'startCurveAttract', self.pu_menu_19: 'thickness'}
            for each_ctrl in set_key_pu_dict.keys():
                cmds.popupMenu(each_ctrl, e=True, dai=True)
                cmds.menuItem(l='Set Key', p=each_ctrl, c=partial(self.set_keyframe_popup_command, hsys, set_key_pu_dict[each_ctrl]))
                cmds.menuItem(l='Break Connection', p=each_ctrl, c=partial(self.break_connection_popup_command, hsys, set_key_pu_dict[each_ctrl]))

            hair_attrs = []
            for x in hsys:
                hair_attrs.append([x+y for y in attrs])

            sliders = [self.fs_grp_01, self.fs_grp_04, self.fs_grp_02, self.fs_grp_03, self.fs_grp_08, self.checkbox_03,
                       self.fs_grp_09, self.checkbox_11, self.checkbox_04]

            values = [cmds.getAttr(hsys[-1] + attr) for attr in attrs]
            if len(hair_attrs) >> 1:

                if all(len(x) == len(hair_attrs[0]) for x in hair_attrs[0:]):
                    sort_hair_attrs = []
                    for si in xrange(len(hair_attrs[0])):
                        sort_hair_attrs.append([hair_attrs[x][si] for x in xrange(len(hair_attrs))])
            else:
                sort_hair_attrs = hair_attrs

            if len(sort_hair_attrs) >> 1:
                for each in zip(sliders, sort_hair_attrs, values):
                    cmds.connectControl(each[0], each[1])
            else:
                for each in zip(sliders, sort_hair_attrs[0], values):
                    cmds.connectControl(each[0], each[1])
                    cmds.setAttr(each[1], each[2])
            # Clean up gradient connections
            all_hsys = [cmds.getAttr(x + '_Dynamics_GRP.hairSystem') for x in cmds.textScrollList(self.ts_list_01, q=True, ai=True)]
            for ea in all_hsys:
                source = cmds.listConnections(ea + '.attractionScale', s=True, d=False, p=True)
                if source:
                    cmds.disconnectAttr(source[0], ea + '.attractionScale')
                else:
                    continue
            # Connect
            if len(hsys) >> 1:
                cmds.gradientControl(self.ramp_01, e=True, at=hsys[-1] + '.attractionScale', vis=True)
                for each in hsys[:-1]:
                    cmds.connectAttr(hsys[-1] + '.attractionScale', each + '.attractionScale', f=True)
            else:
                cmds.gradientControl(self.ramp_01, e=True, at=hsys[-1] + '.attractionScale', vis=True)

        self.update_keyed_controls(1)
        self.update_preset_list()

    def connect_cloth_controls(self, *args):
        setups = DK_Utils.get_scroll_list(self.ts_list_04)
        if setups:
            setups = [setup + '_Dynamics_GRP' for setup in setups]
            ncloth = [cmds.getAttr(setup + '.nCloth') for setup in setups]
            attrs = ['.stretchResistance', '.compressionResistance', '.bendResistance', '.damp', '.drag',
                     '.inputMeshAttract', '.inputAttractDamp', '.collide', '.thickness', '.ignoreSolverGravity', '.solverDisplay']

            col_flag_value = cmds.getAttr(ncloth[-1] + '.collisionFlag')
            cmds.optionMenuGrp(self.op_menu_grp_01, e=True, sl=col_flag_value)

            set_key_pu_dict = {self.pu_menu_09: 'stretchResistance', self.pu_menu_10: 'compressionResistance', self.pu_menu_11: 'bendResistance',
                               self.pu_menu_12: 'damp', self.pu_menu_13: 'drag', self.pu_menu_14: 'inputMeshAttract',
                               self.pu_menu_15: 'inputAttractDamp', self.pu_menu_20: 'thickness'}
            for each_ctrl in set_key_pu_dict.keys():
                cmds.popupMenu(each_ctrl, e=True, dai=True)
                cmds.menuItem(l='Set Key', p=each_ctrl, c=partial(self.set_keyframe_popup_command, ncloth, set_key_pu_dict[each_ctrl]))
                cmds.menuItem(l='Break Connection', p=each_ctrl, c=partial(self.break_connection_popup_command, ncloth, set_key_pu_dict[each_ctrl]))

            cloth_attrs = []
            for x in ncloth:
                cloth_attrs.append([x + y for y in attrs])

            sliders = [self.fs_grp_05, self.fs_grp_07, self.fs_grp_12, self.fs_grp_15, self.fs_grp_16,
                       self.fs_grp_18, self.fs_grp_19, self.checkbox_07, self.fs_grp_24, self.checkbox_12, self.checkbox_10]

            values = [cmds.getAttr(ncloth[-1] + attr) for attr in attrs]
            if len(cloth_attrs) >> 1:

                if all(len(x) == len(cloth_attrs[0]) for x in cloth_attrs[0:]):
                    sort_cloth_attrs = []
                    for si in xrange(len(cloth_attrs[0])):
                        sort_cloth_attrs.append([cloth_attrs[x][si] for x in xrange(len(cloth_attrs))])
            else:
                sort_cloth_attrs = cloth_attrs

            if len(sort_cloth_attrs) >> 1:
                for each in zip(sliders, sort_cloth_attrs, values):
                    cmds.connectControl(each[0], each[1])
            else:
                for each in zip(sliders, sort_cloth_attrs[0], values):
                    cmds.connectControl(each[0], each[1])
                    cmds.setAttr(each[1], each[2])

        self.update_keyed_controls(1)
        self.update_preset_list()

    '''Build Functions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'''
    def build_curve_setup(self, *args):
        sel = DK_Utils.get_selection()
        if sel:
            cb_state = DK_Utils.checkbox_state(self.checkbox_01)
            ctrl_scale = cmds.floatFieldGrp(self.ff_grp_03, q=True, v1=True) * 1.4
            self.DK_hair.construct_setup(sel, cb_state, ctrl_scale, None)
            DK_Utils.command_iterator([self.fs_grp_06, self.fs_grp_10, self.is_grp_01, self.if_grp_01],
                                      [self.nucleus_name + '.gravity', self.nucleus_name + '.spaceScale',
                                      self.nucleus_name + '.subSteps', self.nucleus_name + '.startFrame'],
                                      command='connectControl', mode=1)
            self.refresh()
        else:
            cmds.warning('Nothing selected.')

    def build_cloth_setup(self, *args):
        sel = DK_Utils.get_selection()
        if sel:
            cb_state = DK_Utils.checkbox_state(self.checkbox_09)
            self.DK_cloth.construct_setup(sel, None, cb_state=cb_state)
            self.refresh()
        else:
            cmds.warning('Nothing selected.')

    def bake_dyn_to_anim(self, *args):
        current_tab_index = cmds.tabLayout(self.tab_layout_01, q=True, sti=True)
        if current_tab_index == 1:
            scroll_list = self.ts_list_01
        elif current_tab_index == 2:
            scroll_list = self.ts_list_04
        setups = DK_Utils.get_scroll_list(scroll_list)
        if setups:
            setups = [x + '_Dynamics_GRP' for x in setups]
            controls = []
            for setup in setups:
                list_anim_controls = cmds.getAttr(setup + '.animControls')
                for each_control in list_anim_controls:
                    controls.append(each_control)
            enable = DK_Utils.checkbox_state(self.checkbox_02)
            tolerance = 0.05
            if cmds.floatFieldGrp(self.ff_grp_02, q=True, en=True):
                tolerance = cmds.floatFieldGrp(self.ff_grp_02, q=True, v=True)
            #  Get timerange
            start_frame = cmds.playbackOptions(q=True, min=True)
            end_frame = cmds.playbackOptions(q=True, max=True)
            time_range = int(start_frame), int(end_frame)
            #  Overwrite Display text
            OpenMaya.MGlobal.displayInfo('')
            #  Bake simulation
            cmds.bakeResults(controls, sm=True, t=time_range, at=['tx', 'ty', 'tz', 'rx', 'ry', 'rz'])
            if enable:
                cmds.filterCurve(controls, f='simplify', tto=tolerance[0], tol=0.01, startTime=start_frame, endTime=end_frame)
            for each_control in controls:
                list_attrs = cmds.listAttr(each_control, k=True)
                for attr in list_attrs:
                    if 'blendParent' in attr:
                        cmds.deleteAttr(each_control + '.' + attr)
            #  Delete component
            self.delete_setup()
        else:
            cmds.warning('No dynamics setups selected to bake.')

    def delete_setup(self, *args):
        current_tab_index = cmds.tabLayout(self.tab_layout_01, q=True, sti=True)
        if current_tab_index == 1:
            scroll_list = self.ts_list_01
        else:
            scroll_list = self.ts_list_04
        setups = DK_Utils.get_scroll_list(scroll_list)
        if setups:
            setups = [x + '_Dynamics_GRP' for x in setups]
            dynamics_keyers = DK_Utils.search_namespaces('.isDynamicsKeyer_Curve') + DK_Utils.search_namespaces('.isDynamicsKeyer_Cloth')
            if abs(len(setups) - len(dynamics_keyers)) == 0:
                all_par = []
                for setup in setups:
                    parents = DK_Utils.get_all_parents(setup)
                    if parents:
                        all_par.extend(parents)
                for par in all_par:
                    if "Dynamics_Main_GRP" not in par:
                        all_par.remove(par)
                    else:
                        continue
                if all_par:
                    cmds.delete(all_par)
                list_shaders = DK_Utils.search_namespaces('dynamics_keyer*blinn')
                if list_shaders:
                    for shader in list_shaders:
                        cmds.delete(shader)
            else:
                for setup in setups:
                    all_attrs = cmds.listAttr(setup)
                    if "cacheParts" in all_attrs:
                        cache_parts = cmds.getAttr(setup + '.cacheParts')
                        for each_part in cache_parts:
                            if cmds.objExists(each_part):
                                cmds.delete(each_part)
                            else:
                                continue
                cmds.delete(setups)
            if current_tab_index == 1:
                self.update_curve_setup_scroll_list()
            elif current_tab_index == 2:
                self.update_cloth_setup_scroll_list()
            self.refresh()
        else:
            cmds.warning('No dynamics setups selected to delete.')
            return

    def add_collider(self, objs, *args):
        if objs:
            for ea in objs:
                if cmds.nodeType(cmds.listRelatives(ea, s=True, ni=True, f=True)) == 'mesh':
                    old_nuke = cmds.ls(type='nucleus')
                    nucleus = DK_Utils.create_nucleus(self.nucleus_name)
                    if cmds.getAttr(nucleus + '.visibility') != 0:
                        cmds.setAttr(nucleus + '.visibility', 0)
                    cmds.select(ea)
                    make_collider = mel.eval('makeCollideNCloth;')
                    new_nuke = cmds.ls(type='nucleus')
                    DK_Utils.connect_nucleus(nucleus, make_collider[0])
                    trash = []
                    for each in new_nuke:
                        if each != nucleus and each not in old_nuke:
                            trash.append(each)
                    if trash:
                        cmds.delete(trash)
                    parent = cmds.listRelatives(make_collider[0], p=True)[0]
                    parent = cmds.rename(parent, ea + '_' + parent)
                    nrigid_shape = cmds.listRelatives(parent, s=True, ni=True)[0]
                    col_disp = self.create_collision_display(ea, nrigid_shape)
                    # TAG
                    cmds.addAttr(parent, ln='isCollider', at='message')
                    cmds.addAttr(parent, ln='collisionDisplay', dt="string")
                    cmds.setAttr(parent + '.collisionDisplay', col_disp, type="string")
                    cmds.setAttr(parent + '.visibility', 0)
                    DK_Utils.organize(parent, 'Dynamics_Main_GRP/Collider_GRP')
                    collider_grps = DK_Utils.search_namespaces('Dynamics_Main_GRP|Collider_GRP')
                    for each in collider_grps:
                        if cmds.getAttr(each + '.visibility') != 1:
                            cmds.setAttr(each + '.visibility', 1)
                    cmds.select(cl=True)
                else:
                    cmds.warning('Selected must be "polymesh" object/objects.')
            self.update_collider_scroll_list()
        else:
            cmds.warning('Nothing selected.')

    def add_collider_sel(self, *args):
        sel = DK_Utils.get_selection()
        if sel:
            self.add_collider(sel)
        else:
            cmds.warning('Nothing Selected.')

    def remove_collider(self, *args):
        objs = cmds.textScrollList(self.ts_list_02, q=True, si=True)
        if objs:
            trash = []
            for ea in objs:
                trans_shape = cmds.listRelatives(ea, s=True, ni=True)
                if trans_shape:
                    if cmds.objectType(trans_shape, i='nRigid'):
                        col_disp = cmds.getAttr(ea + '.collisionDisplay')
                        trash.append(ea)
                        trash.append(col_disp)
            if trash:
                result = cmds.confirmDialog(t='Delete Collider', m='Are you sure to delete "' + '", "'.join(objs) + '" ?',
                                            b=['OK', 'Cancel'], db='OK', cb='Cancel')
                if result == 'OK':
                    cmds.delete(trash)
            self.update_collider_scroll_list()
        else:
            cmds.warning('Nothing Selected.')

    def add_constraint(self, objs, *args):
        if objs:
            type_dict = {}
            for each in objs:
                obj_type = cmds.objectType(each)
                if obj_type not in type_dict.keys():
                    type_dict[obj_type] = [each]
                else:
                    type_dict[obj_type].append(each)

            if 'mesh' in type_dict.keys() and 'transform' in type_dict.keys():
                api.log.logger().debug(type_dict['mesh'])
                pars = list(set(x.split('.')[0] for x in type_dict['mesh']))
                for par in pars:
                    if 'outputCloth' in par:
                        par_trans = cmds.listRelatives(par, p=True)[0].replace('_NC_MESH', '')
                        par_shape = par
                    else:
                        continue
                par_input_plug = cmds.listConnections(par_shape, type='nCloth')
                trans_shape = cmds.listRelatives(type_dict['transform'], s=True, ni=True)
                trans_input_plug = cmds.listConnections(trans_shape, type='nRigid')
                if not trans_input_plug:
                    self.add_collider(type_dict['transform'])
                if par_input_plug:
                    cmds.select(type_dict['mesh'], type_dict['transform'])
                    results = mel.eval('createNConstraint pointToSurface 0;')
                    for each in results:
                        if cmds.objectType(each, i='dynamicConstraint'):
                            par_each = cmds.listRelatives(each, p=True)[0]
                            # TAG
                            target = type_dict['transform'][0]
                            setup = par_trans
                            cmds.addAttr(par_each, ln='target', dt="string")
                            cmds.setAttr(par_each + '.target', target, type="string")
                            cmds.addAttr(par_each, ln='setup', dt="string")
                            cmds.setAttr(par_each + '.setup', setup, type="string")
                            cmds.addAttr(par_each, ln='isConstraint', at='message')
                            par_each = cmds.rename(par_each, setup + '_' + par_each)
                            DK_Utils.organize(par_each, 'Dynamics_Main_GRP/Cloth_Setups_GRP/Dynamic_Constraint_GRP')
                else:
                    cmds.warning('No nMesh selected.')
                self.update_constraint_scroll_list()
            else:
                cmds.warning('Need to select verts and geo.')

    def add_constraint_sel(self, *args):
        sel = DK_Utils.get_selection()
        self.add_constraint(sel)

    def remove_constraint(self, objs, *args):
        objs = cmds.textScrollList(self.ts_list_05, q=True, si=True)
        if objs:
            trash = []
            for each in objs:
                trans_shape = cmds.listRelatives(each, s=True, ni=True)
                if trans_shape:
                    if cmds.objectType(trans_shape, i='dynamicConstraint'):
                        trash.append(each)
            if trash:
                result = cmds.confirmDialog(t='Delete Constraint',
                                            m='Are you sure to delete "' + '", "'.join(trash) + '" ?',
                                            b=['OK', 'Cancel'], db='OK', cb='Cancel')
                if result == 'OK':
                    cmds.delete(trash)
                else:
                    return
            self.update_constraint_scroll_list()
        else:
            cmds.warning('Nothing Selected.')

    def create_collision_display(self, collider, nrigid_node):
        display_mesh = cmds.duplicate(collider, n=collider + '_COL_DISP_MESH')[0]
        display_mesh_shapes = cmds.listRelatives(display_mesh, s=True, ni=True, f=True)
        for shape in display_mesh_shapes:
            if 'outputCloth' in shape:
                cmds.delete(shape)
            else:
                cmds.setAttr(shape + '.visibility', 1)
                display_mesh_shape = shape
        collider_output = cmds.listRelatives(collider, s=True, ni=True)[0]
        cmds.connectAttr(collider_output + '.worldMesh[0]', display_mesh_shape + '.inMesh')
        face_count = cmds.polyEvaluate(display_mesh_shape, f=True)
        cmds.polyExtrudeFacet(display_mesh_shape + '.f[0:' + str(face_count - 1) + ']', kft=True)
        re_count = cmds.polyEvaluate(display_mesh_shape, f=True)
        thickness_extrude = cmds.polyExtrudeFacet(display_mesh_shape + '.f[0:' + str(re_count - 1) + ']', kft=True)[0]
        cmds.connectAttr(nrigid_node + '.thickness', thickness_extrude + '.localTranslateZ')
        cmds.connectAttr(nrigid_node + '.solverDisplay', display_mesh + '.visibility')
        # ASSIGN SHADER TO COLLISION DISPLAY MESH
        shader = 'dynamics_keyer_collider_collision_display_blinn'
        if not cmds.objExists(shader):
            shader = cmds.shadingNode('blinn', n=shader, asShader=True)
        cmds.setAttr(shader + '.color', 1.0, 0.5, 0.5, type='double3')
        cmds.setAttr(shader + '.transparency', 0.75, 0.75, 0.75, type='double3')
        SG = shader + 'SG'
        if not cmds.objExists(SG):
            SG = cmds.sets(renderable=True, noSurfaceShader=True, empty=True, name=shader + 'SG')
        plug = cmds.listConnections(SG + '.surfaceShader', s=True, d=False, p=True)
        if plug:
            if shader not in plug[0]:
                cmds.connectAttr(shader + '.outColor', SG + '.surfaceShader', f=True)
        else:
            cmds.connectAttr(shader + '.outColor', SG + '.surfaceShader', f=True)
        cmds.sets(display_mesh_shape, e=True, forceElement=SG)
        cons = cmds.listConnections(display_mesh, p=True)
        for con in cons:
            if '.drawInfo' in con:
                cmds.disconnectAttr(con, display_mesh + '.drawOverride')
        cmds.setAttr(display_mesh + '.overrideEnabled', 1)
        cmds.setAttr(display_mesh + '.overrideDisplayType', 2)
        DK_Utils.organize(display_mesh, 'Dynamics_Main_GRP/Collider_GRP/Geometry_GRP')
        return display_mesh

    def connect_control_collider(self, *args):
        colliders = DK_Utils.get_scroll_list(self.ts_list_02)
        if colliders:
            colliders = [cmds.listRelatives(collider, s=True, ni=True)[0] for collider in colliders]
            attrs = ['.collide', '.thickness', '.solverDisplay']

            set_key_pu_dict = {self.pu_menu_16: 'thickness'}
            for each_ctrl in set_key_pu_dict.keys():
                cmds.popupMenu(each_ctrl, e=True, dai=True)
                cmds.menuItem(l='Set Key', p=each_ctrl, c=partial(self.set_keyframe_popup_command,
                              colliders, set_key_pu_dict[each_ctrl]))
                cmds.menuItem(l='Break Connection', p=each_ctrl, c=partial(self.break_connection_popup_command,
                              colliders, set_key_pu_dict[each_ctrl]))

            collider_attr = []
            for x in colliders:
                collider_attr.append([x + y for y in attrs])

            sliders = [self.checkbox_05, self.fs_grp_11, self.checkbox_06]

            values = [cmds.getAttr(colliders[-1] + attr) for attr in attrs]
            if len(collider_attr) >> 1:

                if all(len(x) == len(collider_attr[0]) for x in collider_attr[0:]):
                    sort_collider_attr = []
                    for si in xrange(len(collider_attr[0])):
                        sort_collider_attr.append([collider_attr[x][si] for x in xrange(len(collider_attr))])
            else:
                sort_collider_attr = collider_attr

            if len(sort_collider_attr) >> 1:
                for each in zip(sliders, sort_collider_attr, values):
                    cmds.connectControl(each[0], each[1])
            else:
                for each in zip(sliders, sort_collider_attr[0], values):
                    cmds.connectControl(each[0], each[1])
                    cmds.setAttr(each[1], each[2])

        self.update_keyed_controls(2)

    def connect_control_constraint(self, *args):
        constraints = DK_Utils.get_scroll_list(self.ts_list_05)
        if constraints:
            constraints = [cmds.listRelatives(constraint, s=True, ni=True)[0] for constraint in constraints]
            attrs = ['.enable', '.strength', '.glueStrength']

            set_key_pu_dict = {self.pu_menu_17: 'strength', self.pu_menu_18: 'glueStrength'}
            for each_ctrl in set_key_pu_dict.keys():
                cmds.popupMenu(each_ctrl, e=True, dai=True)
                cmds.menuItem(l='Set Key', p=each_ctrl, c=partial(self.set_keyframe_popup_command,
                              constraints, set_key_pu_dict[each_ctrl]))
                cmds.menuItem(l='Break Connection', p=each_ctrl, c=partial(self.break_connection_popup_command,
                              constraints, set_key_pu_dict[each_ctrl]))

            constraint_attr = []
            for x in constraints:
                constraint_attr.append([x + y for y in attrs])

            sliders = [self.checkbox_13, self.fs_grp_20, self.fs_grp_13]

            values = [cmds.getAttr(constraints[-1] + attr) for attr in attrs]
            if len(constraint_attr) >> 1:

                if all(len(x) == len(constraint_attr[0]) for x in constraint_attr[0:]):
                    sort_constraint_attr = []
                    for si in xrange(len(constraint_attr[0])):
                        sort_constraint_attr.append([constraint_attr[x][si] for x in xrange(len(constraint_attr))])
            else:
                sort_constraint_attr = constraint_attr

            if len(sort_constraint_attr) >> 1:
                for each in zip(sliders, sort_constraint_attr, values):
                    cmds.connectControl(each[0], each[1])
            else:
                for each in zip(sliders, sort_constraint_attr[0], values):
                    cmds.connectControl(each[0], each[1])
                    cmds.setAttr(each[1], each[2])

        self.update_keyed_controls(2)
