from maya import cmds


class DK_Utils:

    @staticmethod
    def organize(obj, path):
        node_type = cmds.nodeType(obj)
        if node_type != 'transform' and node_type != 'nucleus' and node_type != 'joint' and node_type != 'ikHandle':
            return
        parents = path.split('/')
        for i, each_parent in enumerate(parents):
            if not cmds.objExists(each_parent):
                cmds.group(em=True, n=each_parent)
            if i != 0:
                if not cmds.listRelatives(each_parent, p=True):
                    cmds.parent(each_parent, parents[i - 1])
        cmds.parent(obj, parents[-1])
        destination_path = cmds.ls(parents[-1], l=True)[0]
        return str(destination_path)

    @staticmethod
    def distance(pointA=0.0, pointB=0.0):
        if pointA > pointB:
            distance = abs(pointA - pointB)
        elif pointB > pointA:
            distance = abs(pointB - pointA)
        elif pointA == pointB:
            distance = 0.0
        return distance

    @classmethod
    def get_radius(cls, obj):
        bbox = cmds.exactWorldBoundingBox(obj)
        size_x = round(cls.distance(bbox[0], bbox[3]) / 2, 2)
        size_y = round(cls.distance(bbox[1], bbox[4]) / 2, 2)
        size_z = round(cls.distance(bbox[2], bbox[5]) / 2, 2)
        averaged_radius = (size_x + size_y + size_z) / 3
        return averaged_radius

    @staticmethod
    def get_selection(l=False):
        sel = cmds.ls(sl=True, flatten=True, l=l)
        if not sel:
            cmds.warning('Nothing selected.')
            return
        cmds.select(cl=True)
        return sel

    @staticmethod
    def get_scroll_list(scroll_list):
        items = cmds.textScrollList(scroll_list, q=True, si=True)
        if items:
            return items
        else:
            return

    @classmethod
    def get_hairsystem(cls, scroll_list, *args):
        setups = cls.get_scroll_list(scroll_list)
        if setups:
            hairsystems = [cmds.getAttr(x + '_Dynamics_GRP.hairSystem') for x in setups]
            if hairsystems:
                return hairsystems
            else:
                return
        else:
            return

    @staticmethod
    def search_namespaces(string, *args):
        namespaces = cmds.namespaceInfo(lon=True, r=True)
        if 'UI' in namespaces:
            namespaces.remove('UI')
        if 'shared' in namespaces:
            namespaces.remove('shared')
        if namespaces != [] and namespaces is not None:
            nsp_depth = max([x.count(':') for x in namespaces])
            if nsp_depth == 0:
                nsp_depth = 1
        else:
            nsp_depth = 0

        search_strings = ['*:' * x + '*' + string for x in range(nsp_depth + 1)]
        list_all = []
        for each_term in search_strings:
            list_all.extend(cmds.ls(each_term))

        return list_all

    @classmethod
    def create_nucleus(cls, nucleus, *args):
        if not cmds.objExists(nucleus):
            nucleus = cmds.createNode('nucleus', n=nucleus)
            cmds.connectAttr('time1.outTime', nucleus + '.currentTime')
            cls.organize(nucleus, 'Dynamics_Main_GRP')
        else:
            nucleus = nucleus
        return nucleus

    @staticmethod
    def scroll_list_select(scroll_list, *args):
        item = cmds.textScrollList(scroll_list, q=True, si=True)
        cmds.select(item)
        return item

    @staticmethod
    def checkbox_state(checkbox):
        state = cmds.checkBox(checkbox, q=True, v=True)
        return state

    @staticmethod
    def command_iterator(objA, objB, prefix='cmds.', command='', flags='', mode=0):
        if command != '':
            if mode == 0:
                for obj in objA:
                    eval("%s(obj, %s)" % prefix + command, objB + flags)

            elif mode == 1:
                for objAB in zip(objA, objB):
                    print("%s('%s', '%s'%s)" % (prefix + command, objAB[0], objAB[1], flags))
                    eval("%s('%s', '%s'%s)" % (prefix + command, objAB[0], objAB[1], flags))
        else:
            cmds.warning('No command specified to execute.')

    @staticmethod
    def connect_nucleus(nucleus, dynamic_obj):
        if not cmds.objectType(dynamic_obj, i='nRigid'):
            dynamic_obj_attrs = ['.currentState', '.startState', '.nextState']
            nuke_attrs = ['.inputActive', '.inputActiveStart', '.outputObjects']
            attrs_indice = [cmds.getAttr(nucleus + each_attr, mi=True) for each_attr in nuke_attrs]
        else:
            dynamic_obj_attrs = ['.currentState', '.startState']
            nuke_attrs = ['.inputPassive', '.inputPassiveStart']
            attrs_indice = [cmds.getAttr(nucleus + each_attr, mi=True) for each_attr in nuke_attrs]

        for i, each_items in enumerate(zip(attrs_indice, nuke_attrs, dynamic_obj_attrs)):
            if each_items[0]:
                if i != 2:
                    terms = [True, False]
                else:
                    terms = [False, True]
                all_plugs = []
                for each_index in each_items[0]:
                    plug = cmds.listConnections(nucleus + each_items[1] + '[' + str(each_index) + ']', s=terms[0], d=terms[1], p=True)
                    if plug:
                        all_plugs.extend(plug)
                check_list = [each_plug for each_plug in all_plugs if dynamic_obj in each_plug]
                if not check_list:
                    if i != 2:
                        cmds.connectAttr(dynamic_obj + each_items[2], nucleus + each_items[1] + '[' + str(len(each_items[0])) + ']', f=True)
                    else:
                        cmds.connectAttr(nucleus + each_items[1] + '[' + str(len(each_items[0])) + ']', dynamic_obj + each_items[2], f=True)

            else:
                if i != 2:
                    cmds.connectAttr(dynamic_obj + each_items[2], nucleus + each_items[1] + '[0]', f=True)
                else:
                    cmds.connectAttr(nucleus + each_items[1] + '[0]', dynamic_obj + each_items[2], f=True)

        start_frame_plug = cmds.listConnections(dynamic_obj + '.startFrame', s=True, d=False, p=True)
        if start_frame_plug:
            for each_plug in start_frame_plug:
                if dynamic_obj in each_plug:
                    cmds.disconnectAttr(each_plug, dynamic_obj + '.startFrame')
                    cmds.connectAttr(nucleus + '.startFrame', dynamic_obj + '.startFrame', f=True)
        else:
            cmds.connectAttr(nucleus + '.startFrame', dynamic_obj + '.startFrame', f=True)

    @classmethod
    def closest_vert_on_mesh(cls, base_mesh, target_obj):
        if cmds.objectType(base_mesh, i='mesh'):
            vert_count = cmds.polyEvaluate(base_mesh, v=True)
            target_trans = cmds.xform(target_obj, q=True, ws=True, t=True)
            for x in xrange(vert_count):
                vert_trans = cmds.xform(base_mesh + '.vtx[' + str(x) + ']', q=True, ws=True, t=True)
                dist_list = [cls.distance(each_pair[0], each_pair[1]) for each_pair in zip(vert_trans, target_trans)]
                dist = sum(dist_list)
                if x == 0:
                    closest = dist
                    closest_vert = base_mesh + '.vtx[' + str(x) + ']'
                else:
                    if dist < closest:
                        closest = dist
                        closest_vert = base_mesh + '.vtx[' + str(x) + ']'
            return closest_vert
        else:
            cmds.warning(base_mesh + 'is not a "mesh" object.')
            return

    @classmethod
    def closest_vert_on_mesh_sel(cls):
        sel = cls.get_selection()
        if len(sel) == 2:
            closest_vert = cls.closest_vert_on_mesh(sel[0], sel[1])
            return closest_vert
        else:
            cmds.warning('You must select two objects, base mesh and then target object')
            return

    @classmethod
    def get_vertex_UV_coordinates(cls, vert):
        '''This method will calculate the UV coordinates base on the world space position of the given vert.'''
        vertex_UV = cmds.polyListComponentConversion(vert, fv=True, tuv=True)[0]
        uv_coor = cmds.polyEditUV(vertex_UV, q=True, r=False)
        return uv_coor

    @staticmethod
    def sort_object_type(objs, type='All'):
        '''By default, this method will spit out a dictionary, unless "type" is specified.'''
        if isinstance(objs, (str, list)):
            type_dict = {}
            if isinstance(objs, str):
                objs = [objs]
            for obj in objs:
                obj = str(obj)
                obj_shape = cmds.listRelatives(obj, s=True, ni=True)
                if obj_shape:
                    obj_shape = obj_shape[0]
                    obj_type = str(cmds.objectType(obj_shape))
                    if obj_type not in type_dict.keys():
                        type_dict[obj_type] = [obj]
                    else:
                        type_dict[obj_type].append(obj)

                else:
                    continue
            if type == 'All':
                return type_dict
            else:
                if type in type_dict.keys():
                    return type_dict[type]
                else:
                    return []
        else:
            cmds.warning('"objs" must be a list or a string.')
            return

    @staticmethod
    def get_all_parents(obj):
        '''This method will output a list of all parents of the given object'''
        parent = cmds.listRelatives(obj, p=True)
        if parent:
            all_parents = parent
            while True:
                parent = cmds.listRelatives(parent, p=True)
                if not parent:
                    break
                else:
                    all_parents.extend(parent)
            return all_parents
        else:
            return
