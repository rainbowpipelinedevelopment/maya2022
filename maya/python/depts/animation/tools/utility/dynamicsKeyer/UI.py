from maya import cmds


class ProgressWindow(object):

    def __init__(self, max_value=100, title='Progress', status=''):
        self._win_title = title
        self.__MAX_VALUE = max_value
        self.value = 0.0
        self.status = status
        cmds.progressWindow(t=self._win_title, progress=self.value, isInterruptable=True, maxValue=self.__MAX_VALUE, status=status)

    def update(self, ammount=1.0):
        if cmds.progressWindow(q=True, isCancelled=True):
            self.kill(True)
        self.value += ammount
        cmds.progressWindow(e=True, progress=self.value)
        if self.value >= self.__MAX_VALUE:
            self.kill()

    def set_progress(self, value):
        self.value = value
        self.update(0)

    def set_status(self, stat):
        self.status = stat
        cmds.progressWindow(e=True, status=stat)

    def kill(self, interrupt=False):
        cmds.progressWindow(e=True, endProgress=True)
        if interrupt:
            raise Exception('User canceled process.')


class ShelfButtonMenu(object):

    def __init__(self, shelf_name, button_name):
        self.shelf = self._get_shelf_path(shelf_name)
        self.button = self._get_button_path(button_name, shelf=self.shelf)
        self.menu_name = self.button.split('|')[-1] + '_menu'
        self.menu = self._get_menu()
        self.menu_items = cmds.popupMenu(self.menu, q=True, itemArray=True)

    def _get_menu(self):
        menu_count = cmds.shelfButton(self.button, q=True, npm=True)
        if menu_count > 1:
            return cmds.shelfButton(self.button, q=True, pma=True)[1]
        else:
            return cmds.popupMenu(self.menu_name, button=1, p=self.button)

    def _get_shelf_path(self, name):
        if not cmds.shelfLayout(name, q=True, ex=True):
            raise Exception('Shelf {0} does not exist.'.format(name))
        parent = cmds.shelfLayout(name, q=True, parent=True)
        return '|'.join([parent, name])

    def _get_button_path(self, name, shelf):
        if not cmds.shelfButton(name, q=True, ex=True):
            raise Exception('Shelf Button {0} does not exist.'.format(name))
        return '|'.join([shelf, name])

    def remove_item(self, name):
        self.menu_items = cmds.popupMenu(self.menu, q=True, itemArray=True)
        if self.menu_items:
            if name in self.menu_items:
                cmds.deleteUI('|'.join([self.menu, name]))

    def add_item(self, name, func):
        if self.menu_items is None:
            self.menu_items = []
        if name in self.menu_items:
            self.remove_item(name)
        cmds.menuItem(name, label=name, c=func, p=self.menu)
        self.menu_items = cmds.popupMenu(self.menu, q=True, itemArray=True)

    def add_divider(self):
        divider_name = 'divider'
        if self.menu_items is None:
            self.menu_items = []
        index = len([x for x in self.menu_items if divider_name in x]) + 1
        cmds.menuItem('{0}{1}'.format(divider_name, index), divider=True, p=self.menu)
        self.menu_items = cmds.popupMenu(self.menu, q=True, itemArray=True)


class Dialog(object):

    def __init__(self, title='dialog', type=0):
        self.title = title
        if type == 0:
            self.button = 'OK'
        elif type == 1:
            self.button = ['OK', 'Cancel']
        elif type == 2:
            self.button = ['OK', 'Skip', 'Cancel']
        elif type == 3:
            self.button = ['OK', 'Skip', 'Abort', 'Cancel']

    def prompt(self, message='enter message:', default_txt=''):
        result = cmds.promptDialog(t=self.title, m=message, tx=default_txt, b=self.button, db='OK', cb='Cancel', ds='Cancel')
        if result == 'OK':
            txt = cmds.promptDialog(q=True, tx=True)
            return txt
        else:
            return

    def confirm(self, message='enter message:'):
        result = cmds.confirmDialog(t=self.title, m=message, b=self.button, db='OK', cb='Cancel', ds='Cancel')
        return result
