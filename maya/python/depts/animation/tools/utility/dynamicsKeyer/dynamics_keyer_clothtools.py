from maya import cmds, mel
from depts.animation.tools.utility.dynamicsKeyer.dynamics_keyer_utils import DK_Utils
import depts.animation.tools.utility.dynamicsKeyer.UI as UI


class DK_ClothTools:

    def __init__(self):
        self.nucleus = 'Dynamics_Keyer_Nucleus'

    def construct_setup(self, sel, progress_window, cb_state=False):
        if not progress_window:
            progress_window = UI.ProgressWindow(max_value=100, title='Building Setup', status='Constructing Cloth Setup...')
        input_meshes = DK_Utils.sort_object_type(sel, type='mesh')
        progress_window.update(10)
        input_controls = DK_Utils.sort_object_type(sel, type='nurbsCurve') + DK_Utils.sort_object_type(sel, type='nurbsSurface')
        progress_window.update(10)
        self.setup_name = input_meshes[0].replace('|', '_')
        progress_window.update(10)
        cloth_mesh = self.create_cloth_mesh(input_meshes)
        progress_window.update(10)
        if cb_state:
            cache_parts = self.pre_bake(input_meshes, cloth_mesh)
            progress_window.update(10)
        else:
            cache_parts = None
            progress_window.update(10)
        cloth_node = self.create_cloth(cloth_mesh)
        progress_window.update(10)
        self.create_collision_display(cloth_mesh, cloth_node)
        progress_window.update(10)
        follicles = [self.create_follicle(control, cloth_mesh) for control in input_controls]
        progress_window.update(10)
        constraints = [cmds.pointConstraint(pair[0], pair[1], mo=True)[0] for pair in zip(follicles, input_controls)]
        progress_window.update(10)
        # TAG SETUP GROUP
        self.tag_setup(input_controls, input_meshes, cloth_node, constraints, cache_parts)
        progress_window.update(10)
        return cloth_mesh

    def tag_setup(self, anim_ctrls, input_meshes, cloth_node, constraints, cache):
        cmds.addAttr(self.setup_name + '_Dynamics_GRP', ln='isDynamicsKeyer_Cloth', at='message')
        anim_ctrls_SA = ''.join('"' + '", "'.join(anim_ctrls) + '"')
        cmds.addAttr(self.setup_name + '_Dynamics_GRP', ln='animControls', dt='stringArray')
        eval("cmds.setAttr(self.setup_name + '_Dynamics_GRP.animControls', {0}, {1}, type='stringArray')".format(len(anim_ctrls), anim_ctrls_SA))
        input_meshes_SA = ''.join('"' + '", "'.join(input_meshes) + '"')
        cmds.addAttr(self.setup_name + '_Dynamics_GRP', ln='inputMeshes', dt='stringArray')
        eval("cmds.setAttr(self.setup_name + '_Dynamics_GRP.inputMeshes', {0}, {1}, type='stringArray')".format(len(input_meshes), input_meshes_SA))
        constraints_SA = ''.join('"' + '", "'.join(constraints) + '"')
        cmds.addAttr(self.setup_name + '_Dynamics_GRP', ln='ctrlConstraints', dt='stringArray')
        eval("cmds.setAttr(self.setup_name + '_Dynamics_GRP.ctrlConstraints', {0}, {1}, type='stringArray')".format(len(constraints), constraints_SA))
        cmds.addAttr(self.setup_name + '_Dynamics_GRP', ln='nCloth', dt="string")
        cmds.setAttr(self.setup_name + '_Dynamics_GRP.nCloth', cloth_node, type="string")
        if cache:
            cache_SA = ''.join('"' + '", "'.join(cache) + '"')
            cmds.addAttr(self.setup_name + '_Dynamics_GRP', ln='cacheParts', dt='stringArray')
            eval("cmds.setAttr(self.setup_name + '_Dynamics_GRP.cacheParts', {0}, {1}, type='stringArray')".format(len(cache), cache_SA))

    def create_cloth_mesh(self, input_meshes):
        if input_meshes:
            if len(input_meshes) >> 1:
                copies = []
                for ea in input_meshes:
                    copies.append(cmds.duplicate(ea, n=ea + '_TEMP_GEO')[0])
                cloth_mesh = cmds.rename(cmds.polyUnite(copies, ch=False), input_meshes[0] + '_NC_MESH')
                cmds.polyLayoutUV(cloth_mesh + '.f[*]', l=2, fr=True, se=2, sc=1, ch=False)
                cmds.delete(copies)
            else:
                cloth_mesh = cmds.duplicate(input_meshes[0], n=input_meshes[0] + '_NC_MESH')[0]
            # TIDY UP
            DK_Utils.organize(cloth_mesh, 'Dynamics_Main_GRP/Cloth_Setups_GRP/' + self.setup_name + '_Dynamics_GRP/' +
                              self.setup_name + '_Geometry_GRP')
            return cloth_mesh
        else:
            cmds.warning('No input mesh provided.')

    def create_collision_display(self, cloth_mesh, cloth_node):
        display_mesh = cmds.duplicate(cloth_mesh, n=cloth_mesh.replace('_NC_MESH', '_COL_DISP_MESH'))[0]
        display_mesh_shapes = cmds.listRelatives(display_mesh, s=True, ni=True, f=True)
        for shape in display_mesh_shapes:
            if 'outputCloth' in shape:
                cmds.delete(shape)
            else:
                cmds.setAttr(shape + '.visibility', 1)
                display_mesh_shape = shape
        cloth_output = ''.join(x for x in cmds.listRelatives(cloth_mesh, s=True, ni=True) if 'outputCloth' in x)
        cmds.connectAttr(cloth_output + '.worldMesh[0]', display_mesh_shape + '.inMesh')
        face_count = cmds.polyEvaluate(display_mesh_shape, f=True)
        cmds.polyExtrudeFacet(display_mesh_shape + '.f[0:' + str(face_count - 1) + ']', kft=True)
        re_count = cmds.polyEvaluate(display_mesh_shape, f=True)
        thickness_extrude = cmds.polyExtrudeFacet(display_mesh_shape + '.f[0:' + str(re_count - 1) + ']', kft=True)[0]
        cmds.connectAttr(cloth_node + '.thickness', thickness_extrude + '.localTranslateZ')
        cmds.connectAttr(cloth_node + '.solverDisplay', display_mesh + '.visibility')
        # ASSIGN SHADER TO COLLISION DISPLAY MESH
        shader = 'dynamics_keyer_cloth_collision_display_blinn'
        if not cmds.objExists(shader):
            shader = cmds.shadingNode('blinn', n=shader, asShader=True)
        cmds.setAttr(shader + '.color', 1.0, 0.9, 0.0, type='double3')
        cmds.setAttr(shader + '.transparency', 0.75, 0.75, 0.75, type='double3')
        SG = shader + 'SG'
        if not cmds.objExists(SG):
            SG = cmds.sets(renderable=True, noSurfaceShader=True, empty=True, name=shader + 'SG')
        plug = cmds.listConnections(SG + '.surfaceShader', s=True, d=False, p=True)
        if plug:
            if shader not in plug[0]:
                cmds.connectAttr(shader + '.outColor', SG + '.surfaceShader', f=True)
        else:
            cmds.connectAttr(shader + '.outColor', SG + '.surfaceShader', f=True)
        cmds.sets(display_mesh_shape, e=True, forceElement=SG)
        cons = cmds.listConnections(display_mesh, p=True)
        for con in cons:
            if '.drawInfo' in con:
                cmds.disconnectAttr(con, display_mesh + '.drawOverride')
        cmds.setAttr(display_mesh + '.overrideEnabled', 1)
        cmds.setAttr(display_mesh + '.overrideDisplayType', 2)
        return display_mesh

    def create_cloth(self, cloth_mesh):
        if cloth_mesh:
            nucleus = DK_Utils.create_nucleus(self.nucleus)
            ncloth_shape = cmds.createNode('nCloth', n=self.setup_name + '_CLOTHShape')
            ncloth_trans = cmds.listRelatives(ncloth_shape, p=True)[0]
            DK_Utils.connect_nucleus(nucleus, ncloth_shape)
            # CONNECTING ATTRIBUTES BETWEEN NCLOTH NODES
            in_mesh = cmds.listRelatives(cloth_mesh, s=True, ni=True)[0]
            cmds.connectAttr(in_mesh + '.worldMesh', ncloth_shape + '.inputMesh')
            out_mesh = cmds.createNode('mesh', p=cloth_mesh, n='outputCloth#')
            cmds.setAttr(in_mesh + '.visibility', 0)
            cmds.connectAttr(ncloth_shape + '.outputMesh', out_mesh + '.inMesh')
            if not cmds.objExists('time1'):
                cmds.createNode('time', n='time#')
            cmds.connectAttr('time1.outTime', ncloth_shape + '.currentTime')
            # SET MAGIC VALUE
            cmds.setAttr(ncloth_shape + '.localSpaceOutput', True)
            # CREATE AND ASSIGN SHADER TO CLOTH MESH
            shader = 'dynamics_keyer_cloth_mesh_blinn'
            if not cmds.objExists(shader):
                shader = cmds.shadingNode('blinn', n=shader, asShader=True)
            cmds.setAttr(shader + '.color', 0.2, 0.4, 0.6, type='double3')
            SG = shader + 'SG'
            if not cmds.objExists(SG):
                SG = cmds.sets(renderable=True, noSurfaceShader=True, empty=True, name=shader + 'SG')
            plug = cmds.listConnections(SG + '.surfaceShader', s=True, d=False, p=True)
            if plug:
                if shader not in plug[0]:
                    cmds.connectAttr(shader + '.outColor', SG + '.surfaceShader', f=True)
            else:
                cmds.connectAttr(shader + '.outColor', SG + '.surfaceShader', f=True)
            cmds.sets(out_mesh, e=True, forceElement=SG)
            # TIDY UP
            path = DK_Utils.organize(ncloth_trans, 'Dynamics_Main_GRP/Cloth_Setups_GRP/' + self.setup_name + '_Dynamics_GRP/' +
                                     self.setup_name + '_Solver_GRP')
            cmds.setAttr(path + '.visibility', 0)
            return ncloth_shape
        else:
            cmds.warning('No cloth mesh provided.')

    def create_follicle(self, target, base):
        base_shape = ''.join(shape for shape in cmds.listRelatives(base, s=True, ni=True, pa=True) if 'outputCloth' in shape)
        closest_vert = DK_Utils.closest_vert_on_mesh(base_shape, target)
        uv_coor = DK_Utils.get_vertex_UV_coordinates(closest_vert)
        follicle_shape = cmds.createNode('follicle', n=target + '_FOLLICLEShape')
        follicle_trans = cmds.listRelatives(follicle_shape, p=True)[0]
        cmds.setAttr(follicle_shape + '.parameterU', uv_coor[0])
        cmds.setAttr(follicle_shape + '.parameterV', uv_coor[1])
        for attr_pair in zip(['.worldMesh[0]', '.worldMatrix[0]'], ['.inputMesh', '.inputWorldMatrix']):
            cmds.connectAttr(base_shape + attr_pair[0], follicle_shape + attr_pair[1])
        for attr_pair in zip(['.outRotate', '.outTranslate'], ['.rotate', '.translate']):
            cmds.connectAttr(follicle_shape + attr_pair[0], follicle_trans + attr_pair[1])
        path = DK_Utils.organize(follicle_trans, 'Dynamics_Main_GRP/Cloth_Setups_GRP/' + self.setup_name + '_Dynamics_GRP/' +
                                 self.setup_name + '_Misc_GRP/' + self.setup_name + '_Follicles_GRP')
        cmds.setAttr(path + '.visibility', 0)
        return follicle_trans

    def pre_bake(self, input_meshes, cloth_mesh):
        if input_meshes:
            start_frame = cmds.playbackOptions(q=True, min=True)
            end_frame = cmds.playbackOptions(q=True, max=True)
            if len(input_meshes) >> 1:
                copies = []
                blendshapes = []
                for ea in input_meshes:
                    cache_copy = cmds.duplicate(ea, n=ea + '_TEMP_GEO')[0]
                    blendshape = cmds.blendShape(ea, cache_copy, w=[0, 1])[0]
                    copies.append(cache_copy)
                    blendshapes.append(blendshape)
                    cmds.setAttr(cache_copy + '.visibility', 0)
                DK_Utils.organize(copies, 'Dynamics_Main_GRP/Cloth_Setups_GRP/' + self.setup_name + '_Dynamics_GRP/' +
                                  self.setup_name + '_Geometry_GRP/' + self.setup_name + '_Cache_GRP')
                cache_mesh = cmds.polyUnite(copies, n=input_meshes[0] + '_CACHE_MESH', ch=True)[0]
            else:
                cache_mesh = cmds.duplicate(input_meshes[0], n=input_meshes[0] + '_CACHE_MESH')[0]
            DK_Utils.organize(cache_mesh, 'Dynamics_Main_GRP/Cloth_Setups_GRP/' + self.setup_name + '_Dynamics_GRP/' +
                              self.setup_name + '_Geometry_GRP/' + self.setup_name + '_Cache_GRP')
            cache_mesh_shape = cmds.listRelatives(cache_mesh, s=True, ni=True)[0]
            cache_blendshape = cmds.blendShape(cache_mesh, cloth_mesh, w=[0, 1], n=input_meshes[0] + '_INPUT_ATTRACT_BS')
            cache_file = cmds.cacheFile(pts=cache_mesh_shape, f='input_animation_temp_cache', sch=True, fm='OneFile',
                                        ws=True, st=start_frame, et=end_frame)
            switch = cmds.rename(mel.eval('createHistorySwitch("%s",false)' % cache_mesh_shape), 'input_cache_switch')
            cache_node = cmds.cacheFile(f=cache_file[0], cnm=cache_mesh_shape, ia=switch + '.inp[0]', af=True)
            cmds.setAttr(switch + '.playFromCache', 1)
            cmds.delete(blendshapes)
            cache_parts = []
            for each in [cache_mesh, cache_node, cache_blendshape]:
                if each:
                    cache_parts.extend(each)
            if cache_parts:
                return cache_parts
            else:
                return
        else:
            cmds.warning('No input mesh provided.')
            return

    def set_default_values(self, ncloth):
        cmds.setAttr(ncloth + '.stretchResistance', 120)
        cmds.setAttr(ncloth + '.compressionResistance', 80)
        cmds.setAttr(ncloth + '.bendResistance', 0.05)
        cmds.setAttr(ncloth + '.damp', 0.05)
        cmds.setAttr(ncloth + '.drag', 0.05)
        cmds.setAttr(ncloth + '.inputMeshAttract', 0)
        cmds.setAttr(ncloth + '.inputAttractDamp', 0.001)
