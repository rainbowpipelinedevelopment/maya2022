import os
import importlib
import math

import maya.cmds as cmd

from maya.api import OpenMaya

import api.log
import api.scene
import api.widgets.rbw_UI as rbwUI

# importlib.reload(rbwUI)
importlib.reload(api.scene)
importlib.reload(api.log)

TANGENTS = ["auto", "clamped", "fast", "flat", "linear", "plateau", "slow", "spline", "stepnext"]
ATTRIBUTES = ["translate", "rotate", "scale"]
CHANNELS = ["X", "Y", "Z"]

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'arakno')


class AnchorTransformUI(rbwUI.RBWWindow):
    def __init__(self, *args, **kwargs):
        super(AnchorTransformUI, self).__init__(*args, **kwargs)

        if cmd.window('AnchorTransformUI', exists=True):
            cmd.deleteUI('AnchorTransformUI')

        self.setObjectName('AnchorTransformUI')
        self.initUI()
        self.updateMargins()

    def initUI(self):
        self.setMain(True)
        self.setStyle()

        frame = rbwUI.RBWFrame('V')

        self.driverEdit = rbwUI.RBWLineEdit('Driver:')
        self.driverEdit.setText('World')
        self.startEdit = rbwUI.RBWLineEdit('Start frame:')
        self.startEdit.setEnabled(False)
        self.endEdit = rbwUI.RBWLineEdit('End frame:')
        self.endEdit.setEnabled(False)
        self.timelineCheck = rbwUI.RBWCheckBox('From time control selection')
        self.timelineCheck.setChecked(True)
        self.timelineCheck.stateChanged.connect(self.setManualInputField)

        buttonsBox = rbwUI.RBWFrame('H', margins=[0, 0, 0, 0], bgColor='transparent')
        self.driverButton = rbwUI.RBWButton('Set Driver from selection', size=[None, 30])
        self.driverButton.clicked.connect(self.setDriver)
        self.anchorButton = rbwUI.RBWButton('Anchor selected transforms', size=[None, 30])
        self.anchorButton.clicked.connect(self.doAnchor)
        buttonsBox.addWidget(self.driverButton)
        buttonsBox.addWidget(self.anchorButton)

        frame.addWidget(self.driverEdit)
        frame.addWidget(self.startEdit)
        frame.addWidget(self.endEdit)
        frame.addWidget(self.timelineCheck)
        frame.addWidget(buttonsBox)

        self.mainLayout.addWidget(frame)

        self.setMinimumSize(360, 200)
        self.setTitle('Anchor transform')
        self.setIcon('../animation/anchor.png')
        self.setFocus()

    def setDriver(self):
        self.driverEdit.setText(cmd.ls(sl=True)[0] or [""])

    def setManualInputField(self, state):
        self.startEdit.setEnabled(not state)
        self.endEdit.setEnabled(not state)

    def getFrameRangeFromTimeControl(self):
        rangeVisible = cmd.timeControl(api.scene.getTimeline(), q=True, rangeVisible=True)

        if not rangeVisible:
            return

        r = cmd.timeControl(api.scene.getTimeline(), query=True, ra=True)
        return [int(r[0]), int(r[-1])]

    def getFrameRangeFromUI(self):
        if int(self.startEdit.text()) >= int(self.endEdit.text()):
            return

        return [int(self.startEdit.text()), int(self.endEdit.text())]

    def getFrameRange(self):
        if self.timelineCheck.isChecked():
            return self.getFrameRangeFromTimeControl()
        else:
            return self.getFrameRangeFromUI()

    def doAnchor(self):
        frameRange = self.getFrameRange()
        if not frameRange:
            raise ValueError("No valid frame range could be found!")

        anchorSelection(self.driverEdit.text() if cmd.objExists(self.driverEdit.text()) else None, *frameRange)


class UndoChunkContext(object):
    def __enter__(self):
        cmd.undoInfo(openChunk=True)

    def __exit__(self, *exc_info):
        cmd.undoInfo(closeChunk=True)


def anchorSelection(driver, start, end):
    invalidChannels = []

    # get selection
    transforms = cmd.ls(sl=True, transforms=True) or []

    # make sure the driver is not inside the transforms list
    transforms = [t for t in transforms if not t == driver]

    # check for invalid selection
    for transform in transforms:
        invalidChannels.extend(getInvalidAttributes(transform))

    if invalidChannels:
        if not rbwUI.RBWWarning(text="The following invalid attributes where found\nand will be ignored!\n\n{}\n\nWould you like to continue?".format("\n".join(invalidChannels))).result():
            return

    # anchor transforms
    for transform in transforms:
        anchorTransform(transform, driver, start, end)


def anchorTransform(transform, driver, start, end):
    with UndoChunkContext():
        # get parent
        rotOrder = cmd.getAttr("{0}.rotateOrder".format(transform))

        # get driver matrix
        driverInverseMatrix = getMatrix(driver, start, "worldInverseMatrix")

        # get start matrix
        anchorMatrix = getMatrix(transform, start, "worldMatrix")

        # get invalid attributes
        invalidAttributes = getInvalidAttributes(transform)

        # key frame attributes
        for i in range(start, end):
            # get driver and transform matrices
            driverMatrix = getMatrix(driver, i, "worldMatrix")
            inverseMatrix = getMatrix(transform, i, "parentInverseMatrix")

            # get driver matrix difference
            differenceMatrix = driverInverseMatrix * driverMatrix

            # get local matrix
            localMatrix = differenceMatrix * anchorMatrix * inverseMatrix

            # extract transform values from matrix
            rotPivot = cmd.getAttr("{0}.rotatePivot".format(transform))[0]
            transformValues = decomposeMatrix(localMatrix, rotOrder, rotPivot)

            for attr, value in zip(ATTRIBUTES, transformValues):
                for j, channel in enumerate(CHANNELS):
                    # variables
                    node = "{0}.{1}{2}".format(transform, attr, channel)
                    tangents = {
                        "inTangentType": "linear",
                        "outTangentType": "linear"
                    }

                    # skip if its an invalid attribute
                    if node in invalidAttributes:
                        continue

                    # check if input connections are
                    animInputs = cmd.listConnections(node, type="animCurve", destination=False)

                    # adjust tangents
                    if animInputs and i == end:
                        tangent = getOutTangent(animInputs[0], end)

                        if tangent in TANGENTS:
                            tangents["outTangentType"] = tangent

                    elif animInputs and i == start:
                        tangent = getInTangent(animInputs[0], start)

                        if tangent in TANGENTS:
                            tangents["inTangentType"] = tangent

                    # set key frame
                    cmd.setKeyframe(node, t=i, v=value[j], **tangents)

        # apply euler filter
        applyEulerFilter(transform)


def getInvalidAttributes(transform):
    invalidChannels = []
    for attr in ATTRIBUTES:
        # get connection of parent attribute
        node = "{0}.{1}".format(transform, attr)
        if cmd.listConnections(node, destination=False):
            invalidChannels.extend([node + channel for channel in CHANNELS])
            continue

        # get connection of individual channels
        for channel in CHANNELS:
            node = "{0}.{1}{2}".format(transform, attr, channel)

            # get connections
            animInputs = cmd.listConnections(node, type="animCurve", destination=False)
            allInputs = cmd.listConnections(node, destination=False)

            # check if connections are not of anim curve type
            locked = cmd.getAttr(node, lock=True)
            if (not animInputs and allInputs) or locked:
                invalidChannels.append(node)

    return invalidChannels


def getMatrix(transform, time=None, matrixType="worldMatrix"):
    if not transform:
        return OpenMaya.MMatrix()

    if not time:
        time = cmd.currentTime(query=True)

    matrix = cmd.getAttr("{0}.{1}".format(transform, matrixType), time=time)
    return OpenMaya.MMatrix(matrix)


def decomposeMatrix(matrix, rotOrder, rotPivot):
    matrixTransform = OpenMaya.MTransformationMatrix(matrix)

    # set pivots
    matrixTransform.setRotatePivot(OpenMaya.MPoint(rotPivot), OpenMaya.MSpace.kTransform, True)

    # get rotation pivot translation
    posOffset = matrixTransform.rotatePivotTranslation(OpenMaya.MSpace.kTransform)

    # get pos values
    pos = matrixTransform.translation(OpenMaya.MSpace.kTransform)
    pos += posOffset
    pos = [pos.x, pos.y, pos.z]

    # get rot values
    euler = matrixTransform.rotation()
    euler.reorderIt(rotOrder)
    rot = [math.degrees(angle) for angle in [euler.x, euler.y, euler.z]]

    # get scale values
    scale = matrixTransform.scale(OpenMaya.MSpace.kTransform)

    return [pos, rot, scale]


def getInTangent(animCurve, time):
    times = cmd.keyframe(animCurve, query=True, timeChange=True) or []
    for t in times:
        if t <= time:
            continue

        tangent = cmd.keyTangent(animCurve, time=(t, t), query=True, inTangentType=True)

        return tangent[0]

    return "auto"


def getOutTangent(animCurve, time):
    times = cmd.keyframe(animCurve, query=True, timeChange=True) or []
    for t in times:
        if t >= time:
            continue

        tangent = cmd.keyTangent(animCurve, time=(t, t), query=True, outTangentType=True)

        return tangent[0]

    return "auto"


def applyEulerFilter(transform):
    rotationCurves = []
    for channel in CHANNELS:
        # variables
        node = "{0}.rotate{1}".format(transform, channel)

        # get connected animation curve
        rotationCurves.extend(cmd.listConnections(node, type="animCurve", destination=False) or [])

    # apply euler filter
    if rotationCurves:
        cmd.filterCurve(*rotationCurves, filter="euler")
