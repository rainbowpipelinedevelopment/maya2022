import maya.cmds as cmd

import api.log


def attackTrainToRail():
    inputSets = cmd.ls('*:MP_Rails')
    destSets = cmd.ls('*:MP_Train')

    sourceArray = []
    destArray = []

    if len(inputSets) == 0:
        cmd.confirmDialog(title='ATTENTION', icon='critical', message='No Rail prop or no MP_Rails set in scene.', button=['OK'])

    if len(destSets) == 0:
        cmd.confirmDialog(title='ATTENTION', icon='critical', message='No Train prop or no MP_Train set in scene.', button=['OK'])

    if len(inputSets) > 1 and len(destSets) > 1:
        cmd.confirmDialog(title='ATTENTION', icon='critical', message='More than one train or rail prop in scene.', button=['OK'])

    else:
        railSet = inputSets[0]
        trainSet = destSets[0]
        cmd.select(railSet, replace=True)
        joints = cmd.ls(selection=True)
        cmd.select(clear=True)

        trainNamespace = trainSet.split(':')[0]

        for jointNode in joints:
            joint = jointNode.split(':')[1]
            jointInfo = joint.split('_')
            if len(jointInfo) == 3:
                control = '{}:CNT_{}_{}_MotionPath'.format(trainNamespace, jointInfo[1], jointInfo[2])
            else:
                control = '{}:GRP_CNT_{}_Offset'.format(trainNamespace, jointInfo[1])

            if cmd.objExists(control):
                sourceArray.append(jointNode)
                destArray.append(control)

        if len(sourceArray) == len(destArray):
            for i in range(0, len(sourceArray)):
                cmd.parentConstraint(sourceArray[i], destArray[i], maintainOffset=0)
        else:
            cmd.confirmDialog(title='ATTENTION', icon='critical', message='Source and destination objects are not the same number.\nThe costraint are skipped.', button=['OK'])
