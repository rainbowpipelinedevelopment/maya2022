import os
import importlib
import functools
import maya.cmds as cmd

import api.database
import api.scene
import api.assembly
import api.widgets.rbw_UI as rbwUI
import api.log

importlib.reload(api.database)
importlib.reload(api.scene)
# importlib.reload(api.assembly)
# importlib.reload(rbwUI)
importlib.reload(api.log)


################################################################################
# @brief      creates a mirror camera.
#
def addMirrorCamera():
    cmd.select("*:Cam_World_Ctrl", r=1)
    copyCam = cmd.duplicate(rr=1)
    cmd.parent(copyCam, w=1)
    number = 1
    howMany = []
    try:
        cmd.select("MIRROR_CAM*")
        howMany = cmd.ls(sl=1)
    except:
        pass

    if howMany:
        number = len(howMany) / 2
        rbwUI.RBWError(text="There are already other mirror cameras..")

    cmd.rename(copyCam, "MIRROR_CAM_{}".format(str(number)))
    rbwUI.RBWDialog(text="The mirror camera has been created")


################################################################################
# @brief      Switch between reference and instance.
#
def referenceToggle():
    selections = cmd.ls(sl=True, long=True)
    if not selections:
        rbwUI.RBWError(text='You should select something first!')
        return

    parentGroup = '|'.join(selections[0].split('|')[0:2])
    paletteGrp = "{}_GRP_palette".format(parentGroup)
    instanceGrp = "{}_GRP_instances".format(parentGroup)

    outObjs = []
    for selected in selections:
        refFile = cmd.referenceQuery(selected, f=1)
        if refFile.count("set_") and refFile.count("_lay_"):
            rbwUI.RBWError(text='The selection is not appropriate to use this tool')
            return

        if refFile:
            cmd.select(selected, r=1)
            mtx = cmd.xform(selected, q=1, m=1, ws=1)
            masterRef = cmd.listConnections('{}.creationDate'.format(selected))
            currSelection = None
            isDest = cmd.connectionInfo('{}.creationDate'.format(selected), isDestination=True)
            cmd.select(masterRef[0] if isDest else selected)
            createInstanceFromPalette(mtx, parentGroup, paletteGrp, instanceGrp)
            currSelection = cmd.ls(sl=1, l=1)

            if isDest:
                cmd.file(refFile, rr=True)

            if currSelection:
                cmd.select(currSelection, r=1)
        else:
            absPaletteGroup = '{}{}_GRP_palette'.format(parentGroup, parentGroup)
            mtx = cmd.xform(selected, q=1, m=1, ws=1)

            ref = cmd.listConnections('{}.creationDate'.format(selected))[0]
            cmd.select(ref)
            refFile = cmd.referenceQuery(ref, f=1)
            newNamespace = cmd.file(refFile, q=1, namespace=1)
            newRef = cmd.file(refFile, r=True, namespace=newNamespace)

            cmd.file(newRef, sa=1)
            refObj = cmd.ls(sl=1, l=1)[0]
            cmd.xform(refObj, m=mtx, ws=1)
            cmd.connectAttr('{}.creationDate'.format(ref), '{}.creationDate'.format(refObj), f=1)
            cmd.parent(refObj, absPaletteGroup)
            cmd.delete(selected)
            try:
                shape = cmd.listRelatives(refObj, c=1, s=1)[0]
                cmd.setAttr("{}.shapeType".format(shape), 0)
            except:
                pass

            cmd.parent(cmd.ls(sl=1, l=1)[0], '|'.join(cmd.ls(sl=1, l=1)[0].split('|')[0:2]))

        currObj = cmd.ls(sl=1, l=1)
        if (currObj):
            outObjs.append(currObj[0])

    absPaletteGroup = '{}{}'.format('|'.join(selections[0].split('|')[0:2]), paletteGrp)
    absInstGroup = '{}{}'.format('|'.join(selections[0].split('|')[0:2]), instanceGrp)

    childPalette = cmd.listRelatives(absPaletteGroup, c=1)
    if not childPalette:
        cmd.delete(absPaletteGroup)

    childInst = cmd.listRelatives(absInstGroup, c=1)
    if not childInst:
        cmd.delete(absInstGroup)

    if childPalette and not childInst:
        for child in childPalette:
            refFile = cmd.referenceQuery(child, f=1)
            cmd.file(refFile, rr=True)
        cmd.delete(absPaletteGroup)

    cmd.select(outObjs, r=1)


################################################################################
# @brief      create instance.
#
# @param      selected  selected item
# @param      parentGroup  parent group name
# @param      paletteGrp  palette group name
# @param      instanceGrp  instance group name
# @param      mtx  cmd.xform() result
#
def createInstanceFromPalette(selected, parentGroup, paletteGrp, instanceGrp, mtx=None):
    absPaletteGroup = '{}{}'.format(parentGroup, paletteGrp)
    absPath = '|'.join(cmd.ls(sl=1, l=1)[0].split('|')[:-1])
    absPathFull = absPath
    outPath = None

    if (len(cmd.ls(sl=1)) > 1):
        outPath = '|'.join(cmd.ls(sl=1, l=1)[1].split('|')[:-1])

    moveToPaletteGroup = False
    if absPath.count('GRP_palette') > 0:
        absPath = '|'.join(absPath.split('|')[:-1])
    else:
        moveToPaletteGroup = True

    removeRef = None
    # check if in the palette group there is already a reference to the same file and link against this one
    if moveToPaletteGroup:
        if cmd.objExists(absPaletteGroup):
            tmpRefFile = cmd.referenceQuery('{}|{}'.format(absPathFull, selected), f=1)
            refFile = tmpRefFile.split('{')[0]
            childrens = cmd.listRelatives(absPaletteGroup, c=1, type="transform")
            if childrens:
                for pal in childrens:
                    currRefFile = cmd.referenceQuery('{}|{}'.format(absPaletteGroup, pal), f=1).split('{')[0]
                    if (refFile == currRefFile):
                        # find already an instance with this file, link against this one
                        absPathFull += paletteGrp
                        absPath = '|'.join(absPathFull.split('|')[:-1])
                        selected = pal
                        moveToPaletteGroup = False
                        removeRef = tmpRefFile
                        break

    if moveToPaletteGroup:
        for n in cmd.listAnimatable(selected):
            conns = cmd.listConnections(n)
            if conns:
                cmd.delete(conns[0])

    # check if there is a GRP_instances group
    inst = cmd.instance(selected, lf=False, st=False)[0]
    if not cmd.objExists('{}{}'.format(absPath, instanceGrp)):
        grp = cmd.group(n=instanceGrp, em=1)
        if absPath:
            cmd.parent(grp, absPath)

    objs = cmd.listRelatives(absPath + instanceGrp, c=1)
    if outPath:
        objs = cmd.listRelatives(outPath, c=1)
    i = 1
    if objs:
        for obj in objs:
            if (obj.count(selected) > 0):
                idx = obj.find(selected)
                if idx != -1:
                    try:
                        n = int(obj[idx + len(selected):])
                        if n > i:
                            i = n + 1
                        else:
                            i = i + 1
                    except:
                        i = i + 1

    newObj = cmd.rename(inst, '{}{}'.format(selected, str(i)))
    newObjPath = newObj
    cmd.connectAttr('{}.creationDate'.format(selected), '{}.creationDate'.format(newObj), f=1)
    if (mtx):
        cmd.xform(newObj, m=mtx, ws=1)

    if (outPath):
        cmd.parent(newObj, outPath)
        newObjPath = '{}|{}'.format(outPath, newObj)
    else:
        cmd.parent("{}|{}".format(absPathFull, newObj), '{}{}'.format(absPath, instanceGrp))
        newObjPath = '{}{}|{}'.format(absPath, instanceGrp, newObj)

    if moveToPaletteGroup:
        if not cmd.objExists(absPaletteGroup):
            grp = cmd.group(n=paletteGrp, em=1)
            cmd.setAttr("{}.visibility".format(grp), 0)
            cmd.parent(grp, parentGroup)
        cmd.xform('{}|{}'.format(absPath, selected), ws=1, m=[1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1])

        # change rainbow locator icon
        try:
            shape = cmd.listRelatives('{}|{}'.format(absPath, selected), c=1, s=1)[0]
            cmd.setAttr("{}.shapeType".format(shape), 3)
        except:
            pass
        cmd.parent('{}|{}'.format(absPath, selected), absPaletteGroup)

    if removeRef:
        cmd.file(removeRef, rr=True)

    cmd.select(newObjPath, r=1)


################################################################################
# @brief      kills the selected asset.
#
def killAsset():
    sel = cmd.ls(sl=1)
    if sel:
        items = sel
        removes = []
        badSelections = []
        if rbwUI.RBWWarning(text="Do you really want to delete?:\n - {}".format('\n - '.join(items)), resizable=True).result():
            for item in items:
                try:
                    if item.count(':'):
                        selectedNamespace = item.split(':')[0]
                        rigSaveNode = '{}:*_saveNode'.format(selectedNamespace)
                        if cmd.objExists(rigSaveNode):
                            ref = cmd.referenceQuery(item, f=1)
                            cmd.file(ref, rr=1)
                            forsterParents = cmd.ls('{}*'.format(selectedNamespace), type='fosterParent')
                            if forsterParents:
                                cmd.delete(forsterParents)
                            removes.append(item)

                            [cmd.delete(group) for group in ['CHARACTER', 'PROP', 'LIBRARY'] if cmd.objExists(group) and not cmd.listRelatives(group)]
                        else:
                            badSelections.append(item)
                    else:
                        if cmd.listRelatives(item, parent=True)[0] == 'SET':
                            cmd.delete(item)
                            removes.append(item)

                            if not cmd.listRelatives('SET'):
                                if cmd.objExists('SET_RIG_annotations'):
                                    cmd.delete('SET_RIG_annotations')

                                cmd.delete('SET')
                        else:
                            badSelections.append(item)
                except TypeError:
                    badSelections.append(item)

        if len(removes) > 0 and not len(badSelections):
            rbwUI.RBWDialog(text='Assets Removed:\n - {}'.format('\n - '.join(removes)), resizable=True)
        elif len(removes) > 0:
            rbwUI.RBWWarning(text='Assets Removed:\n - {}\n\nThose selected objects are not referenced or set:\n - {}'.format('\n - '.join(removes), '\n - '.join(badSelections)), defCancel=None, resizable=True)
        elif len(badSelections) > 0:
            rbwUI.RBWError(text='Those selected objects are not referenced or set:\n - {}\n\nOperation aborted'.format('\n - '.join(badSelections)), resizable=True)
    else:
        rbwUI.RBWError(text="You should select something first!")


################################################################################
# @brief      checks the selection and run the right 'give me' function.
#
def checkAvailableStuff():
    selectionList = cmd.ls(selection=True)
    for sel in selectionList:
        if cmd.objectType(sel) == 'assemblyReference':
            paths = {}
            assemblyObj = api.assembly.Assembly(node=sel)
            for deptInfo in [['set', 'hig'], ['fx', 'ani'], ['rigging', 'hig']]:
                savedAssetParams = {'asset': assemblyObj.getAsset(), 'approvation': 'def', 'dept': deptInfo[0], 'deptType': deptInfo[1]}
                savedAsset = api.savedAsset.SavedAsset(params=savedAssetParams)
                latest = savedAsset.getLatest()
                if latest:
                    paths[deptInfo[0]] = api.savedAsset.SavedAsset(params={'db_id': latest[0], 'approvation': 'def'})

            if len(paths.keys()) == 1:
                api.log.logger().debug("There is only one category of asset, the procedure will go for it...")
                key = [x for x in paths.keys()][0]
                if key == 'set':
                    giveMeSet(sel, paths['set'])
                else:
                    giveMeInReference(sel, paths[key])

            elif len(paths.keys()) > 1:
                buttons = []
                for dept in paths.keys():
                    if dept == 'set':
                        buttons.append(['set', '', giveMeSet(sel, paths[dept])])
                    else:
                        buttons.append([dept, '', giveMeInReference(sel, paths[dept])])

                rbwUI.RBWConfirm(text="There are more than 1 option to import,\n what do you want to load?", defOk=False, buttons=buttons).result()

            elif not paths:
                rbwUI.RBWWarning(text='Nothing available to switch {}'.format(sel))
        else:
            rbwUI.RBWError(text='{} is not an available object to be switched'.format(sel))


################################################################################
# @brief      switch the given prop/fx assembly to the rig/fx.
#
# @param      item    the item in scene
# @param      savedAsset   the savedAsset obj
#
def giveMeInReference(item, savedAsset):
    translation = cmd.xform(item, query=True, translation=True)
    rotation = cmd.xform(item, query=True, rotation=True)
    scale = cmd.xform(item, query=True, scale=True)
    newNamespace = savedAsset.getNamespace()

    newItem = api.scene.addToPropGroup(savedAsset.path, newNamespace, translation, rotation, scale)

    cmd.select(newItem, replace=True)
    cmd.setKeyframe()

    cmd.select(item, replace=True)
    itemLongName = cmd.ls(selection=True, long=True)
    cmd.parent(itemLongName, world=True)
    cmd.select(clear=True)

    if not cmd.objExists('SET_EDIT'):
        cmd.group(empty=True, n='SET_EDIT')

    cmd.parent(item, 'SET_EDIT')
    cmd.setAttr('{}.visibility'.format(item), 0)


################################################################################
# @brief      switch the set block into the sub assemblies components.
#
# @param      item    the item in scene
# @param      savedAsset   the savedAsset obj
#
def giveMeSet(item, savedAsset):
    if not cmd.objExists('SET_EDIT'):
        cmd.group(empty=True, n='SET_EDIT')

    translation = cmd.xform(item, query=True, translation=True, worldSpace=True)
    rotation = cmd.xform(item, query=True, rotation=True, worldSpace=True)
    scale = cmd.xform(item, query=True, scale=True, worldSpace=True)

    cmd.hide(item)
    cmd.parent(item, 'SET_EDIT')

    api.scene.addToSetGroup(savedAsset.path, translation=translation, rotation=rotation, scale=scale)


############################################################################
# @brief      Replace asset tool
#
class ReplaceAsset(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs the object
    #
    # @param      self  The object
    #
    def __init__(self):
        if not cmd.ls(sl=True) or len(cmd.ls(sl=True)) > 1:
            rbwUI.RBWError(text='Select 1 asset!')
            return

        self.categories = ["character", "prop", "set"]
        self.previousData = []
        self.item = None

        self.selectedObject = cmd.ls(sl=True)[0]
        if self.selectedObject.count(':'):
            selectedNamespace = self.selectedObject.split(':')[0]
            rigSaveNode = '{}:rigging_saveNode'.format(selectedNamespace)
            if cmd.objExists(rigSaveNode):
                oldRefPath = cmd.referenceQuery(rigSaveNode, filename=True)
                refNode = cmd.file(oldRefPath, query=True, referenceNode=True)
                self.categories.remove('set')
                self.previousData = [rigSaveNode, oldRefPath, refNode]
        else:
            fullPath = cmd.listRelatives(self.selectedObject, fullPath=True)
            if fullPath[0].count('|SET|'):
                fullPathParts = fullPath[0].split('|')
                currentSet = fullPathParts[2]
                self.previousData = [currentSet]
                self.categories.remove('character')
                self.categories.remove('prop')
            else:
                rbwUI.RBWError(text="The selection is not correct, it could be only a rig or a set")
                return

        super(ReplaceAsset, self).__init__()

        if cmd.window('ReplaceAsset', exists=True):
            cmd.deleteUI('ReplaceAsset')

        self.setObjectName('ReplaceAsset')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      initUI method
    #
    # @param      self  The object
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        frame = rbwUI.RBWFrame('V')

        self.categoryCombo = rbwUI.RBWComboBox('Category:', items=self.categories, size=200)
        self.groupCombo = rbwUI.RBWComboBox('Group:', size=200)
        self.nameCombo = rbwUI.RBWComboBox('Name:', size=200)
        self.variantCombo = rbwUI.RBWComboBox('Variant:', size=200)
        self.runButton = rbwUI.RBWButton('Replace', icon=['refresh.png'])
        self.runButton.clicked.connect(self.replace)

        self.categoryCombo.activated.connect(functools.partial(self.fillCombos, 'group', [('category', self.categoryCombo)], self.groupCombo))
        self.groupCombo.activated.connect(functools.partial(self.fillCombos, 'name', [('category', self.categoryCombo), ('group', self.groupCombo)], self.nameCombo))
        self.nameCombo.activated.connect(functools.partial(self.fillCombos, 'variant', [('category', self.categoryCombo), ('group', self.groupCombo), ('name', self.nameCombo)], self.variantCombo))
        self.variantCombo.activated.connect(functools.partial(self.fillCombos, None))

        frame.addWidget(self.categoryCombo)
        frame.addWidget(self.groupCombo)
        frame.addWidget(self.nameCombo)
        frame.addWidget(self.variantCombo)
        frame.addWidget(self.runButton)

        self.mainLayout.addWidget(frame)

        self.fillCombos(None)

        self.setMinimumSize(200, 200)
        self.setTitle('Replace {}'.format(self.selectedObject))
        self.setIcon('replace.png')
        self.setFocus()

    ############################################################################
    # @brief      Gets the selected save node.
    #
    # @param      field  the field you wanna find on DB
    # @param      filters  list of other known fields to make the query
    # @param      combo  the combo you wanna fill
    #
    def fillCombos(self, field, filters=[], combo=None, index=0):
        if field:
            query = "SELECT DISTINCT `{}` FROM `V_assetList` WHERE `projectID` = {} AND `category` = '{}'".format(field, os.getenv('ID_MV'), self.categoryCombo.currentText())
            for item in filters:
                query += " AND `{}` = '{}'".format(item[0], item[1].currentText())
            res = sorted([x[0] for x in api.database.selectQuery(query)])

            combo.clear()
            combo.addItems(res)
        else:
            self.item = '_'.join([self.categoryCombo.currentText(), self.groupCombo.currentText(), self.nameCombo.currentText(), self.variantCombo.currentText()])

            query = "SELECT `path` FROM `savedAsset` WHERE `dept` = '{}' AND `deptType` = 'hig' AND `path` LIKE '%{}%' AND `visible`= 1 ORDER BY 'version' DESC".format('set' if self.item.count('set_') else 'rigging', self.item)
            res = api.database.selectSingleQuery(query)
            self.path = res[0] if res else ''

            self.runButton.setText('Replace with: {}'.format(self.item))

    ############################################################################
    # @brief      Replace selected reference
    #
    def replace(self):
        if not self.item:
            rbwUI.RBWError(text='Select an asset to\nreplace the selected one first', parent=self)
            return

        if self.item.count('set_'):
            cmd.delete(self.previousData[0])
            api.savedAsset.SavedAsset(params={'path': self.path}).load('merge')

        else:
            tokens = self.item.split('_')
            newBaseNamespace = '{}_{}_{}_'.format(tokens[1], tokens[2], tokens[3])
            namespaceExists = False
            maxNumber = 1
            referenceList = cmd.ls(type='reference')
            for reference in referenceList:
                if 'sharedReferenceNode' not in reference:
                    try:
                        referenceNamespace = cmd.referenceQuery(reference, namespace=True)
                        if newBaseNamespace in referenceNamespace:
                            if referenceNamespace.split('_')[-1] == '' and not namespaceExists:
                                newNamespace = '{}1'.format(newBaseNamespace)
                            elif referenceNamespace.split('_')[-1] != '':
                                referenceNumber = int(referenceNamespace.split('_')[-1]) + 1
                                if referenceNumber > maxNumber:
                                    maxNumber = referenceNumber
                                newNamespace = '{}{}'.format(newBaseNamespace, maxNumber)
                            namespaceExists = True
                    except:
                        pass
            if not namespaceExists:
                newNamespace = newBaseNamespace

            cmd.file(os.path.join(os.getenv('MC_FOLDER'), self.path).replace('\\', '/'), loadReference=self.previousData[2], type='mayaBinary', options='v=0')
            cmd.namespace(rename=[('{}'.format(self.previousData[0].split(':')[0])), (newNamespace)], force=True)
            api.log.logger().debug('Replaced {} with {}'.format(self.previousData[0].split(':')[0], self.item))
