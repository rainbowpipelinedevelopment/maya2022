import importlib

import depts.sc_depts as scDepts

importlib.reload(scDepts)


################################################################################
# @brief      This class describes a layout.
#
class Layout(scDepts.Animation):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.name = "Lay"


################################################################################
# @brief      This class describes a primary.
#
class Primary(scDepts.Animation):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.name = "Pri"


################################################################################
# @brief      This class describes a secondary.
#
class Secondary(scDepts.Animation):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.name = "Sec"
