import importlib
import os

import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI
# importlib.reload(rbwUI)

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'common')


def run_importLightrig(*args):
    import depts.lighting.tools.lightrig.importLightrigUI as importLightrigUI
    importlib.reload(importLightrigUI)

    win = importLightrigUI.ImportLightrigUI()
    win.show()


def run_deleteLightrig(*args):
    import depts.lighting.tools.sceneSetup.cleanUpMl as cleanUpMl
    importlib.reload(cleanUpMl)

    cleanUpMl.deleteLightrig()


def run_constraintLightrigToItem(*args):
    import depts.lighting.tools.sceneSetup.sceneSetup as setup
    importlib.reload(setup)

    constraintType = rbwUI.RBWChoise(title='Attach Lightrig Options', text='Please choose a constraint type', mode={'Constraint Type': {'type': 'combo', 'items': ['', 'Point', 'Parent', 'Parent + Offset']}}, resizable=True).result()['Constraint Type']
    selection = cmd.ls(selection=True)
    if selection:
        item = selection[1]
        itemNamespace = item.split(':')[0]

        proxies = cmd.ls('{}:*'.format(itemNamespace), type='VRayProxy')
        if proxies:
            setup.constraintLigtRigToCachedItem(parentMode=constraintType.lower())
        else:
            setup.constraintLigtRigToItem(parentMode=constraintType.lower())
    else:
        rbwUI.RBWError(text='You must select something')


def run_constraintLightrigToCamera(*args):
    import maya.cmds as cmd
    import maya.mel as mel

    selection = cmd.ls(selection=True)
    if len(selection) == 1:
        lightrigNamespace = selection[0].split(':')[0]
        if cmd.objExists('{}:rim_offset'.format(lightrigNamespace)):
            cmd.select(clear=True)
            cmd.select('BAKED_CAM')
            cmd.select('{}:rim_offset'.format(lightrigNamespace), add=True)

            mel.eval('aimConstraint -offset 0 0 0 -weight 1 -aimVector 0 0 1 -upVector 0 1 0 -worldUpType "vector" -worldUpVector 0 1 0;')
        else:
            rbwUI.RBWError(text='The selected lighting {} has not a group named "rim_offset". The procedure is skipped.'.format(selection[0]))
    else:
        rbwUI.RBWError(title='ATTENTION', icon='critical', message='You have to select only the lightrig.')


importLightrigTool = {
    'name': 'Import Lightrig',
    'icon': os.path.join(iconDir, 'merge.png'),
    'launch': run_importLightrig
}

deleteLightrigTool = {
    'name': 'Delete Lightrig',
    'icon': os.path.join(iconDir, 'delete.png'),
    'launch': run_deleteLightrig
}

attachLightrigTool = {
    'name': 'Attach Lightrig',
    'icon': os.path.join(iconDir, 'chain.png'),
    'launch': run_constraintLightrigToItem
}

constraintLightrigToCameraTool = {
    'name': 'Constraint Lightrig to cam',
    'launch': run_constraintLightrigToCamera
}


tools = [
    importLightrigTool,
    deleteLightrigTool,
    attachLightrigTool,
    constraintLightrigToCameraTool
]

PackageTool = {
    "name": "lightrig",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
