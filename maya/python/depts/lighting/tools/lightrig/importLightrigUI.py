import importlib
import os
from PySide2 import QtWidgets, QtCore

import maya.cmds as cmd
import maya.mel as mel

import api.widgets.rbw_UI as rbwUI
import api.log
import api.system
import api.scene
import api.asset
import api.savedAsset
import depts.lighting.tools.sceneSetup.sceneSetup as sceneSetup

# importlib.reload(rbwUI)
importlib.reload(api.log)
importlib.reload(api.system)
importlib.reload(api.scene)
# importlib.reload(api.asset)
# importlib.reload(api.savedAsset)
importlib.reload(sceneSetup)


################################################################################
# @brief      This class describes an import lightrig ui.
#
class ImportLightrigUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      parent  The parent
    #
    def __init__(self, parent=None):
        super().__init__()
        if cmd.window('ImportLightrigUI', exists=True):
            cmd.deleteUI('ImportLightrigUI')

        self.id_mv = os.getenv('ID_MV')

        self.assets = api.system.getAssetsFromDB(adds=[['`category`', '=', '"lightrig"']])['lightrig']
        api.log.logger().debug(self.assets)
        self.sceneData = self.getSceneEntities()
        api.log.logger().debug('sceneData: {}'.format(self.sceneData))

        self.setObjectName('ImportLightrigUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)

        self.mainLayout.addWidget(self.splitter)

        ########################################################################
        # LEFT PANEL
        #
        self.selectLightrigPanel = rbwUI.RBWFrame(layout='V', margins=[5, 5, 5, 5], radius=5)

        # lightrig features
        self.lightrigFeaturesGroup = rbwUI.RBWGroupBox(title='Lightrig:', layout='V', fontSize=12)

        self.groupComboBox = rbwUI.RBWComboBox(text='Group:', bgColor='transparent', size=150)
        self.groupComboBox.activated.connect(self.fillNameComboBox)
        self.nameComboBox = rbwUI.RBWComboBox(text='Name:', bgColor='transparent', size=150)
        self.nameComboBox.activated.connect(self.setLightrigName)

        self.lightrigFeaturesGroup.addWidget(self.groupComboBox)
        self.lightrigFeaturesGroup.addWidget(self.nameComboBox)

        self.selectLightrigPanel.addWidget(self.lightrigFeaturesGroup)

        self.selectLightrigPanel.addStretch()

        # add button
        self.addLightrigButton = rbwUI.RBWButton(text='Add LightRig', icon=['add.png'], size=[220, 35])
        self.addLightrigButton.clicked.connect(self.addLightrigRow)
        self.importButton = rbwUI.RBWButton(text='Import', icon=['merge.png'], size=[220, 35])
        self.importButton.clicked.connect(self.importLightrigs)

        self.selectLightrigPanel.addWidget(self.addLightrigButton, alignment=QtCore.Qt.AlignHCenter)
        self.selectLightrigPanel.addWidget(self.importButton, alignment=QtCore.Qt.AlignHCenter)

        self.selectLightrigPanel.addStretch()

        self.splitter.addWidget(self.selectLightrigPanel)

        ########################################################################
        # RIGHT PANEL
        #
        self.lightrigVariantsPanel = rbwUI.RBWFrame(layout='V', margins=[5, 5, 5, 5], radius=5)

        # lightrig tree
        self.lightrigVariantsTree = rbwUI.RBWTreeWidget(fields=['Lightrig', 'Variant', 'Constarint Obj', 'Constarint Type', 'Camera Constarint', 'Delete'], size=[150, 100, 280, 110, 110, 35])

        self.lightrigVariantsPanel.addWidget(self.lightrigVariantsTree)

        self.splitter.addWidget(self.lightrigVariantsPanel)

        self.splitter.setStretchFactor(0, 0)
        self.splitter.setStretchFactor(1, 3)

        self.fillGroupComboBox()

        self.setMinimumSize(1050, 220)
        self.setTitle('Import Lightirg UI')
        self.setFocus()

    ############################################################################
    # @brief      Gets the scene entities.
    #
    # @return     The scene entities.
    #
    def getSceneEntities(self):
        return {key: cmd.listRelatives(key) for key in ['CHARACTER', 'PROP', 'SET'] if cmd.objExists(key)}

    ############################################################################
    # @brief      fill group combo box.
    #
    def fillGroupComboBox(self):
        self.groupComboBox.clear()

        for group in sorted(self.assets.keys()):
            self.groupComboBox.addItem(group)
        self.groupComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

        if len(self.assets.keys()) == 1:
            self.groupComboBox.setCurrentIndex(0)
            self.fillNameComboBox()

    ############################################################################
    # @brief      fill name combo box.
    #
    def fillNameComboBox(self):
        self.nameComboBox.clear()
        self.currentGroup = self.groupComboBox.currentText()

        for name in sorted(self.assets[self.currentGroup].keys()):
            self.nameComboBox.addItem(name)
        self.nameComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

        if len(self.assets[self.currentGroup].keys()) == 1:
            self.nameComboBox.setCurrentIndex(0)
            self.setLightrigName()

    ############################################################################
    # @brief      Sets the lightrig name.
    #
    def setLightrigName(self):
        self.currentName = self.nameComboBox.currentText()

    ############################################################################
    # @brief      Adds a lightrig row.
    #
    def addLightrigRow(self):
        objToConstraint = [item for key in ['CHARACTER', 'PROP'] if key in self.sceneData for item in self.sceneData[key]]
        LightrigTreeWidgetItem(self.currentGroup, self.currentName, self.assets[self.currentGroup][self.currentName], objToConstraint, self.lightrigVariantsTree)

    ############################################################################
    # @brief      import all the lightrigs from the tree.
    #
    def importLightrigs(self):
        root = self.lightrigVariantsTree.invisibleRootItem()
        for i in range(0, root.childCount()):
            item = root.child(i)
            item.importLightrig()


################################################################################
# @brief      This class describes a lightrig tree widget item.
#
class LightrigTreeWidgetItem(QtWidgets.QTreeWidgetItem):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      genre    The genre
    # @param      genreId  The genre identifier
    # @param      group    The group
    # @param      groupId  The group identifier
    # @param      name     The name
    # @param      nameId   The name identifier
    # @param      parent   The parent
    #
    def __init__(self, group, name, variants, sceneData, parent=None):
        super(LightrigTreeWidgetItem, self).__init__(parent)

        self.category = 'lightrig'
        self.group = group
        self.name = name
        self.variants = variants
        self.sceneData = sceneData

        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setText(0, '{}_{}_{}'.format(self.category, self.group, self.name))

        self.variantComboBox = rbwUI.RBWComboBox(bgColor='transparent')
        self.treeWidget().setItemWidget(self, 1, self.variantComboBox)

        self.objComboBox = rbwUI.RBWComboBox(bgColor='transparent', items=[''] + self.sceneData)
        self.treeWidget().setItemWidget(self, 2, self.objComboBox)

        self.constraintComboBox = rbwUI.RBWComboBox(bgColor='transparent', items=['', 'Point', 'Parent', 'Parent + Offset'])
        self.treeWidget().setItemWidget(self, 3, self.constraintComboBox)

        self.cameraConstraintSwitch = rbwUI.RBWSwitch()
        self.treeWidget().setItemWidget(self, 4, self.cameraConstraintSwitch)

        self.deleteButton = rbwUI.RBWButton(icon=['delete.png'], size=[None, 18])
        self.deleteButton.clicked.connect(self.delete)
        self.treeWidget().setItemWidget(self, 5, self.deleteButton)

        self.fillVariantComboBox()

    ############################################################################
    # @brief      Deletes the object.
    #
    def delete(self):
        root = self.treeWidget().invisibleRootItem()
        root.removeChild(self)

    ############################################################################
    # @brief      fill variant combo box.
    #
    def fillVariantComboBox(self):
        self.variantComboBox.clear()

        for variant in sorted(self.variants):
            self.variantComboBox.addItem(variant)

    ############################################################################
    # @brief      import the lightrig.
    #
    def importLightrig(self):
        self.variant = self.variantComboBox.currentText()

        asset = api.asset.Asset(params={
            'category': self.category,
            'group': self.group,
            'name': self.name,
            'variant': self.variant
        })

        savedAsset = api.savedAsset.SavedAsset(params={
            'asset': asset,
            'dept': 'surfacing',
            'deptType': 'rnd',
            'approvation': 'def'
        })

        if savedAsset.getLatest():
            lastLightrigPath = os.path.join(os.getenv('MC_FOLDER'), savedAsset.getLatest()[2]).replace('\\', '/')
            lightrigNamespace = '{}_{}_{}_LGT'.format(self.group, self.name, self.variant)

            lightrig = api.scene.addToLightsGroup(lastLightrigPath, lightrigNamespace)

            lightrigRealNamespace = lightrig.split(':')[0]

            if self.cameraConstraintSwitch.isChecked():
                if cmd.objExists('{}:rim_offset'.format(lightrigRealNamespace)):
                    cmd.select(clear=True)
                    cmd.select('BAKED_CAM')
                    cmd.select('{}:rim_offset'.format(lightrigRealNamespace), add=True)

                    mel.eval('aimConstraint -offset 0 0 0 -weight 1 -aimVector 0 0 1 -upVector 0 1 0 -worldUpType "vector" -worldUpVector 0 1 0;')
                else:
                    cmd.confirmDialog(title='ATTENTION', icon='critical', message='The selected lighting {} has not a group named "rim_offset". The procedure is skipped.'.format(lightrig), button=['OK'])

            constraintObj = self.objComboBox.currentText()
            if constraintObj != '':
                constraintType = self.constraintComboBox.currentText().lower()

                cmd.select(clear=True)
                cmd.select(lightrig)
                cmd.select(constraintObj, add=True)

                sceneSetup.constraintLigtRigToCachedItem(parentMode=constraintType)

        else:
            rbwUI.RBWError(title='ATTENTION', text='No surfacing save for this lightrig "lightrig {} {} {}", this import is skipped.'.format(
                self.group,
                self.name,
                self.variant
            ))

            return
