import importlib
from PySide2 import QtGui, QtCore, QtWidgets
import os

import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI
import api.json
import api.system
import api.scene
import api.asset

# importlib.reload(rbwUI)
importlib.reload(api.json)
importlib.reload(api.system)
importlib.reload(api.scene)
importlib.reload(api.asset)


################################################################################
# @brief      This class describes a get shader from def rnd ui.
#
class GetShadersFromDefRndUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(GetShadersFromDefRndUI, self).__init__()
        if cmd.window('GetShadersFromDefRndUI', exists=True):
            cmd.deleteUI('GetShadersFromDefRndUI')

        self.mcFolder = os.getenv('MC_FOLDER')
        self.id_mv = os.getenv('ID_MV')
        self.assets = api.system.getAssetsFromDB()

        self.setObjectName('GetShadersFromDefRndUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topWidget = rbwUI.RBWFrame(layout='V')
        self.mainLayout.addWidget(self.topWidget)

        # asset info
        self.sourceAssetGroup = rbwUI.RBWGroupBox(title='Source Asset:', layout='V', fontSize=12)

        self.assetCategoryComboBox = rbwUI.RBWComboBox('Category', bgColor='transparent', size=150)
        self.assetGroupComboBox = rbwUI.RBWComboBox('Group', bgColor='transparent', size=150)
        self.assetNameComboBox = rbwUI.RBWComboBox('Name', bgColor='transparent', size=150)
        self.assetVariantComboBox = rbwUI.RBWComboBox('Variant', bgColor='transparent', size=150)

        self.assetCategoryComboBox.activated.connect(self.fillAssetGroupComboBox)
        self.assetGroupComboBox.activated.connect(self.fillAssetNameComboBox)
        self.assetNameComboBox.activated.connect(self.fillAssetVariantComboBox)
        self.assetVariantComboBox.activated.connect(self.setCurrentAsset)

        self.topWidget.addWidget(self.sourceAssetGroup)

        self.sourceAssetGroup.addWidget(self.assetCategoryComboBox)
        self.sourceAssetGroup.addWidget(self.assetGroupComboBox)
        self.sourceAssetGroup.addWidget(self.assetNameComboBox)
        self.sourceAssetGroup.addWidget(self.assetVariantComboBox)

        self.runButton = rbwUI.RBWButton(text='Run', icon=['start.png'], size=[None, 35])
        self.runButton.clicked.connect(self.run)

        self.topWidget.addWidget(self.runButton)

        self.fillAssetCategoryComboBox()

        self.setMinimumSize(400, 250)

        self.setTitle('Get Rnd Shaders UI')
        self.setFocus()

    ############################################################################
    # @brief      fill the asset category combo box.
    #
    def fillAssetCategoryComboBox(self):
        self.assetCategoryComboBox.clear()
        categories = sorted(self.assets.keys())

        self.assetCategoryComboBox.addItems(categories)

        if len(categories) == 1:
            self.assetCategoryComboBox.setCurrentIndex(0)
            self.fillAssetGroupComboBox()

    ############################################################################
    # @brief      fill the group combo box.
    #
    def fillAssetGroupComboBox(self):
        self.currentCategory = self.assetCategoryComboBox.currentText()

        self.assetGroupComboBox.clear()
        groups = sorted(self.assets[self.currentCategory].keys())

        self.assetGroupComboBox.addItems(groups)

        if len(groups) == 1:
            self.assetGroupComboBox.setCurrentIndex(0)
            self.fillAssetNameComboBox()

    ############################################################################
    # @brief      fill th name combo box.
    #
    def fillAssetNameComboBox(self):
        self.currentGroup = self.assetGroupComboBox.currentText()

        self.assetNameComboBox.clear()
        names = sorted(self.assets[self.currentCategory][self.currentGroup].keys())

        self.assetNameComboBox.addItems(names)

        if len(names) == 1:
            self.assetNameComboBox.setCurrentIndex(0)
            self.fillAssetVariantComboBox()

    ############################################################################
    # @brief      fill the variant combo box.
    #
    def fillAssetVariantComboBox(self):
        self.currentName = self.assetNameComboBox.currentText()

        self.assetVariantComboBox.clear()
        variants = sorted(self.assets[self.currentCategory][self.currentGroup][self.currentName].keys())

        self.assetVariantComboBox.addItems(variants)

        if len(variants) == 1:
            self.assetVariantComboBox.setCurrentIndex(0)
            self.setCurrentAsset()

    ############################################################################
    # @brief      Sets the current asset.
    #
    def setCurrentAsset(self):
        self.currentVariant = self.assetVariantComboBox.currentText()

        self.currentAsset = api.asset.Asset({
            'category': self.currentCategory,
            'group': self.currentGroup,
            'name': self.currentName,
            'variant': self.currentVariant
        })

    ############################################################################
    # @brief      run function.
    #
    def run(self):
        selection = cmd.ls(selection=True)
        if selection:
            if len(selection) == 1:
                topGroup = selection[0]
                surfExportFolder = os.path.join(self.currentAsset.getAssetFolder(), 'export', 'surfacing', 'fxData').replace('\\', '/')
                if os.path.exists(surfExportFolder):
                    versions = [file for file in os.listdir(surfExportFolder) if file.startswith('vr')]
                    if versions:
                        versions.sort()
                        latestVersionFolder = os.path.join(surfExportFolder, versions[-1]).replace('\\', '/')

                        assetInScene = False
                        assetNamespace = None
                        for surfSaveNode in api.scene.getSaveNodes(dept='surfacing'):
                            if int(cmd.getAttr('{}.asset_id'.format(surfSaveNode))) == self.currentAsset.variantId:
                                assetInScene = True
                                assetNamespace = cmd.listConnections(surfSaveNode)[0].split(':')[0]
                                break

                        shadersInfo = api.json.json_read(os.path.join(latestVersionFolder, '{}_{}_shaders.json'.format(self.currentAsset.getAssetCode(), versions[-1])))
                        if not assetInScene:
                            shaderFile = os.path.join(latestVersionFolder, '{}_{}_shaders.mb'.format(self.currentAsset.getAssetCode(), versions[-1]))
                            assetNamespace = '{}_{}_{}_RND'.format(self.currentAsset.group, self.currentAsset.name, self.currentAsset.variant)
                            api.scene.createReference(shaderFile, 'mayaBinary', assetNamespace)

                        proxies = cmd.listRelatives(topGroup, allDescendents=True, type='VRayProxy')

                        for proxy in proxies:
                            objectsToShade = cmd.vrayUpdateProxy(proxy, getShaderSets=True)
                            for i in range(0, len(objectsToShade)):
                                shapeName = objectsToShade[i].split('/')[-1]
                                shader = '{}:{}'.format(assetNamespace, shadersInfo[shapeName.replace('ShapeDeformed', 'Shape')])

                                wrapperMaterial = cmd.defaultNavigation(defaultTraversal=True, destination='{}.shaders[{}].shadersConnections'.format(proxy, i))[0]
                                switchMaterial = cmd.defaultNavigation(defaultTraversal=True, destination='{}.baseMaterial'.format(wrapperMaterial))[0]
                                cmd.connectAttr('{}.outColor'.format(shader), '{}.material_0'.format(switchMaterial), force=True)

                else:
                    rbwUI.RBWError(text='No export data for this asset.')
                    return
            else:
                rbwUI.RBWError(text='You can only select one object.')
                return
        else:
            rbwUI.RBWError(text='Please select something.')
            return
