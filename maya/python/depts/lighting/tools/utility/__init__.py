import importlib
import os

import maya.cmds as cmd

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'common')


def run_deleteVrscene(*args):
    vrsceneTopNode = cmd.ls(selection=True)[0]
    vrsceneName = vrsceneTopNode[:-5]

    cmd.delete('{}_expression'.format(vrsceneName))
    cmd.delete(vrsceneTopNode)


def run_deleteNamespace(*args):
    import utilz.namespaceUtil as nmUtil
    importlib.reload(nmUtil)

    nmUtil.delNameSpaceSelected()


def run_killAsset(*args):
    import depts.animation.tools.utility.sceneManagement as sceneMan
    importlib.reload(sceneMan)
    sceneMan.killAsset()


def run_makeButton(*args):
    import common.makeButton.makeButton as makeButton
    importlib.reload(makeButton)

    makeButton.DM_buttonMaker()


def run_assignMaterialToProxy(*args):
    import api.widgets.rbw_UI as rbwUI
    importlib.reload(rbwUI)

    proxy = cmd.listRelatives(cmd.ls(selection=True)[0], shapes=True)[0]
    shader = cmd.ls(selection=True)[1]

    wrapperMaterial = cmd.defaultNavigation(defaultTraversal=True, destination='{}.shaders[0].shadersConnections'.format(proxy))[0]
    try:
        switchMaterial = cmd.defaultNavigation(defaultTraversal=True, destination='{}.baseMaterial'.format(wrapperMaterial))[0]
        cmd.connectAttr('{}.outColor'.format(shader), '{}.material_0'.format(switchMaterial), force=True)
    except:
        rbwUI.RBWError(text='Sorry but {} has not the pipeline shader structure with the araknoWrapper'.format(proxy))


def run_getRndShaders(*args):
    import depts.lighting.tools.utility.getShadersFromDefRnd as getShadersFromDefRnd
    importlib.reload(getShadersFromDefRnd)

    win = getShadersFromDefRnd.GetShadersFromDefRndUI()
    win.show()


deleteVrsceneTool = {
    'name': 'Delete Vrscene',
    'launch': run_deleteVrscene,
    'icon': os.path.join(iconDir, "delete.png")
}


deleteNamespaceTool = {
    "name": "Delete Namespace",
    "launch": run_deleteNamespace,
    "icon": os.path.join(iconDir, 'remove.png'),
    "statustip": "Delete namespace from selected objects"
}


killAssetTool = {
    "name": "Kill Asset Tool",
    "launch": run_killAsset,
    "icon": os.path.join(iconDir, "..", "animation", "kill.png"),
    "statustip": "Removes a reference or a set"
}


makeButtonTool = {
    "name": "Make Button",
    "launch": run_makeButton,
    "icon": os.path.join(iconDir, 'mesh.png'),
    "statustip": "Delete namespace from selected objects"
}

assignShaderTool = {
    "name": "Assign Shader to Proxy",
    "launch": run_assignMaterialToProxy,
    "icon": os.path.join(iconDir, "..", "lighting", "assignShaderToProxy.png"),
    "statustip": "Assign to the selected proxy, the selected shader"
}


getRndShadersTool = {
    "name": "Get Rnd Shaders",
    "launch": run_getRndShaders,
    "icon": os.path.join(iconDir, 'merge.png')
}

tools = [
    deleteVrsceneTool,
    deleteNamespaceTool,
    killAssetTool,
    makeButtonTool,
    assignShaderTool,
    getRndShadersTool
]

PackageTool = {
    "name": "utility",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
