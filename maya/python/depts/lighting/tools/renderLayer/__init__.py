import importlib
import os

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'common')


def run_createCustomRenderLayer(*args):
    import depts.lighting.tools.renderLayer.createCustomRenderLayerUI as createCustomRenderLayerUI
    importlib.reload(createCustomRenderLayerUI)

    win = createCustomRenderLayerUI.CreateCustomRenderLayerUI()
    win.show()


def run_manageRenderLayers(*args):
    import depts.lighting.tools.renderLayer.manageRenderLayers as manageRenderLayers
    importlib.reload(manageRenderLayers)

    win = manageRenderLayers.ManageRenderLayersUI()
    win.show()


def run_duplicateRenderLayer(*args):
    import depts.lighting.tools.renderLayer.duplicateRenderLayerUI as duplicateRenderLayerUI
    importlib.reload(duplicateRenderLayerUI)

    win = duplicateRenderLayerUI.DuplicateRenderLayerUI()
    win.show()


def run_createDeepFromVisible(*args):
    import api.renderlayer
    importlib.reload(api.renderlayer)

    api.renderlayer.createDeepFromVisibleLayer()


createCustomRenderLayerTool = {
    'name': 'Create Custom RL',
    'launch': run_createCustomRenderLayer
}

manageRenderLayersTool = {
    'name': 'Manage Render Layers',
    'launch': run_manageRenderLayers,
    'icon': os.path.join(iconDir, "option.png")
}


duplicateRenderLayerTool = {
    'name': 'Duplicate RL',
    'launch': run_duplicateRenderLayer,
    'icon': os.path.join(iconDir, "copy.png")
}


createDeepFromVisibleTool = {
    "name": "Create Deep From Visible",
    "launch": run_createDeepFromVisible,
    "statustip": "Create deep layer from the visible layer",
    "icon": os.path.join(iconDir, 'add.png')
}


tools = [
    createCustomRenderLayerTool,
    manageRenderLayersTool,
    duplicateRenderLayerTool,
    createDeepFromVisibleTool
]


PackageTool = {
    "name": "renderLayer",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
