import importlib
import os
from PySide2 import QtWidgets, QtCore

import maya.cmds as cmd
import maya.mel as mel

import api.widgets.rbw_UI as rbwUI
import api.log
import api.renderlayer

# importlib.reload(rbwUI)
importlib.reload(api.log)
importlib.reload(api.renderlayer)


################################################################################
# @brief      This class describes a create custom render layer ui.
#
class CreateCustomRenderLayerUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      parent  The parent
    #
    def __init__(self, parent=None):
        super().__init__()
        if cmd.window('CreateCustomRenderLayerUI', exists=True):
            cmd.deleteUI('CreateCustomRenderLayerUI')

        self.templateLayerFolder = os.path.join(
            os.getenv('LOCALPIPE'),
            'json_file'
        ).replace('\\', '/')

        self.getLayers()

        self.setObjectName('CreateCustomRenderLayerUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)

        self.mainLayout.addWidget(self.splitter)

        ########################################################################
        # LEFT PANEL
        #
        self.selectRenderLayerPanel = rbwUI.RBWFrame(layout='V', margins=[5, 5, 5, 5], radius=5)

        self.typeComboBox = rbwUI.RBWComboBox(text='Type:', size=150, bgColor='rgba(30, 30, 30, 150)', items=self.layers)
        self.addLayerButton = rbwUI.RBWButton(text='Add Layer', icon=['add.png'], size=[220, 35])
        self.addLayerButton.clicked.connect(self.addLayer)
        self.createButton = rbwUI.RBWButton(text='Create', icon=['../lighting/addLayer.png'], size=[220, 35])
        self.createButton.clicked.connect(self.createLayers)

        self.selectRenderLayerPanel.addWidget(self.typeComboBox, alignment=QtCore.Qt.AlignHCenter)
        self.selectRenderLayerPanel.addStretch()
        self.selectRenderLayerPanel.addWidget(self.addLayerButton, alignment=QtCore.Qt.AlignHCenter)
        self.selectRenderLayerPanel.addWidget(self.createButton, alignment=QtCore.Qt.AlignHCenter)

        self.splitter.addWidget(self.selectRenderLayerPanel)

        ########################################################################
        # RIGHT PANEL
        #
        self.renderLayerSettingsPanel = rbwUI.RBWFrame(layout='V', margins=[5, 5, 5, 5], radius=5)

        self.renderLayerSettingsTree = rbwUI.RBWTreeWidget(fields=['Type', 'Name', 'Preview Name', 'Delete'], size=[150, 150, 150, 35])

        self.renderLayerSettingsPanel.addWidget(self.renderLayerSettingsTree)

        self.splitter.addWidget(self.renderLayerSettingsPanel)

        self.splitter.setStretchFactor(0, 0)
        self.splitter.setStretchFactor(1, 3)

        self.setMinimumSize(750, 220)
        self.setTitle('Create Custom Render Layer UI')
        self.setFocus()

    ############################################################################
    # @brief      Gets the layers.
    #
    def getLayers(self):
        self.layers = []
        for file in os.listdir(self.templateLayerFolder):
            if file.startswith('template'):
                if 'slapcomp' not in file:
                    self.layers.append(file.split('_')[1][:-5])

    ############################################################################
    # @brief      Adds a layer.
    #
    def addLayer(self):
        layerType = self.typeComboBox.currentText()
        RenderLayerTreeWidgetItem(self.layers, layerType, self.renderLayerSettingsTree)

    def createLayers(self):
        root = self.renderLayerSettingsTree.invisibleRootItem()
        for i in range(0, root.childCount()):
            item = root.child(i)
            layerName = item.getLayerName()
            layerType = item.getLayerType()

            api.renderlayer.createFromTemplate(layerType, layerName)


################################################################################
# @brief      This class describes a render layer tree widget item.
#
class RenderLayerTreeWidgetItem(QtWidgets.QTreeWidgetItem):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      renderLayerTypes  The render layer types
    # @param      currentType       The current type
    # @param      parent            The parent
    #
    def __init__(self, renderLayerTypes, currentType, parent=None):
        super(RenderLayerTreeWidgetItem, self).__init__(parent)

        self.renderLayerTypes = renderLayerTypes
        self.currentType = currentType
        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.renderLayerTypesComboBox = rbwUI.RBWComboBox(bgColor='transparent', items=self.renderLayerTypes)
        self.renderLayerTypesComboBox.activated.connect(self.changePreviewName)
        self.treeWidget().setItemWidget(self, 0, self.renderLayerTypesComboBox)

        self.nameLine = rbwUI.RBWLineEdit(bgColor='transparent')
        self.nameLine.textChanged.connect(self.changePreviewName)
        self.treeWidget().setItemWidget(self, 1, self.nameLine)

        self.previewLabel = rbwUI.RBWLabel(text='', size=13, bold=False, italic=False)
        self.previewLabel.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.treeWidget().setItemWidget(self, 2, self.previewLabel)

        self.deleteButton = rbwUI.RBWButton(icon=['delete.png'], size=[None, 18])
        self.deleteButton.clicked.connect(self.delete)
        self.treeWidget().setItemWidget(self, 3, self.deleteButton)

        typeIndex = self.renderLayerTypesComboBox.findText(self.currentType, QtCore.Qt.MatchFixedString)
        self.renderLayerTypesComboBox.setCurrentIndex(typeIndex)

    ############################################################################
    # @brief      change preview name label text
    #
    def changePreviewName(self):
        self.currentType = self.renderLayerTypesComboBox.currentText()
        self.insertName = self.nameLine.text()

        if self.insertName != '':
            if ' ' in self.insertName:
                self.insertName = self.insertName.replace(' ', '_')
            insertNameTokens = self.insertName.split('_')

            for i in range(0, len(insertNameTokens)):
                token = insertNameTokens[i].lower().capitalize()
                insertNameTokens[i] = token

            self.name = ''.join(insertNameTokens)
            self.previewLabel.setText('{}_{}_RL'.format(self.currentType, self.name))
        else:
            self.previewLabel.setText('')

    ############################################################################
    # @brief      Deletes the object.
    #
    def delete(self):
        root = self.treeWidget().invisibleRootItem()
        root.removeChild(self)

    ############################################################################
    # @brief      Gets the layer name.
    #
    # @return     The layer name.
    #
    def getLayerName(self):
        return self.name

    ############################################################################
    # @brief      Gets the layer type.
    #
    # @return     The layer type.
    #
    def getLayerType(self):
        return self.currentType
