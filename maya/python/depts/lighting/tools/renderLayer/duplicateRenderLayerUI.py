import importlib
from PySide2 import QtWidgets, QtCore

import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI
import api.log
import api.renderlayer

# importlib.reload(rbwUI)
importlib.reload(api.log)
importlib.reload(api.renderlayer)

################################################################################
# @brief      This class describes a clean up ml ui.
#
class DuplicateRenderLayerUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      parent  The parent
    #
    def __init__(self, parent=None):
        super().__init__()
        if cmd.window('DuplicateRenderLayerUI', exists=True):
            cmd.deleteUI('DuplicateRenderLayerUI')

        self.layers = api.renderlayer.list()

        self.setObjectName('DuplicateRenderLayerUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topWidget = rbwUI.RBWFrame(layout='G', radius=5)
        self.mainLayout.addWidget(self.topWidget)

        self.currentLayerComboBox = rbwUI.RBWComboBox(text='Current Layer', stretch=False, bgColor='rgba(30, 30, 30, 150)', size=150, items=self.layers)
        self.newNameLine = rbwUI.RBWLineEdit(title='New Name', stretch=False, bgColor='rgba(30, 30, 30, 150)')
        self.previewLine = rbwUI.RBWLineEdit(title='Preview Layer', stretch=False, bgColor='rgba(30, 30, 30, 150)')
        self.duplicateButton = rbwUI.RBWButton(text='Duplicate', icon=['copy.png'], color='rgba(15, 15, 15, 150)', hover='rgba(30, 30, 30, 150)', size=[None, 20])

        self.newNameLine.textChanged.connect(self.updatePreviewLine)
        self.previewLine.setEnabled(False)
        self.duplicateButton.clicked.connect(self.duplicateLayer)

        self.topWidget.layout.addWidget(self.currentLayerComboBox, 0, 0, 1, 2)
        self.topWidget.layout.addWidget(self.newNameLine, 0, 2, 1, 2)
        self.topWidget.layout.addWidget(self.previewLine, 1, 0, 1, 2)
        self.topWidget.layout.addWidget(self.duplicateButton, 1, 2, 1, 2)

        self.setMinimumSize(515, 120)
        self.setTitle('Duplicate Render Layer UI')
        self.setFocus()

    ############################################################################
    # @brief      { function_description }
    #
    def updatePreviewLine(self):
        currentType = self.currentLayerComboBox.currentText().split('_')[0]
        self.insertName = self.newNameLine.text()

        if self.insertName != '':
            if ' ' in self.insertName:
                self.insertName = self.insertName.replace(' ', '_')
            insertNameTokens = self.insertName.split('_')

            for i in range(0, len(insertNameTokens)):
                token = insertNameTokens[i].lower().capitalize()
                insertNameTokens[i] = token

            name = ''.join(insertNameTokens)
            self.previewLine.setText('{}_{}_RL'.format(currentType, name))
        else:
            self.previewLine.setText('')

    ############################################################################
    # @brief      Duplicates a layer.
    #
    def duplicateLayer(self):
        oldName = self.currentLayerComboBox.currentText()
        newName = self.previewLine.text()

        api.renderlayer.duplicateAndRenameRenderLayer(oldName, newName)

        self.layers = api.renderlayer.list()
        self.currentLayerComboBox.clear()
        self.currentLayerComboBox.addItems(self.layers)
