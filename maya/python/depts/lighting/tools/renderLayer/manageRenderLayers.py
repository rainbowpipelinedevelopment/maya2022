import importlib
import os
from PySide2 import QtWidgets, QtCore

import maya.cmds as cmd
import maya.mel as mel

import api.widgets.rbw_UI as rbwUI
import api.log
import api.system
import api.scene
import api.renderlayer
import api.database
import api.shot
import api.savedShot


# importlib.reload(rbwUI)
importlib.reload(api.log)
importlib.reload(api.system)
importlib.reload(api.scene)
importlib.reload(api.renderlayer)
importlib.reload(api.database)
# importlib.reload(api.shot)
# importlib.reload(api.savedShot)


################################################################################
# @brief      This class describes a manage render layers ui.
#
class ManageRenderLayersUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      parent  The parent
    #
    def __init__(self, parent=None):
        super().__init__()
        if cmd.window('ManageRenderLayersUI', exists=True):
            cmd.deleteUI('ManageRenderLayersUI')

        self.id_mv = os.getenv('ID_MV')
        self.mcFolder = os.getenv('MC_FOLDER')
        self.animationFolder = os.path.join(self.mcFolder, 'scenes', 'animation').replace('\\', '/')
        self.shots = api.system.getShotsFromDB()

        self.setObjectName('ManageRenderLayersUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)

        self.mainLayout.addWidget(self.splitter)

        ########################################################################
        # LEFT PANEL
        #
        self.selectShotPanel = rbwUI.RBWFrame(layout='V', margins=[5, 5, 5, 5], radius=5)

        # shot features
        self.shotFeaturesGroup = rbwUI.RBWGroupBox(title='Shot:', layout='V', fontSize=12)

        self.seasonComboBox = rbwUI.RBWComboBox(text='Season:', bgColor='transparent', size=100)
        self.seasonComboBox.activated.connect(self.fillEpisodeComboBox)
        self.episodeComboBox = rbwUI.RBWComboBox(text='Episode:', bgColor='transparent', size=100)
        self.episodeComboBox.activated.connect(self.fillSequenceComboBox)
        self.sequenceComboBox = rbwUI.RBWComboBox(text='Sequence:', bgColor='transparent', size=100)
        self.sequenceComboBox.activated.connect(self.fillShotComboBox)
        self.shotComboBox = rbwUI.RBWComboBox(text='Shot:', bgColor='transparent', size=100)
        self.shotComboBox.activated.connect(self.setCurrentShot)

        self.shotFeaturesGroup.addWidget(self.seasonComboBox)
        self.shotFeaturesGroup.addWidget(self.episodeComboBox)
        self.shotFeaturesGroup.addWidget(self.sequenceComboBox)
        self.shotFeaturesGroup.addWidget(self.shotComboBox)

        self.selectShotPanel.addWidget(self.shotFeaturesGroup)
        self.selectShotPanel.addStretch()

        self.splitter.addWidget(self.selectShotPanel)

        ########################################################################
        # RIGHT PANEL
        #
        self.renderLayersPanel = rbwUI.RBWFrame(layout='V', margins=[5, 5, 5, 5], radius=5)

        # render layers tree
        self.renderLayersTree = rbwUI.RBWTreeWidget(fields=['Render Layer', 'Create BH', 'Latest Version', 'Export RL'], size=[150, 100, 100, 100])
        self.renderLayersTree.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

        self.renderLayersPanel.addWidget(self.renderLayersTree)

        # selected buttons
        self.exportButtonsWidget = rbwUI.RBWFrame(layout='H', margins=[0, 0, 0, 0], radius=5, bgColor='transparent')

        self.createSelectedBhButton = rbwUI.RBWButton(text='Create BH for selected', icon=['add.png'], size=[None, 35])
        self.createSelectedBhButton.clicked.connect(self.createBhForSelected)
        self.exportSelectedLayersButton = rbwUI.RBWButton(text='Export Selected Layers', icon=['export.png'], size=[None, 35])
        self.exportSelectedLayersButton.clicked.connect(self.exportSelectedRenderLayer)

        self.exportButtonsWidget.addWidget(self.createSelectedBhButton)
        self.exportButtonsWidget.addWidget(self.exportSelectedLayersButton)

        self.renderLayersPanel.addWidget(self.exportButtonsWidget)

        self.importSelectedButton = rbwUI.RBWButton(text='Import Selected', icon=['merge.png'], size=[None, 35])
        self.importSelectedButton.hide()
        self.importSelectedButton.clicked.connect(self.importSelectedRenderLayer)

        self.renderLayersPanel.addWidget(self.importSelectedButton)

        self.splitter.addWidget(self.renderLayersPanel)

        self.autoFillCombo()

        self.splitter.setStretchFactor(0, 0)
        self.splitter.setStretchFactor(1, 3)

        self.setMinimumSize(800, 250)
        self.setTitle('Render Layers UI')
        self.setFocus()

    ############################################################################
    # @brief      fill season combo box.
    #
    def fillSeasonComboBox(self):
        for season in sorted(self.shots.keys()):
            self.seasonComboBox.addItem(season)
        self.seasonComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

        if len(self.shots.keys()) == 1:
            self.seasonComboBox.setCurrentIndex(0)
            self.fillEpisodeComboBox()

    ############################################################################
    # @brief      fill episode combo box.
    #
    def fillEpisodeComboBox(self):
        self.currentSeason = self.seasonComboBox.currentText()
        self.episodeComboBox.clear()

        for episode in sorted(self.shots[self.currentSeason].keys()):
            self.episodeComboBox.addItem(episode)
        self.episodeComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

        if len(self.shots[self.currentSeason].keys()) == 1:
            self.episodeComboBox.setCurrentIndex(0)
            self.fillSequenceComboBox()

    ############################################################################
    # @brief      fill sequence combo box.
    #
    def fillSequenceComboBox(self):
        self.currentEpisode = self.episodeComboBox.currentText()
        self.sequenceComboBox.clear()

        for sequence in sorted(self.shots[self.currentSeason][self.currentEpisode].keys()):
            self.sequenceComboBox.addItem(sequence)
        self.sequenceComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

        if len(self.shots[self.currentSeason][self.currentEpisode].keys()) == 1:
            self.sequenceComboBox.setCurrentIndex(0)
            self.fillShotComboBox()

    ############################################################################
    # @brief      fill shot combo box.
    #
    def fillShotComboBox(self):
        self.currentSequence = self.sequenceComboBox.currentText()
        self.shotComboBox.clear()

        for shot in sorted(self.shots[self.currentSeason][self.currentEpisode][self.currentSequence].keys()):
            self.shotComboBox.addItem(shot)
        self.shotComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

        if len(self.shots[self.currentSeason][self.currentEpisode][self.currentSequence].keys()) == 1:
            self.shotComboBox.setCurrentIndex(0)
            self.setCurrentShot()

    ############################################################################
    # @brief      Sets the current shot.
    #
    def setCurrentShot(self):
        self.currentShot = self.shotComboBox.currentText()

        self.currentShotObj = api.shot.Shot({
            'season': self.currentSeason,
            'episode': self.currentEpisode,
            'sequence': self.currentSequence,
            'shot': self.currentShot
        })

        self.fillRenderLayerTree()

    ############################################################################
    # @brief      Determines if that shot.
    #
    # @return     True if that shot, False otherwise.
    #
    def isThatShot(self):
        openShot = api.scene.getShotByScene()

        return True if self.currentShotObj.shotId == openShot.shotId else False

    ############################################################################
    # @brief      fill the render layer tree.
    #
    def fillRenderLayerTree(self):
        self.renderLayersTree.clear()
        if self.isThatShot():
            self.exportButtonsWidget.show()
            self.importSelectedButton.hide()
            self.renderLayersTree.updateTree(fields=['Render Layer', 'Create BH', 'Latest Version', 'Export RL'], size=[150, 100, 100, 100])
            for renderLayer in api.renderlayer.list():
                SceneRenderLayerItem(renderLayer, self.currentShotObj, self.renderLayersTree)
        else:
            self.importSelectedButton.show()
            self.exportButtonsWidget.hide()
            self.renderLayersTree.updateTree(fields=['Render Layer', 'RL Version', 'Import RL'], size=[150, 100, 100])
            for renderLayer in self.getShotLayers(self.currentShotObj):
                ImportedRenderLayerItem(renderLayer, self.currentShotObj, self.renderLayersTree)

    ############################################################################
    # @brief      Gets the shot layers.
    #
    # @param      shot  The shot
    #
    # @return     The shot layers.
    #
    def getShotLayers(self, shot):
        renderLayers = []
        renderLayerQuery = "SELECT `name` FROM `renderLayer` WHERE `linkId`={} AND `enabled`=1 AND `type`='shot'".format(shot.shotId)
        results = api.database.selectQuery(renderLayerQuery)

        for result in results:
            renderLayers.append(result[0])
        renderLayers.sort()

        return renderLayers

    ############################################################################
    # @brief      Creates a bh for selected.
    #
    def createBhForSelected(self):
        for item in self.renderLayersTree.selectedItems():
            item.createBlackHoleCollection()

    ############################################################################
    # @brief      selected render layers
    #
    def exportSelectedRenderLayer(self):
        for item in self.renderLayersTree.selectedItems():
            item.exportRenderLayer()

    ############################################################################
    # @brief      import the selected render layer.
    #
    def importSelectedRenderLayer(self):
        for item in self.renderLayersTree.selectedItems():
            item.importRenderLayer()

        ############################################################################
    # @brief      auto fill of all the combos.
    #
    def autoFillCombo(self):
        fileName = api.scene.scenename()
        if self.animationFolder in fileName:
            try:
                relPath = fileName.split(self.animationFolder)[1]
                season = relPath.split('/')[1]
                episode = relPath.split('/')[2]
                sequence = relPath.split('/')[3]
                shot = relPath.split('/')[4]

                self.fillSeasonComboBox()
                seasonIndex = self.seasonComboBox.findText(season, QtCore.Qt.MatchFixedString)
                self.seasonComboBox.setCurrentIndex(seasonIndex)

                self.fillEpisodeComboBox()
                episodeIndex = self.episodeComboBox.findText(episode, QtCore.Qt.MatchFixedString)
                self.episodeComboBox.setCurrentIndex(episodeIndex)

                self.fillSequenceComboBox()
                sequenceIndex = self.sequenceComboBox.findText(sequence, QtCore.Qt.MatchFixedString)
                self.sequenceComboBox.setCurrentIndex(sequenceIndex)

                self.fillShotComboBox()
                shotIndex = self.shotComboBox.findText(shot, QtCore.Qt.MatchFixedString)
                self.shotComboBox.setCurrentIndex(shotIndex)
                self.setCurrentShot()

            except:
                self.fillSeasonComboBox()
        else:
            self.fillSeasonComboBox()

################################################################################
# @brief      This class describes a render layer item.
#
class RenderLayerItem(QtWidgets.QTreeWidgetItem):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      renderLayer  The render layer
    #
    def __init__(self, renderLayer, shotObj, parent):
        super().__init__(parent)
        self.layerName = renderLayer
        self.shotObj = shotObj

        self.id_mv = os.getenv('ID_MV')
        self.mcFolder = os.getenv('MC_FOLDER')
        self.animationFolder = os.path.join(self.mcFolder, 'scenes', 'animation').replace('\\', '/')

        self.versions = self.getVersions()

    ############################################################################
    # @brief      Gets the versions.
    #
    # @return     The versions.
    #
    def getVersions(self, reverse=False):
        lgtSavedShot = api.savedShot.SavedShot(params={'shot': self.shotObj, 'dept': 'Lgt'})
        savedShotsInfo = lgtSavedShot.getAvailableVersions()

        versions = []
        if savedShotsInfo:
            for saveRecod in savedShotsInfo:
                renderLayerFile = os.path.join(self.mcFolder, saveRecod[2], '..', 'export', 'renderLayers', '{}_{}.json'.format(self.layerName, saveRecod[3])).replace('\\', '/')
                if os.path.exists(renderLayerFile):
                    versions.append(saveRecod[3])

            versions.sort(reverse=reverse)
        else:
            pass

        return versions


################################################################################
# @brief      This class describes a scene render layer item.
#
class SceneRenderLayerItem(RenderLayerItem):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      renderLayer  The render layer
    # @param      shotObj      The shot object
    #
    def __init__(self, renderLayer, shotObj, parent):
        super().__init__(renderLayer, shotObj, parent)

        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.nameLabel = rbwUI.RBWLabel(text=self.layerName, size=12, bold=False, italic=False)
        self.treeWidget().setItemWidget(self, 0, self.nameLabel)

        if self.checkBlackHole():
            self.blackHoleLabel = rbwUI.RBWLabel(text='Already Exists', size=12, bold=False, italic=False, alignment='AlignCenter')
            self.treeWidget().setItemWidget(self, 1, self.blackHoleLabel)
        else:
            self.createBlackHoleButton = rbwUI.RBWButton(text='Create BH col', icon=['add.png'])
            self.createBlackHoleButton.clicked.connect(self.createBlackHoleCollection)
            self.treeWidget().setItemWidget(self, 1, self.createBlackHoleButton)

        if self.versions:
            lgtSavedShot = api.savedShot.SavedShot(params={'shot': self.shotObj, 'dept': 'Lgt'})
            lastVersion = lgtSavedShot.getLatest()[3]

            if lastVersion == self.versions[-1]:
                color = 'rgba(0, 255, 0, 150)'
            else:
                color = 'rgba(255, 0, 0, 150)'

            self.versionLabel = rbwUI.RBWLabel(text=self.versions[-1], size=12, bold=False, italic=False, color=color)
        else:
            self.versionLabel = rbwUI.RBWLabel(text='NA', size=12, bold=False, italic=False)
        self.treeWidget().setItemWidget(self, 2, self.versionLabel)

        self.exportLayerButton = rbwUI.RBWButton(text='Export Layer', icon=['export.png'])
        self.exportLayerButton.clicked.connect(self.exportRenderLayer)
        self.treeWidget().setItemWidget(self, 3, self.exportLayerButton)

    ############################################################################
    # @brief      check if the render layer has a black hole collections.
    #
    # @return     the check result
    #
    def checkBlackHole(self):
        return api.renderlayer.hasBHCollection(self.layerName)

    ############################################################################
    # @brief      Creates a black hole collection.
    #
    def createBlackHoleCollection(self):
        api.renderlayer.createBlackHoleCollectionForRenderLayer(self.layerName)
        self.initUI()

    ############################################################################
    # @brief      export render layer
    #
    def exportRenderLayer(self):
        exportFolder = os.path.join(os.path.dirname(api.scene.scenename()), 'export', 'renderLayers').replace('\\', '/')
        fileName = '{}_{}.json'.format(self.layerName, exportFolder.split('/')[-3])

        exportPath = os.path.join(exportFolder, fileName).replace('\\', '/')

        if os.path.exists(exportPath):
            warningMessage = '{} e\' stato gia\' esportato da questa versione del file.\nVuoi sovrascrivere tale export?'.format(self.layerName)
            if not rbwUI.RBWWarning(text=warningMessage).result():
                return

        if not os.path.exists(exportFolder):
            os.makedirs(exportFolder)

        api.renderlayer.exportSelectedRenderLayer(self.layerName, exportPath)
        api.renderlayer.cleanUpRenderElement(exportPath)

        self.initUI()


################################################################################
# @brief      This class describes an imported render layer item.
#
class ImportedRenderLayerItem(RenderLayerItem):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      renderLayer  The render layer
    # @param      shotObj      The shot object
    # @param      parent       The parent
    #
    def __init__(self, renderLayer, shotObj, parent):
        super().__init__(renderLayer, shotObj, parent)
        self.versions = self.getVersions(reverse=True)

        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.nameLabel = rbwUI.RBWLabel(text=self.layerName, size=12, bold=False, italic=False)
        self.treeWidget().setItemWidget(self, 0, self.nameLabel)

        self.versionsComboBox = rbwUI.RBWComboBox(items=self.versions, bgColor='transparent')
        self.treeWidget().setItemWidget(self, 1, self.versionsComboBox)

        self.importLayerButton = rbwUI.RBWButton(text='Import Layer', icon=['merge.png'])
        self.importLayerButton.clicked.connect(self.importRenderLayer)
        self.treeWidget().setItemWidget(self, 2, self.importLayerButton)

    ############################################################################
    # @brief      import render layer
    #
    def importRenderLayer(self):
        importedVersion = self.versionsComboBox.currentText()
        renderLayerFolder = os.path.join(self.shotObj.getShotFolder(), 'Lgt', importedVersion, 'export', 'renderLayers').replace('\\', '/')
        fileName = '{}_{}.json'.format(self.layerName, importedVersion)
        filePath = os.path.join(renderLayerFolder, fileName).replace('\\', '/')

        api.renderlayer.importFromTemplate(filePath)
