import importlib
import os

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'common')


def run_switchToVrScene(*args):
    import common.switchAssembly as switchAssembly
    importlib.reload(switchAssembly)

    switchAssembly.switchAssembliesTo('vrscene')


def run_switchToRnd(*args):
    import common.switchAssembly as switchAssembly
    importlib.reload(switchAssembly)

    switchAssembly.switchAssembliesTo('surfacing')


def run_switchToVrMesh(*args):
    import common.switchAssembly as switchAssembly
    importlib.reload(switchAssembly)

    switchAssembly.switchAssembliesTo('vrmesh')


def run_switchToCacheBase(*args):
    import common.switchAssembly as switchAssembly
    importlib.reload(switchAssembly)

    switchAssembly.switchAssembliesTo('cache base')


switchVrSceneTool = {
    'name': 'Switch To VrScene',
    'launch': run_switchToVrScene,
    "icon": os.path.join(iconDir, "switch.png")
}

switchRndTool = {
    'name': 'Switch To RND',
    'launch': run_switchToRnd,
    "icon": os.path.join(iconDir, "switch.png")
}

switchVrMeshTool = {
    'name': 'Switch To VrMesh',
    'launch': run_switchToVrMesh,
    "icon": os.path.join(iconDir, "switch.png")
}

switchBaseTool = {
    'name': 'Switch To Base',
    'launch': run_switchToCacheBase,
    "icon": os.path.join(iconDir, "switch.png")
}

tools = [
    switchVrSceneTool,
    switchRndTool,
    switchVrMeshTool,
    switchBaseTool
]


PackageTool = {
    "name": "switches",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
