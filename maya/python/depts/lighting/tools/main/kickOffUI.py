import importlib
import os
import functools
from PySide2 import QtWidgets, QtCore
import requests

import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI
import api.log
import api.scene
import api.savedShot
import api.renderlayer
import depts.lighting.tools.main.kickOff as kickOff

# importlib.reload(rbwUI)
importlib.reload(api.log)
importlib.reload(api.scene)
# importlib.reload(api.savedShot)
importlib.reload(api.renderlayer)
importlib.reload(kickOff)


################################################################################
# @brief      This class describes a new kick off ui.
#
class KickOffUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      parent  The parent
    #
    def __init__(self, parent=None):
        super().__init__()
        if cmd.window('KickOffUI', exists=True):
            cmd.deleteUI('KickOffUI')

        self.setObjectName('KickOffUI')
        self.initUI()
        self.updateMargins()

        self.detectShot()
        self.fillRenderLayersTree()
        self.fillRenderableCameraComboBox()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)

        self.mainLayout.addWidget(self.splitter)

        ########################################################################
        # LEFT PANEL
        #
        self.renderLayerPanel = rbwUI.RBWFrame(layout='V', margins=[5, 5, 5, 5], radius=5)

        # scene info group
        self.sceneInfosGroupBox = rbwUI.RBWGroupBox(title='Scene Infos', layout='V', fontSize=12, margins=[12, 20, 12, 12])

        self.seasonLineEdit = rbwUI.RBWLineEdit(title='Season:', bgColor='transparent')
        self.episodeLineEdit = rbwUI.RBWLineEdit(title='Episode:', bgColor='transparent')
        self.sequenceLineEdit = rbwUI.RBWLineEdit(title='Sequence:', bgColor='transparent')
        self.shotLineEdit = rbwUI.RBWLineEdit(title='Shot:', bgColor='transparent')
        self.savePromotedCheckBox = rbwUI.RBWCheckBox(text='Save as Promoted')

        self.sceneInfosGroupBox.addWidget(self.seasonLineEdit)
        self.sceneInfosGroupBox.addWidget(self.episodeLineEdit)
        self.sceneInfosGroupBox.addWidget(self.sequenceLineEdit)
        self.sceneInfosGroupBox.addWidget(self.shotLineEdit)
        self.sceneInfosGroupBox.addWidget(self.savePromotedCheckBox)

        self.renderLayerPanel.addWidget(self.sceneInfosGroupBox)

        self.kickOffModeComboBox = rbwUI.RBWComboBox(text='KickOff Mode', items=['fml', 'ffr'], bgColor='rgba(30, 30, 30, 150)')
        self.kickOffModeComboBox.activated.connect(self.setKickOffMode)
        self.renderLayerPanel.addWidget(self.kickOffModeComboBox)

        # render layers tree
        self.renderLayersTree = rbwUI.RBWTreeWidget(fields=['Render Layers'])
        self.renderLayersTree.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.renderLayersTree.itemSelectionChanged.connect(self.changeUIOptions)

        self.renderLayerPanel.addWidget(self.renderLayersTree)

        # render layer options
        self.optionButtonGroupBox = rbwUI.RBWGroupBox(title='Options', layout='V', fontSize=12, margins=[12, 20, 12, 12])

        self.refreshSelectedRenderLayerButton = rbwUI.RBWButton(text='Refresh Selected RLs', size=[200, 35])
        self.toggleRenderableSelectedRenderLayerButton = rbwUI.RBWButton(text='Toggle Renderable Selected RLs', size=[200, 35])

        self.refreshSelectedRenderLayerButton.clicked.connect(self.refreshAllRenderLayers)
        self.toggleRenderableSelectedRenderLayerButton.clicked.connect(self.toggleRenderableRenderLayer)

        self.optionButtonGroupBox.addWidget(self.refreshSelectedRenderLayerButton, alignment=QtCore.Qt.AlignCenter)
        self.optionButtonGroupBox.addWidget(self.toggleRenderableSelectedRenderLayerButton, alignment=QtCore.Qt.AlignCenter)

        self.renderLayerPanel.addWidget(self.optionButtonGroupBox)

        # kickoff button
        self.kickOffButton = rbwUI.RBWButton(text='KICKOFF', size=[250, 35], icon=['../lighting/kick.png'])
        self.kickOffButton.clicked.connect(self.runKickOff)
        self.renderLayerPanel.addWidget(self.kickOffButton, alignment=QtCore.Qt.AlignCenter)

        self.splitter.addWidget(self.renderLayerPanel)

        ########################################################################
        # RIGHT PANEL
        #
        self.kickOffOptionsPanel = rbwUI.RBWFrame(layout='V', margins=[5, 5, 5, 5], radius=5)

        # select label
        self.selectLabelWidget = rbwUI.RBWFrame(layout='H', margins=[5, 5, 5, 5], radius=5, bgColor='rgba(30, 30, 30, 150)')
        self.selectLabel = rbwUI.RBWLabel(text='',)
        self.selectLabelWidget.addWidget(self.selectLabel)

        self.kickOffOptionsPanel.addWidget(self.selectLabelWidget)

        # frame range
        self.frameRangeGroupBox = rbwUI.RBWGroupBox(title='Frame Range', layout='V', fontSize=12, margins=[12, 20, 12, 12])

        self.frameRangeTypeComboBox = rbwUI.RBWComboBox(text='Frame Range Type', items=['Single', 'Range', 'Custom'], size=150)
        self.frameRangeTypeComboBox.activated.connect(self.setAnimType)
        self.frameRangeValueLine = rbwUI.RBWLineEdit(title='Frame Range Value')
        self.frameRangeValueLine.textChanged.connect(self.changeFrameRangeValue)

        self.frameRangeGroupBox.addStretch()
        self.frameRangeGroupBox.addWidget(self.frameRangeTypeComboBox)
        self.frameRangeGroupBox.addWidget(self.frameRangeValueLine)
        self.frameRangeGroupBox.addStretch()

        self.kickOffOptionsPanel.addWidget(self.frameRangeGroupBox)

        # resolution area
        self.resolutionGroupBox = rbwUI.RBWGroupBox(title='Resolution', layout='V', fontSize=12, margins=[12, 20, 12, 12])

        self.resolutionPresetCombobox = rbwUI.RBWComboBox(text='Resolution Preset', items=['8K', '4K', 'Full HD', '720p', 'Full', 'Half', 'Quarter', 'Custom'], size=150)
        self.resolutionPresetCombobox.activated.connect(self.setResolution)
        self.customResolutionValueLine = rbwUI.RBWLineEdit(title='Custom Resolution Value', placeHolder='2048x2048')
        self.customResolutionValueLine.textChanged.connect(self.changeResolutionValue)

        self.resolutionGroupBox.addStretch()
        self.resolutionGroupBox.addWidget(self.resolutionPresetCombobox)
        self.resolutionGroupBox.addWidget(self.customResolutionValueLine)
        self.resolutionGroupBox.addStretch()

        self.kickOffOptionsPanel.addWidget(self.resolutionGroupBox)

        # render quality
        self.renderQualityPresetGroupBox = rbwUI.RBWGroupBox(title='Render Quality', layout='H', fontSize=12, margins=[12, 20, 12, 12])

        self.renderQualityPresetComboBox = rbwUI.RBWComboBox(text='Render Quality Preset', items=['Extra Low', 'Low', 'Medium', 'High', 'Custom'], size=150)
        self.renderQualityPresetComboBox.activated.connect(self.setQuality)

        self.renderQualityPresetGroupBox.addWidget(self.renderQualityPresetComboBox)

        self.kickOffOptionsPanel.addWidget(self.renderQualityPresetGroupBox)

        # new widget contains 3
        self.checkBoxWidget = rbwUI.RBWFrame(layout='H', bgColor='transparent', margins=[0, 0, 0, 0])

        # render layer mode
        self.renderLayerModeGroupBox = rbwUI.RBWGroupBox(title='Render Layer Mode', layout='V', fontSize=12, margins=[12, 20, 12, 12])

        self.beautyModeCheckBox = rbwUI.RBWCheckBox(text='Beauty', stretch=False)
        self.beautyModeCheckBox.toggled.connect(self.changeBeautyModeValue)
        self.deepModeCheckBox = rbwUI.RBWCheckBox(text='Deep', stretch=False)
        self.deepModeCheckBox.toggled.connect(self.changeDeepModeValue)

        self.renderLayerModeGroupBox.addWidget(self.beautyModeCheckBox)
        self.renderLayerModeGroupBox.addWidget(self.deepModeCheckBox)

        self.checkBoxWidget.addWidget(self.renderLayerModeGroupBox)

        # job advanced option
        self.advancedJobOptionsGroupBox = rbwUI.RBWGroupBox(title='Advanced Job Options', layout='V', fontSize=12, margins=[12, 20, 12, 12])

        self.isAnHighJobCheckBox = rbwUI.RBWCheckBox(text='Is An High Job', stretch=False)
        self.isALightJobCheckBox = rbwUI.RBWCheckBox(text='Is a Light Job', stretch=False)
        self.chunckJobCheckBox = rbwUI.RBWCheckBox(text='Chunk Job', stretch=False)
        self.extraTimeCheckBox = rbwUI.RBWCheckBox(text='Extra Time', stretch=False)
        self.keepBeautyCheckBox = rbwUI.RBWCheckBox(text='Keep Beauty', stretch=False)

        self.isAnHighJobCheckBox.stateChanged.connect(self.changeIsAnHighJobValue)
        self.isALightJobCheckBox.stateChanged.connect(self.changeIsALightJobValue)
        self.chunckJobCheckBox.stateChanged.connect(self.changeChunkJobValue)
        self.extraTimeCheckBox.stateChanged.connect(self.changeExtraTimeValue)
        self.keepBeautyCheckBox.stateChanged.connect(self.changeKeepBeautyValue)

        self.advancedJobOptionsGroupBox.addWidget(self.isAnHighJobCheckBox)
        self.advancedJobOptionsGroupBox.addWidget(self.isALightJobCheckBox)
        self.advancedJobOptionsGroupBox.addWidget(self.chunckJobCheckBox)
        self.advancedJobOptionsGroupBox.addWidget(self.extraTimeCheckBox)
        self.advancedJobOptionsGroupBox.addWidget(self.keepBeautyCheckBox)

        self.checkBoxWidget.addWidget(self.advancedJobOptionsGroupBox)

        # job priority options
        self.jobProirityOptionsGroupBox = rbwUI.RBWGroupBox(title='Job Priority Options', layout='V', fontSize=12, margins=[12, 20, 12, 12])

        self.extraRetakeCheckBox = rbwUI.RBWCheckBox(text='Extra Retake', stretch=False)
        self.extraRetakeCheckBox.stateChanged.connect(self.changePriorityValue)

        self.jobProirityOptionsGroupBox.addWidget(self.extraRetakeCheckBox)

        self.checkBoxWidget.addWidget(self.jobProirityOptionsGroupBox)

        self.kickOffOptionsPanel.addWidget(self.checkBoxWidget)

        # final widget
        self.bottomWidget = rbwUI.RBWFrame(layout='V', margins=[12, 12, 12, 12], bgColor='rgb(30, 30, 30, 150)')

        self.renderCameraComboBox = rbwUI.RBWComboBox(text='Render Camera', size=150)
        self.jobNotesLine = rbwUI.RBWLineEdit(title='Job Notes')
        self.jobNotesLine.textChanged.connect(self.changeNotesValue)

        self.bottomWidget.addWidget(self.renderCameraComboBox)
        self.bottomWidget.addWidget(self.jobNotesLine)

        self.kickOffOptionsPanel.addWidget(self.bottomWidget)

        self.splitter.addWidget(self.kickOffOptionsPanel)

        self.splitter.setStretchFactor(0, 0)
        self.splitter.setStretchFactor(1, 3)

        self.setFixedSize(720, 600)
        self.setTitle('KickOff UI')
        self.setFocus()

    ############################################################################
    # @brief      Sets the kick off mode.
    #
    def setKickOffMode(self):
        self.kickOffMode = self.kickOffModeComboBox.currentText()

        items = []
        root = self.renderLayersTree.invisibleRootItem()
        for i in range(0, root.childCount()):
            items.append(root.child(i))
        self.refreshRenderLayers(items)

    ############################################################################
    # @brief      Sets the animation type.
    #
    # @param      value  The value
    #
    def setAnimType(self):
        renderLayerItems = self.renderLayersTree.selectedItems()
        for renderLayerItem in renderLayerItems:
            renderLayer = self.renderLayersTree.itemWidget(renderLayerItem, 0).getText()
            self.renderLayerSettings[renderLayer]['animType'] = self.frameRangeTypeComboBox.currentIndex()

            if self.renderLayerSettings[renderLayer]['animType'] == 0:
                self.frameRangeValueLine.setText('')
                self.frameRangeValueLine.setEnabled(False)
            elif self.renderLayerSettings[renderLayer]['animType'] == 1:
                self.frameRangeValueLine.setText('{}-{}'.format(1, self.duration))
                self.frameRangeValueLine.setEnabled(True)
            else:
                self.frameRangeValueLine.setText('')
                self.frameRangeValueLine.setEnabled(True)

            self.renderLayerSettings[renderLayer]['frames'] = self.frameRangeValueLine.text()

    ############################################################################
    # @brief      change the frame range value for the selected render layer.
    #
    def changeFrameRangeValue(self):
        renderLayerItems = self.renderLayersTree.selectedItems()
        for renderLayerItem in renderLayerItems:
            renderLayer = self.renderLayersTree.itemWidget(renderLayerItem, 0).getText()
            self.renderLayerSettings[renderLayer]['frames'] = self.frameRangeValueLine.text()

    ############################################################################
    # @brief      Sets the resolution.
    #
    # @param      value  The value
    #
    def setResolution(self, value):
        renderLayerItems = self.renderLayersTree.selectedItems()
        for renderLayerItem in renderLayerItems:
            renderLayer = self.renderLayersTree.itemWidget(renderLayerItem, 0).getText()
            self.renderLayerSettings[renderLayer]['resolution'] = self.resolutionPresetCombobox.currentIndex()

            if value == 7:
                self.customResolutionValueLine.show()
            else:
                self.customResolutionValueLine.hide()

    ############################################################################
    # @brief      change the resolution value for the selected render layer.
    #
    def changeResolutionValue(self):
        renderLayerItems = self.renderLayersTree.selectedItems()
        for renderLayerItem in renderLayerItems:
            renderLayer = self.renderLayersTree.itemWidget(renderLayerItem, 0).getText()
            self.renderLayerSettings[renderLayer]['resolution_value'] = self.customResolutionValueLine.text()

    ############################################################################
    # @brief      Sets the quality.
    #
    # @param      value  The value
    #
    def setQuality(self):
        renderLayerItems = self.renderLayersTree.selectedItems()
        for renderLayerItem in renderLayerItems:
            renderLayer = self.renderLayersTree.itemWidget(renderLayerItem, 0).getText()
            self.renderLayerSettings[renderLayer]['quality'] = self.renderQualityPresetComboBox.currentIndex()

    ############################################################################
    # @brief      change beauty render mode for the selected render layer.
    #
    def changeBeautyModeValue(self):
        renderLayerItems = self.renderLayersTree.selectedItems()
        for renderLayerItem in renderLayerItems:
            renderLayer = self.renderLayersTree.itemWidget(renderLayerItem, 0).getText()
            wantedState = self.beautyModeCheckBox.isChecked()
            if wantedState:
                self.renderLayerSettings[renderLayer]['renderBeauty'] = wantedState
            else:
                self.beautyModeCheckBox.toggle()

    ############################################################################
    # @brief      change deep render mode for the selected render layer.
    #
    def changeDeepModeValue(self):
        renderLayerItems = self.renderLayersTree.selectedItems()
        for renderLayerItem in renderLayerItems:
            renderLayer = self.renderLayersTree.itemWidget(renderLayerItem, 0).getText()
            wantedState = self.deepModeCheckBox.isChecked()
            if not wantedState:
                self.renderLayerSettings[renderLayer]['renderDeep'] = wantedState
            else:
                if self.canApplyDeep(renderLayer):
                    self.renderLayerSettings[renderLayer]['renderDeep'] = True
                else:
                    warningMessage = 'ATTENZIONE\nStai tentando di attivare il deep ma o esiste un deep Layer a parte o ne stai settando uno.\nNel primo caso cancella il deep layer specifico per usare questa modalita\'.\nRicontrolla per favore.'
                    rbwUI.RBWWarning(title='ATTENZIONE', text=warningMessage, defCancel=None)
                    self.deepModeCheckBox.toggle()

    ############################################################################
    # @brief      change is an env job value for the selected render layer.
    #
    def changeIsAnHighJobValue(self):
        renderLayerItems = self.renderLayersTree.selectedItems()
        for renderLayerItem in renderLayerItems:
            renderLayer = self.renderLayersTree.itemWidget(renderLayerItem, 0).getText()
            self.renderLayerSettings[renderLayer]['isHighJob'] = self.isAnHighJobCheckBox.isChecked()

    ############################################################################
    # @brief      change is a light job value for the selected render layer.
    #
    def changeIsALightJobValue(self):
        renderLayerItems = self.renderLayersTree.selectedItems()
        for renderLayerItem in renderLayerItems:
            renderLayer = self.renderLayersTree.itemWidget(renderLayerItem, 0).getText()
            self.renderLayerSettings[renderLayer]['isLightJob'] = self.isALightJobCheckBox.isChecked()

    ############################################################################
    # @brief      change chink job value for the selected render layer.
    #
    def changeChunkJobValue(self):
        renderLayerItems = self.renderLayersTree.selectedItems()
        for renderLayerItem in renderLayerItems:
            renderLayer = self.renderLayersTree.itemWidget(renderLayerItem, 0).getText()
            self.renderLayerSettings[renderLayer]['chunk'] = self.chunckJobCheckBox.isChecked()

    ############################################################################
    # @brief      change extra time value for the selected render layer.
    #
    def changeExtraTimeValue(self):
        renderLayerItems = self.renderLayersTree.selectedItems()
        for renderLayerItem in renderLayerItems:
            renderLayer = self.renderLayersTree.itemWidget(renderLayerItem, 0).getText()
            self.renderLayerSettings[renderLayer]['extraTime'] = self.extraTimeCheckBox.isChecked()

    ############################################################################
    # @brief      change keep beauty value for the selected render layer.
    #
    def changeKeepBeautyValue(self):
        renderLayerItems = self.renderLayersTree.selectedItems()
        for renderLayerItem in renderLayerItems:
            renderLayer = self.renderLayersTree.itemWidget(renderLayerItem, 0).getText()
            self.renderLayerSettings[renderLayer]['keepBeauty'] = self.keepBeautyCheckBox.isChecked()

    ############################################################################
    # @brief      change priority value for the selected render layer.
    #
    def changePriorityValue(self):
        renderLayerItems = self.renderLayersTree.selectedItems()
        for renderLayerItem in renderLayerItems:
            renderLayer = self.renderLayersTree.itemWidget(renderLayerItem, 0).getText()
            self.renderLayerSettings[renderLayer]['priority'] = self.extraRetakeCheckBox.isChecked()

    ############################################################################
    # @brief      change job notes for the selected render layer.
    #
    def changeNotesValue(self):
        renderLayerItems = self.renderLayersTree.selectedItems()
        for renderLayerItem in renderLayerItems:
            renderLayer = self.renderLayersTree.itemWidget(renderLayerItem, 0).getText()
            self.renderLayerSettings[renderLayer]['notes'] = self.jobNotesLine.text()

    ############################################################################
    # @brief      Determines ability to apply deep.
    #
    # @param      renderLayer  The render layer
    #
    # @return     True if able to apply deep, False otherwise.
    #
    def canApplyDeep(self, renderLayer):
        if renderLayer.startswith('DEEP'):
            return False
        else:
            renderLayerInfoList = renderLayer.split('_')
            renderLayerInfoList = ['DEEP'] + renderLayerInfoList
            eventualeDeepLayer = '_'.join(renderLayerInfoList)
            renderLayersList = api.renderlayer.list()
            if eventualeDeepLayer in renderLayersList:
                return False
            else:
                return True

    ############################################################################
    # @brief      detect the current shot.
    #
    def detectShot(self):
        self.renderLayerSettings = {}

        sceneName = api.scene.scenename(relative=True)
        self.savedShot = api.savedShot.SavedShot(params={'path': sceneName})

        if self.savedShot:
            self.seasonLineEdit.setText(self.savedShot.shot.season)
            self.episodeLineEdit.setText(self.savedShot.shot.episode)
            self.sequenceLineEdit.setText(self.savedShot.shot.sequence)
            self.shotLineEdit.setText(self.savedShot.shot.shot)

            self.kickOffObject = kickOff.KickOff()

            self.shotFolder = self.savedShot.shot.getShotFolder()

            self.vrsceneFolder = os.path.join(
                self.shotFolder,
                'sys',
                'VRSCENE'
            ).replace('\\', '/')

        else:
            return

        self.seasonLineEdit.setReadOnly(True)
        self.episodeLineEdit.setReadOnly(True)
        self.sequenceLineEdit.setReadOnly(True)
        self.shotLineEdit.setReadOnly(True)

        self.id_mv = os.getenv('ID_MV')

        self.duration = self.savedShot.shot.getShotDuration()

        # get shot render layers infos
        timelineStart = 1
        timelineEnd = self.duration
        timelineMiddle = (timelineEnd + timelineStart) // 2

        self.defaultSettings = {
            'mode': 'ffr',
            'animType': 1,
            'frames': '{}-{}'.format(timelineStart, timelineEnd),
            'resolution': 4,
            'resolution_value': '',
            'quality': 2,
            'renderBeauty': True,
            'renderDeep': False,
            'isHighJob': False,
            'isLightJob': False,
            'chunk': False,
            'extraTime': False,
            'keepBeauty': False,
            'priority': False,
            'notes': '',
            'suspended': False,
            'overscan': False,
            'extraAttr': ''
        }

        shotgunInfo = self.savedShot.shot.getFramesWithCameraMovement()
        if shotgunInfo is not None:

            self.defaultEnvSettings = {
                'mode': 'ffr',
                'animType': 2,
                'frames': shotgunInfo,
                'resolution': 4,
                'resolution_value': '',
                'quality': 2,
                'renderBeauty': True,
                'renderDeep': False,
                'isHighJob': False,
                'isLightJob': False,
                'chunk': False,
                'extraTime': False,
                'keepBeauty': False,
                'priority': False,
                'notes': '',
                'overscan': False,
                'extraAttr': ''
            }
        else:
            rbwUI.RBWWarning(title='Warning', text='Il tool no ha trovato informazioni riguardanti lo shot su shotgun\nIn caso inserisci a mano dei frame speciali per l\'environment', defCancel=None)
            self.defaultEnvSettings = self.defaultSettings.copy()

        self.fmlRenderSettings = {
            'mode': 'fml',
            'animType': 2,
            'frames': '{};{};{}'.format(timelineStart, timelineMiddle, timelineEnd),
            'resolution': 4,
            'resolution_value': '',
            'quality': 0,
            'renderBeauty': True,
            'renderDeep': False,
            'isHighJob': False,
            'isLightJob': False,
            'chunk': False,
            'extraTime': False,
            'keepBeauty': False,
            'priority': False,
            'notes': '',
            'overscan': False,
            'extraAttr': ''
        }

    ############################################################################
    # @brief      fill the render layers tree.
    #
    def fillRenderLayersTree(self):
        self.renderLayersTree.clear()

        layerList = api.renderlayer.list()
        newLayers = []

        if layerList:
            for i in range(0, len(layerList)):
                layer = layerList[i]
                item = QtWidgets.QTreeWidgetItem(self.renderLayersTree)
                itemCheckBox = rbwUI.RBWCheckBox(text=layer, margins=[2, 2, 2, 2])

                try:
                    state = True if api.renderlayer.isRenderable(layer) else False
                except ValueError:
                    state = True if layerList[layer] else False

                itemCheckBox.setChecked(state)
                self.renderLayersTree.setItemWidget(item, 0, itemCheckBox)

                layerVrsceneFolder = os.path.join(self.vrsceneFolder, layer).replace('\\', '/')
                versionNumeber = self.kickOffObject.getVersion(layer, self.savedShot)
                version = 'v{}'.format(str(versionNumeber).zfill(3))
                settingsJsonFileName = os.path.join(layerVrsceneFolder, version, 'settings.json').replace('\\', '/')

                oldSettingsJsonFileName = os.path.join(layerVrsceneFolder, version.replace('v', 'vr'), 'settings.json').replace('\\', '/')

                if os.path.exists(settingsJsonFileName):
                    self.renderLayerSettings[layer] = api.json.json_read(settingsJsonFileName)
                    self.kickOffMode = self.renderLayerSettings[layer]['mode']

                elif os.path.exists(oldSettingsJsonFileName):
                    self.renderLayerSettings[layer] = api.json.json_read(oldSettingsJsonFileName)
                    self.kickOffMode = self.renderLayerSettings[layer]['mode']
                else:
                    newLayers.append(layer)

                if i == 0:
                    self.renderLayersTree.setCurrentItem(item)

            if not hasattr(self, 'kickOffMode'):
                self.kickOffMode = 'fml'

            for layer in newLayers:
                if self.kickOffMode == 'ffr':
                    # ffr mode
                    if layer.split('_')[0] not in ['ENV', 'DOME']:
                        self.renderLayerSettings[layer] = self.defaultSettings.copy()
                        if layer.split('_')[0] == 'CHAR' and self.canApplyDeep(layer):
                            self.renderLayerSettings[layer]['renderDeep'] = True
                    else:
                        self.renderLayerSettings[layer] = self.defaultEnvSettings.copy()
                        if self.canApplyDeep(layer):
                            self.renderLayerSettings[layer]['renderDeep'] = True
                else:
                    # fml mode
                    self.renderLayerSettings[layer] = self.fmlRenderSettings.copy()
                    if layer.split('_')[0] in ['ENV', 'FX']:
                        self.renderLayerSettings[layer]['quality'] = 2

            if self.kickOffMode == 'fml':
                self.kickOffModeComboBox.setCurrentIndex(0)
            else:
                self.kickOffModeComboBox.setCurrentIndex(1)

    ############################################################################
    # @brief      Fill the Renderable Camera combo box
    #
    # @param      self  The object
    #
    # @return     None
    #
    def fillRenderableCameraComboBox(self):
        self.renderCameraComboBox.clear()
        cameras = cmd.ls(type='camera')
        cameras = cmd.listRelatives(cameras, parent=True)
        for camera in cameras:
            if camera not in ['top', 'front', 'side', 'bottom']:
                self.renderCameraComboBox.addItem(camera)

    ############################################################################
    # @brief      restore UI settings from the layer settings dict.
    #
    def changeUIOptions(self):
        renderLayerItems = self.renderLayersTree.selectedItems()
        renderLayer = self.renderLayersTree.itemWidget(renderLayerItems[0], 0).getText()

        if len(renderLayerItems) == 1:
            self.selectLabel.setText(renderLayer)
        else:
            self.selectLabel.setText('Multiple RL selection')

        # kickOff mode
        if self.renderLayerSettings[renderLayer]['mode'] == 'ffr':
            self.keepBeautyCheckBox.show()
        else:
            self.keepBeautyCheckBox.hide()

        # Frame Range
        currentAnimTypeIndex = self.renderLayerSettings[renderLayer]['animType']
        self.frameRangeTypeComboBox.setCurrentIndex(currentAnimTypeIndex)
        self.frameRangeValueLine.setText(self.renderLayerSettings[renderLayer]['frames'])
        if currentAnimTypeIndex == 0:
            self.frameRangeValueLine.setEnabled(False)
        else:
            self.frameRangeValueLine.setEnabled(True)

        # Resolution
        currentResolutionPresetindex = self.renderLayerSettings[renderLayer]['resolution']
        self.resolutionPresetCombobox.setCurrentIndex(currentResolutionPresetindex)

        if currentResolutionPresetindex == 7:
            self.customResolutionValueLine.show()
            self.customResolutionValueLine.setText(self.renderLayerSettings[renderLayer]['resolution_value'])
        else:
            self.customResolutionValueLine.hide()

        # render quality
        currentQualityPresetIndex = self.renderLayerSettings[renderLayer]['quality']
        self.renderQualityPresetComboBox.setCurrentIndex(currentQualityPresetIndex)

        # render mode
        self.beautyModeCheckBox.setChecked(self.renderLayerSettings[renderLayer]['renderBeauty'])
        self.deepModeCheckBox.setChecked(self.renderLayerSettings[renderLayer]['renderDeep'])

        # Advanced Option
        self.isAnHighJobCheckBox.setChecked(self.renderLayerSettings[renderLayer]['isHighJob'])

        self.isALightJobCheckBox.setChecked(self.renderLayerSettings[renderLayer]['isLightJob'])

        self.chunckJobCheckBox.setChecked(self.renderLayerSettings[renderLayer]['chunk'])

        self.extraTimeCheckBox.setChecked(self.renderLayerSettings[renderLayer]['extraTime'])

        self.keepBeautyCheckBox.setChecked(self.renderLayerSettings[renderLayer]['keepBeauty'])

        # priority
        self.extraRetakeCheckBox.setChecked(self.renderLayerSettings[renderLayer]['priority'])

        # Notes
        self.jobNotesLine.setText(self.renderLayerSettings[renderLayer]['notes'])

    ############################################################################
    # @brief      Refresh the default settings all render layers
    #
    def refreshAllRenderLayers(self):
        renderLayerItems = self.renderLayersTree.selectedItems()
        self.refreshRenderLayers(renderLayerItems)

    ############################################################################
    # @brief      Refresh the default settings for the selected render layers
    #
    # @param      renderLayerItems  The render layer items
    #
    def refreshRenderLayers(self, renderLayerItems):
        for renderLayerItem in renderLayerItems:
            renderLayer = self.renderLayersTree.itemWidget(renderLayerItem, 0).getText()
            if self.kickOffMode == 'ffr':
                if renderLayer.split('_')[0] in ['ENV', 'DOME']:
                    self.renderLayerSettings[renderLayer] = self.defaultEnvSettings.copy()
                    if self.canApplyDeep(renderLayer):
                        self.renderLayerSettings[renderLayer]['renderDeep'] = True
                else:
                    self.renderLayerSettings[renderLayer] = self.defaultSettings.copy()
                    if renderLayer.split('_')[0] in ['CHAR', 'PROP'] and self.canApplyDeep(renderLayer):
                        self.renderLayerSettings[renderLayer]['renderDeep'] = True

                self.changeUIOptions()
            else:
                self.renderLayerSettings[renderLayer] = self.fmlRenderSettings.copy()
                if renderLayer.split('_')[0] in ['ENV', 'FX']:
                    self.renderLayerSettings[renderLayer]['quality'] = 2
                self.changeUIOptions()

    ############################################################################
    # @brief      toggle the renderable switch for selected render layers
    #
    def toggleRenderableRenderLayer(self):
        selectedItems = self.renderLayersTree.selectedItems()

        for item in selectedItems:
            self.renderLayersTree.itemWidget(item, 0).toggle()

    ############################################################################
    # @brief      check before kickOff launch
    #
    # @return     the check result.
    #
    def checkUiOptions(self):
        # devo controllare se animType e contenuto del range siano coerenti
        rangeMessage = 'Please check the match between the syntax and the selected mode.\nEXAMPLES:\n-Single: "1"\n-Range: "1-100"\n-Custom "1;2-10;11-100:2"'

        sendLayers = []
        for itemIndex in range(self.renderLayersTree.topLevelItemCount()):
            item = self.renderLayersTree.topLevelItem(itemIndex)
            if self.renderLayersTree.itemWidget(item, 0).isChecked():
                sendLayers.append(self.renderLayersTree.itemWidget(item, 0).getText())

        for sendLayer in sendLayers:
            layerSettings = self.renderLayerSettings[sendLayer]

            if layerSettings['animType'] == 0:
                if not layerSettings['frames'].isdigit():
                    rbwUI.RBWWarning(title='WARNING', text=rangeMessage, defCancel=None)
                    return False

            elif layerSettings['animType'] == 1:
                startFrame = layerSettings['frames'].split('-')[0]
                endFrame = layerSettings['frames'].split('-')[1]

                if not(startFrame.isdigit() and endFrame.isdigit()):
                    rbwUI.RBWWarning(title='WARNING', text=rangeMessage, defCancel=None)
                    return False

        api.renderlayer.switch(api.renderlayer.getDefaultRenderLayer())
        doubles, length = self.kickOffObject.checkDoubleRenderElements()

        if length:
            for renderLayer in api.renderlayer.list():
                if not renderLayer.startswith('SHADOW_') and not renderLayer.startswith('SHADOWGRASS_'):
                    api.renderlayer.switchByName(renderLayer)
                    if cmd.getAttr('AO.enabled'):
                        errorMessage = 'One or more render layer (not shadow type) has the AO "render element" active.\nPlease turn off AO in all layer that are not SHADOW and try again.'
                        rbwUI.RBWError(text=errorMessage)
                        return
        # inserire controllo render element AO e denoiser
        if doubles:
            doubleMsg = 'One or more Render Elements are duplicate.\nPlease clean the scene and try again.'
            rbwUI.RBWWarning(title='WARNING', text=doubleMsg, defCancel=None)
            return False
        else:
            if length is None:
                noneMsg = 'You don\'t have any render elements in scene.\nDo you want to continue?'
                noneAnswer = rbwUI.RBWWarning(title='WARNING', text=noneMsg).result()
                if noneAnswer:
                    return True
                else:
                    return False
            elif cmd.getAttr('vraySettings.relements_enableall') == 0:
                enableMsg = 'You have all the Render Elements disabled\nDo you want to continue?'
                enableAnswer = rbwUI.RBWWarning(title='WARNING', text=enableMsg, button=['Yes', 'No']).result()
                if enableAnswer:
                    return True
                else:
                    return False
            else:
                return True

    ############################################################################
    # @brief      run kickoff function.
    #
    def runKickOff(self):
        self.errors = []
        checkOptions = self.checkUiOptions()
        if checkOptions:
            toSendLayers = []
            for itemIndex in range(self.renderLayersTree.topLevelItemCount()):
                item = self.renderLayersTree.topLevelItem(itemIndex)
                if self.renderLayersTree.itemWidget(item, 0).isChecked():
                    toSendLayers.append(self.renderLayersTree.itemWidget(item, 0).getText())

            for renderLayer in toSendLayers:
                renderCamera = self.renderCameraComboBox.currentText()
                self.renderLayerSettings[renderLayer]['camera'] = renderCamera

                self.renderLayerSettings[renderLayer]['current_frame'] = int(cmd.currentTime(query=True))

                self.kickOffObject.setUpRenderLayer(renderLayer, self.renderLayerSettings[renderLayer], self.savedShot)

                if api.renderlayer.hasAVrscene(renderLayer):
                    self.renderLayerSettings[renderLayer]['vrscenes'] = True
                else:
                    self.renderLayerSettings[renderLayer]['vrscenes'] = False

            # new control for bad render settings override collection
            for renderLayer in toSendLayers:
                if api.renderlayer.getRenderLayerCollectionByName(renderLayer, 'vraySettings_col') is not None:
                    errorMsg = 'Sorry but one or more layer has a bad vraySettings collection.\nPlease clean the sceneand try again.'
                    rbwUI.RBWError(text=errorMsg)
                    return

            startMsg = 'ATTENZIONE:\nTutti i settaggi sulla qualita\' non sono validi per i layer SHADOW e DOME si prega di effettuarlo a mano.\nProcedere?'
            startAnswer = rbwUI.RBWConfirm(title='VRay', text=startMsg).result()
            if startAnswer:
                cmd.setAttr('vraySettings.animBatchOnly', 0)
                # qui va salvato lo shot di lighting
                saveNote = rbwUI.RBWInput(title='Insert Save Note:', text='Please Insert a note for the save if you want', mode={'Note': 'string'}, resizable=True).result()['Note']
                self.savedShot.note = saveNote
                self.savedShot.promoted = 1 if self.savePromotedCheckBox.isChecked() else 0
                success = self.savedShot.save()
                if not success:
                    return

            else:
                return

            for renderLayer in toSendLayers:
                try:
                    self.kickOffObject.sendJobToFarm(renderLayer, self.renderLayerSettings[renderLayer], self.savedShot)
                except requests.exceptions.RetryError:
                    api.log.logger().debug('Error Creating job for layer {}'.format(renderLayer))
                    self.errors.append(renderLayer)

                if len(self.errors) > 0:
                    layersString = ','.join(self.errors)
                    errorMsg = 'I seguenti render layer non sono stati lanciati in farm\n{}'.format(layersString)
                    rbwUI.RBWError(text=errorMsg)

            for layerName in self.renderLayerSettings:
                settingJsonFolder = os.path.join(self.savedShot.shot.getShotFolder(), 'sys', 'VRSCENE', layerName, 'v{}'.format(str(self.kickOffObject.getVersion(layerName, self.savedShot)).zfill(3)))
                settingJsonFile = os.path.join(settingJsonFolder, 'settings.json').replace('\\', '/')
                api.json.json_write(self.renderLayerSettings[layerName], settingJsonFile)

            rbwUI.RBWConfirm(title='Info', text='Kickoff end correctly', defCancel=None)
        else:
            pass


################################################################################
# @brief      check before open
#
# @return     check result.
#
def checkToOpen():
    try:
        sceneName = api.scene.scenename(relative=True)
    except IndexError:
        rbwUI.RBWError(text='Please open an official scene', resizable=True)
        return False

    for renderLayer in api.renderlayer.list():
        if api.renderlayer.hasUntitledCollections(renderLayer):
            rbwUI.RBWError(text='"_untitled_" collection in {} render layer.\nPlease check.'.format(renderLayer), resizable=True)
            return False

    return True
