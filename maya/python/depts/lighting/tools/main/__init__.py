import importlib
import os

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'arakno')


################################################################################
# @brief      run load and save UI
#
# @param      args  The arguments
#
def run_loadAndSave(*args):
    import common.loadAndSaveShotUI as lss
    importlib.reload(lss)

    win = lss.ShotUI()
    win.show()


################################################################################
# @brief      run checkupdate tool.
#
# @param      args  The arguments
#
def run_checkUpdate(*args):
    import common.checkUpdate.checkUpdateUI as checkUpdateUI
    importlib.reload(checkUpdateUI)

    win = checkUpdateUI.CheckUpdateUI('lighting')
    win.show()


################################################################################
# @brief      run kickoff tool.
#
# @param      args  The arguments
#
def run_kickOff(*args):
    import depts.lighting.tools.main.kickOffUI as kickOffUI
    importlib.reload(kickOffUI)

    if kickOffUI.checkToOpen():
        win = kickOffUI.KickOffUI()
        win.show()


loadAndSaveTool = {
    "name": "L/S Lighting",
    "launch": run_loadAndSave,
    "icon": os.path.join(iconDir, "loadAndSaveLighting.png"),
    "statustip": "Load & Save"
}

checkupdateTool = {
    "name": "Check Update",
    "launch": run_checkUpdate,
    "icon": os.path.join(iconDir, '..', 'common', "refresh.png"),
    "statustip": "Check Update"
}


kickOffTool = {
    'name': 'KickOff',
    'launch': run_kickOff,
    'icon': os.path.join(iconDir, '..', 'lighting', "kick.png")
}


tools = [
    loadAndSaveTool,
    checkupdateTool,
    kickOffTool
]

PackageTool = {
    "name": "main",
    "tools": tools
}
