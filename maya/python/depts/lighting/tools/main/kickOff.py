import importlib
import os
from datetime import datetime
import time

import maya.cmds as cmd

import api.log
import api.database
import api.renderlayer
import api.farm
import api.vray

importlib.reload(api.log)
importlib.reload(api.database)
importlib.reload(api.renderlayer)
importlib.reload(api.farm)
importlib.reload(api.vray)


################################################################################
# @brief      This class describes a kick off.
#
class KickOff(object):

    def __init__(self):
        super(KickOff, self).__init__()

        self.id_mv = os.getenv('ID_MV')

        self.extraLowLayersOverride = {
            'dmcThreshold': 0.05,
            'dmcMaxSubdivs': 4,
            'dmcMinSubdivs': 2
        }

        self.lowLayersOverride = {
            'dmcThreshold': 0.05,
            'dmcMaxSubdivs': 8,
            'dmcMinSubdivs': 4
        }

        self.mediumLayersOverride = {
            'dmcThreshold': 0.01,
            'dmcMaxSubdivs': 16,
            'dmcMinSubdivs': 4
        }

        self.highLayersOverride = {
            'dmcThreshold': 0.001,
            'dmcMaxSubdivs': 24,
            'dmcMinSubdivs': 4
        }

        self.mediumEnvOverride = {
            'dmcThreshold': 0.03,
            'dmcMaxSubdivs': 4,
            'dmcMinSubdivs': 1
        }

        self.highEnvOverride = {
            'dmcThreshold': 0.01,
            'dmcMaxSubdivs': 8,
            'dmcMinSubdivs': 2
        }

        resolutionQuery = "SELECT width, height FROM `p_prj_resolution` AS pr JOIN `p_resolution` AS r ON pr.resolutionId = r.id WHERE pr.id_mv = {}".format(self.id_mv)

        resolutionTuple = api.database.selectQuery(resolutionQuery)[0]

        self.renderingResolution = {'width': resolutionTuple[0], 'height': resolutionTuple[1]}

    ############################################################################
    # @brief      Gets the version.
    #
    # @param      layerName  The layer name
    # @param      savedShot  The saved shot
    #
    # @return     The version.
    #
    def getVersion(self, layerName, savedShot):
        existingQuery = "SELECT * FROM `renderLayer` WHERE `type`='shot' AND `linkId`={} AND `name`='{}'".format(savedShot.getShotId(), layerName)
        results = api.database.selectSingleQuery(existingQuery)

        # get render layer
        if results:
            layerId = results[0]
        else:
            fields = 'name, type, linkId, enabled'
            args = (layerName, 'shot', savedShot.getShotId(), 1)
            pers = ", ".join(["\'{}\'".format(o) for o in args])

            insertQuery = 'INSERT INTO `renderLayer` (%s) VALUES (%s) ' % (fields, pers)

            api.log.logger().debug(insertQuery)

            layerId = api.database.insertQuery(insertQuery)

        # get render layer version
        versionQuery = "SELECT sre.version FROM `savedRenderLayer` AS sre JOIN `savedShot` AS ss ON sre.linkId=ss.id WHERE ss.dept='{}' AND `renderLayerId`={} ORDER BY sre.date DESC".format(savedShot.dept, layerId)

        api.log.logger().debug(versionQuery)

        versionResult = api.database.selectQuery(versionQuery)

        if versionResult:
            version = int(versionResult[0][0].split('v')[1])
        else:
            version = 0

        return version

    ############################################################################
    # @brief      Gets the file name.
    #
    # @param      layerName  The layer name
    # @param      savedShot  The saved shot
    # @param      mode       The mode
    #
    # @return     The file name.
    #
    def getFileName(self, layerName, savedShot, mode='rendering'):
        shotFolder = savedShot.shot.getShotFolder()

        if mode == 'rendering':
            fileFolder = os.path.join(shotFolder, 'Lgt', 'RENDER', layerName).replace('\\', '/')
            ext = 'exr'
        else:
            fileFolder = os.path.join(shotFolder, 'sys', 'VRSCENE', layerName).replace('\\', '/')
            ext = 'vrscene'

        version = 'v{}'.format(str(self.getVersion(layerName, savedShot) + 1).zfill(3))

        filePath = os.path.join(fileFolder, version, '{}.{}'.format(layerName, ext)).replace('\\', '/')

        return filePath

    ############################################################################
    # @brief      Saves on bd.
    #
    # @param      layerName  The layer name
    # @param      savedShot  The saved shot
    # @param      version    The version
    #
    def saveOnBD(self, layerName, savedShot, version):
        existingQuery = "SELECT `id` FROM `renderLayer` WHERE `type`='shot' AND `linkId`={} AND `name`='{}'".format(savedShot.getShotId(), layerName)
        layerId = api.database.selectSingleQuery(existingQuery)[0]

        dateVar = datetime.fromtimestamp(time.time())
        date = dateVar.strftime("%Y.%m.%d-%H.%M.%S")

        fields = 'renderLayerId, linkId, version, approvation, date'
        args = (layerId, savedShot.db_id, version, 0, date)
        pers = ", ".join(["\'{}\'".format(o) for o in args])
        insertQuery = 'INSERT INTO `savedRenderLayer` (%s) VALUES (%s) ' % (fields, pers)

        api.log.logger().debug(insertQuery)

        api.database.insertQuery(insertQuery)

    ############################################################################
    # @brief      check if double render element are in scene.
    #
    def checkDoubleRenderElements(self):
        masterLayer = api.renderlayer.getDefaultRenderLayer()
        api.renderlayer.switchByName(masterLayer)
        renderElementsFilter = cmd.itemFilter(byType='VRayRenderElement')
        renderElementsSetFilter = cmd.itemFilter(byType='VRayRenderElementSet')
        allRenderElementsFilter = cmd.itemFilter(union=(renderElementsFilter, renderElementsSetFilter))
        elements = cmd.lsThroughFilter(allRenderElementsFilter)

        if elements is not None:
            for element in elements:
                if 'renderElements_' in element:
                    return True, len(elements)

            return False, len(elements)

        else:
            return False, None

    ############################################################################
    # @brief      Reads a value from scene.
    #
    # @return     { description_of_the_return_value }
    #
    def readValueFromScene(self):

        overrides = {
            'dmcThreshold': "",
            'dmcMaxSubdivs': "",
            'dmcMinSubdivs': ""
        }

        for override in overrides:
            overrides[override] = cmd.getAttr('vraySettings.{}'.format(override))

        return overrides

    ################################################################################
    # @brief      override render settings
    #
    # @param      layer  The render layer
    # @param      quality      The quality
    #
    # @return     { description_of_the_return_value }
    #
    def overrideRenderSettings(self, layer, quality):
        excludeLayersType = ['DOME', 'SHADOW', 'DEEP']

        if layer.split('_')[0] not in excludeLayersType:
            if quality != 4:
                if quality == 0:
                    overrides = self.extraLowLayersOverride
                elif quality == 1:
                    overrides = self.lowLayersOverride
                elif quality == 2:
                    if layer.split('_')[0] == 'ENV':
                        overrides = self.mediumEnvOverride
                    else:
                        overrides = self.mediumLayersOverride
                elif quality == 3:
                    if layer.split('_')[0] == 'ENV':
                        overrides = self.highEnvOverride
                    else:
                        overrides = self.highLayersOverride

                for override in overrides:
                    if overrides[override] is not None:
                        api.renderlayer.createRenderSettingsOverride(layer, override, overrides[override])
                    else:
                        api.renderlayer.removeRenderSettingsOverride(layer, override)
            else:
                overrides = self.readValueFromScene()

            return overrides

        else:
            overrides = self.readValueFromScene()
            return overrides

    ############################################################################
    # @brief      setup for the deep render.
    #
    # @param      layer             The layer
    # @param      layerJobSettings  The layer job settings
    # @param      savedShot         The saved shot
    #
    def deepSetup(self, layer, layerJobSettings, savedShot):
        if not layer.startswith('DEEP_'):
            deepOverrides = {
                "relements_deepMergeMode": 1,
                "relements_deepMergeCoeff": 0.1,
                "relements_deepExcludeRGB": 1
            }

            api.renderlayer.switchByName(layer)
            if layerJobSettings['renderDeep']:

                layerNameList = layer.split('_')
                layerNameList = ['DEEP'] + layerNameList
                deepLayer = '_'.join(layerNameList)
                layerJobSettings['deepVersion'] = self.getVersion(deepLayer, savedShot) + 1

                for override in deepOverrides:
                    api.renderlayer.createRenderSettingsOverride(layer, override, deepOverrides[override])
            else:
                for override in deepOverrides:
                    api.renderlayer.removeRenderSettingsOverride(layer, override)
        else:
            pass

    ############################################################################
    # @brief      Sets the metadata.
    #
    # @param      layer             The layer
    # @param      layerJobSettings  The layer job settings
    #
    def setMetadata(self, layer, layerJobSettings):
        if layerJobSettings['mode'] == 'fml':
            api.renderlayer.createRenderSettingsOverride(layer, 'imgOpt_exr_attributes', 'kickOffMode=fml;')
        else:
            api.renderlayer.createRenderSettingsOverride(layer, 'imgOpt_exr_attributes', 'kickOffMode=ffr;')

    ############################################################################
    # @brief      setup the rendeer layer with all the override.
    #
    # @param      layer             The layer
    # @param      layerJobSettings  The layer job settings
    # @param      savedShot         The saved shot
    #
    def setUpRenderLayer(self, layer, layerJobSettings, savedShot):
        quality = layerJobSettings['quality']

        api.renderlayer.switch(layer)

        self.deepSetup(layer, layerJobSettings, savedShot)

        self.setMetadata(layer, layerJobSettings)

        overrides = self.overrideRenderSettings(layer, quality)

        extraAttr = ''
        overrideNames = list(overrides.keys())
        overrideNames.sort()
        for override in overrideNames:
            extraAttr = '{}{}={}, '.format(extraAttr, override, overrides[override])

        layerJobSettings['extraAttr'] = extraAttr

    ############################################################################
    # @brief      Decodes layer settings.
    #
    # @param      layer          The layer
    # @param      layerSettings  The layer settings
    #
    # @return     The layer job settings.
    #
    def decodeLayerSettings(self, layer, layerSettings):
        layerJobSettings = layerSettings.copy()

        # frame range
        if layerJobSettings['animType'] == 0:
            layerJobSettings['frames'] = '{}'.format(layerSettings['current_frame'])

        # resolution
        if layerJobSettings['resolution'] == 0:
            layerJobSettings['resolution_value'] = '7680x4320'

        elif layerJobSettings['resolution'] == 1:
            layerJobSettings['resolution_value'] = '3840x2160'

        elif layerJobSettings['resolution'] == 2:
            layerJobSettings['resolution_value'] = '1920x1080'

        elif layerJobSettings['resolution'] == 3:
            layerJobSettings['resolution_value'] = '1280x720'

        elif layerJobSettings['resolution'] == 4:
            layerJobSettings['resolution_value'] = '{}x{}'.format(self.renderingResolution['width'], self.renderingResolution['height'])

        elif layerJobSettings['resolution'] == 5:
            layerJobSettings['resolution_value'] = '{}x{}'.format(self.renderingResolution['width'] / 2, self.renderingResolution['height'] / 2)

        elif layerJobSettings['resolution'] == 6:
            layerJobSettings['resolution_value'] = '{}x{}'.format(self.renderingResolution['width'] / 4, self.renderingResolution['height'] / 4)

        else:
            pass

        if cmd.getAttr('vraySettings.cam_overscanMode') != 0:
            layerJobSettings['overscan'] = True

        return layerJobSettings

    ############################################################################
    # @brief      Gets the minimum range.
    #
    # @param      inputString  The input string
    #
    # @return     The minimum range.
    #
    def getMinimumRange(self, inputString):
        framesInfo = inputString.split(';')

        if len(framesInfo) > 1:
            minimum = 9999
            maximum = 1

            for frameInfo in framesInfo:
                if '-' in frameInfo:
                    startTest = int(frameInfo.split('-')[0])
                    endTest = int(frameInfo.split('-')[1])

                    if startTest < minimum:
                        minimum = startTest

                    if startTest > maximum:
                        maximum = startTest

                    if endTest < minimum:
                        minimum = endTest

                    if endTest > maximum:
                        maximum = endTest

                else:
                    testFrame = int(frameInfo)

                    if testFrame < minimum:
                        minimum = testFrame

                    if testFrame > maximum:
                        maximum = testFrame

            return '{}-{}'.format(minimum, maximum)
        else:
            return inputString

    ############################################################################
    # @brief      Gets the rbw elements to denoise.
    #
    def getRBWElementsToDenoise(self):
        elementsToDenoise = []

        defaultDenoise = api.vray.getRBWDefaultDenoisedElements()
        renderElements = api.vray.render_elements()
        for element in renderElements:
            if ':' not in element:
                if cmd.getAttr('{}.enabled'.format(element)):
                    if element in defaultDenoise:
                        elementsToDenoise.append(element)

        return elementsToDenoise

    ############################################################################
    # @brief      Sends a job to farm.
    #
    # @param      layer          The layer
    # @param      layerSettings  The layer settings
    # @param      savedShot      The saved shot
    #
    def sendJobToFarm(self, layer, layerSettings, savedShot):
        layerJobSettings = self.decodeLayerSettings(layer, layerSettings)
        shotCode = savedShot.shot.getShotCode()

        extraAttr = layerJobSettings['extraAttr']
        animType = layerJobSettings['animType']
        frames = layerJobSettings['frames']

        frameWidth = int(layerJobSettings['resolution_value'].split('x')[0])
        frameHeight = int(layerJobSettings['resolution_value'].split('x')[1])

        layerJobSettings['extraAttr'] = '{} res: {} x {}'.format(extraAttr, frameWidth, frameHeight)

        dependencyList = []

        batchName = '{}_RENDERING_{}'.format(shotCode, layerJobSettings['mode'].upper())
        kickOffBatchName = batchName.replace('RENDERING_{}'.format(layerJobSettings['mode'].upper()), 'KICKOFF')

        newImagePath = self.getFileName(layer, savedShot, 'rendering')
        version = newImagePath.split('/')[-2]

        jobName = '{}_{}_{}'.format(shotCode, layer, version)
        kickOffJobName = '{}_{}'.format(jobName, layerJobSettings['mode'].upper())

        vrsceneFilename = self.getFileName(layer, savedShot, 'vrscene')

        kickOffVrsceneFilename = vrsceneFilename.replace('{}.vrscene'.format(layer), '<Layer>.vrscene')

        kickOffJobSettings = layerJobSettings.copy()

        if animType == 2:
            kickOffJobSettings['frames'] = self.getMinimumRange(frames)

        kickOffJobSettings['jobName'] = kickOffJobName
        kickOffJobSettings['batchName'] = kickOffBatchName
        kickOffJobSettings['inputFileName'] = os.path.join(savedShot.savedShotFolder, '{}.ma'.format(savedShot.sceneName)).replace('\\', '/')
        kickOffJobSettings['outputFileName'] = kickOffVrsceneFilename
        kickOffJobSettings['goal'] = 'lighting vrscene caching'
        kickOffJobSettings['layer'] = layer
        kickOffJobSettings['dept'] = 'Lighting'
        kickOffJobSettings['obj'] = savedShot.getShotObj()

        kickOffJobId = api.farm.createAndSubmitJob(kickOffJobSettings)

        dependencyList.append(kickOffJobId)

        jobSettings = layerJobSettings.copy()
        jobSettings['dependency'] = dependencyList
        jobSettings['jobName'] = jobName
        jobSettings['batchName'] = batchName
        jobSettings['inputFileName'] = vrsceneFilename
        jobSettings['outputFileName'] = newImagePath
        jobSettings['goal'] = 'rendering'
        jobSettings['layer'] = layer
        jobSettings['dept'] = 'Lighting'
        jobSettings['obj'] = savedShot.getShotObj()

        renderJobId = api.farm.createAndSubmitJob(jobSettings)
        if jobSettings['renderDeep']:
            self.saveOnBD('DEEP_{}'.format(layer), savedShot, 'v{}'.format(str(jobSettings['deepVersion']).zfill(3)))

        if layerJobSettings['mode'] == 'ffr' and 'DEEP_' not in layer and 'SHADOW_' not in layer and 'SHADOWGRASS_' not in layer:
            elementsToDenoise = self.getRBWElementsToDenoise()
            elementList = 'RGB color'
            for element in elementsToDenoise:
                elementList = '{};{}'.format(elementList, api.vray.getRenderElementRealName(element))
            dependencyList.append(renderJobId)

            denoiseJobSettings = layerSettings.copy()
            denoiseJobSettings['dependency'] = dependencyList
            denoiseJobSettings['jobName'] = jobName
            denoiseJobSettings['batchName'] = batchName.replace('RENDERING_FFR', 'DENOISE')
            denoiseJobSettings['inputFileName'] = newImagePath.replace('.exr', '.????.exr')
            denoiseJobSettings['outputFileName'] = newImagePath
            denoiseJobSettings['elementsList'] = elementList
            denoiseJobSettings['goal'] = 'denoise'
            denoiseJobSettings['layer'] = layer
            denoiseJobSettings['dept'] = 'Lighting'
            denoiseJobSettings['denoiseMode'] = cmd.getAttr('vrayRE_Denoiser.vray_preset_denoiser')
            denoiseJobSettings['keepBeauty'] = layerJobSettings['keepBeauty']
            denoiseJobSettings['obj'] = savedShot.getShotObj()

            denoiseJobId = api.farm.createAndSubmitJob(denoiseJobSettings)

            if denoiseJobId:
                self.saveOnBD(layer, savedShot, version)

        else:
            if renderJobId:
                self.saveOnBD(layer, savedShot, version)
