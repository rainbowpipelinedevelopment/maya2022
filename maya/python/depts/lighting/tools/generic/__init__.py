import importlib
import os

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'common')


def run_manageRenderLayerOverrides(*args):
    import depts.lighting.tools.generic.manageRenderLayerOverrides.manageRenderLayerOverrides as manageRenderLayerOverrides
    importlib.reload(manageRenderLayerOverrides)

    win = manageRenderLayerOverrides.ManageRenderLayerOverridesUI()
    win.show()


manageRenderLayerOverridesTool = {
    'name': 'Manage RL Overrides',
    'launch': run_manageRenderLayerOverrides,
    'icon': os.path.join(iconDir, "option.png")
}


tools = [
    manageRenderLayerOverridesTool
]

PackageTool = {
    "name": "generic",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
