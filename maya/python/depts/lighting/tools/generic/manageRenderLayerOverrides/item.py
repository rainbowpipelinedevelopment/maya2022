import importlib
from PySide2 import QtWidgets, QtGui
import functools

import maya.cmds as cmd

import api.renderlayer
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.renderlayer)
# importlib.reload(rbwUI)


################################################################################
# @brief      This class describes a render layer item.
#
class RenderLayerItem(QtWidgets.QTreeWidgetItem):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      renderLayer  The render layer
    # @param      parent       The parent
    #
    def __init__(self, renderLayer, parent):
        super().__init__(parent)
        self.layerName = renderLayer

        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.nameLabel = rbwUI.RBWLabel(text=self.layerName, size=12, bold=False, italic=False, alignment='AlignVCenter')
        self.treeWidget().setItemWidget(self, 0, self.nameLabel)

    ############################################################################
    # @brief      fill the given list with the given items.
    #
    # @param      listWidget  The list widget
    # @param      items       The items
    #
    def fillList(self, listWidget, items):
        for item in items:
            listItem = QtWidgets.QListWidgetItem(item, listWidget)
            if not cmd.objExists(item):
                listItem.setForeground(QtGui.QBrush(QtGui.QColor("grey")))
                itemFont = listItem.font()
                itemFont.setStrikeOut(True)
                listItem.setFont(itemFont)

    ############################################################################
    # @brief      select the item from item text.
    #
    # @param      listWidget  The list widget
    #
    def selectItem(self, item):
        toSelect = item.text()
        cmd.select(clear=True)
        if cmd.objExists(toSelect):
            cmd.select(toSelect)


################################################################################
# @brief      This class describes a geo render layer item.
#
class GeoRenderLayerItem(RenderLayerItem):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, renderLayer, parent):
        super().__init__(renderLayer, parent)

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        super().initUI()

        if api.renderlayer.hasCollection('_geo_col', self.layerName):
            geoCol = api.renderlayer.getRenderLayerCollectionByName(self.layerName, 'geo_col')
            geoItems = geoCol.getSelector().getStaticSelection().split('\n')
            self.geoListWidget = QtWidgets.QListWidget()
            self.geoListWidget.itemDoubleClicked.connect(self.selectItem)
            self.geoListWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
            self.geoListWidget.setMaximumHeight(100)
            if geoItems != ['']:
                self.fillList(self.geoListWidget, geoItems)
            self.treeWidget().setItemWidget(self, 1, self.geoListWidget)
        else:
            self.notExistsGeoLabel = rbwUI.RBWLabel(text='Not Exists', size=12, bold=False, italic=False, alignment='AlignCenter')
            self.treeWidget().setItemWidget(self, 1, self.notExistsGeoLabel)

        if api.renderlayer.hasCollection('_geo_prune_col', self.layerName):
            geoPruneCol = api.renderlayer.getRenderLayerCollectionByName(self.layerName, 'geo_prune_col')
            geoPruneItems = geoPruneCol.getSelector().getStaticSelection().split('\n')
            self.geoPruneListWidget = QtWidgets.QListWidget()
            self.geoPruneListWidget.itemDoubleClicked.connect(self.selectItem)
            self.geoPruneListWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
            self.geoPruneListWidget.setMaximumHeight(100)
            if geoPruneItems != ['']:
                self.fillList(self.geoPruneListWidget, geoPruneItems)
            self.treeWidget().setItemWidget(self, 2, self.geoPruneListWidget)
        else:
            self.notExistsGeoPruneLabel = rbwUI.RBWLabel(text='Not Exists', size=12, bold=False, italic=False, alignment='AlignCenter')
            self.treeWidget().setItemWidget(self, 2, self.notExistsGeoPruneLabel)

    ############################################################################
    # @brief      Adds a geo.
    #
    def addGeo(self):
        selectedObject = cmd.ls(selection=True, long=True)
        if selectedObject:
            self.geoListWidget.addItems(selectedObject)
        self.apply()

    ############################################################################
    # @brief      Adds a geo prune.
    #
    def addGeoPrune(self):
        selectedObject = cmd.ls(selection=True, long=True)
        if selectedObject:
            self.geoPruneListWidget.addItems(selectedObject)
        self.apply()

    ############################################################################
    # @brief      Removes a geo.
    #
    def removeGeo(self):
        selectedTraceItems = self.geoListWidget.selectedItems()
        if selectedTraceItems:
            for selectedTraceItem in selectedTraceItems:
                self.geoListWidget.takeItem(self.geoListWidget.row(selectedTraceItem))
        self.apply()

    ############################################################################
    # @brief      Removes a geo prune.
    #
    def removeGeoPrune(self):
        selectedShaderItems = self.geoPruneListWidget.selectedItems()
        if selectedShaderItems:
            for selectedShaderItem in selectedShaderItems:
                self.geoPruneListWidget.takeItem(self.geoPruneListWidget.row(selectedShaderItem))
        self.apply()

    ############################################################################
    # @brief      Applies the object.
    #
    def apply(self):
        if api.renderlayer.hasCollection('_geo_col', self.layerName):
            itemList = []
            for x in range(self.geoListWidget.count()):
                item = self.geoListWidget.item(x).text()
                if item != '':
                    itemList.append(item)

            contentString = '\n'.join(itemList)
            geoCol = api.renderlayer.getRenderLayerCollectionByName(self.layerName, 'geo_col')
            geoCol.getSelector().setStaticSelection(contentString)

        if api.renderlayer.hasCollection('_geo_prune_col', self.layerName):
            itemList = []
            for x in range(self.geoPruneListWidget.count()):
                item = self.geoPruneListWidget.item(x).text()
                if item != '':
                    itemList.append(item)

            contentString = '\n'.join(itemList)
            geoPruneCol = api.renderlayer.getRenderLayerCollectionByName(self.layerName, 'geo_prune_col')
            geoPruneCol.getSelector().setStaticSelection(contentString)


################################################################################
# @brief      This class describes a vrscene render layer item.
#
class VrsceneRenderLayerItem(RenderLayerItem):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, renderLayer, parent):
        super().__init__(renderLayer, parent)

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        super().initUI()

        if api.renderlayer.hasCollection('_vrscene_col', self.layerName):
            vrsceneCol = api.renderlayer.getRenderLayerCollectionByName(self.layerName, 'vrscene_col')
            vrsceneItems = vrsceneCol.getSelector().getStaticSelection().split('\n')
            self.vrsceneListWidget = QtWidgets.QListWidget()
            self.vrsceneListWidget.itemDoubleClicked.connect(self.selectItem)
            self.vrsceneListWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
            self.vrsceneListWidget.setMaximumHeight(100)
            if vrsceneItems != ['']:
                self.fillList(self.vrsceneListWidget, vrsceneItems)
            self.treeWidget().setItemWidget(self, 1, self.vrsceneListWidget)
        else:
            self.notExistsVrsceneLabel = rbwUI.RBWLabel(text='Not Exists', size=12, bold=False, italic=False, alignment='AlignCenter')
            self.treeWidget().setItemWidget(self, 1, self.notExistsVrsceneLabel)

    ############################################################################
    # @brief      Adds a vrscene.
    #
    def addVrscene(self):
        selectedObject = cmd.ls(selection=True, long=True)
        if selectedObject:
            self.vrsceneListWidget.addItems(selectedObject)
        self.apply()

    ############################################################################
    # @brief      Removes a vrscene.
    #
    def removeVrscene(self):
        selectedTraceItems = self.vrsceneListWidget.selectedItems()
        if selectedTraceItems:
            for selectedTraceItem in selectedTraceItems:
                self.vrsceneListWidget.takeItem(self.vrsceneListWidget.row(selectedTraceItem))
        self.apply()

    ############################################################################
    # @brief      Applies the object.
    #
    def apply(self):
        if api.renderlayer.hasCollection('_vrscene_col', self.layerName):
            itemList = []
            for x in range(self.vrsceneListWidget.count()):
                item = self.vrsceneListWidget.item(x).text()
                if item != '':
                    itemList.append(item)

            contentString = '\n'.join(itemList)
            vrsceneCol = api.renderlayer.getRenderLayerCollectionByName(self.layerName, 'vrscene_col')
            vrsceneCol.getSelector().setStaticSelection(contentString)


################################################################################
# @brief      This class describes a trace render layer item.
#
class TraceRenderLayerItem(RenderLayerItem):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, renderLayer, parent):
        super().__init__(renderLayer, parent)

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        super().initUI()

        if api.renderlayer.hasCollection('_geo_trace_shader_override_col', self.layerName):
            traceShaderCol = api.renderlayer.getRenderLayerCollectionByName(self.layerName, 'geo_trace_shader_override_col')
            traceShaderItems = traceShaderCol.getSelector().getStaticSelection().split('\n')
            self.traceShaderListWidget = QtWidgets.QListWidget()
            self.traceShaderListWidget.itemDoubleClicked.connect(self.selectItem)
            self.traceShaderListWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
            self.traceShaderListWidget.setMaximumHeight(100)
            if traceShaderItems != ['']:
                self.fillList(self.traceShaderListWidget, traceShaderItems)
            self.treeWidget().setItemWidget(self, 1, self.traceShaderListWidget)
        else:
            self.notExistsTraceShaderLabel = rbwUI.RBWLabel(text='Not Exists', size=12, bold=False, italic=False, alignment='AlignCenter')
            self.treeWidget().setItemWidget(self, 1, self.notExistsTraceShaderLabel)

        if api.renderlayer.hasCollection('_geo_trace_col', self.layerName):
            traceCol = api.renderlayer.getRenderLayerCollectionByName(self.layerName, 'geo_trace_col')
            traceItems = traceCol.getSelector().getStaticSelection().split('\n')
            self.traceListWidget = QtWidgets.QListWidget()
            self.traceListWidget.itemDoubleClicked.connect(self.selectItem)
            self.traceListWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
            self.traceListWidget.setMaximumHeight(100)
            if traceItems != ['']:
                self.fillList(self.traceListWidget, traceItems)
            self.treeWidget().setItemWidget(self, 2, self.traceListWidget)
        else:
            self.notExistsTraceLabel = rbwUI.RBWLabel(text='Not Exists', size=12, bold=False, italic=False, alignment='AlignCenter')
            self.treeWidget().setItemWidget(self, 2, self.notExistsTraceLabel)

    ############################################################################
    # @brief      Add the object.
    #
    def addTrace(self):
        selectedObject = cmd.ls(selection=True, long=True)
        if selectedObject:
            self.traceListWidget.addItems(selectedObject)
        self.apply()

    ############################################################################
    # @brief      Adds an override.
    #
    def addOverride(self):
        selectedObject = cmd.ls(selection=True, long=True)
        api.log.logger().debug(selectedObject)
        if selectedObject:
            self.traceShaderListWidget.addItems(selectedObject)
        self.apply()

    ############################################################################
    # @brief      Removes the object.
    #
    def removeTrace(self):
        selectedTraceItems = self.traceListWidget.selectedItems()
        if selectedTraceItems:
            for selectedTraceItem in selectedTraceItems:
                self.traceListWidget.takeItem(self.traceListWidget.row(selectedTraceItem))
        self.apply()

    ############################################################################
    # @brief      Removes an override.
    #
    def removeOverride(self):
        selectedShaderItems = self.traceShaderListWidget.selectedItems()
        if selectedShaderItems:
            for selectedShaderItem in selectedShaderItems:
                self.traceShaderListWidget.takeItem(self.traceShaderListWidget.row(selectedShaderItem))
        self.apply()

    ############################################################################
    # @brief      Applies the object.
    #
    def apply(self):
        if api.renderlayer.hasCollection('_geo_trace_col', self.layerName):
            itemList = []
            for x in range(self.traceListWidget.count()):
                item = self.traceListWidget.item(x).text()
                if item != '':
                    itemList.append(item)

            contentString = '\n'.join(itemList)
            traceCol = api.renderlayer.getRenderLayerCollectionByName(self.layerName, 'geo_trace_col')
            traceCol.getSelector().setStaticSelection(contentString)

        if api.renderlayer.hasCollection('_geo_trace_shader_override_col', self.layerName):
            itemOverrideList = []
            wildCards = []
            for x in range(self.traceShaderListWidget.count()):
                shaderItem = self.traceShaderListWidget.item(x).text()
                if shaderItem != '':
                    itemOverrideList.append(shaderItem)
                    namespace = shaderItem.split('|')[-1].split(':')[0]
                    if namespace.split('_')[3] != 'RND':
                        wildCard = '*_switchOvr_{}{}{}_shader*'.format(namespace.split('_')[1], namespace.split('_')[2].capitalize(), namespace.split('_')[3][:-3])
                    else:
                        wildCard = '*_switchOvr_{}{}_shader*'.format(namespace.split('_')[1], namespace.split('_')[2].capitalize())

                    if wildCard not in wildCards:
                        wildCards.append(wildCard)

            traceShaderCol = api.renderlayer.getRenderLayerCollectionByName(self.layerName, 'geo_trace_shader_override_col')

            shaderContentString = '\n'.join(itemOverrideList)
            traceShaderCol.getSelector().setStaticSelection(shaderContentString)

            shaderWildCard = ';'.join(wildCards)
            traceShaderCol.getSelector().setPattern(shaderWildCard)


################################################################################
# @brief      This class describes a light render layer item.
#
class LightRenderLayerItem(RenderLayerItem):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, renderLayer, parent):
        self.sceneLights = cmd.listRelatives('LIGHTS', fullPath=True) if cmd.objExists('LIGHTS') else []
        super().__init__(renderLayer, parent)

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        super().initUI()

        if api.renderlayer.hasCollection('_lights_col', self.layerName) and api.renderlayer.hasCollection('_lights_prune_col', self.layerName):
            lightPruneCol = api.renderlayer.getRenderLayerCollectionByName(self.layerName, '_lights_prune_col')
            lightPruneCard = lightPruneCol.getSelector().getPattern()

            self.lightListWidget = QtWidgets.QListWidget()
            self.lightListWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
            self.lightListWidget.setMaximumHeight(100)

            if not lightPruneCard:
                self.lightListWidget.addItems(self.sceneLights)
            elif not lightPruneCard.startswith('|LIGHTS|*:*'):
                self.lightListWidget.addItems(self.sceneLights)
            else:
                if len(lightPruneCard) > 11:
                    for card in lightPruneCard.split('-')[1:]:
                        cardString = card[:-1] if card[-1] == '*' else card[:-2]
                        for light in self.sceneLights:
                            if cardString in light:
                                self.lightListWidget.addItem(light)
                else:
                    self.lightListWidget.addItems(self.sceneLights)

            self.treeWidget().setItemWidget(self, 1, self.lightListWidget)
        else:
            self.notExistsLabel = rbwUI.RBWLabel(text='Not Exists', size=12, bold=False, italic=False, alignment='AlignCenter')
            self.treeWidget().setItemWidget(self, 1, self.notExistsLabel)

    ############################################################################
    # @brief      Adds a light.
    #
    def addLight(self):
        selectedObject = cmd.ls(selection=True, long=True)
        if selectedObject:
            self.lightListWidget.addItems(selectedObject)
        self.apply()

    ############################################################################
    # @brief      Removes a light.
    #
    def removeLight(self):
        selectedTraceItems = self.lightListWidget.selectedItems()
        if selectedTraceItems:
            for selectedTraceItem in selectedTraceItems:
                self.lightListWidget.takeItem(self.lightListWidget.row(selectedTraceItem))
        self.apply()

    ############################################################################
    # @brief      Applies the object.
    #
    def apply(self):
        if api.renderlayer.hasCollection('_lights_col', self.layerName) and api.renderlayer.hasCollection('_lights_prune_col', self.layerName):
            itemList = []
            for x in range(self.lightListWidget.count()):
                item = self.lightListWidget.item(x).text()
                if item != '':
                    itemList.append(item)

            pattern = '|LIGHTS|*:*'
            for item in itemList:
                pattern = '{} -{}:*'.format(pattern, item.split(':')[0])

            lightPruneCol = api.renderlayer.getRenderLayerCollectionByName(self.layerName, '_lights_prune_col')
            lightPruneCol.getSelector().setPattern(pattern)


################################################################################
# @brief      This class describes a bh render layer item.
#
class BHRenderLayerItem(RenderLayerItem):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, renderLayer, parent):
        super().__init__(renderLayer, parent)

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        super().initUI()

        if api.renderlayer.hasBHCollection(self.layerName):
            bhCol = api.renderlayer.getRenderLayerCollectionByName(self.layerName, '{}_BH_col'.format(self.layerName))
            bhItems = bhCol.getSelector().getStaticSelection().split('\n')
            self.bhListWidget = QtWidgets.QListWidget()
            self.bhListWidget.itemDoubleClicked.connect(self.selectItem)
            self.bhListWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
            self.bhListWidget.setMaximumHeight(100)
            if bhItems != ['']:
                self.fillList(self.bhListWidget, bhItems)
            self.treeWidget().setItemWidget(self, 1, self.bhListWidget)
        else:
            self.createBhButton = rbwUI.RBWButton(text='Create BH', icon=['add.png'], size=[150, 35])
            self.createBhButton.clicked.connect(self.createBh)
            self.treeWidget().setItemWidget(self, 1, self.createBhButton)

    ############################################################################
    # @brief      Creates a bh.
    #
    def createBh(self):
        api.renderlayer.createBlackHoleCollectionForRenderLayer(self.layerName)
        self.initUI()

    ############################################################################
    # @brief      Add the object.
    #
    def add(self):
        selectedObject = cmd.ls(selection=True, long=True)
        if selectedObject:
            self.bhListWidget.addItems(selectedObject)
        self.apply()

    ############################################################################
    # @brief      Removes the object.
    #
    def remove(self):
        selectedTraceItems = self.bhListWidget.selectedItems()
        if selectedTraceItems:
            for selectedTraceItem in selectedTraceItems:
                self.bhListWidget.takeItem(self.bhListWidget.row(selectedTraceItem))
        self.apply()

    ############################################################################
    # @brief      Applies the object.
    #
    def apply(self):
        if api.renderlayer.hasBHCollection(self.layerName):
            itemList = []
            wildCards = []
            for x in range(self.bhListWidget.count()):
                item = self.bhListWidget.item(x).text()
                if item != '':
                    itemList.append(item)
                    namespace = item.split('|')[-1].split(':')[0]
                    if namespace.split('_')[3] != 'RND':
                        wildCard = '*_araknoWrapper_{}{}{}_shader*'.format(namespace.split('_')[1], namespace.split('_')[2].capitalize(), namespace.split('_')[3][:-3])
                    else:
                        wildCard = '*_araknoWrapper_{}{}_shader*'.format(namespace.split('_')[1], namespace.split('_')[2].capitalize())

                    if wildCard not in wildCards:
                        wildCards.append(wildCard)

            bhCol = api.renderlayer.getRenderLayerCollectionByName(self.layerName, '{}_BH_col'.format(self.layerName))

            contentString = '\n'.join(itemList)
            bhCol.getSelector().setStaticSelection(contentString)

            contentWildCard = ';'.join(wildCards)
            bhCol.getSelector().setPattern(contentWildCard)


################################################################################
# @brief      This class describes a shadow render layer item.
#
class ShadowRenderLayerItem(RenderLayerItem):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, renderLayer, parent):
        super().__init__(renderLayer, parent)

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        super().initUI()

        if api.renderlayer.hasCollection('_caster_col', self.layerName):
            casterCol = api.renderlayer.getRenderLayerCollectionByName(self.layerName, 'caster_col')
            casterItems = casterCol.getSelector().getStaticSelection().split('\n')
            self.casterListWidget = QtWidgets.QListWidget()
            self.casterListWidget.itemDoubleClicked.connect(self.selectItem)
            self.casterListWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
            self.casterListWidget.setMaximumHeight(100)
            if casterItems != ['']:
                self.fillList(self.casterListWidget, casterItems)
            self.treeWidget().setItemWidget(self, 1, self.casterListWidget)
        else:
            self.notExistsCasterLabel = rbwUI.RBWLabel(text='Not Exists', size=12, bold=False, italic=False, alignment='AlignCenter')
            self.treeWidget().setItemWidget(self, 1, self.notExistsCasterLabel)

        if api.renderlayer.hasCollection('_catcher_col', self.layerName):
            catcherCol = api.renderlayer.getRenderLayerCollectionByName(self.layerName, 'catcher_col')
            catcherItems = catcherCol.getSelector().getStaticSelection().split('\n')
            self.catcherListWidget = QtWidgets.QListWidget()
            self.catcherListWidget.itemDoubleClicked.connect(self.selectItem)
            self.catcherListWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
            self.catcherListWidget.setMaximumHeight(100)
            if catcherItems != ['']:
                self.fillList(self.catcherListWidget, catcherItems)
            self.treeWidget().setItemWidget(self, 2, self.catcherListWidget)
        else:
            self.notExistsCatcherPruneLabel = rbwUI.RBWLabel(text='Not Exists', size=12, bold=False, italic=False, alignment='AlignCenter')
            self.treeWidget().setItemWidget(self, 2, self.notExistsCatcherPruneLabel)

    ############################################################################
    # @brief      Adds a caster.
    #
    def addCaster(self):
        selectedObject = cmd.ls(selection=True, long=True)
        if selectedObject:
            self.casterListWidget.addItems(selectedObject)
        self.apply()

    ############################################################################
    # @brief      Adds a catcher.
    #
    def addCatcher(self):
        selectedObject = cmd.ls(selection=True, long=True)
        api.log.logger().debug(selectedObject)
        if selectedObject:
            self.catcherListWidget.addItems(selectedObject)
        self.apply()

    ############################################################################
    # @brief      Removes a caster.
    #
    def removeCaster(self):
        selectedTraceItems = self.casterListWidget.selectedItems()
        if selectedTraceItems:
            for selectedTraceItem in selectedTraceItems:
                self.casterListWidget.takeItem(self.casterListWidget.row(selectedTraceItem))
        self.apply()

    ############################################################################
    # @brief      Removes a catcher.
    #
    def removeCatcher(self):
        selectedShaderItems = self.catcherListWidget.selectedItems()
        if selectedShaderItems:
            for selectedShaderItem in selectedShaderItems:
                self.catcherListWidget.takeItem(self.catcherListWidget.row(selectedShaderItem))
        self.apply()

    ############################################################################
    # @brief      Applies the object.
    #
    def apply(self):
        if api.renderlayer.hasCollection('_caster_col', self.layerName):
            itemList = []
            for x in range(self.casterListWidget.count()):
                item = self.casterListWidget.item(x).text()
                if item != '':
                    itemList.append(item)

            contentString = '\n'.join(itemList)
            casterCol = api.renderlayer.getRenderLayerCollectionByName(self.layerName, 'caster_col')
            casterCol.getSelector().setStaticSelection(contentString)

        if api.renderlayer.hasCollection('_catcher_col', self.layerName):
            itemList = []
            wildCards = []
            for x in range(self.catcherListWidget.count()):
                item = self.catcherListWidget.item(x).text()
                if item != '':
                    itemList.append(item)
                    namespace = item.split('|')[-1].split(':')[0]
                    if namespace.split('_')[3] != 'RND':
                        wildCard = '*_araknoWrapper_{}{}{}_shader*'.format(namespace.split('_')[1], namespace.split('_')[2].capitalize(), namespace.split('_')[3][:-3])
                    else:
                        wildCard = '*_araknoWrapper_{}{}_shader*'.format(namespace.split('_')[1], namespace.split('_')[2].capitalize())

                    if wildCard not in wildCards:
                        wildCards.append(wildCard)

            catcherCol = api.renderlayer.getRenderLayerCollectionByName(self.layerName, 'catcher_col')

            contentString = '\n'.join(itemList)
            catcherCol.getSelector().setStaticSelection(contentString)

            contentWildCard = ';'.join(wildCards)
            catcherCol.getSelector().setPattern(contentWildCard)
