import importlib
import os
from PySide2 import QtWidgets

import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI
import api.renderlayer
import depts.lighting.tools.generic.manageRenderLayerOverrides.item as item

# importlib.reload(rbwUI)
importlib.reload(api.renderlayer)
importlib.reload(item)


################################################################################
# @brief      This class describes a frame tab.
#
class FrameTab(rbwUI.RBWFrame):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      treeHeader  The tree header
    # @param      treeSize    The tree size
    #
    def __init__(self, treeHeader, treeSize):
        super().__init__(layout='V', margins=[5, 5, 5, 5], radius=5, bgColor='transparent')

        self.treeHeader = treeHeader
        self.treeSize = treeSize

        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        ########################################################################
        # TOP AREA

        # render layer tree
        self.renderLayerTree = rbwUI.RBWTreeWidget(fields=self.treeHeader, size=self.treeSize, darkMode=True)
        self.renderLayerTree.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

        self.addWidget(self.renderLayerTree)

        ########################################################################
        # BUTTONS AREA

        # buttons widget
        self.buttonsWidget = rbwUI.RBWFrame(layout='H', margins=[5, 5, 5, 5], radius=5)

        self.updateListButton = rbwUI.RBWButton(text='Update List', size=[150, 75], icon=['refresh.png'])
        self.updateListButton.clicked.connect(self.fillTree)
        self.buttonsWidget.addWidget(self.updateListButton)

        self.addWidget(self.buttonsWidget)


################################################################################
# @brief      This class describes a geo tab.
#
class GeoTab(FrameTab):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super().__init__(treeHeader=['Render Layer', 'Geo Collection', 'Geo Prune Collection'], treeSize=[155, 510, 510])

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        super().initUI()

        # geo buttons
        self.geoButtonWidget = rbwUI.RBWGroupBox(title='Geo', layout='H', fontSize=12, margins=[12, 20, 12, 12])

        self.addButton = rbwUI.RBWButton(text='Add Geo', size=[None, 35], icon=['add.png'])
        self.addButton.clicked.connect(self.addGeo)
        self.removeButton = rbwUI.RBWButton(text='Remove Geo', size=[None, 35], icon=['remove.png'])
        self.removeButton.clicked.connect(self.removeGeo)

        self.geoButtonWidget.addWidget(self.addButton)
        self.geoButtonWidget.addWidget(self.removeButton)

        # geo prune button
        self.geoPruneButtonWidget = rbwUI.RBWGroupBox(title='Geo Prune', layout='H', fontSize=12, margins=[12, 20, 12, 12])

        self.addPruneButton = rbwUI.RBWButton(text='Add Prune', size=[None, 35], icon=['add.png'])
        self.addPruneButton.clicked.connect(self.addGeoPrune)
        self.removePruneButton = rbwUI.RBWButton(text='Remove Prune', size=[None, 35], icon=['remove.png'])
        self.removePruneButton.clicked.connect(self.removeGeoPrune)

        self.geoPruneButtonWidget.addWidget(self.addPruneButton)
        self.geoPruneButtonWidget.addWidget(self.removePruneButton)

        self.buttonsWidget.addWidget(self.geoButtonWidget)
        self.buttonsWidget.addWidget(self.geoPruneButtonWidget)

    ############################################################################
    # @brief      fill the tree
    #
    def fillTree(self):
        self.renderLayerTree.clear()
        for layer in api.renderlayer.list():
            item.GeoRenderLayerItem(layer, self.renderLayerTree)

    ############################################################################
    # @brief      Adds a geo.
    #
    def addGeo(self):
        for layerItem in self.renderLayerTree.selectedItems():
            layerItem.addGeo()

    ############################################################################
    # @brief      Removes a geo.
    #
    def removeGeo(self):
        for layerItem in self.renderLayerTree.selectedItems():
            layerItem.removeGeo()

    ############################################################################
    # @brief      Adds a geo prune.
    #
    def addGeoPrune(self):
        for layerItem in self.renderLayerTree.selectedItems():
            layerItem.addGeoPrune()

    ############################################################################
    # @brief      Removes a geo prune.
    #
    def removeGeoPrune(self):
        for layerItem in self.renderLayerTree.selectedItems():
            layerItem.removeGeoPrune()


################################################################################
# @brief      This class describes a vrscene tab.
#
class VrsceneTab(FrameTab):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super().__init__(treeHeader=['Render Layer', 'Vrscene Collection'], treeSize=[155, 510])

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        super().initUI()

        # vrscene buttons
        self.vrsceneButtonWidget = rbwUI.RBWGroupBox(title='Vrscene', layout='H', fontSize=12, margins=[12, 20, 12, 12])

        self.addButton = rbwUI.RBWButton(text='Add Vrscene', size=[None, 35], icon=['add.png'])
        self.addButton.clicked.connect(self.addVrscene)
        self.removeButton = rbwUI.RBWButton(text='Remove Vrscene', size=[None, 35], icon=['remove.png'])
        self.removeButton.clicked.connect(self.removeVrscene)

        self.vrsceneButtonWidget.addWidget(self.addButton)
        self.vrsceneButtonWidget.addWidget(self.removeButton)

        self.buttonsWidget.addWidget(self.vrsceneButtonWidget)

    ############################################################################
    # @brief      fill the tree
    #
    def fillTree(self):
        self.renderLayerTree.clear()
        for layer in api.renderlayer.list():
            item.VrsceneRenderLayerItem(layer, self.renderLayerTree)

    ############################################################################
    # @brief      Adds a vrscene.
    #
    def addVrscene(self):
        for layerItem in self.renderLayerTree.selectedItems():
            layerItem.addVrscene()

    ############################################################################
    # @brief      Removes a vrscene.
    #
    def removeVrscene(self):
        for layerItem in self.renderLayerTree.selectedItems():
            layerItem.removeVrscene()


################################################################################
# @brief      This class describes a trace tab.
#
class TraceTab(FrameTab):

    def __init__(self):
        super().__init__(treeHeader=['Render Layer', 'Trace Override Collection', 'Trace Collection'], treeSize=[155, 510, 510])

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        super().initUI()

        # overrides button
        self.overrideButtonWidget = rbwUI.RBWGroupBox(title='Override', layout='H', fontSize=12, margins=[12, 20, 12, 12])

        self.addOverrideButton = rbwUI.RBWButton(text='Add Override', size=[None, 35], icon=['add.png'])
        self.addOverrideButton.clicked.connect(self.addOverride)
        self.removeOverrideButton = rbwUI.RBWButton(text='Remove Override', size=[None, 35], icon=['remove.png'])
        self.removeOverrideButton.clicked.connect(self.removeOverride)

        self.overrideButtonWidget.addWidget(self.addOverrideButton)
        self.overrideButtonWidget.addWidget(self.removeOverrideButton)

        # original buttons
        self.originalButtonWidget = rbwUI.RBWGroupBox(title='Original', layout='H', fontSize=12, margins=[12, 20, 12, 12])

        self.addButton = rbwUI.RBWButton(text='Add Original', size=[None, 35], icon=['add.png'])
        self.addButton.clicked.connect(self.addTrace)
        self.removeButton = rbwUI.RBWButton(text='Remove Original', size=[None, 35], icon=['remove.png'])
        self.removeButton.clicked.connect(self.removeTrace)

        self.originalButtonWidget.addWidget(self.addButton)
        self.originalButtonWidget.addWidget(self.removeButton)

        self.buttonsWidget.addWidget(self.overrideButtonWidget)
        self.buttonsWidget.addWidget(self.originalButtonWidget)

    ############################################################################
    # @brief      fill the tree
    #
    def fillTree(self):
        self.renderLayerTree.clear()
        for layer in api.renderlayer.list():
            item.TraceRenderLayerItem(layer, self.renderLayerTree)

    ############################################################################
    # @brief      Adds a trace.
    #
    def addTrace(self):
        for layerItem in self.renderLayerTree.selectedItems():
            layerItem.addTrace()

    ############################################################################
    # @brief      Removes a trace.
    #
    def removeTrace(self):
        for layerItem in self.renderLayerTree.selectedItems():
            layerItem.removeTrace()

    ############################################################################
    # @brief      Adds an override.
    #
    def addOverride(self):
        for layerItem in self.renderLayerTree.selectedItems():
            layerItem.addOverride()

    ############################################################################
    # @brief      Removes an override.
    #
    def removeOverride(self):
        for layerItem in self.renderLayerTree.selectedItems():
            layerItem.removeOverride()


################################################################################
# @brief      This class describes a lights tab.
#
class LightsTab(FrameTab):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super().__init__(treeHeader=['Render Layer', 'Lights Collection'], treeSize=[155, 510])

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        super().initUI()

        # lights buttons
        self.lightsButtonWidget = rbwUI.RBWGroupBox(title='Lights', layout='H', fontSize=12, margins=[12, 20, 12, 12])

        self.addButton = rbwUI.RBWButton(text='Add Light', size=[None, 35], icon=['add.png'])
        self.addButton.clicked.connect(self.addLight)
        self.removeButton = rbwUI.RBWButton(text='Remove Light', size=[None, 35], icon=['remove.png'])
        self.removeButton.clicked.connect(self.removeLight)

        self.lightsButtonWidget.addWidget(self.addButton)
        self.lightsButtonWidget.addWidget(self.removeButton)

        self.buttonsWidget.addWidget(self.lightsButtonWidget)

    ############################################################################
    # @brief      fill the tree
    #
    def fillTree(self):
        self.renderLayerTree.clear()
        for layer in api.renderlayer.list():
            if not layer.startswith('DOME'):
                item.LightRenderLayerItem(layer, self.renderLayerTree)

    ############################################################################
    # @brief      Adds a light.
    #
    def addLight(self):
        for layerItem in self.renderLayerTree.selectedItems():
            layerItem.addLight()

    ############################################################################
    # @brief      Removes a light.
    #
    def removeLight(self):
        for layerItem in self.renderLayerTree.selectedItems():
            layerItem.removeLight()


################################################################################
# @brief      This class describes a black hole tab.
#
class BlackHoleTab(FrameTab):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super().__init__(treeHeader=['Render Layer', 'Black Hole Collection'], treeSize=[155, 510])

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        super().initUI()

        # bh buttons
        self.bhButtonWidget = rbwUI.RBWGroupBox(title='Black Hole', layout='H', fontSize=12, margins=[12, 20, 12, 12])

        self.addButton = rbwUI.RBWButton(text='Add BH', size=[None, 35], icon=['add.png'])
        self.addButton.clicked.connect(self.add)
        self.removeButton = rbwUI.RBWButton(text='Remove BH', size=[None, 35], icon=['remove.png'])
        self.removeButton.clicked.connect(self.remove)

        self.bhButtonWidget.addWidget(self.addButton)
        self.bhButtonWidget.addWidget(self.removeButton)

        self.buttonsWidget.addWidget(self.bhButtonWidget)

    ############################################################################
    # @brief      fill the tree
    #
    def fillTree(self):
        self.renderLayerTree.clear()
        for layer in api.renderlayer.list():
            item.BHRenderLayerItem(layer, self.renderLayerTree)

    ############################################################################
    # @brief      Add the object.
    #
    def add(self):
        for layerItem in self.renderLayerTree.selectedItems():
            layerItem.add()

    ############################################################################
    # @brief      Removes the object.
    #
    def remove(self):
        for layerItem in self.renderLayerTree.selectedItems():
            layerItem.remove()


################################################################################
# @brief      This class describes a shadow tab.
#
class ShadowTab(FrameTab):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super().__init__(treeHeader=['Render Layer', 'Caster Collection', 'Catcher Collection'], treeSize=[155, 510, 510])

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        super().initUI()

        # caster buttons
        self.casterButtonWidget = rbwUI.RBWGroupBox(title='Caster', layout='H', fontSize=12, margins=[12, 20, 12, 12])

        self.addCasterButton = rbwUI.RBWButton(text='Add Caster', size=[None, 35], icon=['add.png'])
        self.addCasterButton.clicked.connect(self.addCaster)
        self.removeCasterButton = rbwUI.RBWButton(text='Remove Caster', size=[None, 35], icon=['remove.png'])
        self.removeCasterButton.clicked.connect(self.removeCaster)

        self.casterButtonWidget.addWidget(self.addCasterButton)
        self.casterButtonWidget.addWidget(self.removeCasterButton)

        # Catcher button
        self.catcherButtonWidget = rbwUI.RBWGroupBox(title='Catcher', layout='H', fontSize=12, margins=[12, 20, 12, 12])

        self.addCatcherButton = rbwUI.RBWButton(text='Add Catcher', size=[None, 35], icon=['add.png'])
        self.addCatcherButton.clicked.connect(self.addCatcher)
        self.removeCatcherButton = rbwUI.RBWButton(text='Remove Catcher', size=[None, 35], icon=['remove.png'])
        self.removeCatcherButton.clicked.connect(self.removeCatcher)

        self.catcherButtonWidget.addWidget(self.addCatcherButton)
        self.catcherButtonWidget.addWidget(self.removeCatcherButton)

        self.buttonsWidget.addWidget(self.casterButtonWidget)
        self.buttonsWidget.addWidget(self.catcherButtonWidget)

    ############################################################################
    # @brief      fill the tree
    #
    def fillTree(self):
        self.renderLayerTree.clear()
        for layer in api.renderlayer.list():
            if layer.startswith('SHADOW'):
                item.ShadowRenderLayerItem(layer, self.renderLayerTree)

    ############################################################################
    # @brief      Adds a caster.
    #
    def addCaster(self):
        for layerItem in self.renderLayerTree.selectedItems():
            layerItem.addCaster()

    ############################################################################
    # @brief      Removes a caster.
    #
    def removeCaster(self):
        for layerItem in self.renderLayerTree.selectedItems():
            layerItem.removeCaster()

    ############################################################################
    # @brief      Adds a catcher.
    #
    def addCatcher(self):
        for layerItem in self.renderLayerTree.selectedItems():
            layerItem.addCatcher()

    ############################################################################
    # @brief      Removes a catcher.
    #
    def removeCatcher(self):
        for layerItem in self.renderLayerTree.selectedItems():
            layerItem.removeCatcher()
