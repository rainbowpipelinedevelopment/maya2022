import importlib
import os
from PySide2 import QtWidgets

import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI
import api.renderlayer
import depts.lighting.tools.generic.manageRenderLayerOverrides.frameTab as frameTab

# importlib.reload(rbwUI)
importlib.reload(api.renderlayer)
importlib.reload(frameTab)


################################################################################
# @brief      This class describes a manage render layer overrides ui.
#
class ManageRenderLayerOverridesUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      parent  The parent
    #
    def __init__(self, parent=None):
        super().__init__()
        if cmd.window('ManageRenderLayerOverridesUI', exists=True):
            cmd.deleteUI('ManageRenderLayerOverridesUI')

        self.id_mv = os.getenv('ID_MV')
        self.mcFolder = os.getenv('MC_FOLDER')
        self.animationFolder = os.path.join(self.mcFolder, 'scenes', 'animation').replace('\\', '/')

        self.setObjectName('ManageRenderLayerOverridesUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.tabWidget = rbwUI.RBWTabWidget(12)
        self.mainLayout.addWidget(self.tabWidget)

        # costruzione delle varie UI per i tab
        self.geoTab = frameTab.GeoTab()
        self.vrsceneTab = frameTab.VrsceneTab()
        self.traceTab = frameTab.TraceTab()
        self.lightsTab = frameTab.LightsTab()
        self.bhTab = frameTab.BlackHoleTab()
        self.shadowTab = frameTab.ShadowTab()

        self.tabWidget.addTab(self.geoTab, 'Geo')
        self.tabWidget.addTab(self.vrsceneTab, 'Vrscene')
        self.tabWidget.addTab(self.traceTab, 'Trace')
        self.tabWidget.addTab(self.lightsTab, 'Lights')
        self.tabWidget.addTab(self.bhTab, 'Black Hole')
        self.tabWidget.addTab(self.shadowTab, 'Shadow')

        self.tabWidget.currentChanged.connect(self.changeWidget)

        self.tabWidget.currentWidget().fillTree()

        self.setFixedSize(1200, 600)
        self.setTitle('Render Layer Overrides UI')
        self.setFocus()

    ############################################################################
    # @brief      change active widget.
    #
    def changeWidget(self):
        currentWidget = self.tabWidget.currentWidget()
        currentWidget.fillTree()
