import importlib
import os

import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'common')


def run_giveMeRnd(*args):
    import common.switchAssembly as switchAssembly
    importlib.reload(switchAssembly)

    selection = cmd.ls(selection=True, type='assemblyReference')
    if selection:
        for obj in selection:
            if obj.split('_')[2].endswith('block'):
                rbwUI.RBWError(title='Bad Selection', text='You select one or more block assemblies.')
                return

    switchAssembly.getRepresentationInScene(representationName='surfacing', onlyImport=True, deleteSource=False, world=False, reference=True, approvation='def')


def run_giveMeVrMesh(*args):
    import common.switchAssembly as switchAssembly
    importlib.reload(switchAssembly)

    switchAssembly.getRepresentationInScene(representationName='vrmesh', onlyImport=True, deleteSource=False, world=False, reference=True, approvation='def')


def run_giveMeFx(*args):
    import common.switchAssembly as switchAssembly
    importlib.reload(switchAssembly)

    switchAssembly.getRepresentationInScene(dept='fx', deptType='rnd', onlyImport=True, deleteSource=False, world=False, reference=True, approvation='def')


def run_giveMeSet(*args):
    import common.switchAssembly as switchAssembly
    importlib.reload(switchAssembly)

    switchAssembly.getSavedAssetFromAssembly(dept='set', deptType='hig', deleteSource=False, world=False, reference=False, approvation='def')


def run_giveMeAssembly(*args):
    import common.switchAssembly as switchAssembly
    importlib.reload(switchAssembly)

    switchAssembly.getAssemblyFromReference(deleteSource=False, parentAssembly=False)


def run_giveMeMod(*args):
    import common.switchAssembly as switchAssembly
    importlib.reload(switchAssembly)

    selection = cmd.ls(selection=True, type='assemblyReference')
    if selection:
        for obj in selection:
            if obj.split('_')[2].endswith('block'):
                rbwUI.RBWError(title='Bad Selection', text='You select one or more block assemblies.')
                return

    switchAssembly.getRepresentationInScene(representationName='cache base', onlyImport=True, deleteSource=False, world=False, reference=True, approvation='def')


giveMeRndTool = {
    'name': 'Give Me RND',
    'launch': run_giveMeRnd
}

giveMeVrmeshTool = {
    'name': 'Give Me VrMesh',
    'launch': run_giveMeVrMesh
}

giveMeFxTool = {
    'name': 'Give Me Fx',
    'launch': run_giveMeFx
}

giveMeSetTool = {
    'name': 'Give Me Set',
    'launch': run_giveMeSet
}

giveMeAssemblyTool = {
    'name': 'Give Me Assembly',
    'launch': run_giveMeAssembly
}

giveMeModTool = {
    'name': 'Give Me Modeling',
    'launch': run_giveMeMod
}


tools = [
    giveMeRndTool,
    giveMeVrmeshTool,
    giveMeFxTool,
    giveMeSetTool,
    giveMeAssemblyTool,
    giveMeModTool
]

PackageTool = {
    "name": "giveMe",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
