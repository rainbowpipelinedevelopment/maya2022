import importlib
import os
import functools
from PySide2 import QtWidgets, QtCore

import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI
import api.log
import api.renderlayer
import depts.lighting.tools.sceneSetup.cleanUpMl as cleanUpMl

# importlib.reload(rbwUI)
importlib.reload(api.log)
importlib.reload(api.renderlayer)
importlib.reload(cleanUpMl)


################################################################################
# @brief      This class describes a clean up ml ui.
#
class CleanUpMlUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      parent  The parent
    #
    def __init__(self, parent=None):
        super().__init__()
        if cmd.window('CleanUpMlUI', exists=True):
            cmd.deleteUI('CleanUpMlUI')

        self.templateLayerFolder = os.path.join(
            os.getenv('LOCALPIPE'),
            'json_file'
        ).replace('\\', '/')

        self.getLayers()

        self.setObjectName('CleanUpMlUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topWidget = rbwUI.RBWFrame(layout='V', radius=5)
        self.mainLayout.addWidget(self.topWidget)

        self.renderLayerTree = rbwUI.RBWTreeWidget(fields=['Layer', 'Clean ML'], size=[300, 60])
        self.renderLayerTree.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.topWidget.addWidget(self.renderLayerTree)

        self.cleanSeletedButton = rbwUI.RBWButton(text='Clean Selected', size=[150, 35], icon=['clear.png'])
        self.cleanSeletedButton.clicked.connect(self.cleanUpSelectedMl)
        self.topWidget.addWidget(self.cleanSeletedButton, alignment=QtCore.Qt.AlignCenter)

        self.fillTree()

        self.setMinimumSize(400, 300)
        self.setTitle('Clean Up ML UI')
        self.setFocus()

    ############################################################################
    # @brief      Gets the layers.
    #
    def getLayers(self):
        reList = api.renderlayer.list()
        self.layers = []

        for layer in reList:
            if 'DEEP' not in layer and 'SHADOW' not in layer:
                self.layers.append(layer)

    ############################################################################
    # @brief      fill tree.
    #
    def fillTree(self):
        for layer in self.layers:
            child = QtWidgets.QTreeWidgetItem(self.renderLayerTree)

            layerLabel = rbwUI.RBWLabel(text=layer, size=13, italic=False, bold=False)
            self.renderLayerTree.setItemWidget(child, 0, layerLabel)

            cleanButton = rbwUI.RBWButton(icon=['clear.png'], size=[None, 18])
            cleanButton.clicked.connect(functools.partial(self.cleanUpMl, layer))
            self.renderLayerTree.setItemWidget(child, 1, cleanButton)

    ############################################################################
    # @brief      { function_description }
    #
    # @param      layer  The layer
    #
    def cleanUpMl(self, layer):
        cleanUpMl.turnOffUnusedPass(layer)

    ############################################################################
    # @brief      { function_description }
    #
    def cleanUpSelectedMl(self):
        items = self.renderLayerTree.selectedItems()
        for item in items:
            button = self.renderLayerTree.itemWidget(item, 1)
            button.click()
