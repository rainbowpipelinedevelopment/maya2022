import importlib
import os
from PySide2 import QtWidgets, QtCore, QtGui

import maya.cmds as cmd
import maya.mel as mel

import api.widgets.rbw_UI as rbwUI
import api.log
import api.system
import api.json
import depts.lighting.tools.sceneSetup.recache.recacheFromScratch as recacheFromScratch
import depts.lighting.tools.sceneSetup.recache.recacheConvertion as recacheConvertion

# importlib.reload(rbwUI)
importlib.reload(api.log)
importlib.reload(api.system)
importlib.reload(api.json)
importlib.reload(recacheFromScratch)


################################################################################
# @brief      This class describes a recache ui.
#
class RecacheUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      parent  The parent
    #
    def __init__(self, parent=None):
        super().__init__()
        if cmd.window('RecacheUI', exists=True):
            cmd.deleteUI('RecacheUI')

        self.id_mv = os.getenv('ID_MV')
        self.shots = api.system.getShotsFromDB()
        self.openShotObj = api.scene.getShotByScene()

        self.setObjectName('RecacheUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topWidget = rbwUI.RBWFrame(layout='V', margins=[5, 5, 5, 5], radius=5)
        self.mainLayout.addWidget(self.topWidget)

        self.shotGroup = rbwUI.RBWGroupBox(title='Shot', layout='V', fontSize=12)

        self.seasonComboBox = rbwUI.RBWComboBox(text='Season', bgColor='transparent')
        self.seasonComboBox.activated.connect(self.fillEpisodeComboBox)

        self.episodeComboBox = rbwUI.RBWComboBox(text='Episode', bgColor='transparent')
        self.episodeComboBox.activated.connect(self.fillSequenceComboBox)

        self.sequenceComboBox = rbwUI.RBWComboBox(text='Sequence', bgColor='transparent')
        self.sequenceComboBox.activated.connect(self.fillShotComboBox)

        self.shotComboBox = rbwUI.RBWComboBox(text='Shot', bgColor='transparent')
        self.shotComboBox.activated.connect(self.setCurrentShot)

        self.shotGroup.addWidget(self.seasonComboBox)
        self.shotGroup.addWidget(self.episodeComboBox)
        self.shotGroup.addWidget(self.sequenceComboBox)
        self.shotGroup.addWidget(self.shotComboBox)

        self.topWidget.addWidget(self.shotGroup)

        self.modeGroup = rbwUI.RBWGroupBox(title='Mode', layout='H', fontSize=12)

        self.convertionRadioButton = QtWidgets.QRadioButton('Convertion')
        self.fromScratchRadioButton = QtWidgets.QRadioButton('From Scratch')

        self.modeGroup.addWidget(self.convertionRadioButton)
        self.modeGroup.addWidget(self.fromScratchRadioButton)

        self.topWidget.addWidget(self.modeGroup)

        self.runButton = rbwUI.RBWButton(text='Run', size=[280, 35], icon=['start.png'])
        self.runButton.clicked.connect(self.run)

        self.topWidget.addWidget(self.runButton, alignment=QtCore.Qt.AlignCenter)

        self.fillSeasonComboBox()

        self.setFixedSize(300, 270)
        self.setTitle('Recache UI')
        self.setFocus()

    ############################################################################
    # @brief      fill the shot season combo box.
    #
    def fillSeasonComboBox(self):
        self.seasonComboBox.clear()
        seasons = self.shots.keys()

        self.seasonComboBox.addItems(seasons)

        if len(seasons) == 1:
            self.seasonComboBox.setCurrentIndex(0)
            self.fillEpisodeComboBox()

    ############################################################################
    # @brief      fill the episode combo box.
    #
    def fillEpisodeComboBox(self):
        self.currentSeason = self.seasonComboBox.currentText()

        self.episodeComboBox.clear()
        episodes = self.shots[self.currentSeason].keys()

        self.episodeComboBox.addItems(episodes)

        if len(episodes) == 1:
            self.episodeComboBox.setCurrentIndex(0)
            self.fillSequenceComboBox()

    ############################################################################
    # @brief      fill the sequence combo box.
    #
    def fillSequenceComboBox(self):
        self.currentEpisode = self.episodeComboBox.currentText()

        self.sequenceComboBox.clear()
        sequences = self.shots[self.currentSeason][self.currentEpisode].keys()

        self.sequenceComboBox.addItems(sequences)

        if len(sequences) == 1:
            self.sequenceComboBox.setCurrentIndex(0)
            self.fillShotComboBox()

    ############################################################################
    # @brief      fill the shot combo box.
    #
    def fillShotComboBox(self):
        self.currentSequence = self.sequenceComboBox.currentText()

        self.shotComboBox.clear()
        shots = self.shots[self.currentSeason][self.currentEpisode][self.currentSequence].keys()

        self.shotComboBox.addItems(shots)

        if len(shots) == 1:
            self.shotComboBox.setCurrentIndex(0)
            self.setCurrentShot()

    ############################################################################
    # @brief      Sets the current shot.
    #
    def setCurrentShot(self):
        self.currentShot = self.shotComboBox.currentText()

        self.currentShotObj = api.shot.Shot({
            'season': self.currentSeason,
            'episode': self.currentEpisode,
            'sequence': self.currentSequence,
            'shot': self.currentShot
        })

    ############################################################################
    # @brief      { function_description }
    #
    def run(self):
        if self.convertionRadioButton.isChecked():
            recacheConvertionObj = recacheConvertion.RecacheConvertion(self.openShotObj, self.currentShotObj)
            recacheConvertionObj.recache()
        else:
            recacheFromScratchObj = recacheFromScratch.RecacheFromScratch(self.openShotObj, self.currentShotObj)
            recacheFromScratchObj.recache()
