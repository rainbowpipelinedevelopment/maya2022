import importlib
import os

import maya.cmds as cmd

import api.log
import api.system
import api.json
import api.vray
import depts.lighting.tools.sceneSetup.sceneSetup as setup
import api.savedShot
import depts.finishing.tools.generic.collectAll as collectAll
import depts.finishing.tools.generic.cacheManager as cacheManager

importlib.reload(api.log)
importlib.reload(api.system)
importlib.reload(api.json)
importlib.reload(api.vray)
importlib.reload(setup)
# importlib.reload(api.SavedShot)
importlib.reload(collectAll)
importlib.reload(cacheManager)


################################################################################
# @brief      This class describes a recache convertion.
#
class RecacheConvertion(object):

    def __init__(self, openShotObj, currentShotObj):
        self.openShotObj = openShotObj
        self.currentShotObj = currentShotObj

    ############################################################################
    # @brief      { function_description }
    #
    def recache(self):
        # clean up scene
        self.cleanUpScene()

        # clean render settings
        self.cleanupEnvRenderSettings()

        # import from shot
        self.importFromShot()

        # set duration
        self.setDuration()

        # save
        savedShotParams = {
            'shot': self.currentShotObj,
            'dept': 'Lgt',
            'note': 'Recache (convertion) from shot {}'.format(self.openShotObj.getShotCode())
        }

        savedShot = api.savedShot.SavedShot(params=savedShotParams)
        savedShot.save()

    ############################################################################
    # @brief      { function_description }
    #
    def cleanUpScene(self):
        # cleanup vrscenes
        if cmd.objExists('VRSCENE'):
            vrscenes = cmd.listRelatives('VRSCENE')
        else:
            vrscenes = []

        if len(vrscenes) > 0:
            for vrscene in vrscenes:
                vrsceneName = vrscene[:-5]

                cmd.delete('{}_expression'.format(vrsceneName))
                cmd.delete(vrscene)

        # cleanup camera
        if cmd.objExists('BAKED_CAM'):
            cmd.delete('BAKED_CAM')

        if cmd.objExists('LIBRARY'):
            cmd.delete('LIBRARY')

    ################################################################################
    # @brief      cleanup env layer when import to recache the shot.
    #
    def cleanupEnvRenderSettings(self):
        envDefaultSettings = {
            'giOn': 1,
            'primaryEngine': 0,
            'secondaryEngine': 3,
            'imap_currentPreset': 2,
            'subdivs': 500,
            'mode': 0,
            'useCameraPath': 0,
            'imap_mode': 0,
            'imap_useCameraPath': 0,
        }

        envRemoveSettings = [
            'globopt_gi_dontRenderImage',
            'lc_autoSaveFile',
            'lc_fileName',
            'imap_autoSaveFile2',
            'imap_fileName2'
        ]

        renderLayerList = api.renderlayer.list()
        for renderLayer in renderLayerList:
            if renderLayer.startswith('ENV'):
                # devo resettare i settaggi del layer ENV appena creato
                api.renderlayer.switchByName(renderLayer)
                for setting in envDefaultSettings:
                    api.renderlayer.changeRenderSettingsOverrideValue(renderLayer, setting, envDefaultSettings[setting])

                for setting in envRemoveSettings:
                    api.renderlayer.removeRenderSettingsOverride(renderLayer, setting)

    ############################################################################
    # @brief      Gets the shot entites.
    #
    # @return     The shot entites.
    #
    def getShotEntites(self):
        finishingJson = os.path.join(self.currentShotObj.getShotFolder(), 'Fin', 'finishingInitInfos.json')
        if os.path.exists(finishingJson):
            data = api.json.json_read(finishingJson)
        else:
            pass

        entities = {}
        nodes = []

        if cmd.objExists('CHARACTER'):
            nodes = nodes + cmd.listRelatives('CHARACTER')

        if cmd.objExists('PROP'):
            nodes = nodes + cmd.listRelatives('PROP')

        if nodes:
            for node in nodes:
                nodeName = node
                namespace = node.split(':')[0]
                entityName = namespace[:-4] if namespace[-4] == '_' else namespace[:-3]

                versions = [version for version in os.listdir(data[entityName]['cacheDir']) if version.startswith('vr')]
                if versions:
                    versions.sort()
                    entities[entityName] = {'namespace': namespace, 'node': nodeName, 'cacheDir': os.path.join(data[entityName]['cacheDir'], versions[-1]).replace('\\', '/')}

        return entities

    ############################################################################
    # @brief      { function_description }
    #
    def importFromShot(self):
        # camera
        cameraQuery = "SELECT `path` FROM `savedShot` WHERE `shotId`={} AND `dept`='Cam' ORDER BY `date` DESC".format(self.currentShotObj.shotId)
        cameraPath = api.database.selectSingleQuery(cameraQuery)

        if cameraPath:
            cameraSavedShot = api.savedShot.SavedShot(params={'path': cameraPath[0]})
            cameraSavedShot.load(mode='merge')

        # vray proxy path
        entities = self.getShotEntities()
        for entity in entities:
            vrayProxies = cmd.ls('{}:*'.format(entities[entity]['namespace']), type='VRayProxy')
            for vrayProxy in vrayProxies:
                proxyPath = cmd.getAttr('{}.fileName'.format(vrayProxy))
                if proxyPath.endswith('.vrmesh'):
                    newProxyPath = os.path.join(entities[entity]['cacheDir'], os.path.basename(proxyPath)).replace('\\', '/')
                else:
                    newProxyPath = os.path.join(entities[entity]['cacheDir'].replace('/vrmesh/', '/innerAlembic/'), os.path.basename(proxyPath)).replace('\\', '/')
                cmd.setAttr('{}.fileName'.format(vrayProxy), newProxyPath, type='string')

            rivets = cmd.ls('{}:*|RIVETS'.format(entities[entity]['namespace']))[0]
            cmd.delete(rivets)
            cacheManager.importCNT(entities[entity])


        # vrscenes
        vrsceneFiles = collectAll.getVrsceneFiles(self.currentShotObj)
        for vrsceneFile in vrsceneFiles:
            vrsceneSavedShot = api.savedShot.SavedShot(params={'path': vrsceneFile})
            vrsceneSavedShot.load(mode='merge')

    ############################################################################
    # @brief      Sets the duration.
    #
    def setDuration(self):
        duration = self.currentShotObj.getShotDuration()

        cmd.playbackOptions(max=duration)
        cmd.playbackOptions(animationEndTime=duration)
