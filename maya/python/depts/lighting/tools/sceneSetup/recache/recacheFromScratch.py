import importlib
import os
from PySide2 import QtWidgets

import maya.cmds as cmd
import maya.mel as mel

import api.widgets.rbw_UI as rbwUI
import api.log
import api.system
import api.json
import api.vray
import depts.lighting.tools.sceneSetup.sceneSetup as setup
import api.savedShot

# importlib.reload(rbwUI)
importlib.reload(api.log)
importlib.reload(api.system)
importlib.reload(api.json)
importlib.reload(api.vray)
importlib.reload(setup)
# importlib.reload(api.SavedShot)

################################################################################
# @brief      This class describes a recache from scratch.
#
class RecacheFromScratch(object):

    def __init__(self, openShotObj, currentShotObj):
        self.openShotObj = openShotObj
        self.currentShotObj = currentShotObj

    ############################################################################
    # @brief      recache import material from another shot.
    #
    def recache(self):
        # import RE and lightrig
        self.getExportMaterials()

        # constrin to cam lightrig
        self.constraintLightrigToCam()

        # sub directories
        setup.createSubDirectories()

        # wrapper
        api.vray.createWrapper()

        # render settings
        setup.setVraySettings()

        # import RL
        self.getShotLayers(self.currentShotObj)

        # set ML
        setup.fillMLSet()

        # env map
        setup.setEnvReflectText()

        # cleanup settings
        self.cleanupEnvRenderSettings()

        # save
        savedShotParams = {
            'shot': self.openShotObj,
            'dept': 'Lgt',
            'note': 'Recache (scratch) from shot {}'.format(self.currentShotObj.getShotCode())
        }

        savedShot = api.savedShot.SavedShot(params=savedShotParams)
        savedShot.save()

    ############################################################################
    # @brief      Gets the shot layers.
    #
    # @param      shot  The shot
    #
    # @return     The shot layers.
    #
    def getShotLayers(self, shot):
        renderLayers = []
        renderLayerQuery = "SELECT `name` FROM `renderLayer` WHERE `linkId`={} AND `enabled`=1 AND `type`='shot'".format(self.currentShotObj.shotId)
        results = api.database.selectQuery(renderLayerQuery)

        for result in results:
            renderLayers.append(result[0])
        renderLayers.sort()

        versionsQuery = "SELECT `version`, `id` FROM `savedShot` WHERE `shotId` = '{}' AND `dept` = 'Lgt' AND `softwareVersion` = 'maya2022' ORDER BY `date` DESC".format(self.currentShotObj.shotId)
        versions = api.database.selectQuery(versionsQuery)

        for renderLayer in renderLayers:
            for version in versions:
                renderLayerPath = os.path.join(self.currentShotObj.getShotFolder(), 'Lgt', version[0], 'export', 'renderLayers', '{}_{}.json'.format(renderLayer, version[0])).replace('\\', '/')
                api.log.logger().debug(renderLayerPath)
                if os.path.exists(renderLayerPath):
                    renderLayersData = api.json.json_read(renderLayerPath)
                    if 'sceneAOVs' in renderLayersData:
                        # pulisco il file per i prossimi recache
                        api.renderlayer.cleanupExportSettings(renderLayerPath)

                    api.renderlayer.importFromTemplate(renderLayerPath)
                    break

    ############################################################################
    # @brief      { function_description }
    #
    # @return     { description_of_the_return_value }
    #
    def topGroups(self):
        top_nodes = cmd.ls(assemblies=True)
        defaultNodes = ['persp', 'top', 'front', 'side']
        top_groups = [grp for grp in top_nodes if grp not in defaultNodes]
        return top_groups

    ############################################################################
    # @brief      Gets the export materials. (RE and lightrigs)
    #
    def getExportMaterials(self):
        self.lightrigs = []

        lgtSavedShotParams = {
            'shot': self.currentShotObj,
            'dept': 'Lgt'
        }

        lgtSavedShot = api.savedShot.SavedShot(params=lgtSavedShotParams)
        availableVersions = lgtSavedShot.getAvailableVersions()
        if availableVersions:
            for version in availableVersions:
                if version[11]:
                    versionString = version[3]
                    break

            exportFolder = os.path.join(self.currentShotObj.getShotFolder(), 'Lgt', versionString, 'export').replace('\\', '/')

            for file in os.listdir(exportFolder):
                fullFileName = os.path.join(exportFolder, file).replace('\\', '/')
                if file.endswith('.ma'):
                    if '_lightRig_' in file:
                        nodeName = '_'.join(file[:-3].split('_')[5:-1]).replace('LGT_lightRig', 'LGT:lightRig')
                        self.lightrigs.append(nodeName)
                    oldNodes = self.topGroups()
                    cmd.file(fullFileName, i=True, type="mayaAscii", ignoreVersion=True, mergeNamespacesOnClash=False, pr=True)
                    newNodes = self.topGroups()
                    if cmd.objExists('LIGHTS'):
                        pass
                    else:
                        cmd.group(empty=True, n='LIGHTS')
                    for node in newNodes:
                        if node not in oldNodes and node != 'GEOPROXY_GRP':
                            cmd.parent(node, 'LIGHTS')
                            api.log.logger().debug('Add {} to LIGHTS'.format(node))
                elif file.endswith('.mb'):
                    api.log.logger().debug(file)
                    api.vray.importRenderElements(fullFileName)

    ################################################################################
    # @brief      cleanup env layer when import to recache the shot.
    #
    def cleanupEnvRenderSettings(self):
        envDefaultSettings = {
            'giOn': 1,
            'primaryEngine': 0,
            'secondaryEngine': 3,
            'imap_currentPreset': 2,
            'subdivs': 500,
            'mode': 0,
            'useCameraPath': 0,
            'imap_mode': 0,
            'imap_useCameraPath': 0,
        }

        envRemoveSettings = [
            'globopt_gi_dontRenderImage',
            'lc_autoSaveFile',
            'lc_fileName',
            'imap_autoSaveFile2',
            'imap_fileName2'
        ]

        renderLayerList = api.renderlayer.list()
        for renderLayer in renderLayerList:
            if renderLayer.startswith('ENV'):
                # devo resettare i settaggi del layer ENV appena creato
                api.renderlayer.switchByName(renderLayer)
                for setting in envDefaultSettings:
                    api.renderlayer.changeRenderSettingsOverrideValue(renderLayer, setting, envDefaultSettings[setting])

                for setting in envRemoveSettings:
                    api.renderlayer.removeRenderSettingsOverride(renderLayer, setting)

    ############################################################################
    # @brief      { function_description }
    #
    def constraintLightrigToCam(self):
        toConstraint = LightrigsPanel(self.lightrigs).result()

        for lightrig in toConstraint:
            if cmd.objExists('{}:rim_offset'.format(lightrig.split(':')[0])):
                cmd.select(clear=True)
                cmd.select('BAKED_CAM')
                cmd.select('{}:rim_offset'.format(lightrig.split(':')[0]), add=True)

                mel.eval('aimConstraint -offset 0 0 0 -weight 1 -aimVector 0 0 1 -upVector 0 1 0 -worldUpType "vector" -worldUpVector 0 1 0;')


################################################################################
# @brief      This class describes a lightrigs panel.
#
class LightrigsPanel(rbwUI.RBWConfirm):

    def __init__(self, lightrigs, **params):
        params['isMain'] = False
        params['resizable'] = True
        params['defCancel'] = None
        params['title'] = 'Constraint LightRig to Cam Dialog'

        self.lightrigs = lightrigs

        params['widget'] = [self.initUI()]

        super().__init__(**params)

        self.close = False
        self.setMinimumSize(300, 300)
        self.updateMargins()
        self.exec_()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        mainWidget = rbwUI.RBWGroupBox(title='Do you want constraint lightrig to render cam?', layout='V', fontSize=12)

        self.scrollWidget = QtWidgets.QScrollArea()
        self.scrollWidget.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.scrollWidget.setWidgetResizable(True)
        self.scrollWidget.setStyleSheet('background-color: transparent; border-color: transparent')

        self.lightrigWidget = rbwUI.RBWFrame(layout='V', margins=[0, 0, 3, 0], bgColor='transparent')

        self.scrollWidget.setWidget(self.lightrigWidget)

        for lightrig in self.lightrigs:
            checkBox = rbwUI.RBWCheckBox(text=lightrig, stretch=False)
            checkBox.setObjectName('{}CheckBox'.format(lightrig))
            self.lightrigWidget.addWidget(checkBox)

        mainWidget.addWidget(self.scrollWidget)

        return mainWidget

    ############################################################################
    # @brief      { function_description }
    #
    def result(self):
        lightrigs = self.widget[0].findChildren(rbwUI.RBWCheckBox)
        results = []

        for lightrig in lightrigs:
            if lightrig.isChecked():
                results.append(lightrig.getText())

        return results
