import importlib
import os
from PySide2 import QtWidgets, QtCore, QtGui

import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI
import api.log
import depts.lighting.tools.sceneSetup.sceneSetup as setup

# importlib.reload(rbwUI)
importlib.reload(api.log)
importlib.reload(setup)


################################################################################
# @brief      This class describes a scene setup ui.
#
class SceneSetupUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      parent  The parent
    #
    def __init__(self, parent=None):
        super().__init__()
        if cmd.window('SceneSetupUI', exists=True):
            cmd.deleteUI('SceneSetupUI')

        self.id_mv = os.getenv('ID_MV')

        self.sceneData = setup.getSceneEntities()
        api.log.logger().debug('sceneData: {}'.format(self.sceneData))

        self.setObjectName('SceneSetupUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topWidget = rbwUI.RBWFrame(layout='V', radius=5)
        self.mainLayout.addWidget(self.topWidget)

        self.sceneTree = rbwUI.RBWTreeWidget(fields=['Namespace', 'Preview Name', 'Create RL'], size=[250, 125, 50], decorate=True)
        self.topWidget.addWidget(self.sceneTree)

        self.runButton = rbwUI.RBWButton(text='Run', size=[150, 35], icon=['start.png'])
        self.runButton.clicked.connect(self.run)
        self.topWidget.addWidget(self.runButton, alignment=QtCore.Qt.AlignCenter)

        self.fillSceneTree()

        self.setMinimumSize(457, 300)
        self.setTitle('Scene Setup UI')
        self.setFocus()

    ############################################################################
    # @brief      fill the scene tree.
    #
    def fillSceneTree(self):
        for group in self.sceneData:
            child = QtWidgets.QTreeWidgetItem(self.sceneTree)
            child.setText(0, group)

            for item in self.sceneData[group]:
                subChild = QtWidgets.QTreeWidgetItem(child)
                subChild.setText(0, item)
                subChild.setText(1, self.sceneData[group][item]['layerName'])
                switch = rbwUI.RBWSwitch()
                self.sceneTree.setItemWidget(subChild, 2, switch)

    ############################################################################
    # @brief      run function.
    #
    def run(self):
        layerToCreate = {}
        root = self.sceneTree.invisibleRootItem()
        for i in range(0, root.childCount()):
            item = root.child(i)
            group = item.text(0)
            for j in range(0, item.childCount()):
                subChild = item.child(j)

                if self.sceneTree.itemWidget(subChild, 2).isChecked():
                    if group not in layerToCreate:
                        layerToCreate[group] = []

                    layerToCreate[group].append(self.sceneData[group][subChild.text(0)])

        setup.setUpScene(layerToCreate)

        rbwUI.RBWDialog(title='Setup Complete', text='Scene Setup Complete')
