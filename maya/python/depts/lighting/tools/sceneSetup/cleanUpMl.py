import importlib

import maya.cmds as cmd

import api.log
import api.renderlayer
import config.dictionaries

importlib.reload(api.renderlayer)
importlib.reload(api.log)
importlib.reload(config.dictionaries)

lightsType = config.dictionaries.lightsType


################################################################################
# @brief      recursive function that list visible lights from a root
#
# @param      returnList  The return list
# @param      rootNode    The root node
#
# @return     { description_of_the_return_value }
#
def recursiveListLights(returnList, rootNode='LIGHTS', all=False):
    for node in cmd.listRelatives(rootNode, fullPath=True):
        if cmd.nodeType(node, apiType=True) == 'kTransform' and cmd.getAttr('{}.visibility'.format(node)) and not all:
            recursiveListLights(returnList, node, all)
        elif cmd.nodeType(node, apiType=True) == 'kTransform' and all:
            recursiveListLights(returnList, node, all)
        elif cmd.nodeType(node) in lightsType and cmd.getAttr('{}.visibility'.format(node)) and not all:
            returnList.append(node)
        elif cmd.nodeType(node) in lightsType and all:
            returnList.append(node)


################################################################################
# @brief      Gets the visible lights.
#
# @return     The visible lights.
#
def getVisibleLights(selection=['LIGHTS'], all=False):
    lightsList = []

    for selectObj in selection:
        if cmd.nodeType(selectObj) in lightsType:
            lightsList.append(selectObj)
        else:
            recursiveListLights(lightsList, selection, all)
    return lightsList


################################################################################
# @brief      Gets the render layer lights.
#
# @param      layer  The layer
# @param      index  The index
#
# @return     The render layer lights.
#
def getRenderLayerLights(layer, index):
    api.renderlayer.switchByName(layer)
    ml = []

    elements = api.renderlayer.getRenderLayerMember(layer)

    if len(elements) > 0:
        for element in elements:
            if cmd.nodeType(element) in lightsType:
                nameList = element.split('|')[-1].split('_')

                if 'ML{}'.format(index) in nameList:
                    ml.append(element)

    return ml


################################################################################
# @brief      Determines if used.
#
# @param      usedLightsList  The used lights list
# @param      compareList     The compare list
#
# @return     True if used, False otherwise.
#
def isUsed(usedLightsList=None, compareList=None):
    for light in compareList:
        if light in usedLightsList:
            return True

    return False


################################################################################
# @brief      turn off the unused poasses.
#
def turnOffUnusedPass(layer):
    api.renderlayer.switchByName(layer)
    usedLightsList = getVisibleLights()
    for i in range(0, 15):
        ml = getRenderLayerLights(layer, i)

        isUsedMl = isUsed(usedLightsList, ml)

        if not isUsedMl:

            if cmd.objExists('diff_ML{}'.format(i)):
                api.renderlayer.createAoOverride(layer, 'diff_ML{}'.format(i), 0)

            if cmd.objExists('spec_ML{}'.format(i)):
                api.renderlayer.createAoOverride(layer, 'spec_ML{}'.format(i), 0)

            if cmd.objExists('gidiff_ML{}'.format(i)):
                api.renderlayer.createAoOverride(layer, 'gidiff_ML{}'.format(i), 0)

            if cmd.objExists('gispec_ML{}'.format(i)):
                api.renderlayer.createAoOverride(layer, 'gispec_ML{}'.format(i), 0)

            api.log.logger().debug('TurnOff ML{} on Layer: {}'.format(i, layer))
        else:

            if cmd.objExists('diff_ML{}'.format(i)):
                api.renderlayer.createAoOverride(layer, 'diff_ML{}'.format(i), 1)

            if cmd.objExists('spec_ML{}'.format(i)):
                api.renderlayer.createAoOverride(layer, 'spec_ML{}'.format(i), 1)

            if cmd.objExists('gidiff_ML{}'.format(i)):
                api.renderlayer.createAoOverride(layer, 'gidiff_ML{}'.format(i), 1)

            if cmd.objExists('gispec_ML{}'.format(i)):
                api.renderlayer.createAoOverride(layer, 'gispec_ML{}'.format(i), 1)

            api.log.logger().debug('TurnOn ML{} on Layer: {}'.format(i, layer))


################################################################################
# @brief      delete lightirg.
#
def deleteLightrig():
    grps = ['diff', 'gidiff', 'spec', 'gispec']

    lightrig = cmd.ls(sl=True)
    if not lightrig or not lightrig[0].count('LGT'):
        cmd.confirmDialog(title='Select a Lightrig', message='Please select a lightrig to use this tool', icon='critical', button=['OK'])

    lightsList = getVisibleLights(selection=lightrig, all=True)
    for light in lightsList:
        code = light.split('_')[-2]

        for grp in grps:
            grpName = '{}_{}'.format(grp, code)
            if cmd.objExists(grpName):
                setMembers = cmd.listConnections('{}.dagSetMembers'.format(grpName))
                if setMembers:
                    for lightML in setMembers:
                        if lightML.count(lightrig[0].split(':')[0]):
                            cmd.sets(lightML, rm=grpName)

    rNode = cmd.referenceQuery(lightrig, referenceNode=True)
    cmd.file(rfn=rNode, rr=True)

    fosterParents = cmd.ls(type='fosterParent')
    if fosterParents:
        cmd.delete(fosterParents)
