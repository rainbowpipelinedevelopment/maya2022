import importlib
import os

import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI
# importlib.reload(rbwUI)

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'common')


def run_sceneSetup(*args):
    import depts.lighting.tools.sceneSetup.sceneSetupUI as setupUI
    importlib.reload(setupUI)

    renderer = cmd.getAttr("defaultRenderGlobals.currentRenderer")
    if renderer == 'vray':
        if cmd.objExists('LIGHTS'):
            win = setupUI.SceneSetupUI()
            win.show()
        else:
            rbwUI.RBWError(title='No LIGHTS in scene', text='No lights in scene.\nPlease use import lightrig tool and then proceed with scene setup.')
    else:
        rbwUI.RBWError(title='ACTIVE VRAY PLEASE', text='Sorry to setup your scene switch to vray in the render setting and retry again')


def run_setMl(*args):
    import depts.lighting.tools.sceneSetup.sceneSetup as setup
    importlib.reload(setup)

    setup.fillMLSet()


def run_cleanUpMlUI(*args):
    import depts.lighting.tools.sceneSetup.cleanUpMlUI as cleanUpMlUI
    importlib.reload(cleanUpMlUI)

    win = cleanUpMlUI.CleanUpMlUI()
    win.show()


def run_recache(*args):
    import depts.lighting.tools.sceneSetup.recache.recacheUI as recacheUI
    importlib.reload(recacheUI)

    renderer = cmd.getAttr("defaultRenderGlobals.currentRenderer")
    if renderer == 'vray':
        win = recacheUI.RecacheUI()
        win.show()
    else:
        rbwUI.RBWError(title='ACTIVE VRAY PLEASE', text='Sorry to setup your scene switch to vray in the render setting and retry again')


sceneSetupUI = {
    'name': 'Scene Setup',
    'icon': os.path.join(iconDir, 'start.png'),
    'launch': run_sceneSetup
}

setMlTool = {
    'name': 'Set ML',
    'icon': os.path.join(iconDir, '..', 'lighting', 'ml.png'),
    'launch': run_setMl
}

cleanUpMlTool = {
    'name': 'Clean Up ML',
    'icon': os.path.join(iconDir, '..', 'lighting', 'ml.png'),
    'launch': run_cleanUpMlUI
}


recacheTool = {
    'name': 'Recache Tool',
    'launch': run_recache
}


tools = [
    sceneSetupUI,
    setMlTool,
    cleanUpMlTool,
    recacheTool
]

PackageTool = {
    "name": "sceneSetup",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
