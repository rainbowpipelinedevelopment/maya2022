import importlib
import os
from PySide2 import QtWidgets
from decimal import Decimal

import maya.cmds as cmd
import maya.mel as mel

import api.vray
import api.scene
import api.json
import api.renderlayer
import api.asset
import api.savedAsset
import api.widgets.rbw_UI as rbwUI
import api.savedShot
import api.exception
import depts.lighting.tools.renderElements.manageRenderElements as manageRenderElements
import config.dictionaries
import common.makeButton.makeButton as makeButton

importlib.reload(api.vray)
importlib.reload(api.scene)
importlib.reload(api.json)
importlib.reload(api.renderlayer)
importlib.reload(api.asset)
# importlib.reload(api.savedAsset)
# importlib.reload(rbwUI)
# importlib.reload(api.savedShot)
importlib.reload(api.exception)
importlib.reload(manageRenderElements)
importlib.reload(config.dictionaries)


masterLayerOverride = {
    'imgOpt_exr_autoDataWindow': 0,
    'imgOpt_exr_bitsPerChannel': 16,
    'animType': 0,
    'samplerType': 4,
    'aaFilterType': 6,
    'dmcMaxSubdivs': 4,
    'dmcMinSubdivs': 1,
    'dmcThreshold': 0.03,
    'sys_regsgen_xc': 32,
    'giOn': 1,
    'secondaryEngine': 2,
    'ddisplac_maxSubdivs': 64,
    'globopt_geom_displacement': 1,
    'globopt_probabilistic_lights_on': 0,
    'rt_maxPathsPerPixel': 0,
    'relements_enableall': 1,
    'relements_usereferenced': 1,
    'dmc_depth': 3,
    'relements_deepMergeMode': 0,
    'cmap_gamma': 2.200,
    'pixelAspect': 1
}

subDirectories = {
    'rendering': 'RENDER',
    'imageSequence': 'SLC/IS/v001',
    'clip': 'SLC/CLIP'
}

lightsType = config.dictionaries.lightsType

ourRenderLayers = []

nodeTypes = {
    'character': 'CHAR',
    'prop': 'PROP'
}

configDir = os.path.join('${CONTENT}', '_config')
configDir = os.path.expandvars(configDir)

mcLibrary = "//speed.nas/library/02_production/01_content/mc_library"


def getSceneEntities():
    entities = {}
    nodes = []

    if cmd.objExists('CHARACTER'):
        nodes = nodes + cmd.listRelatives('CHARACTER')

    if cmd.objExists('PROP'):
        nodes = nodes + cmd.listRelatives('PROP')

    if nodes:
        for node in nodes:
            nodeName = node
            namespace = node.split(':')[0]

            saveNode = cmd.listConnections(node, destination=False, source=True)[0]
            category = cmd.getAttr('{}.genre'.format(saveNode))
            name = cmd.getAttr('{}.name'.format(saveNode))
            variant = cmd.getAttr('{}.variant'.format(saveNode))

            if category not in entities:
                entities[category] = {}
            entities[category][namespace] = {'layerName': '{}{}'.format(name, variant.capitalize()), 'node': nodeName}

    return entities


################################################################################
# @brief      Creates sub directories.
#
def createSubDirectories():
    fileName = api.scene.scenename()
    rootFolder = os.path.dirname(os.path.dirname(fileName)).replace('\\', '/')
    for key in subDirectories:
        subDirectory = os.path.join(rootFolder, subDirectories[key])
        if not os.path.exists(subDirectory):
            os.makedirs(subDirectory)


#################################################################################################
# @brief      the function select all the VRay lights and put hit in the right renderElementSet
#
def fillMLSet():
    # check if the sets are empty or not; in case not remove the ligths
    for vraySet in cmd.ls(type='VRayRenderElementSet'):
        vraySet_lights = cmd.listRelatives(vraySet, fullPath=True)
        if vraySet_lights is not None:
            for light in vraySet_lights:
                if cmd.nodeType(light) in lightsType:
                    lightPath = light.split('|')
                    lightPath = lightPath[:-1][2:]
                    light = '|'.join(lightPath)
                    api.log.logger().debug('rimuovo {} dal set {}'.format(light, vraySet))
                    cmd.sets(light, remove=vraySet)

    # now i parse every light and put in the right set
    nameFilter = cmd.itemFilter(byName='*ML*')
    firstTypeFilter = cmd.itemFilter(byType=lightsType[0])
    lightsFilter = cmd.itemFilter(intersect=(nameFilter, firstTypeFilter))

    for i in range(1, len(lightsType)):
        typeFilter = cmd.itemFilter(byType=lightsType[i])
        lightFilter = cmd.itemFilter(intersect=(nameFilter, typeFilter))

        lightsFilter = cmd.itemFilter(union=(lightsFilter, lightFilter))

    found = cmd.lsThroughFilter(lightsFilter)

    if found is not None:

        for p in found:
            lightName = cmd.listRelatives(p, parent=True, fullPath=True)[0]
            mlNumber = lightName.split('_')[-2]

            cmd.ctxEditMode()
            cmd.sets(lightName, forceElement='diff_{}'.format(mlNumber))
            cmd.sets(lightName, forceElement='spec_{}'.format(mlNumber))

            if cmd.objExists('gidiff_{}'.format(mlNumber)):
                cmd.sets(lightName, forceElement='gidiff_{}'.format(mlNumber))
            if cmd.objExists('gispec_{}'.format(mlNumber)):
                cmd.sets(lightName, forceElement='gispec_{}'.format(mlNumber))


################################################################################
# @brief      Sets the vray settings.
#
# @return     { description_of_the_return_value }
#
def setVraySettings():
    defaultLayer = api.renderlayer.getDefaultRenderLayer()
    api.renderlayer.switchByName(defaultLayer)
    # Common Tab
    api.vray.setImageFormat(6)

    try:
        resolutionQuery = "SELECT width, height FROM `p_prj_resolution` AS pr JOIN `p_resolution` AS r ON pr.resolutionId = r.id WHERE pr.id_mv = {}".format(
            os.getenv("ID_MV")
        )

        resolutionTuple = api.database.selectQuery(resolutionQuery)[0]

        api.vray.setResolution(resolutionTuple[0], resolutionTuple[1])
    except:
        api.vray.setResolution(1920, 1080)

    cmd.setAttr('vraySettings.fileNamePrefix', '/tmp/', type='string')
    cmd.setAttr('vraySettings.globopt_geom_displacement', 1)

    for override in masterLayerOverride:
        cmd.setAttr('vraySettings.{}'.format(override), masterLayerOverride[override])
    cmd.setAttr('vraySettings.globopt_consistentLightingElements', 1)


################################################################################
# @brief      fill the cahr and prop layers
#
# @param      entities  The entities
#
def fillRenderLayers(entities):
    for group in entities:
        for entry in entities[group]:
            charTokens = entry['node'].split(':')[0].split('RND')[0]

            nodeType = nodeTypes[group]

            # creazione beauty render layer
            api.renderlayer.createFromTemplate(nodeType, entry['layerName'])
            realLayerName = '{}_{}_RL'.format(nodeType, entry['layerName'])
            ourRenderLayers.append(realLayerName)

            # beauty geo collection
            charGeosCollection = api.renderlayer.getRenderLayerCollectionByName(realLayerName, 'geo')
            charGeosCollection.getSelector().staticSelection.set([entry['node']])

            # beauty vrscene collection
            vrscenePattern = 'VRSCENE|{}Mesh'.format(charTokens)

            charVrsceneCollection = api.renderlayer.getRenderLayerCollectionByName(realLayerName, 'vrscene_col')
            charVrsceneCollection.getSelector().setPattern(vrscenePattern)

            # lights collection for beauty
            if cmd.objExists('LIGHTS'):
                charLightsCollection = api.renderlayer.getRenderLayerCollectionByName(realLayerName, 'lights')
                charLightsCollection.getSelector().staticSelection.set(['LIGHTS'])


################################################################################
# @brief      fill env render layer.
#
def fillEnvRenderLayers():
    if cmd.objExists('SET') and cmd.nodeType('SET') == 'transform':
        env1 = ['SET']
        env2 = []

        if cmd.objExists('SET_EDIT'):
            env2 = ['SET_EDIT']

        env = env1 + env2

        domes = []
        for assembly in cmd.listRelatives('SET', allDescendents=True, type='assembly'):
            if 'dome' in assembly:
                domes.append(assembly)

        # create beauty env render layer
        api.renderlayer.createFromTemplate('ENV', '01')
        ourRenderLayers.append('ENV_01_RL')

        # beauty geo collection
        envGeosCollection = api.renderlayer.getRenderLayerCollectionByName('ENV_01_RL', 'geo')
        envGeosCollection.getSelector().staticSelection.set(env)

        # beauty light collection
        if cmd.objExists('LIGHTS'):
            envLightsCollection = api.renderlayer.getRenderLayerCollectionByName('ENV_01_RL', 'lights')
            envLightsCollection.getSelector().staticSelection.set(['LIGHTS'])

        if len(domes) > 0:
            geoPruneCollection = api.renderlayer.getRenderLayerCollectionByName('ENV_01_RL', 'geo_prune')
            geoPruneCollection.getSelector().staticSelection.set(domes)
    else:
        pass


################################################################################
# @brief      Creates secondary render layers.
#
def createSecondaryRenderLayers():
    api.renderlayer.createFromTemplate('SHADOW', '01')
    ourRenderLayers.append('SHADOW_01_RL')

    api.renderlayer.createFromTemplate('DOME', 'Sky')
    ourRenderLayers.append('DOME_Sky_RL')
    skyGeosCollection = api.renderlayer.getRenderLayerCollectionByName('DOME_Sky_RL', 'geo')
    skyGeosCollection.getSelector().setPattern('PROP|*dome*:*|*:*|*:*|*:*GRP_sky*')

    api.renderlayer.createFromTemplate('DOME', 'Layers')
    ourRenderLayers.append('DOME_Layers_RL')
    layersGeosCollection = api.renderlayer.getRenderLayerCollectionByName('DOME_Layers_RL', 'geo')
    layersGeosCollection.getSelector().setPattern('PROP|*dome*:*|*:*|*:*|*:*GRP_Layers*')

    api.renderlayer.createFromTemplate('DOME', 'Clouds')
    ourRenderLayers.append('DOME_Clouds_RL')
    cloudsGeosCollection = api.renderlayer.getRenderLayerCollectionByName('DOME_Clouds_RL', 'geo')
    cloudsGeosCollection.getSelector().setPattern('PROP|*dome*:*|*:*|*:*|*:*GRP_clouds*')


################################################################################
# @brief      constrain lightRig to cached item.
#
# @param      parentMode  The parent mode
#
def constraintLigtRigToCachedItem(parentMode='point'):
    selection = cmd.ls(selection=True)

    if len(selection) != 2:
        rbwUI.RBWError(title='ATTENTION', text='You have to select 2 objects')
    else:

        lightrig = selection[0]
        item = selection[1]

        namespace = item.split(':')[0]
        api.log.logger().debug('namespace: {}'.format(namespace))

        proxy2File = {}
        vrayProxies = cmd.ls('{}:*'.format(namespace), type='VRayProxy')
        for vrayProxy in vrayProxies:
            proxy2File[vrayProxy] = cmd.getAttr('{}.fileName'.format(vrayProxy))

        saveNode = cmd.ls('{}:surfacing_saveNode'.format(namespace))[0]
        savedAsset = api.savedAsset.SavedAsset(saveNode=saveNode)
        savedAssetCntInfo = os.path.join(os.getenv('MC_FOLDER'), savedAsset.path.replace('.mb', '_cnts.json')).replace('\\', '/')

        rivets = cmd.ls('RIVETS|RVT_{}'.format(namespace[:-3]))

        if rivets:
            rivet = rivets[0]
            currentFrame = int(cmd.currentTime(query=True))

            try:
                data = api.json.json_read(savedAssetCntInfo)
                cntData = data[next(iter(data))]

                translation = [float(Decimal(x)) for x in cntData['translation']]
                rotation = [float(Decimal(x)) for x in cntData['rotation']]
                scale = [float(Decimal(x)) for x in cntData['scale']]

                cmd.setAttr('{}.translateX'.format(rivet), translation[0])
                cmd.setAttr('{}.translateY'.format(rivet), translation[1])
                cmd.setAttr('{}.translateZ'.format(rivet), translation[2])

                cmd.setAttr('{}.rotateX'.format(rivet), rotation[0])
                cmd.setAttr('{}.rotateY'.format(rivet), rotation[1])
                cmd.setAttr('{}.rotateZ'.format(rivet), rotation[2])

                cmd.setAttr('{}.scaleX'.format(rivet), scale[0])
                cmd.setAttr('{}.scaleY'.format(rivet), scale[1])
                cmd.setAttr('{}.scaleZ'.format(rivet), scale[2])

            except FileNotFoundError:
                pass

            if parentMode == 'parent':
                cmd.parentConstraint(rivet, lightrig, sr=["x", "z"], maintainOffset=True, weight=True)
            elif parentMode == 'parent + offset':
                cmd.parentConstraint(rivet, lightrig, maintainOffset=True, weight=True)
            else:
                cmd.pointConstraint(rivet, lightrig, maintainOffset=False, weight=True)

            cmd.currentTime(currentFrame + 1)
            cmd.currentTime(currentFrame)

            lightirgSaveNode = cmd.listConnections(lightrig, destination=False)[0]
            lightirgReferenceNode = cmd.referenceQuery(lightirgSaveNode, referenceNode=True)
            lightrigReferencePath = cmd.referenceQuery(lightirgReferenceNode, filename=True)
            currentLightrigNamespace = cmd.referenceQuery(lightrigReferencePath, namespace=True)[1:]

            newLightrigNamespace = '{}_{}'.format(''.join(namespace.split('_')[1:-1]), '_'.join(currentLightrigNamespace.split('_')[-3:]))

            if newLightrigNamespace != currentLightrigNamespace:
                cmd.file(lightrigReferencePath, edit=True, namespace=newLightrigNamespace)
                lightrig = '{}:{}'.format(newLightrigNamespace, lightrig.split(':')[1])

            vrayProxyShotFolder = os.path.dirname(proxy2File[vrayProxies[0]]).replace('\\', '/').replace('/innerAlembic/', '/vrmeshes/')
            for file in os.listdir(vrayProxyShotFolder):
                if file.endswith('scale.json'):
                    scaleData = api.json.json_read(os.path.join(vrayProxyShotFolder, file).replace('\\', '/'))['scale']
                    if isinstance(scaleData, list):
                        for couple in scaleData:
                            frame = couple[0]
                            value = couple[1]
                            cmd.setKeyframe(lightrig, time=frame, attribute='scaleX', value=value)
                            cmd.setKeyframe(lightrig, time=frame, attribute='scaleY', value=value)
                            cmd.setKeyframe(lightrig, time=frame, attribute='scaleZ', value=value)
                    else:
                        value = scaleData
                        cmd.setAttr('{}.scale'.format(lightrig), value, value, value, type='float3')
        else:
            rbwUI.RBWError(title='ATTENTION', text='No hip rivet for {}'.format(item))


################################################################################
# @brief      constrain lightRig to item.
#
# @param      parentMode  The parent mode
#
def constraintLigtRigToItem(parentMode='point'):
    selection = cmd.ls(selection=True)

    if len(selection) != 2:
        rbwUI.RBWError(title='ATTENTION', text='You have to select 2 objects')
    else:
        lightrig = selection[0]
        char = selection[1]

        namespace = char.split(':')[0]
        api.log.logger().debug('namespace: {}'.format(namespace))

        edges = []

        name = char.split('_')[1]
        currentSet = '{}:CNT'.format(char.split(':')[0])

        if cmd.objExists(currentSet):
            for elem in cmd.sets(currentSet, q=True):
                if 'Shape' not in elem:
                    edges.append(elem)

        cmd.select(edges, replace=True)
        rivet = makeButton.DM_buttonMaker()
        rivet = cmd.rename(rivet, 'RVT_{}'.format(name))
        if parentMode == 'parent':
            cmd.parentConstraint(rivet, lightrig, sr=["x", "z"], maintainOffset=True, weight=True)
        elif parentMode == 'parent + offset':
            cmd.parentConstraint(rivet, lightrig, maintainOffset=True, weight=True)
        else:
            cmd.pointConstraint(rivet, lightrig, maintainOffset=False, weight=True)

        lightirgSaveNode = cmd.listConnections(lightrig, destination=False)[0]
        lightirgReferenceNode = cmd.referenceQuery(lightirgSaveNode, referenceNode=True)
        lightrigReferencePath = cmd.referenceQuery(lightirgReferenceNode, filename=True)
        currentLightrigNamespace = cmd.referenceQuery(lightrigReferencePath, namespace=True)[1:]

        newLightrigNamespace = '{}_{}'.format(''.join(namespace.split('_')[1:-1]), '_'.join(currentLightrigNamespace.split('_')[-3:]))

        if newLightrigNamespace != currentLightrigNamespace:
            cmd.file(lightrigReferencePath, edit=True, namespace=newLightrigNamespace)
            lightrig = '{}:{}'.format(newLightrigNamespace, lightrig.split(':')[1])


################################################################################
# @brief      Sets the environment reflect text.
#
def setEnvReflectText():
    answer = rbwUI.RBWConfirm(title='Reflect Map', text='Do you want set Environment Reflection Map').result()
    if answer:
        envReflectLoadPanel = QtWidgets.QFileDialog()
        envReflect = os.path.join(configDir, 'lightRig', 'ReflectionMap', 'REFLECTION_MAP')

        api.log.logger().debug('envReflect: {}'.format(envReflect))
        envReflectLoadPanel.setDirectory(envReflect)

        envReflectionNode = 'envReflectTextShadingNode'

        try:
            cmd.shadingNode('file', name=envReflectionNode, asTexture=True)
            path = envReflectLoadPanel.getOpenFileName(None, 'Load Env Reflection Texture')[0]

            cmd.setAttr("vraySettings.cam_overrideEnvtex", 1)
            cmd.setAttr('{}.fileTextureName'.format(envReflectionNode), path, type='string')
            cmd.connectAttr('{}.outColor'.format(envReflectionNode), 'vraySettings.cam_envtexReflect', force=True)

            cmd.setAttr('vraySettings.cam_envtexBg', 0, 0, 0, type='double3')
            cmd.setAttr('vraySettings.cam_envtexGi', 0, 0, 0, type='double3')

            mel.eval('vrayUpdateAE;')

            # fix richiesto da surfacing 05/12/2019
            # attivare 'Override Environment' (render vray) e distativare 'Affect Reflections' (attribute light)
            # solo se esiste una mappa di riflessione
            if 'envReflectTextShadingNode' in cmd.ls(type='file'):
                cmd.setAttr("vraySettings.cam_overrideEnvtex", 1)
                for dome in cmd.ls(type="VRayLightDomeShape"):
                    cmd.setAttr("{}.affectReflections".format(dome), 0)
        except RuntimeError:
            message = 'Sorry but the scene is corrupted.\nPlease follow this few passages.\n1) Close the current maya session.\n2) Re-open Maya.\n3) Use \'Merge\' instead of \'Load\' Function.\n4) Make a new save for your shot.\n5) Reload Vray.\n6) Work as you will.'
            rbwUI.RBWError(title='BAD VRAY LOADED', text=message)
    else:
        pass


################################################################################
# @brief      this function reparir all the shape link in the RND
#
# @return     { description_of_the_return_value }
#
def manageTexturesRef():
    if cmd.objExists('CHARACTER') and len(cmd.listRelatives('CHARACTER')) != 0:
        repairTextRef('CHARACTER')
    else:
        pass

    if cmd.objExists('PROP') and len(cmd.listRelatives('PROP')) != 0:
        repairTextRef('PROP')
    else:
        pass


################################################################################
# @brief      function to repain texture ref
#
# @param      rootNode  The root node
#
# @return     { description_of_the_return_value }
#
def repairTextRef(rootNode):
    items = cmd.listRelatives(rootNode, fullPath=True)
    for item in items:
        shapesDeformed = []
        select = item
        cmd.select(clear=True)

        api.log.logger().debug('sto\' ispezionando il nodo {}'.format(select))

        list_transform = cmd.listRelatives(select, allDescendents=True, type='mesh', fullPath=True)
        for transform in list_transform:
            if 'texturesRef' not in transform and 'Deformed' in transform and 'clump_mesh' not in transform and 'clm' not in transform:
                shapesDeformed.append(transform)
        api.log.logger().debug(len(shapesDeformed))

        cmd.select(shapesDeformed, add=True)
        createTextureReference()
        cmd.select(clear=True)

        for shapeDeformed in shapesDeformed:
            namespace = item.split('|')[-1].split(':')[0]
            texturesRefNode = '{}:texturesRef'.format(namespace)

            if cmd.objExists(texturesRefNode):
                shapePathList = shapeDeformed.split('|')
                shapeReferenceNode = '{}:{}'.format(namespace, shapePathList[-1].replace('Deformed', '_reference'))

                if 'CHARACTER' in shapePathList:
                    shapeReference = '|{0}|{1}|{2}|{3}|{3}'.format(
                        shapePathList[1],
                        shapePathList[2],
                        texturesRefNode,
                        shapeReferenceNode)
                else:
                    shapeReference = '|{0}|{1}|{2}|{3}|{4}|{5}|{5}'.format(
                        shapePathList[1],
                        shapePathList[2],
                        shapePathList[3],
                        shapePathList[4],
                        texturesRefNode,
                        shapeReferenceNode)

                try:
                    api.log.logger().debug('CONNECTION:\nshapeDeformed: {}\nshapeReference: {}\n'.format(shapeDeformed, shapeReference))
                    cmd.connectAttr('{}.message'.format(shapeReference), '{}.referenceObject'.format(shapeDeformed), force=True)
                except:
                    shapeReference = '{}_Shape'.format(shapeReference)
                    api.log.logger().debug('CONNECTION:\nshapeDeformed: {}\nshapeReference: {}\n'.format(shapeDeformed, shapeReference))
                    cmd.connectAttr('{}.message'.format(shapeReference), '{}.referenceObject'.format(shapeDeformed), force=True)

            else:
                pass

        cmd.delete('|texturesRef')


################################################################################
# @brief      Creates a texture reference.
#
def createTextureReference():
    sel = cmd.ls(sl=1, dag=1, type='mesh', l=1, ni=1)

    g = cmd.group(em=1, n='texturesRef')
    for s in sel:
        try:
            api.log.logger().debug(s.split(':')[-2])
            cmd.select(s)
            mel.eval('CreateTextureReferenceObject')
            r = cmd.ls(sl=1)
            cmd.parent(r, g)
        except:
            api.log.logger().debug(s.split('|')[-2])
            cmd.select(s)
            mel.eval('CreateTextureReferenceObject')
            r = cmd.ls(sl=1)
            cmd.parent(r, g)


################################################################################
# @brief      import the render elements file.
#
def importRenderElements():
    vrayVersion = api.vray.getVrayVersion()
    renderElementLastVersion = manageRenderElements.getRenderElementsVersion()
    if renderElementLastVersion != 0:
        renderElementConfPath = os.path.join(manageRenderElements.renderElementsFolder, 'renderElements_v{}.mb'.format(str(renderElementLastVersion).zfill(3))).replace('\\', '/')
    else:
        renderElementConfPath = os.path.join(manageRenderElements.renderElementsRoot, 'renderElements.mb').replace('\\', '/')

    api.log.logger().debug(renderElementConfPath)
    if int(vrayVersion) >= 52000:
        api.vray.importRenderElements(renderElementConfPath)
    else:
        cmd.file(renderElementConfPath, i=True)


############################################################################
# @brief      import the lgt shader from name.
#
# @param      name  The name
#
def importLgtShader(name):
    shaderParams = {'asset': {'id_mv': 1, 'category': 'shader', 'group': 'generic', 'name': name, 'variant': 'df'}, 'dept': 'surfacing', 'deptType': 'rnd', 'approvation': 'def'}
    params = shaderParams.copy()
    assetParams = params.pop('asset')
    asset = api.asset.Asset(params=assetParams)
    params['asset'] = asset
    shaderSavedAssets = api.savedAsset.SavedAsset(params=params)

    shaderPath = os.path.join(mcLibrary, shaderSavedAssets.getLatest()[2]).replace('\\', '/')
    cmd.file(shaderPath, i=True, type="mayaBinary", ignoreVersion=True, mergeNamespacesOnClash=False, pr=True)


################################################################################
# @brief      check if lighting materials exists.
#
def checkLightingMaterials():
    if not cmd.objExists('sl_assetName_reflective_meshName_shader'):
        importLgtShader('reflective')
    if not cmd.objExists('sl_assetName_trace_meshName_shader'):
        importLgtShader('trace')


################################################################################
# @brief      main function
#
# @param      layersToCreate  The layers to create
#
def setUpScene(layersToCreate):
    scenePath = api.scene.scenename(relative=True)
    savedShot = api.savedShot.SavedShot(params={'path': scenePath})

    if savedShot.dept == 'Lgt':
        createSubDirectories()
        renderer = cmd.getAttr("defaultRenderGlobals.currentRenderer")
        if renderer == 'vray':
            checkLightingMaterials()
            importRenderElements()
            fillMLSet()
            setVraySettings()
            fillRenderLayers(layersToCreate)
            fillEnvRenderLayers()
            api.vray.createWrapper()
            createSecondaryRenderLayers()
            api.renderlayer.switchByName(api.renderlayer.getDefaultRenderLayer())
            if not cmd.objExists('GEOPROXY_GRP'):
                cmd.group(empty=True, n='GEOPROXY_GRP')
            api.renderlayer.setRenderable(api.renderlayer.getDefaultRenderLayer(), False)
            ourRenderLayers.append(api.renderlayer.getDefaultRenderLayer())
            setEnvReflectText()
        else:
            rbwUI.RBWError(title='ACTIVE VRAY PLEASE', text='Sorry to setup your scene switch to vray in the render setting and retry again')
    else:
        rbwUI.RBWError(title='SAVE THE SCENE PLEASE', text='Save a lighting version first and try again')
