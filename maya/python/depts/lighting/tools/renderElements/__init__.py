import importlib
import os

import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI
# importlib.reload(rbwUI)

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'common')


def run_loadRenderElements(*args):
    import depts.lighting.tools.renderElements.manageRenderElements as manageRenderElements
    importlib.reload(manageRenderElements)

    manageRenderElements.load()


def run_saveRenderElements(*args):
    import depts.lighting.tools.renderElements.manageRenderElements as manageRenderElements
    importlib.reload(manageRenderElements)

    manageRenderElements.save()


def run_assignRenderElementToLayer(*args):
    import depts.lighting.tools.renderElements.assignRenderElementToLayerUI as assignRenderElementToLayerUI
    importlib.reload(assignRenderElementToLayerUI)

    win = assignRenderElementToLayerUI.AssignRenderElementToLayerUI()
    win.show()


def run_assignElementsToDenoise(*args):
    import depts.lighting.tools.renderElements.assignRenderElementsToDenoiseUI as assignRenderElementsToDenoiseUI
    importlib.reload(assignRenderElementsToDenoiseUI)

    assignRenderElementsToDenoiseUI.AssignRenderElementsToDenoiseUI()


def run_resetRenderElements(*args):
    import depts.lighting.tools.renderElements.resetRenderElements as resetRenderElements
    importlib.reload(resetRenderElements)

    resetRenderElements.resetRenderElements()


loadRenderElements = {
    "name": "Load Render Elements",
    "icon": os.path.join(iconDir, 'load.png'),
    "launch": run_loadRenderElements
}


saveRenderElements = {
    "name": "Save Render Elements",
    "icon": os.path.join(iconDir, 'save.png'),
    "launch": run_saveRenderElements
}


assignRenderElementToLayer = {
    'name': 'Assign RE to Layer',
    "icon": os.path.join(iconDir, 'option.png'),
    'launch': run_assignRenderElementToLayer
}


assignElementsToDenoise = {
    "name": "Assign RE to Denoise Job",
    "icon": os.path.join(iconDir, 'option.png'),
    "launch": run_assignElementsToDenoise
}

resetRenderElementsTool = {
    'name': 'Reset Render Elements',
    'icon': os.path.join(iconDir, 'refresh.png'),
    'launch': run_resetRenderElements
}


tools = [
    loadRenderElements,
    saveRenderElements,
    assignRenderElementToLayer,
    assignElementsToDenoise,
    resetRenderElementsTool
]


PackageTool = {
    "name": "renderElements",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
