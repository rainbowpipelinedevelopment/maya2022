import importlib
import os
import functools

from PySide2 import QtWidgets, QtGui, QtCore
from maya.app.general.mayaMixin import MayaQWidgetDockableMixin
import maya.cmds as cmd

import api.json
import api.widgets.rbw_UI as rbwUI
import depts.lighting.tools.renderElements.manageRenderElements as manageRenderElements

importlib.reload(api.json)
# importlib.reload(rbwUI)
importlib.reload(manageRenderElements)


################################################################################
# @brief      This class describes an assign render elements to layer ui.
#
class AssignRenderElementToLayerUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super().__init__()

        if cmd.window('AssignRenderElementToLayerUI', exists=True):
            cmd.deleteUI('AssignRenderElementToLayerUI')

        self.content = os.getenv('CONTENT').replace('\\', '/')
        self.mcFolder = os.getenv('MC_FOLDER').replace('\\', '/')

        self.id_mv = os.getenv('ID_MV')

        self.templateLayerFolder = os.path.join(
            os.getenv('LOCALPIPE'),
            'json_file'
        ).replace('\\', '/')

        self.renderElementsRoot = manageRenderElements.renderElementsRoot
        self.renderElementsFolder = manageRenderElements.renderElementsFolder
        self.renderElementsSettingsFolder = manageRenderElements.renderElementsSettingsFolder

        self.getLayers()
        self.settings, self.elements = self.getSettings()

        api.log.logger().debug('settings: {}'.format(self.settings))
        api.log.logger().debug('elements: {}'.format(self.elements))

        if not self.settings:
            rbwUI.RBWError(title='ATTENTION', text='Sorry, before use this tool you have to use the "Save Render Elements" tool.')
            return

        self.setObjectName('AssignRenderElementToLayerUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topWidget = rbwUI.RBWFrame(layout='H', radius=5)

        # ------------------------------- LAYERS ----------------------------- #
        self.layerGroup = rbwUI.RBWGroupBox(title='Layers:', layout='V', fontSize=12)
        self.createLayerRadio()

        self.topWidget.addWidget(self.layerGroup)

        ########################################################################
        # RIGHT WIDGET
        #
        self.rightWidget = rbwUI.RBWFrame(layout='G', radius=5, bgColor='transparent', margins=[0, 0, 0, 0])

        # ------------------------------ ELEMENTS ---------------------------- #
        self.elementGroup = rbwUI.RBWGroupBox(title='Elements:', layout='V', fontSize=12)

        self.scrollWidget = QtWidgets.QScrollArea()
        self.scrollWidget.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.scrollWidget.setWidgetResizable(True)
        self.scrollWidget.setStyleSheet('background-color: transparent; border-color: transparent')

        self.elementWidget = rbwUI.RBWFrame(layout='V', margins=[0, 0, 3, 0], bgColor='transparent')

        self.scrollWidget.setWidget(self.elementWidget)

        self.elementGroup.addWidget(self.scrollWidget)

        self.rightWidget.layout.addWidget(self.elementGroup, 0, 0, 1, 3)

        # ---------------------------- SAVE BUTTON ----------------------------#
        self.saveButton = rbwUI.RBWButton(text='Save', icon=['save.png'], size=[150, 35])
        self.saveButton.clicked.connect(self.save)

        self.rightWidget.layout.addWidget(self.saveButton, 1, 1)

        self.topWidget.addWidget(self.rightWidget)

        self.mainLayout.addWidget(self.topWidget)

        self.setMinimumSize(650, 370)

        self.setTitle('Assign Render Elements To Layer UI')
        self.setIcon('option.png')
        self.setFocus()
        self.show()

    ############################################################################
    # @brief      Gets the layers.
    #
    def getLayers(self):
        self.layers = []
        for file in os.listdir(self.templateLayerFolder):
            if file.startswith('template'):
                if 'slapcomp' not in file:
                    self.layers.append(file.split('_')[1][:-5])

    ############################################################################
    # @brief      Sets the layer.
    #
    # @param      layer  The layer
    #
    def setLayer(self, layer):
        self.currentLayer = layer
        self.fillElementWidget()

    ############################################################################
    # @brief      Creates an element checkbox .
    #
    def fillElementWidget(self):
        for widget in self.elementWidget.widget.children():
            if isinstance(widget, rbwUI.RBWCheckBox):
                widget.setParent(None)
                widget.deleteLater()

        for element in self.elements:
            checkBox = rbwUI.RBWCheckBox(text=element)
            checkBox.setObjectName('{}_CheckBox'.format(element))
            checkBox.setChecked(self.settings[self.currentLayer][element])
            checkBox.stateChanged.connect(functools.partial(self.changeLayerSettings, element))
            self.elementWidget.addWidget(checkBox)

    ############################################################################
    # @brief      Gets the settings.
    #
    # @return     The settings.
    #
    def getSettings(self):
        settings = {}
        lastVersion = manageRenderElements.getRenderElementsSettingsVersion()
        lastVersionConfigFile = os.path.join(self.renderElementsSettingsFolder, 'renderElements_settings_v{}.json'.format(str(lastVersion).zfill(3))).replace('\\', '/')

        lastVersionElements = manageRenderElements.getRenderElementsVersion()
        if lastVersionElements != 0:
            lastVersionRenderElementsFile = os.path.join(self.renderElementsFolder, 'renderElements_v{}.json'.format(str(lastVersionElements).zfill(3))).replace('\\', '/')
            elements = api.json.json_read(lastVersionRenderElementsFile)['elements']
        else:
            return

        if os.path.exists(lastVersionConfigFile):
            oldSettings = api.json.json_read(lastVersionConfigFile)
            for layer in self.layers:
                layerSettings = {}
                for element in elements:
                    if layer in oldSettings:
                        if element in oldSettings[layer]:
                            layerSettings[element] = oldSettings[layer][element]
                        else:
                            layerSettings[element] = False
                    else:
                        layerSettings[element] = False
                settings[layer] = layerSettings
        else:
            for layer in self.layers:
                layerSettings = {}
                for element in elements:
                    layerSettings[element] = False
                settings[layer] = layerSettings

        return settings, elements

    ############################################################################
    # @brief      Creates a layer radio.
    #
    def createLayerRadio(self):
        for layer in self.layers:
            layerRadioButton = QtWidgets.QRadioButton(layer)
            layerRadioButton.setObjectName('{}_RadioButton'.format(layer))
            layerRadioButton.clicked.connect(functools.partial(self.setLayer, layer))
            self.layerGroup.addWidget(layerRadioButton)

    ############################################################################
    # @brief      change layer settings.
    #
    # @param      rowIndex  The row index
    # @param      state     The state
    #
    def changeLayerSettings(self, element, state):
        if state == 2:
            self.settings[self.currentLayer][element] = True
        else:
            self.settings[self.currentLayer][element] = False

    ############################################################################
    # @brief      save function.
    #
    def save(self):
        if not os.path.exists(self.renderElementsSettingsFolder):
            os.makedirs(self.renderElementsSettingsFolder)
        lastVersion = manageRenderElements.getRenderElementsSettingsVersion()
        newVersionConfigFile = os.path.join(self.renderElementsSettingsFolder, 'renderElements_settings_v{}.json'.format(str(lastVersion + 1).zfill(3))).replace('\\', '/')

        api.json.json_write(self.settings, newVersionConfigFile)

        rbwUI.RBWConfirm(title='SAVE COMPLETE', text='Settings save complete', defCancel=None)
