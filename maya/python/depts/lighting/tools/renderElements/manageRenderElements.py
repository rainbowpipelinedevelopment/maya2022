import importlib
import os
import re

import maya.cmds as cmd

import api.scene
import api.vray
import api.json

importlib.reload(api.scene)
importlib.reload(api.vray)
importlib.reload(api.json)


renderElementsRoot = os.path.join(os.getenv('CONTENT'), '_config', 'lightRig').replace('\\', '/')
renderElementsFolder = os.path.join(renderElementsRoot, 'renderElements').replace('\\', '/')
renderElementsSettingsFolder = os.path.join(renderElementsRoot, 'renderElementsSettings').replace('\\', '/')


################################################################################
# @brief      Gets the render elements version.
#
# @return     The render elements version.
#
def getRenderElementsVersion():
    if os.path.exists(renderElementsFolder):
        files = [file for file in os.listdir(renderElementsFolder) if file.endswith('.mb')]
        if files:
            files.sort()
            lastFile = files[-1]
            lastVersion = int(lastFile[:-3].split('_')[1].split('v')[1])
        else:
            lastVersion = 0
    else:
        lastVersion = 0

    return lastVersion


################################################################################
# @brief      Gets the render elements settings version.
#
# @return     The render elements settings version.
#
def getRenderElementsSettingsVersion():
    if os.path.exists(renderElementsSettingsFolder):
        files = [file for file in os.listdir(renderElementsSettingsFolder) if file.endswith('.json')]

        if files:
            files.sort()
            lastFile = files[-1]
            lastVersion = int(lastFile[:-5].split('_')[-1].split('v')[1])
        else:
            lastVersion = 0
    else:
        lastVersion = 0

    return lastVersion


################################################################################
# @brief      load function.
#
def load(merge=False):
    # get last version
    lastVersion = getRenderElementsVersion()

    # build file path
    if lastVersion == 0:
        fullPath = os.path.join(renderElementsRoot, 'renderElements.mb').replace('\\', '/')
    else:
        fullPath = os.path.join(renderElementsFolder, 'renderElements_v{}.mb'.format(str(lastVersion).zfill(3))).replace('\\', '/')

    # load the file
    if not merge:
        cmd.file(fullPath, open=True, force=True)
    else:
        cmd.file(fullPath, i=True)


################################################################################
# @brief      save function.
#
def save():
    if not os.path.exists(renderElementsFolder):
        os.makedirs(renderElementsFolder)
    newFileName = os.path.join(renderElementsFolder, 'renderElements_v{}.mb'.format(str(getRenderElementsVersion() + 1).zfill(3))).replace('\\', '/')
    cmd.file(rename=newFileName)
    cmd.file(save=True, force=True, type='mayaBinary')

    exportElememnts()


################################################################################
# @brief      export the render elements ina json file.
#
def exportElememnts():
    fileName = api.scene.scenename()
    exportFileName = fileName.replace('.mb', '.json')

    elements = {'elements': api.vray.render_elements()}
    api.json.json_write(elements, exportFileName)
