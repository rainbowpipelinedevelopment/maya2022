import importlib
import os

from PySide2 import QtWidgets, QtGui, QtCore
import maya.cmds as cmd

import functools
import api.json
import api.widgets.rbw_UI as rbwUI

importlib.reload(functools)
importlib.reload(api.json)
# importlib.reload(rbwUI)


renderElementsRoot = os.path.join(os.getenv('CONTENT'), '_config', 'lightRig').replace('\\', '/')
renderElementsFolder = os.path.join(renderElementsRoot, 'renderElements').replace('\\', '/')
renderElementsDenoiseSettingsFolder = os.path.join(renderElementsRoot, 'renderElementsDenoiseSettings').replace('\\', '/')


################################################################################
# @brief      This class describes an assign elements to denoise.
#
class AssignRenderElementsToDenoiseUI(rbwUI.RBWWindow):

    ###############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super().__init__()

        if cmd.window('AssignRenderElementsToDenoiseUI', exists=True):
            cmd.deleteUI('AssignRenderElementsToDenoiseUI')

        self.renderElementsFolder = renderElementsFolder
        self.renderElementsDenoiseSettingsFolder = renderElementsDenoiseSettingsFolder

        self.denoiseSettings, self.elements = self.getDenoiseSettings()

        self.setObjectName('AssignRenderElementsToDenoiseUI')
        self.initUI()
        self.updateMargins()

    ###############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topWidget = rbwUI.RBWFrame(layout='G', radius=5)

        # ------------------------------ ELEMENTS ---------------------------- #
        self.elementGroup = rbwUI.RBWGroupBox(title='Elements:', layout='V', fontSize=12)

        self.scrollWidget = QtWidgets.QScrollArea()
        self.scrollWidget.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.scrollWidget.setWidgetResizable(True)
        self.scrollWidget.setStyleSheet('background-color: transparent; border-color: transparent')

        self.elementWidget = rbwUI.RBWFrame(layout='V', margins=[0, 0, 3, 0], bgColor='transparent')

        self.scrollWidget.setWidget(self.elementWidget)

        self.elementGroup.addWidget(self.scrollWidget)

        self.topWidget.layout.addWidget(self.elementGroup, 0, 0, 1, 3)

        self.createElementCheckBox()

        # SAVE BUTTON
        self.saveButton = rbwUI.RBWButton(text='Save', icon=['save.png'], size=[150, 35])
        self.saveButton.clicked.connect(self.save)
        self.topWidget.layout.addWidget(self.saveButton, 1, 1)

        self.mainLayout.addWidget(self.topWidget)

        self.setMinimumSize(450, 500)

        self.setTitle('Assign RE to Denoise Job')
        self.setIcon('option.png')
        self.setFocus()
        self.show()

    ###############################################################################
    # @brief      Gets the denoise settings.
    #
    # @return     The denoise settings.
    #
    def getDenoiseSettings(self):
        denoiseSettings = {}
        lastVersion = self.getRenderElementsDenoiseSettingsVersion()
        lastVersionConfigFile = os.path.join(self.renderElementsDenoiseSettingsFolder, 'renderElements_denoiseSettings_v{}.json'.format(str(lastVersion).zfill(3))).replace('\\', '/')

        elements = self.getElements()
        elements.sort()

        if os.path.exists(lastVersionConfigFile):
            oldSettings = api.json.json_read(lastVersionConfigFile)
            denoiseSettings = oldSettings
        else:
            for element in elements:
                denoiseSettings[element] = False

        return denoiseSettings, elements

    ###############################################################################
    # @brief      Gets the elements.
    #
    def getElements(self):
        lastVersionElements = self.getRenderElementsVersion()
        if lastVersionElements != 0:
            lastVersionRenderElementsFile = os.path.join(self.renderElementsFolder, 'renderElements_v{}.json'.format(str(lastVersionElements).zfill(3))).replace('\\', '/')
            elements = api.json.json_read(lastVersionRenderElementsFile)['elements']
            return elements
        else:
            return

    ###############################################################################
    # @brief      Creates element switches.
    #
    def createElementCheckBox(self):
        for element in self.elements:
            checkBox = rbwUI.RBWCheckBox(text=element)
            checkBox.setObjectName('{}_CheckBox'.format(element))
            if self.denoiseSettings[element]:
                checkBox.setChecked(True)
            self.elementWidget.addWidget(checkBox)

    ###############################################################################
    # @brief      saves render elements denoise settings.
    #
    def save(self):
        checkBoxes = [child for child in self.elementWidget.children() if isinstance(child, rbwUI.RBWCheckBox)]
        for checkBox in checkBoxes:
            element = checkBox.objectName().split('_CheckBox')[0]
            if checkBox.isChecked():
                self.denoiseSettings[element] = True
            else:
                self.denoiseSettings[element] = False

        if not os.path.exists(self.renderElementsDenoiseSettingsFolder):
            os.makedirs(self.renderElementsDenoiseSettingsFolder)
        lastVersion = self.getRenderElementsDenoiseSettingsVersion()
        newVersionConfigFile = os.path.join(self.renderElementsDenoiseSettingsFolder, 'renderElements_denoiseSettings_v{}.json'.format(str(lastVersion + 1).zfill(3))).replace('\\', '/')

        api.json.json_write(self.denoiseSettings, newVersionConfigFile)

        rbwUI.RBWConfirm(title='SAVE COMPLETE', text='Settings save complete', defCancel=None)

    ################################################################################
    # @brief      Gets the render elements denoise settings version.
    #
    # @return     The render elements denoise settings version.
    #
    def getRenderElementsDenoiseSettingsVersion(self):
        if os.path.exists(self.renderElementsDenoiseSettingsFolder):
            files = [file for file in os.listdir(self.renderElementsDenoiseSettingsFolder) if file.endswith('.json')]

            if files:
                files.sort()
                lastFile = files[-1]
                lastVersion = int(lastFile[:-5].split('_')[-1].split('v')[1])
            else:
                lastVersion = 0
        else:
            lastVersion = 0

        return lastVersion

    ################################################################################
    # @brief      Gets the render elements version.
    #
    # @return     The render elements version.
    #
    def getRenderElementsVersion(self):
        if os.path.exists(self.renderElementsFolder):
            files = [file for file in os.listdir(self.renderElementsFolder) if file.endswith('.mb')]
            if files:
                files.sort()
                lastFile = files[-1]
                lastVersion = int(lastFile[:-3].split('_')[1].split('v')[1])
            else:
                lastVersion = 0
        else:
            lastVersion = 0

        return lastVersion
