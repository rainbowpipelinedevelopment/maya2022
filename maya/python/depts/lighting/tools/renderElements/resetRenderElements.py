import importlib
import os
import maya.cmds as cmd

import depts.lighting.tools.renderElements.manageRenderElements as manageRE
import depts.lighting.tools.sceneSetup.sceneSetup as lgtSetUp
import depts.lighting.tools.sceneSetup.cleanUpMl as cleanML
import api.renderlayer as rl
import api.vray as vray
import api.json as json
import api.log as log
import api.widgets.rbw_UI as rbwUI

importlib.reload(manageRE)
importlib.reload(lgtSetUp)
importlib.reload(cleanML)
importlib.reload(rl)
importlib.reload(vray)
importlib.reload(json)
importlib.reload(log)
# importlib.reload(rbwUI)


def resetRenderElements():
    log.logger().debug('Resetting default Render Elements...')

    grps = ['diff_ML', 'gidiff_ML', 'spec_ML', 'gispec_ML']
    for grp in grps:
        for i in range(0, 15):
            grpName = '{}{}'.format(grp, str(i))
            if cmd.objExists(grpName):
                setMembers = cmd.listConnections('{}.dagSetMembers'.format(grpName))
                if setMembers:
                    for lightML in setMembers:
                        cmd.sets(lightML, rm=grpName)
    log.logger().debug('ML sets emptied')

    defaultLayers = {}
    jsonPath = os.path.join(manageRE.renderElementsFolder, 'renderElements_v{}.json'.format(str(manageRE.getRenderElementsVersion()).zfill(3)))
    defaultLayers = json.json_read(jsonPath)

    if defaultLayers:
        defaultRe = [defaultLayers[key] for key in defaultLayers][0]
        rElements = vray.render_elements()
        for el in rElements:
            if el in defaultRe:
                cmd.delete(el)
        log.logger().debug('Default prj Render Elements deleted from scene')

        lgtSetUp.importRenderElements()
        log.logger().debug('Imported latest Render Elements')

        rElements = vray.render_elements()
        renderElementsSettingsPath = os.path.join(manageRE.renderElementsSettingsFolder, 'renderElements_settings_v{}.json'.format(str(manageRE.getRenderElementsSettingsVersion()).zfill(3)))
        if os.path.exists(renderElementsSettingsPath):
            renderElementsSettingsData = json.json_read(renderElementsSettingsPath)

            for layer in rl.list():
                layerData = renderElementsSettingsData[layer.split('_')[0]]
                for renderElement in rElements:
                    if renderElement in layerData and not layerData[renderElement]:
                        rl.createAoOverride(layer, renderElement, 0)
            log.logger().debug('Render Elements default values ovverride done')
        else:
            rbwUI.RBWWarnig(title='Warning', text="I can't find the default RE settings json file!\nPlease contact Pipeline Department")

        lgtSetUp.fillMLSet()
        log.logger().debug('ML sets setted')

        cleanML.turnOffUnusedPass()
        log.logger().debug('ML cleaned')

    else:
        rbwUI.RBWWarnig(title='Warning', text="I can't find the default RE json file!\nPlease contact Pipeline Department")

    log.logger().debug('Default Render Elements resetted.')
