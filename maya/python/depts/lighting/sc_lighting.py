import importlib
import os

import maya.cmds as cmd

import api.log
import api.widgets.rbw_UI as rbwUI
import api.vray
import api.renderlayer
import depts.sc_depts as scDepts

importlib.reload(api.log)
importlib.reload(rbwUI)
importlib.reload(api.vray)
importlib.reload(scDepts)
importlib.reload(api.renderlayer)


################################################################################
# @brief      This class describes a lighting.
#
class Lighting(scDepts.Dept):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.name = "Lgt"
        self.isPromoted = True if self.savedShot.promoted == 1 else False

    ############################################################################
    # @brief      { function_description }
    #
    # @param      kwargs  The keywords arguments
    #
    def load(self, mode='load', confirm=True):
        super(Lighting, self).load(mode, confirm)
        self.sanizeVrscene()

        if not api.vray.isVrayRightLoaded():
            message = 'Sorry but vray is not loaded right.\nPlease follow this few passages.\n1) Close the scene.\n2) Unload and uncheck autoload vray.\n3) Close Maya.\n4) Restart Maya.\n5) Reload Vray.\n6) Reopen the scene.'
            rbwUI.RBWError(text=message)

    ###########################################################################
    # @brief      sc save method
    #
    # @param      comment  The comment
    #
    def save(self):
        self.realSave()
        self.exportXML()
        self.setSceneLayer()

        if self.isPromoted:
            self.exportSetupFromScene()

        return True

    ############################################################################
    # @brief      Gets the scene layers.
    #
    # @return     The scene layers.
    #
    def getSceneLayers(self):
        dbSceneLayersQuery = "SELECT * FROM `renderLayer` WHERE `type`='shot' AND `linkId`={}".format(self.savedShot.getShotId())
        dbSceneLayersRecords = api.database.selectQuery(dbSceneLayersQuery)

        if dbSceneLayersRecords:
            dbSceneLayers = {}
            for layer in dbSceneLayersRecords:
                dbSceneLayers[layer[1]] = {'enabled': layer[4], 'id': layer[0]}
            return dbSceneLayers
        else:
            return None

    ############################################################################
    # @brief      Sets the scene layer.
    #
    def setSceneLayer(self):
        sceneLayers = api.renderlayer.list()
        dbSceneLayers = self.getSceneLayers()

        toCreate = []

        if dbSceneLayers:
            toCheckLayers = list(dbSceneLayers.keys())[:]
            for renderLayer in sceneLayers:
                if renderLayer not in dbSceneLayers:
                    toCreate.append(renderLayer)
                else:
                    if dbSceneLayers[renderLayer]['enabled'] == 0:
                        updateQuery = "UPDATE `renderLayer` SET `enabled`=1 WHERE `id`={}".format(dbSceneLayers[renderLayer]['id'])
                        api.database.deleteQuery(updateQuery)
                    toCheckLayers.remove(renderLayer)

            if len(toCheckLayers) > 0:
                for layer in toCheckLayers:
                    if not layer.startswith('DEEP_'):
                        disableQuery = "UPDATE `renderLayer` SET `enabled`=0 WHERE `id`={}".format(dbSceneLayers[layer]['id'])
                        api.database.deleteQuery(disableQuery)
        else:
            toCreate = sceneLayers[:]

        for renderLayer in toCreate:
            fields = 'name, type, linkId, enabled'
            args = (renderLayer, 'shot', self.savedShot.getShotId(), 1)
            pers = ", ".join(["\'{}\'".format(o) for o in args])

            insertQuery = 'INSERT INTO `renderLayer` (%s) VALUES (%s) ' % (fields, pers)

            api.log.logger().debug(insertQuery)

            layerId = api.database.insertQuery(insertQuery)

            api.log.logger().debug('Successfully create render layer {} record with id: {}'.format(renderLayer, layerId))

    ############################################################################
    # @brief      export Render Layer Settings From shot.
    #
    # @param      layerList  The layer list
    #
    def exportRenderLayers(self, layerList):
        exportFolder = os.path.join(self.savedShot.savedShotFolder, 'export', 'renderLayers').replace('\\', '/')
        if not os.path.exists(exportFolder):
            os.makedirs(exportFolder)

        for layer in layerList:
            fileName = '{}_{}.json'.format(layer, self.savedShot.version)
            exportPath = os.path.join(exportFolder, fileName).replace('\\', '/')
            api.renderlayer.exportSelectedRenderLayer(layer, exportPath)
            api.renderlayer.cleanUpRenderElement(exportPath)

    ############################################################################
    # @brief      export all lightRigs From shot.
    #
    def exportLightRigs(self):
        exportFolder = os.path.join(self.savedShot.savedShotFolder, 'export').replace('\\', '/')
        if not os.path.exists(exportFolder):
            os.makedirs(exportFolder)

        shotInfoList = self.savedShot.sceneName.split('_')

        if cmd.objExists('LIGHTS'):
            lights = cmd.listRelatives('LIGHTS')
            for light in lights:
                cmd.parent('LIGHTS|{}'.format(light), world=True)
                cmd.select(clear=True)
                cmd.select(light, replace=True)

                if '_Env_' in light:
                    constraints = True
                else:
                    constraints = False

                exportLightFileName = '{}_{}_{}_{}_{}_{}_{}.ma'.format(
                    shotInfoList[0],
                    shotInfoList[1],
                    shotInfoList[2],
                    shotInfoList[3],
                    shotInfoList[4],
                    light.replace(':', '_'),
                    shotInfoList[7]
                )

                api.log.logger().debug('exportLightFileName: {}'.format(exportLightFileName))

                exportLightPath = os.path.join(exportFolder, exportLightFileName).replace('\\', '/')
                cmd.file(exportLightPath, force=True, options='v=0;', type='mayaAscii', preserveReferences=True, exportSelected=True, constructionHistory=False, constraints=constraints)
                cmd.parent(light, 'LIGHTS')

    ############################################################################
    # @brief      export Goeproxy GRP From shot.
    #
    def exportBlockers(self):
        exportFolder = os.path.join(self.savedShot.savedShotFolder, 'export').replace('\\', '/')
        if not os.path.exists(exportFolder):
            os.makedirs(exportFolder)

        if cmd.objExists('GEOPROXY_GRP'):
            cmd.select(clear=True)
            cmd.select('GEOPROXY_GRP')
            shotInfoList = self.savedShot.sceneName.split('_')
            geoproxyFileName = '{}_{}_{}_{}_{}_GEOPROXY_GRP_{}.ma'.format(
                shotInfoList[0],
                shotInfoList[1],
                shotInfoList[2],
                shotInfoList[3],
                shotInfoList[4],
                shotInfoList[7]
            )

            exportGeoproxyPath = os.path.join(exportFolder, geoproxyFileName).replace('\\', '/')
            cmd.file(exportGeoproxyPath, force=True, options='v=0;', type='mayaAscii', preserveReferences=True, exportSelected=True)

    ############################################################################
    # @brief      export scene render elements.
    #
    def exportRenderElements(self):
        setsData = {}
        sets = cmd.ls(type='VRayRenderElementSet')
        for set in sets:
            setsData[set] = cmd.sets(set, query=True)
            cmd.sets(clear=set)

        exportFolder = os.path.join(self.savedShot.savedShotFolder, 'export').replace('\\', '/')
        if not os.path.exists(exportFolder):
            os.makedirs(exportFolder)

        shotInfoList = self.savedShot.sceneName.split('_')

        exportRenderElementsFileName = '{}_{}_{}_{}_{}_{}_{}.mb'.format(
            shotInfoList[0],
            shotInfoList[1],
            shotInfoList[2],
            shotInfoList[3],
            shotInfoList[4],
            'renderElements',
            shotInfoList[7]
        )

        api.renderlayer.switchByName(api.renderlayer.getDefaultRenderLayer())

        renderElements = api.vray.render_elements()
        toExport = renderElements[:]

        for element in renderElements:
            if ':' in element:
                toExport.remove(element)

        cmd.select(clear=True)
        cmd.select(toExport, noExpand=True)

        exportRenderElementsPath = os.path.join(exportFolder, exportRenderElementsFileName).replace('\\', '/')
        api.vray.exportRenderElements(exportRenderElementsPath)
        cmd.select(clear=True)

        for set in setsData:
            cmd.sets(setsData[set], forceElement=set)

    ############################################################################
    # @brief      export all the necessary from the lighting shot.
    #
    def exportSetupFromScene(self):
        renderLayers = api.renderlayer.list()
        if renderLayers is not None:
            self.exportRenderLayers(renderLayers)
            self.exportLightRigs()
            self.exportBlockers()
            self.exportRenderElements()
