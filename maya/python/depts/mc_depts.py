import importlib
import os
import xml.etree.cElementTree as ET
import xml.dom.minidom as xdm

import maya.cmds as cmd

import api.log
import api.database
import api.scene
import api.shotgrid
import api.vray
import api.export
import api.widgets.rbw_UI as rbwUI
import utilz.snapshot
import config.dictionaries

importlib.reload(api.log)
importlib.reload(api.database)
importlib.reload(api.scene)
importlib.reload(api.shotgrid)
importlib.reload(api.vray)
importlib.reload(api.export)
# importlib.reload(rbwUI)
importlib.reload(utilz.snapshot)
importlib.reload(config.dictionaries)


################################################################################
# @brief      This class describes a dept.
#
class Dept(object):

    ############################################################################
    # Constructs a new instance.
    #
    #
    def __init__(self, savedAsset):
        self.deptName = "dept"
        self.savedAsset = savedAsset

        if cmd.objExists("_UNKNOWN_REF_NODE_"):
            cmd.delete("_UNKNOWN_REF_NODE_")
        if cmd.objExists("*:_UNKNOWN_REF_NODE_"):
            try:
                cmd.delete("*:_UNKNOWN_REF_NODE_")
            except:
                pass

        self.id_mv = self.getAsset().id_mv
        self.projectName = self.getAsset().projectName
        self.projectFolder = self.getAsset().content
        self.tempFolder = os.path.join(os.getenv('HOMEDRIVE'), os.getenv('HOMEPATH'), 'Documents', 'RBW_maya_tmp').replace('\\', '/')

        try:
            self.path = os.path.join(self.savedAsset.savedAssetFolder, '{}.mb'.format(self.savedAsset.sceneName)).replace('\\', '/')
        except AttributeError:
            self.path = os.path.join(self.projectFolder, self.savedAsset.path).replace('\\', '/')

        self.shotgridObj = api.shotgrid.ShotgridObj()

        self.errors = []

    ############################################################################
    # @brief      load function.
    #
    # @param      mode    The mode
    # @param      params  The parameters
    #
    def load(self, mode='load', params=None):
        if mode == 'load':
            cmd.file(self.path, open=True, force=True)
        elif mode == 'merge':
            cmd.file(self.path, i=True, options="v=0;p=17", loadReferenceDepth="all")
        else:
            ext = 'mayaBinary'

            if params:
                if 'namespace' not in params:
                    namespace = self.savedAsset.getNamespace()
                else:
                    namespace = params['namespace']

                if 'vrmesh' in params and params['vrmesh']:
                    self.path = self.path.replace('.mb', '_vrmesh.ma')
                    ext = 'mayaAscii'
            else:
                namespace = self.savedAsset.getNamespace()

            api.scene.createReference(self.path, ext, namespace)

    ############################################################################
    # @brief      save function.
    #
    def save(self):
        if self.isDef():
            self.deleteUnusedShapeOrig()
            self.cleanupDisplayLayers()

        self.checkTexturesPath()

        self.localSave()

        self.realSave()

        self.exportXML()

        return True

    ############################################################################
    # @brief      tag the meshes.
    #
    def tagMeshes(self):
        saveNode = '{}_saveNode'.format(self.savedAsset.dept)

        # === tag each mesh (only modeling and surfacing) ===
        if cmd.objExists(saveNode):
            # get meshes
            if cmd.objExists("CTRL_set"):
                meshes = cmd.listRelatives("CTRL_set", children=True, allDescendents=True, type="mesh", noIntermediate=True)
            else:
                # caso per characters models che non hanno il ctrl set...
                api.log.logger().debug("This model has been finalized as a character model (without the CTRL_set).")

                topGroup = self.getGroupName()
                meshes = cmd.listRelatives(topGroup, children=True, allDescendents=True, type="mesh", noIntermediate=True, fullPath=True)

            # get publish informations and set them to each mesh shape
            # (to have in future to know for each mesh where coming from..)
            allAttrs = cmd.listAttr(saveNode)
            coolAttrs = config.dictionaries.saveLabels
            for attr in allAttrs:
                if attr in coolAttrs:
                    api.log.logger().debug(attr)
                    val = cmd.getAttr('{}.{}'.format(saveNode, attr))
                    if meshes:
                        for mesh in meshes:
                            if cmd.objExists('{}.{}_sn_{}'.format(mesh, self.savedAsset.dept, attr)):
                                cmd.setAttr('{}.{}_sn_{}'.format(mesh, self.savedAsset.dept, attr), lock=False)
                            else:
                                cmd.addAttr(mesh, longName='{}_sn_{}'.format(self.savedAsset.dept, attr), dataType='string')
                            # riempio i campi con i nuovi valori in entrambi i casi
                            # (esisteva o nuovo)
                            cmd.setAttr('{}.{}_sn_{}'.format(mesh, self.savedAsset.dept, attr), val, type="string")
                            cmd.setAttr('{}.{}_sn_{}'.format(mesh, self.savedAsset.dept, attr), lock=True)
                    else:
                        api.log.logger().debug("There is no mesh!")

    ############################################################################
    # @brief      Gets the group name.
    #
    # @return     The group name.
    #
    def getGroupName(self):
        api.log.logger().debug(" === Get group name ===")
        if hasattr(self.savedAsset, 'selection') and self.savedAsset.selection:
            api.log.logger().debug("export selected: {}".format(self.savedAsset.selection))
            return self.savedAsset.selection

        else:
            result = api.scene.getGroupName()

            if isinstance(result, str):
                return result
            elif isinstance(result, list):
                errorMessage = 'Warning the are more than one top group in this scene: {}'.format(all)
                api.log.logger().error(errorMessage)
                self.errors.append(errorMessage)
                return

    ############################################################################
    # @brief      rename the scene and then save.
    #
    # @param      path    The path
    # @param      format  The format
    #
    def renameAndSave(self, path, format):
        try:
            showFramerateQuery = "SELECT name FROM `p_prj_framerate` AS pf JOIN `p_framerate` AS f ON pf.framerate_id = f.id WHERE pf.id_mv = {}".format(self.id_mv)
            showFps = api.database.selectSingleQuery(showFramerateQuery)[0]
            if showFps.count('Frame'):
                showFps = showFps.split('Frame')[0]
            cmd.currentUnit(time=showFps.lower())
        except TypeError:
            cmd.currentUnit(time='pal')
            api.log.logger().error('Can\'t get fps from DB, set to (pal) by default')

        remotePath = path
        cmd.file(rename=remotePath)
        if hasattr(self.savedAsset, 'selection') and self.savedAsset.selection:
            self.exportSelection(format)
            api.log.logger().debug("Export selection to {}".format(remotePath))
        else:
            api.log.logger().debug("Saving to {}".format(remotePath))
            cmd.file(save=True, force=True, type=format)

    ############################################################################
    # @brief      local save on user machine.
    #
    def localSave(self):
        filename = '{}.mb'.format(self.savedAsset.sceneName)
        localPath = os.path.join(self.tempFolder, filename).replace('\\', '/')

        if not os.path.isdir(self.tempFolder):
            os.makedirs(self.tempFolder)

        cmd.file(rename=localPath)
        api.log.logger().debug('Local save on {}'.format(localPath))

        # eliino un eventuale omonimo
        if os.path.exists(localPath):
            os.remove(localPath)

        cmd.file(save=True, force=True, type='mayaBinary')

    ############################################################################
    # @brief      real save on server function.
    #
    def realSave(self):
        api.log.logger().debug('realSave maya binary (default)')
        api.log.logger().debug('server save')

        # some cleanup operations
        api.scene.badPluginCleanUp()

        if cmd.unknownPlugin(query=True, list=True) is not None:
            api.scene.pluginCheck()

        # special clean up
        api.scene.specialCleanup()

        # real save maya ascii
        try:
            maPath = os.path.join(self.savedAsset.savedAssetFolder, '{}.ma'.format(self.savedAsset.sceneName)).replace('\\', '/')
            self.renameAndSave(maPath, 'mayaAscii')
            api.log.logger().debug('ma saved in: {}'.format(maPath))
        except RuntimeError:
            rbwUI.RBWWarning(text='Error: Cannot save in Maya Ascii! The save will continue.\nPlease contact pipeline department')

        # real save maya binary
        mbPath = maPath.replace('.ma', '.mb')
        self.renameAndSave(maPath.replace('.ma', '.mb'), 'mayaBinary')
        api.log.logger().debug('mb saved in: {}'.format(mbPath))
        cmd.file(rename=mbPath)

        # === take a snapshot ===
        utilz.snapshot.snapshots(mbPath, rigging=True if self.deptName == 'rigging' else False)

        # create publish on shotgun
        try:
            if self.isDef():
                self.saveOnShotgrid()
        except:
            api.log.logger().error("Issues at publishing on Shotgun... Skipped!")

    ############################################################################
    # @brief      export selection.
    #
    # @param      format  The format
    #
    def exportSelection(self, format):
        cmd.select(self.savedAsset.selection)
        if cmd.objExists('MSH'):
            cmd.select('MSH', ne=True, add=True)
        cmd.file(exportSelected=True, options='v=0', typ=format, preserveReferences=1)
        return

    ############################################################################
    # @brief      export xml from the current asset.
    #
    # @return     export success.
    #
    def exportXML(self):
        path = os.path.join(self.savedAsset.savedAssetFolder, '{}.xml'.format(self.savedAsset.sceneName)).replace('\\', '/')
        root = ET.Element("scene", name=cmd.file(query=True, sceneName=True, shortName=True))

        self.buildXML(root)
        self.getRequiredAssett(root)

        if root:
            tree = ET.ElementTree(root)
            tree.write(path)

            xml = xdm.parse(path)
            pretty_xml_as_string = xml.toprettyxml(indent='\t')

            with open(path, "w") as f:
                f.write(pretty_xml_as_string)

            return True

    ############################################################################
    # @brief      Builds a xml.
    #
    # @param      root  The root
    #
    def buildXML(self, root):
        exclude = ['|front', '|persp', '|side', '|top', '|blendshape']
        transformList = cmd.ls(assemblies=True, long=True)

        for i in exclude:
            if i in transformList:
                transformList.remove(i)

        meshes = ET.SubElement(root, "meshes")

        for transform in transformList:
            self.buildNode(transform, meshes)

    ############################################################################
    # @brief      Builds a node.
    #
    # @param      node        The node
    # @param      parentNode  The parent node
    #
    def buildNode(self, node, parentNode):
        sn = cmd.ls(node)[0].split(":")[0].split('|')
        if cmd.objectType(node, i="transform"):
            children = cmd.listRelatives(node, children=True, fullPath=True)
            newNode = ET.SubElement(parentNode, "transform", fullpath=node)
            newNode.text = sn[len(sn) - 1]
            if children:
                for child in children:
                    self.buildNode(child, newNode)
        else:
            vert = str(cmd.polyEvaluate(node, v=1))
            if cmd.objectType(node, i='mesh'):

                mesh = node.split('|')[-1]
                miFile = None
                try:
                    miFile = cmd.getAttr(mesh + '.miProxyFile')
                except:
                    pass
                if not miFile:
                    newNode = ET.SubElement(parentNode, "shape", vertices=vert, fullpath=node)
                else:
                    mcName = 'mc_{}'.format(self.projectName)
                    mcVar = '${' + mcName + '}'
                    miFile = mcVar + miFile.split(mcName)[1]
                    newNode = ET.SubElement(parentNode, "shape", vertices=vert, fullpath=node, miPath=miFile)
            else:
                newNode = ET.SubElement(parentNode, "shape", vertices=vert, fullpath=node)
            newNode.text = sn[len(sn) - 1]

    ############################################################################
    # @brief      Gets the required assett.
    #
    # @param      root  The root
    #
    def getRequiredAssett(self, root):
        assets = {}
        assets["reference"] = []
        assets["texture"] = []
        assets["cache"] = []
        assets["gpucaches"] = []

        # retrieve reference
        for reference in cmd.ls(references=True):
            try:
                referencePath = cmd.referenceQuery(reference, filename=True, unresolvedName=True)
                if not referencePath.count(".mb{") and not referencePath.count(".ma{"):
                    if referencePath not in assets["reference"]:
                        assets["reference"].append(referencePath)
            except:
                pass

        # retrieve textures
        for texture in cmd.ls(type="file"):
            texturePath = cmd.getAttr("{}.fileTextureName".format(texture))
            if texturePath not in assets["texture"]:
                assets["texture"].append(texturePath)

        # retrieve caches
        for geoCache in cmd.ls(type='cacheFile'):
            geoCachePath = cmd.getAttr("{}.cachePath".format(geoCache))
            if geoCachePath not in assets["cache"]:
                assets["cache"].append(geoCachePath)

        # retrieve caches from alembic node
        for alembicCache in cmd.ls(type='AlembicNode'):
            alembicCachePath = cmd.getAttr("{}.abc_File".format(alembicCache))
            if alembicCachePath not in assets["cache"]:
                assets["cache"].append(alembicCachePath)

        # retrieve GPU caches
        for gpuCache in cmd.ls(type='gpuCache'):
            gpuCachePath = cmd.getAttr("{}.cacheFileName".format(gpuCache))
            if gpuCachePath not in assets["gpucaches"]:
                assets["gpucaches"].append(gpuCachePath)

        # create asset elements
        if assets:
            requiresNode = ET.SubElement(root, "required_assetts")
            for requireType in assets:
                if assets[requireType]:
                    for require in assets[requireType]:
                        assettElement = ET.SubElement(requiresNode, requireType, path=require)

    ############################################################################
    # @brief      delete the unused shape orig.
    #
    def deleteUnusedShapeOrig(self):
        shapes = [shape for shape in cmd.ls(type='shape') if shape.endswith('Orig')]
        for shape in shapes:
            if not cmd.listConnections(shape):
                cmd.delete(shape)

    ############################################################################
    # @brief      clean display layers.
    #
    def cleanupDisplayLayers(self):
        layers = cmd.ls(long=True, type='displayLayer')
        for layer in layers:
            if layer.find('defaultLayer') == -1:
                cmd.setAttr('{}.visibility'.format(layer), 1)
                cmd.delete(layer)

    ############################################################################
    # @brief      Saves on shotgrid.
    #
    def saveOnShotgrid(self):
        currentVersionInt = int(self.savedAsset.version[2:])
        path = os.path.normpath(os.path.join(self.savedAsset.savedAssetFolder, '{}.mb'.format(self.savedAsset.sceneName)))

        thumbnailPath = os.path.join(self.savedAsset.savedAssetFolder, "snapshots", "{}_persp.jpg".format(self.savedAsset.sceneName)).replace('\\', '/')
        if not os.path.exists(thumbnailPath):
            thumbnailPath = thumbnailPath.replace("_persp.jpg", ".jpg")

        shotgridPublishParams = {
            "filename": '{}.mb'.format(self.savedAsset.sceneName),
            "path": path,
            "thumbnailPath": thumbnailPath,
            "assetId": self.getSgId(),
            "status": "ip",
            "versionNumber": currentVersionInt,
            "description": '{}\n{}'.format(os.getenv('USERNAME'), self.savedAsset.note),
            "dept": self.savedAsset.dept
        }

        api.log.logger().debug("-" * 60)
        api.log.logger().debug("Saving asset on shotgun..")
        api.log.logger().debug("-" * 60)
        publishFile = self.shotgridObj.createScenePublish(shotgridPublishParams, isMayaScene=True)
        self.shotgridObj.updateLastDeptPublish(publishFile)

    ############################################################################
    # @brief      check the texture path and fix it.
    #
    def checkTexturesPath(self):
        api.log.logger().debug('Replacing absolute paths with relative ones')
        workspacePathNew = '{}/'.format(api.scene.workspace())
        wrongWorkspacePathNew = workspacePathNew.replace('//speed.nas/', '//speed.nas.rbw.cgi/')

        for fileNode in cmd.ls(type="file"):
            oldTexturePath = cmd.getAttr('{}.fileTextureName'.format(fileNode))
            newTexturePath = self.getCaseSensitivePath(oldTexturePath)
            if workspacePathNew in newTexturePath:
                cmd.setAttr('{}.fileTextureName'.format(fileNode), newTexturePath.replace(workspacePathNew, ""), type='string')
            elif wrongWorkspacePathNew in newTexturePath:
                cmd.setAttr('{}.fileTextureName'.format(fileNode), newTexturePath.replace(wrongWorkspacePathNew, ""), type='string')

    ############################################################################
    # @brief      Gets the case sensitive path.
    #
    # @param      path  The path
    #
    # @return     The case sensitive path.
    #
    def getCaseSensitivePath(self, path):
        pathUp = os.path.dirname(path)
        drive = os.path.splitdrive(path)[0]

        if pathUp == path and pathUp[:-1] == drive:
            return drive

        if pathUp == path or path == '//':
            return ''
        else:
            try:
                listDirs = os.listdir(pathUp)
            except WindowsError:
                return path

            pathBasename = os.path.basename(path)

            if pathBasename in listDirs:
                return os.path.join(self.getCaseSensitivePath(pathUp), pathBasename).replace('\\', '/')
            else:
                for el in listDirs:
                    if pathBasename.lower() == el.lower():
                        return os.path.join(self.getCaseSensitivePath(pathUp), el).replace('\\', '/')

    ##################################################################################
    # @brief      check in the scene if there are double names for shapes or trasforms
    #
    # @return     the check exit.
    #
    def checkForDoubleNames(self):
        success = 1
        allTopNodes = cmd.ls(assemblies=1)
        cmd.select(allTopNodes, replace=1)

        items = cmd.listRelatives(cmd.ls(selection=1), allDescendents=1)

        counter = 0
        for i in range(0, len(items), 1):
            if items.count(items[i]) != 1:
                api.log.logger().debug('This item has the same name of another one:')
                api.log.logger().debug(items[i])
                counter = counter + 1
                self.errors.append('This item has the same name of another one: {}'.format(items[i]))

        if counter > 0:
            api.log.logger().debug('Naming Convention problem: ')
            api.log.logger().debug('In this model you have {} items with similar names. Please check the items mentioned before in the script editor.'.format(counter + 1))
            success = 0
        else:
            api.log.logger().debug('This model scene has passed the naming convention and double names check.')

        api.log.logger().debug('check for double names has been done.')
        return success

    ############################################################################
    # @brief      Exports the vrscene for the current asset to be used in the
    #             lighting dept
    #
    # @param      self  The object
    #
    # @return     the vrscene file path.
    #
    def exportVRscene(self):
        vrscenefilename = api.vray.createVRayFile()

        cmd.file(new=True, force=True)
        api.vray.addVrscene(vrscenefilename)
        basename, ext = os.path.splitext(vrscenefilename)
        vrscenefilename = api.export.export_ma('{}_vrscene.ma'.format(basename))

        return vrscenefilename

    ############################################################################
    # @brief      Gets the uv set information.
    #
    # @return     The uv set information.
    #
    def getUvSetInfo(self):
        for mesh in cmd.sets('MSH', query=True):
            if len(cmd.polyUVSet(mesh, allUVSets=True, query=True)) > 1:
                return False

        return True

    ############################################################################
    # @brief      Gets the asset.
    #
    # @return     The asset.
    #
    def getAsset(self):
        return self.savedAsset.getAsset()

    ############################################################################
    # @brief      Gets the category.
    #
    # @return     The category.
    #
    def getCategory(self):
        return self.savedAsset.getCategory()

    ############################################################################
    # @brief      Gets the group.
    #
    # @return     The group.
    #
    def getGroup(self):
        return self.savedAsset.getGroup()

    ############################################################################
    # @brief      Gets the name.
    #
    # @return     The name.
    #
    def getName(self):
        return self.savedAsset.getName()

    ############################################################################
    # @brief      Gets the varient.
    #
    # @return     The varient.
    #
    def getVariant(self):
        return self.savedAsset.getVariant()

    ############################################################################
    # @brief      Gets the sg identifier.
    #
    # @return     The sg identifier.
    #
    def getSgId(self):
        return self.savedAsset.getShotgunId()

    ############################################################################
    # @brief      Gets the dept type.
    #
    # @return     The dept type.
    #
    def getDeptType(self):
        return self.savedAsset.deptType

    ############################################################################
    # @brief      Determines if definition.
    #
    # @return     True if definition, False otherwise.
    #
    def isDef(self):
        if self.savedAsset.approvation == 'def':
            return True
        else:
            return False

    ############################################################################
    # @brief      Determines if character.
    #
    # @return     True if character, False otherwise.
    #
    def isCharacter(self):
        if self.getCategory() == 'character':
            return True
        else:
            return False

    ############################################################################
    # @brief      Determines if property.
    #
    # @return     True if property, False otherwise.
    #
    def isProp(self):
        if self.getCategory() == 'prop':
            return True
        else:
            return False

    ############################################################################
    # @brief      Determines if set.
    #
    # @return     True if set, False otherwise.
    #
    def isSet(self):
        if self.getCategory() == 'set':
            return True
        else:
            return False

    ############################################################################
    # @brief      Determines if shader.
    #
    # @return     True if shader, False otherwise.
    #
    def isShader(self):
        if self.getCategory() == 'shader':
            return True
        else:
            return False

    ############################################################################
    # @brief      Determines if lightrig.
    #
    # @return     True if lightrig, False otherwise.
    #
    def isLightrig(self):
        if self.getCategory() == 'lightrig':
            return True
        else:
            return False

    ############################################################################
    # @brief      Determines if fx.
    #
    # @return     True if fx, False otherwise.
    #
    def isFx(self):
        if self.getCategory() == 'fx':
            return True
        else:
            return False
