import importlib
import os
import shutil

import maya.cmds as cmd
import maya.mel as mel

import depts.mc_depts as mcdepts
import depts.grooming.tools.generic.renameCollection as renameCollection
import api.scene
import api.xgen
import api.widgets.rbw_UI as rbwUI

importlib.reload(mcdepts)
importlib.reload(renameCollection)
importlib.reload(api.scene)
importlib.reload(api.xgen)
# importlib.reload(rbwUI)


################################################################################
# @brief      This class describes a grooming.
#
class Grooming(mcdepts.Dept):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, savedAsset):
        super(Grooming, self).__init__(savedAsset)
        self.deptName = "grooming"

    ############################################################################
    # @brief      load function
    #
    # @param      mode    The mode
    # @param      params  The parameters
    #
    # @return     { description_of_the_return_value }
    #
    def load(self, mode='load', params=None):
        mbFolder = os.path.dirname(self.path)
        mbFileName = os.path.basename(self.path)
        mbFile = mbFileName.split('.')[0]
        _usersDir = os.path.join(os.getenv('CONTENT'), '_users', os.getenv('USERNAME')).replace('\\', '/')
        if not os.path.exists(_usersDir):
            os.makedirs(_usersDir)
            api.log.logger().debug('A new directory called \"{}\" was created'.format(os.getenv('USERNAME')))

        # creo il nome della cartella del progetto
        dirName = 'mc_{}_{}_{}_{}'.format(self.getCategory(), self.getGroup(), self.getName(), self.getVariant())
        newMayaProject = os.path.join(_usersDir, dirName).replace('\\', '/')

        # se la cartella esiste, l'utente puo scegliere di cancellare la cartella oppure interrompere il loading
        if os.path.exists(newMayaProject):
            messageContent = "The folder {} already exists in your user directory.\n If you continue the loading process, the old folder will be replaced".format(dirName)
            response = rbwUI.RBWWarning(text=messageContent, title="ATTENTION")
            if response:
                cmd.file(new=True, force=True)
                shutil.rmtree(newMayaProject)
            else:
                warningMessage = 'The loading was interrupted.\n'
                rbwUI.RBWWarning(message=warningMessage, title='ATTENTION')
                return

        os.makedirs(newMayaProject)

        collectionName = None

        files = os.listdir(mbFolder)
        for file in files:
            if mbFile in file:
                newFilePath = os.path.join(newMayaProject, file).replace('\\', '/')
                oldFilePath = os.path.join(mbFolder, file).replace('\\', '/')
                if os.path.isfile(oldFilePath):
                    shutil.copyfile(oldFilePath, newFilePath)
                    if file.endswith('.xgen'):
                        collectionName = file.split('__')[1].split('.xgen')[0]

        if collectionName:
            xgenFolder = os.path.join(mbFolder, 'xgen', 'collections', collectionName).replace('\\', '/')
            newXgenFolder = os.path.join(newMayaProject, 'xgen', 'collections').replace('\\', '/')
            if not os.path.exists(newXgenFolder):
                os.makedirs(newXgenFolder)
            newXgenFolder = os.path.join(newXgenFolder, collectionName).replace('\\', '/')
            shutil.copytree(xgenFolder, newXgenFolder)

        sourceWorkspaceMel = os.path.join(
            os.getenv('LOCALPIPE'),
            'mel',
            'workspace.mel'
        ).replace('\\', '/')

        destWorkspaceMel = os.path.join(
            newMayaProject,
            'workspace.mel'
        ).replace('\\', '/')
        shutil.copyfile(sourceWorkspaceMel, destWorkspaceMel)

        newMaPath = os.path.join(newMayaProject, mbFileName).replace('\\', '/')
        cmd.file(newMaPath, open=True, force=True)
        cmd.workspace(newMayaProject, openWorkspace=True)

    def save(self):
        if self.isDef():
            self.deleteUnusedShapeOrig()

        scenename = api.scene.scenename()
        version = os.path.basename(self.savedAsset.savedAssetFolder)

        if len(api.xgen.listCollection()) > 0:
            hasXgen = True
            renameCollection.renameCollection(version=version)

            # hide all guides in order to prevert crash
            for guide in cmd.ls(type='xgmSplineGuide'):
                cmd.setAttr('{}.visibility'.format(guide), False)
        else:
            hasXgen = False
            api.log.logger().debug('No xgen in file.')

        self.localSave()
        cmd.file(rename=scenename)

        pipeMayaProject = os.getenv('MC_FOLDER').replace('\\', '/')
        currentMayaProject = os.path.dirname(api.scene.scenename()).replace('\\', '/')
        # Remove absolute paths
        try:
            self.checkTexturesPath()
        except:
            rbwUI.RBWWarning(text='Error while checking and replacing absolute paths with relative ones.\nThe save will contine but please contact pipeline@rbw-cgi.it')
        try:
            if api.xgen.currentCollection() != '':
                xgenDir = os.path.join(currentMayaProject, 'xgen', 'collections', api.xgen.currentCollection()).replace('\\', '/')
                newXgenDir = os.path.join(self.savedAsset.savedAssetFolder, 'xgen', 'collections', api.xgen.currentCollection()).replace('\\', '/')
                api.xgen.setProjectPath(self.savedAsset.savedAssetFolder.replace('\\', '/'))
                api.log.logger().debug('Ho settato il progetto di xGen qui: {}'.format(self.savedAsset.savedAssetFolder))

                shutil.copytree(xgenDir, newXgenDir)
            else:
                rbwUI.RBWWarning(message='Can\'t export the xgen files from file.\nIf the asset is free of such materials alright. If not, contact the pipeline department pipeline@rbw-cgi.it\nThe save will contine')
        except AttributeError:
            rbwUI.RBWWarning(text='Can\'t export the xgen files from file.\nIf the asset is free of such materials alright. If not, contact the pipeline department pipeline@rbw-cgi.it\nThe save will contine')

        # create SET_CAC_READER group if it does not exist
        if not cmd.objExists("SET_CAC_READER"):
            cacsel = cmd.ls('CAC*', type='transform')
            cmd.sets(cacsel, name='SET_CAC_READER')

        # fix on CAC_Reader shape name (only if SET_CAC_READER group exists and it's not empty)
        if cmd.objExists('SET_CAC_READER') and cmd.listRelatives('SET_CAC_READER', children=True):
            self.fixCacReaderName()

        self.staticProc()

        try:
            mel.eval('displayHairCurves "current" 1;')
        except:
            pass

        # === Real Save On Disk!! ===
        self.realSave()

        try:
            self.hairSysFinalize()
        except:
            pass

        if self.isDef():
            scenefilename = api.scene.scenename()
            self.exportVRscene()

            cmd.file(scenefilename, open=True, force=True)
            cmd.select(clear=True)

            if cmd.objExists('LOC_meshcut_tag'):
                self.exportMeshcutJson()

        if hasXgen:
            self.exportScalps()
            self.exportShaders()

        shutil.rmtree(currentMayaProject)
        cmd.file(new=True, force=True)
        cmd.workspace(pipeMayaProject, openWorkspace=True)

        # leave this!!
        return True

    ############################################################################
    # @brief      export a json file containing the meshcut information
    #
    def exportMeshcutJson(self):
        scenefilename = api.scene.scenename()
        allAttributeList = cmd.listAttr('LOC_meshcut_tag')
        meshList = []
        for elem in allAttributeList:
            # select the attributes with the tag MESHCUT_
            # before appending it in the meshList remove the tag
            if elem.count('MESHCUT_'):
                meshList.append(elem.replace('MESHCUT_', ''))

        meshcut = {}
        meshcut['meshes'] = []
        for mesh in meshList:
            meshcut['meshes'].append(mesh)

        api.json.json_write(meshcut, scenefilename.replace('.mb', '_meshcut.json'))

    ############################################################################
    #   eventual CAC reader nurbs export
    #
    def hairSysFinalize(self):
        groupCac = "GRP_CAC_READER"
        filename = api.scene.scenename()[:-3] + '_CacReader.ma'
        if cmd.objExists(groupCac):
            cmd.select(cmd.listRelatives(groupCac, c=1), r=True)
            cmd.select(groupCac, r=1)
            cmd.file(filename, force=True, typ="mayaAscii", exportSelected=True, preserveReferences=True, shader=True, expressions=True, constructionHistory=True)

        api.log.logger().debug("nExport CAC_READER nurbs surface here: {}".format(filename))

    ############################################################################
    # @brief      sxport scalps data.
    #
    def exportScalps(self):
        scenename = api.scene.scenename()

        scalps = []
        sceneScalps = api.xgen.getScalps()
        for scalp in sceneScalps:
            scalpName = scalp.split('|')[-1]
            if scalpName not in scalps:
                scalps.append(scalpName)
        scalpsData = {'scalps': scalps}

        api.json.json_write(scalpsData, scenename.replace('.mb', '_scalps.json'))

    ############################################################################
    # @brief      export  scene shaders.
    #
    def exportShaders(self):
        materials = []
        description2material = {}

        if len(api.xgen.listCollection()) > 0:
            for description in cmd.ls(type='xgmDescription'):
                shadeEng = cmd.listConnections(description, type='shadingEngine')[0]
                material = cmd.ls(cmd.listConnections(shadeEng), materials=True)[0]
                descriptionTransform = cmd.listRelatives(description, parent=True, fullPath=True)[0]

                if material not in materials:
                    materials.append(material)

                description2material[descriptionTransform.split('_')[-1]] = material

            scenename = api.scene.scenename()

            jsonPath = scenename.replace('.mb', '_shaders.json')
            api.json.json_write(description2material, jsonPath)

            cmd.select(clear=True)
            cmd.select(materials)
            exportPath = scenename.replace('.mb', '_shaders.mb')
            cmd.file(exportPath, force=True, typ="mayaBinary", exportSelected=True, preserveReferences=True, shader=True, expressions=True, constructionHistory=True)
        else:
            return

    ############################################################################
    # @brief      { function_description }
    #
    def staticProc(self):
        nuc = cmd.ls(type='nucleus')
        if nuc:
            cmd.setAttr(nuc[0] + '.startFrame', 10000)
            hs = cmd.ls(type='hairSystem')
            cmd.setAttr(hs[0] + '.simulationMethod', 1)
            flc = cmd.ls(type='follicle')
            for f in flc:
                cmd.setAttr(f + '.simulationMethod', 0)

    ############################################################################
    # @brief      Fix cac reader shape names.
    #
    # @param      self  The object
    #
    # @return     None
    #
    def fixCacReaderName(self):
        cacReaderList = cmd.listRelatives('SET_CAC_READER', children=True)

        for cacShape in cacReaderList:
            transform = cmd.listRelatives(cacShape, parent=True, fullPath=True)[0]
            transformName = transform.split('|')[-1]

            cmd.rename(cacShape, '{}Shape'.format(transformName))
