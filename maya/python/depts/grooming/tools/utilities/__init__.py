import importlib
import os

import api.log

importlib.reload(api.log)

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'common')


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_cleanupMayaScriptJob(*args):
    import common.cleanupMayaScriptJobs as cleanupMayaScriptJobs
    importlib.reload(cleanupMayaScriptJobs)

    cleanApp = cleanupMayaScriptJobs.cleanupMayaScriptJobs()
    cleanApp.delete_worm_nodes()
    api.log.logger().debug("..eventual cleanup for worms is done...")


cleanupMayaScriptJobTool = {
    "name": "Clean Maya Scripts",
    "statustip": "Launch a Maya Script Job for Clena",
    "launch": run_cleanupMayaScriptJob,
    "icon": os.path.join(iconDir, "clear.png")
}


tools = [
    cleanupMayaScriptJobTool
]


PackageTool = {
    "name": "utilities",
    "tools": tools,
    "icon_size": "32",
    "icon_only": False
}