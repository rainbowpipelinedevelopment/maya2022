import importlib
import os
from PySide2 import QtWidgets

import maya.cmds as cmd
import xgenm as xg

import common.loadAndSaveAssetUI
import api.savedAsset
import api.xgen
import api.widgets.rbw_UI as rbwUI

importlib.reload(common.loadAndSaveAssetUI)
# importlib.reload(api.savedAsset)
importlib.reload(api.xgen)
# importlib.reload(rbwUI)

################################################################################
# @brief      This class describes a load and save grooming ui.
#
class LoadAndSaveGroomingUI(common.loadAndSaveAssetUI.AssetUI):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(LoadAndSaveGroomingUI, self).__init__('Grooming')

    ############################################################################
    # @brief      save function.
    #
    def save(self):
        try:
            savedAssetParam = {
                'asset': self.currentAsset,
                'dept': self.deptName.lower(),
                'deptType': self.currentDeptType,
                'approvation': self.currentApprovation,
                'note': self.saveNotesTextEdit.toPlainText()
            }

            if self.saveExportSelectionSwitch.isChecked():
                savedAssetParam['selection'] = cmd.ls(selection=True)[0]
            else:
                savedAssetParam['selection'] = None

            if self.currentApprovation == 'def':
                if not self.checkToSave():
                    return

            savedAsset = api.savedAsset.SavedAsset(params=savedAssetParam)
            success = savedAsset.save()

            if success:
                rbwUI.RBWConfirm(title='Save {} Complete'.format(self.deptName), text='Asset Save Complete', defCancel=None)
                self.fillSavedAssetsTree()
            else:
                rbwUI.RBWError(title='ATTENTION', text='Some Error During Save')

        except AttributeError:
            rbwUI.RBWError(title='ATTENTION', text='One between deptType or approvation are not properly selected.\nSelect those and try again.')

    ############################################################################
    # @brief      check the length of the full ptex path ${DESC}/../name.ptex
    #
    # @return     succes or not
    #
    def checkPtexLength(self):
        if len(api.xgen.listCollection()) > 0:
            currentCollection = api.xgen.currentCollection()
            currentCollectionInfo = currentCollection.split('_')
            if len(currentCollectionInfo) == 4:
                currentCollection = '{}_vr000'.format(currentCollection)
            currentDescription = api.xgen.currentDescription()

            for expression in api.xgen.listExpressions(api.xgen.currentCollection()):
                attrValue = xg.getAttr('custom_color_{}'.format(expression), api.xgen.currentCollection())
                if attrValue == '':
                    continue
                relShortPath = attrValue.split('"')[1]
                descFolder = os.path.join(
                    os.getenv('MC_FOLDER'),
                    'scenes',
                    self.currentAsset.category,
                    self.currentAsset.group,
                    self.currentAsset.name,
                    self.currentAsset.variant,
                    'grooming'
                ).replace('\\', '/')

                if self.currentApprovation == 'def':
                    descFolder = os.path.join(descFolder, 'def', 'vr000').replace('\\', '/')
                else:
                    descFolder = os.path.join(descFolder, 'wip').replace('\\', '/')

                descFolder = os.path.join(
                    descFolder,
                    'xgen',
                    'collection',
                    currentCollection,
                    currentDescription,
                ).replace('\\', '/')
                relLongPath = relShortPath.replace('${DESC}', descFolder)

                if len(relLongPath) >= 260:
                    rbwUI.RBWError(title='ATTENTION', text='The full path of this texture is too long:\n\t{}'.format(relLongPath))
                    return False

            return True
        else:
            # no xgen in file
            return True

    ############################################################################
    # @brief      check if the grooming asset is ready to be saved.
    #
    # @return     { description_of_the_return_value }
    #
    def checkGrooming(self):
        # FOR EVERY HAIR SYSTEM CHECK REFRACT AND REFLECT
        hairSystems = cmd.ls(type='hairSystem')
        if len(hairSystems) > 0:
            for hairSystem in hairSystems:
                visibleInReflections = cmd.getAttr('{}.visibleInReflections'.format(hairSystem))
                api.log.logger().debug('visibleInReflections: {}, for {}'.format(visibleInReflections, hairSystem))
                visibleInRefractions = cmd.getAttr('{}.visibleInRefractions'.format(hairSystem))
                api.log.logger().debug('visibleInRefractions: {}, for {}'.format(visibleInRefractions, hairSystem))
                if not (visibleInReflections & visibleInRefractions):
                    rbwUI.RBWError(title='ATTENTION', text='One of visibleInReflections or visibleInRefractions or both in {} is disable.\nPlease enable and make a new save.'.format(hairSystem))
                    return False

        # CHECK IF EVERY DESCRIPTION HAS A MATERIAL ASSIGN
        possibleMaterials = ['VRayHairNextMtl', 'VRayBlendMtl', 'VRayMtl']

        descriptions = cmd.ls(type='xgmDescription')
        if len(descriptions) > 0:
            for description in descriptions:
                shadeEng = cmd.listConnections(description, type='shadingEngine')[0]
                material = cmd.ls(cmd.listConnections(shadeEng), materials=True)[0]
                materialType = cmd.nodeType(material)

                if materialType not in possibleMaterials:
                    rbwUI.RBWError(title='ATTENTION', text='The description {} has no approved material assign.\nPlease control and make a new save.'.format(description))
                    return False

        # CHECK IF xgGoom NODE EXISTS
        if cmd.objExists('xgGroom'):
            rbwUI.RBWError(title='ATTENTION', text='The xgGroom is in scene.\nAre you sure you finalize the asset?\nPlease check and make a new save.')
            return False

        # CHECK IF BLENDSHAPES IN SCENE
        blendShapes = cmd.ls(type='blendShape')
        if len(blendShapes) > 0:
            rbwUI.RBWError(title='ATTENTION', text='There are one or more blendShapes in scene.\nPlease check and make a new save.')
            return False

        # CHECK IF THERE ARE DISPLAY LEVELS OUTSIDE THE DEFAULT
        displayLayers = cmd.ls(type='displayLayer')
        if len(displayLayers) > 2:
            rbwUI.RBWError(title='ATTENTION', text='There are more than 1 display layer in scene.\nPLease check and make a new save.')
            return False

        # CHECK FOR A MESHCUT LOCATOR
        if not cmd.objExists('LOC_meshcut_tag'):
            # the user can interrupt the saving process by pressing CANCEL
            # and use the "Create Meshcut Set" tool
            noMeshCutMessage = "no Meshcut Set found! Do you want to continue the saving process?"
            answer = rbwUI.RBWWarning(text=noMeshCutMessage, title='Create Meshcut Set')
            if not answer:
                return False

        return True

    ############################################################################
    # @brief      check function to save.
    #
    # @return     check result
    #
    def checkToSave(self):
        super(LoadAndSaveGroomingUI, self).checkToSave()

        if not self.checkPtexLength():
            return False

        if not self.checkGrooming():
            return False

        return True
