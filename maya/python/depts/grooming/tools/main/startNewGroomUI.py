import os
import shutil
import importlib
from datetime import datetime
from PySide2 import QtWidgets, QtGui, QtCore

import maya.cmds as cmd

import api.database as db
import api.log
import api.asset
import api.savedAsset
import api.widgets.rbw_UI as rbwUI
import config.dictionaries

importlib.reload(db)
importlib.reload(api.log)
# importlib.reload(api.asset)
# importlib.reload(api.savedAsset)
# importlib.reload(rbwUI)
importlib.reload(config.dictionaries)


################################################################################
# @brief      This class describes a start new groom ui.
#
class StartNewGroomUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(StartNewGroomUI, self).__init__()

        if cmd.window('StartNewGroomUI', exists=True):
            cmd.deleteUI('StartNewGroomUI')

        self.mcFolder = os.getenv('MC_FOLDER')
        api.log.logger().debug(self.mcFolder)
        self.id_mv = os.getenv('ID_MV')
        api.log.logger().debug(self.id_mv)

        self.assets = api.system.getAssetsFromDB()

        self.setObjectName('StartNewGroomUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topWidget = rbwUI.RBWFrame(layout='G', radius=5)

        # ------------------------- search widget ------------------------- #
        self.searchGroup = rbwUI.RBWGroupBox(title='Search asset name or a part of this:', layout='H', fontSize=12)

        self.searchLineEdit = rbwUI.RBWLineEdit(title='Type asset name:', bgColor='transparent', stretch=False)
        self.searchLineEdit.returnPressed.connect(self.searchEntry)
        self.searchButton = rbwUI.RBWButton(icon=['search.png'])
        self.searchButton.clicked.connect(self.searchEntry)

        self.searchGroup.addWidget(self.searchLineEdit)
        self.searchGroup.addWidget(self.searchButton)

        self.topWidget.layout.addWidget(self.searchGroup, 0, 0, 1, 2)

        self.assetInfoGroup = rbwUI.RBWGroupBox(title='Asset Info:', layout='V', fontSize=12, spacing=3)

        self.assetCategoryComboBox = rbwUI.RBWComboBox(text='Category:', bgColor='transparent')
        self.assetGroupComboBox = rbwUI.RBWComboBox(text='Group:', bgColor='transparent')
        self.assetNameComboBox = rbwUI.RBWComboBox(text='Name:', bgColor='transparent')
        self.assetVariantComboBox = rbwUI.RBWComboBox(text='Variant:', bgColor='transparent')

        self.assetCategoryComboBox.activated.connect(self.fillAssetGroupComboBox)
        self.assetGroupComboBox.activated.connect(self.fillAssetNameComboBox)
        self.assetNameComboBox.activated.connect(self.fillAssetVariantComboBox)
        self.assetVariantComboBox.activated.connect(self.setCurrentAsset)

        self.assetInfoGroup.addWidget(self.assetCategoryComboBox)
        self.assetInfoGroup.addWidget(self.assetGroupComboBox)
        self.assetInfoGroup.addWidget(self.assetNameComboBox)
        self.assetInfoGroup.addWidget(self.assetVariantComboBox)

        self.topWidget.layout.addWidget(self.assetInfoGroup, 1, 0, 2, 1)

        self.searchResultList = QtWidgets.QListWidget()
        self.searchResultList.itemClicked.connect(self.matchAsset)

        self.topWidget.layout.addWidget(self.searchResultList, 1, 1)

        self.startNewButton = rbwUI.RBWButton(text='Start New')
        self.startNewButton.clicked.connect(self.startNewGroom)
        self.topWidget.layout.addWidget(self.startNewButton, 2, 1)

        self.mainLayout.addWidget(self.topWidget)

        self.fillAssetCategoryComboBox()

        self.setMinimumSize(400, 250)
        self.setTitle('Start New Groom UI')
        self.setFocus()

    ############################################################################
    # @brief      fill the asset category combo box.
    #
    def fillAssetCategoryComboBox(self):
        self.assetCategoryComboBox.clear()
        categories = self.assets.keys()

        self.assetCategoryComboBox.addItems(categories)

        if len(categories) == 1:
            self.assetCategoryComboBox.setCurrentIndex(0)
            self.fillAssetGroupComboBox()

    ############################################################################
    # @brief      fill the group combo box.
    #
    def fillAssetGroupComboBox(self):
        self.currentCategory = self.assetCategoryComboBox.currentText()

        self.assetGroupComboBox.clear()
        groups = self.assets[self.currentCategory].keys()

        self.assetGroupComboBox.addItems(groups)

        if len(groups) == 1:
            self.assetGroupComboBox.setCurrentIndex(0)
            self.fillAssetNameComboBox()

    ############################################################################
    # @brief      fill th name combo box.
    #
    def fillAssetNameComboBox(self):
        self.currentGroup = self.assetGroupComboBox.currentText()

        self.assetNameComboBox.clear()
        names = self.assets[self.currentCategory][self.currentGroup].keys()

        self.assetNameComboBox.addItems(names)

        if len(names) == 1:
            self.assetNameComboBox.setCurrentIndex(0)
            self.fillAssetVariantComboBox()

    ############################################################################
    # @brief      fill the variant combo box.
    #
    def fillAssetVariantComboBox(self):
        self.currentName = self.assetNameComboBox.currentText()

        self.assetVariantComboBox.clear()
        variants = self.assets[self.currentCategory][self.currentGroup][self.currentName].keys()

        self.assetVariantComboBox.addItems(variants)

        if len(variants) == 1:
            self.assetVariantComboBox.setCurrentIndex(0)
            self.setCurrentAsset()

    ############################################################################
    # @brief      search the give asset.
    #
    def searchEntry(self):
        wantedEntryName = self.searchLineEdit.text()
        searchAssetFromNameQuery = "SELECT DISTINCT `category`, `group`, `name` FROM `V_assetList` WHERE `projectId`={} AND `name` LIKE '%{}%'".format(
            self.id_mv,
            wantedEntryName
        )

        results = api.database.selectQuery(searchAssetFromNameQuery)
        if results:
            self.updateResultList(results)

    ############################################################################
    # @brief      update the list result.
    #
    # @param      queryResults  The query results
    #
    def updateResultList(self, queryResults):
        self.searchResultList.clear()
        self.searchResultList.show()
        for infoTupla in queryResults:
            item = QtWidgets.QListWidgetItem('{} - {} - {}'.format(infoTupla[0], infoTupla[1], infoTupla[2]))
            self.searchResultList.addItem(item)

    ############################################################################
    # @brief      update the combo to match the selected asset.
    #
    def matchAsset(self):
        wantedCategory, wantedGroup, wantedName = self.searchResultList.currentItem().text().split(' - ')

        self.fillAssetCategoryComboBox()
        categoryIndex = self.assetCategoryComboBox.findText(wantedCategory, QtCore.Qt.MatchFixedString)
        self.assetCategoryComboBox.setCurrentIndex(categoryIndex)

        self.fillAssetGroupComboBox()
        groupIndex = self.assetGroupComboBox.findText(wantedGroup, QtCore.Qt.MatchFixedString)
        self.assetGroupComboBox.setCurrentIndex(groupIndex)

        self.fillAssetNameComboBox()
        nameIndex = self.assetNameComboBox.findText(wantedName, QtCore.Qt.MatchFixedString)
        self.assetNameComboBox.setCurrentIndex(nameIndex)

        self.fillAssetVariantComboBox()

    ############################################################################
    # @brief      Sets the current asset.
    #
    def setCurrentAsset(self):
        self.currentVariant = self.assetVariantComboBox.currentText()

        self.currentAsset = api.asset.Asset({
            'category': self.currentCategory,
            'group': self.currentGroup,
            'name': self.currentName,
            'variant': self.currentVariant
        })

    ############################################################################
    # @brief      import last rigging def save in scene.
    #
    def importLastRig(self):
        rigSavedAssetParams = {
            'asset': self.currentAsset,
            'dept': 'rigging',
            'deptType': 'hig',
            'approvation': 'def'
        }
        rigSavedAsset = api.savedAsset.SavedAsset(params=rigSavedAssetParams)

        latestRigInfos = rigSavedAsset.getLatest()
        if latestRigInfos:
            latestRigId = latestRigInfos[0]
            latestRigSavedAsset = api.savedAsset.SavedAsset(params={'db_id': latestRigId, 'approvation': 'def'})

            latestRigSavedAsset.load(mode='merge')
        else:
            rbwUI.RBWWarning(title='No Def Rig', text='No rigging save def was found in content.\nLet\'s starts with an empty file.')

    def startNewGroom(self):
        _usersDir = os.path.join(os.getenv('CONTENT'), '_users', os.getenv('USERNAME')).replace('\\', '/')
        if not os.path.exists(_usersDir):
            os.makedirs(_usersDir)

        newProjectName = 'mc_{}_{}_{}_{}'.format(
            self.currentCategory,
            self.currentGroup,
            self.currentName,
            self.currentVariant
        )

        newProjectDir = os.path.join(_usersDir, newProjectName).replace('\\', '/')
        if not os.path.exists(newProjectDir):
            os.makedirs(newProjectDir)

        newXgenFolder = os.path.join(newProjectDir, 'xgen').replace('\\', '/')
        if not os.path.exists(newXgenFolder):
            os.makedirs(newXgenFolder)

        currentMayaProject = os.getenv('MC_FOLDER').replace('\\', '/')
        currentWorkspaceFile = os.path.join(currentMayaProject, 'workspace.mel').replace('\\', '/')
        newWorkspaceFile = os.path.join(newProjectDir, 'workspace.mel').replace('\\', '/')
        if not os.path.isfile(newWorkspaceFile):
            shutil.copyfile(currentWorkspaceFile, newWorkspaceFile)

        self.importLastRig()

        newSceneName = 'grooming_{}_{}_{}_hrs_vr000'.format(
            self.currentGroup,
            self.currentName,
            self.currentVariant
        )

        newScenePath = os.path.join(newProjectDir, newSceneName).replace('\\', '/')

        try:
            cmd.file(rename='{}.ma'.format(newScenePath))
            cmd.file(save=True, type='mayaAscii')
        except:
            rbwUI.RBWError(title='Save Problems', text='Problem in save .ma\nPlease contact the pipeline department')
            return

        try:
            cmd.file(rename='{}.mb'.format(newScenePath))
            cmd.file(save=True, type='mayaBinary')
        except:
            rbwUI.RBWError(title='Save Problems', text='Problem in save .mb\nPlease contact the pipeline department')
            return

        cmd.workspace(newProjectDir, openWorkspace=True)
        rbwUI.RBWConfirm(title='DONE', text='Start New Groom Complete', defCancel=None)
