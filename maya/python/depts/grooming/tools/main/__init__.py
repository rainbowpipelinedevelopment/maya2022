import importlib
import os

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'arakno')


def run_loadAndSave(*args):
    import depts.grooming.tools.main.loadAndSaveGroomingUI as loadAndSaveAssetUI
    importlib.reload(loadAndSaveAssetUI)

    win = loadAndSaveAssetUI.LoadAndSaveGroomingUI()
    win.show()


def run_loadAndSaveModeling(*args):
    import depts.modeling.tools.main.loadAndSaveModelingUI as loadAndSaveMod
    importlib.reload(loadAndSaveMod)

    win = loadAndSaveMod.LoadAndSaveModelingUI()
    win.show()


def run_loadAndSaveRigging(*args):
    import common.loadAndSaveAssetUI as loadAndSaveAssetUI
    importlib.reload(loadAndSaveAssetUI)

    win = loadAndSaveAssetUI.AssetUI('Rigging')
    win.show()


def run_startNewGroomUI(*args):
    import depts.grooming.tools.main.startNewGroomUI as startNewGroomUI
    importlib.reload(startNewGroomUI)
    win = startNewGroomUI.StartNewGroomUI()
    win.show()


loadAndSaveTool = {
    "name": "L/S Grooming",
    "launch": run_loadAndSave,
    "icon": os.path.join(iconDir, "loadAndSaveGrooming.png"),
    "statustip": "Load & Save Grooming"
}

loadAndSaveModTool = {
    "name": "L/S Modeling",
    "launch": run_loadAndSaveModeling,
    "icon": os.path.join(iconDir, "loadAndSaveModeling.png"),
    "statustip": "Load & Save Modeling"
}

loadAndSaveRigTool = {
    "name": "L/S Rigging",
    "launch": run_loadAndSaveRigging,
    "icon": os.path.join(iconDir, "loadAndSaveRigging.png"),
    "statustip": "Load & Save Rigging"
}

startNewGroomUI = {
    "name": "Start New Groom",
    "launch": run_startNewGroomUI,
    "icon": os.path.join(iconDir, "../grooming", ("start_new_groom.png")),
    "statustip": "Start New Groom UI",
}

tools = [
    loadAndSaveTool,
    loadAndSaveModTool,
    loadAndSaveRigTool,
    startNewGroomUI
]

PackageTool = {
    "name": "main",
    "tools": tools
}