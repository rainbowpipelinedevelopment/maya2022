import importlib
from functools import partial

import maya.cmds as cmd

import depts.grooming.tools.hairdress.extractCurvesFromOpenTubeMesh as extCurve

importlib.reload(extCurve)


def extractCurves(rand_num, curves, *args):
    rnd_number = cmd.floatSliderGrp(rand_num, q=1, v=1)
    num_curve = cmd.intSliderGrp(curves, q=1, v=1)
    extCurve.extractCurvesFromOpenTubeMesh(rnd_number, num_curve)


def extractCurvesFromList(rand_num, curves, *args):
    rnd_number = cmd.floatSliderGrp(rand_num, q=1, v=1)
    num_curve = cmd.intSliderGrp(curves, q=1, v=1)
    extCurve.extractCurveList(rnd_number, num_curve)


def generate_hair_system(*args):
    extCurve.makeSelectedCurvesDynamic()


def generate_read_write(*args):
    extCurve.makeConnectedCacReaderAndWriter()


class quickHairToolsUI():
    def __init__(self):
        pass

    winName = "HairRigUI"
    winWidth = 500

    def open(self):
        if cmd.window(self.winName, exists=1):
            cmd.deleteUI(self.winName)
        if cmd.window(self.winName, exists=1):
            cmd.windowPref(self.winName, remove=1)

        cmd.window(self.winName, title="Hair Tools", widthHeight=[self.winWidth, self.winWidth], bgc=[0.1, 0.1, 0.1], s=1)
        cmd.rowColumnLayout(numberOfColumns=1, columnWidth=[(1, self.winWidth)], cal=(1, "center"), bgc=[0.1, 0.1, 0.1], columnAttach=[5, "both", 5], rowSpacing=[5, 5])
        cmd.text(label="Select a tube mesh \n to generate curves", bgc=[0.1, 0.1, 0.1], w=self.winWidth, fn="obliqueLabelFont")
        cmd.rowColumnLayout(numberOfColumns=1, columnWidth=[(1, self.winWidth)], bgc=[0.1, 0.1, 0.1], columnAttach=[5, "both", 5], rowSpacing=[5, 5])
        int_field = cmd.intSliderGrp(field=True, label='Curve Number', minValue=1, maxValue=50, fieldMinValue=1, fieldMaxValue=300, value=1)
        int_slider = cmd.floatSliderGrp(field=True, label='Random Number', minValue=1, maxValue=100, fieldMinValue=1, fieldMaxValue=500, value=1)
        cmd.button(label="Generate curve", command=partial(extractCurves, int_slider, int_field), ann="Create curve from tube mesh", h=50, w=self.winWidth, bgc=[0.2, 0.2, 0.2])
        cmd.button(label="Generate curve from list", command=partial(extractCurvesFromList, int_slider, int_field), ann="Create curve from multiple tube meshes", h=50, w=self.winWidth, bgc=[0.2, 0.2, 0.2])
        cmd.text("Generate Hair System \n from the selected curve", fn="obliqueLabelFont")
        cmd.button(label="Generate HairSystem", command=generate_hair_system, ann="Set curve origin", h=50, w=self.winWidth, bgc=[0.2, 0.2, 0.2])
        cmd.text(label="Generate cache Reader and writer \n geometry from all Hair sytem", fn="obliqueLabelFont")
        cmd.button(label="Generate Cache Reader and Writer", command=generate_read_write, ann="Create cache reader and writer from hair", w=self.winWidth, h=50, bgc=[0.2, 0.2, 0.2])
        cmd.setParent("..")
        cmd.showWindow()
