import importlib
import random
from math import sqrt

import maya.cmds as cmd
import maya.mel as mel

import api.log

importlib.reload(api.log)


def extractCurvesFromOpenTubeMesh(randNum, howManyCurves=30):
    randNum = randNum * 1.0
    # print randNum
    sel_2 = cmd.ls(sl=1, l=1)
    autoEdge = checkIfRingOrLoop()
    curves = []
    excurves = []
    sel = cmd.ls(sl=1, fl=1)
    closed = checkIfClosed(sel_2)
    if (closed == 0):
        return mel.eval('warning "The selected mesh is closed!"')
    cmd.select(sel)
    mel.eval('pickWalk -d up -type edgering;')
    ring = cmd.ls(sl=1, fl=1)
    count = len(ring)
    # print count
    cmd.select(sel)
    mel.eval('pickWalk -d left -type edgeloop;')

    for i in range(0, count, 1):
        mel.eval('pickWalk -d left -type edgeloop;')
        mel.eval('polyToCurve -form 2 -degree 3;')
        c = cmd.ls(sl=1, l=1)

        curves.append(c[0])
    cmd.select(curves)
    cmd.nurbsToPolygonsPref(f=2, chr=0.1, cht=0.2, d=0.1, es=0, ft=0.01, mel=0.001, pc=200, m=0, mt=0.1, mrt=0, pt=1, uch=0, ucr=0, ut=3, vn=1, vt=3, un=1)
    myloft = cmd.loft(curves, ch=1, u=1, c=1, ar=1, d=3, ss=1, rn=0, po=0, rsn=1)
    spans = cmd.getAttr('{}.spansUV'.format(myloft[0]))

    for i in range(0, spans[0][1], 1):
        cmd.duplicateCurve('{}.v[{}]'.format(myloft[0], ), ch=1, rn=0, local=0)
        c = cmd.ls(sl=1, l=1)
        excurves.append(c[0])
    cmd.delete(myloft)
    cmd.delete(curves)

    cmd.select(excurves)
    extracted = fillCurvesProfileByCurves(randNum, c=howManyCurves)
    cmd.delete(excurves)
    cmd.sets(extracted, name='SET_temp_clump1')


def extractCurves():
    autoEdge = checkIfRingOrLoop()
    curves = []
    excurves = []
    sel = cmd.ls(sl=1, fl=1)
    mel.eval('pickWalk -d up -type edgering;')
    ring = cmd.ls(sl=1, fl=1)
    count = len(ring)
    cmd.select(sel)
    mel.eval('pickWalk -d left -type edgeloop;')
    for i in range(0, count, 1):
        mel.eval('pickWalk -d left -type edgeloop;')
        mel.eval('polyToCurve -form 2 -degree 3;')
        c = cmd.ls(sl=1, l=1)
        curves.append(c[0])
    cmd.sets(curves, name='SET_temp_clump1')
    return curves


def checkIfRingOrLoop():
    sel = cmd.ls(sl=1, l=1)
    for i in range(0, 150, 1):
        cmd.select('{}.e[{}]'.format(sel[0], str(i)))
        mel.eval('pickWalk -d up -type edgeloop;')
        mel.eval('polyToCurve -form 2 -degree 3;')
        excurve = cmd.ls(sl=1, l=1)
        form = cmd.getAttr('{}.form'.format(excurve[0]))
        cmd.delete(excurve)
        if (form == 0):
            cmd.select('{}.e[{}]'.format(sel[0], i))
            return excurve


def fillCurvesProfileByCurves(randNum, c=100):
    randNum = randNum + 0.0001
    sel = cmd.ls(sl=1)
    pos = uniformItemsAlongCurve()
    extracted = []

    for i in range(0, c, 1):

        arrayran = []
        curvcoords = []

        for i in range(0, len(pos), len(sel)):
            if (i == 0):
                arrayran = convexCombinationCoefficientGenerator(len(sel), num_zeroes=int(random.uniform(len(sel) / randNum, len(sel))))
            curvcoords.append(randomBarycentricCoordinates(pos[i:i + len(sel)], len(sel), arrayran))

        cc = cmd.curve(d=3, p=curvcoords)
        extracted.append(cc)
    cmd.select(sel)
    return extracted


def randomBarycentricCoordinates(pos, num, arrayran):
    coords = [0, 0, 0]

    for s in range(num):
        coords[0] += arrayran[s] * pos[s][0]
        coords[1] += arrayran[s] * pos[s][1]
        coords[2] += arrayran[s] * pos[s][2]

    return coords


def randomArrayToOne(num, randNum):
    arrayran = []
    arraysum = 0
    for i in range(0, num - 1, 1):
        tmp = random.uniform(0, 1) / (num * randNum)  # *random.uniform(0.5,1))
        arrayran.append(tmp)
        arraysum += tmp

    arrayran.append(1 - arraysum)

    s = 0
    for i in range(0, num, 1):
        s += arrayran[i]

    random.shuffle(arrayran)

    return arrayran


def findOrderedNearest():
    sel = cmd.ls(sl=1, fl=1, l=1)
    found = sel[0]
    ordered = []
    while (len(sel) != len(ordered)):
        first = found
        dist = []
        for i in range(0, len(sel), 1):
            if ((sel[i].split('|')[-1]) not in ordered):
                p1 = cmd.xform(first, q=1, t=1, ws=1)
                p2 = cmd.xform(sel[i], q=1, t=1, ws=1)
                d = '{}___{}'.format(
                    sqrt((p2[0] - p1[0]) ** 2 + (p2[1] - p1[1]) ** 2 + (p2[2] - p1[2]) ** 2),
                    sel[i].split('|')[-1]
                )
                dist.append(d)

        dist = sorted(dist, reverse=False)
        ordered.append(dist[0].split('___')[-1].split('|')[-1])
        found = dist[0].split('___')[-1].split('|')[-1]


def uniformItemsAlongCurve():
    posarray = []
    sel = cmd.ls(sl=1, l=1)
    tempSelSortByV = []
    for i in range(0, len(sel), 1):
        sp = cmd.getAttr('{}.spans'.format(sel[i]))
        tempStr = str(sp) + sel[i]
        tempSelSortByV.append(tempStr)

    minPoints = sorted(tempSelSortByV)[0].split('|')[-1]
    p = cmd.getAttr('{}.spans'.format(minPoints))
    cstep = 1.0 / p

    for i in range(0, len(sel), 1):
        cpar = 0
        for x in range(0, p, 1):
            for s in sel:
                if (cpar < 1.0):
                    pos = cmd.pointOnCurve(sel[i], pr=cpar, top=1)
                    cpar += cstep
                    posarray.append(pos)

    num = len(sel)
    pairarray = []
    step = len(posarray) / num
    for i in range(0, len(posarray) / num, 1):
        for x in range(i, len(posarray), step):
            pairarray.append(posarray[x])

    return pairarray


def makeSelectedCurvesDynamic():

    check = checkSetsAndMultipleNames()
    if (check == 0):
        return (mel.eval('warning "SOMETHING WRONG. Check script editor "'))
    else:
        pass

    sel = cmd.ls(sl=1, l=1)
    mel.eval('makeCurvesDynamic 2 { "0", "0", "0", "1", "0"};')
    hs = cmd.ls(sl=1, l=1)
    hsp = cmd.listRelatives(hs[0], p=1, f=1)
    # print hsp
    nuc = cmd.listConnections(hs[0], type='nucleus')[0]
    # print nuc
    flc = cmd.listConnections(type='follicle')
    flcShapes = cmd.listRelatives(flc, shapes=1, f=1)
    flcgroup = cmd.listRelatives(flc, p=1, f=1)[0]
    stc = []
    opc = []
    for f in flcShapes:
        sc = cmd.listConnections(f, plugs=1)
        stc.append(sc[0].split('.')[0])
        opc.append(sc[4].split('.')[0])
        cmd.setAttr('{}.restPose'.format(f), 1)
        cmd.setAttr('{}.pointLock'.format(f), 1)
        cmd.setAttr('{}.degree'.format(f), 3)

    stcGrp = (cmd.listRelatives(stc[0], p=1, f=1)[0]).split('|')[1]
    opcGrp = (cmd.listRelatives(opc[0], p=1, f=1)[0]).split('|')[1]

    for s in range(0, len(stc) / 2, 1):
        sp = cmd.listRelatives(stc[s], p=1, f=1)
        flc = cmd.listRelatives(sp, p=1, f=1)
        cmd.rename(sp, 'STC_{}'.format(PAD(s, 5)))
        cmd.rename(flc, 'FLC_{}'.format(PAD(s, 5)))

    for o in range(0, len(opc) / 2, 1):
        op = cmd.listRelatives(opc[o], p=1, f=1)
        cmd.rename(op, 'OPC_{}'.format(PAD(o, 5)))

    checkGrpHsDynName = cmd.ls('GRP_HAIR_dynamic_*')
    howHs = len(checkGrpHsDynName)
    ofGrpHsName = 'GRP_HAIR_dynamic_00{}'.format(howHs + 1)

    checkFlcDynName = cmd.ls('GRP_FLC_*')
    howFlc = len(checkFlcDynName)
    ofFlcName = 'GRP_FLC_00{}'.format(howFlc + 1)
    # print ofFlcName

    checkOpcDynName = cmd.ls('GRP_OPC_*')
    howOpc = len(checkOpcDynName)
    ofOpcName = 'GRP_OPC_00{}'.format(howOpc + 1)
    # print ofOpcName

    checkNucDynName = cmd.ls('NCL_dynamic_*')
    howNuc = len(checkNucDynName)
    ofNucName = 'NCL_dynamic_00 {}'.format(howNuc + 1)
    # print ofNucName

    checkHsDynName = cmd.ls('HS_dynamic_*')
    howHs = len(checkHsDynName)
    ofHsName = 'HS_dynamic_00{}'.format(howHs)
    # print ofHsName

    grp_root = cmd.group(em=1, name=ofGrpHsName)

    cmd.select(stcGrp)
    cmd.rename(stcGrp, ofFlcName)
    stcGrp = cmd.ls(sl=1)
    cmd.select(opcGrp)
    cmd.rename(opcGrp, ofOpcName)
    opcGrp = cmd.ls(sl=1)
    cmd.select(nuc)
    cmd.rename(nuc, ofNucName)
    nuc = cmd.ls(sl=1)
    cmd.select(hsp)
    cmd.rename(hsp[0], ofHsName)
    hsp = cmd.ls(sl=1)

    cmd.parent(stcGrp, grp_root)
    cmd.parent(opcGrp, grp_root)
    cmd.parent(nuc, grp_root)
    cmd.parent(hsp, grp_root)

    cmd.select(cmd.listRelatives(opcGrp, ad=1, f=1))
    cmd.select(cl=1)
    mel.eval('displayHairCurves "currentAndStart" 1')


def makeConnectedCacReaderAndWriter():
    sel1 = []
    sel2 = []
    myLoft = []
    tempReaders = []
    tempWriters = []
    follicles = cmd.ls(type="follicle", l=1)
    for flc in follicles:
        cmd.setAttr("{}.degree".format(flc), 3)
        cmd.setAttr("{}.simulationMethod".format(flc), 0)

    hairsystem = cmd.ls(type="hairSystem", l=1)

    for hsys in hairsystem:
        cmd.setAttr("{}.simulationMethod".format(hsys), 1)
        cmd.setAttr("{}.active".format(hsys), 0)

    nucleis = cmd.ls(type="nucleus", l=1)

    for ncl in nucleis:
        cmd.setAttr("{}.enable".format(ncl), 0)

    hairSets = cmd.ls('*SET_temp_clump*')
    api.log.logger().debug(hairSets)

    for s in hairSets:
        cmd.select(s)
        sel1 = cmd.ls(selection=1)
        cmd.pickWalk(d='up')
        cmd.pickWalk(d='up')
        cmd.pickWalk(d='up')
        mainGroup = cmd.ls(sl=1, l=1)
        cmd.select(s)
        sel1 = cmd.ls(selection=1)
        cmd.nurbsToPolygonsPref(f=2, chr=0.1, cht=0.2, d=0.1, es=0, ft=0.01, mel=0.001, pc=200, m=0, mt=0.1, mrt=0, pt=1, uch=0, ucr=0, ut=3, vn=1, vt=3, un=1)
        mel.eval('loft -ch 1 -u 1 -c 1 -ar 1 -d 3 -ss 1 -rn 0 -po 1 -rsn true;')
        myLoft1 = cmd.ls(sl=1, l=1)
        cmd.delete(myLoft1[0], ch=1)
        cmd.select(myLoft1[0])
        sel2 = extractCurves()

        for i in range(0, len(sel1), 1):
            for x in range(0, len(sel2), 1):
                p1 = cmd.xform('{}.cv[0]'.format(sel1[i]), q=1, t=1, ws=1)
                p2 = cmd.xform('{}.cv[0]'.format(sel2[x]), q=1, t=1, ws=1)
                dist = sqrt((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2 + (p1[2] - p2[2]) ** 2)
                if (dist < 0.001):
                    sel2s = cmd.listRelatives(sel2[x], shapes=1, ni=1, f=1)
                    petc = cmd.listConnections(sel2s, type='polyEdgeToCurve')
                    cmd.connectAttr('{}.outputcurve'.format(petc[0]), '{}.create'.format(sel1[i]), f=1)

        cmd.delete(sel2)

        cmd.select(s)
        sel1 = cmd.ls(selection=1)
        mel.eval('convertHairSelection "current"')
        mel.eval('displayHairCurves "currentAndStart" 1')
        cmd.nurbsToPolygonsPref(f=2, chr=0.1, cht=0.2, d=0.1, es=0, ft=0.01, mel=0.001, pc=200, m=0, mt=0.1, mrt=0, pt=1, uch=0, ucr=0, ut=3, vn=1, vt=3, un=1)
        mel.eval('loft -ch 1 -u 1 -c 1 -ar 1 -d 3 -ss 1 -rn 0 -po 1 -rsn true;')
        myLoft2 = cmd.ls(sl=1, l=1)
        cmd.select(myLoft1[0])
        cmd.rename(myLoft1[0], '___tempReader_{}'.format(PAD(s, 5)))
        tempReader = cmd.ls(sl=1, l=1)
        tempReaders.append(tempReader[0])
        cmd.select(myLoft2[0])
        cmd.rename(myLoft2[0], '___tempWriter_{}'.format(PAD(s, 5)))
        tempWriter = cmd.ls(sl=1, l=1)
        tempWriters.append(tempWriter[0])

        # invece di cancellare i set temp dei clump li teniamo tutti in un altro set
        newHairSet = "temp_hair_sets"
        if not cmd.objExists(newHairSet):
            cmd.sets(name=newHairSet, empty=1)
        cmd.sets(s, add=newHairSet)

    trdup = []
    combineWriters = []

    if (len(tempReaders) > 1):
        cmd.select(tempReaders)
        cmd.duplicate(tempReaders, name='tempReaders_DUP')
        oldReaders = cmd.ls(sl=1, l=1)
        cmd.polyUnite(oldReaders, ch=1, mergeUVSets=1, name='cacheWriter')
        cmd.delete(ch=1)
        trdup = cmd.ls(sl=1, l=1)
        for r in range(0, len(tempReaders), 1):
            cmd.select(tempReaders[r])
            cmd.select(trdup, tgl=1)
            mel.eval('doWrapArgList "7" { "1","1","0.0", "2", "1", "0", "0", "1" }')

    if (len(tempReaders) > 1):
        cmd.rename(trdup, 'cacheReader')
    else:
        cmd.rename(tempReaders, 'cacheReader')

    if (len(tempWriters) > 1):
        cmd.select(tempWriters)
        cmd.polyUnite(ch=1, mergeUVSets=1, name='cacheWriter')
        combineWriters = cmd.ls(sl=1, l=1)
    else:
        cmd.rename(tempWriters, 'cacheWriter')

    cmd.select(cl=1)
    grp = cmd.group(em=1, name='GRP_hairCache')

    if len(tempReaders) > 1:
        grpold = cmd.group(em=1, name='GRP_hairCache_readers_history')
        cmd.parent(tempReaders, grpold)
        cmd.setAttr('{}.intermediateObject'.format(grpold), 1)
        cmd.parent(trdup, grp)
        cmd.parent('cacheReader', grp)
        cmd.parent(grpold, mainGroup)

    else:
        cmd.parent('cacheReader', grp)
        cmd.setAttr('{}.v'.format(grp), 0)

    if (len(tempWriters) > 1):

        trdups = cmd.listRelatives('cacheReader', shapes=1, ni=1, f=1)
        wraps = cmd.listConnections(trdups, type='wrap')
        wrapsMeshes = cmd.listConnections(wraps, type='mesh')
        for w in wrapsMeshes:
            if (w.find('Base') != -1):
                cmd.parent(w, grpold)

    if (len(tempWriters) > 1):
        cmd.parent(tempWriters, grpold)

    cmd.parent(grp, mainGroup)

    if (len(tempWriters) == 1):
        cmd.parent('cacheWriter', grp)

    nucs = cmd.ls(type='nucleus')
    hs = cmd.ls(type='hairSystem')

    for h in hs:
        try:
            cmd.select(h)
            mel.eval('assignNSolver {}'.format(nucs[0]))
        except:
            pass

    cmd.delete(nucs[1:])

    cmd.select('cacheReader')
    cmd.select('cacheWriter', tgl=1)
    cmd.sets(name='SET_hairCache')

    hs = cmd.ls(type='hairSystem', l=1)
    for h in hs:
        cmd.setAttr('{}.hairWidth'.format(h), 0.005)
        cmd.setAttr('{}.hairColor'.format(h), 0, 1, 0, type='double3')

    cmd.select(cl=1)

    cmd.evalDeferred("import depts.grooming.tools.hairdress.extractCurvesFromOpenTubeMesh as extCurve\nextCurve.fixCacheReader()")

    grps = cmd.ls('*GRP_HAIR_dynamic*')
    mgrp = cmd.group(name='GRP_hair', em=1)
    cmd.parent(grps, mgrp)

    attributeCreationAndConnectionForHair()

    cmd.select('cacheWriter')
    cmd.select('cacheReader', tgl=1)
    conformMeshToMesh()


def reverseCurveByDistance():
    # SELECT SOME CURVES AND A REFERENCE LOCATOR
    # SCRIPT WILL READ THE LOCATOR DISTANCE AND REVERSE A CURVE
    sel = cmd.ls(sl=1, l=1)
    for s in sel:
        length = cmd.arclen(s.split('.')[0].split('|')[-1])
        pos1 = cmd.xform(s.split('|')[-1], q=1, t=1, ws=1)
        pos2 = cmd.xform('{}.cv[1]'.format(s.split('.')[0].split('|')[-1]), q=1, t=1, ws=1)
        len2 = sqrt((pos1[0] - pos2[0]) ** 2 + (pos1[1] - pos2[1]) ** 2 + (pos1[2] - pos2[2]) ** 2)
        if (len2 > length / 10):
            cmd.reverseCurve(s.split('.')[0].split('|')[-1], ch=0, rpo=1)
        mel.eval('toggleSelMode;')


def convexCombinationCoefficientGenerator(quantity, num_zeroes=0):
    if num_zeroes >= quantity:
        raise Exception("Bad assignment to Zeroes Quantity (must be less than Quantity)")

    import random

    # random.sample( population, size )
    list = random.sample(xrange(1, 10001), quantity - num_zeroes)
    random.shuffle(list)

    for i in xrange(num_zeroes):
        list.append(0.0)

    list_sum = sum(list) * 1.0
    list = [x / list_sum for x in list]
    random.shuffle(list)
    return list


def PAD(i, padding):
    counter = str(i)
    while len(counter) < padding:
        counter = '0' + counter

    return counter


def extractCurveList(randNum, num_curves=30):
    sel = cmd.ls(sl=1, l=1)
    for x in sel:
        cmd.select(x, r=1)
        extractCurvesFromOpenTubeMesh(randNum, num_curves)


def checkIfClosed(sel):
    c1 = cmd.polyEvaluate(sel, f=1)
    pc = cmd.polyCloseBorder(sel, ch=1)
    c2 = cmd.polyEvaluate(sel, f=1)
    if (c2 - c1 == 2):
        cmd.delete(pc)
        api.log.logger().debug("OPEN")
        return 1
    else:
        cmd.delete(pc)
        api.log.logger().debug("CLOSED")
        return 0


def fixCacheReader():
    reader = 'cacheReader'
    cmd.select('___tempReader*')
    tempReaders = cmd.ls(sl=1, dag=1, shapes=1, ni=1, l=1)
    wrap = cmd.listConnections(reader, type='wrap')
    wrap = list(set(wrap))
    cmd.delete(wrap)
    sep = cmd.polySeparate(reader, ch=1)
    cmd.setAttr('cacheReaderShape.intermediateObject', 0)
    sepreader = cmd.listRelatives(reader, c=1, f=1)
    for i in range(0, len(tempReaders), 1):
        cmd.connectAttr('{}.output[{}]'.format(sep[-1], i), '{}.inMesh'.format(tempReaders[i]), f=1)
    cmd.parent('cacheReaderShape', 'cacheReader', r=1, s=1)
    cmd.delete(sepreader)
    notInteresting = cmd.ls(type=['polySeparate', 'polyUnite', 'nurbsTessellate', 'loft'])
    for ni in notInteresting:
        cmd.setAttr('{}.ihi'.format(ni), 0)


def attributeCreationAndConnectionForHair():
    hs = []
    hstemp = cmd.ls(type='hairSystem', l=1)
    for h in hstemp:
        id = cmd.getAttr('{}.simulationMethod'.format(h))
        if (id != 1):
            hs.append(h)

    for h in hs:
        # ATTRIBUTES CREATION
        ctrl = mel.eval(
            'curve -d 1 -p 0.3 0 0 -p 0.3 2.4 0 -p 1.220755 2.397749 0 -p 1.504471 2.364929 0 -p 1.746226 2.263422 0 -p 1.943783 2.083188 0 -p 2.073859 1.872508 0 -p 2.173637 1.539895 0 -p 2.197065 1.326483 0 -p 1.896905 1.331556 0 -p 1.87764 1.504533 0 -p 1.77432 1.794687 0 -p 1.631344 1.965264 0 -p 1.444289 2.066077 0 -p 1.196941 2.097906 0 -p 0.6 2.09948 0 -p 0.6 0.3 0 -p 1.216523 0.30331 0 -p 1.40474 0.328434 0 -p 1.602737 0.4119 0 -p 1.744666 0.560662 0 -p 1.867064 0.868851 0 -p 1.898012 1.123773 0 -p 2.198318 1.123369 0 -p 2.173227 0.877471 0 -p 2.093463 0.592834 0 -p 1.975141 0.369096 0 -p 1.808032 0.187587 0 -p 1.594275 0.0697967 0 -p 1.276429 0.00551256 0 -p 0.3 0 0 -k 0 -k 1 -k 2 -k 3 -k 4 -k 5 -k 6 -k 7 -k 8 -k 9 -k 10 -k 11 -k 12 -k 13 -k 14 -k 15 -k 16 -k 17 -k 18 -k 19 -k 20 -k 21 -k 22 -k 23 -k 24 -k 25 -k 26 -k 27 -k 28 -k 29 -k 30 -n tempclump_CTRL_dynamics;')

        mel.eval('CreateCluster')
        mel.eval('move -rpr 0 0 0 ;')
        cmd.select(ctrl)
        cmd.delete(ch=1)
        cmd.setAttr('{}.overrideEnabled'.format(ctrl), 1)
        cmd.setAttr('{}.overrideColor'.format(ctrl), 13)
        cmd.setAttr('{}.tx'.format(ctrl), k=0, channelBox=0)
        cmd.setAttr('{}.ty'.format(ctrl), k=0, channelBox=0)
        cmd.setAttr('{}.tz'.format(ctrl), k=0, channelBox=0)
        cmd.setAttr('{}.rx'.format(ctrl), k=0, channelBox=0)
        cmd.setAttr('{}.ry'.format(ctrl), k=0, channelBox=0)
        cmd.setAttr('{}.rz'.format(ctrl), k=0, channelBox=0)
        cmd.setAttr('{}.sx'.format(ctrl), k=0, channelBox=0)
        cmd.setAttr('{}.sy'.format(ctrl), k=0, channelBox=0)
        cmd.setAttr('{}.sz'.format(ctrl), k=0, channelBox=0)
        cmd.setAttr('{}.v'.format(ctrl), k=0, channelBox=0)
        ctrls = cmd.listRelatives(ctrl, shapes=1, f=1)
        cmd.setAttr('{}.ihi'.format(ctrls[0]), 0)

        atc = cmd.ls('tempclump_CTRL_dynamics*')
        cmd.parent(h, atc, add=1, s=1)
        cmd.rename(atc, 'CTRL_{}'.format(h.split('|')[-1]))


def checkSetsAndMultipleNames():
    sel = cmd.ls(sl=1, l=1)
    set = 0
    check = 0
    dup = 0

    for s in sel:
        ss = cmd.listConnections(s, type='objectSet')
        try:
            if ('SET_temp_' in ss[0]):
                set = 1
                check = 1
        except:
            set = 0
            check = 0

        if (set == 0):
            api.log.logger().debug('-' * 100)
            api.log.logger().debug('{} has not a right selection set connected'.format(s.split('|')[-1]))
            api.log.logger().debug('-' * 100)

        try:
            cmd.select(s.split('|')[-1])
            d = cmd.ls(sl=1)
        except:
            dup = 1
            api.log.logger().debug('-' * 100)
            api.log.logger().debug('{} has a duplicated ({})'.format(s.split('|')[-1], s))
            api.log.logger().debug('-' * 100)

    cmd.select(sel)

    if (check == 0 or dup == 1):
        return 0
    else:
        return 1


def conformMeshToMesh():
    globalSel = cmd.ls(sl=True)
    deformThisMesh = globalSel[0]
    sourceShapeMesh = globalSel[1]
    nVerts = cmd.polyEvaluate(deformThisMesh, vertex=True)
    nVertsSrc = cmd.polyEvaluate(sourceShapeMesh, vertex=True)

    if nVerts < nVertsSrc:
        api.log.logger().debug("conformMeshToMesh: deforming mesh has less points than source mesh")
    if nVerts > nVertsSrc:
        api.log.logger().debug("conformMeshToMesh: deforming mesh has more points than source mesh")
    nPointsToMove = min(nVerts, nVertsSrc)
    for i in range(0, nPointsToMove, 1):
        pts = cmd.xform("{}.vtx[{}]".format(sourceShapeMesh, i), objectSpace=True, q=True, translation=True)
        cmd.xform("{}.vtx[{}]".format(deformThisMesh, i), objectSpace=True, translation=[pts[0], pts[1], pts[2]])


def min(nVerts, nVertsSrc):
    retv = None
    if nVerts < nVertsSrc:
        retv = nVerts
    else:
        retv = nVertsSrc
    return retv
