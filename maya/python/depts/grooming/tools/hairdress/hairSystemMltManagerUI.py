import importlib
from maya.app.general.mayaMixin import MayaQWidgetDockableMixin
from PySide2 import QtGui, QtCore, QtWidgets
import functools

import maya.cmds as cmd

import api.log
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.log)
# importlib.reload(rbwUI)


################################################################################
# This class describes a hair system mlt manager ui.
#
class hairSystemMltManagerUI(MayaQWidgetDockableMixin, QtWidgets.QMainWindow):

    ############################################################################
    # @brief      Constructs the object
    #
    # @param      self  The object
    #
    def __init__(self):
        super(hairSystemMltManagerUI, self).__init__()
        if cmd.window('hairSystemMltManagerUI', exists=True):
            cmd.deleteUI('hairSystemMltManagerUI')

        self.setObjectName('hairSystemMltManagerUI')
        self.materials = ['VRayHairNextMtl', 'VRayMtl', 'lambert']
        self.initUI()

    def initUI(self):
        self.centralWidget = QtWidgets.QWidget()
        self.topBox = QtWidgets.QVBoxLayout()

        self.centralWidget.setLayout(self.topBox)
        self.setCentralWidget(self.centralWidget)

        #############################################################
        #
        # INPUTS
        self.inputsLayout = QtWidgets.QHBoxLayout()

        # hair systems in scene list
        self.hairSystemInSceneBox = QtWidgets.QWidget()
        self.hairSystemInSceneLayout = QtWidgets.QVBoxLayout()
        self.hairSystemInSceneLabel = QtWidgets.QLabel('Hair Systems:')
        self.hairSystemInSceneTree = QtWidgets.QTreeWidget()
        self.hairSystemInSceneTree.setColumnCount(1)
        hairSystemInSceneHeaders = QtWidgets.QTreeWidgetItem(['Hair System Name'])
        self.hairSystemInSceneTree.setHeaderItem(hairSystemInSceneHeaders)
        self.hairSystemInSceneLayout.addWidget(self.hairSystemInSceneLabel)
        self.hairSystemInSceneLayout.addWidget(self.hairSystemInSceneTree)
        self.hairSystemInSceneBox.setLayout(self.hairSystemInSceneLayout)
        self.inputsLayout.addWidget(self.hairSystemInSceneBox)

        # material filter
        self.materialFilterGroup = QtWidgets.QGroupBox('Material Type')
        self.materialFilterLayout = QtWidgets.QVBoxLayout()
        self.materialFilterGroup.setLayout(self.materialFilterLayout)
        self.getRadioButtons(self.materials, self.materialFilterLayout)
        self.inputsLayout.addWidget(self.materialFilterGroup)

        # existing materials list
        self.materialInSceneBox = QtWidgets.QWidget()
        self.materialInSceneLayout = QtWidgets.QVBoxLayout()
        self.materialInSceneLabel = QtWidgets.QLabel('Exists Material:')
        self.materialInSceneTree = QtWidgets.QTreeWidget()
        self.materialInSceneTree.setColumnCount(1)
        materialInSceneHeaders = QtWidgets.QTreeWidgetItem(['Exists Material Name'])
        self.materialInSceneTree.setHeaderItem(materialInSceneHeaders)
        self.materialInSceneLayout.addWidget(self.materialInSceneLabel)
        self.materialInSceneLayout.addWidget(self.materialInSceneTree)
        self.materialInSceneBox.setLayout(self.materialInSceneLayout)
        self.inputsLayout.addWidget(self.materialInSceneBox)

        ########################################################################
        #
        # RUN ACTION
        self.runLayout = QtWidgets.QHBoxLayout()

        # add checkBox for create new material and go button
        self.createMaterialCheckBox = QtWidgets.QCheckBox('Create New Material')
        self.runButton = QtWidgets.QPushButton('Run')
        self.runButton.clicked.connect(self.runAction)
        self.runLayout.addWidget(self.createMaterialCheckBox)
        self.runLayout.addWidget(QtWidgets.QLabel(''))
        self.runLayout.addWidget(self.runButton)

        # add to topbox
        self.topBox.addLayout(self.inputsLayout)
        self.topBox.addLayout(self.runLayout)

        # riempo l'albero degli hair system
        self.fillHairSystemTree()

        # attivo di default il primo bottone
        firstRadio = self.materialFilterGroup.findChild(QtWidgets.QRadioButton, '{}RadioButton'.format(self.materials[0]))
        firstRadio.setChecked(True)
        firstRadio.click()

        self.setWindowTitle('Hair System Mlt Manager UI')
        self.setFocus()

    def getRadioButtons(self, inputLabels, layout):
        for label in inputLabels:
            radioBtn = QtWidgets.QRadioButton(label)
            radioBtn.setObjectName('{}RadioButton'.format(label))
            radioBtn.clicked.connect(functools.partial(self.fillMaterialTree, label))
            layout.addWidget(radioBtn)

    def fillMaterialTree(self, materialType):
        self.materialInSceneTree.clear()
        self.materialType = materialType
        materials = self.findMaterialByType(materialType)
        for material in materials:
            item = QtWidgets.QTreeWidgetItem(self.materialInSceneTree)
            item.setText(0, material)

    def findMaterialByType(self, materialType):
        materials = cmd.ls(type=materialType)
        return materials

    def fillHairSystemTree(self):
        self.hairSystemInSceneTree.clear()
        systems = cmd.ls(type='hairSystem')
        for system in systems:
            item = QtWidgets.QTreeWidgetItem(self.hairSystemInSceneTree)
            item.setText(0, system)

    def runAction(self):
        try:
            currentHairItem = self.hairSystemInSceneTree.currentItem()
            currentHairSystem = currentHairItem.text(0)

            attribute = cmd.attributeQuery('vrayHairShader', node=currentHairSystem, exists=True)

            if not attribute:
                cmd.vray('addAttributesFromGroup', currentHairSystem, 'vray_hair_shader', True)
            else:
                result = rbwUI.RBWConfirm(title='Override Question', text='The selected hair system has already a shader connects\nDo you want override it?')

            if not attribute or result:
                createNewShader = self.createMaterialCheckBox.checkState()

                if createNewShader:
                    result = cmd.promptDialog(
                        title='New Shader Name',
                        message='Enter the new Shader Name:',
                        button=['OK', 'Cancel'],
                        defaultButton='OK',
                        cancelButton='Cancel',
                        dismissString='Cancel')
                    if result == 'OK':
                        shaderName = cmd.promptDialog(query=True, text=True)
                        currentShader = cmd.shadingNode(self.materialType, name=shaderName, asShader=True)
                        if currentShader != shaderName:
                            cmd.rename(currentShader, shaderName)
                            currentShader = shaderName
                        currentShaderSG = cmd.sets(name='{}SG'.format(currentShader), empty=True, renderable=True, noSurfaceShader=True)
                        cmd.connectAttr('{}.outColor'.format(currentShader), '{}.surfaceShader'.format(currentShaderSG))
                    else:
                        return
                else:
                    currentShaderItem = self.materialInSceneTree.currentItem()
                    currentShader = currentShaderItem.text(0)

                cmd.connectAttr('{}.outColor'.format(currentShader), '{}.vrayHairShader'.format(currentHairSystem), force=True)
            else:
                pass
        except AttributeError:
            api.log.logger().debug('Please select an Hair System from list')
