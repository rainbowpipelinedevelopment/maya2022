import importlib
from functools import partial

import maya.cmds as cmd

import depts.grooming.tools.hairdress.extractCurvesFromOpenTubeMesh as extCurve
import depts.grooming.tools.hairdress.hair_utilities as hutil

importlib.reload(extCurve)
importlib.reload(hutil)


def connecthairfollicles(*args):
    hutil.connectHairSystemToSomeFollicleAttributes()


def follicledriver(*args):
    hutil.connectFollicleWidthScaleToAdriver()


def curvesfrommesh(*args):
    extCurve.extractCurves()


def reversecurves(*args):
    extCurve.reverseCurveByDistance()


def betweencurves(curves, *args):
    num_curve = cmd.intSliderGrp(curves, q=1, v=1)
    hutil.addCurvesBetweenSelectedCurves(num_curve)


def deletecurves(*args):
    hutil.deleteHairFromList()


def deleterandcurves(curves_del, *args):
    num_curve_del = cmd.intSliderGrp(curves_del, q=1, v=1)
    hutil.deleteRandomCurves(num_curve_del)


def fillgapcurves(*args):
    hutil.fillGapCurves()


def rebuildcurves(*args):
    hutil.rebuildCurves()


def makehalfcurves(*args):
    hutil.makeHalfCurves()


class hairutilitiesUI():
    def __init__(self):
        pass

    winName = "HairUtilitiesUI"
    winWidth = 500

    def open(self):
        if cmd.window(self.winName, exists=1):
            cmd.deleteUI(self.winName)
        if cmd.window(self.winName, exists=1):
            cmd.windowPref(self.winName, remove=1)

        cmd.window(self.winName, title="Hair Utils", widthHeight=[self.winWidth, self.winWidth], bgc=[0.1, 0.1, 0.1],
                   s=1)
        cmd.rowColumnLayout(numberOfColumns=1, columnWidth=[(1, self.winWidth)], cal=(1, "center"), bgc=[0.1, 0.1, 0.1],
                            columnAttach=[5, "both", 5], rowSpacing=[5, 5])
        cmd.text(label="Select 2 curves \n to add Curve between", fn="obliqueLabelFont")
        int_slider = cmd.intSliderGrp(field=True, label='Curves to Add', minValue=1, maxValue=20, fieldMinValue=1, fieldMaxValue=100, value=1)
        cmd.button(label="Generate curve between", command=partial(betweencurves, int_slider), ann="Create curve between", h=50, w=self.winWidth, bgc=[0.2, 0.2, 0.2])
        cmd.button(label="Fill gap curves", command=fillgapcurves, ann="Fill gap curves between selected", h=50, w=self.winWidth, bgc=[0.2, 0.2, 0.2])
        cmd.button(label="Rebuild curves", command=rebuildcurves, ann="From selected curves it will rebuild one by one and delete the original keeping the maya set property.", h=50, w=self.winWidth, bgc=[0.2, 0.2, 0.2])
        cmd.button(label="Make half curves", command=makehalfcurves, ann="From selected curves it will delete half of them.", h=50, w=self.winWidth, bgc=[0.2, 0.2, 0.2])
        cmd.text(label="Select an hairSystem \n and follicles to create a connection", bgc=[0.1, 0.1, 0.1], w=self.winWidth, fn="obliqueLabelFont")
        cmd.button(label="Connect follicles to override clump", command=connecthairfollicles, ann="Connect follicles clump attributes", h=50, w=self.winWidth, bgc=[0.2, 0.2, 0.2])
        cmd.text("Generate a follicle \n to drive overrides", fn="obliqueLabelFont")
        cmd.button(label="Generate Follicle driver", command=follicledriver, ann="Generate driver", h=50, w=self.winWidth, bgc=[0.2, 0.2, 0.2])
        cmd.text(label="Select a mesh to generate \n curves from edge", fn="obliqueLabelFont")
        cmd.button(label="Generate Curves", command=curvesfrommesh, ann="Generate Curves for static hair", w=self.winWidth, h=50, bgc=[0.2, 0.2, 0.2])
        cmd.text(label="Select some CV on the curves \n to reverse Curve direction", fn="obliqueLabelFont")
        cmd.button(label="Reverse Curve direction", command=reversecurves, ann="Reverse Curve direction", w=self.winWidth, h=50, bgc=[0.2, 0.2, 0.2])
        cmd.text(label="Select one or more \n Hairs to delete", fn="obliqueLabelFont")
        cmd.button(label="Delete Hairs", command=deletecurves, ann="Delete Selected hairs", w=self.winWidth, h=50, bgc=[0.2, 0.2, 0.2])
        cmd.text(label="Select Clump \n to delete random hairs", fn="obliqueLabelFont")
        int_slider_del = cmd.intSliderGrp(field=True, label='Hairs to Delete', minValue=1, maxValue=30, fieldMinValue=1, fieldMaxValue=100, value=1)
        cmd.button(label="Delete random hairs", command=partial(deleterandcurves, int_slider_del), ann="Delete hairs from Clump", h=50, w=self.winWidth, bgc=[0.2, 0.2, 0.2])

        cmd.setParent("..")
        cmd.showWindow()
