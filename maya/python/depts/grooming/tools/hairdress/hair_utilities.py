import importlib

import random as rand

import maya.cmds as cmd
import maya.mel as mel

import api.log
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.log)
# importlib.reload(rbwUI)


def connectHairSystemToSomeFollicleAttributes():
    # SELECT AN HAIRSYSTEM AND SOME FOLLICLES

    sel = cmd.ls(sl=1, l=1)
    hs = sel[0]
    flc = sel[1:]
    hss = cmd.listRelatives(hs, shapes=1, ni=1, f=1)
    flcs = cmd.listRelatives(flc, shapes=1, ni=1, f=1)
    attrs = [
        'lengthFlex',
        'damp',
        'stiffness',
        'stiffnessScale',
        'startCurveAttract',
        'attractionScale'
    ]
    for f in flcs:
        cmd.setAttr('{}.overrideDynamics'.format(f), 1)
        for a in attrs:
            try:
                cmd.connectAttr('{}.{}'.format(hss[0], a), '{}.{}'.format(f, a), f=1)
            except:
                pass


def connectFollicleWidthScaleToAdriver():
    mel.eval('convertHairSelection "follicles";')
    flc = cmd.ls(sl=1, dag=1, type='follicle')
    driver = cmd.createNode('follicle', name='driverShape')
    cmd.setAttr(driver + '.overrideDynamics', 1)
    api.log.logger().debug(driver)
    for f in flc:
        cmd.connectAttr('{}.clumpWidthScale'.format(driver), '{}.clumpWidthScale'.format(f), f=1)


def addCurvesBetweenSelectedCurves(num=50):
    # SELECT SOME CURVES
    sel = cmd.ls(sl=1, l=1)
    # print sel
    cmd.makeIdentity(apply=1, t=1, r=1, s=1, n=0, pn=1)
    l = cmd.loft(sel, ch=1, u=1, c=0, ar=1, d=3, ss=1, rn=0, po=0, rsn=1)
    # print l
    div = float(1.0 / num)
    divp = 0
    set_list = cmd.listSets(o=sel[0])
    for i in range(0, num - 1, 1):
        divp += div
        d = cmd.duplicateCurve('{}.v[{}]'.format(l[0], divp), ch=1, rn=0, local=0, name='DUP_curve_{}'.format(PAD(i, 5)))
        cmd.connectAttr('{}.outputSurface'.format(l[1]), '{}.inputSurface'.format(d[1]), f=1)
        cmd.sets(d[0], add=set_list[0], e=1)

    cmd.delete(l[0])


def randomAddCurves(num=15):
    sel = cmd.ls(sl=1, l=1)
    for i in range(0, len(sel), 1):
        r = int(rand.uniform(0, len(sel)))
        cmd.select(sel[i])
        cmd.select(sel[r], tgl=1)
        checkSel = cmd.ls(sl=1, l=1)
        if (checkSel != []):
            addCurvesBetweenSelectedCurves(num)
        else:
            cmd.select(sel[i])
            cmd.select(sel[r], tgl=1)
    cmd.select(sel)


def reduceCurves(howMuchCurves=70):
    sel = cmd.ls(sl=1, l=1)
    for i in range(0, len(sel) - howMuchCurves, 1):
        r = int(rand.uniform(0, len(sel)))
        try:
            cmd.delete(sel[r])
        except:
            pass


def reduceCurvesByNameDUP(howMuchCurves=10):
    sel = cmd.ls(sl=1, l=1)
    dup = []
    for i in range(0, len(sel), 1):
        if (sel[i].find('DUP') == 1):
            dup.append(sel[i])

    for d in range(0, len(dup), 1):
        r = int(rand.uniform(0, len(sel)))
        try:
            cmd.delete(dup[r])
        except:
            pass


def PAD(i, padding):
    return str(i).zfill(padding)


def choose_rand_elem(generic_list, elem_number=3):
    # insert the max number of elem return a list of randomic elem all different
    final_list = []
    while elem_number > 0:
        elem = rand.choice(generic_list)
        # print elem
        if elem in final_list:
            pass  # elem already in list choose another
        else:
            final_list.append(elem)  # good element for the list
            elem_number -= 1  # decrease counter to end cycle
    return final_list


def deleteHair(hair):  # begin with the start curve
    delete_list = []
    delete_list.append(hair)  # add the curve to the delete list
    connections = cmd.listConnections(hair, d=1, s=0, sh=1)
    follicle = cmd.ls(connections, type="follicle")[0]  # get the follicle shape from the connection
    delete_list.append(cmd.listRelatives(follicle, f=1, p=1)[0])  # add the follicle transform to the delete list
    out_curve = cmd.listConnections("{}.outCurve".format(follicle), d=1, s=0)  # get the out curve from the follicle
    delete_list.append(cmd.ls(out_curve, l=1))  # add the output curve long name
    for x in delete_list:
        cmd.delete(x)


def deleteRandomCurves(max_curve=3):  # delete random curve from selected clump
    set_list = cmd.ls(sl=1, l=1)
    delete_list = []
    for x in set_list:
        curve_list = cmd.sets(x, q=1)
        delete_list = choose_rand_elem(curve_list, max_curve)
        for x in delete_list:
            deleteHair(x)


def deleteHairFromList():
    list_hair = cmd.ls(sl=1, l=1)
    for x in list_hair:
        deleteHair(x)


def fillGapCurves():
    allCurves = cmd.ls(sl=1)
    for i in xrange(0, len(allCurves) / 2, 1):
        cloneCurve = cmd.duplicate(allCurves[i], rr=1)
        api.log.logger().debug("cloneCurve: {}".format(cloneCurve))
        api.log.logger().debug("allCurves[-i]: {}".format(allCurves[i + 1]))

        # detect number of CVs of the curves
        degs = cmd.getAttr('{}.degree'.format(cloneCurve[0]))
        spans = cmd.getAttr('{}.spans'.format(cloneCurve[0]))
        actualCvs = degs + spans
        api.log.logger().debug("actualCvs: {}".format(actualCvs))

        degs = cmd.getAttr('{}.degree'.format(allCurves[i + 1]))
        spans = cmd.getAttr('{}.spans'.format(allCurves[i + 1]))
        wantedCvs = degs + spans
        api.log.logger().debug("wantedCvs: {}".format(wantedCvs))

        # eventuale rebuild della curva clonata per funzionare in blendshape con la destination curve
        if not actualCvs == wantedCvs:
            # rebuild curve to match the cvs number
            api.log.logger().debug("rebuild")
            cmd.rebuildCurve(cloneCurve[0], allCurves[i + 1], rt=2)

        blend = cmd.blendShape(allCurves[i + 1], cloneCurve[0])
        api.log.logger().debug("blend: {}".format(blend))
        cmd.setAttr("{}.{}".format(blend[0], allCurves[i + 1]), 0.5)
        cmd.setAttr("{}.origin".format(blend[0]), 0)
        cmd.delete(cloneCurve[0], constructionHistory=1)


def rebuildCurves():
    original_curves = cmd.ls(sl=True)
    for original_curve in original_curves:
        api.log.logger().debug(original_curve)
        set_list = cmd.listSets(o=original_curve)
        cv_number = cmd.getAttr("{}.spans".format(original_curve)) + 3
        api.log.logger().debug("{} CVs".format(cv_number))
        cv_array = []
        for i in range(0, cv_number):
            try:
                cv_array.append(cmd.xform('{}.cv[{}]'.format(original_curve, i), q=True, t=True, ws=True))
            except:
                pass

        cv_array_tuples = []
        for coord in cv_array:
            cv_array_tuples.append(tuple(coord))
        api.log.logger().debug(cv_array)
        api.log.logger().debug(cv_array_tuples)
        new_curve = cmd.curve(p=cv_array_tuples, d=3, ws=True)
        new_curve = cmd.rename(new_curve, "{}_copy".format(original_curve))
        cmd.sets(new_curve, add=set_list[0], e=1)


def makeHalfCurves():
    selectedCurves = cmd.ls(sl=1)
    if len(selectedCurves) == 0:
        api.log.logger().error("There is not selection, please check!")
        return
    api.log.logger().debug("selectedCurves {} {}".format(len(selectedCurves), selectedCurves))
    result = rbwUI.RBWConfirm(title='Confirm', message='Are you sure to delete: {} curves? (the half of the selected curves)'.format(len(selectedCurves) / 2))
    if result:
        cmd.select(clear=1)
        number = 0
        for crv in selectedCurves:
            number = number + 1
            if number % 2 == 0:
                cmd.delete(crv)
        api.log.logger().debug("The total number of curves now is more or less: ".format(len(selectedCurves) / 2))
