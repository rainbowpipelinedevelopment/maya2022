import importlib
import os

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'grooming')


def run_qht(*args):
    import depts.grooming.tools.hairdress.hirrigmenu as hair
    importlib.reload(hair)
    hairmenu = hair.quickHairToolsUI()
    hairmenu.open()


def run_hutils(*args):
    import depts.grooming.tools.hairdress.hair_utilities_menu as hrsutil
    importlib.reload(hrsutil)
    hairutils = hrsutil.hairutilitiesUI()
    hairutils.open()


def run_mlt_manager(*args):
    import depts.grooming.tools.hairdress.hairSystemMltManagerUI as mltUI
    importlib.reload(mltUI)

    win = mltUI.hairSystemMltManagerUI()
    win.show()


qhtTool = {
    "name": "QuickHairRig",
    "statustip": "Apre pulsantiera Rig Hair",
    "launch": run_qht,
    "icon": os.path.join(iconDir, "quickhair.png")
}


hairutils = {
    "name": "HairUtilities",
    "statustip": "Apre pulsantiera Hair utilities",
    "launch": run_hutils,
    "icon": os.path.join(iconDir, "hairtool_icon.png")
}


mltManagerTool = {
    "name": "MltManagerTool",
    "statustip": "Gestisce l'associazione tra hair systems e shaders",
    "launch": run_mlt_manager,
    "icon": os.path.join(iconDir, "mlt_manager.png")
}


tools = [
    qhtTool,
    hairutils,
    mltManagerTool
]


PackageTool = {
    "name": "hairdress",
    "tools": tools,
    "icon_size": "32",
    "icon_only": False
}