import importlib
from PySide2 import QtGui, QtCore, QtWidgets
import os
import getpass
import re

import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI

# importlib.reload(rbwUI)

################################################################################
# @brief      This class describes a set user content ui.
#
class setUserContentUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(setUserContentUI, self).__init__()
        if cmd.window('setUserContentUI', exists=True):
            cmd.deleteUI('setUserContentUI')
        self.pipeMayaProject = os.getenv('MC_FOLDER').replace('\\', '/')
        self._usersDir = os.path.join(os.path.dirname(self.pipeMayaProject), '_users', getpass.getuser()).replace('\\', '/')
        self.setObjectName('setUserContentUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        # imposto la finestra
        self.topWidget = rbwUI.RBWFrame(layout='V', radius=5)

        # inizializzo la griglia e gli elementi da inserire nella griglia
        self.selectionGroup = rbwUI.RBWGroupBox(title='Select the folder:', layout='H', fontSize=12)

        self.folderComboBox = rbwUI.RBWComboBox(bgColor='transparent')

        self.setButton = rbwUI.RBWButton(text='Set', size=[50, 20])
        self.setButton.clicked.connect(self.setProject)

        self.selectionGroup.addWidget(self.folderComboBox)
        self.selectionGroup.addWidget(self.setButton)

        self.topWidget.addWidget(self.selectionGroup)
        self.mainLayout.addWidget(self.topWidget)

        self.fillComboBox()

        self.setMinimumSize(350, 150)

        self.setTitle("Set User Content")
        self.setFocus()

    ############################################################################
    # @brief      Fills the ComboBox.
    #
    def fillComboBox(self):
        folders = []
        files = os.listdir(self._usersDir)

        for file in files:
            filePath = os.path.join(self._usersDir, file).replace('\\', '/')
            reCheck = re.search("mc_(.*)_(.*)_(.*)_(.*)", file)
            if os.path.isdir(filePath) and reCheck:
                folders.append(file)

        self.folderComboBox.addItems(folders)

    ############################################################################
    # @brief      Sets the project.
    #
    def setProject(self):
        currentFolder = self.folderComboBox.currentText()
        if currentFolder != '':
            newMayaProject = os.path.join(self._usersDir, currentFolder).replace('\\', '/')
            cmd.workspace(newMayaProject, o=True)
            confirmMessage = 'MC project: \"{}\" set!'.format(currentFolder)
            rbwUI.RBWConfirm(text=confirmMessage, title='Set User Content', defCancel=None)
        else:
            warningMessage = 'No MC project selected!'
            rbwUI.RBWError(text=warningMessage, title='Set User Content')
