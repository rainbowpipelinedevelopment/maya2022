import importlib
import os
from PySide2 import QtWidgets, QtCore
import maya.cmds as cmd

import api.xgen
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.xgen)
# importlib.reload(rbwUI)


################################################################################
# @brief      This class describes a ptex setup for description ui.
#
class PtexSetupForDescriptionUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(PtexSetupForDescriptionUI, self).__init__()
        if cmd.window('PtexSetupForDescriptionUI', exists=True):
            cmd.deleteUI('PtexSetupForDescriptionUI')

        self.setObjectName('PtexSetupForDescriptionUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topWidget = rbwUI.RBWFrame(layout='G', radius=5)

        # expression tree
        self.expressionsGroup = rbwUI.RBWGroupBox(title='Expression', layout='V', fontSize=12)
        self.topWidget.layout.addWidget(self.expressionsGroup, 0, 0)

        # description tree
        self.descriptionsGroup = rbwUI.RBWGroupBox(title='Description', layout='V', fontSize=12)
        self.topWidget.layout.addWidget(self.descriptionsGroup, 0, 1)

        # run button
        self.setUpDescriptionsButton = rbwUI.RBWButton(text='Setup Descriptions')
        self.setUpDescriptionsButton.clicked.connect(self.setup)
        self.topWidget.layout.addWidget(self.setUpDescriptionsButton, 1, 0, 1, 2)

        self.mainLayout.addWidget(self.topWidget)

        self.fillGroups()

        self.setMinimumSize(300, 500)
        self.setTitle('Ptex Setup for Description UI')
        self.setFocus()

    ############################################################################
    # @brief      fill the groups
    #
    def fillGroups(self):
        self.fillExpressionsGroup()
        self.fillDescriptionsGroup()

    ############################################################################
    # @brief      fill expression group
    #
    def fillExpressionsGroup(self):
        self.expressions = list(api.xgen.listExpressions(api.xgen.currentCollection()))
        self.expressions.sort()
        for expression in self.expressions:
            checkBox = rbwUI.RBWCheckBox(text=expression)
            self.expressionsGroup.addWidget(checkBox)

    ############################################################################
    # @brief      fill descriptions group
    #
    def fillDescriptionsGroup(self):
        self.descriptions = list(api.xgen.listDescription(api.xgen.currentCollection()))
        self.descriptions.sort()
        for description in self.descriptions:
            checkBox = rbwUI.RBWCheckBox(text=description)
            self.descriptionsGroup.addWidget(checkBox)

    ############################################################################
    # @brief      setup function
    #
    def setup(self):
        expressions = []
        for expressionCheckBox in self.expressionsGroup.findChildren(rbwUI.RBWCheckBox):
            expressionName = expressionCheckBox.getText()
            if expressionCheckBox.isChecked():
                expressions.append(expressionName)
        if len(expressions) > 1:
            errorMessage = 'You select more than one expression.\nPlease select only one and press again.'
            rbwUI.RBWError(title='ATTENTION', text=errorMessage)
            return
        elif len(expressions) == 0:
            errorMessage = 'You not select any expression.\nPlease one and press again.'
            rbwUI.RBWError(title='ATTENTION', text=errorMessage)
            return
        elif len(expressions) == 1:
            expression = expressions[0]
            expressionName = '{}()'.format(expression)

        descriptions = []
        for descriptionCheckBox in self.descriptionsGroup.findChildren(rbwUI.RBWCheckBox):
            description = descriptionCheckBox.getText()
            if descriptionCheckBox.isChecked():
                descriptions.append(description)

        if len(descriptions) > 0:
            for description in descriptions:
                api.xgen.createExpression("custom_color_root_color", expressionName, api.xgen.currentCollection(), str(description), "RendermanRenderer")
                api.xgen.createExpression("color", expressionName, api.xgen.currentCollection(), str(description), "GLRenderer")
        else:
            errorMessage = 'You not select any description.\nPlease one and press again.'
            rbwUI.RBWError(title='ATTENTION', text=errorMessage)
            return
