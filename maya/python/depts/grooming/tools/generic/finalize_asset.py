import importlib
import maya.cmds as cmd
import maya.mel as mel
import pymel.core as pm

import api.scene
import api.xgen
import api.log

importlib.reload(api.scene)
importlib.reload(api.xgen)
importlib.reload(api.log)


################################################################################
# @brief      finalize the grooming asset.
#
def finalize():
    # FOR EVERY HAIR SYSTEM SET REFRACT AND REFLECT TO 1
    hairSystems = cmd.ls(type='hairSystem')
    if len(hairSystems) > 0:
        for hairSystem in hairSystems:
            cmd.setAttr('{}.visibleInReflections'.format(hairSystem), 1)
            cmd.setAttr('{}.visibleInRefractions'.format(hairSystem), 1)

    # SET THE PREVIEW TO 100
    xg_palette = api.xgen.listCollection()[0]
    xgen_descriptions = sorted(api.xgen.listDescription(xg_palette))

    for description in xgen_descriptions:
        api.xgen.setPreviewValue(str(xg_palette), str(description), str(100.00))

    # CHECK IF xgGoom NODE EXISTS
    if cmd.objExists('xgGroom'):
        cmd.delete('xgGroom')

    # CHECK IF THERE ARE DISPLAY LEVELS OUTSIDE THE DEFAULT
    displayLayers = cmd.ls(type='displayLayer')
    if len(displayLayers) > 2:
        if displayLayers != 'defaultLayer':
            cmd.delete(displayLayers)

    # DELETE UNUSED NODES FROM THE HYPERSHADE
    mel.eval('hyperShadePanelMenuCommand("hyperShadePanel1", "deleteUnusedNodes");')

    # DELETE UNKNOW NODES
    unknownNodes = cmd.ls(type="unknown")
    for item in unknownNodes:
        if cmd.objExists(item):
            cmd.delete(item)

    # MASSIVE CLEANUP
    massiveCleanup()


################################################################################
# @brief      massive cleanup by Diego
#
def massiveCleanup():
    openScenePath = str(cmd.file(q=1, sceneName=1))
    api.log.logger().debug(openScenePath)

    # TO FIX ERROR: // Error: line : scriptedPanel: Object 'VFBPanelType' not found. //
    mel.eval('evalDeferred "vrayCreateVFBPanel"')

    api.scene.pluginCleanUp()

    if cmd.objExists('aTools_StoreNode'):
        cmd.delete('aTools_StoreNode')

    # TO FIX: // Error: You are attempting to load a file that contains legacy render layers into a scene that uses render setup. This combination is unsupported and may result in unexpected behavior. //
    all = cmd.ls(type="renderLayer")
    cmd.select(all, r=1)
    sel = cmd.ls(sl=True)
    try:
        cmd.delete(sel)
    except:
        pass

    # TO FIX: // Error: line 1: Cannot find procedure "onModelChange3dc". //
    for item in pm.lsUI(editors=True):
        if isinstance(item, pm.ui.ModelEditor):
            pm.modelEditor(item, edit=True, editorChanged="")
