import importlib
from PySide2 import QtWidgets, QtCore
import os
import functools

import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI
import api.xgen

# importlib.reload(rbwUI)
importlib.reload(api.xgen)

################################################################################
# @brief      This class describes a copy expression name ui.
#
class CopyExpressionNameUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(CopyExpressionNameUI, self).__init__()
        if cmd.window('CopyExpressionNameUI', exists=True):
            cmd.deleteUI('CopyExpressionNameUI')

        self.setObjectName('CopyExpressionNameUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topWidget = rbwUI.RBWFrame(layout='V', radius=5)

        self.expressionsTree = rbwUI.RBWTreeWidget(['Expression', 'Copy'])
        self.expressionsTree.setColumnWidth(0, 180)
        self.expressionsTree.setColumnWidth(1, 90)

        self.topWidget.addWidget(self.expressionsTree)

        self.refreshExpressionsListButton = rbwUI.RBWButton(icon=['refresh.png'], text='Refresh List', size=[397, 35])
        self.refreshExpressionsListButton.clicked.connect(self.fillExpressionsTree)
        self.topWidget.addWidget(self.refreshExpressionsListButton)

        self.mainLayout.addWidget(self.topWidget)

        self.fillExpressionsTree()

        self.setMinimumSize(420, 200)
        self.setTitle('Copy Expression Name')
        self.setFocus()

    ############################################################################
    # @brief      fill the expression tree.
    #
    def fillExpressionsTree(self):
        self.expressionsTree.clear()

        self.expressions = api.xgen.listExpressions(api.xgen.currentCollection())
        for expression in self.expressions:
            child = QtWidgets.QTreeWidgetItem(self.expressionsTree)
            child.setText(0, expression)
            copyExpressionNameButton = rbwUI.RBWButton(icon=['copy.png'], text='COPY', hover='#555555')
            copyExpressionNameButton.clicked.connect(functools.partial(self.copyExpressionName, self.expressionsTree.topLevelItemCount() - 1))
            self.expressionsTree.setItemWidget(child, 1, copyExpressionNameButton)

    ############################################################################
    # @brief      copy the expression name.
    #
    # @param      rowIndex  The row index
    #
    def copyExpressionName(self, rowIndex):
        root = self.expressionsTree.invisibleRootItem()
        item = root.child(rowIndex)
        expressionName = item.text(0)

        command = 'echo | set /p nul={}()| clip'.format(expressionName.strip())
        os.system(command)
