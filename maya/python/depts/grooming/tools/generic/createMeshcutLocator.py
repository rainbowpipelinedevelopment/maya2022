import importlib

import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI

# importlib.reload(rbwUI)


################################################################################
# @brief      Gets the top group name.
#
# @return     The top group name or "False" if there is not a top group with 'xgmPalette' node.
#
def getTopGroupName():
    xgenPal = cmd.ls(type='xgmPalette')
    if xgenPal:
        group = cmd.listRelatives(xgenPal, parent=True)
        return group[0]
    else:
        return False


################################################################################
# @brief      Stores a meshcut.
#
# @return
#
def storeMeshcut():
    toolName = 'Create Meshcut Set'
    locatorName = 'LOC_meshcut_tag'

    if cmd.objExists(locatorName):
        existingMeshcutMessage = '''This project already contains a MeshCut Set'''
        rbwUI.RBWWarning(text=existingMeshcutMessage, title=toolName)
        return

    # check for selected meshes
    meshList = cmd.ls(selection=True)
    if meshList:

        # check if every item in the meshList is a proper mesh for the Meshcut set
        for mesh in meshList:

            isMeshCheck = False
            for child in cmd.listRelatives(mesh, children=True):
                if len(cmd.ls(child)) > 1:
                    meshName = child.split(":")[1]
                    repeatingNameMessage = 'WARNING! more than one object is named: {}\n the procedure is interrupted!'.format(meshName)
                    rbwUI.RBWError(text=repeatingNameMessage, title=toolName)
                    return

                if cmd.objectType(child) == "mesh":
                    isMeshCheck = True

            if not isMeshCheck:
                notMeshMessage = '{} is not a mesh.\n ONLY select SINGLE meshes referenced from RIGGING'.format(mesh)
                rbwUI.RBWError(text=notMeshMessage, title=toolName)
                return

            if not mesh.count(":"):
                notReferenceMessage = '{} is not a reference.\n ONLY select SINGLE meshes referenced from RIGGING'.format(mesh)
                rbwUI.RBWError(text=notReferenceMessage, title=toolName)
                return

        topGroup = getTopGroupName()
        if topGroup:
            spaceLocator = cmd.spaceLocator(name=locatorName)
            cmd.parent(spaceLocator, topGroup)

            # create the attributes by adding the MESHCUT_ tag before the mesh name
            for mesh in meshList:
                splitMesh = mesh.split(':')
                cmd.addAttr(longName='MESHCUT_{}'.format(splitMesh[1]), attributeType='bool')

            meshcutDoneMessage = "Meshcut Set Completed!"
            rbwUI.RBWConfirm(text=meshcutDoneMessage, title=toolName, defCancel=None)

        else:
            noTopGroupMessage = '''There are no groups with Xgen nodes! The Meshcut Setting is interrupted!'''
            rbwUI.RBWError(text=noTopGroupMessage, title=toolName)
    else:
        noMeshSelectedMessage = '''No mesh selected! First select the meshes you want.\nThen press the \"{}\" tool again'''.format(toolName)
        rbwUI.RBWError(text=noMeshSelectedMessage, title=toolName)
