import importlib
from PySide2 import QtWidgets, QtCore
import os

import maya.cmds as cmd

import api.log
import api.widgets.rbw_UI as rbwUI
import depts.grooming.tools.generic.CAC_reader as mkReader

importlib.reload(api.log)
# importlib.reload(rbwUI)
importlib.reload(mkReader)


class CACReaderUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs the object
    #
    # @param      self  The object
    #
    def __init__(self):
        super(CACReaderUI, self).__init__()
        if cmd.window('ReaderUI', exists=True):
            cmd.deleteUI('ReaderUI')

        self.setObjectName('ReaderUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      initUI method
    #
    # @param      self  The object
    #
    # @return     { description_of_the_return_value }
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topWidget = rbwUI.RBWFrame(radius=5, layout='V')

        self.optionGroup = rbwUI.RBWGroupBox(title='Options:', layout='V', fontSize=12)

        self.degreeLine = rbwUI.RBWLineEdit(title='Degree', bgColor='transparent')
        self.axisLine = rbwUI.RBWLineEdit(title='Axis', bgColor='transparent')
        self.ignoreAxisCheckBox = rbwUI.RBWCheckBox(text='Ignore Axis')

        self.ignoreAxisCheckBox.stateChanged.connect(self.toggleAxisOption)

        self.optionGroup.addWidget(self.degreeLine)
        self.optionGroup.addWidget(self.axisLine)
        self.optionGroup.addWidget(self.ignoreAxisCheckBox)

        self.topWidget.addWidget(self.optionGroup)

        self.makeReaderButton = rbwUI.RBWButton(size=[378, 30], text='Make CAC Reader')
        self.makeReaderButton.clicked.connect(self.makeReader)
        self.topWidget.addWidget(self.makeReaderButton)

        self.mainLayout.addWidget(self.topWidget)

        self.setMinimumSize(400, 200)
        self.setTitle('Make CAC Reader UI')
        self.setFocus()

    ############################################################################
    # @brief      toggle axis option.
    #
    def toggleAxisOption(self):
        ignoreAxis = self.ignoreAxisCheckBox.isChecked()
        if ignoreAxis:
            self.axisLine.edit.setReadOnly(True)
        else:
            self.axisLine.edit.setReadOnly(False)

    ############################################################################
    # @brief      Makes a reader.
    #
    def makeReader(self):
        degree = int(self.degreeLine.text())
        axis = self.axisLine.text().lower()
        ignoreAxis = self.ignoreAxisCheckBox.isChecked()

        if ignoreAxis:
            mkReader.makeCACwriterAndCACreader(degree, None)
        else:
            mkReader.makeCACwriterAndCACreader(degree, axis)
