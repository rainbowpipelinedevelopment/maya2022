import importlib
import os

import maya.cmds as cmd
import maya.mel as mel
import xgenm as xg

import api.xgen
import api.scene
import api.farm
import api.json
import api.saveNode
import api.savedAsset
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.xgen)
importlib.reload(api.scene)
importlib.reload(api.farm)
importlib.reload(api.json)
importlib.reload(api.saveNode)
# importlib.reload(api.savedAsset)
# importlib.reload(rbwUI)


fiberTypes = {'dynamicFibers': 'DYN', 'innerFibers': 'ING', 'clusterFibers': 'CLG'}
guideTypes = {'dynamicGuides': 'DYN', 'innerGuides': 'ING', 'clusterGuides': 'CLG'}


################################################################################
# @brief      export xgen curve based on guides.
#
def exportCurveFromGuides(guideType=None):
    startScene = api.scene.scenename()
    saveNodes = cmd.ls('grooming_saveNode')

    if len(saveNodes) == 0:
        rbwUI.RBWError(title='ATTENTION', text='No grooming saveNode in scene.\nPlease make a new save and try again.')
        return

    elif len(saveNodes) > 1:
        rbwUI.RBWError(title='ATTENTION', text='More than one grooming saveNode in scene.\nPlease clean your scene and try again.')
        return

    else:
        currentSavedAsset = api.savedAsset.SavedAsset(saveNode=saveNodes[0])

        if guideType:
            exportType = guideType
            descriptionType = guideTypes[guideType]
        else:
            exportType = 'dynamicGuides'
            descriptionType = 'DYN'

        allGuidesDir = os.path.join(
            os.getenv('MC_FOLDER'),
            'scenes',
            currentSavedAsset.getCategory(),
            currentSavedAsset.getGroup(),
            currentSavedAsset.getName(),
            currentSavedAsset.getVariant(),
            'export',
            'grooming',
            exportType
        ).replace('\\', '/')

        if not os.path.exists(allGuidesDir):
            os.makedirs(allGuidesDir)

        versions = [dir for dir in os.listdir(allGuidesDir) if dir.startswith('vr')]

        if versions:
            versions.sort()
            lastVersion = versions[-1]
            lastVersionNumeber = int(lastVersion.split('r')[1])
            version = 'vr{}'.format(str(lastVersionNumeber + 1).zfill(3))
        else:
            version = 'vr001'

        guidesDir = os.path.join(allGuidesDir, version).replace('\\', '/')
        if not os.path.exists(guidesDir):
            os.makedirs(guidesDir)

        currentCollection = api.xgen.currentCollection()
        for description in api.xgen.listDescription(currentCollection):
            descInfo = description.split('_')
            if len(descInfo) >= 4:
                if descInfo[2] == descriptionType:
                    descriptionNumberString = descInfo[-1]
                    cmd.select(description)
                    guides = api.xgen.guidesToCurves()
                    cmd.select(clear=True)

                    cmd.rename('xgCurvesFromGuides1', '{}_{}'.format(exportType, descriptionNumberString))

                    roots_args = ["-root {}".format(guide) for guide in guides]
                    roots_args = " ".join(roots_args)

                    plug = "AbcExport"
                    if not cmd.pluginInfo(plug, q=True, l=True):
                        cmd.loadPlugin(plug, qt=True)

                    descInfo.pop(4)
                    alembicName = '_'.join(descInfo)
                    alembicPath = os.path.join(guidesDir, '{}.abc'.format(alembicName)).replace('\\', '/')

                    alembicCmd = "-frameRange 1 1 -stripNamespaces -uvWrite -worldSpace -dataFormat ogawa {} -file {}".format(roots_args, alembicPath)

                    cmd.AbcExport(j=alembicCmd)
                else:
                    continue
            else:
                api.log.logger().error('The description {} don\'t follow the pipeline naming convenction.')

        descriptionInfo = api.xgen.listDescription(currentCollection)[0].split('_')[:-2]
        guidesGroupName = '{}_{}_{}_{}'.format(descriptionInfo[0], descriptionInfo[1], descriptionType, descriptionInfo[3])

        cmd.group(empty=True, n=guidesGroupName)
        cmd.select('{}_des*'.format(exportType), replace=True)
        cmd.parent(world=True)
        cmd.parent(cmd.ls(selection=True), guidesGroupName)

        # creare il save node e salvare come ma
        guidesSaveNode = api.saveNode.SaveNode()
        guidesSaveNode.create(exportType)

        guidesSaveNodeParam = {
            'version': version,
            'user': os.getenv('USERNAME'),
            'asset_id': currentSavedAsset.getAssetId(),
            'db_id': currentSavedAsset.db_id
        }

        for label in guidesSaveNodeParam:
            guidesSaveNode.setAttr(label, guidesSaveNodeParam[label])

        guidesSaveNode.connect('{}.creator'.format(guidesGroupName))

        cmd.select(guidesGroupName, replace=True)
        exportPath = os.path.join(guidesDir, 'all{}{}.ma'.format(exportType[0].upper(), exportType[1:])).replace('\\', '/')

        api.export.export_ma(exportPath)

        cmd.file(new=True, force=True)
        cmd.file(startScene, open=True, force=True)


################################################################################
# @brief      export all xgen fibers from every desc.
#
def exportFibers(fiberType=None):
    startScene = api.scene.scenename()
    saveNodes = cmd.ls('grooming_saveNode')

    if len(saveNodes) == 0:
        rbwUI.RBWError(title='ATTENTION', text='No grooming saveNode in scene.\nPlease make a new save and try again.')
        return

    elif len(saveNodes) > 1:
        rbwUI.RBWError(title='ATTENTION', text='More than one grooming saveNode in scene.\nPlease clean your scene and try again.')
        return

    else:
        currentSavedAsset = api.savedAsset.SavedAsset(saveNode=saveNodes[0])

        if fiberType:
            exportType = fiberType
            descriptionType = fiberTypes[fiberType]
        else:
            exportType = 'dynamicFibers'
            descriptionType = 'DYN'

        allFibersDir = os.path.join(
            os.getenv('MC_FOLDER'),
            'scenes',
            currentSavedAsset.getCategory(),
            currentSavedAsset.getGroup(),
            currentSavedAsset.getName(),
            currentSavedAsset.getVariant(),
            'export',
            'grooming',
            exportType
        ).replace('\\', '/')

        if not os.path.exists(allFibersDir):
            os.makedirs(allFibersDir)

        versions = [dir for dir in os.listdir(allFibersDir) if dir.startswith('vr')]

        if versions:
            versions.sort()
            lastVersion = versions[-1]
            lastVersionNumeber = int(lastVersion.split('r')[1])
            version = 'vr{}'.format(str(lastVersionNumeber + 1).zfill(3))
        else:
            version = 'vr001'

        fibersDir = os.path.join(allFibersDir, version).replace('\\', '/')
        if not os.path.exists(fibersDir):
            os.makedirs(fibersDir)

        currentCollection = api.xgen.currentCollection()
        for description in api.xgen.listDescription(currentCollection):
            if len(description.split('_')) >= 4:
                if description.split('_')[2] == descriptionType:

                    api.xgen.setCreateMel(currentCollection, description)
                    api.xgen.setMelPath(currentCollection, description, fibersDir)
                    xg.initInterpolation(currentCollection, description)
                    mel.eval('xgmMelRender {"%s"}' % description)

                else:
                    continue
            else:
                api.log.logger().error('The description {} don\'t follow the pipeline naming convenction.')

        melFiles = [file for file in os.listdir(fibersDir) if file.endswith('.mel')]
        melFiles.sort()

        for melFile in melFiles:
            # first job params
            sceneJobParams = {}

            fullMelPath = os.path.join(fibersDir, melFile).replace('\\', '/')

            melInfo = melFile[:-4].split('_')
            if len(melInfo) == 6:
                melInfo.pop(4)
            maFile = '_'.join(melInfo)

            sceneJobParams['batchName'] = '{}_{}_{}_{}_COMPOSE_{}'.format(currentSavedAsset.getCategory(), currentSavedAsset.getGroup(), currentSavedAsset.getName(), currentSavedAsset.getVariant(), exportType.upper())
            sceneJobParams['jobName'] = '{}_{}_compose_scene'.format(maFile, version)
            sceneJobParams['melFile'] = fullMelPath
            sceneJobParams['mode'] = 'ffr'
            sceneJobParams['goal'] = 'compose ma scene'
            sceneJobParams['obj'] = currentSavedAsset.getAsset()

            composeJob = api.farm.createAndSubmitJob(sceneJobParams)

            # second job params
            alembicJobParams = {}
            fullMaPath = os.path.join(fibersDir, '{}.ma'.format(maFile)).replace('\\', '/')
            fullAlembicPath = fullMaPath.replace('.ma', '.abc')

            alembicJobParams['batchName'] = '{}_{}_{}_{}_EXPORT_{}'.format(currentSavedAsset.getCategory(), currentSavedAsset.getGroup(), currentSavedAsset.getName(), currentSavedAsset.getVariant(), exportType.upper())
            alembicJobParams['jobName'] = '{}_{}_export_alembic'.format(maFile, version)
            alembicJobParams['outputFileName'] = fullAlembicPath
            alembicJobParams['inputFileName'] = fullMaPath
            alembicJobParams['dependency'] = [composeJob]
            alembicJobParams['mode'] = 'ffr'
            alembicJobParams['goal'] = 'export alembic'
            alembicJobParams['obj'] = currentSavedAsset.getAsset()

            api.farm.createAndSubmitJob(alembicJobParams)

        cmd.file(new=True, force=True)
        cmd.file(startScene, open=True, force=True)


################################################################################
# @brief      export the xgen scalp of description.
#
def exportScalps():
    saveNodes = cmd.ls('grooming_saveNode')

    if len(saveNodes) == 0:
        rbwUI.RBWError(title='ATTENTION', text='No grooming saveNode in scene.\nPlease make a new save and try again.')
        return

    elif len(saveNodes) > 1:
        rbwUI.RBWError(title='ATTENTION', text='More than one grooming saveNode in scene.\nPlease clean your scene and try again.')
        return

    else:
        currentSavedAsset = api.savedAsset.SavedAsset(saveNode=saveNodes[0])

        groomingGroup = cmd.listConnections(saveNodes[0], source=False)[0]

        allScalpsDir = os.path.join(
            os.getenv('MC_FOLDER'),
            'scenes',
            currentSavedAsset.getCategory(),
            currentSavedAsset.getGroup(),
            currentSavedAsset.getName(),
            currentSavedAsset.getVariant(),
            'export',
            'grooming',
            'scalps'
        ).replace('\\', '/')

        if not os.path.exists(allScalpsDir):
            os.makedirs(allScalpsDir)

        versions = [dir for dir in os.listdir(allScalpsDir) if dir.startswith('vr')]

        if versions:
            versions.sort()
            lastVersion = versions[-1]
            lastVersionNumeber = int(lastVersion.split('r')[1])
            version = 'vr{}'.format(str(lastVersionNumeber + 1).zfill(3))
        else:
            version = 'vr001'

        scalpsDir = os.path.join(allScalpsDir, version).replace('\\', '/')
        if not os.path.exists(scalpsDir):
            os.makedirs(scalpsDir)

        scalps = []
        scalp2Descs = {}

        scalp2DescsJson = os.path.join(scalpsDir, 'scalps2Descs.json').replace('\\', '/')

        for description in api.xgen.listDescription():
            if '_DYN_' in description or '_ING_' in description or '_CLG_' in description:
                scalp = api.xgen.getScalp(description, groomingGroup)
                if scalp not in scalps:
                    scalps.append(scalp)
                if scalp not in scalp2Descs:
                    scalp2Descs[scalp] = []
                scalp2Descs[scalp].append(description)

        for scalp in scalps:
            alembicPath = os.path.join(scalpsDir, '{}.abc'.format(scalp.split('|')[-1])).replace('\\', '/')
            alembicCmd = "-frameRange 1 1 -stripNamespaces -uvWrite -worldSpace -dataFormat ogawa -root {} -file {}".format(scalp, alembicPath)

            cmd.AbcExport(j=alembicCmd)
            api.json.json_write(scalp2Descs, scalp2DescsJson)
