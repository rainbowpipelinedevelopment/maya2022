import importlib


def run_CAC_reader_UI(*args):
    import depts.grooming.tools.generic.CAC_reader_UI as readerUI
    importlib.reload(readerUI)

    win = readerUI.CACReaderUI()
    win.show()


def run_manageMeshCut(*args):
    import depts.grooming.tools.generic.manage_mesh_cut as mmc
    importlib.reload(mmc)

    win = mmc.ManageXGenMeshCutUI()
    win.show()


def run_createExpression(*args):
    import depts.grooming.tools.generic.createExpression as createExpression
    importlib.reload(createExpression)

    win = createExpression.CreateExpressionUI()
    win.show()


def run_copyExpressionName(*args):
    import depts.grooming.tools.generic.copyExpressionName as copyExpressionName
    importlib.reload(copyExpressionName)

    win = copyExpressionName.CopyExpressionNameUI()
    win.show()


def run_ptexSetupForDescrition(*args):
    import depts.grooming.tools.generic.ptexSetupForDescription as ptexSetupForDescription
    importlib.reload(ptexSetupForDescription)

    win = ptexSetupForDescription.PtexSetupForDescriptionUI()
    win.show()


def run_finalize(*args):
    import depts.grooming.tools.generic.finalize_asset as finalize_asset
    importlib.reload(finalize_asset)

    finalize_asset.finalize()


def run_setUserContent(*args):
    import depts.grooming.tools.generic.setUserContent as suc
    importlib.reload(suc)

    window = suc.setUserContentUI()
    window.show()


def run_renameCollection(*args):
    import depts.grooming.tools.generic.renameCollectionUI as renameCollectionUI
    importlib.reload(renameCollectionUI)

    win = renameCollectionUI.RenameCollectionUI()
    win.show()


def run_createMeshcutSet(*args):
    import depts.grooming.tools.generic.createMeshcutLocator as createMeshcutLocator
    importlib.reload(createMeshcutLocator)
    createMeshcutLocator.storeMeshcut()


def run_exportMaterials(*args):
    import depts.grooming.tools.generic.exportMaterialsUI as exportMaterialsUI
    importlib.reload(exportMaterialsUI)

    win = exportMaterialsUI.ExportMaterialsUI()
    win.show()


makeCACreader = {
    'name': 'Make CAC reader',
    'launch': run_CAC_reader_UI
}

manageMeshCut = {
    'name': 'Manage XGen MeshCut',
    'launch': run_manageMeshCut
}

createExpression = {
    'name': 'Create Expression',
    'launch': run_createExpression
}

copyExpressionName = {
    'name': 'Copy Expression Name',
    'launch': run_copyExpressionName
}

ptexSetupForDescription = {
    'name': 'Ptex Description Setup',
    'launch': run_ptexSetupForDescrition
}

finalizeAsset = {
    'name': 'Finalize',
    'launch': run_finalize
}

setUserContent = {
    'name': 'Set User Content',
    'launch': run_setUserContent
}

renameCollection = {
    'name': 'Rename Xgen Collection',
    'launch': run_renameCollection
}

createMeshcutSet = {
    'name': 'Create Meshcut Set',
    'launch': run_createMeshcutSet
}

exportMaterialsTools = {
    "name": "Export Materials",
    "launch": run_exportMaterials
}


tools = [
    makeCACreader,
    manageMeshCut,
    createExpression,
    copyExpressionName,
    ptexSetupForDescription,
    finalizeAsset,
    setUserContent,
    renameCollection,
    createMeshcutSet,
    exportMaterialsTools
]


PackageTool = {
    "name": "generic",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}