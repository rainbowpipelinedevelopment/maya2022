import importlib
import maya.cmds as cmd
import maya.mel as mel

import api.log
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.log)
# importlib.reload(rbwUI)


def makeCACwriterAndCACreader(degreeValue=3, axis='x'):
    '''
    THIS SCRIPT MAKE A NURBS SURFACE LOFT BY CURVES SELECTION
    AND RECONNECT ORIGINAL SELECTED CURVES AS EXTRACTED FROM THE GENERATED NURBS SURFACE.
    SELECT CURVES OR START CURVES IN ORDER AND RUN: makeCACwriterAndCACreader()
    '''

    selCurves = []
    selCurvesShapes = cmd.ls(selection=True, dagObjects=1, type='nurbsCurve')

    if axis is not None:

        curvesShape = {}

        for curve in selCurvesShapes:
            component = cmd.getAttr('{}.boundingBoxCenter{}'.format(curve, axis.upper()))
            curvesShape[component] = curve

        positions = curvesShape.keys()
        positions.sort()

        selCurvesShapes = []

        for position in positions:
            selCurvesShapes.append(curvesShape[position])

    if selCurvesShapes is None:
        return mel.eval('warning "SELECT AT LEAST TWO CURVES"')
    # print 'selCurvesShapes ===> ', selCurvesShapes
    for sel in selCurvesShapes:
        if cmd.nodeType(sel) == 'nurbsCurve':
            selCurves.append(sel)

    if selCurves is None:
        return mel.eval('warning "NO CURVES SELECTED"')
    if len(selCurves) == 1:
        return mel.eval('warning "SELECT AT LEAST TWO CURVES"')
    else:
        for sel in selCurves:
            # print 'sel ===> ', sel
            pass

    # CHECK IF THERE ARE HAIRSYSTEM CONNECTED TO CURVES
    # IF YES RUN fixPFXhairScaleResample()
    # fixPFXhairScaleResample CONNECTS AND REPAIRS PFX HAIR SCALABLE OPTIONS
    # BASED ON CLUMP WIDTH AND HAIR WIDTH ATTRIBUTES.

    hairSystem = []
    mel.eval('convertHairSelection "hairSystems"')
    lookForHairSystem = cmd.ls(selection=True, dagObjects=1, type='hairSystem')
    # print 'current hairSystem ===> ', lookForHairSystem
    if lookForHairSystem == [] or lookForHairSystem is None:
        return mel.eval('warning "THERE ARE NO HAIRSYSTEM CONNECTION FROM THIS CURVE... ...PLEASE CHECK"')
    else:
        hairSystem += lookForHairSystem

    # print 'HairSystems connected to curves ===> ', hairSystem
    # remove_dups REMOVES DUPLICATED ITEMS IN ARRAY
    hairSystem = remove_dups(hairSystem)

    for hs in hairSystem:
        cmd.select(hs)
        fixPFXhairScaleResample()

    # CHECK IF SELECTED CURVES HAS ALREADY A CONNECTION TO A CACHE WRITER OR A CACHE RADER
    # ----------------------------------------------------------------------------------------------------------------------------------
    check_CAC_READER_connection = []
    check_CAC_WRITER_connection = []
    for sel in selCurvesShapes:
        try:
            check_CAC_READER_connection = cmd.listConnections(sel, destination=0, source=1, shapes=1, type='curveFromSurfaceIso')
            check_CAC_WRITER_connection = cmd.listConnections(sel, destination=0, source=1, shapes=1, type='follicle')
        except:
            pass

    if check_CAC_READER_connection is not None:
        return mel.eval('warning "SELECTED CURVES ARE ALREADY CONNECTED TO A CACHE READER NODE"')

    if check_CAC_WRITER_connection is not None:
        return mel.eval('warning "SELECTED CURVES ARE CURRENT CURVES... ...SELECT START CURVES OR CLEAN CURVES"')
    # ----------------------------------------------------------------------------------------------------------------------------------

    # CHECK IS CURVES ARE CONNECTED TO AN HAIRSYSTEM
    # IF CONNECTED, WILL BE GENERATED BOTH CAC_WRITER AND CAC_READER, OTHERWISE ONLY CAC_READER WILL BE GENERATED
    # ----------------------------------------------------------------------------------------------------------------------------------
    hairSystem = 0
    howManyConnections = 0
    howManyCurves = len(selCurvesShapes)
    for curve in selCurvesShapes:
        lookForFollicle = cmd.listConnections(curve, type='follicle')
        if lookForFollicle is not None:
            howManyConnections += 1
        else:
            api.log.logger().debug('curve without connection: {}'.format(curve))

    if howManyConnections != 0 and howManyCurves != howManyConnections:
        return mel.eval('warning "SOME CURVE HAS NOT A RELATED DYNAMIC CURVE... ...CHECK HAIRSYSTEM"')

    if howManyConnections == 0:
        hairSystem = 0
        api.log.logger().debug('THERE ARE NO HAIRSYSTEMS CONNECTIONS...')
    else:
        if howManyConnections == howManyCurves:
            hairSystem = 1
            api.log.logger().debug('HAIRSYSTEM CONNECTION PASSED...')
    # ----------------------------------------------------------------------------------------------------------------------------------

    cmd.makeIdentity(selCurves, apply=True, translate=1, rotate=1, scale=1, normal=0)
    cmd.delete(selCurves, constructionHistory=1)

    noRebuildLoftGen = cmd.loft(selCurvesShapes, constructionHistory=1, uniform=1, close=1, autoReverse=0, degree=degreeValue, sectionSpans=1, range=0, polygon=0, reverseSurfaceNormals=True)

    if degreeValue == 1:
        loftGen = cmd.rebuildSurface(noRebuildLoftGen, constructionHistory=1, replaceOriginal=1, rebuildType=0, endKnots=1, keepRange=2, keepControlPoints=1, keepCorners=0, spansU=0, degreeU=1, spansV=0, degreeV=1)
    else:
        loftGen = noRebuildLoftGen

    cmd.delete(constructionHistory=1)
    numOfV = cmd.getAttr(".".join([loftGen[0], "minMaxRangeV"]))
    Vnum = numOfV[0][1]
    curveFrom = cmd.select(clear=True)
    delCurve = cmd.select(clear=True)
    curveFrom = []
    delCurve = []
    for a in range(0, int(Vnum) + 1):
        name = '{}.v[{}]'.format(loftGen[0], a)
        extractedCurve = cmd.duplicateCurve(name, constructionHistory=1, range=0, local=0)
        curveFrom.append(extractedCurve[1])
        delCurve.append(extractedCurve[0])

    for i in range(len(selCurvesShapes)):
        fromA = ".".join([curveFrom[i], "outputCurve"])
        toA = ".".join([selCurvesShapes[i], "create"])
        cmd.connectAttr(fromA, toA)
    cmd.delete(delCurve)
    for sel in cmd.listRelatives(selCurves, parent=1):
        cmd.setAttr(sel + '.inheritsTransform', 0)
        cmd.setAttr(sel + '.translateX', 0)
        cmd.setAttr(sel + '.translateY', 0)
        cmd.setAttr(sel + '.translateZ', 0)
        cmd.setAttr(sel + '.rotateX', 0)
        cmd.setAttr(sel + '.rotateY', 0)
        cmd.setAttr(sel + '.rotateZ', 0)
        cmd.setAttr(sel + '.scaleX', 1)
        cmd.setAttr(sel + '.scaleY', 1)
        cmd.setAttr(sel + '.scaleZ', 1)
        pass

    if hairSystem == 1:
        cmd.select(selCurves)
        mel.eval('convertHairSelection "current"')
        currentCurves = cmd.ls(selection=1)
        loftGen2 = cmd.loft(currentCurves, constructionHistory=1, uniform=1, close=1, autoReverse=0, degree=degreeValue, sectionSpans=1, range=0, polygon=0, reverseSurfaceNormals=True)

    CAC_READER = cmd.ls('GRP_CAC_READER')
    if CAC_READER is None or CAC_READER == []:
        cmd.select(clear=1)
        CAC_READER = cmd.group(empty=1, name='GRP_CAC_READER')
    cmd.parent(loftGen[0], CAC_READER)
    cmd.rename('CAC_001')
    CURRENT_CAC_READER = cmd.ls(selection=1)

    if hairSystem == 1:
        CAC_WRITER = cmd.ls('GRP_CAC_WRITER')
        if CAC_WRITER is None or CAC_WRITER == []:
            cmd.select(clear=1)
            CAC_WRITER = cmd.group(empty=1, name='GRP_CAC_WRITER')
        cmd.parent(loftGen2[0], CAC_WRITER)
        cmd.rename(CURRENT_CAC_READER)

    else:
        pass

    if degreeValue == 1:
        mel.eval("displayHairCurves \"start\" 1;")
        for sel in selCurves:
            cmd.rebuildCurve(sel, constructionHistory=1, replaceOriginal=1, rebuildType=0, endKnots=1, keepRange=0, keepControlPoints=1, keepEndPoints=1, keepTangents=0, spans=4, degree=3, tolerance=0.01)
        rbwUI.RBWConfirm(title='Attenzione', text='La generazione di una superficie di grado 1\npuo\' introdurre modifiche alle curve generatrici\nPer sicurezza effettuare un controllo', defCancel=None)

    api.log.logger().debug("DONE!")


def fixPFXhairScaleResample():
    '''
    # ------------------------------------------------------------------------
    # THIS SCRIPT FIXES CLUMP WIDTH AND HAIR WIDTH ATTRIBUTES
    # ON RIGGED HAIRSYSTEMS, GIVING THE POSSIBILITY TO SCALE THEM
    # AND RESAMPLING THE PAINT EFFECTS RESULTS.
    # ------------------------------------------------------------------------
    # USAGE:
    # SELECT HAIRSYSTEM NODES AND RUN: fixHairDressPFXscale()
    # ------------------------------------------------------------------------
    '''
    hairSystemShape = cmd.ls(selection=True, dagObjects=1, type='hairSystem')
    if hairSystemShape is None or hairSystemShape == []:
        return mel.eval('warning "No HairSystem Selected"')
    # print 'hairSystemShape ===> ', hairSystemShape

    mel.eval('convertHairSelection "startCurves"')
    startCurves = cmd.ls(selection=True, dagObjects=1, type='nurbsCurve')
    if startCurves is None or startCurves == [] or len(startCurves) == 0:
        return mel.eval('warning "No start curves connection identified during script... ...please check"')
    # print 'startCurves ===> ', startCurves

    curveInfoNode = cmd.createNode('curveInfo', name='curveLen_001_' + hairSystemShape[0])
    # print 'curveInfoNode ===> ', curveInfoNode

    longestCurve = []
    lengthArray = []
    for curve in startCurves:
        cmd.connectAttr(curve + '.worldSpace', curveInfoNode + '.inputCurve', force=1)
        lengthArray.append(cmd.getAttr(curveInfoNode + '.arcLength'))

    lengthArray.sort()
    cmd.delete(curveInfoNode)
    # print 'lengthArray ===> ', lengthArray
    # print 'longest lengthArray ===> ', lengthArray[-1]

    curveInfoNode = cmd.createNode('curveInfo', name='curveLen_001_' + hairSystemShape[0])
    for curve in startCurves:
        cmd.connectAttr(curve + '.worldSpace', curveInfoNode + '.inputCurve', force=1)
        if cmd.getAttr(curveInfoNode + '.arcLength') == lengthArray[-1]:
            longestCurve.append(curve)
            break

    # print 'longestCurve ===> ', longestCurve

    setRange = cmd.createNode('setRange', name='setRange_' + hairSystemShape[0])
    cmd.connectAttr(curveInfoNode + '.arcLength', setRange + '.valueX')
    cmd.setAttr(setRange + '.minX', 0)
    cmd.setAttr(setRange + '.maxX', (1 * 100000))
    cmd.setAttr(setRange + '.oldMinX', 0)
    cmd.setAttr(setRange + '.oldMaxX', (lengthArray[-1] * 100000))

    cmd.addAttr(setRange, longName="resultX", attributeType='double')
    cmd.setAttr(setRange + '.resultX', edit=1, keyable=1)
    cmd.connectAttr(setRange + '.outValueX', setRange + '.resultX', force=1)

    cmd.setAttr(setRange + '.vx', lock=1)
    cmd.setAttr(setRange + '.vy', lock=1)
    cmd.setAttr(setRange + '.vz', lock=1)
    cmd.setAttr(setRange + '.nx', lock=1)
    cmd.setAttr(setRange + '.ny', lock=1)
    cmd.setAttr(setRange + '.nz', lock=1)
    cmd.setAttr(setRange + '.mx', lock=1)
    cmd.setAttr(setRange + '.my', lock=1)
    cmd.setAttr(setRange + '.mz', lock=1)
    cmd.setAttr(setRange + '.onx', lock=1)
    cmd.setAttr(setRange + '.ony', lock=1)
    cmd.setAttr(setRange + '.onz', lock=1)
    cmd.setAttr(setRange + '.omx', lock=1)
    cmd.setAttr(setRange + '.omy', lock=1)
    cmd.setAttr(setRange + '.omz', lock=1)
    cmd.setAttr(setRange + '.resultX', lock=1)

    hairSystemExtraScaleAttributes = ['clumpWidth', 'hairWidth']
    for attr in hairSystemExtraScaleAttributes:
        currentMD = cmd.createNode('multiplyDivide', name='MD_' + attr)
        # print 'currentMD ===> ', currentMD
        cmd.setAttr(currentMD + '.input1X', cmd.getAttr(hairSystemShape[0] + '.' + attr))
        cmd.connectAttr(setRange + '.resultX', currentMD + '.input2X', force=1)
        cmd.setAttr(currentMD + '.input1X', lock=1)
        cmd.setAttr(currentMD + '.input2X', lock=1)
        cmd.setAttr(currentMD + '.input1Y', lock=1)
        cmd.setAttr(currentMD + '.input2Y', lock=1)
        cmd.setAttr(currentMD + '.input1Y', lock=1)
        cmd.setAttr(currentMD + '.input1Z', lock=1)
        cmd.setAttr(currentMD + '.input2Z', lock=1)
        cmd.setAttr(currentMD + '.operation', lock=1)
        cmd.connectAttr(currentMD + '.outputX', hairSystemShape[0] + '.' + attr, force=1)


def remove_dups(seq):
    x = {}
    for y in seq:
        x[y] = 1
    u = x.keys()
    return u
