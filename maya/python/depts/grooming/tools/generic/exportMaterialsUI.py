import importlib
import os
from PySide2 import QtWidgets, QtCore


import maya.cmds as cmd

import api.log
import api.xgen
import api.widgets.rbw_UI as rbwUI
import depts.grooming.tools.generic.exportMaterials as exportMaterials

importlib.reload(api.log)
importlib.reload(api.xgen)
# importlib.reload(rbwUI)
importlib.reload(exportMaterials)


################################################################################
# @brief      This class describes an export materials ui.
#
class ExportMaterialsUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(ExportMaterialsUI, self).__init__()
        if cmd.window('ExportMaterialsUI', exists=True):
            cmd.deleteUI('ExportMaterialsUI')

        self.exportScalp = False
        self.exportDynamicGuideAndDynamicFiber = False
        self.exportInnerGuideAndInnerFiber = False
        self.exportClusterGuideAndClusterFiber = False

        self.setObjectName('ExportMaterialsUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        ########################################################################
        # BOTTOM WIDGET
        #
        self.topWidget = rbwUI.RBWFrame(layout='G', radius=5, spacing=3)

        # dyn descriptions
        self.dynDescriptionsGroup = rbwUI.RBWGroupBox(title='Dyn Descriptions', layout='V', fontSize=12)
        self.topWidget.layout.addWidget(self.dynDescriptionsGroup, 0, 0)

        # ing description
        self.ingDescriptionsGroup = rbwUI.RBWGroupBox(title='Ing Descriptions', layout='V', fontSize=12)
        self.topWidget.layout.addWidget(self.ingDescriptionsGroup, 0, 1)

        # clg description
        self.clgDescriptionsGroup = rbwUI.RBWGroupBox(title='Clg Descriptions', layout='V', fontSize=12)
        self.topWidget.layout.addWidget(self.clgDescriptionsGroup, 0, 2)

        # scalps
        self.scalpsGroup = rbwUI.RBWGroupBox(title='Scalps', layout='V', fontSize=12)
        self.topWidget.layout.addWidget(self.scalpsGroup, 1, 0, 1, 3)

        self.mainLayout.addWidget(self.topWidget)

        ########################################################################
        # BOTTOM WIDGET
        #
        self.bottomWidget = rbwUI.RBWFrame(layout='V', radius=5)

        # components
        self.componentsGroup = rbwUI.RBWGroupBox(title='Components', layout='V', fontSize=12)

        self.exportScalpCheckBox = rbwUI.RBWCheckBox(text='Scalp')
        self.exportScalpCheckBox.switch.stateChanged.connect(self.toggleScalp)

        self.exportDynamicGuideAndDynamicFiberCheckBox = rbwUI.RBWCheckBox(text='Dynamic Guide + Dynamic Fiber')
        self.exportDynamicGuideAndDynamicFiberCheckBox.switch.stateChanged.connect(self.toggleDynamicGuideAndDynamicFiber)

        self.exportInnerGuidesAndInnerFibersCheckBox = rbwUI.RBWCheckBox(text='Inner Fibers')
        self.exportInnerGuidesAndInnerFibersCheckBox.switch.stateChanged.connect(self.toggleInnerGuideAndInnerFiber)

        self.exportClusterGuidesAndClusterFibersCheckBox = rbwUI.RBWCheckBox(text='Cluster Guides + Cluster Fibers')
        self.exportClusterGuidesAndClusterFibersCheckBox.switch.stateChanged.connect(self.toggleClusterGuideAndClusterFiber)

        self.componentsGroup.addWidget(self.exportScalpCheckBox)
        self.componentsGroup.addWidget(self.exportDynamicGuideAndDynamicFiberCheckBox)
        self.componentsGroup.addWidget(self.exportInnerGuidesAndInnerFibersCheckBox)
        self.componentsGroup.addWidget(self.exportClusterGuidesAndClusterFibersCheckBox)

        self.bottomWidget.addWidget(self.componentsGroup)

        # export button
        self.exportButton = rbwUI.RBWButton(text='Export', size=[478, 35], icon=['export.png'])
        self.exportButton.clicked.connect(self.export)

        self.bottomWidget.addWidget(self.exportButton)

        self.mainLayout.addWidget(self.bottomWidget)

        self.fillInfoGroups()

        self.setMinimumSize(500, 300)

        self.setTitle('Export Grooming Materials UI')
        self.setFocus()

    ############################################################################
    # @brief      fill info groups.
    #
    def fillInfoGroups(self):
        self.fillDynDescriptionGroup()
        self.fillIngDescriptionGroup()
        self.fillClgDescriptionGroup()
        self.fillScalpGroup()

    ############################################################################
    # @brief      fill scapl group.
    #
    def fillScalpGroup(self):
        scalps = []
        currentCollection = api.xgen.currentCollection()
        if currentCollection != '':
            groomingGroup = cmd.listRelatives(currentCollection, parent=True)[0]
            for description in api.xgen.listDescription(currentCollection):
                if description.split('_')[2] in ['DYN', 'ING', 'CLG']:
                    scalps.append(api.xgen.getScalp(description, groomingGroup))

        scalps = set(scalps)

        for scalp in scalps:
            self.scalpsGroup.addWidget(QtWidgets.QLabel(scalp))

    ############################################################################
    # @brief      fill dyn description group.
    #
    def fillDynDescriptionGroup(self):
        counter = 0
        currentCollection = api.xgen.currentCollection()
        if currentCollection != '':
            for description in api.xgen.listDescription(currentCollection):
                if description.split('_')[2] == 'DYN':
                    counter += 1
                    self.dynDescriptionsGroup.addWidget(QtWidgets.QLabel(description))

        if counter == 0:
            self.exportDynamicGuideAndDynamicFiberCheckBox.hide()
            self.dynDescriptionsGroup.hide()
        else:
            self.exportDynamicGuideAndDynamicFiberCheckBox.show()
            self.dynDescriptionsGroup.show()

    ############################################################################
    # @brief      fill ing description group.
    #
    def fillIngDescriptionGroup(self):
        counter = 0
        currentCollection = api.xgen.currentCollection()
        if currentCollection != '':
            for description in sorted(api.xgen.listDescription(currentCollection)):
                if description.split('_')[2] == 'ING':
                    counter += 1
                    self.ingDescriptionsGroup.addWidget(QtWidgets.QLabel(description))

        if counter == 0:
            self.exportInnerGuidesAndInnerFibersCheckBox.hide()
            self.ingDescriptionsGroup.hide()
        else:
            self.exportInnerGuidesAndInnerFibersCheckBox.show()
            self.ingDescriptionsGroup.show()

    ############################################################################
    # @brief      fill clg description group.
    #
    def fillClgDescriptionGroup(self):
        counter = 0
        currentCollection = api.xgen.currentCollection()
        if currentCollection != '':
            for description in sorted(api.xgen.listDescription(currentCollection)):
                if description.split('_')[2] == 'CLG':
                    counter += 1
                    self.clgDescriptionsGroup.addWidget(QtWidgets.QLabel(description))

        if counter == 0:
            self.exportClusterGuidesAndClusterFibersCheckBox.hide()
            self.clgDescriptionsGroup.hide()
        else:
            self.exportClusterGuidesAndClusterFibersCheckBox.show()
            self.clgDescriptionsGroup.show()

    ############################################################################
    # @brief      toggle scalp checkbox.
    #
    def toggleScalp(self):
        wantedState = self.exportScalpCheckBox.isChecked()
        self.exportScalp = wantedState

        api.log.logger().debug('export scalp: {}'.format(wantedState))

    ############################################################################
    # @brief      toggle guide checkbox.
    #
    def toggleDynamicGuideAndDynamicFiber(self):
        wantedState = self.exportDynamicGuideAndDynamicFiberCheckBox.isChecked()
        self.exportDynamicGuideAndDynamicFiber = wantedState

        api.log.logger().debug('export dynamic guide and fiber: {}'.format(wantedState))

    ############################################################################
    # @brief      toggle inner checkbox.
    #
    def toggleInnerGuideAndInnerFiber(self):
        wantedState = self.exportInnerGuidesAndInnerFibersCheckBox.isChecked()
        self.exportInnerGuideAndInnerFiber = wantedState

        api.log.logger().debug('export inner guide and inner fiber: {}'.format(wantedState))

    ############################################################################
    # @brief      toggle cluster guide checkbox.
    #
    def toggleClusterGuideAndClusterFiber(self):
        wantedState = self.exportClusterGuidesAndClusterFibersCheckBox.isChecked()
        self.exportClusterGuideAndClusterFiber = wantedState

        api.log.logger().debug('export cluster guide and cluster fiber: {}'.format(wantedState))

    ############################################################################
    # @brief      export function.
    #
    def export(self):
        if self.exportScalp:
            exportMaterials.exportScalps()

        if self.exportDynamicGuideAndDynamicFiber:
            exportMaterials.exportCurveFromGuides()
            exportMaterials.exportFibers()

        if self.exportInnerGuideAndInnerFiber:
            exportMaterials.exportFibers(fiberType='innerFibers')

        if self.exportClusterGuideAndClusterFiber:
            exportMaterials.exportCurveFromGuides(guideType='clusterGuides')
            exportMaterials.exportFibers(fiberType='clusterFibers')
