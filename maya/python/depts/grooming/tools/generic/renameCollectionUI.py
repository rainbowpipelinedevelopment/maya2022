import importlib
import os
from PySide2 import QtWidgets, QtCore

import maya.cmds as cmd

import api.log
import api.xgen
import api.widgets.rbw_UI as rbwUI
import depts.grooming.tools.generic.renameCollection as renameCollection

importlib.reload(api.log)
importlib.reload(api.xgen)
# importlib.reload(rbwUI)
importlib.reload(renameCollection)


################################################################################
# @brief      This class describes a rename collection ui.
#
class RenameCollectionUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(RenameCollectionUI, self).__init__()
        if cmd.window('RenameCollectionUI', exists=True):
            cmd.deleteUI('RenameCollectionUI')

        self.setObjectName('RenameCollectionUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topWidget = rbwUI.RBWFrame(layout='V', radius=5)

        # name group
        self.newNameGroup = rbwUI.RBWGroupBox(title='Please Insert New Collection Name', layout='H', fontSize=12)

        self.nameLine = rbwUI.RBWLineEdit(title='Name:', bgColor='transparent', stretch=False)
        self.nameLine.edit.setPlaceholderText('ex: Pippo')

        self.newNameGroup.addWidget(self.nameLine)

        self.topWidget.addWidget(self.newNameGroup)

        # description tree widget
        self.descriptionsTree = rbwUI.RBWTreeWidget(fields=['Description', 'DYN', 'ING', 'CLG'], size=[250, 70, 70, 70])
        self.descriptionsTree.header().setDefaultAlignment(QtCore.Qt.AlignCenter)

        self.topWidget.addWidget(self.descriptionsTree)

        # button layout
        self.finalButtonLayout = QtWidgets.QHBoxLayout()

        self.okButton = rbwUI.RBWButton(text='OK')
        self.cancelButton = rbwUI.RBWButton(text='Cancel')

        self.finalButtonLayout.addWidget(self.okButton)
        self.finalButtonLayout.addWidget(self.cancelButton)

        self.okButton.clicked.connect(self.rename)
        self.cancelButton.clicked.connect(self.close)

        self.topWidget.addLayout(self.finalButtonLayout)

        self.mainLayout.addWidget(self.topWidget)

        self.fillDescriptionsTree()

        self.setMinimumSize(500, 300)

        self.setTitle('Rename Collection UI')
        self.setFocus()

    ############################################################################
    # @brief      fill descriptions tree.
    #
    def fillDescriptionsTree(self):
        descriptions = list(api.xgen.listDescription(api.xgen.currentCollection()))

        def myOrder(input):
            return input.split('_')[-1].split('des')[1]

        descriptions.sort(key=myOrder)

        for description in descriptions:
            DescriptionsTreeWidgetItem(description, self.descriptionsTree)

    def rename(self):
        newName = self.nameLine.text()
        descs = {'DYN': [], 'ING': [], 'CLG': [], 'ST': []}

        if newName == '':
            rbwUI.RBWError(title='ATTENTION', text='Please insert something and try again.')
            return

        elif len(newName.split('_')) > 1:
            rbwUI.RBWError(title='ATTENTION', text='Please no "_" in name.')
            return

        else:
            root = self.descriptionsTree.invisibleRootItem()
            for i in range(0, root.childCount()):
                item = root.child(i)
                descs[item.getType()].append(item.getDescription())

        renameCollection.renameCollection(newName=newName, descs=descs)
        self.close()

################################################################################
# @brief      This class describes a descriptions tree widget item.
#
class DescriptionsTreeWidgetItem(QtWidgets.QTreeWidgetItem):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      description  The description
    # @param      parent       The parent
    #
    def __init__(self, description, parent=None):
        super(DescriptionsTreeWidgetItem, self).__init__(parent)

        self.description = description

        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.descriptionLabel = QtWidgets.QLabel(self.description, parent=self.treeWidget())
        self.treeWidget().setItemWidget(self, 0, self.descriptionLabel)

        self.dynSwitchWidget = QtWidgets.QWidget()
        self.dynSwitchLayout = QtWidgets.QHBoxLayout()
        self.dynSwitchLayout.setAlignment(QtCore.Qt.AlignCenter)

        self.dynSwitch = rbwUI.RBWSwitch()
        self.dynSwitch.toggled.connect(self.toggleDyn)

        self.dynSwitchLayout.addWidget(self.dynSwitch)
        self.dynSwitchWidget.setLayout(self.dynSwitchLayout)

        self.treeWidget().setItemWidget(self, 1, self.dynSwitchWidget)

        self.ingSwitchWidget = QtWidgets.QWidget()
        self.ingSwitchLayout = QtWidgets.QHBoxLayout()
        self.ingSwitchLayout.setAlignment(QtCore.Qt.AlignCenter)

        self.ingSwitch = rbwUI.RBWSwitch()
        self.ingSwitch.toggled.connect(self.toggleIng)

        self.ingSwitchLayout.addWidget(self.ingSwitch)
        self.ingSwitchWidget.setLayout(self.ingSwitchLayout)

        self.treeWidget().setItemWidget(self, 2, self.ingSwitchWidget)

        self.clgSwitchWidget = QtWidgets.QWidget()
        self.clgSwitchLayout = QtWidgets.QHBoxLayout()
        self.clgSwitchLayout.setAlignment(QtCore.Qt.AlignCenter)

        self.clgSwitch = rbwUI.RBWSwitch()
        self.clgSwitch.toggled.connect(self.toggleClg)

        self.clgSwitchLayout.addWidget(self.clgSwitch)
        self.clgSwitchWidget.setLayout(self.clgSwitchLayout)

        self.treeWidget().setItemWidget(self, 3, self.clgSwitchWidget)

        if self.description.split('_')[2] == 'DYN':
            self.dynSwitch.setChecked(True)
        elif self.description.split('_')[2] == 'ING':
            self.ingSwitch.setChecked(True)
        elif self.description.split('_')[2] == 'CLG':
            self.clgSwitch.setChecked(True)

    ############################################################################
    # @brief      toggle dyn checkbox function.
    #
    def toggleDyn(self):
        wantedState = self.dynSwitch.isChecked()

        if wantedState:
            self.ingSwitch.setChecked(False)
            self.clgSwitch.setChecked(False)

    ############################################################################
    # @brief      toggle ing checkbox function.
    #
    def toggleIng(self):
        wantedState = self.ingSwitch.isChecked()

        if wantedState:
            self.dynSwitch.setChecked(False)
            self.clgSwitch.setChecked(False)

    ############################################################################
    # @brief      toggle clg checkbox function.
    #
    def toggleClg(self):
        wantedState = self.clgSwitch.isChecked()

        if wantedState:
            self.dynSwitch.setChecked(False)
            self.ingSwitch.setChecked(False)

    ############################################################################
    # @brief      Gets the description.
    #
    # @return     The description.
    #
    def getDescription(self):
        return self.description.split('_')[-1].split('des')[1]

    ############################################################################
    # @brief      Gets the type.
    #
    # @return     The type.
    #
    def getType(self):
        if self.dynSwitch.isChecked():
            return 'DYN'
        elif self.ingSwitch.isChecked():
            return 'ING'
        elif self.clgSwitch.isChecked():
            return 'CLG'
        else:
            return 'ST'
