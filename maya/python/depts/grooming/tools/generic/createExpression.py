import importlib
from PySide2 import QtWidgets, QtCore

import maya.cmds as cmd
import os

import api.xgen
import api.log
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.xgen)
importlib.reload(api.log)
# importlib.reload(rbwUI)


################################################################################
# @brief      This class describes a create expression ui.
#
class CreateExpressionUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(CreateExpressionUI, self).__init__()
        if cmd.window('CreateExpressionUI', exists=True):
            cmd.deleteUI('CreateExpressionUI')

        self.setObjectName('CreateExpressionUI')
        self.initUI()
        self.updateMargins()

    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topWidget = rbwUI.RBWFrame(layout='V', radius=5)

        self.optionsGroup = rbwUI.RBWGroupBox(title='Options:', layout='V', fontSize=12)

        # expression name
        self.insertExpressionNameLine = rbwUI.RBWLineEdit(title='Expression Name:', bgColor='transparent')

        self.optionsGroup.addWidget(self.insertExpressionNameLine)

        # expression type
        self.expressionTypeCombo = rbwUI.RBWComboBox(text='Expression Type:', bgColor='transparent')
        self.expressionTypeCombo.activated.connect(self.setExpressionType)

        self.optionsGroup.addWidget(self.expressionTypeCombo)

        self.includeGammaCheckBox = rbwUI.RBWCheckBox(text='Include Gamma')
        self.optionsGroup.addWidget(self.includeGammaCheckBox)

        # insert float value
        self.insertFloatLine = rbwUI.RBWLineEdit(title='Insert Float Value:', bgColor='transparent')

        self.optionsGroup.addWidget(self.insertFloatLine)

        # select Map
        self.selectMapCombo = rbwUI.RBWComboBox(text='Select a Map:', bgColor='transparent')

        self.optionsGroup.addWidget(self.selectMapCombo)

        self.topWidget.addWidget(self.optionsGroup)

        # create
        self.createButton = rbwUI.RBWButton(text='Create', icon=['add.png'], size=[378, 35])
        self.createButton.clicked.connect(self.createExpression)

        self.topWidget.addWidget(self.createButton)
        self.mainLayout.addWidget(self.topWidget)

        self.resetUI()
        self.setExpressionType()

        self.setMinimumSize(400, 220)

        self.setTitle('Create Expression UI')
        self.setFocus()

    def resetUI(self):
        self.insertExpressionNameLine.setText('')

        self.insertFloatLine.hide()

        self.selectMapCombo.hide()
        self.includeGammaCheckBox.hide()

        self.expressionTypeCombo.clear()
        self.expressionTypeCombo.addItems(['color', 'float'])
        self.expressionTypeCombo.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

    def setExpressionType(self):
        selectedType = self.expressionTypeCombo.currentText()

        if selectedType == 'color':
            self.expressionType = 'color'

            self.selectMapCombo.show()
            self.includeGammaCheckBox.show()

            self.insertFloatLine.hide()

            self.fillMapCombo()

        elif selectedType == 'float':
            self.expressionType = 'float'

            self.insertFloatLine.show()

            self.selectMapCombo.hide()
            self.includeGammaCheckBox.hide()

    def getCurrentMaps(self):
        descriptionFolder = api.xgen.descriptionPath()
        ptexFolder = os.path.join(descriptionFolder, '..', 'ptex').replace('\\', '/')

        ptexes = []
        for file in os.listdir(ptexFolder):
            if file.endswith('.ptx'):
                ptexes.append(file)

        return ptexes

    def fillMapCombo(self):
        ptexes = self.getCurrentMaps()

        self.selectMapCombo.clear()
        self.selectMapCombo.addItems(ptexes)
        self.selectMapCombo.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

    def createExpression(self):
        self.expressionName = "custom_{}_{}".format(self.expressionType, self.insertExpressionNameLine.text())

        if self.expressionType == 'color':
            if self.includeGammaCheckBox.isChecked():
                self.expressionText = "gamma(map(\"${{DESC}}/../ptex/{}\"), 0.455)".format(self.selectMapCombo.currentText())
            else:
                self.expressionText = "map(\"${{DESC}}/../ptex/{}\")".format(self.selectMapCombo.currentText())
        else:
            self.expressionText = str(self.insertFloatLine.text())

        currentCollection = api.xgen.currentCollection()

        api.xgen.createExpression(self.expressionName, self.expressionText, currentCollection)

        self.resetUI()
