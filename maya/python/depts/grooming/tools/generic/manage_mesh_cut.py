import importlib
import os
from PySide2 import QtGui, QtWidgets, QtCore

import maya.cmds as cmd

import xgenm
import xgenm.xgGlobal as xgGlobal
import xgenm.XgExternalAPI as xgext

import api.xgen
import api.log
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.xgen)
importlib.reload(api.log)
# importlib.reload(rbwUI)


class ManageXGenMeshCutUI(rbwUI.RBWWindow):

    ###############################################################################
    # Constructs a new instance.
    #
    def __init__(self):
        super(ManageXGenMeshCutUI, self).__init__()

        if cmd.window('MeshCutUI', exists=True):
            cmd.deleteUI('MeshCutUI')

        self.setObjectName('MeshCutUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topWidget = rbwUI.RBWFrame(layout='G', radius=5)

        self.abcNameComboBox = rbwUI.RBWComboBox(text='Alembic Name:', bgColor='transparent')

        self.makeMeshCutButton = rbwUI.RBWButton(text='Make MeshCuts')
        self.makeMeshCutButton.clicked.connect(self.createMeshCuts)

        self.deleteMeshCutButton = rbwUI.RBWButton(text='Delete MeshCuts')
        self.deleteMeshCutButton.clicked.connect(self.deleteMeshCuts)

        self.cutTypeCombo = rbwUI.RBWComboBox(text='Cut Type:', bgColor='transparent')
        self.cutTypeCombo.addItem('Keep Param')
        self.cutTypeCombo.addItem('Reparam')
        self.cutTypeCombo.addItem('Cull')

        self.topWidget.layout.addWidget(self.abcNameComboBox, 0, 0)
        self.topWidget.layout.addWidget(self.makeMeshCutButton, 0, 1)
        self.topWidget.layout.addWidget(self.cutTypeCombo, 1, 0)
        self.topWidget.layout.addWidget(self.deleteMeshCutButton, 1, 1)
        self.mainLayout.addWidget(self.topWidget)

        self.fillAbcNameCombo()

        self.setMinimumSize(360, 200)
        self.setTitle('Manage MeshCut')
        self.setFocus()

    ############################################################################
    # @brief      fill the abc name combobox.
    #
    def fillAbcNameCombo(self):
        self.abcNameComboBox.clear()
        descriptionFolder = api.xgen.descriptionPath()
        meshcutFolder = os.path.join(descriptionFolder, '..', 'meshcut').replace('\\', '/')

        for file in os.listdir(meshcutFolder):
            if file.endswith('.abc'):
                self.abcNameComboBox.addItem(file)

    ############################################################################
    # Creates meshCuts.
    #
    def createMeshCuts(self):
        abcName = self.abcNameComboBox.currentText()

        api.log.logger().debug('Alembic file found at the given path, starting meshcut creation procedure...')

        # get the Xgen group and list all descriptions
        xgenPalettes = api.xgen.listCollection()
        xgenDescriptions = sorted(api.xgen.listDescription(xgenPalettes[0]))

        # attach meshcut to all descriptions
        self.attachMeshcutToGrooming(abcName)

        for pal in xgenPalettes:
            for desc in xgenDescriptions:
                for mod in xgenm.fxModules(pal, desc):
                    if xgenm.fxModuleType(pal, desc, mod) == 'MeshCutFXModule' and 'mesh_cut_vrscene' in mod:
                        xgenm.setAttr("cutType", str(self.cutTypeCombo.currentIndex()), xgenPalettes[0], desc, mod)

        xgGlobal.DescriptionEditor.refresh("Full")
        api.log.logger().debug('Meshcuts created!')

        infoMex = 'MeshCuts created!'
        rbwUI.RBWConfirm(title='Make MeshCuts Procedure', text=infoMex, defCancel=None)

    ############################################################################
    # Delete meshCuts (only the ones created by the createMeshCuts procedure).
    #
    def deleteMeshCuts(self):
        # get the Xgen group and list all descriptions
        xgenPalettes = api.xgen.listCollection()
        xgenDescriptions = sorted(api.xgen.listDescription(xgenPalettes[0]))

        for pal in xgenPalettes:
            for desc in xgenDescriptions:
                for mod in xgenm.fxModules(pal, desc):
                    if xgenm.fxModuleType(pal, desc, mod) == 'MeshCutFXModule' and 'mesh_cut_vrscene' in mod:
                        xgenm.removeFXModule(pal, desc, mod)

        xgGlobal.DescriptionEditor.refresh("Full")
        api.log.logger().debug('All meshcuts deleted!')

        infoMex = 'MeshCuts deleted!'
        rbwUI.RBWConfirm(title='Delete MeshCuts Procedure', text=infoMex, defCancel=None)

    ############################################################################
    # @brief      Attaches the meshcut to grooming.
    #
    # @param      meshcutName  The meshcut name
    # @param      cullMode     The cull mode
    #
    def attachMeshcutToGrooming(self, meshcutName):
        api.log.logger().debug("-" * 60)
        api.log.logger().debug("attaching meshcut {} to grooming".format(meshcutName))
        api.log.logger().debug("-" * 60)

        # converting meshcut path to UNIX style and CONVERTING TO STRING
        # (meshcutPath type is unicode but xgen doesn't like it!)
        meshcutPath = "${{DESC}}/../meshcut/{}".format(meshcutName)

        palettes = api.xgen.listCollection()

        descriptions = api.xgen.listDescription(palettes[0])

        for description in descriptions:
            mesh_cut_fx = xgenm.addFXModule(palettes[0], description, "MeshCutFXModule", "mesh_cut_vrscene")

            xgenm.setAttr("cutType", xgext.prepForAttribute("0"), palettes[0], description, mesh_cut_fx)

            xgenm.setAttr("meshFile", meshcutPath, palettes[0], description, mesh_cut_fx)

            xgenm.setAttr("cullFlag", xgext.prepForAttribute("False"), palettes[0], description, "RandomGenerator")

        de = xgGlobal.DescriptionEditor
        de.refresh("Full")
