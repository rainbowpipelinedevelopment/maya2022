import importlib
import os
import shutil

import maya.cmds as cmd

import api.scene
import api.xgen

importlib.reload(api.scene)
importlib.reload(api.xgen)


################################################################################
# @brief      Creates a new collection folder.
#
# @param      oldCollectionName  The old collection name
# @param      newCollectionName  The new collection name
# @param      versioning         The versioning
#
# @return     new description name list
#
def createNewCollectionFolder(oldCollectionName, newCollectionName, versioning=False, descs={}):
    newDescs = []
    currentWorkspace = cmd.workspace(query=True, fullName=True)

    oldCollectionDir = os.path.join(currentWorkspace, 'xgen', 'collections', oldCollectionName).replace('\\', '/')
    newCollectionDir = os.path.join(currentWorkspace, 'xgen', 'collections', newCollectionName).replace('\\', '/')

    if not os.path.exists(newCollectionDir):
        os.makedirs(newCollectionDir)

    for oldDirName in os.listdir(oldCollectionDir):
        oldDir = os.path.join(oldCollectionDir, oldDirName).replace('\\', '/')
        if os.path.isdir(oldDir):
            oldDescInfo = oldDirName.split('_')
            if len(oldDescInfo) > 1:
                newDescInfo = oldDescInfo[:]
                newDescInfo[1] = newCollectionName.split('_')[1]

                if versioning:
                    if len(newDescInfo) == 6:
                        newDescInfo[4] = newCollectionName.split('_')[4]
                    else:
                        descNumber = newDescInfo[-1]
                        newDescInfo[4] = newCollectionName.split('_')[4]
                        newDescInfo.append(descNumber)

                if descs:
                    if newDescInfo[-1].split('des')[1] in descs['DYN']:
                        newDescInfo[2] = 'DYN'

                    elif newDescInfo[-1].split('des')[1] in descs['ING']:
                        newDescInfo[2] = 'ING'

                    elif newDescInfo[-1].split('des')[1] in descs['CLG']:
                        newDescInfo[2] = 'CLG'

                    else:
                        newDescInfo[2] = 'ST'
                else:
                    if newDescInfo[2] == 'CH':
                        newDescInfo[2] = 'ST'

                newDescName = '_'.join(newDescInfo)
                newDescs.append(newDescName)
                newDescDir = os.path.join(newCollectionDir, newDescName).replace('\\', '/')
            else:
                newDescDir = os.path.join(newCollectionDir, oldDirName).replace('\\', '/')

            if not os.path.exists(newDescDir):
                os.makedirs(newDescDir)

            for file in os.listdir(oldDir):
                oldFile = os.path.join(oldDir, file).replace('\\', '/')
                newFile = os.path.join(newDescDir, file).replace('\\', '/')

                if os.path.isdir(oldFile):
                    shutil.copytree(oldFile, newFile)
                else:
                    shutil.copyfile(oldFile, newFile)
    return newDescs


################################################################################
# @brief      Creates a new xgen file.
#
# @param      oldDescs           The old descs
# @param      newDescs           The new descs
# @param      oldCollectionName  The old collection name
# @param      newCollectionName  The new collection name
#
# @return     the new xgen file path.
#
def createNewXgenFile(oldDescs, newDescs, oldCollectionName, newCollectionName):
    currentSceneName = api.scene.scenename()

    oldXgenFile = currentSceneName.replace('.mb', '__{}.xgen'.format(oldCollectionName))
    newXgenFile = currentSceneName.replace('.mb', '__{}.xgen'.format(newCollectionName))

    with open(oldXgenFile, 'r') as oldXgenDataFile:
        xgenData = oldXgenDataFile.read()

    for i in range(0, len(oldDescs)):
        xgenData = xgenData.replace(oldDescs[i], newDescs[i])

    xgenData = xgenData.replace('${{PROJECT}}xgen/collections/{}/'.format(oldCollectionName), '${{PROJECT}}xgen/collections/{}/'.format(newCollectionName))
    xgenData = xgenData.replace('{}\n'.format(oldCollectionName), '{}\n'.format(newCollectionName))

    with open(newXgenFile, 'w') as newXgenDataFile:
        newXgenDataFile.write(xgenData)

    return newXgenFile


################################################################################
# @brief      rename xgen collection.
#
# @param      newName  The new name
# @param      version  The version
#
def renameCollection(newName=None, version=None, descs={}):
    # hide all guides in order to prevert crash
    for guide in cmd.ls(type='xgmSplineGuide'):
        cmd.setAttr('{}.visibility'.format(guide), False)

    # NO CULL set
    if cmd.objExists('NO_CULL'):
        cullSet = True
    else:
        cullSet = False

    # get current collection
    currentCollectionName = api.xgen.currentCollection()
    currentCollectionInfo = currentCollectionName.split('_')

    oldName = currentCollectionInfo[1]

    versioning = False
    if version:
        versioning = True

    if not newName:
        newName = oldName
        versioning = True

    descInfo = {}
    descriptions = cmd.ls(type='xgmDescription')
    if len(descriptions) > 0:
        for description in descriptions:
            shadeEng = cmd.listConnections(description, type='shadingEngine')[0]
            material = cmd.ls(cmd.listConnections(shadeEng), materials=True)[0]
            descriptionTransform = cmd.listRelatives(description, parent=True, fullPath=True)[0]
            visibility = cmd.getAttr('{}.visibility'.format(descriptionTransform))
            descInfo[description] = {'shading': shadeEng, 'material': material, 'visibility': visibility}
            if cullSet and cmd.sets(description[:-5], im='NO_CULL'):
                descInfo[description]['cull'] = False
            else:
                descInfo[description]['cull'] = True

    newCollectionName = currentCollectionName.replace(oldName, newName)

    if versioning:
        if len(currentCollectionInfo) == 5:
            currentVersionString = currentCollectionInfo[-1]
            if not version:
                currentVersion = int(currentVersionString.split('vr')[1])
                newVersionString = 'vr{}'.format(str(currentVersion + 1).zfill(3))

            else:
                newVersionString = version

            newCollectionName = newCollectionName.replace(currentVersionString, newVersionString)

        elif len(currentCollectionInfo) == 4:
            if not version:
                newCollectionName = '{}_vr001'.format(newCollectionName)
            else:
                newCollectionName = '{}_{}'.format(newCollectionName, version)

        else:
            return

    if newCollectionName == currentCollectionName:
        return

    newDescs = createNewCollectionFolder(currentCollectionName, newCollectionName, versioning, descs)
    oldDescs = list(api.xgen.listDescription(currentCollectionName))

    newDescs.sort(key=myOrder)
    oldDescs.sort(key=myOrder)

    newCollectionFile = createNewXgenFile(oldDescs, newDescs, currentCollectionName, newCollectionName)

    api.xgen.importCollection(str(newCollectionFile))

    for i in range(0, len(oldDescs)):
        oldDescriptionTransform = oldDescs[i]
        oldDescriptionName = cmd.listRelatives(oldDescriptionTransform)[0]
        cmd.select(clear=True)
        newDescriptionTransform = newDescs[i]
        newDescriptionName = cmd.listRelatives(newDescriptionTransform)[0]
        toSelect = [newDescriptionName]
        patches = cmd.listRelatives(newDescriptionTransform, type='xgmSubdPatch', allDescendents=True, fullPath=True)
        toSelect = toSelect + patches
        splines = cmd.listRelatives(newDescriptionTransform, type='xgmSplineGuide', allDescendents=True, fullPath=True)
        toSelect = toSelect + splines
        cmd.select(toSelect, replace=True)
        cmd.hyperShade(splines, assign=descInfo[oldDescriptionName]['shading'])
        cmd.sets(e=True, forceElement=descInfo[oldDescriptionName]['shading'])
        newDescriptionTransformFull = cmd.listRelatives(newDescriptionName, parent=True, fullPath=True)[0]
        cmd.setAttr('{}.visibility'.format(newDescriptionTransformFull), descInfo[oldDescriptionName]['visibility'])
        if not descInfo[oldDescriptionName]['cull']:
            if not cmd.objExists('NO_CULL'):
                cmd.sets(name='NO_CULL', empty=True)
            cmd.sets(newDescriptionTransform, add='NO_CULL')
        api.log.logger().debug('Set shader for description {}'.format(newDescriptionTransform))

    oldTopGroup = 'GRP_{}_groom'.format(oldName)
    newTopGroup = 'GRP_{}_groom'.format(newName)

    if oldTopGroup != newTopGroup:
        cmd.rename(oldTopGroup, newTopGroup)
        cmd.delete(currentCollectionName)

    if cmd.objExists(currentCollectionName):
        cmd.delete(currentCollectionName)


############################################################################
# @brief      order cause for descriptions list.
#
# @param      input  The input
#
# @return     the descrption number
#
def myOrder(input):
    return int(input.split('_')[-1].split('des')[1])
