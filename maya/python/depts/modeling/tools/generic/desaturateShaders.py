import importlib

import colorsys
import maya.cmds as cmd

import api.scene

importlib.reload(api.scene)


################################################################################
# @brief      desaturate selected shaders.
#
# @param      selection  The selection
#
def desaturateShaders():
    selection = cmd.ls(selection=True)
    if selection:
        materials = []
        for obj in selection:
            shape = cmd.listRelatives(obj, children=True)[0]
            shadeEng = cmd.listConnections(shape, type='shadingEngine')[0]
            material = cmd.ls(cmd.listConnections(shadeEng), materials=True)[0]
            if material not in materials:
                materials.append(material)
    else:
        materials = api.scene.getMaterialsInScene()

    for material in materials:
        color = cmd.getAttr('{}.color'.format(material))[0]
        hsvColor = colorsys.rgb_to_hsv(color[0], color[1], color[2])
        newColor = colorsys.hsv_to_rgb(hsvColor[0], 0.0, hsvColor[2])
        cmd.setAttr('{}.color'.format(material), newColor[0], newColor[1], newColor[2], type='double3')
