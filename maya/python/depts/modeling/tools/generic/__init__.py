import importlib


def run_abSymMesh(*args):
    import maya.mel as mel
    mel.eval("source abSymMesh.mel;")
    mel.eval("abSymMesh();")


def run_desaturateShaders(*args):
    import depts.modeling.tools.generic.desaturateShaders as desaturateShaders
    importlib.reload(desaturateShaders)

    desaturateShaders.desaturateShaders()


abSymMeshTool = {
    "name": "abSymMesh",
    "launch": run_abSymMesh
}


desaturateShadersTools = {
    "name": "Desaturate Shaders",
    "launch": run_desaturateShaders
}


tools = [
    abSymMeshTool,
    desaturateShadersTools
]

PackageTool = {
    "name": "generic",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
