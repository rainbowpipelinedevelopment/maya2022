import importlib


def run_chk(*args):
    import depts.modeling.tools.finalize.integrityCheck as minc
    importlib.reload(minc)
    minc.integrityCheck()


def run_ingest_chk(*args):
    import depts.modeling.tools.finalize.integrityCheck as minc
    importlib.reload(minc)
    minc.integrityCheck(ingest=True)


def run_fin(*args):
    import depts.modeling.tools.finalize.finalize as fm
    importlib.reload(fm)
    fm.finalize(character=False)


def run_finChar(*args):
    import depts.modeling.tools.finalize.finalize as fm
    importlib.reload(fm)
    fm.finalize(character=True)


finTool = {
    "name": "finTool",
    "launch": run_fin
}

finCharTool = {
    "name": "finCharTool",
    "launch": run_finChar
}


chkTool = {
    "name": "chkTool",
    "launch": run_chk
}

ingest_ChkTool = {
    "name": "ingest chkTool",
    "launch": run_ingest_chk
}


tools = [
    chkTool,
    ingest_ChkTool,
    finTool,
    finCharTool,
]


PackageTool = {
    "name": "finalize",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
