import importlib
import collections

import maya.cmds as cmd
import maya.mel as mel

import api.log
import api.scene
import api.widgets.rbw_UI as rbwUI
import config.dictionaries

importlib.reload(api.log)
importlib.reload(api.scene)
# importlib.reload(rbwUI)
importlib.reload(config.dictionaries)


eye_Locs = config.dictionaries.eye_locators


################################################################################
# @brief      clean up history function
#
def cleanUpHistory(originalSelection):
    api.log.logger().debug('start cleanUpHistory')
    toDeleteHistory = cmd.listRelatives(originalSelection, allDescendents=True, children=True)
    cmd.delete(toDeleteHistory, constructionHistory=True)
    api.log.logger().debug('end cleanUpHistory')


################################################################################
# @brief      check blendshape group
#
# @return     check result
#
def checkBlendshapes():
    api.log.logger().debug('start checkBlendshapes')
    if cmd.objExists('blendshape'):
        api.log.logger().debug("There is the blendshape group, finalize is skipped.")
        api.log.logger().debug('end checkBlendshapes')
        return False

    api.log.logger().debug('end checkBlendshapes')
    return True


################################################################################
# @brief      check if reference in scene
#
# @return     check result
#
def checkForReferences():
    api.log.logger().debug('start checkForReferences')
    allref = cmd.file(query=True, reference=True)
    if len(allref):
        api.log.logger().debug('The reference node exsists!')
        return False
    else:
        return True
    api.log.logger().debug('end checkForReferences')


################################################################################
# @brief      unparent all eye locators in scene.
#
# @param      originalSelection  The original selection
#
def unparentAllEyeLocs(originalSelection):
    api.log.logger().debug('start unparentAllEyeLocs')
    if originalSelection:
        children = cmd.listRelatives(originalSelection, ad=True, fullPath=True)
        for child in children:
            if child.split('|')[-1] == str(eye_Locs[0]):
                try:
                    cmd.parent(child, world=True)
                except:
                    pass
            if child.split('|')[-1] == str(eye_Locs[1]):
                try:
                    cmd.parent(child, world=True)
                except:
                    pass
    api.log.logger().debug('end unparentAllEyeLocs')


################################################################################
# @brief      Creates a temporary top group.
#
# @param      originalSel  The original selected
#
# @return     the top group name.
#
def createTmpTopGroup(originalSelection):
    api.log.logger().debug('start createTmpTopGroup')
    tmpnam = 'GRP_temp_____'
    if not cmd.objExists(tmpnam):
        cmd.select(clear=True)
        cmd.group(name=tmpnam, world=True, empty=True)
        cmd.xform(objectSpace=True, pivots=[0, 0, 0])
        cmd.parent(originalSelection, tmpnam)
        cmd.select(tmpnam, replace=True)

    api.log.logger().debug('end createTmpTopGroup')
    return tmpnam


################################################################################
# @brief      double names control.
#
# @param      topGroup  The top group
#
# @return     the check result.
#
def doubleNamesControl(topGroup):
    api.log.logger().debug('start doubleNamesControl')

    children = cmd.listRelatives(topGroup, allDescendents=True)
    childrenFull = cmd.listRelatives(topGroup, allDescendents=True, fullPath=True)
    doubles = [subItem for subItem, count in collections.Counter(children).items() if count > 1]

    for child in childrenFull:
        for double in doubles:
            if child.endswith(double):
                api.log.logger().debug('Warning there are some items named like this: {}'.format(child))
                api.log.logger().debug('end doubleNamesControl')
                return False

    api.log.logger().debug('end doubleNamesControl')
    return True


################################################################################
# @brief      reset all the transform attrs.
#
# @param      topGroup  The top group
#
def resetAllTransform(topGroup):
    api.log.logger().debug('start resetAllTransform')
    pivotAttrs = [
        "rpx", "rpy", "rpz", "rptx", "rpty", "rptz",
        "spx", "spy", "spz", "sptx", "spty", "sptz"
    ]
    transformAttrs = ["tx", "ty", "tz", "rx", "ry", "rz", "sx", "sy", "sz", "v"]

    parents = [str(a) for a in cmd.listRelatives(topGroup, children=True, allDescendents=True, fullPath=True)]
    for parent in parents:
        if cmd.nodeType(parent) == 'transform':
            # new check for locked rotation pivot points
            for pivotAttr in pivotAttrs:
                if cmd.getAttr('{}.{}'.format(parent, pivotAttr), lock=True):
                    cmd.setAttr('{}.{}'.format(parent, pivotAttr), lock=False)
            for transformAttr in transformAttrs:
                cmd.setAttr('{}.{}'.format(parent, transformAttr), lock=False)
        mel.eval('doBakeNonDefHistory( 1, {"prePost" });')
        cmd.makeIdentity(parent, apply=True)
        cmd.delete(parent, constructionHistory=True)
        cmd.xform(parent, pivots=[0, 0, 0])

    # secondo giro per avere davvero tutto a zero come pivot
    # (un solo ciclo non basta)
    for parent in parents:
        if cmd.nodeType(parent) == 'transform':
            for transformAttr in transformAttrs:
                cmd.setAttr('{}.{}'.format(parent, transformAttr), lock=False)
        mel.eval('doBakeNonDefHistory( 1, {"prePost" });')
        cmd.makeIdentity(parent, apply=True)
        cmd.delete(parent, constructionHistory=True)
        cmd.xform(parent, pivots=[0, 0, 0])

        # unlock all overrides
        shapes = cmd.listRelatives(parent)
        try:
            for shape in shapes:
                cmd.setAttr("{}.overrideEnabled".format(shape), 0)
            cmd.setAttr("{}.overrideEnabled".format(parent), 0)
        except:
            pass

    api.log.logger().debug('end resetAllTransform')


################################################################################
# @brief      re-parent all the eye locs.
#
# @param      topGroup  The top group
#
def parentAllTheEyeLocs(topGroup):
    api.log.logger().debug('start parentAllTheEyeLocs')
    for loc in eye_Locs:
        if cmd.objExists(loc):
            cmd.parent(loc, topGroup)
    api.log.logger().debug('end parentAllTheEyeLocs')


################################################################################
# @brief      Creates a top group and sub control.
#
# @param      selection  The selection
#
# @return     locator to top group.
#
def createTopGroupAndSubControl(selection):
    api.log.logger().debug('start createTopGroupAndSubControl')
    cmd.select(selection, replace=True)

    ctrlName = "CTRL_set"

    locator = cmd.spaceLocator(name=ctrlName)[0]
    sub_locator = cmd.spaceLocator(name="{}subControl".format(ctrlName))[0]

    api.log.logger().debug('sub_locator: {}'.format(sub_locator))

    loc_shape = "{}Shape".format(locator)
    sub_loc_shape = "{}Shape".format(sub_locator)

    cmd.addAttr(
        loc_shape, shortName="subC",
        defaultValue=True, attributeType="bool",
        niceName="subControl", keyable=False
    )

    cmd.setAttr("{}.subC".format(loc_shape), True, channelBox=True)
    cmd.parent(sub_locator, locator)
    cmd.setAttr("{}.v".format(sub_loc_shape), False)
    cmd.connectAttr("{}.subC".format(loc_shape), "{}.v".format(sub_loc_shape))
    cmd.setAttr('{}.subC'.format(loc_shape), False)

    api.log.logger().debug('end createTopGroupAndSubControl')
    return locator, sub_locator, ctrlName


################################################################################
# @brief      Stores a meshes in finalize node.
#
# @param      selection  The selection
#
def storeMeshesInFinalizeNode(selection):
    api.log.logger().debug('start storeMeshesInFinalizeNode')
    meshes = cmd.listRelatives(selection[0], children=True, allDescendents=True, type="mesh", noIntermediate=True)
    try:
        cmd.addAttr(selection[0], longName='top_group_finalized', dataType='string')
    except:
        cmd.setAttr('{}.top_group_finalized'.format(selection[0]), lock=False)
    if meshes:
        try:
            cmd.addAttr(selection[0], longName='all_children_meshes', dataType="stringArray")
        except:
            pass

    cmd.setAttr(
        ('{}.top_group_finalized'.format(selection[0])),
        ('{} - {} - {}'.format(selection[0], str(cmd.about(currentDate=True)), str(cmd.about(currentTime=True)))),
        type='string'
    )
    cmd.setAttr('{}.top_group_finalized'.format(selection[0]), lock=True)

    if meshes:
        cmd.setAttr('{}.all_children_meshes'.format(selection[0]), type="stringArray", *([len(meshes)] + meshes))
    api.log.logger().debug('end storeMeshesInFinalizeNode')


################################################################################
# @brief      Ends a property finialize.
#
# @param      selection    The selection
# @param      locator      The locator
# @param      sub_locator  The sub locator
# @param      tmpnam       The tmpname
#
def endPropFinialize(selection, locator, sub_locator, tmpnam):
    api.log.logger().debug('start endPropFinialize')
    gruppo = cmd.ls(selection[0])[0]
    # riaggiunto l'attributo reference al top group CTRL_set
    cmd.addAttr(
        locator, shortName="Rattr",
        defaultValue=True, attributeType="bool",
        niceName="reference", keyable=False
    )
    cmd.setAttr("{}.Rattr".format(locator), True, channelBox=True)
    cmd.connectAttr("{}.Rattr".format(locator), "{}.overrideEnabled".format(gruppo))

    # gruppo.overrideDisplayType.set(2)
    cmd.setAttr('{}.overrideDisplayType'.format(gruppo), 2)
    cmd.parent(selection, sub_locator)
    cmd.delete(tmpnam)
    api.log.logger().debug('end endPropFinialize')


################################################################################
# @brief      Ends a character finalize.
#
# @param      selection  The selection
# @param      ctrlName   The control name
# @param      tmpnam     The tmp name
#
def endCharFinalize(selection, ctrlName, tmpnam):
    api.log.logger().debug('start endCharFinalize')
    # ADD LOCATORS FOR EYE PIVOTS SE NON SONO PRESENTI
    existed = True

    if not cmd.objExists(str(eye_Locs[0])):
        existed = False
        eyeLocL = cmd.spaceLocator(name=eye_Locs[0], position=[0, 0, 0])[0]
        cmd.setAttr('{}.translateX'.format(str(eyeLocL)), 10)
        cmd.setAttr('{}.overrideEnabled'.format(str(eyeLocL)), 1)
        cmd.setAttr('{}.overrideColor'.format(str(eyeLocL)), 6)
        cmd.parent(str(eyeLocL), selection)

    if not cmd.objExists(str(eye_Locs[1])):
        existed = False
        eyeLocR = cmd.spaceLocator(name=eye_Locs[1], position=[0, 0, 0])[0]
        cmd.setAttr('{}.translateX'.format(str(eyeLocR)), -10)
        cmd.setAttr('{}.overrideEnabled'.format(str(eyeLocR)), 1)
        cmd.setAttr('{}.overrideColor'.format(str(eyeLocR)), 13)
        cmd.parent(str(eyeLocR), selection)

    cmd.parent(selection, world=True)
    cmd.delete([ctrlName, tmpnam])
    cmd.select(clear=True)

    if not existed:
        rbwUI.RBWConfirm(text='The character finalize has been done.\n\nPlease put the eye-pivot-point locators in \nthe appropriate position for this char.', title='ATTENTION', defCancel=None)
    else:
        cmd.headsUpMessage('The modeling finalize for this character has been done.\nThe eye locators found.', time=1)
    api.log.logger().debug('end endCharFinalize')


################################################################################
# @brief      finalize the asset.
#
# @param      character  The character
#
def finalize(character=False):
    api.log.logger().debug('start finalize')

    """ Finalize the selected top group of the model

    For all the meshes and trasform of groups doing:
    - check double names
    - frezee all transformations
    - set the pivots at the origin
    - delete history
    - set the override display to reference for the top group
    (connected to the CTRL_set created for set dressing purpose)
    """

    originalSelection = cmd.ls(selection=True)
    if not originalSelection:
        rbwUI.RBWError(title='ATTENTION', text='You must select something.')

    # check sulla selezione
    if not cmd.ls(selection=True, type="transform"):
        api.log.logger().error("You must select your top group node to finalize this.")
        api.log.logger().debug('end finalize')
        return

    # clean up all the scene first
    cleanUpHistory(originalSelection)

    if originalSelection == [api.scene.getGroupName()]:
        # check blendshapes in scene
        if not checkBlendshapes():
            api.log.logger().debug('end finalize')
            return

        if not (checkForReferences()):
            api.log.logger().debug('There is at least one reference file related to this scene,' '\nplease remove it to publish a clean model asset.')
            api.log.logger().debug('end finalize')
            return

    # butto fuori i locators per gli occhi
    unparentAllEyeLocs(originalSelection)

    tmpnam = createTmpTopGroup(originalSelection)

    # FINAL CHECK FOR DOUBLE NAMES!!!!
    if doubleNamesControl(tmpnam):
        upperGrp = cmd.ls(selection=True, type="transform")

        if upperGrp is None:
            api.log.logger().debug("error ! select at least only an object")

        elif len(upperGrp) > 1:
            api.log.logger().debug("error ! select only an object")

        else:
            resetAllTransform(tmpnam)

            # rimetto dentro i locators degli occhi
            parentAllTheEyeLocs(originalSelection)

            # create the top group and the subContrl locator
            locator, sub_locator, ctrlName = createTopGroupAndSubControl(originalSelection)

            # add two extra attribute to the top group finalized to keep track
            # prendo tutte le mesh figlie del gruppo in finalizzazione per storarle nel nodo del finalize
            storeMeshesInFinalizeNode(originalSelection)

            if not character:
                endPropFinialize(originalSelection, locator, sub_locator, tmpnam)
            else:
                endCharFinalize(originalSelection, ctrlName, tmpnam)

            cmd.headsUpMessage('The modeling finalize has been done.', time=1)
            api.log.logger().debug('end finalize')

    else:
        cmd.parent(originalSelection, world=True)
        cmd.delete(tmpnam)
        api.log.logger().debug('finalize model fail, please check and fix the double names!')
        rbwUI.RBWError(title='ATTENTION', text='finalize model fail, please check and fix the double names!')
        api.log.logger().debug('end finalize')
