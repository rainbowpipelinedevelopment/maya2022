import importlib

import maya.cmds as cmd
import maya.mel as mel

import api.log
import api.widgets.rbw_UI as rbwUI
import common.renameShape as renameShape

importlib.reload(api.log)
# importlib.reload(rbwUI)
importlib.reload(renameShape)

excludeGroup = 'blendshape'


################################################################################
# @brief      This script centers the pivot of all meshes and freezes
#             transformations.
#
# @param      ingest  The ingest
#
# @return     the check result.
#
def integrityCheck(ingest=False):
    if cmd.objExists(excludeGroup):
        api.log.logger().debug("There is the blendshape group, integrityCheck is skipped.")
        return

    allReturn = []
    duplicatedItems = lookingForDuplicate()
    nestedMeshes = lookForNestedMeshes()
    if nestedMeshes:
        rbwUI.RBWError(text="In this asset's structure there is at least 1 nested mesh,\nthis means that a 'GRP' is parented under a 'MSH'.\n\nPlease fix this asset before saving it!", title='Nested meshes found!')

    if not duplicatedItems and not nestedMeshes:
        allReturn.append('-' * 150)
        lookForLockedChannel()
        allReturn.append('MULTIPLE ITEMS WITH THE SAME NAME ===> PASSED')

        allReturn.append('-' * 150)
        renamed = renameShape.renameShape()
        allReturn.append('SHAPES RENAMED BY TRANSFORM NAME ===> {} ITEMS'.format(renamed))

        allReturn.append('-' * 150)
        unparentFreezeAndCenterPivot()
        allReturn.append('FREEZING AND CENTER PIVOT ===> PASSED')

        allReturn.append('-' * 150)
        wraps = deleteWrapperButWrapped()
        allReturn.append('WRAP REMOVED ===> {}'.format(wraps))

        if not ingest:
            prefix = lookingForMSHprefix()
        else:
            prefix = 0

        allReturn.append('-' * 150)
        allReturn.append('ADDED "MSH_" PREFIX TO ===> {} ITEMS'.format(prefix))
        cmd.select(clear=1)

        allReturn.append('-' * 150)
        intermediate = lookingForIntermediate()
        allReturn.append('FOUND AND REMOVED ===> {} INTERMEDIATE OBJECTS'.format(intermediate))

        allReturn.append('-' * 150)
        deleteGrp = deleteEmptyGroups()
        allReturn.append('EMPTY GROUPS REMOVED ===> {} '.format(deleteGrp))

        allReturn.append('-' * 150)
        cmd.select(clear=1)
        instances = lookForInstances()
        allReturn.append('INSTANCED MESHES FOUND ===> {} '.format(instances))

        allReturn.append('-' * 150)
        cmd.select(clear=1)
        allReturn.append('CHECK FOR NON QUAD FACES ===> {} '.format('NOT PASSED' if hasNonQuadsFaces() else 'PASSED'))

        allReturn.append('-' * 150)
        cmd.select(clear=1)
        allTransformInScene = [item for item in cmd.ls(type='transform', l=1) if not item.count(excludeGroup)]
        cmd.delete(allTransformInScene, constructionHistory=1)

        rbwUI.RBWConfirm(title='Model Integrity Check', text='\n'.join(allReturn), defCancel=None)

        api.log.logger().debug('\n'.join(allReturn))
        return True

    return False


################################################################################
# @brief      This script looks for locked channel and animated channels unlocks them and deletes animation.
#
# @return     animated channel number.
#
def lookForLockedChannel():
    transforms = [item for item in cmd.ls(type='transform', l=1) if not item.count(excludeGroup)]

    if transforms is not None:
        cmd.currentTime(1)
        for transform in transforms:
            if cmd.referenceQuery(transform, isNodeReferenced=True):
                continue

            cmd.setAttr('{}.tx'.format(transform), lock=0)
            cmd.setAttr('{}.ty'.format(transform), lock=0)
            cmd.setAttr('{}.tz'.format(transform), lock=0)
            cmd.setAttr('{}.rx'.format(transform), lock=0)
            cmd.setAttr('{}.ry'.format(transform), lock=0)
            cmd.setAttr('{}.rz'.format(transform), lock=0)
            cmd.setAttr('{}.sx'.format(transform), lock=0)
            cmd.setAttr('{}.sy'.format(transform), lock=0)
            cmd.setAttr('{}.sz'.format(transform), lock=0)
            cmd.setAttr('{}.v'.format(transform), lock=0)

            cmd.cutKey(transform, attribute='translateX', option="keys")
            cmd.cutKey(transform, attribute='translateY', option="keys")
            cmd.cutKey(transform, attribute='translateZ', option="keys")
            cmd.cutKey(transform, attribute='rotateX', option="keys")
            cmd.cutKey(transform, attribute='rotateY', option="keys")
            cmd.cutKey(transform, attribute='rotateZ', option="keys")
            cmd.cutKey(transform, attribute='scaleX', option="keys")
            cmd.cutKey(transform, attribute='scaleY', option="keys")
            cmd.cutKey(transform, attribute='scaleZ', option="keys")
            cmd.cutKey(transform, attribute='visibility', option="keys")

    return len(transforms)


################################################################################
# @brief       Looking for duplicate meshes and groups name.
#
# @return     if double name are or not.
#
def lookingForDuplicate():
    # Array contenente tutti gli oggetti con lo stesso nome
    sameNameArray = []

    try:
        allGroups = getAllGroupsInScene()
        notDuplicatedGroups = []
        duplicatedGroups = []

        for group in allGroups:
            if group not in notDuplicatedGroups:
                notDuplicatedGroups.append(group)
            else:
                duplicatedGroups.append(group)

        if len(duplicatedGroups) > 0:
            rbwUI.RBWConfirm(text='\n'.join(duplicatedGroups), title='MULTIPLE GROUPS', defCancel=None)
            selectionArray = []
            for group in duplicatedGroups:
                selectionArray.append(
                    cmd.ls(group.split('|')[-1], type='transform'))

            for sa in selectionArray:
                cmd.select(sa, tgl=1)
            return True

    except RuntimeError as e:
        api.log.logger().debug(e)

    meshes = cmd.ls(long=True, noIntermediate=True, exactType=['mesh', 'gpuCache'])
    meshes = cmd.listRelatives(meshes, parent=True)

    if meshes:
        # LOOKING FOR ITEMS WITH THE SAME NAME
        counter = 0
        fullArray = []
        for i in range(0, len(meshes), 1):
            if meshes.count(meshes[i]) > 1:
                fullArray.append(cmd.ls(meshes[i], long=True)[0])
                sameNameArray.append(cmd.ls(meshes[i], noIntermediate=True)[0])
                api.log.logger().debug(meshes[i])
                counter += 1

        if fullArray:
            filteredSameNameArray = []
            for s in sameNameArray:
                filteredSameNameArray.append(s.split('|')[-1])

            sameNameArray = list(set(filteredSameNameArray))

            if len(fullArray) == 0:
                return False
            else:
                rbwUI.RBWConfirm(text='\n'.join(sameNameArray), title='MULTIPLE ITEMS', defCancel=None)
                cmd.select(cl=1)
                selectionArray = []
                for s in sameNameArray:
                    selectionArray.append(
                        cmd.ls(s.split('|')[-1], type='transform'))

                for sa in selectionArray:
                    cmd.select(sa, tgl=1)
                return True
    return False


################################################################################
# @brief      Looking for nested MSH transform.
#
# @return     if nested or not.
#
def lookForNestedMeshes():
    res = False
    for x in cmd.ls(type='transform'):
        par = cmd.listRelatives(x, parent=True)
        if par and par[0].startswith('MSH_'):
            res = True

    return res


################################################################################
# @brief      Looks for intermediate objects.
#
# @return     a list of all the intermediate found.
#
def lookingForIntermediate():
    foundIntermediate = 0
    # LOOKS FOR INTERMEDIATE OBJECTS
    allItems = [item for item in cmd.ls(type='mesh', l=1) if not item.count(excludeGroup)]

    for item in allItems:
        isInt = cmd.getAttr('{}.intermediateObject'.format(item))
        if isInt:
            isConn = cmd.listConnections(item)
            if not isConn:
                try:
                    cmd.delete(item)
                    foundIntermediate += 1
                except:
                    pass

    api.log.logger().debug('foundIntermediate ===> {}'.format(foundIntermediate))
    return foundIntermediate


################################################################################
# @brief      This script center the pivot, freeze and set to "0" all vertices.
#
def unparentFreezeAndCenterPivot():
    allTransform = [item for item in cmd.ls(type='transform', l=1) if not item.count(excludeGroup)]

    for item in allTransform:
        checkShape = cmd.nodeType(item)
        if checkShape != 'camera':
            cmd.makeIdentity(item, apply=1, translate=1, rotate=1, scale=1, jointOrient=0, normal=0)
    cmd.select(clear=1)


################################################################################
# @brief      Loops over all the meshes in the scene and add the MSH_ prefix
#             if missing.
#
# @return     add prefix meshes number.
#
def lookingForMSHprefix():
    addPrefix = 0
    meshShape = [item for item in cmd.ls(dag=1, type=['mesh'], noIntermediate=1, long=1) if not item.count(excludeGroup)]
    if meshShape is not None:
        meshParent = cmd.listRelatives(meshShape, parent=1, fullPath=1)
        # LOOKS FOR 'MSH_' PREFIX... ...IF DOES'T EXIST SCRIPT ADD IT
        if meshParent is not None:
            for x in meshParent:
                if x.split('|')[-1].find('MSH_') == -1:
                    spltBase = x.split('|')
                    newName = 'MSH_{}'.format(spltBase[-1])
                    cmd.rename(x, newName)
                    addPrefix += 1

    return addPrefix


################################################################################
# @brief      This script check if all existing meshes have a blendeshape
#             output connection and removes them
#
# @return     Number of mesh with blend shapes
#
def lookForBlendShapeOutputConnections():
    cmd.scriptEditorInfo(suppressWarnings=True)
    meshShape = cmd.ls(dag=1, type=['mesh'], ni=1, l=1)
    meshesHavingBlendshapeOutput = []
    blendShapeEnvelopes = []
    visibleMeshesSelection = cmd.ls(type='mesh', ni=1, l=1)

    for visible in visibleMeshesSelection:
        buffer = cmd.listConnections(visible, s=0, d=1)
        for b in buffer:
            bType = cmd.nodeType(b)
            if bType == 'blendShape':
                meshesHavingBlendshapeOutput.append(
                    cmd.listRelatives(visible, p=1, f=1)[0])
                blendShapeEnvelopes.append(b + '.envelope')

    if meshShape is not None:
        bsFind = meshesHavingBlendshapeOutput, blendShapeEnvelopes
        for b in bsFind[1]:
            cmd.setAttr(b, 0)
        for b in bsFind[0]:
            try:
                cmd.delete(b)
            except:
                pass
    cmd.scriptEditorInfo(suppressWarnings=False)
    return len(meshesHavingBlendshapeOutput)


################################################################################
# @brief      This script check if all existing meshes have a wrap output
#             connection
#
# @return     wrapped meshes number
#
def deleteWrapperButWrapped():
    cmd.scriptEditorInfo(suppressWarnings=True)
    wrappedMesh = []
    wrappedMeshBuffer = []
    wrap = cmd.ls(type='wrap', l=1)

    if str(wrap) == '[]':
        api.log.logger().debug("There aren't any mesh with wrap deformers.")
        meshConnectedToWrap = 0
    else:
        meshConnectedToWrap = cmd.listConnections(wrap, type='mesh', shapes=1)

    # DETECTS WRAPPED MESH
    wrap = cmd.ls(type='wrap', l=1)
    if wrap:
        for w in wrap:
            a = cmd.listConnections(w, source=0, shapes=1)
            b = cmd.listConnections(a, source=0, shapes=1)
            wrappedMeshBuffer.append(b)
        tempWrappedMesh = list(set(wrappedMeshBuffer))
        for temp in tempWrappedMesh:
            nodeType = cmd.nodeType(temp)
            if nodeType == 'mesh':
                wrappedMesh.append(temp)

        if meshConnectedToWrap > 0:
            for mesh in meshConnectedToWrap:
                for w in wrappedMesh:
                    if mesh.split('|')[-1] != w.split('|')[-1]:
                        try:
                            cmd.delete(mesh)
                        except:
                            pass
            cmd.scriptEditorInfo(suppressWarnings=False)
    return len(wrappedMesh)


################################################################################
# @brief      Loops through all the meshes and all the transforms, removing all
#             the empty groups.
#
# @return     number of deleted group.
#
def deleteEmptyGroups():
    deletedGroups = 0
    roots = []
    allTransform = [item for item in cmd.ls(type='transform', l=1) if not item.count(excludeGroup)]
    for transf in allTransform:
        cmd.delete(transf, constructionHistory=1)

    # REMOVING RESIDUAL INTERMIDIATE OBJECT
    allIntermediateMeshes = [item for item in cmd.ls(type='mesh', l=1) if not item.count(excludeGroup)]
    for interMesh in allIntermediateMeshes:
        isInt = cmd.getAttr('{}.intermediateObject'.format(interMesh))
        if isInt:
            cmd.delete(interMesh)

    # LOOK FOR GROUP ROOTS
    for item in allTransform:
        root = mel.eval('rootOf("{}")'.format(item))
        isCamera = cmd.listRelatives(root, type='camera', fullPath=1)
        if not isCamera:
            roots.append(root)
        else:
            pass

    # LOOK FOR ROOTS CHILDRENS
    listChildren = []
    roots = list(set(roots))
    for root in roots:
        listChildren.append(cmd.listRelatives(root, allDescendents=1, type='transform', fullPath=1))
        for child in listChildren:
            pass

    for i in range(0, len(listChildren), 1):
        if listChildren[i] is not None:
            for a in range(0, len(listChildren[i]), 1):
                hasShape = cmd.listRelatives(listChildren[i][a], noIntermediate=1, fullPath=1)
                if hasShape is None:
                    cmd.delete(listChildren[i][a])
                    deletedGroups += 1

    # LOOK FOR ROOTS CHILDRENS
    listChildren = []
    roots = list(set(roots))
    for root in roots:
        hasShape = cmd.listRelatives(root, noIntermediate=1, fullPath=1)
        if hasShape is None:
            cmd.delete(root)
            deletedGroups += 1

    return deletedGroups


################################################################################
# @brief      Loops over all the meshes and returns the number of mesh
#             instances in the scene.
#
# @return     number of instance.
#
def lookForInstances():
    meshes = [item for item in cmd.ls(type='mesh') if not item.count(excludeGroup)]
    instanceNum = 0
    for i in meshes:
        inst = cmd.listRelatives(i, allParents=1)
        if len(inst) > 1:
            instanceNum += len(inst)
    return instanceNum


################################################################################
# @brief      Recursive function that check child groups from parent.
#
# @param      parent  The parent
# @param      groups  The groups
#
# @return     { description_of_the_return_value }
#
def recursiveFindGroup(parent, groups):
    childs = cmd.listRelatives(parent, type="transform", fullPath=True) or []

    grps = [c for c in childs if isGroup(c)]
    ret = None

    for g in grps:
        groups.append(g.split("|")[-1])
        ret = recursiveFindGroup(g, groups)

    return ret


################################################################################
# @brief      Gets all groups in scene.
#
# @return     All groups in scene.
#
def getAllGroupsInScene():
    top_groups = filter(isGroup, topGroups())
    sub_groups = []

    for grp in top_groups:
        recursiveFindGroup(grp, sub_groups)

    all_groups = list(top_groups)
    for sub_group in sub_groups:
        all_groups.append(sub_group)

    return all_groups


################################################################################
# @brief       Return top groups that are not default cameras nodes
#
# @return     the top group list.
#
def topGroups():
    top_nodes = cmd.ls(assemblies=True)
    defaultNodes = ['persp', 'top', 'front', 'side']
    custom_nodes = [grp for grp in top_nodes if grp not in defaultNodes]
    top_groups = [grp for grp in custom_nodes if isGroup(grp)]
    return top_groups


################################################################################
# @brief      Determines whether the specified node is group.
#
# @param      node  The node
#
# @return     True if the specified node is group, False otherwise.
#
def isGroup(node):
    if "transform" not in cmd.nodeType(node):
        return False

    children = cmd.listRelatives(node, children=True, fullPath=True)

    if children:
        for child in children:
            isNotTransform = "transform" not in cmd.nodeType(child)
            isNotXgenPalette = "xgmPalette" not in cmd.nodeType(child)
            isNotAssemblyReference = "assemblyReference" not in cmd.nodeType(child)

            if isNotTransform and isNotXgenPalette and isNotAssemblyReference:
                return False

    return True


############################################################################
# @brief      Checks every face of every mesh and counts its vertices.
#
# @return     True or False depending on the existance of non quads faces.
#
def hasNonQuadsFaces():
    cmd.select(clear=True)
    meshes = cmd.ls(type='mesh')
    progress = rbwUI.RBWProgressBar(len(meshes))
    for mesh in meshes:
        progress.cycles['Main'].setText('Looking for nGons in: {}'.format(mesh))

        cmd.select('{}.f[*]'.format(mesh))
        faces = cmd.ls(sl=True)
        for x in cmd.filterExpand(faces, sm=34):
            vertices = len([x for x in cmd.polyInfo(x, faceToVertex=True)[0].split(':')[1].replace(' \n', '').split(' ') if x and x != ' '])
            if vertices > 4:
                progress.cycles['Main'].updateProgress(len(meshes))
                return True

        progress.cycles['Main'].updateProgress()

    return False
