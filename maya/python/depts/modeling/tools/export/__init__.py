import importlib


def run_exportFbx(*args):
    import depts.modeling.tools.export.exportFbx as expFbx
    importlib.reload(expFbx)
    expFbx.export()


exportFbxTool = {
    'name': 'Export fbx',
    'launch': run_exportFbx,
    # 'statustip': 'Export the fbx of the currently open scene'
}

tools = [
    exportFbxTool
]


PackageTool = {
    "name": "export",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
