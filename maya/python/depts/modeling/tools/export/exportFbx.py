import importlib

import maya.cmds as cmd
import maya.mel as mel

import api.log
importlib.reload(api.log)


def export(combine=False):
    reOpen = 0
    setCtrl = 'CTRL_set'
    subSetCtrl = 'CTRL_setsubControl'

    if cmd.objExists(setCtrl):
        originalFile = cmd.file(q=1, sceneName=1)
        reOpen = 1

        # workaround to avoid the export with overrive-template
        cmd.setAttr(setCtrl + ".Rattr", 0)
        selection = cmd.listRelatives(subSetCtrl, c=True)[1]
        cmd.parent(selection, world=True)
        cmd.delete([setCtrl, subSetCtrl])
    else:
        selection = cmd.ls(sl=True)

    if not selection:
        topGroups = cmd.ls(assemblies=1)

        for top in topGroups:
            if top not in ["front", "side", "persp", "top"]:
                selection = top

    if not selection:
        api.log.logger().debug("There is nothing in this scene, please check!")
        return

    if combine:
        cmd.xform(selection, rotation=(90, 0, 90))
        mel.eval('setUpAxis "z";')
        combinedMesh = 'MSH_combinedMesh'
        objects = [cmd.listRelatives(mesh, parent=True, fullPath=True)[0] for mesh in cmd.ls(type='mesh')]
        if len(objects) > 1:
            cmd.polyUnite(objects, ch=True, mergeUVSets=True, centerPivot=True, name=combinedMesh)
            cmd.parent(combinedMesh, selection)
            mel.eval('DeleteAllHistory')

    if combine:
        file = str(cmd.file(q=1, sceneName=1)).replace('.mb', "_ue.fbx")
    else:
        file = str(cmd.file(q=1, sceneName=1)).replace('.mb', ".fbx")

    plugin = "fbxmaya.mll"
    cmd.loadPlugin(plugin, qt=True)

    if selection:
        cmd.select(selection, r=1)

    mel.eval("FBXExportInAscii -v true; ")
    api.log.logger().debug("start FBX")
    mel.eval("FBXExportFileVersion -v FBX201200; ")
    if combine:
        mel.eval("FBXExportSmoothingGroups -v true;")
    mel.eval('FBXExport -f "{}" -s'.format(file))
    api.log.logger().debug("Exported FBX here: {}".format(file))

    # restore the Y up pref
    mel.eval('setUpAxis "y";')

    if reOpen:
        cmd.file(originalFile, open=True, force=True)
