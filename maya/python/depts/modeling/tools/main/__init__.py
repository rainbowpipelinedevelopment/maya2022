import importlib
import os

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'arakno')


def run_loadAndSave(*args):
    import depts.modeling.tools.main.loadAndSaveModelingUI as loadAndSaveMod
    importlib.reload(loadAndSaveMod)

    win = loadAndSaveMod.LoadAndSaveModelingUI()
    win.show()


loadAndSaveTool = {
    "name": "L/S Modeling",
    "launch": run_loadAndSave,
    "icon": os.path.join(iconDir, "loadAndSaveModeling.png"),
    "statustip": "Load & Save Modeling"
}


tools = [
    loadAndSaveTool
]

PackageTool = {
    "name": "main",
    "tools": tools
}
