import importlib
from PySide2 import QtWidgets

import maya.cmds as cmd

import common.loadAndSaveAssetUI
import api.savedAsset
import api.scene
import api.widgets.rbw_UI as rbwUI
import depts.modeling.tools.finalize.integrityCheck as mic

importlib.reload(common.loadAndSaveAssetUI)
# importlib.reload(api.savedAsset)
importlib.reload(api.scene)
# importlib.reload(rbwUI)
importlib.reload(mic)


################################################################################
# @brief      This class describes a load and save modeling ui.
#
class LoadAndSaveModelingUI(common.loadAndSaveAssetUI.AssetUI):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(LoadAndSaveModelingUI, self).__init__('Modeling')

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        super(LoadAndSaveModelingUI, self).initUI()

        self.saveUploadOnSgLabel = rbwUI.RBWLabel(text='Upload On SG', italic=False, size=12)
        self.saveUploadOnSgSwitch = rbwUI.RBWSwitch()
        self.saveUploadOnSgSwitch.setChecked(False)
        self.saveAutoProxyLabel = rbwUI.RBWLabel(text='Generate Proxy', italic=False, size=12)
        self.saveAutoProxySwitch = rbwUI.RBWSwitch()
        self.saveReducePercentLabel = rbwUI.RBWLabel(text='Reduce Percent', italic=False, size=12)
        self.saveReducePercentLineEdit = rbwUI.RBWLineEdit(bgColor='transparent', stretch=False)
        self.saveReducePercentLineEdit.setText('50')

        self.saveInfoGroup.addRow(self.saveUploadOnSgLabel, self.saveUploadOnSgSwitch)
        self.saveInfoGroup.addRow(self.saveAutoProxyLabel, self.saveAutoProxySwitch)
        self.saveInfoGroup.addRow(self.saveReducePercentLabel, self.saveReducePercentLineEdit)

    ############################################################################
    # @brief      save function.
    #
    def save(self):
        try:
            savedAssetParam = {
                'asset': self.currentAsset,
                'dept': self.deptName.lower(),
                'deptType': self.currentDeptType,
                'approvation': self.currentApprovation,
                'note': self.saveNotesTextEdit.toPlainText(),
                'sgUploadPicture': self.saveUploadOnSgSwitch.isChecked()
            }

            if self.saveExportSelectionSwitch.isChecked():
                savedAssetParam['selection'] = cmd.ls(selection=True)[0]
            else:
                savedAssetParam['selection'] = None

            if savedAssetParam['approvation'] == 'def' and savedAssetParam['deptType'] not in ['gbs', 'bs', 'utls'] and self.currentAsset.category != 'set':
                checkIntegrity = mic.integrityCheck()
            else:
                checkIntegrity = True

            if checkIntegrity:

                if self.currentApprovation == 'def':
                    wipSavedAssetParam = {
                        'asset': self.currentAsset,
                        'dept': self.deptName.lower(),
                        'deptType': self.currentDeptType,
                        'approvation': 'wip',
                        'note': 'Pipeline autosave before def save',
                        'sgUploadPicture': False,
                        'selection': savedAssetParam['selection']
                    }

                    wipSavedAsset = api.savedAsset.SavedAsset(params=wipSavedAssetParam)
                    wipSuccess = wipSavedAsset.save()

                    if wipSuccess:
                        savedAssetParam['parent'] = wipSavedAsset.db_id
                        savedAsset = api.savedAsset.SavedAsset(params=savedAssetParam)
                        success = savedAsset.save()
                        if success:
                            self.updateWipParentOnDB(wipSavedAsset, savedAsset)
                            if self.currentDeptType == 'hig' and self.saveAutoProxySwitch.isChecked() and self.saveReducePercentLineEdit.text() != '':
                                try:
                                    scenefilename = api.scene.scenename()

                                    if savedAssetParam['selection']:
                                        topGroup = savedAssetParam['selection']
                                    else:
                                        topGroup = api.scene.getGroupName()

                                    cmd.select(topGroup, replace=True)
                                    percent = int(self.saveReducePercentLineEdit.text())
                                    self.polyReduceSelection(percent)
                                    cmd.delete(topGroup, constructionHistory=True)

                                    prxWipSavedAssetParam = {
                                        'asset': self.currentAsset,
                                        'dept': self.deptName.lower(),
                                        'deptType': 'prx',
                                        'approvation': 'wip',
                                        'note': 'Pipeline autosave proxy',
                                        'sgUploadPicture': False,
                                        'selection': topGroup
                                    }

                                    prxWipSavedAsset = api.savedAsset.SavedAsset(params=prxWipSavedAssetParam)
                                    prxWipSucces = prxWipSavedAsset.save()

                                    if prxWipSucces:
                                        prxSavedAssetParam = {
                                            'asset': self.currentAsset,
                                            'dept': self.deptName.lower(),
                                            'deptType': 'prx',
                                            'approvation': 'def',
                                            'note': 'Pipeline autosave proxy',
                                            'sgUploadPicture': False,
                                            'selection': topGroup,
                                            'parent': prxWipSavedAsset.db_id
                                        }

                                        prxSavedAsset = api.savedAsset.SavedAsset(params=prxSavedAssetParam)
                                        prxSucces = prxSavedAsset.save()

                                        if prxSucces:
                                            self.updateWipParentOnDB(prxWipSavedAsset, prxSavedAsset)
                                            cmd.file(new=True, force=True)
                                            cmd.file(scenefilename, open=True, force=True)
                                except:
                                    rbwUI.RBWWarning(title='ATTENTION', text='Problems during autosave prx.\nThe hig def save instead is correctly ended.')
                                    return
                    else:
                        rbwUI.RBWError(title='ATTENTION', text='Problems during autosave wip.\nPleas control the scene and make a new save.')
                        return

                else:
                    savedAsset = api.savedAsset.SavedAsset(params=savedAssetParam)
                    success = savedAsset.save()

                if success:
                    rbwUI.RBWConfirm(title='Save {} Complete'.format(self.deptName), text='Asset Save Complete', defCancel=None)
                    self.fillSavedAssetsTree()
                else:
                    rbwUI.RBWError(title='ATTENTION', text='Some Error During Save')
                    return
            else:
                rbwUI.RBWError(title='ATTENTION', text='The scene doesn\'t pass the Modeling Check Integrity, control and make a new save.')
                return

        except AttributeError:
            rbwUI.RBWError(title='ATTENTION', text='One between deptType or approvation are not properly selected.\nSelect those and try again.')
            return

    ################################################################################
    # @brief      generate the proxy version of the asset from his top group.
    #
    def polyReduceSelection(self, percent=50):
        topGroup = cmd.ls(selection=True)[0]

        objects = cmd.listRelatives(topGroup, children=True, allDescendents=True, type='mesh', fullPath=True)

        api.log.logger().debug('percent: {}'.format(percent))

        for obj in objects:
            cmd.polyReduce(
                obj,
                version=1,
                sharpness=1,
                keepMapBorder=0,
                keepColorBorder=0,
                keepFaceGroupBorder=0,
                keepHardEdge=0,
                keepCreaseEdge=0,
                keepBorderWeight=1,
                symmetryPlaneX=0,
                symmetryPlaneY=1,
                symmetryPlaneZ=0,
                vertexMapName="",
                cachingReduce=1,
                constructionHistory=1,
                percentage=percent,
                replaceOriginal=1
            )

    ############################################################################
    # @brief      check function
    #
    def checkToSave(self):
        super(LoadAndSaveModelingUI, self).checkToSave()

        if not self.checkTextureExt():
            return

        return True
