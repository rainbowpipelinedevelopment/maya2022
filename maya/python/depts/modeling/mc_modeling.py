import importlib
import os

import maya.cmds as cmd

import depts.mc_depts as mcdepts
import api.log
import api.json
import api.saveNode
import api.scene
import api.widgets.rbw_UI as rbwUI
import depts.modeling.tools.export.exportFbx as expFbx
import common.renameShape as renameShape

importlib.reload(mcdepts)
importlib.reload(api.log)
importlib.reload(api.json)
importlib.reload(api.saveNode)
importlib.reload(api.scene)
# importlib.reload(rbwUI)
importlib.reload(expFbx)
importlib.reload(renameShape)


################################################################################
# @brief      This class describes a modeling.
#
class Modeling(mcdepts.Dept):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, savedAsset):
        super(Modeling, self).__init__(savedAsset)
        self.deptName = "modeling"

    ############################################################################
    # @brief      save function
    #
    # @return     the success of the save.
    #
    def save(self):
        api.scene.pluginCleanUp()
        self.localSave()

        if self.getDeptType() not in ["bs", "gbs", "utls"]:
            api.log.logger().debug('Modeling save checks startup')
            # Remove absolute paths
            try:
                self.checkTexturesPath()
            except:
                rbwUI.RBWWarning(title='warning', text='Error while checking and replacing absolute paths with relative ones./nThe save will contine but please contact pipeline@rbw-cgi.it')

            # rinomino automatico delle shapes
            renameShape.renameShape()

            if self.isDef():
                if not self.savedAsset.selection:
                    api.log.logger().debug('primo controllo unico top group starting')
                    if not self.checkGroupTransform():
                        return
                    api.log.logger().debug('primo controllo unico top group done')

                if not self.isSet():
                    api.log.logger().debug('secondo controllo finalize starting')
                    if not (self.checkForModelFinalize()):
                        return
                    api.log.logger().debug('secondo controllo finalize  done')

                    api.log.logger().debug('terzo controllo tutti i trasform a zero starting')
                    if not (self.checkZeroTransform()):
                        return
                    api.log.logger().debug('terzo controllo trasform done')

                    api.log.logger().debug('quarto controllo double names starting')
                    if not (self.checkForDoubleNames()):
                        return
                    api.log.logger().debug('quarto controllo double names done')

                    api.log.logger().debug('quinto controllo frozen object not properly finalized starting')
                    if not (self.checkForFrozenObjects()):
                        return
                    api.log.logger().debug('quinto controllo frozen object not properly finalized done')

                    if self.getDeptType() != 'prx':
                        api.log.logger().debug('start make MSH set')
                        mshMeshes = self.createMSHSet()
                        api.log.logger().debug('end make MSH set')

            api.log.logger().debug("   *** model consistence prx -hig starting... ***")
            # NEW FOR MODELING CONSISTENCE
            self.exportXML()

            # make the sourceimages folders
            self.craeteTextureFolders()
            api.log.logger().debug("cartelline dentro sourceimages per il surfacing.. done")

        # === Display Layers ON and remove ===
        if self.isDef():
            self.deleteUnusedShapeOrig()
            self.cleanupDisplayLayers()

        # Viene eseguito il tagging delle mesh in base al save-node
        self.tagMeshes()

        # === Real Save On Disk!! ===
        self.realSave()

        scenefilename = api.scene.scenename()

        if self.isDef():
            # === New Export Alembic .abc file too ===
            if self.getDeptType() not in ["bs", "gbs", "utls"]:
                alembic, gpuBase, gpuSmooth = self.saveAlembicToo()

                if not self.isSet() and self.getDeptType() != 'prx':
                    if cmd.objExists('MSH'):
                        data = {'MSH': mshMeshes}
                        api.json.json_write(data, os.path.join(self.savedAsset.savedAssetFolder, '{}_MSH.json'.format(self.savedAsset.sceneName)).replace('\\', '/'))

                cmd.select(clear=True)
                expFbx.export()
                cmd.select(clear=True)
                expFbx.export(combine=True)

            cmd.file(new=True, force=True)
            cmd.file(scenefilename, open=True, force=True)

        self.publishOnShotgun()

        return True

    ############################################################################
    # @brief      Gets all trasform.
    #
    # @return     All trasform.
    #
    def getAllTrasform(self):
        # default top nodes
        default = ["front", "side", "persp", "top", "left", "bottom", "back", "blendshape", 'IMPORT', 'LIBRARY', 'EXTRA', 'PRX']

        all = cmd.ls(assemblies=True)
        # ciclo su un clone della lista
        for it in all[:]:
            if it in default:
                # rimuovo gli elementi di default
                all.remove(it)

        allItems = []
        for i in all:
            items = cmd.listRelatives(i, allDescendents=True, fullPath=True)
            if items:
                for p in items:
                    stop = False
                    if p.count('blendshape') or p.count('GRP_place3d'):
                        continue
                    for lock in ['LOC_eyePivot_L', 'LOC_eyePivot_R']:
                        if p.count(lock):
                            stop = True
                    if stop:
                        continue

                    if cmd.objectType(p) == 'transform':
                        # escludo dalla lista i locator degli occhi che non sono
                        # riuscito a riprendere i nomi dal dictionaries chiedere a dora..!
                        allItems.append(p)

        api.log.logger().debug('Items: {}'.format(allItems))

        return allItems

    ########################################################################
    # @brief      check gropu transform.
    #
    # @return     check result.
    #
    def checkGroupTransform(self):
        success = True
        api.log.logger().debug(" === Check group transform ===")
        grp = ''
        if self.getGroupName():
            grp = self.getGroupName()
        else:
            self.errors.append("Warning, there are more top groups in this scene, please check!")
            success = False
        return success

    ########################################################################
    # @brief      check if all transform has 0 value.
    #
    # @return     check result.
    #
    def checkZeroTransform(self):
        success = True
        allStuff = api.scene.getAllTrasform()
        keys = ["translateX", "translateY", "translateZ", "rotateX",
                "rotateY", "rotateZ"]
        scales = ["scaleX", "scaleY", "scaleZ"]
        for item in allStuff:
            api.log.logger().debug('Item: {}'.format(item))
            for key in keys:
                if cmd.getAttr('{}.{}'.format(item, key)) != 0:
                    success = False
                    api.log.logger().error('transform not zero!!')
                    self.errors.append('transform not zero: {} - {}'.format(item, str(key)))
            for scale in scales:
                if cmd.getAttr('{}.{}'.format(item, scale)) != 1:
                    success = False
                    api.log.logger().error('scale not zero!!')
                    self.errors.append('scale not zero: {} - {}'.format(item, str(key)))
        return success

    ########################################################################
    # @brief      check for frozen object
    #
    # @return     check result.
    #
    def checkForFrozenObjects(self):
        api.log.logger().debug(" === checkForFrozenObjects ===")
        success = True
        allStuff = api.scene.getAllTrasform()
        keys = ['transMinusRotatePivotX', 'transMinusRotatePivotY',
                'transMinusRotatePivotZ']
        for item in allStuff:
            for key in keys:
                if cmd.getAttr('{}.{}'.format(item, key)) != 0:
                    success = False
                    api.log.logger().error('Local Space Frozen not zero value!')
                    self.errors.append('transMinusRotatePivot not to zero: {} - {}'.format(item, str(key)))
        return success

    ####################################################################
    # @brief      check if the model has been finalize.
    #
    # @return     check result.
    #
    def checkForModelFinalize(self):
        success = 1
        if cmd.objExists("blendshape"):
            api.log.logger.debug("bs blendshape save")
        else:
            if not cmd.objExists('CTRL_set'):
                if not self.isProp():
                    grp = self.getGroupName()
                    if cmd.objExists('{}.top_group_finalized'.format(grp)):
                        api.log.logger().debug('Ok is a char finalized!')
                        api.log.logger().debug(cmd.getAttr('{}.top_group_finalized'.format(grp)))
                    else:
                        api.log.logger().error('this is a char but is not finalized properly!')
                        self.errors.append('this is a char but is not finalized properly!')
                        success = False
                else:
                    api.log.logger().error('this is a prop not finalized properly!')
                    self.errors.append('this is a prop not finalized properly!')
                    success = False

        return success

    ####################################################################
    # @brief      create the folder for surfacing textures
    #
    def craeteTextureFolders(self):
        subFolders = {"mudbox": ["obj"], "psd": [], "tiff": []}

        sn = api.saveNode.SaveNode()
        sn.load('modeling')

        txtPath = os.path.join(self.projectFolder, "sourceimages").replace('\\', '/')

        for attr in ["genre", "group", "name", "variant"]:
            txtPath = os.path.join(txtPath, sn.getAttr(attr)).replace('\\', '/')

        for sub in subFolders:
            subPathText = os.path.join(txtPath, sub)
            api.log.logger().debug(subPathText)
            try:
                os.makedirs(subPathText)
                for subSubFolder in subFolders[sub]:
                    subSubPathText = os.path.join(subPathText, subSubFolder)
                    api.log.logger().debug(subSubPathText)
                    try:
                        os.makedirs(subSubPathText)
                    except Exception as e:
                        api.log.logger().error(e)
                        pass
            except Exception as e:
                api.log.logger().error(e)
                pass

    ########################################################################
    # @brief      publish on shotgun
    #
    #
    def publishOnShotgun(self):
        if hasattr(self.savedAsset, 'sgUploadPicture') and self.savedAsset.sgUploadPicture:
            try:
                saveNode = cmd.ls('modeling_saveNode')[0]
            except IndexError:
                rbwUI.RBWError(title='ATTENZIONE', text='Il SaveNode non esiste')
                return

            # variabili create con il SaveNode per l'upload su Shotgun della Version
            shotgunId = cmd.getAttr('{}.shotgun_id'.format(saveNode))
            note = cmd.getAttr('{}.note'.format(saveNode))

            assetTasks = self.shotgridObj.taskByPipelineStepByAsset('Modeling', assetId=int(shotgunId))
            if assetTasks:
                if len(assetTasks) == 1:
                    taskName = assetTasks[0]['cached_display_name']
                else:
                    for task in assetTasks:
                        if task['cached_display_name'].lower() != 'blendshape':
                            taskName = task['cached_display_name']
            else:
                rbwUI.RBWError(title='ATTENTION', text='No Task assigned, upload picture on Shotgun failed.\nAsset saved!')
                api.log.logger().error("No Task assigned, upload picture on Shotgun failed. Asset saved!")
                return

            try:
                taskParam = self.shotgridObj.taskByPipelineStepByAsset('Modeling', assetId=int(shotgunId), taskName=taskName)[0]
            except TypeError:
                rbwUI.RBWError(title='ATTENTION', text='Error with Task during upload picture on Shotgun.\nAsset saved!')
                api.log.logger().error("Error with Task during upload picture on Shotgun. Asset saved!")
                return

            entity = {'type': 'Asset', 'id': int(shotgunId)}
            taskId = taskParam['id']
            filename = api.scene.scenename().split('/')[-1]
            filePath = api.scene.scenename().replace(filename, '')
            imagePath = '{}snapshots/{}_persp.jpg'.format(filePath, filename[:-3])

            # upload della nuova Version su Shotgun
            try:
                self.shotgridObj.createAssetVersion(filename[:-3], note, entity, taskId, imagePath, '')
            except:
                rbwUI.RBWError(title='ATTENTION', text='Some errors during upload picture on Shotgun.\nAsset saved!')
                api.log.logger().error("Some errors during upload picture on Shotgun. Asset saved!")
                return

            rbwUI.RBWConfirm(title='SG Upload Picture', text='Correct SG upload picture', defCancel=None)

    ########################################################################
    # @brief      update shotgun delivery status.
    #
    def updateShotgunDeliveryStatus(self):
        api.log.logger().debug("updating sg_delivery_status of {}".format(self.filename))

        if self.shotgridObj.checkAssetDelivered(self.path, self.getSgId()):
            new_status = "dpo"
            self.shotgridObj.updateAssetDeliveryStatus(self.path, new_status, self.getSgId())

    ############################################################################
    # @brief      Saves an alembic too.
    #
    # @return     the alembic path.
    #
    def saveAlembicToo(self):
        openScenePath = cmd.file(q=1, sceneName=1)

        meshes = cmd.ls(type="mesh", readOnly=False)
        readOnlyMeshs = cmd.ls(type="mesh", readOnly=True)

        for mesh in readOnlyMeshs:
            meshes.remove(mesh)

        if meshes:
            cmd.select(meshes, r=1)
            childrens = cmd.ls(sl=1, l=1)
        else:
            return

        if not childrens:
            return

        root = "|".join(childrens[0].split("|")[1:3])

        name = root.split('|')[-1]
        name = name.split(':')[0] + '.abc'

        # --- plugin check
        plug = "AbcExport"
        if not cmd.pluginInfo(plug, q=True, loaded=True):
            cmd.loadPlugin(plug, quiet=True)

        cacheDir = openScenePath.replace((openScenePath.split("/")[-1]), "")
        abcFile = openScenePath.replace(".mb", ".abc")

        if not os.path.isdir(cacheDir):
            os.makedirs(cacheDir)

        cmd.select(childrens, hierarchy=1)

        splittedPath = os.path.split(openScenePath)

        # Viene disabilitato lo smooth dell'oggetto
        cmd.displaySmoothness(du=0, dv=0, pw=4, ps=1, po=1)

        api.log.logger().debug('start alembic gpu.')
        gpuBase = cmd.gpuCache(
            root.split('|')[0],
            startTime=1,
            endTime=1,
            optimize=True,
            optimizationThreshold=512,
            writeMaterials=True,
            dataFormat='ogawa',
            useBaseTessellation=True,
            directory=splittedPath[0],
            f='{}_base'.format(splittedPath[1].replace('.mb', ''))
        )

        api.log.logger().debug('exported alembic gpu.')

        # The export of the smoothed gpucache is performed only for scene that have
        # a specific amount of vertices

        gpuSmooth = gpuBase

        if self.getDeptType() != 'prx' and not self.isSet():
            # smooth only meshes which are not in nosmooth group
            allMeshShapeList = cmd.ls(type='mesh')
            readAllMeshShapeList = cmd.ls(type="mesh", readOnly=True)
            for mesh in readAllMeshShapeList:
                allMeshShapeList.remove(mesh)

            allMeshList = cmd.listRelatives(allMeshShapeList, parent=True)

            for mesh in allMeshList:
                nodePath = cmd.ls(mesh, long=True)[0]
                parentNodesList = nodePath.split('|')
                smoothCheck = True

                for parentNode in parentNodesList:
                    if parentNode == 'nosmooth':
                        smoothCheck = False
                        break

                if smoothCheck:
                    cmd.displaySmoothness(mesh, du=3, dv=3, pw=16, ps=4, po=3)
                else:
                    cmd.displaySmoothness(mesh, du=0, dv=0, pw=4, ps=1, po=1)

            api.log.logger().debug('start alembic smooth gpu.')
            gpuSmooth = cmd.gpuCache(
                root.split('|')[0],
                startTime=1,
                endTime=1,
                optimize=True,
                optimizationThreshold=512,
                writeMaterials=True,
                dataFormat='ogawa',
                directory=splittedPath[0],
                f='{}_smooth'.format(splittedPath[1].replace('.mb', ''))
            )

            api.log.logger().debug('exported alembic smooth gpu.')

        return abcFile, gpuBase, gpuSmooth

    ############################################################################
    # @brief      Creates a msh set.
    #
    # @return     the msh meshes list.
    #
    def createMSHSet(self):
        meshes = cmd.ls('MSH_*', type='shape', long=True)
        mshMeshes = []

        for mesh in meshes:
            if '|proxy|' not in mesh:
                mshMeshes.append(mesh)

        if mshMeshes:
            if not cmd.objExists('MSH'):
                cmd.sets(name='MSH', empty=True)
                mshMembers = []
            else:
                mshMembers = cmd.sets('MSH', query=True)

            for mesh in mshMeshes:
                parent = mesh.split('|')[-2]
                if parent not in mshMembers:
                    cmd.sets(parent, add='MSH')

            return cmd.sets('MSH', query=True)

        else:
            return []
