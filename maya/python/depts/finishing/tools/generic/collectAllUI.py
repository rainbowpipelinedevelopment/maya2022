import importlib
from PySide2 import QtGui, QtCore, QtWidgets
import os

import maya.cmds as cmd

import api.database
import api.scene
import api.shot
import api.widgets.rbw_UI as rbwUI
import depts.finishing.tools.generic.collectAll as collectAll
import config.dictionaries

importlib.reload(api.database)
importlib.reload(api.scene)
# importlib.reload(api.shot)
# importlib.reload(rbwUI)
importlib.reload(collectAll)
importlib.reload(config.dictionaries)


################################################################################
# @brief      This class describes a collect all ui.
#
class CollectAllUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(CollectAllUI, self).__init__()
        if cmd.window('CollectUI', exists=True):
            cmd.deleteUI('CollectUI')

        self.mcFolder = os.getenv('MC_FOLDER')
        self.id_mv = os.getenv('ID_MV')
        self.animationFolder = os.path.join(self.mcFolder, 'scenes', 'animation').replace('\\', '/')
        self.shots = api.system.getShotsFromDB()

        self.setObjectName('CollectUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.centralWidget = rbwUI.RBWFrame(layout='G', radius=5)

        self.shotGroup = rbwUI.RBWGroupBox(title='Shot:', layout='V', fontSize=12, spacing=1)

        self.seasonComboBox = rbwUI.RBWComboBox(text='Season', bgColor='transparent')
        self.episodeComboBox = rbwUI.RBWComboBox(text='Episode', bgColor='transparent')
        self.sequenceComboBox = rbwUI.RBWComboBox(text='Sequence', bgColor='transparent')
        self.shotComboBox = rbwUI.RBWComboBox(text='Shot', bgColor='transparent')

        self.seasonComboBox.activated.connect(self.fillEpisodeComboBox)
        self.episodeComboBox.activated.connect(self.fillSequenceComboBox)
        self.sequenceComboBox.activated.connect(self.fillShotComboBox)
        self.shotComboBox.activated.connect(self.checkCfx)

        self.shotGroup.addWidget(self.seasonComboBox)
        self.shotGroup.addWidget(self.episodeComboBox)
        self.shotGroup.addWidget(self.sequenceComboBox)
        self.shotGroup.addWidget(self.shotComboBox)

        self.centralWidget.layout.addWidget(self.shotGroup, 0, 0)

        self.optionsGroup = rbwUI.RBWGroupBox(title='Options:', layout='V', fontSize=12, spacing=2)

        self.includeVrsceneCheckBox = rbwUI.RBWCheckBox('Include Vrscene')
        self.includeVrsceneCheckBox.setChecked(True)
        self.includeAlembicCheckBox = rbwUI.RBWCheckBox('Include Alembic')
        self.includeAlembicCheckBox.setChecked(False)
        self.includeAlembicCheckBox.hide()
        self.saveCheckBox = rbwUI.RBWCheckBox('Save')

        self.optionsGroup.addWidget(self.includeVrsceneCheckBox)
        self.optionsGroup.addWidget(self.includeAlembicCheckBox)
        self.optionsGroup.addWidget(self.saveCheckBox)

        self.centralWidget.layout.addWidget(self.optionsGroup, 0, 1)

        self.collectAllButton = rbwUI.RBWButton(text='Collect All', icon=['collect.png'], size=[378, 35])
        self.collectAllButton.clicked.connect(self.collectAll)

        self.centralWidget.layout.addWidget(self.collectAllButton, 1, 0, 1, 2)

        self.mainLayout.addWidget(self.centralWidget)

        self.autoFillCombo()
        self.setFixedSize(400, 220)

        self.setTitle('Collect All UI')
        self.setFocus()

    ############################################################################
    # @brief      fill the shot season combo box.
    #
    def fillSeasonComboBox(self):
        self.seasonComboBox.clear()
        seasons = sorted(self.shots.keys())

        self.seasonComboBox.addItems(seasons)

        if len(seasons) == 1:
            self.seasonComboBox.setCurrentIndex(0)
            self.fillEpisodeComboBox()

    ############################################################################
    # @brief      fill the episode combo box.
    #
    def fillEpisodeComboBox(self):
        self.currentSeason = self.seasonComboBox.currentText()

        self.episodeComboBox.clear()
        episodes = sorted(self.shots[self.currentSeason].keys())

        self.episodeComboBox.addItems(episodes)

        if len(episodes) == 1:
            self.episodeComboBox.setCurrentIndex(0)
            self.fillSequenceComboBox()

    ############################################################################
    # @brief      fill the sequence combo box.
    #
    def fillSequenceComboBox(self):
        self.currentEpisode = self.episodeComboBox.currentText()

        self.sequenceComboBox.clear()
        sequences = sorted(self.shots[self.currentSeason][self.currentEpisode].keys())

        self.sequenceComboBox.addItems(sequences)

        if len(sequences) == 1:
            self.sequenceComboBox.setCurrentIndex(0)
            self.fillShotComboBox()

    ############################################################################
    # @brief      fill the shot combo box.
    #
    def fillShotComboBox(self):
        self.currentSequence = self.sequenceComboBox.currentText()

        self.shotComboBox.clear()
        shots = sorted(self.shots[self.currentSeason][self.currentEpisode][self.currentSequence].keys())

        self.shotComboBox.addItems(shots)

        if len(shots) == 1:
            self.shotComboBox.setCurrentIndex(0)

    ############################################################################
    # @brief      auto fill of all the combos.
    #
    def autoFillCombo(self):
        fileName = api.scene.scenename()
        if self.animationFolder in fileName:
            try:
                relPath = fileName.split(self.animationFolder)[1]
                season = relPath.split('/')[1]
                episode = relPath.split('/')[2]
                sequence = relPath.split('/')[3]
                shot = relPath.split('/')[4]

                self.fillSeasonComboBox()
                seasonIndex = self.seasonComboBox.findText(season, QtCore.Qt.MatchFixedString)
                self.seasonComboBox.setCurrentIndex(seasonIndex)

                self.fillEpisodeComboBox()
                episodeIndex = self.episodeComboBox.findText(episode, QtCore.Qt.MatchFixedString)
                self.episodeComboBox.setCurrentIndex(episodeIndex)

                self.fillSequenceComboBox()
                sequenceIndex = self.sequenceComboBox.findText(sequence, QtCore.Qt.MatchFixedString)
                self.sequenceComboBox.setCurrentIndex(sequenceIndex)

                self.fillShotComboBox()
                shotIndex = self.shotComboBox.findText(shot, QtCore.Qt.MatchFixedString)
                self.shotComboBox.setCurrentIndex(shotIndex)
                self.checkCfx()

            except:
                self.fillSeasonComboBox()
        else:
            self.fillSeasonComboBox()

    ############################################################################
    # @brief      check if cfx folder exists.
    #
    def checkCfx(self):
        self.currentShot = self.shotComboBox.currentText()
        self.shotObj = api.shot.Shot(params={'season': self.currentSeason, 'episode': self.currentEpisode, 'sequence': self.currentSequence, 'shot': self.currentShot, 'id_mv': self.id_mv})

        cfxFolder = os.path.join(self.shotObj.getShotFolder(), 'Cfx').replace('\\', '/')

        if os.path.exists(cfxFolder):
            self.includeAlembicCheckBox.setChecked(True)
            self.includeAlembicCheckBox.show()
        else:
            self.includeAlembicCheckBox.setChecked(False)
            self.includeAlembicCheckBox.hide()

    ############################################################################
    # @brief      collect all function
    #
    def collectAll(self):
        includeVrscene = self.includeVrsceneCheckBox.isChecked()
        includeAlembic = self.includeAlembicCheckBox.isChecked()
        save = self.saveCheckBox.isChecked()

        collectAll.collectAll(self.shotObj, includeVrscene, includeAlembic, save)
