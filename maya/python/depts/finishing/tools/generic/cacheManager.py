import importlib
import re
import os
from xml.dom import minidom

import maya.cmds as cmd
import maya.mel as mel
import xgenm
import xgenm.xgGlobal as xgGlobal
import xgenm.XgExternalAPI as xgext

import api.log
import api.farm
import api.scene
import api.export
import api.asset
import api.savedAsset
import api.database
import api.widgets.rbw_UI as rbwUI
import common.makeButton.makeButton as makeButton
import startup
import lookdev.lookdevTools as lookdevTools

importlib.reload(api.log)
importlib.reload(api.farm)
importlib.reload(api.scene)
importlib.reload(api.export)
# importlib.reload(api.asset)
# importlib.reload(api.savedAsset)
importlib.reload(api.database)
# importlib.reload(rbwUI)
importlib.reload(makeButton)
importlib.reload(startup)
importlib.reload(lookdevTools)


mcLibrary = "//speed.nas/library/02_production/01_content/mc_library"


################################################################################
# @brief      isolate panel
#
# @param      isolate  The isolate
#
def isolatePanel(isolate=1):
    panel = mel.eval("getPanel -wf")

    melCmd = ("getPanel -to {}".format(panel))
    if (mel.eval(melCmd) == "modelPanel"):
        activeModelPanel = panel
    else:
        activeModelPanel = "modelPanel4"

    melCmd = (
        'enableIsolateSelect {}  {}'.format(str(activeModelPanel), str(isolate))
    )
    mel.eval(melCmd)


################################################################################
# @brief      Gets the shapes from set.
#
# @param      mayaSet  The maya set
#
# @return     The shapes from set and a count of those.
#
def getShapesFromSet(mayaSet, alembic=False, combine=False):
    if not combine:
        cmd.select(mayaSet, replace=True)
        childs = cmd.ls(selection=1)

        shapes = []
        count = 0

        for shape in childs:
            count = count + 1

            cmd.select(shape, replace=True)

            if cmd.objectType(shape) == "mesh":
                pass
            elif cmd.objectType(shape) == "transform":
                # with fullpath enabled, avoid duplicated names
                itemShape = cmd.listRelatives(shape, children=True, shapes=True, noIntermediate=False, fullPath=True)
                if itemShape:
                    if alembic:
                        shape = itemShape[0]
                    else:
                        shape = itemShape[1]
            else:
                continue

            if cmd.objExists(shape):
                shapes.append(shape)
    else:
        cmd.select(clear=True)
        mayaSet = mayaSet.replace('RND', '')
        if cmd.objExists('{}:no_shadows'.format(mayaSet.split(':')[0])):
            mshMeshes = cmd.listRelatives(mayaSet, children=True, fullPath=True)
            noShadowMeshes = cmd.listRelatives('{}:no_shadows'.format(mayaSet.split(':')[0]), children=True, fullPath=True)

            meshes = []
            for mesh in mshMeshes:
                if mesh not in noShadowMeshes:
                    meshes.append(mesh)

            if len(meshes) > 1:
                # newMesh = cmd.polyUniteSkinned(meshes, constructionHistory=True, mergeUVSets=1)[0]
                # newMesh = cmd.rename(newMesh, '{}:MSH_combine'.format(mayaSet.split(':')[0]))
                newMesh = cmd.polyUnite(meshes, constructionHistory=True, mergeUVSets=True, centerPivot=True, name='{}:MSH_combine'.format(mayaSet.split(':')[0]))[0]
                shapes = [cmd.listRelatives(newMesh, children=True, shapes=True, noIntermediate=False, fullPath=True)[0]]
                count = 1
            else:
                shapes = [cmd.listRelatives(meshes[0], children=True, shapes=True, noIntermediate=False, fullPath=True)[0]]
                count = 1

            if len(noShadowMeshes) > 1:
                # newNoShadowMesh = cmd.polyUniteSkinned(noShadowMeshes, constructionHistory=True, mergeUVSets=1)[0]
                # newNoShadowMesh = cmd.rename(newNoShadowMesh, '{}:MSH_noShadowCombine'.format(mayaSet.split(':')[0]))
                newNoShadowMesh = cmd.polyUnite(noShadowMeshes, constructionHistory=True, mergeUVSets=True, centerPivot=True, name='{}:MSH_noShadowCombine'.format(mayaSet.split(':')[0]))[0]
                shapes.append(cmd.listRelatives(newNoShadowMesh, children=True, shapes=True, noIntermediate=False, fullPath=True)[0])
                count = 2
            else:
                shapes.append(cmd.listRelatives(noShadowMeshes[0], children=True, shapes=True, noIntermediate=False, fullPath=True)[0])
                count = 2

        else:
            [cmd.select(x, add=True) for x in cmd.listRelatives(mayaSet, children=True, fullPath=True)]
            sel = cmd.ls(sl=True)
            if len(sel) > 1:
                newMesh = cmd.polyUniteSkinned(sel, constructionHistory=True, mergeUVSets=1)[0]
                newMesh = cmd.rename(newMesh, '{}:MSH_combine'.format(mayaSet.split(':')[0]))
                shapes = [cmd.listRelatives(newMesh, children=True, shapes=True, noIntermediate=False, fullPath=True)[0]]
                count = 1

    return shapes, count


################################################################################
# @brief      { function_description }
#
# @param      assetDict  The asset dictionary
#
def exportMaster(assetDict, fx=False):
    if fx:
        saveNode = '{}:fx_saveNode'.format(assetDict['namespace'])
    else:
        saveNode = '{}:rigging_saveNode'.format(assetDict['namespace'])

    topGroup = cmd.listConnections(saveNode)[0]
    cmd.select(topGroup)

    if not fx:
        surfacingSaveNode = '{}RND:surfacing_saveNode'.format(assetDict['namespace'])
        surfTopGroup = cmd.listConnections(surfacingSaveNode)[0]

        cmd.select(surfTopGroup, add=True)

    if not os.path.exists(assetDict['masterDir']):
        version = 'vr001'
    else:
        versions = [int(dir.split('vr')[1]) for dir in os.listdir(assetDict['masterDir']) if re.match(r'vr\d{3}', dir)]
        versions.sort()
        version = 'vr{}'.format(str(versions[-1] + 1).zfill(3))

    masterFolder = os.path.join(assetDict['masterDir'], version).replace('\\', '/')
    os.makedirs(masterFolder)

    sceneName = api.scene.scenename()
    exportFullPath = os.path.join(masterFolder, '{}_{}.ma'.format(assetDict['namespace'], version))
    api.export.export_ma(exportFullPath, selected=True, preserveReferences=True)

    if not fx:
        referencePath = cmd.referenceQuery('{}RND:MSH'.format(assetDict['namespace']), f=True)
        cmd.file(referencePath, rr=True)
        fosterParents = cmd.ls(type='fosterParent')
        if fosterParents:
            cmd.delete(fosterParents)
    cmd.file(rename=sceneName)


################################################################################
# @brief      Saves a master.
#
# @param      assetDict  The asset dictionary
#
def saveMaster(assetDict):
    if not os.path.exists(assetDict['masterDir']):
        version = 'vr001'
    else:
        versions = [int(dir.split('vr')[1]) for dir in os.listdir(assetDict['masterDir']) if re.match(r'vr\d{3}', dir)]
        versions.sort()
        version = 'vr{}'.format(str(versions[-1] + 1).zfill(3))

    masterFolder = os.path.join(assetDict['masterDir'], version).replace('\\', '/')
    os.makedirs(masterFolder)

    savePath = os.path.join(assetDict['masterDir'], version, '{}_{}.ma'.format(assetDict['namespace'], version))
    cmd.file(rename=savePath)
    api.log.logger().debug("Saving to {}".format(savePath))
    cmd.file(save=True, force=True, type='mayaAscii')


################################################################################
# @brief      Load a latest master.
#
# @param      assetDict  The asset dictionary
# @param      shotObj    The shot object
#
def loadLatestMaster(assetDict, shotObj):
    masterDir = assetDict['masterDir']
    if os.path.exists(masterDir):
        versions = [dir for dir in os.listdir(masterDir) if dir.startswith('vr')]
        if len(versions) > 0:
            versions.sort()
            version = versions[-1]

        maFile = os.path.join(assetDict['masterDir'], version, '{}_{}.ma'.format(assetDict['namespace'], version)).replace('\\', '/')
        if os.path.exists(maFile):
            cmd.file(new=True, force=True)
            cmd.file(maFile, open=True, force=True)

            startFrame = 1
            endFrame = shotObj.getShotDuration()

            cmd.playbackOptions(ast=startFrame)
            cmd.playbackOptions(aet=endFrame)
            cmd.playbackOptions(min=startFrame)
            cmd.playbackOptions(max=endFrame)


################################################################################
# @brief      export maya cache.
#
# @param      assetDict  The asset info dict
# @param      shotObj    The shot object
#
def exportMayaCache(assetDict, shotObj):
    mshSet = '{}RND:MSH'.format(assetDict['namespace'])
    if cmd.objExists(mshSet):
        cmd.select(mshSet, replace=True)
        childs = cmd.ls(selection=1)
        count = len(childs)
        cmd.select(clear=True)

    if count > 0:
        jobName = '{}_{}_{}'.format(shotObj.getShotCode(), assetDict['namespace'], os.path.basename(assetDict['cacheDir']))

        exportVrmeshParams = {
            'frames': '{}-{}'.format(0, int(cmd.playbackOptions(query=True, maxTime=True)) + 1),
            'mayaSet': mshSet,
            'jobName': jobName,
            'batchName': '{}_{}_{}_{}_{}_EXPORT_VRMESH'.format(os.getenv('PROJECT').upper(), shotObj.season, shotObj.episode, shotObj.sequence, shotObj.shot),
            'goal': 'export vrmesh',
            'inputFileName': api.scene.scenename(),
            'outputFileName': assetDict['cacheDir'],
            'obj': shotObj
        }

        api.farm.createAndSubmitJob(exportVrmeshParams)


################################################################################
# @brief      attach in blendshape rnd to rig
#
# @param      namespace  The namespace
#
def blendRigAndRnd(namespace):
    rigMeshes = cmd.sets('{}:MSH'.format(namespace), query=True)
    rndMeshes = cmd.sets('{}RND:MSH'.format(namespace), query=True)

    rigMeshes.sort()
    rndMeshes.sort()

    for i in range(0, len(rigMeshes)):
        rigMesh = rigMeshes[i]
        rndMesh = rndMeshes[i]

        cmd.select(rigMesh)
        cmd.select(rndMesh, add=True)
        cmd.blendShape(name="{}_blendshape".format(rigMesh.split(':')[1]), origin="world", weight=[0, 1.0])
        cmd.select(clear=True)


################################################################################
# @brief      { function_description }
#
# @param      assetDict  The asset dictionary
#
def exportMeshcut(assetDict):
    cmd.select(clear=True)

    if cmd.objExists("{}:meshcut".format(assetDict['namespace'])):
        cmd.select("{}:meshcut".format(assetDict['namespace']), add=True)

    if cmd.objExists("meshcut"):
        cmd.select("meshcut", add=True)

    childs = cmd.ls(selection=True, long=True)

    # Discard from selection the not-visible meshes
    api.log.logger().debug("Discarding not visible meshcuts for the alembic export...")
    notVisibleRMlist = api.scene.getVisRM()[1]

    for obj in childs:
        childLeafName = obj.split('|')[-1]
        if childLeafName in notVisibleRMlist:
            cmd.select(obj, deselect=True)

    childrens = cmd.listRelatives(children=1, fullPath=1)

    if childrens:
        smoothItems = []
        for child in childrens:
            if cmd.polyEvaluate(child, face=True) <= 600:
                api.log.logger().debug("child: {}".format(child))
                if cmd.getAttr("{}.intermediateObject".format(child)) == 0:
                    smoothItem = mel.eval("polySmooth  -mth 0 -sdt 0 -ovb 1 -ofb 3 -ofc 0 -ost 1 -ocr 0 -dv 2 -bnr 1 -c 1 -kb 0 -ksb 1 -khe 0 -kt 1 -kmb 0 -suv 1 -peh 0 -sl 1 -dpe 1 -ps 0.1 -ro 1 -ch 1 {};".format(child))[0]
                    smoothItems.append(smoothItem)

        root = "|".join(childrens[0].split("|")[1:3])
        rootsToCheckList = []
        rootsToCheckList.append(root)

        for child in childrens:
            newRoot = "|".join(child.split("|")[1:3])

            if newRoot in rootsToCheckList:
                continue
            else:
                root += ' -root ' + newRoot
                rootsToCheckList.append(newRoot)

        meshcutPath = assetDict['cacheDir'].replace("/vrmesh/", "/meshcut/")
        if not os.path.isdir(meshcutPath):
            os.makedirs(meshcutPath)

        meshcutFile = os.path.join(meshcutPath, '{}.abc'.format(assetDict['namespace'])).replace('\\', '/')

        cmd.select(childrens, hierarchy=1)

        startFrame = (cmd.playbackOptions(q=1, ast=1)) - 1.0
        endFrame = (cmd.playbackOptions(q=1, aet=1)) + 1.0

        alembicCmd = "-selection -root {} -frameRange {} {} -wholeFrameGeo -writeColorSets -uvWrite -file {}".format(root, startFrame, endFrame, meshcutFile)
        cmd.AbcExport(j=alembicCmd, verbose=True)

        if len(smoothItems) > 0:
            cmd.delete(smoothItems)


################################################################################
# @brief      export alembic with substeps.
#
# @param      assetDict  The asset info dict
# @param      subSteps   The sub steps
#
def exportAlembicWithSubSteps(assetDict, subSteps):
    abcCacheDir = os.path.dirname(assetDict['cacheDir'].replace('/vrmesh/', '/alembic/')).replace('\\', '/')
    if not os.path.isdir(abcCacheDir):
        os.makedirs(abcCacheDir)
        version = 0
    else:
        versions = [dir for dir in os.listdir(abcCacheDir) if dir.startswith('vr')]
        versions.sort()
        if len(versions) > 0:
            version = int(versions[-1].split('vr')[1])

    abcCacheDir = os.path.join(abcCacheDir, 'vr{}'.format(str(version + 1).zfill(3))).replace('\\', '/')
    if not os.path.isdir(abcCacheDir):
        os.makedirs(abcCacheDir)

    abcCacheFile = os.path.join(abcCacheDir, '{}.abc'.format(assetDict['namespace'])).replace('\\', '/')

    plug = "AbcExport"
    if not cmd.pluginInfo(plug, q=True, l=True):
        cmd.loadPlugin(plug, qt=True)

    cmd.select("{}:MSH".format(assetDict['namespace']))

    childrens = cmd.listRelatives(children=1, fullPath=1)
    if not childrens:
        return

    cmd.select(childrens, hierarchy=1)

    root = "|".join(childrens[0].split("|")[1:3])

    startFrame = (cmd.playbackOptions(q=1, ast=1)) - 1.0
    endFrame = (cmd.playbackOptions(q=1, aet=1)) + 1.0

    if subSteps != 1.0:
        alembicCmd = '''
                -selection {} -frameRange {} {} -step {} -uvWrite -writeColorSets -writeFaceSets -worldSpace -wholeFrameGeo -writeUVSets -writeVisibility True -dataFormat ogawa -file {}
                '''.format(root, startFrame, endFrame, subSteps, abcCacheFile)
    else:
        alembicCmd = '''
                -selection {} -frameRange {} {} -uvWrite -writeColorSets -writeFaceSets -worldSpace -wholeFrameGeo -writeUVSets -writeVisibility True -dataFormat ogawa -file {}
                '''.format(root, startFrame, endFrame, abcCacheFile)

    cmd.AbcExport(j=alembicCmd, verbose=True)


################################################################################
# @brief      export alembic from MSH set
#
# @param      assetDict     The asset dictionary
# @param      deselectList  The deselect list
#
def exportAlembic(assetDict, deselectList=None, combine=False):
    mshChildren, count = getShapesFromSet('{}RND:MSH'.format(assetDict['namespace']), alembic=True, combine=combine)
    if deselectList:
        for mesh in deselectList:
            fullPathMesh = cmd.listRelatives('{}RND:{}'.format(assetDict['namespace'], mesh), children=True, shapes=True, noIntermediate=False, fullPath=True)[0]
            mshChildren.remove(fullPathMesh)

    startFrame = (cmd.playbackOptions(q=1, ast=1)) - 1.0
    endFrame = cmd.playbackOptions(q=1, aet=1) + 1

    alembicFolder = assetDict['cacheDir'].replace('/vrmesh/', '/alembic/')
    if not os.path.isdir(alembicFolder):
        os.makedirs(alembicFolder)

    if not combine:
        alembicFile = os.path.join(alembicFolder, '{}.abc'.format(assetDict['namespace'])).replace('\\', '/')
        roots_args = ["-root {}".format('|'.join(child.split('|')[:-1])) for child in mshChildren]
        roots_args = " ".join(roots_args)

        alembicCommand = '{} -frameRange {} {} -uvWrite -writeColorSets -writeFaceSets -worldSpace -wholeFrameGeo -writeUVSets -writeVisibility True -dataFormat ogawa -file {}'.format(roots_args, startFrame, endFrame, alembicFile)
        cmd.AbcExport(j=alembicCommand, verbose=True)
    else:
        if count == 2:
            alembicFile = os.path.join(alembicFolder, '{}.abc'.format(assetDict['namespace'])).replace('\\', '/')
            roots_args = ["-root {}".format('|'.join(child.split('|')[:-1])) for child in mshChildren[:1]]
            roots_args = " ".join(roots_args)

            alembicCommand = '{} -frameRange {} {} -uvWrite -writeColorSets -writeFaceSets -worldSpace -wholeFrameGeo -writeUVSets -writeVisibility True -dataFormat ogawa -file {}'.format(roots_args, startFrame, endFrame, alembicFile)
            cmd.AbcExport(j=alembicCommand, verbose=True)

            noShadowAlembicFile = os.path.join(alembicFolder, '{}_noShadow.abc'.format(assetDict['namespace'])).replace('\\', '/')
            noShadowRoots_args = ["-root {}".format('|'.join(child.split('|')[:-1])) for child in mshChildren[1:]]
            noShadowRoots_args = " ".join(noShadowRoots_args)

            noShadowAlembicCommand = '{} -frameRange {} {} -uvWrite -writeColorSets -writeFaceSets -worldSpace -wholeFrameGeo -writeUVSets -writeVisibility True -dataFormat ogawa -file {}'.format(noShadowRoots_args, startFrame, endFrame, noShadowAlembicFile)
            cmd.AbcExport(j=noShadowAlembicCommand, verbose=True)

        else:
            alembicFile = os.path.join(alembicFolder, '{}.abc'.format(assetDict['namespace'])).replace('\\', '/')
            roots_args = ["-root {}".format('|'.join(child.split('|')[:-1])) for child in mshChildren]
            roots_args = " ".join(roots_args)

            alembicCommand = '{} -frameRange {} {} -uvWrite -writeColorSets -writeFaceSets -worldSpace -wholeFrameGeo -writeUVSets -writeVisibility True -dataFormat ogawa -file {}'.format(roots_args, startFrame, endFrame, alembicFile)
            cmd.AbcExport(j=alembicCommand, verbose=True)


################################################################################
# @brief      export mesh visibility from scene.
#
# @param      assetDict  The asset dictionary
#
def exportMeshVisibility(assetDict):
    namespace = assetDict['namespace']
    meshes = [mesh.split(':')[1] for mesh in cmd.sets('{}:MSH'.format(namespace), query=True)]

    data = {}
    for mesh in meshes:
        data[mesh] = {'visibility': []}

    start = int(cmd.playbackOptions(query=True, minTime=True))
    end = int(cmd.playbackOptions(query=True, maxTime=True)) + 1

    for i in range(start, end):
        cmd.currentTime(i)
        visibleObjctes = [obj.split(':')[1] for obj in cmd.ls('{}:*'.format(namespace), visible=True)]
        for mesh in meshes:
            if mesh in visibleObjctes:
                data[mesh]['visibility'].append(True)
            else:
                data[mesh]['visibility'].append(False)

    for mesh in data:
        values = data[mesh]['visibility']
        if values.count(values[0]) == len(values):
            data[mesh]['visibility'] = values[0]

    jsonPath = os.path.join(assetDict['cacheDir'], '{}visibility.json'.format(namespace)).replace('\\', '/')

    api.json.json_write(data, jsonPath)


################################################################################
# @brief      export multiswitch data for the given asset
#
# @param      assetDict  The asset dictionary
#
def exportMultiswitch(assetDict):
    animLayers = cmd.ls(type='animLayer')

    if len(animLayers) > 1:
        api.log.logger().debug('There are multiple animation layers in scene, merging them into one...')
        cmd.optionVar(intValue=('animLayerMergeDeleteLayers', 1))
        cmd.optionVar(floatValue=('animLayerMergeByTime', 1.0))
        cmd.optionVar(intValue=('animLayerMergeUp', 1))

        mel.eval('animLayerMerge {"%s"}' % '", "'.join(animLayers))
        api.log.logger().debug('Animation layers merge completed!')

    multiswitchSet = '{}:multiswitch'.format(assetDict['namespace'])
    msControlList = []
    data = {}
    attrToIgnore = ['visibility']

    if cmd.objExists(multiswitchSet):
        for element in cmd.sets(multiswitchSet, query=True):
            if cmd.objExists(element + '.multiswitch'):
                msControlList.append(element)

    api.log.logger().debug("msControlList: {}".format(msControlList))
    if len(msControlList) > 0:
        for msControl in msControlList:
            controlData = {}

            for attr in cmd.listAttr(msControl, k=True):
                if attr not in attrToIgnore:
                    msAttr = '{}.{}'.format(msControl, attr)

                    if cmd.keyframe(msControl, attribute=attr, query=True):
                        cmd.bakeResults(msAttr, preserveOutsideKeys=True, sparseAnimCurveBake=False)
                        frames = cmd.keyframe(msControl, query=True, attribute=attr)
                        values = cmd.keyframe(msControl, query=True, attribute=attr, valueChange=True)

                        controlData[attr] = list(zip(frames, values))
                    else:
                        controlData[attr] = cmd.getAttr(msAttr)

            data[msControl] = controlData

        msJsonPath = os.path.join(assetDict['cacheDir'], '{}multiswitch.json'.format(assetDict['namespace'])).replace('\\', '/')
        api.json.json_write(data, msJsonPath)


################################################################################
# @brief      export the master scale data for the given asset
#
# @param      assetDict  The asset dictionary
#
def exportMasterScale(assetDict):
    if cmd.objExists('{}:Transform_Ctrl'.format(assetDict['namespace'])):
        if cmd.objExists('{}:Transform_Ctrl.masterScale'.format(assetDict['namespace'])):
            control = '{}:Transform_Ctrl'.format(assetDict['namespace'])
            attr = 'masterScale'
        else:
            return

    elif cmd.objExists('{}:CNT_constraint'.format(assetDict['namespace'])):
        if cmd.objExists('{}:CNT_constraint.Master_Scale'.format(assetDict['namespace'])):
            control = '{}:CNT_constraint'.format(assetDict['namespace'])
            attr = 'Master_Scale'
        else:
            return

    elif cmd.objExists('{}:CNT_Constraint'.format(assetDict['namespace'])):
        if cmd.objExists('{}:CNT_Constraint.Master_Scale'.format(assetDict['namespace'])):
            control = '{}:CNT_Constraint'.format(assetDict['namespace'])
            attr = 'Master_Scale'
        else:
            return
    else:
        return

    fullAttrName = '{}.{}'.format(control, attr)

    if cmd.objExists(fullAttrName):
        if cmd.keyframe(control, attribute=attr, query=True):
            cmd.bakeResults(fullAttrName, preserveOutsideKeys=True, sparseAnimCurveBake=False)

            frames = cmd.keyframe(control, query=True, attribute=attr)
            values = cmd.keyframe(control, query=True, attribute=attr, valueChange=True)

            data = {'scale': list(zip(frames, values))}
        else:
            data = {'scale': cmd.getAttr(fullAttrName)}

        # convert the dictionary in a json and save
        scaleJsonPath = os.path.join(assetDict['cacheDir'], '{}scale.json'.format(assetDict['namespace'])).replace('\\', '/')
        api.json.json_write(data, scaleJsonPath)


################################################################################
# @brief      export the position of the rig.
#
# @param      assetDict  The asset dictionary
#
def exportRigPositionData(assetDict):
    rigSaveNode = '{}:rigging_saveNode'.format(assetDict['namespace'])
    assetId = cmd.getAttr('{}.asset_id'.format(rigSaveNode))

    lastRndFxQuery = "SELECT path FROM `V_assetList` AS va JOIN `savedAsset` as sa ON va.variantID = sa.assettId " \
        "WHERE `variantID` = '{}' AND `dept` = 'fx' and `deptType` = 'rnd' AND `visible`= 1 ORDER BY `date` DESC".format(assetId)
    try:
        lastRndFxRelPath = api.database.selectSingleQuery(lastRndFxQuery)[0]
    except:
        lastRndFxRelPath = ''

    if lastRndFxRelPath:
        if cmd.objExists('{}:CNT_main'.format(assetDict['namespace'])):
            control = '{}:CNT_main'.format(assetDict['namespace'])
        else:
            control = '{}:CTRL_set'.format(assetDict['namespace'])

        constraints = []

        controlName = control.split(':')[1]
        loc = cmd.spaceLocator(name='loc_{}'.format(controlName))
        parentConstraint = cmd.parentConstraint(control, loc, maintainOffset=False, weight=True)
        scaleConstraint = cmd.scaleConstraint(control, loc, maintainOffset=False, weight=True)
        constraints.append(parentConstraint[0])
        constraints.append(scaleConstraint[0])

        # bake all the locators
        isolatePanel(1)
        inTime = cmd.playbackOptions(query=True, minTime=True)
        outTime = cmd.playbackOptions(query=True, maxTime=True)
        cmd.bakeResults(loc, simulation=1, t=(inTime, outTime), sampleBy=1, disableImplicitControl=1, preserveOutsideKeys=1, sparseAnimCurveBake=0, controlPoints=0, shape=0)
        isolatePanel(0)
        cmd.delete(constraints)
        # export all the locators in a .ma file
        cmd.select(loc, replace=True)
        emitterLocs = '{}position_moving_locators.ma'.format(assetDict['namespace'])
        exportPath = os.path.join(assetDict['cacheDir'], emitterLocs).replace('\\', '/')
        cmd.file(exportPath, es=1, typ="mayaAscii", pr=1, ch=1, chn=1, con=1, exp=1, f=1)
        cmd.delete(loc)
        api.log.logger().debug("The position moving-locators has been exported for: {}".format(assetDict['namespace']))
    else:
        return


################################################################################
# @brief      export scalps geocache
#
# @param      assetDict  The asset dictionary
#
def exportScalpsCache(assetDict):
    rigSavedAsset = api.savedAsset.SavedAsset(params={'db_id': assetDict['rigSavedAsset'], 'approvation': 'def'})

    groomingSAvedAssetParmas = {
        'asset': rigSavedAsset.asset,
        'dept': 'grooming',
        'deptType': 'rnd',
        'approvation': 'def'
    }

    groomingSavedAsset = api.savedAsset.SavedAsset(params=groomingSAvedAssetParmas)
    lastestGroomingSavedAsset = groomingSavedAsset.getLatest()

    if lastestGroomingSavedAsset:
        groomingId = lastestGroomingSavedAsset[0]
    else:
        return

    groomingLatestSavedAsset = api.savedAsset.SavedAsset(params={'db_id': groomingId, 'approvation': 'def'})

    groomingSaveFolder = os.path.join(os.getenv('MC_FOLDER'), groomingLatestSavedAsset.path).replace('\\', '/')
    groomingScalpJson = groomingSaveFolder.replace('.mb', '_scalps.json')

    scalps = api.json.json_read(groomingScalpJson)['scalps']

    startFrame = (cmd.playbackOptions(q=1, ast=1)) - 1.0
    endFrame = (cmd.playbackOptions(q=1, aet=1)) + 1.0

    cacheDir = assetDict['cacheDir'].replace('/vrmesh/', '/geocache/')
    for scalp in scalps:
        transform = cmd.ls('{}:{}'.format(assetDict['namespace'], scalp))[0]
        shape = cmd.listRelatives(transform, children=True, shapes=True, noIntermediate=True, fullPath=True)[0]
        cmd.cacheFile(dir=cacheDir, f=shape.split('|')[-1], st=startFrame, fm="OneFile", et=endFrame, points=shape, sch=1, ws=1)


################################################################################
# @brief      export the hair system
#
# @param      assetDict  The asset dictionary
#
def exportHairCache(assetDict):
    if cmd.objExists("{}:SET_CAC_READER".format(assetDict['namespace'])):
        if cmd.sets("{}:SET_CAC_READER".format(assetDict['namespace']), query=True):
            hairSysSet = "{}:SET_CAC_READER".format(assetDict['namespace'])
            cmd.select(hairSysSet, replace=True)
            items = cmd.ls(selection=True)
            childs = []
            for item in items:
                child = cmd.listRelatives(item, children=True, noIntermediate=True, fullPath=True)
                if child:
                    if cmd.objectType(child[0]) == "nurbsSurface":
                        childs.append(child[0])
                    elif cmd.objectType(child[0]) == "mesh":
                        childs.append(child[0])

            startFrame = (cmd.playbackOptions(q=1, ast=1)) - 1.0
            endFrame = (cmd.playbackOptions(q=1, aet=1)) + 1.0

            hairnurbsFolder = assetDict['cacheDir'].replace("/vrmesh/", "/hairnurbs/")
            api.log.logger().debug("hairnurbsFolder: {}".format(hairnurbsFolder))
            cmd.select(childs)
            mel.eval('doCreateGeometryCache 6 {{ "3", "{}", "{}", "OneFile", "0", "{}","0","{}","0", "export", "0", "1", "1","0","1","mcx","1" }}'.format(
                startFrame,
                endFrame,
                hairnurbsFolder,
                assetDict['namespace']
            ))
        else:
            return
    else:
        return


################################################################################
# @brief      export Cfx scalps.
#
# @param      assetDict  The asset dictionary
# @param      shotObj    The shot object
#
# @return     some files
#
def exportCfxScalps(assetDict, shotObj):
    itemName = assetDict['itemName']
    namespace = assetDict['namespace']

    dynInfoPath = os.path.join(shotObj.getShotFolder(), 'Fin', 'cfxInitInfos.json').replace('\\', '/')
    dynInfoData = api.json.json_read(dynInfoPath)

    scalps = []
    innerScalps = []
    clusterScalps = []
    shaders = []

    if 'scalps' in dynInfoData['assets'][itemName]['rest']:
        scalps = ['{}RND:{}'.format(namespace, file[:-4]) for file in dynInfoData['assets'][itemName]['rest']['scalps']['files']]
    if 'innerScalps' in dynInfoData['assets'][itemName]['rest']:
        innerScalps = ['{}RND:{}'.format(namespace, file[:-4]) for file in dynInfoData['assets'][itemName]['rest']['innerScalps']['files']]
    if 'clusterScalps' in dynInfoData['assets'][itemName]['rest']:
        clusterScalps = ['{}RND:{}'.format(namespace, file[:-4]) for file in dynInfoData['assets'][itemName]['rest']['clusterScalps']['files']]

    startFrame = (cmd.playbackOptions(q=1, ast=1)) - 1.0
    endFrame = (cmd.playbackOptions(q=1, aet=1)) + 1.0

    scalpFolder = assetDict['cacheDir'].replace("/vrmesh/", "/scalps/")
    if not os.path.exists(scalpFolder):
        os.makedirs(scalpFolder)

    files = []
    innerFiles = []
    clusterFiles = []

    for scalp in scalps:
        alembicName = '{}.abc'.format(scalp.split(':')[1])
        alembicFile = os.path.join(scalpFolder, alembicName).replace('\\', '/')
        alembicCmd = '-root {} -frameRange {} {} -uvWrite -worldSpace -dataFormat ogawa -file {}'.format(scalp, startFrame, endFrame, alembicFile)
        cmd.AbcExport(j=alembicCmd, verbose=True)
        files.append(alembicName)

    for innerScalp in innerScalps:
        alembicName = '{}.abc'.format(innerScalp.split(':')[1])
        alembicFile = os.path.join(scalpFolder, alembicName).replace('\\', '/')
        alembicCmd = '-root {} -frameRange {} {} -uvWrite -worldSpace -dataFormat ogawa -file {}'.format(innerScalp, startFrame, endFrame, alembicFile)
        cmd.AbcExport(j=alembicCmd, verbose=True)
        innerFiles.append(alembicName)

        shape = cmd.listRelatives(innerScalp, shapes=True)[0]
        shadeEng = cmd.listConnections(shape, type='shadingEngine')[0]
        material = cmd.ls(cmd.listConnections(shadeEng), materials=True)[0]
        shaders.append(material)

    for clusterScalp in clusterScalps:
        alembicName = '{}.abc'.format(clusterScalp.split(':')[1])
        alembicFile = os.path.join(scalpFolder, alembicName).replace('\\', '/')
        alembicCmd = '-root {} -frameRange {} {} -uvWrite -worldSpace -dataFormat ogawa -file {}'.format(clusterScalp, startFrame, endFrame, alembicFile)
        cmd.AbcExport(j=alembicCmd, verbose=True)
        clusterFiles.append(alembicName)

        shape = cmd.listRelatives(clusterScalp, shapes=True)[0]
        shadeEng = cmd.listConnections(shape, type='shadingEngine')[0]
        material = cmd.ls(cmd.listConnections(shadeEng), materials=True)[0]
        shaders.append(material)

    return scalpFolder, files, innerFiles, clusterFiles, shaders


################################################################################
# @brief      export guides.
#
# @param      assetDict   The asset dictionary
# @param      guidesType  The guides type
#
# @return     files.
#
def exportGuides(assetDict, guidesType):
    guidesDir = assetDict['cacheDir'].replace("/vrmesh/", "/{}/".format(guidesType))
    if not os.path.exists(guidesDir):
        os.makedirs(guidesDir)

    guidesFiles = []

    descriptionSets = cmd.ls('{}:{}_des*'.format(assetDict['namespace'], guidesType), type='objectSet')
    for descriptionSet in descriptionSets:
        desNumber = descriptionSet.split('_')[-1]
        alembicName = '{}{}.abc'.format(assetDict['namespace'], desNumber)
        alembicFile = os.path.join(guidesDir, alembicName)

        guides = []
        guidesShapes = cmd.listRelatives(descriptionSet, fullPath=True)
        for guideShape in guidesShapes:
            if not guideShape.endswith('ShapeOrig'):
                guides.append('|'.join(guideShape.split('|')[:-1]))

        roots_args = ["-root {}".format(guide) for guide in guides]
        roots_args = " ".join(roots_args)

        startFrame = (cmd.playbackOptions(q=1, ast=1)) - 1.0
        endFrame = (cmd.playbackOptions(q=1, aet=1)) + 1.0

        alembicCmd = '{} -frameRange {} {} -uvWrite -worldSpace -dataFormat ogawa -file {}'.format(roots_args, startFrame, endFrame, alembicFile)
        cmd.AbcExport(j=alembicCmd, verbose=True)

        guidesFiles.append(alembicName)

    return guidesDir, guidesFiles


################################################################################
# @brief      export extra geos.
#
# @param      assetDict  The asset dictionary
# @param      shotObj    The shot object
#
# @return     the folder and the files.
#
def exportExtraGeos(assetDict, shotObj):
    extraGeosFolder = assetDict['cacheDir'].replace("/vrmesh/", "/extraGeos/")
    extraGeosFiles = []
    shaders = []
    if not os.path.exists(extraGeosFolder):
        os.makedirs(extraGeosFolder)

    for geo in cmd.listRelatives('{}RND:dynamic_geos'.format(assetDict['namespace'])):
        if not geo.endswith('ShapeDeformed'):
            shadeEng = cmd.listConnections(geo, type='shadingEngine')[0]
            material = cmd.ls(cmd.listConnections(shadeEng), materials=True)[0]

            parent = cmd.listRelatives(geo, parent=True)[0]
            fileName = parent.split(':')[1]

            shaders.append(material)

            startFrame = (cmd.playbackOptions(q=1, ast=1)) - 1.0
            endFrame = (cmd.playbackOptions(q=1, aet=1)) + 1.0

            alembicFile = os.path.join(extraGeosFolder, '{}.abc'.format(fileName)).replace('\\', '/')
            alembicCommand = '''
                -root {} -frameRange {} {} -uvWrite -writeColorSets -writeFaceSets -worldSpace -wholeFrameGeo -writeVisibility True -dataFormat ogawa -file {}
            '''.format(parent, startFrame, endFrame, alembicFile)

            cmd.AbcExport(j=alembicCommand, verbose=True)
            extraGeosFiles.append('{}.abc'.format(fileName))

    return extraGeosFolder, extraGeosFiles, shaders


################################################################################
# @brief      export Cfx materials.
#
# @param      assetDict  The asset dictionary
# @param      shotObj    The shot object
#
def exportCfx(assetDict, shotObj):
    if cmd.objExists('{}:*Guides_saveNode'.format(assetDict['namespace'])):
        scalpsFolder, scalpsFiles, innerScalpsFiles, clusterScalpsFiles, scalpShaders = exportCfxScalps(assetDict, shotObj)
        jsonData = {}
        shaders = []

        if len(scalpsFiles) > 0:
            jsonData['scalps'] = {'folder': scalpsFolder, 'files': scalpsFiles}
        if len(innerScalpsFiles) > 0:
            jsonData['innerScalps'] = {'folder': scalpsFolder, 'files': innerScalpsFiles}
        if len(clusterScalpsFiles) > 0:
            jsonData['clusterScalps'] = {'folder': scalpsFolder, 'files': clusterScalpsFiles}

        if scalpShaders:
            shaders = shaders + scalpShaders

        for guidesSaveNode in cmd.ls('{}:*Guides_saveNode'.format(assetDict['namespace'])):

            guidesType = guidesSaveNode.split(':')[1].split('_')[0]
            guidesFolder, guidesFiles = exportGuides(assetDict, guidesType)

            alembicFolder = assetDict['cacheDir'].replace("/vrmesh/", "/alembic/")
            if not os.path.exists(alembicFolder):
                os.makedirs(alembicFolder)

            jsonData[guidesType] = {'folder': guidesFolder, 'files': guidesFiles}

        if cmd.objExists('{}RND:dynamic_geos'.format(assetDict['namespace'])):
            extraGeosFolder, extraGeosFiles, extraGeosShader = exportExtraGeos(assetDict, shotObj)
            if len(extraGeosFiles) > 0:
                jsonData['extraGeos'] = {'folder': extraGeosFolder, 'files': extraGeosFiles}
                if extraGeosShader:
                    shaders = shaders + extraGeosShader

        jsonPath = os.path.join(alembicFolder, '{}.json'.format(assetDict['namespace'])).replace('\\', '/')
        api.json.json_write(jsonData, jsonPath)

        if len(shaders) > 0:
            shaderFileName = os.path.join(alembicFolder, '{}.ma'.format(assetDict['namespace'])).replace('\\', '/')
            cmd.select(shaders, replace=True)
            cmd.file(shaderFileName, force=True, options='v=0;', type='mayaAscii', preserveReferences=False, exportSelected=True, constructionHistory=True)


################################################################################
# @brief      export fx alembic into the rig.
#
# @param      assetDict  The asset dictionary
#
def exportFxAlembics(assetDict):
    startFrame = cmd.playbackOptions(q=1, ast=1) - 1.0
    endFrame = cmd.playbackOptions(q=1, aet=1) + 1.0

    restoreMultiswitch(assetDict)

    riggingSaveNode = cmd.ls('{}:rigging_saveNode'.format(assetDict['namespace']))[0]
    riggingSavedAsset = api.savedAsset.SavedAsset(saveNode=riggingSaveNode)

    surfacingSaveNode = cmd.ls('{}RND:surfacing_saveNode'.format(assetDict['namespace']))[0]
    surfacingSavedAsset = api.savedAsset.SavedAsset(saveNode=surfacingSaveNode)

    riggingAlembicInfoJson = os.path.join(os.getenv('MC_FOLDER'), riggingSavedAsset.path.replace('.mb', '_alembic.json')).replace('\\', '/')
    if os.path.exists(riggingAlembicInfoJson):
        riggingAlembicInfo = api.json.json_read(riggingAlembicInfoJson)
        if riggingAlembicInfo:
            surfacingAlembicInfoJson = os.path.join(os.getenv('MC_FOLDER'), surfacingSavedAsset.path.replace('.mb', '_alembic.json')).replace('\\', '/')
            if os.path.exists(surfacingAlembicInfoJson):
                surfacingAlembicInfo = api.json.json_read(surfacingAlembicInfoJson)
                if surfacingAlembicInfo:
                    alembicFolder = assetDict['cacheDir'].replace('/vrmesh/', '/innerAlembic/')
                    if not os.path.exists(alembicFolder):
                        os.makedirs(alembicFolder)

                    for alembic in riggingAlembicInfo:
                        alembicDict = riggingAlembicInfo[alembic]
                        ffd = cmd.listConnections('{}:{}'.format(assetDict['namespace'], alembicDict['objects'][0]['shape']), type='ffd')[0]
                        ffdSet = cmd.listConnections(ffd, type="objectSet")[0]

                        surfAlembicDict = surfacingAlembicInfo[alembic.replace('_low_', '_high_')]
                        rndReferenceMeshes = ['{}RND:{}'.format(assetDict['namespace'], obj['transform']) for obj in surfAlembicDict['objects']]
                        meshes = cmd.duplicate(rndReferenceMeshes, returnRootsOnly=True, upstreamNodes=True)
                        cmd.parent(meshes, world=True)
                        cmd.sets(meshes, add=ffdSet)

                        # take attributes for the export
                        attrs = []
                        rndReferenceShapes = ['{}RND:{}'.format(assetDict['namespace'], obj['shape']) for obj in surfAlembicDict['objects']]
                        for shape in rndReferenceShapes:
                            attributes = cmd.listAttr(shape, read=True, array=True)
                            if attributes:
                                for attr in attributes:
                                    if attr not in attrs:
                                        attrs.append(attr)

                        alembicFile = os.path.join(alembicFolder, surfAlembicDict['proxy'].replace('_vrayproxy', '.abc')).replace('\\', '/')
                        roots_args = ["-root {}".format(mesh) for mesh in meshes]
                        roots_args = " ".join(roots_args)
                        attrs_args = ["-attr {}".format(attr) for attr in attrs]
                        attrs_args = " ".join(attrs_args)

                        # -attr opacity -attr id per prendere gli attributi da houdini
                        alembicCommand = '{} {} -frameRange {} {} -uvWrite -writeColorSets -writeFaceSets -worldSpace -wholeFrameGeo -writeUVSets -writeVisibility -dataFormat ogawa -file {}'.format(roots_args, attrs_args, startFrame, endFrame, alembicFile)
                        api.log.logger().debug('alembicCommand: {}'.format(alembicCommand))
                        cmd.AbcExport(j=alembicCommand, verbose=True)


################################################################################
# @brief      export CNT nodes.
#
# @param      assetDict  The asset dictionary
#
def exportCNTs(assetDict):
    # istruzione per selezionare le due edge del set CNT
    cntSets = ['{}RND:CNT'.format(assetDict['namespace'])]

    for set in cmd.ls('{}RND:CNT*'.format(assetDict['namespace'])):
        if re.match(r"{}RND:CNT_\d{{3}}".format(assetDict['namespace']), set):
            cntSets.append(set)

    rivets = []
    for currentSet in cntSets:
        edges = []
        for elem in cmd.sets(currentSet, q=True):
            if 'Shape' not in elem:
                edges.append(elem)

        # costruisco il rivet
        cmd.select(clear=True)
        cmd.select(edges, replace=True)
        rivet = makeButton.DM_buttonMaker()
        rivet = cmd.rename(rivet, 'RVT_{}'.format(assetDict['namespace']))
        rivets.append(rivet)

    # faccio il bake dei rivet
    for rivet in rivets:
        cmd.bakeResults(rivet, simulation=True, t=(0, int(cmd.playbackOptions(query=True, maxTime=True)) + 1), sampleBy=True, disableImplicitControl=True, preserveOutsideKeys=True, sparseAnimCurveBake=False, controlPoints=False, shape=True)

    # cancellare constraint
    for rivet in rivets:
        aimConstraint = cmd.ls('{}|aim*'.format(rivet))[0]
        cmd.delete(aimConstraint)

    # rimuovere la ref di rnd
    rndReferenceNode = cmd.referenceQuery('{}RND:surfacing_saveNode'.format(assetDict['namespace']), referenceNode=True)
    api.scene.deleteReference(rndReferenceNode)

    # rimuovere la ref rig
    rigReferenceNode = cmd.referenceQuery('{}:rigging_saveNode'.format(assetDict['namespace']), referenceNode=True)
    api.scene.deleteReference(rigReferenceNode)

    # delete all fosterParent
    for fosterParent in cmd.ls(type='fosterParent'):
        cmd.delete(fosterParent)

    # close all in a top group
    cmd.group(empty=True, n='RIVETS')
    for rivet in rivets:
        cmd.parent(rivet, 'RIVETS')

    # export the rivets
    cmd.select('RIVETS', replace=True)
    api.export.export_ma(os.path.join(assetDict['cacheDir'], '{}_rivets.ma'.format(assetDict['namespace'])).replace('\\', '/'))


################################################################################
# @brief      main function of export cache
#
# @param      assetDict  The asset info dict
# @param      shotObj    The shot object
# @param      subSteps   The sub steps
#
def exportCache(assetDict, shotObj, type='default', subSteps=None):
    loadLatestMaster(assetDict, shotObj)

    exportMayaCache(assetDict, shotObj)
    exportCfx(assetDict, shotObj)

    if type != 'default':
        if subSteps:
            exportAlembicWithSubSteps(assetDict, subSteps)
        else:
            deselectList = []
            alembicJson = os.path.join(assetDict['cacheDir'].replace('/vrmesh/', '/alembic/'), '{}.json'.format(assetDict['namespace']))
            if os.path.exists(alembicJson):
                alembicData = api.json.json_read(alembicJson)

                if 'clusterScalps' in alembicData:
                    deselectList = deselectList + [scalp[:-4] for scalp in alembicData['clusterScalps']['files']]

                if 'innerScalps' in alembicData:
                    deselectList = deselectList + [scalp[:-4] for scalp in alembicData['innerScalps']['files']]

                if 'extraGeos' in alembicData:
                    deselectList = deselectList + [geo[:-4] for geo in alembicData['extraGeos']['files']]

            exportAlembic(assetDict, deselectList=deselectList, combine=shotObj.isUeProject())

    exportMeshcut(assetDict)
    exportMeshVisibility(assetDict)
    exportMultiswitch(assetDict)
    exportMasterScale(assetDict)
    exportRigPositionData(assetDict)
    exportScalpsCache(assetDict)
    exportHairCache(assetDict)

    # qui inserisco la novita' per l'export dei CNT (in seguito se servira' la metteremo a aprte con altro)
    loadLatestMaster(assetDict, shotObj)
    exportCNTs(assetDict)

    loadLatestMaster(assetDict, shotObj)
    exportFxAlembics(assetDict)

    cmd.file(new=True, force=True)


################################################################################
# @brief      import latest rnd
#
# @param      assetDict  The asset dictionary
#
def importLatestRnd(assetDict, vrmesh=False):
    rndSavedAsset = api.savedAsset.SavedAsset(params={'db_id': assetDict['surfSavedAsset'], 'approvation': 'def'})

    latestId = rndSavedAsset.getLatest()[0]
    if rndSavedAsset.db_id != latestId:
        latestRndSavedAsset = api.savedAsset.SavedAsset(params={'db_id': latestId, 'approvation': 'def'})
    else:
        latestRndSavedAsset = rndSavedAsset

    namespace = '{}RND'.format(assetDict['namespace'])

    latestRndSavedAsset.load(mode='reference', params={'namespace': namespace, 'vrmesh': vrmesh})


################################################################################
# @brief      change the shaders.
#
# @param      namespace     The namespace
# @param      proxy2Shader  The proxy 2 shader
#
def changeShaders(namespace, proxy2Shader):
    if namespace.split('_')[3] != '':
        meshName = '{}{}{}'.format(namespace.split('_')[1], namespace.split('_')[2].capitalize(), namespace.split('_')[3])
    else:
        meshName = '{}{}'.format(namespace.split('_')[1], namespace.split('_')[2].capitalize())

    for shader in proxy2Shader:
        # pup_tank_body_body_shader
        shaderParams = {'category': 'shader', 'group': 'generic', 'name': '{}{}'.format(shader.split(':')[1].split('_')[2], shader.split(':')[1].split('_')[3].capitalize())}
        if len(shader.split(':')[1].split('_')) > 4:
            shaderParams['variant'] = shader.split(':')[1].split('_')[4].capitalize()
        else:
            shaderParams['variant'] = 'df'

        testString = '{}_{}{}_araknoWrapper_{}_shader'.format(os.getenv('PROJECT'), shaderParams['name'], shaderParams['variant'].capitalize(), meshName)
        if not cmd.objExists(testString):
            bridgeParams = {'asset': {'id_mv': 1, 'category': 'shader', 'group': 'generic', 'name': 'bridge', 'variant': 'df'}, 'dept': 'surfacing', 'deptType': 'rnd', 'approvation': 'def'}
            params = bridgeParams.copy()
            assetParams = params.pop('asset')
            asset = api.asset.Asset(params=assetParams)
            params['asset'] = asset
            bridgeSavedAssets = api.savedAsset.SavedAsset(params=params)

            bridgePath = os.path.join(mcLibrary, bridgeSavedAssets.getLatest()[2]).replace('\\', '/')
            cmd.file(bridgePath, i=True, type="mayaBinary", ignoreVersion=True, mergeNamespacesOnClash=False, pr=True)

            material = 'sl_assetName_araknoWrapper_meshName_shader'

            lookdevTools.renameShadingEngine(material, shaderParams, meshName)
            newMaterialName = lookdevTools.renameMaterial(material, shaderParams, meshName)
            lookdevTools.renameMaterialSubnode(newMaterialName, shaderParams, meshName)
        else:
            newMaterialName = testString

        cmd.connectAttr('{}.outColor'.format(shader), '{}.material_0'.format(newMaterialName.replace('_araknoWrapper_', '_switchOvr_')), force=True)
        for proxyDict in proxy2Shader[shader]:
            cmd.connectAttr('{}.outColor'.format(newMaterialName), '{}.shaders[{}].shadersConnections'.format(proxyDict['proxy'], proxyDict['index']), force=True)
        # <prj>_*_switchOvr_<assetName><assetVariantCapirtaliza><numberIfExists>_shader wildcard


################################################################################
# @brief      Attaches the cache to rnd.
#
# @param      assetDict  The asset info dict
#
def attachCacheToRnd(assetDict, shotObj):
    # post create reference
    namespace = '{}RND'.format(assetDict['namespace'])

    proxies = cmd.ls(type='VRayProxy')

    shader2Proxy = {}
    proxy2Shader = {}
    for proxy in proxies:
        shader2Proxy[proxy] = []
        shaderOverrides = cmd.vrayUpdateProxy(proxy, getShaderSets=True)
        for i in range(0, len(shaderOverrides)):
            shaderOverrideString = cmd.getAttr('{}.shaders[{}].shadersNames'.format(proxy, i))
            shader2Proxy[proxy].append({'index': i, 'shaderOverrideString': shaderOverrideString})
            shader = cmd.defaultNavigation(defaultTraversal=True, destination='{}.shaders[{}].shadersConnections'.format(proxy, i))[0]
            if 'lambert' not in shader:
                if shader in proxy2Shader:
                    proxy2Shader[shader].append({'proxy': proxy, 'index': i})
                else:
                    proxy2Shader[shader] = [{'proxy': proxy, 'index': i}]

    for proxy in proxies:
        vrmeshName = os.path.basename(cmd.getAttr('{}.fileName'.format(proxy)))
        newVrmeshPath = os.path.join(assetDict['cacheDir'], vrmeshName).replace('\\', '/')

        if os.path.exists(newVrmeshPath):
            cmd.setAttr('{}.fileName'.format(proxy), newVrmeshPath, type='string')
            cmd.setAttr('{}.animType'.format(proxy), 0)
            cmd.setAttr('{}.animOffset'.format(proxy), 0)
            cmd.setAttr('{}.animOverride'.format(proxy), 1)
            cmd.setAttr('{}.geomType'.format(proxy), 3)

            for shaderOverrideDict in shader2Proxy[proxy]:
                if shaderOverrideDict['shaderOverrideString'] != 'lambert1':
                    cmd.setAttr('{}.shaders[{}].shadersNames'.format(proxy, shaderOverrideDict['index']), '{}:{}'.format(namespace, shaderOverrideDict['shaderOverrideString']), type='string')

    changeShaders(assetDict['namespace'], proxy2Shader)

    startFrame = 1
    endFrame = shotObj.getShotDuration()

    cmd.playbackOptions(ast=startFrame)
    cmd.playbackOptions(aet=endFrame)
    cmd.playbackOptions(min=startFrame)
    cmd.playbackOptions(max=endFrame)


################################################################################
# @brief      import the innerAlembic files.
#
# @param      assetDict  The asset dictionary
# @param      shotObj    The shot object
#
def importFxAlembic(assetDict, shotObj):
    # post create reference
    namespace = '{}RND'.format(assetDict['namespace'])

    surfacingSaveNode = cmd.ls('{}:surfacing_saveNode'.format(namespace))[0]
    surfacingSavedAsset = api.savedAsset.SavedAsset(saveNode=surfacingSaveNode)
    surfacingAlembicInfoJson = os.path.join(os.getenv('MC_FOLDER'), surfacingSavedAsset.path.replace('.mb', '_alembic.json')).replace('\\', '/')
    if os.path.exists(surfacingAlembicInfoJson):
        surfacingAlembicInfo = api.json.json_read(surfacingAlembicInfoJson)

        innerAlembicFolder = assetDict['cacheDir'].replace('/vrmesh/', '/innerAlembic/')
        api.log.logger().debug('innerAlembicFolder: {}'.format(innerAlembicFolder))

        proxies = []
        for alembic in surfacingAlembicInfo:
            alembicDict = surfacingAlembicInfo[alembic]
            proxies.append('{}:{}'.format(namespace, alembicDict['proxy']))
            for obj in alembicDict['objects']:
                try:
                    cmd.setAttr('{}:{}.visibility'.format(namespace, obj['transform']), 0)
                except RuntimeError:
                    pass

        for proxy in proxies:
            alembicPath = os.path.join(innerAlembicFolder, proxy.split(':')[1].replace('_vrayproxy', '.abc')).replace('\\', '/')
            api.log.logger().debug('alembicPath: {}'.format(alembicPath))
            cmd.setAttr('{}.fileName'.format(proxy), alembicPath, type='string')
            cmd.setAttr("{}.animType".format(proxy), 1)
            cmd.setAttr("{}.animOffset".format(proxy), 0)
            cmd.setAttr("{}.animOverride".format(proxy), 0)
            cmd.setAttr('{}.geomType'.format(proxy), 3)

            # inserire la correzione dei shaderNames in base agli oggetti che ci sono nel proxy, ristabilire i nomi corretti
            rightOverrideNames = cmd.vrayUpdateProxy(proxy, getShaderSets=True)
            for i in range(0, len(rightOverrideNames)):
                cmd.setAttr('{}.shaders[{}].shadersNames'.format(proxy, i), rightOverrideNames[i], type='string')
    else:
        return


################################################################################
# @brief      restore the visibility from json
#
# @param      assetDict  The asset dictionary
#
def restoreVisibility(assetDict):
    jsonPath = os.path.join(assetDict['cacheDir'], '{}visibility.json'.format(assetDict['namespace'])).replace('\\', '/')

    if os.path.exists(jsonPath):
        visibilityData = api.json.json_read(jsonPath)

        cmd.select(cmd.ls(type='VRayProxy'))

        meshes = [proxy.replace('_vrayproxy', '')for proxy in cmd.ls(selection=True)]
        for mesh in meshes:
            meshName = mesh.split(':')[1]
            if meshName in visibilityData:
                meshData = visibilityData[meshName]['visibility']

                if isinstance(meshData, list):
                    for i in range(0, len(meshData)):
                        frame = i + 1
                        cmd.setKeyframe(mesh, time=frame, attribute='visibility', value=meshData[i])
                else:
                    cmd.setAttr('{}.visibility'.format(mesh), meshData)
    else:
        api.log.logger().warning('No visibility file found for this asset: {}'.format(assetDict['itemName']))


################################################################################
# @brief      restore multiswitch value.
#
# @param      assetDict  The asset dictionary
#
# @return     the missing controls
#
def restoreMultiswitch(assetDict):
    missingControls = []

    jsonPath = os.path.join(assetDict['cacheDir'], '{}multiswitch.json'.format(assetDict['namespace'])).replace('\\', '/')
    if os.path.exists(jsonPath):
        multiswitchData = api.json.json_read(jsonPath)

        for control, controlData in multiswitchData.items():
            msRndControl = control.replace(':', 'RND:')

            if cmd.objExists(msRndControl):
                for attr, attrData in controlData.items():
                    if isinstance(attrData, list):
                        for frame, attrValue in attrData:
                            cmd.setKeyframe(msRndControl, time=frame, attribute=attr, value=attrValue)
                    else:
                        try:
                            cmd.setAttr('{}.{}'.format(msRndControl, attr), attrData)
                        except:
                            pass
            else:
                missingControls.append(msRndControl)
    else:
        api.log.logger().warning('No multiswitch file found for this asset: {}'.format(assetDict['itemName']))

    return missingControls


################################################################################
# @brief      restore the master scal data for the given asset
#
# @param      assetDict  The asset dictionary
#
def restoreScale(assetDict):
    scaleJson = os.path.join(assetDict['cacheDir'], '{}scale.json'.format(assetDict['namespace'])).replace('\\', '/')

    if os.path.exists(scaleJson):
        scaleData = api.json.json_read(scaleJson)['scale']

        # shader override
        vrayMtls = cmd.ls('{}RND:*'.format(assetDict['namespace']), type='VRayMtl')
        vrayFastSSS2s = cmd.ls('{}RND:*'.format(assetDict['namespace']), type='VRayFastSSS2')

        for vrayMtl in vrayMtls:
            fogMultInitial = cmd.getAttr('{}.fogMult'.format(vrayMtl))
            bumpMultInitial = cmd.getAttr('{}.bumpMult'.format(vrayMtl))
            if isinstance(scaleData, list):
                for couple in scaleData:
                    frame = couple[0]
                    value = couple[1]

                    if value != 0.0:
                        cmd.setKeyframe(vrayMtl, time=frame, attribute='fogMult', value=(fogMultInitial / value))
                    else:
                        cmd.setKeyframe(vrayMtl, time=frame, attribute='fogMult', value=(fogMultInitial / 0.0000000001))
                    cmd.setKeyframe(vrayMtl, time=frame, attribute='bumpMult', value=(bumpMultInitial * value))
            else:
                cmd.setAttr('{}.fogMult'.format(vrayMtl), (fogMultInitial / scaleData))
                cmd.setAttr('{}.bumpMult'.format(vrayMtl), (bumpMultInitial * scaleData))

        for vrayFastSSS2 in vrayFastSSS2s:
            scatterRadiusInitial = cmd.getAttr('{}.scatterRadiusMult'.format(vrayFastSSS2))
            bumpMultInitial = cmd.getAttr('{}.bumpMult'.format(vrayFastSSS2))
            if isinstance(scaleData, list):
                for couple in scaleData:
                    frame = couple[0]
                    value = couple[1]
                    cmd.setKeyframe(vrayFastSSS2, time=frame, attribute='scatterRadiusMult', value=(scatterRadiusInitial * value))
                    cmd.setKeyframe(vrayFastSSS2, time=frame, attribute='bumpMult', value=(bumpMultInitial * value))
            else:
                cmd.setAttr('{}.scatterRadiusMult'.format(vrayFastSSS2), (scatterRadiusInitial * scaleData))
                cmd.setAttr('{}.bumpMult'.format(vrayFastSSS2), (bumpMultInitial * scaleData))

        # displacement override
        displacements = cmd.ls('{}RND:*'.format(assetDict['namespace']), type='VRayDisplacement')

        for displacement in displacements:
            attrs = cmd.listAttr(displacement)
            if 'vrayDisplacementAmount' in attrs and 'vrayDisplacementShift' in attrs:
                amountInitial = cmd.getAttr('{}.vrayDisplacementAmount'.format(displacement))
                shiftInitial = cmd.getAttr('{}.vrayDisplacementShift'.format(displacement))
                if isinstance(scaleData, list):
                    for couple in scaleData:
                        frame = couple[0]
                        value = couple[1]
                        cmd.setKeyframe('{}.vrayDisplacementAmount'.format(displacement), time=frame, value=(amountInitial * value))
                        cmd.setKeyframe('{}.vrayDisplacementShift'.format(displacement), time=frame, value=(shiftInitial * value))
                else:
                    cmd.setAttr('{}.vrayDisplacementAmount'.format(displacement), (amountInitial * scaleData))
                    cmd.setAttr('{}.vrayDisplacementShift'.format(displacement), (shiftInitial * scaleData))
    else:
        api.log.logger().warning('No scale file found for this asset: {}'.format(assetDict['itemName']))


################################################################################
# @brief      Adds a fx to msh.
#
# @param      assetDict  The asset dictionary
#
def addFxToMsh(assetDict):
    positionMa = os.path.join(assetDict['cacheDir'], '{}position_moving_locators.ma'.format(assetDict['namespace'])).replace('\\', '/')
    if os.path.exists(positionMa):
        rigSavedAsset = api.savedAsset.SavedAsset(params={'db_id': assetDict['rigSavedAsset'], 'approvation': 'def'})
        fxSavedAsset = api.savedAsset.SavedAsset(params={'asset': rigSavedAsset.getAsset(), 'dept': 'fx', 'deptType': 'rnd', 'approvation': 'def'})
        lastRndFxPath = os.path.join(os.getenv('MC_CONTENT'), fxSavedAsset.getLatest()[2]).replace('\\', '/')

        api.scene.createReference(lastRndFxPath, 'MayaBinary', '{}FX'.format(assetDict['namespace']))

        cmd.file(positionMa, i=True, type="mayaAscii", ignoreVersion=True, mergeNamespacesOnClash=False, pr=True)
        control = '{}FX:GRP_FX'.format(assetDict['namespace'])

        if cmd.objExists('loc_CNT_main'):
            importLoc = 'loc_CNT_main'
        else:
            importLoc = 'loc_CTRL_set'

        loc = cmd.rename(importLoc, '{}{}'.format(assetDict['namespace'], importLoc))

        cmd.parentConstraint(loc, control, maintainOffset=0, weight=1)
        cmd.scaleConstraint(loc, control, maintainOffset=0, weight=1)
        api.log.logger().debug("main control transform connected to the moving locator: {}".format(loc))


################################################################################
# @brief      import CNT file.
#
# @param      assetDict  The asset dictionary
#
def importCNTs(assetDict):
    exportSceneName = '{}_rivets.ma'.format(assetDict['namespace'])
    exportFullPath = os.path.join(assetDict['cacheDir'], exportSceneName).replace('\\', '/')

    rivets = api.scene.mergeAsset(exportFullPath)
    topGroup = cmd.listConnections('{}RND:surfacing_saveNode'.format(assetDict['namespace']))[0]

    cmd.parent(rivets, topGroup)


################################################################################
# @brief      Saves a msh file.
#
# @param      assetDict  The asset dictionary
# @param      shotObj    The shot object
#
# @return     the new saved shot object
#
def saveMshFile(assetDict, shotObj):
    savedShotParam = {
        'shot': shotObj,
        'itemName': assetDict['itemName'],
        'dept': 'Fin',
        'deptType': 'msh',
        'note': 'Save from create MSH function'
    }

    savedShot = api.savedShot.SavedShot(params=savedShotParam)

    savedShot.save()

    return savedShot


################################################################################
# @brief      Makes a msh.
#
# @param      assetDict      The asset dictionary
# @param      shotObj        The shot object
#
def makeMsh(assetDict, shotObj):
    cmd.file(new=True, force=True)
    importLatestRnd(assetDict, vrmesh=True)
    attachCacheToRnd(assetDict, shotObj)
    importFxAlembic(assetDict, shotObj)

    restoreVisibility(assetDict)
    missingControls = restoreMultiswitch(assetDict)
    restoreScale(assetDict)
    addFxToMsh(assetDict)

    api.log.logger().debug('missingControls: {}'.format(missingControls))

    importCNTs(assetDict)

    saveMshFile(assetDict, shotObj)


################################################################################
# @brief      Loads a lastest grooming.
#
# @param      assetDict  The asset dictionary
#
# @return     latest grooming saved asset (the file loaded)
#
def loadLastestGrooming(assetDict):
    rndSavedAsset = api.savedAsset.SavedAsset(params={'db_id': assetDict['surfSavedAsset'], 'approvation': 'def'})

    groomingSavedAsset = api.savedAsset.SavedAsset(params={'asset': rndSavedAsset.asset, 'approvation': 'def', 'dept': 'grooming', 'deptType': 'rnd'})
    lastestGroomingSavedAssetId = groomingSavedAsset.getLatest()[0]

    lastestGroomingSavedAsset = api.savedAsset.SavedAsset(params={'db_id': lastestGroomingSavedAssetId, 'approvation': 'def'})

    lastestGroomingSavedAsset.load('load')

    return lastestGroomingSavedAsset


################################################################################
# @brief      Sets the scene information.
#
# @param      shotObj  The shot object
#
def setSceneInfo(shotObj):
    # frame rate
    startup.setMayaFramerate()
    # evaluation mode
    cmd.evaluationManager(mode='parallel')
    # gpu override
    cmd.optionVar(iv=('gpuOverride', 0))
    # vray as render engine
    cmd.setAttr("defaultRenderGlobals.currentRenderer", "vray", type="string")

    # resolution
    resolutionQuery = "SELECT width, height FROM `p_prj_resolution` AS pr JOIN `p_resolution` AS r ON pr.resolutionId = r.id WHERE pr.id_mv = {}".format(os.getenv('ID_MV'))
    resolutionTupla = api.database.selectSingleQuery(resolutionQuery)

    api.vray.setResolution(width=int(resolutionTupla[0]), height=int(resolutionTupla[1]))

    # duration
    duration = shotObj.getShotDuration()

    cmd.playbackOptions(minTime=1)
    cmd.playbackOptions(maxTime=duration)


################################################################################
# @brief      Connects a maya cache.
#
# @param      assetDict  The asset dictionary
#
def connectMayaCache(assetDict):
    scalps = api.xgen.getScalps()

    scalps = list(set(scalps))

    cacheDir = assetDict['cacheDir'].replace('/vrmesh/', '/geocache/')

    for scalp in scalps:
        cachePath = os.path.join(
            cacheDir,
            '{}_{}_Shape.xml'.format(assetDict['namespace'], scalp.split('|')[-1])
        ).replace('\\', '/')

        api.log.logger().debug('cachePath: {}'.format(cachePath))

        cmd.select(scalp, replace=True)
        mel.eval("source import_geocache.mel")
        cacheCmd = "importGeoCache(\"{}\")".format(cachePath)
        api.log.logger().debug('cacheCmd: {}'.format(cacheCmd))
        mel.eval(cacheCmd)

    caches = cmd.ls(type='cacheFile')
    for cache in caches:
        oldPath = cmd.getAttr('{}.cachePath'.format(cache))
        if '//speed.nas/' in oldPath:
            newPath = oldPath.replace('//speed.nas/', '${SPEED_SHARE}/')
            cmd.setAttr('{}.cachePath'.format(cache), newPath, type='string')


############################################################################
# @brief      Attaches the meshcut to grooming.
#
# @param      meshcutPath  The meshcut path
# @param      cullMode     The cull mode
#
def attachMeshcutToGrooming(assetDict, cullMode):
    meshcutPath = os.path.join(assetDict['cacheDir'].replace('/vrmesh/', '/meshcut/'), '{}.abc'.format(assetDict['namespace'])).replace('\\', '/')
    # converting meshcut path to UNIX style and CONVERTING TO STRING
    # (meshcutPath type is unicode but xgen doesn't like it!)
    meshcutPath = str(meshcutPath.replace("\\", "/").replace("\\\\", "//"))

    palettes = api.xgen.listCollection()

    descriptions = api.xgen.listDescription(palettes[0])

    for description in descriptions:
        mesh_cut_fx = xgenm.addFXModule(palettes[0], description, "MeshCutFXModule", "mesh_cut_vrscene")

        if cullMode:
            xgenm.setAttr("cutType", xgext.prepForAttribute("2"), palettes[0], description, mesh_cut_fx)
        else:
            xgenm.setAttr("cutType", xgext.prepForAttribute("0"), palettes[0], description, mesh_cut_fx)

        xgenm.setAttr("meshFile", meshcutPath, palettes[0], description, mesh_cut_fx)

        xgenm.setAttr("cullFlag", xgext.prepForAttribute("False"), palettes[0], description, "RandomGenerator")

    de = xgGlobal.DescriptionEditor
    de.refresh("Full")


################################################################################
# @brief      import the shot camera.
#
# @param      shotObj  The shot object
#
def importShotCamera(shotObj):
    cameraSavedShotParams = {
        'shot': shotObj,
        'dept': 'cam'
    }

    cameraSavedShot = api.savedShot.SavedShot(params=cameraSavedShotParams)

    latestCameraId = cameraSavedShot.getLatest()[0]

    cameraSavedShot = api.savedShot.SavedShot(params={'db_id': latestCameraId})

    cameraSavedShot.load('merge')


################################################################################
# @brief      active camera culling on description.
#
# @param      assetDict  The asset dictionary
#
def activeCulling(assetDict):
    if not cmd.objExists('NO_CULL'):
        api.xgen.enableCullingForEveryDescription()
    else:
        xg_palette = api.xgen.listCollection()[0]
        xgen_descriptions = sorted(api.xgen.listDescription(xg_palette))

        for description in xgen_descriptions:
            if not cmd.sets(description, im='NO_CULL'):
                api.xgen.enableCulling(str(xg_palette), str(description))


################################################################################
# @brief      Sets the descritpion visibility.
#
# @param      assetDict  The asset dictionary
#
def setDescritpionVisibility(assetDict):
    jsonPath = os.path.join(assetDict['cacheDir'], '{}visibility.json'.format(assetDict['namespace'])).replace('\\', '/')
    visibilityData = api.json.json_read(jsonPath)

    scalps = api.xgen.getScalps()
    scalps = list(set(scalps))

    for scalp in scalps:
        scalpName = scalp.split('|')[-1]
        scalpData = visibilityData[scalpName]['visibility']

        connectedDescriptions = cmd.listConnections(scalp, type='xgmSubdPatch')

        if isinstance(scalpData, list):
            for couple in scalpData:
                frame = couple[0]
                value = couple[1]

                for connectedDescription in connectedDescriptions:
                    cmd.setKeyframe(connectedDescription, time=frame, attribute='visibility', value=value)
        else:
            for connectedDescription in connectedDescriptions:
                cmd.setAttr('{}.visibility'.format(connectedDescription), scalpData)


################################################################################
# @brief      Attaches the hair system.
#
# @param      assetDict  The asset dictionary
#
def attachHairSystem(assetDict):
    mayaSetHairs = "SET_CAC_READER"

    cachesFolder = assetDict['cacheDir'].replace('/vrmesh/', '/hairnurbs/')
    api.log.logger().debug('cachesFolder: {}'.format(cachesFolder))
    assetNamespace = assetDict['namespace']

    hairs = False
    if cmd.objExists(mayaSetHairs):
        if cmd.sets(mayaSetHairs, query=True):
            cachePath = os.path.join(cachesFolder, '{}.xml'.format(assetNamespace)).replace('\\', '/')
            if os.path.exists(cachePath):
                hairs = True
            else:
                api.log.logger().warning('No Hair System caches found! Skipping step...')
        else:
            api.log.logger().warning('No Hair System caches found! Skipping step...')
    else:
        api.log.logger().warning('No Hair System caches found! Skipping step...')

    if hairs:
        mel.eval('displayHairCurves "start" 1;')
        cmd.select(cmd.sets(mayaSetHairs, query=True), r=True)
        mel.eval("source import_geocache.mel")
        cacheCmd = "importGeoCache(\"%s\")" % (cachePath)
        api.log.logger().debug("IMPORT HAIR CACHE: {}".format(cacheCmd))
        mel.eval(cacheCmd)
        mel.eval('displayHairCurves "current" 1;')
        selection = cmd.ls(type='pfxHair')
        for object in selection:
            cmd.setAttr('{}.intermediateObject'.format(object), 0)


################################################################################
# @brief      active the lod system on grooming
#
# @param      assetDict  The asset dictionary
# @param      shotObj    The shot object
#
def activeLOD(assetDict, shotObj):
    secondarySavedShotParams = {
        'shot': shotObj,
        'dept': 'Sec',
        'deptType': '',
        'itemName': ''
    }

    secondarySavedShot = api.savedShot.SavedShot(params=secondarySavedShotParams)

    latestSecId = secondarySavedShot.getLatest()[0]

    latestSecondarySavedShot = api.savedShot.SavedShot(params={'db_id': latestSecId})

    jsonPath = os.path.join(os.getenv('MC_FOLDER'), latestSecondarySavedShot.path.replace('.ma', '_distance.json')).replace('\\', '/')

    if os.path.isfile(jsonPath):
        distanceJsonData = api.json.json_read(jsonPath)
        if assetDict['namespace'] in distanceJsonData.keys():
            characterDistance = int(distanceJsonData[assetDict['namespace']])
            camFocal = int(distanceJsonData['camFocal'])

            halfLimitStart = camFocal * 0.6875
            halfLimitEnd = camFocal * 10

            if characterDistance >= halfLimitStart and characterDistance < halfLimitEnd:
                api.xgen.halveDensityToAllDescriptions()
                return True

            elif characterDistance >= halfLimitEnd:
                # in questo caso la vrscene non va proprio calcolata quindi blocchiamo la procedure avvisando l'artista
                distanceMessage = 'L\'asset e\' molto lontano dalla camera quindi non e\' necessario creare la vrscene'
                rbwUI.RBWConfirm(text=distanceMessage, defCancel=None)
                return None

            else:
                # in questo caso l'asset e' troppo vicino sia per spegnere del tutto il groomig sia per dsimezzare le fibre
                return True
        else:
            api.log.logger().warning('No distance info for this asset {}'.format(assetDict['namespace']))
            return True

    else:
        api.log.logger().warning("The distance json file is missing, the procedure will work at max resolution.")
        return True


################################################################################
# @brief      Saves a vrscene file.
#
# @param      assetDict  The asset dictionary
# @param      shotObj    The shot object
#
# @return     the saved shot
#
def saveVrsceneFile(assetDict, shotObj):
    savedShotParam = {
        'shot': shotObj,
        'itemName': assetDict['itemName'],
        'dept': 'Fin',
        'deptType': 'vrscene',
        'note': 'Save from create Master Vrscene function'
    }

    savedShot = api.savedShot.SavedShot(params=savedShotParam)

    savedShot.save()

    return savedShot


################################################################################
# @brief      Creates a vrscene.
#
# @param      assetDict    The asset dictionary
# @param      shotObj      The shot object
# @param      ignoreLod    The ignore lod
# @param      useCullMode  The use cull mode
#
def createVrscene(assetDict, shotObj, ignoreLod, useCullMode):
    cmd.file(new=True, force=True)
    loadLastestGrooming(assetDict)
    setSceneInfo(shotObj)
    connectMayaCache(assetDict)
    attachMeshcutToGrooming(assetDict, useCullMode)
    importShotCamera(shotObj)
    activeCulling(assetDict)
    attachHairSystem(assetDict)
    if not ignoreLod:
        activeLOD(assetDict, shotObj)
    setDescritpionVisibility(assetDict)

    # add velocity pass from vray
    mel.eval("vrayAddRenderElement velocityChannel;")

    savedShot = saveVrsceneFile(assetDict, shotObj)
    if savedShot:
        rbwUI.RBWConfirm(title='Save Complete', text='Master Vrscene save successfully', defCancel=None)


################################################################################
# @brief      export fx caches.
#
# @param      assetDict  The asset dictionary
# @param      shotObj    The shot object
#
def exportFxCache(assetDict, shotObj):
    cmd.file(new=True, force=True)
    loadLatestMaster(assetDict, shotObj)

    startFrame = cmd.playbackOptions(q=1, ast=1) - 1.0
    endFrame = cmd.playbackOptions(q=1, aet=1) + 1.0

    mayaSet = '{}:FX_CACHES'.format(assetDict['namespace'])
    if cmd.objExists(mayaSet):
        attrsToExport = []
        children, count = getShapesFromSet(mayaSet)

        for child in children:
            extraAttrs = cmd.listAttr(child, userDefined=True)
            if extraAttrs:
                for attr in extraAttrs:
                    if attr not in attrsToExport:
                        attrsToExport.append(attr)

        roots_args = ["-root {}".format(cmd.listRelatives(child, parent=True)[0]) for child in children]
        roots_args = " ".join(roots_args)

        attrs_args = ["-attr {}".format(attr) for attr in attrsToExport]
        attrs_args = " ".join(attrs_args)

        alembicFolder = assetDict['cacheDir']
        if not os.path.exists(alembicFolder):
            os.makedirs(alembicFolder)

        alembicFile = os.path.join(alembicFolder, '{}.abc'.format(assetDict['namespace']))

        alembicCommand = "{} -frameRange {} {} {} -wholeFrameGeo -writeColorSets -uvWrite -worldSpace -writeFaceSets -writeUVSets -writeVisibility True -dataFormat ogawa -file {}".format(
            roots_args,
            startFrame,
            endFrame,
            attrs_args,
            alembicFile
        )

        cmd.AbcExport(j=alembicCommand, verbose=True)


################################################################################
# @brief      finilaze the given fx.
#
# @param      assetDict  The asset dictionary
# @param      shotObj    The shot object
#
def finalizeFx(assetDict, shotObj):
    cmd.file(new=True, force=True)
    loadLatestMaster(assetDict, shotObj)

    saveNode = '{}:fx_saveNode'.format(assetDict['namespace'])
    topGroup = cmd.listConnections(saveNode)[0]
    cmd.parent(topGroup, world=True)
    if cmd.objExists('PROP'):
        cmd.delete('PROP')

    try:
        rndFxSavedAsset = api.savedAsset.SavedAsset(params={'db_id': assetDict['surfSavedAsset'], 'approvation': 'def'})
        lastRndFxPath = rndFxSavedAsset.getLatest()[2]
        lastRndFxFullPath = os.path.join(os.getenv('MC_FOLDER'), lastRndFxPath).replace('\\', '/')
        user = rndFxSavedAsset.getLatest()[7]

        # get reference node name
        if cmd.referenceQuery(saveNode, inr=True):
            refNode = cmd.referenceQuery(saveNode, rfn=True)

            # switch reference
            cmd.file(lastRndFxPath, loadReference=refNode)
            cmd.file(lastRndFxPath, edit=True, namespace=':{}FX'.format(assetDict['namespace']))
            fxTextures = getTextureBySavedAsset(lastRndFxFullPath)
            fileNodes = cmd.ls('{}FX:*'.format(assetDict['namespace']), type='file')
            badTextures = {}
            for fileNode in fileNodes:
                path = cmd.getAttr('{}.fileTextureName'.format(fileNode))
                if path.endswith('.png') or path.endswith('.jpg'):
                    badTextures[fileNode] = path
            if len(badTextures) > 0:
                for node in badTextures:
                    texturesName = os.path.basename(badTextures[node])
                    newPath = os.path.join(os.path.dirname(lastRndFxFullPath), 'textures', '{}_tiled.exr'.format(texturesName[:-4]))
                    if os.path.exists(newPath):
                        cmd.setAttr('{}.fileTextureName'.format(node), newPath, type='string')

            sceneFxTextures = getTexturesFromNamespace('{}FX'.format(assetDict['namespace']))

            notImportedTextures = []
            for texture in fxTextures:
                if texture not in sceneFxTextures:
                    notImportedTextures.append(texture)

            if len(notImportedTextures):
                errorMessage = 'Those texture are not imported in scene:\n'
                for texture in notImportedTextures:
                    errorMessage = '{}-t{}\n'.format(errorMessage, texture)
                errorMessage = '{}Ask to {} for fix'.format(errorMessage, user)
                rbwUI.RBWError(text=errorMessage)
                return
            else:
                proxies = cmd.ls(type='VRayProxy')
                proxy2Shader = {}
                for proxy in proxies:
                    shaderOverrides = cmd.vrayUpdateProxy(proxy, getShaderSets=True)
                    for i in range(0, len(shaderOverrides)):
                        shader = cmd.defaultNavigation(defaultTraversal=True, destination='{}.shaders[{}].shadersConnections'.format(proxy, i))[0]
                        if 'lambert' not in shader:
                            if shader in proxy2Shader:
                                proxy2Shader[shader].append({'proxy': proxy, 'index': i})
                            else:
                                proxy2Shader[shader] = [{'proxy': proxy, 'index': i}]

                changeShaders(assetDict['namespace'], proxy2Shader)

                fxSavedShotParams = {
                    'shot': shotObj,
                    'dept': 'Fx',
                    'itemName': assetDict['itemName'],
                    'note': 'Save from finalize Fx'
                }

                fxSavedShot = api.savedShot.SavedShot(params=fxSavedShotParams)
                fxSavedShot.save()

    except api.exception.InvalidSavedAsset:
        rbwUI.RBWError(text='Error searching rnd save for Fx')


################################################################################
# @brief      Gets the texture by saved asset.
#
# @param      path  The path
#
# @return     The texture by saved asset.
#
def getTextureBySavedAsset(path):
    textures = []
    xmlFile = path.replace('.mb', '.xml')
    xmldoc = minidom.parse(xmlFile)
    requiredAssets = xmldoc.getElementsByTagName('required_assetts')
    for i in range(0, len(requiredAssets), 1):
        for elem in requiredAssets[i].childNodes:
            if elem.nodeName.count('texture'):
                sourcePath = elem.getAttribute('path').replace('\\', '/')
                if os.getenv('CONTENT').replace('\\', '/') in sourcePath:
                    folder, fileName = os.path.split(sourcePath)
                    fileInfoList = fileName.split('.')
                    if len(fileInfoList) > 2:
                        padding = fileInfoList[-2]
                        paddingInt = len(padding)
                        paddingString = '#'
                        for i in range(0, paddingInt):
                            paddingString = '{}#'.format(paddingString)
                        fileInfoList[-2] = paddingString
                        textureName = '.'.join(fileInfoList)
                        texturePath = os.path.join(folder, textureName).replace('\\', '/')
                    else:
                        texturePath = sourcePath

                    if texturePath not in textures:
                        textures.append(texturePath)
    return textures


################################################################################
# @brief      Gets the textures from namespace.
#
# @param      namespace  The namespace
#
# @return     The textures from namespace.
#
def getTexturesFromNamespace(namespace):
    textures = []
    fileNodes = cmd.ls('{}:*'.format(namespace), type='file')

    for fileNode in fileNodes:
        sourcePath = cmd.getAttr('{}.fileTextureName'.format(fileNode))
        folder, fileName = os.path.split(sourcePath)
        texturePathInfo = fileName.split('.')
        if len(texturePathInfo) > 2:
            padding = texturePathInfo[-2]
            paddingInt = len(padding)
            paddingString = '#'
            for i in range(0, paddingInt):
                paddingString = '{}#'.format(paddingString)
            texturePathInfo[-2] = paddingString
            textureName = '.'.join(texturePathInfo)
            texturePath = os.path.join(folder, textureName).replace('\\', '/')
        else:
            texturePath = sourcePath

        if texturePath not in textures:
            textures.append(texturePath)

    return textures
