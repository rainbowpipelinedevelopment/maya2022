import importlib
import os

import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI
import api.savedShot
import api.scene
import api.exception
import common.switchAssembly as switchAssembly
import depts.finishing.tools.generic.cacheManager as cacheManager

# importlib.reload(rbwUI)
# importlib.reload(api.savedShot)
importlib.reload(api.scene)
importlib.reload(api.exception)
importlib.reload(switchAssembly)
importlib.reload(cacheManager)


################################################################################
# @brief      export set items function.
#
def exportSetItems():
    selection, savedShot = checkFunction()
    if not selection:
        return

    newItems = switchObjects(selection)

    alembicFolder = os.path.join(savedShot.shot.getShotFolder(), 'Fin', '01_cache', 'setItems').replace('\\', '/')

    alembicFile = exportAlembic(newItems, alembicFolder)

    if alembicFile:
        rbwUI.RBWConfirm(title='Export Alembic Complete', text='Alemibc correctly export in: {}'.format(alembicFile), defCancel=None)
    else:
        rbwUI.RBWError(text='Problems during export')


################################################################################
# @brief      check funxtion.
#
# @return     the selected objects.
#
def checkFunction():
    filename = api.scene.scenename(relative=True)

    try:
        currentSavedShot = api.savedShot.SavedShot(params={'path': filename})
    except api.exception.InvalidSavedShot:
        errorMessage = 'You have to open an official shot'
        rbwUI.RBWError(text=errorMessage)
        return None, None

    if currentSavedShot.dept != 'Fin' or currentSavedShot.deptType != 'set':
        errorMessage = 'You have to open the finishing set shot to use this tool'
        rbwUI.RBWError(text=errorMessage)
        return None, None

    latestSavedShotPath = currentSavedShot.getLatest()[2]
    if currentSavedShot.path != latestSavedShotPath:
        errorMessage = 'You have to open the latest finshing set file'
        rbwUI.RBWError(text=errorMessage)
        return None, None

    selection = cmd.ls(selection=True, type='assemblyReference')
    if len(selection) == 0:
        errorMessage = 'You must select somethings'
        rbwUI.RBWError(text=errorMessage)
        return None, None

    return selection, currentSavedShot


################################################################################
# @brief      switch object in selection
#
# @param      selection  The selection
#
# @return     the switched object lists.
#
def switchObjects(selection):
    surfNewItems = []
    modNewItems = []
    missingSurfItems = []
    missingImportItems = []

    cmd.select(clear=True)
    for assembly in selection:
        cmd.select(assembly, replace=True)
        try:
            newItem = switchAssembly.getRepresentationInScene(representationName='surfacing', onlyImport=True, deleteSource=False, world=False, reference=True, approvation='def')[0]
            surfNewItems.append(newItem)
        except api.exception.InvalidSavedAsset:
            missingSurfItems.append(assembly)

    cmd.select(clear=True)
    if len(missingSurfItems) > 0:
        for assembly in missingSurfItems:
            cmd.select(assembly, replace=True)
            try:
                newItem = switchAssembly.getRepresentationInScene(representationName='cache base', onlyImport=True, deleteSource=False, world=False, reference=True, approvation='def')[0]
                modNewItems.append(newItem)
            except api.exception.InvalidSavedAsset:
                missingImportItems.append(assembly)

    for item in surfNewItems:
        if not cmd.objExists('SURF'):
            cmd.group(empty=True, n='SURF')

        cmd.parent(item, world=True)
        cmd.parent(item, 'SURF')

    for item in modNewItems:
        if not cmd.objExists('MOD'):
            cmd.group(empty=True, n='MOD')

        cmd.parent(item, world=True)
        cmd.parent(item, 'MOD')

    return surfNewItems + modNewItems


################################################################################
# @brief      export alembic
#
# @param      items          The items
# @param      alembicFolder  The alembic folder
#
def exportAlembic(items, alembicFolder):
    mshChildren = []
    for item in items:
        mshSet = '{}:MSH'.format(item.split(':')[0])
        mshChildren = mshChildren + cacheManager.getShapesFromSet(mshSet, alembic=True, combine=False)[0]

    if not os.path.exists(alembicFolder):
        version = 'vr001'
    else:
        versions = [file for file in os.listdir(alembicFolder) if file.startswith('vr')]
        versions.sort()

        latestVersion = versions[-1]

        version = 'vr{}'.format(str(int(latestVersion.split('r')[1]) + 1).zfill(3))

    alembicFullFolder = os.path.join(alembicFolder, version).replace('\\', '/')
    os.makedirs(alembicFullFolder)

    alembicFullPath = os.path.join(alembicFullFolder, 'setItems.abc')

    roots_args = ["-root {}".format('|'.join(child.split('|')[:-1])) for child in mshChildren]
    roots_args = " ".join(roots_args)

    cmd.select(mshChildren, hierarchy=1)

    plug = "AbcExport"
    if not cmd.pluginInfo(plug, q=True, l=True):
        cmd.loadPlugin(plug, qt=True)

    startFrame = (cmd.playbackOptions(q=1, ast=1))
    endFrame = (cmd.playbackOptions(q=1, aet=1))

    alembicCmd = "-selection -root {} -frameRange {} {} -wholeFrameGeo -worldSpace -writeColorSets -uvWrite -writeVisibility True -file {}".format(roots_args, startFrame, endFrame, alembicFullPath)
    cmd.AbcExport(j=alembicCmd, verbose=True)

    if os.path.exists(alembicFullPath):
        return alembicFullPath
    else:
        return
