import importlib

import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI

# importlib.reload(rbwUI)


settings = {
    'Fastest': {
        'width': 960,
        'height': 540,
        'samplerType': 3,
        'giOn': 0
    },

    'Fast': {
        'width': 960,
        'height': 540,
        'samplerType': 1,
        'giOn': 1,
        'primaryEngine': 2,
        'secondaryEngine': 2,
        'dmcMinSubdivs': 1,
        'dmcMaxSubdivs': 4,
        'dmcThreshold': 0.003
    },
    'Normal': {
        'width': 1024,
        'height': 768,
        'samplerType': 1,
        'giOn': 1,
        'primaryEngine': 2,
        'secondaryEngine': 2,
        'dmcMinSubdivs': 2,
        'dmcMaxSubdivs': 4,
        'dmcThreshold': 0.003
    },
    'Slow': {
        'width': 1920,
        'height': 1080,
        'samplerType': 1,
        'giOn': 1,
        'primaryEngine': 2,
        'secondaryEngine': 2,
        'dmcMinSubdivs': 4,
        'dmcMaxSubdivs': 8,
        'dmcThreshold': 0.003
    },
    'Slowest': {
        'width': 1920,
        'height': 1080,
        'samplerType': 1,
        'giOn': 1,
        'primaryEngine': 2,
        'secondaryEngine': 2,
        'dmcMinSubdivs': 8,
        'dmcMaxSubdivs': 16,
        'dmcThreshold': 0.003
    },
}


################################################################################
# @brief      This class describes an override render settings ui.
#
class OverrideRenderSettingsUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(OverrideRenderSettingsUI, self).__init__()
        if cmd.window('OverrideRenderSettingsUI', exists=True):
            cmd.deleteUI('OverrideRenderSettingsUI')

        self.setObjectName('OverrideRenderSettingsUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.centralWidget = rbwUI.RBWFrame(layout='G', radius=5)
        self.mainLayout.addWidget(self.centralWidget)

        self.presetComboBox = rbwUI.RBWComboBox(text='Preset', stretch=False, bgColor='rgba(30, 30, 30, 150)', items=list(settings.keys()))
        self.overrideButton = rbwUI.RBWButton(text='Override', size=[100, 35])
        self.resetButton = rbwUI.RBWButton(text='Reset', size=[100, 35])

        self.overrideButton.clicked.connect(self.override)
        self.resetButton.clicked.connect(self.reset)

        self.centralWidget.layout.addWidget(self.presetComboBox, 0, 0, 1, 2)
        self.centralWidget.layout.addWidget(self.overrideButton, 1, 0)
        self.centralWidget.layout.addWidget(self.resetButton, 1, 1)

        self.setMinimumSize(250, 140)
        self.setTitle('Override RS')
        self.setFocus()

    ############################################################################
    # @brief      override render settings.
    #
    def override(self):
        if cmd.objExists('VRSCENE') and cmd.listRelatives('VRSCENE', children=True):
            for scene in cmd.listRelatives('VRSCENE', children=True):
                cmd.setAttr('{}.visibility'.format(scene), 1)
        else:
            rbwUI.RBWError(text='There are no VrScenes in this scene!')
            return

        if cmd.objExists('BAKED_CAM'):
            focusPanel = cmd.getPanel(withFocus=True)
            cmd.lookThru('BAKED_CAM', focusPanel)
        else:
            rbwUI.RBWError(text='There is no baked-cam in this scene!')
            return

        cmd.modelEditor('modelPanel4', e=True, rnm='base_OpenGL_Renderer')
        cmd.modelEditor('modelPanel4', edit=True, allObjects=True)
        cmd.modelEditor('modelPanel4', edit=True, displayAppearance='smoothShaded')

        cmd.setAttr('defaultRenderGlobals.ren', 'vray', type='string')

        self.preset = self.presetComboBox.currentText()
        self.settings = settings[self.preset]
        for setting in self.settings:
            cmd.setAttr('vraySettings.{}'.format(setting), self.settings[setting])

        if self.preset != 'Fastest':
            if not cmd.objExists('VRayLightDome1'):
                cmd.shadingNode('VRayLightDomeShape', asLight=True)
                cmd.setAttr("VRayLightDomeShape1.invisible", 1)
        else:
            if cmd.objExists('VRayLightDome1'):
                cmd.delete('VRayLightDome1')

        cmd.vray("vfbControl", "-history", 1, "-showhistory", 1, "-resetposition")
        cmd.vray("showVFB")

    ############################################################################
    # @brief      Resets the object.
    #
    def reset(self):
        if cmd.objExists('VRayLightDome1'):
            cmd.delete('VRayLightDome1')

        cmd.setAttr('defaultRenderGlobals.ren', 'mayaSoftware', type='string')

        if cmd.objExists('VRSCENE') and cmd.listRelatives('VRSCENE', children=True):
            for scene in cmd.listRelatives('VRSCENE', children=True):
                cmd.setAttr('{}.visibility'.format(scene), 0)
