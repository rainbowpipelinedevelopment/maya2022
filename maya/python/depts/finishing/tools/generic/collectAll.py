import importlib
import os

import maya.cmds as cmd

import api.log
import api.json
import api.vray
import api.shot
import api.savedShot
import api.database

importlib.reload(api.log)
importlib.reload(api.json)
importlib.reload(api.vray)
# importlib.reload(api.shot)
# importlib.reload(api.savedShot)
importlib.reload(api.database)


################################################################################
# @brief      collect all finisahing file.
#
# @param      shotObj         The shot object
# @param      includeVrscene  The include vrscene
# @param      includeAlembic  The include alembic
# @param      save            The save
#
def collectAll(shotObj, includeVrscene=False, includeAlembic=False, save=False):
    cmd.file(new=True, force=True)
    setFile = getSetFile(shotObj)
    if setFile:
        api.log.logger().debug('setFile: {}'.format(setFile))
        setSavedShot = api.savedShot.SavedShot(params={'path': setFile})
        setSavedShot.load()

    cameraFile = getCamFile(shotObj)
    if cameraFile:
        api.log.logger().debug('cameraFile: {}'.format(cameraFile))
        cameraSavedShot = api.savedShot.SavedShot(params={'path': cameraFile})
        cameraSavedShot.load(mode='merge')

    mshFiles = getMshFiles(shotObj)
    if mshFiles:
        api.log.logger().debug('mshFiles: {}'.format(mshFiles))
        for mshFile in mshFiles:
            mshSavedShot = api.savedShot.SavedShot(params={'path': mshFile})
            mshSavedShot.load(mode='merge')

    fxFiles = getFxFiles(shotObj)
    if fxFiles:
        api.log.logger().debug('fxFiles: {}'.format(fxFiles))
        for fxFile in fxFiles:
            fxSavedShot = api.savedShot.SavedShot(params={'path': fxFile})
            fxSavedShot.load(mode='merge')

    if includeVrscene:
        vrsceneFiles = getVrsceneFiles(shotObj)
        if vrsceneFiles:
            api.log.logger().debug('vrsceneFiles: {}'.format(vrsceneFiles))
            for vrsceneFile in vrsceneFiles:
                vrsceneSavedShot = api.savedShot.SavedShot(params={'path': vrsceneFile})
                vrsceneSavedShot.load(mode='merge')

    if save:
        finAllParams = {
            'shot': shotObj,
            'dept': 'Fin',
            'deptType': 'all',
            'note': 'from collect All tool'
        }

        finAllSavedShot = api.savedShot.SavedShot(params=finAllParams)
        finAllSavedShot.save()


################################################################################
# @brief      Gets the set file.
#
# @param      shotObj  The shot object
#
# @return     The set file.
#
def getSetFile(shotObj):
    setQuery = "SELECT `path` FROM `savedShot` WHERE `shotId`={} AND `dept`='Fin' AND `deptType`='set' ORDER BY `date` DESC".format(shotObj.shotId)
    setPath = api.database.selectSingleQuery(setQuery)

    if setPath:
        return setPath[0]
    else:
        return


################################################################################
# @brief      Gets the camera file.
#
# @param      shotObj  The shot object
#
# @return     The camera file.
#
def getCamFile(shotObj):
    cameraQuery = "SELECT `path` FROM `savedShot` WHERE `shotId`={} AND `dept`='Cam' ORDER BY `date` DESC".format(shotObj.shotId)
    cameraPath = api.database.selectSingleQuery(cameraQuery)

    if cameraPath:
        return cameraPath[0]
    else:
        return


################################################################################
# @brief      Gets the msh files.
#
# @param      shotObj  The shot object
#
# @return     The msh files.
#
def getMshFiles(shotObj):
    mshFiles = []
    jsonFile = os.path.join(shotObj.getShotFolder(), 'Fin', 'finishingInitInfos.json').replace('\\', '/')
    if os.path.exists(jsonFile):
        assets = api.json.json_read(jsonFile).keys()
    else:
        return

    for asset in assets:
        mshQuery = "SELECT `path` FROM `savedShot` WHERE `shotId`={} AND `dept`='Fin' AND `deptType`='msh' AND `itemName`='{}' ORDER BY `date` DESC".format(shotObj.shotId, asset)
        mshPath = api.database.selectSingleQuery(mshQuery)

        if mshPath:
            mshFiles.append(mshPath[0])

    return mshFiles


################################################################################
# @brief      Gets the effects files.
#
# @param      shotObj  The shot object
#
# @return     The effects files.
#
def getFxFiles(shotObj):
    fxFiles = []
    jsonFile = os.path.join(shotObj.getShotFolder(), 'Fin', 'fxInitInfos.json').replace('\\', '/')
    if os.path.exists(jsonFile):
        assets = api.json.json_read(jsonFile).keys()
    else:
        return

    for asset in assets:
        fxQuery = "SELECT `path` FROM `savedShot` WHERE `shotId`={} AND `dept`='Fx' AND `itemName`='{}' ORDER BY `date` DESC".format(shotObj.shotId, asset)
        fxPath = api.database.selectSingleQuery(fxQuery)

        if fxPath:
            fxFiles.append(fxPath[0])

    return fxFiles


################################################################################
# @brief      Gets the vrscene files.
#
# @param      shotObj  The shot object
#
# @return     The vrscene files.
#
def getVrsceneFiles(shotObj):
    vrsceneFiles = []
    jsonFile = os.path.join(shotObj.getShotFolder(), 'Fin', 'finishingInitInfos.json').replace('\\', '/')
    if os.path.exists(jsonFile):
        assets = api.json.json_read(jsonFile).keys()
    else:
        return

    for asset in assets:
        vrsceneQuery = "SELECT `path` FROM `savedShot` WHERE `shotId`={} AND `dept`='Fin' AND `deptType`='vrscene' AND `itemName`='{}' ORDER BY `date` DESC".format(shotObj.shotId, asset)
        vrscenePath = api.database.selectSingleQuery(vrsceneQuery)

        if vrscenePath:
            vrsceneFile = os.path.join(os.getenv('MC_FOLDER'), vrscenePath[0].replace('.ma', '_0001.vrscene')).replace('\\', '/')
            if os.path.exists(vrsceneFile):
                vrsceneFiles.append(vrscenePath[0])

    return vrsceneFiles
