from PySide2 import QtWidgets, QtGui
import importlib
import os

import api.shot
import api.savedShot
import api.widgets.rbw_UI as rbwUI
import depts.finishing.tools.generic.iecacheTool.dialog as iecacheToolDialog
import depts.finishing.tools.generic.cacheManager as cacheManager

# importlib.reload(api.shot)
# importlib.reload(api.savedShot)
# importlib.reload(rbwUI)
importlib.reload(iecacheToolDialog)
importlib.reload(cacheManager)


iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'finishing')

################################################################################
# @brief      This class describes an item.
#
class Item(QtWidgets.QTreeWidgetItem):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      params  The params
    #
    def __init__(self, params, parent):
        super().__init__(parent)

        for attr in params:
            setattr(self, attr, params[attr])


################################################################################
# @brief      This class describes a master item.
#
class MasterItem(Item):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        itemName = self.assetDict['namespace'] if not self.assetDict['namespace'].endswith('_') else self.assetDict['namespace'][:-1]
        self.assetDict['itemName'] = itemName

        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        icon = QtGui.QIcon()

        masterDir = self.assetDict['masterDir']
        if os.path.exists(masterDir):
            versions = [dir for dir in os.listdir(masterDir) if dir.startswith('vr')]
            if len(versions) > 0:
                versions.sort()
                self.version = int(versions[-1].split('vr')[1])
                icon.addFile(os.path.join(iconDir, 'done.png'))
                self.setText(1, os.path.join(masterDir.split('Fin')[1][1:], versions[-1]).replace('\\', '/'))
                self.status = 'done'
            else:
                self.version = 0
                icon.addFile(os.path.join(iconDir, 'warning.png'))
                self.setText(1, 'TO DO {}'.format(self.assetDict['itemName']))
                self.status = 'toDo'
        else:
            self.version = 0
            icon.addFile(os.path.join(iconDir, 'warning.png'))
            self.setText(1, 'TO DO')
            self.status = 'toDo'

        self.setText(0, self.assetDict['itemName'])
        self.setIcon(0, icon)

        self.loadButton = rbwUI.RBWButton(text='Load', icon=['load.png'], size=[130, 25])
        self.saveButton = rbwUI.RBWButton(text='Save', icon=['save.png'], size=[130, 25])

        self.treeWidget().setItemWidget(self, 2, self.loadButton)
        self.treeWidget().setItemWidget(self, 3, self.saveButton)

        self.loadButton.clicked.connect(self.load)
        self.saveButton.clicked.connect(self.save)

    ############################################################################
    # @brief      load the selected master file.
    #
    def load(self):
        cacheManager.loadLatestMaster(self.assetDict, self.shotObj)

    ############################################################################
    # @brief      Save the selected master file.
    #
    def save(self):
        cacheManager.saveMaster(self.assetDict)
        self.initUI()


################################################################################
# @brief      This class describes a cache item.
#
class CacheItem(Item):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.exportType = 'default'
        self.subStep = None

        itemName = self.assetDict['namespace'] if not self.assetDict['namespace'].endswith('_') else self.assetDict['namespace'][:-1]
        self.assetDict['itemName'] = itemName

        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        icon = QtGui.QIcon()

        cacheDir = self.assetDict['cacheDir']
        if os.path.exists(cacheDir):
            versions = [dir for dir in os.listdir(cacheDir) if dir.startswith('vr')]
            if len(versions) > 0:
                versions.sort()
                self.version = int(versions[-1].split('vr')[1])
                icon.addFile(os.path.join(iconDir, 'done.png'))
                self.setText(1, os.path.join(cacheDir.split('Fin')[1][1:], versions[-1]).replace('\\', '/'))
                self.status = 'done'
            else:
                self.version = 0
                icon.addFile(os.path.join(iconDir, 'warning.png'))
                self.setText(1, 'TO DO {}'.format(self.assetDict['itemName']))
                self.status = 'toDo'
        else:
            self.version = 0
            icon.addFile(os.path.join(iconDir, 'warning.png'))
            self.setText(1, 'TO DO')
            self.status = 'toDo'

        self.setText(0, self.assetDict['itemName'])
        self.setIcon(0, icon)

        self.optionButton = rbwUI.RBWButton(text='Options', icon=['option.png'], size=[130, 25])
        self.exportButton = rbwUI.RBWButton(text='Export', icon=['export.png'], size=[130, 25])

        self.treeWidget().setItemWidget(self, 2, self.optionButton)
        self.treeWidget().setItemWidget(self, 3, self.exportButton)

        self.optionButton.clicked.connect(self.showOptionPanel)
        self.exportButton.clicked.connect(self.exportCache)

    ############################################################################
    # @brief      Sets the export type.
    #
    # @param      exportType  The export type
    #
    def setExportType(self, exportType):
        self.exportType = exportType

    ############################################################################
    # @brief      Sets the sub step.
    #
    # @param      value  The value
    #
    def setSubStep(self, value):
        self.subStep = value

    ############################################################################
    # @brief      Shows the option panel.
    #
    def showOptionPanel(self):
        result = iecacheToolDialog.CacheDialog(inputParams={'initialStatus': self.exportType, 'subStep': self.subStep}, parent=QtWidgets.QApplication.activeWindow()).result()
        self.setExportType(result[0])
        self.setSubStep(result[1])

    ############################################################################
    # @brief      export cache function
    #
    def exportCache(self):
        cacheDict = self.assetDict.copy()
        cacheDict['cacheDir'] = os.path.join(self.assetDict['cacheDir'], 'vr{}'.format(str(self.version + 1).zfill(3))).replace('\\', '/')
        cacheManager.exportCache(assetDict=cacheDict, shotObj=self.shotObj, type=self.exportType, subSteps=self.subStep)
        self.initUI()


################################################################################
# @brief      This class describes a msh item.
#
class MshItem(Item):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        itemName = self.assetDict['namespace'] if not self.assetDict['namespace'].endswith('_') else self.assetDict['namespace'][:-1]
        self.assetDict['itemName'] = itemName

        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        mshSavedShotParams = {
            'shot': self.shotObj,
            'dept': 'Fin',
            'deptType': 'msh',
            'itemName': self.assetDict['itemName']
        }

        mshSavedShot = api.savedShot.SavedShot(params=mshSavedShotParams)
        latestMsh = mshSavedShot.getLatest()

        icon = QtGui.QIcon()
        if latestMsh:
            icon.addFile(os.path.join(iconDir, 'done.png'))
            self.setText(1, os.path.basename(latestMsh[2]))
            self.status = 'done'
        else:
            icon.addFile(os.path.join(iconDir, 'warning.png'))
            self.setText(1, 'TO DO {}'.format(self.assetDict['itemName']))
            self.status = 'toDo'

        self.setText(0, self.assetDict['itemName'])
        self.setIcon(0, icon)

        self.makeButton = rbwUI.RBWButton(text='Make MSH', icon=['mesh.png'], size=[260, 25])

        self.treeWidget().setItemWidget(self, 2, self.makeButton)

        self.makeButton.clicked.connect(self.makeMsh)

    ############################################################################
    # @brief      Makes a msh.
    #
    def makeMsh(self):
        if os.path.exists(self.assetDict['cacheDir']):
            versions = [dir for dir in os.listdir(self.assetDict['cacheDir']) if dir.startswith('vr')]
            if len(versions) > 0:
                versions.sort()
                mshDict = self.assetDict.copy()
                mshDict['cacheDir'] = os.path.join(self.assetDict['cacheDir'], versions[-1]).replace('\\', '/')

                cacheManager.makeMsh(mshDict, self.shotObj)
                self.initUI()
            else:
                return
        else:
            return


################################################################################
# @brief      This class describes a vrscene item.
#
class VrsceneItem(Item):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        itemName = self.assetDict['namespace'] if not self.assetDict['namespace'].endswith('_') else self.assetDict['namespace'][:-1]
        self.assetDict['itemName'] = itemName

        self.ignoreLod = False
        self.useCullMode = False

        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        vrsceneSavedShotParams = {
            'shot': self.shotObj,
            'dept': 'Fin',
            'deptType': 'vrscene',
            'itemName': self.assetDict['itemName']
        }

        vrsceneSavedShot = api.savedShot.SavedShot(params=vrsceneSavedShotParams)
        latestVrscene = vrsceneSavedShot.getLatest()

        icon = QtGui.QIcon()
        if latestVrscene:
            icon.addFile(os.path.join(iconDir, 'done.png'))
            self.setText(1, os.path.basename(latestVrscene[2]))
            self.status = 'done'
        else:
            icon.addFile(os.path.join(iconDir, 'warning.png'))
            self.setText(1, 'TO DO {}'.format(self.assetDict['itemName']))
            self.status = 'toDo'

        self.setText(0, self.assetDict['itemName'])
        self.setIcon(0, icon)

        self.optionButton = rbwUI.RBWButton(text='Options', icon=['option.png'], size=[130, 25])
        self.createButton = rbwUI.RBWButton(text='Create Vrscene', icon=['hair.png'], size=[130, 25])

        self.treeWidget().setItemWidget(self, 2, self.optionButton)
        self.treeWidget().setItemWidget(self, 3, self.createButton)

        self.optionButton.clicked.connect(self.showOptionPanel)
        self.createButton.clicked.connect(self.createVrscene)

    ############################################################################
    # @brief      Sets the ignore lod.
    #
    # @param      value  The value
    #
    def setIgnoreLod(self, value):
        self.ignoreLod = value

    ############################################################################
    # @brief      Sets the use cull mode.
    #
    # @param      value  The value
    #
    def setUseCullMode(self, value):
        self.useCullMode = value

    ############################################################################
    # @brief      Shows the option panel.
    #
    def showOptionPanel(self):
        result = iecacheToolDialog.VrsceneDialog(inputParams={'ignoreLod': self.ignoreLod, 'useCullMode': self.useCullMode}, parent=QtWidgets.QApplication.activeWindow()).result()
        self.setIgnoreLod(result['ignoreLod'])
        self.setUseCullMode(result['useCullMode'])

    ############################################################################
    # @brief      Creates a vrscene.
    #
    def createVrscene(self):
        if os.path.exists(self.assetDict['cacheDir']):
            versions = [dir for dir in os.listdir(self.assetDict['cacheDir']) if dir.startswith('vr')]
            if len(versions) > 0:
                versions.sort()
                vrsceneDict = self.assetDict.copy()
                vrsceneDict['cacheDir'] = os.path.join(self.assetDict['cacheDir'], versions[-1]).replace('\\', '/')

                cacheManager.createVrscene(vrsceneDict, self.shotObj, self.ignoreLod, self.useCullMode)
                self.initUI()
            else:
                return
        else:
            return


################################################################################
# @brief      This class describes a cache effects item.
#
class CacheFxItem(Item):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        itemName = self.assetDict['namespace'] if not self.assetDict['namespace'].endswith('_') else self.assetDict['namespace'][:-1]
        self.assetDict['itemName'] = itemName

        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        icon = QtGui.QIcon()

        cacheDir = self.assetDict['cacheDir']
        if os.path.exists(cacheDir):
            versions = [dir for dir in os.listdir(cacheDir) if dir.startswith('vr')]
            if len(versions) > 0:
                versions.sort()
                self.version = int(versions[-1].split('vr')[1])
                icon.addFile(os.path.join(iconDir, 'done.png'))
                self.setText(1, os.path.join(cacheDir.split('Fin')[1][1:], versions[-1]).replace('\\', '/'))
                self.status = 'done'
            else:
                self.version = 0
                icon.addFile(os.path.join(iconDir, 'warning.png'))
                self.setText(1, 'TO DO {}'.format(self.assetDict['itemName']))
                self.status = 'toDo'
        else:
            self.version = 0
            icon.addFile(os.path.join(iconDir, 'warning.png'))
            self.setText(1, 'TO DO')
            self.status = 'toDo'

        self.setText(0, self.assetDict['itemName'])
        self.setIcon(0, icon)

        self.exportButton = rbwUI.RBWButton(text='Export', icon=['export.png'], size=[130, 25])
        self.treeWidget().setItemWidget(self, 2, self.exportButton)
        self.exportButton.clicked.connect(self.exportFxCache)

    ############################################################################
    # @brief      export fx caches
    #
    def exportFxCache(self):
        cacheDict = self.assetDict.copy()
        cacheDict['cacheDir'] = os.path.join(self.assetDict['cacheDir'], 'vr{}'.format(str(self.version + 1).zfill(3))).replace('\\', '/')
        cacheManager.exportFxCache(assetDict=cacheDict, shotObj=self.shotObj)
        self.initUI()


################################################################################
# @brief      This class describes a finalize effects item.
#
class FinalizeFxItem(Item):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        itemName = self.assetDict['namespace'] if not self.assetDict['namespace'].endswith('_') else self.assetDict['namespace'][:-1]
        self.assetDict['itemName'] = itemName

        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        fxSavedShotParams = {
            'shot': self.shotObj,
            'dept': 'Fx',
            'itemName': self.assetDict['itemName']
        }

        fxSavedShot = api.savedShot.SavedShot(params=fxSavedShotParams)
        latestFx = fxSavedShot.getLatest()

        icon = QtGui.QIcon()
        if latestFx:
            icon.addFile(os.path.join(iconDir, 'done.png'))
            self.setText(1, os.path.basename(latestFx[2]))
            self.status = 'done'
        else:
            icon.addFile(os.path.join(iconDir, 'warning.png'))
            self.setText(1, 'TO DO {}'.format(self.assetDict['itemName']))
            self.status = 'toDo'

        self.setText(0, self.assetDict['itemName'])
        self.setIcon(0, icon)

        self.finalizeButton = rbwUI.RBWButton(text='Finalize Fx', icon=['mesh.png'], size=[260, 25])

        self.treeWidget().setItemWidget(self, 2, self.finalizeButton)

        self.finalizeButton.clicked.connect(self.finalizeFx)

    ############################################################################
    # @brief      call finalize fx function.
    #
    def finalizeFx(self):
        cacheDict = self.assetDict.copy()
        cacheManager.finalizeFx(assetDict=cacheDict, shotObj=self.shotObj)
        self.initUI()
