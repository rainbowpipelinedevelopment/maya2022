import importlib
from PySide2 import QtWidgets

import api.log
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.log)
# importlib.reload(rbwUI)


################################################################################
# @brief      This class describes a dialog.
#
class Dialog(rbwUI.RBWConfirm):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      inputParams  The input parameters
    # @param      parent       The parent
    #
    def __init__(self, inputParams=None, parent=None, **params):
        params['isMain'] = False
        params['resizable'] = True
        params['title'] = 'Iecache Tool Dialog'
        params['frameScale'] = [120, 120]

        self.inputParams = inputParams

        params['widget'] = self.initUI()

        super().__init__(**params)
        self.buttons['Cancel'].clicked.connect(self.close)
        self.close = False
        self.setMinimumSize(120, 200)
        self.updateMargins()
        self.exec_()

################################################################################
# @brief      This class describes a cache dialog.
#
class CacheDialog(Dialog):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      params  The parameters
    #
    def __init__(self, **params):
        params['title'] = 'Cache Options'
        super().__init__(**params)

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        centralWidget = rbwUI.RBWFrame(layout='V', radius=5)

        self.exportTypeGroupBox = QtWidgets.QGroupBox('Export Type')
        self.exportTypeLayout = QtWidgets.QGridLayout(self.exportTypeGroupBox)
        self.exportTypeLayout.setContentsMargins(12, 20, 12, 6)

        self.geometryCacheRadio = QtWidgets.QRadioButton('geometry-cache')
        self.geometryPlusAlembicRadio = QtWidgets.QRadioButton('geometry + alembic')
        self.geometryPlusAlembicWithSubstepsRadio = QtWidgets.QRadioButton('geometry + alembic cache with substeps')
        self.geometryPlusAlembicWithSubstepsLabel = QtWidgets.QLabel('Set substep value:')
        self.geometryPlusAlembicWithSubstepsLineEdit = QtWidgets.QLineEdit()

        self.geometryPlusAlembicWithSubstepsLineEdit.setText('0.1')

        self.geometryCacheRadio.toggled.connect(self.toggleDefault)
        self.geometryPlusAlembicRadio.toggled.connect(self.toggleAlembic)
        self.geometryPlusAlembicWithSubstepsRadio.toggled.connect(self.toggleSubSteps)

        if self.inputParams:
            if self.inputParams['initialStatus'] == 'default':
                self.geometryCacheRadio.setChecked(True)
            elif self.inputParams['initialStatus'] == 'alembic':
                self.geometryPlusAlembicRadio.setChecked(True)
            else:
                self.geometryPlusAlembicWithSubstepsRadio.setChecked(True)
                self.geometryPlusAlembicWithSubstepsLineEdit.setText(str(self.inputParams['subStep']))
        else:
            self.geometryCacheRadio.setChecked(True)

        self.exportTypeLayout.addWidget(self.geometryCacheRadio, 0, 0, 1, 2)
        self.exportTypeLayout.addWidget(self.geometryPlusAlembicRadio, 1, 0, 1, 2)
        self.exportTypeLayout.addWidget(self.geometryPlusAlembicWithSubstepsRadio, 2, 0, 1, 2)
        self.exportTypeLayout.addWidget(self.geometryPlusAlembicWithSubstepsLabel, 3, 0)
        self.exportTypeLayout.addWidget(self.geometryPlusAlembicWithSubstepsLineEdit, 3, 1)

        centralWidget.addWidget(self.exportTypeGroupBox)

        return [centralWidget]

    ############################################################################
    # @brief      toggle default radio
    #
    def toggleDefault(self):
        wantedState = self.geometryCacheRadio.isChecked()
        if wantedState:
            self.geometryPlusAlembicWithSubstepsLineEdit.setEnabled(False)
            self.geometryPlusAlembicWithSubstepsLabel.hide()
            self.geometryPlusAlembicWithSubstepsLineEdit.hide()

    ############################################################################
    # @brief      toggle alembic radio
    #
    def toggleAlembic(self):
        wantedState = self.geometryPlusAlembicRadio.isChecked()
        if wantedState:
            self.geometryPlusAlembicWithSubstepsLineEdit.setEnabled(False)
            self.geometryPlusAlembicWithSubstepsLabel.hide()
            self.geometryPlusAlembicWithSubstepsLineEdit.hide()

    ############################################################################
    # @brief      toggle sub step radio
    #
    def toggleSubSteps(self):
        wantedState = self.geometryPlusAlembicWithSubstepsRadio.isChecked()
        if wantedState:
            self.geometryPlusAlembicWithSubstepsLineEdit.setEnabled(True)
            self.geometryPlusAlembicWithSubstepsLabel.show()
            self.geometryPlusAlembicWithSubstepsLineEdit.show()

    ############################################################################
    # @brief      Gets the inputs.
    #
    # @return     The inputs.
    #
    def result(self):
        if self.geometryCacheRadio.isChecked():
            return ['default', None]
        elif self.geometryPlusAlembicRadio.isChecked():
            return ['alembic', None]
        else:
            return ['substeps', float(self.geometryPlusAlembicWithSubstepsLineEdit.text())]


################################################################################
# @brief      This class describes a vrscene dialog.
#
class VrsceneDialog(Dialog):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      params  The parameters
    #
    def __init__(self, **params):
        params['title'] = 'Vrscene Options'
        super().__init__(**params)

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        centralWidget = rbwUI.RBWFrame(layout='V', radius=5)

        self.vrsceneOptionsGroupBox = QtWidgets.QGroupBox('Vrscene Options')
        self.vrsceneOptionsLayout = QtWidgets.QVBoxLayout()
        self.vrsceneOptionsLayout.setContentsMargins(12, 20, 12, 6)

        self.ignoreLodCheckBox = rbwUI.RBWCheckBox('Ignore Lod')
        self.useCullModeCheckBox = rbwUI.RBWCheckBox('Use Cull Mode')

        if self.inputParams:
            self.ignoreLodCheckBox.setChecked(self.inputParams['ignoreLod'])
            self.useCullModeCheckBox.setChecked(self.inputParams['useCullMode'])

        self.vrsceneOptionsLayout.addWidget(self.ignoreLodCheckBox)
        self.vrsceneOptionsLayout.addWidget(self.useCullModeCheckBox)

        self.vrsceneOptionsGroupBox.setLayout(self.vrsceneOptionsLayout)

        centralWidget.addWidget(self.vrsceneOptionsGroupBox)

        return [centralWidget]

    ############################################################################
    # @brief      Gets the inputs.
    #
    # @return     The inputs.
    #
    def result(self):
        return {'ignoreLod': self.ignoreLodCheckBox.isChecked(), 'useCullMode': self.useCullModeCheckBox.isChecked()}
