import importlib
import re
import os
import copy

import api.json
import api.shot

importlib.reload(api.json)
# importlib.reload(api.shot)


################################################################################
# @brief      This class describes an iecache tool.
#
class IecacheTool(object):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super().__init__()
        self.shot = None

    ############################################################################
    # @brief      Sets the shot.
    #
    # @param      shotObject  The shot object
    #
    def setShot(self, shotObject):
        self.shot = shotObject
        self.finFolder = os.path.join(self.shot.getShotFolder(), 'Fin').replace('\\', '/')
        self.finInfoJson = os.path.join(self.finFolder, 'finishingInitInfos.json').replace('\\', '/')
        self.fxInfoJson = os.path.join(self.finFolder, 'fxInitInfos.json').replace('\\', '/')

    ############################################################################
    # @brief      Gets the assets.
    #
    # @return     The assets.
    #
    def getAssets(self):
        assets = {'rigs': {}, 'fxs': {}}

        testString = r"vr\d{3}"

        if os.path.exists(self.finInfoJson):
            bckRigData = api.json.json_read(self.finInfoJson)
            rigsData = copy.deepcopy(bckRigData)
            for rig in rigsData:
                if re.search(testString, rigsData[rig]['masterDir']):
                    rigsData[rig]['masterDir'] = os.path.dirname(rigsData[rig]['masterDir']).replace('\\', '/')
            assets['rigs'] = rigsData

            if bckRigData != rigsData:
                api.json.json_write(rigsData, self.finInfoJson)

        if os.path.exists(self.fxInfoJson):
            bckFxData = api.json.json_read(self.fxInfoJson)
            fxsData = copy.deepcopy(bckFxData)
            for fx in fxsData:
                if re.search(testString, fxsData[fx]['masterDir']):
                    fxsData[fx]['masterDir'] = os.path.dirname(fxsData[fx]['masterDir']).replace('\\', '/')
            assets['fxs'] = fxsData

            if bckFxData != fxsData:
                api.json.json_write(fxsData, self.fxInfoJson)

        return assets

    ############################################################################
    # @brief      Determines if grooming save.
    #
    # @param      assetDict  The asset dictionary
    #
    # @return     True if grooming save, False otherwise.
    #
    def hasGroomingSave(self, assetDict):
        rndSavedAsset = api.savedAsset.SavedAsset(params={'db_id': assetDict['surfSavedAsset'], 'approvation': 'def'})

        groomingSavedAsset = api.savedAsset.SavedAsset(params={'asset': rndSavedAsset.asset, 'approvation': 'def', 'dept': 'grooming', 'deptType': 'rnd'})
        lastestGroomingSavedAsset = groomingSavedAsset.getLatest()

        if lastestGroomingSavedAsset:
            return True
        else:
            return
