from PySide2 import QtWidgets, QtGui, QtCore
import os


################################################################################
# @brief      This class describes a header.
#
class Header(QtWidgets.QWidget):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      name            The name
    # @param      content_widget  The content widget
    #
    def __init__(self, name, content_widget):

        super().__init__()
        self.content = content_widget
        self.expand_ico = QtGui.QPixmap(":teDownArrow.png")
        self.collapse_ico = QtGui.QPixmap(":teRightArrow.png")
        self.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)

        allDone_ico = QtGui.QPixmap(os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'finishing', 'allDone.png'))
        self.allDone_ico = allDone_ico.scaled(15, 15, mode=QtCore.Qt.SmoothTransformation)
        missing_ico = QtGui.QPixmap(os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'finishing', 'missing.png'))
        self.missing_ico = missing_ico.scaled(15, 15, mode=QtCore.Qt.SmoothTransformation)

        stacked = QtWidgets.QStackedLayout(self)
        stacked.setStackingMode(QtWidgets.QStackedLayout.StackAll)
        self.background = QtWidgets.QLabel()
        self.background.setStyleSheet("QLabel{ background-color: rgba(30, 30, 30, 150); border-radius:6px}")

        widget = QtWidgets.QWidget()
        layout = QtWidgets.QHBoxLayout(widget)

        self.icon = QtWidgets.QLabel()
        self.icon.setPixmap(self.expand_ico)
        layout.addWidget(self.icon)
        layout.setContentsMargins(11, 0, 11, 0)

        font = QtGui.QFont()
        font.setBold(True)
        label = QtWidgets.QLabel(name)
        label.setFont(font)

        self.statusIcon = QtWidgets.QLabel()
        self.statusIcon.setPixmap(self.allDone_ico)
        self.statusIcon.hide()

        layout.addWidget(label)
        layout.addWidget(self.statusIcon)
        layout.addItem(QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding))

        stacked.addWidget(widget)
        stacked.addWidget(self.background)
        self.background.setMinimumHeight(layout.sizeHint().height() * 1.5)

    ############################################################################
    # @brief      Handle mouse events, call the function to toggle groups
    #
    # @param      args  The arguments
    #
    def mousePressEvent(self, *args):
        self.expand() if not self.content.isVisible() else self.collapse()

    ############################################################################
    # @brief      expand the widget
    #
    def expand(self):
        self.content.setVisible(True)
        self.icon.setPixmap(self.expand_ico)

    ############################################################################
    # @brief      collapse the widget
    #
    def collapse(self):
        self.content.setVisible(False)
        self.icon.setPixmap(self.collapse_ico)

    ############################################################################
    # @brief      Sets the status.
    #
    # @param      status  The status
    #
    def setStatus(self, status):
        if status == 'allDone':
            self.statusIcon.show()
            self.statusIcon.setPixmap(self.allDone_ico)
        elif status == 'missing':
            self.statusIcon.show()
            self.statusIcon.setPixmap(self.missing_ico)
        else:
            self.statusIcon.hide()

################################################################################
# @brief      This class describes a container.
#
class Container(QtWidgets.QWidget):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      name              The name
    # @param      color_background  Whether or not to color the background lighter like in maya (bool)
    #
    def __init__(self, name, layout='V', color_background=False):

        super().__init__()
        self.layout = QtWidgets.QVBoxLayout(self)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self._content_widget = QtWidgets.QWidget()
        self._content_widget.setObjectName('contentWidget')
        if color_background:
            self._content_widget.setStyleSheet("QWidget#contentWidget{background-color: rgba(30, 30, 30, 150); "
                                               "margin-left: 2px; margin-right: 2px; border-radius: 5px}")
        self.header = Header(name, self._content_widget)
        self.layout.addWidget(self.header)
        self.layout.addWidget(self._content_widget)

        if layout == 'V':
            self._content_layout = QtWidgets.QVBoxLayout(self._content_widget)
        elif layout == 'H':
            self._content_layout = QtWidgets.QHBoxLayout(self._content_widget)
        elif layout == 'G':
            self._content_layout = QtWidgets.QGridLayout(self._content_widget)
        elif layout == 'F':
            self._content_layout = QtWidgets.QFormLayout(self._content_widget)

        # assign header methods to instance attributes so they can be called outside of this class
        self.collapse = self.header.collapse
        self.expand = self.header.expand
        self.toggle = self.header.mousePressEvent

    ############################################################################
    # @brief      get the content_widget
    #
    # @return     the content_widget
    #
    @property
    def getContentWidget(self):
        return self._content_widget

    ############################################################################
    # @brief      Adds a widget.
    #
    # @param      widget     The widget
    # @param      alignment  The alignment
    #
    def addWidget(self, widget, alignment=None):
        if alignment:
            self._content_layout.addWidget(widget, alignment=alignment)
        else:
            self._content_layout.addWidget(widget)
