import importlib
import os
from PySide2 import QtWidgets, QtCore
import functools

import maya.cmds as cmd

import api.log
import api.shot
import api.system
import api.scene
import api.savedShot
import config.dictionaries
import api.widgets.rbw_UI as rbwUI
import depts.finishing.tools.generic.iecacheTool.iecacheTool as iecacheTool
import depts.finishing.tools.generic.iecacheTool.container as ieCacheToolContainer
import depts.finishing.tools.generic.iecacheTool.widget as iecacheToolWidget
import depts.finishing.tools.generic.iecacheTool.item as iecacheToolItem

importlib.reload(api.log)
importlib.reload(api.system)
# importlib.reload(api.shot)
# importlib.reload(api.savedShot)
importlib.reload(config.dictionaries)
# importlib.reload(rbwUI)
importlib.reload(iecacheTool)
importlib.reload(ieCacheToolContainer)
importlib.reload(iecacheToolWidget)
importlib.reload(iecacheToolItem)


################################################################################
# @brief      This class describes an ie cache tool ui.
#
class IecacheToolUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      parent  The parent
    #
    def __init__(self, parent=None):
        super().__init__()
        if cmd.window('IecacheToolUI', exists=True):
            cmd.deleteUI('IecacheToolUI')

        self.id_mv = os.getenv('ID_MV')
        self.animationFolder = os.path.join(os.getenv('MC_FOLDER'), 'scenes', 'animation').replace('\\', '/')

        self.iecacheObject = iecacheTool.IecacheTool()
        self.shots = api.system.getShotsFromDB()

        self.setObjectName('IecacheToolUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)
        self.splitter.setObjectName('splitter')
        self.splitter.setStyleSheet('QSplitter#splitter{background-color: transparent; border-radius: 5px;}')

        self.mainLayout.addWidget(self.splitter)

        ########################################################################
        # LEFT PANEL
        #
        self.availableAssetPanel = rbwUI.RBWFrame(layout='V', radius=5)

        # current shot form
        self.currentShotGroup = rbwUI.RBWGroupBox(title='Shot:', layout='V', fontSize=12)

        self.seasonComboBox = rbwUI.RBWComboBox(text='Season', bgColor='transparent')
        self.seasonComboBox.activated.connect(self.fillEpisodeComboBox)

        self.episodeComboBox = rbwUI.RBWComboBox(text='Episode', bgColor='transparent')
        self.episodeComboBox.activated.connect(self.fillSequenceComboBox)

        self.sequenceComboBox = rbwUI.RBWComboBox(text='Sequence', bgColor='transparent')
        self.sequenceComboBox.activated.connect(self.fillShotComboBox)

        self.shotComboBox = rbwUI.RBWComboBox(text='Shot', bgColor='transparent')
        self.shotComboBox.activated.connect(self.fillAvailableAssetsPanel)

        self.currentShotGroup.addWidget(self.seasonComboBox)
        self.currentShotGroup.addWidget(self.episodeComboBox)
        self.currentShotGroup.addWidget(self.sequenceComboBox)
        self.currentShotGroup.addWidget(self.shotComboBox)

        self.availableAssetPanel.addWidget(self.currentShotGroup)

        # available rigs tree
        self.availableRigsTree = rbwUI.RBWTreeWidget(['Available Rigs'])
        self.availableRigsTree.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.availableRigsTree.itemSelectionChanged.connect(functools.partial(self.fillMainPanel, 'rigs'))

        self.availableAssetPanel.addWidget(self.availableRigsTree)

        # available fxs tree
        self.availableFxsTree = rbwUI.RBWTreeWidget(['Available Fxs'])
        self.availableFxsTree.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.availableFxsTree.itemSelectionChanged.connect(functools.partial(self.fillMainPanel, 'fxs'))

        self.availableAssetPanel.addWidget(self.availableFxsTree)

        self.selectAllAssetsButton = rbwUI.RBWButton(text='Select All', size=[200, 35])
        self.selectAllAssetsButton.clicked.connect(self.selectAllAssets)
        self.availableAssetPanel.addWidget(self.selectAllAssetsButton)

        self.splitter.addWidget(self.availableAssetPanel)

        ########################################################################
        # RIGHT PANEL
        #
        self.mainPanel = rbwUI.RBWFrame(layout='V', radius=5)

        self.fromAnimationGroupBox = rbwUI.RBWGroupBox(title='From Animation:', layout='V', fontSize=12)

        # master group
        self.masterContainer = ieCacheToolContainer.Container('MASTER', layout='V')
        self.masterContainer.collapse()

        self.masterWidget = iecacheToolWidget.MasterWidget()
        self.masterContainer.addWidget(self.masterWidget)

        self.fromAnimationGroupBox.addWidget(self.masterContainer)

        self.mainPanel.addWidget(self.fromAnimationGroupBox)

        self.fromRiggingGroupBox = rbwUI.RBWGroupBox(title='From Rigs:', layout='V', fontSize=12)

        # cache group
        self.cacheContainer = ieCacheToolContainer.Container('CACHE', layout='V')
        self.cacheContainer.collapse()

        self.cacheWidget = iecacheToolWidget.CacheWidget()
        self.cacheContainer.addWidget(self.cacheWidget)

        self.fromRiggingGroupBox.addWidget(self.cacheContainer)

        # msh group
        self.mshContainer = ieCacheToolContainer.Container('MSH', layout='V')
        self.mshContainer.collapse()

        self.mshWidget = iecacheToolWidget.MshWidget()
        self.mshContainer.addWidget(self.mshWidget)

        self.fromRiggingGroupBox.addWidget(self.mshContainer)

        # vrscene group
        self.vrsceneContainer = ieCacheToolContainer.Container('MASTER VRSCENE', layout='V')
        self.vrsceneContainer.collapse()

        self.vrsceneWidget = iecacheToolWidget.VrsceneWidget()
        self.vrsceneContainer.addWidget(self.vrsceneWidget)

        self.fromRiggingGroupBox.addWidget(self.vrsceneContainer)

        self.mainPanel.addWidget(self.fromRiggingGroupBox)

        self.fromFxGroupBox = rbwUI.RBWGroupBox(title='From Fxs:', layout='V', fontSize=12)

        # dyn alembic group
        # self.dynAlembicsContainer = ieCacheToolContainer.Container('DYN ALEMBICS', layout='V')
        # self.dynAlembicsContainer.collapse()

        # self.dynAlembicsWidget = iecacheToolWidget.DynAlembicsWidget()
        # self.dynAlembicsContainer.addWidget(self.dynAlembicsWidget)

        # self.fromRiggingGroupBox.addWidget(self.dynAlembicsContainer)

        # cache fx group
        self.cacheFxContainer = ieCacheToolContainer.Container('CACHE FX', layout='V')
        self.cacheFxContainer.collapse()

        self.cacheFxWidget = iecacheToolWidget.CacheFxWidget()
        self.cacheFxContainer.addWidget(self.cacheFxWidget)

        self.fromFxGroupBox.addWidget(self.cacheFxContainer)

        # finalize fx group
        self.finalizeFxContainer = ieCacheToolContainer.Container('FINALIZE FX', layout='V')
        self.finalizeFxContainer.collapse()

        self.finalizeFxWidget = iecacheToolWidget.FinalizeFxWidget()
        self.finalizeFxContainer.addWidget(self.finalizeFxWidget)

        self.fromFxGroupBox.addWidget(self.finalizeFxContainer)

        self.mainPanel.addWidget(self.fromFxGroupBox)

        self.splitter.addWidget(self.mainPanel)

        self.splitter.setStretchFactor(0, 1)
        self.splitter.setStretchFactor(1, 9)

        self.autoFillCombo()

        self.setMinimumSize(1320, 500)

        self.setTitle('Iecache Tool')
        self.setFocus()

    ############################################################################
    # @brief      fill the shot season combo box.
    #
    def fillSeasonComboBox(self):
        self.seasonComboBox.clear()
        seasons = sorted(self.shots.keys())

        self.seasonComboBox.addItems(seasons)

        if len(seasons) == 1:
            self.seasonComboBox.setCurrentIndex(0)
            self.fillEpisodeComboBox()

    ############################################################################
    # @brief      fill the episode combo box.
    #
    def fillEpisodeComboBox(self):
        self.currentSeason = self.seasonComboBox.currentText()

        self.episodeComboBox.clear()
        episodes = sorted(self.shots[self.currentSeason].keys())

        self.episodeComboBox.addItems(episodes)

        if len(episodes) == 1:
            self.episodeComboBox.setCurrentIndex(0)
            self.fillSequenceComboBox()

    ############################################################################
    # @brief      fill the sequence combo box.
    #
    def fillSequenceComboBox(self):
        self.currentEpisode = self.episodeComboBox.currentText()

        self.sequenceComboBox.clear()
        sequences = sorted(self.shots[self.currentSeason][self.currentEpisode].keys())

        self.sequenceComboBox.addItems(sequences)

        if len(sequences) == 1:
            self.sequenceComboBox.setCurrentIndex(0)
            self.fillShotComboBox()

    ############################################################################
    # @brief      fill the shot combo box.
    #
    def fillShotComboBox(self):
        self.currentSequence = self.sequenceComboBox.currentText()

        self.shotComboBox.clear()
        shots = sorted(self.shots[self.currentSeason][self.currentEpisode][self.currentSequence].keys())

        self.shotComboBox.addItems(shots)

        if len(shots) == 1:
            self.shotComboBox.setCurrentIndex(0)
            self.fillAvailableAssetsPanel()

    ############################################################################
    # @brief      fill available assets panel.
    #
    def fillAvailableAssetsPanel(self):
        self.currentShot = self.shotComboBox.currentText()
        self.currentShotObj = api.shot.Shot(params={'season': self.currentSeason, 'episode': self.currentEpisode, 'sequence': self.currentSequence, 'shot': self.currentShot})
        self.iecacheObject.setShot(self.currentShotObj)

        self.availableRigsTree.clear()
        self.availableFxsTree.clear()
        self.assets = self.iecacheObject.getAssets()

        for rigItem in self.assets['rigs']:
            child = QtWidgets.QTreeWidgetItem(self.availableRigsTree)
            child.setText(0, rigItem)

        for fxItem in self.assets['fxs']:
            child = QtWidgets.QTreeWidgetItem(self.availableFxsTree)
            child.setText(0, fxItem)

    ############################################################################
    # @brief      auto fill of all the combos.
    #
    def autoFillCombo(self):
        fileName = api.scene.scenename()
        if self.animationFolder in fileName:
            try:
                relPath = fileName.split(self.animationFolder)[1]
                season = relPath.split('/')[1]
                episode = relPath.split('/')[2]
                sequence = relPath.split('/')[3]
                shot = relPath.split('/')[4]

                self.fillSeasonComboBox()
                seasonIndex = self.seasonComboBox.findText(season, QtCore.Qt.MatchFixedString)
                self.seasonComboBox.setCurrentIndex(seasonIndex)

                self.fillEpisodeComboBox()
                episodeIndex = self.episodeComboBox.findText(episode, QtCore.Qt.MatchFixedString)
                self.episodeComboBox.setCurrentIndex(episodeIndex)

                self.fillSequenceComboBox()
                sequenceIndex = self.sequenceComboBox.findText(sequence, QtCore.Qt.MatchFixedString)
                self.sequenceComboBox.setCurrentIndex(sequenceIndex)

                self.fillShotComboBox()
                shotIndex = self.shotComboBox.findText(shot, QtCore.Qt.MatchFixedString)
                self.shotComboBox.setCurrentIndex(shotIndex)
                self.fillAvailableAssetsPanel()
            except:
                self.fillSeasonComboBox()
        else:
            self.fillSeasonComboBox()

    ############################################################################
    # @brief      { function_description }
    #
    def fillMainPanel(self, mode):
        self.fillMasterGroup()
        if mode == 'rigs':
            self.fillCacheGroup()
            self.fillMshGroup()
            self.fillVRsceneGroup()
        else:
            self.fillCacheFxGroup()
            self.fillFinalizeFxGroup()

    ############################################################################
    # @brief      fill the master group box.
    #
    def fillMasterGroup(self):
        self.masterWidget.tree.clear()

        rigItems = self.availableRigsTree.selectedItems()
        fxItems = self.availableFxsTree.selectedItems()

        items = rigItems + fxItems

        if len(items) == 0:
            self.masterContainer.header.setStatus('wait')
            return

        itemsStatus = []

        for item in items:
            itemName = item.text(0)
            if itemName in self.assets['rigs']:
                assetDict = self.assets['rigs'][itemName]
            else:
                assetDict = self.assets['fxs'][itemName]

            params = {'assetDict': assetDict, 'shotObj': self.currentShotObj}

            status = self.masterWidget.addItem(params=params)
            itemsStatus.append(status)

        for itemsStatus in itemsStatus:
            if status == 'toDo':
                self.masterContainer.header.setStatus('missing')
                return

        self.masterContainer.header.setStatus('allDone')

    ############################################################################
    # @brief      fill the cache group box.
    #
    # @param      itemName  The item name
    #
    def fillCacheGroup(self):
        self.cacheWidget.tree.clear()

        items = self.availableRigsTree.selectedItems()
        if len(items) == 0:
            self.cacheContainer.header.setStatus('wait')
            return

        itemsStatus = []

        for item in items:
            itemName = item.text(0)
            assetDict = self.assets['rigs'][itemName]

            params = {'assetDict': assetDict, 'shotObj': self.currentShotObj}

            status = self.cacheWidget.addItem(params=params)
            itemsStatus.append(status)

        for itemsStatus in itemsStatus:
            if status == 'toDo':
                self.cacheContainer.header.setStatus('missing')
                return

        self.cacheContainer.header.setStatus('allDone')

    ############################################################################
    # @brief      fill the msh group box.
    #
    def fillMshGroup(self):
        self.mshWidget.tree.clear()

        items = self.availableRigsTree.selectedItems()
        if len(items) == 0:
            self.mshContainer.header.setStatus('wait')
            return

        itemsStatus = []

        for item in items:
            itemName = item.text(0)
            assetDict = self.assets['rigs'][itemName]

            params = {'assetDict': assetDict, 'shotObj': self.currentShotObj}

            status = self.mshWidget.addItem(params=params)
            itemsStatus.append(status)

        for itemsStatus in itemsStatus:
            if status == 'toDo':
                self.mshContainer.header.setStatus('missing')
                return

        self.mshContainer.header.setStatus('allDone')

    ############################################################################
    # @brief      fill the vrscene group box.
    #
    def fillVRsceneGroup(self):
        self.vrsceneWidget.tree.clear()

        items = self.availableRigsTree.selectedItems()
        if len(items) == 0:
            self.vrsceneContainer.header.setStatus('wait')
            return

        itemsStatus = []

        for item in items:
            itemName = item.text(0)
            assetDict = self.assets['rigs'][itemName]

            if self.iecacheObject.hasGroomingSave(assetDict):
                params = {'assetDict': assetDict, 'shotObj': self.currentShotObj}

                status = self.vrsceneWidget.addItem(params=params)
                itemsStatus.append(status)

        if len(itemsStatus) > 0:
            for itemsStatus in itemsStatus:
                if status == 'toDo':
                    self.vrsceneContainer.header.setStatus('missing')
                    return

            self.vrsceneContainer.header.setStatus('allDone')
        else:
            return

    ############################################################################
    # @brief      { function_description }
    #
    def fillCacheFxGroup(self):
        self.cacheFxWidget.tree.clear()

        items = self.availableFxsTree.selectedItems()
        if len(items) == 0:
            self.cacheFxContainer.header.setStatus('wait')
            return

        itemsStatus = []

        for item in items:
            itemName = item.text(0)
            assetDict = self.assets['fxs'][itemName]

            if not assetDict['surfSavedAsset']:
                params = {'assetDict': assetDict, 'shotObj': self.currentShotObj}

                status = self.cacheFxWidget.addItem(params=params)
                itemsStatus.append(status)

        if len(itemsStatus) > 0:
            for itemsStatus in itemsStatus:
                if status == 'toDo':
                    self.cacheFxContainer.header.setStatus('missing')
                    return

            self.cacheFxContainer.header.setStatus('allDone')
        else:
            return

    ############################################################################
    # @brief      { function_description }
    #
    def fillFinalizeFxGroup(self):
        self.finalizeFxWidget.tree.clear()

        items = self.availableFxsTree.selectedItems()
        if len(items) == 0:
            self.finalizeFxContainer.header.setStatus('wait')
            return

        itemsStatus = []

        for item in items:
            itemName = item.text(0)
            assetDict = self.assets['fxs'][itemName]

            if assetDict['surfSavedAsset']:
                params = {'assetDict': assetDict, 'shotObj': self.currentShotObj}

                status = self.finalizeFxWidget.addItem(params=params)
                itemsStatus.append(status)

        if len(itemsStatus) > 0:
            for itemsStatus in itemsStatus:
                if status == 'toDo':
                    self.finalizeFxContainer.header.setStatus('missing')
                    return

            self.finalizeFxContainer.header.setStatus('allDone')
        else:
            return

    ############################################################################
    # @brief      { function_description }
    #
    def selectAllAssets(self):
        self.availableRigsTree.selectAll()
        self.availableFxsTree.selectAll()
