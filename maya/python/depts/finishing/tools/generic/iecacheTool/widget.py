from PySide2 import QtWidgets, QtCore
import importlib

import api.log
import api.widgets.rbw_UI as rbwUI
import depts.finishing.tools.generic.iecacheTool.item as iecacheToolItem
import depts.finishing.tools.generic.iecacheTool.dialog as iecacheToolDialog

importlib.reload(api.log)
# importlib.reload(rbwUI)
importlib.reload(iecacheToolItem)
importlib.reload(iecacheToolDialog)

################################################################################
# @brief      This class describes a widget.
#
class Widget(QtWidgets.QWidget):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super().__init__()

        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.layout = QtWidgets.QVBoxLayout()

        self.tree = QtWidgets.QTreeWidget()
        self.tree.setColumnCount(4)
        self.tree.setHeaderHidden(True)
        self.tree.setColumnWidth(0, 300)
        self.tree.setColumnWidth(1, 450)
        self.tree.setColumnWidth(2, 130)
        self.tree.setColumnWidth(3, 130)

        self.tree.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.tree.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)

        self.layout.addWidget(self.tree)

        self.setLayout(self.layout)


################################################################################
# @brief      This class describes a master widget.
#
class MasterWidget(Widget):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    ############################################################################
    # @brief      Adds an item.
    #
    # @param      params  The parameters
    #
    # @return     the new item status
    #
    def addItem(self, params):
        masterItem = iecacheToolItem.MasterItem(params=params, parent=self.tree)
        return masterItem.status

################################################################################
# @brief      This class describes a cache widget.
#
class CacheWidget(Widget):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        super().initUI()

        self.buttonsLayout = QtWidgets.QHBoxLayout()
        self.commonOptionButton = rbwUI.RBWButton(text='Common Options', icon=['option.png'], size=[150, 35])
        self.allButton = rbwUI.RBWButton(text='Export All', icon=['export.png'], size=[150, 35])

        self.buttonsLayout.addWidget(self.commonOptionButton)
        self.buttonsLayout.addWidget(self.allButton)

        self.commonOptionButton.clicked.connect(self.showCommonOptionPanel)
        self.allButton.clicked.connect(self.cacheAll)

        self.layout.addLayout(self.buttonsLayout)

    ############################################################################
    # @brief      Shows the common option panel.
    #
    def showCommonOptionPanel(self):
        result = iecacheToolDialog.CacheDialog(parent=QtWidgets.QApplication.activeWindow()).result()
        root = self.tree.invisibleRootItem()
        for i in range(0, root.childCount()):
            item = root.child(i)
            item.setExportType(result[0])
            item.setSubStep(result[1])

    ############################################################################
    # @brief      cache all function.
    #
    def cacheAll(self):
        root = self.tree.invisibleRootItem()
        for i in range(0, root.childCount()):
            item = root.child(i)
            item.exportCache()

    ############################################################################
    # @brief      Adds an item.
    #
    # @param      params  The parameters
    #
    # @return     the new item status
    #
    def addItem(self, params):
        cacheItem = iecacheToolItem.CacheItem(params=params, parent=self.tree)
        return cacheItem.status


################################################################################
# @brief      This class describes a msh widget.
#
class MshWidget(Widget):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        super().initUI()

        self.tree.setColumnCount(3)

        self.buttonsLayout = QtWidgets.QHBoxLayout()
        self.allButton = rbwUI.RBWButton(text='Make All MSH', icon=['mesh.png'], size=[150, 35])

        self.buttonsLayout.addWidget(self.allButton)

        self.allButton.clicked.connect(self.makeAllMsh)

        self.layout.addLayout(self.buttonsLayout)

    ############################################################################
    # @brief      make all msh function.
    #
    def makeAllMsh(self):
        root = self.tree.invisibleRootItem()
        for i in range(0, root.childCount()):
            item = root.child(i)
            item.makeMsh()

    ############################################################################
    # @brief      Adds an item.
    #
    # @param      params  The parameters
    #
    # @return     the new item status
    #
    def addItem(self, params):
        mshItem = iecacheToolItem.MshItem(params=params, parent=self.tree)
        return mshItem.status


################################################################################
# @brief      This class describes a vrscene widget.
#
class VrsceneWidget(Widget):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    ############################################################################
    # @brief      Adds an item.
    #
    # @param      params  The parameters
    #
    # @return     the new item status
    #
    def addItem(self, params):
        vrsceneItem = iecacheToolItem.VrsceneItem(params=params, parent=self.tree)
        return vrsceneItem.status

################################################################################
# @brief      This class describes a dyn alembics widget.
#
class DynAlembicsWidget(Widget):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        super().initUI()

        self.buttonsLayout = QtWidgets.QHBoxLayout()
        self.buildNewButton = rbwUI.RBWButton(text='Build New', size=[None, 35])
        self.updateButton = rbwUI.RBWButton(text='Update', size=[None, 35])

        self.buttonsLayout.addWidget(self.buildNewButton)
        self.buttonsLayout.addWidget(self.updateButton)
        self.layout.addLayout(self.buttonsLayout)


################################################################################
# @brief      This class describes a cache effects widget.
#
class CacheFxWidget(Widget):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        super().initUI()

        self.tree.setColumnCount(3)

        self.buttonsLayout = QtWidgets.QHBoxLayout()
        self.allButton = rbwUI.RBWButton(text='Export All Fxs', icon=['export.png'], size=[150, 35])

        self.buttonsLayout.addWidget(self.allButton)

        self.allButton.clicked.connect(self.cacheAllFxs)

        self.layout.addLayout(self.buttonsLayout)

    ############################################################################
    # @brief      cache all fxs function
    #
    def cacheAllFxs(self):
        root = self.tree.invisibleRootItem()
        for i in range(0, root.childCount()):
            item = root.child(i)
            item.exportFxCache()

    ############################################################################
    # @brief      Adds an item.
    #
    # @param      params  The parameters
    #
    # @return     the new item status
    #
    def addItem(self, params):
        cacheFxItem = iecacheToolItem.CacheFxItem(params=params, parent=self.tree)
        return cacheFxItem.status


################################################################################
# @brief      This class describes a finalize effects widget.
#
class FinalizeFxWidget(Widget):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        super().initUI()

        self.tree.setColumnCount(3)

        self.buttonsLayout = QtWidgets.QHBoxLayout()
        self.allButton = rbwUI.RBWButton(text='Finalize All Fxs', icon=['mesh.png'], size=[150, 35])

        self.buttonsLayout.addWidget(self.allButton)

        self.allButton.clicked.connect(self.finalizeAllFxs)

        self.layout.addLayout(self.buttonsLayout)

    ############################################################################
    # @brief      finalize all fxs function
    #
    def finalizeAllFxs(self):
        root = self.tree.invisibleRootItem()
        for i in range(0, root.childCount()):
            item = root.child(i)
            item.finalizeFx()

    ############################################################################
    # @brief      Adds an item.
    #
    # @param      params  The parameters
    #
    # @return     the new item status
    #
    def addItem(self, params):
        finalizeFxItem = iecacheToolItem.FinalizeFxItem(params=params, parent=self.tree)
        return finalizeFxItem.status
