import importlib
import random
import maya.cmds as cmd

import api.log as log
importlib.reload(log)

charsName = 'CHARACTER'
transparency = {}


def geometryCacheMeshRebuilder():
    originalSelectionShape = cmd.ls(sl=1, dag=1, shapes=1, ni=1, type='mesh', l=1)
    originalSelection = cmd.listRelatives(originalSelectionShape, p=1, f=1)

    duplicatedMeshes = []
    originalShapes = []
    if not originalSelectionShape:
        log.logger().warning('Please select at least an asset!')
        return
    else:
        emptyGroup = cmd.group(em=1, name='MSH_colored_')
        for i in range(0, len(originalSelectionShape), 1):
            parents = cmd.listRelatives(originalSelectionShape[i], allParents=True)
            visibility = cmd.getAttr('{}.visibility'.format(originalSelectionShape[i]))
            keyFrames = originalSelection[i] if cmd.keyframe('{}.visibility'.format(originalSelection[i]), q=True, keyframeCount=True) else False
            if visibility or keyFrames:
                for parent in parents:
                    if not cmd.getAttr('{}.visibility'.format(parent)):
                        if not cmd.keyframe('{}.visibility'.format(parent), q=True, keyframeCount=True):
                            visibility = False
                        else:
                            keyFrames = parent if not keyFrames else keyFrames

            if visibility and not originalSelectionShape[i].count('RM_Shape'):
                if cmd.nodeType(originalSelectionShape[i]) == 'mesh':
                    newShape = cmd.createNode('mesh', name='msh_001Shape')
                elif cmd.nodeType(originalSelectionShape[i]) == 'nurbsSurface':
                    newShape = cmd.createNode('nurbsSurface', name='msh_001Shape')
                elif cmd.nodeType(originalSelectionShape[i]) == 'VRayProxy':
                    proxyPath = cmd.getAttr('{}.fileName'.format(originalSelectionShape[i])).replace('\\', '/')
                    newShape = cmd.vrayCreateProxy(dir=proxyPath, node='msh_001', geomToLoad=4, newProxyNode=True, createProxyNode=True, existing=True)[0]
                    newShape = '{}_vrayproxy'.format(newShape)
                else:
                    newShape = cmd.createNode('nurbsCurve', name='msh_001Shape')

                if keyFrames:
                    cmd.copyKey(keyFrames, attribute='visibility', animation='objects', option='keys')
                    cmd.pasteKey(newShape, attribute='visibility', animation='objects', option='replaceCompletely')

                newName = originalSelection[i].split(':')
                cmd.parent(newShape, emptyGroup, s=True)

                if len(newName) > 1:
                    cmd.rename(originalSelection[i].split(':')[-1].split('|')[-1])
                else:
                    cmd.rename(originalSelection[i].split('|')[-1])

                originalShapes.append(originalSelectionShape[i])
                cmd.delete('msh_001')

    duplicatedMeshes = cmd.listRelatives(emptyGroup, c=1, f=1)
    duplicatedMeshesShape = cmd.listRelatives(duplicatedMeshes, shapes=1, ni=1, f=1)
    if duplicatedMeshes:
        log.logger().debug('Meshes duplicated')

        # inserito per prevenire l'errore di aver bloccato
        cmd.lockNode('initialShadingGroup', l=0, lockUnpublished=0)

        for i in range(0, len(duplicatedMeshes), 1):
            if cmd.nodeType(originalShapes[i]) == 'mesh':
                if cmd.attributeQuery('outMesh', node=originalShapes[i], exists=True):
                    cmd.connectAttr('{}.outMesh'.format(originalShapes[i]), '{}.inMesh'.format(duplicatedMeshesShape[i]), f=1)

                    cmd.sets(duplicatedMeshesShape[i], e=1, forceElement='initialShadingGroup')

                    # mettere un if se c'e' history o no...
                    # test workaround diego per ovviare al problema delle mesh mosse dal trasform..
                    cmd.select(originalShapes[i], r=1)
                    cmd.select(duplicatedMeshesShape[i], add=1)
                    morph = cmd.blendShape(origin='world')
                    morphAttr = cmd.aliasAttr(morph, q=1)
                    cmd.setAttr('{}.{}'.format(morph[0], morphAttr[0]), 1)
                    cmd.rename(cmd.listRelatives(duplicatedMeshesShape[i], parent=True), originalShapes[i].split('|')[-2])
            else:
                if cmd.attributeQuery('worldSpace', node=originalShapes[i], exists=True):
                    cmd.connectAttr('{}.worldSpace'.format(originalShapes[i]), '{}.create'.format(duplicatedMeshesShape[i]), f=1)
                    cmd.sets(duplicatedMeshesShape[i], e=1, forceElement='initialShadingGroup')
                    cmd.rename(cmd.listRelatives(duplicatedMeshesShape[i], parent=True), originalShapes[i].split('|')[-2])
    else:
        log.logger().warning('No mesh duplicated!')


def mainForBlast():
    if cmd.objExists('MSH_colored_'):
        log.logger().debug("there is already the group MSH_colored_ for colored playblast...")
    else:
        cmd.select(cl=1)
        log.logger().debug("there isn't the coloured group the function will create it for the blast and then will remove it...")
        main()
        cmd.rename('MSH_colored_', 'MSH_colored_blast')


def main():
    if cmd.ls(sl=1):
        log.logger().debug("there is something selected, the function will color all the selected meshes...")
    else:
        if cmd.objExists('*:MSH'):
            log.logger().debug("there wasn't any selection the function will color all the character's meshes...")
            cmd.select('*:MSH', r=1)
        else:
            log.logger().error('Nothing to color in scene')
            return

    geometryCacheMeshRebuilder()
    allDuplicates = cmd.listRelatives('MSH_colored_', children=1, fullPath=1)
    cmd.select(allDuplicates, r=1)
    makeColored()

    cmd.select(cl=1)


def eventualCleanup():
    if cmd.objExists('MSH_colored_blast'):
        cmd.delete('MSH_colored_blast')

    if cmd.objExists(charsName):
        cmd.setAttr('{}.visibility'.format(charsName), 1)

    cmd.select(cl=1)


def makeColored():
    sel = cmd.ls(sl=1)
    longs = cmd.ls(sel, long=1, dag=1, shapes=1, ni=1, type='mesh')
    col = [
        {'name': "red", 'values': [1, 0, 0]},
        {'name': "green", 'values': [0, 1, 0]},
        {'name': "cyan", 'values': [0, 1, 1]},
        {'name': "blue", 'values': [0, 0, 1]},
        {'name': "fucsia", 'values': [1, 0, 1]},
        {'name': "white", 'values': [1, 1, 1]},
        {'name': "pink", 'values': [1, 0.779, 0.721]},
        {'name': "yellow", 'values': [1, 1, 0]}
    ]

    for x in longs:
        color = random.choice(col)
        shd = cmd.shadingNode("lambert", asShader=1, n="SHD_{}".format(color['name']))
        cmd.select(x)
        cmd.hyperShade(assign=shd)
        cmd.sets(renderable=True, noSurfaceShader=True, empty=True, name='{}SG'.format(shd))
        cmd.setAttr("{}.color".format(shd), color['values'][0], color['values'][1], color['values'][2], type="double3")
