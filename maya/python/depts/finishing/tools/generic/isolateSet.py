import importlib
import os
from xml.dom import minidom

import maya.cmds as cmd

import api.scene
import api.database
import api.exception
import api.shot
import api.savedShot
import api.savedAsset
import api.widgets.rbw_UI as rbwUI
import common.checkUpdate.checkUpdate as ckup

importlib.reload(api.scene)
importlib.reload(api.database)
importlib.reload(api.exception)
# importlib.reload(api.shot)
# importlib.reload(api.savedShot)
# importlib.reload(api.savedAsset)
# importlib.reload(rbwUI)
importlib.reload(ckup)


################################################################################
# @brief      { function_description }
#
def isolateSet():
    riggingSaveNodes = api.scene.getSaveNodes(dept='rigging')
    fxSaveNodeList = api.scene.getSaveNodes(dept='fx')

    saveNodes = riggingSaveNodes + fxSaveNodeList

    for saveNode in saveNodes:
        if cmd.listConnections(saveNode) is not None:
            referenceNode = cmd.referenceQuery(saveNode, referenceNode=True)
            referencePath = cmd.referenceQuery(referenceNode, filename=True)
            cmd.file(referencePath, removeReference=True)

    # add vfx from the assembly
    addVfxFromAssembly()

    relPath = api.scene.scenename().split(os.getenv('MC_FOLDER').replace('\\', '/'))[1][1:]

    secSavedShot = api.savedShot.SavedShot(params={'path': relPath})
    savedShot = api.savedShot.SavedShot(params={'shot': secSavedShot.shot, 'dept': 'Fin', 'deptType': 'set', 'note': ''})

    savedShot.save()


################################################################################
# @brief      Adds a vfx from assembly.
#
def addVfxFromAssembly():
    assemblies = ckup.listAssemblies()

    for assembly in assemblies:
        category, group, name, variant = assembly.split('_')[0:4]
        lastVfxRndPathQuery = "SELECT path FROM `V_assetList` AS va JOIN `savedAsset` as sa ON va.variantID = sa.assetId " \
            "WHERE `projectID` = '{}' AND `category` = '{}' AND `group` = '{}' AND `name` = '{}' AND `variant` = '{}' " \
            "AND `dept` = 'fx' and `deptType` = 'rnd' AND `visible`= 1 ORDER BY `date` DESC".format(os.getenv('ID_MV'), category, group, name, variant)
        try:
            lastVfxRndRelPath = api.database.selectSingleQuery(lastVfxRndPathQuery)[0]
        except:
            lastVfxRndRelPath = ''

        if lastVfxRndRelPath:
            lastVfxRndPath = os.path.join(os.getenv('MC_FOLDER'), lastVfxRndRelPath).replace('\\', '/')

            for node in assemblies[assembly]['nodes']:
                translation = cmd.xform(node, translation=True, query=True)
                rotation = cmd.xform(node, rotation=True, query=True)
                scale = cmd.xform(node, scale=True, query=True)

                api.scene.addToPropGroup(lastVfxRndPath, '{}_FX'.format(assembly), translation=translation, rotation=rotation, scale=scale)
