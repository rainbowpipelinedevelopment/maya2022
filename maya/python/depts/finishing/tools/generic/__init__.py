import importlib
import os

import maya.cmds as cmd

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'common')


def run_initFinishingShot(*args):
    import depts.finishing.tools.generic.initFinishing as initFinishing
    importlib.reload(initFinishing)

    initFinishing.detectRig()


def run_iecacheTool(*args):
    import depts.finishing.tools.generic.iecacheTool.iecacheToolUI as iecacheToolUI
    importlib.reload(iecacheToolUI)

    win = iecacheToolUI.IecacheToolUI()
    win.show()


def run_isolateSet(*args):
    import depts.finishing.tools.generic.isolateSet as isolateSet
    importlib.reload(isolateSet)

    isolateSet.isolateSet()


def run_collectAll(*args):
    import depts.finishing.tools.generic.collectAllUI as collectAllUI
    importlib.reload(collectAllUI)

    win = collectAllUI.CollectAllUI()
    win.show()


def run_exportSetItems(*args):
    import depts.finishing.tools.generic.exportSetItems as exportSetItems
    importlib.reload(exportSetItems)

    exportSetItems.exportSetItems()


def run_colored(*args):
    import depts.finishing.tools.generic.makeColored as mkCol
    importlib.reload(mkCol)

    mkCol.main()


def run_deleteVrscene(*args):
    vrsceneTopNode = cmd.ls(selection=True)[0]
    vrsceneName = vrsceneTopNode[:-5]

    cmd.delete('{}_expression'.format(vrsceneName))
    cmd.delete(vrsceneTopNode)


def run_overrideRenderSettings(*args):
    import depts.finishing.tools.generic.overrideRenderSettings as overrideRenderSettings
    importlib.reload(overrideRenderSettings)

    win = overrideRenderSettings.OverrideRenderSettingsUI()
    win.show()


initFinishingTool = {
    'name': 'Init Shot',
    'launch': run_initFinishingShot
}


iecaheTool = {
    'name': 'iecache Tool',
    'launch': run_iecacheTool
}


isolateSetTool = {
    'name': 'Isolate Set',
    'launch': run_isolateSet
}


collectAllTool = {
    'name': 'Collect All',
    'launch': run_collectAll
}

exportSetItemsTools = {
    'name': 'Export Set Items',
    'launch': run_exportSetItems,
    'icon': os.path.join(iconDir, "export.png")
}


makeColoredTool = {
    'name': 'Make Colored',
    'launch': run_colored
}

deleteVrsceneTool = {
    'name': 'Delete Vrscene',
    'launch': run_deleteVrscene,
    'icon': os.path.join(iconDir, "delete.png")
}

overrideRenderSettingsTool = {
    'name': 'Override Render Settings',
    'launch': run_overrideRenderSettings,
    'icon': os.path.join(iconDir, "option.png")
}

tools = [
    initFinishingTool,
    iecaheTool,
    isolateSetTool,
    collectAllTool,
    exportSetItemsTools,
    makeColoredTool,
    deleteVrsceneTool,
    overrideRenderSettingsTool
]

PackageTool = {
    "name": "generic",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
