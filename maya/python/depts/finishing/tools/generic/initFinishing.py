import importlib
import os
import re

import maya.cmds as cmd

import api.json
import api.database
import api.scene
import api.export
import api.asset
import api.shot
import api.savedAsset
import api.savedShot
import api.widgets.rbw_UI as rbwUI
import depts.finishing.tools.generic.cacheManager as cacheManager

importlib.reload(api.json)
importlib.reload(api.database)
importlib.reload(api.scene)
importlib.reload(api.export)
# importlib.reload(api.asset)
# importlib.reload(api.shot)
# importlib.reload(api.savedAsset)
# importlib.reload(api.savedShot)
# importlib.reload(rbwUI)
importlib.reload(cacheManager)


exportDepts = {
    'dynamicGuides': 'grooming',
    'innerGuides': 'grooming',
    'clusterGuides': 'grooming',
    'dynamicFibers': 'grooming',
    'innerFibers': 'grooming',
    'clusterFibers': 'grooming',
    'scalps': 'grooming',
    'extraGeos': 'surfacing',
    'fxData': 'surfacing'
}


################################################################################
# @brief      Create Finishing Environment.
#
def detectRig():
    sceneName = api.scene.scenename(relative=True)
    savedShot = api.savedShot.SavedShot(params={'path': sceneName})
    if savedShot.promoted:
        contentDir = os.getenv('MC_FOLDER').replace('\\', '/')

        shotObj = savedShot.getShotObj()

        riggs = {}
        dynGroomingInfoes = {'assets': {}, 'shotParams': {'season': shotObj.season, 'episode': shotObj.episode, 'sequence': shotObj.sequence, 'shot': shotObj.shot}}
        missingRndRig = []
        missingRndFx = []
        wipRig = []
        rigSaveNodes = api.scene.getSaveNodes(dept='rigging')

        fxs = {}
        wipFx = []
        fxSaveNodes = api.scene.getSaveNodes(dept='fx')

        for rigSaveNode in rigSaveNodes:
            api.log.logger().debug(rigSaveNode)
            if cmd.listConnections(rigSaveNode) is not None and cmd.listConnections(rigSaveNode)[0] != 'MayaNodeEditorSavedTabsInfo':
                if cmd.getAttr('{}.approvation'.format(rigSaveNode)) == 'def':
                    if cmd.getAttr('{}.genre'.format(rigSaveNode)) in ["character", "prop"]:
                        # get namespace
                        namespace = rigSaveNode.split(":")[0]
                        if namespace.endswith('_'):
                            itemName = namespace[:-1]
                        else:
                            itemName = namespace

                        # get rig saved asset
                        rigSavedAsset = api.savedAsset.SavedAsset(saveNode=rigSaveNode)

                        # get rnd saved asset
                        attachRndParams = {'asset': rigSavedAsset.getAsset(), 'dept': 'surfacing', 'deptType': 'rnd', 'approvation': 'def'}
                        attachRnd = api.savedAsset.SavedAsset(params=attachRndParams)

                        if not attachRnd.getLatest():
                            missingRndRig.append(rigSavedAsset.getNamespace())
                            continue

                        # get refereneNode
                        referenceNode = cmd.referenceQuery(rigSaveNode, referenceNode=True)

                        riggs[itemName] = {
                            'namespace': namespace,
                            'referenceNode': referenceNode,
                            'rigSavedAsset': rigSavedAsset.db_id,
                            'surfSavedAsset': attachRnd.getLatest()[0],
                            'masterDir': os.path.join(shotObj.getShotFolder(), 'Fin', '00_master', itemName).replace('\\', '/'),
                            'cacheDir': os.path.join(shotObj.getShotFolder(), 'Fin', '01_cache', 'vrmesh', itemName).replace('\\', '/')
                        }

                        if cmd.objExists('{}:*Guides_saveNode'.format(namespace)):
                            hdaId = getHdaId(rigSavedAsset.getAssetId())
                            widthInfo = getWidthInfo(rigSavedAsset.getAsset())

                            camParams = {
                                'shot': shotObj,
                                'dept': 'Cam',
                            }
                            cameraSavedAsset = api.savedShot.SavedShot(params=camParams)
                            latestCam = cameraSavedAsset.getLatest()
                            if latestCam:
                                latestCamPath = os.path.join(contentDir, latestCam[2]).replace('\\', '/')
                                latestCameraFolder = os.path.dirname(latestCamPath).replace('\\', '/')
                                latestCameraFbx = os.path.basename(latestCamPath).replace('.ma', '.fbx')
                                latestCameraLocators = os.path.basename(latestCamPath).replace('.ma', '_locators.abc')

                                dynGroomingInfoes['extra'] = {'camera': {'folder': latestCameraFolder, 'locators': latestCameraLocators, 'fbx': latestCameraFbx}}

                            dynGroomingInfoes['assets'][itemName] = {
                                'category': rigSavedAsset.getCategory(),
                                'group': rigSavedAsset.getGroup(),
                                'name': rigSavedAsset.getName(),
                                'variant': rigSavedAsset.getVariant(),
                                'namespace': namespace,
                                'assetId': rigSavedAsset.getAssetId(),
                                'hdaId': hdaId,
                                'width': widthInfo
                            }

                            scalpsVersionFolder, scalpsFiles = getExportFolderAndFiles(rigSavedAsset.asset, 'scalps')

                            if scalpsVersionFolder:
                                scalps = []
                                innerScalps = []
                                clusterScalps = []
                                if os.path.exists(os.path.join(scalpsVersionFolder, 'scalps2Descs.json')):
                                    scalpsData = api.json.json_read(os.path.join(scalpsVersionFolder, 'scalps2Descs.json'))
                                    for scalp in scalpsData:
                                        alembicFileName = '{}.abc'.format(scalp.split('|')[-1])
                                        for desc in scalpsData[scalp]:
                                            if '_ING_' in desc:
                                                innerScalps.append(alembicFileName)
                                                break
                                            if '_CLG_' in desc:
                                                clusterScalps.append(alembicFileName)
                                                break
                                            if alembicFileName not in scalps:
                                                scalps.append(alembicFileName)
                                else:
                                    scalps = scalpsFiles

                                if 'rest' not in dynGroomingInfoes['assets'][itemName]:
                                    dynGroomingInfoes['assets'][itemName]['rest'] = {}
                                if len(scalps) > 0:
                                    dynGroomingInfoes['assets'][itemName]['rest']['scalps'] = {'folder': scalpsVersionFolder, 'files': scalps}
                                if len(innerScalps) > 0:
                                    dynGroomingInfoes['assets'][itemName]['rest']['innerScalps'] = {'folder': scalpsVersionFolder, 'files': innerScalps}
                                if len(clusterScalps) > 0:
                                    dynGroomingInfoes['assets'][itemName]['rest']['clusterScalps'] = {'folder': scalpsVersionFolder, 'files': clusterScalps}

                            for saveNode in cmd.ls('{}:*Guides_saveNode'.format(namespace)):
                                guidesType = saveNode.split(':')[1].split('_')[0]

                                if guidesType == 'dynamicGuides':
                                    fibersType = 'dynamicFibers'
                                else:
                                    fibersType = guidesType.replace('Guides', 'Fibers')

                                guidesVersion = cmd.getAttr('{}.version'.format(saveNode))
                                fibersVersion = guidesVersion

                                guidesVersionFolder, guidesFiles = getExportFolderAndFiles(rigSavedAsset.asset, guidesType, version=guidesVersion)

                                fibersVersionFolder, fibersFiles = getExportFolderAndFiles(rigSavedAsset.asset, fibersType, version=fibersVersion)

                                extraGeoVersionFolder, extraGeoFiles = getExportFolderAndFiles(rigSavedAsset.asset, 'extraGeos')

                                geoVersionFolder, geoFiles = getExportFolderAndFiles(rigSavedAsset.asset, 'fxData')

                                if guidesVersionFolder:
                                    if 'rest' not in dynGroomingInfoes['assets'][itemName]:
                                        dynGroomingInfoes['assets'][itemName]['rest'] = {}
                                    dynGroomingInfoes['assets'][itemName]['rest'][guidesType] = {'folder': guidesVersionFolder, 'files': guidesFiles}

                                if fibersVersionFolder:
                                    if 'rest' not in dynGroomingInfoes['assets'][itemName]:
                                        dynGroomingInfoes['assets'][itemName]['rest'] = {}
                                    dynGroomingInfoes['assets'][itemName]['rest'][fibersType] = {'folder': fibersVersionFolder, 'files': fibersFiles}

                                if extraGeoVersionFolder:
                                    if 'rest' not in dynGroomingInfoes['assets'][itemName]:
                                        dynGroomingInfoes['assets'][itemName]['rest'] = {}
                                    dynGroomingInfoes['assets'][itemName]['rest']['extraGeos'] = {'folder': extraGeoVersionFolder, 'files': extraGeoFiles}

                                if geoVersionFolder:
                                    if 'rest' not in dynGroomingInfoes['assets'][itemName]:
                                        dynGroomingInfoes['assets'][itemName]['rest'] = {}
                                    dynGroomingInfoes['assets'][itemName]['rest']['geos'] = {'folder': geoVersionFolder, 'files': geoFiles}

                        # inserire la chiamata alla funzione che importa l'rnd e fa la bs per poi esportare
                        cacheManager.importLatestRnd(riggs[itemName])
                        cacheManager.blendRigAndRnd(namespace)
                        cacheManager.exportMaster(riggs[itemName])

                else:
                    wipRig.append(cmd.listConnections(rigSaveNode)[0])
            else:
                continue

        for fxSaveNode in fxSaveNodes:
            if cmd.listConnections(fxSaveNode) is not None and cmd.listConnections(fxSaveNode)[0] != 'MayaNodeEditorSavedTabsInfo':
                if cmd.getAttr('{}.approvation'.format(fxSaveNode)) == 'def':
                    if cmd.getAttr('{}.genre'.format(fxSaveNode)) in ["character", "prop"]:
                        # get namespace
                        namespace = fxSaveNode.split(":")[0]
                        if namespace.endswith('_'):
                            itemName = namespace[:-1]
                        else:
                            itemName = namespace

                        # get fx saved asset
                        fxSavedAsset = api.savedAsset.SavedAsset(saveNode=fxSaveNode)

                        # get cache set
                        fxCacheSet = cmd.objExists('{}:FX_CACHES'.format(namespace))

                        # get rnd saved asset
                        attachRndParams = {'asset': fxSavedAsset.getAsset(), 'dept': 'fx', 'deptType': 'rnd', 'approvation': 'def'}
                        attachRnd = api.savedAsset.SavedAsset(params=attachRndParams)

                        if not fxCacheSet and not attachRnd:
                            missingRndFx.append(fxSavedAsset.getNamespace())
                            continue

                        # get refereneNode
                        referenceNode = cmd.referenceQuery(fxSaveNode, referenceNode=True)

                        fxs[itemName] = {
                            'namespace': namespace,
                            'referenceNode': referenceNode,
                            'fxAniSavedAsset': fxSavedAsset.db_id,
                            'surfSavedAsset': attachRnd.getLatest()[0] if attachRnd.getLatest() else None,
                            'masterDir': os.path.join(shotObj.getShotFolder(), 'Fin', '00_master', itemName).replace('\\', '/'),
                            'cacheDir': os.path.join(shotObj.getShotFolder(), 'Fin', '01_cache', 'alembic', itemName).replace('\\', '/')
                        }

                        cacheManager.exportMaster(fxs[itemName], fx=True)
                    else:
                        wipFx.append(cmd.listConnections(fxSaveNodes)[0])
            else:
                continue

        if missingRndRig:
            warningMessage = 'These assets have no def surfacing save:'
            for rndRig in missingRndRig:
                warningMessage = '{}\n- {}'.format(warningMessage, rndRig)
            warningMessage = '{}\nThe other assets can normally be finalized.'.format(warningMessage)
            rbwUI.RBWWarning(title='No RND Save', text=warningMessage, defCancel=None, resizable=True)

        if missingRndFx:
            warningMessage = 'These assets have no def fx rnd save:'
            for rndFx in missingRndFx:
                warningMessage = '{}\n- {}'.format(warningMessage, rndFx)
            warningMessage = '{}\nThe other assets can normally be finalized.'.format(warningMessage)
            rbwUI.RBWWarning(title='No RND Save', text=warningMessage, defCancel=None, resizable=True)

        jsonPath = os.path.join(shotObj.getShotFolder(), 'Fin', 'finishingInitInfos.json').replace('\\', '/')
        api.log.logger().debug('jsonPath: {}'.format(jsonPath))
        api.json.json_write(riggs, jsonPath)

        if len(dynGroomingInfoes['assets']) > 0:
            cfxFolder = os.path.join(shotObj.getShotFolder(), 'Cfx').replace('\\', '/')
            if not os.path.exists(cfxFolder):
                os.makedirs(cfxFolder)

            cfxJsonPath = os.path.join(shotObj.getShotFolder(), 'Fin', 'cfxInitInfos.json').replace('\\', '/')
            api.json.json_write(dynGroomingInfoes, cfxJsonPath)

        if len(fxs) > 0:
            fxJsonPath = os.path.join(shotObj.getShotFolder(), 'Fin', 'fxInitInfos.json').replace('\\', '/')
            api.json.json_write(fxs, fxJsonPath)
    else:
        rbwUI.RBWError(text='This shot is not a Promoted save.\nPlease make a new save promoted or open the right scene.')


############################################################################
# @brief      Gets the hda identifier.
#
# @param      assetId  The asset identifier
#
# @return     The hda identifier.
#
def getHdaId(assetId):
    try:
        hdaIdQuery = "SELECT `hda_id` FROM `hdaToAssets` WHERE `asset_id` = '{}'".format(assetId)
        hdaId = api.database.selectSingleQuery(hdaIdQuery)[0]

        return hdaId
    except TypeError:
        return -1


############################################################################
# @brief      Gets the export folder and files.
#
# @param      asset        The asset
# @param      exportType   The export type
# @param      version      The version
#
# @return     The export folder and files.
#
def getExportFolderAndFiles(asset, exportType, version=None):
    exportFolder = os.path.join(
        asset.getAssetFolder(),
        'export',
        exportDepts[exportType],
        exportType,
    ).replace('\\', '/')

    if os.path.exists(exportFolder):
        if version:
            exportVersionFolder = os.path.join(exportFolder, version).replace('\\', '/')
        else:
            versions = [file for file in os.listdir(exportFolder) if file.startswith('vr')]
            versions.sort()

            exportVersionFolder = os.path.join(exportFolder, versions[-1]).replace('\\', '/')

        abcFiles = [file for file in os.listdir(exportVersionFolder) if file.endswith('.abc')]

        return exportVersionFolder, abcFiles
    else:
        return None, None


################################################################################
# @brief      Gets the width information.
#
# @param      asset  The asset
#
# @return     The width information.
#
def getWidthInfo(asset):
    groomingSavedAssetParams = {
        'asset': asset,
        'dept': 'grooming',
        'deptType': 'rnd',
        'approvation': 'def'
    }

    groomingSavedAsset = api.savedAsset.SavedAsset(params=groomingSavedAssetParams)

    latestGrooming = os.path.join(os.getenv('MC_FOLDER'), groomingSavedAsset.getLatest()[2])

    # qui devo avere già il path all'ultimo grooming
    latestGroomingFolder = os.path.dirname(latestGrooming)

    latestXgenFileName = [file for file in os.listdir(latestGroomingFolder) if file.endswith('.xgen')][0]
    latestXgenFile = os.path.join(latestGroomingFolder, latestXgenFileName).replace('\\', '/')

    with open(latestXgenFile, 'r') as inputFile:
        inputData = inputFile.readlines()

    descriptionIndex = {}
    widthIndex = {}
    widthRampIndex = {}

    for i in range(0, len(inputData)):
        inputLine = inputData[i]

        if inputLine.startswith('Description'):
            descriptionName = inputData[i + 1].split('\t')[-1].split('\n')[0]
            descriptionIndex[i] = descriptionName

        testString = r"a=\d.\d{1,5};"
        if inputLine.startswith('\twidth\t'):
            value = inputLine.split('\t')[-1].split('\n')[0]
            if re.search(testString, value):
                widthIndex[i] = value.split(';')[0].split('=')[1]
            else:
                widthIndex[i] = value

        if inputLine.startswith('\twidthRamp\t'):
            widthRampIndex[i] = inputLine.split('\t')[-1].split('rampUI')[1].split('\n')[0][1:-1].split(':')

    descriptionIndexes = list(descriptionIndex.keys())
    widthIndexes = list(widthIndex.keys())
    widthRampIndexes = list(widthRampIndex.keys())

    descriptionIndexes.sort()
    widthIndexes.sort()
    widthRampIndexes.sort()

    widthInfo = {}

    for i in range(0, len(descriptionIndexes)):
        descriptionName = descriptionIndex[descriptionIndexes[i]]
        if descriptionName.split('_')[2] != 'ST':
            width = float(widthIndex[widthIndexes[i]])
            widthRamp = []
            for coordinateString in widthRampIndex[widthRampIndexes[i]]:
                coordinatesString = coordinateString.split(',')
                for j in range(0, len(coordinatesString)):
                    coordinatesString[j] = float(coordinatesString[j])
                widthRamp.append(coordinatesString)
            widthInfo[descriptionName.split('_')[-1]] = {'base': width, 'widthRamp': widthRamp}

    return widthInfo
