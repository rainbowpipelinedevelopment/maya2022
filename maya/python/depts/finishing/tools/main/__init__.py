import importlib
import os

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'arakno')


################################################################################
# @brief      run load and save UI
#
# @param      args  The arguments
#
def run_loadAndSave(*args):
    import common.loadAndSaveShotUI as lss
    importlib.reload(lss)

    win = lss.ShotUI()
    win.show()


################################################################################
# @brief      run checkupdate tool.
#
# @param      args  The arguments
#
def run_checkUpdate(*args):
    import common.checkUpdate.checkUpdateUI as checkUpdateUI
    importlib.reload(checkUpdateUI)

    win = checkUpdateUI.CheckUpdateUI('finishing')
    win.show()


################################################################################
# @brief      run playblast tool.
#
# @param      args  The arguments
#
def run_playblast(*args):
    import common.playblast as playblastUI
    importlib.reload(playblastUI)

    win = playblastUI.PlayblastTool('Fin')
    win.show()


loadAndSaveTool = {
    "name": "L/S Finishing",
    "launch": run_loadAndSave,
    "icon": os.path.join(iconDir, "loadAndSaveFinishing.png"),
    "statustip": "Load & Save"
}

checkupdateTool = {
    "name": "Check Update",
    "launch": run_checkUpdate,
    "icon": os.path.join(iconDir, '..', 'common', "refresh.png"),
    "statustip": "Check Update"
}

playblastTool = {
    "name": "Playblast Tool",
    "launch": run_playblast,
    "icon": os.path.join(iconDir, '..', 'common', "playblast.png"),
    "statustip": "Playblast Tool"
}


tools = [
    loadAndSaveTool,
    checkupdateTool,
    playblastTool
]

PackageTool = {
    "name": "main",
    "tools": tools
}
