import importlib
import os
import shutil
import xml.etree.cElementTree as ET
import xml.dom.minidom as xdm

import maya.cmds as cmd

import api.log
import api.scene
import api.xgen
import api.vray
import api.farm
import api.widgets.rbw_UI as rbwUI
import depts.sc_depts as scDepts

importlib.reload(api.log)
importlib.reload(api.scene)
importlib.reload(api.xgen)
importlib.reload(api.vray)
importlib.reload(api.farm)
# importlib.reload(rbwUI)
importlib.reload(scDepts)


################################################################################
# @brief      This class describes a finishing.
#
class Finishing(scDepts.Dept):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.name = "Fin"

        self._usersFolder = os.path.join(os.path.dirname(os.getenv('MC_FOLDER')), '_users', os.getenv('USERNAME')).replace('\\', '/')

    ###########################################################################
    # @brief      sc save method
    #
    # @param      comment  The comment
    #
    def save(self):
        if self.getDeptType() == 'vrscene':
            currentMayaProject = os.path.dirname(api.scene.scenename()).replace('\\', '/')

            groomingSaveNode = cmd.ls('grooming_saveNode')[0]
            groomingSavedAssetId = cmd.getAttr('{}.db_id'.format(groomingSaveNode))
            groomingSavedAsset = api.savedAsset.SavedAsset(params={'db_id': groomingSavedAssetId, 'approvation': 'def'})

            xgenProjectPath = os.path.join(os.getenv('MC_FOLDER'), os.path.dirname(groomingSavedAsset.path)).replace('\\', '/')
            api.xgen.setProjectPath(xgenProjectPath)

        if self.getDeptType() == 'all':
            self.setViewport()

        self.realSave()
        self.exportXML()

        if self.getDeptType() == 'vrscene':
            alembicFile = self.exportPatchAlembic()
            self.editXgenFile(alembicFile)
            self.sendVrsceneInFarm()
            shutil.rmtree(currentMayaProject)
            cmd.file(new=True, force=True)
            cmd.workspace(os.getenv('MC_FOLDER').replace('\\', '/'), openWorkspace=True)
        return True

    ############################################################################
    # @brief      load function.
    #
    def load(self, mode='load', confirm=True):
        if mode == 'load':
            if self.getDeptType() == 'vrscene':
                usersFolder = os.path.join(self._usersFolder, self.getItemName()).replace('\\', '/')

                # se la cartella esiste, l'utente puo scegliere di cancellare la cartella oppure interrompere il loading
                if os.path.exists(usersFolder):
                    messageContent = "The folder {} already exists in your user directory.\n If you continue the loading process, the old folder will be replaced".format(self.getItemName())
                    response = rbwUI.RBWWarning(title='ATTENTION', text=messageContent)
                    if response:
                        cmd.file(new=True, force=True)
                        shutil.rmtree(usersFolder)
                    else:
                        warningMessage = 'The loading was interrupted.\n'
                        rbwUI.RBWWarning(text=warningMessage, title='ATTENTION')
                        return

                os.makedirs(usersFolder)

                # ma file
                sourceFolder = os.path.dirname(self.path).replace('\\', '/')

                for file in os.listdir(sourceFolder):
                    if not file.endswith('.vrscene'):
                        sourceFile = os.path.join(sourceFolder, file)
                        destFile = os.path.join(usersFolder, file)
                        shutil.copyfile(sourceFile, destFile)

                # xgen folder
                groomingXgenFolder = self.getGroomingXgen()

                shutil.copytree(groomingXgenFolder, os.path.join(usersFolder, 'xgen').replace('\\', '/'))

                # workspace.mel
                sourceWorkspaceMel = os.path.join(os.getenv('LOCALPIPE'), 'mel', 'workspace.mel').replace('\\', '/')
                destWorkspaceMel = os.path.join(usersFolder, 'workspace.mel').replace('\\', '/')

                shutil.copyfile(sourceWorkspaceMel, destWorkspaceMel)

                cmd.workspace(usersFolder, openWorkspace=True)

                newPath = os.path.join(usersFolder, os.path.basename(self.path)).replace('\\', '/')
                cmd.file(newPath, open=True, force=True)

            else:
                super(Finishing, self).load(mode, confirm)
                self.sanizeVrscene()

        elif mode == 'merge':
            if self.getDeptType() == 'vrscene':
                vrsceneFolder = os.path.dirname(self.path).replace('\\', '/')
                vrsceneFileName = os.path.basename(self.path).replace('.ma', '_0001.vrscene')

                vrsceneNode = api.vray.importMultipleVrScene(inputFolder=vrsceneFolder, inputFile=vrsceneFileName)
                cmd.setAttr('{}.visibility'.format(vrsceneNode), 0)

            else:
                node = api.scene.mergeAsset(self.path)

                if cmd.referenceQuery(node, inr=True):
                    referenceNode = cmd.referenceQuery(node, rfn=True)
                    referencePath = cmd.referenceQuery(referenceNode, filename=True, withoutCopyNumber=True)
                    api.log.logger().debug('referencePath: {}'.format(referencePath))

                    referenceNamespace = cmd.referenceQuery(referenceNode, namespace=True)

                    if 'character' in referencePath:
                        if not cmd.objExists('CHARACTER'):
                            cmd.group(empty=True, n='CHARACTER')

                        cmd.parent(node, 'CHARACTER')
                        cacheMshSetName = referenceNamespace.replace('RND', 'cache')
                        if cmd.objExists('MSH'):
                            cmd.rename('MSH', '{}_MSH'.format(cacheMshSetName))
                        api.log.logger().debug('Add {} to CHARACTER'.format(node))

                    elif 'prop':
                        if not cmd.objExists('PROP'):
                            cmd.group(empty=True, n='PROP')

                        cmd.parent(node, 'PROP')
                        cacheMshSetName = referenceNamespace.replace('RND', 'cache')
                        if cmd.objExists('MSH'):
                            cmd.rename('MSH', '{}_MSH'.format(cacheMshSetName))
                        api.log.logger().debug('Add {} to PROP'.format(node))

                    self.attachLgtShaders(node)
                else:
                    if not cmd.objExists('EXTRA'):
                        cmd.group(empty=True, n='EXTRA')

                    cmd.parent(node, 'EXTRA')
                    api.log.logger().debug('Add {} to EXTRA'.format(node))

    ############################################################################
    # @brief      export the xml file.
    #
    def exportXML(self):
        if self.getDeptType() == 'vrscene':
            xmlPath = os.path.join(self.savedShot.savedShotFolder, '{}.xml'.format(self.savedShot.sceneName)).replace('\\', '/')

            sceneRoot = ET.Element('scene')
            assetsRoot = ET.SubElement(sceneRoot, 'assets')

            saveNode = cmd.ls('grooming_saveNode')[0]
            if cmd.listConnections(saveNode) is not None:
                savedAsset = api.savedAsset.SavedAsset(saveNode=saveNode)
                ET.SubElement(assetsRoot, 'asset', name=saveNode, id=str(savedAsset.getAssetId()), dept=savedAsset.dept, savedAssetId=str(savedAsset.db_id))

            ET.SubElement(sceneRoot, 'comment', value=self.savedShot.note)
            ET.SubElement(sceneRoot, 'user', name=os.getenv('USERNAME'))
            ET.SubElement(sceneRoot, 'currentFrame', value=str(cmd.currentTime(query=True)))
            ET.SubElement(sceneRoot, 'shotId', value=str(self.getShotId()))
            ET.SubElement(sceneRoot, 'savedShotId', value=str(self.savedShot.db_id))
            ET.SubElement(sceneRoot, 'saveDate', value=str(self.savedShot.date))

            if sceneRoot:
                tree = ET.ElementTree(sceneRoot)
                tree.write(xmlPath)

                xml = xdm.parse(xmlPath)
                pretty_xml_as_string = xml.toprettyxml(indent='\t')

                with open(xmlPath, "w") as f:
                    f.write(pretty_xml_as_string)

                return True
        else:
            super().exportXML()

    ############################################################################
    # @brief      Gets the grooming xgen.
    #
    # @return     The grooming xgen.
    #
    def getGroomingXgen(self):
        xmlFile = self.path.replace('.ma', '.xml')

        xmlDoc = xdm.parse(xmlFile)
        asset = xmlDoc.getElementsByTagName('asset')[0]
        savedAssetId = asset.attributes['savedAssetId'].value

        groomingSavedAsset = api.savedAsset.SavedAsset(params={'db_id': savedAssetId, 'approvation': 'def'})

        xgenFolder = os.path.join(
            os.getenv('MC_FOLDER'),
            os.path.dirname(groomingSavedAsset.path),
            'xgen'
        ).replace('\\', '/')

        return xgenFolder

    ################################################################################
    # @brief      export patch alembic.
    #
    # @return     the alembic path
    #
    def exportPatchAlembic(self):
        caches = cmd.ls(type='cacheFile')
        start = 0
        end = int(cmd.getAttr('{}.originalEnd'.format(caches[0])))

        scalps = api.xgen.getScalps()
        scalps = list(set(scalps))

        collection = api.xgen.currentCollection()

        roots_args = ["-root {}".format(scalp) for scalp in scalps]
        roots_args = " ".join(roots_args)

        alembicFile = os.path.join(self.savedShot.savedShotFolder, '{}__{}.abc'.format(self.savedShot.sceneName, collection)).replace('\\', '/')

        command = '''
        -frameRange {start} {end} -uvwrite -attrPrefix xgen -worldSpace {roots} -stripNamespaces -file '{saveName}'"
        '''.format(start=start, end=end, roots=roots_args, saveName=alembicFile)

        cmd.AbcExport(j=command, verbose=True)

        return alembicFile

    ################################################################################
    # @brief      edit the .xgen file
    #
    # @param      alembicFile  The alembic file
    #
    def editXgenFile(self, alembicFile):
        xgenFile = alembicFile.replace('.abc', '.xgen')
        xgenTmpFile = alembicFile.replace('.abc', '.xgen.tmp')

        os.rename(xgenFile, xgenTmpFile)

        with open(xgenTmpFile, "r") as xgenDataRead:
            xgenData = xgenDataRead.read()

        newXgenData = xgenData.replace('//speed.nas', '${SPEED_SHARE}')

        with open(xgenFile, "w") as xgenDataWrite:
            xgenDataWrite.write(newXgenData)

    ################################################################################
    # @brief      Sends a vrscene in farm.
    #
    def sendVrsceneInFarm(self):
        caches = cmd.ls(type='cacheFile')
        start = 0
        end = int(cmd.getAttr('{}.originalEnd'.format(caches[0])))

        relPath = os.path.join(self.savedShot.savedShotFolder.split('mc_{}/'.format(os.getenv('PROJECT')))[1], '{}.ma'.format(self.savedShot.sceneName)).replace('\\', '/')

        vrsceneJobParams = {
            'inputFileName': relPath,
            'outputFileName': relPath.replace('.ma', '.vrscene'),
            'jobName': self.savedShot.sceneName,
            'batchName': '{}_{}_{}_{}_{}_CACHING_VRSCENE'.format(os.getenv('PROJECT').upper(), self.getSeason(), self.getEpisode(), self.getSequence(), self.getShot()),
            'frames': '{}-{}'.format(start, end),
            'dept': 'Finishing',
            'goal': 'finishing vrscene caching',
            'obj': self.savedShot.getShotObj()
        }

        api.farm.createAndSubmitJob(vrsceneJobParams)

    ############################################################################
    # @brief      Sets the viewport.
    #
    def setViewport(self):
        # qui inserire procedure che nasconde le gpu cache e spegne altri valori di viewport
        modelPanels = cmd.getPanel(type="modelPanel")
        visiblePanels = cmd.getPanel(visiblePanels=True)
        modelPanel = list(set(modelPanels) & set(visiblePanels))[0]
        # spengo gpu cache
        cmd.modelEditor(modelPanel, edit=True, pluginObjects=['gpuCacheDisplayFilter', False])
        # spengo le shadow
        cmd.modelEditor(modelPanel, edit=True, shadows=False)
        # bounding box view
        cmd.modelEditor(modelPanel, edit=True, displayAppearance='boundingBox')
        # spengo motion blur
        cmd.setAttr('hardwareRenderingGlobals.motionBlurEnable', 0)
        # spengo AO
        cmd.setAttr('hardwareRenderingGlobals.ssaoEnable', 0)
        # spengo anti-aliasing
        cmd.setAttr('hardwareRenderingGlobals.multiSampleEnable', 0)
