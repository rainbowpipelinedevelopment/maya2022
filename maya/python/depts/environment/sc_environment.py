import importlib

import depts.sc_depts as scDepts

importlib.reload(scDepts)


################################################################################
# @brief      This class describes an environment.
#
class Environment(scDepts.Dept):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.name = "Env"

    ###########################################################################
    # @brief      sc save method
    #
    def save(self):
        self.realSave()
        self.exportXML()
        return True
