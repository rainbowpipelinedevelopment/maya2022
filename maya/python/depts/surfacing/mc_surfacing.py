import importlib
import os
import shutil
from PySide2 import QtWidgets
from decimal import Decimal

import maya.cmds as cmd
import maya.mel as mel

import depts.mc_depts as mcdepts
import api.log
import api.scene
import api.json
import api.vray
import api.renderlayer
import api.farm
import api.widgets.rbw_UI as rbwUI
import config.dictionaries as dictionary
import depts.surfacing.tools.vray.texture_convert as texture_convert
import depts.surfacing.tools.export.exportFxData as exportFxData
import depts.modeling.tools.export.exportFbx as expFbx

importlib.reload(mcdepts)
importlib.reload(api.log)
importlib.reload(api.scene)
importlib.reload(api.json)
importlib.reload(api.vray)
importlib.reload(api.renderlayer)
importlib.reload(api.farm)
# importlib.reload(rbwUI)
importlib.reload(dictionary)
importlib.reload(texture_convert)
importlib.reload(exportFxData)
importlib.reload(expFbx)


renderStatsAttrs = [
    'castsShadows',
    'receiveShadows',
    'holdOut',
    'motionBlur',
    'primaryVisibility',
    'smoothShading',
    'visibleInReflections',
    'visibleInRefractions',
    'doubleSided',
    'opposite',
    'geometryAntialiasingOverride',
    'shadingSamplesOverride'
]

################################################################################
# @brief      This class describes a surfacing.
#
class Surfacing(mcdepts.Dept):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, savedAsset):
        super(Surfacing, self).__init__(savedAsset)
        self.deptName = "surfacing"

    ############################################################################
    # @brief      Perform the actual save
    #
    # @param      self  The object
    #
    # @return     None
    #
    def save(self):
        api.scene.pluginCleanUp()
        self.closeAllWindows()

        # exclude set because there are assembly and not mesh
        if not self.isSet():
            self.checkTexturesPath()

        self.localSave()

        # remove Display Layers
        if self.isDef():
            self.cleanupDisplayLayers()

            if cmd.objExists('CTRL_set') and not self.isLightrig():
                if cmd.objExists('CTRL_set.Rattr'):
                    cmd.setAttr('CTRL_set.Rattr', True)

            if self.isShader():
                self.prepareShaderSceneToSave()
                self.sendShaderSceneToFarm()
                self.writeShaderInfoes()
                cmd.file(rename=self.path)

            # elif self.isFur():
            #     self.prepareFurSceneToSave()
            #     self.sendFurSceneToFarm()
            #     self.writeFurInfoes()
            #     cmd.file(rename=self.path)

            self.deleteUnusedShapeOrig()
            self.deletePolySurfaceShape()

            # delete bad components
            self.clearSceneToSave()

            if not self.isShader():
                if self.getDeptType() != 'txt':

                    self.editViewport()
                    if not self.isLightrig():
                        # delete unused nodes
                        mel.eval("MLdeleteUnused;")

                    # trascode tiff to exr
                    self.finalizeTextures()

        # NEW FOR MODELING CONSISTENCE
        self.exportXML()

        # === Real Save On Disk!! ===
        self.realSave()

        # ripartire da qui
        scenefilename = api.scene.scenename()

        if hasattr(self.savedAsset, 'vrscene') and self.savedAsset.vrscene and not self.isDef():
            api.vray.createVRayFile()

            cmd.file(scenefilename, open=True, force=True)
            self.closeAllWindows()
            return True

        if self.isDef():
            if self.getDeptType() not in ['uvs', 'txt']:
                if cmd.objExists('CNT'):
                    self.exportCNTs()
                    cmd.file(scenefilename, open=True, force=True)

                if not self.isLightrig() and not self.isShader():
                    vrmeshfilename = None
                    # === Export VRMesh ===
                    vrmeshfilename, noSmooth = self.exportVRmesh()

                    if noSmooth:
                        infoMsg = 'The asset has not pipeline smooth node.\nIf the asset is free of such materials alright. If not, contact the pipeline department pipeline@rbw-cgi.it\nThe save will contine'
                        rbwUI.RBWConfirm(text=infoMsg, defCancel=None)

                if self.isSet() or self.isProp():
                    # === Export VRScene and uv set info ===
                    cmd.file(scenefilename, open=True, force=True)
                    if self.getUvSetInfo():
                        infoData = {'multiUV': True}
                    else:
                        infoData = {'multiUV': False}
                    api.json.json_write(infoData, scenefilename.replace('.mb', '_info.json'))

                    self.exportVRscene()

                cmd.file(scenefilename, open=True, force=True)
                if self.isCharacter() or self.isProp():
                    exportFxData.exportAlembicForFx()

                if cmd.objExists('dynamic_geos'):
                    self.exportDynGeo()

                self.exportAlembicInfo()

            elif self.getDeptType() == 'uvs':
                expFbx.export(combine=True)

        cmd.file(scenefilename, open=True, force=True)
        self.closeAllWindows()

        # leave this!!
        return True

    ############################################################################
    # @brief      Closes all windows.
    #
    def closeAllWindows(self):
        for windows in cmd.lsUI(windows=True):
            if 'MayaWindow' in windows:
                continue
            try:
                cmd.deleteUI(windows, window=True)
            except Exception:
                api.log.logger().error('cannot close {}'.format(windows))

    ############################################################################
    # @brief      clean the scene before save.
    #
    def clearSceneToSave(self):
        self.deleteBadReferences()
        self.deleteBadNamespaces()
        self.deleteBadRenderLayers()
        self.deleteBadRenderElenemts()
        self.deleteUknownNodes()
        self.deleteBlendShapes()
        if self.getName() != 'camera':
            self.deleteBadCameras()

    ############################################################################
    # @brief      delete bad reference nodes.
    #
    def deleteBadReferences(self):
        for badReference in cmd.ls(type='reference'):
            if cmd.lockNode(badReference, query=True, lock=True):
                cmd.lockNode(badReference, lock=False)
            try:
                referencePath = cmd.referenceQuery(badReference, filename=True, withoutCopyNumber=True)
                cmd.file(referencePath, removeReference=True)
            except:
                cmd.delete(badReference)

        if cmd.objExists('LIBRARY'):
            libraryElements = cmd.listRelatives('LIBRARY')

            if not libraryElements:
                cmd.delete('LIBRARY')

        fosterParents = cmd.ls(type='fosterParent')
        if len(fosterParents) > 0:
            for fosterParent in fosterParents:
                cmd.delete(fosterParent)

    ############################################################################
    # @brief      delete all the namespaces in scene.
    #
    def deleteBadNamespaces(self):
        defaults = ['UI', 'shared']

        # Used as a sort key, this will sort namespaces by how many children they have.
        def num_children(ns):
            return ns.count(':')

        namespaces = [namespace for namespace in cmd.namespaceInfo(lon=True, r=True) if namespace not in defaults]
        # We want to reverse the list, so that namespaces with more children are at the front of the list.
        namespaces.sort(key=num_children, reverse=True)
        for namespace in namespaces:
            try:
                cmd.namespace(rm=namespace)
            except RuntimeError:
                api.log.logger().error('The namespace: {} is not empty so i can\'t delete it.')
                pass

    ############################################################################
    # @brief      delete all the rendere layers
    #
    # @param      self  The object
    #
    def deleteBadRenderLayers(self):
        import maya.app.renderSetup.model.renderSetup as renderSetup
        rs = renderSetup.instance()
        rs.clearAll()

    ############################################################################
    # @brief      delete all the render elements in scene
    #
    # @param      self  The object
    #
    def deleteBadRenderElenemts(self):
        api.log.logger().debug('export render elements: {}'.format(self.savedAsset.exportElements))
        if not self.savedAsset.exportElements and api.vray.isVrayRightLoaded():
            renderElementsFilter = cmd.itemFilter(byType='VRayRenderElement')
            renderElementsSetFilter = cmd.itemFilter(byType='VRayRenderElementSet')
            allRenderElementsFilter = cmd.itemFilter(union=(renderElementsFilter, renderElementsSetFilter))
            elements = cmd.lsThroughFilter(allRenderElementsFilter)

            if elements is not None:
                for element in elements:
                    cmd.delete(element)
        else:
            api.log.logger().debug('Voglio esportare i render element')

    ############################################################################
    # @brief      delete the unknown nodes in scene.
    #
    def deleteUknownNodes(self):
        unknownNodes = cmd.ls(type="unknown")
        unknownNodes += cmd.ls(type="unknownDag")
        for item in unknownNodes:
            if cmd.objExists(item):
                cmd.lockNode(item, lock=False)
                cmd.delete(item)

    ############################################################################
    # @brief      delete all blendshapes in scene.
    #
    def deleteBlendShapes(self):
        # azzero le bs esisenti con questa reference
        for bs in cmd.ls(type='blendShape'):
            inputConnections = cmd.listConnections('{}.inputTarget'.format(bs), source=True, destination=False)
            for inputConnection in inputConnections:
                inputMesh = inputConnection.split(':')[1] if ':' in inputConnection else inputConnection
                # restore bs value at 0
                cmd.setAttr('{}.{}'.format(bs, inputMesh), 0)
                # delete blendshape node
                cmd.delete(bs)

    ############################################################################
    # @brief      delete all the not default Maya camera
    #
    # @param      self  The object
    #
    def deleteBadCameras(self):
        defaultCameras = ['frontShape', 'perspShape', 'sideShape', 'topShape']
        for camera in cmd.ls(type='camera'):
            if camera not in defaultCameras:
                cmd.select(camera)
                cameraTransform = cmd.pickWalk(direction='up')
                cmd.delete(cameraTransform)

    ############################################################################
    # @brief      edit the viewport.
    #
    def editViewport(self):
        # Display Smooth a 0 su tutte le mesh
        cmd.select(cmd.ls(assemblies=1), replace=1)
        cmd.displaySmoothness(polygonObject=1)
        cmd.select(clear=1)

        # bounding box view
        modelPanels = cmd.getPanel(type="modelPanel")
        visiblePanels = cmd.getPanel(visiblePanels=True)
        modelPanel = list(set(modelPanels) & set(visiblePanels))[0]
        cmd.modelEditor(modelPanel, edit=True, displayAppearance='boundingBox')

    ############################################################################
    # @brief      export cnts data.
    #
    def exportCNTs(self):
        import re
        import common.makeButton.makeButton as makeButton

        cntSets = ['CNT']

        for set in cmd.ls('CNT*'):
            if re.match(r"CNT_\d{{3}}", set):
                cntSets.append(set)

        api.log.logger().debug('CNTSSETS: {}'.format(cntSets))

        rivets = []
        rivetsData = {}
        for currentSet in cntSets:
            edges = []
            for elem in cmd.sets(currentSet, q=True):
                if 'Shape' not in elem:
                    edges.append(elem)

            # costruisco il rivet
            cmd.select(clear=True)
            cmd.select(edges, replace=True)
            rivetGroup = makeButton.DM_buttonMaker()
            rivetGroup = cmd.rename(rivetGroup, 'RVT_{}'.format(self.savedAsset.getNamespace()))
            rivets.append(cmd.listRelatives(rivetGroup, type='joint', fullPath=True)[0])

        if rivets:
            for rivet in rivets:
                rawTranslation = cmd.xform(rivet, query=True, translation=True, worldSpace=True)
                rawRotation = cmd.xform(rivet, query=True, rotation=True, worldSpace=True)
                rawScale = cmd.xform(rivet, query=True, scale=True, worldSpace=True)

                translation = [float(Decimal(x)) for x in rawTranslation]
                rotation = [float(Decimal(x)) for x in rawRotation]
                scale = [float(Decimal(x)) for x in rawScale]

                rivetsData[rivet] = {'translation': translation, 'rotation': rotation, 'scale': scale}

            scenefilename = api.scene.scenename()
            jsonFileName = scenefilename.replace('.mb', '_cnts.json')
            api.json.json_write(rivetsData, jsonFileName)

    ############################################################################
    # @brief      export vr mesh function.
    #
    # @return     vrmesh file name and a boolean for the noSmooth assets
    #
    def exportVRmesh(self):
        scenefilename = api.scene.scenename()
        sceneFolder = os.path.dirname(scenefilename)
        sceneName = os.path.basename(scenefilename)

        noSmoothAsset = False

        vrmeshFolder = os.path.join(sceneFolder, 'vrmeshes').replace('\\', '/')
        if not os.path.exists(vrmeshFolder):
            os.makedirs(vrmeshFolder)

        allMeshes = cmd.ls(type='mesh', long=True)
        mshMeshes = cmd.listRelatives('MSH', fullPath=True)

        displacements = {}
        vrayDisplacementNodes = cmd.ls(type='VRayDisplacement')
        if len(vrayDisplacementNodes) == 0:
            noSmoothAsset = True

        for vrayDisplacementNode in vrayDisplacementNodes:
            meshes = cmd.listRelatives(vrayDisplacementNode, children=1, fullPath=1)
            displacements[vrayDisplacementNode] = meshes

        extras = {}
        extraSets = cmd.ls('EXTRA_*', type='objectSet')
        if extraSets:
            for extraSet in extraSets:
                extras[extraSet] = cmd.listRelatives(extraSet, fullPath=True)

        for mesh in allMeshes:
            renderStats = {}
            for attr in renderStatsAttrs:
                renderStats[attr] = cmd.getAttr('{}.{}'.format(mesh, attr))

            cmd.select(mesh, replace=True)
            displace = None
            for vrayDisplacementNode in vrayDisplacementNodes:
                if mesh in displacements[vrayDisplacementNode]:
                    displace = vrayDisplacementNode
                    break

            msh = False
            if mesh in mshMeshes:
                msh = True

            extra = None
            for extraSet in extras:
                if mesh in extras[extraSet]:
                    extra = extraSet
                    break

            vrmeshName = cmd.listRelatives(mesh, parent=True)[0]
            meshVisibility = cmd.getAttr('{}.visibility'.format(vrmeshName))

            if displace:
                cmd.sets(vrmeshName, remove=displace)
            if msh:
                cmd.sets(vrmeshName, remove='MSH')
            if extra:
                cmd.sets(vrmeshName, remove=extra)

            parentGroup = '|'.join(cmd.ls(vrmeshName, long=True)[0].split('|')[:-1])
            cmd.parent(mesh, world=True)
            api.vray.createVrayProxy(vrmeshFolder, vrmeshName)
            cmd.setAttr('{}.geomType'.format(vrmeshName), 1)
            for attr in renderStats:
                cmd.setAttr('{}.{}'.format(vrmeshName, attr))
            cmd.parent(vrmeshName, parentGroup)

            if displace:
                cmd.sets(vrmeshName, addElement=displace)
            if msh:
                cmd.sets(vrmeshName, addElement='MSH')
            if extra:
                cmd.sets(vrmeshName, addElement=extra)

            cmd.setAttr('{}.visibility'.format(vrmeshName), meshVisibility)

        for i in range(0, len(vrayDisplacementNodes)):
            vrayDisplacementNode = vrayDisplacementNodes[i]
            cmd.vray('addAttributesFromGroup', vrayDisplacementNode, 'vray_subdivision', True)
            try:
                cmd.setAttr('{}.vrayOsdSubdivEnable'.format(vrayDisplacementNode), 0)
            except RuntimeError:
                pass
            cmd.setAttr('{}.vrayPreserveMapBorders'.format(vrayDisplacementNode), 0)
            mel.eval('vrayUpdateAE;')

        cmd.select(clear=True)
        mel.eval("MLdeleteUnused;")

        vrmeshfilename = os.path.join(sceneFolder, sceneName.replace('.mb', '_vrmesh.ma')).replace('\\', '/')
        cmd.file(rename=vrmeshfilename)
        cmd.file(save=True, force=True, exportSelected=False, type='mayaAscii')

        api.log.logger().debug(60 * '!!!')
        api.log.logger().debug('save the file {}'.format(vrmeshfilename))
        api.log.logger().debug(60 * '!!!')

        return vrmeshfilename, noSmoothAsset

    ############################################################################
    # @brief      export dynamic geos.
    #
    def exportDynGeo(self):
        scenefilename = api.scene.scenename()
        surfacingDynGeoFolder = os.path.join('/'.join(scenefilename.split('/')[:-4]), 'export', 'surfacing', 'extraGeos').replace('\\', '/')

        if not os.path.exists(surfacingDynGeoFolder):
            os.makedirs(surfacingDynGeoFolder)
            version = 1
        else:
            version = int(sorted([dir for dir in os.listdir(surfacingDynGeoFolder)])[-1].split('vr')[1]) + 1

        exportFolder = os.path.join(surfacingDynGeoFolder, 'vr{}'.format(str(version).zfill(3))).replace('\\', '/')
        os.makedirs(exportFolder)

        dynGeos = []

        for shape in cmd.listRelatives('dynamic_geos'):
            dynGeos.append(cmd.listRelatives(shape, parent=True)[0])

        for geo in dynGeos:
            alembicPath = os.path.join(exportFolder, '{}.abc'.format(geo)).replace('\\', '/')
            alembicCmd = "-frameRange 1 1 -stripNamespaces -uvWrite -worldSpace -dataFormat ogawa -root {} -file {}".format(geo, alembicPath)

            cmd.AbcExport(j=alembicCmd)

    ############################################################################
    # @brief      copy texture from sourceimages.
    #
    def copyTexturesFromSourceImages(self):
        texturesFolder = os.path.join(self.savedAsset.savedAssetFolder, 'textures').replace('\\', '/')
        if not os.path.exists(texturesFolder):
            os.makedirs(texturesFolder)

        for fileNode in cmd.ls(type='file'):
            sourceImagesFileName = cmd.getAttr('{}.fileTextureName'.format(fileNode))
            fileName = os.path.basename(sourceImagesFileName)

            newFileName = os.path.join(texturesFolder, fileName).replace('\\', '/')
            shutil.copy2(sourceImagesFileName, texturesFolder)

            cmd.setAttr('{}.fileTextureName'.format(fileNode), newFileName, type='string')

    ############################################################################
    # @brief      finalize textures.
    #
    def finalizeTextures(self):
        mbPath = os.path.join(self.savedAsset.savedAssetFolder, '{}.mb'.format(self.savedAsset.sceneName)).replace('\\', '/')
        if not self.isLightrig():
            texture_convert.finalizeTextures(sceneName=mbPath)
        else:
            self.copyTexturesFromSourceImages()

    ############################################################################
    # @brief      delete all poly surface shape.
    #
    def deletePolySurfaceShape(self):
        cmd.delete(cmd.ls('*polySurfaceShape*'))

    ############################################################################
    # @brief      { function_description }
    #
    def exportAlembicInfo(self):
        alembicData = {}
        alembicNodes = cmd.ls(type='AlembicNode')
        vrayProxies = cmd.ls(type='VRayProxy')
        for alembicNode in alembicNodes:
            alembicDict = {'node': alembicNode, 'path': cmd.getAttr('{}.abc_File'.format(alembicNode)), 'objects': []}
            for mesh in set(cmd.listConnections(alembicNode, destination=True, source=False)):
                if cmd.nodeType(mesh) == 'transform':
                    shape = cmd.listRelatives(mesh)[0]
                    alembicDict['objects'].append({'transform': mesh, 'shape': shape})
            for proxy in vrayProxies:
                if cmd.getAttr('{}.fileName'.format(proxy)) == cmd.getAttr('{}.abc_File'.format(alembicNode)):
                    alembicDict['proxy'] = proxy
                    break
            alembicData[alembicNode] = alembicDict

        jsonPath = os.path.join(self.savedAsset.savedAssetFolder, '{}_alembic.json'.format(self.savedAsset.sceneName)).replace('\\', '/')
        if alembicData:
            api.json.json_write(alembicData, jsonPath)

    ############################################################################
    # @brief      prepare the shader scene for the farm before the save.
    #
    def prepareShaderSceneToSave(self):
        # turnoff the ground and on the spherical geo
        cmd.setAttr('*:ground_geo.visibility', 0)
        cmd.setAttr('*:hemisphere_geo.visibility', 1)

        framesMap = {
            'MSH_cube': 1,
            'MSH_cloth': 2,
            'MSH_plane': 3,
            'MSH_liquid': 4,
            'MSH_substanceSphere': 5,
            'MSH_glassSheet': 6,
            'GRP_refractiveMesh': 7,
            'MSH_cup': 8,
            'MSH_custom': 9
        }

        geoListString = ''

        geos = cmd.listRelatives('viewtool_geos_shader_:GRP_geos')
        visibleGeos = []
        for geo in geos:
            if cmd.getAttr('{}.visibility'.format(geo)) and geo.startswith('viewtool_geos_shader_:MSH_'):
                visibleGeos.append(geo)

        constraints = cmd.ls(type='parentConstraint')
        if len(constraints) == 1:
            cmd.currentTime(1)
            constraint = constraints[0]
            cmd.delete(constraint)

        if len(visibleGeos) == 1:
            visibleGeo = visibleGeos[0]
            geoListString = visibleGeo
            frame = framesMap[visibleGeo.split(':')[1]]
            cmd.currentTime(frame)

        name = self.getName()
        variant = self.getVariant()

        shaderName = '{}{}{}'.format(name, variant[0].upper(), variant[1:])
        renderLayerName = 'SHADER_{}_RL'.format(shaderName)
        api.renderlayer.createFromTemplate('SHADER', shaderName)

        hemisphereGeos = cmd.ls('*:hemisphere_geo')
        if len(hemisphereGeos) == 1:
            geoListString = '{}\n{}'.format(geoListString, hemisphereGeos[0])

        hdriSphereGeos = cmd.ls('*:hdriSphereReflection_geo')
        if len(hdriSphereGeos) == 1:
            geoListString = '{}\n{}'.format(geoListString, hdriSphereGeos[0])

        api.renderlayer.switchByName(renderLayerName)
        geoCol = api.renderlayer.getRenderLayerCollectionByName(renderLayerName, 'geo_col')
        geoCol.getSelector().setStaticSelection(geoListString)

        lightsCol = api.renderlayer.getRenderLayerCollectionByName(renderLayerName, 'lights_col')
        lightsCol.getSelector().setPattern('*:hdri_ML0_*')

    ############################################################################
    # @brief      Sends a shader scene to farm.
    #
    def sendShaderSceneToFarm(self):
        defFolder = self.savedAsset.savedAssetFolder
        farmBackUpFolder = os.path.join(defFolder, 'farm').replace('\\', '/')
        if not os.path.exists(farmBackUpFolder):
            os.makedirs(farmBackUpFolder)
        backUpScene = os.path.join(farmBackUpFolder, '{}_farmBackup.mb'.format(self.savedAsset.sceneName)).replace('\\', '/')
        cmd.file(rename=backUpScene)
        cmd.file(save=True, force=True, exportSelected=False, type='mayaBinary')

        name = self.getName()
        variant = self.getVariant()
        shaderName = '{}{}{}'.format(name, variant[0].upper(), variant[1:])
        layer = 'SHADER_{}_RL'.format(shaderName)

        version = self.savedAsset.sceneName.split('_')[-1]

        frame = str(int(cmd.currentTime(query=True)))

        vrsceneFileName = os.path.join(farmBackUpFolder, '<Layer>.vrscene').replace('\\', '/')

        kickOffJobSettings = {
            'jobName': 'sl_{}_{}'.format(shaderName, version),
            'batchName': 'sl_{}_KICKOFF'.format(shaderName),
            'inputFileName': backUpScene,
            'outputFileName': vrsceneFileName,
            'goal': 'lighting vrscene caching',
            'layer': layer,
            'dept': 'Surfacing',
            'frames': frame,
            'current_frame': frame,
            'camera': 'viewtool_camera_shader_:CAMERA35mmShader',
            'enableAnimatedGIJobs': False,
            'enableGIJobs': False,
            'resolution_value': '2048x2048',
            'renderDeep': False,
            'renderBeauty': True,
            'notes': 'From shader save Def',
            'mode': 'fml',
            'isLightJob': False,
            'isHighJob': False,
            'extraAttr': '',
            'extraTime': False,
            'priority': None,
            'chunk': False,
            'suspended': False,
            'overscan': False,
            'vrscenes': False,
            'obj': self.getAsset()
        }

        kickOffJob = api.farm.createAndSubmitJob(kickOffJobSettings)

        jobSettings = {
            'jobName': 'sl_{}_{}'.format(shaderName, version),
            'batchName': 'sl_{}_RENDERING'.format(shaderName),
            'inputFileName': vrsceneFileName.replace('<Layer>', layer),
            'outputFileName': self.path.replace('.mb', '.jpg'),
            'goal': 'rendering',
            'layer': layer,
            'dept': 'Surfacing',
            'dependency': [kickOffJob],
            'frames': frame,
            'current_frame': frame,
            'enableAnimatedGIJobs': False,
            'enableGIJobs': False,
            'resolution_value': '2048x2048',
            'renderDeep': False,
            'renderBeauty': True,
            'notes': 'From shader save Def',
            'mode': 'fml',
            'isLightJob': False,
            'isHighJob': False,
            'extraAttr': '',
            'extraTime': False,
            'priority': None,
            'chunk': False,
            'suspended': False,
            'overscan': False,
            'obj': self.getAsset()
        }

        api.farm.createAndSubmitJob(jobSettings)

    ############################################################################
    # @brief      Gets the shader information.
    #
    # @return     The shader information.
    #
    def getShaderInfoes(self):
        shaderInfoes = {}
        for material in api.scene.listUsedSceneMaterials():
            if ':' not in material and material != 'lambert1':
                materialType = cmd.nodeType(material)
                textures, multiSubText = self.getInfoes(material)
                shaderInfoes['material'] = {'type': materialType}
                shaderInfoes['material']['textures'] = textures
                shaderInfoes['material']['multiSubText'] = multiSubText

        return shaderInfoes

    ############################################################################
    # @brief      Gets the infos.
    #
    # @param      node  The node
    #
    # @return     The infos.
    #
    def getInfoes(self, node):
        textures = 0
        multiSubText = False
        connections = cmd.listConnections(node, destination=False)
        if connections:
            if len(connections) == 1 and connections[0] == node:
                return textures, False

            for connection in connections:
                if cmd.nodeType(connection) == 'file':
                    textures += 1
                elif cmd.nodeType(connection) == 'VRayMultiSubTex':
                    multiSubText = True
                else:
                    pass
                subTextures, subMultiSubText = self.getInfoes(connection)
                textures += subTextures
                multiSubText = multiSubText | subMultiSubText
            return textures, multiSubText
        else:
            return textures, False

    ############################################################################
    # @brief      Writes shader infos.
    #
    def writeShaderInfoes(self):
        shaderData = self.getShaderInfoes()
        shaderInfoesPath = self.path.replace('.mb', '_shader_infoes.json')
        api.json.json_write(shaderData, shaderInfoesPath)

    ############################################################################
    # @brief      prepare the fur scene for the farm before the save.
    #
    def prepareFurSceneToSave(self):
        # turnoff the ground and on the spherical geo
        cmd.setAttr('*:ground_geo.visibility', 0)
        cmd.setAttr('*:hemisphere_geo.visibility', 1)

        topGroup = cmd.ls('GRP_FUR_*')[0]
        geoListString = '{}'.format(topGroup)

        constraints = cmd.ls(type='parentConstraint')
        if len(constraints) == 1:
            cmd.currentTime(1)
            constraint = constraints[0]
            cmd.delete(constraint)

        name = self.getName()
        variant = self.getVariant()

        furName = '{}{}{}'.format(name, variant[0].upper(), variant[1:])
        renderLayerName = 'FUR_{}_RL'.format(furName)
        api.renderlayer.createFromTemplate('FUR', furName)

        hemisphereGeos = cmd.ls('*:hemisphere_geo')
        if len(hemisphereGeos) == 1:
            geoListString = '{}\n{}'.format(geoListString, hemisphereGeos[0])

        hdriSphereGeos = cmd.ls('*:hdriSphereReflection_geo')
        if len(hdriSphereGeos) == 1:
            geoListString = '{}\n{}'.format(geoListString, hdriSphereGeos[0])

        api.renderlayer.switchByName(renderLayerName)
        geoCol = api.renderlayer.getRenderLayerCollectionByName(renderLayerName, 'geo_col')
        geoCol.getSelector().setStaticSelection(geoListString)

        lightsCol = api.renderlayer.getRenderLayerCollectionByName(renderLayerName, 'lights_col')
        lightsCol.getSelector().setPattern('*:hdri_ML0_*')

    ############################################################################
    # @brief      Sends a shader scene to farm.
    #
    def sendFurSceneToFarm(self):
        vrayFurs, renderTypes = self.selectRenderView()
        for vrayFur in vrayFurs:
            furType = vrayFur.split('_')[2]
            vrayFurTransform = cmd.listRelatives(vrayFur, parent=True)[0]
            if furType in renderTypes:
                if renderTypes[furType]:
                    cmd.setKeyframe(vrayFurTransform, value=True, attribute='visibility', t=1)
                else:
                    cmd.setKeyframe(vrayFurTransform, value=False, attribute='visibility', t=1)
            else:
                cmd.setKeyframe(vrayFurTransform, value=False, attribute='visibility', t=1)
        isThereLod = False
        for vrayFur in vrayFurs:
            furType = vrayFur.split('_')[2]
            vrayFurTransform = cmd.listRelatives(vrayFur, parent=True)[0]
            if furType == 'LOD':
                isThereLod = True
                cmd.setKeyframe(vrayFurTransform, value=True, attribute='visibility', t=2)
            else:
                cmd.setKeyframe(vrayFurTransform, value=False, attribute='visibility', t=2)

        defFolder = self.savedAsset.savedAssetFolder
        farmBackUpFolder = os.path.join(defFolder, 'farm')
        if not os.path.exists(farmBackUpFolder):
            os.makedirs(farmBackUpFolder)
        backUpScene = os.path.join(farmBackUpFolder, '{}_farmBackup.mb'.format(self.savedAsset.sceneName)).replace('\\', '/')
        cmd.file(rename=backUpScene)
        cmd.file(save=True, force=True, exportSelected=False, type='mayaBinary')

        name = self.getName()
        variant = self.getVariant()
        furName = '{}{}{}'.format(name, variant[0].upper(), variant[1:])
        layer = 'FUR_{}_RL'.format(furName)

        version = self.savedAsset.sceneName.split('_')[-1]

        if isThereLod:
            frame = '1-2'
        else:
            frame = '1'

        vrsceneFileName = os.path.join(farmBackUpFolder, '<Layer>.vrscene')

        kickOffJobSettings = {
            'jobName': 'fur_{}_{}'.format(furName, version),
            'batchName': 'fur_{}_KICKOFF'.format(furName),
            'inputFileName': backUpScene,
            'outputFileName': vrsceneFileName,
            'goal': 'lighting vrscene caching',
            'layer': layer,
            'dept': 'Surfacing',
            'frames': frame,
            'current_frame': frame,
            'camera': 'viewtool_camera_fur_:CAMERA35mmFur',
            'enableAnimatedGIJobs': False,
            'enableGIJobs': False,
            'resolution_value': '2048x2048',
            'renderDeep': False,
            'renderBeauty': True,
            'notes': 'From fur save Def',
            'mode': 'fml',
            'isLightJob': False,
            'isHighJob': False,
            'extraAttr': '',
            'extraTime': False,
            'priority': None,
            'chunk': False,
            'suspended': False,
            'overscan': False,
            'vrscenes': False,
            'obj': self.savedAsset.getAsset()
        }

        kickOffJob = api.farm.createAndSubmitJob(kickOffJobSettings)

        jobSettings = {
            'jobName': 'fur_{}_{}'.format(furName, version),
            'batchName': 'fur_{}_RENDERING'.format(furName),
            'inputFileName': vrsceneFileName.replace('.vrscene', '_0001.vrscene').replace('<Layer>', layer),
            'outputFileName': '{}.jpg'.format(self.path),
            'goal': 'rendering',
            'layer': layer,
            'dept': 'Surfacing',
            'dependency': [kickOffJob],
            'frames': frame,
            'current_frame': frame,
            'enableAnimatedGIJobs': False,
            'enableGIJobs': False,
            'resolution_value': '2048x2048',
            'renderDeep': False,
            'renderBeauty': True,
            'notes': 'From fur save Def',
            'mode': 'fml',
            'isLightJob': False,
            'isHighJob': False,
            'extraAttr': '',
            'extraTime': False,
            'priority': None,
            'chunk': False,
            'suspended': False,
            'overscan': False,
            'obj': self.savedAsset.getAsset()
        }

        api.farm.createAndSubmitJob(jobSettings)

        for vrayFur in vrayFurs:
            cmd.cutKey(cmd.listRelatives(vrayFur, parent=True)[0], time=(1, 2), attribute='visibility')

    ############################################################################
    # @brief      Gets the fur infos.
    #
    # @return     The fur infos.
    #
    def getFurInfos(self):
        furTypes = []
        activeTypes = []
        vrayFurs = cmd.ls(type='VRayFurPreview')

        for vrayFur in vrayFurs:
            furType = vrayFur.split('_')[2]
            if furType not in furTypes:
                furTypes.append(furType)
            furParent = cmd.listRelatives(vrayFur, parent=True)[0]
            if cmd.getAttr('{}.visibility'.format(furParent)):
                activeTypes.append(furType)

        data = {'fur': {'typeNumber': len(furTypes), 'types': ' - '.join(furTypes), 'defaultType': ' - '.join(activeTypes)}}

        return data

    ############################################################################
    # @brief      Writes fur infos.
    #
    def writeFurInfos(self):
        furData = self.getFurInfos()
        furInfosPath = self.path.replace('.mb', '.json')
        api.json.json_write(furData, furInfosPath)

    ############################################################################
    # @brief      select the render views.
    #
    def selectRenderView(self):
        furTypes = []
        vrayFurs = cmd.ls(type='VRayFurPreview')

        for vrayFur in vrayFurs:
            furType = vrayFur.split('_')[2]
            if furType not in furTypes:
                furTypes.append(furType)

        dialog = FurTypeDialog(furTypes, parent=QtWidgets.QApplication.activeWindow())
        if dialog.exec_():
            results = dialog.getInputs()

        return vrayFurs, results


################################################################################
# @brief      This class describes a fur type dialog.
#
class FurTypeDialog(QtWidgets.QDialog):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      typesList  The types list
    # @param      parent     The parent
    #
    def __init__(self, typesList, parent=None):
        super(FurTypeDialog, self).__init__(parent)
        self.typesList = typesList
        self.fields = []

        self.setObjectName('FurTypePanel')
        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.mainLayout = QtWidgets.QVBoxLayout(self)

        self.checkBoxGroup = QtWidgets.QGroupBox('Choose fur type to render:')
        self.checkBoxGroupLayout = QtWidgets.QVBoxLayout()
        for furType in self.typesList:
            if furType != 'LOD' and furType != 'CU':
                tmpButton = QtWidgets.QCheckBox(furType)
                tmpButton.setObjectName(furType)
                self.checkBoxGroupLayout.addWidget(tmpButton)
                self.fields.append(tmpButton)
        self.checkBoxGroup.setLayout(self.checkBoxGroupLayout)
        self.mainLayout.addWidget(self.checkBoxGroup)

        self.checkBoxGroup.setStyleSheet('''
            QGroupBox
            {
                font: bold;
                border: 1px solid #2d2d2d;
                border-radius: 6px;
                margin-top: 6px;
            }
        ''')

        self.checkBoxGroup.setStyleSheet('''
            QGroupBox::title
            {
                subcontrol-origin: margin;
                left: 7px;
                padding: 0px 5px 0px 5px;
                color: white;
            }
        ''')

        self.okButton = QtWidgets.QPushButton('OK')
        self.okButton.setAutoDefault(False)
        self.cancelButton = QtWidgets.QPushButton('Cancel')
        self.cancelButton.setAutoDefault(False)
        self.buttonBox = QtWidgets.QDialogButtonBox()
        self.okButton.clicked.connect(self.accept)
        self.buttonBox.addButton(self.okButton, QtWidgets.QDialogButtonBox.ActionRole)
        self.buttonBox.addButton(self.cancelButton, QtWidgets.QDialogButtonBox.RejectRole)
        self.cancelButton.clicked.connect(self.reject)

        self.mainLayout.addWidget(self.buttonBox)

        self.setWindowTitle('Fur Type For Render')
        self.setFocus()

    ############################################################################
    # @brief      Gets the inputs.
    #
    # @return     The inputs.
    #
    def getInputs(self):
        inputs = {}
        for field in self.fields:
            inputs[field.objectName()] = field.isChecked()

        return inputs
