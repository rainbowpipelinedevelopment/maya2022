import importlib
from PySide2 import QtWidgets, QtCore
import os
import xml.etree.ElementTree as ET

import maya.cmds as cmd
import maya.mel as mel

import api.scene
import api.asset
import api.savedAsset
import api.widgets.rbw_UI as rbwUI
import lookdev.lookdevTools as lookdevTools

importlib.reload(api.scene)
# importlib.reload(api.asset)
# importlib.reload(api.savedAsset)
# importlib.reload(rbwUI)
importlib.reload(lookdevTools)


class CloneAssetUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(CloneAssetUI, self).__init__()

        if cmd.window('CloneAssetUI', exists=True):
            cmd.deleteUI('CloneAssetUI')

        self.mcFolder = os.getenv('MC_FOLDER').replace('\\', '/')
        self.id_mv = os.getenv('ID_MV')

        self.assets = lookdevTools.getAllAssets()

        self.setObjectName('CloneAssetUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)

        self.mainLayout.addWidget(self.splitter)

        ########################################################################
        # LEFT PANEL
        #
        self.leftWidget = rbwUI.RBWFrame(layout='V', radius=5)

        # ------------------------ asset features ---------------------------- #
        self.assetFeaturesGroup = rbwUI.RBWGroupBox(title='Asset:', fontSize=13, layout='V')

        self.categoryComboBox = rbwUI.RBWComboBox(text='Category:', bgColor='transparent')
        self.categoryComboBox.activated.connect(self.fillGroupComboBox)

        self.assetFeaturesGroup.addWidget(self.categoryComboBox)

        self.groupComboBox = rbwUI.RBWComboBox(text='Group:', bgColor='transparent')
        self.groupComboBox.activated.connect(self.fillNameComboBox)

        self.assetFeaturesGroup.addWidget(self.groupComboBox)

        self.nameComboBox = rbwUI.RBWComboBox(text='Name:', bgColor='transparent')
        self.nameComboBox.activated.connect(self.fillVariantComboBox)

        self.assetFeaturesGroup.addWidget(self.nameComboBox)

        self.variantComboBox = rbwUI.RBWComboBox(text='Variant:', bgColor='transparent')
        self.variantComboBox.activated.connect(self.setCurrentVariant)

        self.assetFeaturesGroup.addWidget(self.variantComboBox)

        self.leftWidget.addWidget(self.assetFeaturesGroup)

        # --------------------------- set button ---------------------------- #
        self.setButton = rbwUI.RBWButton(text='Set', icon=['right.png'], size=[182, 35])
        self.setButton.clicked.connect(self.checkToProceed)

        self.leftWidget.addWidget(self.setButton)

        self.splitter.addWidget(self.leftWidget)

        ########################################################################
        # RIGHT PANEL
        #
        self.rightPanel = rbwUI.RBWFrame(layout='V')

        # -------------------------- scroll message ------------------------ #
        self.label = rbwUI.RBWLabel('', italic=False)
        self.scrollArea = QtWidgets.QScrollArea()
        self.scrollArea.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.scrollArea.setStyleSheet('background-color: rgba(30, 30, 30, 150); border-radius: 5px')
        self.scrollWidget = rbwUI.RBWFrame('V', margins=[0, 0, 0, 0], bgColor='transparent')
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setWidget(self.scrollWidget)
        self.scrollWidget.addWidget(self.label, alignment=QtCore.Qt.AlignCenter)

        self.rightPanel.addWidget(self.scrollArea)

        # --------------------------- clone button ------------------------- #
        self.cloneButton = rbwUI.RBWButton(text='Clone', icon=['clone.png'], size=[150, 35])
        self.cloneButton.setEnabled(False)
        self.cloneButton.clicked.connect(self.cloneAsset)

        self.rightPanel.addWidget(self.cloneButton)

        self.splitter.addWidget(self.rightPanel)

        self.fillCategoryComboBox()

        self.splitter.setStretchFactor(0, 1)
        self.splitter.setStretchFactor(1, 3)

        self.setMinimumSize(840, 250)
        self.setTitle('Clone Asset Tool')
        self.setIcon('clone.png')
        self.setFocus()

    ############################################################################
    # @brief      fill the category combobox.
    #
    def fillCategoryComboBox(self):
        self.categoryComboBox.clear()

        for category in sorted(self.assets.keys()):
            self.categoryComboBox.addItem(category)
        self.categoryComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

    ############################################################################
    # @brief      fill the group combobox.
    #
    def fillGroupComboBox(self):
        self.groupComboBox.clear()
        self.currentCategory = self.categoryComboBox.currentText()

        for group in sorted(self.assets[self.currentCategory].keys()):
            self.groupComboBox.addItem(group)
        self.groupComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

    ############################################################################
    # @brief      fill the name combobox.
    #
    def fillNameComboBox(self):
        self.nameComboBox.clear()
        self.currentGroup = self.groupComboBox.currentText()

        for name in sorted(self.assets[self.currentCategory][self.currentGroup].keys()):
            self.nameComboBox.addItem(name)
        self.nameComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

    ############################################################################
    # @brief      fill the varinat combobox.
    #
    def fillVariantComboBox(self):
        self.variantComboBox.clear()
        self.currentName = self.nameComboBox.currentText()

        for variant in sorted(self.assets[self.currentCategory][self.currentGroup][self.currentName]):
            self.variantComboBox.addItem(variant)
        self.variantComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

    ############################################################################
    # @brief      Sets the current variant.
    #
    def setCurrentVariant(self):
        self.currentVariant = self.variantComboBox.currentText()
        self.currentAsset = api.asset.Asset(params={'category': self.currentCategory, 'group': self.currentGroup, 'name': self.currentName, 'variant': self.currentVariant})

    ############################################################################
    # @brief      Update the UI if you can proceed.
    #
    def checkToProceed(self):
        modelingSavedAssetParams = {'asset': self.currentAsset, 'dept': 'modeling', 'deptType': 'hig', 'approvation': 'def'}
        modelingSavedAsset = api.savedAsset.SavedAsset(params=modelingSavedAssetParams)

        latest = modelingSavedAsset.getLatest()
        if latest:
            self.latestModelingSavedAsset = api.savedAsset.SavedAsset(params={'db_id': latest[0], 'approvation': 'def'})

            assetTopGroups = api.scene.getTopGroups()
            if len(assetTopGroups) == 1:
                self.assetTopGroup = assetTopGroups[0]

                self.getXmlFile()
                self.getModelingShapes()
                self.getSceneShapes()

                # CHECK MESH NUMBER
                if len(self.modelingShapes.keys()) == len(self.sceneShapes.keys()):
                    self.meshNumber = True
                else:
                    self.meshNumber = False

                # CHECK MISSING ITEMS
                self.missingShapes = {}
                for mesh in self.modelingShapes:
                    if mesh not in self.sceneShapes:
                        self.missingShapes[mesh] = self.modelingShapes[mesh]

                # CHECK PLUS ITEMS
                self.plusShapes = {}
                for mesh in self.sceneShapes:
                    if mesh not in self.modelingShapes:
                        self.plusShapes[mesh] = self.sceneShapes[mesh]

                # CHECK DIFFERENT VERTICES
                self.differentVerticesShapes = []
                for shape in self.modelingShapes:
                    if shape not in self.missingShapes:
                        if self.modelingShapes[shape]['vertices'] != self.sceneShapes[shape]['vertices']:
                            self.differentVerticesShapes.append(shape)

                # CHECK DIFFERENT HIERARCHY
                self.differentHierarchyShapes = []
                for shape in self.modelingShapes:
                    if shape not in self.missingShapes:
                        modelingShapeFullPath = '|'.join(self.modelingShapes[shape]['fullpath'].split('|')[2:])
                        sceneShapeFullPath = '|'.join(self.sceneShapes[shape]['fullpath'].split('|')[2:])
                        if modelingShapeFullPath != sceneShapeFullPath:
                            self.differentHierarchyShapes.append(shape)

                message = ''

                if not self.meshNumber:
                    message = 'SOURCE AND DESTINATION ASSET HAS A DIFFERENT MESH COUNT.\n- SOURCE MESH COUNT: {}\n- DESTINATION MESH COUNT: {}\n'.format(len(self.modelingShapes.keys()), len(self.sceneShapes.keys()))

                if len(self.missingShapes) > 0:
                    message = '{}THE MISSING MESHES ARE:\n'.format(message)
                    for shape in self.missingShapes:
                        message = '{}- {}\n'.format(message, shape)

                if len(self.plusShapes) > 0:
                    message = '{}THE EXTRA MESHES ARE:\n'.format(message)
                    for shape in self.plusShapes:
                        message = '{}- {}\n'.format(message, shape)

                if len(self.differentHierarchyShapes) > 0:
                    message = '{}SOME MESH HAS A DIFFERENT PLACE IN THE OUTLINE\'S FOLDER STRUCTERES:\n'.format(message)
                    for shape in self.differentHierarchyShapes:
                        message = '{}SOURCE HIERARCHY: {}\nDESTINATION HIERARCHY: {}\n'.format(message, '|'.join(self.sceneShapes[shape]['fullpath'].split('|')[2:]), '|'.join(self.modelingShapes[shape]['fullpath'].split('|')[2:]))

                if message != '':
                    message = 'WARNING! PLEASE READ CAREFULLY.\n{}'.format(message)
                else:
                    message = 'THE TWO ASSETS ARE PERFECTLY COMPATIBLE'

                self.label.setText(message)
                self.cloneButton.setEnabled(True)

            else:
                rbwUI.RBWError(title='Many top groups', text='More than one top group in scene.\nClean outliner and try again.')
                self.cloneButton.setEnabled(False)
        else:
            rbwUI.RBWError(title='No modeling save def', text='No modeling def save for the given asset.')
            self.cloneButton.setEnabled(False)

    ############################################################################
    # @brief      Gets the xml file.
    #
    def getXmlFile(self):
        self.xmlFile = os.path.join(self.mcFolder, self.latestModelingSavedAsset.path).replace('\\', '/').replace('.mb', '.xml')

    ############################################################################
    # @brief      Gets the modeling shapes.
    #
    def getModelingShapes(self):
        tree = ET.parse(self.xmlFile)
        root = tree.getroot()

        self.modelingShapes = {}
        for shape in root.iter('shape'):
            try:
                shapeDict = shape.attrib
                if '|proxy|' not in shapeDict['fullpath']:
                    self.modelingShapes[shape.text] = {'fullpath': shapeDict['fullpath'], 'vertices': int(shapeDict['vertices'])}
            except ValueError:
                pass

    ################################################################################
    # @brief      Gets the scene shapes.
    #
    # @return     The scene shapes.
    #
    def getSceneShapes(self):
        self.sceneShapes = {}

        allSceneShapes = cmd.listRelatives(self.assetTopGroup, allDescendents=True)
        iterList = allSceneShapes[:]

        for testNode in iterList:
            if cmd.nodeType(testNode) != 'mesh':
                allSceneShapes.remove(testNode)

        for shape in allSceneShapes:
            if shape.endswith('Orig') and not cmd.listConnections(shape):
                continue
            else:
                self.sceneShapes[shape] = {
                    'fullpath': mel.eval("longNameOf(\"{}\")".format(shape)),
                    'vertices': cmd.polyEvaluate(shape, vertex=True)
                }

    ############################################################################
    # @brief      get all the used matirial in scene.
    #
    # @return     the matirials.
    #
    def getMaterialsInScene(self):
        for shadingEngine in cmd.ls(type='shadingEngine'):
            if cmd.sets(shadingEngine, q=True):
                for material in cmd.ls(cmd.listConnections(shadingEngine), materials=True):
                    if material.count('_') == 4:
                        yield material

    ################################################################################
    # @brief      copy and rename textures.
    #
    def copyAndRenameTextures(self):
        assetParams = {
            'category': self.currentCategory,
            'group': self.currentGroup,
            'name': self.currentName,
            'variant': self.currentVariant
        }

        for material in self.getMaterialsInScene():
            lookdevTools.copyTextureInContent(assetParams, material=material)

    ############################################################################
    # @brief      rename the asset top group.
    #
    def renameTopGroup(self):
        testShape = self.modelingShapes.keys()[0]
        if self.currentCategory == 'character':
            newTopGroup = self.modelingShapes[testShape]['fullpath'].split('|')[1]
        else:
            newTopGroup = self.modelingShapes[testShape]['fullpath'].split('|')[3]

        oldTopGroup = self.assetTopGroup
        if not self.assetTopGroup.count('GRP_'):
            allGrp = [x for x in cmd.listRelatives(self.assetTopGroup, allDescendents=True) if x.count('GRP_')]
            if allGrp:
                oldTopGroup = allGrp[0]

        cmd.rename(oldTopGroup, newTopGroup)
        self.assetTopGroup = newTopGroup

    ############################################################################
    # @brief      recursive delete meshes.
    #
    # @param      shape  The shape
    #
    def recursiveDelete(self, shape):
        parent = cmd.listRelatives(shape, parent=True)
        cmd.delete(shape)
        api.log.logger().debug('delete: {}'.format(shape))

        if not cmd.listRelatives(parent, allDescendents=True):
            self.recursiveDelete(parent)

    ############################################################################
    # @brief      delete shapes.
    #
    def deleteShapes(self):
        if len(self.plusShapes) > 0:
            for shape in self.plusShapes:
                self.recursiveDelete(self.plusShapes[shape]['fullpath'])

    ############################################################################
    # @brief      recursive create the shape and hit hierarchy.
    #
    # @param      shape  The shape
    #
    def recursiveAdd(self, shape):
        hierarchyToCreate = self.missingShapes[shape]['fullpath'].split('|')[2:-2]
        newTransformShape = cmd.duplicate('*:{}'.format(shape))
        if len(hierarchyToCreate) > 0:
            for i in range(0, len(hierarchyToCreate)):
                transformName = hierarchyToCreate[i]
                if cmd.objExists(transformName):
                    transform = transformName
                else:
                    transform = cmd.createNode('transform', name=transformName)

                if i == 0:
                    targetNode = self.assetTopGroup
                else:
                    targetNode = hierarchyToCreate[i - 1]

                parent = False
                children = cmd.listRelatives(targetNode)
                if not children:
                    parent = True
                elif transform not in children:
                    parent = True

                if parent:
                    api.log.logger().debug('cmd.parent({}, {})'.format(transform, targetNode))
                    cmd.parent(transform, targetNode)
            cmd.parent(newTransformShape, hierarchyToCreate[-1])
        else:
            cmd.parent(newTransformShape, self.assetTopGroup)

    ############################################################################
    # @brief      Adds shapes.
    #
    def addShapes(self):
        if len(self.missingShapes) > 0:
            mbPath = self.xmlFile.replace('.xml', '.mb')
            # import the asset in reference
            api.scene.addToImportGroup(mbPath, 'tmp_ref_')
            for shape in self.missingShapes:
                self.recursiveAdd(shape)
            # delete the asset in reference
            for referencePath in cmd.file(query=True, reference=True):
                cmd.file(referencePath, removeReference=True)
            cmd.delete('IMPORT')

    ############################################################################
    # @brief      Saves a new asset.
    #
    def saveNewAsset(self):
        sceneNameList = api.scene.scenename().split('/')
        assetName = '_'.join(sceneNameList[8:12])
        version = sceneNameList[-1][:-3].split('_')[-1]

        savedAssetParams = {
            'asset': self.currentAsset,
            'deptType': 'rnd',
            'dept': 'surfacing',
            'approvation': 'wip',
            'note': 'Clone from {} wip {}'.format(assetName, version)
        }

        newSavedAsset = api.savedAsset.SavedAsset(params=savedAssetParams)
        newSavedAsset.save()

    ############################################################################
    # @brief      clone asset function.
    #
    def cloneAsset(self):
        # recursive delete
        self.deleteShapes()

        # recursive add
        self.addShapes()

        # copy and rename texture
        self.copyAndRenameTextures()

        # rename top group
        # self.renameTopGroup()

        # save new wip
        self.saveNewAsset()
