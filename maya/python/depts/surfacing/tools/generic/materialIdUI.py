import importlib
import os
from PySide2 import QtCore, QtWidgets, QtGui

import maya.cmds as cmd

import api.json
import api.vray
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.json)
importlib.reload(api.vray)
# importlib.reload(rbwUI)


materialJson = os.path.join(os.getenv('LOCALPIPE'), 'json_file', 'config_material_ID.json').replace('\\', '/')


################################################################################
# @brief      This class describes a material identifier ui.
#
class MaterialIdUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(MaterialIdUI, self).__init__()
        if cmd.window('MaterialIdUI', exists=True):
            cmd.deleteUI('MaterialIdUI')

        self.setObjectName('MaterialIdUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topFrame = rbwUI.RBWFrame(layout='V')
        self.mainLayout.addWidget(self.topFrame)

        # ------------------------ create material id ------------------------ #
        self.createMaterialIdGroup = rbwUI.RBWGroupBox(title='Create New Material Id', fontSize=12, layout='H')

        self.newMaterialIdLine = rbwUI.RBWLineEdit(title='New Id value', text='0', stretch=False)
        self.createNewMaterialIdButton = rbwUI.RBWButton(text='Create Id', icon=['../surfacing/MaterialId.png'], size=[100, 20])
        self.createNewMaterialIdButton.clicked.connect(self.createNewMaterialID)

        self.createMaterialIdGroup.addWidget(self.newMaterialIdLine)
        self.createMaterialIdGroup.addWidget(self.createNewMaterialIdButton)

        self.topFrame.addWidget(self.createMaterialIdGroup)

        # ------------------------ search material id ------------------------ #
        self.searchMaterialIdWidget = rbwUI.RBWFrame(layout='H', bgColor='transparent', margins=[0, 0, 0, 0])

        self.searchMaterialIdLine = rbwUI.RBWLineEdit(title='Search Material Id', stretch=False, bgColor='rgba(30, 30, 30, 150)')
        self.searchMaterialIdLine.returnPressed.connect(self.searchMaterialId)
        self.searchMaterialIdButton = rbwUI.RBWButton(icon=['search.png'])
        self.searchMaterialIdButton.clicked.connect(self.searchMaterialId)

        self.searchMaterialIdWidget.addWidget(self.searchMaterialIdLine)
        self.searchMaterialIdWidget.addWidget(self.searchMaterialIdButton)

        self.topFrame.addWidget(self.searchMaterialIdWidget)

        # ------------------------- material id tree ------------------------ #
        self.materialIdTree = rbwUI.RBWTreeWidget(['Material', 'ID'], decorate=True)
        self.materialIdTree.itemClicked.connect(self.setMaterialID)
        self.materialIdTree.setColumnWidth(0, 275)
        self.materialIdTree.setColumnWidth(1, 25)

        self.topFrame.addWidget(self.materialIdTree)

        # ------------------------ check material id ------------------------- #
        self.checkMaterialIdButton = rbwUI.RBWButton(text='Check Material Id', icon=['check.png'], size=[200, 35])
        self.checkMaterialIdButton.clicked.connect(run_checkMaterialId)

        self.topFrame.addWidget(self.checkMaterialIdButton, alignment=QtCore.Qt.AlignHCenter)

        self.fillTree()

        self.setMinimumSize(400, 300)
        self.setTitle('Material Id')
        self.setIcon('../surfacing/MaterialId.png')
        self.setFocus()

    ############################################################################
    # @brief      fill material Id tree.
    #
    def fillTree(self):
        data = api.json.json_read(materialJson)
        for cat in list(data.keys()):
            catChild = QtWidgets.QTreeWidgetItem(self.materialIdTree)
            catChild.setText(0, cat)
            for mat, dic in sorted(data[cat].items()):
                matChild = QtWidgets.QTreeWidgetItem(catChild)
                matChild.setText(0, mat)
                matChild.setText(1, str(dic['value']))

                if 'color' in dic.keys():
                    matChild.setForeground(0, QtGui.QColor(dic['color']))

    ############################################################################
    # @brief      search materil Id
    #
    def searchMaterialId(self):
        text = self.searchMaterialIdLine.text()
        root = self.materialIdTree.invisibleRootItem()

        for i in range(root.childCount()):
            catItem = root.child(i)
            [catItem.child(x).setHidden(False) for x in range(catItem.childCount())]
            [catItem.child(x).setHidden(True) for x in range(catItem.childCount()) if (not catItem.child(x).text(0).lower().count(text) and not catItem.child(x).text(1).lower().count(text))]

        self.materialIdTree.expandAll()

    ############################################################################
    # @brief      Creates a new material id.
    #
    def createNewMaterialID(self):
        matId = int(self.newMaterialIdLine.text())
        self.setMaterialID(matId=matId)

    ############################################################################
    # @brief      Sets the material id.
    #
    # @param      item    The item
    # @param      column  The column
    # @param      matId   The matrix identifier
    #
    def setMaterialID(self, item=None, column=None, matId=None):
        matId = matId if matId else int(item.text(1))
        selectionObjects = cmd.ls(selection=1)

        for obj in selectionObjects:
            api.vray.assignMaterialId(obj, matId)


############################################################################
# @brief      Creates a new material id.
#
def run_checkMaterialId():
    import depts.surfacing.tools.vray.vrayObjectId as vrayObjectId
    importlib.reload(vrayObjectId)

    vrayObjectId.checkMaterialId(confirm=True)
