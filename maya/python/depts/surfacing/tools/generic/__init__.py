import importlib
import os

iconpath = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'common')


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_chk(*args):
    import depts.modeling.tools.finalize.integrityCheck as minc
    importlib.reload(minc)
    minc.integrityCheck()


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_cloneUI(*args):
    import depts.surfacing.tools.generic.cloneAssetUI as cloneAssetUI
    importlib.reload(cloneAssetUI)

    win = cloneAssetUI.CloneAssetUI()
    win.show()


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_textureManager(*args):
    import depts.surfacing.tools.generic.textureManager as textureManager
    importlib.reload(textureManager)

    win = textureManager.TextureManager()
    win.show()


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_selectShapes(*args):
    import maya.cmds as cmd

    selection = cmd.ls(selection=True)

    cmd.select(clear=True)

    for selected in selection:

        list_transform = cmd.listRelatives(selected, allDescendents=True, type='mesh', fullPath=True)
        list_transform = cmd.listRelatives(list_transform, parent=True, fullPath=True, shapes=True, noIntermediate=True)
        cmd.select(list_transform, add=True)


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_renameFileUI(*args):
    import common.fileRenamerUI as fileRenamerUI
    importlib.reload(fileRenamerUI)

    win = fileRenamerUI.FileRenamerUI()
    win.show()


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_renameUI(*args):
    import common.renameToolUI as renameToolUI
    importlib.reload(renameToolUI)

    win = renameToolUI.RenameToolUI()
    win.show()


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_uvTransfer(*args):
    import depts.surfacing.tools.generic.uvTransferUI as uvTransferUI
    importlib.reload(uvTransferUI)

    win = uvTransferUI.UvTransferUI()
    win.show()


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_exportObj(*args):
    import depts.surfacing.tools.generic.exportObjUI as exportObjUI
    importlib.reload(exportObjUI)

    win = exportObjUI.ExportObjUI()
    win.show()


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_smoothSelection(*args):
    import depts.surfacing.tools.generic.vraySmooth as vraySmooth
    importlib.reload(vraySmooth)

    vraySmooth.smoothSelection()


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_deleteNamespace(*args):
    import utiliz.namespaceUtil as nmUtil
    importlib.reload(nmUtil)

    nmUtil.delNameSpaceSelected()


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_materialIdUI(*args):
    import depts.surfacing.tools.generic.materialIdUI as materialIdUI
    importlib.reload(materialIdUI)

    win = materialIdUI.MaterialIdUI()
    win.show()


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_smartTools(*args):
    import depts.surfacing.tools.generic.smartTools.smartToolsUI as smartToolsUI
    importlib.reload(smartToolsUI)

    win = smartToolsUI.SmartToolsUI()
    win.show(dockable=True, area='left')


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_checkGCE(*args):
    import depts.surfacing.tools.generic.vrayGtrEnergyCompensation as vrayGtrEnergyCompensation
    importlib.reload(vrayGtrEnergyCompensation)

    vrayGtrEnergyCompensation.checkGtrEnergyCompensation(confirm=True)


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_turnOffGEC(*args):
    import depts.surfacing.tools.generic.vrayGtrEnergyCompensation as vrayGtrEnergyCompensation
    importlib.reload(vrayGtrEnergyCompensation)

    vrayGtrEnergyCompensation.turnOffGtrEnergyCompensation()


chkTool = {
    "name": "chkTool",
    "launch": run_chk,
    "icon": os.path.join(iconpath, "check.png"),
    "statustip": "Modeling check tool"
}

cloneUI = {
    "name": "Clone Asset Tool",
    "launch": run_cloneUI,
    "icon": os.path.join(iconpath, "clone.png"),
    "statustip": "Clone Asset Tool"
}

textureManager = {
    "name": "Texture Manager",
    "launch": run_textureManager,
    "icon": os.path.join(iconpath, "../surfacing/textureManager.png"),
    "statustip": "Texture Manager"
}

selectShapesTool = {
    'name': 'Select shapes',
    'launch': run_selectShapes,
    'icon': os.path.join(iconpath, "select.png"),
    'statustip': "Select shapes under selection"
}

renameFileTool = {
    "name": "File Renamer Tool",
    "launch": run_renameFileUI,
    "icon": os.path.join(iconpath, "rename.png"),
    "statustip": "File Renamer Tool"
}

renameTool = {
    "name": "Rename Tool",
    "launch": run_renameUI,
    "icon": os.path.join(iconpath, "rename.png"),
    "statustip": "Rename Tool"
}

uvTransferTool = {
    "name": "UV Transfer",
    "launch": run_uvTransfer,
    "icon": os.path.join(iconpath, 'switch.png'),
    "statustip": "UV Transfer"
}

exportObjTool = {
    "name": "Export OBJ",
    "launch": run_exportObj,
    "icon": os.path.join(iconpath, 'export.png'),
    "statustip": "Export OBJ"
}

smoothSelectionTool = {
    "name": "Vray Smooth Selection",
    "launch": run_smoothSelection,
    "icon": os.path.join(iconpath, 'displacement.png'),
    "statustip": "Take selection in a vray displacement node"
}

deleteNamespaceTool = {
    "name": "Delete Namespace",
    "launch": run_deleteNamespace,
    "icon": os.path.join(iconpath, 'remove.png'),
    "statustip": "Delete namespace from selected objects"
}

materialIdTool = {
    "name": "Material ID UI",
    "launch": run_materialIdUI,
    "icon": os.path.join(iconpath, '..', 'surfacing', 'MaterialId.png'),
    "statustip": "Open Material ID UI"
}

smartToolsUI = {
    "name": "Smart Tools",
    "launch": run_smartTools,
    "icon": os.path.join(iconpath, '..', 'surfacing', 'smart.png'),
    "statustip": "Open the Smart Tools window"
}

checkGtrEnergyCompensationTool = {
    "name": "Check GEC",
    "launch": run_checkGCE,
    "icon": os.path.join(iconpath, 'check.png'),
    "statustip": 'Check the GtrEnergyCompensation attribute on selected shaders'
}

turnOffGtrEnergyCompensationTool = {
    "name": "Turn Off GEC",
    "launch": run_turnOffGEC,
    "icon": os.path.join(iconpath, 'delete.png'),
    "statustip": 'Turn off the GtrEnergyCompensation attribute on selected shaders'
}

tools = [
    chkTool,
    cloneUI,
    textureManager,
    selectShapesTool,
    renameFileTool,
    renameTool,
    uvTransferTool,
    exportObjTool,
    smoothSelectionTool,
    deleteNamespaceTool,
    materialIdTool,
    smartToolsUI,
    checkGtrEnergyCompensationTool,
    turnOffGtrEnergyCompensationTool
]

PackageTool = {
    "name": "generic",
    "tools": tools,
    "icon_size": "32",
    "icon_only": False
}
