import importlib

import maya.cmds as cmd

import api.log
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.log)
# importlib.reload(rbwUI)


############################################################################
# @brief      check if the parameter is off.
#
# @return     the check value
#
def checkGtrEnergyCompensation(confirm=False):
    materials = []
    missingId = []
    for shadingEngine in cmd.ls(type='shadingEngine'):
        if cmd.sets(shadingEngine, q=True):
            for material in cmd.ls(cmd.listConnections(shadingEngine), materials=True):
                if 'gtrEnergyCompensation' in cmd.listAttr(material):
                    materials.append(material)

    if len(materials) == 0:
        if confirm:
            rbwUI.RBWDialog(text="Everything seems to be ok")
        return True

    for material in materials:
        if cmd.getAttr('{}.gtrEnergyCompensation'.format(material)):
            missingId.append(material)

    if len(missingId) > 0:
        cmd.select(clear=True)
        warningMessage = 'One or more materials has the attribute \'gtrEnergyCompensation\' set on ON:\n'
        for material in missingId:
            cmd.select(material, add=True)
            warningMessage = '{}- {}\n'.format(warningMessage, material)
        warningMessage = '{}Would you like to fix them?.'.format(warningMessage)

        if confirm:
            answer = rbwUI.RBWWarning(title='ATTENTION', text=warningMessage, resizable=True).result()
            if answer:
                turnOffGtrEnergyCompensation()
                return True
            else:
                return False
        else:
            return False
    else:
        if confirm:
            rbwUI.RBWDialog(text="Everything seems to be ok")
    return True


#####################################################################################
# @brief      turn off the gtrEnergyCompensation attribute for the selected shaders.
#
def turnOffGtrEnergyCompensation():
    materials = cmd.ls(sl=True)
    for material in materials:
        cmd.setAttr('{}.gtrEnergyCompensation'.format(material), False)
        api.log.logger().debug('gtrEnergyCompensation set Off on {}'.format(material))
