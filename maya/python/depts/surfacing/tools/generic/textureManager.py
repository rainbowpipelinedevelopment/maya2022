import os
import importlib
import shutil
import maya.cmds as cmd

from PySide2 import QtGui, QtWidgets, QtCore

import api.scene
import api.widgets.rbw_UI as rbwUI
import lookdev.lookdevTools as lookdevTools
import api.log

importlib.reload(api.scene)
# importlib.reload(rbwUI)
importlib.reload(lookdevTools)
importlib.reload(api.log)


class TextureManager(rbwUI.RBWWindow):

    def __init__(self, showDialog=True):
        super(TextureManager, self).__init__()

        if cmd.window("TextureManager", exists=True):
            cmd.deleteUI("TextureManager")
        self.setObjectName("TextureManager")

        cmd.select(clear=True)
        self.showDialog = showDialog
        self.initUI()
        self.updateMargins()

    def initUI(self):
        treeFrame = rbwUI.RBWFrame('V', bgColor='rgba(30, 30, 30, 150)', margins=[0, 0, 0, 0])
        self.textureTree = rbwUI.RBWTreeWidget(['Filename', 'Extension', 'Texture path'], scrollH=False, decorate=True)
        self.textureTree.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.textureTree.itemSelectionChanged.connect(self.checkAndSelect)
        treeFrame.addWidget(self.textureTree)

        self.referencesLabel = rbwUI.RBWLabel('')
        self.warningLabel = rbwUI.RBWLabel('')

        widgetFrame = rbwUI.RBWFrame('V')

        buttonBox = rbwUI.RBWFrame('H', bgColor='transparent', margins=[0, 0, 0, 0])
        self.refreshButton = rbwUI.RBWButton('Refresh list', size=[100, 25])
        self.refreshButton.clicked.connect(self.getAllTextures)
        self.toggleButton = rbwUI.RBWButton('Toggle ALL', size=[100, 25])
        self.toggleButton.clicked.connect(self.switchSelect)
        self.hyperButton = rbwUI.RBWButton('Select from Hypershade', size=[160, 25])
        self.hyperButton.clicked.connect(self.hypershadeSelection)
        self.nameCheck = rbwUI.RBWCheckBox('Keep original texture name')
        self.nameCheck.stateChanged.connect(self.switchName)
        buttonBox.addWidget(self.refreshButton)
        buttonBox.addWidget(self.toggleButton)
        buttonBox.addWidget(self.hyperButton)
        buttonBox.addStretch()
        buttonBox.addWidget(self.nameCheck)

        self.nameEdit = rbwUI.RBWLineEdit('New name:')
        self.nameEdit.textChanged.connect(self.setFolder)
        self.pathEdit = rbwUI.RBWLineEdit('Folder path:', toolButton='path')

        buttonsLay = rbwUI.RBWFrame('H', bgColor='transparent', margins=[0, 0, 0, 0])
        self.copyButton = rbwUI.RBWButton('Copy textures', icon=['copy.png'], size=[200, 30])
        self.copyButton.clicked.connect(self.copyTextures)
        self.repathButton = rbwUI.RBWButton('Repath textures', icon=['remap.png'], size=[200, 30])
        self.repathButton.clicked.connect(self.repathtextures)
        buttonsLay.addStretch()
        buttonsLay.addWidget(self.copyButton, alignment=QtCore.Qt.AlignHCenter)
        buttonsLay.addWidget(self.repathButton, alignment=QtCore.Qt.AlignHCenter)
        buttonsLay.addStretch()

        widgetFrame.addWidget(buttonBox)
        widgetFrame.addWidget(self.nameEdit)
        widgetFrame.addWidget(self.pathEdit)
        widgetFrame.addWidget(buttonsLay)

        self.mainLayout.addWidget(treeFrame)
        self.mainLayout.addWidget(self.referencesLabel, alignment=QtCore.Qt.AlignHCenter)
        self.mainLayout.addWidget(self.warningLabel, alignment=QtCore.Qt.AlignHCenter)
        self.mainLayout.addWidget(widgetFrame)

        self.getAllTextures()
        self.setName()

        self.setMain(True)
        self.setStyle()
        self.setMinimumSize(800, 300)
        self.setTitle('Texture Manager')
        self.setIcon('../surfacing/textureManager.png')
        self.setFocus()

    def getAllTextures(self):
        self.textureTree.clear()
        pathDict = {}
        errors = []
        references = []
        nodes = cmd.ls(type='file')
        for node in nodes:
            texturePath = cmd.getAttr('{}.fileTextureName'.format(node))
            absPath = os.path.dirname(texturePath)
            ext = texturePath.split('.')[-1]

            if ext not in ['png', 'exr', 'hdr']:
                # if node name mismatch fileTexture name, use file namespace to overwrite node name
                if not node.count(':') and os.path.basename(texturePath).split('.')[0] not in node:
                    renamedNode = cmd.rename(node, os.path.basename(texturePath).split('.')[0])
                    node = renamedNode

                if absPath not in pathDict:
                    pathDict[absPath] = []

                if [node, ext, texturePath] not in pathDict[absPath]:
                    pathDict[absPath].append([node, ext, texturePath])

        for absPath in pathDict:
            child = QtWidgets.QTreeWidgetItem(self.textureTree)
            child.setText(2, absPath)

            for item in pathDict[absPath]:
                child1 = QtWidgets.QTreeWidgetItem(child)
                child1.setTextAlignment(1, QtCore.Qt.AlignHCenter)
                child1.setText(0, item[0])
                child1.setText(1, item[1])
                child1.setText(2, item[2])

                if item[1].count(':'):
                    references.append(item[2])
                    child1.setText(1, item[1].split(':')[1])
                    child1.setForeground(0, QtGui.QColor('grey'))
                    child1.setForeground(1, QtGui.QColor('grey'))
                    child1.setForeground(2, QtGui.QColor('grey'))

                if not os.path.exists(item[2]):
                    errors.append(item[2])
                    child1.setForeground(2, QtGui.QColor('red'))

        if references:
            self.referencesLabel.setText("There are {} texture in reference".format(len(references)))
            self.referencesLabel.setStyleSheet('color: grey')
            self.referencesLabel.show()
        else:
            self.referencesLabel.hide()

        if errors:
            self.warningLabel.setText("{} textures path doesn't exists!".format(len(errors)))
            self.warningLabel.setStyleSheet('color: red')
            self.warningLabel.show()
        else:
            self.warningLabel.hide()

    def switchSelect(self):
        if self.textureTree.selectedItems():
            [x.setSelected(False) for x in self.textureTree.selectedItems()]
        else:
            root = self.textureTree.invisibleRootItem()
            [root.child(i).setSelected(True) for i in range(0, root.childCount())]

    def hypershadeSelection(self):
        sel = cmd.ls(sl=True)
        if sel:
            if self.textureTree.selectedItems():
                [x.setSelected(False) for x in self.textureTree.selectedItems()]

            root = self.textureTree.invisibleRootItem()
            parentItems = [root.child(i) for i in range(0, root.childCount())]

            [x.child(i).setSelected(True) for node in sel for x in parentItems for i in range(0, x.childCount()) if x.child(i).text(0) == node]

    def switchName(self, state):
        self.nameEdit.setEnabled(False if state else True)

    def setName(self):
        if api.scene.filename().count('_'):
            split = api.scene.filename().split('_')
            self.category = split[1]
            self.group = split[2]
            self.nameEdit.setText(split[3])

    def setFolder(self, name=''):
        if name:
            destPath = os.path.join(os.getenv('MC_FOLDER'), 'sourceimages').replace('\\', '/')
            if self.category:
                destPath = os.path.join(destPath, self.category, self.group, self.nameEdit.text(), 'df', 'tiff').replace('\\', '/')
        else:
            destPath = name

        self.pathEdit.setText(destPath)

    def checkAndSelect(self):
        if self.textureTree.selectedItems():
            for x in self.textureTree.selectedItems():
                if x.childCount():
                    for i in range(0, x.childCount()):
                        x.child(i).setSelected(True)

    def copyTextures(self):
        destPath = self.pathEdit.text()
        if destPath and os.path.exists(destPath):
            cantCopy = []
            missingNodes = []
            for x in self.textureTree.selectedItems():
                if not x.childCount():
                    if not x.text(0).count(':'):
                        oldNode = x.text(0)
                        ext = x.text(1)
                        source = x.text(2)
                        oldTextureName = os.path.basename(source).split('.')[0]

                        splitName = oldTextureName.split('_')
                        if not self.nameCheck.isChecked():
                            tokens = [os.getenv('PROJECT'), self.nameEdit.text()] + splitName[2:]
                            newTextureName = '_'.join(tokens)
                            newNode = oldNode.replace(oldTextureName, newTextureName)
                        else:
                            newTextureName = oldTextureName
                            newNode = oldNode

                        dest = os.path.join(destPath, '{}.{}'.format(os.path.basename(x.text(2)).split(ext)[0][:-1].replace(oldTextureName, newTextureName), ext)).replace('\\', '/')
                        couples = [(source, dest)]
                        if os.path.basename(source).count('.') == 2:
                            tok = os.path.basename(source).split('.')[1]
                            for file in os.listdir(os.path.dirname(source)):
                                if file.count(oldTextureName):
                                    num = file.split('.')[1]
                                    if num != tok:
                                        couples.append((source.replace(tok, num), dest.replace(tok, num)))

                        for x in couples:
                            if os.path.exists(x[0]):
                                if not os.path.exists(x[1]):
                                    shutil.copy(x[0], x[1])
                                else:
                                    api.log.logger().warning("This texture already exists: {}".format(newTextureName))
                            else:
                                cantCopy.append(x[0])
                                api.log.logger().error("I can't find this path: {}".format(x[0]))

                        if not self.repathtextures([oldNode, newNode, splitName, dest]):
                            missingNodes.append(oldNode)
                    else:
                        rbwUI.RBWWarning(text="I can't copy textures from referenced files!", defCancel=None, parent=self)

            msg = "Procedure completed"
            if cantCopy:
                msg += "\n\nBut i couldn't copy these ones:\n\n - {}".format('\n - '.join(cantCopy))
            if missingNodes:
                msg += "\n\nThese nodes doesn't exists:\n\n - {}".format('\n - '.join(missingNodes))
            if self.showDialog:
                rbwUI.RBWDialog(text=msg, parent=self)
        else:
            rbwUI.RBWWarning(text="This destination path is not valid\nplease try with another one!", defCancel=None, parent=self)

    def repathtextures(self, texture=None):
        destPath = self.pathEdit.text()
        if destPath and os.path.exists(destPath):
            if texture:
                if cmd.objExists(texture[0]):
                    cmd.rename(texture[0], texture[1])
                    cmd.setAttr('{}.fileTextureName'.format(texture[1]), texture[3], type='string')
                    lookdevTools.renameMaterialSubnode(texture[1], {'name': texture[2][2], 'variant': texture[2][3]}, '')
                    api.log.logger().debug("Texture renamed and remapped: {}".format(texture[0]))
                    return True
                else:
                    return False
            else:
                if not self.textureTree.selectedItems():
                    rbwUI.RBWWarning(text="Select at least 1 texture first!", defCancel=None, parent=self)
                    return

                for x in self.textureTree.selectedItems():
                    if not x.childCount():
                        if not x.text(0).count(':'):
                            node = x.text(0)
                            dest = os.path.join(destPath, os.path.basename(x.text(2)))

                            cmd.setAttr('{}.fileTextureName'.format(node), dest, type='string')
                            api.log.logger().debug("Texture repath done: {}".format(node))

                self.getAllTextures()
                rbwUI.RBWDialog(text="Texture repath done!", parent=self)
        else:
            rbwUI.RBWWarning(text="This destination path is not valid\nplease try with another one!", defCancel=None, parent=self)
