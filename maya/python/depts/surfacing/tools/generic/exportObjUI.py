import importlib
import os
from PySide2 import QtWidgets, QtCore
import functools
import os

import maya.cmds as cmd
import maya.mel as mel

import api.log
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.log)
# importlib.reload(rbwUI)


################################################################################
# @brief      This class describes an export object ui.
#
class ExportObjUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super().__init__()

        if cmd.window('ExportObjUI', exists=True):
            cmd.deleteUI('ExportObjUI')

        self.setObjectName('ExportObjUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topFrame = rbwUI.RBWFrame(layout='V')
        self.mainLayout.addWidget(self.topFrame)

        self.selectedObjectsGroup = rbwUI.RBWGroupBox(title='Selected Objects', fontSize=12, layout='G')

        self.selectedObjectsList = QtWidgets.QListWidget()

        self.addButton = rbwUI.RBWButton(text='Add', size=[125, 25], icon=['add.png'])
        self.addButton.clicked.connect(functools.partial(self.fillSelectionList, 1))

        self.removeButton = rbwUI.RBWButton(text='Remove', size=[125, 25], icon=['delete.png'])
        self.removeButton.clicked.connect(functools.partial(self.clearSelectionList, 1))

        self.fromSelectionButton = rbwUI.RBWButton(text='From Selection', size=[125, 25], icon=['select.png'])
        self.fromSelectionButton.clicked.connect(functools.partial(self.fillSelectionList))

        self.clearButton = rbwUI.RBWButton(text='Clear', size=[125, 25], icon=['clear.png'])
        self.clearButton.clicked.connect(functools.partial(self.clearSelectionList))

        self.selectedObjectsGroup.layout.addWidget(self.selectedObjectsList, 0, 0, 4, 2)
        self.selectedObjectsGroup.layout.addWidget(self.addButton, 0, 2)
        self.selectedObjectsGroup.layout.addWidget(self.removeButton, 1, 2)
        self.selectedObjectsGroup.layout.addWidget(self.fromSelectionButton, 2, 2)
        self.selectedObjectsGroup.layout.addWidget(self.clearButton, 3, 2)

        self.topFrame.addWidget(self.selectedObjectsGroup)

        self.exportOptionsGroup = rbwUI.RBWGroupBox(title='Export Options', fontSize=12, layout='G')

        self.exportInContentRadio = QtWidgets.QRadioButton('Content')
        self.exportInContentRadio.clicked.connect(functools.partial(self.setPath))

        self.exportInDesktopRadio = QtWidgets.QRadioButton('Desktop')
        self.exportInDesktopRadio.clicked.connect(functools.partial(self.setPath, 1))

        self.exportPathLine = rbwUI.RBWLineEdit(title='Export Path', stretch=False)
        self.exportFileNameLine = rbwUI.RBWLineEdit(title='File Name', stretch=False)

        self.exportOptionsGroup.layout.addWidget(self.exportInContentRadio, 0, 0)
        self.exportOptionsGroup.layout.addWidget(self.exportInDesktopRadio, 0, 1)
        self.exportOptionsGroup.layout.addWidget(self.exportPathLine, 1, 0, 1, 2)
        self.exportOptionsGroup.layout.addWidget(self.exportFileNameLine, 2, 0, 1, 2)

        self.topFrame.addWidget(self.exportOptionsGroup)

        self.exportObjButton = rbwUI.RBWButton(text='Export OBJ', icon=['export.png'], size=[150, 35])
        self.exportObjButton.clicked.connect(self.exportObj)
        self.topFrame.addWidget(self.exportObjButton, alignment=QtCore.Qt.AlignHCenter)

        self.fillSelectionList()

        self.setMinimumWidth(750)
        self.setFixedHeight(350)
        self.setTitle('Export OBJ')
        self.setIcon('export.png')
        self.setFocus()

    ############################################################################
    # @brief      fill Selection List
    #
    # @param      mode  The mode
    #
    def fillSelectionList(self, mode=0):
        selection = cmd.ls(selection=True, long=True)

        if mode == 0:
            self.selectedObjectsList.clear()

        self.selectedObjectsList.addItems(selection)

    ############################################################################
    # @brief      clear the Selection List.
    #
    # @param      mode  The mode
    #
    def clearSelectionList(self, mode=0):
        if mode == 0:
            self.selectedObjectsList.clear()
        else:
            selectedObjects = self.selectedObjectsList.selectedItems()
            for selectedObject in selectedObjects:
                self.selectedObjectsList.takeItem(self.selectedObjectsList.row(selectedObject))

    ############################################################################
    # @brief      Sets the path.
    #
    # @param      mode  The mode
    #
    def setPath(self, mode=0):
        if mode == 0:
            saveNode = cmd.ls('surfacing_saveNode')[0]
            assetId = cmd.getAttr('{}.asset_id'.format(saveNode))

            asset = api.asset.Asset(params={'id': assetId})
            assetFolder = asset.getAssetFolder()

            self.exportPathLine.setText(os.path.join(assetFolder.replace('/scenes/', '/sourceimages/'), 'mudbox', 'obj').replace('\\', '/'))
        else:
            self.exportPathLine.setText(os.path.join(os.environ['USERPROFILE'], 'Desktop').replace('\\', '/'))

    def exportObj(self):

        for i in range(self.selectedObjectsList.count()):
            cmd.polySmooth(
                self.selectedObjectsList.item(i).text(),
                divisions=2,
                method=0,
                subdivisionType=0,
                osdVertBoundary=1,
                osdFvarBoundary=3,
                osdFvarPropagateCorners=0,
                osdSmoothTriangles=0,
                osdCreaseMethod=0,
                boundaryRule=1,
                continuity=1,
                keepBorder=0,
                keepSelectionBorder=1,
                keepHardEdge=0,
                keepTessellation=1,
                keepMapBorders=0,
                smoothUVs=1,
                propagateEdgeHardness=0,
                divisionsPerEdge=1,
                pushStrength=0.1,
                roundness=1,
                constructionHistory=1
            )

        exportPath = self.exportPathLine.text()
        fileName = self.exportFileNameLine.text()

        objFilePath = '{}.obj'.format(os.path.join(exportPath, fileName))
        mtlFilePath = objFilePath.replace('.obj', '.mtl')

        cmd.file(objFilePath, typ="OBJexport", es=1, op='groups=0; ptgroups=0; materials=0; smoothing=1; normals=1')
        os.remove(mtlFilePath)

        for i in range(0, self.selectedObjectsList.count()):
            cmd.setAttr('polySmoothFace{}.divisions'.format(str(i + 1)), 0)

        mel.eval("DeleteHistory;")





'''def surfacing_exportOBJ():
    selection = cmd.ls(sl=1)
    selNum = len(selelction)

    if selNum == 0:
        rbwUI.RBWError(text='You must select something')
        return
    else:
        for object in selection:
            cmd.polySmooth(
                object,
                divisions=2,
                method=0,
                subdivisionType=0,
                osdVertBoundary=1,
                osdFvarBoundary=3,
                osdFvarPropagateCorners=0,
                osdSmoothTriangles=0,
                osdCreaseMethod=0,
                boundaryRule=1,
                continuity=1,
                keepBorder=0,
                keepSelectionBorder=1,
                keepHardEdge=0,
                keepTessellation=1,
                keepMapBorders=0,
                smoothUVs=1,
                propagateEdgeHardness=0,
                divisionsPerEdge=1,
                pushStrength=0.1,
                roundness=1,
                constructionHistory=1
            )

    #---------------Finestra------------------

        result = cmd.promptDialog(
                        title='Name Obj',
                        message='Enter Name:',
                        button=['Content', 'Desktop'],
                        dismissString='Cancel')

        Objname =  cmd.promptDialog(query=True, text=True)

        #---------------Path------------------
        if result == 'Desktop':

            desktop = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop')
            Objfile =  "{}/{}.obj".format(desktop.replace("\\", "/"), Objname)

        elif result == 'Content':        
            data = cmd.file(query=1, location=1)
            pathElement = data.split("/")
            newPathOBJContent = "{}/sourceimages/{}/Mudbox/Obj/".format("/".join(pathElement[:7]), "/".join(pathElement[8:12]))

            cmd.sysFile(newPathOBJContent, makeDir=True )
            Objfile = "{}{}.obj".format(newPathOBJContent, Objname)

        else:
            return

        Mtldel = Objfile.replace(".obj", ".mtl")

    #---------------Export------------------

        cmd.file(Objfile ,typ="OBJexport",es=1,op='groups=0; ptgroups=0; materials=0; smoothing=1; normals=1')
        os.remove(Mtldel)

    #---------------NO SMOOTH------------------

        for i in range(0,selNum):
            cmd.setAttr('polySmoothFace' + str(i+1) + '.divisions', 0)

        mel.eval("DeleteHistory;")

        print "Path del file {}.obj: {}".format(Objname, Objfile.replace("/{}.obj".format(Objname), "").replace("/", "\\"))
        cmd.confirmDialog(title='Export OBJ', message='Done!', button=['OK'])'''