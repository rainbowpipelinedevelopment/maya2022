import importlib

import maya.cmds as cmd
import maya.mel as mel

import api.vray
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.vray)
# importlib.reload(rbwUI)


def smoothSelection():
    selection = cmd.ls(selection=True)

    cmd.select(clear=True)

    for selected in selection:
        list_transform = cmd.listRelatives(selected, allDescendents=True, type='mesh', fullPath=True)
        list_transform = cmd.listRelatives(list_transform, parent=True, fullPath=True, shapes=True, noIntermediate=True)
        cmd.select(list_transform, add=True)

    meshes = cmd.ls(selection=True, long=True)

    smoothMeshes = []
    noSmoothMeshes = []

    for mesh in meshes:
        if 'nosmooth' not in mesh:
            smoothMeshes.append(mesh)
        else:
            noSmoothMeshes.append(mesh)

    if len(smoothMeshes) > 0:
        selection = smoothMeshes
    else:
        rbwUI.RBWError(text='no mesh to make \'smooth\' in the selction')
        return

    api.vray.createVrayDislpacement(selection)
