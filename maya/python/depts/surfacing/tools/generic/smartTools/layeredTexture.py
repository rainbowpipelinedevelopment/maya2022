import maya.cmds as cmd

from depts.surfacing.tools.smart_tools import remapHsv

import API.log
reload(API.log)


def main():
    sel = cmd.ls(sl=1)
    if sel:
        for i in range(0, len(sel)):
            nomeLayer = '{}_layeredTexture'.format(sel[i])
            cmd.shadingNode('layeredTexture', asUtility=True, n=nomeLayer)

            remapHsv.connectNodes(sel[i], nomeLayer, '.inputs[0].color')
            API.log.logger().debug('LayeredTexture done for {}'.format(sel[i]))
    else:
        cmd.confirmDialog(title='Warning', message='Please select a node first!', button=['OK'])
