import importlib

import os
import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI

# importlib.reload(rbwUI)


################################################################################
# @brief      This class describes a smart tools ui.
#
class SmartToolsUI(rbwUI.RBWDockableWindow):
    def __init__(self):
        super().__init__()

        if cmd.window('SmartToolsUI', exists=True):
            cmd.deleteUI('SmartToolsUI')

        if cmd.window('SmartToolsUIWorkspaceControl', exists=True):
            cmd.deleteUI('SmartToolsUIWorkspaceControl')

        self.setObjectName('SmartToolsUI')
        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setStyle()

        self.topFrame = rbwUI.RBWFrame(layout='V', margins=[1, 1, 1, 1], spacing=1, bgColor='rgba(20, 20, 20, 150)')
        self.mainLayout.addWidget(self.topFrame)

        buttons = {
            'Create shader': ['../surfacing/createshader.png', 'Import the textures you need, select them, than click the button to create the shader', run_createShader],
            'Fake fresnel': ['../surfacing/fakefresnel.png', 'Select a node, than click the button to create the "FakeFresnel" node structure', run_fakeFresnel],
            'Layered texture': ['../surfacing/layeredtexture.png', 'Select a node, than click the button to create a layeredTexture', run_layeredTexture],
            'Remap HSV': ['../surfacing/remaphsv.png', 'Select a node, than click the button to create a remapHsv', run_remapHsv],
            'Smart bake': ['../surfacing/bake.png', 'Select a mesh, than click the button to start the bake', run_smartBake]
        }

        for item in buttons:
            button = rbwUI.RBWButton(text=item, icon=[buttons[item][0]], size=[150, 35], tooltip=buttons[item][1])
            button.clicked.connect(buttons[item][2])
            self.topFrame.addWidget(button)

        self.setFixedSize(180, 200)
        self.setWindowTitle('Smart tools')
        self.setFocus()


def run_createShader():
    from depts.surfacing.tools.smart_tools import createShader
    importlib.reload(createShader)
    createShader.main()


def run_fakeFresnel():
    from depts.surfacing.tools.smart_tools import fakeFresnel
    importlib.reload(fakeFresnel)
    fakeFresnel.main()


def run_layeredTexture():
    from depts.surfacing.tools.smart_tools import layeredTexture
    importlib.reload(layeredTexture)
    layeredTexture.main()


def run_remapHsv():
    from depts.surfacing.tools.smart_tools import remapHsv
    importlib.reload(remapHsv)
    remapHsv.main()


def run_smartBake():
    from depts.surfacing.tools.smart_tools import smartBake
    importlib.reload(smartBake)
    smartBake.main()
