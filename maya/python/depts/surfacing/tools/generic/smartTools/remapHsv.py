import maya.cmds as cmd

import API.log
reload(API.log)


def main():
    sel = cmd.ls(sl=1)
    if sel:
        for i in range(0, len(sel)):
            hsv = '{}_Hsv'.format(sel[i])
            cmd.shadingNode('remapHsv', asUtility=True, n=hsv)

            connectNodes(sel[i], hsv, '.color')
            API.log.logger().debug('RemapHSV done for {}'.format(sel[i]))
    else:
        cmd.confirmDialog(title='Warning', message='Please select a node first!', button=['OK'])


def connectNodes(sel, node, defExt):
    def getOutCode(conn):
        if cmd.connectionInfo(conn, isDestination=True):
            outCode = cmd.connectionInfo(conn, sourceFromDestination=True)
            return outCode

    def getConnName(conn):
        node = conn.split('.')[0]
        connName = '.{}'.format(conn.split(node)[1].split('.')[-1])
        return connName

    conn = [s for s in cmd.listConnections(sel, s=False, p=True) if not s.count('defaultTextureList') and not s.count('hyperShadePrimaryNodeEditorSavedTabsInfo')]
    if conn:
        inShader = conn[0]
        outTexture = getOutCode(inShader)
        outExt = getConnName(outTexture)

        inNode = '{}{}'.format(node, defExt)
        outNode = '{}{}'.format(node, outExt)

        cmd.disconnectAttr(outTexture, inShader)
        cmd.connectAttr(outTexture.replace(outExt, '.outColor'), inNode)
        cmd.connectAttr(outNode, inShader)
    else:
        inShader = None
        cmd.confirmDialog(title='Warning', message="I couldn't find any connected node!\nI created {} node but you'll need to connect it manually".format(node), button=['OK'])
