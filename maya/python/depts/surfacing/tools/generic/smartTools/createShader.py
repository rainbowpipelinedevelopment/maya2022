import maya.cmds as cmd
import maya.mel as mel


def main():
    sel = cmd.ls(sl=1)
    if sel:
        data = sel[0].split("_")
        place = '{}_{}_{}_{}_place2dTexture'.format(data[0], data[1], data[2], data[3])
        mtl = '{}_{}_{}_{}_shader'.format(data[0], data[1], data[2], data[3])
        cmd.shadingNode('place2dTexture', asUtility=True, n=place)
        cmd.shadingNode('VRayMtl', asShader=True, n=mtl)

        for i in range(0, len(sel)):
            cerca = cmd.listConnections(sel[i], d=True, s=True, p=True)
            data = sel[i].split("_")
            tex = '{}_{}_{}_{}_{}'.format(data[0], data[1], data[2], data[3], data[4])
            cmd.rename(sel[i], tex)

            sub = 'place2d'
            placeTex = next((s for s in cerca if sub in s), None)

            dataTex = placeTex.split(".")
            cmd.delete(dataTex[0])

            attrs = [
                'outUV',
                'outUvFilterSize',
                'coverage',
                'translateFrame',
                'rotateFrame',
                'mirrorU',
                'mirrorV',
                'stagger',
                'wrapU',
                'wrapV',
                'repeatUV',
                'vertexUvOne',
                'vertexUvTwo',
                'vertexUvThree',
                'vertexCameraOne',
                'noiseUV',
                'offset',
                'rotateUV'
            ]

            for i in attrs:
                if i == 'outUV':
                    cmd.connectAttr('{}.{}'.format(place, i), '{}.uvCoord'.format(tex))
                elif i == 'outUvFilterSize':
                    cmd.connectAttr('{}.{}'.format(place, i), '{}.uvFilterSize'.format(tex))
                else:
                    cmd.connectAttr('{}.{}'.format(place, i), '{}.{}'.format(tex, i))

            if data[4] == "C":
                cmd.connectAttr('{}.outColor'.format(tex), '{}.diffuseColor'.format(mtl))
            if data[4] == "RA":
                cmd.connectAttr('{}.outColorR'.format(tex), '{}.reflectionColorAmount'.format(mtl))
            if data[4] == "B":
                cmd.connectAttr('{}.outColor'.format(tex), '{}.bumpMap'.format(mtl))
            if data[4] == "N":
                cmd.connectAttr('{}.outColor'.format(tex), '{}.bumpMap'.format(mtl))
                cmd.setAttr('{}.bumpMapType'.format(mtl), 1)
            if data[4] == "RG":
                cmd.connectAttr('{}.outColorR'.format(tex), '{}.reflectionGlossiness'.format(mtl))
            if data[4] == "RC":
                cmd.connectAttr('{}.outColor'.format(tex), '{}.reflectionColor'.format(mtl))
            if data[4] == "RFA":
                cmd.connectAttr('{}.outColorR'.format(tex), '{}.refractionColorAmount'.format(mtl))
            if data[4] == "MLN":
                cmd.connectAttr('{}.outColorR'.format(tex), '{}.metalness'.format(mtl))

        mel.eval('hyperShadePanelGraphCommand("hyperShadePanel1", "showUpAndDownstream");')
    else:
        cmd.confirmDialog(title='Warning', message='Please select all textures first!', button=['OK'])
