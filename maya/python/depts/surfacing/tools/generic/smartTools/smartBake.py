import maya.cmds as cmd
import maya.mel as mel


def main():
    sel = cmd.ls(sl=1)
    if sel:
        mel.eval('vray vfbControl -bkgr false;')
        cmd.vray('objectProperties', 'add_single', 'VrayBakeOptions')
        cmd.createNode('VRayBakeOptions', n='vrayBakeOptions')
        cmd.sets(sel[0], add='vrayBakeOptions')
        cmd.setAttr('vrayBakeOptions.resolutionX', 4096)
        cmd.setAttr('vraySettings.relements_enableall', 0)
        cmd.select(sel, r=True)
        cmd.setAttr('vraySettings.imageFormatStr', 'tif', type='string')

        mel.eval('vrayShowBakeOptionsWindow')
        mel.eval('vrayStartBake')

        cmd.delete('vrayBakeOptions')
        cmd.delete('vrayDefaultBakeOptions')
        cmd.setAttr('vraySettings.relements_enableall', 1)
        cmd.setAttr('vraySettings.imageFormatStr', 'exr (multichannel)', type='string')
        mel.eval('vray vfbControl -bkgr true;')
    else:
        cmd.confirmDialog(title='Warning', message='Please select a mesh first!', button=['OK'])
