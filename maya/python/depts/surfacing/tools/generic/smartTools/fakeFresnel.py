import maya.cmds as cmd
import maya.mel as mel


def main():
    sel = cmd.ls(sl=1)
    if sel:
        data = sel[0].split("_")
        namespace = '{}_{}_{}_{}_{}'.format(data[0], data[1], data[2], data[3], data[4])
        sampler = '{}_samplerinfo'.format(namespace)
        ramp = '{}_ramp'.format(namespace)
        remap = '{}_Hsv'.format(namespace)
        shader = '{}_{}_{}_Shader'.format(data[0], data[1], data[2])

        cmd.shadingNode('samplerInfo', asUtility=True, n=sampler)
        cmd.shadingNode('ramp', asShader=True, n=ramp)
        cmd.shadingNode('remapHsv', asShader=True, n=remap)

        cmd.connectAttr('{}.facingRatio'.format(sampler), '{}.vCoord'.format(ramp))
        cmd.connectAttr('{}.outColor'.format(sel[0]), '{}.colorEntryList[1].color'.format(ramp))
        cmd.setAttr('{}.colorEntryList[1].position'.format(ramp), 1)
        cmd.connectAttr('{}.outColor'.format(sel[0]), '{}.color'.format(remap))
        cmd.connectAttr('{}.outColor'.format(remap), '{}.colorEntryList[0].color'.format(ramp))
        cmd.connectAttr('{}.outColor'.format(ramp), '{}.diffuseColor'.format(shader))

        mel.eval('hyperShadePanelGraphCommand("hyperShadePanel1", "showUpAndDownstream");')
    else:
        cmd.confirmDialog(title='Warning', message='Please select a node first!', button=['OK'])
