import importlib
from PySide2 import QtCore, QtWidgets

import maya.cmds as cmd

import api.log
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.log)
# importlib.reload(rbwUI)


################################################################################
# @brief      This class describes an uv transfer ui.
#
class UvTransferUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(UvTransferUI, self).__init__()

        if cmd.window('UvTransferUI', exists=True):
            cmd.deleteUI('UvTransferUI')

        self.setObjectName('UvTransferUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topFrame = rbwUI.RBWFrame(layout='V')
        self.mainLayout.addWidget(self.topFrame)

        self.sampleSpaceGroup = rbwUI.RBWGroupBox(title='Sample Space', fontSize=12, layout='V')

        self.radioButtonWorld = QtWidgets.QRadioButton("World")
        self.radioButtonLocal = QtWidgets.QRadioButton("Local")
        self.radioButtonUV = QtWidgets.QRadioButton("UV")
        self.radioButtonComponent = QtWidgets.QRadioButton("Component")
        self.radioButtonTopology = QtWidgets.QRadioButton("Topology")

        self.sampleSpaceGroup.addWidget(self.radioButtonWorld)
        self.sampleSpaceGroup.addWidget(self.radioButtonLocal)
        self.sampleSpaceGroup.addWidget(self.radioButtonUV)
        self.sampleSpaceGroup.addWidget(self.radioButtonComponent)
        self.sampleSpaceGroup.addWidget(self.radioButtonTopology)

        self.topFrame.addWidget(self.sampleSpaceGroup)

        self.deleteHistoryCheckBox = rbwUI.RBWCheckBox(text='Delete History')
        self.topFrame.addWidget(self.deleteHistoryCheckBox)

        self.transferButton = rbwUI.RBWButton(text='Transfer UV', size=[150, 35], icon=['switch.png'])
        self.transferButton.clicked.connect(self.transferAttr)
        self.topFrame.addWidget(self.transferButton, alignment=QtCore.Qt.AlignHCenter)

        self.setMinimumWidth(310)
        self.setFixedHeight(250)
        self.setTitle('UV Transfer')
        self.setIcon('switch.png')
        self.setFocus()

    ############################################################################
    # @brief      Gets the sample space.
    #
    # @return     The sample space.
    #
    def getSampleSpace(self):
        radioButton = {self.radioButtonWorld: 0, self.radioButtonLocal: 1, self.radioButtonUV: 3, self.radioButtonComponent: 4, self.radioButtonTopology: 5}
        buttonActive = None

        for button in radioButton.keys():
            if button.isChecked():
                buttonActive = button
        return radioButton[buttonActive]

    ############################################################################
    # @brief      transfer attribute.
    #
    def transferAttr(self):
        selection = cmd.ls(sl=1)
        selNum = len(selection) - 1
        deleteHistory = self.deleteHistoryCheckBox.isChecked()
        sampleSpace = self.getSampleSpace()

        for n in range(0, selNum):
            cmd.transferAttributes(
                selection[selNum],
                selection[n],
                transferPositions=0,
                transferNormals=0,
                transferUVs=2,
                transferColors=2,
                sampleSpace=sampleSpace,
                sourceUvSpace="map1",
                targetUvSpace="map1",
                searchMethod=3,
                flipUVs=0,
                colorBorders=1
            )

        if deleteHistory:
            cmd.DeleteHistory()
