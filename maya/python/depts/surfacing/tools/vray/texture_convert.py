import importlib
import os
import subprocess
import threading

import maya.cmds as cmd

import api.log
import api.scene
import api.vray
import api.texture
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.log)
importlib.reload(api.scene)
importlib.reload(api.vray)
importlib.reload(api.texture)
# importlib.reload(rbwUI)

extensions = ['.tif', '.tiff', '.png']

################################################################################
# @brief      Gets all textures.
#
# @return     All textures.
#
def getAllTextures():

    textureDictionary = {}

    textureNodes = api.texture.listTextureNodes()

    # Scan all the texture nodes to build a dictionary of node and images
    # assigned to the nodes
    for node in textureNodes:
        api.log.logger().debug('Texture name: {}'.format(node))
        imagePath = api.texture.getImageFromTextureNode(node)
        basename = os.path.basename(imagePath)
        for ext in extensions:
            if basename.endswith(ext):
                dirname = os.path.dirname(imagePath)
                filenames = [basename]

                # If the current texture stored in the dictionary is an UDIM texture
                if api.texture.isUDIM(basename, ext):
                    prefix = basename.split('.')[0]
                    # Loop through all the files in the same texture folder
                    for file in api.system.list_file(dirname):
                        if prefix in file:
                            if file not in filenames:
                                filenames.append(file)

                if dirname not in textureDictionary.keys():
                    textureDictionary[dirname] = []

                textureDictionary[dirname].append({'node': node, 'filenames': filenames})

    api.log.logger().debug(textureDictionary)
    return textureDictionary


################################################################################
# @brief      Executes the conversion from the sourceImage into an exr specified
#             in the destImage
#
# @param      sourceImage  The source image
# @param      destImage    The destination image
#
# @return     the converter thread.
def toExr(sourceImage=None, destImage=None):
    if sourceImage is None or not isinstance(sourceImage, str):
        raise TypeError('The source image is not a valid data')

    converter = False
    while(not converter):
        converter = api.vray.vrayExrConverterPath()

    converterOptions = ' '.join([
        '"{}"'.format(converter),
        '-half',
        '-compression zip',
        '-linear off',
        '"{}"'.format(sourceImage),
        '"{}"'.format(destImage)
    ])

    api.log.logger().debug(converterOptions)

    def worker(converterOptions):
        subprocess.call(converterOptions, shell=False)

    t = threading.Thread(target=worker, args=(converterOptions, ))
    t.start()

    return t


################################################################################
# @brief      Promotes the tiff to exr.
#
# @return     new exr filename.
#
def promoteToExr(filename=None, sceneName=None):
    if not sceneName:
        sceneName = api.scene.scenename()
    destDir = os.path.join(os.path.dirname(sceneName), 'textures').replace('\\', '/')

    if not os.path.exists(destDir):
        os.makedirs(destDir)

    destFilename = '{}/{}_tiled.exr'.format(destDir, os.path.splitext(os.path.basename(filename))[0])
    destFilename = os.path.normpath(destFilename).replace('\\', '/')

    thread = toExr(filename, destFilename)
    return destFilename, thread


################################################################################
# @brief      Check the system for a TIF that has the same name of the EXR and
#             return the new path
#
# @param      sourceImage  The source image
# @param      destImage    The destination image
#
# @return     the new image path.
#
def exrToTiff(sourceImage=None, destImage=None):
    if sourceImage is None or not isinstance(sourceImage, basestring):
        raise TypeError('The source image is not a valid data')

    filename = os.path.basename(sourceImage)
    dirname = os.path.dirname(sourceImage)

    destDirname = os.path.join(dirname, 'Tiff')
    filename = filename.replace('_tiled.exr', '.tif')

    if os.path.exists(os.path.join(destDirname, filename)):
        return os.path.join(destDirname, filename)
    elif os.path.exists(os.path.join(destDirname, filename.replace('.tif', '.tiff'))):
        return os.path.exists(os.path.join(destDirname, filename.replace('.tif', '.tiff')))

    return False


################################################################################
# @brief      Switch all the EXR in the scene back to TIF.
#
def revertTextures():
    for textureNode in api.texture.listTextureNodes():
        imageTexturePath = api.texture.getImageFromTextureNode(textureNode)
        # print imageTexturePath
        if api.texture.isExr(imageTexturePath):
            tifTexturePath = exrToTiff(imageTexturePath)
            api.texture.remapTextureNode(textureNode, tifTexturePath)
            api.texture.setFilterTypeQuadratic(textureNode)

            # If the map is recognized as vectorial
            if api.texture.isVectorialMap(tifTexturePath):
                api.vray.removeVrayTextureinputGamma(textureNode)

    rbwUI.RBWConfirm(title='Conversion EXR to TIF', text='Conversion complete!\nNow all the EXRs are sitched back to the TIF version.', defCancel=None)


################################################################################
# @brief      Finalize all the textures in the scene.
#
# @details    Converting all of them into EXR and adding an InputGamma attribute
#             that tells VRAY that the texture is SRGB and changing the
#             FilterType to MipMap to better use the converted piramidal exrs.
#
# @param      sceneName  The scene name
#
def finalizeTextures(sceneName=None):
    textureInScene = getAllTextures()

    for directory in textureInScene.keys():
        ranges = list(divideChunks(range(len(textureInScene[directory])), 40))
        for indexRange in ranges:
            for i in indexRange:
                rangeThreads = []
                textureDict = textureInScene[directory][int(i)]
                nodeName = textureDict['node']
                innerRanges = list(divideChunks(range(len(textureDict['filenames'])), 40))
                for innerRange in innerRanges:
                    for j in innerRange:
                        rangeThreads = []
                        fileName = textureDict['filenames'][int(j)]
                        currentImagePath = os.path.join(directory, fileName)
                        currentImagePath = os.path.normpath(currentImagePath).replace('\\', '/')

                        exrTexturePath, thread = promoteToExr(filename=currentImagePath, sceneName=sceneName)
                        rangeThreads.append(thread)

                        if api.texture.isVectorialMap(currentImagePath):
                            if not api.texture.isNormalMap(currentImagePath):
                                api.vray.addTextureInputGamma(nodeName)
                                api.vray.setTextureInputGammaSRGB(nodeName)

                        api.texture.setFilterTypeMipmap(nodeName)
                    api.texture.remapTextureNode(nodeName, exrTexturePath)

                for thread in rangeThreads:
                    thread.join()


def divideChunks(inputList, n):
    for i in range(0, len(inputList), n):
        yield inputList[i:i + n]


if __name__ == '__main__':
    pass
