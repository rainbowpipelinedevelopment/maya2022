import importlib

import maya.cmds as cmd
import maya.mel as mel
import random as random

import api.widgets.rbw_UI as rbwUI
# importlib.reload(rbwUI)


################################################################################
# @brief      set a random vray object id for selected objects.
#
def randomObjectId():
    selection = cmd.ls(selection=True)

    # MIN E MAX VALUES FOR RANDOM
    min = 1
    max = 10

    for selected in selection:

        list_transform = cmd.listRelatives(selected, allDescendents=True, type='mesh', fullPath=True)
        list_transform = cmd.listRelatives(list_transform, parent=True, fullPath=True)
        list_transform = cmd.listRelatives(list_transform, children=True, fullPath=True)

        if not isinstance(list_transform, list):
            continue

        for t in list_transform:
            if 'Deformed' in t:
                mel.eval('vray addAttributesFromGroup {} vray_objectID 1;'.format(t))
                object_id = random.randint(min, max)

                cmd.setAttr('{}.vrayObjectID'.format(t), object_id)


################################################################################
# @brief      check if all material in scene has a vray material Id.
#
# @return     check result.
#
def checkMaterialId(confirm=False):
    excludeMaterials = ['lambert', 'VRayBlendMtl', 'VRaySwitchMtl', 'VRayMtl2Sided', 'VRayEnvironmentFog']
    materials = []
    missingId = []
    for shading_engine in cmd.ls(type='shadingEngine'):
        if cmd.sets(shading_engine, q=True):
            for material in cmd.ls(cmd.listConnections(shading_engine), materials=True):
                materialType = cmd.nodeType(material)
                if materialType not in excludeMaterials:
                    materials.append(material)

    if len(materials) == 0:
        if confirm:
            rbwUI.RBWDialog(text="Everything seems to be ok")
        return True

    for material in materials:
        attrs = cmd.listAttr(material)
        if 'vrayMaterialId' not in attrs:
            missingId.append(material)

    if len(missingId) > 0:
        cmd.select(clear=True)
        errorMessage = 'One or more materials has not a vray material Id:\n'
        for material in missingId:
            cmd.select(material, add=True)
            errorMessage = '{}- {}\n'.format(errorMessage, material)
        errorMessage = '{}Please control and try again.'.format(errorMessage)

        rbwUI.RBWError(title='ATTENTION', text=errorMessage)

        return False
    else:
        if confirm:
            rbwUI.RBWDialog(text="Everything seems to be ok")
        return True
