import importlib
import os

iconpath = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'common')


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_exportFxData(*args):
    import depts.surfacing.tools.export.exportFxData as exportFxData
    importlib.reload(exportFxData)

    exportFxData.exportAlembicForFx()


exportFxDataTool = {
    "name": "Export Fx Data",
    "launch": run_exportFxData,
    "icon": os.path.join(iconpath, 'export.png'),
    "statustip": "Export Fx Data"
}

tools = [
    exportFxDataTool
]

PackageTool = {
    "name": "export",
    "tools": tools,
    "icon_size": "32",
    "icon_only": False
}
