import importlib
import os

import maya.cmds as cmd

import api.asset
import api.log
import api.json

# importlib.reload(api.asset)
importlib.reload(api.log)
importlib.reload(api.json)


def exportAlembicForFx():
    if cmd.objExists('MSH'):
        saveNode = 'surfacing_saveNode'
        assetParams = {
            'category': cmd.getAttr('{}.genre'.format(saveNode)),
            'group': cmd.getAttr('{}.group'.format(saveNode)),
            'name': cmd.getAttr('{}.name'.format(saveNode)),
            'variant': cmd.getAttr('{}.variant'.format(saveNode))
        }

        currentAsset = api.asset.Asset(params=assetParams)

        exportFolder = os.path.join(currentAsset.getAssetFolder(), 'export').replace('\\', '/')
        api.log.logger().debug('exportFolder: {}'.format(exportFolder))

        potentialMeshes = cmd.sets('MSH', query=True)

        # controllo che non ci siano le extra geo
        geoToHide = []
        if cmd.objExists('dynamic_geos'):
            for geo in cmd.listRelatives('dynamic_geos'):
                if not geo.endswith('ShapeDeformed'):
                    parent = cmd.listRelatives(geo, parent=True)[0]
                    geoToHide.append(parent)

        # controllo il tipo di scalpo
        groomingExportFolder = os.path.join(exportFolder, 'grooming', 'scalps').replace('\\', '/')
        if os.path.exists(groomingExportFolder):
            versions = [dir for dir in os.listdir(groomingExportFolder) if dir.startswith('vr')]
            if versions:
                versions.sort()
                lastVersion = versions[-1]
                scalpsJson = os.path.join(groomingExportFolder, lastVersion, 'scalps2Descs.json').replace('\\', '/')
                scalpsData = api.json.json_read(scalpsJson)
                for scalp in scalpsData:
                    for desc in scalpsData[scalp]:
                        meshName = scalp.split('|')[-1][:-2]
                        if '_ING_' in desc:
                            geoToHide.append(meshName)
                            break
                        if '_CLG_' in desc:
                            geoToHide.append(meshName)
                            break
        if geoToHide:
            meshes = [mesh for mesh in potentialMeshes if mesh not in geoToHide]
        else:
            meshes = potentialMeshes

        roots_args = ["-root {}".format(mesh) for mesh in meshes]
        roots_args = " ".join(roots_args)

        startFrame = 1
        endFrame = 1

        surfExportFolder = os.path.join(exportFolder, 'surfacing', 'fxData').replace('\\', '/')
        api.log.logger().debug('surfExportFolder: {}'.format(surfExportFolder))

        if not os.path.exists(surfExportFolder):
            os.makedirs(surfExportFolder)
            lastVersion = 0
        else:
            versions = [dir for dir in os.listdir(surfExportFolder) if dir.startswith('vr')]
            versions.sort()

            lastVersion = int(versions[-1].split('vr')[1])
        versionString = 'vr{}'.format(str(lastVersion + 1).zfill(3))
        exportVersionFolder = os.path.join(surfExportFolder, versionString).replace('\\', '/')
        os.makedirs(exportVersionFolder)

        # alembic
        alembicFile = os.path.join(exportVersionFolder, '{}_{}_{}_{}_{}.abc'.format(currentAsset.category, currentAsset.group, currentAsset.name, currentAsset.variant, versionString)).replace('\\', '/')
        api.log.logger().debug('alembicFile: {}'.format(alembicFile))

        alembicCommand = '{} -frameRange {} {} -uvWrite -writeColorSets -writeFaceSets -worldSpace -wholeFrameGeo -writeVisibility True -dataFormat ogawa -file {}'.format(roots_args, startFrame, endFrame, alembicFile)

        cmd.AbcExport(j=alembicCommand, verbose=True)

        # shaders .json
        shaderJsonFile = alembicFile.replace('.abc', '_shaders.json')
        api.log.logger().debug('shaderJsonFile: {}'.format(shaderJsonFile))
        shaderDict = {}
        shaders = []

        for msh in meshes:
            shadeEng = cmd.listConnections('{}_Shape'.format(msh), type='shadingEngine')[0]
            material = cmd.ls(cmd.listConnections(shadeEng), materials=True)[0]

            shaderDict['{}_Shape'.format(msh)] = material
            if material not in shaders:
                shaders.append(material)

        api.json.json_write(shaderDict, shaderJsonFile)

        # shaders .json
        shaderMbFile = shaderJsonFile.replace('.json', '.mb')
        api.log.logger().debug('shaderMbFile: {}'.format(shaderMbFile))
        cmd.select(shaders, replace=True)
        cmd.file(shaderMbFile, force=True, typ="mayaBinary", exportSelected=True, preserveReferences=True, shader=True, expressions=True, constructionHistory=True)

    else:
        cmd.confirmDialog(title='ATTENTION', icon='critical', message='The MSH set does not exists.\nCreate and try again.', button=['OK'])
        return
