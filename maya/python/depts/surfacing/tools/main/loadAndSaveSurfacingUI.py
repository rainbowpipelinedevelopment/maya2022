import importlib
from PySide2 import QtWidgets
import re

import maya.cmds as cmd

import common.loadAndSaveAssetUI
import api.savedAsset
import api.scene
import api.widgets.rbw_UI as rbwUI
import depts.surfacing.tools.vray.vrayObjectId as vrayObjectId
import depts.surfacing.tools.generic.vrayGtrEnergyCompensation as vrayGtrEnergyCompensation

importlib.reload(common.loadAndSaveAssetUI)
# importlib.reload(api.savedAsset)
importlib.reload(api.scene)
# importlib.reload(rbwUI)
importlib.reload(vrayObjectId)
importlib.reload(vrayGtrEnergyCompensation)


################################################################################
# @brief      This class describes a load and save surfacing ui.
#
class LoadAndSaveSurfacingUI(common.loadAndSaveAssetUI.AssetUI):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(LoadAndSaveSurfacingUI, self).__init__('Surfacing')

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        super(LoadAndSaveSurfacingUI, self).initUI()

        self.saveExportWithRenderElementsLabel = rbwUI.RBWLabel(text='Export With Render Elements', italic=False, size=12)
        self.saveExportWithRenderElementsSwitch = rbwUI.RBWSwitch()
        self.saveExportVrSceneWipLabel = rbwUI.RBWLabel(text='Export VrScene (Only WIP)', italic=False, size=12)
        self.saveExportVrSceneWipSwitch = rbwUI.RBWSwitch()

        self.saveInfoGroup.addRow(self.saveExportWithRenderElementsLabel, self.saveExportWithRenderElementsSwitch)
        self.saveInfoGroup.addRow(self.saveExportVrSceneWipLabel, self.saveExportVrSceneWipSwitch)

    ############################################################################
    # @brief      save funnction.
    #
    # @return     { description_of_the_return_value }
    #
    def save(self):
        try:
            savedAssetParam = {
                'asset': self.currentAsset,
                'dept': self.deptName.lower(),
                'deptType': self.currentDeptType,
                'approvation': self.currentApprovation,
                'note': self.saveNotesTextEdit.toPlainText(),
                'exportElements': self.saveExportWithRenderElementsSwitch.isChecked(),
                'vrscene': self.saveExportVrSceneWipSwitch.isChecked()
            }

            if self.saveExportSelectionSwitch.isChecked():
                savedAssetParam['selection'] = cmd.ls(selection=True)[0]
            else:
                savedAssetParam['selection'] = None

            if self.currentApprovation == 'def':

                if not self.checkToSave():
                    return

                wipSavedAssetParam = {
                    'asset': self.currentAsset,
                    'dept': self.deptName.lower(),
                    'deptType': self.currentDeptType,
                    'approvation': 'wip',
                    'note': 'Pipeline autosave before def save',
                    'vrscene': False,
                    'exportElements': False,
                    'selection': savedAssetParam['selection']
                }

                wipSavedAsset = api.savedAsset.SavedAsset(params=wipSavedAssetParam)
                wipSuccess = wipSavedAsset.save()

                if wipSuccess:
                    savedAssetParam['parent'] = wipSavedAsset.db_id
                    savedAsset = api.savedAsset.SavedAsset(params=savedAssetParam)
                    success = savedAsset.save()
                    if success:
                        self.updateWipParentOnDB(wipSavedAsset, savedAsset)
                else:
                    rbwUI.RBWError(title='ATTENTION', text='Problems during autosave wip.\nPleas control the scene and make a new save.')
                    return
            else:
                savedAsset = api.savedAsset.SavedAsset(params=savedAssetParam)
                success = savedAsset.save()

            if success:
                rbwUI.RBWConfirm(title='Save {} Complete'.format(self.deptName), text='Asset Save Complete', defCancel=None)
                self.fillSavedAssetsTree()
            else:
                rbwUI.RBWError(title='ATTENTION', text='Some Error During Save')

        except AttributeError:
            if not cmd.pluginInfo("AbcExport", q=True, l=True):
                cmd.loadPlugin("AbcExport", qt=True)
                rbwUI.RBWError(title='ATTENTION', text='You had AbcExport plugin turned off, I loaded it for you.\nTry again to save.')
            else:
                rbwUI.RBWError(title='ATTENTION', text='One between deptType or approvation are not properly selected.\nSelect those and try again.')

    ################################################################################
    # @brief      checks the outliner.
    #
    # @return     False if the outliner contains nested msh; True otherwise.
    #
    def checkOutliner(self):
        check = True

        # checks for nested msh
        parents = {}
        for x in cmd.ls(type="transform"):
            par = cmd.listRelatives(x, parent=True)
            if par and par[0].startswith('MSH_'):
                check = False
                parents[x] = par[0]

        # error message
        if not check:
            errorMessage = "There are nested meshes!\n"
            for child in parents.keys():
                errorMessage += "\n- {} child of {}".format(child, parents[child])
            rbwUI.RBWError(title="ATTENTION", text=errorMessage)

        return check

    ################################################################################
    # @brief      checks the displacement node.
    #
    # @return     True if the displacement node contains only shapes; False otherwise.
    #
    def checkDisplacement(self):
        check = True
        outliers = []

        # checks for shapes
        displacementNodes = list(cmd.ls(type='VRayDisplacement'))
        for node in displacementNodes:
            children = list(cmd.listRelatives(node, children=True))
            for child in children:
                if not str(cmd.objectType(child)) == "mesh":
                    check = False
                    outliers.append((child, cmd.objectType(child)))

        # error message
        if not check:
            errorMessage = "Displacement Node contains outliers!\n"
            for outlier in outliers:
                errorMessage += "\n- {} of type {}".format(outlier[0], outlier[1])
            rbwUI.RBWError(title="ATTENTION", text=errorMessage)

        return check

    ################################################################################
    # @brief      check CNT set before saving
    #
    # @return     boolean
    #
    def checkCNTset(self):
        setCNTs = cmd.ls('CNT*', type='objectSet')

        if len(setCNTs) == 0:
            rbwUI.RBWError(title='ATTENTION', text='CNT set is missing.\nSaving is aborted.')
            return False

        testString = r"CNT_\d{3}"
        for cntSet in setCNTs:
            if not re.match(testString, cntSet) and cntSet != 'CNT':
                rbwUI.RBWError(title='ATTENTION', text='{} must be renamed "CNT_###" or "CNT".\nSaving is aborted.'.format(cntSet))
                return False

        return True

    ################################################################################
    # @brief      check material name before save.
    #
    # @return     the check value.
    #
    def checkMaterialsName(self):
        sceneMaterial = api.scene.listUsedSceneMaterials()

        for material in sceneMaterial:
            if material.startswith('sl_'):
                rbwUI.RBWError(title='ATTENTION', text='One or more materials still have the name of the shader library.\nSaving is aborted.')
                return False

        return True

    ################################################################################
    # @brief      check the connection between the mesh and the shader.
    #
    # @return     the check value.
    #
    def checkShaderConnections(self):
        errors = []
        if cmd.objExists('MSH'):
            meshes = cmd.sets('MSH', query=True)
        else:
            meshes = cmd.ls('MSH*', type='transform')

        for mesh in meshes:
            try:
                shapeItem = cmd.listRelatives(mesh, shapes=True)[0]
                if shapeItem.endswith('vrayproxy'):
                    continue
                shadeEng = cmd.listConnections(shapeItem, type='shadingEngine')[0]
                cmd.ls(cmd.listConnections(shadeEng), materials=True)[0]
            except:
                errors.append(mesh)

        if errors:
            rbwUI.RBWWarning(text="There are some problems with the shader connections of these meshes:\n - {}".format('\n - '.join(errors)), parent=self)
            return False

        return True

    ############################################################################
    # @brief      check to save function.
    #
    # @return     check result.
    #
    def checkToSave(self):
        super(LoadAndSaveSurfacingUI, self).checkToSave()

        if self.currentDeptType in ['uvs', 'txt']:
            if not self.checkTextureExt():
                return
            return True

        if self.currentAsset.category in ['character', 'prop']:
            if not self.checkCNTset():
                return False

        if self.currentApprovation == 'def' and self.currentAsset.category == 'character':
            if not self.checkShaderConnections():
                return False

        if not vrayObjectId.checkMaterialId():
            return
        if not vrayGtrEnergyCompensation.checkGtrEnergyCompensation():
            warningMessage = 'One or more materials has the attribute \'gtrEnergyCompensation\' set on ON.\nUse the tool for check and sanize.'
            rbwUI.RBWWarning(title='ATTENTION', text=warningMessage, resizable=True, defCancel=None)
            return
        if not self.checkOutliner():
            return
        if not self.checkDisplacement():
            return

        if self.currentAsset.category not in ['shader', 'library']:
            if not self.checkMaterialsName():
                return

            if not self.checkTexturesPath():
                return

            if self.currentAsset.category != 'lightrig':
                if not self.checkMSHSet():
                    return

        return True
