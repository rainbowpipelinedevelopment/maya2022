import importlib
import os

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'arakno')


def run_loadAndSaveModeling(*args):
    import depts.modeling.tools.main.loadAndSaveModelingUI as loadAndSaveMod
    importlib.reload(loadAndSaveMod)

    win = loadAndSaveMod.LoadAndSaveModelingUI()
    win.show()


def run_loadAndSave(*args):
    import depts.surfacing.tools.main.loadAndSaveSurfacingUI as loadAndSaveSurf
    importlib.reload(loadAndSaveSurf)

    win = loadAndSaveSurf.LoadAndSaveSurfacingUI()
    win.show()


def run_openSourceImagesFolder(*args):
    import common.openSceneFolder as openSceneFolder
    importlib.reload(openSceneFolder)

    openSceneFolder.openSourceImagesFolder('surfacing')


def run_shaderLibrary(*args):
    import depts.surfacing.tools.main.shaderLibraryUI as shaderLibraryUI
    importlib.reload(shaderLibraryUI)

    win = shaderLibraryUI.ShaderLibraryUI()
    win.show()


loadAndSaveModelingTool = {
    "name": "L/S Modeling",
    "launch": run_loadAndSaveModeling,
    "icon": os.path.join(iconDir, "loadAndSaveModeling.png"),
    "statustip": "Load & Save Modeling"
}


loadAndSaveTool = {
    "name": "L/S Surfacing",
    "launch": run_loadAndSave,
    "icon": os.path.join(iconDir, "loadAndSaveSurfacing.png"),
    "statustip": "Load & Save Surfacing"
}


openSourceImagesFolder = {
    "name": "Sourceimages Folder",
    "launch": run_openSourceImagesFolder,
    "icon": os.path.join(iconDir, "..", "surfacing", 'sourceimages.png'),
    "statustip": "Open Source Images Folder"
}


shaderLibraryTool = {
    "name": "Shader Library",
    "launch": run_shaderLibrary,
    "icon": os.path.join(iconDir, "..", "surfacing", "shaderLibrary.png"),
    "statustip": "Open shader library UI"
}


tools = [
    loadAndSaveModelingTool,
    loadAndSaveTool,
    openSourceImagesFolder,
    shaderLibraryTool
]

PackageTool = {
    "name": "main",
    "tools": tools
}
