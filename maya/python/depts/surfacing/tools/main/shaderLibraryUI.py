import importlib
from PySide2 import QtWidgets, QtGui, QtCore
import os

import maya.cmds as cmd

import api.json
import api.database
import api.asset
import api.savedAsset
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.json)
importlib.reload(api.database)
# importlib.reload(api.asset)
# importlib.reload(api.savedAsset)
# importlib.reload(rbwUI)


iconPath = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'common')


################################################################################
# @brief      This class describes a shader library ui.
#
class ShaderLibraryUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(ShaderLibraryUI, self).__init__()

        if cmd.window('ShaderLibraryUI', exists=True):
            cmd.deleteUI('ShaderLibraryUI')

        self.getShaders()
        self.mcLibrary = "//speed.nas/library/02_production/01_content/mc_library"

        self.setObjectName('ShaderLibraryUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)
        self.mainLayout.addWidget(self.splitter)

        ########################################################################
        # LEFT PANEL
        #
        self.shaderPanel = rbwUI.RBWFrame(layout='V')

        # ------------------------- search bar ------------------------------- #
        self.searchLineEdit = rbwUI.RBWLineEdit(title='Search:', placeHolder='Insert shader name', stretch=False, bgColor='rgba(30, 30, 30, 150)')
        self.searchLineEdit.edit.returnPressed.connect(self.searchShader)

        self.shaderPanel.addWidget(self.searchLineEdit)

        # ------------------------ result area ------------------------------- #
        self.resultList = QtWidgets.QListWidget()
        self.resultList.itemClicked.connect(self.traverseToShader)
        self.shaderPanel.addWidget(self.resultList)

        # ------------------------ shader tree ------------------------------- #
        self.shadersTree = rbwUI.RBWTreeWidget(['Shader'], decorate=True)
        self.shadersTree.itemSelectionChanged.connect(self.fillImageList)
        self.shaderPanel.addWidget(self.shadersTree)

        self.splitter.addWidget(self.shaderPanel)

        ########################################################################
        # RIGHT PANEL
        #
        self.mainPanel = rbwUI.RBWFrame(layout='V')

        # ------------------------ iconSize slider --------------------------- #
        self.iconSizeWidget = rbwUI.RBWFrame(bgColor='transparent', layout='G')
        for i in range(0, 4):
            self.iconSizeWidget.layout.setColumnStretch(i, 40)

        self.iconSizeLabel = rbwUI.RBWLabel(text='Icon Size:', size=12, italic=False)
        self.iconSizeSlider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        self.iconSizeSlider.setMinimum(3)
        self.iconSizeSlider.setMaximum(5)
        self.iconSizeSlider.setTickInterval(1)
        self.iconSizeSlider.setSingleStep(1)
        self.iconSizeSlider.setSliderPosition(3)
        self.iconSizeSlider.valueChanged.connect(self.changeIconSize)

        self.smallLabel = rbwUI.RBWLabel('S', size=12, italic=False)
        self.smallLabel.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.mediumLabel = rbwUI.RBWLabel('M', size=12, italic=False)
        self.mediumLabel.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.largeLabel = rbwUI.RBWLabel('L', size=12, italic=False)
        self.largeLabel.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

        self.iconSizeWidget.layout.addWidget(self.iconSizeLabel, 0, 4, 2, 1)
        self.iconSizeWidget.layout.addWidget(self.iconSizeSlider, 0, 5, 1, 3)
        self.iconSizeWidget.layout.addWidget(self.smallLabel, 1, 5, 1, 1)
        self.iconSizeWidget.layout.addWidget(self.mediumLabel, 1, 6, 1, 1)
        self.iconSizeWidget.layout.addWidget(self.largeLabel, 1, 7, 1, 1)

        self.mainPanel.addWidget(self.iconSizeWidget, alignment=QtCore.Qt.AlignRight)

        # ------------------------ image list ------------------------------- #
        self.imageListWidget = QtWidgets.QListWidget()
        self.imageListWidget.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.imageListWidget.setViewMode(QtWidgets.QListView.IconMode)
        self.imageListWidget.setResizeMode(QtWidgets.QListView.Adjust)
        self.imageListWidget.setMovement(QtWidgets.QListView.Static)
        self.imageListWidget.setIconSize(QtCore.QSize(150, 150))
        self.imageListWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.imageListWidget.customContextMenuRequested.connect(self.openRightClickMenu)
        self.imageListWidget.itemClicked.connect(self.previewShader)
        self.mainPanel.addWidget(self.imageListWidget)

        self.splitter.addWidget(self.mainPanel)

        self.splitter.setStretchFactor(0, 1)
        self.splitter.setStretchFactor(1, 3)

        self.fillShaderTree()

        self.setMinimumSize(1300, 700)

        self.setTitle('Shader Library UI')
        self.setIcon('../surfacing/ShaderLibrary.png')
        self.setFocus()

    ############################################################################
    # @brief      Opens a right click menu.
    #
    # @param      position  The position
    #
    def openRightClickMenu(self, position):
        popMenu = QtWidgets.QMenu()

        previewAction = QtWidgets.QAction("Preview", self)
        previewIcon = QtGui.QIcon()
        previewIcon.addFile(os.path.join(iconPath, 'preview.png'))
        previewAction.setIcon(previewIcon)

        importAction = QtWidgets.QAction("Import", self)
        importIcon = QtGui.QIcon()
        importIcon.addFile(os.path.join(iconPath, 'merge.png'))
        importAction.setIcon(importIcon)

        if self.imageListWidget.itemAt(position):
            popMenu.addAction(previewAction)
            popMenu.addAction(importAction)

        previewAction.triggered.connect(self.previewShader)
        importAction.triggered.connect(self.importShaderInScene)
        popMenu.exec_(self.imageListWidget.mapToGlobal(position))

    ################################################################################
    # @brief      Gets the shaders.
    #
    def getShaders(self):
        self.shaders = {}

        groups = ['generic', os.getenv('PROJECT').lower()]
        for group in groups:
            self.shaders[group] = {}

        for group in groups:
            selectShaderNameQuery = "SELECT `name`, `variant` FROM `V_assetList` WHERE `projectID` = '1' AND `category` = 'shader' AND `group` = '{}'".format(group)

            results = api.database.selectQuery(selectShaderNameQuery)

            for shaderTupla in results:
                key = shaderTupla[0]
                value = shaderTupla[1]

                if key in self.shaders[group]:
                    self.shaders[group][key].append(value)
                else:
                    self.shaders[group][key] = [value]

    ############################################################################
    # @brief      fill the shader list on the left.
    #
    def fillShaderTree(self):
        for shaderGroup in self.shaders:
            child = QtWidgets.QTreeWidgetItem(self.shadersTree)
            child.setText(0, shaderGroup)

            shaderNames = list(self.shaders[shaderGroup].keys())
            shaderNames.sort()

            for shaderName in shaderNames:
                subChild = QtWidgets.QTreeWidgetItem(child)
                subChild.setText(0, shaderName)

    ############################################################################
    # @brief      Creates an image item.
    #
    # @param      imagePath  The image path
    # @param      itemName   The item name
    #
    # @return     the item
    #
    def createImageItem(self, imagePath, itemName):
        pixmap = QtGui.QPixmap(imagePath)
        icon = QtGui.QIcon()
        icon.addPixmap(pixmap)

        imageItem = QtWidgets.QListWidgetItem(icon, itemName)

        return imageItem

    ############################################################################
    # @brief      fill the image panel in the main panel.
    #
    def fillImageList(self):
        self.imageListWidget.clear()

        try:
            self.currentCategory = 'shader'
            self.currentName = self.shadersTree.selectedItems()[0].text(0)
            self.currentGroup = self.shadersTree.selectedItems()[0].parent().text(0)

            allVariant = self.shaders[self.currentGroup][self.currentName]
            allVariant.sort()

            for i in range(0, len(allVariant)):
                variant = allVariant[i]

                assetParams = {
                    'category': self.currentCategory,
                    'group': self.currentGroup,
                    'name': self.currentName,
                    'variant': variant,
                    'id_mv': 1
                }

                currentAsset = api.asset.Asset(params=assetParams)

                savedAssetParams = {
                    'asset': currentAsset,
                    'dept': 'surfacing',
                    'deptType': 'rnd',
                    'approvation': 'def',
                }

                currentSavedAsset = api.savedAsset.SavedAsset(params=savedAssetParams)

                lastShaderSavedAsset = currentSavedAsset.getLatest()

                if lastShaderSavedAsset:
                    lastShaderFolder = os.path.join(self.mcLibrary, os.path.dirname(lastShaderSavedAsset[2])).replace('\\', '/')
                    for file in os.listdir(lastShaderFolder):
                        if file.endswith('.jpg'):
                            lastShaderPath = os.path.join(lastShaderFolder, file).replace('\\', '/')
                            break

                    imageItem = self.createImageItem(lastShaderPath, currentAsset.variant)
                    self.imageListWidget.addItem(imageItem)

        except AttributeError:
            pass

    ############################################################################
    # @brief      change the main panel icon size.
    #
    def changeIconSize(self):
        scaleFactor = self.iconSizeSlider.value()
        iconSize = scaleFactor * 50
        self.imageListWidget.setIconSize(QtCore.QSize(iconSize, iconSize))

    ############################################################################
    # @brief      Sets the current shader.
    #
    def setCurrentShader(self):
        item = self.imageListWidget.currentItem()
        self.currentVariant = item.text()

        currentAssetParams = {
            'category': 'shader',
            'group': self.currentGroup,
            'name': self.currentName,
            'variant': self.currentVariant,
            'id_mv': 1
        }

        self.currentAsset = api.asset.Asset(params=currentAssetParams)

        currentSavedAssetParams = {
            'asset': self.currentAsset,
            'dept': 'surfacing',
            'deptType': 'rnd',
            'approvation': 'def'
        }

        self.currentSavedAsset = api.savedAsset.SavedAsset(params=currentSavedAssetParams)

    ############################################################################
    # @brief      get a preview for the selected shader.
    #
    def previewShader(self):
        self.setCurrentShader()

        latestShaderId = self.currentSavedAsset.getLatest()[0]

        previewPanel = PreviewShaderPanel(latestShaderId)
        previewPanel.show()

    ############################################################################
    # @brief      import shader in scene.
    #
    def importShaderInScene(self):
        self.setCurrentShader()
        lastShaderPath = os.path.join(self.mcLibrary, self.currentSavedAsset.getLatest()[2])

        cmd.file(lastShaderPath, i=True, type="mayaBinary", ignoreVersion=True, mergeNamespacesOnClash=False, pr=True)

    ############################################################################
    # @brief      fill the result shader list.
    #
    def searchShader(self):
        self.resultList.clear()
        wantedName = self.searchLineEdit.text()

        if wantedName != '':
            for group in self.shaders:
                for name in self.shaders[group]:
                    if wantedName in name:
                        for variant in self.shaders[group][name]:
                            item = QtWidgets.QListWidgetItem('{}-{}-{}'.format(group, name, variant))
                            self.resultList.addItem(item)

    ############################################################################
    # @brief      traverse to the wanted shader.
    #
    def traverseToShader(self):
        currentSelection = self.resultList.currentItem().text()

        wantedGroup = currentSelection.split('-')[0]
        wantedName = currentSelection.split('-')[1]
        wantedVariant = currentSelection.split('-')[2]

        groupItem = self.shadersTree.findItems(wantedGroup, QtCore.Qt.MatchFixedString)[0]
        groupItem.setExpanded(True)

        try:
            for i in range(0, groupItem.childCount()):
                if groupItem.child(i).text(0) == wantedName:
                    nameItem = groupItem.child(i)
                    self.shadersTree.setCurrentItem(nameItem)
                    imageItem = self.imageListWidget.findItems(wantedVariant, QtCore.Qt.MatchFixedString)[0]
                    self.imageListWidget.setCurrentItem(imageItem)
                    self.previewShader()
        except IndexError:
            pass


################################################################################
# @brief      This class describes a preview shader panel.
#
class PreviewShaderPanel(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self, shaderId):
        super().__init__()

        self.shader = api.savedAsset.SavedAsset(params={'db_id': shaderId, 'approvation': 'def'})
        self.mcLibrary = "//speed.nas/library/02_production/01_content/mc_library"

        if cmd.window('PreviewShaderPanel', exists=True):
            cmd.deleteUI('PreviewShaderPanel')

        self.setObjectName('PreviewShaderPanel')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topWidget = rbwUI.RBWFrame(layout='V')
        self.mainLayout.addWidget(self.topWidget)

        # shader view
        self.shaderImage = self.getPreviewImage()
        self.shaderViewer = rbwUI.RBWPhotoViewer(self, self.shaderImage)
        self.topWidget.addWidget(self.shaderViewer, alignment=QtCore.Qt.AlignHCenter)

        # shader infos
        self.shaderInfosTree = rbwUI.RBWTreeWidget(['Property', 'Value'])
        self.shaderInfosTree.setColumnWidth(0, 235)

        shaderInfosPath = os.path.join(self.mcLibrary, self.shader.path.replace('.mb', '_shader_infoes.json')).replace('\\', '/')
        if os.path.exists(shaderInfosPath):
            shaderInfos = api.json.json_read(shaderInfosPath)

            child0 = QtWidgets.QTreeWidgetItem(self.shaderInfosTree)
            child0.setText(0, 'Type')
            child0.setTextAlignment(0, QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
            child0.setText(1, str(shaderInfos['material']['type']))
            child0.setTextAlignment(1, QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

            child1 = QtWidgets.QTreeWidgetItem(self.shaderInfosTree)
            child1.setText(0, 'Textures Number')
            child1.setTextAlignment(0, QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
            child1.setText(1, str(shaderInfos['material']['textures']))
            child1.setTextAlignment(1, QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

            child2 = QtWidgets.QTreeWidgetItem(self.shaderInfosTree)
            child2.setText(0, 'Multi SubTex')
            child2.setTextAlignment(0, QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
            child2.setText(1, str(shaderInfos['material']['multiSubText']))
            child2.setTextAlignment(1, QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

            self.topWidget.addWidget(self.shaderInfosTree)

        # import button
        self.importButton = rbwUI.RBWButton(text='Import', size=[480, 35], icon=['merge.png'])
        self.importButton.clicked.connect(self.importShaderInScene)
        self.topWidget.addWidget(self.importButton, alignment=QtCore.Qt.AlignHCenter)

        self.setFixedSize(502, 680)
        self.setTitle('Preview {} {}'.format(self.shader.getName(), self.shader.getVariant()))
        self.setIcon('preview.png')
        self.setFocus()

    ############################################################################
    # @brief      Gets the preview image.
    #
    # @return     The preview image.
    #
    def getPreviewImage(self):
        lastShaderFolder = os.path.join(self.mcLibrary, os.path.dirname(self.shader.path)).replace('\\', '/')
        for file in os.listdir(lastShaderFolder):
            if file.endswith('.jpg'):
                lastShaderPath = os.path.join(lastShaderFolder, file).replace('\\', '/')
                return lastShaderPath

    ############################################################################
    # @brief      import shader in scene.
    #
    def importShaderInScene(self):
        lastShaderPath = os.path.join(self.mcLibrary, self.shader.path).replace('\\', '/')
        cmd.file(lastShaderPath, i=True, type="mayaBinary", ignoreVersion=True, mergeNamespacesOnClash=False, pr=True)
