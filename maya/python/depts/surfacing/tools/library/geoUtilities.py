import maya.cmds as cmd


def setAsCustomGeo():
    geosTopGroup = 'viewtool_geos_shader_:GRP_geos'
    customMSHName = 'viewtool_geos_shader_:MSH_custom'
    selectedObjs = cmd.ls(selection=True)

    if len(selectedObjs) == 1:
        selectedObj = selectedObjs[0]
        cmd.parent(selectedObj, geosTopGroup)
        cmd.rename(selectedObj, customMSHName)
    else:
        cmd.confirmDialog(title='ERROR', icon='critical', message='You have to select a maximum of one object.', button=['OK'])
