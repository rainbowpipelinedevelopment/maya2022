import importlib
import os

import maya.cmds as cmd

import api.log
import api.vray
import api.asset
import api.savedAsset
import depts.lighting.tools.generic.sceneSetup as sceneSetup

importlib.reload(api.log)
importlib.reload(api.vray)
# importlib.reload(api.asset)
# importlib.reload(api.savedAsset)
importlib.reload(sceneSetup)


defaultRenderElements = [
    'diffuseChannel',
    'bumpNormalsChannel',
    'reflectChannel',
    'reflectGlossinessChannel',
    'specularChannel',
    'refractChannel',
    'refractGlossinessChannel'
]


def setupScene():
    collectObjects()
    setRenderSettings()


################################################################################
# @brief      collect the reference for the scene setup.
#
def collectObjects():
    mcLibraryPath = os.getenv('MC_FOLDER').replace(os.getenv('PROJECT'), 'library').replace('\\', '/')
    # import the camera
    cameraSavedAsset = api.savedAsset.SavedAsset(params={'asset': api.asset.Asset(params={'category': 'library', 'group': 'viewtool', 'name': 'camera', 'variant': 'shader', 'id_mv': 1}), 'dept': 'surfacing', 'deptType': 'rnd', 'approvation': 'def'})
    cameraNamespace = '{}_{}_{}_'.format(cameraSavedAsset.getGroup(), cameraSavedAsset.getName(), cameraSavedAsset.getVariant())
    cameraPath = os.path.join(mcLibraryPath, cameraSavedAsset.getLatest()[2])
    api.scene.addToLibraryGroup(cameraPath, cameraNamespace)

    # import the geos
    geosSavedAsset = api.savedAsset.SavedAsset(params={'asset': api.asset.Asset(params={'category': 'library', 'group': 'viewtool', 'name': 'geos', 'variant': 'shader', 'id_mv': 1}), 'dept': 'surfacing', 'deptType': 'rnd', 'approvation': 'def'})
    geosNamespace = '{}_{}_{}_'.format(geosSavedAsset.getGroup(), geosSavedAsset.getName(), geosSavedAsset.getVariant())
    geosPath = os.path.join(mcLibraryPath, geosSavedAsset.getLatest()[2])
    api.scene.addToLibraryGroup(geosPath, geosNamespace)

    # import the lightrig
    lightrigSavedAsset = api.savedAsset.SavedAsset(params={'asset': api.asset.Asset(params={'category': 'lightrig', 'group': 'generic', 'name': 'shader', 'variant': 'default', 'id_mv': 1}), 'dept': 'surfacing', 'deptType': 'rnd', 'approvation': 'def'})
    lightrigNamespace = '{}_{}_{}_'.format(lightrigSavedAsset.getGroup(), lightrigSavedAsset.getName(), lightrigSavedAsset.getVariant())
    lightrigPath = os.path.join(mcLibraryPath, lightrigSavedAsset.getLatest()[2])
    api.scene.addToLibraryGroup(lightrigPath, lightrigNamespace)

    # delete the animation on hdri rotation
    animCurveNodes = cmd.ls('{}:*_VRayPlaceEnvTex_horRotation'.format(lightrigNamespace), type='animCurveTU')
    for animCurveNode in animCurveNodes:
        cmd.disconnectAttr('{}.output'.format(animCurveNode), '{}.horRotation'.format('_'.join(animCurveNode.split('_')[:-1])))


################################################################################
# @brief      Sets the render settings.
#
def setRenderSettings():
    # render settings
    if cmd.getAttr('defaultRenderGlobals.currentRenderer') != 'vray':
        cmd.setAttr("defaultRenderGlobals.currentRenderer", "vray", type="string")

    masterLayerOverrides = sceneSetup.masterLayerOverride

    for override in masterLayerOverrides:
        cmd.setAttr('vraySettings.{}'.format(override), masterLayerOverrides[override])

    api.vray.setImageFormat(2)
    cmd.setAttr("vraySettings.aspectLock", 0)
    cmd.setAttr("vraySettings.width", 2048)
    cmd.setAttr("vraySettings.height", 2048)
    cmd.setAttr("vraySettings.aspectLock", 1)

    for renderElement in defaultRenderElements:
        api.vray.addRenderElement(renderElement)
