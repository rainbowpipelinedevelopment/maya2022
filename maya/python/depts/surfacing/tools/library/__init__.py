import importlib
import os

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'surfacing')


################################################################################
# @brief      { function_description }
#
# @param      args  The arguments
#
def run_shaderSceneSetup(*args):
    import depts.surfacing.tools.library.shaderSceneSetup as shaderSceneSetup
    importlib.reload(shaderSceneSetup)

    shaderSceneSetup.setupScene()


def run_setAsCustomGeo(*args):
    import depts.surfacing.tools.library.geoUtilities as geoUtilities
    importlib.reload(geoUtilities)

    geoUtilities.setAsCustomGeo()


shaderSceneSetupTool = {
    "name": "Shader Scene Setup",
    "statustip": "Set up scene for shader",
    "launch": run_shaderSceneSetup,
    "icon": os.path.join(iconDir, '..', 'common', "setup.png")
}


setAsCustomGeoTool = {
    "name": "Set As Custom Geo",
    "launch": run_setAsCustomGeo,
    "statustip": "Set The Selected Obj As Custom Geo",
    "icon": os.path.join(iconDir, '..', 'common', "option.png")
}

tools = [
    shaderSceneSetupTool,
    setAsCustomGeoTool
]

PackageTool = {
    "name": "library",
    "tools": tools,
    "icon_size": "32",
    "icon_only": False
}
