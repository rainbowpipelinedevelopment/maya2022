import importlib
import os

import maya.cmds as cmd
import maya.mel as mel

import depts.mc_depts as mcdepts
import api.log
import api.scene
import api.json
import api.widgets.rbw_UI as rbwUI
import config.dictionaries as dictionary
import depts.rigging.tools.generic.createMeshcutSet as createMeshcutSet

importlib.reload(mcdepts)
importlib.reload(api.log)
importlib.reload(api.scene)
importlib.reload(api.json)
# importlib.reload(rbwUI)
importlib.reload(dictionary)
importlib.reload(createMeshcutSet)


################################################################################
# @brief      This class describes a rigging.
#
class Rigging(mcdepts.Dept):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      kwargs  The keywords arguments
    #
    def __init__(self, savedAsset):
        super(Rigging, self).__init__(savedAsset)
        self.deptName = "rigging"

    ############################################################################
    # @brief      save function
    #
    # @return     the save succes.
    #
    def save(self):
        if self.isDef():
            api.scene.pluginCleanUp()

            # se c'e' nodo mash repro faccio il maya set
            self.createMashSet()

        # local save first
        self.localSave()

        # export for modeling consistence
        self.exportXML()

        # Remove absolute paths
        try:
            self.checkTexturesPath()
        except:
            rbwUI.RBWWarning(title='warning', text='Error while checking and replacing absolute paths with relative ones.\nThe save will contine but please contact pipeline@rbw-cgi.it')

        # solo in caso di def assemblato fa il controllo dei set necessari (hardcoded for now..)
        if cmd.objExists('CHAR'):
            if self.checkSetPresence():
                api.log.logger().debug('The required maya set for the assembled def are missing, please check!')

        if self.isDef():
            self.deleteUnusedShapeOrig()
            # Meshcut set procedure:
            createMeshcutSet.meshcutSet()

        # === Real Save On Disk!! ===
        self.realSave()

        scenefilename = api.scene.scenename()

        # --------------------------------------------------------------------------
        # Export the multiswitch controls in .ma file format
        #
        self.exportMultiswitch()

        if self.isDef():
            # export NCACHE file
            self.exportNCache()

            # export MASH_Repro file
            self.exportMash()

            self.exportAlembicInfo()

        # --------------------------------------------------------------------------
        cmd.file(scenefilename, open=True, force=True)

        # leave this!
        return True

    ############################################################################
    # @brief      Creates a mash set.
    #
    def createMashSet(self):
        eventualMashReproRig = cmd.ls(type='MASH_Repro')
        if not cmd.objExists('MASHREPRO'):
            if len(eventualMashReproRig):
                cmd.select(eventualMashReproRig, replace=True)
                cmd.sets(name='MASHREPRO')
                cmd.select(clear=True)
                api.log.logger().debug("In this rig exists a 'MASH_Repro' node so it has been created the maya-set: 'MASHREPRO' for finishing purpose.")

    def checkSetPresence(self):
        missingSet = []
        sets = dictionary.riggingSets
        for set in sets:
            cmd.select(cl=1)
            api.log.logger().debug(set)
            if not cmd.objExists(set):
                cmd.sets(name=set)
                missingSet.append(set)
        if missingSet:
            self.errors.append("Set missing:\n{}".format("\n".join(missingSet)))
            return True

    ############################################################################
    # @brief      export meshes data.
    #
    def exportMeshes(self):
        try:
            openScenePath = str(cmd.file(q=1, sceneName=1))
            jsonPath = openScenePath.replace('.mb', '_MSH.json')

            rmSetName = 'All_Rig_Driver_Meshes_RM'
            cmd.select(rmSetName)
            allRM = cmd.ls(selection=True)

            api.json.json_write(allRM, jsonPath)
        except:
            api.log.logger().error("nProblem to save the json file of RM meshes for Surfacing check.")

    ############################################################################
    # @brief      export multiswitch data.
    #
    def exportMultiswitch(self):
        scenefilename = api.scene.scenename()

        msSet = 'multiswitch'
        msControlList = []
        cmd.select(clear=True)

        # Check if the multiswitch set exists and eventually get the controls list
        if cmd.objExists(msSet):
            for element in cmd.sets(msSet, query=True):
                childList = cmd.listRelatives(element, children=True, fullPath=True)

                for child in childList:
                    if cmd.objectType(child, isType='nurbsCurve') or cmd.objectType(child, isType='nurbsSurface'):
                        msControlList.append(element)
                        break

        # Double for on the control list in case of nested multiswitch controls (unparent first, delete sons later)
        if len(msControlList) > 0:
            # Unparent the control
            for control in msControlList:
                cmd.parent(control, world=True)

            # Delete its not children (if they are not nurbsCurve or nurbsSurface)
            for control in msControlList:
                if '|' in control:
                    control = '|{}'.format(control.split('|')[-1])
                for child in cmd.listRelatives(control, children=True, fullPath=True):
                    if not (cmd.objectType(child, isType='nurbsCurve') or cmd.objectType(child, isType='nurbsSurface')):
                        cmd.delete(child)

                # Get the unparented control
                controlToExport = '|{}'.format(control.split('|')[-1])
                cmd.select(controlToExport, add=True)

            # Export the control curves
            exportPath = scenefilename.replace('.mb', '_multiswitch.ma')
            cmd.file(exportPath, force=True, typ="mayaAscii", exportSelected=True, preserveReferences=True, shader=True, expressions=True, constructionHistory=True)

    ############################################################################
    # @brief      export ncache data.
    #
    def exportNCache(self):
        scenefilename = api.scene.scenename()

        if cmd.objExists('NCACHE'):
            exportNcachePath = scenefilename.replace('.mb', '_ncache.ma')

            # unparent the groups in NCACHE set
            cmd.select('NCACHE', replace=True)
            exportObj = cmd.ls(selection=True)[0]
            cmd.parent(exportObj, world=True)
            cmd.InvertSelection()
            cmd.delete()

            # switchShader
            effectName = exportObj.split('_')[1]
            aniShader = '{}_Shader_ani'.format(effectName)
            rndShader = '{}_Shader_rnd'.format(effectName)

            if cmd.objExists(aniShader) and cmd.objExists(rndShader):
                cmd.select(clear=True)
                cmd.hyperShade(objects=aniShader)
                objects = cmd.ls(selection=1)

                if rndShader:
                    if cmd.objExists(rndShader):
                        shaderConnections = cmd.listConnections(rndShader, d=True, et=True, t='shadingEngine')
                        if shaderConnections:
                            shadingGroup = shaderConnections[0]

                if shadingGroup:
                    cmd.select(clear=True)
                    cmd.select(objects)
                    cmd.sets(e=True, forceElement=shadingGroup)
            else:
                rbwUI.RBWWarning(title='ATTENTION', text='rnd and ani shaders not found.\nShader switch skipped.\nControl with your supervisor.\nThe save process continues.')

            # scollego la scala delle mesh usate dall'istancer
            melCmd0 = 'source "channelBoxCommand.mel";'
            mel.eval(melCmd0)
            attrs = ['sx', 'sy', 'sz']

            istancerObjects = cmd.ls(type='instancer')
            for ist in istancerObjects:
                for item in cmd.listConnections(ist):
                    if item.count('MSH_'):
                        for atr in attrs:
                            melCmd = ('CBdeleteConnection "{}.{}";'.format(item, atr))
                            mel.eval(melCmd)

            cmd.select(exportObj, replace=True)
            cmd.file(exportNcachePath, force=True, typ="mayaAscii", exportSelected=True, preserveReferences=True, shader=True, expressions=True, constructionHistory=True)

    ############################################################################
    # @brief      export mash node.
    #
    def exportMash(self):
        scenefilename = api.scene.scenename()
        eventualMashReproRig = cmd.ls(type='MASH_Repro')

        if cmd.objExists('MASHREPRO'):
            allMeshIstance = []
            allReproMesh = []
            for rprMash in eventualMashReproRig:
                reproMesh = cmd.listConnections('{}.outMesh'.format(rprMash))
                allReproMesh.append(reproMesh[0])

                for mesh in cmd.listConnections(eventualMashReproRig, connections=True):
                    if mesh.count('instancedGroup'):
                        if mesh.count('instancedMesh'):
                            if mesh.count('.mesh'):
                                istaMesh = cmd.listConnections(mesh)
                                allMeshIstance.append(istaMesh[0])

            exportMashIstances = scenefilename.replace('.mb', '_mashIstances.ma')

            cmd.select(allMeshIstance, replace=True)
            exportObj = cmd.ls(selection=True)

            # unparent
            cmd.parent(exportObj, world=True)
            if cmd.objExists('CTRL_set'):
                cmd.delete('CTRL_set')

            # switchShader
            cmd.select(exportObj, replace=True)
            cmd.hyperShade(smn=True)
            aniShader = cmd.ls(selection=True)
            rndShader = aniShader[0].replace('ani', 'rnd')

            if cmd.objExists(rndShader):
                shaderConnections = cmd.listConnections(aniShader, d=True, et=True, t='shadingEngine')
                shadingGroup = shaderConnections[0]
                cmd.connectAttr('{}.outColor'.format(rndShader), '{}.surfaceShader'.format(shadingGroup), force=True)

            cmd.select(exportObj, replace=True)
            cmd.file(exportMashIstances, force=True, typ="mayaAscii", exportSelected=True, preserveReferences=True, shader=True, expressions=True, constructionHistory=True)

    ############################################################################
    # @brief      { function_description }
    #
    def exportAlembicInfo(self):
        alembicData = {}
        alembicNodes = cmd.ls(type='AlembicNode')
        for alembicNode in alembicNodes:
            alembicDict = {'node': alembicNode, 'path': cmd.getAttr('{}.abc_File'.format(alembicNode)), 'objects': []}
            for mesh in set(cmd.listConnections(alembicNode, destination=True, source=False)):
                if cmd.nodeType(mesh) == 'transform':
                    shape = cmd.listRelatives(mesh)[0]
                    alembicDict['objects'].append({'transform': mesh, 'shape': shape})
            alembicData[alembicNode] = alembicDict

        jsonPath = os.path.join(self.savedAsset.savedAssetFolder, '{}_alembic.json'.format(self.savedAsset.sceneName)).replace('\\', '/')
        if alembicData:
            api.json.json_write(alembicData, jsonPath)
