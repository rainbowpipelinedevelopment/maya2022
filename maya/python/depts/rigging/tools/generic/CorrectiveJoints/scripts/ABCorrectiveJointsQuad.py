# -*- coding: utf-8 -*-

#author: Michela Cerrone

#this script works to make support joints on body and fingers, this help the deformation to be more realistic.


import maya.cmds as cmds
import maya.mel as mel
import maya.utils
from maya import OpenMayaUI as omui
import re
import os
from functools import partial

# -----------------------------------------------------------------
# Path
# -----------------------------------------------------------------
localpipe = os.getenv("LOCALPY")
myPath = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "CorrectiveJoints")
image_path = os.path.join(myPath, "icons")


# -----------------------------------------------------------------
# PySide
# -----------------------------------------------------------------
try:
    from shiboken2 import wrapInstance
except ImportError:
    from shiboken import wrapInstance

try:
    from PySide2.QtGui import QIcon
    from PySide2.QtWidgets import QWidget
except ImportError:
    from PySide.QtGui import QIcon, QWidget
    


def create_vector_product(input1_attr, operation, input2_attr, output_attr):
    vector_product_node = cmds.createNode('vectorProduct')
    cmds.setAttr(vector_product_node + '.operation', operation)

    # Sostituisci gli underscore con 'X' nel nome lungo
    long_name = output_attr.replace('_', 'X')

    # Rimuovi caratteri non validi nel nome lungo
    long_name = ''.join(char if char.isalnum() or char in ('_', ' ') else '' for char in long_name)

    # Collega gli attributi di input
    cmds.connectAttr(input1_attr, '{}.input1'.format(vector_product_node), force=True)
    cmds.connectAttr(input2_attr, '{}.input2'.format(vector_product_node), force=True)

    # Crea l'attributo di output personalizzato se non esiste
    if not cmds.attributeQuery(long_name, node=vector_product_node, exists=True):
        cmds.addAttr(vector_product_node, longName=long_name, attributeType='float3', keyable=True)
        cmds.addAttr(vector_product_node, longName=long_name + 'X', attributeType='float', parent=long_name, keyable=True)
        cmds.addAttr(vector_product_node, longName=long_name + 'Y', attributeType='float', parent=long_name, keyable=True)
        cmds.addAttr(vector_product_node, longName=long_name + 'Z', attributeType='float', parent=long_name, keyable=True)


    return vector_product_node


def create_multiply_divide(node_name, operation):
    # Crea il nodo multiplyDivide
    multiply_divide_node = cmds.createNode('multiplyDivide', name=node_name)

    # Imposta l'operazione del nodo multiplyDivide
    cmds.setAttr(multiply_divide_node + '.operation', operation)

    return multiply_divide_node
    
    
def create_condition(node_name, operation):
    # Crea il nodo condition
    condition_node = cmds.createNode('condition', name=node_name)

    # Imposta l'operazione del nodo condition
    cmds.setAttr(condition_node + '.operation', operation)

    return condition_node

def process_joint(joint_name, operations):
    if not cmds.objExists(joint_name) or cmds.nodeType(joint_name) != "joint":
        print("Il joint {} non esiste o non è un joint.".format(joint_name))
        return
    
    for operation in operations:
        operation(joint_name)



def duplicate_and_move_translation_locator(locator_name):
    translation_locator_02 = cmds.duplicate(locator_name)[0]
    translation_locator_name = cmds.rename(translation_locator_02, locator_name.replace("_Corr_Position_loc", "_Corr_Rotation_loc"))
    
    joint_rotation_translate = cmds.xform(translation_locator_name, query=True, translation=True, worldSpace=True)
    joint_rotation_rotate = cmds.xform(translation_locator_name, query=True, rotation=True, worldSpace=True)
    
    cmds.xform(translation_locator_name, translation=joint_rotation_translate, worldSpace=True)
    cmds.xform(translation_locator_name, rotation=joint_rotation_rotate, worldSpace=True)
    
    cmds.move(2.5, 0, 0, translation_locator_name, relative=True, objectSpace=True)
    cmds.parent(translation_locator_name, locator_name)
    
    print("Creato locator di traslazione per {}: {}".format(locator_name, translation_locator_name))

    return translation_locator_name


def create_and_match_joint(locator_name):
    joint_tmp = cmds.joint()

    # Rinomina il joint come il locator con alcune modifiche
    joint_name = cmds.rename(joint_tmp, locator_name.replace("_Corr_Position_loc", "_Corr_Driver_jnt"))

    # Match delle trasformate con il locator
    cmds.matchTransform(joint_name, locator_name, pos=True, rot=True)

    # Ottieni posizione e orientamento del joint
    joint_translate = cmds.xform(joint_name, query=True, translation=True, worldSpace=True)
    joint_rotate = cmds.xform(joint_name, query=True, rotation=True, worldSpace=True)

    # Sposta il joint in base all'orientamento
    cmds.move(-1.5, 0, 0, joint_name, relative=True, objectSpace=True)

    # Congela le rotazioni del joint
    cmds.makeIdentity(joint_name, apply=True, rotate=True)

    print("Creato joint per {}: {}".format(locator_name, joint_name))

    return joint_name, joint_translate

def create_controls(joint_name, rotation_joint_name, locator_name, global_father_name):
    control_tmp = cmds.circle(name=rotation_joint_name.replace("_Corr_Weight_jnt", "_Corr_Ctrl"), radius=7)[0]
    replace_sh(control_tmp)
    control_father = cmds.group(em=True)
    control_father_name = cmds.rename(control_father, control_tmp.replace("_Corr_Ctrl", "_Corr_Ctrl_Grp"))
    cmds.matchTransform(control_father_name, control_tmp)
    cmds.parent(control_tmp, control_father_name)
    control_father_offset = cmds.group(em=True)
    control_father_name_offset = cmds.rename(control_father_offset, control_tmp.replace("_Corr_Ctrl", "_Corr_Ctrl_Offset_Grp"))
    cmds.matchTransform(control_father_name_offset, control_tmp)
    cmds.parent(control_father_name, control_father_name_offset)
    cmds.DeleteHistory(control_tmp)
    cmds.matchTransform(control_father_name_offset, rotation_joint_name)
    cmds.parentConstraint(control_tmp, rotation_joint_name, maintainOffset=True)
    cmds.scaleConstraint(control_tmp, rotation_joint_name, maintainOffset=True)
    
    control_one = cmds.duplicate(control_tmp, name=rotation_joint_name.replace("_Corr_Weight_jnt", "_Corr_Driver_Ctrl"))
    cmds.parent(control_one, world=True)
    control_one_father_name = cmds.group(em=True, name=rotation_joint_name.replace("_Corr_Weight_jnt", "_Corr_Driver_Ctrl_Grp"))
    cmds.matchTransform(control_one_father_name, control_one)
    cmds.parent(control_one, control_one_father_name)
    control_one_father_offset = cmds.group(em=True)
    control_one_father_name_offset = cmds.rename(control_one_father_offset, rotation_joint_name.replace("_Corr_Weight_jnt", "_Corr_Driver_Ctrl_Offset_Grp"))
    cmds.matchTransform(control_one_father_name_offset, control_one)
    cmds.parent(control_one_father_name, control_one_father_name_offset)
    cmds.DeleteHistory(control_one)
    cmds.matchTransform(control_one_father_name_offset, joint_name)
    cmds.parentConstraint(control_one, joint_name, maintainOffset=True)
    cmds.scaleConstraint(control_one, joint_name, maintainOffset=True)
    cmds.parent(control_father_name_offset, control_one)
    cmds.parent(control_one_father_name_offset, global_father_name)
    control_one_shape = cmds.listRelatives(control_one, shapes=True, fullPath=True)
    cmds.setAttr('{}.visibility'.format(control_one_shape[0]), 0)
    cmds.setAttr('{}.visibility'.format(control_tmp), lock=True, keyable=False)

    return control_tmp, control_father_name, control_one_father_name
    
    
def parent_joint_fn(joint_name, joint_rotation_translate, rotation_joint_name, locator_name):
    cmds.parent(joint_name, world=True, relative=False)
    cmds.parent(rotation_joint_name, joint_name) 
    joint_father = cmds.group(em=True)
    joint_father_name = cmds.rename(joint_father, locator_name.replace("_Corr_Position_loc", "_Corr_Joints_Grp"))
    cmds.matchTransform(joint_father_name, joint_name)
    cmds.parent(joint_name, joint_father_name)
    global_father = cmds.group(em=True)
    global_father_name = cmds.rename(global_father, locator_name.replace("_Corr_Position_loc", "_Corr_Grp"))
    locator_father = cmds.group(em=True)
    locator_father_name = cmds.rename(locator_father, locator_name.replace("_Corr_Position_loc","_Corr_Locator_Grp"))
    cmds.matchTransform(locator_father_name, locator_name)
    cmds.parent(locator_name, locator_father_name)
    cmds.parent(joint_father_name, global_father_name)
    cmds.parent(locator_father_name, global_father_name)
    
    a, b, control_one_father_name = create_controls(joint_name, rotation_joint_name, locator_name, global_father_name)
    
    print(joint_name)
    print(joint_father_name)
    
    return global_father_name, control_one_father_name
    
def duplicate_and_match_rotation_joint(joint_name, locator_name, joint_rotation_translate):
    # Duplica il joint precedentemente creato
    print(joint_name)
    rotation_joint = cmds.duplicate(joint_name)[0]
    print(rotation_joint)

    # Match delle trasformate con il locator di posizione
    cmds.matchTransform(rotation_joint, locator_name, pos=True, rot=True)

    # Rinomina il secondo joint aggiungendo un suffisso diverso
    rotation_joint_name = cmds.rename(rotation_joint, joint_name.replace("_Corr_Driver_jnt", "_Corr_Weight_jnt"))
    print(rotation_joint_name)

    # Congela solo le rotazioni del joint
    cmds.makeIdentity(rotation_joint_name, apply=True, rotate=True)

    print("Creato joint di rotazione per {}: {}".format(joint_name, rotation_joint_name))
    
    a, control_one_father_name = parent_joint_fn(joint_name, joint_rotation_translate, rotation_joint_name, locator_name)
    
    return rotation_joint_name, joint_name, control_one_father_name
    
def create_locator(joint_name):
    locator_suffix = "_Corr_Position_loc"
    
    locator_tmp = cmds.spaceLocator()[0]
    
    joint_translate = cmds.xform(joint_name, query=True, translation=True, worldSpace=True)
    joint_rotate = cmds.xform(joint_name, query=True, rotation=True, worldSpace=True)
    
    cmds.xform(locator_tmp, translation=joint_translate, worldSpace=True)
    cmds.xform(locator_tmp, rotation=joint_rotate, worldSpace=True)
    
    # Rimuovi il prefisso "LOC_" dal nome del locator
    locator_name = cmds.rename(locator_tmp, joint_name.replace("_Corr", locator_suffix).replace("LOC_", ""))
    print("Creato locator per {}: {}".format(joint_name, locator_name))

    translation_locator_name = duplicate_and_move_translation_locator(locator_name)

    joint_created, joint_rotation_translate = create_and_match_joint(locator_name)

    # Crea un altro joint duplicandolo dal precedente
    a, b, control_one_father_name = duplicate_and_match_rotation_joint(joint_created, locator_name, joint_rotation_translate)
    
    return locator_name, translation_locator_name, joint_created, control_one_father_name

 
def save_sel():
    return cmds.ls(selection=True)

def replace_sh(control_tmp):
    # Verifica se gli oggetti esistono prima di selezionarli
    if not cmds.objExists("Arrow_Curve") or not cmds.objExists(control_tmp):
        print("Uno o entrambi gli oggetti non esistono.")
        return

    for control_name in control_tmp:
        cmds.select("Arrow_Curve", control_tmp)
        sel = save_sel()

        for i in range(len(sel)-1):
            cnt = cmds.duplicate(sel[0])[0]  # Aggiunto [0] per ottenere il primo elemento della lista
            oldcnt = sel[i+1]

            if cmds.objExists(cnt):
                child = cmds.listRelatives(cnt, c=True, pa=True)
                child_shp = cmds.listRelatives(cnt, shapes=True, pa=True)

                for i in child:
                    if i not in child_shp:
                        cmds.delete(i)

                cnt = cmds.rename(cnt, "NEW")
                newshape = cmds.listRelatives(cnt, shapes=True, pa=True)
                oldshape = cmds.listRelatives(oldcnt, shapes=True, pa=True)

                for a in oldshape:
                    cmds.delete(a)

                if newshape and oldshape:
                    newname = str(cmds.rename(newshape[0], str(oldshape[0])))
                    cmds.select(newname, oldcnt)
                    mel.eval("parent -r -s;")
                    
    delete_new_objects()
                
    return
    
def delete_new_objects():
    new_objects = cmds.ls("NEW*")
    if new_objects:
        cmds.delete(new_objects)
        print("Eliminati gli oggetti 'NEW' dalla scena.")
    else:
        print("Nessun oggetto 'NEW' trovato nella scena.")

def integrate_function(locator_name, translation_locator_name, joint_name, control_one_father_name):
    
    print("Locator Name:", locator_name)
    print("Translation Locator Name:", translation_locator_name)

    # Verifica la presenza dell'oggetto
    if not cmds.objExists(locator_name):
        print("Error: Locator '{}' does not exist.".format(locator_name))
        return

    # Verifica la presenza dell'attributo translate sull'oggetto
    if not cmds.attributeQuery('translate', node=locator_name, exists=True):
        print("Error: Attribute 'translate' not found on locator '{}'.".format(locator_name))
        return

    # Crea i nodi Vector Product
    position_vp = create_vector_product('{}.translate'.format(locator_name), 1, '{}.translate'.format(translation_locator_name), '{}.output'.format('VP_{}_PositionRotation'.format(locator_name)))
    rotation_vp = create_vector_product('{}.translate'.format(translation_locator_name), 1, '{}.translate'.format(translation_locator_name), '{}.output'.format('VP_{}_Rotation'.format(translation_locator_name)))

    # Collega i nodi Vector Product ai locator
    cmds.connectAttr(locator_name + '.translate', position_vp + '.input1', force=True)
    cmds.connectAttr(translation_locator_name + '.translate', position_vp + '.input2', force=True)

    cmds.connectAttr(translation_locator_name + '.translate', rotation_vp + '.input1', force=True)
    cmds.connectAttr(translation_locator_name + '.translate', rotation_vp + '.input2', force=True)

    # Disconnetti la translate del locator dall'input 1 del rotation_vp
    cmds.disconnectAttr(translation_locator_name + '.translate', rotation_vp + '.input1')

    # Copia i valori numerici dall'input 2 all'input 1 del rotation_Vp
    translation_values = cmds.getAttr(translation_locator_name + '.translate')[0]
    cmds.setAttr(rotation_vp + '.input1', translation_values[0], translation_values[1], translation_values[2])

    # Crea i nodi MultiplyDivide
    rotation_divide_node = create_multiply_divide('MLD_{}_Rotation_Divide'.format(locator_name), 2)  # 2 corrisponde a divide
    rotation_multiply_node = create_multiply_divide('MLD_{}_Rotation_Multiply'.format(locator_name), 1)  # 1 corrisponde a multiply

    # Collega i nodi MultiplyDivide
    cmds.connectAttr(position_vp + '.output', rotation_divide_node + '.input1', force=True)
    cmds.connectAttr(rotation_vp + '.output', rotation_divide_node + '.input2', force=True)

    # Rinomina il primo MultiplyDivide
    cmds.rename(rotation_divide_node, 'MLD_{}_Rotation_Divide'.format(locator_name))

    # Collega il secondo MultiplyDivide
    cmds.setAttr(rotation_multiply_node + '.input1X', 4)
    cmds.setAttr(rotation_multiply_node + '.input1Y', 0)
    cmds.setAttr(rotation_multiply_node + '.input1Z', 4)

    # Imposta il secondo MultiplyDivide su multiply
    cmds.setAttr(rotation_multiply_node + '.operation', 1)  # 1 corrisponde a multiply

    # Collega l'output del primo MultiplyDivide al secondo
    cmds.connectAttr(rotation_divide_node + '.output', rotation_multiply_node + '.input2', force=True)

    # Crea il joint solo se non esiste già uno con lo stesso nome
    if not cmds.objExists(joint_name):
        joint_name, joint_rotation_translate = create_and_match_joint(locator_name)

    # Crea i nodi Condition
    condition_x = create_condition('CND_{}_Translate_X'.format(locator_name), 2)  # 2 corrisponde a less than
    condition_y = create_condition('CND_{}_Translate_Y'.format(locator_name), 2)  # 2 corrisponde a less than
    condition_z = create_condition('CND_{}_Translate_Z'.format(locator_name), 2)  # 2 corrisponde a less than

    # Collegamento dei nodi MultiplyDivide ai nodi Condition
    cmds.connectAttr(rotation_multiply_node + '.outputX', condition_x + '.firstTerm', force=True)
    cmds.connectAttr(rotation_multiply_node + '.outputY', condition_y + '.firstTerm', force=True)
    cmds.connectAttr(rotation_multiply_node + '.outputZ', condition_z + '.firstTerm', force=True)

    # Collegamento degli output X, Y, Z ai colorIfFalseR dei nodi Condition
    cmds.connectAttr(rotation_multiply_node + '.outputX', condition_x + '.colorIfFalseR', force=True)
    cmds.connectAttr(rotation_multiply_node + '.outputY', condition_y + '.colorIfFalseR', force=True)
    cmds.connectAttr(rotation_multiply_node + '.outputZ', condition_z + '.colorIfFalseR', force=True)

    # Crea i nodi Condition aggiuntivi con suffisso _Zero
    condition_x_zero = create_condition('CND_{}_Translate_X_Zero'.format(locator_name), 4)  # 2 corrisponde a less than
    condition_y_zero = create_condition('CND_{}_Translate_Y_Zero'.format(locator_name), 4)  # 2 corrisponde a less than
    condition_z_zero = create_condition('CND_{}_Translate_Z_Zero'.format(locator_name), 4)  # 2 corrisponde a less than

    # Collegamento degli output X, Y, Z ai firstTerm dei nuovi Condition
    cmds.connectAttr(condition_x + '.outColorR', condition_x_zero + '.firstTerm', force=True)
    cmds.connectAttr(condition_y + '.outColorR', condition_y_zero + '.firstTerm', force=True)
    cmds.connectAttr(condition_z + '.outColorR', condition_z_zero + '.firstTerm', force=True)

    # Collegamento degli output X, Y, Z ai colorIfFalseR dei nuovi Condition
    cmds.connectAttr(condition_x + '.outColorR', condition_x_zero + '.colorIfFalseR', force=True)
    cmds.connectAttr(condition_y + '.outColorR', condition_y_zero + '.colorIfFalseR', force=True)
    cmds.connectAttr(condition_z + '.outColorR', condition_z_zero + '.colorIfFalseR', force=True)

    cmds.sets(condition_x, condition_y, condition_z, condition_x_zero, condition_z_zero, condition_y_zero,
              rotation_multiply_node, name='SET_{}_SliderNODE'.format(locator_name))

    # Colleghiamo le condizioni zero ai canali di traslazione del joint creato
    cmds.connectAttr(condition_x_zero + '.outColorR', '{}.translateX'.format(control_one_father_name), force=True)
    cmds.connectAttr(condition_y_zero + '.outColorR', '{}.translateY'.format(control_one_father_name), force=True)
    cmds.connectAttr(condition_z_zero + '.outColorR', '{}.translateZ'.format(control_one_father_name), force=True)


def process(joint_list, *args):
    print("Processing joints...")
    joint_list = ["LOC_L_legFront_Knee_Ext_Corr", "LOC_L_legFront_Knee_Int_Corr", "LOC_L_legFront_Ankle_Ext_Corr",
                  "LOC_Neck_Low_Corr", "LOC_Neck_Up_Corr", "LOC_L_Neck_Corr", "LOC_L_legFront_Up_Int_Corr",
                  "LOC_L_legFront_Up_Ext_Corr", "LOC_L_legFront_Ankle_Int_Corr", "LOC_L_legRear_Knee_Ext_Corr",
                  "LOC_L_legRear_Ankle_Int_Corr", "LOC_L_legRear_Up_Ext_Corr", "LOC_L_legRear_Knee_Int_Corr",
                  "LOC_L_legRear_Ankle_Ext_Corr", "LOC_L_legRear_Up_Int_Corr", "LOC_R_legFront_Knee_Ext_Corr",
                  "LOC_R_legFront_Knee_Int_Corr", "LOC_R_legFront_Ankle_Ext_Corr", "LOC_R_Neck_Corr",
                  "LOC_R_legFront_Up_Int_Corr", "LOC_R_legFront_Up_Ext_Corr", "LOC_R_legFront_Ankle_Int_Corr",
                  "LOC_R_legRear_Knee_Int_Corr", "LOC_R_legRear_Knee_Ext_Corr", "LOC_R_legRear_Up_Ext_Corr",
                  "LOC_R_legRear_Ankle_Ext_Corr", "LOC_R_legRear_Ankle_Int_Corr", "LOC_R_legRear_Up_Int_Corr"]

    for joint_name in joint_list:
        print(f"Processing joint: {joint_name}")
        if not cmds.objExists(joint_name):
            print(f"{joint_name} non esiste, procedo con il successivo.")
            continue

        locator_name, translation_locator_name, joint_created, control_one_father_name = create_locator(joint_name)
        if locator_name and translation_locator_name and control_one_father_name:
            integrate_function(locator_name, translation_locator_name, joint_name, control_one_father_name)
        else:
            print(f"Non è stato possibile creare i locatori per {joint_name}")

#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    
    

def DoubleParentMatrix(*args):
 
    def create_offset_matrix_attributes(obj, target_name, suffix):
        attr1_name = "{}_{}_OffsetMatrix_01".format(target_name, suffix)
        attr2_name = "{}_{}_OffsetMatrix_02".format(target_name, suffix)
    
        if cmds.attributeQuery(attr1_name, node=obj, exists=True) or cmds.attributeQuery(attr2_name, node=obj, exists=True):
            cmds.warning("Gli attributi {} e/o {} esistono già.".format(attr1_name, attr2_name))
            return attr1_name, attr2_name
    
        cmds.addAttr(obj, longName=attr1_name, attributeType='matrix', usedAsColor=False, keyable=False)
        cmds.addAttr(obj, longName=attr2_name, attributeType='matrix', usedAsColor=False, keyable=False)
    
        return attr1_name, attr2_name
    
    def calculate_offset_matrix(source_obj, target_obj, result_obj, attr1_name, attr2_name, suffix):
        mult_matrix_node_1 = cmds.createNode('multMatrix', name="{}_{}_Delete1".format(source_obj, suffix))
        mult_matrix_node_2 = cmds.createNode('multMatrix', name="{}_{}_Delete2".format(target_obj, suffix))
    
        cmds.connectAttr("{}.worldMatrix[0]".format(target_obj), "{}.matrixIn[0]".format(mult_matrix_node_1))
        cmds.connectAttr("{}.worldInverseMatrix[0]".format(source_obj), "{}.matrixIn[1]".format(mult_matrix_node_1))
        cmds.connectAttr("{}.matrixSum".format(mult_matrix_node_1), "{}.{}".format(result_obj, attr1_name))
    
        cmds.connectAttr("{}.worldMatrix[0]".format(target_obj), "{}.matrixIn[0]".format(mult_matrix_node_2))
        cmds.connectAttr("{}.worldInverseMatrix[0]".format(selected_objects[1]), "{}.matrixIn[1]".format(mult_matrix_node_2))
        cmds.connectAttr("{}.matrixSum".format(mult_matrix_node_2), "{}.{}".format(result_obj, attr2_name))
        
        cmds.disconnectAttr("{}.matrixSum".format(mult_matrix_node_1), "{}.{}".format(result_object, attr1_name))
        cmds.disconnectAttr("{}.matrixSum".format(mult_matrix_node_2), "{}.{}".format(result_object, attr2_name))
        
        cmds.delete(mult_matrix_node_1)
        cmds.delete(mult_matrix_node_2)
        
        mult_matrix_node_3 = cmds.createNode('multMatrix', name="MM_{}_OffsetMatrix".format(selected_objects[0]))
        mult_matrix_node_4 = cmds.createNode('multMatrix', name="MM_{}_OffsetMatrix".format(selected_objects[1]))
    
        cmds.connectAttr("{}.{}".format(result_object, attr1_name), "{}.matrixIn[0]".format(mult_matrix_node_3))
        cmds.connectAttr("{}.worldMatrix[0]".format(selected_objects[0]), "{}.matrixIn[1]".format(mult_matrix_node_3))
        cmds.connectAttr("{}.parentInverseMatrix[0]".format(selected_objects[2]), "{}.matrixIn[2]".format(mult_matrix_node_3))
        
        cmds.connectAttr("{}.{}".format(result_object, attr2_name), "{}.matrixIn[0]".format(mult_matrix_node_4))
        cmds.connectAttr("{}.worldMatrix[0]".format(selected_objects[1]), "{}.matrixIn[1]".format(mult_matrix_node_4))
        cmds.connectAttr("{}.parentInverseMatrix[0]".format(selected_objects[2]), "{}.matrixIn[2]".format(mult_matrix_node_4))
        
        decompose_matrix_node_1 = cmds.createNode('decomposeMatrix', name="DM_{}_OffsetMatrix".format(selected_objects[0]))
        decompose_matrix_node_2 = cmds.createNode('decomposeMatrix', name="DM_{}_OffsetMatrix".format(selected_objects[1]))
        
        cmds.connectAttr("{}.matrixSum".format(mult_matrix_node_3), "{}.inputMatrix".format(decompose_matrix_node_1))
        
        cmds.connectAttr("{}.matrixSum".format(mult_matrix_node_4), "{}.inputMatrix".format(decompose_matrix_node_2))
        
        multiply_divide_node_1 = cmds.createNode('multiplyDivide', name="MLD_{}_Position".format(selected_objects[0]))
        multiply_divide_node_2 = cmds.createNode('multiplyDivide', name="MLD_{}_Position".format(selected_objects[1]))
        
        cmds.setAttr("{}.input2".format(multiply_divide_node_1), 0.5, 0.5, 0.5)
        cmds.setAttr("{}.input2".format(multiply_divide_node_2), 0.5, 0.5, 0.5)
        
        cmds.connectAttr("{}.outputTranslate".format(decompose_matrix_node_1), "{}.input1".format(multiply_divide_node_1))
        
        cmds.connectAttr("{}.outputTranslate".format(decompose_matrix_node_2), "{}.input1".format(multiply_divide_node_2))
        
        plus_minus_node = cmds.createNode('plusMinusAverage', name="PMA_Position")
        
        cmds.connectAttr("{}.output".format(multiply_divide_node_1), "{}.input3D[0]".format(plus_minus_node))
        cmds.connectAttr("{}.output".format(multiply_divide_node_2), "{}.input3D[1]".format(plus_minus_node))
        
        cmds.connectAttr("{}.output3D".format(plus_minus_node), "{}.translate".format(selected_objects[2]))
    
    
    selected_objects = cmds.ls(selection=True, type='transform')
    
    if len(selected_objects) == 4:
        source_object = selected_objects[0]
        target_object = selected_objects[2]
        result_object = selected_objects[3]
        suffix = "Offset"
    
        attr1_name, attr2_name = create_offset_matrix_attributes(result_object, target_object, suffix)
    
        calculate_offset_matrix(source_object, target_object, result_object, attr1_name, attr2_name, suffix)
    
        print("Attributi di offset matrix creati su {} basati su {} e {}. MultMatrix creati e collegati agli attributi di offset.".format(result_object, source_object, target_object))
    else:
        cmds.warning("Seleziona esattamente quattro oggetti prima di eseguire lo script.")
        
        
    return
    
#BODY_FINALIZE____________________________________________________________________________________________________________________________________________________________________________________________________________

def finalize(*args):
    def check_and_apply_double_parent_matrix(selection):
        if all(cmds.objExists(obj) for obj in selection):
            cmds.select(selection)
            DoubleParentMatrix(selection)

    # Definisci le liste di oggetti da selezionare
    neck_low_position = ["neck_Ctrl", "spineHigh_Ctrl", "Neck_Low_Corr_Position_loc", "Neck_Low_Corr_Locator_Grp"]
    neck_low_rotation = ["neck_Ctrl", "spineHigh_Ctrl", "Neck_Low_Corr_Rotation_loc", "Neck_Low_Corr_Locator_Grp"]
    neck_up_position = ["neck_Ctrl", "spineHigh_Ctrl", "Neck_Up_Corr_Position_loc", "Neck_Up_Corr_Locator_Grp"]
    neck_up_rotation = ["neck_Ctrl", "spineHigh_Ctrl", "Neck_Up_Corr_Rotation_loc", "Neck_Up_Corr_Locator_Grp"]
    l_neck_position = ["neck_Ctrl", "spineHigh_Ctrl", "L_Neck_Corr_Position_loc", "L_Neck_Corr_Locator_Grp"]
    l_neck_rotation = ["neck_Ctrl", "spineHigh_Ctrl", "L_Neck_Corr_Rotation_loc", "L_Neck_Corr_Locator_Grp"]
    r_neck_position = ["neck_Ctrl", "spineHigh_Ctrl", "R_Neck_Corr_Position_loc", "R_Neck_Corr_Locator_Grp"]
    r_neck_rotation = ["neck_Ctrl", "spineHigh_Ctrl", "R_Neck_Corr_Rotation_loc", "R_Neck_Corr_Locator_Grp"]

    # Sinistro (L_)

    l_legFront_Up_Int_position = ["L_front_Upleg_jnt", "L_front_hip1_bendy_1_jnt",
                                  "L_legFront_Up_Int_Corr_Position_loc", "L_legFront_Up_Int_Corr_Locator_Grp"]
    l_legFront_Up_Int_rotation = ["L_front_Upleg_jnt", "L_front_hip1_bendy_1_jnt",
                                  "L_legFront_Up_Int_Corr_Rotation_loc", "L_legFront_Up_Int_Corr_Locator_Grp"]
    l_legFront_Up_Ext_position = ["L_front_Upleg_jnt", "L_front_hip1_bendy_1_jnt",
                                  "L_legFront_Up_Ext_Corr_Position_loc", "L_legFront_Up_Ext_Corr_Locator_Grp"]
    l_legFront_Up_Ext_rotation = ["L_front_Upleg_jnt", "L_front_hip1_bendy_1_jnt",
                                  "L_legFront_Up_Ext_Corr_Rotation_loc", "L_legFront_Up_Ext_Corr_Locator_Grp"]
    l_legFront_Knee_Ext_position = ["L_front_hip1_bendy_5_jnt", "L_front_hip2_bendy_1_jnt",
                                    "L_legFront_Knee_Ext_Corr_Position_loc", "L_legFront_Knee_Ext_Corr_Locator_Grp"]
    l_legFront_Knee_Ext_rotation = ["L_front_hip1_bendy_5_jnt", "L_front_hip2_bendy_1_jnt",
                                    "L_legFront_Knee_Ext_Corr_Rotation_loc", "L_legFront_Knee_Ext_Corr_Locator_Grp"]
    l_legFront_Knee_Int_position = ["L_front_hip1_bendy_5_jnt", "L_front_hip2_bendy_1_jnt",
                                    "L_legFront_Knee_Int_Corr_Position_loc", "L_legFront_Knee_Int_Corr_Locator_Grp"]
    l_legFront_Knee_Int_rotation = ["L_front_hip1_bendy_5_jnt", "L_front_hip2_bendy_1_jnt",
                                    "L_legFront_Knee_Int_Corr_Rotation_loc", "L_legFront_Knee_Int_Corr_Locator_Grp"]
    l_legFront_Ankle_Ext_position = ["L_front_knee1_jnt", "L_front_ankle_jnt", "L_legFront_Ankle_Ext_Corr_Position_loc",
                                     "L_legFront_Ankle_Ext_Corr_Locator_Grp"]
    l_legFront_Ankle_Ext_rotation = ["L_front_knee1_jnt", "L_front_ankle_jnt", "L_legFront_Ankle_Ext_Corr_Rotation_loc",
                                     "L_legFront_Ankle_Ext_Corr_Locator_Grp"]
    l_legFront_Ankle_Int_position = ["L_front_knee1_jnt", "L_front_ankle_jnt", "L_legFront_Ankle_Int_Corr_Position_loc",
                                     "L_legFront_Ankle_Int_Corr_Locator_Grp"]
    l_legFront_Ankle_Int_rotation = ["L_front_knee1_jnt", "L_front_ankle_jnt", "L_legFront_Ankle_Int_Corr_Rotation_loc",
                                     "L_legFront_Ankle_Int_Corr_Locator_Grp"]

    l_legRear_Up_Int_position = ["L_rear_Upleg_jnt", "L_rear_hip1_bendy_1_jnt", "L_legRear_Up_Int_Corr_Position_loc",
                                 "L_legRear_Up_Int_Corr_Locator_Grp"]
    l_legRear_Up_Int_rotation = ["L_rear_Upleg_jnt", "L_rear_hip1_bendy_1_jnt", "L_legRear_Up_Int_Corr_Rotation_loc",
                                 "L_legRear_Up_Int_Corr_Locator_Grp"]
    l_legRear_Up_Ext_position = ["L_rear_Upleg_jnt", "L_rear_hip1_bendy_1_jnt", "L_legRear_Up_Ext_Corr_Position_loc",
                                 "L_legRear_Up_Ext_Corr_Locator_Grp"]
    l_legRear_Up_Ext_rotation = ["L_rear_Upleg_jnt", "L_rear_hip1_bendy_1_jnt", "L_legRear_Up_Ext_Corr_Rotation_loc",
                                 "L_legRear_Up_Ext_Corr_Locator_Grp"]
    l_legRear_Knee_Ext_position = ["L_rear_hip1_bendy_5_jnt", "L_rear_hip2_bendy_1_jnt",
                                   "L_legRear_Knee_Ext_Corr_Position_loc", "L_legRear_Knee_Ext_Corr_Locator_Grp"]
    l_legRear_Knee_Ext_rotation = ["L_rear_hip1_bendy_5_jnt", "L_rear_hip2_bendy_1_jnt",
                                   "L_legRear_Knee_Ext_Corr_Rotation_loc", "L_legRear_Knee_Ext_Corr_Locator_Grp"]
    l_legRear_Knee_Int_position = ["L_rear_hip1_bendy_5_jnt", "L_rear_hip2_bendy_1_jnt",
                                   "L_legRear_Knee_Int_Corr_Position_loc", "L_legRear_Knee_Int_Corr_Locator_Grp"]
    l_legRear_Knee_Int_rotation = ["L_rear_hip1_bendy_5_jnt", "L_rear_hip2_bendy_1_jnt",
                                   "L_legRear_Knee_Int_Corr_Rotation_loc", "L_legRear_Knee_Int_Corr_Locator_Grp"]
    l_legRear_Ankle_Ext_position = ["L_rear_knee1_jnt", "L_rear_ankle_jnt", "L_legRear_Ankle_Ext_Corr_Position_loc",
                                    "L_legRear_Ankle_Ext_Corr_Locator_Grp"]
    l_legRear_Ankle_Ext_rotation = ["L_rear_knee1_jnt", "L_rear_ankle_jnt", "L_legRear_Ankle_Ext_Corr_Rotation_loc",
                                    "L_legRear_Ankle_Ext_Corr_Locator_Grp"]
    l_legRear_Ankle_Int_position = ["L_rear_knee1_jnt", "L_rear_ankle_jnt", "L_legRear_Ankle_Int_Corr_Position_loc",
                                    "L_legRear_Ankle_Int_Corr_Locator_Grp"]
    l_legRear_Ankle_Int_rotation = ["L_rear_knee1_jnt", "L_rear_ankle_jnt", "L_legRear_Ankle_Int_Corr_Rotation_loc",
                                    "L_legRear_Ankle_Int_Corr_Locator_Grp"]

    # Destro (R_)

    r_legFront_Up_Int_position = ["R_front_Upleg_jnt", "R_front_hip1_bendy_1_jnt",
                                  "R_legFront_Up_Int_Corr_Position_loc", "R_legFront_Up_Int_Corr_Locator_Grp"]
    r_legFront_Up_Int_rotation = ["R_front_Upleg_jnt", "R_front_hip1_bendy_1_jnt",
                                  "R_legFront_Up_Int_Corr_Rotation_loc", "R_legFront_Up_Int_Corr_Locator_Grp"]
    r_legFront_Up_Ext_position = ["R_front_Upleg_jnt", "R_front_hip1_bendy_1_jnt",
                                  "R_legFront_Up_Ext_Corr_Position_loc", "R_legFront_Up_Ext_Corr_Locator_Grp"]
    r_legFront_Up_Ext_rotation = ["R_front_Upleg_jnt", "R_front_hip1_bendy_1_jnt",
                                  "R_legFront_Up_Ext_Corr_Rotation_loc", "R_legFront_Up_Ext_Corr_Locator_Grp"]
    r_legFront_Knee_Ext_position = ["R_front_hip1_bendy_5_jnt", "R_front_hip2_bendy_1_jnt",
                                    "R_legFront_Knee_Ext_Corr_Position_loc", "R_legFront_Knee_Ext_Corr_Locator_Grp"]
    r_legFront_Knee_Ext_rotation = ["R_front_hip1_bendy_5_jnt", "R_front_hip2_bendy_1_jnt",
                                    "R_legFront_Knee_Ext_Corr_Rotation_loc", "R_legFront_Knee_Ext_Corr_Locator_Grp"]
    r_legFront_Knee_Int_position = ["R_front_hip1_bendy_5_jnt", "R_front_hip2_bendy_1_jnt",
                                    "R_legFront_Knee_Int_Corr_Position_loc", "R_legFront_Knee_Int_Corr_Locator_Grp"]
    r_legFront_Knee_Int_rotation = ["R_front_hip1_bendy_5_jnt", "R_front_hip2_bendy_1_jnt",
                                    "R_legFront_Knee_Int_Corr_Rotation_loc", "R_legFront_Knee_Int_Corr_Locator_Grp"]
    r_legFront_Ankle_Ext_position = ["R_front_knee1_jnt", "R_front_ankle_jnt", "R_legFront_Ankle_Ext_Corr_Position_loc",
                                     "R_legFront_Ankle_Ext_Corr_Locator_Grp"]
    r_legFront_Ankle_Ext_rotation = ["R_front_knee1_jnt", "R_front_ankle_jnt", "R_legFront_Ankle_Ext_Corr_Rotation_loc",
                                     "R_legFront_Ankle_Ext_Corr_Locator_Grp"]
    r_legFront_Ankle_Int_position = ["R_front_knee1_jnt", "R_front_ankle_jnt", "R_legFront_Ankle_Int_Corr_Position_loc",
                                     "R_legFront_Ankle_Int_Corr_Locator_Grp"]
    r_legFront_Ankle_Int_rotation = ["R_front_knee1_jnt", "R_front_ankle_jnt", "R_legFront_Ankle_Int_Corr_Rotation_loc",
                                     "R_legFront_Ankle_Int_Corr_Locator_Grp"]

    r_legRear_Up_Int_position = ["R_rear_Upleg_jnt", "R_rear_hip1_bendy_1_jnt", "R_legRear_Up_Int_Corr_Position_loc",
                                 "R_legRear_Up_Int_Corr_Locator_Grp"]
    r_legRear_Up_Int_rotation = ["R_rear_Upleg_jnt", "R_rear_hip1_bendy_1_jnt", "R_legRear_Up_Int_Corr_Rotation_loc",
                                 "R_legRear_Up_Int_Corr_Locator_Grp"]
    r_legRear_Up_Ext_position = ["R_rear_Upleg_jnt", "R_rear_hip1_bendy_1_jnt", "R_legRear_Up_Ext_Corr_Position_loc",
                                 "R_legRear_Up_Ext_Corr_Locator_Grp"]
    r_legRear_Up_Ext_rotation = ["R_rear_Upleg_jnt", "R_rear_hip1_bendy_1_jnt", "R_legRear_Up_Ext_Corr_Rotation_loc",
                                 "R_legRear_Up_Ext_Corr_Locator_Grp"]
    r_legRear_Knee_Ext_position = ["R_rear_hip1_bendy_5_jnt", "R_rear_hip2_bendy_1_jnt",
                                   "R_legRear_Knee_Ext_Corr_Position_loc", "R_legRear_Knee_Ext_Corr_Locator_Grp"]
    r_legRear_Knee_Ext_rotation = ["R_rear_hip1_bendy_5_jnt", "R_rear_hip2_bendy_1_jnt",
                                   "R_legRear_Knee_Ext_Corr_Rotation_loc", "R_legRear_Knee_Ext_Corr_Locator_Grp"]
    r_legRear_Knee_Int_position = ["R_rear_hip1_bendy_5_jnt", "R_rear_hip2_bendy_1_jnt",
                                   "R_legRear_Knee_Int_Corr_Position_loc", "R_legRear_Knee_Int_Corr_Locator_Grp"]
    r_legRear_Knee_Int_rotation = ["R_rear_hip1_bendy_5_jnt", "R_rear_hip2_bendy_1_jnt",
                                   "R_legRear_Knee_Int_Corr_Rotation_loc", "R_legRear_Knee_Int_Corr_Locator_Grp"]
    r_legRear_Ankle_Ext_position = ["R_rear_knee1_jnt", "R_rear_ankle_jnt", "R_legRear_Ankle_Ext_Corr_Position_loc",
                                    "R_legRear_Ankle_Ext_Corr_Locator_Grp"]
    r_legRear_Ankle_Ext_rotation = ["R_rear_knee1_jnt", "R_rear_ankle_jnt", "R_legRear_Ankle_Ext_Corr_Rotation_loc",
                                    "R_legRear_Ankle_Ext_Corr_Locator_Grp"]
    r_legRear_Ankle_Int_position = ["R_rear_knee1_jnt", "R_rear_ankle_jnt", "R_legRear_Ankle_Int_Corr_Position_loc",
                                    "R_legRear_Ankle_Int_Corr_Locator_Grp"]
    r_legRear_Ankle_Int_rotation = ["R_rear_knee1_jnt", "R_rear_ankle_jnt", "R_legRear_Ankle_Int_Corr_Rotation_loc",
                                    "R_legRear_Ankle_Int_Corr_Locator_Grp"]

    # Applica Double Parent Matrix a tutte le liste di oggetti
    check_and_apply_double_parent_matrix(neck_low_position)
    check_and_apply_double_parent_matrix(neck_low_rotation)
    check_and_apply_double_parent_matrix(neck_up_position)
    check_and_apply_double_parent_matrix(neck_up_rotation)
    check_and_apply_double_parent_matrix(l_neck_position)
    check_and_apply_double_parent_matrix(l_neck_rotation)
    check_and_apply_double_parent_matrix(r_neck_position)
    check_and_apply_double_parent_matrix(r_neck_rotation)
    check_and_apply_double_parent_matrix(l_legFront_Up_Int_position)
    check_and_apply_double_parent_matrix(l_legFront_Up_Int_rotation)
    check_and_apply_double_parent_matrix(l_legFront_Up_Ext_position)
    check_and_apply_double_parent_matrix(l_legFront_Up_Ext_rotation)
    check_and_apply_double_parent_matrix(l_legFront_Knee_Ext_position)
    check_and_apply_double_parent_matrix(l_legFront_Knee_Ext_rotation)
    check_and_apply_double_parent_matrix(l_legFront_Knee_Int_position)
    check_and_apply_double_parent_matrix(l_legFront_Knee_Int_rotation)
    check_and_apply_double_parent_matrix(l_legFront_Ankle_Ext_position)
    check_and_apply_double_parent_matrix(l_legFront_Ankle_Ext_rotation)
    check_and_apply_double_parent_matrix(l_legFront_Ankle_Int_position)
    check_and_apply_double_parent_matrix(l_legFront_Ankle_Int_rotation)
    check_and_apply_double_parent_matrix(l_legRear_Up_Int_position)
    check_and_apply_double_parent_matrix(l_legRear_Up_Int_rotation)
    check_and_apply_double_parent_matrix(l_legRear_Up_Ext_position)
    check_and_apply_double_parent_matrix(l_legRear_Up_Ext_rotation)
    check_and_apply_double_parent_matrix(l_legRear_Knee_Ext_position)
    check_and_apply_double_parent_matrix(l_legRear_Knee_Ext_rotation)
    check_and_apply_double_parent_matrix(l_legRear_Knee_Int_position)
    check_and_apply_double_parent_matrix(l_legRear_Knee_Int_rotation)
    check_and_apply_double_parent_matrix(l_legRear_Ankle_Ext_position)
    check_and_apply_double_parent_matrix(l_legRear_Ankle_Ext_rotation)
    check_and_apply_double_parent_matrix(l_legRear_Ankle_Int_position)
    check_and_apply_double_parent_matrix(l_legRear_Ankle_Int_rotation)
    check_and_apply_double_parent_matrix(r_legFront_Up_Int_position)
    check_and_apply_double_parent_matrix(r_legFront_Up_Int_rotation)
    check_and_apply_double_parent_matrix(r_legFront_Up_Ext_position)
    check_and_apply_double_parent_matrix(r_legFront_Up_Ext_rotation)
    check_and_apply_double_parent_matrix(r_legFront_Knee_Ext_position)
    check_and_apply_double_parent_matrix(r_legFront_Knee_Ext_rotation)
    check_and_apply_double_parent_matrix(r_legFront_Knee_Int_position)
    check_and_apply_double_parent_matrix(r_legFront_Knee_Int_rotation)
    check_and_apply_double_parent_matrix(r_legFront_Ankle_Ext_position)
    check_and_apply_double_parent_matrix(r_legFront_Ankle_Ext_rotation)
    check_and_apply_double_parent_matrix(r_legFront_Ankle_Int_position)
    check_and_apply_double_parent_matrix(r_legFront_Ankle_Int_rotation)
    check_and_apply_double_parent_matrix(r_legRear_Up_Int_position)
    check_and_apply_double_parent_matrix(r_legRear_Up_Int_rotation)
    check_and_apply_double_parent_matrix(r_legRear_Up_Ext_position)
    check_and_apply_double_parent_matrix(r_legRear_Up_Ext_rotation)
    check_and_apply_double_parent_matrix(r_legRear_Knee_Ext_position)
    check_and_apply_double_parent_matrix(r_legRear_Knee_Ext_rotation)
    check_and_apply_double_parent_matrix(r_legRear_Knee_Int_position)
    check_and_apply_double_parent_matrix(r_legRear_Knee_Int_rotation)
    check_and_apply_double_parent_matrix(r_legRear_Ankle_Ext_position)
    check_and_apply_double_parent_matrix(r_legRear_Ankle_Ext_rotation)
    check_and_apply_double_parent_matrix(r_legRear_Ankle_Int_position)
    check_and_apply_double_parent_matrix(r_legRear_Ankle_Int_rotation)


    def safe_delete(obj_name):
        if cmds.objExists(obj_name):
            cmds.delete(obj_name)

    # Funzione di supporto per applicare parentConstraint solo se gli oggetti esistono
    def safe_parent_constraint(source, target, mo=True):
        if cmds.objExists(source) and cmds.objExists(target):
            cmds.parentConstraint(source, target, mo=mo)

    # Funzione di supporto per il grouping solo se tutti gli oggetti esistono
    def safe_group(objects, group_name):
        filtered_objects = [obj for obj in objects if cmds.objExists(obj)]
        if filtered_objects:
            cmds.group(*filtered_objects, n=group_name)

    # Funzione di supporto per il set solo se tutti gli oggetti esistono
    def safe_set(objects, set_name):
        filtered_objects = [obj for obj in objects if cmds.objExists(obj)]
        if filtered_objects:
            cmds.sets(*filtered_objects, n=set_name)

    # Funzione di supporto per nascondere oggetti esistenti
    def safe_hide(objects):
        for obj in objects:
            if cmds.objExists(obj):
                cmds.hide(obj)

    # Cancellazione degli oggetti se esistono
    safe_delete("Arrow_Curve")
    safe_delete("L_Side_Correctrive")
    safe_delete("R_Side_Correctrive")

    # Creazione dei gruppi per il lato sinistro, raggruppando solo gli oggetti esistenti
    left_groups = [
        "L_legFront_Knee_Ext_Corr_Grp", "L_legFront_Knee_Int_Corr_Grp", "L_legFront_Ankle_Ext_Corr_Grp",
        "L_Neck_Corr_Grp", "L_legFront_Up_Int_Corr_Grp", "L_legFront_Up_Ext_Corr_Grp", "L_legRear_Knee_Ext_Corr_Grp",
        "L_legRear_Ankle_Int_Corr_Grp", "L_legRear_Up_Ext_Corr_Grp", "L_legRear_Knee_Int_Corr_Grp",
        "L_legRear_Ankle_Ext_Corr_Grp", "L_legRear_Up_Int_Corr_Grp", "Neck_Low_Corr_Grp", "Neck_Up_Corr_Grp", "L_legFront_Ankle_Int_Corr_Grp"

    ]
    safe_group(left_groups, "L_Side_Corrective_Grp_RIG")

    # Creazione dei gruppi per il lato destro
    right_groups = [
        "R_legFront_Knee_Ext_Corr_Grp", "R_legFront_Knee_Int_Corr_Grp", "R_legFront_Ankle_Ext_Corr_Grp",
        "R_Neck_Corr_Grp", "R_legFront_Up_Int_Corr_Grp", "R_legFront_Up_Ext_Corr_Grp", "R_legRear_Knee_Ext_Corr_Grp",
        "R_legRear_Up_Ext_Corr_Grp", "R_legRear_Ankle_Ext_Corr_Grp", "R_legRear_Up_Int_Corr_Grp", "R_legFront_Ankle_Int_Corr_Grp",
        "R_legRear_Knee_Int_Corr_Grp", "R_legRear_Ankle_Int_Corr_Grp"
    ]

    safe_group(right_groups, "R_Side_Corrective_Grp_RIG")

    oggetti_da_nascondere_l = (
        'L_legFront_Knee_Ext_Corr_Joints_Grp', 'L_legFront_Knee_Ext_Corr_Locator_Grp', 'L_legFront_Knee_Int_Corr_Joints_Grp',
        'L_legFront_Knee_Int_Corr_Locator_Grp', 'L_legFront_Ankle_Ext_Corr_Joints_Grp', 'L_legFront_Ankle_Ext_Corr_Locator_Grp',
        'Neck_Low_Corr_Joints_Grp', 'Neck_Low_Corr_Locator_Grp', 'Neck_Up_Corr_Joints_Grp', 'Neck_Up_Corr_Locator_Grp',
        'L_Neck_Corr_Joints_Grp', 'L_Neck_Corr_Locator_Grp', 'L_legFront_Up_Int_Corr_Joints_Grp',
        'L_legFront_Up_Int_Corr_Locator_Grp', 'L_legFront_Up_Ext_Corr_Joints_Grp', 'L_legFront_Up_Ext_Corr_Locator_Grp',
        'L_legRear_Knee_Ext_Corr_Joints_Grp', 'L_legRear_Knee_Ext_Corr_Locator_Grp', 'L_legFront_Ankle_Int_Corr_Locator_Grp',
        'L_legFront_Ankle_Int_Corr_Joints_Grp'
    )
    safe_hide(oggetti_da_nascondere_l)

    # Nascondere gli oggetti per il lato destro
    oggetti_da_nascondere_r = (
        'R_legFront_Knee_Ext_Corr_Joints_Grp', 'R_legFront_Knee_Ext_Corr_Locator_Grp', 'R_legFront_Knee_Int_Corr_Joints_Grp',
        'R_legFront_Knee_Int_Corr_Locator_Grp', 'R_legFront_Ankle_Ext_Corr_Joints_Grp', 'R_legFront_Ankle_Ext_Corr_Locator_Grp',
        'R_Neck_Corr_Joints_Grp', 'R_Neck_Corr_Locator_Grp', 'R_legFront_Up_Int_Corr_Joints_Grp', 'R_legFront_Up_Int_Corr_Locator_Grp',
        'R_legFront_Ankle_Int_Corr_Joints_Grp', 'R_legFront_Ankle_Int_Corr_Locator_Grp', 'R_legRear_Knee_Int_Corr_Joints_Grp',
        'R_legRear_Knee_Int_Corr_Locator_Grp', 'R_legRear_Ankle_Int_Corr_Joints_Grp', 'R_legRear_Ankle_Int_Corr_Locator_Grp',
        'R_legRear_Knee_Ext_Corr_Joints_Grp', 'R_legRear_Knee_Ext_Corr_Locator_Grp', 'R_legRear_Up_Ext_Corr_Joints_Grp',
        'R_legRear_Up_Ext_Corr_Locator_Grp', 'R_legRear_Ankle_Ext_Corr_Joints_Grp', 'R_legRear_Ankle_Ext_Corr_Locator_Grp',
        'R_legRear_Up_Int_Corr_Joints_Grp', 'R_legRear_Up_Int_Corr_Locator_Grp', 'R_legFront_Up_Ext_Corr_Joints_Grp',
        'R_legFront_Up_Ext_Corr_Locator_Grp', 'L_legRear_Ankle_Ext_Corr_Locator_Grp', 'L_legRear_Ankle_Ext_Corr_Joints_Grp',
        'L_legRear_Ankle_Int_Corr_Joints_Grp', 'L_legRear_Ankle_Int_Corr_Locator_Grp', 'L_legRear_Up_Ext_Corr_Joints_Grp',
        'L_legRear_Up_Ext_Corr_Locator_Grp', 'L_legRear_Knee_Int_Corr_Joints_Grp', 'L_legRear_Knee_Int_Corr_Locator_Grp',
        'L_legRear_Up_Int_Corr_Joints_Grp', 'L_legRear_Up_Int_Corr_Locator_Grp'
    )
    safe_hide(oggetti_da_nascondere_r)

    # Parent e scale constraint con condizioni
    safe_parent_constraint("L_front_hip1_bendy_5_jnt", "L_legFront_Knee_Ext_Corr_Grp")
    safe_parent_constraint("L_front_hip1_bendy_5_jnt", "L_legFront_Knee_Int_Corr_Grp")
    safe_parent_constraint("L_front_knee1_jnt", "L_legFront_Ankle_Ext_Corr_Grp")
    safe_parent_constraint("L_front_knee1_jnt", "L_legFront_Ankle_Int_Corr_Grp")
    safe_parent_constraint("L_front_Upleg_jnt", "L_legFront_Up_Ext_Corr_Grp")
    safe_parent_constraint("L_front_Upleg_jnt", "L_legFront_Up_Int_Corr_Grp")
    safe_parent_constraint("spineHigh_Ctrl", "Neck_Low_Corr_Grp")
    safe_parent_constraint("spineHigh_Ctrl", "Neck_Up_Corr_Grp")
    safe_parent_constraint("spineHigh_Ctrl", "L_Neck_Corr_Grp")
    safe_parent_constraint("spineHigh_Ctrl", "R_Neck_Corr_Grp")
    safe_parent_constraint("L_rear_hip1_bendy_5_jnt", "L_legRear_Knee_Ext_Corr_Grp")
    safe_parent_constraint("L_rear_hip1_bendy_5_jnt", "L_legRear_Knee_Int_Corr_Grp")
    safe_parent_constraint("L_rear_knee1_jnt", "L_legRear_Ankle_Int_Corr_Grp")
    safe_parent_constraint("L_rear_knee1_jnt", "L_legRear_Ankle_Ext_Corr_Grp")
    safe_parent_constraint("L_rear_Upleg_jnt", "L_legRear_Up_Ext_Corr_Grp")
    safe_parent_constraint("L_rear_Upleg_jnt", "L_legRear_Up_Int_Corr_Grp")

    safe_parent_constraint("R_front_hip1_bendy_5_jnt", "R_legFront_Knee_Ext_Corr_Grp")
    safe_parent_constraint("R_front_hip1_bendy_5_jnt", "R_legFront_Knee_Int_Corr_Grp")
    safe_parent_constraint("R_front_knee1_jnt", "R_legFront_Ankle_Ext_Corr_Grp")
    safe_parent_constraint("R_front_knee1_jnt", "R_legFront_Ankle_Int_Corr_Grp")
    safe_parent_constraint("R_front_Upleg_jnt", "R_legFront_Up_Ext_Corr_Grp")
    safe_parent_constraint("R_front_Upleg_jnt", "R_legFront_Up_Int_Corr_Grp")
    safe_parent_constraint("R_rear_hip1_bendy_5_jnt", "R_legRear_Knee_Ext_Corr_Grp")
    safe_parent_constraint("R_rear_hip1_bendy_5_jnt", "R_legRear_Knee_Int_Corr_Grp")
    safe_parent_constraint("R_rear_knee1_jnt", "R_legRear_Ankle_Int_Corr_Grp")
    safe_parent_constraint("R_rear_knee1_jnt", "R_legRear_Ankle_Ext_Corr_Grp")
    safe_parent_constraint("R_rear_Upleg_jnt", "R_legRear_Up_Ext_Corr_Grp")
    safe_parent_constraint("R_rear_Upleg_jnt", "R_legRear_Up_Int_Corr_Grp")

    # Creazione dei set per il lato sinistro e destro
    left_weights = [
        "L_legFront_Knee_Ext_Corr_Weight_jnt", "L_legFront_Knee_Int_Corr_Weight_jnt", "L_legFront_Ankle_Ext_Corr_Weight_jnt",
        "Neck_Low_Corr_Weight_jnt", "Neck_Up_Corr_Weight_jnt", "L_Neck_Corr_Weight_jnt", "L_legFront_Up_Int_Corr_Weight_jnt",
        "L_legFront_Up_Ext_Corr_Weight_jnt", "L_legFront_Ankle_Int_Corr_Weight_jnt", "L_legRear_Knee_Ext_Corr_Weight_jnt",
        "L_legRear_Ankle_Int_Corr_Weight_jnt", "L_legRear_Up_Ext_Corr_Weight_jnt", "L_legRear_Knee_Int_Corr_Weight_jnt",
        "L_legRear_Ankle_Ext_Corr_Weight_jnt", "L_legRear_Up_Int_Corr_Weight_jnt"
    ]
    safe_set(left_weights, "SET_L_Body_Corrective_Weights")

    right_weights = [
        "R_legFront_Knee_Ext_Corr_Weight_jnt", "R_legFront_Knee_Int_Corr_Weight_jnt", "R_legFront_Ankle_Ext_Corr_Weight_jnt",
        "R_Neck_Corr_Weight_jnt", "R_legFront_Up_Int_Corr_Weight_jnt", "R_legFront_Up_Ext_Corr_Weight_jnt",
        "R_legFront_Ankle_Int_Corr_Weight_jnt", "R_legRear_Knee_Int_Corr_Weight_jnt", "R_legRear_Knee_Ext_Corr_Weight_jnt",
        "R_legRear_Up_Ext_Corr_Weight_jnt", "R_legRear_Ankle_Ext_Corr_Weight_jnt", "R_legRear_Ankle_Int_Corr_Weight_jnt",
        "R_legRear_Up_Int_Corr_Weight_jnt"
    ]
    safe_set(right_weights, "SET_R_Body_Corrective_Weights")

    # Creazione del set per i controlli del lato sinistro
    left_controls = [
        "L_legFront_Knee_Ext_Corr_Ctrl", "L_legFront_Knee_Int_Corr_Ctrl", "L_legFront_Ankle_Ext_Corr_Ctrl",
        "Neck_Low_Corr_Ctrl", "Neck_Up_Corr_Ctrl", "L_Neck_Corr_Ctrl", "L_legFront_Up_Int_Corr_Ctrl",
        "L_legFront_Up_Ext_Corr_Ctrl", "L_legFront_Ankle_Int_Corr_Ctrl", "L_legRear_Knee_Ext_Corr_Ctrl",
        "L_legRear_Ankle_Int_Corr_Ctrl", "L_legRear_Up_Ext_Corr_Ctrl", "L_legRear_Knee_Int_Corr_Ctrl",
        "L_legRear_Ankle_Ext_Corr_Ctrl", "L_legRear_Up_Int_Corr_Ctrl"
    ]
    safe_set(left_controls, "SET_L_Body_Corrective_CONTROLS")

    # Creazione del set per i controlli del lato destro
    right_controls = [
        "R_legFront_Knee_Ext_Corr_Ctrl", "R_legFront_Knee_Int_Corr_Ctrl", "R_legFront_Ankle_Ext_Corr_Ctrl",
        "R_Neck_Corr_Ctrl", "R_legFront_Up_Int_Corr_Ctrl", "R_legFront_Up_Ext_Corr_Ctrl", "R_legFront_Ankle_Int_Corr_Ctrl",
        "R_legRear_Knee_Int_Corr_Ctrl", "R_legRear_Knee_Ext_Corr_Ctrl", "R_legRear_Up_Ext_Corr_Ctrl",
        "R_legRear_Ankle_Ext_Corr_Ctrl", "R_legRear_Ankle_Int_Corr_Ctrl", "R_legRear_Up_Int_Corr_Ctrl"
    ]
    safe_set(right_controls, "SET_R_Body_Corrective_CONTROLS")

    position_sets = [
        "SET_L_legFront_Ankle_Ext_Corr_Position_loc_SliderNODE",
        "SET_L_legFront_Ankle_Int_Corr_Position_loc_SliderNODE",
        "SET_L_legFront_Knee_Ext_Corr_Position_loc_SliderNODE",
        "SET_L_legFront_Knee_Int_Corr_Position_loc_SliderNODE",
        "SET_L_legFront_Up_Ext_Corr_Position_loc_SliderNODE",
        "SET_L_legFront_Up_Int_Corr_Position_loc_SliderNODE",
        "SET_L_legRear_Ankle_Ext_Corr_Position_loc_SliderNODE",
        "SET_L_legRear_Ankle_Int_Corr_Position_loc_SliderNODE",
        "SET_L_legRear_Knee_Ext_Corr_Position_loc_SliderNODE",
        "SET_L_legRear_Knee_Int_Corr_Position_loc_SliderNODE",
        "SET_L_legRear_Up_Ext_Corr_Position_loc_SliderNODE",
        "SET_L_legRear_Up_Int_Corr_Position_loc_SliderNODE",
        "SET_L_Neck_Corr_Position_loc_SliderNODE",
        "SET_Neck_Low_Corr_Position_loc_SliderNODE",
        "SET_Neck_Up_Corr_Position_loc_SliderNODE"
    ]

    # Crea o aggiorna il set di posizionamenti
    safe_set(position_sets, "SET_L_Body_Corrective_SLIDER")

    right_position_sets = [
        "SET_R_legFront_Ankle_Ext_Corr_Position_loc_SliderNODE",
        "SET_R_legFront_Ankle_Int_Corr_Position_loc_SliderNODE",
        "SET_R_legFront_Knee_Ext_Corr_Position_loc_SliderNODE",
        "SET_R_legFront_Knee_Int_Corr_Position_loc_SliderNODE",
        "SET_R_legFront_Up_Ext_Corr_Position_loc_SliderNODE",
        "SET_R_legFront_Up_Int_Corr_Position_loc_SliderNODE",
        "SET_R_legRear_Ankle_Ext_Corr_Position_loc_SliderNODE",
        "SET_R_legRear_Ankle_Int_Corr_Position_loc_SliderNODE",
        "SET_R_legRear_Knee_Ext_Corr_Position_loc_SliderNODE",
        "SET_R_legRear_Knee_Int_Corr_Position_loc_SliderNODE",
        "SET_R_legRear_Up_Ext_Corr_Position_loc_SliderNODE",
        "SET_R_legRear_Up_Int_Corr_Position_loc_SliderNODE",
        "SET_R_Neck_Corr_Position_loc_SliderNODE"
    ]

    # Crea o aggiorna il set di posizionamenti per il lato destro
    safe_set(right_position_sets, "SET_R_Body_Corrective_SLIDER")

    for jnt_set in ("SET_L_Body_Corrective_Weights", "SET_R_Body_Corrective_Weights"):
        if cmds.objExists(jnt_set):
            set_tmp = cmds.sets(jnt_set, q=True)
            for jnt in set_tmp:
                cmds.setAttr(f"{jnt}.segmentScaleCompensate", 0)



#DELETE_R_SIDE____________________________________________________________________________________________________________________________________________________________________________________________________________


def delete_multiply_nodes(*args):
    multiply_nodes = [
        'MLD_LOC_L_legFront_Ankle_Int_Corr_01', u'MLD_LOC_L_legFront_Ankle_Int_Corr_02', u'MLD_LOC_L_Shoulder_Back_Corr_01', u'MLD_LOC_L_legFront_Knee_Ext_Corr_01', u'MLD_LOC_L_legFront_Knee_Ext_Corr_02', u'MLD_LOC_L_legRear_Ankle_Ext_Corr_01', u'MLD_LOC_L_legRear_Ankle_Ext_Corr_02', u'MLD_LOC_L_legFront_Knee_Int_Corr_01', u'MLD_LOC_L_legFront_Knee_Int_Corr_02', u'MLD_LOC_L_legRear_Ankle_Int_Corr_01', u'MLD_LOC_L_legRear_Ankle_Int_Corr_02', u'MLD_LOC_L_legRear_Knee_Ext_Corr_01', u'MLD_LOC_L_legRear_Knee_Ext_Corr_02', u'MLD_LOC_L_legFront_Ankle_Ext_Corr_01', u'LOC_L_legFront_Ankle_Ext_Corr_02', u'MLD_LOC_L_legRear_Knee_Int_Corr_01', u'MLD_LOC_L_legRear_Knee_Int_Corr_02', u'MLD_LOC_L_legFront_Up_Ext_Corr_01', u'MLD_LOC_L_legFront_Up_Ext_Corr_02', u'MLD_LOC_L_legFront_Up_Int_Corr_01', u'MLD_LOC_L_legFront_Up_Int_Corr_02', u'MLD_LOC_L_legRear_Up_Ext_Corr_01', u'MLD_LOC_L_legRear_Up_Int_Corr_04', u'MLD_LOC_L_Neck_Corr_01', u'MLD_LOC_L_Neck_Corr_02', u'MLD_LOC_L_legRear_Up_Ext_Corr_02', u'MLD_LOC_L_legRear_Up_Int_Corr_01'

    ]

    for node in multiply_nodes:
        if cmds.objExists(node):
            cmds.delete(node)
            print("Nodo MultiplyDivide eliminato:", node)
        else:
            print("Nodo MultiplyDivide non trovato:", node)


    nodes_to_rotate = [
        "LOC_R_legFront_Knee_Ext_Corr",
        "LOC_R_legFront_Knee_Int_Corr",
        "LOC_R_legFront_Ankle_Ext_Corr",
        "LOC_R_Neck_Corr",
        "LOC_R_legFront_Up_Int_Corr",
        "LOC_R_legFront_Up_Ext_Corr",
        "LOC_R_legFront_Ankle_Int_Corr",
        "LOC_R_legRear_Knee_Int_Corr",
        "LOC_R_legRear_Knee_Ext_Corr",
        "LOC_R_legRear_Up_Ext_Corr",
        "LOC_R_legRear_Ankle_Ext_Corr",
        "LOC_R_legRear_Ankle_Int_Corr",
        "LOC_R_legRear_Up_Int_Corr"
    ]

    for node in nodes_to_rotate:
        if cmds.objExists(node):
            cmds.rotate(0, 0, 180, node, os=True, r=True)

            
def importLocator(*args):
    localpipeBackDir = os.path.dirname(localpipe)
    scenesDir = os.path.join(localpipeBackDir, 'python', 'depts', 'rigging', 'tools', 'generic', 'CorrectiveJoints', 'scenes')
    fileToImport = os.path.join(scenesDir, 'CorrectiveLocatorToImportQuad.ma')
    cmds.file(fileToImport, i=True, type="mayaAscii", namespace=":")
    fileToImport = os.path.join(scenesDir, 'ShapeQuad.ma')
    cmds.file(fileToImport, i=True, type="mayaAscii", namespace=":")

    def match_locator_with_joint(locator, joint):
        if cmds.objExists(joint):
            if cmds.objExists(locator):
                cmds.matchTransform(locator, joint)
        else:
            if cmds.objExists(locator):
                cmds.delete(locator)

    def rotate_and_move(locator, rotate_values, move_values):
        if cmds.objExists(locator):
            if rotate_values:
                cmds.rotate(*rotate_values, locator, os=True, r=True)
            if move_values:
                cmds.move(*move_values, locator, os=True, r=True)

    match_locator_with_joint("LOC_L_legFront_Up_Int_Corr", "L_front_hip1_jnt")
    rotate_and_move("LOC_L_legFront_Up_Int_Corr", (0, 0, -90), (-6, 4, 0))
    rotate_and_move("LOC_L_legFront_Up_Int_Corr", (0, 180, 0), None)

    match_locator_with_joint("LOC_L_Neck_Corr", "neck_jnt")
    rotate_and_move("LOC_L_Neck_Corr", (90, 70, 90), (0, 0, 10))
    rotate_and_move("LOC_L_Neck_Corr", (0, -90, 0), None)

    match_locator_with_joint("LOC_Neck_Low_Corr", "neck_jnt")
    rotate_and_move("LOC_Neck_Low_Corr", (25, 0, 0), (0, 0, 10))
    rotate_and_move("LOC_Neck_Low_Corr", (0, -90, 0), None)

    match_locator_with_joint("LOC_Neck_Up_Corr", "neck_jnt")
    rotate_and_move("LOC_Neck_Up_Corr", (-220, 0, 180), (0, 0, 10))
    rotate_and_move("LOC_Neck_Up_Corr", (0, -90, 0), None)

    match_locator_with_joint("LOC_L_legFront_Up_Ext_Corr", "L_front_hip1_jnt")
    rotate_and_move("LOC_L_legFront_Up_Ext_Corr", (180, 0, -90), (6, 0, 0))

    match_locator_with_joint("LOC_L_legFront_Knee_Int_Corr", "L_front_hip2_jnt")
    rotate_and_move("LOC_L_legFront_Knee_Int_Corr", (0, 0, 0), (0, 0, 6))
    rotate_and_move("LOC_L_legFront_Knee_Int_Corr", (0, -90, 0), None)

    match_locator_with_joint("LOC_L_legFront_Knee_Ext_Corr", "L_front_hip2_jnt")
    rotate_and_move("LOC_L_legFront_Knee_Ext_Corr", (0, 0, 0), (0, 1, 6))
    rotate_and_move("LOC_L_legFront_Knee_Ext_Corr", (-180, 0, 0), None)
    rotate_and_move("LOC_L_legFront_Knee_Ext_Corr", (0, -90, 0), None)

    match_locator_with_joint("LOC_L_legFront_Ankle_Int_Corr", "L_front_ankle_jnt")
    rotate_and_move("LOC_L_legFront_Ankle_Int_Corr", (0, 0, 0), (0, 0, 6))
    rotate_and_move("LOC_L_legFront_Ankle_Int_Corr", (-25, 0, 0), None)
    rotate_and_move("LOC_L_legFront_Ankle_Int_Corr", (0, -90, 0), None)

    match_locator_with_joint("LOC_L_legFront_Ankle_Ext_Corr", "L_front_ankle_jnt")
    rotate_and_move("LOC_L_legFront_Ankle_Ext_Corr", (0, 0, 0), (0, 0, 6))
    rotate_and_move("LOC_L_legFront_Ankle_Ext_Corr", (-180, 0, 0), None)
    rotate_and_move("LOC_L_legFront_Ankle_Ext_Corr", (0, -90, 0), None)

    # Rear leg locators
    match_locator_with_joint("LOC_L_legRear_Up_Int_Corr", "L_rear_hip1_jnt")
    rotate_and_move("LOC_L_legRear_Up_Int_Corr", (0, 0, -90), (8, -4, -1))
    rotate_and_move("LOC_L_legRear_Up_Int_Corr", (180, 0, 0), None)

    match_locator_with_joint("LOC_L_legRear_Up_Ext_Corr", "L_rear_hip1_jnt")
    rotate_and_move("LOC_L_legRear_Up_Ext_Corr", (0, 0, -90), (6, 4, -1))
    rotate_and_move("LOC_L_legRear_Up_Ext_Corr", (-180, 0, 0), None)
    rotate_and_move("LOC_L_legRear_Up_Ext_Corr", (0, 0, 180), None)

    match_locator_with_joint("LOC_L_legRear_Knee_Int_Corr", "L_rear_hip2_jnt")
    rotate_and_move("LOC_L_legRear_Knee_Int_Corr", (0, 0, 0), (0, -1, 6))
    rotate_and_move("LOC_L_legRear_Knee_Int_Corr", (0, -90, 0), None)

    match_locator_with_joint("LOC_L_legRear_Knee_Ext_Corr", "L_rear_hip2_jnt")
    rotate_and_move("LOC_L_legRear_Knee_Ext_Corr", (0, 0, 0), (0, 1, 7))
    rotate_and_move("LOC_L_legRear_Knee_Ext_Corr", (-180, 0, 0), None)
    rotate_and_move("LOC_L_legRear_Knee_Ext_Corr", (0, -90, 0), None)

    match_locator_with_joint("LOC_L_legRear_Ankle_Int_Corr", "L_rear_ankle_jnt")
    rotate_and_move("LOC_L_legRear_Ankle_Int_Corr", (0, 0, 0), (0, 0, 6))
    rotate_and_move("LOC_L_legRear_Ankle_Int_Corr", (-25, 0, 0), None)
    rotate_and_move("LOC_L_legRear_Ankle_Int_Corr", (0, -90, 0), None)

    match_locator_with_joint("LOC_L_legRear_Ankle_Ext_Corr", "L_rear_ankle_jnt")
    rotate_and_move("LOC_L_legRear_Ankle_Ext_Corr", (0, 0, 0), (0, 0, 6))
    rotate_and_move("LOC_L_legRear_Ankle_Ext_Corr", (-180, 0, 0), None)
    rotate_and_move("LOC_L_legRear_Ankle_Ext_Corr", (0, -90, 0), None)


    
def linkWiki(*args):
    import webbrowser
    pathToHelp="https://sites.google.com/rbw-cgi.it/correctivejoints/home-page"
    webbrowser.open(pathToHelp)
    
from PySide2 import QtWidgets, QtGui
class AttributeExporterUI(QtWidgets.QWidget):
    def __init__(self):
        super(AttributeExporterUI, self).__init__()

        self.setWindowTitle("Attribute Exporter/Importer")
        self.setGeometry(300, 300, 300, 100)

        self.create_widgets()

    def create_widgets(self):
        layout = QtWidgets.QVBoxLayout()

        # Pulsante per l'esportazione
        export_button = QtWidgets.QPushButton("Export Attributes")
        export_button.clicked.connect(self.export_attributes)
        layout.addWidget(export_button)

        # Pulsante per l'importazione
        import_button = QtWidgets.QPushButton("Import Attributes")
        import_button.clicked.connect(self.import_attributes)
        layout.addWidget(import_button)

        self.setLayout(layout)

    def export_attributes(self):
        # Lista di attributi specifici
        attributes_to_export = [
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",            
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Z",           
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",            
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Z",            
            "CND_L_Neck_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_Neck_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_Neck_Corr_Position_loc_Translate_X.operation",
            "CND_L_Neck_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_Neck_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_Neck_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_Neck_Corr_Position_loc_Translate_Y.operation",
            "CND_L_Neck_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_Neck_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_Neck_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_Neck_Corr_Position_loc_Translate_Z.operation",
            "CND_L_Neck_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_Neck_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_Neck_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_Neck_Corr_Position_loc_Rotation_Multiply.input1Z",           
            "CND_Neck_Low_Corr_Position_loc_Translate_X.secondTerm",
            "CND_Neck_Low_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_Neck_Low_Corr_Position_loc_Translate_X.operation",
            "CND_Neck_Low_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_Neck_Low_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_Neck_Low_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_Neck_Low_Corr_Position_loc_Translate_Y.operation",
            "CND_Neck_Low_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_Neck_Low_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_Neck_Low_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_Neck_Low_Corr_Position_loc_Translate_Z.operation",
            "CND_Neck_Low_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_Neck_Low_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_Neck_Low_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_Neck_Low_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_Neck_Up_Corr_Position_loc_Translate_X.secondTerm",
            "CND_Neck_Up_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_Neck_Up_Corr_Position_loc_Translate_X.operation",
            "CND_Neck_Up_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_Neck_Up_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_Neck_Up_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_Neck_Up_Corr_Position_loc_Translate_Y.operation",
            "CND_Neck_Up_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_Neck_Up_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_Neck_Up_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_Neck_Up_Corr_Position_loc_Translate_Z.operation",
            "CND_Neck_Up_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_Neck_Up_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_Neck_Up_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_Neck_Up_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",            
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Z",           
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",            
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Z",            
            "CND_R_Neck_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_Neck_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_Neck_Corr_Position_loc_Translate_X.operation",
            "CND_R_Neck_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_Neck_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_Neck_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_Neck_Corr_Position_loc_Translate_Y.operation",
            "CND_R_Neck_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_Neck_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_Neck_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_Neck_Corr_Position_loc_Translate_Z.operation",
            "CND_R_Neck_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_Neck_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_Neck_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_Neck_Corr_Position_loc_Rotation_Multiply.input1Z",           
        ]

        # Specifica il percorso e il nome del file per l'esportazione
        export_file_path, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Export Attributes", "", "Text Files (*.txt);;")
        if export_file_path:
            attributes_to_export_existing = [attr for attr in attributes_to_export if cmds.objExists(attr)]
            export_attributes(export_file_path, attributes_to_export_existing)


    def import_attributes(self):
        # Lista di attributi specifici
        attributes_to_import = [
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",            
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Z",           
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",            
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Z",            
            "CND_L_Neck_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_Neck_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_Neck_Corr_Position_loc_Translate_X.operation",
            "CND_L_Neck_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_Neck_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_Neck_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_Neck_Corr_Position_loc_Translate_Y.operation",
            "CND_L_Neck_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_Neck_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_Neck_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_Neck_Corr_Position_loc_Translate_Z.operation",
            "CND_L_Neck_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_Neck_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_Neck_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_Neck_Corr_Position_loc_Rotation_Multiply.input1Z",           
            "CND_Neck_Low_Corr_Position_loc_Translate_X.secondTerm",
            "CND_Neck_Low_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_Neck_Low_Corr_Position_loc_Translate_X.operation",
            "CND_Neck_Low_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_Neck_Low_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_Neck_Low_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_Neck_Low_Corr_Position_loc_Translate_Y.operation",
            "CND_Neck_Low_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_Neck_Low_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_Neck_Low_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_Neck_Low_Corr_Position_loc_Translate_Z.operation",
            "CND_Neck_Low_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_Neck_Low_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_Neck_Low_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_Neck_Low_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_Neck_Up_Corr_Position_loc_Translate_X.secondTerm",
            "CND_Neck_Up_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_Neck_Up_Corr_Position_loc_Translate_X.operation",
            "CND_Neck_Up_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_Neck_Up_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_Neck_Up_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_Neck_Up_Corr_Position_loc_Translate_Y.operation",
            "CND_Neck_Up_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_Neck_Up_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_Neck_Up_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_Neck_Up_Corr_Position_loc_Translate_Z.operation",
            "CND_Neck_Up_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_Neck_Up_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_Neck_Up_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_Neck_Up_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",            
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Z",           
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",            
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Z",            
            "CND_R_Neck_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_Neck_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_Neck_Corr_Position_loc_Translate_X.operation",
            "CND_R_Neck_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_Neck_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_Neck_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_Neck_Corr_Position_loc_Translate_Y.operation",
            "CND_R_Neck_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_Neck_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_Neck_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_Neck_Corr_Position_loc_Translate_Z.operation",
            "CND_R_Neck_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_Neck_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_Neck_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_Neck_Corr_Position_loc_Rotation_Multiply.input1Z",
        ]


        import_file_path, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Import Attributes", "", "Text Files (*.txt);;")
        if import_file_path:
            attributes_to_import_existing = [attr for attr in attributes_to_import if cmds.objExists(attr)]
            import_attributes(import_file_path, attributes_to_import_existing)
            QtWidgets.QMessageBox.information(self, "Import Complete", "Attributi importati con successo.")

def export_attributes(file_path, attribute_list):
    # Creazione del dizionario per memorizzare i valori degli attributi
    attribute_values = {}

    # Popolamento del dizionario con i valori degli attributi specificati
    for attribute in attribute_list:
        try:
            value = cmds.getAttr(attribute)
            attribute_values[attribute] = value
        except RuntimeError:
            print("Attribute {} not found or has a missing value.".format(attribute))

    # Scrittura dei valori nel file di testo
    with open(file_path, 'w') as file:
        for attribute, value in attribute_values.items():
            file.write("{} {}\n".format(attribute, value))

def import_attributes(file_path, attribute_list):
    # Leggi i valori dal file di testo
    with open(file_path, 'r') as file:
        lines = file.readlines()

    # Creazione del dizionario per memorizzare i valori degli attributi
    attribute_values = {}

    # Popolamento del dizionario con i valori letti dal file di testo
    for line in lines:
        parts = line.strip().split()
        if len(parts) == 2:
            attribute, value = parts
            attribute_values[attribute] = float(value)

    # Applica i valori agli attributi
    for attribute in attribute_list:
        if attribute in attribute_values:
            cmds.setAttr(attribute, attribute_values[attribute])
        else:
            print ("Attribute {} not found in the imported data.".format(attribute))


def AB_poseReaderQuad(*args):
    if cmds.window("CorrectiveJointUIAB", q=True, ex=True):
        cmds.deleteUI("CorrectiveJointUIAB")

    ww = cmds.window("CorrectiveJointUIAB", t="Rainbow CGI - Corrective Joints for AB Quad", widthHeight=(250, 50), sizeable=True, resizeToFitChildren=True)

    # Get a pointer and convert it to Qt Widget object
    qw = omui.MQtUtil.findWindow(ww)
    widget = wrapInstance(int(qw), QWidget)

    # Create a QIcon object
    iconpath = os.path.join(image_path, "RainbowCGI_icona.ico")

    # Assign the icon
    icon = QIcon(iconpath)
    widget.setWindowIcon(icon)

    col = cmds.columnLayout(adjustableColumn=1)

    # Carica l'immagine
    img = os.path.join(image_path, "Quad_AB_PoseReader.ico")
    cmds.text(h = 10, l = "")
    cmds.iconTextButton(style="iconOnly", image1=img, p=col, w=100)
    cmds.text(h = 10, l = "")
    cmds.setParent("..")

    col1 = cmds.columnLayout(adj=1, p=col, bgc=[0.2, 0.2, 0.2])
    cmds.rowLayout(numberOfColumns=2, p=col1, adj=2, cat=[(1, "left", 5), (2, "left", 0)])
    cmds.text("RBW - Corrective Joints supporting Deformation for AB QUAD", h=20)

    cmds.frameLayout(label="Corrective Joints Creation for AB QUAD", collapsable=1, collapse=0, p=col, bgc=[0.7, 0.2, 0.6], w=10)
    cmds.columnLayout(adj=1, bgc=[0.2, 0.2, 0.2])
    cmds.button(l="| Import Locator QUAD |", c=importLocator, h=30, bgc=[0.3, 0.3, 0.3], w=10)
    cmds.button(l="| Delete R Parent Side QUAD |", c=delete_multiply_nodes, h=30, bgc=[0.3, 0.3, 0.3], w=10)
    cmds.button(l="| Create Corrective Joints QUAD|", c=process, h=30, bgc=[0.3, 0.3, 0.3], w=10)
    cmds.button(l="| Finalize QUAD |", c=finalize, h=30, bgc=[0.3, 0.3, 0.3], w=10)
    cmds.button(l="| Double Parent Matrix QUAD |", c=DoubleParentMatrix, h=30, bgc=[0.2, 0.2, 0.2], w=10)
    cmds.frameLayout(label="Utilities QUAD", collapsable=1, collapse=0, p=col, bgc=[0.5, 0.2, 0.6], w=10)
    cmds.columnLayout(adj=1, bgc=[0.2, 0.2, 0.2])
    cmds.button(l="| Import Values QUAD |", c=lambda *args: show_attribute_exporter_ui(), h=30, bgc=[0.3, 0.3, 0.3], w=10)
    cmds.button(l="| WIKI |", c=linkWiki, h=30, bgc=[0.2, 0.2, 0.2], w=15)

    cmds.columnLayout(adj=1, p=col, bgc=[0.2, 0.2, 0.2])
    cmds.showWindow()


def show_attribute_exporter_ui():
    global attribute_exporter_ui

    if attribute_exporter_ui is not None and attribute_exporter_ui.isVisible():
        attribute_exporter_ui.raise_()
        return

    # Chiudi la finestra se è già presente
    if cmds.window("AttributeExporterUI", exists=True):
        cmds.deleteUI("AttributeExporterUI", window=True)

    attribute_exporter_ui = AttributeExporterUI()
    attribute_exporter_ui.show()


attribute_exporter_ui = None

AB_poseReaderQuad()
