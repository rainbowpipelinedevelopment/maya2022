# -*- coding: utf-8 -*-

#author: Michela Cerrone

#this script works to make support joints on body and fingers, this help the deformation to be more realistic.


import maya.cmds as cmds
import maya.mel as mel
import maya.utils
from maya import OpenMayaUI as omui
import re
import os
from functools import partial

# -----------------------------------------------------------------
# Path
# -----------------------------------------------------------------
localpipe = os.getenv("LOCALPY")
myPath = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "CorrectiveJoints")
image_path = os.path.join(myPath, "icons")


# -----------------------------------------------------------------
# PySide
# -----------------------------------------------------------------
try:
    from shiboken2 import wrapInstance
except ImportError:
    from shiboken import wrapInstance

try:
    from PySide2.QtGui import QIcon
    from PySide2.QtWidgets import QWidget
except ImportError:
    from PySide.QtGui import QIcon, QWidget


def create_vector_product(input1_attr, operation, input2_attr, output_attr):
    vector_product_node = cmds.createNode('vectorProduct')
    cmds.setAttr(vector_product_node + '.operation', operation)

    # Sostituisci gli underscore con 'X' nel nome lungo
    long_name = output_attr.replace('_', 'X')

    # Rimuovi caratteri non validi nel nome lungo
    long_name = ''.join(char if char.isalnum() or char in ('_', ' ') else '' for char in long_name)

    # Collega gli attributi di input
    cmds.connectAttr(input1_attr, '{}.input1'.format(vector_product_node), force=True)
    cmds.connectAttr(input2_attr, '{}.input2'.format(vector_product_node), force=True)

    # Crea l'attributo di output personalizzato se non esiste
    if not cmds.attributeQuery(long_name, node=vector_product_node, exists=True):
        cmds.addAttr(vector_product_node, longName=long_name, attributeType='float3', keyable=True)
        cmds.addAttr(vector_product_node, longName=long_name + 'X', attributeType='float', parent=long_name,
                     keyable=True)
        cmds.addAttr(vector_product_node, longName=long_name + 'Y', attributeType='float', parent=long_name,
                     keyable=True)
        cmds.addAttr(vector_product_node, longName=long_name + 'Z', attributeType='float', parent=long_name,
                     keyable=True)

    return vector_product_node


def create_multiply_divide(node_name, operation):
    # Crea il nodo multiplyDivide
    multiply_divide_node = cmds.createNode('multiplyDivide', name=node_name)

    # Imposta l'operazione del nodo multiplyDivide
    cmds.setAttr(multiply_divide_node + '.operation', operation)

    return multiply_divide_node


def create_condition(node_name, operation):
    # Crea il nodo condition
    condition_node = cmds.createNode('condition', name=node_name)

    # Imposta l'operazione del nodo condition
    cmds.setAttr(condition_node + '.operation', operation)

    return condition_node


def process_joint(joint_name, operations):
    if not cmds.objExists(joint_name) or cmds.nodeType(joint_name) != "joint":
        print("Il joint {} non esiste o non è un joint.".format(joint_name))
        return

    for operation in operations:
        operation(joint_name)


def duplicate_and_move_translation_locator(locator_name):
    translation_locator_02 = cmds.duplicate(locator_name)[0]
    translation_locator_name = cmds.rename(translation_locator_02,
                                           locator_name.replace("_Corr_Position_loc", "_Corr_Rotation_loc"))

    joint_rotation_translate = cmds.xform(translation_locator_name, query=True, translation=True, worldSpace=True)
    joint_rotation_rotate = cmds.xform(translation_locator_name, query=True, rotation=True, worldSpace=True)

    cmds.xform(translation_locator_name, translation=joint_rotation_translate, worldSpace=True)
    cmds.xform(translation_locator_name, rotation=joint_rotation_rotate, worldSpace=True)

    cmds.move(2.5, 0, 0, translation_locator_name, relative=True, objectSpace=True)
    cmds.parent(translation_locator_name, locator_name)

    print("Creato locator di traslazione per {}: {}".format(locator_name, translation_locator_name))

    return translation_locator_name


def create_and_match_joint(locator_name):
    joint_tmp = cmds.joint()

    # Rinomina il joint come il locator con alcune modifiche
    joint_name = cmds.rename(joint_tmp, locator_name.replace("_Corr_Position_loc", "_Corr_Driver_jnt"))

    # Match delle trasformate con il locator
    cmds.matchTransform(joint_name, locator_name, pos=True, rot=True)

    # Ottieni posizione e orientamento del joint
    joint_translate = cmds.xform(joint_name, query=True, translation=True, worldSpace=True)
    joint_rotate = cmds.xform(joint_name, query=True, rotation=True, worldSpace=True)

    # Sposta il joint in base all'orientamento
    cmds.move(-2.5, 0, 0, joint_name, relative=True, objectSpace=True)

    # Congela le rotazioni del joint
    cmds.makeIdentity(joint_name, apply=True, rotate=True)

    print("Creato joint per {}: {}".format(locator_name, joint_name))

    return joint_name, joint_translate


def create_controls(joint_name, rotation_joint_name, locator_name, global_father_name):
    control_tmp = cmds.circle(n=rotation_joint_name.replace("_Corr_Weight_jnt", "_Corr_Ctrl"), r=7)[0]
    replace_sh(control_tmp)
    control_father = cmds.group(em=True)
    control_father_name = cmds.rename(control_father, control_tmp.replace("_Corr_Ctrl", "_Corr_Ctrl_Grp"))
    cmds.matchTransform(control_father_name, control_tmp)
    cmds.parent(control_tmp, control_father_name)
    control_father_offset = cmds.group(em=True)
    control_father_name_offset = cmds.rename(control_father_offset,
                                             control_tmp.replace("_Corr_Ctrl", "_Corr_Ctrl_Offset_Grp"))
    cmds.matchTransform(control_father_name_offset, control_tmp)
    cmds.parent(control_father_name, control_father_name_offset)
    cmds.DeleteHistory(control_tmp)
    cmds.matchTransform(control_father_name_offset, rotation_joint_name)
    cmds.parentConstraint(control_tmp, rotation_joint_name, mo=True)
    cmds.scaleConstraint(control_tmp, rotation_joint_name, mo=True)

    control_one = cmds.duplicate(control_tmp, n=rotation_joint_name.replace("_Corr_Weight_jnt", "_Corr_Driver_Ctrl"))
    cmds.parent(control_one, w=True)
    control_one_father_name = cmds.group(em=True,
                                         n=rotation_joint_name.replace("_Corr_Weight_jnt", "_Corr_Driver_Ctrl_Grp"))
    cmds.matchTransform(control_one_father_name, control_one)
    cmds.parent(control_one, control_one_father_name)
    control_one_father_offset = cmds.group(em=True)
    control_one_father_name_offset = cmds.rename(control_one_father_offset,
                                                 rotation_joint_name.replace("_Corr_Weight_jnt",
                                                                             "_Corr_Driver_Ctrl_Offset_Grp"))
    cmds.matchTransform(control_one_father_name_offset, control_one)
    cmds.parent(control_one_father_name, control_one_father_name_offset)
    cmds.DeleteHistory(control_one)
    cmds.matchTransform(control_one_father_name_offset, joint_name)
    cmds.parentConstraint(control_one, joint_name, mo=True)
    cmds.scaleConstraint(control_one, joint_name, mo=True)
    cmds.parent(control_father_name_offset, control_one)
    cmds.parent(control_one_father_name_offset, global_father_name)
    control_one_shape = cmds.listRelatives(control_one, shapes=True, pa=True)
    cmds.setAttr('{}.visibility'.format(control_one_shape[0]), 0)
    cmds.setAttr('{}.visibility'.format(control_tmp), lock=True, keyable=False)

    return control_tmp, control_father_name, control_one_father_name


def parent_joint_fn(joint_name, joint_rotation_translate, rotation_joint_name, locator_name):
    cmds.parent(joint_name, world=True, relative=False)
    cmds.parent(rotation_joint_name, joint_name)
    joint_father = cmds.group(em=True)
    joint_father_name = cmds.rename(joint_father, locator_name.replace("_Corr_Position_loc", "_Corr_Joints_Grp"))
    cmds.matchTransform(joint_father_name, joint_name)
    cmds.parent(joint_name, joint_father_name)
    global_father = cmds.group(em=True)
    global_father_name = cmds.rename(joint_father, locator_name.replace("_Corr_Position_loc", "_Corr_Grp"))
    locator_father = cmds.group(em=True)
    locator_father_name = cmds.rename(locator_father, locator_name.replace("_Corr_Position_loc", "_Corr_Locator_Grp"))
    cmds.matchTransform(locator_father_name, locator_name)
    cmds.parent(locator_name, locator_father_name)
    cmds.parent(joint_father_name, global_father_name)
    cmds.parent(locator_father_name, global_father_name)

    a, b, control_one_father_name = create_controls(joint_name, rotation_joint_name, locator_name, global_father_name)

    print (joint_name)
    print (joint_father_name)

    return global_father_name, control_one_father_name


def duplicate_and_match_rotation_joint(joint_name, locator_name, joint_rotation_translate):
    # Duplica il joint precedentemente creato
    print (joint_name)
    rotation_joint = cmds.duplicate(joint_name)[0]
    print (rotation_joint)

    # Match delle trasformate con il locator di posizione
    cmds.matchTransform(rotation_joint, locator_name, pos=True, rot=True)

    # Rinomina il secondo joint aggiungendo un suffisso diverso
    rotation_joint_name = cmds.rename(rotation_joint, joint_name.replace("_Corr_Driver_jnt", "_Corr_Weight_jnt"))
    print (rotation_joint_name)

    # Congela solo le rotazioni del joint
    cmds.makeIdentity(rotation_joint_name, apply=True, rotate=True)

    print("Creato joint di rotazione per {}: {}".format(joint_name, rotation_joint_name))

    a, control_one_father_name = parent_joint_fn(joint_name, joint_rotation_translate, rotation_joint_name,
                                                 locator_name)

    return rotation_joint_name, joint_name, control_one_father_name


def create_locator(joint_name):
    locator_suffix = "_Corr_Position_loc"

    locator_tmp = cmds.spaceLocator()[0]

    joint_translate = cmds.xform(joint_name, query=True, translation=True, worldSpace=True)
    joint_rotate = cmds.xform(joint_name, query=True, rotation=True, worldSpace=True)

    cmds.xform(locator_tmp, translation=joint_translate, worldSpace=True)
    cmds.xform(locator_tmp, rotation=joint_rotate, worldSpace=True)

    # Rimuovi il prefisso "LOC_" dal nome del locator
    locator_name = cmds.rename(locator_tmp, joint_name.replace("_Corr", locator_suffix).replace("LOC_", ""))
    print("Creato locator per {}: {}".format(joint_name, locator_name))

    translation_locator_name = duplicate_and_move_translation_locator(locator_name)

    joint_created, joint_rotation_translate = create_and_match_joint(locator_name)

    # Crea un altro joint duplicandolo dal precedente
    a, b, control_one_father_name = duplicate_and_match_rotation_joint(joint_created, locator_name,
                                                                       joint_rotation_translate)

    return locator_name, translation_locator_name, joint_created, control_one_father_name


def saveSel():
    return cmds.ls(selection=True)


def replace_sh(control_tmp):
    # Verifica se gli oggetti esistono prima di selezionarli
    if not cmds.objExists("Arrow_Curve") or not cmds.objExists(control_tmp):
        print("Uno o entrambi gli oggetti non esistono.")
        return

    for control_name in control_tmp:
        cmds.select("Arrow_Curve", control_tmp)
        sel = saveSel()

        for i in range(len(sel) - 1):
            cnt = cmds.duplicate(sel[0])[0]  # Aggiunto [0] per ottenere il primo elemento della lista
            oldcnt = sel[i + 1]

            if cmds.objExists(cnt):
                child = cmds.listRelatives(cnt, c=True, pa=True)
                child_shp = cmds.listRelatives(cnt, shapes=True, pa=True)

                for i in child:
                    if i not in child_shp:
                        cmds.delete(i)

                cnt = cmds.rename(cnt, "NEW")
                newshape = cmds.listRelatives(cnt, shapes=True, pa=True)
                oldshape = cmds.listRelatives(oldcnt, shapes=True, pa=True)

                for a in oldshape:
                    cmds.delete(a)

                if newshape and oldshape:
                    newname = str(cmds.rename(newshape[0], str(oldshape[0])))
                    cmds.select(newname, oldcnt)
                    mel.eval("parent -r -s;")

    delete_new_objects()

    return


def delete_new_objects():
    new_objects = cmds.ls("NEW*")
    if new_objects:
        cmds.delete(new_objects)
        print("Eliminati gli oggetti 'NEW' dalla scena.")
    else:
        print("Nessun oggetto 'NEW' trovato nella scena.")


def integrate_function(locator_name, translation_locator_name, joint_name, control_one_father_name):
    print("Locator Name:", locator_name)
    print("Translation Locator Name:", translation_locator_name)

    # Verifica la presenza dell'oggetto
    if not cmds.objExists(locator_name):
        print("Error: Locator '{}' does not exist.".format(locator_name))
        return

    # Verifica la presenza dell'attributo translate sull'oggetto
    if not cmds.attributeQuery('translate', node=locator_name, exists=True):
        print("Error: Attribute 'translate' not found on locator '{}'.".format(locator_name))
        return

    # Crea i nodi Vector Product
    position_vp = create_vector_product('{}.translate'.format(locator_name), 1,
                                        '{}.translate'.format(translation_locator_name),
                                        '{}.output'.format('VP_{}_PositionRotation'.format(locator_name)))
    rotation_vp = create_vector_product('{}.translate'.format(translation_locator_name), 1,
                                        '{}.translate'.format(translation_locator_name),
                                        '{}.output'.format('VP_{}_Rotation'.format(translation_locator_name)))

    # Collega i nodi Vector Product ai locator
    cmds.connectAttr(locator_name + '.translate', position_vp + '.input1', force=True)
    cmds.connectAttr(translation_locator_name + '.translate', position_vp + '.input2', force=True)

    cmds.connectAttr(translation_locator_name + '.translate', rotation_vp + '.input1', force=True)
    cmds.connectAttr(translation_locator_name + '.translate', rotation_vp + '.input2', force=True)

    # Disconnetti la translate del locator dall'input 1 del rotation_vp
    cmds.disconnectAttr(translation_locator_name + '.translate', rotation_vp + '.input1')

    # Copia i valori numerici dall'input 2 all'input 1 del rotation_Vp
    translation_values = cmds.getAttr(translation_locator_name + '.translate')[0]
    cmds.setAttr(rotation_vp + '.input1', translation_values[0], translation_values[1], translation_values[2])

    # Crea i nodi MultiplyDivide
    rotation_divide_node = create_multiply_divide('MLD_{}_Rotation_Divide'.format(locator_name),
                                                  2)  # 2 corrisponde a divide
    rotation_multiply_node = create_multiply_divide('MLD_{}_Rotation_Multiply'.format(locator_name),
                                                    1)  # 1 corrisponde a multiply

    # Collega i nodi MultiplyDivide
    cmds.connectAttr(position_vp + '.output', rotation_divide_node + '.input1', force=True)
    cmds.connectAttr(rotation_vp + '.output', rotation_divide_node + '.input2', force=True)

    # Rinomina il primo MultiplyDivide
    cmds.rename(rotation_divide_node, 'MLD_{}_Rotation_Divide'.format(locator_name))

    # Collega il secondo MultiplyDivide
    cmds.setAttr(rotation_multiply_node + '.input1X', 4)
    cmds.setAttr(rotation_multiply_node + '.input1Y', 0)
    cmds.setAttr(rotation_multiply_node + '.input1Z', 4)

    # Imposta il secondo MultiplyDivide su multiply
    cmds.setAttr(rotation_multiply_node + '.operation', 1)  # 1 corrisponde a multiply

    # Collega l'output del primo MultiplyDivide al secondo
    cmds.connectAttr(rotation_divide_node + '.output', rotation_multiply_node + '.input2', force=True)

    # Crea il joint solo se non esiste già uno con lo stesso nome
    if not cmds.objExists(joint_name):
        joint_name, joint_rotation_translate = create_and_match_joint(locator_name)

    # Crea i nodi Condition
    condition_x = create_condition('CND_{}_Translate_X'.format(locator_name), 2)  # 2 corrisponde a less than
    condition_y = create_condition('CND_{}_Translate_Y'.format(locator_name), 2)  # 2 corrisponde a less than
    condition_z = create_condition('CND_{}_Translate_Z'.format(locator_name), 2)  # 2 corrisponde a less than

    # Collegamento dei nodi MultiplyDivide ai nodi Condition
    cmds.connectAttr(rotation_multiply_node + '.outputX', condition_x + '.firstTerm', force=True)
    cmds.connectAttr(rotation_multiply_node + '.outputY', condition_y + '.firstTerm', force=True)
    cmds.connectAttr(rotation_multiply_node + '.outputZ', condition_z + '.firstTerm', force=True)

    # Collegamento degli output X, Y, Z ai colorIfFalseR dei nodi Condition
    cmds.connectAttr(rotation_multiply_node + '.outputX', condition_x + '.colorIfFalseR', force=True)
    cmds.connectAttr(rotation_multiply_node + '.outputY', condition_y + '.colorIfFalseR', force=True)
    cmds.connectAttr(rotation_multiply_node + '.outputZ', condition_z + '.colorIfFalseR', force=True)

    # Crea i nodi Condition aggiuntivi con suffisso _Zero
    condition_x_zero = create_condition('CND_{}_Translate_X_Zero'.format(locator_name), 4)  # 2 corrisponde a less than
    condition_y_zero = create_condition('CND_{}_Translate_Y_Zero'.format(locator_name), 4)  # 2 corrisponde a less than
    condition_z_zero = create_condition('CND_{}_Translate_Z_Zero'.format(locator_name), 4)  # 2 corrisponde a less than

    # Collegamento degli output X, Y, Z ai firstTerm dei nuovi Condition
    cmds.connectAttr(condition_x + '.outColorR', condition_x_zero + '.firstTerm', force=True)
    cmds.connectAttr(condition_y + '.outColorR', condition_y_zero + '.firstTerm', force=True)
    cmds.connectAttr(condition_z + '.outColorR', condition_z_zero + '.firstTerm', force=True)

    # Collegamento degli output X, Y, Z ai colorIfFalseR dei nuovi Condition
    cmds.connectAttr(condition_x + '.outColorR', condition_x_zero + '.colorIfFalseR', force=True)
    cmds.connectAttr(condition_y + '.outColorR', condition_y_zero + '.colorIfFalseR', force=True)
    cmds.connectAttr(condition_z + '.outColorR', condition_z_zero + '.colorIfFalseR', force=True)

    set = cmds.sets(condition_x, condition_y, condition_z, condition_x_zero, condition_z_zero, condition_y_zero,
                    rotation_multiply_node, n='SET_{}_SliderNODE'.format(locator_name))

    # Colleghiamo le condizioni zero ai canali di traslazione del joint creato
    cmds.connectAttr(condition_x_zero + '.outColorR', '{}.translateX'.format(control_one_father_name), force=True)
    cmds.connectAttr(condition_y_zero + '.outColorR', '{}.translateY'.format(control_one_father_name), force=True)
    cmds.connectAttr(condition_z_zero + '.outColorR', '{}.translateZ'.format(control_one_father_name), force=True)


def process(joint_list):
    joint_list = [
        "LOC_L_Shoulder_Low_Corr", "LOC_L_Shoulder_Up_Corr", "LOC_L_Shoulder_Back_Corr",
        "LOC_L_Elbow_Int_Corr", "LOC_L_Elbow_Ext_Corr", "LOC_L_Wrist_Ext_Corr",
        "LOC_L_Shoulder_Front_Corr", "LOC_L_legRear_Knee_Ext_Corr", "LOC_L_legRear_Ankle_Int_Corr",
        "LOC_L_legRear_Up_Ext_Corr", "LOC_L_legRear_Knee_Int_Corr", "LOC_L_legRear_Ankle_Ext_Corr",
        "LOC_L_legRear_Up_Int_Corr", "LOC_L_legRear_SIDE_Corr", "LOC_R_Shoulder_Front_Corr",
        "LOC_R_Shoulder_Low_Corr", "LOC_R_Shoulder_Up_Corr", "LOC_R_Shoulder_Back_Corr",
        "LOC_R_Elbow_Int_Corr", "LOC_R_Elbow_Ext_Corr", "LOC_R_Wrist_Ext_Corr",
        "LOC_R_legRear_Knee_Int_Corr", "LOC_R_legRear_Knee_Ext_Corr", "LOC_R_legRear_Up_Ext_Corr",
        "LOC_R_legRear_Ankle_Ext_Corr", "LOC_R_legRear_Ankle_Int_Corr", "LOC_R_legRear_Up_Int_Corr",
        "LOC_R_legRear_SIDE_Corr"
    ]

    for joint_name in joint_list:
        # Controllo se l'oggetto esiste nella scena
        if not cmds.objExists(joint_name):
            print("Oggetto '{}' non esiste. Passo al prossimo.".format(joint_name))
            continue  # Salta questa iterazione se l'oggetto non esiste

        # Se l'oggetto esiste, esegui la funzione di creazione del locator
        locator_name, translation_locator_name, joint_created, control_one_father_name = create_locator(joint_name)

        # Integra la funzione sui locator e controlli creati
        integrate_function(locator_name, translation_locator_name, joint_created, control_one_father_name)


# -----------------------------------------------------------------------------------------------------------------------------------

# fingers--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

def create_vector_product_fingers(input1_attr_fingers, operation_fingers, input2_attr_fingers, output_attr_fingers):
    vector_product_node_fingers = cmds.createNode('vectorProduct')
    cmds.setAttr(vector_product_node_fingers + '.operation', operation_fingers)

    long_name_fingers = output_attr_fingers.replace('_', 'X')

    # Rimuovi caratteri non validi nel nome lungo
    long_name_fingers = ''.join(char if char.isalnum() or char in ('_', ' ') else '' for char in long_name_fingers)

    cmds.connectAttr(input1_attr_fingers, '{}.input1'.format(vector_product_node_fingers), force=True)
    cmds.connectAttr(input2_attr_fingers, '{}.input2'.format(vector_product_node_fingers), force=True)

    if not cmds.attributeQuery(long_name_fingers, node=vector_product_node_fingers, exists=True):
        cmds.addAttr(vector_product_node_fingers, longName=long_name_fingers, attributeType='float3', keyable=True)
        cmds.addAttr(vector_product_node_fingers, longName=long_name_fingers + 'X', attributeType='float',
                     parent=long_name_fingers, keyable=True)
        cmds.addAttr(vector_product_node_fingers, longName=long_name_fingers + 'Y', attributeType='float',
                     parent=long_name_fingers, keyable=True)
        cmds.addAttr(vector_product_node_fingers, longName=long_name_fingers + 'Z', attributeType='float',
                     parent=long_name_fingers, keyable=True)

    return vector_product_node_fingers


def create_multiply_divide_fingers(node_name_fingers, operation_fingers):
    # Crea il nodo multiplyDivide
    multiply_divide_node_fingers = cmds.createNode('multiplyDivide', name=node_name_fingers)

    # Imposta l'operazione del nodo multiplyDivide
    cmds.setAttr(multiply_divide_node_fingers + '.operation', operation_fingers)

    return multiply_divide_node_fingers


def create_condition_fingers(node_name_fingers, operation_fingers):
    # Crea il nodo condition
    condition_node_fingers = cmds.createNode('condition', name=node_name_fingers)

    # Imposta l'operazione del nodo condition
    cmds.setAttr(condition_node_fingers + '.operation', operation_fingers)

    return condition_node_fingers


def process_joint_fingers(joint_name_fingers, operations_fingers):
    if not cmds.objExists(joint_name_fingers) or cmds.nodeType(joint_name_fingers) != "joint":
        print("Il joint {} non esiste o non è un joint.".format(joint_name_fingers))
        return

    for operation_fingers in operations_fingers:
        operation_fingers(joint_name_fingers)


def create_locator_fingers(joint_name_fingers):
    locator_suffix_fingers = "_Corr_Position_loc"

    locator_tmp_fingers = cmds.spaceLocator()[0]

    joint_translate_fingers = cmds.xform(joint_name_fingers, query=True, translation=True, worldSpace=True)
    joint_rotate_fingers = cmds.xform(joint_name_fingers, query=True, rotation=True, worldSpace=True)

    cmds.xform(locator_tmp_fingers, translation=joint_translate_fingers, worldSpace=True)
    cmds.xform(locator_tmp_fingers, rotation=joint_rotate_fingers, worldSpace=True)

    # Rimuovi il prefisso "LOC_" dal nome del locator
    locator_name_fingers = cmds.rename(locator_tmp_fingers,
                                       joint_name_fingers.replace("_Corr", locator_suffix_fingers).replace("LOC_", ""))
    print("Creato locator per {}: {}".format(joint_name_fingers, locator_name_fingers))

    translation_locator_name_fingers = duplicate_and_move_translation_locator_fingers(locator_name_fingers)

    joint_created_fingers, joint_rotation_translate_fingers = create_and_match_joint_fingers(locator_name_fingers)

    # Crea un altro joint duplicandolo dal precedente
    duplicate_and_match_rotation_joint_fingers(joint_created_fingers, locator_name_fingers,
                                               joint_rotation_translate_fingers)

    return locator_name_fingers, translation_locator_name_fingers, joint_created_fingers


def duplicate_and_move_translation_locator_fingers(locator_name_fingers):
    translation_locator_02_fingers = cmds.duplicate(locator_name_fingers)[0]
    translation_locator_name_fingers = cmds.rename(translation_locator_02_fingers,
                                                   locator_name_fingers.replace("_Corr_Position_loc",
                                                                                "_Corr_Rotation_loc"))

    joint_rotation_translate_fingers = cmds.xform(translation_locator_name_fingers, query=True, translation=True,
                                                  worldSpace=True)
    joint_rotation_rotate_fingers = cmds.xform(translation_locator_name_fingers, query=True, rotation=True,
                                               worldSpace=True)

    cmds.xform(translation_locator_name_fingers, translation=joint_rotation_translate_fingers, worldSpace=True)
    cmds.xform(translation_locator_name_fingers, rotation=joint_rotation_rotate_fingers, worldSpace=True)

    cmds.move(1, 0, 0, translation_locator_name_fingers, relative=True, objectSpace=True)
    cmds.parent(translation_locator_name_fingers, locator_name_fingers)

    print("Creato locator di traslazione per {}: {}".format(locator_name_fingers, translation_locator_name_fingers))

    return translation_locator_name_fingers


def create_and_match_joint_fingers(locator_name_fingers):
    joint_tmp_fingers = cmds.joint()

    # Rinomina il joint come il locator con alcune modifiche
    joint_name_fingers = cmds.rename(joint_tmp_fingers,
                                     locator_name_fingers.replace("_Corr_Position_loc", "_Corr_Driver_jnt"))

    # Match delle trasformate con il locator
    cmds.matchTransform(joint_name_fingers, locator_name_fingers, pos=True, rot=True)

    # Ottieni posizione e orientamento del joint
    joint_translate_fingers = cmds.xform(joint_name_fingers, query=True, translation=True, worldSpace=True)
    joint_rotate_fingers = cmds.xform(joint_name_fingers, query=True, rotation=True, worldSpace=True)

    # Sposta il joint in base all'orientamento
    cmds.move(-1, 0, 0, joint_name_fingers, relative=True, objectSpace=True)

    # Congela le rotazioni del joint
    cmds.makeIdentity(joint_name_fingers, apply=True, rotate=True)

    print("Creato joint per {}: {}".format(locator_name_fingers, joint_name_fingers))

    return joint_name_fingers, joint_translate_fingers


def duplicate_and_match_rotation_joint_fingers(joint_name_fingers, locator_name_fingers,
                                               joint_rotation_translate_fingers):
    # Duplica il joint precedentemente creato
    print (joint_name_fingers)
    rotation_joint_fingers = cmds.duplicate(joint_name_fingers)[0]
    print (rotation_joint_fingers)

    # Match delle trasformate con il locator di posizione
    cmds.matchTransform(rotation_joint_fingers, locator_name_fingers, pos=True, rot=True)

    # Rinomina il secondo joint aggiungendo un suffisso diverso
    rotation_joint_name_fingers = cmds.rename(rotation_joint_fingers,
                                              joint_name_fingers.replace("_Corr_Driver_jnt", "_Corr_Weight_jnt"))
    print (rotation_joint_name_fingers)

    # Congela solo le rotazioni del joint
    cmds.makeIdentity(rotation_joint_name_fingers, apply=True, rotate=True)

    print("Creato joint di rotazione per {}: {}".format(joint_name_fingers, rotation_joint_name_fingers))

    parent_joint_fn_fingers(joint_name_fingers, joint_rotation_translate_fingers, rotation_joint_name_fingers,
                            locator_name_fingers)

    return rotation_joint_name_fingers, joint_name_fingers


def parent_joint_fn_fingers(joint_name_fingers, joint_rotation_translate_fingers, rotation_joint_name_fingers,
                            locator_name_fingers):
    cmds.parent(joint_name_fingers, world=True, relative=False)
    cmds.parent(rotation_joint_name_fingers, joint_name_fingers)
    joint_father_fingers = cmds.group(em=True)
    joint_father_name_fingers = cmds.rename(joint_father_fingers,
                                            locator_name_fingers.replace("_Corr_Position_loc", "_Corr_Joints_Grp"))
    cmds.matchTransform(joint_father_name_fingers, joint_name_fingers)
    cmds.parent(joint_name_fingers, joint_father_name_fingers)
    global_father_fingers = cmds.group(em=True)
    global_father_name_fingers = cmds.rename(joint_father_fingers,
                                             locator_name_fingers.replace("_Corr_Position_loc", "_Corr_Grp"))
    locator_father_fingers = cmds.group(em=True)
    locator_father_name_fingers = cmds.rename(locator_father_fingers,
                                              locator_name_fingers.replace("_Corr_Position_loc", "_Corr_Locator_Grp"))
    cmds.matchTransform(locator_father_name_fingers, locator_name_fingers)
    cmds.parent(locator_name_fingers, locator_father_name_fingers)
    cmds.parent(joint_father_name_fingers, global_father_name_fingers)
    cmds.parent(locator_father_name_fingers, global_father_name_fingers)

    print (joint_name_fingers)
    print (joint_father_name_fingers)

    return global_father_name_fingers


def delete_new_objects():
    new_objects = cmds.ls("NEW*")
    if new_objects:
        cmds.delete(new_objects)
        print("Eliminati gli oggetti 'NEW' dalla scena.")
    else:
        print("Nessun oggetto 'NEW' trovato nella scena.")


def integrate_function_fingers(locator_name_fingers, translation_locator_name_fingers, joint_name_fingers):
    print("Locator Name:", locator_name_fingers)
    print("Translation Locator Name:", translation_locator_name_fingers)

    # Verifica la presenza dell'oggetto
    if not cmds.objExists(locator_name_fingers):
        print("Error: Locator '{}' does not exist.".format(locator_name_fingers))
        return

    # Verifica la presenza dell'attributo translate sull'oggetto
    if not cmds.attributeQuery('translate', node=locator_name_fingers, exists=True):
        print("Error: Attribute 'translate' not found on locator '{}'.".format(locator_name_fingers))
        return

    # Crea i nodi Vector Product
    position_vp_fingers = create_vector_product_fingers('{}.translate'.format(locator_name_fingers), 1,
                                                        '{}.translate'.format(translation_locator_name_fingers),
                                                        '{}.output'.format(
                                                            'VP_{}_PositionRotation'.format(locator_name_fingers)))
    rotation_vp_fingers = create_vector_product_fingers('{}.translate'.format(translation_locator_name_fingers), 1,
                                                        '{}.translate'.format(translation_locator_name_fingers),
                                                        '{}.output'.format(
                                                            'VP_{}_Rotation'.format(translation_locator_name_fingers)))

    # Collega i nodi Vector Product ai locator
    cmds.connectAttr(locator_name_fingers + '.translate', position_vp_fingers + '.input1', force=True)
    cmds.connectAttr(translation_locator_name_fingers + '.translate', position_vp_fingers + '.input2', force=True)

    cmds.connectAttr(translation_locator_name_fingers + '.translate', rotation_vp_fingers + '.input1', force=True)
    cmds.connectAttr(translation_locator_name_fingers + '.translate', rotation_vp_fingers + '.input2', force=True)

    # Disconnetti la translate del locator dall'input 1 del rotation_vp
    cmds.disconnectAttr(translation_locator_name_fingers + '.translate', rotation_vp_fingers + '.input1')

    # Copia i valori numerici dall'input 2 all'input 1 del rotation_Vp
    translation_values_fingers = cmds.getAttr(translation_locator_name_fingers + '.translate')[0]
    cmds.setAttr(rotation_vp_fingers + '.input1', translation_values_fingers[0], translation_values_fingers[1],
                 translation_values_fingers[2])

    # Crea i nodi MultiplyDivide
    rotation_divide_node_fingers = create_multiply_divide('MLD_{}_Rotation_Divide'.format(locator_name_fingers),
                                                          2)  # 2 corrisponde a divide
    rotation_multiply_node_fingers = create_multiply_divide('MLD_{}_Rotation_Multiply'.format(locator_name_fingers),
                                                            1)  # 1 corrisponde a multiply

    # Collega i nodi MultiplyDivide
    cmds.connectAttr(position_vp_fingers + '.output', rotation_divide_node_fingers + '.input1', force=True)
    cmds.connectAttr(rotation_vp_fingers + '.output', rotation_divide_node_fingers + '.input2', force=True)

    # Rinomina il primo MultiplyDivide
    cmds.rename(rotation_divide_node_fingers, 'MLD_{}_Rotation_Divide'.format(locator_name_fingers))

    # Collega il secondo MultiplyDivide
    cmds.setAttr(rotation_multiply_node_fingers + '.input1X', 1)
    cmds.setAttr(rotation_multiply_node_fingers + '.input1Y', 0)
    cmds.setAttr(rotation_multiply_node_fingers + '.input1Z', 0.5)

    # Imposta il secondo MultiplyDivide su multiply
    cmds.setAttr(rotation_multiply_node_fingers + '.operation', 1)  # 1 corrisponde a multiply

    # Collega l'output del primo MultiplyDivide al secondo
    cmds.connectAttr(rotation_divide_node_fingers + '.output', rotation_multiply_node_fingers + '.input2', force=True)

    # Crea il joint solo se non esiste già uno con lo stesso nome
    if not cmds.objExists(joint_name_fingers):
        joint_name_fingers, joint_rotation_translate_fingers = create_and_match_joint_fingers(locator_name_fingers)

    # Crea i nodi Condition
    condition_x_fingers = create_condition('CND_{}_Translate_X'.format(locator_name_fingers),
                                           2)  # 2 corrisponde a less than
    condition_y_fingers = create_condition('CND_{}_Translate_Y'.format(locator_name_fingers),
                                           2)  # 2 corrisponde a less than
    condition_z_fingers = create_condition('CND_{}_Translate_Z'.format(locator_name_fingers),
                                           2)  # 2 corrisponde a less than

    cmds.setAttr(condition_x_fingers + '.secondTerm', 2.000)
    cmds.setAttr(condition_x_fingers + '.colorIfTrueR', 2.000)
    cmds.setAttr(condition_z_fingers + '.secondTerm', 1.000)
    cmds.setAttr(condition_z_fingers + '.colorIfTrueR', 1.000)

    # Collegamento dei nodi MultiplyDivide ai nodi Condition
    cmds.connectAttr(rotation_multiply_node_fingers + '.outputX', condition_x_fingers + '.firstTerm', force=True)
    cmds.connectAttr(rotation_multiply_node_fingers + '.outputY', condition_y_fingers + '.firstTerm', force=True)
    cmds.connectAttr(rotation_multiply_node_fingers + '.outputZ', condition_z_fingers + '.firstTerm', force=True)

    # Collegamento degli output X, Y, Z ai colorIfFalseR dei nodi Condition
    cmds.connectAttr(rotation_multiply_node_fingers + '.outputX', condition_x_fingers + '.colorIfFalseR', force=True)
    cmds.connectAttr(rotation_multiply_node_fingers + '.outputY', condition_y_fingers + '.colorIfFalseR', force=True)
    cmds.connectAttr(rotation_multiply_node_fingers + '.outputZ', condition_z_fingers + '.colorIfFalseR', force=True)

    # Crea i nodi Condition aggiuntivi con suffisso _Zero
    condition_x_zero_fingers = create_condition('CND_{}_Translate_X_Zero'.format(locator_name_fingers),
                                                4)  # 2 corrisponde a less than
    condition_y_zero_fingers = create_condition('CND_{}_Translate_Y_Zero'.format(locator_name_fingers),
                                                4)  # 2 corrisponde a less than
    condition_z_zero_fingers = create_condition('CND_{}_Translate_Z_Zero'.format(locator_name_fingers),
                                                4)  # 2 corrisponde a less than

    # Collegamento degli output X, Y, Z ai firstTerm dei nuovi Condition
    cmds.connectAttr(condition_x_fingers + '.outColorR', condition_x_zero_fingers + '.firstTerm', force=True)
    cmds.connectAttr(condition_y_fingers + '.outColorR', condition_y_zero_fingers + '.firstTerm', force=True)
    cmds.connectAttr(condition_z_fingers + '.outColorR', condition_z_zero_fingers + '.firstTerm', force=True)

    # Collegamento degli output X, Y, Z ai colorIfFalseR dei nuovi Condition
    cmds.connectAttr(condition_x_fingers + '.outColorR', condition_x_zero_fingers + '.colorIfFalseR', force=True)
    cmds.connectAttr(condition_y_fingers + '.outColorR', condition_y_zero_fingers + '.colorIfFalseR', force=True)
    cmds.connectAttr(condition_z_fingers + '.outColorR', condition_z_zero_fingers + '.colorIfFalseR', force=True)

    # Colleghiamo le condizioni zero ai canali di traslazione del joint creato
    cmds.connectAttr(condition_x_zero_fingers + '.outColorR', '{}.translateX'.format(joint_name_fingers), force=True)
    cmds.connectAttr(condition_y_zero_fingers + '.outColorR', '{}.translateY'.format(joint_name_fingers), force=True)
    cmds.connectAttr(condition_z_zero_fingers + '.outColorR', '{}.translateZ'.format(joint_name_fingers), force=True)


def process_fingers(joint_list_fingers):
    joint_list_fingers = ["LOC_L_IndexBase_Corr", "LOC_L_IndexMid_Corr", "LOC_L_IndexTop_Corr",
                          "LOC_L_IndexBase_Int_Corr", "LOC_L_MiddleBase_Corr", "LOC_L_MiddleMid_Corr",
                          "LOC_L_MiddleTop_Corr", "LOC_L_MiddleBase_Int_Corr", "LOC_L_RingBase_Corr",
                          "LOC_L_RingMid_Corr", "LOC_L_RingTop_Corr", "LOC_L_RingBase_Int_Corr", "LOC_L_PinkyBase_Corr",
                          "LOC_L_PinkyMid_Corr", "LOC_L_PinkyTop_Corr", "LOC_L_PinkyBase_Int_Corr",
                          "LOC_L_ThumbBase_Corr", "LOC_L_ThumbMid_Corr", "LOC_R_IndexBase_Corr", "LOC_R_IndexMid_Corr",
                          "LOC_R_IndexTop_Corr", "LOC_R_IndexBase_Int_Corr", "LOC_R_MiddleBase_Corr",
                          "LOC_R_MiddleMid_Corr", "LOC_R_MiddleTop_Corr", "LOC_R_MiddleBase_Int_Corr",
                          "LOC_R_RingBase_Corr", "LOC_R_RingMid_Corr", "LOC_R_RingTop_Corr", "LOC_R_RingBase_Int_Corr",
                          "LOC_R_PinkyBase_Corr", "LOC_R_PinkyMid_Corr", "LOC_R_PinkyTop_Corr",
                          "LOC_R_PinkyBase_Int_Corr", "LOC_R_ThumbBase_Corr", "LOC_R_ThumbMid_Corr"]

    for joint_name_fingers in joint_list_fingers:
        # Controlla se l'oggetto esiste nella scena
        if not cmds.objExists(joint_name_fingers):
            print("Oggetto '{}' non esiste. Passo al prossimo.".format(joint_name_fingers))
            continue  # Salta questa iterazione se l'oggetto non esiste

        # Se l'oggetto esiste, esegui la funzione di creazione del locator
        locator_name_fingers, translation_locator_name_fingers, joint_created_fingers = create_locator_fingers(
            joint_name_fingers)

        # Integra la funzione sui locator e controlli creati
        integrate_function_fingers(locator_name_fingers, translation_locator_name_fingers, joint_created_fingers)

# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


def DoubleParentMatrix(*args):
    def create_offset_matrix_attributes(obj, target_name, suffix):
        attr1_name = "{}_{}_OffsetMatrix_01".format(target_name, suffix)
        attr2_name = "{}_{}_OffsetMatrix_02".format(target_name, suffix)

        if cmds.attributeQuery(attr1_name, node=obj, exists=True) or cmds.attributeQuery(attr2_name, node=obj,
                                                                                         exists=True):
            cmds.warning("Gli attributi {} e/o {} esistono già.".format(attr1_name, attr2_name))
            return attr1_name, attr2_name

        cmds.addAttr(obj, longName=attr1_name, attributeType='matrix', usedAsColor=False, keyable=False)
        cmds.addAttr(obj, longName=attr2_name, attributeType='matrix', usedAsColor=False, keyable=False)

        return attr1_name, attr2_name

    def calculate_offset_matrix(source_obj, target_obj, result_obj, attr1_name, attr2_name, suffix):
        mult_matrix_node_1 = cmds.createNode('multMatrix', name="{}_{}_Delete1".format(source_obj, suffix))
        mult_matrix_node_2 = cmds.createNode('multMatrix', name="{}_{}_Delete2".format(target_obj, suffix))

        cmds.connectAttr("{}.worldMatrix[0]".format(target_obj), "{}.matrixIn[0]".format(mult_matrix_node_1))
        cmds.connectAttr("{}.worldInverseMatrix[0]".format(source_obj), "{}.matrixIn[1]".format(mult_matrix_node_1))
        cmds.connectAttr("{}.matrixSum".format(mult_matrix_node_1), "{}.{}".format(result_obj, attr1_name))

        cmds.connectAttr("{}.worldMatrix[0]".format(target_obj), "{}.matrixIn[0]".format(mult_matrix_node_2))
        cmds.connectAttr("{}.worldInverseMatrix[0]".format(selected_objects[1]),
                         "{}.matrixIn[1]".format(mult_matrix_node_2))
        cmds.connectAttr("{}.matrixSum".format(mult_matrix_node_2), "{}.{}".format(result_obj, attr2_name))

        cmds.disconnectAttr("{}.matrixSum".format(mult_matrix_node_1), "{}.{}".format(result_object, attr1_name))
        cmds.disconnectAttr("{}.matrixSum".format(mult_matrix_node_2), "{}.{}".format(result_object, attr2_name))

        cmds.delete(mult_matrix_node_1)
        cmds.delete(mult_matrix_node_2)

        mult_matrix_node_3 = cmds.createNode('multMatrix', name="MM_{}_OffsetMatrix".format(selected_objects[0]))
        mult_matrix_node_4 = cmds.createNode('multMatrix', name="MM_{}_OffsetMatrix".format(selected_objects[1]))

        cmds.connectAttr("{}.{}".format(result_object, attr1_name), "{}.matrixIn[0]".format(mult_matrix_node_3))
        cmds.connectAttr("{}.worldMatrix[0]".format(selected_objects[0]), "{}.matrixIn[1]".format(mult_matrix_node_3))
        cmds.connectAttr("{}.parentInverseMatrix[0]".format(selected_objects[2]),
                         "{}.matrixIn[2]".format(mult_matrix_node_3))

        cmds.connectAttr("{}.{}".format(result_object, attr2_name), "{}.matrixIn[0]".format(mult_matrix_node_4))
        cmds.connectAttr("{}.worldMatrix[0]".format(selected_objects[1]), "{}.matrixIn[1]".format(mult_matrix_node_4))
        cmds.connectAttr("{}.parentInverseMatrix[0]".format(selected_objects[2]),
                         "{}.matrixIn[2]".format(mult_matrix_node_4))

        decompose_matrix_node_1 = cmds.createNode('decomposeMatrix',
                                                  name="DM_{}_OffsetMatrix".format(selected_objects[0]))
        decompose_matrix_node_2 = cmds.createNode('decomposeMatrix',
                                                  name="DM_{}_OffsetMatrix".format(selected_objects[1]))

        cmds.connectAttr("{}.matrixSum".format(mult_matrix_node_3), "{}.inputMatrix".format(decompose_matrix_node_1))

        cmds.connectAttr("{}.matrixSum".format(mult_matrix_node_4), "{}.inputMatrix".format(decompose_matrix_node_2))

        multiply_divide_node_1 = cmds.createNode('multiplyDivide', name="MLD_{}_Position".format(selected_objects[0]))
        multiply_divide_node_2 = cmds.createNode('multiplyDivide', name="MLD_{}_Position".format(selected_objects[1]))

        cmds.setAttr("{}.input2".format(multiply_divide_node_1), 0.5, 0.5, 0.5)
        cmds.setAttr("{}.input2".format(multiply_divide_node_2), 0.5, 0.5, 0.5)

        cmds.connectAttr("{}.outputTranslate".format(decompose_matrix_node_1),
                         "{}.input1".format(multiply_divide_node_1))

        cmds.connectAttr("{}.outputTranslate".format(decompose_matrix_node_2),
                         "{}.input1".format(multiply_divide_node_2))

        plus_minus_node = cmds.createNode('plusMinusAverage', name="PMA_Position")

        cmds.connectAttr("{}.output".format(multiply_divide_node_1), "{}.input3D[0]".format(plus_minus_node))
        cmds.connectAttr("{}.output".format(multiply_divide_node_2), "{}.input3D[1]".format(plus_minus_node))

        cmds.connectAttr("{}.output3D".format(plus_minus_node), "{}.translate".format(selected_objects[2]))

    selected_objects = cmds.ls(selection=True, type='transform')

    if len(selected_objects) == 4:
        source_object = selected_objects[0]
        target_object = selected_objects[2]
        result_object = selected_objects[3]
        suffix = "Offset"

        attr1_name, attr2_name = create_offset_matrix_attributes(result_object, target_object, suffix)

        calculate_offset_matrix(source_object, target_object, result_object, attr1_name, attr2_name, suffix)

        print(
            "Attributi di offset matrix creati su {} basati su {} e {}. MultMatrix creati e collegati agli attributi di offset.".format(
                result_object, source_object, target_object))
    else:
        cmds.warning("Seleziona esattamente quattro oggetti prima di eseguire lo script.")

    return


# BODY_FINALIZE____________________________________________________________________________________________________________________________________________________________________________________________________________

def check_and_apply_double_parent_matrix(selection):
    if all(cmds.objExists(obj) for obj in selection):
        cmds.select(selection)
        DoubleParentMatrix(selection)

    return

def finalize(*args):
    # Correzione: Passa le liste direttamente alla funzione


    l_shoulderlow_position = ["L_clavicle_jnt", "L_upArmWeight_jnt", "L_Shoulder_Low_Corr_Position_loc", "L_Shoulder_Low_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(l_shoulderlow_position)

    l_shoulderlow_rotation = ["L_clavicle_jnt", "L_upArmWeight_jnt", "L_Shoulder_Low_Corr_Rotation_loc", "L_Shoulder_Low_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(l_shoulderlow_rotation)

    l_shoulderup_position = ["L_clavicle_jnt", "L_upArmWeight_jnt", "L_Shoulder_Up_Corr_Position_loc", "L_Shoulder_Up_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(l_shoulderup_position)

    l_shoulderup_rotation = ["L_clavicle_jnt", "L_upArmWeight_jnt", "L_Shoulder_Up_Corr_Rotation_loc", "L_Shoulder_Up_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(l_shoulderup_rotation)

    l_shoulderback_position = ["L_clavicle_jnt", "L_upArmWeight_jnt", "L_Shoulder_Back_Corr_Position_loc", "L_Shoulder_Back_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(l_shoulderback_position)

    l_shoulderback_rotation = ["L_clavicle_jnt", "L_upArmWeight_jnt", "L_Shoulder_Back_Corr_Rotation_loc", "L_Shoulder_Back_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(l_shoulderback_rotation)

    l_shoulderfront_position = ["L_clavicle_jnt", "L_upArmWeight_jnt", "L_Shoulder_Front_Corr_Position_loc", "L_Shoulder_Front_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(l_shoulderfront_position)

    l_shoulderfront_rotation = ["L_clavicle_jnt", "L_upArmWeight_jnt", "L_Shoulder_Front_Corr_Rotation_loc", "L_Shoulder_Front_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(l_shoulderfront_rotation)

    l_elbowint_position = ["L_upArmWeightSplit_4_jnt", "L_elbowWeight_jnt", "L_Elbow_Int_Corr_Position_loc", "L_Elbow_Int_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(l_elbowint_position)

    l_elbowint_rotation = ["L_upArmWeightSplit_4_jnt", "L_elbowWeight_jnt", "L_Elbow_Int_Corr_Rotation_loc", "L_Elbow_Int_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(l_elbowint_rotation)

    l_elbowext_position = ["L_upArmWeightSplit_4_jnt", "L_elbowWeight_jnt", "L_Elbow_Ext_Corr_Position_loc", "L_Elbow_Ext_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(l_elbowext_position)

    l_elbowext_rotation = ["L_upArmWeightSplit_4_jnt", "L_elbowWeight_jnt", "L_Elbow_Ext_Corr_Rotation_loc", "L_Elbow_Ext_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(l_elbowext_rotation)

    l_wrist_position = ["L_elbowWeightSplit_4_jnt", "L_hand_jnt", "L_Wrist_Ext_Corr_Position_loc", "L_Wrist_Ext_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(l_wrist_position)

    l_wrist_rotation = ["L_elbowWeightSplit_4_jnt", "L_hand_jnt", "L_Wrist_Ext_Corr_Rotation_loc", "L_Wrist_Ext_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(l_wrist_rotation)

    r_shoulderlow_position = ["R_clavicle_jnt", "R_upArmWeight_jnt", "R_Shoulder_Low_Corr_Position_loc", "R_Shoulder_Low_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(r_shoulderlow_position)

    r_shoulderlow_rotation = ["R_clavicle_jnt", "R_upArmWeight_jnt", "R_Shoulder_Low_Corr_Rotation_loc", "R_Shoulder_Low_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(r_shoulderlow_rotation)

    r_shoulderup_position = ["R_clavicle_jnt", "R_upArmWeight_jnt", "R_Shoulder_Up_Corr_Position_loc", "R_Shoulder_Up_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(r_shoulderup_position)

    r_shoulderup_rotation = ["R_clavicle_jnt", "R_upArmWeight_jnt", "R_Shoulder_Up_Corr_Rotation_loc", "R_Shoulder_Up_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(r_shoulderup_rotation)

    r_shoulderback_position = ["R_clavicle_jnt", "R_upArmWeight_jnt", "R_Shoulder_Back_Corr_Position_loc", "R_Shoulder_Back_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(r_shoulderback_position)

    r_shoulderback_rotation = ["R_clavicle_jnt", "R_upArmWeight_jnt", "R_Shoulder_Back_Corr_Rotation_loc", "R_Shoulder_Back_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(r_shoulderback_rotation)

    r_shoulderfront_position = ["R_clavicle_jnt", "R_upArmWeight_jnt", "R_Shoulder_Front_Corr_Position_loc", "R_Shoulder_Front_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(r_shoulderfront_position)

    r_shoulderfront_rotation = ["R_clavicle_jnt", "R_upArmWeight_jnt", "R_Shoulder_Front_Corr_Rotation_loc", "R_Shoulder_Front_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(r_shoulderfront_rotation)

    r_elbowint_position = ["R_upArmWeightSplit_4_jnt", "R_elbowWeight_jnt", "R_Elbow_Int_Corr_Position_loc", "R_Elbow_Int_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(r_elbowint_position)

    r_elbowint_rotation = ["R_upArmWeightSplit_4_jnt", "R_elbowWeight_jnt", "R_Elbow_Int_Corr_Rotation_loc", "R_Elbow_Int_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(r_elbowint_rotation)

    r_elbowext_position = ["R_upArmWeightSplit_4_jnt", "R_elbowWeight_jnt", "R_Elbow_Ext_Corr_Position_loc", "R_Elbow_Ext_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(r_elbowext_position)

    r_elbowext_rotation = ["R_upArmWeightSplit_4_jnt", "R_elbowWeight_jnt", "R_Elbow_Ext_Corr_Rotation_loc", "R_Elbow_Ext_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(r_elbowext_rotation)

    r_wrist_position = ["R_elbowWeightSplit_4_jnt", "R_hand_jnt", "R_Wrist_Ext_Corr_Position_loc", "R_Wrist_Ext_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(r_wrist_position)

    r_wrist_rotation = ["R_elbowWeightSplit_4_jnt", "R_hand_jnt", "R_Wrist_Ext_Corr_Rotation_loc", "R_Wrist_Ext_Corr_Locator_Grp"]
    check_and_apply_double_parent_matrix(r_wrist_rotation)

    check_and_apply_double_parent_matrix(
        ["neck_Ctrl", "spineHigh_Ctrl", "Neck_Low_Corr_Position_loc", "Neck_Low_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["neck_Ctrl", "spineHigh_Ctrl", "Neck_Low_Corr_Rotation_loc", "Neck_Low_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["neck_Ctrl", "spineHigh_Ctrl", "Neck_Up_Corr_Position_loc", "Neck_Up_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["neck_Ctrl", "spineHigh_Ctrl", "Neck_Up_Corr_Rotation_loc", "Neck_Up_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["neck_Ctrl", "spineHigh_Ctrl", "L_Neck_Corr_Position_loc", "L_Neck_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["neck_Ctrl", "spineHigh_Ctrl", "L_Neck_Corr_Rotation_loc", "L_Neck_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["neck_Ctrl", "spineHigh_Ctrl", "R_Neck_Corr_Position_loc", "R_Neck_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["neck_Ctrl", "spineHigh_Ctrl", "R_Neck_Corr_Rotation_loc", "R_Neck_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_front_Upleg_jnt", "L_front_hip1_bendy_1_jnt", "L_legFront_Up_Int_Corr_Position_loc",
         "L_legFront_Up_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_front_Upleg_jnt", "L_front_hip1_bendy_1_jnt", "L_legFront_Up_Int_Corr_Rotation_loc",
         "L_legFront_Up_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_front_Upleg_jnt", "L_front_hip1_bendy_1_jnt", "L_legFront_Up_Ext_Corr_Position_loc",
         "L_legFront_Up_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_front_Upleg_jnt", "L_front_hip1_bendy_1_jnt", "L_legFront_Up_Ext_Corr_Rotation_loc",
         "L_legFront_Up_Ext_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_front_hip1_bendy_5_jnt", "L_front_hip2_bendy_1_jnt", "L_legFront_Knee_Ext_Corr_Position_loc",
         "L_legFront_Knee_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_front_hip1_bendy_5_jnt", "L_front_hip2_bendy_1_jnt", "L_legFront_Knee_Ext_Corr_Rotation_loc",
         "L_legFront_Knee_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_front_hip1_bendy_5_jnt", "L_front_hip2_bendy_1_jnt", "L_legFront_Knee_Int_Corr_Position_loc",
         "L_legFront_Knee_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_front_hip1_bendy_5_jnt", "L_front_hip2_bendy_1_jnt", "L_legFront_Knee_Int_Corr_Rotation_loc",
         "L_legFront_Knee_Int_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_front_knee1_jnt", "L_front_ankle_jnt", "L_legFront_Ankle_Ext_Corr_Position_loc",
         "L_legFront_Ankle_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_front_knee1_jnt", "L_front_ankle_jnt", "L_legFront_Ankle_Ext_Corr_Rotation_loc",
         "L_legFront_Ankle_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_front_knee1_jnt", "L_front_ankle_jnt", "L_legFront_Ankle_Int_Corr_Position_loc",
         "L_legFront_Ankle_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_front_knee1_jnt", "L_front_ankle_jnt", "L_legFront_Ankle_Int_Corr_Rotation_loc",
         "L_legFront_Ankle_Int_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_rear_Upleg_jnt", "L_rear_hip1_bendy_1_jnt", "L_legRear_Up_Int_Corr_Position_loc",
         "L_legRear_Up_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_rear_Upleg_jnt", "L_rear_hip1_bendy_1_jnt", "L_legRear_Up_Int_Corr_Rotation_loc",
         "L_legRear_Up_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_rear_Upleg_jnt", "L_rear_hip1_bendy_1_jnt", "L_legRear_Up_Ext_Corr_Position_loc",
         "L_legRear_Up_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_rear_Upleg_jnt", "L_rear_hip1_bendy_1_jnt", "L_legRear_Up_Ext_Corr_Rotation_loc",
         "L_legRear_Up_Ext_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_rear_hip1_bendy_5_jnt", "L_rear_hip2_bendy_1_jnt", "L_legRear_Knee_Ext_Corr_Position_loc",
         "L_legRear_Knee_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_rear_hip1_bendy_5_jnt", "L_rear_hip2_bendy_1_jnt", "L_legRear_Knee_Ext_Corr_Rotation_loc",
         "L_legRear_Knee_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_rear_hip1_bendy_5_jnt", "L_rear_hip2_bendy_1_jnt", "L_legRear_Knee_Int_Corr_Position_loc",
         "L_legRear_Knee_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_rear_hip1_bendy_5_jnt", "L_rear_hip2_bendy_1_jnt", "L_legRear_Knee_Int_Corr_Rotation_loc",
         "L_legRear_Knee_Int_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_rear_knee1_jnt", "L_rear_ankle_jnt", "L_legRear_Ankle_Ext_Corr_Position_loc",
         "L_legRear_Ankle_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_rear_knee1_jnt", "L_rear_ankle_jnt", "L_legRear_Ankle_Ext_Corr_Rotation_loc",
         "L_legRear_Ankle_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_rear_knee1_jnt", "L_rear_ankle_jnt", "L_legRear_Ankle_Int_Corr_Position_loc",
         "L_legRear_Ankle_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_rear_knee1_jnt", "L_rear_ankle_jnt", "L_legRear_Ankle_Int_Corr_Rotation_loc",
         "L_legRear_Ankle_Int_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_front_Upleg_jnt", "R_front_hip1_bendy_1_jnt", "R_legFront_Up_Int_Corr_Position_loc",
         "R_legFront_Up_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_front_Upleg_jnt", "R_front_hip1_bendy_1_jnt", "R_legFront_Up_Int_Corr_Rotation_loc",
         "R_legFront_Up_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_front_Upleg_jnt", "R_front_hip1_bendy_1_jnt", "R_legFront_Up_Ext_Corr_Position_loc",
         "R_legFront_Up_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_front_Upleg_jnt", "R_front_hip1_bendy_1_jnt", "R_legFront_Up_Ext_Corr_Rotation_loc",
         "R_legFront_Up_Ext_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_front_hip1_bendy_5_jnt", "R_front_hip2_bendy_1_jnt", "R_legFront_Knee_Ext_Corr_Position_loc",
         "R_legFront_Knee_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_front_hip1_bendy_5_jnt", "R_front_hip2_bendy_1_jnt", "R_legFront_Knee_Ext_Corr_Rotation_loc",
         "R_legFront_Knee_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_front_hip1_bendy_5_jnt", "R_front_hip2_bendy_1_jnt", "R_legFront_Knee_Int_Corr_Position_loc",
         "R_legFront_Knee_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_front_hip1_bendy_5_jnt", "R_front_hip2_bendy_1_jnt", "R_legFront_Knee_Int_Corr_Rotation_loc",
         "R_legFront_Knee_Int_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_front_knee1_jnt", "R_front_ankle_jnt", "R_legFront_Ankle_Ext_Corr_Position_loc",
         "R_legFront_Ankle_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_front_knee1_jnt", "R_front_ankle_jnt", "R_legFront_Ankle_Ext_Corr_Rotation_loc",
         "R_legFront_Ankle_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_front_knee1_jnt", "R_front_ankle_jnt", "R_legFront_Ankle_Int_Corr_Position_loc",
         "R_legFront_Ankle_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_front_knee1_jnt", "R_front_ankle_jnt", "R_legFront_Ankle_Int_Corr_Rotation_loc",
         "R_legFront_Ankle_Int_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_rear_Upleg_jnt", "R_rear_hip1_bendy_1_jnt", "R_legRear_Up_Int_Corr_Position_loc",
         "R_legRear_Up_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_rear_Upleg_jnt", "R_rear_hip1_bendy_1_jnt", "R_legRear_Up_Int_Corr_Rotation_loc",
         "R_legRear_Up_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_rear_Upleg_jnt", "R_rear_hip1_bendy_1_jnt", "R_legRear_Up_Ext_Corr_Position_loc",
         "R_legRear_Up_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_rear_Upleg_jnt", "R_rear_hip1_bendy_1_jnt", "R_legRear_Up_Ext_Corr_Rotation_loc",
         "R_legRear_Up_Ext_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_rear_hip1_bendy_5_jnt", "R_rear_hip2_bendy_1_jnt", "R_legRear_Knee_Ext_Corr_Position_loc",
         "R_legRear_Knee_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_rear_hip1_bendy_5_jnt", "R_rear_hip2_bendy_1_jnt", "R_legRear_Knee_Ext_Corr_Rotation_loc",
         "R_legRear_Knee_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_rear_hip1_bendy_5_jnt", "R_rear_hip2_bendy_1_jnt", "R_legRear_Knee_Int_Corr_Position_loc",
         "R_legRear_Knee_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_rear_hip1_bendy_5_jnt", "R_rear_hip2_bendy_1_jnt", "R_legRear_Knee_Int_Corr_Rotation_loc",
         "R_legRear_Knee_Int_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_rear_knee1_jnt", "R_rear_ankle_jnt", "R_legRear_Ankle_Ext_Corr_Position_loc",
         "R_legRear_Ankle_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_rear_knee1_jnt", "R_rear_ankle_jnt", "R_legRear_Ankle_Ext_Corr_Rotation_loc",
         "R_legRear_Ankle_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_rear_knee1_jnt", "R_rear_ankle_jnt", "R_legRear_Ankle_Int_Corr_Position_loc",
         "R_legRear_Ankle_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_rear_knee1_jnt", "R_rear_ankle_jnt", "R_legRear_Ankle_Int_Corr_Rotation_loc",
         "R_legRear_Ankle_Int_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["hip_jnt", "L_hip1_bendy_1_jnt", "L_legRear_Up_Int_Corr_Position_loc",
         "L_legRear_Up_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["hip_jnt", "L_hip1_bendy_1_jnt", "L_legRear_Up_Int_Corr_Rotation_loc",
         "L_legRear_Up_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["hip_jnt", "L_hip1_bendy_1_jnt", "L_legRear_Up_Ext_Corr_Position_loc",
         "L_legRear_Up_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["hip_jnt", "L_hip1_bendy_1_jnt", "L_legRear_Up_Ext_Corr_Rotation_loc",
         "L_legRear_Up_Ext_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_hip1_bendy_5_jnt", "L_hip2_bendy_1_jnt", "L_legRear_Knee_Ext_Corr_Position_loc",
         "L_legRear_Knee_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_hip1_bendy_5_jnt", "L_hip2_bendy_1_jnt", "L_legRear_Knee_Ext_Corr_Rotation_loc",
         "L_legRear_Knee_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_hip1_bendy_5_jnt", "L_hip2_bendy_1_jnt", "L_legRear_Knee_Int_Corr_Position_loc",
         "L_legRear_Knee_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_hip1_bendy_5_jnt", "L_hip2_bendy_1_jnt", "L_legRear_Knee_Int_Corr_Rotation_loc",
         "L_legRear_Knee_Int_Corr_Locator_Grp"])


    check_and_apply_double_parent_matrix(
        ["L_knee1_bendy_5_jnt", "L_ankle_jnt", "L_legRear_Ankle_Ext_Corr_Position_loc",
         "L_legRear_Ankle_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_knee1_bendy_5_jnt", "L_ankle_jnt", "L_legRear_Ankle_Ext_Corr_Rotation_loc",
         "L_legRear_Ankle_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_knee1_bendy_5_jnt", "L_ankle_jnt", "L_legRear_Ankle_Int_Corr_Position_loc",
         "L_legRear_Ankle_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_knee1_bendy_5_jnt", "L_ankle_jnt", "L_legRear_Ankle_Int_Corr_Rotation_loc",
         "L_legRear_Ankle_Int_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["hip_jnt", "R_hip1_bendy_1_jnt", "R_legRear_Up_Int_Corr_Position_loc",
         "R_legRear_Up_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["hip_jnt", "R_hip1_bendy_1_jnt", "R_legRear_Up_Int_Corr_Rotation_loc",
         "R_legRear_Up_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["hip_jnt", "R_hip1_bendy_1_jnt", "R_legRear_Up_Ext_Corr_Position_loc",
         "R_legRear_Up_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["hip_jnt", "R_hip1_bendy_1_jnt", "R_legRear_Up_Ext_Corr_Rotation_loc",
         "R_legRear_Up_Ext_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_hip1_bendy_5_jnt", "R_hip2_bendy_1_jnt", "R_legRear_Knee_Ext_Corr_Position_loc",
         "R_legRear_Knee_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_hip1_bendy_5_jnt", "R_hip2_bendy_1_jnt", "R_legRear_Knee_Ext_Corr_Rotation_loc",
         "R_legRear_Knee_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_hip1_bendy_5_jnt", "R_hip2_bendy_1_jnt", "R_legRear_Knee_Int_Corr_Position_loc",
         "R_legRear_Knee_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_hip1_bendy_5_jnt", "R_hip2_bendy_1_jnt", "R_legRear_Knee_Int_Corr_Rotation_loc",
         "R_legRear_Knee_Int_Corr_Locator_Grp"])


    check_and_apply_double_parent_matrix(
        ["R_knee1_bendy_5_jnt", "R_ankle_jnt", "R_legRear_Ankle_Ext_Corr_Position_loc",
         "R_legRear_Ankle_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_knee1_bendy_5_jnt", "R_ankle_jnt", "R_legRear_Ankle_Ext_Corr_Rotation_loc",
         "R_legRear_Ankle_Ext_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_knee1_bendy_5_jnt", "R_ankle_jnt", "R_legRear_Ankle_Int_Corr_Position_loc",
         "R_legRear_Ankle_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_knee1_bendy_5_jnt", "R_ankle_jnt", "R_legRear_Ankle_Int_Corr_Rotation_loc",
         "R_legRear_Ankle_Int_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["hip_jnt", "L_hip1_bendy_1_jnt", "L_legRear_SIDE_Corr_Position_loc",
         "L_legRear_SIDE_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["hip_jnt", "L_hip1_bendy_1_jnt", "L_legRear_SIDE_Corr_Rotation_loc",
         "L_legRear_SIDE_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["hip_jnt", "R_hip1_bendy_1_jnt", "R_legRear_SIDE_Corr_Position_loc",
         "R_legRear_SIDE_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["hip_jnt", "R_hip1_bendy_1_jnt", "R_legRear_SIDE_Corr_Rotation_loc",
         "R_legRear_SIDE_Corr_Locator_Grp"])


    cmds.delete("Arrow_Curve")
    cmds.delete("L_Side_Correctrive")
    cmds.delete("R_Side_Correctrive")

    # Funzione di supporto per filtrare gli oggetti esistenti
    def filter_existing_objects(object_list):
        return [obj for obj in object_list if cmds.objExists(obj)]

    # Creazione dei gruppi per il lato sinistro, raggruppando solo gli oggetti esistenti
    left_groups = [
        "L_BackLeg_Corr_Grp", "L_Knee_Ext_Corr_Grp", "L_Knee_Int_Corr_Grp", "L_UpLeg_Corr_Grp",
        "L_Shoulder_Low_Corr_Grp", "L_Shoulder_Up_Corr_Grp", "L_Shoulder_Back_Corr_Grp",
        "L_Elbow_Int_Corr_Grp", "L_Elbow_Ext_Corr_Grp", "L_Wrist_Ext_Corr_Grp", "L_Shoulder_Front_Corr_Grp"
        "L_legFront_Knee_Ext_Corr_Grp", "L_legFront_Knee_Int_Corr_Grp", "R_legRear_Up_Int_Corr_Grp",
        "L_legFront_Ankle_Ext_Corr_Grp", "L_Neck_Corr_Grp", "L_legFront_Up_Int_Corr_Grp",
        "L_legFront_Up_Ext_Corr_Grp", "L_legFront_Ankle_Int_Corr_Grp", "L_legRear_Knee_Ext_Corr_Grp",
        "L_legRear_Ankle_Int_Corr_Grp", "L_legRear_Up_Ext_Corr_Grp", "L_legRear_Knee_Int_Corr_Grp",
        "L_legRear_Ankle_Ext_Corr_Grp", "L_legRear_Up_Int_Corr_Grp", "Neck_Low_Corr_Grp", "Neck_Up_Corr_Grp", 'L_Shoulder_Front_Corr_Grp',
        'L_legRear_SIDE_Corr_Grp'
    ]

    filtered_left_groups = filter_existing_objects(left_groups)
    if filtered_left_groups:
        cmds.group(*filtered_left_groups, n="L_Side_Corrective_Grp_RIG")

    # Creazione dei gruppi per il lato destro, raggruppando solo gli oggetti esistenti
    right_groups = [
        "R_BackLeg_Corr_Grp", "R_Knee_Ext_Corr_Grp", "R_Knee_Int_Corr_Grp", "R_UpLeg_Corr_Grp",
        "R_Shoulder_Low_Corr_Grp", "R_Shoulder_Up_Corr_Grp", "R_Shoulder_Back_Corr_Grp",
        "R_Elbow_Int_Corr_Grp", "R_Elbow_Ext_Corr_Grp", "R_Wrist_Ext_Corr_Grp", "R_Shoulder_Front_Corr_Grp"
        "R_legRear_Up_Int_Corr_Grp", "R_legFront_Knee_Ext_Corr_Grp", "R_legFront_Knee_Int_Corr_Grp",
        "R_legFront_Ankle_Ext_Corr_Grp", "R_Neck_Corr_Grp", "R_legFront_Up_Int_Corr_Grp",
        "R_legFront_Up_Ext_Corr_Grp", "R_legFront_Ankle_Int_Corr_Grp", "R_legRear_Knee_Int_Corr_Grp",
        "R_legRear_Knee_Ext_Corr_Grp", "R_legRear_Up_Ext_Corr_Grp", "R_legRear_Ankle_Ext_Corr_Grp",
        "R_legRear_Ankle_Int_Corr_Grp", "R_legRear_Up_Int_Corr_Grp", 'R_Shoulder_Front_Corr_Grp',
        'R_legRear_SIDE_Corr_Grp'
    ]

    filtered_right_groups = filter_existing_objects(right_groups)
    if filtered_right_groups:
        cmds.group(*filtered_right_groups, n="R_Side_Corrective_Grp_RIG")

    # Oggetti da nascondere per il lato sinistro
    oggetti_da_nascondere_l = (
        'L_BackLeg_Corr_Joints_Grp', 'L_BackLeg_Corr_Locator_Grp',
        'L_Knee_Ext_Corr_Joints_Grp', 'L_Knee_Ext_Corr_Locator_Grp',
        'L_Knee_Int_Corr_Joints_Grp', 'L_Knee_Int_Corr_Locator_Grp',
        'L_UpLeg_Corr_Joints_Grp', 'L_UpLeg_Corr_Locator_Grp',
        'L_Shoulder_Low_Corr_Joints_Grp', 'L_Shoulder_Low_Corr_Locator_Grp',
        'L_Shoulder_Up_Corr_Joints_Grp', 'L_Shoulder_Up_Corr_Locator_Grp',
        'L_Shoulder_Back_Corr_Joints_Grp', 'L_Shoulder_Back_Corr_Locator_Grp',
        'L_Elbow_Int_Corr_Joints_Grp', 'L_Elbow_Int_Corr_Locator_Grp',
        'L_Elbow_Ext_Corr_Joints_Grp', 'L_Elbow_Ext_Corr_Locator_Grp',
        'L_Wrist_Ext_Corr_Joints_Grp', 'L_Wrist_Ext_Corr_Locator_Grp',
        'L_Shoulder_Front_Corr_Joints_Grp', 'L_Shoulder_Front_Corr_Locator_Grp'
        'L_legFront_Knee_Ext_Corr_Joints_Grp', 'L_legFront_Knee_Ext_Corr_Locator_Grp', 'L_legFront_Knee_Int_Corr_Joints_Grp',
        'L_legFront_Knee_Int_Corr_Locator_Grp', 'L_legFront_Ankle_Ext_Corr_Joints_Grp', 'L_legFront_Ankle_Ext_Corr_Locator_Grp',
        'Neck_Low_Corr_Joints_Grp', 'Neck_Low_Corr_Locator_Grp', 'Neck_Up_Corr_Joints_Grp', 'Neck_Up_Corr_Locator_Grp',
        'L_Neck_Corr_Joints_Grp', 'L_Neck_Corr_Locator_Grp', 'L_legFront_Up_Int_Corr_Joints_Grp', 'L_legFront_Up_Int_Corr_Locator_Grp',
        'L_legFront_Up_Ext_Corr_Joints_Grp', 'L_legFront_Up_Ext_Corr_Locator_Grp', 'L_legFront_Ankle_Int_Corr_Joints_Grp',
        'L_legFront_Ankle_Int_Corr_Locator_Grp', 'L_legRear_Knee_Ext_Corr_Joints_Grp', 'L_legRear_Knee_Ext_Corr_Locator_Grp',
        'L_legRear_Ankle_Int_Corr_Joints_Grp', 'L_legRear_Ankle_Int_Corr_Locator_Grp', 'L_legRear_Up_Ext_Corr_Joints_Grp',
        'L_legRear_Up_Ext_Corr_Locator_Grp', 'L_legRear_Knee_Int_Corr_Joints_Grp', 'L_legRear_Knee_Int_Corr_Locator_Grp',
        'L_legRear_Ankle_Ext_Corr_Joints_Grp', 'L_legRear_Ankle_Ext_Corr_Locator_Grp', 'L_legRear_Up_Int_Corr_Joints_Grp',
        'L_legRear_Up_Int_Corr_Locator_Grp','L_Shoulder_Front_Corr_Locator_Grp','L_legRear_SIDE_Corr_Joints_Grp', 'L_legRear_SIDE_Corr_Locator_Grp'
    )

    # Nascondi solo gli oggetti esistenti per il lato sinistro
    for obj in oggetti_da_nascondere_l:
        if cmds.objExists(obj):
            cmds.hide(obj)

    # Oggetti da nascondere per il lato destro
    oggetti_da_nascondere_r = (
        'R_BackLeg_Corr_Joints_Grp', 'R_BackLeg_Corr_Locator_Grp',
        'R_Knee_Ext_Corr_Joints_Grp', 'R_Knee_Ext_Corr_Locator_Grp',
        'R_Knee_Int_Corr_Joints_Grp', 'R_Knee_Int_Corr_Locator_Grp',
        'R_UpLeg_Corr_Joints_Grp', 'R_UpLeg_Corr_Locator_Grp',
        'R_Shoulder_Low_Corr_Joints_Grp', 'R_Shoulder_Low_Corr_Locator_Grp',
        'R_Shoulder_Up_Corr_Joints_Grp', 'R_Shoulder_Up_Corr_Locator_Grp',
        'R_Shoulder_Back_Corr_Joints_Grp', 'R_Shoulder_Back_Corr_Locator_Grp',
        'R_Elbow_Int_Corr_Joints_Grp', 'R_Elbow_Int_Corr_Locator_Grp',
        'R_Elbow_Ext_Corr_Joints_Grp', 'R_Elbow_Ext_Corr_Locator_Grp',
        'R_Wrist_Ext_Corr_Joints_Grp', 'R_Wrist_Ext_Corr_Locator_Grp',
        'R_Shoulder_Front_Corr_Joints_Grp', 'R_Shoulder_Front_Corr_Locator_Grp'
        'R_legFront_Knee_Ext_Corr_Joints_Grp', 'R_legFront_Knee_Ext_Corr_Locator_Grp', 'R_legFront_Knee_Int_Corr_Joints_Grp',
        'R_legFront_Knee_Int_Corr_Locator_Grp', 'R_legFront_Ankle_Ext_Corr_Joints_Grp', 'R_legFront_Ankle_Ext_Corr_Locator_Grp',
        'R_Neck_Corr_Joints_Grp', 'R_Neck_Corr_Locator_Grp', 'R_legFront_Up_Int_Corr_Joints_Grp', 'R_legFront_Up_Int_Corr_Locator_Grp',
        'R_legFront_Up_Ext_Corr_Joints_Grp', 'R_legFront_Up_Ext_Corr_Locator_Grp', 'R_legFront_Ankle_Int_Corr_Joints_Grp',
        'R_legFront_Ankle_Int_Corr_Locator_Grp', 'R_legRear_Knee_Ext_Corr_Joints_Grp', 'R_legRear_Knee_Ext_Corr_Locator_Grp',
        'R_legRear_Ankle_Int_Corr_Joints_Grp', 'R_legRear_Ankle_Int_Corr_Locator_Grp', 'R_legRear_Up_Ext_Corr_Joints_Grp',
        'R_legRear_Up_Ext_Corr_Locator_Grp', 'R_legRear_Knee_Int_Corr_Joints_Grp', 'R_legRear_Knee_Int_Corr_Locator_Grp',
        'R_legRear_Ankle_Ext_Corr_Joints_Grp', 'R_legRear_Ankle_Ext_Corr_Locator_Grp', 'R_legRear_Up_Int_Corr_Joints_Grp',
        'R_legRear_Up_Int_Corr_Locator_Grp','R_Shoulder_Front_Corr_Locator_Grp','R_legRear_SIDE_Corr_Joints_Grp', 'R_legRear_SIDE_Corr_Locator_Grp'
    )

    # Nascondi solo gli oggetti esistenti per il lato destro
    for obj in oggetti_da_nascondere_r:
        if cmds.objExists(obj):
            cmds.hide(obj)


    # Parent e scale constraint con condizioni
    if cmds.objExists("hip_jnt") and cmds.objExists("L_BackLeg_Corr_Grp"):
        cmds.parentConstraint("hip_jnt", "L_BackLeg_Corr_Grp", mo=True)
        cmds.scaleConstraint("hip_jnt", "L_BackLeg_Corr_Grp", mo=True)

    if cmds.objExists("hip_jnt") and cmds.objExists("R_BackLeg_Corr_Grp"):
        cmds.parentConstraint("hip_jnt", "R_BackLeg_Corr_Grp", mo=True)
        cmds.scaleConstraint("hip_jnt", "R_BackLeg_Corr_Grp", mo=True)

    if cmds.objExists("L_upLegWeightSplit_4_jnt") and cmds.objExists("L_Knee_Ext_Corr_Grp"):
        cmds.parentConstraint("L_upLegWeightSplit_4_jnt", "L_Knee_Ext_Corr_Grp", mo=True)
        cmds.scaleConstraint("L_upLegWeightSplit_4_jnt", "L_Knee_Ext_Corr_Grp", mo=True)

    if cmds.objExists("R_upLegWeightSplit_4_jnt") and cmds.objExists("R_Knee_Ext_Corr_Grp"):
        cmds.parentConstraint("R_upLegWeightSplit_4_jnt", "R_Knee_Ext_Corr_Grp", mo=True)
        cmds.scaleConstraint("R_upLegWeightSplit_4_jnt", "R_Knee_Ext_Corr_Grp", mo=True)

    if cmds.objExists("L_upLegWeightSplit_4_jnt") and cmds.objExists("L_Knee_Int_Corr_Grp"):
        cmds.parentConstraint("L_upLegWeightSplit_4_jnt", "L_Knee_Int_Corr_Grp", mo=True)
        cmds.scaleConstraint("L_upLegWeightSplit_4_jnt", "L_Knee_Int_Corr_Grp", mo=True)

    if cmds.objExists("R_upLegWeightSplit_4_jnt") and cmds.objExists("R_Knee_Int_Corr_Grp"):
        cmds.parentConstraint("R_upLegWeightSplit_4_jnt", "R_Knee_Int_Corr_Grp", mo=True)
        cmds.scaleConstraint("R_upLegWeightSplit_4_jnt", "R_Knee_Int_Corr_Grp", mo=True)

    if cmds.objExists("hip_jnt") and cmds.objExists("L_UpLeg_Corr_Grp"):
        cmds.parentConstraint("hip_jnt", "L_UpLeg_Corr_Grp", mo=True)
        cmds.scaleConstraint("hip_jnt", "L_UpLeg_Corr_Grp", mo=True)

    if cmds.objExists("hip_jnt") and cmds.objExists("R_UpLeg_Corr_Grp"):
        cmds.parentConstraint("hip_jnt", "R_UpLeg_Corr_Grp", mo=True)
        cmds.scaleConstraint("hip_jnt", "R_UpLeg_Corr_Grp", mo=True)

    if cmds.objExists("L_clavicle_jnt") and cmds.objExists("L_Shoulder_Low_Corr_Grp"):
        cmds.parentConstraint("L_clavicle_jnt", "L_Shoulder_Low_Corr_Grp", mo=True)
        cmds.scaleConstraint("L_clavicle_jnt", "L_Shoulder_Low_Corr_Grp", mo=True)

    if cmds.objExists("R_clavicle_jnt") and cmds.objExists("R_Shoulder_Low_Corr_Grp"):
        cmds.parentConstraint("R_clavicle_jnt", "R_Shoulder_Low_Corr_Grp", mo=True)
        cmds.scaleConstraint("R_clavicle_jnt", "R_Shoulder_Low_Corr_Grp", mo=True)

    if cmds.objExists("L_clavicle_jnt") and cmds.objExists("L_Shoulder_Up_Corr_Grp"):
        cmds.parentConstraint("L_clavicle_jnt", "L_Shoulder_Up_Corr_Grp", mo=True)
        cmds.scaleConstraint("L_clavicle_jnt", "L_Shoulder_Up_Corr_Grp", mo=True)

    if cmds.objExists("R_clavicle_jnt") and cmds.objExists("R_Shoulder_Up_Corr_Grp"):
        cmds.parentConstraint("R_clavicle_jnt", "R_Shoulder_Up_Corr_Grp", mo=True)
        cmds.scaleConstraint("R_clavicle_jnt", "R_Shoulder_Up_Corr_Grp", mo=True)

    if cmds.objExists("L_clavicle_jnt") and cmds.objExists("L_Shoulder_Back_Corr_Grp"):
        cmds.parentConstraint("L_clavicle_jnt", "L_Shoulder_Back_Corr_Grp", mo=True)
        cmds.scaleConstraint("L_clavicle_jnt", "L_Shoulder_Back_Corr_Grp", mo=True)

    if cmds.objExists("R_clavicle_jnt") and cmds.objExists("R_Shoulder_Back_Corr_Grp"):
        cmds.parentConstraint("R_clavicle_jnt", "R_Shoulder_Back_Corr_Grp", mo=True)
        cmds.scaleConstraint("R_clavicle_jnt", "R_Shoulder_Back_Corr_Grp", mo=True)

    if cmds.objExists("L_clavicle_jnt") and cmds.objExists("L_Shoulder_Front_Corr_Grp"):
        cmds.parentConstraint("L_clavicle_jnt", "L_Shoulder_Front_Corr_Grp", mo=True)
        cmds.scaleConstraint("L_clavicle_jnt", "L_Shoulder_Front_Corr_Grp", mo=True)

    if cmds.objExists("R_clavicle_jnt") and cmds.objExists("R_Shoulder_Front_Corr_Grp"):
        cmds.parentConstraint("R_clavicle_jnt", "R_Shoulder_Front_Corr_Grp", mo=True)
        cmds.scaleConstraint("R_clavicle_jnt", "R_Shoulder_Front_Corr_Grp", mo=True)

    if cmds.objExists("L_upArmWeightSplit_4_jnt") and cmds.objExists("L_Elbow_Int_Corr_Grp"):
        cmds.parentConstraint("L_upArmWeightSplit_4_jnt", "L_Elbow_Int_Corr_Grp", mo=True)
        cmds.scaleConstraint("L_upArmWeightSplit_4_jnt", "L_Elbow_Int_Corr_Grp", mo=True)

    if cmds.objExists("R_upArmWeightSplit_4_jnt") and cmds.objExists("R_Elbow_Int_Corr_Grp"):
        cmds.parentConstraint("R_upArmWeightSplit_4_jnt", "R_Elbow_Int_Corr_Grp", mo=True)
        cmds.scaleConstraint("R_upArmWeightSplit_4_jnt", "R_Elbow_Int_Corr_Grp", mo=True)

    if cmds.objExists("L_upArmWeightSplit_4_jnt") and cmds.objExists("L_Elbow_Ext_Corr_Grp"):
        cmds.parentConstraint("L_upArmWeightSplit_4_jnt", "L_Elbow_Ext_Corr_Grp", mo=True)
        cmds.scaleConstraint("L_upArmWeightSplit_4_jnt", "L_Elbow_Ext_Corr_Grp", mo=True)

    if cmds.objExists("R_upArmWeightSplit_4_jnt") and cmds.objExists("R_Elbow_Ext_Corr_Grp"):
        cmds.parentConstraint("R_upArmWeightSplit_4_jnt", "R_Elbow_Ext_Corr_Grp", mo=True)
        cmds.scaleConstraint("R_upArmWeightSplit_4_jnt", "R_Elbow_Ext_Corr_Grp", mo=True)

    if cmds.objExists("L_elbowWeightSplit_4_jnt") and cmds.objExists("L_Wrist_Ext_Corr_Grp"):
        cmds.parentConstraint("L_elbowWeightSplit_4_jnt", "L_Wrist_Ext_Corr_Grp", mo=True)
        cmds.scaleConstraint("L_elbowWeightSplit_4_jnt", "L_Wrist_Ext_Corr_Grp", mo=True)

    if cmds.objExists("R_elbowWeightSplit_4_jnt") and cmds.objExists("R_Wrist_Ext_Corr_Grp"):
        cmds.parentConstraint("R_elbowWeightSplit_4_jnt", "R_Wrist_Ext_Corr_Grp", mo=True)
        cmds.scaleConstraint("R_elbowWeightSplit_4_jnt", "R_Wrist_Ext_Corr_Grp", mo=True)

    def check_and_apply_constraint(constraint_type, target, constrained):
        if cmds.objExists(target) and cmds.objExists(constrained):
            constraint_function = getattr(cmds, f"{constraint_type}Constraint")
            constraint_function(target, constrained, mo=True)


    check_and_apply_constraint("parent", "L_hip1_bendy_5_jnt", "L_legRear_Knee_Ext_Corr_Grp")
    check_and_apply_constraint("parent", "L_hip1_bendy_5_jnt", "L_legRear_Knee_Int_Corr_Grp")
    check_and_apply_constraint("parent", "L_knee1_bendy_5_jnt", "L_legRear_Ankle_Int_Corr_Grp")
    check_and_apply_constraint("parent", "L_knee1_bendy_5_jnt", "L_legRear_Ankle_Ext_Corr_Grp")
    check_and_apply_constraint("parent", "hip_jnt", "L_legRear_Up_Ext_Corr_Grp")
    check_and_apply_constraint("parent", "hip_jnt", "L_legRear_Up_Int_Corr_Grp")
    check_and_apply_constraint("parent", "hip_jnt", "L_legRear_SIDE_Corr_Grp")

    check_and_apply_constraint("parent", "R_hip1_bendy_5_jnt", "R_legRear_Knee_Ext_Corr_Grp")
    check_and_apply_constraint("parent", "R_hip1_bendy_5_jnt", "R_legRear_Knee_Int_Corr_Grp")
    check_and_apply_constraint("parent", "R_knee1_bendy_5_jnt", "R_legRear_Ankle_Int_Corr_Grp")
    check_and_apply_constraint("parent", "R_knee1_bendy_5_jnt", "R_legRear_Ankle_Ext_Corr_Grp")
    check_and_apply_constraint("parent", "hip_jnt", "R_legRear_Up_Ext_Corr_Grp")
    check_and_apply_constraint("parent", "hip_jnt", "R_legRear_Up_Int_Corr_Grp")
    check_and_apply_constraint("parent", "hip_jnt", "R_legRear_SIDE_Corr_Grp")



    # Funzione di supporto per filtrare gli oggetti esistenti
    def filter_existing_objects(object_list):
        return [obj for obj in object_list if cmds.objExists(obj)]

    # Creazione del set per il lato sinistro dei pesi
    left_body_weights = [
        "L_Shoulder_Up_Corr_Weight_jnt", "L_Shoulder_Back_Corr_Weight_jnt", "L_Shoulder_Front_Corr_Weight_jnt",
        "L_Shoulder_Low_Corr_Weight_jnt", "L_Elbow_Ext_Corr_Weight_jnt", "L_Elbow_Int_Corr_Weight_jnt",
        "L_Wrist_Ext_Corr_Weight_jnt", "L_BackLeg_Corr_Weight_jnt", "L_UpLeg_Corr_Weight_jnt",
        "L_Knee_Int_Corr_Weight_jnt", "L_Knee_Ext_Corr_Weight_jnt"
        "L_legFront_Knee_Ext_Corr_Weight_jnt", "L_legFront_Knee_Int_Corr_Weight_jnt", "L_legFront_Ankle_Ext_Corr_Weight_jnt",
        "Neck_Low_Corr_Weight_jnt", "Neck_Up_Corr_Weight_jnt", "L_Neck_Corr_Weight_jnt", "L_legFront_Up_Int_Corr_Weight_jnt",
        "L_legFront_Up_Ext_Corr_Weight_jnt", "L_legFront_Ankle_Int_Corr_Weight_jnt", "L_legRear_Knee_Ext_Corr_Weight_jnt",
        "L_legRear_Ankle_Int_Corr_Weight_jnt", "L_legRear_Up_Ext_Corr_Weight_jnt", "L_legRear_Knee_Int_Corr_Weight_jnt",
        "L_legRear_Ankle_Ext_Corr_Weight_jnt", "L_legRear_Up_Int_Corr_Weight_jnt", 'L_legRear_SIDE_Corr_Weight_jnt'
    ]

    # Creazione del set solo con gli oggetti esistenti
    filtered_left_body_weights = filter_existing_objects(left_body_weights)
    if filtered_left_body_weights:
        cmds.sets(*filtered_left_body_weights, n="SET_L_Body_Corrective_Weights")

    # Creazione del set per il lato destro dei pesi
    right_body_weights = [
        "R_Shoulder_Up_Corr_Weight_jnt", "R_Shoulder_Back_Corr_Weight_jnt", "R_Shoulder_Front_Corr_Weight_jnt",
        "R_Shoulder_Low_Corr_Weight_jnt", "R_Elbow_Ext_Corr_Weight_jnt", "R_Elbow_Int_Corr_Weight_jnt",
        "R_Wrist_Ext_Corr_Weight_jnt", "R_BackLeg_Corr_Weight_jnt", "R_UpLeg_Corr_Weight_jnt",
        "R_Knee_Int_Corr_Weight_jnt", "R_Knee_Ext_Corr_Weight_jnt"
        "R_legFront_Knee_Ext_Corr_Weight_jnt", "R_legFront_Knee_Int_Corr_Weight_jnt", "R_legFront_Ankle_Ext_Corr_Weight_jnt",
        "R_Neck_Corr_Weight_jnt", "R_legFront_Up_Int_Corr_Weight_jnt", "R_legFront_Up_Ext_Corr_Weight_jnt",
        "R_legFront_Ankle_Int_Corr_Weight_jnt", "R_legRear_Knee_Int_Corr_Weight_jnt", "R_legRear_Knee_Ext_Corr_Weight_jnt",
        "R_legRear_Up_Ext_Corr_Weight_jnt", "R_legRear_Ankle_Ext_Corr_Weight_jnt", "R_legRear_Ankle_Int_Corr_Weight_jnt",
        "R_legRear_Up_Int_Corr_Weight_jnt", 'R_legRear_SIDE_Corr_Weight_jnt'
    ]

    filtered_right_body_weights = filter_existing_objects(right_body_weights)
    if filtered_right_body_weights:
        cmds.sets(*filtered_right_body_weights, n="SET_R_Body_Corrective_Weights")

    # Creazione del set per i controlli del lato sinistro
    left_body_controls = [
        "L_Shoulder_Up_Corr_Ctrl", "L_Shoulder_Back_Corr_Ctrl", "L_Shoulder_Front_Corr_Ctrl",
        "L_Shoulder_Low_Corr_Ctrl",
        "L_Elbow_Ext_Corr_Ctrl", "L_Elbow_Int_Corr_Ctrl", "L_Wrist_Ext_Corr_Ctrl", "L_BackLeg_Corr_Ctrl",
        "L_UpLeg_Corr_Ctrl", "L_Knee_Int_Corr_Ctrl", "L_Knee_Ext_Corr_Ctrl"
        "L_legFront_Knee_Ext_Corr_Ctrl", "L_legFront_Knee_Int_Corr_Ctrl", "L_legFront_Ankle_Ext_Corr_Ctrl",
        "Neck_Low_Corr_Ctrl", "Neck_Up_Corr_Ctrl", "L_Neck_Corr_Ctrl", "L_legFront_Up_Int_Corr_Ctrl",
        "L_legFront_Up_Ext_Corr_Ctrl", "L_legFront_Ankle_Int_Corr_Ctrl", "L_legRear_Knee_Ext_Corr_Ctrl",
        "L_legRear_Ankle_Int_Corr_Ctrl", "L_legRear_Up_Ext_Corr_Ctrl", "L_legRear_Knee_Int_Corr_Ctrl",
        "L_legRear_Ankle_Ext_Corr_Ctrl", "L_legRear_Up_Int_Corr_Ctrl", 'L_legRear_SIDE_Corr_Ctrl'
    ]

    filtered_left_body_controls = filter_existing_objects(left_body_controls)
    if filtered_left_body_controls:
        cmds.sets(*filtered_left_body_controls, n="SET_L_Body_Corrective_CONTROLS")

    # Creazione del set per i controlli del lato destro
    right_body_controls = [
        "R_Shoulder_Up_Corr_Ctrl", "R_Shoulder_Back_Corr_Ctrl", "R_Shoulder_Front_Corr_Ctrl",
        "R_Shoulder_Low_Corr_Ctrl",
        "R_Elbow_Ext_Corr_Ctrl", "R_Elbow_Int_Corr_Ctrl", "R_Wrist_Ext_Corr_Ctrl", "R_BackLeg_Corr_Ctrl",
        "R_UpLeg_Corr_Ctrl", "L_Knee_Int_Corr_Ctrl", "R_Knee_Ext_Corr_Ctrl"
        "R_legFront_Knee_Ext_Corr_Ctrl", "R_legFront_Knee_Int_Corr_Ctrl", "R_legFront_Ankle_Ext_Corr_Ctrl",
        "R_Neck_Corr_Ctrl", "R_legFront_Up_Int_Corr_Ctrl", "R_legFront_Up_Ext_Corr_Ctrl", "R_legFront_Ankle_Int_Corr_Ctrl",
        "R_legRear_Knee_Int_Corr_Ctrl", "R_legRear_Knee_Ext_Corr_Ctrl", "R_legRear_Up_Ext_Corr_Ctrl",
        "R_legRear_Ankle_Ext_Corr_Ctrl", "R_legRear_Ankle_Int_Corr_Ctrl", "R_legRear_Up_Int_Corr_Ctrl", 'R_legRear_SIDE_Corr_Ctrl'
    ]

    filtered_right_body_controls = filter_existing_objects(right_body_controls)
    if filtered_right_body_controls:
        cmds.sets(*filtered_right_body_controls, n="SET_R_Body_Corrective_CONTROLS")

    # Creazione del set per i nodi Slider del lato sinistro
    left_slider_nodes = [
        "SET_L_BackLeg_Corr_Position_loc_SliderNODE", "SET_L_Elbow_Ext_Corr_Position_loc_SliderNODE",
        "SET_L_Elbow_Int_Corr_Position_loc_SliderNODE", "SET_L_Knee_Ext_Corr_Position_loc_SliderNODE",
        "SET_L_Knee_Int_Corr_Position_loc_SliderNODE", "SET_L_Shoulder_Back_Corr_Position_loc_SliderNODE",
        "SET_L_Shoulder_Front_Corr_Position_loc_SliderNODE", "SET_L_Shoulder_Low_Corr_Position_loc_SliderNODE",
        "SET_L_Shoulder_Up_Corr_Position_loc_SliderNODE", "SET_L_UpLeg_Corr_Position_loc_SliderNODE",
        "SET_L_Wrist_Ext_Corr_Position_loc_SliderNODE"
        "SET_L_legFront_Ankle_Ext_Corr_Position_loc_SliderNODE", "SET_L_legFront_Ankle_Int_Corr_Position_loc_SliderNODE",
        "SET_L_legFront_Knee_Ext_Corr_Position_loc_SliderNODE", "SET_L_legFront_Knee_Int_Corr_Position_loc_SliderNODE",
        "SET_L_legFront_Up_Ext_Corr_Position_loc_SliderNODE", "SET_L_legFront_Up_Int_Corr_Position_loc_SliderNODE",
        "SET_L_legRear_Ankle_Ext_Corr_Position_loc_SliderNODE", "SET_L_legRear_Ankle_Int_Corr_Position_loc_SliderNODE",
        "SET_L_legRear_Knee_Ext_Corr_Position_loc_SliderNODE", "SET_L_legRear_Knee_Int_Corr_Position_loc_SliderNODE",
        "SET_L_legRear_Up_Ext_Corr_Position_loc_SliderNODE", "SET_L_legRear_Up_Int_Corr_Position_loc_SliderNODE",
        "SET_L_Neck_Corr_Position_loc_SliderNODE", "SET_Neck_Low_Corr_Position_loc_SliderNODE",
        "SET_Neck_Up_Corr_Position_loc_SliderNODE", 'SET_L_legRear_SIDE_Corr_Position_loc_SliderNODE', 'SET_L_Wrist_Ext_Corr_Position_loc_SliderNODE'
    ]

    filtered_left_slider_nodes = filter_existing_objects(left_slider_nodes)
    if filtered_left_slider_nodes:
        cmds.sets(*filtered_left_slider_nodes, n="SET_L_Body_Corrective_SLIDER")

    # Creazione del set per i nodi Slider del lato destro
    right_slider_nodes = [
        "SET_R_BackLeg_Corr_Position_loc_SliderNODE", "SET_R_Elbow_Ext_Corr_Position_loc_SliderNODE",
        "SET_R_Elbow_Int_Corr_Position_loc_SliderNODE", "SET_R_Knee_Ext_Corr_Position_loc_SliderNODE",
        "SET_R_Knee_Int_Corr_Position_loc_SliderNODE", "SET_R_Shoulder_Back_Corr_Position_loc_SliderNODE",
        "SET_R_Shoulder_Front_Corr_Position_loc_SliderNODE", "SET_R_Shoulder_Low_Corr_Position_loc_SliderNODE",
        "SET_R_Shoulder_Up_Corr_Position_loc_SliderNODE", "SET_R_UpLeg_Corr_Position_loc_SliderNODE",
        "SET_R_Wrist_Ext_Corr_Position_loc_SliderNODE"
        "SET_R_legFront_Ankle_Ext_Corr_Position_loc_SliderNODE", "SET_R_legFront_Ankle_Int_Corr_Position_loc_SliderNODE",
        "SET_R_legFront_Knee_Ext_Corr_Position_loc_SliderNODE", "SET_R_legFront_Knee_Int_Corr_Position_loc_SliderNODE",
        "SET_R_legFront_Up_Ext_Corr_Position_loc_SliderNODE", "SET_R_legFront_Up_Int_Corr_Position_loc_SliderNODE",
        "SET_R_legRear_Ankle_Ext_Corr_Position_loc_SliderNODE", "SET_R_legRear_Ankle_Int_Corr_Position_loc_SliderNODE",
        "SET_R_legRear_Knee_Ext_Corr_Position_loc_SliderNODE", "SET_R_legRear_Knee_Int_Corr_Position_loc_SliderNODE",
        "SET_R_legRear_Up_Ext_Corr_Position_loc_SliderNODE", "SET_R_legRear_Up_Int_Corr_Position_loc_SliderNODE",
        "SET_R_Neck_Corr_Position_loc_SliderNODE",'SET_R_legRear_SIDE_Corr_Position_loc_SliderNODE', 'SET_R_Wrist_Ext_Corr_Position_loc_SliderNODE'
    ]

    filtered_right_slider_nodes = filter_existing_objects(right_slider_nodes)
    if filtered_right_slider_nodes:
        cmds.sets(*filtered_right_slider_nodes, n="SET_R_Body_Corrective_SLIDER")


# FINGERS_FINALIZE____________________________________________________________________________________________________________________________________________________________________________________________________________

def finalize_fingers(*args):
    # L_Dita________________________________________________________________________________________________________________________________________________________________

    check_and_apply_double_parent_matrix(
        ["L_indexCup_fk_jnt", "L_indexBase_fk_jnt", "L_IndexBase_Corr_Position_loc",
         "L_IndexBase_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_indexCup_fk_jnt", "L_indexBase_fk_jnt", "L_IndexBase_Corr_Rotation_loc",
         "L_IndexBase_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_indexBase_fk_jnt", "L_indexMid_fk_jnt", "L_IndexMid_Corr_Position_loc", "L_IndexMid_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_indexBase_fk_jnt", "L_indexMid_fk_jnt", "L_IndexMid_Corr_Rotation_loc", "L_IndexMid_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_indexMid_fk_jnt", "L_indexTip_fk_jnt", "L_IndexTop_Corr_Position_loc", "L_IndexTop_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_indexMid_fk_jnt", "L_indexTip_fk_jnt", "L_IndexTop_Corr_Rotation_loc", "L_IndexTop_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_indexTop_fk_jnt", "L_indexBase_fk_jnt", "L_IndexBase_Int_Corr_Position_loc",
         "L_IndexBase_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_indexTop_fk_jnt", "L_indexBase_fk_jnt", "L_IndexBase_Int_Corr_Rotation_loc",
         "L_IndexBase_Int_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_middleCup_fk_jnt", "L_middleBase_fk_jnt", "L_MiddleBase_Corr_Position_loc",
         "L_MiddleBase_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_middleCup_fk_jnt", "L_middleBase_fk_jnt", "L_MiddleBase_Corr_Rotation_loc",
         "L_MiddleBase_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_middleBase_fk_jnt", "L_middleMid_fk_jnt", "L_MiddleMid_Corr_Position_loc",
         "L_MiddleMid_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_middleBase_fk_jnt", "L_middleMid_fk_jnt", "L_MiddleMid_Corr_Rotation_loc",
         "L_MiddleMid_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_middleMid_fk_jnt", "L_middleTip_fk_jnt", "L_MiddleTop_Corr_Position_loc",
         "L_MiddleTop_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_middleMid_fk_jnt", "L_middleTip_fk_jnt", "L_MiddleTop_Corr_Rotation_loc",
         "L_MiddleTop_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_middleTop_fk_jnt", "L_middleBase_fk_jnt", "L_MiddleBase_Int_Corr_Position_loc",
         "L_MiddleBase_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_middleTop_fk_jnt", "L_middleBase_fk_jnt", "L_MiddleBase_Int_Corr_Rotation_loc",
         "L_MiddleBase_Int_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_ringTop_fk_jnt", "L_ringBase_fk_jnt", "L_RingBase_Corr_Position_loc", "L_RingBase_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_ringTop_fk_jnt", "L_ringBase_fk_jnt", "L_RingBase_Corr_Rotation_loc", "L_RingBase_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_ringBase_fk_jnt", "L_ringMid_fk_jnt", "L_RingMid_Corr_Position_loc", "L_RingMid_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_ringBase_fk_jnt", "L_ringMid_fk_jnt", "L_RingMid_Corr_Rotation_loc", "L_RingMid_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_ringMid_fk_jnt", "L_ringTip_fk_jnt", "L_RingTop_Corr_Position_loc", "L_RingTop_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_ringMid_fk_jnt", "L_ringTip_fk_jnt", "L_RingTop_Corr_Rotation_loc", "L_RingTop_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_ringTop_fk_jnt", "L_ringBase_fk_jnt", "L_RingBase_Int_Corr_Position_loc", "L_RingBase_Int_Corr_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_ringTop_fk_jnt", "L_ringBase_fk_jnt", "L_RingBase_Int_Corr_Rotation_loc", "L_RingBase_Int_Corr_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_pinkyTop_fk_jnt", "L_pinkyBase_fk_jnt", "L_PinkyBase_Corr_Position_loc",
         "L_PinkyBase_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_pinkyTop_fk_jnt", "L_pinkyBase_fk_jnt", "L_PinkyBase_Corr_Rotation_loc",
         "L_PinkyBase_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_pinkyBase_fk_jnt", "L_pinkyMid_fk_jnt", "L_PinkyMid_Corr_Position_loc", "L_PinkyMid_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_pinkyBase_fk_jnt", "L_pinkyMid_fk_jnt", "L_PinkyMid_Corr_Rotation_loc", "L_PinkyMid_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_pinkyMid_fk_jnt", "L_pinkyTip_fk_jnt", "L_PinkyTop_Corr_Position_loc", "L_PinkyTop_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_pinkyMid_fk_jnt", "L_pinkyTip_fk_jnt", "L_PinkyTop_Corr_Rotation_loc", "L_PinkyTop_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_pinkyTop_fk_jnt", "L_pinkyBase_fk_jnt", "L_PinkyBase_Int_Corr_Position_loc",
         "L_PinkyBase_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_pinkyTop_fk_jnt", "L_pinkyBase_fk_jnt", "L_PinkyBase_Int_Corr_Rotation_loc",
         "L_PinkyBase_Int_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_thumbBase_fk_jnt", "L_thumbMid_fk_jnt", "L_ThumbBase_Corr_Position_loc",
         "L_ThumbBase_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_thumbBase_fk_jnt", "L_thumbMid_fk_jnt", "L_ThumbBase_Corr_Rotation_loc",
         "L_ThumbBase_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["L_thumbMid_fk_jnt", "L_thumbTip_fk_jnt", "L_ThumbMid_Corr_Position_loc", "L_ThumbMid_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["L_thumbMid_fk_jnt", "L_thumbTip_fk_jnt", "L_ThumbMid_Corr_Rotation_loc", "L_ThumbMid_Corr_Locator_Grp"])

    # R_Dita_____________________________________________________________________________________________________________

    check_and_apply_double_parent_matrix(
        ["R_indexTop_fk_jnt", "R_indexBase_fk_jnt", "R_IndexBase_Corr_Position_loc",
         "R_IndexBase_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_indexTop_fk_jnt", "R_indexBase_fk_jnt", "R_IndexBase_Corr_Rotation_loc",
         "R_IndexBase_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_indexBase_fk_jnt", "R_indexMid_fk_jnt", "R_IndexMid_Corr_Position_loc", "R_IndexMid_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_indexBase_fk_jnt", "R_indexMid_fk_jnt", "R_IndexMid_Corr_Rotation_loc", "R_IndexMid_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_indexMid_fk_jnt", "R_indexTip_fk_jnt", "R_IndexTop_Corr_Position_loc", "R_IndexTop_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_indexMid_fk_jnt", "R_indexTip_fk_jnt", "R_IndexTop_Corr_Rotation_loc", "R_IndexTop_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_indexTop_fk_jnt", "R_indexBase_fk_jnt", "R_IndexBase_Int_Corr_Position_loc",
         "R_IndexBase_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_indexTop_fk_jnt", "R_indexBase_fk_jnt", "R_IndexBase_Int_Corr_Rotation_loc",
         "R_IndexBase_Int_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_middleTop_fk_jnt", "R_middleBase_fk_jnt", "R_MiddleBase_Corr_Position_loc",
         "R_MiddleBase_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_middleTop_fk_jnt", "R_middleBase_fk_jnt", "R_MiddleBase_Corr_Rotation_loc",
         "R_MiddleBase_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_middleBase_fk_jnt", "R_middleMid_fk_jnt", "R_MiddleMid_Corr_Position_loc",
         "R_MiddleMid_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_middleBase_fk_jnt", "R_middleMid_fk_jnt", "R_MiddleMid_Corr_Rotation_loc",
         "R_MiddleMid_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_middleMid_fk_jnt", "R_middleTip_fk_jnt", "R_MiddleTop_Corr_Position_loc",
         "R_MiddleTop_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_middleMid_fk_jnt", "R_middleTip_fk_jnt", "R_MiddleTop_Corr_Rotation_loc",
         "R_MiddleTop_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_middleTop_fk_jnt", "R_middleBase_fk_jnt", "R_MiddleBase_Int_Corr_Position_loc",
         "R_MiddleBase_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_middleTop_fk_jnt", "R_middleBase_fk_jnt", "R_MiddleBase_Int_Corr_Rotation_loc",
         "R_MiddleBase_Int_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_ringTop_fk_jnt", "R_ringBase_fk_jnt", "R_RingBase_Corr_Position_loc", "R_RingBase_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_ringTop_fk_jnt", "R_ringBase_fk_jnt", "R_RingBase_Corr_Rotation_loc", "R_RingBase_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_ringBase_fk_jnt", "R_ringMid_fk_jnt", "R_RingMid_Corr_Position_loc", "R_RingMid_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_ringBase_fk_jnt", "R_ringMid_fk_jnt", "R_RingMid_Corr_Rotation_loc", "R_RingMid_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_ringMid_fk_jnt", "R_ringTip_fk_jnt", "R_RingTop_Corr_Position_loc", "R_RingTop_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_ringMid_fk_jnt", "R_ringTip_fk_jnt", "R_RingTop_Corr_Rotation_loc", "R_RingTop_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_ringTop_fk_jnt", "R_ringBase_fk_jnt", "R_RingBase_Int_Corr_Position_loc", "R_RingBase_Int_Corr_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_ringTop_fk_jnt", "R_ringBase_fk_jnt", "R_RingBase_Int_Corr_Rotation_loc", "R_RingBase_Int_Corr_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_pinkyTop_fk_jnt", "R_pinkyBase_fk_jnt", "R_PinkyBase_Corr_Position_loc",
         "R_PinkyBase_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_pinkyTop_fk_jnt", "R_pinkyBase_fk_jnt", "R_PinkyBase_Corr_Rotation_loc",
         "R_PinkyBase_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_pinkyBase_fk_jnt", "R_pinkyMid_fk_jnt", "R_PinkyMid_Corr_Position_loc", "R_PinkyMid_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_pinkyBase_fk_jnt", "R_pinkyMid_fk_jnt", "R_PinkyMid_Corr_Rotation_loc", "R_PinkyMid_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_pinkyMid_fk_jnt", "R_pinkyTip_fk_jnt", "R_PinkyTop_Corr_Position_loc", "R_PinkyTop_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_pinkyMid_fk_jnt", "R_pinkyTip_fk_jnt", "R_PinkyTop_Corr_Rotation_loc", "R_PinkyTop_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_pinkyTop_fk_jnt", "R_pinkyBase_fk_jnt", "R_PinkyBase_Int_Corr_Position_loc",
         "R_PinkyBase_Int_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_pinkyTop_fk_jnt", "R_pinkyBase_fk_jnt", "R_PinkyBase_Int_Corr_Rotation_loc",
         "R_PinkyBase_Int_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_thumbBase_fk_jnt", "R_thumbMid_fk_jnt", "R_ThumbBase_Corr_Position_loc",
         "R_ThumbBase_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_thumbBase_fk_jnt", "R_thumbMid_fk_jnt", "R_ThumbBase_Corr_Rotation_loc",
         "R_ThumbBase_Corr_Locator_Grp"])

    check_and_apply_double_parent_matrix(
        ["R_thumbMid_fk_jnt", "R_thumbTip_fk_jnt", "R_ThumbMid_Corr_Position_loc", "R_ThumbMid_Corr_Locator_Grp"])
    check_and_apply_double_parent_matrix(
        ["R_thumbMid_fk_jnt", "R_thumbTip_fk_jnt", "R_ThumbMid_Corr_Rotation_loc", "R_ThumbMid_Corr_Locator_Grp"])

    # Funzione per filtrare solo gli oggetti esistenti
    def filtra_oggetti_esistenti(lista_oggetti):
        return [obj for obj in lista_oggetti if cmds.objExists(obj)]

    # Elimina i gruppi se esistono
    if cmds.objExists("GRP_L_FingersCorrective"):
        cmds.delete("GRP_L_FingersCorrective")
    if cmds.objExists("GRP_R_FingersCorrective"):
        cmds.delete("GRP_R_FingersCorrective")

    # Oggetti per il gruppo sinistro
    gruppi_sinistra = [
        "L_IndexBase_Corr_Grp", "L_IndexMid_Corr_Grp", "L_IndexTop_Corr_Grp", "L_IndexBase_Int_Corr_Grp",
        "L_MiddleBase_Corr_Grp", "L_MiddleMid_Corr_Grp", "L_MiddleTop_Corr_Grp", "L_MiddleBase_Int_Corr_Grp",
        "L_RingBase_Corr_Grp", "L_RingMid_Corr_Grp", "L_RingTop_Corr_Grp", "L_RingBase_Int_Corr_Grp",
        "L_PinkyBase_Corr_Grp", "L_PinkyMid_Corr_Grp", "L_PinkyTop_Corr_Grp", "L_PinkyBase_Int_Corr_Grp",
        "L_ThumbBase_Corr_Grp", "L_ThumbMid_Corr_Grp"
    ]

    # Filtra gli oggetti che esistono e crea il gruppo sinistro
    gruppi_sinistra_esistenti = filtra_oggetti_esistenti(gruppi_sinistra)
    if gruppi_sinistra_esistenti:
        cmds.group(gruppi_sinistra_esistenti, n="L_Fingers_Corrective_Grp_RIG")

    # Oggetti per il gruppo destro
    gruppi_destra = [
        "R_IndexBase_Corr_Grp", "R_IndexMid_Corr_Grp", "R_IndexTop_Corr_Grp", "R_IndexBase_Int_Corr_Grp",
        "R_MiddleBase_Corr_Grp", "R_MiddleMid_Corr_Grp", "R_MiddleTop_Corr_Grp", "R_MiddleBase_Int_Corr_Grp",
        "R_RingBase_Corr_Grp", "R_RingMid_Corr_Grp", "R_RingTop_Corr_Grp", "R_RingBase_Int_Corr_Grp",
        "R_PinkyBase_Corr_Grp", "R_PinkyMid_Corr_Grp", "R_PinkyTop_Corr_Grp", "R_PinkyBase_Int_Corr_Grp",
        "R_ThumbBase_Corr_Grp", "R_ThumbMid_Corr_Grp"
    ]

    # Filtra gli oggetti che esistono e crea il gruppo destro
    gruppi_destra_esistenti = filtra_oggetti_esistenti(gruppi_destra)
    if gruppi_destra_esistenti:
        cmds.group(gruppi_destra_esistenti, n="R_Fingers_Corrective_Grp_RIG")

    # Nascondi i gruppi se esistono
    if cmds.objExists("L_Fingers_Corrective_Grp_RIG"):
        cmds.hide("L_Fingers_Corrective_Grp_RIG")
    if cmds.objExists("R_Fingers_Corrective_Grp_RIG"):
        cmds.hide("R_Fingers_Corrective_Grp_RIG")

    # Esegui i vincoli e gli scaleConstraint solo sugli oggetti esistenti
    def crea_vincoli(jnt, grp):
        if cmds.objExists(jnt) and cmds.objExists(grp):
            cmds.parentConstraint(jnt, grp, mo=True)
            cmds.scaleConstraint(jnt, grp, mo=True)

    # Lista di vincoli per la mano sinistra
    vincoli_sinistra = [
        ("L_indexCup_fk_jnt", "L_IndexBase_Corr_Grp"),
        ("L_indexBase_fk_jnt", "L_IndexMid_Corr_Grp"),
        ("L_indexMid_fk_jnt", "L_IndexTop_Corr_Grp"),
        ("L_indexCup_fk_jnt", "L_IndexBase_Int_Corr_Grp"),
        ("L_middleCup_fk_jnt", "L_MiddleBase_Corr_Grp"),
        ("L_middleBase_fk_jnt", "L_MiddleMid_Corr_Grp"),
        ("L_middleMid_fk_jnt", "L_MiddleTop_Corr_Grp"),
        ("L_middleCup_fk_jnt", "L_MiddleBase_Int_Corr_Grp"),
        ("L_ringTop_fk_jnt", "L_RingBase_Corr_Grp"),
        ("L_ringBase_fk_jnt", "L_RingMid_Corr_Grp"),
        ("L_ringMid_fk_jnt", "L_RingTop_Corr_Grp"),
        ("L_ringTop_fk_jnt", "L_RingBase_Int_Corr_Grp"),
        ("L_pinkyTop_fk_jnt", "L_PinkyBase_Corr_Grp"),
        ("L_pinkyBase_fk_jnt", "L_PinkyMid_Corr_Grp"),
        ("L_pinkyMid_fk_jnt", "L_PinkyTop_Corr_Grp"),
        ("L_pinkyTop_fk_jnt", "L_PinkyBase_Int_Corr_Grp"),
        ("L_thumbBase_fk_jnt", "L_ThumbBase_Corr_Grp"),
        ("L_thumbMid_fk_jnt", "L_ThumbMid_Corr_Grp")
    ]

    # Applica i vincoli alla mano sinistra
    for jnt, grp in vincoli_sinistra:
        crea_vincoli(jnt, grp)

    # Lista di vincoli per la mano destra
    vincoli_destra = [
        ("R_indexCup_fk_jnt", "R_IndexBase_Corr_Grp"),
        ("R_indexBase_fk_jnt", "R_IndexMid_Corr_Grp"),
        ("R_indexMid_fk_jnt", "R_IndexTop_Corr_Grp"),
        ("R_indexCup_fk_jnt", "R_IndexBase_Int_Corr_Grp"),
        ("R_middleCup_fk_jnt", "R_MiddleBase_Corr_Grp"),
        ("R_middleBase_fk_jnt", "R_MiddleMid_Corr_Grp"),
        ("R_middleMid_fk_jnt", "R_MiddleTop_Corr_Grp"),
        ("R_middleCup_fk_jnt", "R_MiddleBase_Int_Corr_Grp"),
        ("R_ringTop_fk_jnt", "R_RingBase_Corr_Grp"),
        ("R_ringBase_fk_jnt", "R_RingMid_Corr_Grp"),
        ("R_ringMid_fk_jnt", "R_RingTop_Corr_Grp"),
        ("R_ringTop_fk_jnt", "R_RingBase_Int_Corr_Grp"),
        ("R_pinkyTop_fk_jnt", "R_PinkyBase_Corr_Grp"),
        ("R_pinkyBase_fk_jnt", "R_PinkyMid_Corr_Grp"),
        ("R_pinkyMid_fk_jnt", "R_PinkyTop_Corr_Grp"),
        ("R_pinkyTop_fk_jnt", "R_PinkyBase_Int_Corr_Grp"),
        ("R_thumbBase_fk_jnt", "R_ThumbBase_Corr_Grp"),
        ("R_thumbMid_fk_jnt", "R_ThumbMid_Corr_Grp")
    ]

    # Applica i vincoli alla mano destra
    for jnt, grp in vincoli_destra:
        crea_vincoli(jnt, grp)

    # Crea i set di correttivi per le dita
    correttivi_sinistra = filtra_oggetti_esistenti([
        "L_ThumbBase_Corr_Weight_jnt", "L_ThumbMid_Corr_Weight_jnt", "L_IndexBase_Corr_Weight_jnt",
        "L_IndexMid_Corr_Weight_jnt", "L_IndexTop_Corr_Weight_jnt", "L_MiddleBase_Corr_Weight_jnt",
        "L_MiddleMid_Corr_Weight_jnt", "L_MiddleTop_Corr_Weight_jnt", "L_RingBase_Corr_Weight_jnt",
        "L_RingMid_Corr_Weight_jnt", "L_RingTop_Corr_Weight_jnt", "L_PinkyBase_Corr_Weight_jnt",
        "L_PinkyMid_Corr_Weight_jnt", "L_PinkyTop_Corr_Weight_jnt", "L_IndexBase_Int_Corr_Weight_jnt",
        "L_MiddleBase_Int_Corr_Weight_jnt", "L_RingBase_Int_Corr_Weight_jnt", "L_PinkyBase_Int_Corr_Weight_jnt"
    ])
    if correttivi_sinistra:
        cmds.sets(correttivi_sinistra, n="SET_L_Fingers_Corrective_Weights")

    correttivi_destra = filtra_oggetti_esistenti([
        "R_ThumbBase_Corr_Weight_jnt", "R_ThumbMid_Corr_Weight_jnt", "R_IndexBase_Corr_Weight_jnt",
        "R_IndexMid_Corr_Weight_jnt", "R_IndexTop_Corr_Weight_jnt", "R_MiddleBase_Corr_Weight_jnt",
        "R_MiddleMid_Corr_Weight_jnt", "R_MiddleTop_Corr_Weight_jnt", "R_RingBase_Corr_Weight_jnt",
        "R_RingMid_Corr_Weight_jnt", "R_RingTop_Corr_Weight_jnt", "R_PinkyBase_Corr_Weight_jnt",
        "R_PinkyMid_Corr_Weight_jnt", "R_PinkyTop_Corr_Weight_jnt", "R_IndexBase_Int_Corr_Weight_jnt",
        "R_MiddleBase_Int_Corr_Weight_jnt", "R_RingBase_Int_Corr_Weight_jnt", "R_PinkyBase_Int_Corr_Weight_jnt"
    ])
    if correttivi_destra:
        cmds.sets(correttivi_destra, n="SET_R_Fingers_Corrective_Weights")


# DELETE_R_SIDE____________________________________________________________________________________________________________________________________________________________________________________________________________


def delete_multiply_nodes(*args):
    multiply_nodes = [
        'MLD_LOC_L_Shoulder_Low_Corr_01', 'MLD_LOC_L_Shoulder_Low_Corr_02',
        'MLD_LOC_L_Shoulder_Up_Corr_01', 'MLD_LOC_L_Shoulder_Up_Corr_02',
        'MLD_LOC_L_Shoulder_Back_Corr_01', 'MLD_LOC_L_Shoulder_Back_Corr_02',
        'MLD_LOC_L_Elbow_Int_Corr_01', 'MLD_LOC_L_Elbow_Int_Corr_02',
        'MLD_LOC_L_Elbow_Ext_Corr_01', 'MLD_LOC_L_Elbow_Ext_Corr_02',
        'MLD_LOC_L_Wrist_Ext_Corr_01', 'MLD_LOC_L_Wrist_Ext_Corr_02',
        'MLD_LOC_L_Shoulder_Front_Corr_01', 'MLD_LOC_L_Shoulder_Front_Corr_02',
        'MLD_LOC_L_legRear_Knee_Ext_Corr_01', 'MLD_LOC_L_legRear_Knee_Ext_Corr_02',
        'MLD_LOC_L_legRear_Ankle_Int_Corr_01', 'MLD_LOC_L_legRear_Ankle_Int_Corr_02',
        'MLD_LOC_L_legRear_Up_Ext_Corr_01', 'MLD_LOC_L_legRear_Up_Ext_Corr_02',
        'MLD_LOC_L_legRear_Knee_Int_Corr_01', 'MLD_LOC_L_legRear_Knee_Int_Corr_02',
        'MLD_LOC_L_legRear_Ankle_Ext_Corr_01', 'MLD_LOC_L_legRear_Ankle_Ext_Corr_02',
        'MLD_LOC_L_legRear_Up_Int_Corr_01', 'MLD_LOC_L_legRear_Up_Int_Corr_02',
        'MLD_LOC_L_legRear_SIDE_Corr_01', 'MLD_LOC_L_legRear_SIDE_Corr_02',
       'MLD_LOC_L_legRear_Up_Int_Corr_04'
    ]

    for node in multiply_nodes:
        if cmds.objExists(node):
            cmds.delete(node)
            print("Nodo MultiplyDivide eliminato:", node)
        else:
            print("Nodo MultiplyDivide non trovato:", node)

    locators_to_rotate = [
        "LOC_R_Shoulder_Front_Corr", "LOC_R_Shoulder_Low_Corr", "LOC_R_Shoulder_Up_Corr",
        "LOC_R_Shoulder_Back_Corr", "LOC_R_Elbow_Int_Corr", "LOC_R_Elbow_Ext_Corr",
        "LOC_R_Wrist_Ext_Corr", "LOC_R_legRear_Knee_Int_Corr", "LOC_R_legRear_Knee_Ext_Corr",
        "LOC_R_legRear_Up_Ext_Corr", "LOC_R_legRear_Ankle_Ext_Corr", "LOC_R_legRear_Ankle_Int_Corr",
        "LOC_R_legRear_Up_Int_Corr", "LOC_R_legRear_SIDE_Corr"
    ]

    for locator in locators_to_rotate:
        if cmds.objExists(locator):
            cmds.rotate(0, 0, 180, locator, os=True, r=True)
            print("Rotazione applicata a:", locator)
        else:
            print("Locator non trovato per rotazione:", locator)

def delete_multiply_nodes_fingers(*args):
    multiply_nodes_fingers = [
        'MLD_LOC_L_MiddleTop_Corr_ext_01', 'MLD_LOC_L_MiddleTop_Corr_ext_02', 'MLD_LOC_L_ThumbMid_Corr_ext_01',
        'MLD_LOC_L_ThumbMid_Corr_ext_02', 'MLD_LOC_L_RingBaseInt_Corr_ext_01', 'MLD_LOC_L_RingBaseInt_Corr_ext_02',
        'MLD_LOC_L_IndexBaseInt_Corr_ext_01', 'MLD_LOC_L_IndexBaseInt_Corr_ext_02',
        'MLD_LOC_L_MiddleMid_Corr_ext_01', 'MLD_LOC_L_MiddleMid_Corr_ext_02', 'MLD_LOC_L_MiddleBaseInt_Corr_ext_01',
        'MLD_LOC_L_MiddleBaseInt_Corr_ext_02', 'MLD_LOC_L_PinkyBaseInt_Corr_ext_01',
        'MLD_LOC_L_PinkyBaseInt_Corr_ext_02', 'MLD_LOC_L_MiddleBase_Corr_ext_01', 'MLD_LOC_L_MiddleBase_Corr_ext_02',
        'MLD_LOC_L_IndexMid_Corr_ext_01', 'MLD_LOC_L_IndexMid_Corr_ext_02', 'MLD_LOC_L_IndexBase_Corr_ext_01',
        'MLD_LOC_L_IndexBase_Corr_ext_02', 'MLD_LOC_L_PinkyMid_Corr_ext_01', 'MLD_LOC_L_PinkyMid_Corr_ext_02',
        'MLD_LOC_L_PinkyBase_Corr_ext_01', 'MLD_LOC_L_PinkyBase_Corr_ext_02', 'MLD_LOC_L_ThumbBase_Corr_ext_01',
        'MLD_LOC_L_ThumbBase_Corr_ext_01', 'MLD_LOC_L_ThumbBase_Corr_ext_02', 'MLD_LOC_L_IndexTop_Corr_ext_01',
        'MLD_LOC_L_IndexTop_Corr_ext_02', 'MLD_LOC_L_PinkyTop_Corr_ext_01', 'MLD_LOC_L_PinkyTop_Corr_ext_02',
        'MLD_LOC_L_RingBase_Corr_ext_01', 'MLD_LOC_L_RingBase_Corr_ext_02', 'MLD_LOC_L_RingTop_Corr_ext_01',
        'MLD_LOC_L_RingTop_Corr_ext_02', 'MLD_LOC_L_RingMid_Corr_ext_01', 'MLD_LOC_L_RingMid_Corr_ext_02'
    ]

    for node in multiply_nodes_fingers:
        if cmds.objExists(node):
            cmds.delete(node)
            print("Nodo MultiplyDivide eliminato:", node)
        else:
            print("Nodo MultiplyDivide non trovato:", node)

    locators_to_rotate_fingers = [
        "LOC_R_IndexBase_Corr", "LOC_R_IndexMid_Corr", "LOC_R_IndexTop_Corr",
        "LOC_R_IndexBase_Int_Corr", "LOC_R_MiddleBase_Corr", "LOC_R_MiddleMid_Corr",
        "LOC_R_MiddleTop_Corr", "LOC_R_MiddleBase_Int_Corr", "LOC_R_RingBase_Corr",
        "LOC_R_RingMid_Corr", "LOC_R_RingTop_Corr", "LOC_R_RingBase_Int_Corr",
        "LOC_R_PinkyBase_Corr", "LOC_R_PinkyMid_Corr", "LOC_R_PinkyTop_Corr",
        "LOC_R_PinkyBase_Int_Corr", "LOC_R_ThumbBase_Corr", "LOC_R_ThumbMid_Corr"
    ]

    for locator in locators_to_rotate_fingers:
        if cmds.objExists(locator):
            cmds.rotate(0, 0, 180, locator, os=True, r=True)
            print("Rotazione applicata a:", locator)
        else:
            print("Locator non trovato per rotazione:", locator)


def importLocator(*args):
    localpipeBackDir = os.path.dirname(localpipe)
    scenesDir = os.path.join(localpipeBackDir, 'python', 'depts', 'rigging', 'tools', 'generic', 'CorrectiveJoints', 'scenes')
    fileToImport = os.path.join(scenesDir, 'CorrectiveLocatorToImportQuadBiped.ma')
    cmds.file(fileToImport, i=True, type="mayaAscii", namespace=":")

    if cmds.objExists("L_upArmWeight_jnt"):
        cmds.matchTransform("LOC_L_Shoulder_Front_Corr", "L_upArmWeight_jnt")
        cmds.rotate(0, 0, 0, "LOC_L_Shoulder_Front_Corr")
        cmds.move(0, 0, 10, "LOC_L_Shoulder_Front_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_Shoulder_Front_Corr")

    if cmds.objExists("L_upArmWeight_jnt"):
        cmds.matchTransform("LOC_L_Shoulder_Low_Corr", "L_upArmWeight_jnt")
        cmds.rotate(90, 0, 0, "LOC_L_Shoulder_Low_Corr")
        cmds.move(0, 0, 10, "LOC_L_Shoulder_Low_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_Shoulder_Low_Corr")

    if cmds.objExists("L_upArmWeight_jnt"):
        cmds.matchTransform("LOC_L_Shoulder_Up_Corr", "L_upArmWeight_jnt")
        cmds.rotate(-90, 0, 0, "LOC_L_Shoulder_Up_Corr")
        cmds.move(0, 0, 10, "LOC_L_Shoulder_Up_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_Shoulder_Up_Corr")

    if cmds.objExists("L_elbowWeight_jnt"):
        cmds.matchTransform("LOC_L_Elbow_Ext_Corr", "L_elbowWeight_jnt")
        cmds.rotate(180, 0, 0, "LOC_L_Elbow_Ext_Corr", os=True, r=True)
        cmds.move(0, 0, 10, "LOC_L_Elbow_Ext_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_Elbow_Ext_Corr")

    if cmds.objExists("L_elbowWeight_jnt"):
        cmds.matchTransform("LOC_L_Elbow_Int_Corr", "L_elbowWeight_jnt")
        cmds.move(0, 0, 10, "LOC_L_Elbow_Int_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_Elbow_Int_Corr")

    if cmds.objExists("L_hand_jnt"):
        cmds.matchTransform("LOC_L_Wrist_Ext_Corr", "L_hand_jnt")
        cmds.rotate(-90, 0, 0, "LOC_L_Wrist_Ext_Corr", os=True, r=True)
        cmds.move(0, 0, 5, "LOC_L_Wrist_Ext_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_Wrist_Ext_Corr")

    if cmds.objExists("L_upArmWeight_jnt"):
        cmds.matchTransform("LOC_L_Shoulder_Back_Corr", "L_upArmWeight_jnt")
        cmds.rotate(0, 0, 0, "LOC_L_Shoulder_Back_Corr")
        cmds.move(0, 0, -10, "LOC_L_Shoulder_Back_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_Shoulder_Back_Corr")

    if cmds.objExists("L_hip1_jnt"):
        cmds.matchTransform("LOC_L_legRear_Up_Int_Corr", "L_hip1_jnt")
        cmds.rotate(0, 0, -90, "LOC_L_legRear_Up_Int_Corr", os=True, r=True)
        cmds.rotate(180, 0, 0, "LOC_L_legRear_Up_Int_Corr", os=True, r=True)
        cmds.move(14, -4, -1, "LOC_L_legRear_Up_Int_Corr", os=True, r=True)

    else:
        cmds.delete("LOC_L_legRear_Up_Int_Corr")

    if cmds.objExists("L_hip1_jnt"):
        cmds.matchTransform("LOC_L_legRear_Up_Ext_Corr", "L_hip1_jnt")
        cmds.rotate(0, 0, -90, "LOC_L_legRear_Up_Ext_Corr", os=True, r=True)
        cmds.rotate(-180, 0, 0, "LOC_L_legRear_Up_Ext_Corr", os=True, r=True)
        cmds.rotate(0, 0, 180, "LOC_L_legRear_Up_Ext_Corr", os=True, r=True)
        cmds.move(10, 4, -1, "LOC_L_legRear_Up_Ext_Corr", os=True, r=True)

    else:
        cmds.delete("LOC_L_legRear_Up_Ext_Corr")

    if cmds.objExists("L_hip2_jnt"):
        cmds.matchTransform("LOC_L_legRear_Knee_Int_Corr", "L_hip2_jnt")
        cmds.rotate(0, 0, 0, "LOC_L_legRear_Knee_Int_Corr")
        cmds.move(0, -1, 10, "LOC_L_legRear_Knee_Int_Corr", os=True, r=True)
        cmds.rotate(0, -90, 0, "LOC_L_legRear_Knee_Int_Corr")

    else:
        cmds.delete("LOC_L_legRear_Knee_Int_Corr")

    if cmds.objExists("L_hip2_jnt"):
        cmds.matchTransform("LOC_L_legRear_Knee_Ext_Corr", "L_hip2_jnt")
        cmds.rotate(0, 0, 0, "LOC_L_legRear_Knee_Ext_Corr")
        cmds.rotate(-180, 0, 0, "LOC_L_legRear_Knee_Ext_Corr", os=True, r=True)
        cmds.move(0, 1, 10, "LOC_L_legRear_Knee_Ext_Corr", os=True, r=True)
        cmds.rotate(0, -90, 0, "LOC_L_legRear_Knee_Ext_Corr", os=True, r=True)

    else:
        cmds.delete("LOC_L_legRear_Knee_Ext_Corr")

    if cmds.objExists("L_ankle_jnt"):
        cmds.matchTransform("LOC_L_legRear_Ankle_Int_Corr", "L_ankle_jnt")
        cmds.rotate(0, 0, 0, "LOC_L_legRear_Ankle_Int_Corr")
        cmds.rotate(-25, 0, 0, "LOC_L_legRear_Ankle_Int_Corr", os=True, r=True)
        cmds.move(0, 0, 10, "LOC_L_legRear_Ankle_Int_Corr", os=True, r=True)
        cmds.rotate(0, -90, 0, "LOC_L_legRear_Ankle_Int_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_legRear_Ankle_Int_Corr")

    if cmds.objExists("L_ankle_jnt"):
        cmds.matchTransform("LOC_L_legRear_Ankle_Ext_Corr", "L_ankle_jnt")
        cmds.rotate(0, 0, 0, "LOC_L_legRear_Ankle_Ext_Corr")
        cmds.rotate(-180, 0, 0, "LOC_L_legRear_Ankle_Ext_Corr", os=True, r=True)
        cmds.move(0, 0, 10, "LOC_L_legRear_Ankle_Ext_Corr", os=True, r=True)
        cmds.rotate(0, -90, 0, "LOC_L_legRear_Ankle_Ext_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_legRear_Ankle_Ext_Corr")

    if cmds.objExists("L_hip1_jnt"):
        cmds.matchTransform("LOC_L_legRear_SIDE_Corr", "L_hip1_jnt")
        cmds.move(-3, 0, 7, "LOC_L_legRear_SIDE_Corr", os=True, r=True)
        cmds.rotate(-90, -90, 0, "LOC_L_legRear_SIDE_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_legRear_SIDE_Corr")


def importLocatorFingers(*args):
    localpipeBackDir = os.path.dirname(localpipe)
    scenesDir = os.path.join(localpipeBackDir, 'python', 'depts', 'rigging', 'tools', 'generic', 'CorrectiveJoints', 'scenes')
    fileToImport = os.path.join(scenesDir, 'CorrectiveFingers.ma')
    cmds.file(fileToImport, i=True, type="mayaAscii", namespace=":")

    if cmds.objExists("L_indexBase_fk_jnt"):
        cmds.matchTransform("LOC_L_IndexBase_Corr", "L_indexBase_fk_jnt")
        cmds.move(-1, 0, 5, "LOC_L_IndexBase_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_IndexBase_Corr")

    if cmds.objExists("L_indexMid_fk_jnt"):
        cmds.matchTransform("LOC_L_IndexMid_Corr", "L_indexMid_fk_jnt")
        cmds.move(0, 0, 5, "LOC_L_IndexMid_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_IndexMid_Corr")

    if cmds.objExists("L_indexTip_fk_jnt"):
        cmds.matchTransform("LOC_L_IndexTop_Corr", "L_indexTip_fk_jnt")
        cmds.move(0, 0, 5, "LOC_L_IndexTop_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_IndexTop_Corr")

    if cmds.objExists("L_indexBase_fk_jnt"):
        cmds.matchTransform("LOC_L_IndexBase_Int_Corr", "L_indexBase_fk_jnt")
        cmds.rotate(180, 0, 0, "LOC_L_IndexBase_Int_Corr", os=True, r=True)
        cmds.move(0, 0, 5, "LOC_L_IndexBase_Int_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_IndexBase_Int_Corr")

    if cmds.objExists("L_middleBase_fk_jnt"):
        cmds.matchTransform("LOC_L_MiddleBase_Corr", "L_middleBase_fk_jnt")
        cmds.move(-1, 0, 5, "LOC_L_MiddleBase_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_MiddleBase_Corr")

    if cmds.objExists("L_middleMid_fk_jnt"):
        cmds.matchTransform("LOC_L_MiddleMid_Corr", "L_middleMid_fk_jnt")
        cmds.move(0, 0, 5, "LOC_L_MiddleMid_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_MiddleMid_Corr")

    if cmds.objExists("L_middleTip_fk_jnt"):
        cmds.matchTransform("LOC_L_MiddleTop_Corr", "L_middleTip_fk_jnt")
        cmds.move(0, 0, 5, "LOC_L_MiddleTop_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_MiddleTop_Corr")

    if cmds.objExists("L_middleBase_fk_jnt"):
        cmds.matchTransform("LOC_L_MiddleBase_Int_Corr", "L_middleBase_fk_jnt")
        cmds.rotate(180, 0, 0, "LOC_L_MiddleBase_Int_Corr", os=True, r=True)
        cmds.move(0, 0, 5, "LOC_L_MiddleBase_Int_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_MiddleBase_Int_Corr")

    if cmds.objExists("L_ringBase_fk_jnt"):
        cmds.matchTransform("LOC_L_RingBase_Corr", "L_ringBase_fk_jnt")
        cmds.move(-1, 0, 5, "LOC_L_RingBase_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_RingBase_Corr")

    if cmds.objExists("L_ringMid_fk_jnt"):
        cmds.matchTransform("LOC_L_RingMid_Corr", "L_ringMid_fk_jnt")
        cmds.move(0, 0, 5, "LOC_L_RingMid_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_RingMid_Corr")

    if cmds.objExists("L_ringTip_fk_jnt"):
        cmds.matchTransform("LOC_L_RingTop_Corr", "L_ringTip_fk_jnt")
        cmds.move(0, 0, 5, "LOC_L_RingTop_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_RingTop_Corr")

    if cmds.objExists("L_ringBase_fk_jnt"):
        cmds.matchTransform("LOC_L_RingBase_Int_Corr", "L_ringBase_fk_jnt")
        cmds.rotate(180, 0, 0, "LOC_L_RingBase_Int_Corr", os=True, r=True)
        cmds.move(0, 0, 5, "LOC_L_RingBase_Int_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_RingBase_Int_Corr")

    if cmds.objExists("L_pinkyBase_fk_jnt"):
        cmds.matchTransform("LOC_L_PinkyBase_Corr", "L_pinkyBase_fk_jnt")
        cmds.move(-1, 0, 5, "LOC_L_PinkyBase_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_PinkyBase_Corr")

    if cmds.objExists("L_pinkyMid_fk_jnt"):
        cmds.matchTransform("LOC_L_PinkyMid_Corr", "L_pinkyMid_fk_jnt")
        cmds.move(0, 0, 5, "LOC_L_PinkyMid_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_PinkyMid_Corr")

    if cmds.objExists("L_pinkyTip_fk_jnt"):
        cmds.matchTransform("LOC_L_PinkyTop_Corr", "L_pinkyTip_fk_jnt")
        cmds.move(0, 0, 5, "LOC_L_PinkyTop_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_PinkyTop_Corr")

    if cmds.objExists("L_pinkyBase_fk_jnt"):
        cmds.matchTransform("LOC_L_PinkyBase_Int_Corr", "L_pinkyBase_fk_jnt")
        cmds.rotate(180, 0, 0, "LOC_L_PinkyBase_Int_Corr", os=True, r=True)
        cmds.move(0, 0, 5, "LOC_L_PinkyBase_Int_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_PinkyBase_Int_Corr")

    if cmds.objExists("L_thumbMid_fk_jnt"):
        cmds.matchTransform("LOC_L_ThumbBase_Corr", "L_thumbMid_fk_jnt")
        cmds.rotate(90, 0, 0, "LOC_L_ThumbBase_Corr", os=True, r=True)
        cmds.move(0, 0, 5, "LOC_L_ThumbBase_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_ThumbBase_Corr")

    if cmds.objExists("L_thumbTip_fk_jnt"):
        cmds.matchTransform("LOC_L_ThumbMid_Corr", "L_thumbTip_fk_jnt")
        cmds.rotate(90, 0, 0, "LOC_L_ThumbMid_Corr", os=True, r=True)
        cmds.move(0, 0, 5, "LOC_L_ThumbMid_Corr", os=True, r=True)
    else:
        cmds.delete("LOC_L_ThumbMid_Corr")


def linkWiki(*args):
    import webbrowser
    pathToHelp = "https://sites.google.com/rbw-cgi.it/correctivejoints/home-page"
    webbrowser.open(pathToHelp)


from PySide2 import QtWidgets, QtGui

class AttributeExporterUI(QtWidgets.QWidget):
    def __init__(self):
        super(AttributeExporterUI, self).__init__()

        self.setWindowTitle("Attribute Exporter/Importer")
        self.setGeometry(300, 300, 300, 100)

        self.create_widgets()

    def create_widgets(self):
        layout = QtWidgets.QVBoxLayout()

        # Pulsante per l'esportazione
        export_button = QtWidgets.QPushButton("Export Attributes")
        export_button.clicked.connect(self.export_attributes)
        layout.addWidget(export_button)

        # Pulsante per l'importazione
        import_button = QtWidgets.QPushButton("Import Attributes")
        import_button.clicked.connect(self.import_attributes)
        layout.addWidget(import_button)

        self.setLayout(layout)

    def export_attributes(self):
        # Lista di attributi specifici
        attributes_to_export = [
            "CND_L_BackLeg_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_BackLeg_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_BackLeg_Corr_Position_loc_Translate_X.operation",
            "CND_L_BackLeg_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_BackLeg_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_BackLeg_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_BackLeg_Corr_Position_loc_Translate_Y.operation",
            "CND_L_BackLeg_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_BackLeg_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_BackLeg_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_BackLeg_Corr_Position_loc_Translate_Z.operation",
            "CND_L_BackLeg_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_BackLeg_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_BackLeg_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_BackLeg_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_Elbow_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_Elbow_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_Elbow_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_Elbow_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_Elbow_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_Elbow_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_X.operation",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_Y.operation",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_Z.operation",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_Shoulder_Back_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_Shoulder_Back_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_Shoulder_Back_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_X.operation",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_Y.operation",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_Z.operation",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_Shoulder_Front_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_Shoulder_Front_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_Shoulder_Front_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_X.operation",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_Y.operation",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_Z.operation",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_Shoulder_Low_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_Shoulder_Low_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_Shoulder_Low_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_X.operation",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_Y.operation",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_Z.operation",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_Shoulder_Up_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_Shoulder_Up_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_Shoulder_Up_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_UpLeg_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_UpLeg_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_UpLeg_Corr_Position_loc_Translate_X.operation",
            "CND_L_UpLeg_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_UpLeg_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_UpLeg_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_UpLeg_Corr_Position_loc_Translate_Y.operation",
            "CND_L_UpLeg_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_UpLeg_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_UpLeg_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_UpLeg_Corr_Position_loc_Translate_Z.operation",
            "CND_L_UpLeg_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_UpLeg_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_UpLeg_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_UpLeg_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_Wrist_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_Wrist_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_Wrist_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_BackLeg_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_BackLeg_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_BackLeg_Corr_Position_loc_Translate_X.operation",
            "CND_R_BackLeg_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_BackLeg_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_BackLeg_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_BackLeg_Corr_Position_loc_Translate_Y.operation",
            "CND_R_BackLeg_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_BackLeg_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_BackLeg_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_BackLeg_Corr_Position_loc_Translate_Z.operation",
            "CND_R_BackLeg_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_BackLeg_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_BackLeg_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_BackLeg_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_Elbow_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_Elbow_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_Elbow_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_Elbow_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_Elbow_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_Elbow_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_X.operation",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_Y.operation",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_Z.operation",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_Shoulder_Back_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_Shoulder_Back_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_Shoulder_Back_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_X.operation",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_Y.operation",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_Z.operation",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_Shoulder_Front_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_Shoulder_Front_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_Shoulder_Front_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_X.operation",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_Y.operation",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_Z.operation",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_Shoulder_Low_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_Shoulder_Low_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_Shoulder_Low_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_X.operation",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_Y.operation",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_Z.operation",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_Shoulder_Up_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_Shoulder_Up_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_Shoulder_Up_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_UpLeg_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_UpLeg_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_UpLeg_Corr_Position_loc_Translate_X.operation",
            "CND_R_UpLeg_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_UpLeg_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_UpLeg_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_UpLeg_Corr_Position_loc_Translate_Y.operation",
            "CND_R_UpLeg_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_UpLeg_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_UpLeg_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_UpLeg_Corr_Position_loc_Translate_Z.operation",
            "CND_R_UpLeg_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_UpLeg_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_UpLeg_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_UpLeg_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_Wrist_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_Wrist_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_Wrist_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "MLD_L_MiddleBase_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_MiddleBase_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_MiddleBase_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_IndexBase_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_IndexBase_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_IndexBase_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_PinkyBase_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_PinkyBase_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_PinkyBase_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_RingBase_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_RingBase_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_RingBase_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_Y.operation",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_X.operation",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_Z.operation",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_MiddleBase_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_MiddleBase_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_MiddleBase_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_Z.operation",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_X.operation",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_Y.operation",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_Y_Zero.operation",
            "MLD_L_ThumbBase_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_ThumbBase_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_ThumbBase_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_X.operation",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_Y.operation",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_Z.operation",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_MiddleMid_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_MiddleMid_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_MiddleMid_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_X.operation",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_Y.operation",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_Z.operation",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_ThumbMid_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_ThumbMid_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_ThumbMid_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_Z.operation",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_Y.operation",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_X.operation",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_L_MiddleTop_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_MiddleTop_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_MiddleTop_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_IndexBase_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_IndexBase_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_IndexBase_Corr_Position_loc_Translate_Z.operation",
            "CND_L_IndexBase_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_L_IndexBase_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_IndexBase_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_IndexBase_Corr_Position_loc_Translate_Y.operation",
            "CND_L_IndexBase_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_IndexBase_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_IndexBase_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_IndexBase_Corr_Position_loc_Translate_X.operation",
            "CND_L_IndexBase_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_L_IndexBase_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_IndexBase_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_IndexBase_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_X.operation",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_Y.operation",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_Z.operation",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_PinkyBase_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_PinkyBase_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_PinkyBase_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_X.operation",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_Y.operation",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_Z.operation",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_PinkyMid_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_PinkyMid_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_PinkyMid_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_IndexMid_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_IndexMid_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_IndexMid_Corr_Position_loc_Translate_Z.operation",
            "CND_L_IndexMid_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_L_IndexMid_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_IndexMid_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_IndexMid_Corr_Position_loc_Translate_X.operation",
            "CND_L_IndexMid_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_IndexMid_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_IndexMid_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_IndexMid_Corr_Position_loc_Translate_Y.operation",
            "CND_L_IndexMid_Corr_Position_loc_Translate_Y_Zero.operation",
            "MLD_L_IndexMid_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_IndexMid_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_IndexMid_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_RingBase_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_RingBase_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_RingBase_Corr_Position_loc_Translate_Y.operation",
            "CND_L_RingBase_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_RingBase_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_RingBase_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_RingBase_Corr_Position_loc_Translate_Z.operation",
            "CND_L_RingBase_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_L_RingBase_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_RingBase_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_RingBase_Corr_Position_loc_Translate_X.operation",
            "CND_L_RingBase_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_L_RingBase_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_RingBase_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_RingBase_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_IndexTop_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_IndexTop_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_IndexTop_Corr_Position_loc_Translate_Z.operation",
            "CND_L_IndexTop_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_L_IndexTop_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_IndexTop_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_IndexTop_Corr_Position_loc_Translate_Y.operation",
            "CND_L_IndexTop_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_IndexTop_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_IndexTop_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_IndexTop_Corr_Position_loc_Translate_X.operation",
            "CND_L_IndexTop_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_L_IndexTop_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_IndexTop_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_IndexTop_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_Z.operation",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_Y.operation",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_X.operation",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_L_PinkyTop_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_PinkyTop_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_PinkyTop_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_RingMid_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_RingMid_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_RingMid_Corr_Position_loc_Translate_Z.operation",
            "CND_L_RingMid_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_L_RingMid_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_RingMid_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_RingMid_Corr_Position_loc_Translate_Y.operation",
            "CND_L_RingMid_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_RingMid_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_RingMid_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_RingMid_Corr_Position_loc_Translate_X.operation",
            "CND_L_RingMid_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_L_RingMid_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_RingMid_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_RingMid_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_RingTop_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_RingTop_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_RingTop_Corr_Position_loc_Translate_X.operation",
            "CND_L_RingTop_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_RingTop_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_RingTop_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_RingTop_Corr_Position_loc_Translate_Y.operation",
            "CND_L_RingTop_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_RingTop_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_RingTop_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_RingTop_Corr_Position_loc_Translate_Z.operation",
            "CND_L_RingTop_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_RingTop_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_RingTop_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_RingTop_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "MLD_R_MiddleBase_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_MiddleBase_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_MiddleBase_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_IndexBase_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_IndexBase_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_IndexBase_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_PinkyBase_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_PinkyBase_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_PinkyBase_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_RingBase_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_RingBase_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_RingBase_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_Y.operation",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_X.operation",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_Z.operation",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_MiddleBase_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_MiddleBase_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_MiddleBase_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_Z.operation",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_X.operation",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_Y.operation",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_Y_Zero.operation",
            "MLD_R_ThumbBase_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_ThumbBase_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_ThumbBase_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_X.operation",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_Y.operation",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_Z.operation",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_MiddleMid_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_MiddleMid_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_MiddleMid_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_X.operation",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_Y.operation",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_Z.operation",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_ThumbMid_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_ThumbMid_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_ThumbMid_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_Z.operation",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_Y.operation",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_X.operation",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_R_MiddleTop_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_MiddleTop_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_MiddleTop_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_IndexBase_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_IndexBase_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_IndexBase_Corr_Position_loc_Translate_Z.operation",
            "CND_R_IndexBase_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_R_IndexBase_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_IndexBase_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_IndexBase_Corr_Position_loc_Translate_Y.operation",
            "CND_R_IndexBase_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_IndexBase_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_IndexBase_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_IndexBase_Corr_Position_loc_Translate_X.operation",
            "CND_R_IndexBase_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_R_IndexBase_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_IndexBase_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_IndexBase_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_X.operation",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_Y.operation",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_Z.operation",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_PinkyBase_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_PinkyBase_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_PinkyBase_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_X.operation",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_Y.operation",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_Z.operation",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_PinkyMid_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_PinkyMid_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_PinkyMid_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_IndexMid_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_IndexMid_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_IndexMid_Corr_Position_loc_Translate_Z.operation",
            "CND_R_IndexMid_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_R_IndexMid_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_IndexMid_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_IndexMid_Corr_Position_loc_Translate_X.operation",
            "CND_R_IndexMid_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_IndexMid_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_IndexMid_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_IndexMid_Corr_Position_loc_Translate_Y.operation",
            "CND_R_IndexMid_Corr_Position_loc_Translate_Y_Zero.operation",
            "MLD_R_IndexMid_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_IndexMid_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_IndexMid_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_RingBase_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_RingBase_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_RingBase_Corr_Position_loc_Translate_Y.operation",
            "CND_R_RingBase_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_RingBase_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_RingBase_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_RingBase_Corr_Position_loc_Translate_Z.operation",
            "CND_R_RingBase_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_R_RingBase_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_RingBase_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_RingBase_Corr_Position_loc_Translate_X.operation",
            "CND_R_RingBase_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_R_RingBase_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_RingBase_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_RingBase_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_IndexTop_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_IndexTop_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_IndexTop_Corr_Position_loc_Translate_Z.operation",
            "CND_R_IndexTop_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_R_IndexTop_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_IndexTop_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_IndexTop_Corr_Position_loc_Translate_Y.operation",
            "CND_R_IndexTop_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_IndexTop_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_IndexTop_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_IndexTop_Corr_Position_loc_Translate_X.operation",
            "CND_R_IndexTop_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_R_IndexTop_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_IndexTop_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_IndexTop_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_Z.operation",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_Y.operation",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_X.operation",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_R_PinkyTop_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_PinkyTop_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_PinkyTop_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_RingMid_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_RingMid_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_RingMid_Corr_Position_loc_Translate_Z.operation",
            "CND_R_RingMid_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_R_RingMid_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_RingMid_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_RingMid_Corr_Position_loc_Translate_Y.operation",
            "CND_R_RingMid_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_RingMid_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_RingMid_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_RingMid_Corr_Position_loc_Translate_X.operation",
            "CND_R_RingMid_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_R_RingMid_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_RingMid_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_RingMid_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_RingTop_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_RingTop_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_RingTop_Corr_Position_loc_Translate_X.operation",
            "CND_R_RingTop_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_RingTop_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_RingTop_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_RingTop_Corr_Position_loc_Translate_Y.operation",
            "CND_R_RingTop_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_RingTop_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_RingTop_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_RingTop_Corr_Position_loc_Translate_Z.operation",
            "CND_R_RingTop_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_RingTop_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_RingTop_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_RingTop_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_Neck_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_Neck_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_Neck_Corr_Position_loc_Translate_X.operation",
            "CND_L_Neck_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_Neck_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_Neck_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_Neck_Corr_Position_loc_Translate_Y.operation",
            "CND_L_Neck_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_Neck_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_Neck_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_Neck_Corr_Position_loc_Translate_Z.operation",
            "CND_L_Neck_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_Neck_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_Neck_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_Neck_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_Neck_Low_Corr_Position_loc_Translate_X.secondTerm",
            "CND_Neck_Low_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_Neck_Low_Corr_Position_loc_Translate_X.operation",
            "CND_Neck_Low_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_Neck_Low_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_Neck_Low_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_Neck_Low_Corr_Position_loc_Translate_Y.operation",
            "CND_Neck_Low_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_Neck_Low_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_Neck_Low_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_Neck_Low_Corr_Position_loc_Translate_Z.operation",
            "CND_Neck_Low_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_Neck_Low_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_Neck_Low_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_Neck_Low_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_Neck_Up_Corr_Position_loc_Translate_X.secondTerm",
            "CND_Neck_Up_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_Neck_Up_Corr_Position_loc_Translate_X.operation",
            "CND_Neck_Up_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_Neck_Up_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_Neck_Up_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_Neck_Up_Corr_Position_loc_Translate_Y.operation",
            "CND_Neck_Up_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_Neck_Up_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_Neck_Up_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_Neck_Up_Corr_Position_loc_Translate_Z.operation",
            "CND_Neck_Up_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_Neck_Up_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_Neck_Up_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_Neck_Up_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_Neck_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_Neck_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_Neck_Corr_Position_loc_Translate_X.operation",
            "CND_R_Neck_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_Neck_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_Neck_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_Neck_Corr_Position_loc_Translate_Y.operation",
            "CND_R_Neck_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_Neck_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_Neck_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_Neck_Corr_Position_loc_Translate_Z.operation",
            "CND_R_Neck_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_Neck_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_Neck_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_Neck_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_Z.operation",
            "MLD_R_legRear_SIDE_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_SIDE_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_SIDE_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_Z.operation",
            "MLD_L_legRear_SIDE_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_SIDE_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_SIDE_Corr_Position_loc_Rotation_Multiply.input1Z"
        ]

        # Specifica il percorso e il nome del file per l'esportazione
        export_file_path, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Export Attributes", "",
                                                                    "Text Files (*.txt);;")
        if export_file_path:
            attributes_to_export_existing = [attr for attr in attributes_to_export if cmds.objExists(attr)]
            export_attributes(export_file_path, attributes_to_export_existing)

    def import_attributes(self):
        # Lista di attributi specifici
        attributes_to_import = [

            "CND_L_BackLeg_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_BackLeg_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_BackLeg_Corr_Position_loc_Translate_X.operation",
            "CND_L_BackLeg_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_BackLeg_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_BackLeg_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_BackLeg_Corr_Position_loc_Translate_Y.operation",
            "CND_L_BackLeg_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_BackLeg_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_BackLeg_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_BackLeg_Corr_Position_loc_Translate_Z.operation",
            "CND_L_BackLeg_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_BackLeg_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_BackLeg_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_BackLeg_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_Elbow_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_Elbow_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_Elbow_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_Elbow_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_Elbow_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_Elbow_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_Elbow_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_Elbow_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_Knee_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_Knee_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_X.operation",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_Y.operation",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_Z.operation",
            "CND_L_Shoulder_Back_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_Shoulder_Back_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_Shoulder_Back_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_Shoulder_Back_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_X.operation",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_Y.operation",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_Z.operation",
            "CND_L_Shoulder_Front_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_Shoulder_Front_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_Shoulder_Front_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_Shoulder_Front_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_X.operation",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_Y.operation",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_Z.operation",
            "CND_L_Shoulder_Low_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_Shoulder_Low_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_Shoulder_Low_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_Shoulder_Low_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_X.operation",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_Y.operation",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_Z.operation",
            "CND_L_Shoulder_Up_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_Shoulder_Up_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_Shoulder_Up_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_Shoulder_Up_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_UpLeg_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_UpLeg_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_UpLeg_Corr_Position_loc_Translate_X.operation",
            "CND_L_UpLeg_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_UpLeg_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_UpLeg_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_UpLeg_Corr_Position_loc_Translate_Y.operation",
            "CND_L_UpLeg_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_UpLeg_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_UpLeg_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_UpLeg_Corr_Position_loc_Translate_Z.operation",
            "CND_L_UpLeg_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_UpLeg_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_UpLeg_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_UpLeg_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_Wrist_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_Wrist_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_Wrist_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_Wrist_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_BackLeg_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_BackLeg_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_BackLeg_Corr_Position_loc_Translate_X.operation",
            "CND_R_BackLeg_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_BackLeg_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_BackLeg_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_BackLeg_Corr_Position_loc_Translate_Y.operation",
            "CND_R_BackLeg_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_BackLeg_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_BackLeg_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_BackLeg_Corr_Position_loc_Translate_Z.operation",
            "CND_R_BackLeg_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_BackLeg_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_BackLeg_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_BackLeg_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_Elbow_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_Elbow_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_Elbow_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_Elbow_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_Elbow_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_Elbow_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_Elbow_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_Elbow_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_Knee_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_Knee_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_X.operation",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_Y.operation",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_Z.operation",
            "CND_R_Shoulder_Back_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_Shoulder_Back_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_Shoulder_Back_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_Shoulder_Back_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_X.operation",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_Y.operation",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_Z.operation",
            "CND_R_Shoulder_Front_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_Shoulder_Front_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_Shoulder_Front_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_Shoulder_Front_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_X.operation",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_Y.operation",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_Z.operation",
            "CND_R_Shoulder_Low_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_Shoulder_Low_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_Shoulder_Low_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_Shoulder_Low_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_X.operation",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_Y.operation",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_Z.operation",
            "CND_R_Shoulder_Up_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_Shoulder_Up_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_Shoulder_Up_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_Shoulder_Up_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_UpLeg_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_UpLeg_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_UpLeg_Corr_Position_loc_Translate_X.operation",
            "CND_R_UpLeg_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_UpLeg_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_UpLeg_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_UpLeg_Corr_Position_loc_Translate_Y.operation",
            "CND_R_UpLeg_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_UpLeg_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_UpLeg_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_UpLeg_Corr_Position_loc_Translate_Z.operation",
            "CND_R_UpLeg_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_UpLeg_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_UpLeg_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_UpLeg_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_Wrist_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_Wrist_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_Wrist_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_Wrist_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_MiddleBase_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "MLD_L_MiddleBase_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_MiddleBase_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_MiddleBase_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_IndexBase_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_IndexBase_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_IndexBase_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_IndexBase_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_PinkyBase_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_PinkyBase_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_PinkyBase_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_PinkyBase_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_RingBase_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_RingBase_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_RingBase_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_RingBase_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_Y.operation",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_X.operation",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_Z.operation",
            "CND_L_MiddleBase_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_MiddleBase_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_MiddleBase_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_MiddleBase_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_Z.operation",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_X.operation",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_Y.operation",
            "CND_L_ThumbBase_Corr_Position_loc_Translate_Y_Zero.operation",
            "MLD_L_ThumbBase_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_ThumbBase_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_ThumbBase_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_X.operation",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_Y.operation",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_Z.operation",
            "CND_L_MiddleMid_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_MiddleMid_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_MiddleMid_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_MiddleMid_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_X.operation",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_Y.operation",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_Z.operation",
            "CND_L_ThumbMid_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_ThumbMid_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_ThumbMid_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_ThumbMid_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_Z.operation",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_Y.operation",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_X.operation",
            "CND_L_MiddleTop_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_L_MiddleTop_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_MiddleTop_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_MiddleTop_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_IndexBase_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_IndexBase_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_IndexBase_Corr_Position_loc_Translate_Z.operation",
            "CND_L_IndexBase_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_L_IndexBase_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_IndexBase_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_IndexBase_Corr_Position_loc_Translate_Y.operation",
            "CND_L_IndexBase_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_IndexBase_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_IndexBase_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_IndexBase_Corr_Position_loc_Translate_X.operation",
            "CND_L_IndexBase_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_L_IndexBase_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_IndexBase_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_IndexBase_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_X.operation",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_Y.operation",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_Z.operation",
            "CND_L_PinkyBase_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_PinkyBase_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_PinkyBase_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_PinkyBase_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_X.operation",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_Y.operation",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_Z.operation",
            "CND_L_PinkyMid_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_PinkyMid_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_PinkyMid_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_PinkyMid_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_IndexMid_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_IndexMid_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_IndexMid_Corr_Position_loc_Translate_Z.operation",
            "CND_L_IndexMid_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_L_IndexMid_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_IndexMid_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_IndexMid_Corr_Position_loc_Translate_X.operation",
            "CND_L_IndexMid_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_IndexMid_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_IndexMid_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_IndexMid_Corr_Position_loc_Translate_Y.operation",
            "CND_L_IndexMid_Corr_Position_loc_Translate_Y_Zero.operation",
            "MLD_L_IndexMid_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_IndexMid_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_IndexMid_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_RingBase_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_RingBase_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_RingBase_Corr_Position_loc_Translate_Y.operation",
            "CND_L_RingBase_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_RingBase_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_RingBase_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_RingBase_Corr_Position_loc_Translate_Z.operation",
            "CND_L_RingBase_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_L_RingBase_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_RingBase_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_RingBase_Corr_Position_loc_Translate_X.operation",
            "CND_L_RingBase_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_L_RingBase_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_RingBase_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_RingBase_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_IndexTop_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_IndexTop_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_IndexTop_Corr_Position_loc_Translate_Z.operation",
            "CND_L_IndexTop_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_L_IndexTop_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_IndexTop_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_IndexTop_Corr_Position_loc_Translate_Y.operation",
            "CND_L_IndexTop_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_IndexTop_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_IndexTop_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_IndexTop_Corr_Position_loc_Translate_X.operation",
            "CND_L_IndexTop_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_L_IndexTop_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_IndexTop_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_IndexTop_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_Z.operation",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_Y.operation",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_X.operation",
            "CND_L_PinkyTop_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_L_PinkyTop_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_PinkyTop_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_PinkyTop_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_RingMid_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_RingMid_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_RingMid_Corr_Position_loc_Translate_Z.operation",
            "CND_L_RingMid_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_L_RingMid_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_RingMid_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_RingMid_Corr_Position_loc_Translate_Y.operation",
            "CND_L_RingMid_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_RingMid_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_RingMid_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_RingMid_Corr_Position_loc_Translate_X.operation",
            "CND_L_RingMid_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_L_RingMid_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_RingMid_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_RingMid_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_RingTop_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_RingTop_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_RingTop_Corr_Position_loc_Translate_X.operation",
            "CND_L_RingTop_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_RingTop_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_RingTop_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_RingTop_Corr_Position_loc_Translate_Y.operation",
            "CND_L_RingTop_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_RingTop_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_RingTop_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_RingTop_Corr_Position_loc_Translate_Z.operation",
            "CND_L_RingTop_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_RingTop_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_RingTop_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_RingTop_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_MiddleBase_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "MLD_R_MiddleBase_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_MiddleBase_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_MiddleBase_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_IndexBase_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_IndexBase_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_IndexBase_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_IndexBase_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_PinkyBase_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_PinkyBase_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_PinkyBase_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_PinkyBase_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_RingBase_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_RingBase_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_RingBase_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_RingBase_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_Y.operation",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_X.operation",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_Z.operation",
            "CND_R_MiddleBase_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_MiddleBase_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_MiddleBase_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_MiddleBase_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_Z.operation",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_X.operation",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_Y.operation",
            "CND_R_ThumbBase_Corr_Position_loc_Translate_Y_Zero.operation",
            "MLD_R_ThumbBase_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_ThumbBase_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_ThumbBase_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_X.operation",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_Y.operation",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_Z.operation",
            "CND_R_MiddleMid_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_MiddleMid_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_MiddleMid_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_MiddleMid_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_X.operation",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_Y.operation",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_Z.operation",
            "CND_R_ThumbMid_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_ThumbMid_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_ThumbMid_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_ThumbMid_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_Z.operation",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_Y.operation",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_X.operation",
            "CND_R_MiddleTop_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_R_MiddleTop_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_MiddleTop_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_MiddleTop_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_IndexBase_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_IndexBase_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_IndexBase_Corr_Position_loc_Translate_Z.operation",
            "CND_R_IndexBase_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_R_IndexBase_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_IndexBase_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_IndexBase_Corr_Position_loc_Translate_Y.operation",
            "CND_R_IndexBase_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_IndexBase_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_IndexBase_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_IndexBase_Corr_Position_loc_Translate_X.operation",
            "CND_R_IndexBase_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_R_IndexBase_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_IndexBase_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_IndexBase_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_X.operation",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_Y.operation",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_Z.operation",
            "CND_R_PinkyBase_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_PinkyBase_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_PinkyBase_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_PinkyBase_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_X.operation",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_Y.operation",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_Z.operation",
            "CND_R_PinkyMid_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_PinkyMid_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_PinkyMid_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_PinkyMid_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_IndexMid_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_IndexMid_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_IndexMid_Corr_Position_loc_Translate_Z.operation",
            "CND_R_IndexMid_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_R_IndexMid_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_IndexMid_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_IndexMid_Corr_Position_loc_Translate_X.operation",
            "CND_R_IndexMid_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_IndexMid_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_IndexMid_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_IndexMid_Corr_Position_loc_Translate_Y.operation",
            "CND_R_IndexMid_Corr_Position_loc_Translate_Y_Zero.operation",
            "MLD_R_IndexMid_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_IndexMid_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_IndexMid_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_RingBase_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_RingBase_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_RingBase_Corr_Position_loc_Translate_Y.operation",
            "CND_R_RingBase_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_RingBase_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_RingBase_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_RingBase_Corr_Position_loc_Translate_Z.operation",
            "CND_R_RingBase_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_R_RingBase_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_RingBase_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_RingBase_Corr_Position_loc_Translate_X.operation",
            "CND_R_RingBase_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_R_RingBase_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_RingBase_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_RingBase_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_IndexTop_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_IndexTop_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_IndexTop_Corr_Position_loc_Translate_Z.operation",
            "CND_R_IndexTop_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_R_IndexTop_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_IndexTop_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_IndexTop_Corr_Position_loc_Translate_Y.operation",
            "CND_R_IndexTop_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_IndexTop_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_IndexTop_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_IndexTop_Corr_Position_loc_Translate_X.operation",
            "CND_R_IndexTop_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_R_IndexTop_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_IndexTop_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_IndexTop_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_Z.operation",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_Y.operation",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_X.operation",
            "CND_R_PinkyTop_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_R_PinkyTop_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_PinkyTop_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_PinkyTop_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_RingMid_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_RingMid_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_RingMid_Corr_Position_loc_Translate_Z.operation",
            "CND_R_RingMid_Corr_Position_loc_Translate_Z_Zero.operation",
            "CND_R_RingMid_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_RingMid_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_RingMid_Corr_Position_loc_Translate_Y.operation",
            "CND_R_RingMid_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_RingMid_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_RingMid_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_RingMid_Corr_Position_loc_Translate_X.operation",
            "CND_R_RingMid_Corr_Position_loc_Translate_X_Zero.operation",
            "MLD_R_RingMid_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_RingMid_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_RingMid_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_RingTop_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_RingTop_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_RingTop_Corr_Position_loc_Translate_X.operation",
            "CND_R_RingTop_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_RingTop_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_RingTop_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_RingTop_Corr_Position_loc_Translate_Y.operation",
            "CND_R_RingTop_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_RingTop_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_RingTop_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_RingTop_Corr_Position_loc_Translate_Z.operation",
            "CND_R_RingTop_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_RingTop_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_RingTop_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_RingTop_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Ankle_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Knee_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Knee_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Up_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legFront_Up_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Ankle_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Knee_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Knee_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Up_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_Up_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_Neck_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_Neck_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_Neck_Corr_Position_loc_Translate_X.operation",
            "CND_L_Neck_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_L_Neck_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_Neck_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_Neck_Corr_Position_loc_Translate_Y.operation",
            "CND_L_Neck_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_L_Neck_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_Neck_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_Neck_Corr_Position_loc_Translate_Z.operation",
            "CND_L_Neck_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_L_Neck_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_Neck_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_Neck_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_Neck_Low_Corr_Position_loc_Translate_X.secondTerm",
            "CND_Neck_Low_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_Neck_Low_Corr_Position_loc_Translate_X.operation",
            "CND_Neck_Low_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_Neck_Low_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_Neck_Low_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_Neck_Low_Corr_Position_loc_Translate_Y.operation",
            "CND_Neck_Low_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_Neck_Low_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_Neck_Low_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_Neck_Low_Corr_Position_loc_Translate_Z.operation",
            "CND_Neck_Low_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_Neck_Low_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_Neck_Low_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_Neck_Low_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_Neck_Up_Corr_Position_loc_Translate_X.secondTerm",
            "CND_Neck_Up_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_Neck_Up_Corr_Position_loc_Translate_X.operation",
            "CND_Neck_Up_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_Neck_Up_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_Neck_Up_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_Neck_Up_Corr_Position_loc_Translate_Y.operation",
            "CND_Neck_Up_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_Neck_Up_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_Neck_Up_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_Neck_Up_Corr_Position_loc_Translate_Z.operation",
            "CND_Neck_Up_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_Neck_Up_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_Neck_Up_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_Neck_Up_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Ankle_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Ankle_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Knee_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Knee_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Up_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legFront_Up_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legFront_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Ankle_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Ankle_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Ankle_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Ankle_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Knee_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Knee_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Knee_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Knee_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Up_Ext_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Up_Ext_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_Up_Int_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_Up_Int_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_Neck_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_Neck_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_Neck_Corr_Position_loc_Translate_X.operation",
            "CND_R_Neck_Corr_Position_loc_Translate_X_Zero.operation",
            "CND_R_Neck_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_Neck_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_Neck_Corr_Position_loc_Translate_Y.operation",
            "CND_R_Neck_Corr_Position_loc_Translate_Y_Zero.operation",
            "CND_R_Neck_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_Neck_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_Neck_Corr_Position_loc_Translate_Z.operation",
            "CND_R_Neck_Corr_Position_loc_Translate_Z_Zero.operation",
            "MLD_R_Neck_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_Neck_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_Neck_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_X.secondTerm",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_X.operation",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_Y.operation",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_Z.operation",
            "CND_R_legRear_SIDE_Corr_Position_loc_Translate_Z.operation",
            "MLD_R_legRear_SIDE_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_R_legRear_SIDE_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_R_legRear_SIDE_Corr_Position_loc_Rotation_Multiply.input1Z",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_X.secondTerm",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_X.colorIfTrueR",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_X.operation",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_Y.secondTerm",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_Y.colorIfTrueR",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_Y.operation",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_Z.secondTerm",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_Z.colorIfTrueR",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_Z.operation",
            "CND_L_legRear_SIDE_Corr_Position_loc_Translate_Z.operation",
            "MLD_L_legRear_SIDE_Corr_Position_loc_Rotation_Multiply.input1X",
            "MLD_L_legRear_SIDE_Corr_Position_loc_Rotation_Multiply.input1Y",
            "MLD_L_legRear_SIDE_Corr_Position_loc_Rotation_Multiply.input1Z"

        ]


        import_file_path, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Import Attributes", "",
                                                                    "Text Files (*.txt);;")
        if import_file_path:
            attributes_to_import_existing = [attr for attr in attributes_to_import if cmds.objExists(attr)]
        import_attributes(import_file_path, attributes_to_import_existing)
        QtWidgets.QMessageBox.information(self, "Import Complete", "Attributi importati con successo.")

def export_attributes(file_path, attribute_list):
    # Creazione del dizionario per memorizzare i valori degli attributi
    attribute_values = {}

    for attribute in attribute_list:
        try:
            value = cmds.getAttr(attribute)
            attribute_values[attribute] = value
        except RuntimeError:
            print("Attribute {} not found or has a missing value.".format(attribute))

    # Scrittura dei valori nel file di testo
    with open(file_path, 'w') as file:
        for attribute, value in attribute_values.items():
            file.write("{} {}\n".format(attribute, value))

def import_attributes(file_path, attribute_list):
    # Leggi i valori dal file di testo
    with open(file_path, 'r') as file:
        lines = file.readlines()

    # Creazione del dizionario per memorizzare i valori degli attributi
    attribute_values = {}

    # Popolamento del dizionario con i valori letti dal file di testo
    for line in lines:
        parts = line.strip().split()
        if len(parts) == 2:
            attribute, value = parts
            attribute_values[attribute] = float(value)

    # Applica i valori agli attributi
    for attribute in attribute_list:
        if attribute in attribute_values:
            cmds.setAttr(attribute, attribute_values[attribute])
        else:
            print ("Attribute {} not found in the imported data.".format(attribute))

def AB_poseReaderQuadBiped(*args):
    if cmds.window("CorrectiveJointUIABQuadBiped", q=True, ex=True):
        cmds.deleteUI("CorrectiveJointUIABQuadBiped")

    ww = cmds.window("CorrectiveJointUIABQuadBiped", t="Rainbow CGI - Corrective Joints for AB Quad", widthHeight=(250, 50), sizeable=True, resizeToFitChildren=True)

    # Get a pointer and convert it to Qt Widget object
    qw = omui.MQtUtil.findWindow(ww)
    widget = wrapInstance(int(qw), QWidget)

    # Create a QIcon object
    iconpath = os.path.join(image_path, "RainbowCGI_icona.ico")

    # Assign the icon
    icon = QIcon(iconpath)
    widget.setWindowIcon(icon)

    col = cmds.columnLayout(adjustableColumn=1)

    # Carica l'immagine
    img = os.path.join(image_path, "QuadBiped_AB_PoseReader.png")
    cmds.text(h = 10, l = "")
    cmds.iconTextButton(style="iconOnly", image1=img, p=col, w=100)
    cmds.text(h = 10, l = "")
    cmds.setParent("..")

    col1 = cmds.columnLayout(adj=1, p=col, bgc=[0.2, 0.2, 0.2])
    cmds.rowLayout(numberOfColumns=2, p=col1, adj=2, cat=[(1, "left", 5), (2, "left", 0)])
    cmds.text("RBW - Corrective Joints supporting Deformation for AB QUAD BIPED", h=20)

    cmds.frameLayout(label="Corrective Joints Creation for AB QUAD BIPED", collapsable=1, collapse=0, p=col, bgc=[0.65, 0.5, 0], w=10)
    cmds.columnLayout(adj=1, bgc=[0.2, 0.2, 0.2])
    cmds.button(l="| Import Locator QUAD BIPED |", c=importLocator, h=30, bgc=[0.3, 0.3, 0.3], w=10)
    cmds.button(l="| Delete R Parent Side QUAD BIPED |", c=delete_multiply_nodes, h=30, bgc=[0.3, 0.3, 0.3], w=10)
    cmds.button(l="| Create Corrective Joints QUAD BIPED|", c=process, h=30, bgc=[0.3, 0.3, 0.3], w=10)
    cmds.button(l="| Finalize QUAD BIPED |", c=finalize, h=30, bgc=[0.3, 0.3, 0.3], w=10)
    cmds.button(l="| Double Parent Matrix QUAD BIPED |", c=DoubleParentMatrix, h=30, bgc=[0.2, 0.2, 0.2], w=10)
    cmds.frameLayout(label="Corrective Fingers Module", collapsable=1, collapse=0, p=col, bgc=[0.65, 0.5, 0], w=10)
    cmds.columnLayout(adj=1, bgc=[0.2, 0.2, 0.2])
    cmds.button(l="| Import Fingers Locator |", c=importLocatorFingers, h=30, bgc=[0.3, 0.3, 0.3], w=10)
    cmds.button(l="| Delete R Fingers Parent Side |", c=delete_multiply_nodes_fingers, h=30, bgc=[0.3, 0.3, 0.3], w=10)
    cmds.button(l="| Create Fingers Corrective Joints |", c=process_fingers, h=30, bgc=[0.3, 0.3, 0.3], w=10)
    cmds.button(l="| Finalize Fingers |", c=finalize_fingers, h=30, bgc=[0.3, 0.3, 0.3], w=10)
    cmds.frameLayout(label="Utilities QUAD BIPED", collapsable=1, collapse=0, p=col, bgc=[0.4, 0.3, 0.2], w=10)
    cmds.columnLayout(adj=1, bgc=[0.2, 0.2, 0.2])
    cmds.button(l="| Import Values QUAD BIPED |", c=lambda *args: show_attribute_exporter_ui(), h=30, bgc=[0.3, 0.3, 0.3], w=10)
    cmds.button(l="| WIKI |", c=linkWiki, h=30, bgc=[0.2, 0.2, 0.2], w=15)

    cmds.columnLayout(adj=1, p=col, bgc=[0.2, 0.2, 0.2])
    cmds.showWindow()


def show_attribute_exporter_ui():
    global attribute_exporter_ui

    if attribute_exporter_ui is not None and attribute_exporter_ui.isVisible():
        attribute_exporter_ui.raise_()
        return

    # Chiudi la finestra se è già presente
    if cmds.window("AttributeExporterUI", exists=True):
        cmds.deleteUI("AttributeExporterUI", window=True)

    attribute_exporter_ui = AttributeExporterUI()
    attribute_exporter_ui.show()


attribute_exporter_ui = None

AB_poseReaderQuadBiped()
