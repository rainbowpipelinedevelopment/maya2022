# -*- coding: utf-8 -*-


import os
from functools import partial

try:
    from PySide2.QtGui import QIcon
    from PySide2.QtWidgets import QWidget, QVBoxLayout
except ImportError:
    from PySide.QtGui import QIcon, QWidget

import maya.cmds as cmds
import maya.utils
from maya import OpenMayaUI as omui
import re
localpipe = os.getenv("LOCALPY")
myPath = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "CorrectiveJoints")
image_path = os.path.join(myPath, "icons")
try:
    from shiboken2 import wrapInstance
except ImportError:
    from shiboken import wrapInstance


def createCorrectiveJoints(*args):
    import maya.cmds as cmds
    cmds.undoInfo(openChunk=True)

    def remove_suffix(name, *args):
        return ''.join([i for i in name if not i.isdigit()])

    def create_offset_group_for_locator(locator, *args):
        locator_name = locator.replace('LOC_', '')
        offset_grp_name = "GRP_{}_offset".format(locator_name)
        offset_grp = cmds.group(empty=True, name=offset_grp_name)
        locator_pos = cmds.xform(locator, query=True, translation=True, worldSpace=True)
        cmds.xform(offset_grp, translation=locator_pos)
        cmds.parent(locator, offset_grp)

        return offset_grp

    def create_vector_product(input1, operation, input2, result_name, *args):
        vector_product_node = cmds.createNode('vectorProduct', name=result_name)
        cmds.setAttr(vector_product_node + '.operation', operation)
        cmds.connectAttr(input1, vector_product_node + '.input1')
        cmds.connectAttr(input2, vector_product_node + '.input2')
        return vector_product_node

    def create_multiply_divide(node_name, operation, *args):
        multiply_divide_node = cmds.createNode('multiplyDivide', name=node_name)
        cmds.setAttr(multiply_divide_node + '.operation', operation)
        return multiply_divide_node

    def create_condition(node_name, operation, *args):
        condition_node = cmds.createNode('condition', name=node_name)
        cmds.setAttr(condition_node + '.operation', operation)
        return condition_node

    def create_joint_chain_from_locators(selected_locators, *args):
        if len(selected_locators) != 3:
            print("Seleziona esattamente 3 locator.")
            return None

        # Creazione della catena di joint
        joint_chain = []

        # Creazione dei joint
        for i, locator in enumerate(selected_locators):
            joint_name = get_unique_joint_name(locator, i + 1)
            joint = cmds.createNode('joint', name=joint_name)
            joint_chain.append(joint)

        # Match delle trasformate con il primo locator
        locator_pos = cmds.xform(selected_locators[0], query=True, translation=True, worldSpace=True)
        cmds.xform(joint_chain[0], translation=locator_pos, worldSpace=True)

        # Salviamo il nome del primo locator prima di cancellarlo
        first_locator_name = selected_locators[0]

        # Cancella solo il primo locator
        cmds.delete(first_locator_name)

        # Match delle trasformate con gli altri locator
        for i in range(1, len(selected_locators)):
            locator_pos = cmds.xform(selected_locators[i], query=True, translation=True, worldSpace=True)
            cmds.xform(joint_chain[i], translation=locator_pos, worldSpace=True)

        # Metti i joint in catena
        for i in range(len(joint_chain) - 1):
            cmds.parent(joint_chain[i + 1], joint_chain[i])

        # Orienta i joint
        for i in range(len(joint_chain)):
            cmds.joint(joint_chain[i], edit=True, orientJoint='xyz', secondaryAxisOrient='yup', zeroScaleOrient=True)

        return joint_chain

    def create_offset_group(joint, *args):
        offset_grp_name = "{}_offset".format(joint)
        offset_grp = cmds.group(empty=True, name=offset_grp_name, )
        joint_pos = cmds.xform(joint, query=True, translation=True, worldSpace=True)
        cmds.xform(offset_grp, translation=joint_pos)
        cmds.parent(joint, offset_grp)

        return offset_grp

    def create_corrective_name_joints_grp(locator_grp, joint_grp, *args):
        corrective_name_joints_grp = cmds.group(locator_grp, joint_grp, name="Corrective_Name_Joints_Grp")
        return corrective_name_joints_grp

    def get_unique_joint_name(locator_name, index, *args):
        base_name = locator_name.replace('LOC_', '') if locator_name.startswith('LOC_') else locator_name
        base_name = base_name.split("|")[-1]
        base_name = base_name.replace(' ', '_')
        return '{}_jnt{}'.format(base_name, index)

    # Seleziona i locator
    selected_locators = cmds.ls(selection=True)

    # Crea la catena di joint
    joint_chain = create_joint_chain_from_locators(selected_locators)

    if joint_chain:
        print("Joint chain created:", joint_chain)

        # Creazione del gruppo offset solo per il primo joint
        offset_group = create_offset_group(joint_chain[0])
        print("Offset group for first joint created:", offset_group)

        # Creazione del gruppo offset per il secondo locator
        offset_group_locator2 = create_offset_group_for_locator(selected_locators[1])
        print("Offset group for locator 2 created:", offset_group_locator2)

        # Togli i suffissi numerici dal gruppo offset
        new_offset_group_name = remove_suffix(offset_group)
        cmds.rename(offset_group, new_offset_group_name)

        # Seleziona il gruppo padre del secondo locator
        locator_parent_group = cmds.listRelatives(selected_locators[1], parent=True, fullPath=True)

        # Crea un gruppo per il joint e il locator
        if locator_parent_group:
            corrective_name_joints_grp = create_corrective_name_joints_grp(locator_parent_group, new_offset_group_name)
            print("Corrective_Name_Joints_Grp created:", corrective_name_joints_grp)

            # Crea i nodi Vector Product
            position_vp = create_vector_product(selected_locators[1] + '.translate', 1,
                                                selected_locators[2] + '.translate',
                                                'VP_{}_PositionRotation'.format(selected_locators[1]))
            rotation_vp = create_vector_product(selected_locators[2] + '.translate', 1,
                                                selected_locators[2] + '.translate',
                                                'VP_{}_Rotation'.format(selected_locators[2]))

            # Collega i nodi Vector Product ai locator
            cmds.connectAttr(selected_locators[1] + '.translate', position_vp + '.input1', force=True)
            cmds.connectAttr(selected_locators[2] + '.translate', position_vp + '.input2', force=True)

            cmds.connectAttr(selected_locators[2] + '.translate', rotation_vp + '.input1', force=True)
            cmds.connectAttr(selected_locators[2] + '.translate', rotation_vp + '.input2', force=True)

            # Disconnetti la translate del locator (2) dall'input 1 del rotation_vp
            cmds.disconnectAttr(selected_locators[2] + '.translate', rotation_vp + '.input1')

            # Copia i valori numerici dall'input 2 all'input 1 del rotation_Vp
            translation_values = cmds.getAttr(selected_locators[2] + '.translate')[0]
            cmds.setAttr(rotation_vp + '.input1', translation_values[0], translation_values[1], translation_values[2])

            # Crea i nodi MultiplyDivide
            rotation_divide_node = create_multiply_divide('MLD_{}_Rotation_Divide'.format(selected_locators[1]),
                                                          2)  # 2 corrisponde a divide
            rotation_multiply_node = create_multiply_divide('MLD_{}_Rotation_Multiply'.format(selected_locators[1]),
                                                            1)  # 1 corrisponde a multiply

            # Collega i nodi MultiplyDivide
            cmds.connectAttr(position_vp + '.output', rotation_divide_node + '.input1', force=True)
            cmds.connectAttr(rotation_vp + '.output', rotation_divide_node + '.input2', force=True)

            # Rinomina il primo MultiplyDivide
            cmds.rename(rotation_divide_node, 'MLD_{}_Rotation_Divide'.format(selected_locators[1]))

            # Collega il secondo MultiplyDivide
            cmds.setAttr(rotation_multiply_node + '.input1X', 4)
            cmds.setAttr(rotation_multiply_node + '.input1Y', 4)
            cmds.setAttr(rotation_multiply_node + '.input1Z', -4)

            # Imposta il secondo MultiplyDivide su multiply
            cmds.setAttr(rotation_multiply_node + '.operation', 1)  # 1 corrisponde a multiply

            # Collega l'output del primo MultiplyDivide al secondo
            cmds.connectAttr(rotation_divide_node + '.output', rotation_multiply_node + '.input2', force=True)

            # Crea i nodi Condition
            condition_x = create_condition('CND_{}_Translate_X'.format(selected_locators[1]),
                                           2)  # 2 corrisponde a less than
            condition_y = create_condition('CND_{}_Translate_Y'.format(selected_locators[1]),
                                           2)  # 2 corrisponde a less than
            condition_z = create_condition('CND_{}_Translate_Z'.format(selected_locators[1]),
                                           2)  # 2 corrisponde a less than

            # Collegamento dei nodi MultiplyDivide ai nodi Condition
            cmds.connectAttr(rotation_multiply_node + '.outputX', condition_x + '.firstTerm', force=True)
            cmds.connectAttr(rotation_multiply_node + '.outputY', condition_y + '.firstTerm', force=True)
            cmds.connectAttr(rotation_multiply_node + '.outputZ', condition_z + '.firstTerm', force=True)

            # Collegamento degli output X, Y, Z ai colorIfFalseR dei nodi Condition
            cmds.connectAttr(rotation_multiply_node + '.outputX', condition_x + '.colorIfFalseR', force=True)
            cmds.connectAttr(rotation_multiply_node + '.outputY', condition_y + '.colorIfFalseR', force=True)
            cmds.connectAttr(rotation_multiply_node + '.outputZ', condition_z + '.colorIfFalseR', force=True)

            # Crea i nodi Condition aggiuntivi con suffisso _Zero
            condition_x_zero = create_condition('CND_{}_Translate_X_Zero'.format(selected_locators[1]),
                                                2)  # 2 corrisponde a less than
            condition_y_zero = create_condition('CND_{}_Translate_Y_Zero'.format(selected_locators[1]),
                                                2)  # 2 corrisponde a less than
            condition_z_zero = create_condition('CND_{}_Translate_Z_Zero'.format(selected_locators[1]),
                                                2)  # 2 corrisponde a less than

            # Collegamento degli output X, Y, Z ai firstTerm dei nuovi Condition
            cmds.connectAttr(condition_x + '.outColorR', condition_x_zero + '.firstTerm', force=True)
            cmds.connectAttr(condition_y + '.outColorR', condition_y_zero + '.firstTerm', force=True)
            cmds.connectAttr(condition_z + '.outColorR', condition_z_zero + '.firstTerm', force=True)

            # Collegamento degli output X, Y, Z ai colorIfFalseR dei nuovi Condition
            cmds.connectAttr(condition_x + '.outColorR', condition_x_zero + '.colorIfFalseR', force=True)
            cmds.connectAttr(condition_y + '.outColorR', condition_y_zero + '.colorIfFalseR', force=True)
            cmds.connectAttr(condition_z + '.outColorR', condition_z_zero + '.colorIfFalseR', force=True)

            node_set = cmds.sets(condition_x, condition_y, condition_z, condition_x_zero, condition_y_zero,
                                 condition_z_zero, rotation_multiply_node, n="Node_slider_SET")


        else:
            print("Il locator selezionato non ha un gruppo padre.")
    else:
        print("Joint chain creation failed.")

    cmds.select(joint_chain)

    import maya.cmds as cmds
    import re

    def remove_suffix(name, *args):
        return re.sub(r'_jnt\d*$', '_Ctrl', name)

    def create_controls_for_joints(joint_chain, *args):

        controls = []
        print (joint_chain)
        # Assume che joint_chain sia la lista dei tuoi joint
        for i, joint in enumerate(joint_chain):

            ctrl_name = remove_suffix(joint)
            arrow_curve = cmds.curve(d=1, p=[(0, 0, 0), (0, 0, 1), (0, 0.2, 0.8), (0, 0, 1.5), (0, 0, 2), (0, 0, 1.5),
                                             (0, 0, 1), (0, 0, 0)], name="{}_Arrow_Curve".format(ctrl_name))

            # Crea un set (se non esiste già) e aggiungi la curva al set
            set_name = "Arrow_Curves_Set"
            if i == 0:
                # Se è il primo joint, crea il set
                cmds.sets(name=set_name, empty=True)

            # Aggiungi la curva al set
            cmds.sets(arrow_curve, add=set_name)

            # Crea i gruppi offset
            anchor_grp = cmds.group(empty=True, name="{}_Anchor".format(ctrl_name))
            offset_grp = cmds.group(empty=True, name="{}_Offset".format(ctrl_name))

            # Posiziona i gruppi offset
            joint_pos = cmds.xform(joint, query=True, translation=True, worldSpace=True)
            cmds.xform(anchor_grp, translation=joint_pos)
            cmds.xform(offset_grp, translation=joint_pos)

            # Parenta il gruppo offset sotto il gruppo anchor
            cmds.parent(offset_grp, anchor_grp)

            # Parenta la NURBS curve al gruppo offset
            cmds.parent(arrow_curve, offset_grp)

            # Rinomina la NURBS curve
            cmds.rename(arrow_curve, "{}_Arrow_Curve".format(ctrl_name))

            # Aggiungi il controllo alla lista
            controls.append(ctrl_name)

            # Se non è l'ultimo, parenta l'anchor successivo sotto la NURBS curve corrente
            if i < len(joint_chain) - 1:
                next_ctrl_name = remove_suffix(joint_chain[i + 1])
                next_anchor_grp = "{}_Anchor".format(next_ctrl_name)

                # Controlla se l'anchor del joint successivo esiste prima di parentarlo
                if cmds.objExists(next_anchor_grp):
                    cmds.parent(next_anchor_grp, "{}_Arrow_Curve".format(ctrl_name))

        return controls

    # Chiamare la funzione per creare i controlli di animazione
    joint_controls = create_controls_for_joints(joint_chain)
    print("Joint controls created:", joint_controls)

    # Seleziona e parenta gli oggetti in base all'ordine di selezione (all'indietro)
    if len(joint_controls) >= 2:
        # Se ci sono almeno due controlli di animazione
        for i in range(len(joint_controls) - 2, -1, -1):
            cmds.select(joint_controls[i + 1] + "_Anchor")
            cmds.select(joint_controls[i] + "_Arrow_Curve", add=True)
            cmds.parent()

    # Effettua il match delle trasformate tra Anchor e corrispondenti Joint
    for ctrl, joint in zip(joint_controls, joint_chain):
        anchor_group = "{}_Anchor".format(ctrl)

        # Matcha la posizione e l'orientamento dell'Anchor con il joint corrispondente
        joint_pos = cmds.xform(joint, query=True, translation=True, worldSpace=True)
        joint_orient = cmds.xform(joint, query=True, rotation=True, worldSpace=True)
        cmds.xform(anchor_group, translation=joint_pos, worldSpace=True)
        cmds.xform(anchor_group, rotation=joint_orient, worldSpace=True)

        # Ora aggiungiamo il match delle trasformate per gli altri Anchor
        for i in range(1, len(joint_chain)):
            other_joint = joint_chain[i]
            other_anchor = "{}_Anchor".format(joint_controls[i])

            other_joint_pos = cmds.xform(other_joint, query=True, translation=True, worldSpace=True)
            other_joint_orient = cmds.xform(other_joint, query=True, rotation=True, worldSpace=True)

            cmds.xform(other_anchor, translation=other_joint_pos, worldSpace=True)
            cmds.xform(other_anchor, rotation=other_joint_orient, worldSpace=True)

    # Matcha l'orientamento degli Anchor con i corrispondenti Joint dopo la catena
    for ctrl, joint in zip(joint_controls, joint_chain):
        anchor_group = "{}_Anchor".format(ctrl)

        # Matcha solo l'orientamento dell'Anchor con il joint corrispondente
        joint_orient = cmds.xform(joint, query=True, rotation=True, worldSpace=True)
        cmds.xform(anchor_group, rotation=joint_orient, worldSpace=True)

    # Azzerare le coordinate e impostare l'orientamento e l'override del colore
    for ctrl in joint_controls:
        arrow_curve = ctrl + "_Arrow_Curve"
        anchor_group = ctrl + "_Anchor"
        cmds.setAttr(arrow_curve + ".translate", 0, 0, 0)
        cmds.setAttr(arrow_curve + ".overrideColor", 17)  # 17 corrisponde al giallo in Maya

        # Match transform dell'Anchor con il corrispondente Joint (posizione e orientamento)
        ctrl_tokens = ctrl.split("_")
        if len(ctrl_tokens) >= 2 and ctrl_tokens[-1].isdigit():
            joint_index = int(ctrl_tokens[-1])
            joint_name = "joint{}".format(joint_index)
            joint_pos = cmds.xform(joint_name, query=True, translation=True, worldSpace=True)
            joint_orient = cmds.xform(joint_name, query=True, rotation=True, worldSpace=True)
            cmds.xform(anchor_group, translation=joint_pos, worldSpace=True)
            cmds.xform(anchor_group, rotation=joint_orient, worldSpace=True)

    # Aggiungi il match delle trasformate per gli altri Anchor
    for i in range(1, len(joint_controls)):
        ctrl = joint_controls[i]
        arrow_curve_ctrl = "{}_Arrow_Curve".format(ctrl)
        joint = joint_chain[i]

        # Matcha la posizione e l'orientamento dell'Anchor con il joint corrispondente
        joint_pos = cmds.xform(joint, query=True, translation=True, worldSpace=True)
        joint_orient = cmds.xform(joint, query=True, rotation=True, worldSpace=True)
        cmds.xform(arrow_curve_ctrl, translation=joint_pos, worldSpace=True)
        cmds.xform(arrow_curve_ctrl, rotation=joint_orient, worldSpace=True)

        # Vincoli Parent e Scale tra la curva NURBS e il joint
        if cmds.objExists(arrow_curve_ctrl) and cmds.objExists(joint):
            print("ParentConstraint e ScaleConstraint tra:", arrow_curve_ctrl, joint)
            cmds.parentConstraint(arrow_curve_ctrl, joint, maintainOffset=True)
            cmds.scaleConstraint(arrow_curve_ctrl, joint, maintainOffset=True)
        else:
            print("Attenzione: uno o entrambi gli oggetti non esistono nella scena.")

    # Vincoli anche per il primo joint
    first_ctrl = joint_controls[0]
    first_arrow_curve_ctrl = "{}_Arrow_Curve".format(first_ctrl)
    first_joint = joint_chain[0]

    # Matcha la posizione e l'orientamento dell'Anchor con il joint corrispondente
    first_joint_pos = cmds.xform(first_joint, query=True, translation=True, worldSpace=True)
    first_joint_orient = cmds.xform(first_joint, query=True, rotation=True, worldSpace=True)
    cmds.xform(first_arrow_curve_ctrl, translation=first_joint_pos, worldSpace=True)
    cmds.xform(first_arrow_curve_ctrl, rotation=first_joint_orient, worldSpace=True)

    # Vincoli Parent e Scale tra la curva NURBS e il primo joint
    if cmds.objExists(first_arrow_curve_ctrl) and cmds.objExists(first_joint):
        print("ParentConstraint e ScaleConstraint tra:", first_arrow_curve_ctrl, first_joint)
        cmds.parentConstraint(first_arrow_curve_ctrl, first_joint, maintainOffset=True)
        cmds.scaleConstraint(first_arrow_curve_ctrl, first_joint, maintainOffset=True)
    else:
        print("Attenzione: uno o entrambi gli oggetti non esistono nella scena.")

    import maya.cmds as cmds

    first_nurbs_arrow_offset = "{}_Offset".format(first_ctrl)

    cmds.connectAttr('{}.outColorR'.format(condition_x_zero), '{}.translateX'.format(first_nurbs_arrow_offset),
                     force=True)
    cmds.connectAttr('{}.outColorR'.format(condition_y_zero), '{}.translateY'.format(first_nurbs_arrow_offset),
                     force=True)
    cmds.connectAttr('{}.outColorR'.format(condition_z_zero), '{}.translateZ'.format(first_nurbs_arrow_offset),
                     force=True)

    parent_object = cmds.listRelatives(first_nurbs_arrow_offset, parent=True, fullPath=True)
    cmds.parent(parent_object, "Corrective_Name_Joints_Grp")

    object_name = first_nurbs_arrow_offset

    if cmds.objExists("Corrective_Name_Joints_Grp"):
        # Estrai le prime due parole dall'oggetto in first_nurbs_arrow_offset
        name_tokens = first_nurbs_arrow_offset.split('_')
        new_name = '_'.join(name_tokens[:3])

        # Rinomina l'oggetto "Corrective_Name_Joints_Grp"
        cmds.rename("Corrective_Name_Joints_Grp", new_name + "_Corrective_Joints_Grp")
        print("Il gruppo è stato rinominato con successo.")
    else:
        print("L'oggetto 'Corrective_Name_Joints_Grp' non esiste.")


def createTransformOffsetGroup(*args):
    controls = cmds.ls(sl=True);
    for control in controls:
        parent_group = cmds.listRelatives(control, p=True);
        cmds.select(cl=True);
        offset_group = cmds.group(em=True, n="GRP_" + control + "_transform_offset")
        cmds.parent(offset_group, control)
        cmds.setAttr(offset_group + ".tx", 0)
        cmds.setAttr(offset_group + ".ty", 0)
        cmds.setAttr(offset_group + ".tz", 0)
        cmds.setAttr(offset_group + ".rx", 0)
        cmds.setAttr(offset_group + ".ry", 0)
        cmds.setAttr(offset_group + ".rz", 0)
        cmds.parent(offset_group, parent_group)
        cmds.parent(control, offset_group)


def DoubleParentMatrix(*args):
    def create_offset_matrix_attributes(obj, target_name, suffix):
        attr1_name = "{}_{}_OffsetMatrix_01".format(target_name, suffix)
        attr2_name = "{}_{}_OffsetMatrix_02".format(target_name, suffix)

        if cmds.attributeQuery(attr1_name, node=obj, exists=True) or cmds.attributeQuery(attr2_name, node=obj,
                                                                                         exists=True):
            cmds.warning("Gli attributi {} e/o {} esistono già.".format(attr1_name, attr2_name))
            return attr1_name, attr2_name

        cmds.addAttr(obj, longName=attr1_name, attributeType='matrix', usedAsColor=False, keyable=False)
        cmds.addAttr(obj, longName=attr2_name, attributeType='matrix', usedAsColor=False, keyable=False)

        return attr1_name, attr2_name

    def calculate_offset_matrix(source_obj, target_obj, result_obj, attr1_name, attr2_name, suffix):
        mult_matrix_node_1 = cmds.createNode('multMatrix', name="{}_{}_Delete1".format(source_obj, suffix))
        mult_matrix_node_2 = cmds.createNode('multMatrix', name="{}_{}_Delete2".format(target_obj, suffix))

        cmds.connectAttr("{}.worldMatrix[0]".format(target_obj), "{}.matrixIn[0]".format(mult_matrix_node_1))
        cmds.connectAttr("{}.worldInverseMatrix[0]".format(source_obj), "{}.matrixIn[1]".format(mult_matrix_node_1))
        cmds.connectAttr("{}.matrixSum".format(mult_matrix_node_1), "{}.{}".format(result_obj, attr1_name))

        cmds.connectAttr("{}.worldMatrix[0]".format(target_obj), "{}.matrixIn[0]".format(mult_matrix_node_2))
        cmds.connectAttr("{}.worldInverseMatrix[0]".format(selected_objects[1]),
                         "{}.matrixIn[1]".format(mult_matrix_node_2))
        cmds.connectAttr("{}.matrixSum".format(mult_matrix_node_2), "{}.{}".format(result_obj, attr2_name))

        cmds.disconnectAttr("{}.matrixSum".format(mult_matrix_node_1), "{}.{}".format(result_object, attr1_name))
        cmds.disconnectAttr("{}.matrixSum".format(mult_matrix_node_2), "{}.{}".format(result_object, attr2_name))

        cmds.delete(mult_matrix_node_1)
        cmds.delete(mult_matrix_node_2)

        mult_matrix_node_3 = cmds.createNode('multMatrix', name="MM_{}_OffsetMatrix".format(selected_objects[0]))
        mult_matrix_node_4 = cmds.createNode('multMatrix', name="MM_{}_OffsetMatrix".format(selected_objects[1]))

        cmds.connectAttr("{}.{}".format(result_object, attr1_name), "{}.matrixIn[0]".format(mult_matrix_node_3))
        cmds.connectAttr("{}.worldMatrix[0]".format(selected_objects[0]), "{}.matrixIn[1]".format(mult_matrix_node_3))
        cmds.connectAttr("{}.parentInverseMatrix[0]".format(selected_objects[2]),
                         "{}.matrixIn[2]".format(mult_matrix_node_3))

        cmds.connectAttr("{}.{}".format(result_object, attr2_name), "{}.matrixIn[0]".format(mult_matrix_node_4))
        cmds.connectAttr("{}.worldMatrix[0]".format(selected_objects[1]), "{}.matrixIn[1]".format(mult_matrix_node_4))
        cmds.connectAttr("{}.parentInverseMatrix[0]".format(selected_objects[2]),
                         "{}.matrixIn[2]".format(mult_matrix_node_4))

        decompose_matrix_node_1 = cmds.createNode('decomposeMatrix',
                                                  name="DM_{}_OffsetMatrix".format(selected_objects[0]))
        decompose_matrix_node_2 = cmds.createNode('decomposeMatrix',
                                                  name="DM_{}_OffsetMatrix".format(selected_objects[1]))

        cmds.connectAttr("{}.matrixSum".format(mult_matrix_node_3), "{}.inputMatrix".format(decompose_matrix_node_1))

        cmds.connectAttr("{}.matrixSum".format(mult_matrix_node_4), "{}.inputMatrix".format(decompose_matrix_node_2))

        multiply_divide_node_1 = cmds.createNode('multiplyDivide', name="MLD_{}_Position".format(selected_objects[0]))
        multiply_divide_node_2 = cmds.createNode('multiplyDivide', name="MLD_{}_Position".format(selected_objects[1]))

        cmds.setAttr("{}.input2".format(multiply_divide_node_1), 0.5, 0.5, 0.5)
        cmds.setAttr("{}.input2".format(multiply_divide_node_2), 0.5, 0.5, 0.5)

        cmds.connectAttr("{}.outputTranslate".format(decompose_matrix_node_1),
                         "{}.input1".format(multiply_divide_node_1))

        cmds.connectAttr("{}.outputTranslate".format(decompose_matrix_node_2),
                         "{}.input1".format(multiply_divide_node_2))

        plus_minus_node = cmds.createNode('plusMinusAverage', name="PMA_Position")

        cmds.connectAttr("{}.output".format(multiply_divide_node_1), "{}.input3D[0]".format(plus_minus_node))
        cmds.connectAttr("{}.output".format(multiply_divide_node_2), "{}.input3D[1]".format(plus_minus_node))

        cmds.connectAttr("{}.output3D".format(plus_minus_node), "{}.translate".format(selected_objects[2]))

    selected_objects = cmds.ls(selection=True, type='transform')

    if len(selected_objects) == 4:
        source_object = selected_objects[0]
        target_object = selected_objects[2]
        result_object = selected_objects[3]
        suffix = "Offset"

        attr1_name, attr2_name = create_offset_matrix_attributes(result_object, target_object, suffix)

        calculate_offset_matrix(source_object, target_object, result_object, attr1_name, attr2_name, suffix)

        print(
            "Attributi di offset matrix creati su {} basati su {} e {}. MultMatrix creati e collegati agli attributi di offset.".format(
                result_object, source_object, target_object))
    else:
        cmds.warning("Seleziona esattamente quattro oggetti prima di eseguire lo script.")

    cmds.undoInfo(closeChunk=True)


def createTransformOffsetGroup(*args):
    controls = cmds.ls(sl=True);
    for control in controls:
        parent_group = cmds.listRelatives(control, p=True);
        cmds.select(cl=True);
        offset_group = cmds.group(em=True, n="GRP_" + control + "_transform_offset")
        cmds.parent(offset_group, control)
        cmds.setAttr(offset_group + ".tx", 0)
        cmds.setAttr(offset_group + ".ty", 0)
        cmds.setAttr(offset_group + ".tz", 0)
        cmds.setAttr(offset_group + ".rx", 0)
        cmds.setAttr(offset_group + ".ry", 0)
        cmds.setAttr(offset_group + ".rz", 0)
        cmds.parent(offset_group, parent_group)
        cmds.parent(control, offset_group)


def ParentAndScale(*args):
    sel = cmds.ls(sl=True)
    l = len(sel)
    for i in range(l - 1):
        cmds.parentConstraint(sel[0], sel[i + 1], mo=True)
        cmds.scaleConstraint(sel[0], sel[i + 1], mo=True)


def delete_multiply_nodes(*args):
    multiply_nodes = [
        'MLD_LOC_L_Shoulder_Corrective_up_Rotation', u'MLD_LOC_L_Shoulder_Corr_Front_Rotation',
        u'MLD_LOC_L_Shoulder_Corr_Front_Position', u'MLD_LOC_L_Shoulder_Corr_Low_Rotation',
        u'MLD_LOC_L_Shoulder_Corr_Low_Position', u'MLD_LOC_L_Shoulder_Corr_up_Position',
        u'MLD_LOC_L_Shoulder_Front_Corr_01', u'MLD_LOC_L_Knee_Corr_ext_Rotation', u'MLD_LOC_L_Knee_Corr_int_Rotation',
        u'MLD_LOC_L_Knee_Corr_ext_Position', u'MLD_LOC_L_BackLeg_Corr_Rotation', u'MLD_LOC_L_Knee_Corr_int_Position',
        u'MLD_LOC_L_Shoulder_Low_Corr_01', u'MLD_LOC_L_BackLeg_Corr_Position', u'MLD_LOC_L_Shoulder_Up_Corr_01',
        u'MLD_LOC_L_UpLeg_Corr_Rotation', u'MLD_LOC_L_UpLeg_Corr_Position', u'MLD_LOC_L_Elbow_Ext_Rotation',
        u'MLD_LOC_L_Elbow_Int_Rotation', u'MLD_LOC_L_Elbow_Ext_Position', u'MLD_LOC_L_Elbow_Ext_Corr_01',
        u'MLD_LOC_L_Elbow_Int_Position', u'MLD_LOC_L_Wrist_Ext_Rotation', u'MLD_LOC_L_Elbow_Int_Corr_01',
        u'MLD_LOC_L_Wrist_Ext_Position', u'MLD_LOC_L_Wrist_Ext_Corr_01', u'MLD_LOC_L_BackLeg_Corr_01',
        u'MLD_LOC_L_UpLeg_01', u'MLD_LOC_L_Knee_Corr_01', u'MLD_LOC_L_Knee_Corr_02'

    ]

    for node in multiply_nodes:
        if cmds.objExists(node):
            cmds.delete(node)
            print("Nodo MultiplyDivide eliminato:", node)
        else:
            print("Nodo MultiplyDivide non trovato:", node)



def importShape(*args):
    localpipeBackDir = os.path.dirname(localpipe)
    scenesDir = os.path.join(localpipeBackDir, 'python', 'depts', 'rigging', 'tools', 'generic', 'CorrectiveJoints', 'scenes')
    fileToImport = os.path.join(scenesDir, 'Shape.ma')
    cmds.file(fileToImport, i=True, type="mayaAscii", namespace=":")


def linkWiki(*args):
    import webbrowser
    pathToHelp = "https://sites.google.com/rbw-cgi.it/correctivejoints/home-page"
    webbrowser.open(pathToHelp)


def CorrectiveJointsSingleUI(*args):
    if cmds.window("CorrectiveJointSingleUI", q=True, ex=True):
        cmds.deleteUI("CorrectiveJointSingleUI")

    ww = cmds.window("CorrectiveJointSingleUI", t="Rainbow CGI - Corrective Joints", width=250, height=50)

    # Get a pointer and convert it to Qt Widget object
    qw = omui.MQtUtil.findWindow(ww)
    widget = wrapInstance(int(qw), QWidget)

    # Create a QIcon object
    iconpath = os.path.join(image_path, "RainbowCGI_icona.ico")

    # Assign the icon
    icon = QIcon(iconpath)
    widget.setWindowIcon(icon)

    col = cmds.columnLayout(adjustableColumn=1)

    col1 = cmds.columnLayout(adj=1, p=col, bgc=[0.2, 0.2, 0.2])
    cmds.rowLayout(numberOfColumns=2, p=col1, adj=2, cat=[(1, "left", 5), (2, "left", 0)])
    cmds.text("RBW - Corrective Joints supporting Deformation", h=20)

    cmds.frameLayout(label="Corrective Joints Creation", collapsable=1, collapse=0, p=col, bgc=[0.0, 0.5, 0.4], w=10)
    cmds.columnLayout(adj=1, bgc=[0.2, 0.2, 0.2])

    cmds.button(l="| Create Corrective Joints |", c=createCorrectiveJoints, h=30, bgc=[0.3, 0.3, 0.3], w=10)
    cmds.button(l="| Double Parent Matrix |", c=DoubleParentMatrix, h=30, bgc=[0.3, 0.3, 0.3], w=10)
    cmds.frameLayout(label="Utilities", collapsable=1, collapse=0, p=col, bgc=[0.0, 0.5, 0.6], w=10)
    cmds.columnLayout(adj=1, bgc=[0.2, 0.2, 0.2])
    cmds.button(l="| Import Shape |", c=importShape, h=30, bgc=[0.3, 0.3, 0.3], w=10)
    cmds.button(l="| Transform Offset Group |", c=createTransformOffsetGroup, h=30, bgc=[0.3, 0.3, 0.3], w=10)
    cmds.button(l="| Parent And Scale |", c=ParentAndScale, h=30, bgc=[0.3, 0.3, 0.3], w=10)
    cmds.button(l="| WIKI |", c=linkWiki, h=30, bgc=[0.2, 0.2, 0.2], w=15)

    cmds.columnLayout(adj=1, p=col, bgc=[0.2, 0.2, 0.2])
    cmds.showWindow()


CorrectiveJointsSingleUI()
