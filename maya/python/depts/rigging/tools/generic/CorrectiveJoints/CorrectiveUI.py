# -*- coding: utf-8 -*-

import maya.cmds as cmds
from maya import OpenMayaUI as omui
import os
from imp import reload

# -----------------------------------------------------------------
# Path
# -----------------------------------------------------------------
localpipe = os.getenv("LOCALPY")
myPath = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "CorrectiveJoints")
image_path = os.path.join(myPath, "icons")


# -----------------------------------------------------------------
# PySide
# -----------------------------------------------------------------
try:
    from shiboken2 import wrapInstance
except ImportError:
    from shiboken import wrapInstance

try:
    from PySide2.QtGui import QIcon
    from PySide2.QtWidgets import QWidget
except ImportError:
    from PySide.QtGui import QIcon, QWidget


# -----------------------------------------------------------------
# Funzioni lanciate dalla UI
# -----------------------------------------------------------------
def Single_corrective_botton_push(*args):
    import depts.rigging.tools.generic.CorrectiveJoints.scripts.JointCorrectiveSingle as SingleCorrective
    reload(SingleCorrective)
    SingleCorrective.CorrectiveJointsSingleUI()


def Hive_pose_Reader_button_push(*args):
    import depts.rigging.tools.generic.CorrectiveJoints.scripts.hive_poseReader as hivePoseReader
    reload(hivePoseReader)
    hivePoseReader.hive_poseReader()


def AB_pose_Reader_button_push(*args):
    import depts.rigging.tools.generic.CorrectiveJoints.scripts.ab_poseReader as Ab_poseReader
    reload(Ab_poseReader)
    Ab_poseReader.abposeReader()


def AB_pose_Reader_Quadruped_button_push(*args):
    import depts.rigging.tools.generic.CorrectiveJoints.scripts.ABCorrectiveJointsQuad as AbQuad
    reload(AbQuad)
    AbQuad.AB_poseReaderQuad()

def AB_pose_Reader_QuadBiped_button_push(*args):
    import depts.rigging.tools.generic.CorrectiveJoints.scripts.ABCorrectiveJointsQuadBiped as AbQuadBiped
    reload(AbQuadBiped)
    AbQuadBiped.AB_poseReaderQuadBiped()

# -----------------------------------------------------------------
# UI
# -----------------------------------------------------------------
def main():
    winWidth = 250
    bg_color_01 = (0.35, 0.35, 0.35)
    bg_color_02 = (0.4, 0.1, 0.4)

    main_window = "RBW_Corrective_Joints_Creator"
    main_title = "RBW Corrective Joints Creator"

    if cmds.window(main_window, exists = 1):
        cmds.deleteUI(main_window, window = 1)

    ww = cmds.window(main_window, title=main_title, mxb=False, sizeable=True, resizeToFitChildren=True, width=250, height=50)

    # Get a pointer and convert it to Qt Widget object
    qw = omui.MQtUtil.findWindow(ww)
    widget = wrapInstance(int(qw), QWidget)

    # Create a QIcon object
    iconpath = os.path.join(image_path, "RainbowCGI_icona.ico")

    # Assign the icon
    icon = QIcon(iconpath)
    widget.setWindowIcon(icon)

    # Main layout
    col = cmds.columnLayout(co=("both", 5), adjustableColumn=True, bgc=(0.225, 0.225, 0.225))
    
    # Carica l'immagine degli eyesockets
    img = os.path.join(image_path, "Corrective_Joints.png")
    cmds.text(h = 10, l = "")
    cmds.iconTextButton(style="iconOnly", image1=img, p=col, w=100)
    cmds.text(h = 10, l = "")
    cmds.setParent("..")

    # FIELDS
    cmds.frameLayout( label="Pose Reader Modules", marginHeight=6, p=col, bgc=bg_color_02)
    cmds.columnLayout(adj=True, co=("both", 6), rowSpacing=5, columnWidth=250)
    cmds.button(w=winWidth, h=30, l="| Single Corrective Joint |", c=Single_corrective_botton_push, bgc=(0.1, 0.5, 0.4), label=1,  annotation="Crea un singolo joint correttivo")
    cmds.button(w=winWidth, h=30, l="| Hive Pose Reader Biped |", c=Hive_pose_Reader_button_push, bgc=(0.1, 0.1, 0.5), label=1, annotation="Crea i joint correttivi per l'autorig di Hive")
    cmds.button(w=winWidth, h=30, l="| AB Pose Reader Biped |", c=AB_pose_Reader_button_push, bgc=(0.5, 0.1, 0.1),label=1, annotation="Crea i joint correttivi per l'autorig di AB")
    cmds.button(w=winWidth, h=30, l="| AB Pose Reader Quadruped |", c=AB_pose_Reader_Quadruped_button_push, bgc=(0.5, 0.1, 0.5),label=1, annotation="Crea i joint correttivi per l'autorig Quadrupedi di AB")
    cmds.button(w=winWidth, h=30, l="| AB Pose Reader QuadrupedBiped |", c=AB_pose_Reader_QuadBiped_button_push, bgc=(0.5, 0.4, 0),label=1, annotation="Crea i joint correttivi per l'autorig Quadrupedi di AB")

    cmds.setParent("..")
    # cmds.setParent("..")


    
    cmds.showWindow(main_window)
