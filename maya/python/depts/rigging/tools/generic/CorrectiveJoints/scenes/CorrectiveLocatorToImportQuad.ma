//Maya ASCII 2022 scene
//Name: CorrectiveLocatorToImportQuad.ma
//Last modified: Mon, Jun 24, 2024 03:33:13 PM
//Codeset: 1252
requires maya "2022";
requires "stereoCamera" "10.0";
requires "mtoa" "4.1.1";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2022";
fileInfo "version" "2022";
fileInfo "cutIdentifier" "202303271415-baa69b5798";
fileInfo "osv" "Windows 10 Pro for Workstations v2009 (Build: 19045)";
fileInfo "UUID" "2D01696A-4359-5779-3C6A-0A93BA441757";
createNode transform -s -n "persp";
	rename -uid "96984B57-44BC-4D5B-8C8B-E1A6BA1C7B91";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 276.34012298626476 277.91422270132824 214.05343128702009 ;
	setAttr ".r" -type "double3" -22.538352729593694 50.59999999999944 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "9644AE6A-470A-E8E8-2F65-B190A8462A71";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 396.31584063844798;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -6.5567084206463733e-08 100.48902018302914 2.575958490337185 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "FC073EF5-4E30-35D4-6044-03B26D96DA2B";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "324666B0-4463-AB18-D897-6F9BBD3E2D16";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "82E1ED3C-4319-01EB-A607-1DA63BAFA60C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "41D7ACA9-478B-249A-3DC3-218B615043DA";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "55BE59B4-4B1C-AE78-6EDF-269C80B7156F";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "B02755BB-478B-156C-AAC4-8281BD081D66";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "L_Side_Correctrive";
	rename -uid "42CC7F9C-4A20-BD84-5FCB-DBB9AB26808A";
createNode transform -n "LOC_L_legFront_Knee_Ext_Corr" -p "L_Side_Correctrive";
	rename -uid "609DEAA1-4316-C73A-DEBE-FB829810DC0B";
	setAttr ".t" -type "double3" 20.950576782226562 113.80858041320639 -0.82161639569577449 ;
createNode locator -n "LOC_L_legFront_Knee_Ext_Corr_jnt" -p "LOC_L_legFront_Knee_Ext_Corr";
	rename -uid "8A2F85E2-44A9-14D4-C89C-E5B66BC77837";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_L_legFront_Knee_Int_Corr" -p "L_Side_Correctrive";
	rename -uid "94330642-4784-7B63-951D-5EA62CF42801";
	setAttr ".t" -type "double3" 20.950576782226562 116.85767539087929 8.4242810155746977 ;
	setAttr ".s" -type "double3" 0.99999999999999978 1.0000000000000007 1.0000000000000002 ;
createNode locator -n "LOC_L_legFront_Knee_Int_CorrShape" -p "LOC_L_legFront_Knee_Int_Corr";
	rename -uid "7B61168D-4917-5E8C-6BD1-C19DC7BF36B7";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_L_legFront_Ankle_Ext_Corr" -p "L_Side_Correctrive";
	rename -uid "B8B76D74-4D84-C0B6-A25A-B6B8E7F7A9C5";
	setAttr ".t" -type "double3" 20.950576782226562 107.79525306594965 0.84463382740007553 ;
	setAttr ".r" -type "double3" 0 88.352067476292248 90 ;
createNode locator -n "LOC_L_legFront_Ankle_Ext_CorrShape" -p "LOC_L_legFront_Ankle_Ext_Corr";
	rename -uid "A2738BA0-4472-9D61-D4DB-35AFA2DF01D8";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_Neck_Low_Corr" -p "L_Side_Correctrive";
	rename -uid "76CA7E40-4E66-62F7-4152-549DB309F5C3";
	setAttr ".t" -type "double3" 0 136.47135453743562 6.4888680952761106 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999978 0.99999999999999933 ;
createNode locator -n "LOC_Neck_Low_CorrShape" -p "LOC_Neck_Low_Corr";
	rename -uid "B3C14A8A-4316-7A19-9C80-4CBBD0CEA32D";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_Neck_Up_Corr" -p "L_Side_Correctrive";
	rename -uid "23931809-4B2A-07E3-66EE-C0AF2833D15A";
	setAttr ".t" -type "double3" 0 145.18492126467237 -4.2167353630756308 ;
	setAttr ".r" -type "double3" -90 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999978 0.99999999999999933 ;
createNode locator -n "LOC_Neck_Up_CorrShape" -p "LOC_Neck_Up_Corr";
	rename -uid "6C1D78D2-4FFE-378E-A07F-2EABD6C79B5F";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_L_Neck_Corr" -p "L_Side_Correctrive";
	rename -uid "A786BEC6-4109-4F79-F6CF-BAB6FCF73CED";
	setAttr ".t" -type "double3" 8.956600189011148 140.18492126467237 0.78326463692436565 ;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999978 0.99999999999999933 ;
createNode locator -n "LOC_L_Neck_CorrShape" -p "LOC_L_Neck_Corr";
	rename -uid "61240C1C-40A1-7E5E-79B4-0EACD5A0684F";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_L_legFront_Up_Int_Corr" -p "L_Side_Correctrive";
	rename -uid "58C60ADD-4827-A8FA-4BBC-93A70D1463CB";
	setAttr ".t" -type "double3" 20.950575865385865 125.20969170809298 8.1577762252878685 ;
	setAttr ".s" -type "double3" 0.99999999999999956 1 1 ;
createNode locator -n "LOC_L_legFront_Up_Int_CorrShape" -p "LOC_L_legFront_Up_Int_Corr";
	rename -uid "815D3FFD-45AE-21E1-276F-9B947AA66BA6";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_L_legFront_Up_Ext_Corr" -p "L_Side_Correctrive";
	rename -uid "098D6B18-403C-4E80-12B7-8FA2F12837D9";
	setAttr ".t" -type "double3" 20.950576782226562 118.95502357286779 -2.5235951749659984 ;
	setAttr ".s" -type "double3" 0.99999999999999956 1 1 ;
createNode locator -n "LOC_L_legFront_Up_Ext_CorrShape" -p "LOC_L_legFront_Up_Ext_Corr";
	rename -uid "87117996-47A5-9A47-9BEA-988CC31BB0A5";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_L_legFront_Ankle_Int_Corr" -p "L_Side_Correctrive";
	rename -uid "6D3E0DCC-4A4C-1671-F5DD-DBB0FA0EC260";
	setAttr ".t" -type "double3" 20.950576782226562 108.79290614857987 9.3686521080068932 ;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999978 1.0000000000000004 ;
createNode locator -n "LOC_L_legFront_Ankle_Int_CorrShape" -p "LOC_L_legFront_Ankle_Int_Corr";
	rename -uid "3D74B21C-48D7-F9F0-BC68-3F87063C96E2";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_L_legRear_Knee_Ext_Corr" -p "L_Side_Correctrive";
	rename -uid "B81E726E-4C0E-9316-5221-409137486ED7";
	setAttr ".t" -type "double3" 20.950576782226562 113.80858041320639 -39.850184359959783 ;
createNode locator -n "LOC_L_legRear_Knee_Ext_CorrShape" -p "LOC_L_legRear_Knee_Ext_Corr";
	rename -uid "09DF9AA9-45A2-869D-B3EF-1AA03877C88E";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_L_legRear_Ankle_Int_Corr" -p "L_Side_Correctrive";
	rename -uid "9A936F21-4DDB-C865-F914-CE8E3CE7CD4A";
	setAttr ".t" -type "double3" 20.950576782226562 108.79290614857987 -29.659915856257115 ;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999978 1.0000000000000004 ;
createNode locator -n "LOC_L_legRear_Ankle_Int_CorrShape" -p "LOC_L_legRear_Ankle_Int_Corr";
	rename -uid "6AF7F8AE-4BB2-D0F9-5BCF-F0AD7CACF298";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_L_legRear_Up_Ext_Corr" -p "L_Side_Correctrive";
	rename -uid "5D968C40-4EE0-C707-7966-89ABE7DA988B";
	setAttr ".t" -type "double3" 20.950576782226562 118.95502357286779 -41.552163139230011 ;
	setAttr ".s" -type "double3" 0.99999999999999956 1 1 ;
createNode locator -n "LOC_L_legRear_Up_Ext_CorrShape" -p "LOC_L_legRear_Up_Ext_Corr";
	rename -uid "EEE700D0-4623-027B-1E8F-F786EC330255";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_L_legRear_Knee_Int_Corr" -p "L_Side_Correctrive";
	rename -uid "29E9E667-47BB-F70C-F6C3-7B9910ABBC21";
	setAttr ".t" -type "double3" 20.950576782226562 116.85767539087929 -30.604286948689314 ;
	setAttr ".s" -type "double3" 0.99999999999999978 1.0000000000000007 1.0000000000000002 ;
createNode locator -n "LOC_L_legRear_Knee_Int_CorrShape" -p "LOC_L_legRear_Knee_Int_Corr";
	rename -uid "9F174145-41E7-4848-50CB-C18988C7302F";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_L_legRear_Ankle_Ext_Corr" -p "L_Side_Correctrive";
	rename -uid "AD0DD7B9-42F1-35DC-D43B-B68C670C0ED6";
	setAttr ".t" -type "double3" 20.950576782226562 107.79525306594965 -38.18393413686394 ;
	setAttr ".r" -type "double3" 0 88.352067476292248 90 ;
createNode locator -n "LOC_L_legRear_Ankle_Ext_CorrShape" -p "LOC_L_legRear_Ankle_Ext_Corr";
	rename -uid "7D694145-4BE8-3AAC-6119-6B8CCB2DCFA8";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_L_legRear_Up_Int_Corr" -p "L_Side_Correctrive";
	rename -uid "78623687-4623-26BC-348A-19AECDE10699";
	setAttr ".t" -type "double3" 20.950575865385865 125.20969170809298 -30.870791738976145 ;
	setAttr ".s" -type "double3" 0.99999999999999956 1 1 ;
createNode locator -n "LOC_L_legRear_Up_Int_CorrShape" -p "LOC_L_legRear_Up_Int_Corr";
	rename -uid "0E80C12F-4BF0-FACA-528E-A9A36A4667F5";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "R_Side_Correctrive";
	rename -uid "FCDFB1A2-4759-2209-F8BE-C08161338336";
createNode transform -n "LOC_R_legFront_Knee_Ext_Corr" -p "R_Side_Correctrive";
	rename -uid "1AA661AF-4459-BCBD-BD7F-F5B7C0693509";
createNode locator -n "LOC_R_legFront_Knee_Ext_Corr_jnt" -p "LOC_R_legFront_Knee_Ext_Corr";
	rename -uid "C09232AD-45DF-A04A-7712-48B72D902A09";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 20;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_R_legFront_Knee_Int_Corr" -p "R_Side_Correctrive";
	rename -uid "49FAC872-41CD-4062-284B-E8AEAD6C5956";
createNode locator -n "LOC_R_legFront_Knee_Int_CorrShape" -p "LOC_R_legFront_Knee_Int_Corr";
	rename -uid "3927F5CC-4AC7-D532-92E6-A7BCA30F4A31";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 20;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_R_legFront_Ankle_Ext_Corr" -p "R_Side_Correctrive";
	rename -uid "C2554F63-4558-8E3B-1AEC-B092B563DC31";
createNode locator -n "LOC_R_legFront_Ankle_Ext_CorrShape" -p "LOC_R_legFront_Ankle_Ext_Corr";
	rename -uid "BC0A68A0-4085-6386-5C52-EFBA364B09C1";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 20;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_R_Neck_Corr" -p "R_Side_Correctrive";
	rename -uid "6BEF52F5-4E21-AC92-FCA3-77BB0B11EC9C";
createNode locator -n "LOC_R_Neck_CorrShape" -p "LOC_R_Neck_Corr";
	rename -uid "BCFFFF5F-44F3-2584-8D12-DCBB770C3B2E";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 20;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_R_legFront_Up_Int_Corr" -p "R_Side_Correctrive";
	rename -uid "04AB5B8B-4F93-EE25-6D76-39987D4BAB86";
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
createNode locator -n "LOC_R_legFront_Up_Int_CorrShape" -p "LOC_R_legFront_Up_Int_Corr";
	rename -uid "CFDA5109-4CAA-DA0E-7151-15BF6759491C";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 20;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_R_legFront_Up_Ext_Corr" -p "R_Side_Correctrive";
	rename -uid "A63D7057-46CB-B690-A0F8-6E812B5C4B86";
createNode locator -n "LOC_R_legFront_Up_Ext_CorrShape" -p "LOC_R_legFront_Up_Ext_Corr";
	rename -uid "6F9957B2-4D71-5AD6-95C4-D68DC682C1C9";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 20;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_R_legFront_Ankle_Int_Corr" -p "R_Side_Correctrive";
	rename -uid "23D7A10D-4421-5367-A652-9295234D8FA2";
createNode locator -n "LOC_R_legFront_Ankle_Int_CorrShape" -p "LOC_R_legFront_Ankle_Int_Corr";
	rename -uid "18C1468F-4937-198C-2C04-55BFE10DDB07";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 20;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_R_legRear_Knee_Int_Corr" -p "R_Side_Correctrive";
	rename -uid "E148F140-46AB-075A-3E41-91B21C3BADDC";
createNode locator -n "LOC_R_legRear_Knee_Int_CorrShape" -p "LOC_R_legRear_Knee_Int_Corr";
	rename -uid "F8949C68-4FF4-82DD-0F09-EBA620BBD7E7";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 20;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_R_legRear_Knee_Ext_Corr" -p "R_Side_Correctrive";
	rename -uid "3C6FF31B-4C3B-B955-2C2D-709236406102";
createNode locator -n "LOC_R_legRear_Knee_Ext_CorrShape" -p "LOC_R_legRear_Knee_Ext_Corr";
	rename -uid "976F5F8E-4293-F9FC-773F-AABD3B7CA803";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 20;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_R_legRear_Up_Ext_Corr" -p "R_Side_Correctrive";
	rename -uid "CF42CD8A-4C4F-DA54-80F6-CE911E91AB53";
createNode locator -n "LOC_R_legRear_Up_Ext_CorrShape" -p "LOC_R_legRear_Up_Ext_Corr";
	rename -uid "9C456F13-4520-4558-B58E-9084A18D53FE";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 20;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_R_legRear_Ankle_Ext_Corr" -p "R_Side_Correctrive";
	rename -uid "2FD69AAE-47E2-F415-801D-B09B36E78F8E";
createNode locator -n "LOC_R_legRear_Ankle_Ext_CorrShape" -p "LOC_R_legRear_Ankle_Ext_Corr";
	rename -uid "905765C3-4B1E-2AB4-1C3D-40BD4BA63333";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 20;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_R_legRear_Ankle_Int_Corr" -p "R_Side_Correctrive";
	rename -uid "925B05CA-4D36-A0C4-963A-489C62D6752D";
createNode locator -n "LOC_R_legRear_Ankle_Int_CorrShape" -p "LOC_R_legRear_Ankle_Int_Corr";
	rename -uid "ABD18EA7-46CD-FE19-52D7-E7B9BA71A0DE";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 20;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode transform -n "LOC_R_legRear_Up_Int_Corr" -p "R_Side_Correctrive";
	rename -uid "ACD4FE39-421C-06C7-EB34-FB874101668A";
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
createNode locator -n "LOC_R_legRear_Up_Int_CorrShape" -p "LOC_R_legRear_Up_Int_Corr";
	rename -uid "8936424C-4015-3B2B-B0C3-05A5579DD4A7";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 20;
	setAttr ".los" -type "double3" 3 3 3 ;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "1FA38D2E-4D44-5507-072F-A3B81E855F89";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "96C27CB3-4338-6578-ECF7-04A750119076";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "0A712756-422B-741E-48CF-C29EA8669A07";
createNode displayLayerManager -n "layerManager";
	rename -uid "9D55E6B4-4356-D8A0-F88B-B29ACFA3D6F9";
createNode displayLayer -n "defaultLayer";
	rename -uid "BEF9C118-4A59-BB0D-3045-21B49B73CE27";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "3BC38978-47B5-91A2-5283-29B33F42B736";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "8CB10551-411B-68E1-5048-6DB89310686C";
	setAttr ".g" yes;
createNode multiplyDivide -n "MLD_LOC_L_legFront_Knee_Ext_Corr_01";
	rename -uid "B1A67496-472C-6358-5DA7-629DA5956133";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode unitConversion -n "unitConversion15";
	rename -uid "DA1066A7-4CDC-4B75-DBEB-17BF35E380AC";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "MLD_LOC_L_legFront_Knee_Ext_Corr_02";
	rename -uid "84BE3527-4658-8C38-1902-B7818F0CF344";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion313";
	rename -uid "EE9968E6-4A84-93C1-9B0D-33A358A106F3";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "MLD_LOC_L_Neck_Corr_01";
	rename -uid "8A425C54-4476-26C1-E535-66A500FCA8FB";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode unitConversion -n "unitConversion306";
	rename -uid "B4512845-4AF7-16A9-F8CE-DBAD5C8F5135";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "MLD_LOC_L_Neck_Corr_02";
	rename -uid "A3BD7F07-4DAF-6FB7-451D-6C96B026CF82";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion305";
	rename -uid "5176A6FE-4E03-BFAB-F330-0D9BB655E162";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "MLD_LOC_L_legFront_Knee_Int_Corr_01";
	rename -uid "B466D8BC-4AC2-39AD-9383-709370B67EE0";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode unitConversion -n "unitConversion315";
	rename -uid "79AEAAE6-44B0-0EF7-FECB-F4B93071FF77";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "MLD_LOC_L_legFront_Knee_Int_Corr_02";
	rename -uid "346A4B15-4B25-2AF2-667A-C99E63AF6A13";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion314";
	rename -uid "75316D39-4EE1-7051-F06A-9BABDCB82422";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "MLD_LOC_L_legFront_Ankle_Ext_Corr_01";
	rename -uid "121A2B56-40B7-3D21-234C-BAAC9ADB9E74";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode unitConversion -n "unitConversion21";
	rename -uid "4C5CFB5C-42E6-8990-7517-B987018522DE";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "LOC_L_legFront_Ankle_Ext_Corr_02";
	rename -uid "64FB6553-48B1-6220-1E55-8EA6B24613EE";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion317";
	rename -uid "B9E1C5FE-4007-37C3-B36E-BFB0EAC1CBCB";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "MLD_LOC_L_legFront_Up_Int_Corr_01";
	rename -uid "A7D3F32D-44C1-602E-B2EF-D597129CCBA9";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode unitConversion -n "unitConversion311";
	rename -uid "5002B195-42B4-EA5B-E74F-DE9EFBCFF227";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "MLD_LOC_L_legFront_Up_Int_Corr_02";
	rename -uid "A6729557-4F4C-3031-C53B-C7BE14A06EEC";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion9";
	rename -uid "5ED21771-454D-606B-DEBB-729FB4C22959";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "MLD_LOC_L_legFront_Up_Ext_Corr_01";
	rename -uid "944C05D2-4253-AB59-9733-26A813595A0C";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode unitConversion -n "unitConversion310";
	rename -uid "7524B455-4A85-DD81-6AB8-048E686AF463";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "MLD_LOC_L_legFront_Up_Ext_Corr_02";
	rename -uid "F9728975-4A8A-F6C8-F670-4B9F15E3C4C4";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion309";
	rename -uid "DDBD2C52-4C2F-BC2D-6F9F-DA89A5501643";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "MLD_LOC_L_legFront_Ankle_Int_Corr_01";
	rename -uid "14A536E3-407A-2870-4A5B-978F4A0A02A2";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode unitConversion -n "unitConversion12";
	rename -uid "B506C443-4DA2-01F3-ED2B-259EEBF17AD7";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "MLD_LOC_L_legFront_Ankle_Int_Corr_02";
	rename -uid "1DD73A4A-40B0-C1D4-EDAD-F687B180FBAC";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion312";
	rename -uid "15BCD433-4428-322B-30CB-D0A577B92240";
	setAttr ".cf" 57.295779513082323;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "CC971751-4CE8-E6EE-FEEF-1AA34E316841";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 0.6\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 0.6\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 0.6\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n"
		+ "            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n"
		+ "            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 0.6\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n"
		+ "            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1318\n            -height 734\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 0\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 0\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n"
		+ "            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -selectCommand \"print(\\\"\\\")\" \n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n"
		+ "            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n"
		+ "            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n"
		+ "            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n"
		+ "                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n"
		+ "                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showPlayRangeShades \"on\" \n                -lockPlayRangeShades \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n"
		+ "                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -keyMinScale 1\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -valueLinesToggle 1\n                -highlightAffectedCurves 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n"
		+ "                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n"
		+ "                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n"
		+ "            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n"
		+ "                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n"
		+ "                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n"
		+ "                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n{ string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"|:persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n"
		+ "                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 0.6\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n"
		+ "                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName; };\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 0.6\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1318\\n    -height 734\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 0.6\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1318\\n    -height 734\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "19361913-43CF-905A-9E40-DF839E477ED8";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode multiplyDivide -n "MLD_LOC_L_legRear_Knee_Ext_Corr_02";
	rename -uid "814331CB-4FCF-4D88-C5C5-188E62D9A021";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode multiplyDivide -n "MLD_LOC_L_legRear_Knee_Ext_Corr_01";
	rename -uid "B6B555D6-4849-0A5C-D86A-66826B56C520";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode unitConversion -n "unitConversion318";
	rename -uid "92BDDA74-4933-4F4D-D2BC-38B7169A4D1A";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion319";
	rename -uid "90A26E3B-4697-114D-2A0D-BCB224BEEB74";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "MLD_LOC_L_legRear_Ankle_Int_Corr_01";
	rename -uid "F1D56E33-4F74-9CFB-DBA0-F699132DFDDC";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "MLD_LOC_L_legRear_Ankle_Int_Corr_02";
	rename -uid "07CF4914-4938-94FA-543A-6BBDDB4F188C";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion320";
	rename -uid "129B8357-4DAE-7FA2-247B-C78D54C4F257";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion321";
	rename -uid "344C7098-4CAC-6271-E4C0-B0BE147D1D5A";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "MLD_LOC_L_legRear_Up_Ext_Corr_01";
	rename -uid "85FEF7D7-49CA-C7E2-3201-AE8FCBCC26DA";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "MLD_LOC_L_legRear_Up_Ext_Corr_02";
	rename -uid "B8133FD6-4132-5234-4420-EF9BA69DCEBE";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion322";
	rename -uid "A6439171-4261-756B-E8C1-538A06BBE6A6";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion323";
	rename -uid "E3D9CDFE-4FF4-D231-1431-DF9B4A2079B5";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "MLD_LOC_L_legRear_Knee_Int_Corr_01";
	rename -uid "A13F0851-4606-DAC0-4E35-B5850C3364E9";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "MLD_LOC_L_legRear_Knee_Int_Corr_02";
	rename -uid "81983516-4D2C-D08C-A843-6BB15972235B";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion324";
	rename -uid "82BB0064-4F96-443E-4A19-029248B924B7";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion325";
	rename -uid "7D335738-4A52-E3B0-9173-8181AF2F9F53";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "MLD_LOC_L_legRear_Ankle_Ext_Corr_01";
	rename -uid "193BCC09-494A-A936-5B2B-A588AD743596";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "MLD_LOC_L_legRear_Ankle_Ext_Corr_02";
	rename -uid "697DD278-469A-D824-9FBF-16B1FE9D85D1";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion326";
	rename -uid "05A4F87A-44DD-0628-366E-03A6791A88D0";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion327";
	rename -uid "62053872-488D-6940-1FF9-7291663BF9CA";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "MLD_LOC_L_legRear_Up_Int_Corr_04";
	rename -uid "002254F8-4D03-19C6-32A0-B090655566C7";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion328";
	rename -uid "761C73B1-4ECA-D2C5-15AC-44988EA2BEFD";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion329";
	rename -uid "B34F5013-49B1-19F6-A30E-91AC47F5AABE";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "MLD_LOC_L_legRear_Up_Int_Corr_01";
	rename -uid "50915EB0-4B9A-7109-4D0F-099C16D2753E";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode nodeGraphEditorInfo -n "MayaNodeEditorSavedTabsInfo";
	rename -uid "A704E6F0-4040-7EDD-933F-4CB5598CE31A";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" 750.23968737314658 -15572.6609531913 ;
	setAttr ".tgi[0].vh" -type "double2" 15129.469134260893 -8549.1927884824909 ;
	setAttr -s 42 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" 7908.5712890625;
	setAttr ".tgi[0].ni[0].y" -13601.4287109375;
	setAttr ".tgi[0].ni[0].nvs" 18306;
	setAttr ".tgi[0].ni[1].x" 7891.4287109375;
	setAttr ".tgi[0].ni[1].y" -11337.142578125;
	setAttr ".tgi[0].ni[1].nvs" 18306;
	setAttr ".tgi[0].ni[2].x" 7272.85693359375;
	setAttr ".tgi[0].ni[2].y" -11787.142578125;
	setAttr ".tgi[0].ni[2].nvs" 18306;
	setAttr ".tgi[0].ni[3].x" 8240;
	setAttr ".tgi[0].ni[3].y" -10828.5712890625;
	setAttr ".tgi[0].ni[3].nvs" 18304;
	setAttr ".tgi[0].ni[4].x" 8220;
	setAttr ".tgi[0].ni[4].y" -11685.7138671875;
	setAttr ".tgi[0].ni[4].nvs" 18306;
	setAttr ".tgi[0].ni[5].x" 7872.85693359375;
	setAttr ".tgi[0].ni[5].y" -10677.142578125;
	setAttr ".tgi[0].ni[5].nvs" 18306;
	setAttr ".tgi[0].ni[6].x" 7271.4287109375;
	setAttr ".tgi[0].ni[6].y" -13367.142578125;
	setAttr ".tgi[0].ni[6].nvs" 18306;
	setAttr ".tgi[0].ni[7].x" 7264.28564453125;
	setAttr ".tgi[0].ni[7].y" -12907.142578125;
	setAttr ".tgi[0].ni[7].nvs" 18306;
	setAttr ".tgi[0].ni[8].x" 6952.85693359375;
	setAttr ".tgi[0].ni[8].y" -12952.857421875;
	setAttr ".tgi[0].ni[8].nvs" 18304;
	setAttr ".tgi[0].ni[9].x" 7908.5712890625;
	setAttr ".tgi[0].ni[9].y" -13801.4287109375;
	setAttr ".tgi[0].ni[9].nvs" 18306;
	setAttr ".tgi[0].ni[10].x" 7900;
	setAttr ".tgi[0].ni[10].y" -11597.142578125;
	setAttr ".tgi[0].ni[10].nvs" 18306;
	setAttr ".tgi[0].ni[11].x" 7907.14306640625;
	setAttr ".tgi[0].ni[11].y" -13341.4287109375;
	setAttr ".tgi[0].ni[11].nvs" 18306;
	setAttr ".tgi[0].ni[12].x" 8235.7138671875;
	setAttr ".tgi[0].ni[12].y" -13292.857421875;
	setAttr ".tgi[0].ni[12].nvs" 18304;
	setAttr ".tgi[0].ni[13].x" 7198.5712890625;
	setAttr ".tgi[0].ni[13].y" -10902.857421875;
	setAttr ".tgi[0].ni[13].nvs" 18306;
	setAttr ".tgi[0].ni[14].x" 7580;
	setAttr ".tgi[0].ni[14].y" -11802.857421875;
	setAttr ".tgi[0].ni[14].nvs" 18306;
	setAttr ".tgi[0].ni[15].x" 6964.28564453125;
	setAttr ".tgi[0].ni[15].y" -13872.857421875;
	setAttr ".tgi[0].ni[15].nvs" 18304;
	setAttr ".tgi[0].ni[16].x" 7578.5712890625;
	setAttr ".tgi[0].ni[16].y" -13372.857421875;
	setAttr ".tgi[0].ni[16].nvs" 18306;
	setAttr ".tgi[0].ni[17].x" 6948.5712890625;
	setAttr ".tgi[0].ni[17].y" -12535.7138671875;
	setAttr ".tgi[0].ni[17].nvs" 18304;
	setAttr ".tgi[0].ni[18].x" 7900;
	setAttr ".tgi[0].ni[18].y" -12275.7138671875;
	setAttr ".tgi[0].ni[18].nvs" 18306;
	setAttr ".tgi[0].ni[19].x" 7565.71435546875;
	setAttr ".tgi[0].ni[19].y" -11368.5712890625;
	setAttr ".tgi[0].ni[19].nvs" 18306;
	setAttr ".tgi[0].ni[20].x" 6890.09375;
	setAttr ".tgi[0].ni[20].y" -10901.8447265625;
	setAttr ".tgi[0].ni[20].nvs" 18306;
	setAttr ".tgi[0].ni[21].x" 7578.5712890625;
	setAttr ".tgi[0].ni[21].y" -12312.857421875;
	setAttr ".tgi[0].ni[21].nvs" 18306;
	setAttr ".tgi[0].ni[22].x" 7601.4287109375;
	setAttr ".tgi[0].ni[22].y" -13832.857421875;
	setAttr ".tgi[0].ni[22].nvs" 18306;
	setAttr ".tgi[0].ni[23].x" 7902.85693359375;
	setAttr ".tgi[0].ni[23].y" -12681.4287109375;
	setAttr ".tgi[0].ni[23].nvs" 18306;
	setAttr ".tgi[0].ni[24].x" 7872.85693359375;
	setAttr ".tgi[0].ni[24].y" -10877.142578125;
	setAttr ".tgi[0].ni[24].nvs" 18306;
	setAttr ".tgi[0].ni[25].x" 8217.142578125;
	setAttr ".tgi[0].ni[25].y" -11288.5712890625;
	setAttr ".tgi[0].ni[25].nvs" 18304;
	setAttr ".tgi[0].ni[26].x" 6962.85693359375;
	setAttr ".tgi[0].ni[26].y" -13412.857421875;
	setAttr ".tgi[0].ni[26].nvs" 18304;
	setAttr ".tgi[0].ni[27].x" 8215.7138671875;
	setAttr ".tgi[0].ni[27].y" -13752.857421875;
	setAttr ".tgi[0].ni[27].nvs" 18304;
	setAttr ".tgi[0].ni[28].x" 7891.4287109375;
	setAttr ".tgi[0].ni[28].y" -11137.142578125;
	setAttr ".tgi[0].ni[28].nvs" 18306;
	setAttr ".tgi[0].ni[29].x" 7900;
	setAttr ".tgi[0].ni[29].y" -12452.857421875;
	setAttr ".tgi[0].ni[29].nvs" 18306;
	setAttr ".tgi[0].ni[30].x" 7294.28564453125;
	setAttr ".tgi[0].ni[30].y" -13827.142578125;
	setAttr ".tgi[0].ni[30].nvs" 18306;
	setAttr ".tgi[0].ni[31].x" 7258.5712890625;
	setAttr ".tgi[0].ni[31].y" -11362.857421875;
	setAttr ".tgi[0].ni[31].nvs" 18306;
	setAttr ".tgi[0].ni[32].x" 6790.28564453125;
	setAttr ".tgi[0].ni[32].y" -11348.154296875;
	setAttr ".tgi[0].ni[32].nvs" 18306;
	setAttr ".tgi[0].ni[33].x" 6947.14306640625;
	setAttr ".tgi[0].ni[33].y" -11754.2861328125;
	setAttr ".tgi[0].ni[33].nvs" 18306;
	setAttr ".tgi[0].ni[34].x" 7271.4287109375;
	setAttr ".tgi[0].ni[34].y" -12372.857421875;
	setAttr ".tgi[0].ni[34].nvs" 18306;
	setAttr ".tgi[0].ni[35].x" 7571.4287109375;
	setAttr ".tgi[0].ni[35].y" -12912.857421875;
	setAttr ".tgi[0].ni[35].nvs" 18306;
	setAttr ".tgi[0].ni[36].x" 7505.71435546875;
	setAttr ".tgi[0].ni[36].y" -10908.5712890625;
	setAttr ".tgi[0].ni[36].nvs" 18306;
	setAttr ".tgi[0].ni[37].x" 7902.85693359375;
	setAttr ".tgi[0].ni[37].y" -12881.4287109375;
	setAttr ".tgi[0].ni[37].nvs" 18306;
	setAttr ".tgi[0].ni[38].x" 7907.14306640625;
	setAttr ".tgi[0].ni[38].y" -13141.4287109375;
	setAttr ".tgi[0].ni[38].nvs" 18306;
	setAttr ".tgi[0].ni[39].x" 8234.2861328125;
	setAttr ".tgi[0].ni[39].y" -12832.857421875;
	setAttr ".tgi[0].ni[39].nvs" 18304;
	setAttr ".tgi[0].ni[40].x" 8221.4287109375;
	setAttr ".tgi[0].ni[40].y" -12415.7138671875;
	setAttr ".tgi[0].ni[40].nvs" 18304;
	setAttr ".tgi[0].ni[41].x" 7900;
	setAttr ".tgi[0].ni[41].y" -11797.142578125;
	setAttr ".tgi[0].ni[41].nvs" 18306;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr ".edl" no;
	setAttr ".dss" -type "string" "lambert1";
select -ne :defaultResolution;
	setAttr ".w" 1920;
	setAttr ".h" 1080;
	setAttr ".pa" 1;
select -ne :defaultColorMgtGlobals;
	setAttr ".cfe" yes;
	setAttr ".cfp" -type "string" "C:/Program Files/Autodesk/Maya2022/resources/OCIO-configs/Maya-legacy/config.ocio";
	setAttr ".vtn" -type "string" "sRGB gamma (legacy)";
	setAttr ".vn" -type "string" "sRGB gamma";
	setAttr ".dn" -type "string" "legacy";
	setAttr ".wsn" -type "string" "scene-linear Rec 709/sRGB";
	setAttr ".otn" -type "string" "sRGB gamma (legacy)";
	setAttr ".potn" -type "string" "sRGB gamma (legacy)";
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "MLD_LOC_L_legFront_Knee_Ext_Corr_01.o" "LOC_R_legFront_Knee_Ext_Corr.t"
		;
connectAttr "unitConversion15.o" "LOC_R_legFront_Knee_Ext_Corr.r";
connectAttr "MLD_LOC_L_legFront_Knee_Int_Corr_01.o" "LOC_R_legFront_Knee_Int_Corr.t"
		;
connectAttr "unitConversion315.o" "LOC_R_legFront_Knee_Int_Corr.r";
connectAttr "MLD_LOC_L_legFront_Ankle_Ext_Corr_01.o" "LOC_R_legFront_Ankle_Ext_Corr.t"
		;
connectAttr "unitConversion21.o" "LOC_R_legFront_Ankle_Ext_Corr.r";
connectAttr "MLD_LOC_L_Neck_Corr_01.o" "LOC_R_Neck_Corr.t";
connectAttr "unitConversion306.o" "LOC_R_Neck_Corr.r";
connectAttr "MLD_LOC_L_legFront_Up_Int_Corr_01.o" "LOC_R_legFront_Up_Int_Corr.t"
		;
connectAttr "unitConversion311.o" "LOC_R_legFront_Up_Int_Corr.r";
connectAttr "MLD_LOC_L_legFront_Up_Ext_Corr_01.o" "LOC_R_legFront_Up_Ext_Corr.t"
		;
connectAttr "unitConversion310.o" "LOC_R_legFront_Up_Ext_Corr.r";
connectAttr "MLD_LOC_L_legFront_Ankle_Int_Corr_01.o" "LOC_R_legFront_Ankle_Int_Corr.t"
		;
connectAttr "unitConversion12.o" "LOC_R_legFront_Ankle_Int_Corr.r";
connectAttr "MLD_LOC_L_legRear_Knee_Int_Corr_01.o" "LOC_R_legRear_Knee_Int_Corr.t"
		;
connectAttr "unitConversion325.o" "LOC_R_legRear_Knee_Int_Corr.r";
connectAttr "MLD_LOC_L_legRear_Knee_Ext_Corr_01.o" "LOC_R_legRear_Knee_Ext_Corr.t"
		;
connectAttr "unitConversion319.o" "LOC_R_legRear_Knee_Ext_Corr.r";
connectAttr "MLD_LOC_L_legRear_Up_Ext_Corr_01.o" "LOC_R_legRear_Up_Ext_Corr.t";
connectAttr "unitConversion323.o" "LOC_R_legRear_Up_Ext_Corr.r";
connectAttr "MLD_LOC_L_legRear_Ankle_Ext_Corr_01.o" "LOC_R_legRear_Ankle_Ext_Corr.t"
		;
connectAttr "unitConversion327.o" "LOC_R_legRear_Ankle_Ext_Corr.r";
connectAttr "MLD_LOC_L_legRear_Ankle_Int_Corr_01.o" "LOC_R_legRear_Ankle_Int_Corr.t"
		;
connectAttr "unitConversion321.o" "LOC_R_legRear_Ankle_Int_Corr.r";
connectAttr "MLD_LOC_L_legRear_Up_Int_Corr_01.o" "LOC_R_legRear_Up_Int_Corr.t";
connectAttr "unitConversion329.o" "LOC_R_legRear_Up_Int_Corr.r";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "LOC_L_legFront_Knee_Ext_Corr.t" "MLD_LOC_L_legFront_Knee_Ext_Corr_01.i1"
		;
connectAttr "MLD_LOC_L_legFront_Knee_Ext_Corr_02.o" "unitConversion15.i";
connectAttr "unitConversion313.o" "MLD_LOC_L_legFront_Knee_Ext_Corr_02.i1";
connectAttr "LOC_L_legFront_Knee_Ext_Corr.r" "unitConversion313.i";
connectAttr "LOC_L_Neck_Corr.t" "MLD_LOC_L_Neck_Corr_01.i1";
connectAttr "MLD_LOC_L_Neck_Corr_02.o" "unitConversion306.i";
connectAttr "unitConversion305.o" "MLD_LOC_L_Neck_Corr_02.i1";
connectAttr "LOC_L_Neck_Corr.r" "unitConversion305.i";
connectAttr "LOC_L_legFront_Knee_Int_Corr.t" "MLD_LOC_L_legFront_Knee_Int_Corr_01.i1"
		;
connectAttr "MLD_LOC_L_legFront_Knee_Int_Corr_02.o" "unitConversion315.i";
connectAttr "unitConversion314.o" "MLD_LOC_L_legFront_Knee_Int_Corr_02.i1";
connectAttr "LOC_L_legFront_Knee_Int_Corr.r" "unitConversion314.i";
connectAttr "LOC_L_legFront_Ankle_Ext_Corr.t" "MLD_LOC_L_legFront_Ankle_Ext_Corr_01.i1"
		;
connectAttr "LOC_L_legFront_Ankle_Ext_Corr_02.o" "unitConversion21.i";
connectAttr "unitConversion317.o" "LOC_L_legFront_Ankle_Ext_Corr_02.i1";
connectAttr "LOC_L_legFront_Ankle_Ext_Corr.r" "unitConversion317.i";
connectAttr "LOC_L_legFront_Up_Int_Corr.t" "MLD_LOC_L_legFront_Up_Int_Corr_01.i1"
		;
connectAttr "MLD_LOC_L_legFront_Up_Int_Corr_02.o" "unitConversion311.i";
connectAttr "unitConversion9.o" "MLD_LOC_L_legFront_Up_Int_Corr_02.i1";
connectAttr "LOC_L_legFront_Up_Int_Corr.r" "unitConversion9.i";
connectAttr "LOC_L_legFront_Up_Ext_Corr.t" "MLD_LOC_L_legFront_Up_Ext_Corr_01.i1"
		;
connectAttr "MLD_LOC_L_legFront_Up_Ext_Corr_02.o" "unitConversion310.i";
connectAttr "unitConversion309.o" "MLD_LOC_L_legFront_Up_Ext_Corr_02.i1";
connectAttr "LOC_L_legFront_Up_Ext_Corr.r" "unitConversion309.i";
connectAttr "LOC_L_legFront_Ankle_Int_Corr.t" "MLD_LOC_L_legFront_Ankle_Int_Corr_01.i1"
		;
connectAttr "MLD_LOC_L_legFront_Ankle_Int_Corr_02.o" "unitConversion12.i";
connectAttr "unitConversion312.o" "MLD_LOC_L_legFront_Ankle_Int_Corr_02.i1";
connectAttr "LOC_L_legFront_Ankle_Int_Corr.r" "unitConversion312.i";
connectAttr "unitConversion318.o" "MLD_LOC_L_legRear_Knee_Ext_Corr_02.i1";
connectAttr "LOC_L_legRear_Knee_Ext_Corr.t" "MLD_LOC_L_legRear_Knee_Ext_Corr_01.i1"
		;
connectAttr "LOC_L_legRear_Knee_Ext_Corr.r" "unitConversion318.i";
connectAttr "MLD_LOC_L_legRear_Knee_Ext_Corr_02.o" "unitConversion319.i";
connectAttr "LOC_L_legRear_Ankle_Int_Corr.t" "MLD_LOC_L_legRear_Ankle_Int_Corr_01.i1"
		;
connectAttr "unitConversion320.o" "MLD_LOC_L_legRear_Ankle_Int_Corr_02.i1";
connectAttr "LOC_L_legRear_Ankle_Int_Corr.r" "unitConversion320.i";
connectAttr "MLD_LOC_L_legRear_Ankle_Int_Corr_02.o" "unitConversion321.i";
connectAttr "LOC_L_legRear_Up_Ext_Corr.t" "MLD_LOC_L_legRear_Up_Ext_Corr_01.i1";
connectAttr "unitConversion322.o" "MLD_LOC_L_legRear_Up_Ext_Corr_02.i1";
connectAttr "LOC_L_legRear_Up_Ext_Corr.r" "unitConversion322.i";
connectAttr "MLD_LOC_L_legRear_Up_Ext_Corr_02.o" "unitConversion323.i";
connectAttr "LOC_L_legRear_Knee_Int_Corr.t" "MLD_LOC_L_legRear_Knee_Int_Corr_01.i1"
		;
connectAttr "unitConversion324.o" "MLD_LOC_L_legRear_Knee_Int_Corr_02.i1";
connectAttr "LOC_L_legRear_Knee_Int_Corr.r" "unitConversion324.i";
connectAttr "MLD_LOC_L_legRear_Knee_Int_Corr_02.o" "unitConversion325.i";
connectAttr "LOC_L_legRear_Ankle_Ext_Corr.t" "MLD_LOC_L_legRear_Ankle_Ext_Corr_01.i1"
		;
connectAttr "unitConversion326.o" "MLD_LOC_L_legRear_Ankle_Ext_Corr_02.i1";
connectAttr "LOC_L_legRear_Ankle_Ext_Corr.r" "unitConversion326.i";
connectAttr "MLD_LOC_L_legRear_Ankle_Ext_Corr_02.o" "unitConversion327.i";
connectAttr "unitConversion328.o" "MLD_LOC_L_legRear_Up_Int_Corr_04.i1";
connectAttr "LOC_L_legRear_Up_Int_Corr.r" "unitConversion328.i";
connectAttr "MLD_LOC_L_legRear_Up_Int_Corr_04.o" "unitConversion329.i";
connectAttr "LOC_L_legRear_Up_Int_Corr.t" "MLD_LOC_L_legRear_Up_Int_Corr_01.i1";
connectAttr "MLD_LOC_L_legFront_Ankle_Ext_Corr_01.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[0].dn"
		;
connectAttr "unitConversion12.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[1].dn"
		;
connectAttr "unitConversion313.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[2].dn"
		;
connectAttr "LOC_R_Neck_Corr.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[3].dn";
connectAttr "LOC_R_legFront_Knee_Ext_Corr.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[4].dn"
		;
connectAttr "MLD_LOC_L_Neck_Corr_01.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[5].dn"
		;
connectAttr "unitConversion9.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[6].dn";
connectAttr "unitConversion309.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[7].dn"
		;
connectAttr "LOC_L_legFront_Up_Ext_Corr.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[8].dn"
		;
connectAttr "unitConversion21.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[9].dn"
		;
connectAttr "MLD_LOC_L_legFront_Knee_Ext_Corr_01.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[10].dn"
		;
connectAttr "unitConversion311.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[11].dn"
		;
connectAttr "LOC_R_legFront_Up_Int_Corr.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[12].dn"
		;
connectAttr "unitConversion305.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[13].dn"
		;
connectAttr "MLD_LOC_L_legFront_Knee_Ext_Corr_02.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[14].dn"
		;
connectAttr "LOC_L_legFront_Ankle_Ext_Corr.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[15].dn"
		;
connectAttr "MLD_LOC_L_legFront_Up_Int_Corr_02.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[16].dn"
		;
connectAttr "LOC_L_legFront_Knee_Int_Corr.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[17].dn"
		;
connectAttr "unitConversion315.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[18].dn"
		;
connectAttr "MLD_LOC_L_legFront_Ankle_Int_Corr_02.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[19].dn"
		;
connectAttr "LOC_L_Neck_Corr.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[20].dn"
		;
connectAttr "MLD_LOC_L_legFront_Knee_Int_Corr_02.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[21].dn"
		;
connectAttr "LOC_L_legFront_Ankle_Ext_Corr_02.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[22].dn"
		;
connectAttr "MLD_LOC_L_legFront_Up_Ext_Corr_01.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[23].dn"
		;
connectAttr "unitConversion306.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[24].dn"
		;
connectAttr "LOC_R_legFront_Ankle_Int_Corr.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[25].dn"
		;
connectAttr "LOC_L_legFront_Up_Int_Corr.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[26].dn"
		;
connectAttr "LOC_R_legFront_Ankle_Ext_Corr.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[27].dn"
		;
connectAttr "MLD_LOC_L_legFront_Ankle_Int_Corr_01.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[28].dn"
		;
connectAttr "MLD_LOC_L_legFront_Knee_Int_Corr_01.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[29].dn"
		;
connectAttr "unitConversion317.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[30].dn"
		;
connectAttr "unitConversion312.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[31].dn"
		;
connectAttr "LOC_L_legFront_Ankle_Int_Corr.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[32].dn"
		;
connectAttr "LOC_L_legFront_Knee_Ext_Corr.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[33].dn"
		;
connectAttr "unitConversion314.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[34].dn"
		;
connectAttr "MLD_LOC_L_legFront_Up_Ext_Corr_02.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[35].dn"
		;
connectAttr "MLD_LOC_L_Neck_Corr_02.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[36].dn"
		;
connectAttr "unitConversion310.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[37].dn"
		;
connectAttr "MLD_LOC_L_legFront_Up_Int_Corr_01.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[38].dn"
		;
connectAttr "LOC_R_legFront_Up_Ext_Corr.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[39].dn"
		;
connectAttr "LOC_R_legFront_Knee_Int_Corr.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[40].dn"
		;
connectAttr "unitConversion15.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[41].dn"
		;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
dataStructure -fmt "raw" -as "name=IdStruct:int32=ID";
dataStructure -fmt "raw" -as "name=mapManager_ground:string=value";
dataStructure -fmt "raw" -as "name=notes_wildPatchC_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_grassBase:string=value";
dataStructure -fmt "raw" -as "name=notes_juneBackYard:string=value";
dataStructure -fmt "raw" -as "name=faceConnectOutputStructure:bool=faceConnectOutput:string[200]=faceConnectOutputAttributes:string[200]=faceConnectOutputGroups";
dataStructure -fmt "raw" -as "name=OffStruct:float=Offset";
dataStructure -fmt "raw" -as "name=notes_wildPatchH_parShape:string=value";
dataStructure -fmt "raw" -as "name=mapManager_base_right:string=value";
dataStructure -fmt "raw" -as "name=externalContentTablZ:string=nodZ:string=key:string=upath:uint32=upathcrc:string=rpath:string=roles";
dataStructure -fmt "raw" -as "name=notes_slopesGroundGrassC_Combined:string=value";
dataStructure -fmt "raw" -as "name=notes_snapshot_CombinedGrass:string=value";
dataStructure -fmt "raw" -as "name=notes_mountains_parShape:string=value";
dataStructure -fmt "raw" -as "name=mapManager_groundWoods_c_geo1:string=value";
dataStructure -fmt "raw" -as "name=keyValueStructure:string=value";
dataStructure -fmt "raw" -as "name=mapManager_snapshot_CombinedGrass:string=value";
dataStructure -fmt "raw" -as "name=mapManager_pPlane5:string=value";
dataStructure -fmt "raw" -as "name=notes_slopesGroundGrassA_Combined:string=value";
dataStructure -fmt "raw" -as "name=notes_left_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_pPlane1:string=value";
dataStructure -fmt "raw" -as "name=notes_polySurface56:string=value";
dataStructure -fmt "raw" -as "name=notes_groundD_parShape:string=value";
dataStructure -fmt "raw" -as "name=mapManager_pPlane4:string=value";
dataStructure -fmt "raw" -as "name=notes_grass_c_geo1:string=value";
dataStructure -fmt "raw" -as "name=notes_slopesMountainsGrass_Combined:string=value";
dataStructure -fmt "raw" -as "name=notes_slopesGroundGrassD_Combined:string=value";
dataStructure -fmt "raw" -as "name=Curvature:float=mean:float=gaussian:float=ABS:float=RMS";
dataStructure -fmt "raw" -as "name=notes_groundB_parShape:string=value";
dataStructure -fmt "raw" -as "name=faceConnectMarkerStructure:bool=faceConnectMarker:string[200]=faceConnectOutputGroups";
dataStructure -fmt "raw" -as "name=notes_pPlane4:string=value";
dataStructure -fmt "raw" -as "name=mapManager_snapshot_Combined:string=value";
dataStructure -fmt "raw" -as "name=mapManager_juneBackYard:string=value";
dataStructure -fmt "raw" -as "name=notes_base_hojas:string=value";
dataStructure -fmt "raw" -as "name=mapManager_base_hojas:string=value";
dataStructure -fmt "raw" -as "name=mapManager_pPlane2:string=value";
dataStructure -fmt "raw" -as "name=notes_baseScatt:string=value";
dataStructure -fmt "raw" -as "name=mapManager_ground_c_geo:string=value";
dataStructure -fmt "raw" -as "name=notes_pPlane3:string=value";
dataStructure -fmt "raw" -as "name=mapManager_pPlane1:string=value";
dataStructure -fmt "raw" -as "name=DiffArea:float=value";
dataStructure -fmt "raw" -as "name=notes_base_right:string=value";
dataStructure -fmt "raw" -as "name=mapManager_slopesGroundGrassD_Combined:string=value";
dataStructure -fmt "raw" -as "name=notes_decayGrassPatchC_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_groundWoods_c_geo1:string=value";
dataStructure -fmt "raw" -as "name=idStructure:int32=ID";
dataStructure -fmt "raw" -as "name=notes_snapshot_floor:string=value";
dataStructure -fmt "raw" -as "name=notes_trees_left:string=value";
dataStructure -fmt "raw" -as "name=mapManager_trees_left:string=value";
dataStructure -fmt "raw" -as "name=notes_wildPatchF_parShape:string=value";
dataStructure -fmt "raw" -as "name=mapManager_trees_left1:string=value";
dataStructure -fmt "raw" -as "name=notes_baseScatter:string=value";
dataStructure -fmt "raw" -as "name=notes_ground_c_geo:string=value";
dataStructure -fmt "raw" -as "name=notes_pPlane2:string=value";
dataStructure -fmt "raw" -as "name=DiffEdge:float=value";
dataStructure -fmt "raw" -as "name=notes_decayGrassesCenter_parShape:string=value";
dataStructure -fmt "raw" -as "name=mapManager_baseScatt:string=value";
dataStructure -fmt "raw" -as "name=notes_snapshot_Combined:string=value";
dataStructure -fmt "raw" -as "name=notes_pPlane5:string=value";
dataStructure -fmt "raw" -as "name=mapManager_snapshot_floor:string=value";
dataStructure -fmt "raw" -as "name=mapManager_polySurface56:string=value";
dataStructure -fmt "raw" -as "name=FBXFastExportSetting_FBX:string=54";
dataStructure -fmt "raw" -as "name=notes_trees_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_ferns_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_suelo:string=value";
dataStructure -fmt "raw" -as "name=mapManager_degraded:string=value";
dataStructure -fmt "raw" -as "name=notes_base_left:string=value";
dataStructure -fmt "raw" -as "name=notes_slopes_parShape:string=value";
dataStructure -fmt "raw" -as "name=mapManager_floorOrangeConcrete_c_geo:string=value";
dataStructure -fmt "raw" -as "name=notes_ground:string=value";
dataStructure -fmt "raw" -as "name=mapManager_slopesMountainsGrass_Combined:string=value";
dataStructure -fmt "raw" -as "name=notes_degraded:string=value";
dataStructure -fmt "raw" -as "name=Blur3dMetaData:string=Blur3dValue";
dataStructure -fmt "raw" -as "name=notes_pPlane6:string=value";
dataStructure -fmt "raw" -as "name=notes_right_parShape:string=value";
dataStructure -fmt "raw" -as "name=mapManager_pPlane3:string=value";
dataStructure -fmt "raw" -as "name=notes_decayGrassPatchA_parShape:string=value";
dataStructure -fmt "raw" -as "name=mapManager_suelo:string=value";
dataStructure -fmt "raw" -as "name=notes_wildPatchG_parShape:string=value";
dataStructure -fmt "raw" -as "name=mapManager_grass_c_geo:string=value";
dataStructure -fmt "raw" -as "name=notes_slopesGroundGrassB_Combined:string=value";
dataStructure -fmt "raw" -as "name=notes_groundA_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_wildPatchD_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_widlPatchB_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_grassJuneBackYard_parShape:string=value";
dataStructure -fmt "raw" -as "name=FBXFastExportSetting_MB:string=19424";
dataStructure -fmt "raw" -as "name=notes_trees_left1:string=value";
dataStructure -fmt "raw" -as "name=notes_wildPatchA_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_bushes_parShape:string=value";
dataStructure -fmt "raw" -as "name=Offset:float[3]=value";
dataStructure -fmt "raw" -as "name=notes_decayLeaves_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_baseLeaves:string=value";
dataStructure -fmt "raw" -as "name=mapManager_baseScatter:string=value";
dataStructure -fmt "raw" -as "name=notes_groundC_parShape:string=value";
dataStructure -fmt "raw" -as "name=OrgStruct:float[3]=Origin Point";
dataStructure -fmt "raw" -as "name=mapManager_slopesGroundGrassC_Combined:string=value";
dataStructure -fmt "raw" -as "name=mapManager_baseLeaves:string=value";
dataStructure -fmt "raw" -as "name=mapManager_pPlane6:string=value";
dataStructure -fmt "raw" -as "name=notes_grass_c_geo:string=value";
dataStructure -fmt "raw" -as "name=mapManager_slopesGroundGrassB_Combined:string=value";
dataStructure -fmt "raw" -as "name=notes_wildPatchDegraded_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_decayLeavesCarousel_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_floorOrangeConcrete_c_geo:string=value";
dataStructure -fmt "raw" -as "name=mapManager_grass_c_geo1:string=value";
dataStructure -fmt "raw" -as "name=notes_decayGrassPatchB_parShape:string=value";
dataStructure -fmt "raw" -as "name=mapManager_slopesGroundGrassA_Combined:string=value";
dataStructure -fmt "raw" -as "name=mapManager_grassBase:string=value";
dataStructure -fmt "raw" -as "name=notes_decayGrassPatchD_parShape:string=value";
dataStructure -fmt "raw" -as "name=mapManager_base_left:string=value";
dataStructure -fmt "raw" -as "name=notes_wildPatchE_parShape:string=value";
// End of CorrectiveLocatorToImportQuad.ma
