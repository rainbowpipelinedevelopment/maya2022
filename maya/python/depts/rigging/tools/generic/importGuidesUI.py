import importlib
import os
import functools
from PySide2 import QtWidgets, QtCore, QtGui

import maya.cmds as cmd

import api.log
import api.database
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.log)
importlib.reload(api.database)
# importlib.reload(rbwUI)


################################################################################
# @brief      This class describes an import guides ui.
#
class ImportGuidesUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(ImportGuidesUI, self).__init__()
        if cmd.window('ImportGuidesUI', exists=True):
            cmd.deleteUI('ImportGuidesUI')

        self.getAllAssets()

        self.setObjectName('ImportGuidesUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        ########################################################################
        # BUILD CENTRAL WIDGET
        #
        self.topWidget = rbwUI.RBWFrame(layout='G', radius=5)

        # asset group
        self.selectAssetGroup = rbwUI.RBWGroupBox(title='Asset:', layout='V', fontSize=12)

        self.categoryComboBox = rbwUI.RBWComboBox(text='Category', bgColor='transparent', stretch=False)
        self.categoryComboBox.activated.connect(self.fillGroupComboBox)

        self.selectAssetGroup.addWidget(self.categoryComboBox)

        self.groupComboBox = rbwUI.RBWComboBox(text='Group', bgColor='transparent', stretch=False)
        self.groupComboBox.activated.connect(self.fillNameComboBox)

        self.selectAssetGroup.addWidget(self.groupComboBox)

        self.nameComboBox = rbwUI.RBWComboBox(text='Name', bgColor='transparent', stretch=False)
        self.nameComboBox.activated.connect(self.fillVariantComboBox)

        self.selectAssetGroup.addWidget(self.nameComboBox)

        self.variantComboBox = rbwUI.RBWComboBox(text='Variant', bgColor='transparent', stretch=False)
        self.variantComboBox.activated.connect(self.fillVersions)
        self.selectAssetGroup.addWidget(self.variantComboBox)

        self.topWidget.layout.addWidget(self.selectAssetGroup, 0, 0, 1, 3)

        # version option
        self.selectDynamicGuideGroup = rbwUI.RBWGroupBox(title='Dynamic Guide', layout='V', fontSize=12)

        self.guideVersionComboBox = rbwUI.RBWComboBox(text='Version', bgColor='transparent')
        self.importGuideButton = rbwUI.RBWButton(text='Import', icon=['merge.png'], size=[80, 30])
        self.importGuideButton.clicked.connect(functools.partial(self.importGuides))

        self.selectDynamicGuideGroup.addWidget(self.guideVersionComboBox)
        self.selectDynamicGuideGroup.addWidget(self.importGuideButton, alignment=QtCore.Qt.AlignCenter)

        self.topWidget.layout.addWidget(self.selectDynamicGuideGroup, 1, 0)

        # inner guide
        self.selectInnerGuideGroup = rbwUI.RBWGroupBox(title='Inner Guide', layout='V', fontSize=12)

        self.innerGuideVersionComboBox = rbwUI.RBWComboBox(text='Version', bgColor='transparent')
        self.importInnerGuideButton = rbwUI.RBWButton(text='Import', icon=['merge.png'], size=[80, 30])
        self.importInnerGuideButton.clicked.connect(functools.partial(self.importGuides, 'innerGuides'))

        self.selectInnerGuideGroup.addWidget(self.innerGuideVersionComboBox)
        self.selectInnerGuideGroup.addWidget(self.importInnerGuideButton, alignment=QtCore.Qt.AlignCenter)

        self.topWidget.layout.addWidget(self.selectInnerGuideGroup, 1, 1)

        # cluster guide
        self.selectClusterGuideGroup = rbwUI.RBWGroupBox(title='Cluster Guide', layout='V', fontSize=12)

        self.clusterGuideVersionComboBox = rbwUI.RBWComboBox(text='Version', bgColor='transparent')
        self.importClusterGuideButton = rbwUI.RBWButton(text='Import', icon=['merge.png'], size=[80, 30])
        self.importClusterGuideButton.clicked.connect(functools.partial(self.importGuides, 'clusterGuides'))

        self.selectClusterGuideGroup.addWidget(self.clusterGuideVersionComboBox)
        self.selectClusterGuideGroup.addWidget(self.importClusterGuideButton, alignment=QtCore.Qt.AlignCenter)

        self.topWidget.layout.addWidget(self.selectClusterGuideGroup, 1, 2)

        self.mainLayout.addWidget(self.topWidget)

        self.autoFillComboBox()

        self.setMinimumSize(550, 300)
        self.setTitle('Import Guides UI')
        self.setFocus()

    ############################################################################
    # @brief      Gets all assets.
    #
    def getAllAssets(self):
        query = "SELECT ac.name, ag.name, an.name, a.variant FROM `asset` AS `a` JOIN `assetName` AS `an` ON a.nameId=an.id JOIN `assetGroup` AS `ag` ON an.groupId=ag.id JOIN `assetCategory` AS `ac` ON ag.catId=ac.id WHERE a.visible=1 AND ac.id_mv={}".format(os.getenv('ID_MV'))
        results = api.database.selectQuery(query)

        self.assets = {}

        for assetTupla in results:
            category = assetTupla[0]
            if category not in self.assets.keys():
                self.assets[category] = {}

        for assetTupla in results:
            category = assetTupla[0]
            group = assetTupla[1]
            if group not in self.assets[category].keys():
                self.assets[category][group] = {}

        for assetTupla in results:
            category = assetTupla[0]
            group = assetTupla[1]
            name = assetTupla[2]
            if name not in self.assets[category][group].keys():
                self.assets[category][group][name] = []

        for assetTupla in results:
            category = assetTupla[0]
            group = assetTupla[1]
            name = assetTupla[2]
            variant = assetTupla[3]
            if variant not in self.assets[category][group][name]:
                self.assets[category][group][name].append(variant)

    ############################################################################
    # @brief      fill category combo box.
    #
    def fillCategoryComboBox(self):
        self.categoryComboBox.clear()

        for category in sorted(self.assets.keys()):
            self.categoryComboBox.addItem(category)
        self.categoryComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

    ############################################################################
    # @brief      fill group combo box.
    #
    def fillGroupComboBox(self):
        self.groupComboBox.clear()
        self.currentCategory = self.categoryComboBox.currentText()

        for group in sorted(self.assets[self.currentCategory].keys()):
            self.groupComboBox.addItem(group)
        self.groupComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

    ############################################################################
    # @brief      fill name combo box.
    #
    def fillNameComboBox(self):
        self.nameComboBox.clear()
        self.currentGroup = self.groupComboBox.currentText()

        for name in sorted(self.assets[self.currentCategory][self.currentGroup].keys()):
            self.nameComboBox.addItem(name)
        self.nameComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

    ############################################################################
    # @brief      fill variant combo box.
    #
    def fillVariantComboBox(self):
        self.variantComboBox.clear()
        self.currentName = self.nameComboBox.currentText()

        for variant in sorted(self.assets[self.currentCategory][self.currentGroup][self.currentName]):
            self.variantComboBox.addItem(variant)
        self.variantComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

    def autoFillComboBox(self):
        riggingSaveNodes = cmd.ls('rigging_saveNode')

        if riggingSaveNodes:
            saveNode = riggingSaveNodes[0]
            category = cmd.getAttr('{}.genre'.format(saveNode))
            group = cmd.getAttr('{}.group'.format(saveNode))
            name = cmd.getAttr('{}.name'.format(saveNode))
            variant = cmd.getAttr('{}.variant'.format(saveNode))

            self.fillCategoryComboBox()
            categoryIndex = self.categoryComboBox.findText(category, QtCore.Qt.MatchFixedString)

            if categoryIndex >= 0:
                self.fillGroupComboBox()
                groupIndex = self.groupComboBox.findText(group, QtCore.Qt.MatchFixedString)

                if groupIndex >= 0:
                    self.groupComboBox.setCurrentIndex(groupIndex)
                    self.fillNameComboBox()
                    nameIndex = self.nameComboBox.findText(name, QtCore.Qt.MatchFixedString)

                    if nameIndex >= 0:
                        self.nameComboBox.setCurrentIndex(nameIndex)
                        self.fillVariantComboBox()
                        variantIndex = self.variantComboBox.findText(variant, QtCore.Qt.MatchFixedString)

                        if variantIndex >= 0:
                            self.variantComboBox.setCurrentIndex(variantIndex)
                            self.fillVersions()

        else:
            self.fillCategoryComboBox()

    ############################################################################
    # @brief      fill guide version combo box.
    #
    def fillVersionComboBox(self, guideType='dynamicGuides'):
        self.currentVariant = self.variantComboBox.currentText()

        self.groomingExportFolder = os.path.join(
            os.getenv('CONTENT'),
            'mc_{}'.format(os.getenv('PROJECT')),
            'scenes',
            self.currentCategory,
            self.currentGroup,
            self.currentName,
            self.currentVariant,
            'export',
            'grooming'
        ).replace('\\', '/')

        self.guidesFolder = os.path.join(self.groomingExportFolder, 'dynamicGuides').replace('\\', '/')
        self.innerGuidesFolder = os.path.join(self.groomingExportFolder, 'innerGuides').replace('\\', '/')
        self.clusterGuidesFolder = os.path.join(self.groomingExportFolder, 'clusterGuides').replace('\\', '/')

        if guideType == 'innerGuides':
            targetFolder = self.innerGuidesFolder
            targetCombo = self.innerGuideVersionComboBox
            targetFiles = 'innger guides'
        elif guideType == 'clusterGuides':
            targetFolder = self.clusterGuidesFolder
            targetCombo = self.clusterGuideVersionComboBox
            targetFiles = 'cluster guides'
        else:
            targetFolder = self.guidesFolder
            targetCombo = self.guideVersionComboBox
            targetFiles = 'dynamic guides'

        if os.path.exists(targetFolder):
            versions = [dir for dir in os.listdir(targetFolder) if dir.startswith('vr')]
            versions.sort(reverse=True)

            targetCombo.clear()
            targetCombo.addItems(versions)

        else:
            rbwUI.RBWError(title='ATTENTION', text='The chosen asset has not {} file exported.'.format(targetFiles))

    ############################################################################
    # @brief      fill the versions.
    #
    def fillVersions(self):
        self.fillVersionComboBox()
        self.fillVersionComboBox(guideType='innerGuides')
        self.fillVersionComboBox(guideType='clusterGuides')

    ############################################################################
    # @brief      import selected guides.
    #
    def importGuides(self, guideType='dynamicGuides'):
        if guideType == 'innerGuides':
            currentVersion = self.innerGuideVersionComboBox.currentText()
            currentVersionGuidesFolder = os.path.join(self.innerGuidesFolder, currentVersion).replace('\\', '/')
            guidesFile = os.path.join(currentVersionGuidesFolder, 'allInnerGuides.ma').replace('\\', '/')
            guidesSavenodeName = 'innerGuides_saveNode'

        elif guideType == 'clusterGuides':
            currentVersion = self.clusterGuideVersionComboBox.currentText()
            currentVersionGuidesFolder = os.path.join(self.clusterGuidesFolder, currentVersion).replace('\\', '/')
            guidesFile = os.path.join(currentVersionGuidesFolder, 'allClusterGuides.ma').replace('\\', '/')
            guidesSavenodeName = 'clusterGuides_saveNode'

        else:
            currentVersion = self.guideVersionComboBox.currentText()
            currentVersionGuidesFolder = os.path.join(self.guidesFolder, currentVersion).replace('\\', '/')
            guidesFile = os.path.join(currentVersionGuidesFolder, 'allDynamicGuides.ma').replace('\\', '/')
            guidesSavenodeName = 'dynamicGuides_saveNode'

        cmd.file(guidesFile, i=True, type="mayaAscii", ignoreVersion=True, mergeNamespacesOnClash=False, pr=True)

        guidesSavenode = cmd.ls(guidesSavenodeName)[0]
        guidesTopGroup = cmd.listConnections(guidesSavenode)[0]
        guidesGroups = cmd.listRelatives(guidesTopGroup)
        guidesGroups.sort()

        importedTopGroup = guidesTopGroup.replace(os.getenv('PROJECT').upper(), 'GRP')
        importedGroups = []
        cmd.rename(guidesTopGroup, importedTopGroup)

        for group in guidesGroups:
            cmd.rename(group, 'GRP_{}'.format(group))
            importedGroups.append('GRP_{}'.format(group))

        importedGroups.sort()

        descSets = []
        for i in range(0, len(importedGroups)):
            guides = cmd.listRelatives(importedGroups[i])
            cmd.select(guides, replace=True)
            setName = guidesGroups[i]
            cmd.sets(name=setName)
            descSets.append(setName)

        cmd.select(descSets, noExpand=True, replace=True)
        cmd.sets(name=guidesTopGroup)
