//Maya ASCII 2020 scene
//Name: teeth_and_tongue.ma
//Last modified: Mon, Jan 15, 2024 04:56:34 PM
//Codeset: 1252
requires maya "2020";
requires -nodeType "VRaySettingsNode" -dataType "VRaySunParams" -dataType "vrayFloatVectorData"
		 -dataType "vrayFloatVectorData" -dataType "vrayIntData" "vrayformaya" "5";
requires -nodeType "renderSetup" "renderSetup.py" "1.0";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t pal;
fileInfo "vrayBuild" "5.20.03 31979 09ccb64";
fileInfo "application" "maya";
fileInfo "product" "Maya 2020";
fileInfo "version" "2020";
fileInfo "cutIdentifier" "202011110415-b1e20b88e2";
fileInfo "osv" "Microsoft Windows 10 Technical Preview  (Build 19045)\n";
fileInfo "UUID" "85F5A70E-41A0-2E7C-0066-52B88365EF67";
createNode transform -s -n "persp";
	rename -uid "538B8681-42A8-E6E1-0FFC-80B2A8CBB8E2";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 27.052411007438266 167.675579377883 63.894856420091742 ;
	setAttr ".r" -type "double3" -18.338352729602963 25.800000000000203 -8.8317459951156964e-16 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "7CADF5C9-4248-3025-CE9A-84977C9101F2";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 76.715087465900638;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0.015967884017854317 146.32291172045444 3.5281129029106233 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "809F1A39-466E-6B0E-CD1F-AC967CC44777";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "1A666C6A-4472-162D-038E-8888D93811CF";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "ED7E0C6C-42D9-7556-A6B7-3B9A1D74E1B5";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "14E851F6-4D83-DE93-0B64-98A46CABF84E";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "C640F7B0-419A-6AB8-60BF-728212835A96";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "E5084DCF-410C-8F61-CDB1-AA8996FEFD18";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "up_teeth_Ctrl_Grp";
	rename -uid "48CB32C1-4715-307C-838C-D3BC0AB2B2E9";
	setAttr ".t" -type "double3" 0.015850067138671875 147.15875244140628 3.5689961910247767 ;
createNode transform -n "up_teeth_Ctrl_Grp_offset" -p "up_teeth_Ctrl_Grp";
	rename -uid "B8BE4239-471D-1469-B896-B3ABD0EC2A48";
createNode transform -n "up_teeth_Ctrl" -p "up_teeth_Ctrl_Grp_offset";
	rename -uid "A7558920-4547-D490-34FD-33A0E9D2EF3E";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "up_teeth_CtrlShape" -p "up_teeth_Ctrl";
	rename -uid "6628CE80-4D92-B091-6A11-B08CD4630EF4";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-2.1727645390142731 0.6699743243588524 2.8231380995691193
		-4.1175413643194503 0.91247805352175615 -2.9049046757974262
		4.1177769980778152 0.91247805352175615 -2.9049046757974262
		2.1730001727726322 0.6699743243588524 2.8231380995691193
		-2.1727645390142731 0.6699743243588524 2.8231380995691193
		-2.1727645390142731 -0.83136807722350226 2.8231380995691193
		-4.1175413643194503 -0.63658379709611967 -2.9049046757974257
		-4.1175413643194503 0.91247805352175615 -2.9049046757974262
		-2.1727645390142731 0.6699743243588524 2.8231380995691193
		-2.1727645390142731 -0.83136807722350226 2.8231380995691193
		2.1730001727726322 -0.83136807722350226 2.8231380995691193
		2.1730001727726322 0.6699743243588524 2.8231380995691193
		4.1177769980778152 0.91247805352175615 -2.9049046757974262
		4.1177769980778152 -0.63658379709611967 -2.9049046757974257
		2.1730001727726322 -0.83136807722350226 2.8231380995691193
		4.1177769980778152 -0.63658379709611967 -2.9049046757974257
		-4.1175413643194503 -0.63658379709611967 -2.9049046757974257
		;
createNode transform -n "Upper_Teeth_Controls_Grp" -p "up_teeth_Ctrl";
	rename -uid "A8FCA08A-47BD-1F73-32D5-E1BE0A2FFCB8";
	setAttr ".t" -type "double3" 0.04833984375 -0.8857617301483458 -10.552409141540521 ;
createNode transform -n "C_upper_teeth_Ctrl_Grp" -p "Upper_Teeth_Controls_Grp";
	rename -uid "F92D8F41-4FEE-78B5-1F67-C7A0C5C2C779";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".t" -type "double3" -0.048340529253607656 1.0073858862986071 13.080716638495094 ;
	setAttr ".r" -type "double3" 89.999999999999986 0 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000011 1.0000000000000011 ;
createNode transform -n "C_upper_teeth_Ctrl" -p "C_upper_teeth_Ctrl_Grp";
	rename -uid "C9668A66-47F7-0FB2-2424-78BD1E6169EE";
	addAttr -ci true -sn "Mid_Roll" -ln "Mid_Roll" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on ".Mid_Roll";
createNode nurbsCurve -n "C_upper_teeth_CtrlShape" -p "C_upper_teeth_Ctrl";
	rename -uid "5382698E-42AE-FB2F-7F58-4E931181239D";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovc" 17;
	setAttr ".ovrgb" -type "float3" 0 0.052999981 0.31900001 ;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-2.0825928056288348 0.34082949777472499 1.364244463126189
		-2.0825928056288348 0.34082949777472499 -0.59963325162218761
		1.6152225268910767 0.34082949777472499 -0.59963325162218761
		1.6152225268910767 0.34082949777472499 1.364244463126189
		-2.0825928056288348 0.34082949777472499 1.364244463126189
		-2.0825928056288348 -0.83857759865015968 1.364244463126189
		-2.0825928056288348 -0.83857759865015957 -0.59963325162218761
		-2.0825928056288348 0.34082949777472499 -0.59963325162218761
		-2.0825928056288348 0.34082949777472499 1.364244463126189
		-2.0825928056288348 -0.83857759865015968 1.364244463126189
		1.6152225268910767 -0.83857759865015968 1.364244463126189
		1.6152225268910767 0.34082949777472499 1.364244463126189
		1.6152225268910767 0.34082949777472499 -0.59963325162218761
		1.6152225268910767 -0.83857759865015957 -0.59963325162218761
		1.6152225268910767 -0.83857759865015968 1.364244463126189
		1.6152225268910767 -0.83857759865015957 -0.59963325162218761
		-2.0825928056288348 -0.83857759865015957 -0.59963325162218761
		;
createNode transform -n "L_upper_teeth_Ctrl_Grp" -p "Upper_Teeth_Controls_Grp";
	rename -uid "C69D2952-4378-8907-C122-528EC8D12805";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".t" -type "double3" 2.9221464767796483 1.3967518729706114 10.250945494258925 ;
	setAttr ".r" -type "double3" 90.000000000000156 75.000000000000043 0 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000007 1.0000000000000009 ;
createNode transform -n "L_upper_teeth_Ctrl" -p "L_upper_teeth_Ctrl_Grp";
	rename -uid "B6F8F4FF-44FF-9544-F827-FA85B2ED34F7";
	addAttr -ci true -sn "Left_Roll" -ln "Left_Roll" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on ".Left_Roll";
createNode nurbsCurve -n "L_upper_teeth_CtrlShape" -p "L_upper_teeth_Ctrl";
	rename -uid "0CAA9083-473D-4DB0-BC2A-CF9648455EF6";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovc" 6;
	setAttr ".ovrgb" -type "float3" 0 0.052999981 0.31900001 ;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-1.2572455625221659 0.51928681029056833 1.5680004679779687
		-1.2572455625221597 0.51928681029056301 -0.21119065157478017
		2.4242922075743936 -0.26563680578483223 -0.21119065157477812
		2.4242922075743927 -0.268731261876264 1.2864052272811501
		-1.2572455625221659 0.51928681029056833 1.5680004679779687
		-1.5083220188466169 -0.31740933931639165 1.5680004679779687
		-1.5083220188466124 -0.3174093393163922 -0.21119065157477984
		-1.2572455625221597 0.51928681029056301 -0.21119065157478017
		-1.2572455625221659 0.51928681029056833 1.5680004679779687
		-1.5083220188466169 -0.31740933931639165 1.5680004679779687
		2.1732157512499426 -1.1054274114832265 1.2864052272811501
		2.4242922075743927 -0.268731261876264 1.2864052272811501
		2.4242922075743936 -0.26563680578483223 -0.21119065157477812
		2.1732157512499346 -1.1023329553917915 -0.21119065157477779
		2.1732157512499426 -1.1054274114832265 1.2864052272811501
		2.1732157512499346 -1.1023329553917915 -0.21119065157477779
		-1.5083220188466124 -0.3174093393163922 -0.21119065157477984
		;
createNode transform -n "R_upper_teeth_Ctrl_Grp" -p "Upper_Teeth_Controls_Grp";
	rename -uid "A49F7BB5-43DC-75DF-30BA-A6AE6FF15F3C";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".t" -type "double3" -2.922 1.3967518729706114 10.250945494258925 ;
	setAttr ".r" -type "double3" 89.999999999999858 -84.734170470027721 3.4655348445738627e-14 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
createNode transform -n "R_upper_teeth_Ctrl" -p "R_upper_teeth_Ctrl_Grp";
	rename -uid "E21683EE-4A5D-A881-FCAF-298FA688B0A9";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "R_upper_teeth_CtrlShape" -p "R_upper_teeth_Ctrl";
	rename -uid "7C5545F9-4CBD-6D7D-CD53-EFB5E7A6C1D1";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovc" 13;
	setAttr ".ovrgb" -type "float3" 0 0.052999981 0.31900001 ;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-2.3609938047115362 0.30848989071230853 1.1046134237988581
		-2.3609938047115362 0.30848989071230698 -0.17946475466685202
		1.4002352747115667 0.19523643905950835 -0.17946475466685224
		1.4002352747115614 0.19523643905950736 1.6017111475302312
		-2.3609938047115362 0.30848989071230853 1.1046134237988581
		-2.360993804711538 -0.51523268689368273 1.1046134237988581
		-2.3609938047115362 -0.51523268689368051 -0.17946475466685202
		-2.3609938047115362 0.30848989071230698 -0.17946475466685202
		-2.3609938047115362 0.30848989071230853 1.1046134237988581
		-2.360993804711538 -0.51523268689368273 1.1046134237988581
		1.4002352747115572 -0.62848613854647883 1.6017111475302312
		1.4002352747115614 0.19523643905950736 1.6017111475302312
		1.4002352747115667 0.19523643905950835 -0.17946475466685224
		1.400235274711561 -0.62848613854648172 -0.17946475466685224
		1.4002352747115572 -0.62848613854647883 1.6017111475302312
		1.400235274711561 -0.62848613854648172 -0.17946475466685224
		-2.3609938047115362 -0.51523268689368051 -0.17946475466685202
		;
createNode transform -n "low_teeth_Ctrl_Grp";
	rename -uid "9309DCEB-4693-1967-DED2-EB8821E0830B";
	setAttr ".t" -type "double3" -1.1444091796875e-05 145.4883728027344 3.4872614145278895 ;
createNode transform -n "low_teeth_Ctrl_Grp_offset" -p "low_teeth_Ctrl_Grp";
	rename -uid "14586C37-4A8D-5A88-9495-B88AB3E88879";
createNode transform -n "low_teeth_Ctrl" -p "low_teeth_Ctrl_Grp_offset";
	rename -uid "A292B991-4EB6-ECD0-82A0-8E8E33AAF021";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "low_teeth_CtrlShape" -p "low_teeth_Ctrl";
	rename -uid "B334BE10-4C4C-D2AD-AFD3-28AEDC776A94";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".tw" yes;
	setAttr -s 17 ".cp[0:16]" -type "double3" 0.1894642310766792 -0.32223769214748332 
		-0.9019159496810909 0.88329077819914259 0.89322013252654542 1.7533802330141528 -0.96657567038003167 
		0.89322013252654542 1.7533802330141528 -0.1894619774110291 -0.32223769214748332 -0.9019159496810909 
		0.1894642310766792 -0.32223769214748332 -0.9019159496810909 0.1894642310766792 -0.07569959511251878 
		-0.9019159496810909 0.88329077819914259 0.95461400225512327 1.7533802330141528 0.88329077819914259 
		0.89322013252654542 1.7533802330141528 0.1894642310766792 -0.32223769214748332 -0.9019159496810909 
		0.1894642310766792 -0.07569959511251878 -0.9019159496810909 -0.1894619774110291 -0.07569959511251878 
		-0.9019159496810909 -0.1894619774110291 -0.32223769214748332 -0.9019159496810909 
		-0.96657567038003167 0.89322013252654542 1.7533802330141528 -0.96657567038003167 
		0.95461400225512327 1.7533802330141528 -0.1894619774110291 -0.07569959511251878 -0.9019159496810909 
		-0.96657567038003167 0.95461400225512327 1.7533802330141528 0.88329077819914259 0.95461400225512327 
		1.7533802330141528;
createNode nurbsCurve -n "low_teeth_CtrlShapeOrig" -p "low_teeth_Ctrl";
	rename -uid "15D03FF0-4463-4D29-A2E3-CDA301232B5E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-2.3965251176280442 0.4596786144118446 3.4680649657497953
		-4.5414543071774753 0.4596786144118441 -4.3897980508008043
		4.5414258006551362 0.4596786144118441 -4.3897980508008043
		2.3964966111057038 0.4596786144118446 3.4680649657497953
		-2.3965251176280442 0.4596786144118446 3.4680649657497953
		-2.3965251176280442 -0.83808026164101013 3.4680649657497953
		-4.5414543071774753 -0.83808026164101102 -4.3897980508008043
		-4.5414543071774753 0.4596786144118441 -4.3897980508008043
		-2.3965251176280442 0.4596786144118446 3.4680649657497953
		-2.3965251176280442 -0.83808026164101013 3.4680649657497953
		2.3964966111057038 -0.83808026164101013 3.4680649657497953
		2.3964966111057038 0.4596786144118446 3.4680649657497953
		4.5414258006551362 0.4596786144118441 -4.3897980508008043
		4.5414258006551362 -0.83808026164101102 -4.3897980508008043
		2.3964966111057038 -0.83808026164101013 3.4680649657497953
		4.5414258006551362 -0.83808026164101102 -4.3897980508008043
		-4.5414543071774753 -0.83808026164101102 -4.3897980508008043
		;
createNode transform -n "Lower_Teeth_Controls_Grp" -p "low_teeth_Ctrl";
	rename -uid "284DFD28-45DF-E77F-51B6-E2BF18BFCA9B";
	setAttr ".t" -type "double3" 0.04833984375 -0.16149949259951768 -9.8771494950786245 ;
createNode transform -n "C_lower_teeth_Ctrl_Grp" -p "Lower_Teeth_Controls_Grp";
	rename -uid "10802F7B-48F5-3081-24AC-67A46FB8037F";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".t" -type "double3" -0.048340638338572731 -0.49059009202505877 12.188245989217735 ;
	setAttr ".r" -type "double3" 89.999999999999986 7.0622500768802538e-31 180 ;
	setAttr ".s" -type "double3" 1 1.0000000000000009 1.0000000000000009 ;
createNode transform -n "C_lower_teeth_Ctrl" -p "C_lower_teeth_Ctrl_Grp";
	rename -uid "73110395-467E-57BB-EF5F-2BB9DC526BF9";
	addAttr -ci true -sn "Mid_Roll" -ln "Mid_Roll" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on ".Mid_Roll";
createNode nurbsCurve -n "C_lower_teeth_CtrlShape" -p "C_lower_teeth_Ctrl";
	rename -uid "788E7545-47EF-5392-B5B5-7A8C42FA728C";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovc" 17;
	setAttr ".ovrgb" -type "float3" 0.12699997 0.71599996 1 ;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-1.7185896401394838 0.31255584147262017 1.3708583732141899
		-1.718589640139486 0.31255584147262017 -0.35258476836428215
		1.7185896401394483 0.31255584147262017 -0.35258476836428215
		1.7185896401394483 0.31255584147262017 1.3708583732141899
		-1.7185896401394838 0.31255584147262017 1.3708583732141899
		-1.7185896401394838 -0.61403133734582638 1.3708583732141899
		-1.718589640139486 -0.61403133734582638 -0.35258476836428215
		-1.718589640139486 0.31255584147262017 -0.35258476836428215
		-1.7185896401394838 0.31255584147262017 1.3708583732141899
		-1.7185896401394838 -0.61403133734582638 1.3708583732141899
		1.7185896401394483 -0.61403133734582638 1.3708583732141899
		1.7185896401394483 0.31255584147262017 1.3708583732141899
		1.7185896401394483 0.31255584147262017 -0.35258476836428215
		1.7185896401394483 -0.61403133734582638 -0.35258476836428215
		1.7185896401394483 -0.61403133734582638 1.3708583732141899
		1.7185896401394483 -0.61403133734582638 -0.35258476836428215
		-1.718589640139486 -0.61403133734582638 -0.35258476836428215
		;
createNode transform -n "L_lower_teeth_Ctrl_Grp" -p "Lower_Teeth_Controls_Grp";
	rename -uid "DE485EAE-46AE-02A3-3361-99A7C2CFA39C";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".t" -type "double3" 2.7407989885712025 0.22261465141266984 9.6024778179270367 ;
	setAttr ".r" -type "double3" 90.000000000000384 -86.202805765357837 179.99999999999983 ;
	setAttr ".s" -type "double3" 0.99999999999999978 1.0000000000000004 1.0000000000000004 ;
createNode transform -n "L_lower_teeth_Ctrl" -p "L_lower_teeth_Ctrl_Grp";
	rename -uid "1409EB14-469D-6E52-40D5-3D9F281427E9";
	addAttr -ci true -sn "Left_Roll" -ln "Left_Roll" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on ".Left_Roll";
createNode nurbsCurve -n "L_lower_teeth_CtrlShape" -p "L_lower_teeth_Ctrl";
	rename -uid "E733A733-45A8-0DBD-CF5E-4B820F1FCBBB";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovc" 6;
	setAttr ".ovrgb" -type "float3" 0.12699997 0.71599996 1 ;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-1.9301495777838023 0.24891307971237597 1.1075940300517506
		-2.1237067087644226 0.26474493490048512 -0.37598738198478898
		1.4171881537227295 -0.024879810078984557 -0.93517227085469923
		1.6738588551532392 -0.045873990740218806 0.8867042170996321
		-1.9301495777838023 0.24891307971237597 1.1075940300517506
		-1.9999356530753356 -0.60427767748774941 1.1075940300517515
		-2.193492784055953 -0.58844582229963938 -0.37598738198478898
		-2.1237067087644226 0.26474493490048512 -0.37598738198478898
		-1.9301495777838023 0.24891307971237597 1.1075940300517506
		-1.9999356530753356 -0.60427767748774941 1.1075940300517515
		1.6040727798617049 -0.89906474794034363 0.88670421709963232
		1.6738588551532392 -0.045873990740218806 0.8867042170996321
		1.4171881537227295 -0.024879810078984557 -0.93517227085469923
		1.3474020784311955 -0.87807056727910393 -0.93517227085469867
		1.6040727798617049 -0.89906474794034363 0.88670421709963232
		1.3474020784311955 -0.87807056727910393 -0.93517227085469867
		-2.193492784055953 -0.58844582229963938 -0.37598738198478898
		;
createNode transform -n "R_lower_teeth_Ctrl_Grp" -p "Lower_Teeth_Controls_Grp";
	rename -uid "761ACE22-4E20-F7F5-9F6E-3196C86EF12F";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".t" -type "double3" -2.741 0.22261465141266967 9.6024778179270367 ;
	setAttr ".r" -type "double3" 90.000000000000156 74.999999999999972 -179.99999999999955 ;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "R_lower_teeth_Ctrl" -p "R_lower_teeth_Ctrl_Grp";
	rename -uid "2FF2CFAC-4E6A-3DF4-19D3-D09830E2C355";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "R_lower_teeth_CtrlShape" -p "R_lower_teeth_Ctrl";
	rename -uid "D6F2F6B1-4557-F848-0029-66A0F0F0C06B";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovc" 13;
	setAttr ".ovrgb" -type "float3" 0.12699997 0.71599996 1 ;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-1.7034619994945346 0.34172487050070655 0.91207609279884194
		-1.4038556256648216 0.30250583913198409 -0.9402984850041326
		2.4887130630268399 0.10555837762116349 -0.27299649771176432
		2.2627778479096299 0.13513371734346249 1.1238918373522715
		-1.7034619994945346 0.34172487050070655 0.91207609279884194
		-1.8062980922430307 -0.58397239092480713 0.91504234009700491
		-1.5066917184133155 -0.62319142229352464 -0.93733223770596952
		-1.4038556256648216 0.30250583913198409 -0.9402984850041326
		-1.7034619994945346 0.34172487050070655 0.91207609279884194
		-1.8062980922430307 -0.58397239092480713 0.91504234009700491
		2.1599417551611331 -0.79056354408205243 1.1268580846504357
		2.2627778479096299 0.13513371734346249 1.1238918373522715
		2.4887130630268399 0.10555837762116349 -0.27299649771176432
		2.3858769702783467 -0.82013888380435218 -0.27003025041360124
		2.1599417551611331 -0.79056354408205243 1.1268580846504357
		2.3858769702783467 -0.82013888380435218 -0.27003025041360124
		-1.5066917184133155 -0.62319142229352464 -0.93733223770596952
		;
createNode transform -n "Teeth_joints_Grp";
	rename -uid "5E567DD5-4DDA-59AD-3D20-B9A86823E33A";
	setAttr ".t" -type "double3" 8.8595365645274504e-30 2.8421709430404007e-14 -8.8817841970012523e-15 ;
	setAttr ".s" -type "double3" 0.99999999999999967 1 0.99999999999999989 ;
createNode joint -n "L_upper_teeth_jnt" -p "Teeth_joints_Grp";
	rename -uid "90E5D381-4BBF-181F-9BAA-01BCD23D31AB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90.000000000000156 75.000000000000043 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.25881904510252018 -2.3585880124878049e-16 -0.96592582628906853 0
		 0.96592582628906809 -2.36513988710429e-15 0.25881904510252007 0 -2.2266907561572161e-15 -1 -3.8857805861880474e-16 0
		 2.9863363876683189 147.66974258422854 3.2675325437431768 1;
	setAttr ".radi" 0.5;
createNode parentConstraint -n "L_upper_teeth_jnt_parentConstraint1" -p "L_upper_teeth_jnt";
	rename -uid "F1F37D33-43B1-24AA-D7EA-D5A3B81180B3";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "L_upper_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 3.5527136788005009e-15 -1.7763568394002505e-15 
		-3.5527136788005009e-15 ;
	setAttr ".tg[0].tor" -type "double3" 1.4124500153760508e-30 0 1.5902773407317584e-15 ;
	setAttr ".lr" -type "double3" 1.2722218725854064e-14 6.3611093629270304e-15 6.3611093629270335e-15 ;
	setAttr ".rst" -type "double3" 1.5463641881942733 13.801017761230476 12.425179481506346 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "L_upper_teeth_jnt_scaleConstraint1" -p "L_upper_teeth_jnt";
	rename -uid "CE1F94F3-4FA3-42CF-5B2A-BBACB3A4E655";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "L_upper_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.99999999999999978 0.99999999999999933 0.99999999999999911 ;
	setAttr -k on ".w0";
createNode joint -n "C_upper_teeth_jnt" -p "Teeth_joints_Grp";
	rename -uid "30F6369C-4741-ED78-00AD-28A51CF192CA";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99999999999999989 2.2814897236420741e-16 1.8257625707878869e-17 0
		 -5.2289679590516637e-17 5.2735593669694946e-16 1 0 2.1579989801812771e-16 -1.0000000000000002 4.9960036108132044e-16 0
		 0.015849381635064216 147.28037659755654 6.097303687979351 1;
	setAttr ".radi" 0.5;
createNode parentConstraint -n "C_upper_teeth_jnt_parentConstraint1" -p "C_upper_teeth_jnt";
	rename -uid "C7FAF8A2-48D7-4FFE-DF78-AFB207A84C00";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "C_upper_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 0 1.7763568394002505e-15 -3.5527136788005009e-15 ;
	setAttr ".rst" -type "double3" -6.8550360765584628e-07 13.823082923889164 13.922321319580078 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "C_upper_teeth_jnt_scaleConstraint1" -p "C_upper_teeth_jnt";
	rename -uid "37EDBA30-400F-C985-52BE-EAA02E799CDD";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "C_upper_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1 0.99999999999999867 0.99999999999999867 ;
	setAttr -k on ".w0";
createNode joint -n "R_upper_teeth_jnt" -p "Teeth_joints_Grp";
	rename -uid "0375B4AF-46B6-9084-E577-1F919CF460CC";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999943 -75 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.091776733581064571 5.4700617117941932e-16 0.99577960973962043 0
		 -0.99577960973961999 3.5594589440456384e-16 0.091776733581064737 0 -4.5033391675696614e-16 -0.99999999999999978 3.8857805861880469e-16 0
		 -2.8578100891113278 147.66974258422854 3.267532543743179 1;
	setAttr ".radi" 0.5;
createNode parentConstraint -n "R_upper_teeth_jnt_parentConstraint1" -p "R_upper_teeth_jnt";
	rename -uid "3DB4A929-4AA4-E5F2-FA70-E794468278F5";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "R_upper_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -1.7763568394002505e-15 0 -1.7763568394002505e-15 ;
	setAttr ".tg[0].tor" -type "double3" 2.4717875269080888e-30 1.4124500153760508e-30 
		0 ;
	setAttr ".lr" -type "double3" -8.9813825530153438e-14 -5.1204696134978466e-15 9.7341704700277081 ;
	setAttr ".rst" -type "double3" -1.546364545822144 13.801013946533205 12.425180435180661 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "R_upper_teeth_jnt_scaleConstraint1" -p "R_upper_teeth_jnt";
	rename -uid "16871A8F-4D5D-6AF6-37EB-108DBAC12C07";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "R_upper_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
	setAttr -k on ".w0";
createNode joint -n "L_lower_teeth_jnt" -p "Teeth_joints_Grp";
	rename -uid "F86A4640-4EBC-DFD5-A15E-DDBA7266D044";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90 -75 179.99999999999997 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.066225038138776379 4.005542136935994e-16 0.99780471251819502 0
		 0.99780471251819436 -1.9622183547807717e-16 0.066225038138776365 0 3.3931161429445029e-16 0.99999999999999978 -2.7755575615628909e-16 0
		 2.7891273882294061 145.54948796154756 3.212589737376303 1;
	setAttr ".radi" 0.5;
createNode parentConstraint -n "L_lower_teeth_jnt_parentConstraint1" -p "L_lower_teeth_jnt";
	rename -uid "BCB9ADE3-4213-5FB0-9895-F4A91C88A9C5";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "L_lower_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 1.7763568394002505e-15 8.8817841970012523e-16 
		3.5527136788005009e-15 ;
	setAttr ".tg[0].tor" -type "double3" -2.9131781567131048e-30 7.0622500768802538e-31 
		0 ;
	setAttr ".lr" -type "double3" 1.7819191944541933e-13 -7.8888314441182836e-15 11.202805765357859 ;
	setAttr ".rst" -type "double3" 1.5188629627227788 12.104801177978519 12.348910331726074 ;
	setAttr ".rsrr" -type "double3" -8.8278125961003172e-32 -3.1805546814635168e-15 
		3.1805546814635168e-15 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "L_lower_teeth_jnt_scaleConstraint1" -p "L_lower_teeth_jnt";
	rename -uid "D83C9F6A-46CC-9D38-A117-69A2B1664B46";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "L_lower_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1.0000000000000004 0.99999999999999956 0.99999999999999956 ;
	setAttr -k on ".w0";
createNode joint -n "C_lower_teeth_jnt" -p "Teeth_joints_Grp";
	rename -uid "2331ACEB-4366-D0CD-0464-E382D05FEFA9";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999986 7.0622500768802538e-31 180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.99999999999999989 -1.0568429244947204e-16 -1.8257625707878915e-17 0
		 -5.2289679590516526e-17 -1.3877787807814447e-16 1 0 -9.3335218103392424e-17 1.0000000000000002 -5.5511151231257901e-17 0
		 -1.2238680369605626e-05 144.83628321810983 5.7983579086669979 1;
	setAttr ".radi" 0.5;
createNode parentConstraint -n "C_lower_teeth_jnt_parentConstraint1" -p "C_lower_teeth_jnt";
	rename -uid "2D2649A2-4BEE-77FF-D181-31B0D7DE5BC2";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "C_lower_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 0 -1.7763568394002505e-15 0 ;
	setAttr ".tg[0].tor" -type "double3" 0 3.1362690564052943e-46 -1.5681345282026479e-46 ;
	setAttr ".rst" -type "double3" -7.9458857271674788e-07 12.119765281677246 13.597440719604489 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "C_lower_teeth_jnt_scaleConstraint1" -p "C_lower_teeth_jnt";
	rename -uid "15F8DECB-448B-409A-9F15-08B2C2955249";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "C_lower_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1 0.99999999999999889 0.99999999999999889 ;
	setAttr -k on ".w0";
createNode joint -n "R_lower_teeth_jnt" -p "Teeth_joints_Grp";
	rename -uid "2165043C-4496-EAA2-3BC7-C393B6C6EC83";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90.000000000000156 74.999999999999972 -179.99999999999955 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.25881904510252113 -2.3523588439109141e-15 -0.96592582628906853 0
		 -0.96592582628906776 -5.4704251155004637e-15 0.25881904510252118 0 -5.8224261723751672e-15 1.0000000000000002 -7.2164496600635195e-16 0
		 -2.692671600341797 145.54948796154756 3.212589737376303 1;
	setAttr ".radi" 0.5;
createNode parentConstraint -n "R_lower_teeth_jnt_parentConstraint1" -p "R_lower_teeth_jnt";
	rename -uid "A98A4926-475F-3821-DE36-C29DCB0D5431";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "R_lower_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -1.7763568394002505e-15 8.8817841970012523e-16 
		3.5527136788005009e-15 ;
	setAttr ".tg[0].tor" -type "double3" 2.6836550292144963e-29 1.6949400184512606e-29 
		-3.180554681463516e-15 ;
	setAttr ".lr" -type "double3" -1.9083328088781101e-14 3.1805546814635168e-15 -5.2966875576601921e-31 ;
	setAttr ".rst" -type "double3" -1.5188646316528325 12.104801177978519 12.348910331726074 ;
	setAttr ".rsrr" -type "double3" -1.9083328088781101e-14 3.1805546814635168e-15 -5.2966875576601921e-31 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "R_lower_teeth_jnt_scaleConstraint1" -p "R_lower_teeth_jnt";
	rename -uid "D5FA27B3-4226-C7E6-1FE0-4883CB9EBD87";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "R_lower_teeth_CtrlW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
	setAttr -k on ".w0";
createNode transform -n "Tongue_Controls_Grp";
	rename -uid "6D2366AF-4A13-2427-799C-2A84C4A7467A";
	setAttr ".t" -type "double3" 2.3665827039080919e-30 2.8421709430404007e-14 -4.8849813083506888e-15 ;
createNode transform -n "tongue_01_Ctrl_Grp" -p "Tongue_Controls_Grp";
	rename -uid "1828CDB9-4C9E-2A79-EB56-2EA4A5675386";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".t" -type "double3" 1.380102491973467e-19 143.68276977539063 -1.441702008247375 ;
	setAttr ".r" -type "double3" -90 -48.100688621388002 90.000000000000028 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999978 1.0000000000000002 ;
createNode transform -n "tongue_01_Ctrl_Grp_rotate_offset" -p "tongue_01_Ctrl_Grp";
	rename -uid "A67988B8-42B5-CBE5-8521-DCB197F1E9BB";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
createNode transform -n "tongue_01_Ctrl_Grp_translate_offset" -p "tongue_01_Ctrl_Grp_rotate_offset";
	rename -uid "3BA867CD-412A-5B3D-B203-7197DB5B0F47";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 0 -3.7947076036992655e-19 ;
	setAttr ".s" -type "double3" 1 1.0000000000000004 1.0000000000000002 ;
createNode transform -n "tongue_01_Ctrl_Grp_offset" -p "tongue_01_Ctrl_Grp_translate_offset";
	rename -uid "1765A9C9-4158-C11A-6F1F-068AECF7A0AC";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 0 -2.1684043449710089e-19 ;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999989 1 ;
createNode transform -n "tongue_01_Ctrl" -p "tongue_01_Ctrl_Grp_offset";
	rename -uid "D8171B25-41F4-179B-C827-968D6688C24E";
	setAttr -l on -k off ".v";
	setAttr ".rp" -type "double3" 1.4210854715202004e-14 -1.4210854715202016e-14 5.96311192708704e-19 ;
	setAttr ".sp" -type "double3" 1.4210854715202004e-14 -1.4210854715202004e-14 5.96311192708704e-19 ;
	setAttr ".spt" -type "double3" 0 -1.3065508742723067e-29 0 ;
createNode nurbsCurve -n "tongue_01_CtrlShape" -p "tongue_01_Ctrl";
	rename -uid "98250DD1-4E95-4F03-763F-1AB177B88E04";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-0.062457137266068358 0.52397057173747907 1.7052390348361783
		-0.062457137266068358 0.52397057173747907 -1.7046608304477486
		2.2943521663370543 0.64002396840216358 -2.4710336881004564
		2.2943521663370987 0.64002396840217912 2.473312687971585
		-0.062457137266068358 0.52397057173747907 1.7052390348361783
		-0.041693038982734548 -0.70182472450514111 1.7041742318485296
		-0.041693038982734548 -0.70182472450514111 -1.7057256334353972
		-0.062457137266068358 0.52397057173747907 -1.7046608304477486
		-0.062457137266068358 0.52397057173747907 1.7052390348361783
		-0.041693038982734548 -0.70182472450514111 1.7041742318485296
		2.0379180158558841 -0.67699119678894359 2.5583761469076793
		2.2943521663370987 0.64002396840217912 2.473312687971585
		2.2943521663370543 0.64002396840216358 -2.4710336881004564
		2.0379180158559347 -0.67699119678894459 -2.5592640176962527
		2.0379180158558841 -0.67699119678894359 2.5583761469076793
		2.0379180158559347 -0.67699119678894459 -2.5592640176962527
		-0.041693038982734548 -0.70182472450514111 -1.7057256334353972
		;
createNode transform -n "tongue_02_Ctrl_Grp" -p "tongue_01_Ctrl";
	rename -uid "53366673-496B-7DF8-EC5C-358E11673E83";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".t" -type "double3" 2.2141598339521096 -2.8421709430404007e-14 1.1384122811287123e-18 ;
	setAttr ".r" -type "double3" 0 0 -23.679852013670622 ;
	setAttr ".s" -type "double3" 0.99999999999999911 1.0000000000000002 1.0000000000000009 ;
createNode transform -n "tongue_02_Ctrl_Grp_offset" -p "tongue_02_Ctrl_Grp";
	rename -uid "5D974FD7-41C5-15D3-EB73-18B20C1E2D32";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 2.1684043449710089e-19 ;
	setAttr ".s" -type "double3" 1 1.0000000000000004 1.0000000000000002 ;
createNode transform -n "tongue_02_Ctrl" -p "tongue_02_Ctrl_Grp_offset";
	rename -uid "F934AE2C-4D38-67C6-6FFB-6EB093323043";
	setAttr -l on -k off ".v";
	setAttr ".rp" -type "double3" -1.3269527698867023e-09 4.3675640881701818e-10 -1.0839416232143264e-19 ;
	setAttr ".sp" -type "double3" -1.3269527698867023e-09 4.3675640881701838e-10 -1.0839416232143247e-19 ;
	setAttr ".spt" -type "double3" 0 -2.0682039669775495e-25 -1.6851887013104222e-34 ;
createNode nurbsCurve -n "tongue_02_CtrlShape" -p "tongue_02_Ctrl";
	rename -uid "E19A5EA6-480D-5460-1092-47BB61826721";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-0.18360634919471 0.61835386521697122 2.4733125991447764
		-0.18360634919471 0.61835386521697122 -2.4710336863371278
		1.3109548890003322 0.49645275684900275 -2.3801056735544641
		1.3109548890003362 0.49645275684897017 2.3764963738947325
		-0.18360634919471 0.61835386521697122 2.4733125991447764
		0.11049096164595215 -0.69078424175854825 2.5583761664490003
		0.11049096164595215 -0.69078424175854825 -2.5592640204831434
		-0.18360634919471 0.61835386521697122 -2.4710336863371278
		-0.18360634919471 0.61835386521697122 2.4733125991447764
		0.11049096164595215 -0.69078424175854825 2.5583761664490003
		1.0714344951403745 -0.51396158186441687 2.3764963738947329
		1.3109548890003362 0.49645275684897017 2.3764963738947325
		1.3109548890003322 0.49645275684900275 -2.3801056735544641
		1.0714344951403665 -0.51396158186449614 -2.3801056735544641
		1.0714344951403745 -0.51396158186441687 2.3764963738947329
		1.0714344951403665 -0.51396158186449614 -2.3801056735544641
		0.11049096164595215 -0.69078424175854825 -2.5592640204831434
		;
createNode transform -n "tongue_03_Ctrl_Grp" -p "tongue_02_Ctrl";
	rename -uid "F1CC6AE5-49B5-FF4B-4954-CCBA29671ED9";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".t" -type "double3" 1.19327051118605 2.8421709430404007e-14 1.2742086032134629e-16 ;
	setAttr ".r" -type "double3" 0 0 -13.336003514900856 ;
	setAttr ".s" -type "double3" 0.99999999999999745 1 1.0000000000000016 ;
createNode transform -n "tongue_03_Ctrl_Grp_offset" -p "tongue_03_Ctrl_Grp";
	rename -uid "6CA0B267-4B8C-9C21-7DDB-1690B799DD98";
	setAttr ".t" -type "double3" 0 1.7763568394002505e-15 2.1684043449710089e-19 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
createNode transform -n "tongue_03_Ctrl" -p "tongue_03_Ctrl_Grp_offset";
	rename -uid "C8620E01-456A-1449-5AD5-289FD9F79C0E";
	setAttr -l on -k off ".v";
	setAttr ".rp" -type "double3" -6.49484478643103e-07 -7.6016991954475088e-06 2.4038328222708802e-21 ;
	setAttr ".sp" -type "double3" -6.494844786431031e-07 -7.6016991954475088e-06 2.4038328222708802e-21 ;
	setAttr ".spt" -type "double3" 1.0587911820957208e-22 0 0 ;
createNode nurbsCurve -n "tongue_03_CtrlShape" -p "tongue_03_Ctrl";
	rename -uid "E085D9FB-4367-4765-A238-509452E8F6AB";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		5.031531761584979e-15 0.51022796774422674 2.3764963206621279
		5.031531761584979e-15 0.51022796774422674 -2.3801056783185985
		1.625224545380868 0.50482165038840321 -2.1826262791044138
		1.6252756097358827 0.50001388250568324 2.1791011411988208
		5.031531761584979e-15 0.51022796774422674 2.3764963206621279
		-2.1366763747808845e-15 -0.52820951860187448 2.3764963206621279
		-2.1366763747808845e-15 -0.52820951860187448 -2.3801056783185985
		5.031531761584979e-15 0.51022796774422674 -2.3801056783185985
		5.031531761584979e-15 0.51022796774422674 2.3764963206621279
		-2.1366763747808845e-15 -0.52820951860187448 2.3764963206621279
		1.522693116129848 -0.48220996805713212 2.1791011412017927
		1.6251734810260143 0.50962941827123165 2.1791011412047618
		1.6252245453810232 0.50482165038840321 -2.1826262791044138
		1.522693116129848 -0.48220996805715105 -2.1826262791044138
		1.522693116129848 -0.48220996805713212 2.1791011412017927
		1.522693116129848 -0.48220996805715105 -2.1826262791044138
		-2.1366763747808845e-15 -0.52820951860187448 -2.3801056783185985
		;
createNode transform -n "tongue_04_Ctrl_Grp" -p "tongue_03_Ctrl";
	rename -uid "816DA21C-4E52-D4E9-6B48-A3A50D1616BE";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".t" -type "double3" 1.5815476449291666 0 2.702373914920246e-16 ;
	setAttr ".r" -type "double3" 0 0 -5.9305091909821863 ;
	setAttr ".s" -type "double3" 0.99999999999999523 0.99999999999999989 1.0000000000000051 ;
createNode transform -n "tongue_04_Ctrl_Grp_offset" -p "tongue_04_Ctrl_Grp";
	rename -uid "599F3096-4B8C-8790-70E1-1F98A1634619";
	setAttr ".t" -type "double3" 0 0 -5.4210108624275222e-20 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000002 1.0000000000000004 ;
createNode transform -n "tongue_04_Ctrl" -p "tongue_04_Ctrl_Grp_offset";
	rename -uid "4B240CB1-4F93-C825-83E6-B8BCA8730E30";
	setAttr -l on -k off ".v";
	setAttr ".rp" -type "double3" 1.1918937958821369e-07 2.1784103410086744e-09 -5.4015880628239472e-23 ;
	setAttr ".sp" -type "double3" 1.1918937958821374e-07 2.1784103410027456e-09 -2.1689445037181248e-19 ;
	setAttr ".spt" -type "double3" -5.2939556639595453e-23 5.9288170373183815e-21 2.1684043449118424e-19 ;
createNode nurbsCurve -n "tongue_04_CtrlShape" -p "tongue_04_Ctrl";
	rename -uid "4B420E3F-48DF-879C-5A69-148133FAD85A";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-0.008815223561619661 0.5076947954049098 2.1791011450679307
		-0.008815223561619661 0.5076947954049098 -2.1826262044599121
		1.3673975342214248 0.46590935917200238 -1.8972675634712939
		1.3676662404610456 0.4659142701984727 1.8972675634712897
		-0.008815223561619661 0.5076947954049098 2.1791011450679307
		-0.0088152235616330982 -0.4846529866911497 2.1791011450679187
		-0.0088152235616330982 -0.4846529866911497 -2.1826262044599156
		-0.008815223561619661 0.5076947954049098 -2.1826262044599121
		-0.008815223561619661 0.5076947954049098 2.1791011450679307
		-0.0088152235616330982 -0.4846529866911497 2.1791011450679187
		1.3622257773026434 -0.47726718789644024 1.8972675634712894
		1.3676662404610456 0.4659142701984727 1.8972675634712897
		1.3673975342214248 0.46590935917200238 -1.8972675634712939
		1.3619565641183542 -0.47724436150462679 -1.8972675634712945
		1.3622257773026434 -0.47726718789644024 1.8972675634712894
		1.3619565641183542 -0.47724436150462679 -1.8972675634712945
		-0.0088152235616330982 -0.4846529866911497 -2.1826262044599156
		;
createNode transform -n "tongue_05_Ctrl_Grp" -p "tongue_04_Ctrl";
	rename -uid "17C9CE15-4C9E-8BD2-9BB6-BF9367F99FE6";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".t" -type "double3" 1.3648439306084601 2.8421709430404007e-14 2.6690346981160643e-16 ;
	setAttr ".r" -type "double3" -1.1725138118817024e-05 0.0040652135736868843 -0.33051108044916527 ;
	setAttr ".s" -type "double3" 0.99999999999998868 1.0000000000000004 1.0000000000000122 ;
createNode transform -n "tongue_05_Ctrl_Grp_offset" -p "tongue_05_Ctrl_Grp";
	rename -uid "C68C4A69-482D-2609-3731-19B5BF12E2FE";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 0 0 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000002 ;
createNode transform -n "tongue_05_Ctrl" -p "tongue_05_Ctrl_Grp_offset";
	rename -uid "C47AF814-4A05-D781-B414-84B37BE1980E";
	setAttr -l on -k off ".v";
	setAttr ".rp" -type "double3" 1.8341613139405371e-07 -7.6271894329238421e-06 1.1452778599075307e-11 ;
	setAttr ".sp" -type "double3" 1.834161312830318e-07 -7.6271894329238421e-06 1.1452778599075307e-11 ;
	setAttr ".spt" -type "double3" 1.1102190541582244e-16 0 0 ;
createNode nurbsCurve -n "tongue_05_CtrlShape" -p "tongue_05_Ctrl";
	rename -uid "5952EF03-40C6-64E1-F79B-97B5FFC0C2D7";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-1.6732080406848097e-16 0.46593131809691862 1.8972674783649608
		-1.6732080406848097e-16 0.46593131809691862 -1.8972674783649481
		1.4118145728834144 0.39207559596117136 -1.2597368802363429
		1.4118145728834144 0.39207559596117136 1.2595663720166181
		-1.6732080406848097e-16 0.46593131809691862 1.8972674783649608
		-7.0198475837863433e-15 -0.4772614626028106 1.8972674783649521
		-7.0198475837863433e-15 -0.4772614626028106 -1.8972674783649561
		-1.6732080406848097e-16 0.46593131809691862 -1.8972674783649481
		-1.6732080406848097e-16 0.46593131809691862 1.8972674783649608
		-7.0198475837863433e-15 -0.4772614626028106 1.8972674783649521
		1.4118145728834082 -0.38176739500050133 1.2595663720166124
		1.4118145728834144 0.39207559596117136 1.2595663720166181
		1.4118145728834144 0.39207559596117136 -1.2597368802363429
		1.4118145728834082 -0.38176739500050133 -1.2597368802363509
		1.4118145728834082 -0.38176739500050133 1.2595663720166124
		1.4118145728834082 -0.38176739500050133 -1.2597368802363509
		-7.0198475837863433e-15 -0.4772614626028106 -1.8972674783649561
		;
createNode transform -n "Tongue_joints_Grp";
	rename -uid "CD725FCC-4CFA-D611-981A-AD878D1603C8";
	setAttr ".t" -type "double3" 8.8595365645274504e-30 2.8421709430404007e-14 -8.8817841970012523e-15 ;
	setAttr ".s" -type "double3" 0.99999999999999967 1 0.99999999999999989 ;
createNode joint -n "tongue_01_jnt" -p "Tongue_joints_Grp";
	rename -uid "8EC98FAE-4C96-19E2-B700-C6971134633F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 1.3801024918769823e-19 143.68276977539063 -1.4417020082473806 ;
	setAttr ".r" -type "double3" -3.1805546814635168e-15 8.8278125961003172e-32 3.1805546814635168e-15 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -90 -48.100688621388002 90.000000000000028 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -4.0508110377239025e-16 0.66782360975780197 0.74431957266355608 0
		 -3.4774841022150546e-16 0.74431957266355642 -0.66782360975780197 0 -0.99999999999999989 -5.6121587975175438e-16 -1.8257625707878777e-17 0
		 1.3801024919548883e-19 143.68276977539065 -1.4417020082473893 1;
	setAttr ".typ" 1;
	setAttr ".radi" 0.5;
createNode joint -n "tongue_02_jnt" -p "tongue_01_jnt";
	rename -uid "2910957C-466A-F894-2203-9987F865E7FD";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 2.2141598339521096 2.8421709430404007e-14 6.5052130349130266e-19 ;
	setAttr ".r" -type "double3" 0 0 -6.3611093629270335e-15 ;
	setAttr ".s" -type "double3" 1 1.0000000000000009 1.0000000000000007 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -23.679852013670622 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -2.313101273780839e-16 0.31265753948934866 0.9498659184329471 0
		 -4.8116035000546074e-16 0.94986591843294821 -0.3126575394893491 0 -1.0000000000000004 -5.6121587975175477e-16 -1.8257625707878789e-17 0
		 -4.9215475662506614e-16 145.16143798828131 0.206340493168647 1;
	setAttr ".radi" 0.5;
createNode joint -n "tongue_03_jnt" -p "tongue_02_jnt";
	rename -uid "2245ED55-4783-CEB5-8740-0AAB1CD76C67";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 1.1932705111860784 5.6843418860808015e-14 1.2774612097309825e-16 ;
	setAttr ".r" -type "double3" 0 0 -1.2722218725854067e-14 ;
	setAttr ".s" -type "double3" 0.999999999999999 0.99999999999999933 1.0000000000000004 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -13.336003514900856 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -1.1408761444385797e-16 0.085129224987064905 0.9963699187817241 0
		 -5.2153971437998091e-16 0.99636991878172543 -0.085129224987065683 0 -1.0000000000000009 -5.6121587975175507e-16 -1.8257625707878798e-17 0
		 -7.5613714190094772e-16 145.53452301025396 1.3397874832153414 1;
	setAttr ".radi" 0.5;
createNode joint -n "tongue_04_jnt" -p "tongue_03_jnt";
	rename -uid "51686BC4-444A-A01F-59A9-1B90E501C3F6";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 1.5815476449291914 0 2.7018318138338139e-16 ;
	setAttr ".r" -type "double3" 0 0 -2.4649298781342254e-14 ;
	setAttr ".s" -type "double3" 0.99999999999999922 1.0000000000000007 1.0000000000000033 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -5.9305091909821863 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -5.9590386667410244e-17 -0.018273511105107568 0.99983302545569464 0
		 -5.3053618468665393e-16 0.99983302545569741 0.018273511105106693 0 -1.0000000000000042 -5.6121587975175694e-16 -1.825762570787886e-17 0
		 -1.09530248537118e-15 145.66915893554693 2.9155939817428607 1;
	setAttr ".radi" 0.50052768041686158;
createNode joint -n "tongue_05_jnt" -p "tongue_04_jnt";
	rename -uid "F6209C24-4E84-ABCE-AF9D-77A3415E78BE";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 1.3648439306084543 -5.6843418860808015e-14 2.669239693748925e-16 ;
	setAttr ".r" -type "double3" 1.9958543590574262e-18 -2.17394353320093e-17 -2.8724384434524254e-14 ;
	setAttr ".s" -type "double3" 0.99999999999999434 1 1.0000000000000049 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.1725138118817035e-05 0.0040652135736869467 -0.33051108044916527 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 7.0951361598016306e-05 -0.024040718401777295 0.99971097764534711 0
		 2.0464226437848531e-07 0.99971098016200399 0.024040718447772225 0 -0.99999999748294055 -1.5011385895065452e-06 7.0935774994083974e-05 0
		 -1.3775848523579696e-15 145.64421844482416 4.2802100181579528 1;
	setAttr ".radi" 0.50227282651671001;
createNode joint -n "tongue_06_jnt" -p "tongue_05_jnt";
	rename -uid "D5E8587C-4E1E-0828-2334-55954677DEF5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 1.3809673054870384 -9.2148526219454302e-15 -2.4615883883883368e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -8.4082091718014191e-05 -1.8223526324225785e-05 -12.228858466511156 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 6.898002049420542e-05 -0.23525106892065945 0.97193463247968992 0
		 1.6696217647564905e-05 0.97193463492751864 0.23525106832817572 0 -0.99999999748150592 8.6484937003834339e-12 7.0971874084901202e-05 0
		 9.7981510645517674e-05 145.61101899871088 5.6607781932226606 1;
	setAttr ".radi" 0.50227282651671001;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "CA6BDA69-4EBC-91FE-F613-E488E5A80766";
	setAttr -s 5 ".lnk";
	setAttr -s 3 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "28C81D3B-446E-CD5F-6A0C-67893D5D5A58";
	setAttr -s 2 ".bsdt";
	setAttr ".bsdt[0].bscd" -type "Int32Array" 1 -1 ;
	setAttr ".bsdt[1].bscd" -type "Int32Array" 0 ;
	setAttr ".bsdt[1].bsdn" -type "string" "Group 1";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "6D067812-4EF0-D795-2D8D-A0AE49AB633E";
createNode displayLayerManager -n "layerManager";
	rename -uid "F1815CC9-4191-0465-1B82-148CD6D2A92A";
	setAttr ".cdl" 2;
	setAttr -s 3 ".dli[1:2]"  2 4;
createNode displayLayer -n "defaultLayer";
	rename -uid "38111566-40B0-8351-E6A6-FDB9F34E59F4";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "F3BEE09C-4C11-B577-BF07-FB912070DBE1";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "5F384F71-4EFB-59BB-4B84-C09611D91F6E";
	setAttr ".g" yes;
createNode renderSetup -n "renderSetup";
	rename -uid "6971BB56-4E3C-84A4-B228-F2A907C435A7";
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "9693D2EB-43DE-CE3F-431E-BF80ACE23C4B";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -622.61902287839052 -174.9999930461251 ;
	setAttr ".tgi[0].vh" -type "double2" 592.85711929911758 407.14284096445425 ;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo1";
	rename -uid "91EC58A1-45AD-5079-F089-759B6F097E36";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -622.61902287839052 -174.9999930461251 ;
	setAttr ".tgi[0].vh" -type "double2" 592.85711929911758 407.14284096445425 ;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo3";
	rename -uid "5BC584DB-461E-36C4-AA34-66B0E2609D69";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -5066.4034829977691 3367.7242350210277 ;
	setAttr ".tgi[0].vh" -type "double2" -770.55877479946446 4351.7579602868564 ;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo4";
	rename -uid "79896E7B-4684-9ED6-EC81-9CA4C2AD4A29";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -511.94655948481727 -100.99472870802012 ;
	setAttr ".tgi[0].vh" -type "double2" 723.05588272111322 338.09522466054091 ;
createNode VRaySettingsNode -s -n "vraySettings";
	rename -uid "F564C785-4A56-F78D-055C-56838F99C385";
	setAttr ".gi" yes;
	setAttr ".rfc" yes;
	setAttr ".pe" 2;
	setAttr ".se" 3;
	setAttr ".cmph" 60;
	setAttr ".csdu" 0;
	setAttr ".cfile" -type "string" "";
	setAttr ".cfile2" -type "string" "";
	setAttr ".casf" -type "string" "";
	setAttr ".casf2" -type "string" "";
	setAttr ".st" 3;
	setAttr ".msr" 6;
	setAttr ".aaft" 3;
	setAttr ".aafs" 2;
	setAttr ".dma" 24;
	setAttr ".dmig" 1;
	setAttr ".dmag" 48;
	setAttr ".dam" 1;
	setAttr ".pt" 0.0099999997764825821;
	setAttr ".pmt" 0;
	setAttr ".sd" 1000;
	setAttr ".ss" 0.01;
	setAttr ".pfts" 20;
	setAttr ".ufg" yes;
	setAttr ".fnm" -type "string" "";
	setAttr ".lcfnm" -type "string" "";
	setAttr ".asf" -type "string" "";
	setAttr ".lcasf" -type "string" "";
	setAttr ".urtrshd" yes;
	setAttr ".rtrshd" 2;
	setAttr ".lightCacheType" 1;
	setAttr ".ifile" -type "string" "";
	setAttr ".ifile2" -type "string" "";
	setAttr ".iasf" -type "string" "";
	setAttr ".iasf2" -type "string" "";
	setAttr ".pmfile" -type "string" "";
	setAttr ".pmfile2" -type "string" "";
	setAttr ".pmasf" -type "string" "";
	setAttr ".pmasf2" -type "string" "";
	setAttr ".dmcstd" yes;
	setAttr ".dmculs" no;
	setAttr ".dmcsat" 0.004999999888241291;
	setAttr ".cmtp" 6;
	setAttr ".cmao" 2;
	setAttr ".cg" 2.2000000476837158;
	setAttr ".mtah" yes;
	setAttr ".rgbcs" -1;
	setAttr ".suv" 0;
	setAttr ".srflc" 1;
	setAttr ".srdml" 0;
	setAttr ".seu" yes;
	setAttr ".gormio" yes;
	setAttr ".gocle" yes;
	setAttr ".gopl" 2;
	setAttr ".gopv" yes;
	setAttr ".wi" 960;
	setAttr ".he" 540;
	setAttr ".aspr" 1.7777780294418335;
	setAttr ".productionGPUResizeTextures" 0;
	setAttr ".autolt" 0;
	setAttr ".jpegq" 100;
	setAttr ".vfbOn" yes;
	setAttr ".vfbSA" -type "Int32Array" 1026 0 4096 1 4084 0 1
		 4076 1700143739 1869181810 825893486 1632379436 1936876921 578501154 1936876886 577662825 573321530 1935764579 574235251
		 1953460082 1881287714 1701867378 1701409906 2067407475 1919252002 1852795251 741423650 1835101730 574235237 1696738338 1818386798
		 1949966949 744846706 1886938402 577007201 1818322490 573334899 1634760805 1650549870 975332716 1702195828 1931619453 1814913653
		 1919252833 1530536563 1818436219 577991521 1751327290 779317089 1886611812 1132028268 1701999215 1869182051 573317742 1886351984
		 1769239141 975336293 1702240891 1869181810 825893486 1634607660 975332717 1936278562 2036427888 1919894304 1952671090 577662825
		 1852121644 1701601889 1920219682 573334901 1634760805 975332462 1702195828 2019893804 1684955504 1701601889 1920219682 573334901
		 1718579824 577072233 573321530 1869641829 1701999987 774912546 1931619376 1600484961 1600284530 1835627120 1986622569 975336293
		 1936482662 1763847269 1717527395 577072233 740434490 1667459362 1869770847 1701603686 1952539743 1849303649 745303157 1667459362
		 1852142175 1953392996 578055781 573321274 1886088290 1852793716 1715085942 1702063201 1668227628 1717530473 577072233 740434490
		 1768124194 1868783471 1936879468 1701011824 741358114 1768124194 1768185711 1634496627 1986356345 577069929 573321274 1869177711
		 1701410399 1634890871 1868985198 975334770 1864510512 1601136995 1702257011 1835626089 577070945 1818322490 746415475 1651864354
		 2036427821 577991269 578509626 1935764579 574235251 1868654691 1701981811 1869819494 1701016181 1684828006 740455013 1869770786
		 1953654128 577987945 1981971258 1769173605 975335023 1847733297 577072481 1867719226 1701016181 1196564538 573317698 1650552421
		 975332716 1702195828 2019893804 1684955504 1634089506 744846188 1886938402 1633971809 577072226 1818322490 573334899 1852140642
		 1869438820 975332708 1864510512 1768120688 975337844 741355057 1869116194 1919967095 1701410405 1949966967 744846706 1668444962
		 1887007839 809116261 1931619453 1814913653 1919252833 1530536563 1818436219 577991521 1751327290 779317089 778462578 1751607660
		 2020175220 1881287714 1701867378 1701409906 2067407475 1919252002 1852795251 741423650 1835101730 574235237 1751607628 2020167028
		 1696738338 1818386798 1715085925 1702063201 2019893804 1684955504 1634089506 744846188 1886938402 1633971809 577072226 1970435130
		 573341029 761427315 1702453612 975336306 746413403 1818436219 577991521 1751327290 779317089 778462578 1886220131 1953067887
		 573317733 1886351984 1769239141 975336293 1702240891 1869181810 825893486 1634607660 975332717 1836008226 1769172848 740451700
		 1634624802 577072226 1818322490 573334899 1634760805 975332462 1936482662 1696738405 1851879544 1818386788 1949966949 744846706
		 1701601826 1834968174 577070191 573321274 1667330159 578385001 808333626 1752375852 1885304687 1769366898 975337317 1702195828
		 1931619453 1814913653 1919252833 1530536563 2103278941 1663204140 1936941420 1663187490 1936679272 778399790 1869505892 1919251305
		 1881287714 1701867378 1701409906 2067407475 1919252002 1852795251 741423650 1835101730 574235237 1869505860 1919251305 1853169722
		 1767994977 1818386796 573317733 1650552421 975332716 1936482662 1696738405 1851879544 1715085924 1702063201 2019893804 1684955504
		 1701601889 1920219682 573334901 1852140642 1869438820 975332708 1864510512 1768120688 975337844 741355057 1952669986 577074793
		 1818322490 573334899 1936028272 975336549 1931619378 1852142196 577270887 808333626 1634869804 1937074532 808532514 573321262
		 1665234792 1701602659 1702125938 1920219682 573334901 1869505892 1919251305 1685024095 825893477 1931619453 1814913653 1919252833
		 1530536563 2066513245 1634493218 975336307 1634231074 1882092399 1752378981 1701868129 1818373742 740455029 1869770786 1953654128
		 577987945 1981971258 1769173605 975335023 1847733297 577072481 1750278714 1701868129 1816276846 740455029 1634624802 577072226
		 1818322490 573334899 1634760805 975332462 1936482662 1696738405 1851879544 1818386788 1949966949 744846706 1701601826 1834968174
		 577070191 573321274 1667330159 578385001 808333626 1752375852 1701868129 1818386286 1667199605 1970302319 975332724 1936482662
		 1931619429 1886544232 1633644133 1853189997 825893492 573321262 1918986355 1601070448 1768186226 975336309 741682736 1970037282
		 1634885490 1937074532 774978082 808465203 875573296 892418354 1931619453 1814913653 1919252833 1530536563 2066513245 1634493218
		 975336307 1634231074 1882092399 1701588581 2019980142 1881287714 1701867378 1701409906 2067407475 1919252002 1852795251 741423650
		 1835101730 574235237 1936614732 1717978400 1937007461 1696738338 1818386798 1715085925 1702063201 2019893804 1684955504 1634089506
		 744846188 1886938402 1633971809 577072226 1970435130 1646406757 1684956524 1685024095 842670693 1886331436 1953063777 825893497
		 573321262 1918987367 1852792677 1634089506 744846188 1634494242 1935631730 577075817 774910778 1730292784 1701994860 1768257375
		 578054247 808333626 1818370604 1601007471 1734960503 975336552 808726064 808464432 959787056 1730292790 1701994860 1919448159
		 1869116261 975332460 741355057 1818846754 1601332596 1635020658 1852795252 774912546 1931619376 1920300129 1869182049 825893486
		 573321262 1685217640 1701994871 1667457375 1919249509 1684370529 1920219682 573334901 1684828003 1918990175 1715085933 1702063201
		 1852383788 1634887028 1986622563 1949966949 744846706 1986097954 1818713957 577073761 1970435130 1646406757 1600482145 1918987367
		 1702322021 1952999273 1634089506 744846188 1701995298 1600484449 1701209701 1601401955 1970496882 1667200108 1852727656 975334501
		 1936482662 1696738405 1818386798 1818386277 1936024673 1920219682 573334901 1701079411 909779571 1818370604 1936024673 1953460831
		 1869182049 825893486 741355061 1920234274 1600872805 1920298082 774912546 808464434 808464432 741882162 1702065442 1634887519
		 1735289204 1634089506 744846188 1634887458 1735289204 1852138591 2037672307 808794658 573321262 1952543335 1600613993 1735288172
		 975333492 808333361 1919361580 1852404833 1870290791 975334767 741355061 1634887458 1735289204 1869378399 975332720 741355056
		 1634887458 1735289204 1920234335 1952935525 825893480 573321262 1600484213 1818452847 1869181813 1715085934 1702063201 1668227628
		 1937075299 1601073001 1835891059 1769108581 1949966947 744846706 1667460898 1769174380 1885302383 1701016165 975336558 808333362
		 1668227628 1937075299 1601073001 1635020658 1852795252 774912546 1864510512 1970037603 1852795251 1668440415 808532514 741355056
		 1702065442 1919120223 1751348321 1634089506 744846188 1919120162 1836675935 1920230765 975332201 1702195828 1668489772 1634754418
		 1919251572 809116270 1668489772 1970102130 1734964332 1701994860 1970234207 975336558 1931619377 1683976803 1769172581 975337844
		 808333365 1668489772 1701601138 1752459118 808794658 573321262 1601332083 1886350451 1635147621 1851877746 975332707 741355056
		 1919120162 1684633439 1985964148 1634300513 577069934 808333370 1668489772 1702059890 975332453 1931619376 2053075555 577597295
		 808333882 1668489772 1869766514 1769234804 975335023 741355056 1919120162 1920234335 1952935525 825893480 573321262 1600484213
		 1953723748 1634089506 744846188 1937073186 1634754420 1919251572 809116270 1969496620 1683977331 1769172581 975337844 808333365
		 1969496620 1918858355 1969841249 1635147635 1851877746 975332707 741355056 1937073186 1768578932 1919251572 774912546 1679961136
		 1601467253 1836019578 775043618 1679961136 1601467253 1635020658 1852795252 774912546 1679961136 1601467253 1701999731 1752459118
		 774978082 1730292784 1701994860 1702065503 1935830879 1818452340 1835622245 577070945 1818322490 573334899 1918987367 1651466085
		 1667331187 1767859564 1701273965 1952542815 574235240 1864510498 1601467234 1734438249 1870290789 975334767 741355057 1935830818
		 1835622260 1600481121 1635020658 1852795252 774912546 1864510512 1601467234 1734438249 1953718117 1735288178 975333492 741355057
		 1702065442 1818846815 1601332596 1734438249 1715085925 1702063201 1818698284 1600483937 1734438249 1634754405 975333492 573317666
		 1701667171 1952407922 577073273 573321274 1919251571 1834970981 577070191 746401850 1651864354 2036427821 577991269 2103270202
		 2066513245 1634493218 975336307 1634231074 1865315183 1819436406 1932425569 1886216564 1881287714 1701867378 1701409906 2067407475
		 1919252002 1852795251 741423650 1835101730 574235237 1835103315 573317744 1650552421 975332716 1936482662 1696738405 1851879544
		 1715085924 1702063201 2019893804 1684955504 1701601889 1920219682 573334901 1835103347 1869111152 1601857906 1734962273 825893486
		 1953702444 1601203553 1953654134 1768710495 975335015 573321779 1835103347 1868783472 577924972 774986554 774974512 774974512
		 573332784 1835103347 1868980080 975336558 1702240891 1869181810 825893486 1869619756 1601465961 1702521203 808532514 1634083372
		 2037148013 741358114 2037674786 975332716 1998728240 1751607653 809116276 1634083372 975332707 1769095458 2099407969 1953702444
		 1601203553 1769108595 975333230 1702240891 1869181810 825893486 1634869804 1953718135 1735289202 572668450 1768301100 1600938350
		 1769108595 975333230 2105352738 1970479660 1634479458 1936876921 1566259746 746413437 1734693410 1198419817 975335013 1702240891
		 1869181810 825893486 1869423148 1600484213 1819045734 1700755311 1818386798 975332453 1936482662 1830956133 1702065519 1819240031
		 1601662828 1852403568 1869373300 1684368227 1634089506 744846188 1970236706 1717527923 1869376623 1869635447 1601465961 809116280
		 1869423148 1600484213 1819045734 1885304687 1953393007 975337823 573340976 1684956498 1767273061 975337317 1702240891 1869181810
		 825893486 1852121644 1701601889 1852142175 1601332580 1768383858 975335023 1936482662 1914842213 1701080677 1701994354 1852795239
		 573601887 774974778 1914842160 1701080677 1701994354 1852795239 573602143 774974778 1914842160 1701080677 1701994354 1852795239
		 573667423 774974778 1914842160 1701080677 1701994354 1852795239 573667679 774974778 1981951024 1601660265 577004914 1970435130
		 1981951077 1601660265 1701147239 1949966958 744846706 1701410338 1818386295 975332725 1702195828 1769349676 1834973029 577728111
		 1818322490 573334899 1869377379 1818451826 1601203553 1701080941 959855138 1868767788 1601335148 1835101283 1852792688 1920219682
		 573334901 1600484213 1702390128 1935761260 1952671088 577662815 1818322490 573334899 1702390128 1852399468 1818193766 1701536623
		 1715085924 1702063201 1768956460 1600939384 1868983913 1919902559 1952671090 1667196005 1919904879 1715085939 1702063201 1092758653
		 1869182051 975336302 1702240891 1869181810 825893486 1634738732 1231385461 1667191376 1801676136 975332453 1936482662 1948396645
		 1383363429 1918858085 1869177953 825571874 1702109740 1699902579 1751342963 1701536613 1715085924 1702063201 1701061164 1399289186
		 1768186216 1918855022 1869177953 909457954 1701061164 1399289186 1768186216 1667196782 1801676136 975332453 1936482662 1931619429
		 1701995892 1685015919 1634885477 577726820 741881658 1702130466 1299146098 1600480367 1667590243 577004907 1818322490 2105369971 ;
	setAttr ".vfbSyncM" yes;
	setAttr ".mSceneName" -type "string" "C:/Users/f.cecchini/Desktop/scenes/teeth_and_tongue.ma";
	setAttr ".rt_cpuRayBundleSize" 4;
	setAttr ".rt_gpuRayBundleSize" 128;
	setAttr ".rt_maxPaths" 10000;
	setAttr ".rt_engineType" 3;
	setAttr ".rt_gpuResizeTextures" 0;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "02E82A76-46FB-0AF1-4352-7197A7B6823A";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 0.6\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 456\n            -height 332\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 0.6\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 919\n            -height 708\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 0.6\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n"
		+ "            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n"
		+ "            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 456\n            -height 332\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n"
		+ "            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 0.6\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n"
		+ "            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 959\n            -height 708\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -autoExpandAnimatedShapes 1\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n"
		+ "            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -selectCommand \"print(\\\"\\\")\" \n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n"
		+ "            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n"
		+ "            -autoExpand 0\n            -autoExpandAnimatedShapes 1\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n"
		+ "            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n"
		+ "                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -autoExpandAnimatedShapes 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n"
		+ "                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -showPlayRangeShades \"on\" \n                -lockPlayRangeShades \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -valueLinesToggle 1\n                -outliner \"graphEditor1OutlineEd\" \n                -highlightAffectedCurves 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n"
		+ "            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -autoExpandAnimatedShapes 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n"
		+ "                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n"
		+ "                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n"
		+ "                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n"
		+ "                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n"
		+ "                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n"
		+ "                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n"
		+ "\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n{ string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n"
		+ "                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 0.6\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n"
		+ "                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n"
		+ "                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName; };\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 0.6\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 959\\n    -height 708\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 0.6\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 959\\n    -height 708\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "8F9B16F0-47E3-7301-00B9-EF90AE126ADF";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -k on ".fzn";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av ".unw" 1;
	setAttr -av -k on ".etw";
	setAttr -av -k on ".tps";
	setAttr -av -k on ".tms";
select -ne :hardwareRenderingGlobals;
	setAttr -av -k on ".cch";
	setAttr -k on ".fzn";
	setAttr -av -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".rm";
	setAttr -k on ".lm";
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr -k on ".hom";
	setAttr -k on ".hodm";
	setAttr -k on ".xry";
	setAttr -k on ".jxr";
	setAttr -k on ".sslt";
	setAttr -k on ".cbr";
	setAttr -k on ".bbr";
	setAttr -av -k on ".mhl";
	setAttr -k on ".cons";
	setAttr -k on ".vac";
	setAttr -av -k on ".hwi";
	setAttr -k on ".csvd";
	setAttr -av -k on ".ta";
	setAttr -av -k on ".tq";
	setAttr -k on ".ts";
	setAttr -av -k on ".etmr";
	setAttr -av -k on ".tmr";
	setAttr -av -k on ".aoon";
	setAttr -av -k on ".aoam";
	setAttr -av -k on ".aora";
	setAttr -k on ".aofr";
	setAttr -av -k on ".aosm";
	setAttr -av -k on ".hff";
	setAttr -av -k on ".hfd";
	setAttr -av -k on ".hfs";
	setAttr -av -k on ".hfe";
	setAttr -av ".hfc";
	setAttr -av -k on ".hfcr";
	setAttr -av -k on ".hfcg";
	setAttr -av -k on ".hfcb";
	setAttr -av -k on ".hfa";
	setAttr -av -k on ".mbe";
	setAttr -k on ".mbt";
	setAttr -av -k on ".mbsof";
	setAttr -k on ".mbsc";
	setAttr -k on ".mbc";
	setAttr -k on ".mbfa";
	setAttr -k on ".mbftb";
	setAttr -k on ".mbftg";
	setAttr -k on ".mbftr";
	setAttr -k on ".mbfta";
	setAttr -k on ".mbfe";
	setAttr -k on ".mbme";
	setAttr -k on ".mbcsx";
	setAttr -k on ".mbcsy";
	setAttr -k on ".mbasx";
	setAttr -k on ".mbasy";
	setAttr -av -k on ".blen";
	setAttr -k on ".blth";
	setAttr -k on ".blfr";
	setAttr -k on ".blfa";
	setAttr -av -k on ".blat";
	setAttr -av -k on ".msaa" yes;
	setAttr -av -k on ".aasc";
	setAttr -k on ".aasq";
	setAttr -k on ".laa";
	setAttr ".fprt" yes;
	setAttr -k on ".rtfm";
select -ne :renderPartition;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -k on ".fzn";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".bbx";
	setAttr -k on ".vwm";
	setAttr -k on ".tpv";
	setAttr -k on ".uit";
	setAttr -k on ".mwc";
	setAttr -av -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -av -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k on ".macc";
	setAttr -av -k on ".macd";
	setAttr -av -k on ".macq";
	setAttr -av -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -av -k on ".clip";
	setAttr -av -k on ".edm";
	setAttr -av -k on ".edl";
	setAttr -av -k on ".ren";
	setAttr -av -k on ".esr";
	setAttr -av -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -av -cb on ".imfkey";
	setAttr -av -k on ".gama";
	setAttr -av -k on ".exrc";
	setAttr -av -k on ".expt";
	setAttr -av -k on ".an";
	setAttr -k on ".ar";
	setAttr -av -k on ".fs" 1;
	setAttr -av -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -av -cb on ".me";
	setAttr -cb on ".se";
	setAttr -av -k on ".be";
	setAttr -av -cb on ".ep";
	setAttr -av -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -av -k on ".pff";
	setAttr -av -k on ".peie";
	setAttr -av -k on ".ifp";
	setAttr -k on ".rv";
	setAttr -av -k on ".comp";
	setAttr -av -k on ".cth";
	setAttr -av -k on ".soll";
	setAttr -av -cb on ".sosl";
	setAttr -av -k on ".rd";
	setAttr -av -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -av -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -av -k on ".mm";
	setAttr -av -k on ".npu";
	setAttr -av -k on ".itf";
	setAttr -av -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -av -k on ".uf";
	setAttr -av -k on ".oi";
	setAttr -av -k on ".rut";
	setAttr -av -k on ".mot";
	setAttr -av -k on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -av -k on ".mbso";
	setAttr -av -k on ".mbsc";
	setAttr -av -k on ".afp";
	setAttr -av -k on ".pfb";
	setAttr -av -k on ".pram";
	setAttr -av -k on ".poam";
	setAttr -av -k on ".prlm";
	setAttr -av -k on ".polm";
	setAttr -av -cb on ".prm";
	setAttr -av -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -av -k on ".ubc";
	setAttr -av -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -av -k on ".udbx";
	setAttr -av -k on ".smc";
	setAttr -av -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -av -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -av -k on ".tlwd";
	setAttr -av -k on ".tlht";
	setAttr -av -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -av -k on ".ope";
	setAttr -av -k on ".oppf";
	setAttr -av -k on ".rcp";
	setAttr -av -k on ".icp";
	setAttr -av -k on ".ocp";
	setAttr -cb on ".hbl";
	setAttr ".dss" -type "string" "lambert1";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -av -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w";
	setAttr -av -k on ".h";
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar";
	setAttr -av -k on ".ldar";
	setAttr -av -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -av -k on ".isu";
	setAttr -av -k on ".pdu";
select -ne :hardwareRenderGlobals;
	setAttr -av -k on ".cch";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -av -k off -cb on ".fbfm";
	setAttr -av -k off -cb on ".ehql";
	setAttr -av -k off -cb on ".eams";
	setAttr -av -k off -cb on ".eeaa";
	setAttr -av -k off -cb on ".engm";
	setAttr -av -k off -cb on ".mes";
	setAttr -av -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -av -k off -cb on ".mbs";
	setAttr -av -k off -cb on ".trm";
	setAttr -av -k off -cb on ".tshc";
	setAttr -av -k off -cb on ".enpt";
	setAttr -av -k off -cb on ".clmt";
	setAttr -av -k off -cb on ".tcov";
	setAttr -av -k off -cb on ".lith";
	setAttr -av -k off -cb on ".sobc";
	setAttr -av -k off -cb on ".cuth";
	setAttr -av -k off -cb on ".hgcd";
	setAttr -av -k off -cb on ".hgci";
	setAttr -av -k off -cb on ".mgcs";
	setAttr -av -k off -cb on ".twa";
	setAttr -av -k off -cb on ".twz";
	setAttr -av -k on ".hwcc";
	setAttr -av -k on ".hwdp";
	setAttr -av -k on ".hwql";
	setAttr -av -k on ".hwfr";
	setAttr -av -k on ".soll";
	setAttr -av -k on ".sosl";
	setAttr -av -k on ".bswa";
	setAttr -av -k on ".shml";
	setAttr -av -k on ".hwel";
connectAttr "low_teeth_CtrlShapeOrig.ws" "low_teeth_CtrlShape.cr";
connectAttr "L_upper_teeth_jnt_parentConstraint1.ctx" "L_upper_teeth_jnt.tx";
connectAttr "L_upper_teeth_jnt_parentConstraint1.cty" "L_upper_teeth_jnt.ty";
connectAttr "L_upper_teeth_jnt_parentConstraint1.ctz" "L_upper_teeth_jnt.tz";
connectAttr "L_upper_teeth_jnt_parentConstraint1.crx" "L_upper_teeth_jnt.rx";
connectAttr "L_upper_teeth_jnt_parentConstraint1.cry" "L_upper_teeth_jnt.ry";
connectAttr "L_upper_teeth_jnt_parentConstraint1.crz" "L_upper_teeth_jnt.rz";
connectAttr "L_upper_teeth_jnt_scaleConstraint1.csx" "L_upper_teeth_jnt.sx";
connectAttr "L_upper_teeth_jnt_scaleConstraint1.csy" "L_upper_teeth_jnt.sy";
connectAttr "L_upper_teeth_jnt_scaleConstraint1.csz" "L_upper_teeth_jnt.sz";
connectAttr "L_upper_teeth_jnt.ro" "L_upper_teeth_jnt_parentConstraint1.cro";
connectAttr "L_upper_teeth_jnt.pim" "L_upper_teeth_jnt_parentConstraint1.cpim";
connectAttr "L_upper_teeth_jnt.rp" "L_upper_teeth_jnt_parentConstraint1.crp";
connectAttr "L_upper_teeth_jnt.rpt" "L_upper_teeth_jnt_parentConstraint1.crt";
connectAttr "L_upper_teeth_jnt.jo" "L_upper_teeth_jnt_parentConstraint1.cjo";
connectAttr "L_upper_teeth_Ctrl.t" "L_upper_teeth_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "L_upper_teeth_Ctrl.rp" "L_upper_teeth_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "L_upper_teeth_Ctrl.rpt" "L_upper_teeth_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "L_upper_teeth_Ctrl.r" "L_upper_teeth_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "L_upper_teeth_Ctrl.ro" "L_upper_teeth_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "L_upper_teeth_Ctrl.s" "L_upper_teeth_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "L_upper_teeth_Ctrl.pm" "L_upper_teeth_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "L_upper_teeth_jnt_parentConstraint1.w0" "L_upper_teeth_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "L_upper_teeth_jnt.pim" "L_upper_teeth_jnt_scaleConstraint1.cpim";
connectAttr "L_upper_teeth_Ctrl.s" "L_upper_teeth_jnt_scaleConstraint1.tg[0].ts"
		;
connectAttr "L_upper_teeth_Ctrl.pm" "L_upper_teeth_jnt_scaleConstraint1.tg[0].tpm"
		;
connectAttr "L_upper_teeth_jnt_scaleConstraint1.w0" "L_upper_teeth_jnt_scaleConstraint1.tg[0].tw"
		;
connectAttr "C_upper_teeth_jnt_parentConstraint1.ctx" "C_upper_teeth_jnt.tx";
connectAttr "C_upper_teeth_jnt_parentConstraint1.cty" "C_upper_teeth_jnt.ty";
connectAttr "C_upper_teeth_jnt_parentConstraint1.ctz" "C_upper_teeth_jnt.tz";
connectAttr "C_upper_teeth_jnt_parentConstraint1.crx" "C_upper_teeth_jnt.rx";
connectAttr "C_upper_teeth_jnt_parentConstraint1.cry" "C_upper_teeth_jnt.ry";
connectAttr "C_upper_teeth_jnt_parentConstraint1.crz" "C_upper_teeth_jnt.rz";
connectAttr "C_upper_teeth_jnt_scaleConstraint1.csx" "C_upper_teeth_jnt.sx";
connectAttr "C_upper_teeth_jnt_scaleConstraint1.csy" "C_upper_teeth_jnt.sy";
connectAttr "C_upper_teeth_jnt_scaleConstraint1.csz" "C_upper_teeth_jnt.sz";
connectAttr "C_upper_teeth_jnt.ro" "C_upper_teeth_jnt_parentConstraint1.cro";
connectAttr "C_upper_teeth_jnt.pim" "C_upper_teeth_jnt_parentConstraint1.cpim";
connectAttr "C_upper_teeth_jnt.rp" "C_upper_teeth_jnt_parentConstraint1.crp";
connectAttr "C_upper_teeth_jnt.rpt" "C_upper_teeth_jnt_parentConstraint1.crt";
connectAttr "C_upper_teeth_jnt.jo" "C_upper_teeth_jnt_parentConstraint1.cjo";
connectAttr "C_upper_teeth_Ctrl.t" "C_upper_teeth_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "C_upper_teeth_Ctrl.rp" "C_upper_teeth_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "C_upper_teeth_Ctrl.rpt" "C_upper_teeth_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "C_upper_teeth_Ctrl.r" "C_upper_teeth_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "C_upper_teeth_Ctrl.ro" "C_upper_teeth_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "C_upper_teeth_Ctrl.s" "C_upper_teeth_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "C_upper_teeth_Ctrl.pm" "C_upper_teeth_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "C_upper_teeth_jnt_parentConstraint1.w0" "C_upper_teeth_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "C_upper_teeth_jnt.pim" "C_upper_teeth_jnt_scaleConstraint1.cpim";
connectAttr "C_upper_teeth_Ctrl.s" "C_upper_teeth_jnt_scaleConstraint1.tg[0].ts"
		;
connectAttr "C_upper_teeth_Ctrl.pm" "C_upper_teeth_jnt_scaleConstraint1.tg[0].tpm"
		;
connectAttr "C_upper_teeth_jnt_scaleConstraint1.w0" "C_upper_teeth_jnt_scaleConstraint1.tg[0].tw"
		;
connectAttr "R_upper_teeth_jnt_parentConstraint1.ctx" "R_upper_teeth_jnt.tx";
connectAttr "R_upper_teeth_jnt_parentConstraint1.cty" "R_upper_teeth_jnt.ty";
connectAttr "R_upper_teeth_jnt_parentConstraint1.ctz" "R_upper_teeth_jnt.tz";
connectAttr "R_upper_teeth_jnt_parentConstraint1.crx" "R_upper_teeth_jnt.rx";
connectAttr "R_upper_teeth_jnt_parentConstraint1.cry" "R_upper_teeth_jnt.ry";
connectAttr "R_upper_teeth_jnt_parentConstraint1.crz" "R_upper_teeth_jnt.rz";
connectAttr "R_upper_teeth_jnt_scaleConstraint1.csx" "R_upper_teeth_jnt.sx";
connectAttr "R_upper_teeth_jnt_scaleConstraint1.csy" "R_upper_teeth_jnt.sy";
connectAttr "R_upper_teeth_jnt_scaleConstraint1.csz" "R_upper_teeth_jnt.sz";
connectAttr "R_upper_teeth_jnt.ro" "R_upper_teeth_jnt_parentConstraint1.cro";
connectAttr "R_upper_teeth_jnt.pim" "R_upper_teeth_jnt_parentConstraint1.cpim";
connectAttr "R_upper_teeth_jnt.rp" "R_upper_teeth_jnt_parentConstraint1.crp";
connectAttr "R_upper_teeth_jnt.rpt" "R_upper_teeth_jnt_parentConstraint1.crt";
connectAttr "R_upper_teeth_jnt.jo" "R_upper_teeth_jnt_parentConstraint1.cjo";
connectAttr "R_upper_teeth_Ctrl.t" "R_upper_teeth_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "R_upper_teeth_Ctrl.rp" "R_upper_teeth_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "R_upper_teeth_Ctrl.rpt" "R_upper_teeth_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "R_upper_teeth_Ctrl.r" "R_upper_teeth_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "R_upper_teeth_Ctrl.ro" "R_upper_teeth_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "R_upper_teeth_Ctrl.s" "R_upper_teeth_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "R_upper_teeth_Ctrl.pm" "R_upper_teeth_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "R_upper_teeth_jnt_parentConstraint1.w0" "R_upper_teeth_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "R_upper_teeth_jnt.pim" "R_upper_teeth_jnt_scaleConstraint1.cpim";
connectAttr "R_upper_teeth_Ctrl.s" "R_upper_teeth_jnt_scaleConstraint1.tg[0].ts"
		;
connectAttr "R_upper_teeth_Ctrl.pm" "R_upper_teeth_jnt_scaleConstraint1.tg[0].tpm"
		;
connectAttr "R_upper_teeth_jnt_scaleConstraint1.w0" "R_upper_teeth_jnt_scaleConstraint1.tg[0].tw"
		;
connectAttr "L_lower_teeth_jnt_parentConstraint1.ctx" "L_lower_teeth_jnt.tx";
connectAttr "L_lower_teeth_jnt_parentConstraint1.cty" "L_lower_teeth_jnt.ty";
connectAttr "L_lower_teeth_jnt_parentConstraint1.ctz" "L_lower_teeth_jnt.tz";
connectAttr "L_lower_teeth_jnt_parentConstraint1.crx" "L_lower_teeth_jnt.rx";
connectAttr "L_lower_teeth_jnt_parentConstraint1.cry" "L_lower_teeth_jnt.ry";
connectAttr "L_lower_teeth_jnt_parentConstraint1.crz" "L_lower_teeth_jnt.rz";
connectAttr "L_lower_teeth_jnt_scaleConstraint1.csx" "L_lower_teeth_jnt.sx";
connectAttr "L_lower_teeth_jnt_scaleConstraint1.csy" "L_lower_teeth_jnt.sy";
connectAttr "L_lower_teeth_jnt_scaleConstraint1.csz" "L_lower_teeth_jnt.sz";
connectAttr "L_lower_teeth_jnt.ro" "L_lower_teeth_jnt_parentConstraint1.cro";
connectAttr "L_lower_teeth_jnt.pim" "L_lower_teeth_jnt_parentConstraint1.cpim";
connectAttr "L_lower_teeth_jnt.rp" "L_lower_teeth_jnt_parentConstraint1.crp";
connectAttr "L_lower_teeth_jnt.rpt" "L_lower_teeth_jnt_parentConstraint1.crt";
connectAttr "L_lower_teeth_jnt.jo" "L_lower_teeth_jnt_parentConstraint1.cjo";
connectAttr "L_lower_teeth_Ctrl.t" "L_lower_teeth_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "L_lower_teeth_Ctrl.rp" "L_lower_teeth_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "L_lower_teeth_Ctrl.rpt" "L_lower_teeth_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "L_lower_teeth_Ctrl.r" "L_lower_teeth_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "L_lower_teeth_Ctrl.ro" "L_lower_teeth_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "L_lower_teeth_Ctrl.s" "L_lower_teeth_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "L_lower_teeth_Ctrl.pm" "L_lower_teeth_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "L_lower_teeth_jnt_parentConstraint1.w0" "L_lower_teeth_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "L_lower_teeth_jnt.pim" "L_lower_teeth_jnt_scaleConstraint1.cpim";
connectAttr "L_lower_teeth_Ctrl.s" "L_lower_teeth_jnt_scaleConstraint1.tg[0].ts"
		;
connectAttr "L_lower_teeth_Ctrl.pm" "L_lower_teeth_jnt_scaleConstraint1.tg[0].tpm"
		;
connectAttr "L_lower_teeth_jnt_scaleConstraint1.w0" "L_lower_teeth_jnt_scaleConstraint1.tg[0].tw"
		;
connectAttr "C_lower_teeth_jnt_parentConstraint1.ctx" "C_lower_teeth_jnt.tx";
connectAttr "C_lower_teeth_jnt_parentConstraint1.cty" "C_lower_teeth_jnt.ty";
connectAttr "C_lower_teeth_jnt_parentConstraint1.ctz" "C_lower_teeth_jnt.tz";
connectAttr "C_lower_teeth_jnt_parentConstraint1.crx" "C_lower_teeth_jnt.rx";
connectAttr "C_lower_teeth_jnt_parentConstraint1.cry" "C_lower_teeth_jnt.ry";
connectAttr "C_lower_teeth_jnt_parentConstraint1.crz" "C_lower_teeth_jnt.rz";
connectAttr "C_lower_teeth_jnt_scaleConstraint1.csx" "C_lower_teeth_jnt.sx";
connectAttr "C_lower_teeth_jnt_scaleConstraint1.csy" "C_lower_teeth_jnt.sy";
connectAttr "C_lower_teeth_jnt_scaleConstraint1.csz" "C_lower_teeth_jnt.sz";
connectAttr "C_lower_teeth_jnt.ro" "C_lower_teeth_jnt_parentConstraint1.cro";
connectAttr "C_lower_teeth_jnt.pim" "C_lower_teeth_jnt_parentConstraint1.cpim";
connectAttr "C_lower_teeth_jnt.rp" "C_lower_teeth_jnt_parentConstraint1.crp";
connectAttr "C_lower_teeth_jnt.rpt" "C_lower_teeth_jnt_parentConstraint1.crt";
connectAttr "C_lower_teeth_jnt.jo" "C_lower_teeth_jnt_parentConstraint1.cjo";
connectAttr "C_lower_teeth_Ctrl.t" "C_lower_teeth_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "C_lower_teeth_Ctrl.rp" "C_lower_teeth_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "C_lower_teeth_Ctrl.rpt" "C_lower_teeth_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "C_lower_teeth_Ctrl.r" "C_lower_teeth_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "C_lower_teeth_Ctrl.ro" "C_lower_teeth_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "C_lower_teeth_Ctrl.s" "C_lower_teeth_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "C_lower_teeth_Ctrl.pm" "C_lower_teeth_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "C_lower_teeth_jnt_parentConstraint1.w0" "C_lower_teeth_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "C_lower_teeth_jnt.pim" "C_lower_teeth_jnt_scaleConstraint1.cpim";
connectAttr "C_lower_teeth_Ctrl.s" "C_lower_teeth_jnt_scaleConstraint1.tg[0].ts"
		;
connectAttr "C_lower_teeth_Ctrl.pm" "C_lower_teeth_jnt_scaleConstraint1.tg[0].tpm"
		;
connectAttr "C_lower_teeth_jnt_scaleConstraint1.w0" "C_lower_teeth_jnt_scaleConstraint1.tg[0].tw"
		;
connectAttr "R_lower_teeth_jnt_parentConstraint1.ctx" "R_lower_teeth_jnt.tx";
connectAttr "R_lower_teeth_jnt_parentConstraint1.cty" "R_lower_teeth_jnt.ty";
connectAttr "R_lower_teeth_jnt_parentConstraint1.ctz" "R_lower_teeth_jnt.tz";
connectAttr "R_lower_teeth_jnt_parentConstraint1.crx" "R_lower_teeth_jnt.rx";
connectAttr "R_lower_teeth_jnt_parentConstraint1.cry" "R_lower_teeth_jnt.ry";
connectAttr "R_lower_teeth_jnt_parentConstraint1.crz" "R_lower_teeth_jnt.rz";
connectAttr "R_lower_teeth_jnt_scaleConstraint1.csx" "R_lower_teeth_jnt.sx";
connectAttr "R_lower_teeth_jnt_scaleConstraint1.csy" "R_lower_teeth_jnt.sy";
connectAttr "R_lower_teeth_jnt_scaleConstraint1.csz" "R_lower_teeth_jnt.sz";
connectAttr "R_lower_teeth_jnt.ro" "R_lower_teeth_jnt_parentConstraint1.cro";
connectAttr "R_lower_teeth_jnt.pim" "R_lower_teeth_jnt_parentConstraint1.cpim";
connectAttr "R_lower_teeth_jnt.rp" "R_lower_teeth_jnt_parentConstraint1.crp";
connectAttr "R_lower_teeth_jnt.rpt" "R_lower_teeth_jnt_parentConstraint1.crt";
connectAttr "R_lower_teeth_jnt.jo" "R_lower_teeth_jnt_parentConstraint1.cjo";
connectAttr "R_lower_teeth_Ctrl.t" "R_lower_teeth_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "R_lower_teeth_Ctrl.rp" "R_lower_teeth_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "R_lower_teeth_Ctrl.rpt" "R_lower_teeth_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "R_lower_teeth_Ctrl.r" "R_lower_teeth_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "R_lower_teeth_Ctrl.ro" "R_lower_teeth_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "R_lower_teeth_Ctrl.s" "R_lower_teeth_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "R_lower_teeth_Ctrl.pm" "R_lower_teeth_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "R_lower_teeth_jnt_parentConstraint1.w0" "R_lower_teeth_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "R_lower_teeth_jnt.pim" "R_lower_teeth_jnt_scaleConstraint1.cpim";
connectAttr "R_lower_teeth_Ctrl.s" "R_lower_teeth_jnt_scaleConstraint1.tg[0].ts"
		;
connectAttr "R_lower_teeth_Ctrl.pm" "R_lower_teeth_jnt_scaleConstraint1.tg[0].tpm"
		;
connectAttr "R_lower_teeth_jnt_scaleConstraint1.w0" "R_lower_teeth_jnt_scaleConstraint1.tg[0].tw"
		;
connectAttr "tongue_01_jnt.s" "tongue_02_jnt.is";
connectAttr "tongue_02_jnt.s" "tongue_03_jnt.is";
connectAttr "tongue_03_jnt.s" "tongue_04_jnt.is";
connectAttr "tongue_04_jnt.s" "tongue_05_jnt.is";
connectAttr "tongue_05_jnt.s" "tongue_06_jnt.is";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":hyperGraphLayout.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":defaultObjectSet.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":defaultHardwareRenderGlobals.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":defaultHardwareRenderGlobals.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
dataStructure -fmt "raw" -as "name=faceConnectMarkerStructure:bool=faceConnectMarker:string[200]=faceConnectOutputGroups";
dataStructure -fmt "raw" -as "name=faceConnectOutputStructure:bool=faceConnectOutput:string[200]=faceConnectOutputAttributes:string[200]=faceConnectOutputGroups";
// End of teeth_and_tongue.ma
