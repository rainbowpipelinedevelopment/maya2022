#-*- coding: utf-8 -*-

import maya.cmds as cmds
from maya import OpenMayaUI as omui
import os, sys
from imp import reload

# -----------------------------------------------------------------
# Path
# -----------------------------------------------------------------
localpipe = os.getenv("LOCALPY")
myPath = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "Face_RIG")
script_path = os.path.join(myPath, "scripts")
scene_path = os.path.join(myPath, "scenes")
image_path = os.path.join(myPath, "icons")


# -----------------------------------------------------------------
# PySide
# -----------------------------------------------------------------
try:
    from shiboken2 import wrapInstance
except ImportError:
    from shiboken import wrapInstance

try:
    from PySide2.QtGui import QIcon
    from PySide2.QtWidgets import QWidget
except ImportError:
    from PySide.QtGui import QIcon, QWidget


# -----------------------------------------------------------------
# Funzioni da associare ai singoli pulsanti della UI
# -----------------------------------------------------------------
# FACE BASE
def sqs_locators_button_push(*args):    
    filePath = os.path.join(scene_path, "locs_sqs.ma")
    print("Attempting to import: {}".format(filePath))
    if cmds.file(filePath, query=True, exists=True):
        print("File exists, importing...")
        try:
            cmds.file(filePath, i=True, type="mayaAscii", ignoreVersion=True, mergeNamespacesOnClash=False, options="v=0")
            print("File imported: {}".format(filePath))
        except Exception as e:
            print("Error during import: {}".format(str(e)))
    else:
        print("File not found: {}".format(filePath))

def sqs_rig_button_push(*args):
    scriptPath = os.path.join(script_path, "face_sqs.py")
    print("Attempting to run script: {}".format(scriptPath))
    try:
        with open(scriptPath, 'r') as file:
            scriptContent = file.read()
            exec(scriptContent, globals())  # Usa globals() per mantenere il contesto globale
        print("Script executed successfully.")
    except Exception as e:
        print("Error executing script: {}".format(str(e)))

def eyes_locators_button_push(*args):
    filePath = os.path.join(scene_path, "locs_eye.ma")
    print("Attempting to import: {}".format(filePath))
    if cmds.file(filePath, query=True, exists=True):
        print("File exists, importing...")
        try:
            cmds.file(filePath, i=True, type="mayaAscii", ignoreVersion=True, mergeNamespacesOnClash=False, options="v=0")
            print("File imported: {}".format(filePath))
        except Exception as e:
            print("Error during import: {}".format(str(e)))
    else:
        print("File not found: {}".format(filePath))

def eyes_rig_button_push(*args):
    scriptPath = os.path.join(script_path, "eyes_rig.py")
    print("Attempting to run script: {}".format(scriptPath))

    try:
        with open(scriptPath, 'r') as file:
            scriptContent = file.read()
            exec(scriptContent, globals())  # Usa globals() per mantenere il contesto globale
        print("Script executed successfully.")
    except Exception as e:
        print("Error executing script: {}".format(str(e)))

def ears_locators_button_push(*args):
    filePath = os.path.join(scene_path, "locs_ears.ma")
    print("Attempting to import: {}".format(filePath))
    if cmds.file(filePath, query=True, exists=True):
        print("File exists, importing...")
        try:
            cmds.file(filePath, i=True, type="mayaAscii", ignoreVersion=True, mergeNamespacesOnClash=False, options="v=0")
            print("File imported: {}".format(filePath))
        except Exception as e:
            print("Error during import: {}".format(str(e)))
    else:
        print("File not found: {}".format(filePath))

def ears_rig_button_push(*args):
    scriptPath = os.path.join(script_path, "ears_rig.py")
    print("Attempting to run script: {}".format(scriptPath))

    try:
        with open(scriptPath, 'r') as file:
            scriptContent = file.read()
            exec(scriptContent, globals())  # Usa globals() per mantenere il contesto globale
        print("Script executed successfully.")
    except Exception as e:
        print("Error executing script: {}".format(str(e)))

def nose_locators_button_push(*args):
    filePath = os.path.join(scene_path, "locs_nose.ma")
    print("Attempting to import: {}".format(filePath))
    if cmds.file(filePath, query=True, exists=True):
        print("File exists, importing...")
        try:
            cmds.file(filePath, i=True, type="mayaAscii", ignoreVersion=True, mergeNamespacesOnClash=False, options="v=0")
            print("File imported: {}".format(filePath))
        except Exception as e:
            print("Error during import: {}".format(str(e)))
    else:
        print("File not found: {}".format(filePath))

def nose_rig_button_push(*args):
    scriptPath = os.path.join(script_path, "nose_rig.py")
    print("Attempting to run script: {}".format(scriptPath))

    try:
        with open(scriptPath, 'r') as file:
            scriptContent = file.read()
            exec(scriptContent, globals())  # Usa globals() per mantenere il contesto globale
        print("Script executed successfully.")
    except Exception as e:
        print("Error executing script: {}".format(str(e)))

def jaw_locators_button_push(*args):
    filePath = os.path.join(scene_path, "jaw_base.ma")
    print("Attempting to import: {}".format(filePath))
    if cmds.file(filePath, query=True, exists=True):
        print("File exists, importing...")
        try:
            cmds.file(filePath, i=True, type="mayaAscii", ignoreVersion=True, mergeNamespacesOnClash=False, options="v=0")
            print("File imported: {}".format(filePath))
        except Exception as e:
            print("Error during import: {}".format(str(e)))
    else:
        print("File not found: {}".format(filePath))

def jaw_rig_button_push(*args):
    scriptPath = os.path.join(script_path, "jaw_rig.py")
    print("Attempting to run script: {}".format(scriptPath))

    try:
        with open(scriptPath, 'r') as file:
            scriptContent = file.read()
            exec(scriptContent, globals())  # Usa globals() per mantenere il contesto globale
        print("Script executed successfully.")
    except Exception as e:
        print("Error executing script: {}".format(str(e)))

def innermouth_locators_button_push(*args):
    filePath = os.path.join(scene_path, "teeth_and_tongue.ma")
    print("Attempting to import: {}".format(filePath))
    if cmds.file(filePath, query=True, exists=True):
        print("File exists, importing...")
        try:
            cmds.file(filePath, i=True, type="mayaAscii", ignoreVersion=True, mergeNamespacesOnClash=False, options="v=0")
            print("File imported: {}".format(filePath))
        except Exception as e:
            print("Error during import: {}".format(str(e)))
    else:
        print("File not found: {}".format(filePath))

def innermouth_rig_button_push(*args):
    scriptPath = os.path.join(script_path, "inner_mouth_rig.py")
    print("Attempting to run script: {}".format(scriptPath))

    try:
        with open(scriptPath, 'r') as file:
            scriptContent = file.read()
            exec(scriptContent, globals())  # Usa globals() per mantenere il contesto globale
        print("Script executed successfully.")
    except Exception as e:
        print("Error executing script: {}".format(str(e)))


# FACE MODULES
def eyes_module_button_push(*args):
    scriptPath = os.path.join(script_path, "eyes_module.py")
    print("Attempting to run script: {}".format(scriptPath))

    try:
        with open(scriptPath, 'r') as file:
            scriptContent = file.read()
            exec(scriptContent, globals())  # Usa globals() per mantenere il contesto globale
        print("Script executed successfully.")
    except Exception as e:
        print("Error executing script: {}".format(str(e)))

def brows_button_push(*args):
    scriptPath = os.path.join(script_path, "brows_build.py")
    print("Attempting to run script: {}".format(scriptPath))

    try:
        with open(scriptPath, 'r') as file:
            scriptContent = file.read()
            exec(scriptContent, globals())  # Usa globals() per mantenere il contesto globale
        print("Script executed successfully.")
    except Exception as e:
        print("Error executing script: {}".format(str(e)))

def mouth_module_button_push(*args):
    scriptPath = os.path.join(script_path, "mouth_module.py")
    print("Attempting to run script: {}".format(scriptPath))

    try:
        with open(scriptPath, 'r') as file:
            scriptContent = file.read()
            exec(scriptContent, globals())  # Usa globals() per mantenere il contesto globale
        print("Script executed successfully.")
    except Exception as e:
        print("Error executing script: {}".format(str(e)))

def cheeks_button_push(*args):
    scriptPath = os.path.join(script_path, "cheeks_build.py")
    print("Attempting to run script: {}".format(scriptPath))

    try:
        with open(scriptPath, 'r') as file:
            scriptContent = file.read()
            exec(scriptContent, globals())  # Usa globals() per mantenere il contesto globale
        print("Script executed successfully.")
    except Exception as e:
        print("Error executing script: {}".format(str(e)))


# UTILITIES
def import_export_ctrl_shapes_button_push(*args):
    scriptPath = os.path.join(script_path, "import_export_ctrl_shapes.py")
    print("Attempting to run script: {}".format(scriptPath))

    try:
        with open(scriptPath, 'r') as file:
            scriptContent = file.read()
            exec(scriptContent, globals())  # Usa globals() per mantenere il contesto globale
        print("Script executed successfully.")
    except Exception as e:
        print("Error executing script: {}".format(str(e)))

def mirror_skinning_driver_curves_button_push(*args):
    scriptPath = os.path.join(script_path, "mirror_skinning_driver_curves.py")
    print("Attempting to run script: {}".format(scriptPath))

    try:
        with open(scriptPath, 'r') as file:
            scriptContent = file.read()
            exec(scriptContent, globals())  # Usa globals() per mantenere il contesto globale
        print("Script executed successfully.")
    except Exception as e:
        print("Error executing script: {}".format(str(e)))

def link_wiki(*args):
    import webbrowser
    pathToHelp="https://sites.google.com/rbw-cgi.it/rbw-rigging-team/home"
    webbrowser.open(pathToHelp) 


# -----------------------------------------------------------------
# UI
# -----------------------------------------------------------------
def main():
    winWidth = 280   
    """Funzione che lancia la UI dei moduli del facciale."""
    if cmds.window("FaceRigUI", q=True, exists=True):
         cmds.deleteUI("FaceRigUI")
    ww = cmds.window("FaceRigUI" ,t="Rainbow CGI - Face Rig",  mxb=False, width=250, height=50)
    
    # Get a pointer and convert it to Qt Widget object
    qw = omui.MQtUtil.findWindow(ww)
    widget = wrapInstance(int(qw), QWidget)

    # Create a QIcon object
    iconpath = os.path.join(image_path, "RainbowCGI_icona.ico")

    # Assign the icon
    icon = QIcon(iconpath)
    widget.setWindowIcon(icon)

    # Main Layout
    col = cmds.columnLayout(adjustableColumn=True, bgc=(0.225, 0.225, 0.225))
    
    # Carica l'immagine del Face Rig
    img = os.path.join(image_path, "Face_Icon.png")
    cmds.iconTextButton(style="iconOnly", image1=img, w=160, h=160, useAlpha=True, p=col)
    cmds.text("FACE RIG", h=20, font='boldLabelFont')
    cmds.setParent("..")

    # Face Base
    cmds.frameLayout(label="Face Base", collapsable=False, collapse=False, p=col, bgc=[0.0, 0.1, 0.6])
    cmds.columnLayout(adj=True, rowSpacing=5, columnWidth=250)
    cmds.text(l="", h=3)
    cmds.rowLayout(numberOfColumns=2, adjustableColumn=2, columnAttach2=['both', 'both'], columnOffset2=[5,5])
    cmds.button(l="Locators", command=sqs_locators_button_push, h=30, bgc=[0.7, 0.7, 0.7], w=80)
    cmds.button(l="SQS RIG", command=sqs_rig_button_push, h=30, bgc=[0.3, 0.3, 0.3], w=140)
    cmds.setParent("..")
    cmds.rowLayout(numberOfColumns=2, adjustableColumn=2, columnAttach2=['both', 'both'], columnOffset2=[5,5])
    cmds.button(l="Locators", command=eyes_locators_button_push, h=30, bgc=[0.7, 0.7, 0.7], w=80)
    cmds.button(l="EYES RIG", command=eyes_rig_button_push, h=30, bgc=[0.3, 0.3, 0.3], w=140)
    cmds.setParent("..")
    cmds.rowLayout(numberOfColumns=2, adjustableColumn=2, columnAttach2=['both', 'both'], columnOffset2=[5,5])
    cmds.button(l="Locators", command=ears_locators_button_push, h=30, bgc=[0.7, 0.7, 0.7], w=80)
    cmds.button(l="EARS RIG", command=ears_rig_button_push, h=30, bgc=[0.3, 0.3, 0.3], w=140)
    cmds.setParent("..")
    cmds.rowLayout(numberOfColumns=2, adjustableColumn=2, columnAttach2=['both', 'both'], columnOffset2=[5,5])
    cmds.button(l="Locators", command=nose_locators_button_push, h=30, bgc=[0.7, 0.7, 0.7], w=80)
    cmds.button(l="NOSE RIG", command=nose_rig_button_push, h=30, bgc=[0.3, 0.3, 0.3], w=140)
    cmds.setParent("..")
    cmds.rowLayout(numberOfColumns=2, adjustableColumn=2, columnAttach2=['both', 'both'], columnOffset2=[5,5])
    cmds.button(l="Locators", command=jaw_locators_button_push, h=30, bgc=[0.7, 0.7, 0.7], w=80)
    cmds.button(l="JAW RIG", command=jaw_rig_button_push, h=30, bgc=[0.3, 0.3, 0.3], w=140)
    cmds.setParent("..")
    cmds.rowLayout(numberOfColumns=2, adjustableColumn=2, columnAttach2=['both', 'both'], columnOffset2=[5,5])
    cmds.button(l="Locators", command=innermouth_locators_button_push, h=30, bgc=[0.7, 0.7, 0.7], w=80)
    cmds.button(l="INNER MOUTH RIG", command=innermouth_rig_button_push, h=30, bgc=[0.3, 0.3, 0.3], w=140)
    cmds.setParent("..")
    cmds.text(l="", h=3)     
    cmds.setParent("..")

    # Face Modules
    cmds.frameLayout(label="Face Modules", collapsable=False, collapse=False, p=col, bgc=[0.0, 0.3, 0.6])
    cmds.columnLayout(adj=True, co=("both", 6), rowSpacing=7, columnWidth=250)
    cmds.text(l="", h=1)     
    cmds.button(w=winWidth, l="| Eyes Module |",command=eyes_module_button_push, h=30, bgc=[0.3, 0.3, 0.3])
    cmds.button(w=winWidth, l="| Brows Module |", command=brows_button_push, h=30, bgc=[0.3, 0.3, 0.3])
    cmds.button(w=winWidth, l="| Mouth Module |", command=mouth_module_button_push, h=30, bgc=[0.3, 0.3, 0.3])
    cmds.button(w=winWidth, l="| Cheeks Module |", command=cheeks_button_push, h=30, bgc=[0.3, 0.3, 0.3])
    cmds.text(l="", h=1)     
    cmds.setParent("..")

    # Utilities
    cmds.frameLayout(label="Utilities", collapsable=True, collapse=True, p=col, bgc=[0.0, 0.5, 0.6])
    cmds.columnLayout(adj=True, co=("both", 6), rowSpacing=7, columnWidth=250)
    cmds.text(l="", h=1)     
    cmds.button(w=winWidth, l="| Import/Export Ctrl Shapes |", command = import_export_ctrl_shapes_button_push, h=30, bgc=[0.8, 0.8, 0.6])
    cmds.button(w=winWidth, l="| Mirrora influenze curve driver |", command = mirror_skinning_driver_curves_button_push, h=30, bgc=[0.8, 0.8, 0.6])
    cmds.text(l="", h=1)     
    cmds.setParent("..")

    # WIKI
    cmds.frameLayout(label="WIKI", labelVisible=False, p=col, bgc=(0.4, 0.4, 0.4))
    col_wiki = cmds.columnLayout(adj=True, co=("both", 6), rowSpacing=7, columnWidth=250)
    cmds.text(l="", h=1)     
    cmds.button(h=30, l="| WIKI |", p=col_wiki, c=link_wiki, bgc=(0.2, 0.2, 0.2), annotation="Hai qualche dubbio? Vai alla Wiki di rigging!")
    cmds.text(l="", h=1)     
    cmds.setParent("..")

    cmds.showWindow()
    
