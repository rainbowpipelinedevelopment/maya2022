import maya.cmds as cmds

def main_rig_process():
    def create_and_parent_joints(locator_names):
        joint_names = {}
        for loc in locator_names:
            position = cmds.xform(loc, query=True, worldSpace=True, translation=True)
            joint_name = loc.replace("LOC_", "") + "_jnt"
            cmds.select(clear=True)
            joint_names[loc] = cmds.joint(name=joint_name, position=position)
            temp_constraint = cmds.orientConstraint(loc, joint_name)
            cmds.delete(temp_constraint)
        cmds.parent(joint_names[locator_names[1]], joint_names[locator_names[0]])
        cmds.select(clear=True)

    def create_control_and_group(joint_name, control_name, parent):
        cmds.select(clear=True)
        control = cmds.circle(name=control_name, normal=(0, 1, 0), radius=1.5)[0]
        cmds.delete(cmds.parentConstraint(joint_name, control))
        grp = cmds.group(name=control_name + "_Grp", empty=True)
        cmds.delete(cmds.parentConstraint(control, grp))
        cmds.parent(control, grp)
        if parent:
            cmds.parentConstraint(control, joint_name, maintainOffset=True)
        return control, grp

    def create_controls_for_joints(joint_names):
        control_dict = {}
        for jnt in joint_names:
            control_name = jnt.replace("_jnt", "_Ctrl_Static")
            control, grp = create_control_and_group(jnt, control_name, parent=True)
            control_dict[jnt] = (control, grp)
        cmds.parent(control_dict[joint_names[1]][1], control_dict[joint_names[0]][0])
        return control_dict
    
    def create_controls(joint_names):
        control_dict = {}
        for jnt in joint_names:
            control_name = jnt.replace("_jnt", "_Ctrl")
            control, grp = create_control_and_group(jnt, control_name, parent=True)
            control_dict[jnt] = (control, grp)

    def organize_jaw_chin_rig():
        # Existing code for grouping and setting attributes
        jaw_chin_joints_grp = cmds.group("Jaw_jnt", "Chin_jnt", name="Jaw_Chin_Joints_Grp")
        cmds.setAttr(jaw_chin_joints_grp + ".visibility", 0)
        jaw_chin_controls_grp = cmds.group("Jaw_Ctrl_Static_Grp", "Chin_Ctrl_Static_Grp", name="Jaw_Chin_Controls_Grp")
        cmds.setAttr(jaw_chin_controls_grp + ".visibility", 0)
        jaw_chin_rig_grp = cmds.group(jaw_chin_joints_grp, jaw_chin_controls_grp, name="Jaw_Chin_RIG_Grp")
        cmds.parent("Chin_Ctrl_Grp", "Jaw_Ctrl")
        cmds.parent("Chin_Ctrl_Static_Grp", "Jaw_Ctrl_Static")
        cmds.parent("Jaw_Ctrl_Grp", "Head_SQSLow_03_jnt")
        cmds.parent("Jaw_Chin_RIG_Grp", "Face_RIG_Grp")

        # Add a float attribute "square" limited between 0 and 10 to the Jaw_Ctrl
        cmds.addAttr("Jaw_Ctrl", longName="square", attributeType="float", min=0, max=10, keyable=True)

        # Add an enum attribute with options 'on' and 'off' to the Jaw_Ctrl
        cmds.addAttr("Jaw_Ctrl", longName="inner_mouth_VIS", attributeType="enum", enumName="off:on")
        cmds.setAttr("Jaw_Ctrl.inner_mouth_VIS", e=True, keyable=False, channelBox=True)
        
        # Lock and hide the visibility attributes for "Jaw_Ctrl" and "Chin_Ctrl"
        for ctrl in ["Jaw_Ctrl", "Chin_Ctrl"]:
            cmds.setAttr(ctrl + ".visibility", lock=True, keyable=False, channelBox=False)

        # Limit the rotate X to -12 for the control "Jaw_Ctrl"
        cmds.transformLimits("Jaw_Ctrl", rx=(-12, 0), erx=(1, 0))


    def replace_and_remove(source_ctrl, target_ctrl):
        # Verifica se i due controlli esistono
        if not cmds.objExists(source_ctrl):
            cmds.warning("Il controllo sorgente '{}' non esiste.".format(source_ctrl))
            return
        
        if not cmds.objExists(target_ctrl):
            cmds.warning("Il controllo di destinazione '{}' non esiste.".format(target_ctrl))
            return

        # Ottieni e elimina le shape esistenti nel controllo di destinazione
        target_shapes = cmds.listRelatives(target_ctrl, shapes=True)
        if target_shapes:
            cmds.delete(target_shapes)

        # Ottieni la shape del controllo sorgente
        source_shapes = cmds.listRelatives(source_ctrl, shapes=True)
        if not source_shapes:
            cmds.warning("Il controllo sorgente '{}' non ha shape associate.".format(source_ctrl))
            return

        # Sposta ogni shape del controllo sorgente al controllo di destinazione
        for source_shape in source_shapes:
            cmds.parent(source_shape, target_ctrl, r=True, shape=True)

        # Trova il gruppo padre del controllo sorgente, se esiste
        source_parent = cmds.listRelatives(source_ctrl, parent=True)

        # Elimina il controllo sorgente
        cmds.delete(source_ctrl)

        # Elimina anche il gruppo padre del controllo sorgente, se non ha altri figli
        if source_parent and not cmds.listRelatives(source_parent[0], children=True):
            cmds.delete(source_parent)

        cmds.warning("Shape sostituita con successo da '{}' a '{}'.".format(source_ctrl, target_ctrl))


    def process_multiple_pairs(control_pairs):
        # Esegui la funzione replace_and_remove per ogni coppia
        for source, target in control_pairs:
            replace_and_remove(source, target)

    control_pairs = [
        ("Jaw_Ctrl_REF", "Jaw_Ctrl"),
        ("Chin_Ctrl_REF", "Chin_Ctrl"),
        # Altre coppie qui se necessario
    ]

    # Processa le coppie di controlli
    process_multiple_pairs(control_pairs)
    def connect_controls():
        """Connette i controlli 'Head_*SQS_Ctrl' con '*SQS_Ctrl'."""
        jaw_controls = ["Jaw_Ctrl", "Chin_Ctrl"]
        jaw_static_controls = ["Jaw_Ctrl_Static", "Chin_Ctrl_Static"]

        for head_ctrl, base_ctrl in zip(jaw_controls, jaw_static_controls):
            # Connette le trasformazioni di translate, rotate e scale
            cmds.connectAttr(head_ctrl + ".translate", base_ctrl + ".translate", f=True)
            cmds.connectAttr(head_ctrl + ".rotate", base_ctrl + ".rotate", f=True)
            cmds.connectAttr(head_ctrl + ".scale", base_ctrl + ".scale", f=True)
        for source, target in control_pairs:
            replace_and_remove(source, target)
        if  cmds.objExists("Jaw_Controls_GRP"):
            cmds.delete("Jaw_Controls_GRP")
        if  cmds.objExists("Jaw_LOCS_GRP"):
            cmds.delete("Jaw_LOCS_GRP")

    def clean_rig():
        # Elenco dei joint e dei controlli coinvolti
        joints = ["Jaw_jnt", "Chin_jnt"]
        jaw_static = "Jaw_Ctrl_Static"
        chin_static = "Chin_Ctrl_Static"

        # Rompe i parent constraint di "Jaw_jnt" e "Chin_jnt"
        for joint in joints:
            # Trova e elimina tutti i parent constraints collegati a ciascun joint
            constraints = cmds.listRelatives(joint, type='parentConstraint')
            if constraints:
                cmds.delete(constraints)

        # Imparenta "Jaw_jnt" a "Jaw_Ctrl_Static"
        cmds.parent("Jaw_jnt", jaw_static)

        # Imparenta "Chin_jnt" a "Chin_Ctrl_Static"
        cmds.parent("Chin_jnt", chin_static)

        # Imposta a 0 la visibilità dei joint "Jaw_jnt" e "Chin_jnt"
        for joint in joints:
            cmds.setAttr(joint + ".visibility", 0)

        # Elimina il gruppo "Jaw_Chin_Joints_Grp" se esiste
        if cmds.objExists("Jaw_Chin_Joints_Grp"):
            cmds.delete("Jaw_Chin_Joints_Grp")


    # Creazione e imparentamento dei Joint
    locator_names = ["LOC_Jaw", "LOC_Chin"]
    create_and_parent_joints(locator_names)

    # Creazione dei controlli per i Joint
    joint_names = ["Jaw_jnt", "Chin_jnt"]
    create_controls_for_joints(joint_names)
    create_controls(joint_names)
    
    # Processa le coppie di controlli
    process_multiple_pairs(control_pairs)

    # Organizzazione del rig di Jaw e Chin
    organize_jaw_chin_rig()
    connect_controls()
    clean_rig()

    # Funzioni di Utility per creazione set
    def create_joints_set(module, rigpart, jnt_list):
        # Creates a set containing the joints for skinning
        joints_for_skin = []
        joints_for_skin.extend(jnt_list)
        
        if not cmds.objExists("FACE_MODULES_Joints"):
            module_joints_set = cmds.sets(empty=True, name=("FACE_MODULES_Joints"))
        else:
            module_joints_set = "FACE_MODULES_Joints"

        # Controlla se il set del modulo di rig dei joints esiste, altrimenti crealo
        if not cmds.objExists("{}_Joints".format(module)): # 'HeadEyes' -'HeadMouth' - 'HeadSquash'
            section_joints_set = cmds.sets(empty=True, name=("{}_Joints".format(module)))
            # Parenta il set della parte di rig dentro al set globale
            cmds.sets(section_joints_set, edit=True, forceElement=module_joints_set)
        else:
            section_joints_set = "{}_Joints".format(module)

        # Controlla se il set della parte di rig dei joints esiste, altrimenti crealo
        if not cmds.objExists("{}_jointsForSkin".format(rigpart)):
            rigpart_joints_set = cmds.sets(empty=True, name=("{}_jointsForSkin".format(rigpart)))
            # Parenta il set della parte di rig dentro al set globale
            cmds.sets(rigpart_joints_set, edit=True, forceElement=section_joints_set)
        else:
            rigpart_joints_set = "{}_jointsForSkin".format(rigpart)


        for jnt in joints_for_skin:
            cmds.sets(jnt, edit=True, forceElement=rigpart_joints_set)

    def create_controls_set(module, rigpart, ctrl_list):
        # Crea il set contenente i controlli creati
        if not cmds.objExists("FACE_MODULES_Controls"):
            module_controls_set = cmds.sets(empty=True, name=("FACE_MODULES_Controls"))
        else:
            module_controls_set = "FACE_MODULES_Controls"

        if not cmds.objExists("{}_controls".format(module)):
            module_ctrl_set = cmds.sets(empty=True, name=("{}_controls".format(module)))
            # Parenta il set del modulo dentro al set globale
            cmds.sets(module_ctrl_set, edit=True, forceElement=module_controls_set)
        else:
            module_ctrl_set = "{}_controls".format(module)
            
        if not cmds.objExists("{}_controls".format(rigpart)):
            rigpart_ctrl_set = cmds.sets(empty=True, name=("{}_controls".format(rigpart)))
            # Parenta il set del modulo dentro al set globale
            cmds.sets(rigpart_ctrl_set, edit=True, forceElement=module_ctrl_set)
        else:
            rigpart_ctrl_set = "{}_controls".format(rigpart)

        for ctrl in ctrl_list:
            cmds.sets(ctrl, edit=True, forceElement=rigpart_ctrl_set)

        return module_ctrl_set

    def rename_ctrl_shapes(set_ctrl):
        """Data una selezione di controlli, rinomina le shapes in base al nome del controllo."""
        cmds.select(clear=True)
        cmds.select(set_ctrl)
        sel = cmds.ls(sl=True)
        cmds.select(clear=True)
        
        for ctrl in sel:
            shapes = cmds.listRelatives(ctrl, shapes=True, pa=True)
            for shape in shapes:
                new_name = "{}Shape".format(ctrl)
                cmds.rename(shape, new_name)

        print("### Le shapes dei controlli sono state rinominate correttamente. ###")

    create_joints_set("HeadMouth", "Jaw", ["Jaw_jnt", "Chin_jnt"])
    controls_set = create_controls_set("HeadMouth", "Jaw", ["Jaw_Ctrl", "Chin_Ctrl"])
    rename_ctrl_shapes(controls_set)

# Esecuzione della funzione principale
main_rig_process()
