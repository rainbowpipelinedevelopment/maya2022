#-*- coding: utf-8 -*-

from maya import cmds, OpenMaya, mel
import math, os, sys
from imp import reload
import depts.rigging.tools.generic.Face_RIG.scripts.matrix_module as mtx
reload(mtx)

def main_rig_process():
    def create_joint_chain_from_locators(locator_names):
        joint_names = []
        # Creazione dei Joint in base ai locator
        for loc in locator_names:
            position = cmds.xform(loc, query=True, worldSpace=True, translation=True)
            joint_name = loc.replace("LOC_", "") + "_jnt"
            cmds.select(clear=True)
            created_joint = cmds.joint(name=joint_name, position=position)
            joint_names.append(created_joint)

        # Applica aim constraints temporanei per orientare i joint
        for i in range(len(joint_names) - 1):
            aim_constraint = cmds.aimConstraint(joint_names[i + 1], joint_names[i], aimVector=(1, 0, 0), upVector=(0, 1, 0), worldUpType="vector", worldUpVector=(0, 1, 0))
            cmds.delete(aim_constraint)

        # Orienta l'ultimo joint verso il penultimo
        if len(joint_names) > 1:
            aim_constraint = cmds.aimConstraint(joint_names[-2], joint_names[-1], aimVector=(-1, 0, 0), upVector=(0, 1, 0), worldUpType="vector", worldUpVector=(0, 1, 0))
            cmds.delete(aim_constraint)

        # Freeze della rotazione e aggiungi rotazione di 90 gradi su Y
        for jnt in joint_names:
            cmds.makeIdentity(jnt, apply=True, rotate=True)
            cmds.rotate(0, 90, 0, jnt, relative=True)
            cmds.makeIdentity(jnt, apply=True, rotate=True)

        return joint_names

    def create_and_orient_nostril_joints(locator_names):
        joint_names = []
        for loc in locator_names:
            position = cmds.xform(loc, query=True, worldSpace=True, translation=True)
            joint_name = loc.replace("LOC_", "") + "_jnt"
            cmds.select(clear=True)
            created_joint = cmds.joint(name=joint_name, position=position)
            joint_names.append(created_joint)

            # Se il joint è "R_Nostril_jnt", applica una rotazione specifica e freeze le trasformazioni
            if joint_name == "R_Nostril_jnt":
                cmds.rotate(0, 180, 0, joint_name, relative=True)
                cmds.makeIdentity(joint_name, apply=True, rotate=True)

        return joint_names

    def create_control_and_group(joint_name, control_name, parent):
        cmds.select(clear=True)
        control = cmds.circle(name=control_name, normal=(0, 1, 0), radius=1.5)[0]
        cmds.delete(cmds.parentConstraint(joint_name, control))

        # Creazione del gruppo Matrix
        matrix_grp_name = control_name + "_Matrix_Grp"
        matrix_grp = cmds.group(name=matrix_grp_name, empty=True)
        cmds.delete(cmds.parentConstraint(control, matrix_grp))
        cmds.parent(control, matrix_grp)

        # Creazione del gruppo principale
        grp_name = control_name + "_Grp"
        grp = cmds.group(name=grp_name, empty=True)
        cmds.delete(cmds.parentConstraint(matrix_grp, grp))
        cmds.parent(matrix_grp, grp)

        if parent:
            cmds.parentConstraint(control, joint_name, maintainOffset=True)
            cmds.scaleConstraint(control, joint_name, maintainOffset=True)
        return control, matrix_grp, grp

    def create_controls_for_joints(joint_names):
        control_dict = {}
        for jnt in joint_names:
            control_static_name = jnt.replace("_jnt", "_Ctrl_Static")
            control_static, matrix_grp_static, grp_static = create_control_and_group(jnt, control_static_name, parent=True)
            control_dict[jnt] = {"static": (control_static, matrix_grp_static, grp_static)}

            control_name = jnt.replace("_jnt", "_Ctrl")
            control, matrix_grp, grp = create_control_and_group(jnt, control_name, parent=False)
            control_dict[jnt]["dynamic"] = (control, matrix_grp, grp)

        parent_controls(control_dict)
        return control_dict


    def parent_controls(control_dict):
        ear_joints = ["L_Ear_jnt", "L_EarLow_jnt", "L_EarTop_jnt", "R_Ear_jnt", "R_EarLow_jnt", "R_EarTop_jnt"]
        for ear in ear_joints:
            if ear in control_dict:
                top = ear.replace("Ear", "EarTop")
                low = ear.replace("Ear", "EarLow")
                if top in control_dict and low in control_dict:
                    cmds.parent(control_dict[top]["dynamic"][1], control_dict[ear]["dynamic"][0])
                    cmds.parent(control_dict[low]["dynamic"][1], control_dict[ear]["dynamic"][0])
                top_static = ear.replace("Ear", "EarTop")
                low_static = ear.replace("Ear", "EarLow")
                if top_static in control_dict and low_static in control_dict:
                    cmds.parent(control_dict[top_static]["static"][1], control_dict[ear]["static"][0])
                    cmds.parent(control_dict[low_static]["static"][1], control_dict[ear]["static"][0])

    def organize_nose_rig():
        nose_joints_grp = cmds.group("NoseHigh_jnt", "NoseMid_jnt", "Nose_jnt", "L_Nostril_jnt", "R_Nostril_jnt", "NoseLow_jnt", name="Nose_Joints_Grp")
        cmds.setAttr(nose_joints_grp + ".visibility", 0)
        nose_controls_static_grp = cmds.group("NoseHigh_Ctrl_Static_Grp", "NoseMid_Ctrl_Static_Grp", "Nose_Ctrl_Static_Grp", "NoseLow_Ctrl_Static_Grp", name="Nose_Controls_Static_Grp")
        cmds.setAttr(nose_controls_static_grp + ".visibility", 0)
        ears_rig_grp = cmds.group(nose_joints_grp, nose_controls_static_grp, name="Nose_RIG_Grp")
        cmds.parent("NoseHigh_Ctrl_Grp", "Head_SQSLow_01_jnt")
        cmds.parent("NoseMid_Ctrl_Grp", "Head_SQSLow_03_jnt")
        cmds.parent("Nose_Ctrl_Grp", "Head_SQSLow_03_jnt")
        cmds.parent("LOC_Nose", "Nose_Controls_Static_Grp")
        cmds.addAttr("Nose_Ctrl", longName="follow", attributeType="float", min=0, max=1, keyable=True)
        cmds.addAttr("L_Nostril_Ctrl", longName="follow", attributeType="float", min=0, max=1, keyable=True)
        cmds.addAttr("R_Nostril_Ctrl", longName="follow", attributeType="float", min=0, max=1, keyable=True)
        
        if cmds.objExists("LOCS_Nose_Grp"):
            cmds.delete("LOCS_Nose_Grp")
            
        # Lock and hide the visibility attributes for Nose Controls
        for ctrl in ["Nose_Ctrl", "NoseHigh_Ctrl", "NoseMid_Ctrl", "L_Nostril_Ctrl", "R_Nostril_Ctrl"]:
            cmds.setAttr(ctrl + ".visibility", lock=True, keyable=False, channelBox=False)

    def replace_and_remove(source_ctrl, target_ctrl):
        # Verifica se i due controlli esistono
        if not cmds.objExists(source_ctrl):
            cmds.warning("Il controllo sorgente '{}' non esiste.".format(source_ctrl))
            return
        
        if not cmds.objExists(target_ctrl):
            cmds.warning("Il controllo di destinazione '{}' non esiste.".format(target_ctrl))
            return

        # Ottieni e elimina le shape esistenti nel controllo di destinazione
        target_shapes = cmds.listRelatives(target_ctrl, shapes=True)
        if target_shapes:
            cmds.delete(target_shapes)

        # Ottieni la shape del controllo sorgente
        source_shapes = cmds.listRelatives(source_ctrl, shapes=True)
        if not source_shapes:
            cmds.warning("Il controllo sorgente '{}' non ha shape associate.".format(source_ctrl))
            return

        # Sposta ogni shape del controllo sorgente al controllo di destinazione
        for source_shape in source_shapes:
            cmds.parent(source_shape, target_ctrl, r=True, shape=True)

        # Trova il gruppo padre del controllo sorgente, se esiste
        source_parent = cmds.listRelatives(source_ctrl, parent=True)

        # Elimina il controllo sorgente
        cmds.delete(source_ctrl)

        # Elimina anche il gruppo padre del controllo sorgente, se non ha altri figli
        if source_parent and not cmds.listRelatives(source_parent[0], children=True):
            cmds.delete(source_parent)

        cmds.warning("Shape sostituita con successo da '{}' a '{}'.".format(source_ctrl, target_ctrl))

    def process_multiple_pairs(control_pairs):
        for source, target in control_pairs:
            replace_and_remove(source, target)

    # Qui inizia il corpo principale della funzione main_rig_process
    nose_locator_names = ["LOC_NoseHigh", "LOC_NoseMid", "LOC_Nose"]
    nose_joints = create_joint_chain_from_locators(nose_locator_names)

    nostril_locator_names = ["LOC_L_Nostril", "LOC_R_Nostril", "LOC_NoseLow"]
    nostril_joints = create_and_orient_nostril_joints(nostril_locator_names)

    all_joints = nose_joints + nostril_joints
    controls_dict = create_controls_for_joints(all_joints)

    control_pairs = [
        ("Nose_High_Ctrl_REF", "NoseHigh_Ctrl"),
        ("Nose_Mid_Ctrl_REF", "NoseMid_Ctrl"),
        ("Nose_Ctrl_REF", "Nose_Ctrl"),
        ("L_Nostril_Ctrl_REF", "L_Nostril_Ctrl"),
        ("R_Nostril_Ctrl_REF", "R_Nostril_Ctrl"),
    ]

    # Posizionamento dei gruppi padre dei nostril sotto "Nose_Ctrl"
    cmds.parent("L_Nostril_Ctrl_Grp", "Nose_Ctrl")
    cmds.parent("R_Nostril_Ctrl_Grp", "Nose_Ctrl")
    cmds.parent("NoseLow_Ctrl_Grp", "Nose_Ctrl")

    # Posizionamento dei gruppi padre dei nostril statici sotto "Nose_Static_Ctrl"
    cmds.parent("L_Nostril_Ctrl_Static_Grp", "Nose_Ctrl_Static")
    cmds.parent("R_Nostril_Ctrl_Static_Grp", "Nose_Ctrl_Static")
    cmds.parent("NoseLow_Ctrl_Static_Grp", "Nose_Ctrl_Static")
    
    
    def connect_ctrls():
        # Dizionario che mappa i gruppi sorgente ai gruppi destinazione
        group_mappings = {
            "Nose_Ctrl_Static_Matrix_Grp": "Nose_Ctrl_Matrix_Grp",
            "NoseMid_Ctrl_Static_Matrix_Grp": "NoseMid_Ctrl_Matrix_Grp",
            "L_Nostril_Ctrl_Static_Matrix_Grp": "L_Nostril_Ctrl_Matrix_Grp",
            "R_Nostril_Ctrl_Static_Matrix_Grp": "R_Nostril_Ctrl_Matrix_Grp",
            "Nose_Ctrl": "Nose_Ctrl_Static",
            "NoseHigh_Ctrl": "NoseHigh_Ctrl_Static",
            "NoseMid_Ctrl": "NoseMid_Ctrl_Static",
            "L_Nostril_Ctrl": "L_Nostril_Ctrl_Static",
            "R_Nostril_Ctrl": "R_Nostril_Ctrl_Static"
        }

        for source, target in group_mappings.items():
            # Connetti le trasformazioni di translate, rotate e scale
            cmds.connectAttr(source + ".translate", target + ".translate", f=True)
            cmds.connectAttr(source + ".rotate", target + ".rotate", f=True)
            cmds.connectAttr(source + ".scale", target + ".scale", f=True)
          
        
    
    def matrici_naso(): 
        selezione, valori = self.trasforma_liste(lista_valori, lista_grp)
        
        for lista_selezione, valori_rotate in zip(selezione, valori):
            print("lista_selezione: {}".format(lista_selezione))
            print("valori_rotate: {}".format(valori_rotate))
            matrice =  mtx.MatrixConstraint(lista_selezione, r=valori_rotate).matrix_constraint()
    
    
    def create_matrix_system():
        nose_list = [['NoseHigh_Ctrl_Static', 'Nose_Ctrl_Static_Matrix_Grp', 'Nose_Ctrl_Static_Grp'],
                     ['MouthMaster_Driver_Ctrl', 'Nose_Ctrl_Static_Matrix_Grp', 'Nose_Ctrl_Static_Grp']]
        nose_mid_list = [['NoseHigh_Ctrl_Static', 'NoseMid_Ctrl_Static_Matrix_Grp', 'NoseMid_Ctrl_Static_Grp'],
                         ['Nose_Ctrl_Static', 'NoseMid_Ctrl_Static_Matrix_Grp', 'NoseMid_Ctrl_Static_Grp']]
        
        # Creazione matrici
        mtx_nose = mtx.MatrixConstraint(nose_list, t=[(1, 1, 1), (0.5, 0.5, 0.5)], r=[(1, 1, 1), (0.5, 0.5, 0.5)]).matrix_constraint()
        mtx_nose_mid = mtx.MatrixConstraint(nose_mid_list, t=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)], r=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)]).matrix_constraint()
             
        # Nomi dei nodi PlusMinusAverage che si prevede siano creati nello script
        pls_nodes = {
            "translate": "PLS_Nose_Ctrl_Static_Matrix_Grp_Translate",
            "rotate": "PLS_Nose_Ctrl_Static_Matrix_Grp_Rotate",
        }

        # Crea e configura i nodi multiplyDivide per ogni trasformazione
        for transform, pls_node in pls_nodes.items():
            # Crea un nuovo nodo multiplyDivide
            md_node = cmds.createNode("multiplyDivide", name="MD_Nose_Ctrl_Static_Matrix_Grp_" + transform.capitalize())

            # Assumi che l'output dei nodi PlusMinusAverage sia l'input1 dei nodi multiplyDivide
            cmds.connectAttr(pls_node + ".output3D", md_node + ".input1", f=True)

            # Imposta input2x, input2y, input2z al valore dell'attributo "follow" del controllo "Nose_Ctrl"
            for axis in ["X", "Y", "Z"]:
                cmds.connectAttr("Nose_Ctrl.follow", md_node + ".input2" + axis, f=True)

            # Connetti l'output del nodo multiplyDivide al gruppo "Nose_Ctrl_Static_Matrix_Grp"
            cmds.connectAttr(md_node + ".output", "Nose_Ctrl_Static_Matrix_Grp." + transform, f=True)
             
        if cmds.objExists("Nose_Controls_Grp_REF"):
            cmds.delete("Nose_Controls_Grp_REF")
        if cmds.objExists("Nose_RIG_Grp") and cmds.objExists("Face_RIG_Grp"):
            cmds.parent("Nose_RIG_Grp", "Face_RIG_Grp")


    process_multiple_pairs(control_pairs)
    organize_nose_rig()
    connect_ctrls()
    create_matrix_system()

    # Funzioni di Utility per creazione set
    def create_joints_set(module, rigpart, jnt_list):
        # Creates a set containing the joints for skinning
        joints_for_skin = []
        joints_for_skin.extend(jnt_list)
        
        if not cmds.objExists("FACE_MODULES_Joints"):
            module_joints_set = cmds.sets(empty=True, name=("FACE_MODULES_Joints"))
        else:
            module_joints_set = "FACE_MODULES_Joints"

        # Controlla se il set del modulo di rig dei joints esiste, altrimenti crealo
        if not cmds.objExists("{}_Joints".format(module)): # 'HeadEyes' -'HeadMouth' - 'HeadSquash'
            section_joints_set = cmds.sets(empty=True, name=("{}_Joints".format(module)))
            # Parenta il set della parte di rig dentro al set globale
            cmds.sets(section_joints_set, edit=True, forceElement=module_joints_set)
        else:
            section_joints_set = "{}_Joints".format(module)

        # Controlla se il set della parte di rig dei joints esiste, altrimenti crealo
        if not cmds.objExists("{}_jointsForSkin".format(rigpart)):
            rigpart_joints_set = cmds.sets(empty=True, name=("{}_jointsForSkin".format(rigpart)))
            # Parenta il set della parte di rig dentro al set globale
            cmds.sets(rigpart_joints_set, edit=True, forceElement=section_joints_set)
        else:
            rigpart_joints_set = "{}_jointsForSkin".format(rigpart)


        for jnt in joints_for_skin:
            cmds.sets(jnt, edit=True, forceElement=rigpart_joints_set)

    def create_controls_set(module, rigpart, ctrl_list):
        # Crea il set contenente i controlli creati
        if not cmds.objExists("FACE_MODULES_Controls"):
            module_controls_set = cmds.sets(empty=True, name=("FACE_MODULES_Controls"))
        else:
            module_controls_set = "FACE_MODULES_Controls"

        if not cmds.objExists("{}_controls".format(module)):
            module_ctrl_set = cmds.sets(empty=True, name=("{}_controls".format(module)))
            # Parenta il set del modulo dentro al set globale
            cmds.sets(module_ctrl_set, edit=True, forceElement=module_controls_set)
        else:
            module_ctrl_set = "{}_controls".format(module)
            
        if not cmds.objExists("{}_controls".format(rigpart)):
            rigpart_ctrl_set = cmds.sets(empty=True, name=("{}_controls".format(rigpart)))
            # Parenta il set del modulo dentro al set globale
            cmds.sets(rigpart_ctrl_set, edit=True, forceElement=module_ctrl_set)
        else:
            rigpart_ctrl_set = "{}_controls".format(rigpart)

        for ctrl in ctrl_list:
            cmds.sets(ctrl, edit=True, forceElement=rigpart_ctrl_set)

        return module_ctrl_set

    def rename_ctrl_shapes(set_ctrl):
        """Data una selezione di controlli, rinomina le shapes in base al nome del controllo."""
        cmds.select(clear=True)
        cmds.select(set_ctrl)
        sel = cmds.ls(sl=True)
        cmds.select(clear=True)
        
        for ctrl in sel:
            shapes = cmds.listRelatives(ctrl, shapes=True, pa=True)
            for shape in shapes:
                new_name = "{}Shape".format(ctrl)
                cmds.rename(shape, new_name)

        print("### Le shapes dei controlli sono state rinominate correttamente. ###")


    create_joints_set("HeadMouth", "Nose", ["NoseHigh_jnt", "NoseMid_jnt", "Nose_jnt", "L_Nostril_jnt", "R_Nostril_jnt", "NoseLow_jnt"])
    controls_set = create_controls_set("HeadMouth", "Nose", ["NoseHigh_Ctrl", "NoseMid_Ctrl", "Nose_Ctrl", "NoseLow_Ctrl", "L_Nostril_Ctrl", "R_Nostril_Ctrl"])
    rename_ctrl_shapes(controls_set)


main_rig_process()
