# -*- coding: utf-8 -*-

import maya.cmds as cmds
from maya import OpenMayaUI as omui
import os
from functools import partial
from imp import reload
import depts.rigging.tools.generic.Face_RIG.scripts.matrix_module as mtx
reload(mtx)


# -----------------------------------------------------------------
# PySide
# -----------------------------------------------------------------
try:
    from shiboken2 import wrapInstance
except ImportError:
    from shiboken import wrapInstance

try:
    from PySide2.QtGui import QIcon
    from PySide2.QtWidgets import QWidget
except ImportError:
    from PySide.QtGui import QIcon, QWidget


class Cheeks:
    def __init__(self):
        # CHEEKMOUTH
        self.num_joints = None
        self.cheek_curve = None
        self.cheek_surface = None
        self.joints_list = []
        self.ctrl_jnts_list = [] 
        self.grp_main = None
        self.grp_jnts = None
        self.grp_ctrls = None
        self.grp_base = None
        self.grp_ctrl_jnts = None
        self.cheek_mouth_ctrl = None
        self.set_cheek_ctrls = []
        self.parent_rig_cheekmouth = ("L_CornerMouth_Ctrl", "R_CornerMouth_Ctrl", "Head_SQSUp_01_jnt")
        self.master_grp_cheekmouth = None
        self.grp_cheek_jnt = None
        self.grp_cheek_ctrl = None
        self.static_grp = None
        self.set_sliders = None

        # CHEEK
        self.grp_main_list = []
        self.grp_cheek_jnt_list = []
        self.grp_cheek_ctrl_list = []
        self.grp_cheek_utility_list = []
        self.cheek_joint_list = []
        self.cheek_ctrl_list= []
        self.cheek_locator_list = []
        self.master_grp_cheek_list = []
        self.mtx_node_squint = []
        self.dcm_node_squint = []
        self.set_slider_cheek = []

        # PUFF
        self.puff_locator_list = []
        

        # -----------------------------------------------------------------
        # Path
        # -----------------------------------------------------------------
        localpipe = os.getenv("LOCALPY")
        myPath = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "Face_RIG")
        script_path = os.path.join(myPath, "scripts")
        scene_path = os.path.join(myPath, "scenes", "controls_shape.ma")
        image_path = os.path.join(myPath, "icons")

        self.my_file_path = scene_path
        self.image_path = image_path
        

    # -----------------------------------------------------------------
    # Funzioni per creare CHEEK MOUTH
    # -----------------------------------------------------------------
    def which_side(self, side, *args):
        """Definisci la variabile 'side'."""
        if side:
            self.side = side
        else:
            cmds.error("Please select a side.\n")


    def select_curve(self, *args):
        """Fa un check se l'oggetto selezionato è una curva e la stora se affermativo."""
        selection = cmds.filterExpand(sm = 9)
        
        if selection == None :
            self.cheek_curve = None
            cmds.error("Please select a curve.\n")
        else :
            self.cheek_curve = selection[0]

        cmds.select(cl=True)

        # Update UI
        cmds.textField(self.txtf_curve, e=1, tx=self.cheek_curve)
        cmds.button(self.btn_select_curve, e=1, en=0)
        cmds.button(self.btn_undo_curve, e=1, en=1)

    
    def select_curve_undo(self, *args):
        """Resetta la variabile della curva."""
        self.cheek_curve = None

        # Update UI
        cmds.textField(self.txtf_curve, e = 1, tx = "")
        cmds.button(self.btn_select_curve, e = 1, en = 1)
        cmds.button(self.btn_undo_curve, e = 1, en = 0)


    def _input_number_ctrls(self):
        """Retrieve text insert from user."""
        input_joints = cmds.intSliderGrp("num_jnts", query=True, value=True)
        self.num_joints = input_joints


    def _create_module_hierarchy_cheekmouth(self, side):
        """Crea la gerachia di gruppi del modulo cheekmouth"""
        if not cmds.objExists("{}_Cheek_RIG_GRP".format(side)):
            self.grp_main = cmds.group(name="{}_Cheek_RIG_GRP".format(side), em=True)
        else:
            self.grp_main = "{}_Cheek_RIG_GRP".format(side)

        self.grp_jnts = cmds.group(name="{}_CheekMouth_JNT_GRP".format(side), em=True)
        self.grp_ctrls = cmds.group(name="{}_CheekMouth_CTRL_GRP".format(side), em=True)
        self.grp_base = cmds.group(name="{}_CheekMouth_Base_RIG_GRP".format(side), em=True)
        self.grp_curves = cmds.group(name="{}_Cheek_curves_GRP".format(side), em=True)
        self.grp_ctrl_jnts = cmds.group(name="{}_Cheek_CTRL_JNT_GRP".format(side), em=True)

        cmds.parent(self.grp_jnts, self.grp_main)
        cmds.parent(self.grp_ctrls, self.grp_main)
        cmds.parent(self.grp_base, self.grp_main)
        cmds.parent(self.grp_curves, self.grp_base)
        cmds.parent(self.grp_ctrl_jnts, self.grp_base)

        cmds.setAttr("{}.visibility".format(self.grp_jnts), 0)
        cmds.setAttr("{}.visibility".format(self.grp_base), 0)

        # Imparenta tutto sotto 'Face_RIG_Grp'
        if cmds.objExists(self.grp_main) and cmds.objExists("Face_RIG_Grp"):
            cmds.parent(self.grp_main, "Face_RIG_Grp")
    

    def _check_curve_direction_and_fixit(self, curve):
        """Controlla se la direzione della curva è dal basso in alto, altrimenti inverte la direzione."""
        # Calcola il numero di vertici nella curva
        numCVs = cmds.getAttr(curve + '.spans') + cmds.getAttr(curve + '.degree')

        pos_cv_start = cmds.xform("{}.cv[0]".format(curve), q=True, ws=True, t=True)
        pos_cv_end = cmds.xform("{}.cv[{}]".format(curve, numCVs-1), q=True, ws=True, t=True)

        # se cv[0] è più in alto dell'ultimo cv vuol dire che bisogna flippare la curva
        if pos_cv_start[1] > pos_cv_end[1]:
            cmds.reverseCurve(curve, ch=True, rpo=True)


    def _create_surface(self, side, curve, grp_curves):
        """Duplica e offsetta la curva, crea la nurbs con il loft e fai il rebuld in base al numero di joints desiderati."""
        # check direzione curva
        self._check_curve_direction_and_fixit(curve)

        # costruisci la nurbs partendo dalla curva
        crv_01 = cmds.duplicate(curve, name="{}_tmp_01".format(curve[0]))
        cmds.move(0.5,0,0, crv_01, r=True, os=True, wd=True)
        crv_02 = cmds.duplicate(curve, name="{}_tmp_01".format(curve[0]))
        cmds.move(-0.5,0,0, crv_02, r=True, os=True, wd=True)

        cheek_surface_lofted = cmds.loft(crv_01, crv_02, name="{}_CheekMouth_surface".format(side), ch=False, u=False, c=False, ar=True, d=1, ss=1, rn=False, po=0, rsn=True)

        cmds.delete(crv_01)
        cmds.delete(crv_02)

        self.cheek_surface = cmds.rebuildSurface(cheek_surface_lofted, constructionHistory=0, replaceOriginal=1, rebuildType=0, endKnots=1, keepRange=0, keepControlPoints=0, 
                                                 keepCorners=0, spansU=(self.num_joints-1), degreeU=3, spansV=1, degreeV=2, tolerance=0.01, fitRebuild=0, direction=2)[0]

        cmds.parent(self.cheek_surface, grp_curves)
        cmds.parent(curve, grp_curves)
        cmds.rename(curve, "{}_CheekMouth_curve".format(side))

        # slider list
        self.set_sliders = self.cheek_surface

    
    def _create_joints_cheek_mouth(self, side, surface, num_joints):
        """Crea i joint che andranno poi pinnati alla nurbs."""
        for idx in range(num_joints):
            jnt = cmds.joint(name="{}_CheekMouth_Skin_0{}_jnt".format(side, idx+1))
            self.joints_list.append(jnt)
            cmds.setAttr("{}.radius".format(jnt), 0.2)
            cmds.parent(jnt, self.grp_jnts)

        # Crea il set contenente i joints per lo skinning della mesh
        jonts_for_set = []
        jonts_for_set.extend(self.joints_list)
        
        # Crea il set contenente i joints per lo skinning della mesh
        if not cmds.objExists("FACE_MODULES_Joints"):
            module_joints_set = cmds.sets(empty=True, name=("FACE_MODULES_Joints"))
        else:
            module_joints_set = "FACE_MODULES_Joints"

        # Controlla se il set della parte di rig dei joints esiste, altrimenti crealo
        if not cmds.objExists("HeadMouth_Joints"):
            rigpart_joints_set = cmds.sets(empty=True, name=("HeadMouth_Joints"))
            # Parenta il set della parte di rig dentro al set globale
            cmds.sets(rigpart_joints_set, edit=True, forceElement=module_joints_set)
        else:
            rigpart_joints_set = "HeadMouth_Joints"
        
        # Controlla se il set dei joints del modulo esiste, altrimenti crealo
        if not cmds.objExists("{}_Cheek_jointsForSkin".format(side)):
            set_skin_jonts = cmds.sets(empty=True, name=("{}_Cheek_jointsForSkin".format(side)))
            # Parenta il set del modulo dentro al set globale
            cmds.sets(set_skin_jonts, edit=True, forceElement=rigpart_joints_set)
        else:
            set_skin_jonts = "{}_Cheek_jointsForSkin".format(side)

        for jnt in jonts_for_set:
            cmds.sets(jnt, edit=True, forceElement=set_skin_jonts)


    def _orient_joints(self, surface, joints_list):
        """Orienta i joint e freeza le rotate."""
        for jnt in joints_list:
            constr = cmds.normalConstraint(surface, jnt, aimVector = (0,0,1),  upVector=(1,0,0), worldUpType="vector", worldUpVector=(0,1,0))
            cmds.delete(constr)
            cmds.makeIdentity(jnt, apply=True, r=True)


    def _uvpin_system(self, side, surface, num_joints, joints_list):
        """Crea gli uvpin e li connette alla nurbs e ai joints."""

        # Crea un solo nodo uvpin da usare per questo rig
        value = 1.0 / (num_joints-1)
        surf_shape = cmds.listRelatives(surface)[0]
        uvpin_node = cmds.createNode("uvPin", name="{}_CheekMouth_uvpin".format(side))
        cmds.connectAttr("{}.worldSpace[0]".format(surf_shape), "{}.deformedGeometry".format(uvpin_node), force=True)
        cmds.connectAttr("{}.local".format(surf_shape), "{}.originalGeometry".format(uvpin_node), force=True)

        # Crea nodi plusMinus e fai connessioni
        for idx in range(num_joints):
            valueU = value * (idx)
            cmds.setAttr("{}.coordinate[{}].coordinateU".format(uvpin_node, idx), valueU)
            cmds.setAttr("{}.coordinate[{}].coordinateV".format(uvpin_node, idx), 0.5)

        # Connette il nodo uvpin ai joint.
        for idx, jnt in enumerate(joints_list):
            cmds.connectAttr(("{}.outputMatrix[{}]").format(uvpin_node, idx), "{}.offsetParentMatrix".format(jnt), force=True)

        # self._orient_joints(surface, joints_list)


    def _make_control_joint_cheekmouth(self, side, joints_list):
        """Crea i joint che skinnano la surface."""
        cmds.select(clear=True)
        for jnt, name in zip((joints_list[0], joints_list[-1]), ("Mouth", "Mouth_Eye")):
            ctrl_jnt = cmds.joint(name="{}_Cheek{}_jnt".format(side, name))
            self.ctrl_jnts_list.append(ctrl_jnt)
            cmds.parent(ctrl_jnt, self.grp_ctrl_jnts)
            tmp = cmds.pointConstraint(jnt, ctrl_jnt, mo=False, w=1)
            cmds.delete(tmp)
            cmds.select(clear=True)

        # Orienta 'Cheek_01_jnt' come il 'CornerMouth_Ctrl'
        cmds.matchTransform(self.ctrl_jnts_list[0], "{}_CornerMouth_Ctrl".format(side), rot=True)


    def _create_controls_cheek_mouth(self, side, ctrl_joints_list, ctrls_grp):
        """Crea i controlli e l mette in gerarchia."""
        jnt = ctrl_joints_list[0]
        ctrl_name = jnt.replace("jnt", "Ctrl")

        self.cheek_mouth_ctrl = cmds.circle(r = 0.15, n=ctrl_name)[0]
                
        for shape in cmds.listRelatives(self.cheek_mouth_ctrl, s=True, f=True) or []:
            shapeRenamed = cmds.rename(shape, "{}Shape".format(ctrl_name))

        self.master_grp_cheekmouth = cmds.group(name = "{}_Grp".format(ctrl_name), em=True)
        matrix_grp = cmds.group(name = "{}_Offset_Grp".format(ctrl_name), em=True)

        cmds.parent(self.cheek_mouth_ctrl, matrix_grp)
        cmds.parent(matrix_grp, self.master_grp_cheekmouth)
        cmds.parent(self.master_grp_cheekmouth, ctrls_grp)
        
        mtt = cmds.matchTransform(self.master_grp_cheekmouth, jnt)
        pac = cmds.parentConstraint(self.cheek_mouth_ctrl , jnt, mo=True, w=1)
        
        vis = cmds.setAttr(self.cheek_mouth_ctrl + ".visibility", lock=True, keyable=False, channelBox=False)
        shp = cmds.setAttr(self.cheek_mouth_ctrl + "Shape.overrideEnabled", 1)
        col = cmds.setAttr(self.cheek_mouth_ctrl + "Shape.overrideColor", 18)

        # Lock and hide unused channels
        for attr in ("sx", "sy", "sz", "v"):
            cmds.setAttr("{}.{}".format(self.cheek_mouth_ctrl, attr), lock=True, keyable=False, channelBox=False)

        # Controlla se il set contenente i controlli esiste, altrimenti crealo
        if not cmds.objExists("FACE_MODULES_Controls"):
            module_controls_set = cmds.sets(empty=True, name=("FACE_MODULES_Controls"))
        else:
            module_controls_set = "FACE_MODULES_Controls"

        # Controlla se il set della parte di rig dei joints esiste, altrimenti crealo
        if not cmds.objExists("HeadMouth_controls"):
            rigpart_controls_set = cmds.sets(empty=True, name=("HeadMouth_controls"))
            # Parenta il set della parte di rig dentro al set globale
            cmds.sets(rigpart_controls_set, edit=True, forceElement=module_controls_set)
        else:
            rigpart_controls_set = "HeadMouth_controls"

        # Controlla se il set dei controlli del modulo esiste, altrimenti crealo
        if not cmds.objExists("{}_Cheek_controls".format(side)):
            self.set_cheek_ctrls = cmds.sets(empty=True, name=("{}_Cheek_controls".format(side)))
            # Parenta il set del modulo dentro al set rigpart
            cmds.sets(self.set_cheek_ctrls, edit=True, forceElement=rigpart_controls_set)
        else:
            self.set_cheek_ctrls = "{}_Cheek_controls".format(side)

        # Aggiungi controlli al set contenente i controlli creati
        cmds.sets(self.cheek_mouth_ctrl, edit=True, forceElement=self.set_cheek_ctrls)


    def _replace_and_remove_shapes(self, scene_path):
        """Sostituisci le shape dei controlli con quelle 'ufficiali'."""
        # lista contenente i controlli da sostituire
        control_pairs = [
                ('L_CheekMouth_Ctrl_REF', 'L_CheekMouth_Ctrl'),
                ('R_CheekMouth_Ctrl_REF', 'R_CheekMouth_Ctrl'),
                ('L_Cheek_Ctrl_REF', 'L_Cheek_Ctrl'),
                ('R_Cheek_Ctrl_REF', 'R_Cheek_Ctrl'),
                ('L_Puff_Ctrl_REF', 'L_Puff_Ctrl'),
                ('R_Puff_Ctrl_REF', 'R_Puff_Ctrl')
            ]

        # importa scena con i controlli
        cmds.file(scene_path, i=True, mergeNamespacesOnClash=True, namespace=':', gr=True, gn="GRP_shapes_REF")

        # itera attraverso le coppie di controlli e esegui la sostituzione
        for source_ctrl, target_ctrl in control_pairs:
            try:
                # Ottieni e elimina le shape esistenti nel controllo di destinazione
                target_shapes = cmds.listRelatives(target_ctrl, shapes=True)
                name_target_shape = target_shapes
                if target_shapes:
                    cmds.delete(target_shapes)

                # Ottieni la shape del controllo sorgente
                new_source_ctrl = cmds.duplicate(source_ctrl, n="{}_NEW".format(source_ctrl))
                source_shapes = cmds.listRelatives(new_source_ctrl, shapes=True)
                if source_shapes:
                    for source_shape in source_shapes:
                        cmds.parent(source_shape, target_ctrl, r=True, shape=True)
                        cmds.rename(source_shape, name_target_shape[0])
            except:
                pass

        # elimina il gruppo dei controlli importati
        grp_del = "GRP_shapes_REF"
        cmds.delete("GRP_shapes_REF")

    
    def _skin_surface(self, surface, ctrl_joints_list):
        """Skinna i singoli cv della nurbs con i joint corrispondenti."""
        kwargs = {
            'toSelectedBones': True,
            'bindMethod': 0,
            'normalizeWeights': 1,
            'weightDistribution': 0,
            'maximumInfluences': 5,
            'obeyMaxInfluences': True,
            'dropoffRate': 4,
            'removeUnusedInfluence': False
        }

        sk_node = cmds.skinCluster(ctrl_joints_list, surface, name="SK_{}".format(surface), **kwargs)[0]
        cmds.select(clear=True)


    def _duplicate_and_rename_groups_and_controls(self, side, base_group, rig_hierarchy):
        """Fa un duplicato di 'base_group' e rinominalo con tutto il suo contenuto col suffisso '_Static'."""
        # Rinomina il gruppo dei controlli 'base_group' col suffisso '_Static'.
        self.static_grp_mouth = cmds.rename(base_group, "{}_Static".format(base_group))
        
        # Duplicalo come 'driver_grp'
        driver_grp_mouth = cmds.duplicate(self.static_grp_mouth, n=base_group)[0]

        # Pulisci il driver_grp da eventuali constraint
        children_mouth = cmds.listRelatives(driver_grp_mouth, allDescendents=True, fullPath=True, shapes=False)
        for item in children_mouth:
            check_type = cmds.nodeType(item)
            if 'Constraint' in check_type:
                cmds.delete(item)

        # Rinomina tutti i gruppi e controlli del gruppo 'static_grp' col suffisso '_Static', saltando le shapes.
        children_mouth = cmds.listRelatives(self.static_grp_mouth, allDescendents=True, fullPath=True, shapes=False)
        for item in children_mouth:
            check_type = cmds.nodeType(item)
            if check_type == 'transform':
                tmp = item.split("|")[-1]
                cmds.rename(item, "{}_Static".format(tmp))

        # Metti in gerachia driver_grp e nascondi lo static_grp
        if side == 'L':
            cmds.parent(self.master_grp_cheekmouth, rig_hierarchy[0])
        elif side == 'R':
            cmds.parent(self.master_grp_cheekmouth, rig_hierarchy[1])
        cmds.setAttr("{}.visibility".format(self.static_grp_mouth), 0)

        # cancella gruppo se vuoto
        check_mouth =  cmds.listRelatives(base_group)
        if not check_mouth:
            cmds.delete(base_group)

        print("# Duplicato e rinominato tutto il contenuto di '{}' col suffisso '_Static'. #".format(base_group))


    def _cleanup_set_controls(self, set_name):
        """Elimina controlli 'Static' dal set dei controlli."""
        set_tmp = cmds.sets(set_name, q=True)

        for item in set_tmp:
            tokens = item.split("_")
            if tokens[-1] == "Static":
                cmds.sets(item, remove=set_name) 

    
    def _connect_driver_to_static(self, driver_grp, static_grp):
        """Crea connessioni dirette tra i controlli e gruppi Matrix di driver e static """
        children_static = cmds.listRelatives(static_grp, allDescendents=True, fullPath=False, shapes=False)
        children_driver = cmds.listRelatives(driver_grp, allDescendents=True, fullPath=False, shapes=False)

        # '_Ctrl' --> '_Ctrl_Static'
        for static_ctrl, driver_ctrl in zip(children_static, children_driver):
            if cmds.nodeType(driver_ctrl) == 'transform':
                name_tokens = driver_ctrl.split("_")[-1]
                if name_tokens == "Ctrl":
                    driver_attrs = cmds.listAttr(driver_ctrl, keyable=True) or []
                    for attr in driver_attrs:
                        if cmds.attributeQuery(attr, node=static_ctrl, exists=True) and cmds.getAttr(driver_ctrl + '.' + attr, keyable=True):
                            cmds.connectAttr("{}.{}".format(driver_ctrl, attr), "{}.{}".format(static_ctrl, attr), force=True)

        # # '_Ctrl_MATRIX_Grp_Static' --> '_Ctrl_MATRIX_Grp' valido per [MATRIX, FOLLOW, SQUARE, COMPENSATE]
        # lista_gruppi = ('MATRIX')
        # for static_grp, driver_grp in zip(children_static, children_driver):
        #     if cmds.nodeType(driver_grp) == 'transform':
        #         name_tokens = driver_grp.split("_")
        #         for grp in lista_gruppi:
        #             if grp in name_tokens:
        #                 driver_attrs = cmds.listAttr(static_grp, keyable=True) or []
        #                 for attr in driver_attrs:
        #                     if cmds.attributeQuery(attr, node=driver_grp, exists=True) and cmds.getAttr("{}.{}".format(static_grp, attr), keyable=True):
        #                         cmds.connectAttr("{}.{}".format(static_grp, attr), "{}.{}".format(driver_grp, attr), force=True)


    def _make_sliders(self, side, nodi):
        """Rinomina i nodi passati come input con il suffisso 'SLIDER' e se non esiste crea il set slider corrispondente."""
        sliders = nodi

        # Controlla se il set globale degli sliders esiste, altrimenti crealo
        if not cmds.objExists("SLIDERS"):
            sliders_set = cmds.sets(empty=True, name=("SLIDERS"))
        else:
            sliders_set = "SLIDERS"

        # Controlla se il set della parte di rig degli sliders esiste, altrimenti crealo
        if not cmds.objExists("SLIDERS_HeadMouth"):
            rigpart_sliders_set = cmds.sets(empty=True, name=("SLIDERS_HeadMouth"))
            # Parenta il set della parte di rig dentro al set globale
            cmds.sets(rigpart_sliders_set, edit=True, forceElement=sliders_set)
        else:
            rigpart_sliders_set = "SLIDERS_HeadMouth"

        # crea sotto-set del singolo modulo
        if not cmds.objExists("{}_Cheek_sliders".format(side)):
            set_module_sliders = cmds.sets(empty=True, name=("{}_Cheek_sliders".format(side)))
            # Parenta il set del modulo dentro al set globale
            cmds.sets(set_module_sliders, edit=True, forceElement=rigpart_sliders_set)
        else:
            set_module_sliders = "{}_Cheek_sliders".format(side)

        # Popola il set
        cmds.sets(sliders, edit=True, forceElement=set_module_sliders)

        print("# Sliders rinominati e set creato. #")

    
    def build_cheek_mouth(self, *args):
        """Build cheekmouth rig."""
        self._input_number_ctrls()
        self._create_module_hierarchy_cheekmouth(self.side)
        self._create_surface(self.side, self.cheek_curve, self.grp_curves)
        self._create_joints_cheek_mouth(self.side, self.cheek_surface, self.num_joints)
        self._uvpin_system(self.side, self.cheek_surface, self.num_joints, self.joints_list)
        self._make_control_joint_cheekmouth(self.side, self.joints_list)
        self._create_controls_cheek_mouth(self.side, self.ctrl_jnts_list, self.grp_ctrls)
        self._replace_and_remove_shapes(self.my_file_path)
        self._skin_surface(self.cheek_surface, self.ctrl_jnts_list)
        self._duplicate_and_rename_groups_and_controls(self.side, self.grp_ctrls, self.parent_rig_cheekmouth)
        self._cleanup_set_controls(self.set_cheek_ctrls)
        self._connect_driver_to_static(self.master_grp_cheekmouth, self.static_grp_mouth)
        self._make_sliders(self.side, self.set_sliders)
        
        # Clear scene & script variables #
        cmds.select(clear=True)

        self.cheek_curve = None
        self.num_joints = None
        self.grp_main = None
        self.grp_jnts = None
        self.grp_ctrls = None
        self.grp_base = None
        self.grp_ctrl_jnts = None
        self.grp_curves = None
        self.joints_list[:] = []
        self.ctrl_jnts_list[:] = []
        self.set_cheek_ctrls = None
        self.static_grp_mouth = None
        self.master_grp_cheekmouth = None
        self.set_sliders = None

        # Update UI #
        cmds.textField(self.txtf_curve, e = 1, tx = "")
        cmds.button(self.btn_select_curve, e = 1, en = 1)
        cmds.button(self.btn_undo_curve, e = 1, en = 0)
        cmds.intSliderGrp(self.input_joints, edit=True, value=4)

        # End message
        print("### {}_CheekMouth have been successfully rigged. ###".format(self.side))


    # -----------------------------------------------------------------
    # Funzioni per creare CHEEK
    # -----------------------------------------------------------------
    def create_cheek_locator_tmp(self, *args):
        """Crea locator per posizionare il Cheek_Ctrl."""
        L_cheek_locator = cmds.spaceLocator(name="L_cheek_locator")[0]
        R_cheek_locator = cmds.spaceLocator(name="R_cheek_locator")[0]

        for attr, value in zip(("X", "Y", "Z"), (4.5, 152, 6.8)):
            cmds.setAttr("{}.translate{}".format(L_cheek_locator, attr), value)
        for attr, value in zip(("X", "Y", "Z"), (-4.5, 152, 6.8)):
            cmds.setAttr("{}.translate{}".format(R_cheek_locator, attr), value)

        # crea nodi multiply e crea connessioni per mirrorare locator di destra
        mlp_node_translate = cmds.createNode("multiplyDivide", n="MLP_{}_mirror_translate".format(R_cheek_locator))
        mlp_node_rotate = cmds.createNode("multiplyDivide", n="MLP_{}_mirror_rotate".format(R_cheek_locator))
        cmds.setAttr("{}.input2X".format(mlp_node_translate), -1)
        cmds.setAttr("{}.input2Y".format(mlp_node_rotate), -1)
        cmds.setAttr("{}.input2Z".format(mlp_node_rotate), -1)

        cmds.connectAttr("{}.translate".format(L_cheek_locator), "{}.input1".format(mlp_node_translate), f=True)
        cmds.connectAttr("{}.output".format(mlp_node_translate), "{}.translate".format(R_cheek_locator), f=True)
        
        cmds.connectAttr("{}.rotate".format(L_cheek_locator), "{}.input1".format(mlp_node_rotate), f=True)
        cmds.connectAttr("{}.output".format(mlp_node_rotate), "{}.rotate".format(R_cheek_locator), f=True)

        for loc in (L_cheek_locator, R_cheek_locator):
            self.cheek_locator_list.append(loc)
            cmds.setAttr("{}.overrideEnabled".format(loc), 1)
            cmds.setAttr("{}.overrideColor".format(loc), 16)

        # Update UI
        cmds.button(self.btn_cheek_locator, e=1, en=0)
        cmds.button(self.btn_cheek_build, e=1, en=1)


    def build_cheek(self, *args):
        """Build cheek rig."""
        self._create_module_hierarchy_cheek()
        self._create_cheek_joint_and_control(self.cheek_locator_list[0:2], self.grp_cheek_jnt_list, self.grp_cheek_ctrl_list)
        self._create_cheek_locators_squint(self.grp_cheek_utility_list, self.cheek_joint_list, self.cheek_ctrl_list)
        self._create_cheek_squint_matrices(self.cheek_locator_list[2][0], self.cheek_locator_list[3][0], self.cheek_ctrl_list)
        self._create_cheek_squint_distance(self.cheek_locator_list[2], self.cheek_locator_list[3], self.cheek_ctrl_list)
        self._connection_squint_eyesocket(self.cheek_ctrl_list)
        self._replace_and_remove_shapes(self.my_file_path)
        self._duplicate_and_rename_groups_and_controls_cheek("L", self.grp_cheek_ctrl_list[0], self.parent_rig_cheekmouth)
        self._duplicate_and_rename_groups_and_controls_cheek("R", self.grp_cheek_ctrl_list[1], self.parent_rig_cheekmouth)
        self._cleanup_set_controls("L_Cheek_controls")
        self._cleanup_set_controls("R_Cheek_controls")
        self._connect_driver_to_static("L_Cheek_CTRL_GRP", "L_Cheek_CTRL_GRP_Static")
        self._connect_driver_to_static("R_Cheek_CTRL_GRP", "R_Cheek_CTRL_GRP_Static")
        self._make_sliders("L", self.set_slider_cheek[0])
        self._make_sliders("R", self.set_slider_cheek[1])

        # Clear scene & script variables #
        cmds.select(clear=True)

        self.cheek_locator_list[:] = []
        self.cheek_joint_list[:] = []
        self.cheek_ctrl_list[:] = []
        self.grp_cheek_jnt_list[:] = []
        self.grp_cheek_ctrl_list[:] = []
        self.grp_cheek_utility_list[:] = []
        self.master_grp_cheek_list[:] = []
        self.mtx_node_squint[:] = []
        self.dcm_node_squint[:] = []
        self.set_cheek_ctrls = None
        self.set_slider_cheek[:] = []
        
        # Update UI #
        cmds.button(self.btn_cheek_locator, e = 1, en=True)
        cmds.button(self.btn_cheek_build, e = 1, en=False)

        # End message
        print("### L_Cheek and R_Cheek have been successfully rigged. ###")


    def cheek_undo(self, *args):
        """Cancella nodi e gruppi della parte delle cheeks."""
        for idx, (ctrl, locator) in enumerate(zip(("L_Cheek", "R_Cheek"), ("L_cheek_locator", "R_cheek_locator"))):
            if cmds.objExists("{}_REMOVE_SET".format(ctrl)):

                cmds.select("{}_REMOVE_SET".format(ctrl))
                set_element = cmds.ls(sl=True)
                # for item in set_element:
                #     print(item)
                cmds.delete(set_element)
                
                # End message
                if idx == 0:
                    print("### {} have been successfully removed. ###".format(ctrl))
                else:
                    print("### {} have been successfully removed. ###".format(ctrl))

            else:
                # Se usato in fase di costruzione, cerca anche se ci sono i locator e cancellali
                if cmds.objExists(locator):
                    cmds.delete(locator)
                    print("### '{}'' have been deleted. ###".format(locator))
                else:
                    cmds.warning("There is no '{}' rig to remove.".format(ctrl))

        self._fix_undo()

        cmds.select(clear=True)
        self.cheek_locator_list[:] = []

        # Update UI
        cmds.button(self.btn_cheek_locator, e=1, en=True)
        cmds.button(self.btn_cheek_build, e=1, en=False)


    # -----------------------------------------------------------------
    def _create_undo_set(self, ctrl, *args):
        for item in args:
            if cmds.objExists("{}_REMOVE_SET".format(ctrl)):
                cmds.sets(item, e=True, add="{}_REMOVE_SET".format(ctrl))
            else:
                ctrl_set = cmds.sets(item, name="{}_REMOVE_SET".format(ctrl))
                cmds.setAttr("{}.hiddenInOutliner".format(ctrl_set), True)


    def _create_module_hierarchy_cheek(self):
        """Crea la gerachia di gruppi del modulo cheek."""
        for side in ("L", "R"):
            if not cmds.objExists("{}_Cheek_RIG_GRP".format(side)):
                grp_main = cmds.group(name="{}_Cheek_RIG_GRP".format(side), em=True)
                self.grp_main_list.append(grp_main)
            else:
                grp_main = "{}_Cheek_RIG_GRP".format(side)
                self.grp_main_list.append(grp_main)

            grp_cheek_jnt = cmds.group(name="{}_Cheek_JNT_GRP".format(side), em=True)
            self.grp_cheek_jnt_list.append(grp_cheek_jnt)
            cmds.parent(grp_cheek_jnt, grp_main)
            cmds.setAttr("{}.visibility".format(grp_cheek_jnt), 0)
        
            grp_cheek_ctrl = cmds.group(name="{}_Cheek_CTRL_GRP".format(side), em=True)
            self.grp_cheek_ctrl_list.append(grp_cheek_ctrl)
            cmds.parent(grp_cheek_ctrl, grp_main)

            grp_cheek_utility = cmds.group(name="{}_Cheek_Utility_GRP".format(side), em=True)
            self.grp_cheek_utility_list.append(grp_cheek_utility)
            cmds.parent(grp_cheek_utility, grp_main)
            cmds.setAttr("{}.visibility".format(grp_cheek_utility), 0)

            # set undo
            self._create_undo_set("{}_Cheek".format(side), grp_cheek_jnt, grp_cheek_utility)

    
    def _create_cheek_joint_and_control(self, locator_list, grp_cheek_jnt_list, grp_cheek_ctrl_list):
        """Crea i joints e i controlli Cheek_Ctrl."""
        for side, locator, grp_cheek_jnt, grp_cheek_ctrl in zip(("L", "R"), locator_list, grp_cheek_jnt_list, grp_cheek_ctrl_list):
            # crea joint
            piv = cmds.xform(locator, q=True, piv=True, ws=True)
            cmds.select(clear=True)
            jnt = cmds.joint(name="{}_Cheek_jnt".format(side), p=piv[0:3])
            self.cheek_joint_list.append(jnt)
            cmds.setAttr("{}.radius".format(jnt), 0.2)
            cmds.parent(jnt, grp_cheek_jnt)

            # se il side è R ruota il joint di 180 sulle Y e freeza le rotate
            if side == "R":
                cmds.setAttr("{}.ry".format(jnt), 180)
                cmds.makeIdentity(jnt, apply=True, rotate=True)

            # Crea il set contenente i joints per lo skinning della mesh
            if not cmds.objExists("FACE_MODULES_Joints"):
                module_joints_set = cmds.sets(empty=True, name=("FACE_MODULES_Joints"))
            else:
                module_joints_set = "FACE_MODULES_Joints"

            # Controlla se il set della parte di rig dei joints esiste, altrimenti crealo
            if not cmds.objExists("HeadMouth_Joints"):
                rigpart_joints_set = cmds.sets(empty=True, name=("HeadMouth_Joints"))
                # Parenta il set della parte di rig dentro al set globale
                cmds.sets(rigpart_joints_set, edit=True, forceElement=module_joints_set)
            else:
                rigpart_joints_set = "HeadMouth_Joints"
            
            # Controlla se il set dei joints del modulo esiste, altrimenti crealo
            if not cmds.objExists("{}_Cheek_jointsForSkin".format(side)):
                set_skin_jonts = cmds.sets(empty=True, name=("{}_Cheek_jointsForSkin".format(side)))
                # Parenta il set del modulo dentro al set globale
                cmds.sets(set_skin_jonts, edit=True, forceElement=rigpart_joints_set)
            else:
                set_skin_jonts = "{}_Cheek_jointsForSkin".format(side)

            cmds.sets(jnt, edit=True, forceElement=set_skin_jonts)

            # crea controllo
            ctrl_name = jnt.replace("jnt", "Ctrl")
            cheek_ctrl = cmds.circle(r = 0.15, n=ctrl_name)[0]
            self.cheek_ctrl_list.append(cheek_ctrl)

            # aggiungo attributo Squint Follow
            cmds.addAttr(cheek_ctrl, ln="squint_follow", at="float", min=0, max=1, dv=1, k=1)
            
            for shape in cmds.listRelatives(cheek_ctrl, s=True, f=True) or []:
                shapeRenamed = cmds.rename(shape, "{}Shape".format(ctrl_name))

            master_grp_cheek = cmds.group(name = "{}_Grp".format(ctrl_name), em=True)
            self.master_grp_cheek_list.append(master_grp_cheek)
            matrix_grp = cmds.group(name = "{}_Offset_Grp".format(ctrl_name), em=True)

            cmds.parent(cheek_ctrl, matrix_grp)
            cmds.parent(matrix_grp, master_grp_cheek)
            cmds.parent(master_grp_cheek, grp_cheek_ctrl)
    
            mtt = cmds.matchTransform(master_grp_cheek, jnt)
            pac = cmds.parentConstraint(cheek_ctrl , jnt, mo=True, w=1)
            
            cmds.setAttr("{}.visibility".format(cheek_ctrl), lock=True, keyable=False, channelBox=False)
            cmds.setAttr("{}Shape.overrideEnabled".format(cheek_ctrl), 1)
            cmds.setAttr("{}Shape.overrideColor".format(cheek_ctrl), 18)
            cmds.setAttr("{}.v".format(cheek_ctrl), lock=True, keyable=False, channelBox=False)
            for attr in ("x", "y", "z"):
                cmds.setAttr("{}.s{}".format(cheek_ctrl, attr), lock=True, keyable=False, channelBox=False)

            # Crea il set contenente i controlli creati
            if not cmds.objExists("FACE_MODULES_Controls"):
                module_controls_set = cmds.sets(empty=True, name=("FACE_MODULES_Controls"))
            else:
                module_controls_set = "FACE_MODULES_Controls"
            
            # Controlla se il set della parte di rig dei joints esiste, altrimenti crealo
            if not cmds.objExists("HeadMouth_controls"):
                rigpart_controls_set = cmds.sets(empty=True, name=("HeadMouth_controls"))
                # Parenta il set della parte di rig dentro al set globale
                cmds.sets(rigpart_controls_set, edit=True, forceElement=module_controls_set)
            else:
                rigpart_controls_set = "HeadMouth_controls"

            # Controlla se il set dei controlli del modulo esiste, altrimenti crealo
            if not cmds.objExists("{}_Cheek_controls".format(side)):
                self.set_cheek_ctrls = cmds.sets(empty=True, name=("{}_Cheek_controls".format(side)))
                # Parenta il set del modulo dentro al set globale
                cmds.sets(self.set_cheek_ctrls, edit=True, forceElement=rigpart_controls_set)
            else:
                self.set_cheek_ctrls = "{}_Cheek_controls".format(side)

            # Aggiungi controlli al set contenente i controlli creati
            cmds.sets(cheek_ctrl, edit=True, forceElement=self.set_cheek_ctrls)

            cmds.select(clear=True)    
        
        for locator in locator_list:
            cmds.delete(locator)


    def _duplicate_and_rename_groups_and_controls_cheek(self, side, cheek_group, rig_hierarchy):
        """Fa un duplicato di 'base_group' e rinominalo con tutto il suo contenuto col suffisso '_Static'."""
        # Rinomina il gruppo dei controlli 'cheek_group' col suffisso '_Static'.
        static_grp_cheek = cmds.rename(cheek_group, "{}_Static".format(cheek_group))
        
        # Duplicalo come 'driver_grp'
        driver_grp_cheek = cmds.duplicate(static_grp_cheek, n=cheek_group)[0]

        # Pulisci il driver_grp da eventuali constraint
        children_cheek = cmds.listRelatives(driver_grp_cheek, allDescendents=True, fullPath=True, shapes=False)
        for item in children_cheek:
            check_type = cmds.nodeType(item)
            if 'Constraint' in check_type:
                cmds.delete(item)

        # Rinomina tutti i gruppi e controlli del gruppo 'static_grp' col suffisso '_Static', saltando le shapes.
        children_cheek = cmds.listRelatives(static_grp_cheek, allDescendents=True, fullPath=True, shapes=False)
        for item in children_cheek:
            check_type = cmds.nodeType(item)
            if check_type == 'transform':
                tmp = item.split("|")[-1]
                cmds.rename(item, "{}_Static".format(tmp))

        # Metti in gerachia driver_grp e nascondi lo static_grp
        cmds.parent(cheek_group, rig_hierarchy[2])
        cmds.setAttr("{}.visibility".format(static_grp_cheek), 0)

        # cancella gruppo se vuoto
        check_cheek =  cmds.listRelatives(cheek_group)
        if not check_cheek:
            cmds.delete(cheek_group)

        # set undo
        self._create_undo_set("{}_Cheek".format(side), static_grp_cheek, driver_grp_cheek)

        print("# Duplicato e rinominato tutto il contenuto di '{}' col suffisso '_Static'. #".format(cheek_group))

    
    def _create_cheek_locators_squint(self, grp_cheek_utility_list, cheek_joint_list, cheek_ctrl_list):
        """Crea sistema di locators per il distance."""
        
        for side, main_grp, jnt, ctrl in zip(("L", "R"), grp_cheek_utility_list, cheek_joint_list, cheek_ctrl_list):
            cheek_squint_loc_list = []
            cheek_squint_grp_loc_list = []

            # loc_driver posizionato sulle trasformate di cheek_jnt
            grp_cheek_driver = cmds.group(name="{}_Cheek_Driver_loc_Grp".format(side), empty=True)
            loc_cheek_driver = cmds.spaceLocator(name="{}_Cheek_Driver_loc".format(side))
            cheek_squint_loc_list.append(loc_cheek_driver[0])
            cheek_squint_grp_loc_list.append(grp_cheek_driver)
            cmds.parent(loc_cheek_driver, grp_cheek_driver)
            cmds.parent(grp_cheek_driver, main_grp)
            cmds.matchTransform(loc_cheek_driver, jnt, pos=True)
            # connessione diretta su tx e ty da cheek_ctl a grp_cheek_driver 
            cmds.connectAttr("{}.translateX".format(ctrl), "{}.translateX".format(grp_cheek_driver), force=True)
            cmds.connectAttr("{}.translateY".format(ctrl), "{}.translateY".format(grp_cheek_driver), force=True)

            # loc_tip posizionato sul centro dei components di iris_Ctrl
            loc_cheek_tip = cmds.spaceLocator(name="{}_Cheek_Tip_loc".format(side))
            cheek_squint_loc_list.append(loc_cheek_tip[0])
            cmds.parent(loc_cheek_tip, main_grp)
            ctrl_cvs = cmds.ls("{}_Iris_Ctrl.cv[:]".format(side), fl=True)
            cmds.select(ctrl_cvs)
            cls_tmp = cmds.cluster(name="cls_tmp")
            cmds.select(clear=True)
            cmds.matchTransform(loc_cheek_tip, cls_tmp, pos=True)
            cmds.delete(cls_tmp)

            # grp_base posizionato sulle trasformate di cheek_jnt
            loc_cheek_base = cmds.spaceLocator(name="{}_Cheek_Base_loc".format(side))
            grp_cheek_base = cmds.group(name="{}_Cheek_Base_loc_Grp".format(side), empty=True)
            cheek_squint_loc_list.append(loc_cheek_base[0])
            cheek_squint_grp_loc_list.append(grp_cheek_base)
            cmds.parent(loc_cheek_base, grp_cheek_base)
            cmds.parent(grp_cheek_base, main_grp)
            cmds.matchTransform(grp_cheek_base, jnt, pos=True)
            # connessione diretta su tx e ty da cheek_ctl a loc_cheek_base 
            cmds.connectAttr("{}.translateY".format(ctrl), "{}.translateY".format(loc_cheek_base[0]), force=True)

            self.cheek_locator_list.append(cheek_squint_loc_list)
            self.grp_cheek_utility_list.append(cheek_squint_grp_loc_list)

            cmds.select(clear=True)


    def _create_cheek_squint_matrices(self, L_loc_cheek_driver, R_loc_cheek_driver, cheek_ctrl_list):
        """Crea sistema matrici per lo squint, con fix connessione usando 2 volte il gruppo squint."""
        # EYELID part
        loc_cheek_driver_list = [L_loc_cheek_driver, R_loc_cheek_driver]
        for side, loc_cheek_driver in zip(("L", "R"), loc_cheek_driver_list):
            eyelid_matrix_list = [["{}".format(loc_cheek_driver), "{}_Eyelid_Low_Ctrl_SQUINT_Grp_Static".format(side), "{}_Eyelid_Low_Ctrl_SQUINT_Grp_Static".format(side)]]
           
            mtx_eyelid_mlp, mtx_eyelid_pls =  mtx.MatrixConstraint(eyelid_matrix_list, t=[(1, 1, 1)]).matrix_constraint()

            # fix connection matrices
            cmds.connectAttr("{}_Eyelid_Low_Ctrl_SQUINT_Grp_Static.parentInverseMatrix[0]".format(side),  "MTX_{}_Cheek_Driver_loc_to_{}_Eyelid_Low_Ctrl_SQUINT_Grp_Static.matrixIn[2]".format(side, side), force=True)

            # trova e metti mtx_node e dcm_node nel set undo
            dcm_node = cmds.listConnections(mtx_eyelid_mlp[0], s=True, d=False)[0]
            mtx_node = cmds.listConnections(dcm_node, s=True, d=False)[0]
    
            # self.mtx_node_squint.append(mtx_node)
            # self.dcm_node_squint.append(dcm_node)

            # set undo
            self._create_undo_set("{}_Cheek".format(side), dcm_node, mtx_node, mtx_eyelid_pls[0], mtx_eyelid_pls[1], mtx_eyelid_pls[2], mtx_eyelid_mlp[0][0], mtx_eyelid_mlp[0][1], mtx_eyelid_mlp[0][2])


    def _create_cheek_squint_distance(self, L_cheek_locator_list, R_cheek_locator_list, cheek_ctrl_list):
        """Crea nodo distance e collegalo ai setRange per sistema squint dell'eyelid.."""
        for idx, (side, loc_shape, ctrl) in enumerate(zip(("L", "R"), (L_cheek_locator_list, R_cheek_locator_list), cheek_ctrl_list)):
            # crea nodi
            dis_node = cmds.createNode("distanceDimShape", name="DIS_{}_Cheek_DriverShape".format(side))
            cmds.parent(dis_node, self.grp_cheek_utility_list[idx])

            sr_node_eyelid_squint = cmds.createNode("setRange", name="SR_{}_Eyelid_Low_SQUINT".format(side))
            mlp_node_eyelid_slider = cmds.createNode("multiplyDivide", name="MLP_{}_Eyelid_Low_SQUINT_Slider".format(side))
            mlp_node_eyelid_follow = cmds.createNode("multiplyDivide", name="MLP_{}_Eyelid_Low_SQUINT_Follow".format(side))
            
            # crea connessioni
            cmds.connectAttr("{}.worldPosition[0]".format(loc_shape[1]),  "{}.startPoint".format(dis_node), force=True)
            cmds.connectAttr("{}.worldPosition[0]".format(loc_shape[2]),  "{}.endPoint".format(dis_node), force=True)

            cmds.connectAttr("PLS_{}_Eyelid_Low_Ctrl_SQUINT_Grp_Static_Translate.output3D".format(side), "{}.input1".format(mlp_node_eyelid_slider), force=True)
            cmds.connectAttr("{}.output".format(mlp_node_eyelid_slider), "{}.min".format(sr_node_eyelid_squint), force=True)

            for attr in ("X", "Y"):
                cmds.connectAttr("{}.distance".format(dis_node), "{}.value{}".format(sr_node_eyelid_squint, attr), force=True)
                cmds.connectAttr("{}.outValue{}".format(sr_node_eyelid_squint, attr), "{}.input1{}".format(mlp_node_eyelid_follow, attr), force=True)

            for attr in ("X", "Y", "Z"):
                cmds.connectAttr("{}.squint_follow".format(ctrl), "{}.input2{}".format(mlp_node_eyelid_follow, attr), force=True)

            cmds.connectAttr("{}.output".format(mlp_node_eyelid_follow), "{}_Eyelid_Low_Ctrl_SQUINT_Grp_Static.translate".format(side), force=True)

            # imposta valori setRange
            cmds.setAttr("{}.valueZ".format(sr_node_eyelid_squint), 2.5)
            for attr, valueMin, valueMax in zip(("X", "Y", "Z"), (1, 0.8, 1), (3, 2.8, 2.5)):
                cmds.setAttr("{}.oldMin{}".format(sr_node_eyelid_squint, attr), valueMin)
                cmds.setAttr("{}.oldMax{}".format(sr_node_eyelid_squint, attr), valueMax)

            # slider list
            self.set_slider_cheek.append(sr_node_eyelid_squint)

            # set undo
            self._create_undo_set("{}_Cheek".format(side), dis_node, sr_node_eyelid_squint, mlp_node_eyelid_slider, mlp_node_eyelid_follow)


    def _connection_squint_eyesocket(self, cheek_ctrl_list):
        """Crea sistema squint dell'eyesocket."""
        for side, ctrl in zip(("L", "R"), cheek_ctrl_list):
            mlp_node_eyesocket_squint_follow = cmds.createNode("multiplyDivide", name="MLP_{}_EyeSocket_Low_SQUINT_Follow".format(side))

            for attr in ('X', 'Y', 'Z'):
                cmds.connectAttr("{}.squint_follow".format(ctrl), "{}.input2{}".format(mlp_node_eyesocket_squint_follow, attr), force=True)

            cmds.connectAttr("{}_Eyelid_Low_Ctrl_SQUINT_Grp_Static.translate".format(side), "{}.input1".format(mlp_node_eyesocket_squint_follow), force=True)
            cmds.connectAttr("{}.output".format(mlp_node_eyesocket_squint_follow), "{}_EyeSocket_Low_Ctrl_SQUINT_Grp_Static.translate".format(side), force=True)

            cmds.connectAttr("{}_Eyelid_Low_Ctrl_SQUINT_Grp_Static.rotate".format(side), "{}_EyeSocket_Low_Ctrl_SQUINT_Grp_Static.rotate".format(side), force=True)
            cmds.connectAttr("{}_Eyelid_Low_Ctrl_SQUINT_Grp_Static.scale".format(side), "{}_EyeSocket_Low_Ctrl_SQUINT_Grp_Static.scale".format(side), force=True)

            # set undo
            self._create_undo_set("{}_Cheek".format(side), mlp_node_eyesocket_squint_follow)

    
    def _fix_undo(self):
        """Fix finali a cause delle matrici."""
        for side in ('L', 'R'):
            cmds.connectAttr("{}_Eyelid_Low_Ctrl_SQUINT_Grp_Static.translate".format(side), "{}_EyeSocket_Low_Ctrl_SQUINT_Grp_Static.translate".format(side), force=True)

            grp_squint_eyelid = "{}_Eyelid_Low_Ctrl_SQUINT_Grp_Static".format(side)
            lista_attributi = cmds.listAttr(grp_squint_eyelid)
            for attr in lista_attributi:
                if attr == "offsetAttr_{}_Cheek_Driver_loc".format(side):
                    cmds.deleteAttr("{}.{}".format(grp_squint_eyelid, attr))

            for attr in ('X', 'Y', 'Z'):
                cmds.setAttr("{}.scale{}".format(grp_squint_eyelid, attr), 1)


    # -----------------------------------------------------------------
    # Funzioni per creare PUFF
    # -----------------------------------------------------------------
    def create_puff_locator(self, *args):
        """Crea locator per posizionare il Puff_Ctrl."""
        L_puff_locator_list = []
        R_puff_locator_list = []
        
        for idx in range(1,4):
            L_loc = cmds.spaceLocator(name="LOC_L_Puff_0{}".format(idx))[0]
            L_puff_locator_list.append(L_loc)
            R_loc = cmds.spaceLocator(name="LOC_R_Puff_0{}".format(idx))[0]
            R_puff_locator_list.append(R_loc)

        for locator, x_value in zip(L_puff_locator_list, (0, 7.5, 12)):
            for attr, value in zip(("X", "Y", "Z"), (x_value, 151.2, 3.6)):
                cmds.setAttr("{}.translate{}".format(locator, attr), value)

        # crea nodi multiply e crea connessioni per mirrorare locator di destra
        for L_puff_locator, R_puff_locator in zip(L_puff_locator_list, R_puff_locator_list):    
            mlp_node_translate = cmds.createNode("multiplyDivide", n="MLP_{}_mirror_translate".format(R_puff_locator))
            mlp_node_rotate = cmds.createNode("multiplyDivide", n="MLP_{}_mirror_rotate".format(R_puff_locator))
            cmds.setAttr("{}.input2X".format(mlp_node_translate), -1)
            cmds.setAttr("{}.input2Y".format(mlp_node_rotate), -1)
            cmds.setAttr("{}.input2Z".format(mlp_node_rotate), -1)

            cmds.connectAttr("{}.translate".format(L_puff_locator), "{}.input1".format(mlp_node_translate), f=True)
            cmds.connectAttr("{}.output".format(mlp_node_translate), "{}.translate".format(R_puff_locator), f=True)
            
            cmds.connectAttr("{}.rotate".format(L_puff_locator), "{}.input1".format(mlp_node_rotate), f=True)
            cmds.connectAttr("{}.output".format(mlp_node_rotate), "{}.rotate".format(R_puff_locator), f=True)

        for loc_list in (L_puff_locator_list, R_puff_locator_list):
            self.puff_locator_list.append(loc_list)

            for loc in loc_list:        
                cmds.setAttr("{}.overrideEnabled".format(loc), 1)
                cmds.setAttr("{}.overrideColor".format(loc), 18)

        # Parenta i locator tra loro
        cmds.parent(L_puff_locator_list[0], L_puff_locator_list[1])
        cmds.parent(L_puff_locator_list[2], L_puff_locator_list[1])
        cmds.parent(R_puff_locator_list[0], R_puff_locator_list[1])
        cmds.parent(R_puff_locator_list[2], R_puff_locator_list[1])

        cmds.select(clear=True)

        # Update UI
        cmds.button(self.btn_puff_locator, e=1, en=0)
        cmds.button(self.btn_puff_build, e=1, en=1)

    
    def _create_joint_chain(self, locator_names, side):
        # Utility che crea la catena di joint
        joint_chain = []
        for loc_name in locator_names:
            joint_name = loc_name.replace("LOC_", "") + "_jnt"
            locator_position = cmds.pointPosition(loc_name, w=True)
            joint = cmds.joint(p=locator_position, name=joint_name)
            cmds.setAttr("{}.radius".format(joint), 0.5)
            joint_chain.append(joint)

        cmds.select(clear=True)
        
        return joint_chain


    def _create_puff_joint_chain(self):
        joint_chains_list = []
        cmds.select(clear=True)

        for side in ("L", "R"):
            # Crea la catena di joint
            locator_names = ["LOC_{}_Puff_01".format(side), "LOC_{}_Puff_02".format(side), "LOC_{}_Puff_03".format(side)]
            joint_chain = self._create_joint_chain(locator_names, side)
            joint_chains_list.append(joint_chain)
            cmds.select(clear=True)

            # Orienta le catene
            cmds.joint(joint_chain[0], e=True, oj="xyz", secondaryAxisOrient="yup", ch=True, zso=True)
            for attr in ("X", "Y", "Z"):
                cmds.setAttr("{}.jointOrient{}".format(joint_chain[-1], attr), 0)

            cmds.select(clear=True)
            
            # Crea gerarchia gruppi joint
            if not cmds.objExists("{}_Cheek_RIG_GRP".format(side)):
                grp_main = cmds.group(name="{}_Cheek_RIG_GRP".format(side), em=True)
            else:
                grp_main = "{}_Cheek_RIG_GRP".format(side)
            
            grp_puff_jnt = cmds.group(name="{}_Puff_JNT_GRP".format(side), em=True)
            cmds.setAttr("{}.visibility".format(grp_puff_jnt), 0)
            cmds.parent(grp_puff_jnt, grp_main)
            cmds.parent(joint_chain[0], grp_puff_jnt)

            # Crea il set contenente i joints per lo skinning della mesh
            if not cmds.objExists("FACE_MODULES_Joints"):
                module_joints_set = cmds.sets(empty=True, name=("FACE_MODULES_Joints"))
            else:
                module_joints_set = "FACE_MODULES_Joints"

            # Controlla se il set della parte di rig dei joints esiste, altrimenti crealo
            if not cmds.objExists("HeadMouth_Joints"):
                rigpart_joints_set = cmds.sets(empty=True, name=("HeadMouth_Joints"))
                # Parenta il set della parte di rig dentro al set globale
                cmds.sets(rigpart_joints_set, edit=True, forceElement=module_joints_set)
            else:
                rigpart_joints_set = "HeadMouth_Joints"
            
            # Controlla se il set dei joints del modulo esiste, altrimenti crealo
            if not cmds.objExists("{}_Cheek_jointsForSkin".format(side)):
                set_skin_jonts = cmds.sets(empty=True, name=("{}_Cheek_jointsForSkin".format(side)))
                # Parenta il set del modulo dentro al set globale
                cmds.sets(set_skin_jonts, edit=True, forceElement=rigpart_joints_set)
            else:
                set_skin_jonts = "{}_Cheek_jointsForSkin".format(side)

            cmds.sets(joint_chain[1], edit=True, forceElement=set_skin_jonts)

            self._create_undo_set("{}_Puff".format(side), grp_puff_jnt)   

        return joint_chains_list


    def _create_linear_curve_with_spans(self, points, curve_name):
        # Utility che cra la curva
        num_spans = 2
        curve = cmds.curve(d=1, p=points, k=[0,1,2], n=curve_name)
        curve_shape_name = "{}Shape".format(curve_name) 
        curve_shape = cmds.listRelatives(curve, shapes=True, path=True)[0]
        cmds.rename(curve_shape, curve_shape_name)
        cmds.select(clear=True)
        
        return curve


    def _create_linear_curves(self, joint_chains):
        # Crea la curva partendo dalla catena di joint
        curve_list = []
        for side, joint_chain in zip(("L", "R"), joint_chains):
            points = []
            side = joint_chain[0].split("_")[0]

            for joint in joint_chain:
                pos = cmds.xform(joint, query=True, translation=True, worldSpace=True) 
                points.append(pos)
            
            grp_puff_curve = cmds.group(name="{}_Puff_curve_GRP".format(side), world=True, em=True)
            curva = self._create_linear_curve_with_spans(points, "CRV_{}_Puff".format(side))
            cmds.parent(curva, grp_puff_curve, relative=True)
            curve_list.append(curva)
            cmds.select(clear=True)

            # Crea gerarchia gruppi curve
            grp_puff_baserig = cmds.group(name="{}_Puff_Base_RIG_GRP".format(side), em=True)
            cmds.setAttr("{}.visibility".format(grp_puff_baserig), 0)
            cmds.parent(grp_puff_baserig, "{}_Cheek_RIG_GRP".format(side))
            cmds.parent(grp_puff_curve, grp_puff_baserig)

            self._create_undo_set("{}_Puff".format(side), grp_puff_baserig)

        return curve_list


    def _create_ik_spline_handle(self, joint_chain, curve_name):
        # Utility per la creazione degli IK Spline Handle
        ik_handle = cmds.ikHandle(solver="ikSplineSolver", sj=joint_chain[0], ee=joint_chain[-1], curve=curve_name, createCurve=False, parentCurve=False)
        ikh = cmds.rename(ik_handle[0], curve_name + "_ikh")
        cmds.rename(ik_handle[1], curve_name + "_effector")
        cmds.select(clear=True)

        return ikh


    def _create_ik_spline_handles(self, joint_chains_list, curves_list):
        # Creazione degli IK Spline Handle
        for side, joint_chain, curve in zip(("L", "R"), joint_chains_list, curves_list):
            ikh = self._create_ik_spline_handle(joint_chain, curve)
            cmds.parent(ikh, "{}_Puff_Base_RIG_GRP".format(side))
        cmds.select(clear=True)


    def _create_joints(self, joint_chains):
        """Crea i tre joint driver all'inizio e fine delle curve."""
        driver_joints = []

        # Crea il joint "Puff_Driver_zero_jnt" nella posizione di "LOC_Puff_01"
        for jnt in (joint_chains[0][0], joint_chains[1][0]):
            cmds.select(clear=True)
            base_name = jnt.split("_01_jnt")
            c_jnt = cmds.joint(name="{}_Driver_zero_jnt".format(base_name[0]))
            driver_joints.append(c_jnt)
            cmds.matchTransform(c_jnt, jnt)
            cmds.select(clear=True)

        # Crea il joint alla fine della curva nella posizione dei joint_03
        for jnt, name_jnt in zip((joint_chains[0][2], joint_chains[1][2]), ("L_Puff_Driver_jnt", "R_Puff_Driver_jnt")):
            cmds.select(clear=True)
            driver_jnt = cmds.duplicate(jnt, n=name_jnt)[0]
            driver_joints.append(driver_jnt)
            cmds.setAttr("{}.radius".format(driver_jnt), 1)
            cmds.parent(driver_jnt, world=True)
            cmds.select(clear=True) 

        # Metti in gerarchia
        for idx, side in enumerate(("L", "R")):
            grp_puff_ctrl_jnts = cmds.group(name="{}_Puff_CTRL_JNT_GRP".format(side), em=True)
            cmds.parent(grp_puff_ctrl_jnts, "{}_Puff_Base_RIG_GRP".format(side))
            cmds.parent(driver_joints[idx], grp_puff_ctrl_jnts)
            cmds.parent(driver_joints[idx+2], grp_puff_ctrl_jnts)

        return driver_joints


    def _create_control_for_joint(self, jnt):
        """
        Crea un controllo NURBS e un gruppo di offset per il joint specificato.
        Restituisce il nome del controllo creato.
        """
        # Creazione del nome del controllo senza "_Jnt" e aggiunta di "_Ctrl"
        ctrl_name = jnt.replace("_Driver_jnt", "_Ctrl")

        # Crea un controllo NURBS (ad esempio, un cerchio)
        ctrl = cmds.circle(r = 1, n=ctrl_name)[0]
                
        for shape in cmds.listRelatives(ctrl, s=True, f=True) or []:
            shapeRenamed = cmds.rename(shape, "{}Shape".format(ctrl_name))

        cmds.setAttr("{}.visibility".format(ctrl), lock=True, keyable=False, channelBox=False)

        # Crea un gruppo di offset per il controllo
        offset_grp = cmds.group(name=ctrl_name + "_Grp_Offset", em=True)
        cmds.parent(ctrl, offset_grp)

        # Crea un gruppo esterno per il gruppo di offset
        external_grp = cmds.group(name=ctrl_name + "_Grp", em=True)
        cmds.parent(offset_grp, external_grp)

        # Sposta il gruppo esterno alla posizione del joint
        cmds.matchTransform(external_grp, jnt)

        return ctrl, external_grp


    def _create_controls_puff(self, driver_joints, chain_joints):
        """Crea i controlli, i relativi gruppi e i parent constraint per i tre joint."""
        created_controls = []
        for jnt in driver_joints[2:]:
            # Crea il controllo per il joint e lo aggiunge alla lista
            ctrl, ctrl_grp = self._create_control_for_joint(jnt)
            created_controls.append(ctrl)
            side = jnt.split("_")[0]
            grp_puff_ctrl = cmds.group(name="{}_Puff_CTRL_GRP".format(side), em=True)
            cmds.parent(grp_puff_ctrl, "{}_Cheek_RIG_GRP".format(side))
            cmds.parent(ctrl_grp, grp_puff_ctrl)

            cmds.setAttr("{}.rx".format(ctrl), lock=True, keyable=False, channelBox=False)

            # Controlla se il set contenente i controlli esiste, altrimenti crealo
            if not cmds.objExists("FACE_MODULES_Controls"):
                module_controls_set = cmds.sets(empty=True, name=("FACE_MODULES_Controls"))
            else:
                module_controls_set = "FACE_MODULES_Controls"

            # Controlla se il set della parte di rig dei joints esiste, altrimenti crealo
            if not cmds.objExists("HeadMouth_controls"):
                rigpart_controls_set = cmds.sets(empty=True, name=("HeadMouth_controls"))
                # Parenta il set della parte di rig dentro al set globale
                cmds.sets(rigpart_controls_set, edit=True, forceElement=module_controls_set)
            else:
                rigpart_controls_set = "HeadMouth_controls"         

            # Controlla se il set dei controlli del modulo esiste, altrimenti crealo
            if not cmds.objExists("{}_Cheek_controls".format(side)):
                set_cheek_ctrls = cmds.sets(empty=True, name=("{}_Cheek_controls".format(side)))
                # Parenta il set del modulo dentro al set globale
                cmds.sets(set_cheek_ctrls, edit=True, forceElement=rigpart_controls_set)
            else:
                set_cheek_ctrls = "{}_Cheek_controls".format(side)

            # Aggiungi controlli al set contenente i controlli creati
            cmds.sets(ctrl, edit=True, forceElement=set_cheek_ctrls)

            self._create_undo_set("{}_Puff".format(side), grp_puff_ctrl)

        for jnt in (chain_joints[0][1], chain_joints[1][1]):
            # Trova il side
            side = jnt.split("_")[0]
            # Ottieni la posizione del pivot del joint_02
            pivot_pos = cmds.xform(jnt, q=True, ws=True, piv=True)
            # Imposta il pivot del controllo alla stessa posizione
            cmds.xform("{}_Puff_Ctrl_Grp".format(side), ws=True, piv=pivot_pos[:3])
            cmds.xform("{}_Puff_Ctrl_Grp_Offset".format(side), ws=True, piv=pivot_pos[:3])
            cmds.xform("{}_Puff_Ctrl".format(side), ws=True, piv=pivot_pos[:3])

        for jnt, ctrl in zip(driver_joints[2:], created_controls):
            # Aggiungi un parent constraint tra il controllo e il joint
            cmds.parentConstraint(ctrl, jnt, mo=True)

        return created_controls


    def _create_curve_for_skinning(self, curve_list):
        """Duplica la curva e rinominala."""
        skinning_curve = []

        for side, curva in zip(("L", "R"), curve_list):
            crv = cmds.duplicate(curva, name="{}_skinning".format(curva))
            skinning_curve.append(crv)
            cmds.select(clear=True)

            # Slider list
            self._make_sliders(side, crv)

        return skinning_curve


    def _bind_skin_to_curve(self, sqs_curve_list, sqs_joint_list):
        """Esegue il bind skin della curva con i joint specificati."""
        for idx, crv in enumerate(sqs_curve_list):
            # Seleziona la curva e i joint ed esegui il bind skin
            cmds.select(crv, sqs_joint_list[idx], sqs_joint_list[idx+2])
            name_sk = crv[0].replace("CRV", "SK")
            cmds.skinCluster(toSelectedBones=True, name=name_sk)
            cmds.select(clear=True)


    def _create_single_wire_deformer(self, curve_list, sqs_curve_list):
        """Crea un singolo Wire deformer tra le curve di costruzioni e quelle skinnate."""

        for build_crv, skin_crv in zip(curve_list, sqs_curve_list):
            # Crea il wire deformer usando 'SQS_cv' come base
            name_wire = build_crv.replace("CRV", "WIR")
            wire_deformer = cmds.wire(build_crv, w=skin_crv, dds=[(0, 9999)], n=name_wire)[0]

            # Imposta lo scale del wire a 0
            cmds.setAttr(wire_deformer + ".scale[0]", 1)

            cmds.select(clear=True)

        # togli curva BaseWire dal set slider
        for side in ("L", "R"):
            name_set = "{}_Cheek_sliders".format(side)
            set_tmp = cmds.sets(name_set, q=True)

            for item in set_tmp:
                tokens = item.split("_")
                if tokens[-1] == "skinningBaseWire":
                    cmds.sets(item, remove=name_set)


    def _create_curve_info_node(self, name, shape_name):
        node = cmds.createNode("curveInfo", name=name)
        cmds.connectAttr(shape_name + ".worldSpace[0]", node + ".inputCurve")
        return node


    def _create_multiply_divide_node(self, name, operation):
        node = cmds.createNode("multiplyDivide", name=name)
        cmds.setAttr(node + ".operation", operation)
        return node


    def _setup_curve(self, curve_shape_name, side):
        # Crea il nodo CurveInfo
        curve_info_node = self._create_curve_info_node("CI_" + side + "_Puff_SQS", curve_shape_name)

        # Crea e configura il nodo MultiplyDivide (Distance)
        multiply_divide_distance = self._create_multiply_divide_node("MLD_" + side + "_Puff_SQS_Distance", 2)
        cmds.connectAttr(curve_info_node + ".arcLength", multiply_divide_distance + ".input1X")

        input1X_value = cmds.getAttr(curve_info_node + ".arcLength")
        cmds.setAttr(multiply_divide_distance + ".input2X", input1X_value)

        # Crea e configura il nodo MultiplyDivide (Power)
        multiply_divide_power = self._create_multiply_divide_node("MLD_" + side + "_Puff_SQS_Power", 3)
        cmds.setAttr(multiply_divide_power + ".input2X", -1)
        cmds.connectAttr(multiply_divide_distance + ".outputX", multiply_divide_power + ".input1X")

        # Crea e configura il nodo MultiplyDivide (Compensate)
        multiply_divide_compensate = self._create_multiply_divide_node("MLD_" + side + "_Puff_SQS_Compensate", 2)
        cmds.connectAttr(multiply_divide_power + ".outputX", multiply_divide_compensate + ".input2X")
        cmds.setAttr(multiply_divide_compensate + ".input1X", 1)



        return multiply_divide_distance, multiply_divide_compensate, multiply_divide_power, curve_info_node


    def _setup_puff_sqs(self, curve_list, joint_chain_list):
        # Esegui tutti i passaggi    
        for curve, joint_chain in zip(curve_list, joint_chain_list):
            # Side
            side = joint_chain[0].split("_")[0]

            # Nome delle shape delle curve di input
            crv_shape = cmds.listRelatives(curve, shapes=True, path=True)[0]

            # Configura le curve
            md_node, mdc_node, mld_power, ci_node = self._setup_curve(crv_shape, side)
            self._create_undo_set("{}_Puff".format(side), md_node, mdc_node, mld_power, ci_node)

            # Slider list
            self._make_sliders(side, mld_power)

            # Connessioni ai joint, aggiornati con i nuovi nomi
            for jnt in joint_chain:
                cmds.connectAttr("{}.outputX".format(md_node), "{}.scaleY".format(jnt))
                cmds.connectAttr("{}.outputX".format(mdc_node), "{}.scaleX".format(jnt))
                cmds.connectAttr("{}.outputX".format(mdc_node), "{}.scaleZ".format(jnt))

        print("Configurazione completata per SQS cheeks.")

    
    def _duplicate_and_rename_groups_and_controls_puff(self, side, cheek_group, rig_hierarchy):
        """Fa un duplicato di 'base_group' e rinominalo con tutto il suo contenuto col suffisso '_Static'."""
        # Rinomina il gruppo dei controlli 'cheek_group' col suffisso '_Static'.
        static_grp_cheek = cmds.rename(cheek_group, "{}_Static".format(cheek_group))
        
        # Duplicalo come 'driver_grp'
        driver_grp_cheek = cmds.duplicate(static_grp_cheek, n=cheek_group)[0]

        # Pulisci il driver_grp da eventuali constraint
        children_cheek = cmds.listRelatives(driver_grp_cheek, allDescendents=True, fullPath=True, shapes=False)
        for item in children_cheek:
            check_type = cmds.nodeType(item)
            if 'Constraint' in check_type:
                cmds.delete(item)

        # Rinomina tutti i gruppi e controlli del gruppo 'static_grp' col suffisso '_Static', saltando le shapes.
        children_cheek = cmds.listRelatives(static_grp_cheek, allDescendents=True, fullPath=True, shapes=False)
        for item in children_cheek:
            check_type = cmds.nodeType(item)
            if check_type == 'transform':
                tmp = item.split("|")[-1]
                cmds.rename(item, "{}_Static".format(tmp))

        # Metti in gerachia driver_grp e nascondi lo static_grp
        cmds.parent(cheek_group, rig_hierarchy)
        cmds.setAttr("{}.visibility".format(static_grp_cheek), 0)

        # cancella gruppo se vuoto
        check_cheek =  cmds.listRelatives(cheek_group)
        if not check_cheek:
            cmds.delete(cheek_group)

        print("# Duplicato e rinominato tutto il contenuto di '{}' col suffisso '_Static'. #".format(cheek_group))


    def build_puff(self, *args):
        """Build puff rig."""
        # 1. Definizione delle catene di joint
        joint_chains = self._create_puff_joint_chain()
        # 2. Creazione della curva
        curve_list = self._create_linear_curves(joint_chains)
        # 3. Creazione degli IK Spline Handles
        self._create_ik_spline_handles(joint_chains, curve_list)
        # 4. Creazione dei controlli
        driver_joints = self._create_joints(joint_chains)
        ctrl_list = self._create_controls_puff(driver_joints, joint_chains)
        # 5. Creazione curva per lo squash
        sqs_curve_list = self._create_curve_for_skinning(curve_list)
        self._bind_skin_to_curve(sqs_curve_list, driver_joints)
        self._create_single_wire_deformer(curve_list, sqs_curve_list)
        # 6. Calcolo stretch
        self._setup_puff_sqs(curve_list, joint_chains)
        # 7. Cambia shape cointrolli, dusplicali e fai le connessioni
        self._replace_and_remove_shapes(self.my_file_path)
        self._duplicate_and_rename_groups_and_controls_puff("L", "L_Puff_CTRL_GRP", "Jaw_Ctrl")
        self._duplicate_and_rename_groups_and_controls_puff("R", "R_Puff_CTRL_GRP", "Jaw_Ctrl")
        self._cleanup_set_controls("L_Cheek_controls")
        self._cleanup_set_controls("R_Cheek_controls")
        self._connect_driver_to_static("L_Puff_CTRL_GRP", "L_Puff_CTRL_GRP_Static")
        self._connect_driver_to_static("R_Puff_CTRL_GRP", "R_Puff_CTRL_GRP_Static")



        # Clear scene & script variables #
        cmds.select(clear=True)
        for loc in self.puff_locator_list:
            cmds.delete(loc)
        self.puff_locator_list[:] = []
        
        # Update UI #
        cmds.button(self.btn_puff_locator, e = 1, en=True)
        cmds.button(self.btn_puff_build, e = 1, en=False)

        # End message
        print("### L_Puff and R_Puff have been successfully rigged. ###")

    
    def puff_undo(self, *args):
        """Cancella nodi e gruppi della parte delle puff."""
        for idx, (ctrl, locator) in enumerate(zip(("L_Puff", "R_Puff"), ("LOC_L_Puff_02", "LOC_R_Puff_02"))):
            if cmds.objExists("{}_REMOVE_SET".format(ctrl)):
                cmds.select("{}_REMOVE_SET".format(ctrl))
                set_element = cmds.ls(sl=True)
                cmds.delete(set_element)
                
                # End message
                print("## {}_Puff have been successfully rigged. ##".format(ctrl))

            # Se usato in fase di costruzione, cerca anche se ci sono i locator e cancellali
            elif cmds.objExists(locator):
                cmds.delete(locator)
                print("## {}_Puff locators have been successfully rigged. ##".format(ctrl))

            else:
                 cmds.warning("There is no '{}' rig to remove.".format(ctrl))

        cmds.select(clear=True)
        self.puff_locator_list[:] = []

        # Update UI
        cmds.button(self.btn_puff_locator, e=1, en=True)
        cmds.button(self.btn_puff_build, e=1, en=False)
    

    # -----------------------------------------------------------------
    # UI
    # -----------------------------------------------------------------
    def UI(self):
        """Funzione che lancia la UI del modulo delle cheeks."""
        winWidth = 290
        color_ui = (0.95, 0.14, 0.43)
        # color_title = (0.476, 0.07, 0.215)
        color_title = (0.18, 0.18, 0.18)
        
        main_window = "RBW_Cheeks_Rig"

        if cmds.window(main_window, exists = 1):
            cmds.deleteUI(main_window, window = 1)

        ww = cmds.window(main_window, title = "RBW Cheeks Rig", mxb = 0, sizeable=True, resizeToFitChildren=True)

        # Get a pointer and convert it to Qt Widget object
        qw = omui.MQtUtil.findWindow(ww)
        widget = wrapInstance(int(qw), QWidget)

        # Create a QIcon object
        iconpath = os.path.join(self.image_path, "RainbowCGI_icona.ico")

        # Assign the icon
        icon = QIcon(iconpath)
        widget.setWindowIcon(icon)
        
        # Main layout
        col = cmds.columnLayout(co=("both", 5), adjustableColumn=True)
        
        # Carica l'immagine delle cheeks
        img = os.path.join(self.image_path, "cheek_icon.png")
        cmds.text(h = 10, l = "")
        cmds.iconTextButton(style="iconOnly", image1=img, p=col, w=100)
        cmds.text(h = 10, l = "")
        cmds.setParent("..")

        # CHEEK MOUTH -----------------------------------------------------
        cmds.frameLayout(label='CHEEK MOUTH', lw=60, bgc=color_title)
        cmds.frameLayout(label='1. Select a side', marginHeight=10, bgc = color_ui)
        cmds.columnLayout(adjustableColumn=True)
        self.radioChecked = cmds.radioButtonGrp(labelArray2=['Left', 'Right'], on1=partial(self.which_side, 'L') , on2=partial(self.which_side, 'R') , numberOfRadioButtons=2, columnWidth2=(150, 150), columnAttach2=('left', 'right'), columnOffset2=(80, 80))
        cmds.setParent("..")
        cmds.setParent("..")

        # Define cheeks curve
        cmds.frameLayout(label="2. Select the curve, then click 'Select curve'", marginHeight=10, bgc = color_ui)
        cmds.rowLayout(numberOfColumns = 2, adjustableColumn = 1)
        self.btn_select_curve = cmds.button(w = ((winWidth / 2) - 2), h=30, l = "Select Curve", c = self.select_curve)
        self.btn_undo_curve = cmds.button(w = ((winWidth / 2) - 2), h=30, l = "Undo", c = self.select_curve_undo, en = 0)

        cmds.setParent("..")
        # cmds.text(h = 5, l = "")
        self.txtf_curve = cmds.textField(w = winWidth, h=30, ed = 0, ebg=True, bgc=(0.22, 0.22, 0.22), font="fixedWidthFont")
        cmds.setParent("..")
        
        # Choose the number of joints
        cmds.frameLayout(label="3. Choose the number of joints", marginHeight=10, bgc = color_ui)
        self.input_joints = cmds.intSliderGrp("num_jnts", field=True, label="n. Joints:", minValue=2, maxValue=10, value=4, cw=[(1, 50), (2, 40)], cat=[(2, 'both', 10), (3, 'right', 10)])
        cmds.setParent("..")
        
        cmds.text(h = 5, l = "")

        # Build final rig
        self.btnBuild = cmds.button(w = winWidth, h = 40, l = "BUILD CHEEK MOUTH", c = self.build_cheek_mouth, bgc = color_ui)
        
        cmds.separator(h = 15, w = winWidth, style = "in")
        
        # CHEEK -----------------------------------------------------------
        cmds.frameLayout(label='CHEEK', lw=60, bgc=color_title)
        cmds.frameLayout(label="Import and place the locators, then click 'Build Cheeks'", marginHeight=10, bgc=color_ui)
        cmds.rowLayout(numberOfColumns = 3, adjustableColumn = 2)
        self.btn_cheek_locator = cmds.button(w=((winWidth / 4) - 2), h = 40, l="LOC", c=self.create_cheek_locator_tmp)
        self.btn_cheek_build = cmds.button(w=((winWidth / 2) - 2), h = 40, l="BUILD CHEEKS", c=self.build_cheek, bgc = color_ui, en=False)
        self.btn_cheek_undo = cmds.button(w=((winWidth / 4) - 2), h = 40, l="Undo", c=self.cheek_undo)
        
        cmds.setParent("..")
        cmds.separator(h = 15, w = winWidth, style = "in")
        
        # CHEEK PUFF ------------------------------------------------------
        cmds.frameLayout( label='PUFF', lw=60, bgc=color_title)
        cmds.frameLayout(label="Import and place the locators, then click 'Build Puff'", marginHeight=10, bgc=color_ui)
        cmds.rowLayout(numberOfColumns = 3, adjustableColumn = 2)
        self.btn_puff_locator = cmds.button(w=((winWidth / 4) - 2), h = 40, l="LOC", c=self.create_puff_locator)
        self.btn_puff_build = cmds.button(w=((winWidth / 2) - 2), h = 40, l="BUILD PUFF", c=self.build_puff, bgc = color_ui, en=False)
        self.btn_puff_undo = cmds.button(w=((winWidth / 4) - 2), h = 40, l="Undo", c=self.puff_undo)
        
        cmds.setParent("..")

        
        cmds.showWindow(main_window)


autoloadCheeks = Cheeks()
autoloadCheeks.UI()