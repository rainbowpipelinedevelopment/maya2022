import maya.cmds as cmds


def mirror_skinning_driver_curves(*args):
    """ Mirrora le influenze dei joints delle curve driver per i moduli Eyelid, EyeSocket e Brows. 
        Selezionare la curva di partenza e poi la curva target."""
    # select driver curve
    crv_selected = cmds.ls(sl=True)[0]
    tokens = crv_selected.split("_")

    # dfetermine the target curve
    if tokens[0] == "L":
        side_selected = "L"
        side_target = "R"
    elif tokens[0] == "R":
        side_selected = "R"
        side_target = "L"
    else:
        cmds.error("Cannot find the right side to mirror.")

    crv_target = side_target
    for idx, item in enumerate(tokens):
        if idx == 0:
            pass
        else:
            crv_target = f"{crv_target}_{item}"

    # get sk node
    sk_node_list = []
    for crv in (crv_selected, crv_target):
        info_list = cmds.listHistory(crv)
        for item in info_list:
            if cmds.objectType(item) == "skinCluster":
                sk_node_list.append(item)
          
    # get info number of cv
    info_spans = cmds.getAttr(f"{crv_selected}.spans")
    info_degree = cmds.getAttr(f"{crv_selected}.degree")
    info_cv = info_spans + info_degree

    # get info jnt influence per cvs of left curbe
    if sk_node_list[0]:
        lista_info_sk = []
        for cv in range(info_cv):
            cmds.select(f"{crv_selected}.cv[{cv}]", r=True)
            jnts_list = cmds.skinPercent(sk_node_list[0], q=True, t=None)
            influence_value_list = cmds.skinPercent(sk_node_list[0], q=True, v=True)
            tmp = [jnts_list, influence_value_list]
            lista_info_sk.append(tmp)

    cmds.select(clear=True)


    for idx, cv_influences in enumerate(lista_info_sk):
        info_influence = []
        cv_curva = f"{crv_selected}[{idx}]"
        print(f"{cv_curva}")

        for jnt, value in zip(cv_influences[0], cv_influences[1]):
            print(f"{jnt}: {value}")
            jnt_target = jnt.replace(jnt[0], side_target)
            tmp_info = (jnt_target, value)
            info_influence.append(tmp_info)

        print(info_influence)
        print("===============")
        cmds.skinPercent(sk_node_list[1], f"{crv_target}.cv[{idx}]", transformValue=info_influence)

    print(f"### Mirrorato influenze joints da '{crv_selected}' a '{crv_target}' ###")

mirror_skinning_driver_curves()