# -*- coding: utf-8 -*-

from maya import cmds, OpenMaya
import math, os, sys
from functools import partial
from imp import reload
import depts.rigging.tools.generic.Face_RIG.scripts.matrix_module as mtx
reload(mtx)


class AER :
    """Automatically rig eyelashes."""
    
    def __init__(self):
        self.eyeSide = None
        self.eyelashes_module = None
        self.joints_grp = None
        self.controls_grp = None
        self.static_grp = None
        self.joints_list = []
        self.ctrls_list = []
        self.ctrls_static_list = []
        self.master_grp_list = []
        self.ctrls_eyelid_list = ["Eyelid_UpInn_Ctrl", "Eyelid_Up_Ctrl", "Eyelid_UpOut_Ctrl", "Eyelid_LowOut_Ctrl", "Eyelid_Low_Ctrl", "Eyelid_LowInn_Ctrl"]
        # -----------------------------------------------------------------
        # Path
        # -----------------------------------------------------------------
        localpipe = os.getenv("LOCALPY")
        myPath = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "Face_RIG")
        scene_path = os.path.join(myPath, "scenes", "controls_shape.ma")
        self.my_file_path = scene_path


    # -----------------------------------------------------------------
    # Parte di costruzione del modulo eyelashes
    # -----------------------------------------------------------------
    def _check_selection(self):
        """Check iniziale sulla selezione."""
        self.sel = cmds.ls(sl=True)
        
        if not self.sel:
            cmds.error("Seleziona il modulo delle eyelid.")
        elif self.sel[0] not in ("L_Eyelid_RIG_GRP", "R_Eyelid_RIG_GRP"):
            cmds.warning("Seleziona il modulo delle eyelid.")


    def _duplicate_eyelid_module(self, selection):
        """Duplica il modulo delle eyelids selezionato e rinominalo come eyelashes."""
        if selection:
            self.eyeSide = selection[0].split("_")[0]
            name_grp = selection[0].replace("Eyelid", "Eyelashes")
            self.eyelashes_module = cmds.duplicate(selection[0], name=name_grp)[0]


    def _delete_unused_groups(self, side, module):
        """Elimina tutti gruppi tranne quello dei ctrl joints."""
        children = cmds.listRelatives(module, fullPath=True, allDescendents=True)
        
        for item in children:
            token = item.split("|")[-1]
            name = token.replace("Eyelid", "Eyelashes")
            cmds.rename(item, name)

        cmds.delete("{}_Eyelashes_JNT_GRP".format(side))
        cmds.parent("{}_Eyelashes_CTRL_JNT_GRP".format(side), "{}_Eyelashes_RIG_GRP".format(side))
        self.joints_grp = cmds.rename("{}_Eyelashes_CTRL_JNT_GRP".format(side), "{}_Eyelashes_JNT_GRP".format(side))
        cmds.delete("{}_Eyelashes_Base_RIG_GRP".format(side))
        cmds.delete("{}_Eyelashes_CTRL_GRP_Static".format(side))


    def _delete_unused_joints(self, side, joints_grp):
        """Pulisci il gruppo dai joint dai corner e dai constraint."""
        children = cmds.listRelatives(joints_grp, allDescendents=True)
        for item in children:
            check_type = cmds.nodeType(item)
            if item in ("{}_Eyelashes_CornerInn_jnt".format(side), "{}_Eyelashes_CornerOut_jnt".format(side)):
                cmds.delete(item)
            if 'Constraint' in check_type:
                cmds.delete(item)


    def _rename_joints(self, side, joints_grp):
        """Rinomina i joint."""
        name_list = ("Eyelashes_Up_01", "Eyelashes_Up_02", "Eyelashes_Up_03", "Eyelashes_Low_03", "Eyelashes_Low_02", "Eyelashes_Low_01")

        children = cmds.listRelatives(joints_grp, allDescendents=True)
        
        for jnt, name in zip(children, name_list):
            new_jnt = cmds.rename(jnt, "{}_{}_jnt".format(side, name))
            self.joints_list.append(new_jnt)


    def _create_controls(self, side, module, joints_list):
        """Crea i controlli e li mette in gerarchia."""
        self.controls_grp = cmds.group(name="{}_Eyelashes_CTRL_GRP".format(side), em=True)
        cmds.parent(self.controls_grp, module)

        ctrl_eyelid_list = (f"{side}_Eyelid_UpInn_Ctrl", f"{side}_Eyelid_Up_Ctrl", f"{side}_Eyelid_UpOut_Ctrl", f"{side}_Eyelid_LowOut_Ctrl", f"{side}_Eyelid_Low_Ctrl", f"{side}_Eyelid_LowInn_Ctrl")

        for jnt, ctrl_eyelid in zip(joints_list, ctrl_eyelid_list):
            ctrl_name = jnt.replace("jnt", "Ctrl")
            ctrl = cmds.circle(r = 0.15, n=ctrl_name)[0]
            self.ctrls_list.append(ctrl)
            for shape in cmds.listRelatives(ctrl, s=True, f=True) or []:
                shapeRenamed = cmds.rename(shape, "{}Shape".format(ctrl_name))

            master_grp = cmds.group(name = "{}_MASTER_Grp".format(ctrl_name), em=True)
            self.master_grp_list.append(master_grp)
            matrix_grp = cmds.group(name = "{}_MATRIX_Grp".format(ctrl_name), em=True)

            cmds.parent(ctrl, matrix_grp)
            cmds.parent(matrix_grp, master_grp)
            cmds.parent(master_grp, self.controls_grp)
            
            # orienta il gruppo master al controllo delle eyelid corrispondente
            mtt = cmds.matchTransform(master_grp, ctrl_eyelid)
            
            vis = cmds.setAttr(ctrl + ".visibility", lock=True, keyable=False, channelBox=False)
            shp = cmds.setAttr(ctrl + "Shape.overrideEnabled", 1)
            col = cmds.setAttr(ctrl + "Shape.overrideColor", 18)


    def _replace_and_remove_shapes(self, path):
        # lista contenente i controlli da sostituire
        control_pairs = [
            ('L_Eyelashes_Up_01_Ctrl_REF', 'L_Eyelashes_Up_01_Ctrl'),
            ('L_Eyelashes_Up_02_Ctrl_REF', 'L_Eyelashes_Up_02_Ctrl'),
            ('L_Eyelashes_Up_03_Ctrl_REF', 'L_Eyelashes_Up_03_Ctrl'),
            ('L_Eyelashes_Low_03_Ctrl_REF', 'L_Eyelashes_Low_03_Ctrl'),
            ('L_Eyelashes_Low_02_Ctrl_REF', 'L_Eyelashes_Low_02_Ctrl'),
            ('L_Eyelashes_Low_01_Ctrl_REF', 'L_Eyelashes_Low_01_Ctrl'),
            ('R_Eyelashes_Up_01_Ctrl_REF', 'R_Eyelashes_Up_01_Ctrl'),
            ('R_Eyelashes_Up_02_Ctrl_REF', 'R_Eyelashes_Up_02_Ctrl'),
            ('R_Eyelashes_Up_03_Ctrl_REF', 'R_Eyelashes_Up_03_Ctrl'),
            ('R_Eyelashes_Low_03_Ctrl_REF', 'R_Eyelashes_Low_03_Ctrl'),
            ('R_Eyelashes_Low_02_Ctrl_REF', 'R_Eyelashes_Low_02_Ctrl'),
            ('R_Eyelashes_Low_01_Ctrl_REF', 'R_Eyelashes_Low_01_Ctrl')
            ]

        # print(path)
        # importa scena con i controlli
        cmds.file(path, i=True, mergeNamespacesOnClash=True, namespace=':', gr=True, gn="GRP_shapes_REF")

        # itera attraverso le coppie di controlli e esegui la sostituzione
        for source_ctrl, target_ctrl in control_pairs:
            try:
                # Ottieni e elimina le shape esistenti nel controllo di destinazione
                target_shapes = cmds.listRelatives(target_ctrl, shapes=True)
                name_target_shape = target_shapes
                if target_shapes:
                    cmds.delete(target_shapes)

                # Ottieni la shape del controllo sorgente
                new_source_ctrl = cmds.duplicate(source_ctrl, n="{}_NEW".format(source_ctrl))
                source_shapes = cmds.listRelatives(source_ctrl, shapes=True)
                if source_shapes:
                    for source_shape in source_shapes:
                        cmds.parent(source_shape, target_ctrl, r=True, shape=True)
                        cmds.rename(source_shape, name_target_shape[0])
            except:
                pass

        # elimina il gruppo dei controlli importati
        grp_del = "GRP_shapes_REF"
        cmds.delete("GRP_shapes_REF")


    def _duplicate_and_rename_groups_and_controls(self, base_group, rig_hierarchy):
        """Fa un duplicato di 'base_group' e rinominalo con tutto il suo contenuto col suffisso '_Static'."""
        # Rinomina il gruppo dei controlli 'base_group' col suffisso '_Static'.
        self.static_grp = cmds.rename(base_group, "{}_Static".format(base_group))
        
        # Duplicalo come 'driver_grp'
        driver_grp = cmds.duplicate(self.static_grp, n=base_group)[0]

        # Pulisci il driver_grp da eventuali constraint
        children = cmds.listRelatives(driver_grp, allDescendents=True, fullPath=True, shapes=False)
        for item in children:
            check_type = cmds.nodeType(item)
            if 'Constraint' in check_type:
                cmds.delete(item)

        # Rinomina tutti i gruppi e controlli del gruppo 'static_grp' col suffisso '_Static', saltando le shapes.
        children = cmds.listRelatives(self.static_grp, allDescendents=True, fullPath=True, shapes=False)
        for item in children:
            check_type = cmds.nodeType(item)
            if check_type == 'transform':
                tmp = item.split("|")[-1]
                cmds.rename(item, "{}_Static".format(tmp))

        # Metti in gerachia driver_grp e nascondi lo static_grp
        # cmds.parent(driver_grp, rig_hierarchy)
        cmds.setAttr("{}.visibility".format(self.static_grp), 0)

        print("# Duplicato e rinominato tutto il contenuto di '{}' col suffisso '_Static'. #".format(base_group))


    def _parent_joints_to_controls(self, joints_list, ctrl_list):
        """Parenta i joints dentro i controlli static."""
        ctrls_static_list = []

        for ctrl in ctrl_list:
            ctrl_static = "{}_Static".format(ctrl)
            ctrls_static_list.append(ctrl_static)

        self.ctrls_static_list = ctrls_static_list

        for jnt, ctrl_static in zip(joints_list, ctrls_static_list):
            cmds.parent(jnt, ctrl_static)


    def _parent_lashes_and_eyelids(self, side, master_grp_list, ctrls_eyelid_list):
        """Parenta i controlli eyelashes dentro i controlli eyelids."""
        for grp_master, ctrl_eyelid in zip(master_grp_list, ctrls_eyelid_list):
            cmds.parent(grp_master, "{}_{}".format(side, ctrl_eyelid))
            # cmds.parent("{}_Static".format(grp_master), "{}_{}_Static".format(side, ctrl_eyelid))


    def _connect_driver_to_static(self, ctrls_list, ctrls_static_list):
        """Crea connessioni dirette tra i controlli e gruppi Matrix di driver e static """
        # '_Ctrl' --> '_Ctrl_Static'
        for static_ctrl, driver_ctrl in zip(ctrls_static_list, ctrls_list):
            # print("{} --> {}".format(driver_ctrl, static_ctrl))
            driver_attrs = cmds.listAttr(driver_ctrl, keyable=True) or []
            for attr in driver_attrs:
                if cmds.attributeQuery(attr, node=static_ctrl, exists=True) and cmds.getAttr(driver_ctrl + '.' + attr, keyable=True):
                    cmds.connectAttr("{}.{}".format(driver_ctrl, attr), "{}.{}".format(static_ctrl, attr), force=True)

        print("# Connessioni dirette completate tra driver e static. #")


    def _populate_sets(self, type_item, side, obj_list):
        """Crea i set relativi alle eyelashes."""
        if type_item is "joints":
            module = "Joints"
            part_name = "Joints"
            set_name = "jointsForSkin"
        elif type_item is "controls":
            module = "Controls"
            part_name = "controls"
            set_name = "controls"
        else:
            cmds.error("Attibuto errato passato a _populate_sets.")

        # Controlla se il set contenente i controlli esiste, altrimenti crealo
        if not cmds.objExists("FACE_MODULES_{}".format(module)):
            module_set = cmds.sets(empty=True, name=("FACE_MODULES_{}".format(module)))
        else:
            module_set = "FACE_MODULES_{}".format(module)

        # Controlla se il set della parte di rig esiste, altrimenti crealo
        if not cmds.objExists("HeadEyes_{}".format(part_name)):
            rigpart_set = cmds.sets(empty=True, name=("HeadEyes_{}".format(part_name)))
            # Parenta il set della parte di rig dentro al set globale
            cmds.sets(rigpart_set, edit=True, forceElement=module_set)
        else:
            rigpart_set = "HeadEyes_{}".format(part_name)

        # Controlla se il set del modulo esiste, altrimenti crealo
        if not cmds.objExists("{}_Eyelashes_{}".format(side, set_name)):
            set_items = cmds.sets(empty=True, name=("{}_Eyelashes_{}".format(side, set_name)))
            # Parenta il set del modulo dentro al set globale
            cmds.sets(set_items, edit=True, forceElement=rigpart_set)
        else:
            set_items = "{}_Eyelashes_{}".format(side, set_name)

        for obj in obj_list:
            cmds.sets(obj, edit=True, forceElement=set_items)

        return set_items


    def _cleanup_rig(self, eyelashes_module):
        """Pulizia dei gruppi vuoti del modulo eyelashes."""
        children = cmds.listRelatives(eyelashes_module)
        for grp in children:
            token = grp.split("_")
            if token[-2] in ['JNT', 'CTRL']:
                print(token)
                cmds.delete(grp)

        # # Imparenta tutto sotto "Face_RIG_Grp"
        # if cmds.objExists(eyelashes_module) and cmds.objExists("Face_RIG_Grp"):
        #     cmds.parent(eyelashes_module, "Face_RIG_Grp")


    def _rename_ctrl_shapes(self, set_ctrl):
        """Data una selezione di controlli, rinomina le shapes in base al nome del controllo."""
        cmds.select(clear=True)
        cmds.select(set_ctrl)
        sel = cmds.ls(sl=True)
        cmds.select(clear=True)
        
        for ctrl in sel:
            shapes = cmds.listRelatives(ctrl, shapes=True, pa=True)
            for shape in shapes:
                new_name = "{}Shape".format(ctrl)
                cmds.rename(shape, new_name)

        print("### Le shapes dei controlli sono state rinominate correttamente. ###")
    

    # -----------------------------------------------------------------
    # Funzioni lanciate dalla UI
    # -----------------------------------------------------------------
    def buildRig(self, *args):
        """Build eyelashes rig."""
        self._check_selection()
        self._duplicate_eyelid_module(self.sel)
        self._delete_unused_groups(self.eyeSide, self.eyelashes_module)
        self._delete_unused_joints(self.eyeSide, self.joints_grp)
        self._rename_joints(self.eyeSide, self.joints_grp)
        self._create_controls(self.eyeSide, self.eyelashes_module, self.joints_list)
        self._replace_and_remove_shapes(self.my_file_path)
        self._duplicate_and_rename_groups_and_controls(self.controls_grp, self.eyelashes_module)
        self._parent_joints_to_controls(self.joints_list, self.ctrls_list)
        self._parent_lashes_and_eyelids(self.eyeSide, self.master_grp_list, self.ctrls_eyelid_list)
        self._connect_driver_to_static(self.ctrls_list, self.ctrls_static_list)
        self._populate_sets("joints", self.eyeSide, self.joints_list)
        set_ctrls = self._populate_sets("controls", self.eyeSide, self.ctrls_list)
        self._cleanup_rig(self.eyelashes_module)
        self._rename_ctrl_shapes(set_ctrls)

        # Clear scene & script variables #
        self.eyelashes_module = None
        self.joints_grp = None
        self.controls_grp = None
        self.static_grp = None
        self.joints_list[:] = []
        self.ctrls_list[:] = []
        self.ctrls_static_list[:] = []
        self.master_grp_list[:] = []

        cmds.select(clear=True)
    
        # End message
        print("### {}_Eyelashes have been successfully rigged. ###".format(self.eyeSide))
        
        self.eyeSide = None
