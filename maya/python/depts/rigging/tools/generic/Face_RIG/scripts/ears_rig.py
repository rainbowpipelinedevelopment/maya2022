import maya.cmds as cmds

def main_rig_process():
    def create_and_parent_joints(locator_names):
        joint_names = {}
        for loc in locator_names:
            position = cmds.xform(loc, query=True, worldSpace=True, translation=True)
            joint_name = loc.replace("LOC_", "") + "_jnt"
            cmds.select(clear=True)
            joint_names[loc] = cmds.joint(name=joint_name, position=position)
            temp_constraint = cmds.orientConstraint(loc, joint_name)
            cmds.delete(temp_constraint)
        cmds.parent(joint_names[locator_names[1]], joint_names[locator_names[0]])
        cmds.parent(joint_names[locator_names[2]], joint_names[locator_names[0]])
        cmds.select(clear=True)

    def create_control_and_group(joint_name, control_name, parent):
        cmds.select(clear=True)
        control = cmds.circle(name=control_name, normal=(0, 1, 0), radius=1.5)[0]
        cmds.setAttr("{}.visibility".format(control), lock=True, keyable=False, channelBox=False)

        cmds.delete(cmds.parentConstraint(joint_name, control))
        grp = cmds.group(name=control_name + "_Grp", empty=True)
        cmds.delete(cmds.parentConstraint(control, grp))
        cmds.parent(control, grp)
        if parent:
            cmds.parentConstraint(control, joint_name, maintainOffset=True)
            cmds.scaleConstraint(control, joint_name)
        return control, grp

    def create_controls_for_joints(joint_names):
        control_dict = {}
        for jnt in joint_names:
            control_static_name = jnt.replace("_jnt", "_Ctrl_Static")
            control_static, grp_static = create_control_and_group(jnt, control_static_name, parent=True)
            control_dict[jnt] = {"static": (control_static, grp_static)}
            control_name = jnt.replace("_jnt", "_Ctrl")
            control, grp = create_control_and_group(jnt, control_name, parent=False)
            control_dict[jnt]["dynamic"] = (control, grp)
        parent_controls(control_dict)
        return control_dict

    def parent_controls(control_dict):
        ear_joints = ["L_Ear_jnt", "L_EarLow_jnt", "L_EarTop_jnt", "R_Ear_jnt", "R_EarLow_jnt", "R_EarTop_jnt"]
        for ear in ear_joints:
            if ear in control_dict:
                top = ear.replace("Ear", "EarTop")
                low = ear.replace("Ear", "EarLow")
                if top in control_dict and low in control_dict:
                    cmds.parent(control_dict[top]["dynamic"][1], control_dict[ear]["dynamic"][0])
                    cmds.parent(control_dict[low]["dynamic"][1], control_dict[ear]["dynamic"][0])
                top_static = ear.replace("Ear", "EarTop")
                low_static = ear.replace("Ear", "EarLow")
                if top_static in control_dict and low_static in control_dict:
                    cmds.parent(control_dict[top_static]["static"][1], control_dict[ear]["static"][0])
                    cmds.parent(control_dict[low_static]["static"][1], control_dict[ear]["static"][0])

    def organize_ears_rig():
        ears_joints_grp = cmds.group("L_Ear_jnt", "R_Ear_jnt", name="Ears_Joints_Grp")
        cmds.setAttr(ears_joints_grp + ".visibility", 0)
        ears_controls_static_grp = cmds.group("L_Ear_Ctrl_Static_Grp", "R_Ear_Ctrl_Static_Grp", name="Ears_Controls_Static_Grp")
        cmds.setAttr(ears_controls_static_grp + ".visibility", 0)
        ears_rig_grp = cmds.group(ears_joints_grp, ears_controls_static_grp, name="Ears_RIG_Static_Grp")
        cmds.parent("L_Ear_Ctrl_Grp", "Head_SQSLow_01_jnt")
        cmds.parent("R_Ear_Ctrl_Grp", "Head_SQSLow_01_jnt")
        if cmds.objExists("LOC_Ears_Grp"):
            cmds.delete("LOC_Ears_Grp")
        # Imparenta 'EyeMaster_Ctrl_Grp' sotto 'Face_RIG_Grp'
        if cmds.objExists("Ears_RIG_Static_Grp") and cmds.objExists("Face_RIG_Grp"):
            cmds.parent("Ears_RIG_Static_Grp", "Face_RIG_Grp")

    def process_multiple_pairs(control_pairs):
        # Esegui la funzione replace_and_remove per ogni coppia
        for source, target in control_pairs:
            replace_and_remove(source, target)
        if cmds.objExists("Ear_Controls_Grp_REF"):
            cmds.delete("Ear_Controls_Grp_REF")

    # Creazione e imparentamento dei Joint
    locator_names_left = ["LOC_L_Ear", "LOC_L_EarLow", "LOC_L_EarTop"]
    locator_names_right = ["LOC_R_Ear", "LOC_R_EarLow", "LOC_R_EarTop"]
    create_and_parent_joints(locator_names_left)
    create_and_parent_joints(locator_names_right)

    # Creazione dei controlli per i Joint
    joint_names_left = ["L_Ear_jnt", "L_EarLow_jnt", "L_EarTop_jnt"]
    joint_names_right = ["R_Ear_jnt", "R_EarLow_jnt", "R_EarTop_jnt"]
    controls_left = create_controls_for_joints(joint_names_left)
    controls_right = create_controls_for_joints(joint_names_right)

    # Organizzazione del rig delle orecchie
    organize_ears_rig()

    control_pairs = [
        ("L_EarTop_Ctrl_REF", "L_EarTop_Ctrl"),
        ("R_EarTop_Ctrl_REF", "R_EarTop_Ctrl"),
        ("L_EarLow_Ctrl_REF", "L_EarLow_Ctrl"),
        ("R_EarLow_Ctrl_REF", "R_EarLow_Ctrl"),
        ("L_Ear_Ctrl_REF", "L_Ear_Ctrl"),
        ("R_Ear_Ctrl_REF", "R_Ear_Ctrl"),
        # Altre coppie qui se necessario
    ]

    # Processa le coppie di controlli
    process_multiple_pairs(control_pairs)
    
    def connect_controls():
        """Connette i controlli 'Head_*SQS_Ctrl' con '*SQS_Ctrl'."""
        ears_controls = ["L_Ear_Ctrl", "L_EarTop_Ctrl", "L_EarLow_Ctrl", "R_Ear_Ctrl", "R_EarTop_Ctrl", "R_EarLow_Ctrl"]
        ears_static_controls = ["L_Ear_Ctrl_Static", "L_EarTop_Ctrl_Static", "L_EarLow_Ctrl_Static", "R_Ear_Ctrl_Static", "R_EarTop_Ctrl_Static", "R_EarLow_Ctrl_Static"]

        for head_ctrl, base_ctrl in zip(ears_controls, ears_static_controls):
            # Connette le trasformazioni di translate, rotate e scale
            cmds.connectAttr(head_ctrl + ".translate", base_ctrl + ".translate", f=True)
            cmds.connectAttr(head_ctrl + ".rotate", base_ctrl + ".rotate", f=True)
            cmds.connectAttr(head_ctrl + ".scale", base_ctrl + ".scale", f=True)
    connect_controls()

    # Funzioni di Utility per creazione set
    def create_joints_set(module, rigpart, jnt_list):
        # Creates a set containing the joints for skinning
        joints_for_skin = []
        joints_for_skin.extend(jnt_list)
        
        if not cmds.objExists("FACE_MODULES_Joints"):
            module_joints_set = cmds.sets(empty=True, name=("FACE_MODULES_Joints"))
        else:
            module_joints_set = "FACE_MODULES_Joints"

        # Controlla se il set del modulo di rig dei joints esiste, altrimenti crealo
        if not cmds.objExists("{}_Joints".format(module)): # 'HeadEyes' -'HeadMouth' - 'HeadSquash'
            section_joints_set = cmds.sets(empty=True, name=("{}_Joints".format(module)))
            # Parenta il set della parte di rig dentro al set globale
            cmds.sets(section_joints_set, edit=True, forceElement=module_joints_set)
        else:
            section_joints_set = "{}_Joints".format(module)

        # Controlla se il set della parte di rig dei joints esiste, altrimenti crealo
        if not cmds.objExists("{}_jointsForSkin".format(rigpart)):
            rigpart_joints_set = cmds.sets(empty=True, name=("{}_jointsForSkin".format(rigpart)))
            # Parenta il set della parte di rig dentro al set globale
            cmds.sets(rigpart_joints_set, edit=True, forceElement=section_joints_set)
        else:
            rigpart_joints_set = "{}_jointsForSkin".format(rigpart)


        for jnt in joints_for_skin:
            cmds.sets(jnt, edit=True, forceElement=rigpart_joints_set)

    def create_controls_set(module, rigpart, ctrl_list):
        # Crea il set contenente i controlli creati
        if not cmds.objExists("FACE_MODULES_Controls"):
            module_controls_set = cmds.sets(empty=True, name=("FACE_MODULES_Controls"))
        else:
            module_controls_set = "FACE_MODULES_Controls"

        if not cmds.objExists("{}_controls".format(module)):
            module_ctrl_set = cmds.sets(empty=True, name=("{}_controls".format(module)))
            # Parenta il set del modulo dentro al set globale
            cmds.sets(module_ctrl_set, edit=True, forceElement=module_controls_set)
        else:
            module_ctrl_set = "{}_controls".format(module)
            
        if not cmds.objExists("{}_controls".format(rigpart)):
            rigpart_ctrl_set = cmds.sets(empty=True, name=("{}_controls".format(rigpart)))
            # Parenta il set del modulo dentro al set globale
            cmds.sets(rigpart_ctrl_set, edit=True, forceElement=module_ctrl_set)
        else:
            rigpart_ctrl_set = "{}_controls".format(rigpart)

        for ctrl in ctrl_list:
            cmds.sets(ctrl, edit=True, forceElement=rigpart_ctrl_set)

        return module_ctrl_set

    def rename_ctrl_shapes(set_ctrl):
        """Data una selezione di controlli, rinomina le shapes in base al nome del controllo."""
        cmds.select(clear=True)
        cmds.select(set_ctrl)
        sel = cmds.ls(sl=True)
        cmds.select(clear=True)
        
        for ctrl in sel:
            shapes = cmds.listRelatives(ctrl, shapes=True, pa=True)
            for shape in shapes:
                new_name = "{}Shape".format(ctrl)
                cmds.rename(shape, new_name)

        print("### Le shapes dei controlli sono state rinominate correttamente. ###")

    create_joints_set("HeadMouth", "Ears", ["L_Ear_jnt", "L_EarTop_jnt", "L_EarLow_jnt", "R_Ear_jnt", "R_EarTop_jnt", "R_EarLow_jnt"])
    controls_set = create_controls_set("HeadMouth", "Ears", ["L_Ear_Ctrl", "L_EarTop_Ctrl", "L_EarLow_Ctrl", "R_Ear_Ctrl", "R_EarTop_Ctrl", "R_EarLow_Ctrl"])
    rename_ctrl_shapes(controls_set)


# Esecuzione della funzione principale
main_rig_process()
