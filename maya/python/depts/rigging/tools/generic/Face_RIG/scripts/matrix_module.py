# -*- coding: utf-8 -*-

import maya.cmds as cmds

class MatrixConstraint:
    """ Collega i vari moduli del facciale al rig con delle matrici, e setta i vari attributi."""

    def __init__(self, selection, **values):
        """ Elenco delle varie selezioni su cui creare le matrici."""
        self.selection = selection

        x = len(selection)
        amount = (0,0,0)
        tmp_list = []
        
        for i in range(x):
            tmp_list.append(amount)

        self.t_values = values.get('t', tmp_list)
        self.r_values = values.get('r', tmp_list)


    # -----------------------------------------------------------------
    # Parte di utilities per la creazione delle matrici
    # -----------------------------------------------------------------
    def _get_object_names_from_selection(self, selected_objects):
        """Prende i nomi degli oggetti selezionati e crea l'attributo offset sull'oggetto 2."""
        # Assicurati di avere selezionati esattamente tre oggetti
        if len(selected_objects) != 3:
            cmds.error("Seleziona esattamente tre oggetti.")
        
        # Ottieni i nomi degli oggetti selezionati e crea un array di stringhe
        self.object_names = [obj.split('|')[-1] for obj in selected_objects]
        
        # Aggiungi un attributo "offset" all'oggetto 2
        self.offset_attr_name = "offsetAttr_{}".format(self.object_names[0])
        if not cmds.attributeQuery(self.offset_attr_name, node=self.object_names[1], exists=True):
            cmds.addAttr(self.object_names[1], longName=self.offset_attr_name, attributeType="matrix")


    def _create_matrix_node_from_objects(self, object_names):
        """Crea il nodo di matrice temporaneo basato sul nome dell'oggetto 01 e lo connette agli oggetti."""
        if object_names:
            # Crea il nome per la matrice basato sul nome dell'oggetto 01
            matrix_name_tmp = "MTX_{}_Distance_toREMOVE".format(object_names[0])
            
            # Crea un nodo di matrice con il nome appropriato
            self.matrix_node_tmp = cmds.createNode("multMatrix", name=matrix_name_tmp)
            
            # Connetti il gruppo Matrix utilizzando il suo WorldMatrix
            cmds.connectAttr("{}.worldMatrix[0]".format(object_names[1]), "{}.matrixIn[0]".format(self.matrix_node_tmp))
            
            # Connetti l'oggetto che guida utilizzando il suo WorldInverseMatrix
            cmds.connectAttr("{}.worldInverseMatrix[0]".format(object_names[0]), "{}.matrixIn[1]".format(self.matrix_node_tmp))
            
        else:
            cmds.warning("Oggetto non trovato.")


    def _matrix_connect_attribute(self, object_names, matrix_node_tmp, offset_attr_name):
        """Collega il MatrixSum dal nodo di matroce temporaneo all'attributo creato dell'oggetto 2."""
        if matrix_node_tmp:
            # Collega MatrixSum all'attributo creato su oggetto 2
            cmds.connectAttr("{}.matrixSum".format(matrix_node_tmp), "{}.{}".format(object_names[1], offset_attr_name))


    def _create_multiply_nodes(self, object_names):
        """Crea i vari nodi di multiplyDivide utili a settare l'influenza sul controllo."""
        # Crea il nome per i nodi di multiply basato sul nome dell'oggetto 01
        self.multiply_nodes = []

        mlp_translate_name = "MLP_{}_to_{}_Translate".format(object_names[0], object_names[1])
        mlp_rotate_name = "MLP_{}_to_{}_Rotate".format(object_names[0], object_names[1])
        mlp_scale_name = "MLP_{}_to_{}_Scale".format(object_names[0], object_names[1])

        # Crea i nodi di multiplyDivide
        self.mlp_translate_node = cmds.createNode("multiplyDivide", name=mlp_translate_name)
        self.mlp_rotate_node = cmds.createNode("multiplyDivide", name=mlp_rotate_name)
        self.mlp_scale_node = cmds.createNode("multiplyDivide", name=mlp_scale_name)  

        # Crea lista dei nodi di multiply
        self.multiply_nodes.append(self.mlp_translate_node) 
        self.multiply_nodes.append(self.mlp_rotate_node) 
        self.multiply_nodes.append(self.mlp_scale_node)

        return self.multiply_nodes


    def _set_value_multiply_nodes(self, multiply_nodes, t_value, r_value):
        """
        Imposta i valori passati alla classe MatrixConstraint ai nodi di multiply.
        I valori passati sono nel formato: value=[(tx, ty, tz), (rx, ry, rz)].
        """
        # translate
        for valore, attribute in zip(t_value, ['X', 'Y', 'Z']):
            cmds.setAttr("{}.input2{}".format(multiply_nodes[0], attribute), valore)

        # rotate
        for valore, attribute in zip(r_value, ['X', 'Y', 'Z']):
            cmds.setAttr("{}.input2{}".format(multiply_nodes[1], attribute), valore)

    
    def _create_matrix_and_decompose(self, object_names):
        """Crea il nodo di multMatrix e decomposeMatrix che costringeranno il controllo."""
        if object_names:
            # Crea il nome per i nodi (matrice, decompose) basato sul nome dell'oggetto 01
            matrix_name = "MTX_{}_to_{}".format(object_names[0], object_names[1])
            decompose_name = "DCM_{}_to_{}".format(object_names[0], object_names[1])
            
            # Crea i nodi (matrice, decompose, multiply) con il nome appropriato
            self.matrix_node = cmds.createNode("multMatrix", name=matrix_name)
            self.decompose_node = cmds.createNode("decomposeMatrix", name=decompose_name)
            
   
    def _matrix_connect_to_objects(self, object_names, matrix_node, decompose_name, offset_attr_name):
        """Crea le connessioni tra gli oggetti selezionati e il nodo di matrice."""
        # Connetti per primo il gruppo Matrix utilizzando l'attributo Offset
        cmds.connectAttr("{}.{}".format(object_names[1], offset_attr_name), "{}.matrixIn[0]".format(matrix_node))
        
        # Connetti per secondo l'oggetto che guida utilizzando il suo WorldMatrix
        cmds.connectAttr("{}.worldMatrix[0]".format(object_names[0]), "{}.matrixIn[1]".format(matrix_node))
        
        # Connetti per terzo il gruppo padre del gruppo Matrix utilizzando il suo InverseWorldMatrix
        cmds.connectAttr("{}.worldInverseMatrix[0]".format(object_names[2]), "{}.matrixIn[2]".format(matrix_node))
        
        # Connetti la matrice al decompose
        cmds.connectAttr("{}.matrixSum".format(matrix_node), "{}.inputMatrix".format(decompose_name))
            
    
    def _connect_decompose_to_multiplies(self, decompose_node, multiply_nodes):
        """Connetti il nodo decompose ai nodi multiply."""
        cmds.connectAttr("{}.outputTranslate".format(decompose_node), "{}.input1".format(multiply_nodes[0]))
        cmds.connectAttr("{}.outputRotate".format(decompose_node), "{}.input1".format(multiply_nodes[1]))
        cmds.connectAttr("{}.outputScale".format(decompose_node), "{}.input1".format(multiply_nodes[2]))
        cmds.select(cl=True)


    def _delete_matrix_distance(self, object_names, matrix_node_tmp, offset_attr_name):
        """Disconnetti e poi elimina il nodo di matrice temporaneo."""
        if matrix_node_tmp:
            # Collega MatrixSum all'attributo creato su oggetto 2
            cmds.disconnectAttr("{}.matrixSum".format(matrix_node_tmp), "{}.{}".format(object_names[1], offset_attr_name))
            cmds.delete(matrix_node_tmp)

    
    def _create_plusminus(self, selection):
        """Crea i nodi di plusMinus che si occupano di sommare le varie matrici tra loro."""
        attributes = ["Translate", "Rotate", "Scale"]
        self.obj_02_pls_nodes = []
        
        # Crea i nodi di plusMinusAverage con il nome appropriato
        for attribute in attributes:
            pls_node = cmds.createNode("plusMinusAverage", name="PLS_{}_{}".format(selection[0][1], attribute))
            self.obj_02_pls_nodes.append(pls_node)


    def _connect_plusminus(self, selection, plusminus_nodes, multiply_nodes):
        """Connetti i nodi multiply ai rispettivi nodi plusMinus, e questi all'oggetto 2."""
        num_nodi = 0
        for idx, mlp_nodes in enumerate(multiply_nodes):
            for mlp_node, pls_node in zip(mlp_nodes, plusminus_nodes):
                cmds.connectAttr("{}.output".format(mlp_node), "{}.input3D[{}]".format(pls_node, idx), f=True)
            num_nodi += 1

        # Collega gli output3d dei nodi di PLS agli attributi dell'oggetto 02
        for pls_node, attribute in zip(plusminus_nodes, ['translate', 'rotate', 'scale']):
            cmds.connectAttr("{}.output3D".format(pls_node), "{}.{}".format(selection[0][1], attribute), f=True)

        # Crea un fake mlp per fixare lo scale del PLS, poi eliminalo
        if num_nodi > 1:
            dummy_node = cmds.createNode("multiplyDivide", name="dummy")
            for attribute in ['X', 'Y', 'Z']:
                cmds.setAttr("{}.input1{}".format(dummy_node, attribute), -(num_nodi - 1))
            cmds.connectAttr("{}.output".format(dummy_node), "{}.input3D[{}]".format(plusminus_nodes[2], num_nodi), f=True)
            cmds.delete(dummy_node)

    
    # -----------------------------------------------------------------
    # Funzione che usa le funzioni create precedentemente per creare 
    # il sistema di matrici dei singoli controlli
    # -----------------------------------------------------------------
    def matrix_constraint(self):
        """Creare il sistema di matrici dei singoli controlli con tutte le connessioni."""

        self.ctrl_mlp_nodes = []

        for idx, (step, t_values, r_values) in enumerate(zip(self.selection, self.t_values, self.r_values)):
            self._get_object_names_from_selection(step)
            self._create_matrix_node_from_objects(self.object_names)
            self._matrix_connect_attribute(self.object_names, self.matrix_node_tmp, self.offset_attr_name)
            mlp_nodes_tmp = self._create_multiply_nodes(self.object_names)
            self.ctrl_mlp_nodes.append(mlp_nodes_tmp)
            self._set_value_multiply_nodes(self.multiply_nodes, t_values, r_values)
            self._create_matrix_and_decompose(self.object_names)
            self._matrix_connect_to_objects(self.object_names, self.matrix_node, self.decompose_node, self.offset_attr_name)
            self._connect_decompose_to_multiplies(self.decompose_node, self.multiply_nodes)
            self._delete_matrix_distance(self.object_names, self.matrix_node_tmp, self.offset_attr_name)

        self._create_plusminus(self.selection)
        self._connect_plusminus(self.selection, self.obj_02_pls_nodes, self.ctrl_mlp_nodes)

        return self.ctrl_mlp_nodes, self.obj_02_pls_nodes
