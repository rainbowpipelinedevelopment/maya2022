# -*- coding: utf-8 -*-
import maya.cmds as cmds

def eye_start():
    # Verifica se i locator esistono
    if cmds.objExists("LOC_L_Eye") and cmds.objExists("LOC_R_Eye"):
        # Ottieni la posizione dei locator
        pos_L_Eye = cmds.xform("LOC_L_Eye", query=True, translation=True, worldSpace=True)
        pos_R_Eye = cmds.xform("LOC_R_Eye", query=True, translation=True, worldSpace=True)
        pos_L_Eye_Iris = cmds.xform("LOC_L_Eye_Iris", query=True, translation=True, worldSpace=True)
        pos_R_Eye_Iris = cmds.xform("LOC_R_Eye_Iris", query=True, translation=True, worldSpace=True)
        pos_L_Eye_Pupil = cmds.xform("LOC_L_Eye_Pupil", query=True, translation=True, worldSpace=True)
        pos_R_Eye_Pupil = cmds.xform("LOC_R_Eye_Pupil", query=True, translation=True, worldSpace=True)
        pos_L_Eye_Spec = cmds.xform("LOC_L_Eye_Spec", query=True, translation=True, worldSpace=True)
        pos_R_Eye_Spec = cmds.xform("LOC_R_Eye_Spec", query=True, translation=True, worldSpace=True)

        # Crea i joint nella posizione dei locator senza imparentarli
        joint_L_Eye = cmds.joint(position=pos_L_Eye)
        joint_R_Eye = cmds.joint(position=pos_R_Eye)
        joint_L_Eye_Iris = cmds.joint(position=pos_L_Eye_Iris)
        joint_R_Eye_Iris = cmds.joint(position=pos_R_Eye_Iris)
        joint_L_Eye_Pupil = cmds.joint(position=pos_L_Eye_Pupil)
        joint_R_Eye_Pupil = cmds.joint(position=pos_R_Eye_Pupil)
        joint_L_Eye_Spec = cmds.joint(position=pos_L_Eye_Spec)
        joint_R_Eye_Spec = cmds.joint(position=pos_R_Eye_Spec)

        # Dissolvi la gerarchia dei joint
        cmds.select(clear=True)
        cmds.parent(joint_L_Eye, joint_R_Eye, joint_L_Eye_Iris, joint_R_Eye_Iris, joint_L_Eye_Pupil, joint_R_Eye_Pupil, joint_L_Eye_Spec, joint_R_Eye_Spec, world=True)

        # Rinomina i joint come "L_Eye_jnt" e "R_Eye_jnt"
        cmds.rename(joint_L_Eye, "L_Eye_jnt")
        cmds.rename(joint_R_Eye, "R_Eye_jnt")
        cmds.rename(joint_L_Eye_Iris, "L_Iris_jnt")
        cmds.rename(joint_R_Eye_Iris, "R_Iris_jnt")
        cmds.rename(joint_L_Eye_Pupil, "L_Pupil_jnt")
        cmds.rename(joint_R_Eye_Pupil, "R_Pupil_jnt")
        cmds.rename(joint_L_Eye_Spec, "L_Spec_jnt")
        cmds.rename(joint_R_Eye_Spec, "R_Spec_jnt")


        print("Joint creati con successo senza gerarchie.")

        # Crea i controlli per i joint
        ctrl_L_Eye = cmds.circle(name="L_Eye_FK_Ctrl")[0]
        ctrl_R_Eye = cmds.circle(name="R_Eye_FK_Ctrl")[0]

        # Crea il gruppo per i controlli
        grp_L_Eye = cmds.group(ctrl_L_Eye, name="L_Eye_FK_Ctrl_Grp")
        grp_R_Eye = cmds.group(ctrl_R_Eye, name="R_Eye_FK_Ctrl_Grp")

        # Posiziona il gruppo nella posizione dei joint
        cmds.matchTransform(grp_L_Eye, "L_Eye_jnt")
        cmds.matchTransform(grp_R_Eye, "R_Eye_jnt")

        print("Controlli e gruppi creati con successo.")

        # Aggiungi il parent constraint tra i controlli e i joint
        cmds.parentConstraint("L_Eye_FK_Ctrl", "L_Eye_jnt")
        cmds.parentConstraint("R_Eye_FK_Ctrl", "R_Eye_jnt")

        print("Parent constraints aggiunti con successo.")

        # Crea i controlli aggiuntivi L_eye_Control e R_eye_Control
        ctrl_L_eye = cmds.circle(name="L_eye_Ctrl")[0]
        ctrl_R_eye = cmds.circle(name="R_eye_Ctrl")[0]

        # Crea il gruppo per i controlli aggiuntivi
        grp_L_eye = cmds.group(ctrl_L_eye, name="L_eye_Ctrl_Grp")
        grp_R_eye = cmds.group(ctrl_R_eye, name="R_eye_Ctrl_Grp")

        # Posiziona il gruppo nella posizione dei joint e sposta lungo l'asse Z di 50
        cmds.matchTransform(grp_L_eye, "L_Eye_jnt")
        cmds.matchTransform(grp_R_eye, "R_Eye_jnt")
        cmds.move(0, 0, 50, grp_L_eye, relative=True)
        cmds.move(0, 0, 50, grp_R_eye, relative=True)

        print("Controlli aggiuntivi e gruppi creati con successo e spostati lungo l'asse Z di 50.")

        # Aggiungi l'aim constraint dal controllo agli oggetti di controllo FK
        cmds.aimConstraint(ctrl_L_eye, grp_L_Eye, aim=(1, 0, 0), u=(0, 1, 0), wu=(0, 1, 0), wut="object", mo=True)
        cmds.aimConstraint(ctrl_R_eye, grp_R_Eye, aim=(1, 0, 0), u=(0, 1, 0), wu=(0, 1, 0), wut="object", mo=True)
        print("Aim constraints aggiunti con successo.")

        # Calcola la posizione a met� tra L_eye_Control_Grp e R_eye_Control_Grp
        pos_L_eye_ctrl_grp = cmds.xform("L_eye_Ctrl_Grp", query=True, translation=True, worldSpace=True)
        pos_R_eye_ctrl_grp = cmds.xform("R_eye_Ctrl_Grp", query=True, translation=True, worldSpace=True)
        pos_EyeMaster = [(pos_L_eye_ctrl_grp[0] + pos_R_eye_ctrl_grp[0]) / 2, (pos_L_eye_ctrl_grp[1] + pos_R_eye_ctrl_grp[1]) / 2, (pos_L_eye_ctrl_grp[2] + pos_R_eye_ctrl_grp[2]) / 2]

        # Crea il controllo "EyeMaster_Ctrl" e il suo gruppo
        ctrl_EyeMaster = cmds.circle(name="EyeMaster_Ctrl")[0]
        grp_EyeMaster = cmds.group(ctrl_EyeMaster, name="EyeMaster_Ctrl_Grp")

        # Posiziona il gruppo nel punto a met� tra i gruppi "L_eye_Control_Grp" e "R_eye_Control_Grp"
        cmds.xform(grp_EyeMaster, translation=pos_EyeMaster, worldSpace=True)

        print("Controllo 'EyeMaster_Ctrl' creato con successo e posizionato a met� tra 'L_eye_Ctrl_Grp' e 'R_eye_Ctrl_Grp'.")

        # Posiziona "L_eye_Control_Grp" e "R_eye_Control_Grp" come figli diretti di "EyeMaster_Ctrl"
        cmds.parent("L_eye_Ctrl_Grp", "EyeMaster_Ctrl")
        cmds.parent("R_eye_Ctrl_Grp", "EyeMaster_Ctrl")

        print("Gruppi 'L_eye_Ctrl_Grp' e 'R_eye_Ctrl_Grp' posizionati come figli diretti di 'EyeMaster_Ctrl'.")
    else:
        print("I locator 'LOC_L_Eye' e/o 'LOC_R_Eye' non esistono.")

    # Raggruppa "L_Eye_jnt" in "L_Eye_jnt_Grp"
    if cmds.objExists("L_Eye_jnt"):
        grp_L_Eye_jnt = cmds.group("L_Eye_jnt", name="L_Eye_jnt_Grp")
        print("Gruppo 'L_Eye_jnt_Grp' creato con successo per 'L_Eye_jnt'.")
    else:
        print("Il joint 'L_Eye_jnt' non esiste.")

    # Raggruppa "R_Eye_jnt" in "R_Eye_jnt_Grp"
    if cmds.objExists("R_Eye_jnt"):
        grp_R_Eye_jnt = cmds.group("R_Eye_jnt", name="R_Eye_jnt_Grp")
        print("Gruppo 'R_Eye_jnt_Grp' creato con successo per 'R_Eye_jnt'.")
    else:
        print("Il joint 'R_Eye_jnt' non esiste.")

    # Imparentare "L_Eye_FK_Ctrl_Grp" e "L_Eye_jnt_Grp" a "LOC_L_Eye"
    if cmds.objExists("LOC_L_Eye"):
        if cmds.objExists("L_Eye_FK_Ctrl_Grp"):
            cmds.parent("L_Eye_FK_Ctrl_Grp", "LOC_L_Eye")
            print("Imparentato 'L_Eye_FK_Ctrl_Grp' a 'LOC_L_Eye'.")
        else:
            print("'L_Eye_FK_Ctrl_Grp' non esiste.")
        
        if cmds.objExists("L_Eye_jnt_Grp"):
            cmds.parent("L_Eye_jnt_Grp", "LOC_L_Eye")
            print("Imparentato 'L_Eye_jnt_Grp' a 'LOC_L_Eye'.")
        else:
            print("'L_Eye_jnt_Grp' non esiste.")
    else:
        print("Il locator 'LOC_L_Eye' non esiste.")

    # Imparentare "R_Eye_FK_Ctrl_Grp" e "R_Eye_jnt_Grp" a "LOC_R_Eye"
    if cmds.objExists("LOC_R_Eye"):
        if cmds.objExists("R_Eye_FK_Ctrl_Grp"):
            cmds.parent("R_Eye_FK_Ctrl_Grp", "LOC_R_Eye")
            print("Imparentato 'R_Eye_FK_Ctrl_Grp' a 'LOC_R_Eye'.")
        else:
            print("'R_Eye_FK_Ctrl_Grp' non esiste.")
        
        if cmds.objExists("R_Eye_jnt_Grp"):
            cmds.parent("R_Eye_jnt_Grp", "LOC_R_Eye")
            print("Imparentato 'R_Eye_jnt_Grp' a 'LOC_R_Eye'.")
        else:
            print("'R_Eye_jnt_Grp' non esiste.")
    else:
        print("Il locator 'LOC_R_Eye' non esiste.")

    # Imparentare "L_Eye_Loc" e "R_Eye_Loc" sotto "Head_SQSUp_02_jnt"
    if cmds.objExists("Head_SQSUp_02_jnt"):
        if cmds.objExists("LOC_L_Eye"):
            cmds.parent("LOC_L_Eye", "Head_SQSUp_02_jnt")
            print("Imparentato 'LOC_L_Eye' sotto 'Head_SQSUp_02_jnt'.")
        else:
            print("'L_Eye_Loc' non esiste.")

        if cmds.objExists("LOC_R_Eye"):
            cmds.parent("LOC_R_Eye", "Head_SQSUp_02_jnt")
            print("Imparentato 'LOC_R_Eye' sotto 'Head_SQSUp_02_jnt'.")
        else:
            print("'LOC_R_Eye' non esiste.")
    else:
        print("'Head_SQSUp_02_jnt' non esiste.")

eye_start()    

def posiziona_controlli():
    eyel_ctrl_grp = "L_Eyeshaper_Ctrl_Grp"
    eyel_locator = "LOC_L_Eyeshaper"
    eyer_ctrl_grp = "R_Eyeshaper_Ctrl_Grp"
    eyer_locator = "LOC_R_Eyeshaper"

    # Posiziona il gruppo di controllo sinistro nella posizione del locator corrispondente
    driver_pos = cmds.xform(eyel_locator, query=True, worldSpace=True, translation=True)
    cmds.xform(eyel_ctrl_grp, worldSpace=True, translation=driver_pos)

    # Posiziona il gruppo di controllo destro nella posizione del locator corrispondente
    master_pos = cmds.xform(eyer_locator, query=True, worldSpace=True, translation=True)
    cmds.xform(eyer_ctrl_grp, worldSpace=True, translation=master_pos)

    # Porta i gruppi di controllo a galla rendendoli oggetti di primo livello
    cmds.parent(eyel_ctrl_grp, world=True)
    cmds.parent(eyer_ctrl_grp, world=True)

    # Elimina i locator
    cmds.delete(eyel_locator, eyer_locator)

    print("Le posizioni sono state aggiornate, i controlli sono ora di primo livello nell'Outliner, e i locator sono stati eliminati.")

# Chiamare la funzione
posiziona_controlli()
    
def create_control_and_group(joint_name, control_name, group_name):
    # Verifica se il joint esiste
    if not cmds.objExists(joint_name):
        print("Joint '{}' non trovato.".format(joint_name))
        return

    # Crea un controllo (cerchio) per il joint
    control = cmds.circle(name=control_name)[0]

    # Crea un gruppo per il controllo
    group = cmds.group(control, name=group_name)

    # Posiziona il gruppo e il controllo nella posizione del joint
    cmds.matchTransform(group, joint_name)

    # Imparenta il joint sotto il controllo
    cmds.parent(joint_name, control)

def setup_pupil_controls():
    # Crea controllo e gruppo per L_Pupil_jnt
    create_control_and_group("L_Pupil_jnt", "L_Pupil_Ctrl", "L_Pupil_Ctrl_Grp")

    # Crea controllo e gruppo per R_Pupil_jnt
    create_control_and_group("R_Pupil_jnt", "R_Pupil_Ctrl", "R_Pupil_Ctrl_Grp")
    
    # Crea controllo e gruppo per L_Iris_jnt
    create_control_and_group("L_Iris_jnt", "L_Iris_Ctrl", "L_Iris_Ctrl_Grp")

    # Crea controllo e gruppo per R_Pupil_jnt
    create_control_and_group("R_Iris_jnt", "R_Iris_Ctrl", "R_Iris_Ctrl_Grp")
    
    # Crea controllo e gruppo per R_Pupil_jnt
    create_control_and_group("L_Spec_jnt", "L_Spec_Ctrl", "L_Spec_Ctrl_Grp")
    
    # Crea controllo e gruppo per R_Pupil_jnt
    create_control_and_group("R_Spec_jnt", "R_Spec_Ctrl", "R_Spec_Ctrl_Grp")

setup_pupil_controls()

# Imparentare "L_Iris_Ctrl_Grp" e "L_Pupil_Ctrl_Grp" a "LOC_L_Eye"
if cmds.objExists("LOC_L_Eye"):
    if cmds.objExists("L_Iris_Ctrl_Grp"):
        cmds.parent("L_Iris_Ctrl_Grp", "LOC_L_Eye")
        print("Imparentato 'L_Eye_FK_Ctrl_Grp' a 'LOC_L_Eye'.")
    else:
        print("'L_Eye_FK_Ctrl_Grp' non esiste.")
    
    if cmds.objExists("L_Pupil_Ctrl_Grp"):
        cmds.parent("L_Pupil_Ctrl_Grp", "LOC_L_Eye")
        print("Imparentato 'L_Pupil_Ctrl_Grp' a 'LOC_L_Eye'.")
    else:
        print("'L_Eye_jnt_Grp' non esiste.")
        
    if cmds.objExists("L_Spec_Ctrl_Grp"):
        cmds.parent("L_Spec_Ctrl_Grp", "LOC_L_Eye")
        print("Imparentato 'L_Spec_Ctrl_Grp' a 'LOC_L_Eye'.")
    else:
        print("'L_Eye_jnt_Grp' non esiste.")
else:
    print("Il locator 'LOC_L_Eye' non esiste.")
    
# Imparentare "R_Iris_Ctrl_Grp" e "R_Pupil_Ctrl_Grp" a "LOC_R_Eye"
if cmds.objExists("LOC_R_Eye"):
    if cmds.objExists("R_Iris_Ctrl_Grp"):
        cmds.parent("R_Iris_Ctrl_Grp", "LOC_R_Eye")
        print("Imparentato 'L_Eye_FK_Ctrl_Grp' a 'LOC_R_Eye'.")
    else:
        print("'R_Iris_Ctrl_Grp' non esiste.")
    
    if cmds.objExists("R_Pupil_Ctrl_Grp"):
        cmds.parent("R_Pupil_Ctrl_Grp", "LOC_R_Eye")
        print("Imparentato 'R_Pupil_Ctrl_Grp' a 'LOC_R_Eye'.")
    else:
        print("'R_Eye_jnt_Grp' non esiste.")
        
    if cmds.objExists("R_Spec_Ctrl_Grp"):
        cmds.parent("R_Spec_Ctrl_Grp", "LOC_R_Eye")
        print("Imparentato 'R_Spec_Ctrl_Grp' a 'LOC_R_Eye'.")
    else:
        print("'R_Eye_jnt_Grp' non esiste.")
else:
    print("Il locator 'LOC_R_Eye' non esiste.")
       
# Definizione delle funzioni
def replace_and_remove(source_ctrl, target_ctrl):
    # Verifica se i due controlli esistono
    if not cmds.objExists(source_ctrl):
        return
    if not cmds.objExists(target_ctrl):
        return

    # Ottieni e elimina le shape esistenti nel controllo di destinazione
    target_shapes = cmds.listRelatives(target_ctrl, shapes=True)
    if target_shapes:
        cmds.delete(target_shapes)

    # Ottieni la shape del controllo sorgente
    source_shapes = cmds.listRelatives(source_ctrl, shapes=True)
    if source_shapes:
        for source_shape in source_shapes:
            cmds.parent(source_shape, target_ctrl, r=True, shape=True)

        # Trova il gruppo padre del controllo sorgente, se esiste
        source_parent = cmds.listRelatives(source_ctrl, parent=True)
        cmds.delete(source_ctrl)

        # Elimina il gruppo padre del controllo sorgente, se non ha altri figli
        if source_parent and not cmds.listRelatives(source_parent[0], children=True):
            cmds.delete(source_parent)

def process_multiple_pairs(control_pairs):
    for source, target in control_pairs:
        replace_and_remove(source, target)

# Elenco delle coppie di controlli da sostituire
control_pairs = [
    ("L_eye_fk_Ctrl_REF", "L_Eye_FK_Ctrl"),
    ("R_eye_fk_Ctrl_REF", "R_Eye_FK_Ctrl"),
    ("EyeMaster_Ctrl_REF", "EyeMaster_Ctrl"),
    ("L_eye_Ctrl_REF", "L_eye_Ctrl"),
    ("R_eye_Ctrl_REF", "R_eye_Ctrl"),
    ("L_Iris_Ctrl_REF", "L_Iris_Ctrl"),
    ("R_Iris_Ctrl_REF", "R_Iris_Ctrl"),
    ("L_Pupil_Ctrl_REF", "L_Pupil_Ctrl"),
    ("R_Pupil_Ctrl_REF", "R_Pupil_Ctrl"),
    ("L_Spec_Ctrl_REF", "L_Spec_Ctrl"),
    ("R_Spec_Ctrl_REF", "R_Spec_Ctrl"),
]

process_multiple_pairs(control_pairs)

# Nascondere i Locator degli occhi
def hide_locator_shapes(locator_names):
    for locator in locator_names:
        shape_nodes = cmds.listRelatives(locator, shapes=True)
        if shape_nodes:  # Verifica se il locator ha shape nodes
            for shape in shape_nodes:
                cmds.setAttr(shape + ".visibility", 0)

hide_locator_shapes(["LOC_L_Eye", "LOC_R_Eye"])

def hide_joints(joint_names):
    for joint in joint_names:
        cmds.setAttr(joint + ".visibility", 0)

# Lista dei joint da nascondere
joints_to_hide = [
    "R_Eye_jnt", "R_Pupil_jnt", "R_Iris_jnt", "R_Spec_jnt",
    "L_Eye_jnt", "L_Pupil_jnt", "L_Iris_jnt", "L_Spec_jnt"
]

# Chiama la funzione per nascondere i joint
hide_joints(joints_to_hide)

def organize_and_clean():
    # Elimina i gruppi dei locator e controlli REF
    for grp in ["Eyes_Contorls_Grp_REF"]:
        if cmds.objExists(grp):
            cmds.delete(grp)
    
    # Trova ed elimina tutti i nodi di tweak
    tweak_nodes = cmds.ls(type='tweak')
    if tweak_nodes:
        cmds.delete(tweak_nodes)

    # Imparenta 'EyeMaster_Ctrl_Grp' sotto 'Face_RIG_Grp'
    if cmds.objExists("EyeMaster_Ctrl_Grp") and cmds.objExists("Face_RIG_Grp"):
        cmds.parent("EyeMaster_Ctrl_Grp", "Face_RIG_Grp")

organize_and_clean()

def create_control_at_locator(locator_name, control_name):
    """
    Crea un controllo nella posizione del locator specificato,
    con un gruppo padre imparentato al locator.
    """
    control = cmds.circle(name=control_name + "_Ctrl")[0]
    group = cmds.group(control, name=control_name + "_Ctrl_Grp")
    temp_constraint = cmds.pointConstraint(locator_name, group)
    cmds.delete(temp_constraint)
    cmds.parent(group, locator_name)
    return control, group

def create_joint_at_control(control_name, joint_name):
    """
    Crea un joint nella posizione del controllo specificato
    e lo imparenta sotto il controllo.
    """
    joint = cmds.joint(name=joint_name)
    temp_constraint = cmds.pointConstraint(control_name, joint)
    cmds.delete(temp_constraint)
    cmds.parent(joint, control_name)
    return joint

def disconnect_translation(locator_name):
    """
    Disconnette le connessioni degli attributi di traslazione di un dato locator.
    """
    for attr in ['translateX', 'translateY', 'translateZ']:
        full_attr = locator_name + "." + attr
        connections = cmds.listConnections(full_attr, plugs=True, source=True, destination=False)
        if connections:
            for src_attr in connections:
                cmds.disconnectAttr(src_attr, full_attr)

def lock_and_hide_visibility(control):
    if cmds.objExists(control):
        # Blocca l'attributo 'visibility'
        cmds.setAttr(control + ".visibility", lock=True, keyable=False, channelBox=False)

def setup_controls():
    # Elimina i nodi
    nodes_to_delete = ["MD_Eye_C", "MD_Eyemover"]
    for node in nodes_to_delete:
        if cmds.objExists(node):
            cmds.delete(node)

    # Creazione dei locator sotto "Head_SQSUp_02_jnt"
    cmds.parent("LOC_L_Eyemover", "Head_SQSUp_02_jnt")
    cmds.parent("LOC_R_Eyemover", "Head_SQSUp_02_jnt")

    # Creazione dei controlli e gruppi gi� imparentati ai locator
    control_l, group_l = create_control_at_locator("LOC_L_Eyemover", "L_Eyemover")
    control_r, group_r = create_control_at_locator("LOC_R_Eyemover", "R_Eyemover")

    # Creazione dei joint
    create_joint_at_control("L_Eyemover_Ctrl", "L_Eyemover_Jnt")
    create_joint_at_control("R_Eyemover_Ctrl", "R_Eyemover_Jnt")

    # Disconnetti le connessioni delle traslazioni per i locator
    disconnect_translation("LOC_R_Eyemover")
    disconnect_translation("LOC_R_Eye")
    
    if cmds.objExists("LOCS_FK_Eye_Grp"):
        cmds.delete("LOCS_FK_Eye_Grp")
        print("Gruppo 'LOCS_FK_Eye_Grp' eliminato con successo.")
    else:
        print("Il gruppo 'LOCS_FK_Eye_Grp' non esiste.")

    # Posizionare "LOC_R_Eye" sotto "R_Eyemover_Ctrl"
    cmds.parent("LOC_R_Eye", "R_Eyemover_Ctrl")

    # Posizionare "LOC_L_Eye" sotto "L_Eyemover_Ctrl"
    cmds.parent("LOC_L_Eye", "L_Eyemover_Ctrl")
    
    # Imposta la visibilit� di controlli e locator a 0
    controls_and_locators_to_hide = ["L_Eyemover_CtrlShape", "R_Eyemover_CtrlShape", 
                                     "LOC_L_EyemoverShape", "LOC_R_EyemoverShape"]
    for obj in controls_and_locators_to_hide:
        if cmds.objExists(obj):
            cmds.setAttr(obj + ".visibility", 0)

    # Imposta la visibilit� dei joint a 0
    joints_to_hide = ["R_Eyemover_Jnt", "L_Eyemover_Jnt"]
    for joint in joints_to_hide:
        if cmds.objExists(joint):
            cmds.setAttr(joint + ".visibility", 0)
            
    # Parenta "L_Eyeshaper_Ctrl_Grp" sotto "LOC_L_Eyemover"
    if cmds.objExists("L_Eyeshaper_Ctrl_Grp") and cmds.objExists("LOC_L_Eyemover"):
        cmds.parent("L_Eyeshaper_Ctrl_Grp", "LOC_L_Eyemover")

    # Parenta "R_Eyeshaper_Ctrl_Grp" sotto "LOC_R_Eyemover"
    if cmds.objExists("R_Eyeshaper_Ctrl_Grp") and cmds.objExists("LOC_R_Eyemover"):
        cmds.parent("R_Eyeshaper_Ctrl_Grp", "LOC_R_Eyemover")
        
    # Posizionare I gruppi padre dei controlli in gerarchia
    cmds.parent("L_Pupil_Ctrl_Grp", "L_Iris_Ctrl")
    cmds.parent("R_Pupil_Ctrl_Grp", "R_Iris_Ctrl")
    cmds.parent("L_Iris_Ctrl_Grp", "L_Eye_FK_Ctrl")
    cmds.parent("R_Iris_Ctrl_Grp", "R_Eye_FK_Ctrl")
    cmds.parent("L_Spec_Ctrl_Grp", "L_Iris_Ctrl")
    cmds.parent("R_Spec_Ctrl_Grp", "R_Iris_Ctrl")

    # Lista dei controlli per lock and hide
    controls_to_lock_hide = ["L_Eye_FK_Ctrl", "L_Iris_Ctrl", "L_Pupil_Ctrl", 
                             "R_Eye_FK_Ctrl", "R_Iris_Ctrl", "R_Pupil_Ctrl", 
                             "EyeMaster_Ctrl", "L_eye_Ctrl", "R_eye_Ctrl"
                             ]

    # Applica lock and hide a ogni controllo
    for control in controls_to_lock_hide:
        lock_and_hide_visibility(control)
        
    if cmds.objExists("EyeMaster_Ctrl"):
        cmds.addAttr("EyeMaster_Ctrl", longName="auto_eyelid", attributeType="float", min=0, max=1, defaultValue=0)
        cmds.setAttr("EyeMaster_Ctrl.auto_eyelid", edit=True, keyable=True)

        # Aggiungi attributo "eye_pose" al controllo "EyeMaster_Ctrl"
        cmds.addAttr("EyeMaster_Ctrl", longName="eye_pose", attributeType="enum", enumName="default:anim", keyable=False)
        cmds.setAttr("EyeMaster_Ctrl.eye_pose", edit=True, channelBox=True)

setup_controls()

def fix_visibility_attributes():
    """lock & hide l'attributo 'scaleZ'"""
    controls = ['L_Iris_Ctrl', 'R_Iris_Ctrl', 'L_Pupil_Ctrl', 'R_Pupil_Ctrl']
    for ctrl in controls:
        if cmds.objExists(ctrl):
            cmds.setAttr("{}.sz".format(ctrl), lock=True, keyable=False, channelBox=False)

def fix_visibility_eyeshaper_ctrl():
    """Crea attributo su Eye_FK_Ctrl per visibility controlli eyeshaper"""
    fk_controls = ('L_Eye_FK_Ctrl', 'R_Eye_FK_Ctrl')
    eyeshaper_groups = ('L_Eyeshaper_Ctrl_Grp', 'R_Eyeshaper_Ctrl_Grp')

    for ctrl, grp in zip(fk_controls, eyeshaper_groups):
        if cmds.objExists(ctrl) and cmds.objExists(grp):
            cmds.addAttr(ctrl, ln="eyeshaper_vis", at="enum", en="off:on:", k=True)
            cmds.setAttr("{}.eyeshaper_vis".format(ctrl), 1)
            cmds.connectAttr("{}.eyeshaper_vis".format(ctrl), "{}.visibility".format(grp), force=True)


fix_visibility_attributes()
fix_visibility_eyeshaper_ctrl()

def connect_controls():
        """Connette i controlli 'Head_*SQS_Ctrl' con '*SQS_Ctrl'."""
        jaw_controls = ["L_Eyeshaper_Ctrl", "R_Eyeshaper_Ctrl"]
        jaw_static_controls = ["L_Eyemover_Ctrl", "R_Eyemover_Ctrl"]

        for head_ctrl, base_ctrl in zip(jaw_controls, jaw_static_controls):
            # Connette le trasformazioni di translate, rotate e scale
            cmds.connectAttr(head_ctrl + ".translate", base_ctrl + ".translate", f=True)
            cmds.connectAttr(head_ctrl + ".rotate", base_ctrl + ".rotate", f=True)
            cmds.connectAttr(head_ctrl + ".scale", base_ctrl + ".scale", f=True)
        for source, target in control_pairs:
            replace_and_remove(source, target)
        if  cmds.objExists("Jaw_Controls_GRP"):
            cmds.delete("Jaw_Controls_GRP")
        if  cmds.objExists("Jaw_LOCS_GRP"):
            cmds.delete("Jaw_LOCS_GRP")
connect_controls()

import maya.cmds as cmds

def setup_eye_helper():
    # Funzione per creare locator e gruppo
    def create_locator_and_group(joint_name, locator_name, group_name, parent_locator):
        if not cmds.objExists(locator_name):
            locator = cmds.spaceLocator(name=locator_name)[0]

            if cmds.objExists(joint_name):
                pos = cmds.xform(joint_name, query=True, translation=True, worldSpace=True)
                cmds.xform(locator, translation=pos, worldSpace=True)

                group = cmds.group(empty=True, name=group_name)
                cmds.parent(locator, group)
                cmds.xform(group, translation=pos, worldSpace=True)
                cmds.xform(locator, translation=(0, 0, 0), objectSpace=True)

                if cmds.objExists(parent_locator):
                    cmds.parent(group, parent_locator)
                else:
                    print("%s non trovato." % parent_locator)
            else:
                print("%s non trovato." % joint_name)

    # Crea locator e gruppo per l'occhio sinistro e imparenta a "LOC_L_Eye"
    create_locator_and_group("L_Eye_jnt", "LOC_L_Eye_Helper", "LOC_L_Eye_Helper_Grp", "LOC_L_Eye")

    # Crea locator e gruppo per l'occhio destro e imparenta a "LOC_R_Eye"
    create_locator_and_group("R_Eye_jnt", "LOC_R_Eye_Helper", "LOC_R_Eye_Helper_Grp", "LOC_R_Eye")

    # Crea orient constraint da "L_Eye_jnt" verso "LOC_L_Eye_Helper"
    if cmds.objExists("L_Eye_jnt") and cmds.objExists("LOC_L_Eye_Helper"):
        cmds.orientConstraint("L_Eye_jnt", "LOC_L_Eye_Helper", maintainOffset=True)

    # Crea orient constraint da "R_Eye_jnt" verso "LOC_R_Eye_Helper"
    if cmds.objExists("R_Eye_jnt") and cmds.objExists("LOC_R_Eye_Helper"):
        cmds.orientConstraint("R_Eye_jnt", "LOC_R_Eye_Helper", maintainOffset=True)
                
    # Imposta la visibilit� di "LOC_L_Eye_Helper" e "LOC_R_Eye_Helper" a 0
    for locator in ["LOC_L_Eye_Helper", "LOC_R_Eye_Helper"]:
        if cmds.objExists(locator):
            cmds.setAttr(locator + ".visibility", 0)

# Funzioni di Utility per creazione set
def create_joints_set(module, rigpart, jnt_list):
    # Creates a set containing the joints for skinning
    joints_for_skin = []
    joints_for_skin.extend(jnt_list)
    
    if not cmds.objExists("FACE_MODULES_Joints"):
        module_joints_set = cmds.sets(empty=True, name=("FACE_MODULES_Joints"))
    else:
        module_joints_set = "FACE_MODULES_Joints"

    # Controlla se il set del modulo di rig dei joints esiste, altrimenti crealo
    if not cmds.objExists("{}_Joints".format(module)): # 'HeadEyes' -'HeadMouth' - 'HeadSquash'
        section_joints_set = cmds.sets(empty=True, name=("{}_Joints".format(module)))
        # Parenta il set della parte di rig dentro al set globale
        cmds.sets(section_joints_set, edit=True, forceElement=module_joints_set)
    else:
        section_joints_set = "{}_Joints".format(module)

    # Controlla se il set della parte di rig dei joints esiste, altrimenti crealo
    if not cmds.objExists("{}_jointsForSkin".format(rigpart)):
        rigpart_joints_set = cmds.sets(empty=True, name=("{}_jointsForSkin".format(rigpart)))
        # Parenta il set della parte di rig dentro al set globale
        cmds.sets(rigpart_joints_set, edit=True, forceElement=section_joints_set)
    else:
        rigpart_joints_set = "{}_jointsForSkin".format(rigpart)


    for jnt in joints_for_skin:
        cmds.sets(jnt, edit=True, forceElement=rigpart_joints_set)

def create_controls_set(module, rigpart, ctrl_list):
    # Crea il set contenente i controlli creati
    if not cmds.objExists("FACE_MODULES_Controls"):
        module_controls_set = cmds.sets(empty=True, name=("FACE_MODULES_Controls"))
    else:
        module_controls_set = "FACE_MODULES_Controls"

    if not cmds.objExists("{}_controls".format(module)):
        module_ctrl_set = cmds.sets(empty=True, name=("{}_controls".format(module)))
        # Parenta il set del modulo dentro al set globale
        cmds.sets(module_ctrl_set, edit=True, forceElement=module_controls_set)
    else:
        module_ctrl_set = "{}_controls".format(module)
        
    if not cmds.objExists("{}_controls".format(rigpart)):
        rigpart_ctrl_set = cmds.sets(empty=True, name=("{}_controls".format(rigpart)))
        # Parenta il set del modulo dentro al set globale
        cmds.sets(rigpart_ctrl_set, edit=True, forceElement=module_ctrl_set)
    else:
        rigpart_ctrl_set = "{}_controls".format(rigpart)

    for ctrl in ctrl_list:
        cmds.sets(ctrl, edit=True, forceElement=rigpart_ctrl_set)

    return module_ctrl_set

def rename_ctrl_shapes(set_ctrl):
    """Data una selezione di controlli, rinomina le shapes in base al nome del controllo."""
    cmds.select(clear=True)
    cmds.select(set_ctrl)
    sel = cmds.ls(sl=True)
    cmds.select(clear=True)
    
    for ctrl in sel:
        shapes = cmds.listRelatives(ctrl, shapes=True, pa=True)
        for shape in shapes:
            new_name = "{}Shape".format(ctrl)
            cmds.rename(shape, new_name)

    print("### Le shapes dei controlli sono state rinominate correttamente. ###")


lista_jnt_set = ["L_Eyemover_Jnt", "L_Eye_jnt", "L_Pupil_jnt", "L_Iris_jnt", "L_Spec_jnt", "R_Eyemover_Jnt", "R_Eye_jnt", "R_Pupil_jnt", "R_Iris_jnt", "R_Spec_jnt"]
lista_ctrl_set = ["L_Eyeshaper_Ctrl", "L_Eye_FK_Ctrl", "L_Iris_Ctrl", "L_Pupil_Ctrl", "L_Spec_Ctrl", "R_Eyeshaper_Ctrl", "R_Eye_FK_Ctrl", "R_Iris_Ctrl", "R_Pupil_Ctrl", "R_Spec_Ctrl", "EyeMaster_Ctrl", "L_eye_Ctrl", "R_eye_Ctrl"]
create_joints_set("HeadEyes", "Eyes", lista_jnt_set)
controls_set = create_controls_set("HeadEyes", "Eyes", lista_ctrl_set)
rename_ctrl_shapes(controls_set)


setup_eye_helper()
