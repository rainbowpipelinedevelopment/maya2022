import maya.cmds as cmds

# Funzioni di Utility per creazione set
def create_joints_set(module, rigpart, jnt_list):
    # Creates a set containing the joints for skinning
    joints_for_skin = []
    joints_for_skin.extend(jnt_list)
    
    if not cmds.objExists("FACE_MODULES_Joints"):
        module_joints_set = cmds.sets(empty=True, name=("FACE_MODULES_Joints"))
    else:
        module_joints_set = "FACE_MODULES_Joints"

    # Controlla se il set del modulo di rig dei joints esiste, altrimenti crealo
    if not cmds.objExists("{}_Joints".format(module)): # 'HeadEyes' -'HeadMouth' - 'HeadSquash'
        section_joints_set = cmds.sets(empty=True, name=("{}_Joints".format(module)))
        # Parenta il set della parte di rig dentro al set globale
        cmds.sets(section_joints_set, edit=True, forceElement=module_joints_set)
    else:
        section_joints_set = "{}_Joints".format(module)

    # Controlla se il set della parte di rig dei joints esiste, altrimenti crealo
    if not cmds.objExists("{}_jointsForSkin".format(rigpart)):
        rigpart_joints_set = cmds.sets(empty=True, name=("{}_jointsForSkin".format(rigpart)))
        # Parenta il set della parte di rig dentro al set globale
        cmds.sets(rigpart_joints_set, edit=True, forceElement=section_joints_set)
    else:
        rigpart_joints_set = "{}_jointsForSkin".format(rigpart)


    for jnt in joints_for_skin:
        cmds.sets(jnt, edit=True, forceElement=rigpart_joints_set)

def create_controls_set(module, rigpart, ctrl_list):
    # Crea il set contenente i controlli creati
    if not cmds.objExists("FACE_MODULES_Controls"):
        module_controls_set = cmds.sets(empty=True, name=("FACE_MODULES_Controls"))
    else:
        module_controls_set = "FACE_MODULES_Controls"

    if not cmds.objExists("{}_controls".format(module)):
        module_ctrl_set = cmds.sets(empty=True, name=("{}_controls".format(module)))
        # Parenta il set del modulo dentro al set globale
        cmds.sets(module_ctrl_set, edit=True, forceElement=module_controls_set)
    else:
        module_ctrl_set = "{}_controls".format(module)
        
    if not cmds.objExists("{}_controls".format(rigpart)):
        rigpart_ctrl_set = cmds.sets(empty=True, name=("{}_controls".format(rigpart)))
        # Parenta il set del modulo dentro al set globale
        cmds.sets(rigpart_ctrl_set, edit=True, forceElement=module_ctrl_set)
    else:
        rigpart_ctrl_set = "{}_controls".format(rigpart)

    for ctrl in ctrl_list:
        cmds.sets(ctrl, edit=True, forceElement=rigpart_ctrl_set)

    return module_ctrl_set

def rename_ctrl_shapes(set_ctrl):
    """Data una selezione di controlli, rinomina le shapes in base al nome del controllo."""
    cmds.select(clear=True)
    cmds.select(set_ctrl)
    sel = cmds.ls(sl=True)
    cmds.select(clear=True)
    
    for ctrl in sel:
        shapes = cmds.listRelatives(ctrl, shapes=True, pa=True)
        for shape in shapes:
            new_name = "{}Shape".format(ctrl)
            cmds.rename(shape, new_name)

    print("### Le shapes dei controlli sono state rinominate correttamente. ###")


# Funzione Main
def main_rig_process():

    # Posiziona "Tongue_Controls_Grp" e "low_teeth_Ctrl_Grp" sotto "Jaw_Ctrl"
    cmds.parent('Tongue_Controls_Grp', 'Jaw_Ctrl')
    cmds.parent('low_teeth_Ctrl_Grp', 'Jaw_Ctrl')

    # Crea un nuovo gruppo "Inner_Mouth_RIG_Grp"
    cmds.group(name='Inner_Mouth_RIG_Grp', empty=True)

    # Aggiungi "Teeth_joints_Grp" e "Tongue_joints_Grp" al gruppo "Inner_Mouth_RIG_Grp"
    cmds.parent('Teeth_joints_Grp', 'Inner_Mouth_RIG_Grp')
    cmds.parent('Tongue_joints_Grp', 'Inner_Mouth_RIG_Grp')

    # Posiziona "Inner_Mouth_RIG_Grp" sotto "Face_RIG_Grp"
    cmds.parent('Inner_Mouth_RIG_Grp', 'Face_RIG_Grp')

    cmds.parent('up_teeth_Ctrl_Grp', 'Head_SQSLow_03_jnt')
    
    cmds.setAttr("Teeth_joints_Grp" + ".visibility", 0)
    cmds.setAttr("Tongue_joints_Grp" + ".visibility", 0)

    # connetti Jaw_Ctrl.inner_mouth_VIS
    cmds.connectAttr("Jaw_Ctrl.inner_mouth_VIS", "Tongue_Controls_Grp.visibility")
    cmds.connectAttr("Jaw_Ctrl.inner_mouth_VIS", "low_teeth_Ctrl_Grp.visibility")
    cmds.connectAttr("Jaw_Ctrl.inner_mouth_VIS", "up_teeth_Ctrl_Grp.visibility")


    lista_joints = ['tongue_01_jnt', 'tongue_02_jnt', 'tongue_03_jnt', 'tongue_04_jnt', 'tongue_05_jnt', 'tongue_06_jnt',
                    'C_lower_teeth_jnt', 'L_lower_teeth_jnt', 'R_lower_teeth_jnt', 
                    'C_upper_teeth_jnt', 'L_upper_teeth_jnt', 'R_upper_teeth_jnt']
    lista_controlli = ['tongue_01_Ctrl', 'tongue_02_Ctrl', 'tongue_03_Ctrl', 'tongue_04_Ctrl', 'tongue_05_Ctrl', 
                       'low_teeth_Ctrl', 'C_lower_teeth_Ctrl', 'L_lower_teeth_Ctrl', 'R_lower_teeth_Ctrl', 
                       'up_teeth_Ctrl', 'C_upper_teeth_Ctrl', 'L_upper_teeth_Ctrl', 'R_upper_teeth_Ctrl']
    
    create_joints_set("HeadMouth", "InnerMouth", lista_joints)
    controls_set = create_controls_set("HeadMouth", "InnerMouth", lista_controlli)
    rename_ctrl_shapes(controls_set)





main_rig_process()