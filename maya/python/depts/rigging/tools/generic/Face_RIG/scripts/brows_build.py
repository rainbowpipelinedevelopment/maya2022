# -*- coding: utf-8 -*-

import maya.cmds as cmds
from maya import OpenMaya
from maya import OpenMayaUI as omui
import math, os, sys
from functools import partial
from imp import reload
import depts.rigging.tools.generic.Face_RIG.scripts.matrix_module as mtx
reload(mtx)


# -----------------------------------------------------------------
# PySide
# -----------------------------------------------------------------
try:
    from shiboken2 import wrapInstance
except ImportError:
    from shiboken import wrapInstance

try:
    from PySide2.QtGui import QIcon
    from PySide2.QtWidgets import QWidget
except ImportError:
    from PySide.QtGui import QIcon, QWidget

    
class AER :
    """Automatically rig brows."""
    
    def __init__(self):
        self.browsLoc = None
        self.browName = None
        self.browsVtx = None
        self.grpAllRig = None
        self.grpBaseRig = None
        self.hierarchyMainGrpCTRL = None
        self.hierarchyMainGrpJNT = None
        self.parent_rig = ["L_Eyemover_Ctrl", "R_Eyemover_Ctrl"]
        self.set_sliders = []
        
        # -----------------------------------------------------------------
        # Path
        # -----------------------------------------------------------------
        localpipe = os.getenv("LOCALPY")
        myPath = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "Face_RIG")
        script_path = os.path.join(myPath, "scripts")
        scene_path = os.path.join(myPath, "scenes", "controls_shape.ma")
        image_path = os.path.join(myPath, "icons")

        self.my_file_path = scene_path
        self.image_path = image_path

    
    # -----------------------------------------------------------------
    # Prerequisiti per i rig
    # ----------------------------------------------------------------- 
    def placeBrowsCenter(self, *args):
        '''Place locator in the center of the eyeball.
        
        Called by 'UI' function.
        Call functions: None '''
        
        selection = cmds.filterExpand(sm = 12) or cmds.filterExpand(sm = 22)
        
        if selection == None :
            self.browsLoc = None
            cmds.error("Please select the eyeball.\n")
        else :
            eyeball = selection [0]

            
            eyeTemp = cmds.duplicate(eyeball) [0]
            cmds.xform(eyeTemp, cp = 1)
            self.browsLoc = cmds.spaceLocator(n = (self.nameRig + "_Center_locator"))[0]
            cnstrTemp = cmds.pointConstraint(eyeTemp, self.browsLoc)
            cmds.delete(cnstrTemp)
            cmds.delete(eyeTemp)
            # lock locator
            cmds.setAttr(self.browsLoc + ".overrideEnabled", 1)
            cmds.setAttr(self.browsLoc + ".overrideDisplayType", 2)
            
            cmds.select(cl = 1)

            # Update UI
            cmds.textField(self.txtfLoc, e = 1, tx = self.browsLoc)
            cmds.button(self.btnPlaceCenter, e = 1, en = 0)
            cmds.button(self.btnUndoPlaceCenter, e = 1, en = 1)
    
    
    def placeBrowsCenterUndo(self, *args):
        '''Undo 'placeBrowsCenter' function.
        
        Called by 'UI' function.
        Call functions: None '''
        
        try :
            cmds.delete(self.browsLoc)
        except ValueError :
            cmds.warning("'" + self.browsLoc + "'" + " doesn't exists.\n")
        finally :
            self.browsLoc = None
            self.browName = None
            cmds.textField(self.txtfLoc, e = 1, tx = "")
            cmds.button(self.btnPlaceCenter, e = 1, en = 1)
            cmds.button(self.btnUndoPlaceCenter, e = 1, en = 0)
    
    
    def upLidVtxSet(self, *args):
        '''List selected vertices as upper lid vertices.
        
        Called by 'UI' function.
        Call functions: None '''
        
        self.browsVtx = cmds.filterExpand(sm = 31)
        
        if self.browsVtx == None :
            cmds.scrollField(self.scrollfUpLid, e = 1, cl = 1)
            cmds.error("Please select vertices of the upper lid.\n")
        else :
            cmds.scrollField(self.scrollfUpLid, e = 1, cl = 1)
            for vtx in self.browsVtx :
                vtxNum = vtx.rpartition(".")[2] # <type 'unicode'>
                cmds.scrollField(self.scrollfUpLid, e = 1, it = (str(vtxNum) + " "))
    

    # -----------------------------------------------------------------
    # Fix fatti allo script originale
    # ----------------------------------------------------------------- 
    def jnt_orient_zeroed(self, jnt, *args):
        '''Zeroed the Joint Orient of the joint child.
        
        Called by 'vtxToJnt' function.
        Call functions: None '''

        for axis in ['X', 'Y', 'Z']:
            cmds.setAttr("{}.jointOrient{}".format(jnt, axis), 0)

    
    def fix_orient_jnts(self, loc, jnt, *args):
        """Fixa l'orientamento dei jnt."""
        piv = cmds.xform(loc, q=True, piv=True, ws=True)
        cmds.select(clear=True)
        jntAim = cmds.joint(n="JNT_ToAim", p=piv[0:3])
        
        side = jnt.split("_")[0]

        if side == 'L':
            aim = cmds.aimConstraint(jntAim, jnt, weight = 1, aimVector = (0,0,-1), upVector = (0,1,0), worldUpType = "vector", worldUpVector = (0,1,0))
        else: # == 'R'
            aim = cmds.aimConstraint(jntAim, jnt, weight = 1, aimVector = (0,0,1), upVector = (0,1,0), worldUpType = "vector", worldUpVector = (0,1,0))

        cmds.delete(aim)
        cmds.delete(jntAim)


    def make_ctrl(self, sel, name, *args):
        '''Creates the controls.
        
        Called by 'createCrvCtrls' function.
        Call functions: None '''

        cmds.select(cl = 1)
        TEMP_CTRL1 = cmds.circle(r = 0.15, n=name)[0]
        TEMP_CTRL2 = cmds.duplicate()[0]
        cmds.setAttr(TEMP_CTRL2 + ".rotateY", 90)
        TEMP_CTRL3 = cmds.duplicate()[0]
        cmds.setAttr(TEMP_CTRL3 + ".rotateX", 90)
        cmds.parent(TEMP_CTRL2, TEMP_CTRL3, TEMP_CTRL1)
        cmds.makeIdentity(apply = 1, t = 1, r = 1, s = 1, n = 0, pn = 1)
        cmds.pickWalk(d = "down")
        cmds.select(TEMP_CTRL1, tgl = 1)
        cmds.parent(r = 1, s = 1)
        cmds.delete(TEMP_CTRL2, TEMP_CTRL3)
        cmds.select(cl = 1)

        return TEMP_CTRL1
        

    def createTransformOffsetGroup(self, sel, *args):
        '''Creates offset group for each item selected.
        
        Called by 'createCrvCtrls' function.
        Call functions: None '''
        
        for item in sel:
            parent_group = cmds.listRelatives(item,p=True);
            cmds.select(cl=True);
            offset_group = cmds.group(em=True, n="GRP_"+item+"_transform_offset")
            cmds.parent(offset_group, item)
            cmds.setAttr(offset_group+".tx",0)
            cmds.setAttr(offset_group+".ty",0)
            cmds.setAttr(offset_group+".tz",0)
            cmds.setAttr(offset_group+".rx",0)
            cmds.setAttr(offset_group+".ry",0)
            cmds.setAttr(offset_group+".rz",0)
            cmds.parent(offset_group, parent_group)
            cmds.parent(item, offset_group)


    def side_eye(self, side, *args):
        if side == None :
            self.side = None
            cmds.error("Please select a side.\n")
        else:
            self.side = side
            self.nameRig = "{}_Brow".format(self.side)
    

    def input_number_ctrls(self, *args):
        # Retrieve text insert from user
        self.nCtrls = cmds.intSliderGrp("nCtrls", query=True, value=True)
        
        self.nEps = self.nCtrls
        self.nCvs = self.nCtrls + 2
        self.nSpans = self.nCtrls - 1

    
    def find_pos_list(self, side, nCtrls, cornerAPos, cornerBPos, upLidEpDriverCrvPos, *args):
        posList = []

        x = 0
        while x < self.nCtrls:
            if x == 0:
                posList.append(self.cornerAPos)
                x += 1
            if x == self.nCtrls - 1:
                posList.append(self.cornerBPos)
                x += 1
            else:
                posList.append(upLidEpDriverCrvPos[x])
                x += 1

        return posList


    # -----------------------------------------------------------------
    # Parte di costruzione del modulo brows
    # -----------------------------------------------------------------
    def _ordinate_cv_base_curve(self, curva):
        """Ordina cv base curve per permettere il mirroring delle influenze dei joint."""

        last_ep = cmds.getAttr(f"{curva}.spans")
        pos_ep_start = cmds.xform(f"{curva}.ep[0]", q=True, ws=True, t=True)
        pos_ep_end = cmds.xform(f"{curva}.ep[{last_ep}]", q=True, ws=True, t=True)


        if self.side == "L":
            if pos_ep_start[0] > pos_ep_end[0]:
                base_curve = cmds.reverseCurve(curva, ch=False, rpo=1)
            else:
                base_curve = curva
            
        elif self.side == "R":
            if pos_ep_start[0] < pos_ep_end[0]:
                base_curve = cmds.reverseCurve(curva, ch=False, rpo=1)
            else:
                base_curve = curva

        return base_curve    

    
    def cv_base_curve_to_joint(self, browCenter, base_crv):
        '''Creates one joint per vertex of the eyelid and parent it to the center of the eye.
        
        Called by 'buildRig' function.
        Call functions: None '''
        
        cmds.select(cl = 1)
        
        self.upLidJntList = []
        
        # Organize rig hierarchy
        hierarchySecondGrp = cmds.group(n = (self.nameRig + "_Base_JNT_GRP"), em = 1)
        hierarchyUpGrp = cmds.group(n = (self.nameRig + "_joints_GRP"), em = 1)
        
        cmds.parent(hierarchyUpGrp, hierarchySecondGrp)
        
        self.hierarchyMainGrpJNT = self.nameRig + "_JNT_GRP"
        
        if cmds.objExists("|" + self.hierarchyMainGrpJNT) :
            cmds.parent(hierarchySecondGrp, ("|" + self.hierarchyMainGrpJNT))
        else :
            cmds.group(n = self.hierarchyMainGrpJNT, em = 1)
            cmds.parent(hierarchySecondGrp, ("|" + self.hierarchyMainGrpJNT))
        
        cmds.setAttr(self.hierarchyMainGrpJNT + ".visibility", 0)
        
        # Calcola il numero di vertici nella curva
        numCVs = cmds.getAttr(base_crv + '.spans') + cmds.getAttr(base_crv + '.degree')

        # Crea joint
        for idx, cv in enumerate(range(numCVs)):
            cmds.select(cl = 1)
            jnt = cmds.joint(rad = 0.01, n = (self.nameRig + "_Skin_%d_jnt" % idx))
            self.upLidJntList.append(jnt)
            position = cmds.xform("{}.cv[{}]".format(base_crv, cv), q = 1, ws = 1, t = 1)
            cmds.xform(jnt, ws = 1, t = position)
            centerPosition = cmds.xform(browCenter, q = 1, ws = 1, t = 1)
            cmds.select(cl = 1)
            centerJnt = cmds.joint(rad = 0.01, n = (self.nameRig + "_Up_Base_%d_jnt" % idx))
            cmds.xform(centerJnt, ws = 1, t = centerPosition)
            cmds.parent(jnt, centerJnt)
            cmds.joint(centerJnt, e = 1, oj = "xyz", secondaryAxisOrient = "yup", ch = 1, zso = 1)
            cmds.parent(centerJnt, hierarchyUpGrp)
            # reset joint orient
            self.jnt_orient_zeroed(jnt)

        # Creates a set containing the joints for skinning
        jntsForSkin = []
        jntsForSkin.extend(self.upLidJntList)
        
        if not cmds.objExists("FACE_MODULES_Joints"):
            module_joints_set = cmds.sets(empty=True, name=("FACE_MODULES_Joints"))
        else:
            module_joints_set = "FACE_MODULES_Joints"

        # Controlla se il set della parte di rig dei joints esiste, altrimenti crealo
        if not cmds.objExists("HeadEyes_Joints"):
            rigpart_joints_set = cmds.sets(empty=True, name=("HeadEyes_Joints"))
            # Parenta il set della parte di rig dentro al set globale
            cmds.sets(rigpart_joints_set, edit=True, forceElement=module_joints_set)
        else:
            rigpart_joints_set = "HeadEyes_Joints"

        # Controlla se il set dei joints del modulo esiste, altrimenti crealo
        if not cmds.objExists("{}_jointsForSkin".format(self.nameRig)):
            setSkinJnts = cmds.sets(empty=True, name=("{}_jointsForSkin".format(self.nameRig)))
            # Parenta il set del modulo dentro al set globale
            cmds.sets(setSkinJnts, edit=True, forceElement=rigpart_joints_set)
        else:
            setSkinJnts = "{}_jointsForSkin".format(self.nameRig)

        for jnt in jntsForSkin:
            cmds.sets(jnt, edit=True, forceElement=setSkinJnts)

    
    def create_groups_for_locators(self, locator):
        
        master_grp_name = "{}_MASTER_Grp".format(locator)
        master_grp = cmds.group(n=master_grp_name, em=1)
        parentC_TEMP = cmds.parentConstraint(locator, master_grp)
        cmds.delete(parentC_TEMP)

        matrix_grp_name = master_grp_name.replace("MASTER", "MATRIX")
        matrix_grp = cmds.duplicate(master_grp, n = matrix_grp_name)[0]
        cmds.parent(locator, matrix_grp)
        cmds.parent(matrix_grp, master_grp)

        return master_grp, matrix_grp


    def place_rig_locators(self, nameRig, upLidJntList):
        '''Creates one locator per brow vertex and constrain each joint to it (aim).
        
        Called by 'buildRig' function.
        Call functions: None '''
        
        self.upLidLocList = []

        # Organize rig hierarchy
        hierarchyThirdGrp = cmds.group(n = (nameRig + "_locator_GRP"), em = 1)
        
        self.grpBaseRig = (nameRig + "_Base_RIG_GRP")
        if cmds.objExists(nameRig + "_Base_RIG_GRP"):
            cmds.setAttr(self.grpBaseRig + ".visibility", 0)
            cmds.parent(hierarchyThirdGrp, self.grpBaseRig)
        else:
            self.grpBaseRig = cmds.group(n = (nameRig + "_Base_RIG_GRP"), em = 1)
            cmds.setAttr(self.grpBaseRig + ".visibility", 0)
            cmds.parent(hierarchyThirdGrp, self.grpBaseRig)
        
        self.grpAllRig = nameRig + "_RIG_GRP"
        if cmds.objExists(nameRig + "_RIG_GRP"):
            cmds.parent(self.grpBaseRig, self.grpAllRig)
            cmds.parent(self.hierarchyMainGrpJNT, self.grpAllRig)
        else:
            self.grpAllRig = cmds.group(n = (nameRig + "_RIG_GRP"), em = 1)
            cmds.parent(self.grpBaseRig, self.grpAllRig)
            cmds.parent(self.hierarchyMainGrpJNT, self.grpAllRig)
        
        self.grp_locator = []
        for upLidJnt in upLidJntList:
            locName = upLidJnt.replace("_Skin", "")
            locName = locName.replace("_jnt", "_loc")
            loc = cmds.spaceLocator(n = locName)[0]
            self.upLidLocList.append(loc)
            cmds.setAttr(loc + "Shape.localScaleX", 0.025)
            cmds.setAttr(loc + "Shape.localScaleY", 0.025)
            cmds.setAttr(loc + "Shape.localScaleZ", 0.025)
            positionLoc = cmds.xform(upLidJnt, q = 1, ws = 1, t = 1)
            cmds.xform(loc, ws = 1, t = positionLoc)
            grp_loc = self.create_groups_for_locators(loc)
            self.grp_locator.append(grp_loc)
            parentJnt = cmds.listRelatives(upLidJnt, p = 1)[0]
            aim = cmds.aimConstraint(loc, parentJnt, weight = 1, aimVector = (1,0,0), upVector = (0,1,0), worldUpType = "vector", worldUpVector = (0,1,0))
            cmds.parent(grp_loc[0], hierarchyThirdGrp)
            cmds.delete(aim)
            jntGrp = "{}_joints_GRP".format(nameRig)
            cmds.parent(upLidJnt, jntGrp)
            cmds.delete(parentJnt)
            pac = cmds.parentConstraint(loc, upLidJnt, mo=True, w=1)

        
        # Imparenta tutto sotto 'Face_RIG_Grp'
        if cmds.objExists(self.grpAllRig) and cmds.objExists("Face_RIG_Grp"):
            cmds.parent(self.grpAllRig, "Face_RIG_Grp")
        
                
    def createBrowsCrv(self, nameRig, browsVtx, rigGrp):
        '''Creates nurbsCurve out of each lid vertices.
        
        Called by 'buildRig' function.
        Call functions: None '''
        
        cmds.select(cl = 1)
        
        # Organize rig hierarchy
        self.grpBaseRig = cmds.group(n = (nameRig + "_Base_RIG_GRP"), em = 1)
        self.hierarchyCrvGrp = cmds.group(n = (nameRig + "_curves_GRP"), em = 1)
        self.hierarchyUpCrvGrp = cmds.group(n = (nameRig + "_Up_curves_GRP"), em = 1)
        
        cmds.parent(self.hierarchyUpCrvGrp, self.hierarchyCrvGrp)
        cmds.parent(self.hierarchyCrvGrp, self.grpBaseRig)
        
        # Upper eyelid
        cmds.select(browsVtx)
        edgeUpLid = cmds.polyListComponentConversion(fv = 1, te = 1, internal = 1)
        cmds.select(edgeUpLid)
        tempCrvUp = cmds.polyToCurve(form = 0, degree = 1, conformToSmoothMeshPreview=0) [0]
        cmds.select(tempCrvUp)
        cmds.delete(ch = 1)
        up_base_crv = self._ordinate_cv_base_curve(tempCrvUp)
        upLidCrvName = nameRig + "_BASE_curve"
        self.upLidCrv = cmds.rename(up_base_crv, upLidCrvName)
        cmds.parent(self.upLidCrv, self.hierarchyUpCrvGrp)
        
        
    def getDagPath(self, objectName):
        '''MARCO GIORDANO'S CODE (http://www.marcogiordanotd.com/)
        
        Called by 'getUParam' function.
        Call functions: None '''
    
        if isinstance(objectName, list)==True:
            oNodeList=[]
            for o in objectName:
                selectionList = OpenMaya.MSelectionList()
                selectionList.add(o)
                oNode = OpenMaya.MDagPath()
                selectionList.getDagPath(0, oNode)
                oNodeList.append(oNode)
            return oNodeList
        else:
            selectionList = OpenMaya.MSelectionList()
            selectionList.add(objectName)
            oNode = OpenMaya.MDagPath()
            selectionList.getDagPath(0, oNode)
            return oNode
    
    
    def getUParam(self, pnt = [], crv = None, *args):
        '''MARCO GIORDANO'S CODE (http://www.marcogiordanotd.com/) - Function called by 
        
        Called by 'connectLocToCrv' function.
        Call functions: 'getDagPath' '''
        
        point = OpenMaya.MPoint(pnt[0],pnt[1],pnt[2])
        curveFn = OpenMaya.MFnNurbsCurve(self.getDagPath(crv))
        paramUtill=OpenMaya.MScriptUtil()
        paramPtr=paramUtill.asDoublePtr()
        isOnCurve = curveFn.isPointOnCurve(point)
        if isOnCurve == True:
            curveFn.getParamAtPoint(point , paramPtr,0.001,OpenMaya.MSpace.kObject )
        else :
            point = curveFn.closestPoint(point,paramPtr,0.001,OpenMaya.MSpace.kObject)
            curveFn.getParamAtPoint(point , paramPtr,0.001,OpenMaya.MSpace.kObject )
        
        param = paramUtill.getDouble(paramPtr)  
        return param
    
    
    def connectLocToCrv(self, upLidLocList, upLidCrv):
        '''Connect locators to brow curves via pointOnCurveInfo nodes.
        
        Called by 'buildRig' function.
        Call functions: None '''
        
        for upLidLoc in self.grp_locator:
            position = cmds.xform(upLidLoc[0], q = 1, ws = 1, t = 1)
            u = self.getUParam(position, upLidCrv)
            name = upLidLoc[0].replace("_locator", "_pointOnCurveInfo")
            ptOnCrvInfo = cmds.createNode("pointOnCurveInfo", n = name)
            cmds.connectAttr(upLidCrv + ".worldSpace", ptOnCrvInfo + ".inputCurve")
            cmds.setAttr(ptOnCrvInfo + ".parameter", u)
            cmds.connectAttr(ptOnCrvInfo + ".position", upLidLoc[0] + ".t")
    
    
    def browsCorners(self, browsEpCrvPos):
        '''Define brows corners position.
        
        Called by 'createDriverCrv' function.
        Call functions: None '''
        
        corner1 = browsEpCrvPos[0]
        corner2 = browsEpCrvPos[-1]

        if self.side == 'L':
            if corner1[0] > corner2[0]:
                self.cornerAPos = corner2
                self.cornerBPos = corner1
            elif corner1[0] < corner2[0]:
                self.cornerAPos = corner1
                self.cornerBPos = corner2
        elif self.side == 'R':
            if corner1[0] > corner2[0]:
                self.cornerAPos = corner1
                self.cornerBPos = corner2
            elif corner1[0] < corner2[0]:
                self.cornerAPos = corner2
                self.cornerBPos = corner1
        
   
    def browsCrvCVs(self, upLidCrv):
        '''List CVs of each guide curve.
        
        Called by 'createDriverCrv' function.
        Call functions: None '''
        
        upLidCVs = []
        x = 0
        while x < self.nCvs :
            posCv = cmds.xform((upLidCrv + ".cv[%d]" % x), q = 1, ws = 1, t = 1)
            upLidCVs.append(posCv)
            x += 1
        
        return upLidCVs
    
    
    def browsMatchTopology(self, cornerAPos, cornerBPos, upLidCVsPos): ## ---> TO BE FIXED!!!!!!!!
        '''Reorganise the CVs of each curve so they have the same topology.
        
        Called by 'createDriverCrv' function.
        Call functions: None '''
        
        # Order of CVs in base lists:           Order of CVs in ordered lists:
        # (upLidCVsPos, lowLidCVsPos)           (upLidCVsOrdered, lowLidCVsOrdered)
        # -----------------------               -------------------------
        # INDEX |       CV      |               | INDEX |       CV      |
        # ------|---------------|               |-------|---------------|
        #   0   | corner: ?     |               |   0   | corner A      |
        #   1   |               |               |   1   |               |
        #   2   |               |               |   2   |               |
        #   3   | middle of crv |               |   3   | middle of crv |
        #   4   |               |               |   4   |               |
        #   5   |               |               |   5   |               |
        #   6   | other corner  |               |   6   | corner B      |
        # -----------------------               -------------------------
        
        # - measure dist between first_CV of baseList and cornerAPos
        # - measure dist between first_CV of baseList and cornerBPos
        # - if CV is closer to cornerA append baseList to orderedList from beginning to end
        # - else (CV closer to cornerB) append baseList to orderedList from end to beginning
        # return orderedLists
        
        # distance formula is: d = sqrt((Ax-Bx)**2 + (Ay-By)**2 + (Az-Bz)**2)
        distTEMP1 = math.sqrt( ( ((upLidCVsPos [0])[0]) - cornerAPos[0] )**2 + ( ((upLidCVsPos [0])[1]) - cornerAPos[1] )**2 + ( ((upLidCVsPos [0])[2]) - cornerAPos[2] )**2 )
        distTEMP2 = math.sqrt( ( ((upLidCVsPos [0])[0]) - cornerBPos[0] )**2 + ( ((upLidCVsPos [0])[1]) - cornerBPos[1] )**2 + ( ((upLidCVsPos [0])[2]) - cornerBPos[2] )**2 )
        if distTEMP1 < distTEMP2 :
            upLidCVsOrdered = upLidCVsPos
        else:
            upLidCVsOrdered = upLidCVsPos[::-1] # reversed 'upLidCVsPos'
        
        return upLidCVsOrdered
    
    
    def createDriverCrv(self, upLidBaseCrv, upRigGrp):
        '''Create a driver curve for each lid curve and connect it to the base curve with a wire deformer.
        
        Called by 'buildRig' function.
        Call functions: 'browsCorners', 'eyeLidsLeftAndRight' (unused), 'browsCrvCVs', 'browsMatchTopology' '''
        
        ## Upper eyelid ##
        upLidDriverCrvTEMP = cmds.duplicate(upLidBaseCrv)[0]
        cmds.delete(upLidDriverCrvTEMP, ch = 1) # delete history
        cmds.rebuildCurve(upLidDriverCrvTEMP, rpo = 1, end = 1, kr = 2, kcp = 0, kep = 1, kt = 0, s = self.nSpans, d = 7, tol = 0.01)
        
        # list the position of the EPs of the upper lid driver curve
        self.upLidEpPosTEMP = []
        x = 0
        while x < self.nEps :
            posEp = cmds.xform((upLidDriverCrvTEMP + ".ep[%d]" % x), q = 1, ws = 1, t = 1)
            self.upLidEpPosTEMP.append(posEp)
            x += 1
        cmds.delete(upLidDriverCrvTEMP)
        
        # Create the upLid 'guide' curve for corner placement and query CVs positions and indexes
        
        upLidGuideCrv = cmds.curve(d = 3, ep = self.upLidEpPosTEMP)
        
        ##
        
        # Find position of eye corners
        self.browsCorners(self.upLidEpPosTEMP)
        
        # List CVs positions of upLidGuideCrv
        upLidCVsPos = self.browsCrvCVs(upLidGuideCrv)

        # List CVs positions in the right order (to match topology)
        upLidCVsOrdered = self.browsMatchTopology(self.cornerAPos, self.cornerBPos, upLidCVsPos)
        
        ##
        
        # Create driver curve
        self.upLidDriverCrv = cmds.curve(d = 3, p = upLidCVsOrdered)
        upLidDriverCrvName = upLidBaseCrv.replace("_BASE_", "_DRIVER_")
        self.upLidDriverCrv = cmds.rename(self.upLidDriverCrv, upLidDriverCrvName)
        cmds.parent(self.upLidDriverCrv, upRigGrp)
        
        cmds.delete(upLidGuideCrv)
        
        ##
        
        cmds.select(cl = 1)
        wireNodeUpLidName = upLidBaseCrv.replace("_BASE_curve", "_controlCurve_wire")
        wireUpLid = cmds.wire(upLidBaseCrv, n = wireNodeUpLidName, w = self.upLidDriverCrv, gw = 0, en = 1, ce = 0, li = 0, dds=[0, 1000])
        
    
    def createJntCtrls(self, nameRig, cornerAPos, cornerBPos, upLidDriverCrv, rigGrp): # MODIFIER
        '''Creates controller joints for each point of the eyelids driver curves.
        
        Called by 'buildRig' function.
        Call functions: None '''
        
        # Find position of EPs of each driver curve for joint placement
        upLidEpDriverCrvPos = []
        x = 0
        while x < self.nEps :
            posEp = cmds.xform((upLidDriverCrv + ".ep[%d]" % x), q = 1, ws = 1, t = 1)
            upLidEpDriverCrvPos.append(posEp)
            x += 1
                
        # Place controller joints
        self.nameBrows = []
        self.ctrlJnts = []

        posList = self.find_pos_list(self.side, self.nCtrls, self.cornerAPos, self.cornerBPos, upLidEpDriverCrvPos)

        # genera lista dei nomi dei controlli
        for i,v in enumerate(posList):
            tmp = "{}_0{}".format(nameRig, i+1)
            self.nameBrows.append(tmp)

        for item, (value, pos) in enumerate(zip(self.nameBrows, posList)):
            jnt = cmds.joint(rad=0.05, p=pos, n="{}_jnt".format(value))
            # Fix orientamento dei joint
            self.fix_orient_jnts(self.browsLoc, jnt) 
            self.ctrlJnts.append(jnt)

        # Organise rig hierarchy
        hierarchyCtrlJntGrp = cmds.group(n = (nameRig + "_CTRL_JNT_GRP"), em = 1)
        cmds.parent(hierarchyCtrlJntGrp, rigGrp)
        for jnt in self.ctrlJnts:
            cmds.parent(jnt, hierarchyCtrlJntGrp)
        
    
    def bindskin_ctrljnts_to_curve(self, upLidDriverCrv, ctrlJnts):
        # Skin controllers joints to each driver curve
        kwargs = {
            'toSelectedBones': True,
            'bindMethod': 0,
            'normalizeWeights': 1,
            'weightDistribution': 0,
            'maximumInfluences': 2,
            'obeyMaxInfluences': True,
            'dropoffRate': 4,
            'removeUnusedInfluence': False
        }

        cmds.select(cl = 1)

        cmds.skinCluster(ctrlJnts, upLidDriverCrv, n=f"SK_{self.side}_brow_driver_upper", **kwargs)


    def createCrvCtrls(self, nameRig, ctrlJnts): 
        '''Creates controller curve for each controller joint.
        
        Called by 'buildRig' function.
        Call functions: make_ctrl '''
        
        # Organize rig hierarchy
        hierarchySecondGrp = cmds.group(n="{}_Base_Ctrl_Grp".format(nameRig), em = 1)
        
        self.hierarchyMainGrpCTRL = "{}_CTRL_GRP".format(nameRig)
        
        if cmds.objExists("|" + self.hierarchyMainGrpCTRL) :
            cmds.parent(hierarchySecondGrp, ("|" + self.hierarchyMainGrpCTRL))
        else :
            cmds.group(n = self.hierarchyMainGrpCTRL, em = 1)
            cmds.parent(hierarchySecondGrp, ("|" + self.hierarchyMainGrpCTRL))
        
        if cmds.objExists(nameRig + "_RIG_GRP"):
            cmds.parent(self.hierarchyMainGrpCTRL, self.grpAllRig)

        # Create the controls and constrain the joints
        self.ctrlList = []
        self.master_grp_list = []
        self.matrix_grp_list = []

        for idx, jnt in enumerate(ctrlJnts):
            ctrlName = jnt.replace("jnt", "Ctrl")
            ctrl = self.make_ctrl(jnt, ctrlName)
            self.ctrlList.append(ctrl)
            parentC_TEMP = cmds.parentConstraint(jnt, ctrl)
            cmds.delete(parentC_TEMP)

            master_grp_name = "{}_MASTER_Grp".format(ctrlName)
            master_grp = cmds.group(n=master_grp_name, em=1)
            parentC_TEMP = cmds.parentConstraint(ctrl, master_grp)
            cmds.delete(parentC_TEMP)
            self.master_grp_list.append(master_grp)

            matrix_grp_name = master_grp_name.replace("MASTER", "MATRIX")
            matrix_grp = cmds.duplicate(master_grp, n = matrix_grp_name)
            cmds.parent(ctrl, matrix_grp)
            cmds.parent(matrix_grp, master_grp)
            cmds.parent(master_grp, hierarchySecondGrp)
            cmds.parentConstraint(ctrl, jnt, mo=True, w=1)
            self.matrix_grp_list.append(matrix_grp[0])

            # Se il controllo è un secondario (numero pari)
            if idx % 2 == 1:
                # Aggiungi attributo Follow sui controlli secondari 
                cmds.addAttr(ctrl, ln="Follow", at="float", min=0, max=1, k=1)

                # Aggiungi gruppo FOLLOW sopra il MATRIX
                follow_grp_name = matrix_grp_name.replace("MATRIX", "FOLLOW")
                cmds.group(matrix_grp, n=follow_grp_name)

        cmds.select (cl = 1)
        
        # Lock and hide unused channels
        for ctrl in self.ctrlList :
            cmds.setAttr((ctrl + ".sx"), lock = 1, keyable = 0, channelBox = 0)
            cmds.setAttr((ctrl + ".sy"), lock = 1, keyable = 0, channelBox = 0)
            cmds.setAttr((ctrl + ".sz"), lock = 1, keyable = 0, channelBox = 0)
            cmds.setAttr((ctrl + ".v"), lock = 1, keyable = 0, channelBox = 0)

        # Controlla se il set contenente i controlli esiste, altrimenti crealo
        if not cmds.objExists("FACE_MODULES_Controls"):
            module_controls_set = cmds.sets(empty=True, name=("FACE_MODULES_Controls"))
        else:
            module_controls_set = "FACE_MODULES_Controls"

        # Controlla se il set della parte di rig dei joints esiste, altrimenti crealo
        if not cmds.objExists("HeadEyes_controls"):
            rigpart_controls_set = cmds.sets(empty=True, name=("HeadEyes_controls"))
            # Parenta il set della parte di rig dentro al set globale
            cmds.sets(rigpart_controls_set, edit=True, forceElement=module_controls_set)
        else:
            rigpart_controls_set = "HeadEyes_controls"

        # Controlla se il set dei controlli del modulo esiste, altrimenti crealo
        if not cmds.objExists("{}_controls".format(self.nameRig)):
            self.set_brow_ctrls = cmds.sets(empty=True, name=("{}_controls".format(self.nameRig)))
            # Parenta il set del modulo dentro al set rigpart
            cmds.sets(self.set_brow_ctrls, edit=True, forceElement=rigpart_controls_set)
        else:
            self.set_brow_ctrls = "{}_controls".format(self.nameRig)

        for ctrl in self.ctrlList:
            cmds.sets(ctrl, edit=True, forceElement=self.set_brow_ctrls)


    def check_position_master_ctrl(self, ctrlList, master_grp_list, matrix_grp_list):
        """Trova la posizione dove creare il master control."""
        self.brows_master_ctrl = None
        checkCtrl = int(self.nCtrls)
        self.index = None

        for self.index, (grp_master, grp_offset, ctrl) in enumerate(zip(master_grp_list, matrix_grp_list, ctrlList)):
            if int(self.nCtrls) == 3:
                self.index = 1
            if int(self.nCtrls) == 4:
                self.index = 1
            if int(self.nCtrls) == 5:
                self.index = 2
            if int(self.nCtrls) == 6:
                self.index = 2
            if int(self.nCtrls) == 7:
                self.index = 3

    
    def add_master_ctrl(self, nameRig, ctrlList, matrix_grp_list, index):
        # creo il controllo master all'origine degli assi
        ctrl_master_name = ctrlList[self.index].replace("0{}".format(self.index+1), "Master")
        self.brows_master_ctrl = cmds.circle(n=ctrl_master_name, c=[0,0,0], nr=[0,1,0], sw=360, r=2, d=3, ut=0, tol=0.01, s=8, ch=0)[0]
        
        # aggiungo attributo Squint Follow
        cmds.addAttr(self.brows_master_ctrl, ln="squint_follow", at="float", min=0, max=1, dv=1, k=1)

        # creo il relativo gruppo MATRIX
        new_grp_matrix_name = "{}_MATRIX_Grp".format(ctrl_master_name)
        new_grp_matrix = cmds.group(name = new_grp_matrix_name, em=True)

        # creo il relativo gruppo MASTER
        new_grp_master_name = "{}_MASTER_Grp".format(ctrl_master_name)
        new_grp_master = cmds.group(name = new_grp_master_name, em=True)
        
        # parento il controllo al gruppo e faccio il matching delle coordinate col gruppo del controllo 02                
        cmds.parent(self.brows_master_ctrl, new_grp_matrix)
        cmds.matchTransform(new_grp_matrix, matrix_grp_list[self.index])
        cmds.matchTransform(new_grp_master, matrix_grp_list[self.index])

        # metto gruppi in gerarchia
        ctrls_grp = "{}_Base_Ctrl_Grp".format(nameRig)
        cmds.parent(new_grp_matrix, new_grp_master)
        cmds.parent(new_grp_master, ctrls_grp)

        # Lock and hide unused channels
        cmds.setAttr ((self.brows_master_ctrl + ".sx"), lock = 1, keyable = 0, channelBox = 0)
        cmds.setAttr ((self.brows_master_ctrl + ".sy"), lock = 1, keyable = 0, channelBox = 0)
        cmds.setAttr ((self.brows_master_ctrl + ".sz"), lock = 1, keyable = 0, channelBox = 0)
        cmds.setAttr ((self.brows_master_ctrl + ".v"), lock = 1, keyable = 0, channelBox = 0)

        # Aggiungi controlli al set contenente i controlli creati
        if cmds.objExists(self.set_brow_ctrls) and cmds.sets(self.set_brow_ctrls, q=True, size=True):
            cmds.sets(self.brows_master_ctrl, e = 1, forceElement = self.set_brow_ctrls)


    def replace_and_remove_shapes(self, *args):
        # lista contenente i controlli da sostituire
        control_pairs = [
                ('L_Brow_01_Ctrl_REF', 'L_Brow_01_Ctrl'), 
                ('L_Brow_02_Ctrl_REF', 'L_Brow_02_Ctrl'), 
                ('L_Brow_03_Ctrl_REF', 'L_Brow_03_Ctrl'), 
                ('L_Brow_04_Ctrl_REF', 'L_Brow_04_Ctrl'), 
                ('L_Brow_03_Ctrl_REF', 'L_Brow_05_Ctrl'), 
                ('L_Brow_04_Ctrl_REF', 'L_Brow_06_Ctrl'), 
                ('L_Brow_03_Ctrl_REF', 'L_Brow_07_Ctrl'), 
                ('R_Brow_01_Ctrl_REF', 'R_Brow_01_Ctrl'), 
                ('R_Brow_02_Ctrl_REF', 'R_Brow_02_Ctrl'), 
                ('R_Brow_03_Ctrl_REF', 'R_Brow_03_Ctrl'), 
                ('R_Brow_04_Ctrl_REF', 'R_Brow_04_Ctrl'),
                ('R_Brow_03_Ctrl_REF', 'R_Brow_05_Ctrl'),
                ('R_Brow_04_Ctrl_REF', 'R_Brow_06_Ctrl'),
                ('R_Brow_03_Ctrl_REF', 'R_Brow_07_Ctrl'),
                ('L_Brow_Master_Ctrl_REF', 'L_Brow_Master_Ctrl'),
                ('R_Brow_Master_Ctrl_REF', 'R_Brow_Master_Ctrl')
            ]

        # importa scena con i controlli
        cmds.file(self.my_file_path, i=True, mergeNamespacesOnClash=True, namespace=':', gr=True, gn="GRP_shapes_REF")

        # itera attraverso le coppie di controlli e esegui la sostituzione
        for source_ctrl, target_ctrl in control_pairs:
            try:
                # Ottieni e elimina le shape esistenti nel controllo di destinazione
                target_shapes = cmds.listRelatives(target_ctrl, shapes=True)
                name_target_shape = target_shapes
                if target_shapes:
                    cmds.delete(target_shapes)

                # Ottieni la shape del controllo sorgente
                new_source_ctrl = cmds.duplicate(source_ctrl, n="{}_NEW".format(source_ctrl))
                source_shapes = cmds.listRelatives(new_source_ctrl, shapes=True)
                if source_shapes:
                    for source_shape in source_shapes:
                        cmds.parent(source_shape, target_ctrl, r=True, shape=True)
                        cmds.rename(source_shape, name_target_shape[0])
            except:
                pass

        # elimina il gruppo dei controlli importati
        grp_del = "GRP_shapes_REF"
        cmds.delete("GRP_shapes_REF")
    
    
    # -----------------------------------------------------------------
    # Sistema per creare matrici che guidano in rotate i locator/joint che skinnano la mesh
    # -----------------------------------------------------------------
    def driver_locator(self, upLidDriverCrv):
        """Crea un locator per ogni cv (ctrlJnts) della curva driver."""
        self.loc_list = []

        for idx, jnt in enumerate(self.ctrlList):
            pos = cmds.xform(jnt, q = 1, ws = 1, t = 1)
            loc = cmds.spaceLocator(n="LOC_{}_{}".format(upLidDriverCrv, idx))
            cmds.xform(loc, ws = 1, t = pos)
            self.loc_list.append(loc)


    def distanza_da_cv_a_locator(self, curva_nome, lista_locator):
        def distanza_euclidea(p1, p2):
            return math.sqrt(sum((a - b) ** 2 for a, b in zip(p1, p2)))

        def trova_locator_vicini(cv_index, locator):
            cv_position = cmds.pointPosition(curva_nome + '.cv[' + str(cv_index) + ']')
            locator_vicini = sorted(locator, key=lambda x: distanza_euclidea(cv_position, cmds.pointPosition(x)))
            return locator_vicini[0], locator_vicini[1]

        # Calcola il numero totale di CV sulla curva
        num_cv = cmds.getAttr(curva_nome + '.spans') + cmds.getAttr(curva_nome + '.degree')

        # Calcola la distanza per ogni CV
        lista_selezione = []
        lista_distanze = []

        for cv in range(num_cv):  # Include tutti i CV
            pos_cv = cmds.pointPosition(curva_nome + '.cv[' + str(cv) + ']')
            locator1, locator2 = trova_locator_vicini(cv, lista_locator)

            # Calcola le distanze
            pos_locator1 = cmds.pointPosition(locator1)
            pos_locator2 = cmds.pointPosition(locator2)

            dist_locator1 = distanza_euclidea(pos_cv, pos_locator1)
            dist_locator2 = distanza_euclidea(pos_cv, pos_locator2)
            
            somma_distanze = dist_locator1 + dist_locator2

            dist_normalizzata1 = round(dist_locator1 / somma_distanze, 2)
            dist_normalizzata2 = round(1 - dist_normalizzata1, 2)

            # Crea lista selezione
            lista_selezione_tmp = [[locator1[0], "{}.cv[{}]".format(curva_nome, cv)], [locator2[0], "{}.cv[{}]".format(curva_nome, cv)]]

            # Crea lista valori
            x = round(dist_normalizzata1, 1)
            y = round(dist_normalizzata2, 1)
            lista_distanze_tmp = [(x, x, x), (y, y, y)]    

            lista_selezione.append(lista_selezione_tmp)
            lista_distanze.append(lista_distanze_tmp)

        return lista_selezione, lista_distanze   

 
    def unlock_rotate_on_mouth_controls(self, nameRig):
        
        # otteniamo self.loc_upper_list e self.loc_lower_list
        self.driver_locator(self.upLidDriverCrv)

        # trova distanza cv/locator
        self.lista_selezione, self.lista_valori = self.distanza_da_cv_a_locator(self.upLidCrv, self.loc_list)

    
    def trasforma_liste(self, lista_info_cv, lista_gruppi, curva):
        lista_target = []
        lista_valori = []

        tokens = curva.split("_")
        if tokens[0] == 'L':
            side = "L"
        else:
            side = "R"

        ckeck_jnts_name = []
        for loc, jnt in zip(self.loc_list, self.ctrlList):
            tmp = [loc[0], jnt]
            ckeck_jnts_name.append(tmp)

        for idx, lista in enumerate(lista_gruppi):
            tmp_target = []
            for i, v in enumerate(lista):
                jnt = []
                for item in ckeck_jnts_name:
                    if lista[i][0] == item[0]:
                        jnt.append(item[1])

                tmp_selezione = [jnt[0], "{}_Brow_{}_loc_MATRIX_Grp".format(side, idx), "{}_Brow_{}_loc_MASTER_Grp".format(side, idx)]
                tmp_target.append(tmp_selezione)
            lista_target.append(tmp_target)

        for valori in lista_info_cv:
            tmp_valori = [valori[1], valori[0]]
            valori = tmp_valori 

            lista_valori.append(valori)

        return lista_target, lista_valori
    

    def matrici_locator(self, lista_valori, lista_grp, curva): 
        selezione, valori = self.trasforma_liste(lista_valori, lista_grp, curva)

        numero_cv = len(lista_valori)
        half = len(lista_valori) / 2

        for idx, (lista_selezione, valori_rotate) in enumerate(zip(selezione, valori)):
            if numero_cv % 2 == 0:
                matrice =  mtx.MatrixConstraint(lista_selezione, r=valori_rotate).matrix_constraint()
            else:    
                if idx == half:
                    # fixa valori locator al centro
                    matrice =  mtx.MatrixConstraint(lista_selezione, r=[(1,1,1), (0,0,0)]).matrix_constraint()
                else:
                    matrice =  mtx.MatrixConstraint(lista_selezione, r=valori_rotate).matrix_constraint()


    def delete_driver_locator(self, loc_list):
        for loc in loc_list:
            cmds.delete(loc)


    # -----------------------------------------------------------------
    # Crea il sistema con le matrici
    # -----------------------------------------------------------------
    def create_matrix_system(self):
        # TODO: aggiornare valori da passare alle matrici per tutte le opzioni tranne quella con 4 controlli, che è OK. 
        if int(self.nCtrls) == 3:
            brow_01_list = [[self.brows_master_ctrl, "{}_Brow_01_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_01_Ctrl_MASTER_Grp".format(self.side)]]
            brow_02_list = [["{}_Brow_01_Ctrl".format(self.side), "{}_Brow_02_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_02_Ctrl_FOLLOW_Grp".format(self.side)],
                            ["{}_Brow_03_Ctrl".format(self.side), "{}_Brow_02_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_02_Ctrl_FOLLOW_Grp".format(self.side)],
                            [self.brows_master_ctrl, "{}_Brow_02_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_02_Ctrl_FOLLOW_Grp".format(self.side)]]
            brow_03_list = [[self.brows_master_ctrl, "{}_Brow_03_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_03_Ctrl_MASTER_Grp".format(self.side)]]
            
            mtx_brow_01 = mtx.MatrixConstraint(brow_01_list, t=[(1, 1, 1)], r=[(1, 1, 1)]).matrix_constraint()
            mtx_brow_02 = mtx.MatrixConstraint(brow_02_list, t=[(0.65, 0.65, 0.65), (0.35, 0.35, 0.35), (0, 0.3, 0)], r=[(0.65, 0.65, 0.65), (0.35, 0.35, 0.35), (0, 0.3, 0)]).matrix_constraint()
            mtx_brow_03 = mtx.MatrixConstraint(brow_03_list, t=[(1, 1, 1)], r=[(1, 1, 1)]).matrix_constraint()

        elif int(self.nCtrls) == 4:
            brow_01_list = [[self.brows_master_ctrl, "{}_Brow_01_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_01_Ctrl_MASTER_Grp".format(self.side)]]
            brow_02_list = [["{}_Brow_01_Ctrl".format(self.side), "{}_Brow_02_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_02_Ctrl_FOLLOW_Grp".format(self.side)],
                            ["{}_Brow_03_Ctrl".format(self.side), "{}_Brow_02_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_02_Ctrl_FOLLOW_Grp".format(self.side)],
                            [self.brows_master_ctrl, "{}_Brow_02_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_02_Ctrl_FOLLOW_Grp".format(self.side)]]
            brow_03_list = [[self.brows_master_ctrl, "{}_Brow_03_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_03_Ctrl_MASTER_Grp".format(self.side)]]
            brow_04_list = [["{}_Brow_03_Ctrl".format(self.side), "{}_Brow_04_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_04_Ctrl_FOLLOW_Grp".format(self.side)]]
            
            mtx_brow_01 = mtx.MatrixConstraint(brow_01_list, t=[(1, 1, 1)], r=[(1, 1, 1)]).matrix_constraint()
            mtx_brow_02 = mtx.MatrixConstraint(brow_02_list, t=[(0.65, 0.65, 0.65), (0.35, 0.35, 0.35), (0, 0.3, 0)], r=[(0.65, 0.65, 0.65), (0.35, 0.35, 0.35), (0, 0.3, 0)]).matrix_constraint()
            mtx_brow_03 = mtx.MatrixConstraint(brow_03_list, t=[(1, 1, 1)], r=[(1, 1, 1)]).matrix_constraint()
            mtx_brow_04 = mtx.MatrixConstraint(brow_04_list, t=[(0.5, 0.5, 0.5)], r=[(0.5, 0.5, 0.5)]).matrix_constraint()
            
        elif int(self.nCtrls) == 5:
            brow_01_list = [[self.brows_master_ctrl, "{}_Brow_01_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_01_Ctrl_MASTER_Grp".format(self.side)]]
            brow_02_list = [["{}_Brow_01_Ctrl".format(self.side), "{}_Brow_02_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_02_Ctrl_FOLLOW_Grp".format(self.side)],
                            ["{}_Brow_03_Ctrl".format(self.side), "{}_Brow_02_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_02_Ctrl_FOLLOW_Grp".format(self.side)]]
            brow_03_list = [[self.brows_master_ctrl, "{}_Brow_03_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_03_Ctrl_MASTER_Grp".format(self.side)]]
            brow_04_list = [["{}_Brow_03_Ctrl".format(self.side), "{}_Brow_04_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_04_Ctrl_FOLLOW_Grp".format(self.side)],
                            ["{}_Brow_05_Ctrl".format(self.side), "{}_Brow_04_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_04_Ctrl_FOLLOW_Grp".format(self.side)]]
            brow_05_list = [[self.brows_master_ctrl, "{}_Brow_05_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_05_Ctrl_MASTER_Grp".format(self.side)]]
            
            mtx_brow_01 = mtx.MatrixConstraint(brow_01_list, t=[(1, 1, 1)], r=[(1, 1, 1)]).matrix_constraint()
            mtx_brow_02 = mtx.MatrixConstraint(brow_02_list, t=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)], r=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)]).matrix_constraint()
            mtx_brow_03 = mtx.MatrixConstraint(brow_03_list, t=[(1, 1, 1)], r=[(1, 1, 1)]).matrix_constraint()
            mtx_brow_04 = mtx.MatrixConstraint(brow_04_list, t=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)], r=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)]).matrix_constraint()
            mtx_brow_05 = mtx.MatrixConstraint(brow_05_list, t=[(1, 1, 1)], r=[(1, 1, 1)]).matrix_constraint()
           
        elif int(self.nCtrls) == 6:
            brow_01_list = [[self.brows_master_ctrl, "{}_Brow_01_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_01_Ctrl_MASTER_Grp".format(self.side)]]
            brow_02_list = [["{}_Brow_01_Ctrl".format(self.side), "{}_Brow_02_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_02_Ctrl_FOLLOW_Grp".format(self.side)],
                            ["{}_Brow_03_Ctrl".format(self.side), "{}_Brow_02_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_02_Ctrl_FOLLOW_Grp".format(self.side)]]
            brow_03_list = [[self.brows_master_ctrl, "{}_Brow_03_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_03_Ctrl_MASTER_Grp".format(self.side)]]
            brow_04_list = [["{}_Brow_03_Ctrl".format(self.side), "{}_Brow_04_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_04_Ctrl_FOLLOW_Grp".format(self.side)],
                            ["{}_Brow_05_Ctrl".format(self.side), "{}_Brow_04_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_04_Ctrl_FOLLOW_Grp".format(self.side)]]
            brow_05_list = [[self.brows_master_ctrl, "{}_Brow_05_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_05_Ctrl_MASTER_Grp".format(self.side)]]
            brow_06_list = [["{}_Brow_05_Ctrl".format(self.side), "{}_Brow_06_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_06_Ctrl_FOLLOW_Grp".format(self.side)]]
            
            mtx_brow_01 = mtx.MatrixConstraint(brow_01_list, t=[(1, 1, 1)], r=[(1, 1, 1)]).matrix_constraint()
            mtx_brow_02 = mtx.MatrixConstraint(brow_02_list, t=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)], r=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)]).matrix_constraint()
            mtx_brow_03 = mtx.MatrixConstraint(brow_03_list, t=[(1, 1, 1)], r=[(1, 1, 1)]).matrix_constraint()
            mtx_brow_04 = mtx.MatrixConstraint(brow_04_list, t=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)], r=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)]).matrix_constraint()
            mtx_brow_05 = mtx.MatrixConstraint(brow_05_list, t=[(1, 1, 1)], r=[(1, 1, 1)]).matrix_constraint()
            mtx_brow_06 = mtx.MatrixConstraint(brow_06_list, t=[(0.5, 0.5, 0.5)], r=[(0.5, 0.5, 0.5)]).matrix_constraint()
           

        elif int(self.nCtrls) == 7:
            brow_01_list = [[self.brows_master_ctrl, "{}_Brow_01_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_01_Ctrl_MASTER_Grp".format(self.side)]]
            brow_02_list = [["{}_Brow_01_Ctrl".format(self.side), "{}_Brow_02_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_02_Ctrl_FOLLOW_Grp".format(self.side)],
                            ["{}_Brow_03_Ctrl".format(self.side), "{}_Brow_02_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_02_Ctrl_FOLLOW_Grp".format(self.side)]]
            brow_03_list = [[self.brows_master_ctrl, "{}_Brow_03_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_03_Ctrl_MASTER_Grp".format(self.side)]]
            brow_04_list = [["{}_Brow_03_Ctrl".format(self.side), "{}_Brow_04_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_04_Ctrl_FOLLOW_Grp".format(self.side)],
                            ["{}_Brow_05_Ctrl".format(self.side), "{}_Brow_04_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_04_Ctrl_FOLLOW_Grp".format(self.side)]]
            brow_05_list = [[self.brows_master_ctrl, "{}_Brow_05_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_05_Ctrl_MASTER_Grp".format(self.side)]]
            brow_06_list = [["{}_Brow_05_Ctrl".format(self.side), "{}_Brow_06_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_06_Ctrl_FOLLOW_Grp".format(self.side)],
                            ["{}_Brow_07_Ctrl".format(self.side), "{}_Brow_06_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_06_Ctrl_FOLLOW_Grp".format(self.side)]]
            brow_07_list = [[self.brows_master_ctrl, "{}_Brow_07_Ctrl_MATRIX_Grp".format(self.side), "{}_Brow_07_Ctrl_MASTER_Grp".format(self.side)]]
            
            mtx_brow_01 = mtx.MatrixConstraint(brow_01_list, t=[(1, 1, 1)], r=[(1, 1, 1)]).matrix_constraint()
            mtx_brow_02 = mtx.MatrixConstraint(brow_02_list, t=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)], r=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)]).matrix_constraint()
            mtx_brow_03 = mtx.MatrixConstraint(brow_03_list, t=[(1, 1, 1)], r=[(1, 1, 1)]).matrix_constraint()
            mtx_brow_04 = mtx.MatrixConstraint(brow_04_list, t=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)], r=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)]).matrix_constraint()
            mtx_brow_05 = mtx.MatrixConstraint(brow_05_list, t=[(1, 1, 1)], r=[(1, 1, 1)]).matrix_constraint()
            mtx_brow_06 = mtx.MatrixConstraint(brow_06_list, t=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)], r=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)]).matrix_constraint()
            mtx_brow_07 = mtx.MatrixConstraint(brow_07_list, t=[(1, 1, 1)], r=[(1, 1, 1)]).matrix_constraint()


    # -----------------------------------------------------------------
    # Sistema Squint Follow
    # -----------------------------------------------------------------
    def squint_follow(self):
        squint_slider = []

        # EYESOCKET part
        # matrice
        mtx_squint_socket_list = [[self.brows_master_ctrl, "{}_EyeSocket_Up_Ctrl_SQUINT_Grp_Static".format(self.side), "{}_EyeSocket_Up_Ctrl_MASTER_Grp_Static".format(self.side)]]
        mtx_squint_socket = mtx.MatrixConstraint(mtx_squint_socket_list, t=[(0, 0.9, 1)], r=[(0.5, 0.5, 0.5)]).matrix_constraint()
        
        # creazione nodi        
        mlp_squint_socket = cmds.createNode("multiplyDivide", n="MLD_{}_Brow_Master_Ctrl_to_{}_EyeSocket_Up_Ctrl_SQUINT_Grp".format(self.side, self.side))

        # connessioni
        # brow_master --> mlp_squint_socket
        for attr in ("X", "Y", "Z"):
            cmds.connectAttr("{}.squint_follow".format(self.brows_master_ctrl),  "{}.input1{}".format(mlp_squint_socket, attr), force=True)
        # PLS_translate --> mlp_squint_socket
        cmds.connectAttr("{}.output3D".format(mtx_squint_socket[1][0]), "{}.input2".format(mlp_squint_socket), force=True)
        # mlp_squint_socket --> SQUINT_Grp
        cmds.connectAttr("{}.output".format(mlp_squint_socket), "{}_EyeSocket_Up_Ctrl_SQUINT_Grp.translate".format(self.side), force=True)

            
        # EYELID part
        # matrice
        mtx_squint_eyelid_list = [[self.brows_master_ctrl, "{}_Eyelid_Up_Ctrl_SQUINT_Grp_Static".format(self.side), "{}_Eyelid_Up_Ctrl_MASTER_Grp_Static".format(self.side)]]
        mtx_squint_eyelid = mtx.MatrixConstraint(mtx_squint_eyelid_list, t=[(0, 0.8, 0)], r=[(0.5, 0.5, 0.5)]).matrix_constraint()

        # creazione nodi        
        mlp_squint_eyelid = cmds.createNode("multiplyDivide", n="MLD_{}_Brow_Master_Ctrl_to_{}_Eyelid_Up_Ctrl_SQUINT_Grp".format(self.side, self.side))

        sr_squint_eyelid_slider = cmds.createNode("setRange", n="SR_{}_Brow_Master_Ctrl_to_{}_Eyelid_Up_Ctrl_SQUINT_Grp".format(self.side, self.side))
        squint_slider.append(sr_squint_eyelid_slider)
        cmds.setAttr("{}.minX".format(sr_squint_eyelid_slider), -2)
        cmds.setAttr("{}.maxY".format(sr_squint_eyelid_slider), 4)
        cmds.setAttr("{}.oldMinX".format(sr_squint_eyelid_slider), -4)
        cmds.setAttr("{}.oldMaxY".format(sr_squint_eyelid_slider), 4)

        pls_squint_eyelid = cmds.createNode("plusMinusAverage", n="PLS_{}_Brow_Master_Ctrl_to_{}_Eyelid_Up_Ctrl_SQUINT_Grp".format(self.side, self.side))

        # connessioni
        # brow_master --> mlp_squint_eyelid
        for attr in ("X", "Y", "Z"):
            cmds.connectAttr("{}.squint_follow".format(self.brows_master_ctrl),  "{}.input1{}".format(mlp_squint_eyelid, attr), force=True)
        # PLS_translate --> mlp_squint_eyelid
        cmds.connectAttr("{}.output3D".format(mtx_squint_eyelid[1][0]), "{}.input2".format(mlp_squint_eyelid), force=True)
        # mlp_squint_eyelid --> sr_squint_eyelid_slider
        for attr in ("X", "Y", "Z"):
            cmds.connectAttr("{}.outputY".format(mlp_squint_eyelid), "{}.value{}".format(sr_squint_eyelid_slider, attr), force=True)
        # sr_squint_eyelid_slider --> pls_squint_eyelid
        for idx, attr in enumerate(("X", "Y")):
            cmds.connectAttr("{}.outValue{}".format(sr_squint_eyelid_slider, attr), "{}.input1D[{}]".format(pls_squint_eyelid, idx), force=True)
        # pls_squint_eyelid --> SQUINT_Grp
        cmds.connectAttr("{}.output1D".format(pls_squint_eyelid), "{}_Eyelid_Up_Ctrl_SQUINT_Grp_Static.translateY".format(self.side), force=True)

        
        # SLIDER list
        for item in squint_slider:
            self.set_sliders.append(item)


    # -----------------------------------------------------------------
    # Finalizza il modulo
    # -----------------------------------------------------------------
    def duplicate_and_rename_groups_and_controls(self, base_group, rig_hierarchy):
        """Fa un duplicato di 'base_group' e rinominalo con tutto il suo contenuto col suffisso '_Static'."""
        # Rinomina il gruppo dei controlli 'base_group' col suffisso '_Static'.
        self.static_grp = cmds.rename(base_group, "{}_Static".format(base_group))
        
        # Duplicalo come 'driver_grp'
        driver_grp = cmds.duplicate(self.static_grp, n=base_group)[0]

        # Pulisci il driver_grp da eventuali constraint
        children = cmds.listRelatives(driver_grp, allDescendents=True, fullPath=True, shapes=False)
        for item in children:
            check_type = cmds.nodeType(item)
            if 'Constraint' in check_type:
                cmds.delete(item)

        # Rinomina tutti i gruppi e controlli del gruppo 'static_grp' col suffisso '_Static', saltando le shapes.
        children = cmds.listRelatives(self.static_grp, allDescendents=True, fullPath=True, shapes=False)
        for item in children:
            check_type = cmds.nodeType(item)
            if check_type == 'transform':
                tmp = item.split("|")[-1]
                cmds.rename(item, "{}_Static".format(tmp))

        # Metti in gerachia driver_grp e nascondi lo static_grp
        side = driver_grp.split("_")[0]
        if side == 'L':
            cmds.parent(driver_grp, rig_hierarchy[0])
        elif side == 'R':
            cmds.parent(driver_grp, rig_hierarchy[1])
        cmds.setAttr("{}.visibility".format(self.static_grp), 0)

        print("# Duplicato e rinominato tutto il contenuto di '{}' col suffisso '_Static'. #".format(base_group))


    def cleanup_set_controls(self, set_name):
        """Elimina controlli 'Static' dal set dei controlli."""
        set_tmp = cmds.sets(set_name, q=True)

        for item in set_tmp:
            tokens = item.split("_")
            if tokens[-1] == "Static":
                cmds.sets(item, remove=set_name) 


    def connect_driver_to_static(self, driver_grp, static_grp):
        """Crea connessioni dirette tra i controlli e gruppi Matrix di driver e static """
        children_static = cmds.listRelatives(static_grp, allDescendents=True, fullPath=False, shapes=False)
        children_driver = cmds.listRelatives(driver_grp, allDescendents=True, fullPath=False, shapes=False)

        # '_Ctrl' --> '_Ctrl_Static'
        for static_ctrl, driver_ctrl in zip(children_static, children_driver):
            if cmds.nodeType(driver_ctrl) == 'transform':
                name_tokens = driver_ctrl.split("_")[-1]
                if name_tokens == "Ctrl":
                    driver_attrs = cmds.listAttr(driver_ctrl, keyable=True) or []
                    for attr in driver_attrs:
                        if cmds.attributeQuery(attr, node=static_ctrl, exists=True) and cmds.getAttr(driver_ctrl + '.' + attr, keyable=True):
                            cmds.connectAttr("{}.{}".format(driver_ctrl, attr), "{}.{}".format(static_ctrl, attr), force=True)

        # '_Ctrl_MATRIX_Grp_Static' --> '_Ctrl_MATRIX_Grp' valido per [MATRIX, FOLLOW, SQUARE, COMPENSATE]
        lista_gruppi = ('MATRIX', 'FOLLOW', 'SQUARE', 'COMPENSATE')
        for static_grp, driver_grp in zip(children_static, children_driver):
            if cmds.nodeType(driver_grp) == 'transform':
                name_tokens = driver_grp.split("_")
                for grp in lista_gruppi:
                    if grp in name_tokens:
                        driver_attrs = cmds.listAttr(static_grp, keyable=True) or []
                        for attr in driver_attrs:
                            if cmds.attributeQuery(attr, node=driver_grp, exists=True) and cmds.getAttr("{}.{}".format(static_grp, attr), keyable=True):
                                cmds.connectAttr("{}.{}".format(static_grp, attr), "{}.{}".format(driver_grp, attr), force=True)

        print("# Connessioni dirette completate tra driver e static. #")


    def make_sliders(self, nameRig, nodi):
        """Rinomina i nodi passati come input con il suffisso 'SLIDER' e se non esiste crea il set slider corrispondente."""
        sliders = []

        for item in nodi:
            nodo = cmds.rename(item, "{}_SLIDER".format(item))
            sliders.append(nodo)

        # Controlla se il set globale degli sliders esiste, altrimenti crealo
        if not cmds.objExists("SLIDERS"):
            sliders_set = cmds.sets(empty=True, name=("SLIDERS"))
        else:
            sliders_set = "SLIDERS"

        # Controlla se il set della parte di rig degli sliders esiste, altrimenti crealo
        if not cmds.objExists("SLIDERS_HeadEyes"):
            rigpart_sliders_set = cmds.sets(empty=True, name=("SLIDERS_HeadEyes"))
            # Parenta il set della parte di rig dentro al set globale
            cmds.sets(rigpart_sliders_set, edit=True, forceElement=sliders_set)
        else:
            rigpart_sliders_set = "SLIDERS_HeadEyes"

        # crea sotto-set del singolo modulo
        if not cmds.objExists("{}_sliders".format(nameRig)):
            set_module_sliders = cmds.sets(empty=True, name=("{}_sliders".format(nameRig)))
            # Parenta il set del modulo dentro al set globale
            cmds.sets(set_module_sliders, edit=True, forceElement=rigpart_sliders_set)
        else:
            set_module_sliders = "{}_sliders".format(nameRig)

        # Popola il set
        for nodo in sliders:
            cmds.sets(nodo, edit=True, forceElement=set_module_sliders)

        print("# Sliders rinominati e set creato. #")


    def insert_driver_crv_in_set_slider(self, nameRig, curve):
        set_module_sliders = "{}_sliders".format(nameRig)
        cmds.sets(curve, edit=True, forceElement=set_module_sliders)


    def rename_ctrl_shapes(self, set_ctrl):
        """Data una selezione di controlli, rinomina le shapes in base al nome del controllo."""
        cmds.select(clear=True)
        cmds.select(set_ctrl)
        sel = cmds.ls(sl=True)
        cmds.select(clear=True)
        
        for ctrl in sel:
            shapes = cmds.listRelatives(ctrl, shapes=True, pa=True)
            for shape in shapes:
                new_name = "{}Shape".format(ctrl)
                cmds.rename(shape, new_name)

        print("### Le shapes dei controlli sono state rinominate correttamente. ###")


    # -----------------------------------------------------------------
    # Funzioni lanciate dalla UI
    # -----------------------------------------------------------------
    def buildRig(self, *args):
        '''Build eyelids rig.
        
        Called by 'UI' function.
        Call functions: 'vtxToJnt', 'placeRigLoc', 'createBrowsCrv', 'connectLocToCrv', 
                        'createDriverCrv', 'createJntCtrls', 'createCrvCtrls', 'addSmartBlink' '''
        
        if self.browsLoc == None or self.browsVtx == None:
            cmds.error("Please define eye center and eyelids vertices.")
        else : # Call functions to build rig #
            # Step 0: retrieve the number of controls to build
            self.input_number_ctrls()
            # Step 1: creates a "high-res" curve for each brow (each vertex is a point of the curve)
            self.createBrowsCrv(self.nameRig, self.browsVtx, self.grpBaseRig)
            # Step 2: places one joint per curve cv
            self.cv_base_curve_to_joint (self.browsLoc, self.upLidCrv)
            # Step 3: places one locator per curve cv and constrain-aim each joint to it (so as it acts like an IK)
            self.place_rig_locators(self.nameRig, self.upLidJntList)
            # Step 4: connects each locator to the curve with a pointOnCurve node, so when the CVs of the curve move, the corresponding locator follows (and so does the joint)
            self.connectLocToCrv(self.upLidLocList, self.upLidCrv)
            # Step 5: creates a "low-res" curve with only 5 control points and makes it drive the high-res curve with a wire deformer
            self.createDriverCrv(self.upLidCrv, self.hierarchyUpCrvGrp)
            # Step 6: creates controller joints to drive the 'driver curve'
            self.createJntCtrls(self.nameRig, self.cornerAPos, self.cornerBPos, self.upLidDriverCrv, self.grpBaseRig)
            # Step 7: creates curve controllers, and attached the corresponding joints to them
            self.createCrvCtrls(self.nameRig, self.ctrlJnts)
            # Step 8: crea controlli brow_master con relativa gerarchia di gruppi
            self.check_position_master_ctrl(self.ctrlList, self.master_grp_list, self.matrix_grp_list)
            self.add_master_ctrl(self.nameRig, self.ctrlList, self.matrix_grp_list, self.index)
            # Step 9: skinna la curva driver con i ctrlJnts 
            self.bindskin_ctrljnts_to_curve(self.upLidDriverCrv, self.ctrlJnts)
            # Step 10: import the new controls_shapes and replace them
            self.replace_and_remove_shapes()
            # Step 11: matrici only rotate per i locator
            self.unlock_rotate_on_mouth_controls(self.nameRig)
            self.matrici_locator(self.lista_valori, self.lista_selezione, self.upLidCrv)
            self.delete_driver_locator(self.loc_list)
            # Step 12: crea tutte le connessioni via matrici
            self.create_matrix_system()
            # Step 13: crea squint_follow
            self.squint_follow()
            # Step 14: finalizza il modulo
            self.duplicate_and_rename_groups_and_controls(self.hierarchyMainGrpCTRL, self.parent_rig)
            self.cleanup_set_controls(self.set_brow_ctrls)
            self.connect_driver_to_static(self.hierarchyMainGrpCTRL, self.static_grp)
            self.make_sliders(self.nameRig, self.set_sliders)
            self.insert_driver_crv_in_set_slider(self.nameRig, self.upLidDriverCrv)
            self.rename_ctrl_shapes(self.set_brow_ctrls)
            

            # Clear scene & script variables #
            cmds.delete(self.browsLoc)
            cmds.select(cl = 1)
            
            self.browsLoc = None
            self.browName = None
            self.browsVtx = None
            self.grpAllRig = None
            self.grpBaseRig = None
            self.hierarchyMainGrpCTRL = None
            self.hierarchyMainGrpJNT = None
            self.set_brow_ctrls = None
            self.set_sliders[:] = []

            # Update UI #
            cmds.textField(self.txtfLoc, e = 1, tx = "")
            cmds.button(self.btnPlaceCenter, e = 1, en = 1)
            cmds.button(self.btnUndoPlaceCenter, e = 1, en = 0)
            cmds.scrollField(self.scrollfUpLid, e = 1, cl = 1)
            cmds.intSliderGrp(self.inputCtrls, edit=True, value=4)
            
            # End message
            print("### {}_EyeBrow have been successfully rigged. ###".format(self.side))
    
    
    # -----------------------------------------------------------------
    # UI
    # -----------------------------------------------------------------
    def UI(self):
        '''Creates UI - Main function
        
        Call functions: 'browParentJnt', 'browParentCtrl', 'placeBrowsCenter', 'placeBrowsCenterUndo', 
                        'upLidVtxSet', 'lowLidVtxSet', 'buildRig' '''

        # Main window
        
        winWidth = 290
        
        UKDP_mainWin = "RBW_EyeBrows_Rig"

        if cmds.window("RBW_EyeBrows_Rig", exists = 1):
            cmds.deleteUI("RBW_EyeBrows_Rig", window = 1)

        ww = cmds.window("RBW_EyeBrows_Rig", title = "RBW EyeBrows Rig", mxb = 0, sizeable=True, resizeToFitChildren=True)

        # Get a pointer and convert it to Qt Widget object
        qw = omui.MQtUtil.findWindow(ww)
        widget = wrapInstance(int(qw), QWidget)

        # Create a QIcon object
        iconpath = os.path.join(self.image_path, "RainbowCGI_icona.ico")

        # Assign the icon
        icon = QIcon(iconpath)
        widget.setWindowIcon(icon)

        # Main layout
        col = cmds.columnLayout(co=("both", 5), adjustableColumn=True)
        
        # Carica l'immagine delle brows
        img = os.path.join(self.image_path, "brows.png")
        cmds.text(h = 10, l = "")
        cmds.iconTextButton(style="iconOnly", image1=img, p=col, w=100)
        cmds.text(h = 10, l = "")
        cmds.setParent("..")

        # Fields
        cmds.frameLayout( label='1. Select a side', marginHeight=10, bgc = (0.50, 0.125, 0.345))
        cmds.columnLayout( adjustableColumn=True )
        self.radioChecked = cmds.radioButtonGrp(labelArray2=['Left', 'Right'], on1=partial(self.side_eye, 'L') , on2=partial(self.side_eye, 'R') , numberOfRadioButtons=2, columnWidth2=(150, 150), columnAttach2=('left', 'right'), columnOffset2=(80, 80))
        cmds.setParent("..")
        cmds.setParent("..")

        cmds.separator(h = 15, w = winWidth, style = "in")

        # Define brows center
        cmds.frameLayout( label="2. Select brows pivot, then click 'Place center'", marginHeight=10, bgc = (0.50, 0.125, 0.345))
        cmds.rowLayout(numberOfColumns = 2, adjustableColumn = 1)
        self.btnPlaceCenter = cmds.button(w = ((winWidth / 2) - 2), l = "Place center", c = self.placeBrowsCenter)
        self.btnUndoPlaceCenter = cmds.button(w = ((winWidth / 2) - 2), l = "Undo", c = self.placeBrowsCenterUndo, en = 0)

        cmds.setParent("..")
        cmds.setParent("..")
        cmds.text(h = 5, l = "")
        self.txtfLoc = cmds.textField(w = winWidth, ed = 0)
        
        cmds.separator(h = 15, w = winWidth, style = "in")

        # List brows vertices
        cmds.frameLayout( label="3. Select vertices of brow, then click 'Set''", marginHeight=10, bgc = (0.50, 0.125, 0.345))
        self.btnUpLid = cmds.button(w = winWidth, l = "Set", c = self.upLidVtxSet)
        self.scrollfUpLid = cmds.scrollField(w = winWidth, h = 35, wordWrap = 1, ed = 0, en = 0)
        cmds.setParent("..")

        cmds.separator (h = 15, w = winWidth, style = "in")
    
        # Choose the number of controls
        cmds.frameLayout( label="4. Choose the number of controls", marginHeight=10, bgc = (0.50, 0.125, 0.345))
        self.inputCtrls = cmds.intSliderGrp("nCtrls", field=True, label="n. Ctrls:", minValue=3, maxValue=7, value=4, cw=[(1, 50), (2, 40)], cat=[(2, 'both', 10), (3, 'right', 10)])
        cmds.setParent("..")
        
        cmds.text(h = 5, l = "")

        # Build final rig
        self.btnBuild = cmds.button(w = winWidth, h = 60, l = "BUILD RIG", c = self.buildRig, bgc = (0.50, 0.125, 0.345))
        
        cmds.text(h = 10, l = "")
        
        cmds.showWindow("RBW_EyeBrows_Rig")




autoEyeBrowsRig = AER()
autoEyeBrowsRig.UI() # load UI