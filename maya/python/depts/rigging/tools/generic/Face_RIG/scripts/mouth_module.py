# -*- coding: utf-8 -*-

import maya.cmds as cmds
from maya import OpenMayaUI as omui
import os, sys
from functools import partial
from imp import reload

# -----------------------------------------------------------------
# Path
# -----------------------------------------------------------------
localpipe = os.getenv("LOCALPY")
myPath = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "Face_RIG")
script_path = os.path.join(myPath, "scripts")
scene_path = os.path.join(myPath, "scenes", "controls_shape.ma")
image_path = os.path.join(myPath, "icons")


# -----------------------------------------------------------------
# PySide
# -----------------------------------------------------------------
try:
    from shiboken2 import wrapInstance
except ImportError:
    from shiboken import wrapInstance

try:
    from PySide2.QtGui import QIcon
    from PySide2.QtWidgets import QWidget
except ImportError:
    from PySide.QtGui import QIcon, QWidget


# -----------------------------------------------------------------
# Funzioni lanciate dalla UI
# -----------------------------------------------------------------
def mouth_button_push(*args):
    import depts.rigging.tools.generic.Face_RIG.scripts.mouth_build as mouth_build
    reload(mouth_build)
    autoMouthRig = mouth_build.AER()
    autoMouthRig.UI()

def lip_roll_button_push(*args):
    import depts.rigging.tools.generic.Face_RIG.scripts.mouth_lip_roll as mouth_lip_roll
    reload(mouth_lip_roll)
    autoLipRoll = mouth_lip_roll.LipRoll()
    autoLipRoll.UI()


# -----------------------------------------------------------------
# UI
# -----------------------------------------------------------------
def mouth_module_UI(path):
    winWidth = 290
    
    main_window = "RBW_Mouth_Module"

    if cmds.window(main_window, exists = 1):
        cmds.deleteUI(main_window, window = 1)

    ww = cmds.window(main_window, title=main_window, mxb = 0, sizeable=True, resizeToFitChildren=True)

    # Get a pointer and convert it to Qt Widget object
    qw = omui.MQtUtil.findWindow(ww)
    widget = wrapInstance(int(qw), QWidget)

    # Create a QIcon object
    iconpath = os.path.join(image_path, "RainbowCGI_icona.ico")

    # Assign the icon
    icon = QIcon(iconpath)
    widget.setWindowIcon(icon)

    # Main layout
    col = cmds.columnLayout(co=("both", 5), adjustableColumn=True, bgc=(0.225, 0.225, 0.225))
    
    # Carica l'immagine della mouth
    img = os.path.join(image_path, "mouth.png")
    cmds.text(h = 10, l = "")
    cmds.iconTextButton(style="iconOnly", image1=img, p=col, w=100)
    cmds.text(h = 10, l = "")
    cmds.setParent("..")

    # FIELDS
    # Mouth
    cmds.frameLayout( label="1. Modulo per creare il rig della mouth.", marginHeight=10, bgc = (0.4, 0.4, 0.4))
    btn_mouth_module = cmds.button(w=winWidth, h=40, l="| Mouth Module |", c=mouth_button_push, bgc=(0.3,0.3,1.0))
    cmds.setParent("..")
    
    cmds.separator (h = 10, w = winWidth, style = "none")

    # Lip Roll
    cmds.frameLayout( label="2. Modulo per creare il sistema dei lip roll.", marginHeight=10, bgc = (0.4, 0.4, 0.4))
    btn_liproll_module = cmds.button(w=winWidth, h=40, l="| Lip Roll Module |", c=lip_roll_button_push, bgc = (0.6, 0.6, 0.6))
    cmds.setParent("..")

    cmds.text(h = 10, l = "")
    
    cmds.showWindow(main_window)

# Caricamento UI
mouth_module_UI(myPath)