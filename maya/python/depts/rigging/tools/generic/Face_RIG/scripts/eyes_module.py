# -*- coding: utf-8 -*-

import maya.cmds as cmds
from maya import OpenMayaUI as omui
import os, sys
from functools import partial
from imp import reload

# -----------------------------------------------------------------
# Path
# -----------------------------------------------------------------
localpipe = os.getenv("LOCALPY")
myPath = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "Face_RIG")
image_path = os.path.join(myPath, "icons")


# -----------------------------------------------------------------
# PySide
# -----------------------------------------------------------------
try:
    from shiboken2 import wrapInstance
except ImportError:
    from shiboken import wrapInstance

try:
    from PySide2.QtGui import QIcon
    from PySide2.QtWidgets import QWidget
except ImportError:
    from PySide.QtGui import QIcon, QWidget


# -----------------------------------------------------------------
# Funzioni lanciate dalla UI
# -----------------------------------------------------------------
def eyelid_button_push(*args):
    import depts.rigging.tools.generic.Face_RIG.scripts.eyelid_build as eyelid_build
    reload(eyelid_build)
    autoEyelidsRig = eyelid_build.AER()
    autoEyelidsRig.UI()

def eyesocket_button_push(*args):
    import depts.rigging.tools.generic.Face_RIG.scripts.eyesocket_build as eyesocket_build
    reload(eyesocket_build)
    autoEyeSocketsRig = eyesocket_build.AER()
    autoEyeSocketsRig.UI()

def eyelashes_button_push(*args):
    import depts.rigging.tools.generic.Face_RIG.scripts.eyelashes_build as eyelashes_build
    reload(eyelashes_build)
    autoEyelashesRig = eyelashes_build.AER()
    autoEyelashesRig.buildRig()

def finalize_eye_button_push(*args):
    import depts.rigging.tools.generic.Face_RIG.scripts.finalize_eye_pose as finalize_eye_pose
    reload(finalize_eye_pose)
    finalize_eye_pose.finalize_eye_pose()

# -----------------------------------------------------------------
# UI
# -----------------------------------------------------------------
def eyes_module_UI():
    winWidth = 290
    
    main_window = "RBW_Eyes_Module"

    if cmds.window(main_window, exists = 1):
        cmds.deleteUI(main_window, window = 1)

    ww = cmds.window(main_window, title=main_window, mxb = 0, sizeable=True, resizeToFitChildren=True)

    # Get a pointer and convert it to Qt Widget object
    qw = omui.MQtUtil.findWindow(ww)
    widget = wrapInstance(int(qw), QWidget)

    # Create a QIcon object
    iconpath = os.path.join(image_path, "RainbowCGI_icona.ico")

    # Assign the icon
    icon = QIcon(iconpath)
    widget.setWindowIcon(icon)

    # Main layout
    col = cmds.columnLayout(co=("both", 5), adjustableColumn=True, bgc=(0.225, 0.225, 0.225))
    
    # Carica l'immagine degli eyesockets
    img = os.path.join(image_path, "eye.png")
    cmds.text(h = 10, l = "")
    cmds.iconTextButton(style="iconOnly", image1=img, p=col, w=100)
    cmds.text(h = 10, l = "")
    cmds.setParent("..")

    # FIELDS
    # Eyelids
    cmds.frameLayout( label="1. Modulo per creare il rig delle eyelids.'", marginHeight=10, bgc = (0.4, 0.4, 0.4))
    btn_eyelid_module = cmds.button(w=winWidth, h=40, l="| Eyelid Module |", c=eyelid_button_push, bgc=(1.0, 0.2, 0.2))
    cmds.setParent("..")
    
    cmds.separator (h=10, w=winWidth, style="none")

    # Eyesockets
    cmds.frameLayout( label="2. Modulo per creare il rig degli eyesockets.", marginHeight=10, bgc = (0.4, 0.4, 0.4))
    btn_eyesicket_module = cmds.button(w=winWidth, h=40, l="| Eyesocket Module |", c=eyesocket_button_push, bgc = (0.8, 0.50, 0.0))
    cmds.setParent("..")

    cmds.separator (h=10, w=winWidth, style="none")

    # Eyelashes
    cmds.frameLayout( label="3. Eyelashes: seleziona il gruppo 'Static' delle eyelids e lancia lo script.", marginHeight=10, bgc = (0.4, 0.4, 0.4))
    btn_eyelashes_module = cmds.button(w=winWidth, h=40, l="| Eyelashes Module |", c=eyelashes_button_push, bgc=(0.9, 0.6, 0.4))
    cmds.setParent("..")

    cmds.separator(h=10, w=winWidth, style="none")

    # Finalize Eye Pose
    cmds.frameLayout( label="4. Metti gli occhi in posa con i 2 controlli 'eye_Ctrl' e lancia lo script.", marginHeight=10, bgc = (0.4, 0.4, 0.4))
    btnBuild = cmds.button(w=winWidth, h=40, l = "| Finalize Eye Pose |", c=finalize_eye_button_push, bgc=(0.8, 0.8, 0.8))
    cmds.setParent("..")
    
    cmds.text(h = 10, l = "")
    
    cmds.showWindow(main_window)


# Caricamento UI
eyes_module_UI()