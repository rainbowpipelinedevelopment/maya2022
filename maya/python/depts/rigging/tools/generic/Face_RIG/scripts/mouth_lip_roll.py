# -*- coding: utf-8 -*-

import maya.cmds as cmds
import os
from functools import partial


class LipRoll:
    def __init__(self):
        self.locators = []
        self.groups = []
        self.groups_static = []
        self.up_lip_ctrl = "UpLip_Roll_Ctrl"
        self.low_lip_ctrl = "LowLip_Roll_Ctrl"
        # -----------------------------------------------------------------
        # Path
        # -----------------------------------------------------------------
        localpipe = os.getenv("LOCALPY")
        myPath = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "Face_RIG")
        script_path = os.path.join(myPath, "scripts")
        scene_path = os.path.join(myPath, "scenes", "controls_shape.ma")
        image_path = os.path.join(myPath, "icons", "mouth_icon")

        self.my_file_path = scene_path
        self.image_path = image_path

    # -----------------------------------------------------------------
    # Funzioni per creare il sistema lip roll
    # -----------------------------------------------------------------
    def _create_undo_set(self, ctrl, *args):
        for item in args:
            if cmds.objExists("{}_REMOVE_SET".format(ctrl)):
                cmds.sets(item, e=True, add="{}_REMOVE_SET".format(ctrl))
            else:
                ctrl_set = cmds.sets(item, name="{}_REMOVE_SET".format(ctrl))
                cmds.setAttr("{}.hiddenInOutliner".format(ctrl_set), True)


    def _delete_locators(self, ctrl, mirror):
        """Cancella i locator di costruzione."""
        if mirror == False:
            for loc in ("{}_roll_IN_loc".format(ctrl), "{}_roll_OUT_loc".format(ctrl)):
                if cmds.objExists(loc):
                    cmds.delete(loc)
        else:
            for side in ("L", "R"):
                for loc in ("{}_{}_roll_IN_loc".format(side, ctrl), "{}_{}_roll_OUT_loc".format(side, ctrl)):
                    if cmds.objExists(loc):
                        cmds.delete(loc)
        

    def _make_groups_utility(self, ctrl):
        """Utility per creare i gruppi rinominati correttamente per il sistema lip_roll."""
        # driver
        groups_in_list = []
        groups_out_list = []
        
        grp_in_pivot = cmds.group(name="{}_ROLLINPIVOT_Grp".format(ctrl), empty=True)
        grp_in = cmds.group(name="{}_ROLLIN_Grp".format(ctrl), empty=True)
        grp_in_reset = cmds.group(name="{}_ROLLINRESET_Grp".format(ctrl), empty=True)
        groups_in_list.append(grp_in_pivot)
        groups_in_list.append(grp_in)
        groups_in_list.append(grp_in_reset)

        grp_out_pivot = cmds.group(name="{}_ROLLOUTPIVOT_Grp".format(ctrl), empty=True)
        grp_out = cmds.group(name="{}_ROLLOUT_Grp".format(ctrl), empty=True)
        grp_out_reset = cmds.group(name="{}_ROLLOUTRESET_Grp".format(ctrl), empty=True)
        groups_out_list.append(grp_out_pivot)
        groups_out_list.append(grp_out)
        groups_out_list.append(grp_out_reset)

        self.groups.append(groups_in_list)
        self.groups.append(groups_out_list)

        # static
        groups_in_static_list = []
        groups_out_static_list = []
        
        grp_in_pivot_static = cmds.group(name="{}_ROLLINPIVOT_Grp_Static".format(ctrl), empty=True)
        grp_in_static = cmds.group(name="{}_ROLLIN_Grp_Static".format(ctrl), empty=True)
        grp_in_reset_static = cmds.group(name="{}_ROLLINRESET_Grp_Static".format(ctrl), empty=True)
        groups_in_static_list.append(grp_in_pivot_static)
        groups_in_static_list.append(grp_in_static)
        groups_in_static_list.append(grp_in_reset_static)
        
        grp_out_pivot_static = cmds.group(name="{}_ROLLOUTPIVOT_Grp_Static".format(ctrl), empty=True)
        grp_out_static = cmds.group(name="{}_ROLLOUT_Grp_Static".format(ctrl), empty=True)
        grp_out_reset_static = cmds.group(name="{}_ROLLOUTRESET_Grp_Static".format(ctrl), empty=True)
        groups_out_static_list.append(grp_out_pivot_static)
        groups_out_static_list.append(grp_out_static)
        groups_out_static_list.append(grp_out_reset_static)
        
        self.groups_static.append(groups_in_static_list)
        self.groups_static.append(groups_out_static_list)

        return grp_in_pivot_static, grp_out_pivot_static

    
    def _make_hierarchy_utility(self, ctrl, loc_list, grp_list):
        """Utility per mettere i gruppi del sistema lilp_roll in gerarchia."""
        parent_grp = cmds.listRelatives(ctrl, ap=True)

        cmds.parent(grp_list[0][1], grp_list[0][0]) # in --> in_pivot
        cmds.parent(grp_list[0][0], parent_grp) # in_pivot --> parent_group
        cmds.parent(grp_list[0][2], grp_list[0][1]) # in_reset --> in 
        cmds.matchTransform(grp_list[0][0], loc_list[0]) # match in_pivot-->loc_in
        cmds.matchTransform(grp_list[0][2], parent_grp) # match in_reset-->parent 

        cmds.parent(grp_list[1][1], grp_list[1][0]) # out --> out_pivot
        cmds.parent(grp_list[1][0], grp_list[0][2]) # out_pivot --> in_reset
        cmds.parent(grp_list[1][2], grp_list[1][1]) # out_reset --> out 
        cmds.matchTransform(grp_list[1][0], loc_list[1]) # match out_pivot-->loc_out
        cmds.matchTransform(grp_list[1][2], parent_grp) # match out_reset-->parent 

        cmds.parent(ctrl, grp_list[1][2]) # ctrl --> out_reset


    def _create_group_roll(self, ctrl, loc_list, mirror):
        """Crea i gruppi roll IN e OUT e i rispettivi gruppi RESET e metti tutto in gerarchia."""
        if mirror == False:
            ctrl_name = "{}_Ctrl".format(ctrl)
            ctrl_name_static = "{}_Ctrl_Static".format(ctrl)
            grp_in_pivot_static, grp_out_pivot_static = self._make_groups_utility(ctrl_name)
            self._make_sliders_groups(ctrl, "rollIn", grp_in_pivot_static)
            self._make_sliders_groups(ctrl, "rollOut", grp_out_pivot_static)
            self._make_hierarchy_utility(ctrl_name, loc_list, self.groups)
            self._make_hierarchy_utility(ctrl_name_static, loc_list, self.groups_static)
            self._create_undo_set(ctrl, self.groups[0][0], self.groups[1][0], self.groups_static[0][0], self.groups_static[1][0])

        else:
            loc_list_mirrored = []
            
            for loc in loc_list:
                name_loc_R = loc.replace("L_", "R_")
                loc_R = cmds.spaceLocator(name=name_loc_R)
                loc_list_mirrored.append(loc_R)

                pos = cmds.xform(loc, q=True, ws=True, t=True)
                rot = cmds.xform(loc, q=True, ws=True, ro=True)

                cmds.xform(loc_R, ws=True, t=(-pos[0], pos[1], pos[2]))
                cmds.xform(loc_R, ws=True, ro=(rot[0], -rot[1], -rot[2]))

            for side, locators in zip(("L", "R"), (loc_list, loc_list_mirrored)):
                ctrl_name = "{}_{}_Ctrl".format(side, ctrl)
                ctrl_name_static = "{}_{}_Ctrl_Static".format(side, ctrl)
                grp_in_pivot_static, grp_out_pivot_static = self._make_groups_utility(ctrl_name)
                self._make_sliders_groups(ctrl, "rollIn", grp_in_pivot_static)
                self._make_sliders_groups(ctrl, "rollOut", grp_out_pivot_static)
                if side == "L":
                    self._make_hierarchy_utility(ctrl_name, locators, self.groups[0:2])
                    self._make_hierarchy_utility(ctrl_name_static, locators, self.groups_static[0:2])
                    self._create_undo_set(ctrl, self.groups[0][0], self.groups[1][0], self.groups_static[0][0], self.groups_static[1][0])
                else:
                    self._make_hierarchy_utility(ctrl_name, locators, self.groups[2:4])
                    self._make_hierarchy_utility(ctrl_name_static, locators, self.groups_static[2:4])
                    self._create_undo_set(ctrl, self.groups[2][0], self.groups[3][0], self.groups_static[2][0], self.groups_static[3][0])


    def _make_sliders(self, nameRig, nodi):
        """Rinomina i nodi passati come input con il suffisso 'SLIDER' e se non esiste crea il set slider corrispondente."""
        # Controlla se il set globale degli sliders esiste, altrimenti crealo
        if not cmds.objExists("SLIDERS"):
            sliders_set = cmds.sets(empty=True, name=("SLIDERS"))
        else:
            sliders_set = "SLIDERS"

        # crea sotto-set del singolo modulo
        if not cmds.objExists("{}_sliders".format(nameRig)):
            set_module_sliders = cmds.sets(empty=True, name=("{}_sliders".format(nameRig)))
            # Parenta il set del modulo dentro al set globale
            cmds.sets(set_module_sliders, edit=True, forceElement=sliders_set)
        else:
            set_module_sliders = "{}_sliders".format(nameRig)

        # Popola il set
        for nodo in nodi:
            cmds.sets(nodo, edit=True, forceElement=set_module_sliders)

        print("# Sliders rinominati e set creato. #")
    

    def _make_sliders_groups(self, ctrl, grp_type, gruppi):
        """Inserise gruppi rollin_pivot e rollout_pivot nel set slider corrispondente."""
        # Controlla se il set globale degli sliders esiste, altrimenti crealo
        if not cmds.objExists("SLIDERS"):
            sliders_set = cmds.sets(empty=True, name=("SLIDERS"))
        else:
            sliders_set = "SLIDERS"

        # Controlla se esiste set Mouth_Sliders
        if not cmds.objExists("SLIDERS_HeadMouth"):
            set_module_sliders = cmds.sets(empty=True, name=("SLIDERS_HeadMouth"))
            # Parenta il set del modulo dentro al set globale
            cmds.sets(set_module_sliders, edit=True, forceElement=sliders_set)
        else:
            set_module_sliders = "SLIDERS_HeadMouth"

        # Crea set controllo --> 'Uppermouth_rollinPivot_sliders'
        token = ctrl.split("_")[0]
        if not cmds.objExists(f"{token}_{grp_type}Pivot_groups"):
            set_ctrl_sliders = cmds.sets(empty=True, name=(f"{token}_{grp_type}Pivot_groups"))
            # Parenta il set del modulo dentro al set globale
            cmds.sets(set_ctrl_sliders, edit=True, forceElement=set_module_sliders)
        else:
            set_ctrl_sliders = f"{token}_{grp_type}Pivot_groups"

        # Popola il set
        cmds.sets(gruppi, edit=True, forceElement=set_ctrl_sliders)

        print("# Sliders rinominati e set creato. #")



    def _create_nodes(self, ctrl, static_grp, mirror, low, lip_ctrl):
        """Crea i nodi setRange e multiplyDivide tra lip_ctrl e gruppi 'roll_in' e 'roll_out.'"""
        sr_node_list = []

        mld_node = cmds.createNode('multiplyDivide', n="MLD_{}_Ctrl_LipRoll".format(ctrl))
        sr_node_in = cmds.createNode('setRange', n="SR_{}_Ctrl_LipRoll_SLIDER_RollIn".format(ctrl))
        sr_node_out = cmds.createNode('setRange', n="SR_{}_Ctrl_LipRoll_SLIDER_RollOut".format(ctrl))
        sr_node_list.append(sr_node_in)
        sr_node_list.append(sr_node_out)
        
        # imposta valori setRange
        cmds.setAttr("{}.minX".format(sr_node_in), -30)
        cmds.setAttr("{}.oldMinX".format(sr_node_in), -0.4)

        cmds.setAttr("{}.maxX".format(sr_node_out), 20)
        cmds.setAttr("{}.oldMaxX".format(sr_node_out), 0.25)


        # crea connessioni
        if low:
            cmds.setAttr("{}.input2X".format(mld_node), 1)
            cmds.setAttr("{}.input2Y".format(mld_node), 1)
        else:
            cmds.setAttr("{}.input2X".format(mld_node), -1)
            cmds.setAttr("{}.input2Y".format(mld_node), -1)

        cmds.connectAttr("{}.translateY".format(lip_ctrl), "{}.valueX".format(sr_node_in), force=True)
        cmds.connectAttr("{}.translateY".format(lip_ctrl), "{}.valueX".format(sr_node_out), force=True)

        cmds.connectAttr("{}.outValueX".format(sr_node_in), "{}.input1X".format(mld_node), force=True)
        cmds.connectAttr("{}.outValueX".format(sr_node_out), "{}.input1Y".format(mld_node), force=True)

        cmds.connectAttr("{}.outputX".format(mld_node), "{}.rotateX".format(static_grp[0][1]), force=True)
        cmds.connectAttr("{}.outputY".format(mld_node), "{}.rotateX".format(static_grp[1][1]), force=True)

        if mirror:
            cmds.connectAttr("{}.outputX".format(mld_node), "{}.rotateX".format(static_grp[2][1]), force=True)
            cmds.connectAttr("{}.outputY".format(mld_node), "{}.rotateX".format(static_grp[3][1]), force=True)

        self._make_sliders("Mouth", sr_node_list)
        self._create_undo_set(ctrl, sr_node_list)


    def _connect_driver_to_static(self, driver_grp, static_grp, mirror):
        """Crea connessioni dirette tra i gruppi driver e static dei gruppi ROLLIN e ROLLOUT. """
        lista_gruppi = ('ROLLIN', 'ROLLOUT', 'ROLLINPIVOT', 'ROLLOUTPIVOT', 'ROLLINRESET', 'ROLLOUTRESET')
        for static_list, driver_list in zip(static_grp, driver_grp):
            for static, driver in zip(static_list, driver_list):
                print("static: {}".format(static))
                print("driver: {}".format(driver))
                if cmds.nodeType(driver) == 'transform':
                    name_tokens = driver.split("_")
                    for grp in lista_gruppi:
                        if grp in name_tokens:
                            driver_attrs = cmds.listAttr(static, keyable=True) or []
                            for attr in driver_attrs:
                                if cmds.attributeQuery(attr, node=driver, exists=True) and cmds.getAttr("{}.{}".format(static, attr), keyable=True):
                                    cmds.connectAttr("{}.{}".format(static, attr), "{}.{}".format(driver, attr), force=True)

        print("# Connessioni dirette completate tra driver e static. #")


    # -----------------------------------------------------------------
    # Funzioni da associare ai singoli pulsanti della UI
    # -----------------------------------------------------------------
    def create_locators_push_button(self, ctrl, mirror, low, *args):
        """Crea i locator per posizionare il pivot dei gruppi lip roll IN e OUT."""
        if mirror == False:
            name_loc_in = "{}_roll_IN_loc".format(ctrl)
            name_loc_out = "{}_roll_OUT_loc".format(ctrl)
            pos_t = cmds.xform("{}_Ctrl".format(ctrl), q=True, ws=True, t=True)
            pos_r = cmds.xform("{}_Ctrl".format(ctrl), q=True, ws=True, ro=True)
        else:
            name_loc_in = "L_{}_roll_IN_loc".format(ctrl)
            name_loc_out = "L_{}_roll_OUT_loc".format(ctrl)
            pos_t = cmds.xform("L_{}_Ctrl".format(ctrl), q=True, ws=True, t=True)
            pos_r = cmds.xform("L_{}_Ctrl".format(ctrl), q=True, ws=True, ro=True)
        
        loc_roll_in = cmds.spaceLocator(name=name_loc_in)
        self.locators.append(loc_roll_in[0])
        loc_roll_out = cmds.spaceLocator(name=name_loc_out)
        self.locators.append(loc_roll_out[0])

        for attr in ['X', 'Y', 'Z']:
            cmds.setAttr("{}.localScale{}".format(loc_roll_in[0], attr), 0.15)
            cmds.setAttr("{}.localScale{}".format(loc_roll_out[0], attr), 0.15)

        cmds.setAttr("{}.overrideEnabled".format(loc_roll_in[0]), 1)
        cmds.setAttr("{}.overrideColor".format(loc_roll_in[0]), 18)
        cmds.setAttr("{}.overrideEnabled".format(loc_roll_out[0]), 1)
        cmds.setAttr("{}.overrideColor".format(loc_roll_out[0]), 16)
        
        cmds.setAttr(loc_roll_in[0] + '.translate', pos_t[0], pos_t[1], pos_t[2])
        cmds.setAttr(loc_roll_in[0] + '.rotate', pos_r[0], pos_r[1], pos_r[2])
        cmds.setAttr(loc_roll_out[0] + '.translate', pos_t[0], pos_t[1], pos_t[2])
        cmds.setAttr(loc_roll_out[0] + '.rotate', pos_r[0], pos_r[1], pos_r[2])

        if low == False:
            cmds.move(0, 0, 0.2, loc_roll_in, r=True, os=True, wd=True)
            cmds.move(0, 0, -0.2, loc_roll_out, r=True, os=True, wd=True)
        else:
            cmds.move(0, 0, -0.2, loc_roll_in, r=True, os=True, wd=True)
            cmds.move(0, 0, 0.2, loc_roll_out, r=True, os=True, wd=True)

        cmds.select(clear=True)


    def finalize_push_button(self, ctrl, mirror, low, *args):
        """Crea e finalizza il sistema lip roll."""
        if not low:
            lip_ctrl = self.up_lip_ctrl
        else:
            lip_ctrl = self.low_lip_ctrl

        self._create_group_roll(ctrl, self.locators, mirror)
        self._delete_locators(ctrl, mirror)
        self._create_nodes(ctrl, self.groups_static, mirror, low, lip_ctrl)
        self._connect_driver_to_static(self.groups, self.groups_static, mirror)
        
        self.locators[:] = []
        self.groups[:] = []
        self.groups_static[:] = []
        cmds.select(clear=True)

        # End message
        print("### Lip roll for {} have been successfully rigged. ###".format(ctrl))


    def undo_push_button(self, ctrl, mirror, *args):
        """Cancella nodi e gruppi del sistema lip roll."""
        if mirror == False:
            if cmds.objExists("{}_REMOVE_SET".format(ctrl)):
                # per prima cosa rimetti il controllo in gerarchia
                grp_parent = cmds.listRelatives("{}_Ctrl_ROLLINPIVOT_Grp".format(ctrl), parent=True)[0]
                cmds.parent("{}_Ctrl".format(ctrl), grp_parent)

                # per prima cosa rimetti il controllo Static in gerarchia
                grp_static_parent = cmds.listRelatives("{}_Ctrl_ROLLINPIVOT_Grp_Static".format(ctrl), parent=True)[0]
                cmds.parent("{}_Ctrl_Static".format(ctrl), grp_static_parent)

                # seleziona gli elementi nel set e cancellali
                cmds.select("{}_REMOVE_SET".format(ctrl))
                set_element = cmds.ls(sl=True)
                cmds.delete(set_element)
            else:
                cmds.warning("There is no '{}' lip roll system to remove.".format(ctrl))
        else:
            if cmds.objExists("{}_REMOVE_SET".format(ctrl)):
                # per prima cosa rimetti i controlli in gerarchia
                for side in ("L", "R"):
                    grp_parent = cmds.listRelatives("{}_{}_Ctrl_ROLLINPIVOT_Grp".format(side, ctrl), parent=True)[0]
                    cmds.parent("{}_{}_Ctrl".format(side, ctrl), grp_parent)

                    # per prima cosa rimetti il controllo Static in gerarchia
                    grp_static_parent = cmds.listRelatives("{}_{}_Ctrl_ROLLINPIVOT_Grp_Static".format(side, ctrl), parent=True)[0]
                    cmds.parent("{}_{}_Ctrl_Static".format(side, ctrl), grp_static_parent)

                # seleziona gli elementi nel set e cancellali
                cmds.select("{}_REMOVE_SET".format(ctrl))
                set_element = cmds.ls(sl=True)
                cmds.delete(set_element)
            else:
                cmds.warning("There is no '{}' lip roll system to remove.".format(ctrl))

        cmds.select(clear=True)

        # End message
        print("### Lip roll for {} have been successfully removed. ###".format(ctrl))


    # -----------------------------------------------------------------
    # UI
    # -----------------------------------------------------------------
    def UI(self):
        """Funzione che lancia la UI dei moduli del facciale."""
        
        main_window = "RBW_LipRoll_Module"
        iconpath = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'icons'))

        if cmds.window(main_window, q=True, exists=True):
            cmds.deleteUI(main_window, window=True)
        
        cmds.window(main_window, title=main_window, width=250, height=50)
        
        # Main Layout
        col = cmds.columnLayout(adjustableColumn=True, bgc=(0.225, 0.225, 0.225))
        
        # Face Base
        cmds.columnLayout(adj=True, rowSpacing=5, columnWidth=250)
        cmds.text(l="", h=3)
        
        row_1 = cmds.rowLayout(numberOfColumns=4, adjustableColumn=3, columnAttach4=['both', 'both', 'both', 'both'], columnOffset4=[5,10,5,10])
        img = os.path.join(self.image_path, "mouth_01.png")
        cmds.iconTextButton(label="Upper_Center", style="iconAndTextVertical", image1=img, p=row_1, w=80)
        cmds.button(l="Locators", command=partial(self.create_locators_push_button, "UpperMouth_Center", False, False), h=30, bgc=[0.7, 0.7, 0.7], w=60)
        cmds.button(l="FINALIZE", command=partial(self.finalize_push_button, "UpperMouth_Center", False, False), h=30, bgc=[0.3,0.3,1.0], w=100)
        cmds.button(l="Undo", command=partial(self.undo_push_button, "UpperMouth_Center", False), h=30, bgc=[0.3, 0.3, 0.3], w=40)
        cmds.setParent("..")
        
        row_2 = cmds.rowLayout(numberOfColumns=4, adjustableColumn=3, columnAttach4=['both', 'both', 'both', 'both'], columnOffset4=[5,10,5,10])
        img = os.path.join(self.image_path, "mouth_02.png")
        cmds.iconTextButton(label="Upper_01", style="iconAndTextVertical", image1=img, p=row_2, w=80)
        cmds.button(l="Locators", command=partial(self.create_locators_push_button, "UpperMouth_01", True, False), h=30, bgc=[0.7, 0.7, 0.7], w=60)
        cmds.button(l="FINALIZE", command=partial(self.finalize_push_button, "UpperMouth_01", True, False), h=30, bgc=[0.3,0.3,1.0], w=100)
        cmds.button(l="Undo", command=partial(self.undo_push_button, "UpperMouth_01", True), h=30, bgc=[0.3, 0.3, 0.3], w=40)
        cmds.setParent("..")

        row_3 = cmds.rowLayout(numberOfColumns=4, adjustableColumn=3, columnAttach4=['both', 'both', 'both', 'both'], columnOffset4=[5,10,5,10])
        img = os.path.join(self.image_path, "mouth_03.png")
        cmds.iconTextButton(label="Upper_02", style="iconAndTextVertical", image1=img, p=row_3, w=80)
        cmds.button(l="Locators", command=partial(self.create_locators_push_button, "UpperMouth_02", True, False), h=30, bgc=[0.7, 0.7, 0.7], w=60)
        cmds.button(l="FINALIZE", command=partial(self.finalize_push_button, "UpperMouth_02", True, False), h=30, bgc=[0.3,0.3,1.0], w=100)
        cmds.button(l="Undo", command=partial(self.undo_push_button, "UpperMouth_02", True), h=30, bgc=[0.3, 0.3, 0.3], w=40)
        cmds.setParent("..")
        cmds.separator(h = 10, style="in")

        row_4 = cmds.rowLayout(numberOfColumns=4, adjustableColumn=3, columnAttach4=['both', 'both', 'both', 'both'], columnOffset4=[5,10,5,10])
        img = os.path.join(self.image_path, "mouth_04.png")
        cmds.iconTextButton(label="Lower_Center", style="iconAndTextVertical", image1=img, p=row_4, w=80)
        cmds.button(l="Locators", command=partial(self.create_locators_push_button, "LowerMouth_Center", False, True), h=30, bgc=[0.7, 0.7, 0.7], w=60)
        cmds.button(l="FINALIZE", command=partial(self.finalize_push_button, "LowerMouth_Center", False, True), h=30, bgc=[0.3,0.3,1.0], w=100)
        cmds.button(l="Undo", command=partial(self.undo_push_button, "LowerMouth_Center", False), h=30, bgc=[0.3, 0.3, 0.3], w=40)
        cmds.setParent("..")
        
        row_5 = cmds.rowLayout(numberOfColumns=4, adjustableColumn=3, columnAttach4=['both', 'both', 'both', 'both'], columnOffset4=[5,10,5,10])
        img = os.path.join(self.image_path, "mouth_05.png")
        cmds.iconTextButton(label="Lower_01", style="iconAndTextVertical", image1=img, p=row_5, w=80)
        cmds.button(l="Locators", command=partial(self.create_locators_push_button, "LowerMouth_01", True, True), h=30, bgc=[0.7, 0.7, 0.7], w=60)
        cmds.button(l="FINALIZE", command=partial(self.finalize_push_button, "LowerMouth_01", True, True), h=30, bgc=[0.3,0.3,1.0], w=100)
        cmds.button(l="Undo", command=partial(self.undo_push_button, "LowerMouth_01", True), h=30, bgc=[0.3, 0.3, 0.3], w=40)
        cmds.setParent("..")

        row_6 = cmds.rowLayout(numberOfColumns=4, adjustableColumn=3, columnAttach4=['both', 'both', 'both', 'both'], columnOffset4=[5,10,5,10])
        img = os.path.join(self.image_path, "mouth_06.png")
        cmds.iconTextButton(label="Lower_02", style="iconAndTextVertical", image1=img, p=row_6, w=80)
        cmds.button(l="Locators", command=partial(self.create_locators_push_button, "LowerMouth_02", True, True), h=30, bgc=[0.7, 0.7, 0.7], w=60)
        cmds.button(l="FINALIZE", command=partial(self.finalize_push_button, "LowerMouth_02", True, True), h=30, bgc=[0.3,0.3,1.0], w=100)
        cmds.button(l="Undo", command=partial(self.undo_push_button, "LowerMouth_02", True), h=30, bgc=[0.3, 0.3, 0.3], w=40)
        cmds.setParent("..")


        cmds.text(l="", h=3)     
        cmds.setParent("..")

        cmds.showWindow()
