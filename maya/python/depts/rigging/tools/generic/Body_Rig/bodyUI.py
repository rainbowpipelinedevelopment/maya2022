# -*- coding: utf-8 -*-

import maya.cmds as cmds
from maya import OpenMayaUI as omui
import os
from imp import reload

# -----------------------------------------------------------------
# Path
# -----------------------------------------------------------------
localpipe = os.getenv("LOCALPY")
myPath = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "Body_Rig")
image_path = os.path.join(myPath, "icons")


# -----------------------------------------------------------------
# PySide
# -----------------------------------------------------------------
try:
    from shiboken2 import wrapInstance
except ImportError:
    from shiboken import wrapInstance

try:
    from PySide2.QtGui import QIcon
    from PySide2.QtWidgets import QWidget
except ImportError:
    from PySide.QtGui import QIcon, QWidget


# -----------------------------------------------------------------
# Funzioni lanciate dalla UI
# -----------------------------------------------------------------
def breast_and_breath_button_push(*args):
    import depts.rigging.tools.generic.Body_Rig.scripts.breast_and_breath_rig_UI as bb_rig_UI
    reload(bb_rig_UI)
    breathRig = bb_rig_UI.BreathAndBreastRig()
    breathRig.main()


def nurbs_ribbon_button_push(*args):
    import depts.rigging.tools.generic.Body_Rig.scripts.nurbs_ribbon_UI as nurbs_ribbon_UI
    reload(nurbs_ribbon_UI)
    nurbsRibbonRig = nurbs_ribbon_UI.NurbsRibbon()
    nurbsRibbonRig.main()


def rig_reverse_base_button_push(*args):
    import depts.rigging.tools.generic.Body_Rig.scripts.rig_reverse_base as rig_reverse_base
    reload(rig_reverse_base)
    rig_reverse_base.main()


def attach_head_button_push(*args):
    import depts.rigging.tools.generic.Body_Rig.scripts.attach_head as attach_head
    reload(attach_head)
    attach_head.main()

def fix_eyes_button_push(*args):
    import depts.rigging.tools.generic.Body_Rig.scripts.fix_eyes as fix_eyes
    reload(fix_eyes)
    fix_eyes.fix_eyes()
    

# UTILITIES
def getinfoskincluster_button_push(*args):
    import depts.rigging.tools.generic.Body_Rig.scripts.get_info_skincluster as get_info_skincluster
    reload(get_info_skincluster)
    get_info_skincluster.influenceJnts()

def nurbs_copyskinweights_button_push(*args):
    import depts.rigging.tools.generic.Body_Rig.scripts.nurbs_copyskinweights as nurbs_copyskin
    reload(nurbs_copyskin)
    nurbs_copyskin.copySkinWeights_nurbs()

def bind_prematrix_connector_button_push(*args):
    import depts.rigging.tools.generic.Body_Rig.scripts.bind_prematrix_connector as bind_prematrix_connector
    reload(bind_prematrix_connector)
    bind_prematrix_connector.bind_prematrix_connector()

def link_wiki_button_push(*args):
    import webbrowser
    pathToHelp="https://sites.google.com/rbw-cgi.it/rbw-rigging-team/home"
    webbrowser.open(pathToHelp) 

# -----------------------------------------------------------------
# UI
# -----------------------------------------------------------------
def main():
    winWidth = 280
    bg_color_01 = (0.35, 0.35, 0.35)
    bg_color_02 = (0.6, 0.5, 0.2)
    bg_color_03 = (0.4, 0.3, 0.2)
    
    main_window = "RBW_Body_Module"
    main_title = "RBW Body Module"

    if cmds.window(main_window, exists = 1):
        cmds.deleteUI(main_window, window = 1)

    ww = cmds.window(main_window, title=main_title, mxb=False, sizeable=True, resizeToFitChildren=True, width=250, height=50)

    # Get a pointer and convert it to Qt Widget object
    qw = omui.MQtUtil.findWindow(ww)
    widget = wrapInstance(int(qw), QWidget)

    # Create a QIcon object
    iconpath = os.path.join(image_path, "RainbowCGI_icona.ico")

    # Assign the icon
    icon = QIcon(iconpath)
    widget.setWindowIcon(icon)

    # Main layout
    col = cmds.columnLayout(co=("both", 5), adjustableColumn=True, bgc=(0.225, 0.225, 0.225))
    
    # Carica l'immagine degli eyesockets
    img = os.path.join(image_path, "body.png")
    cmds.text(h = 10, l = "")
    cmds.iconTextButton(style="iconOnly", image1=img, p=col, w=100)
    cmds.text(h = 10, l = "")
    cmds.setParent("..")

    # FIELDS
    cmds.frameLayout( label="Body Modules", marginHeight=10, p=col, bgc=bg_color_02)
    cmds.columnLayout(adj=True, co=("both", 6), rowSpacing=5, columnWidth=250)
    cmds.button(w=winWidth, h=30, l="| Breast e Breath Module |", c=breast_and_breath_button_push, bgc=bg_color_01, annotation="Crea il sistema per il Breast e Breath Rig")
    cmds.button(w=winWidth, h=30, l="| Nurbs Ribbon Module |", c=nurbs_ribbon_button_push, bgc=bg_color_01, annotation="Crea una ribbon usando una nurbs e gli uvpin")
    cmds.button(w=winWidth, h=30, l="| Rig Reverse Base |", c=rig_reverse_base_button_push, bgc=bg_color_01, annotation="Tool per ritornare alle guide di 'Hive'")
    cmds.button(w=winWidth, h=30, l="| Attach Head |", c=attach_head_button_push, bgc=bg_color_01, annotation="Automatiza l'attacco del rig della faccia al rig del body")
    cmds.button(w=winWidth, h=30, l="| Fix Eyes |", c=fix_eyes_button_push, bgc=bg_color_01, annotation="Automatiza i fix del 'follow', 'aim' e 'scale' degli occhi")
    cmds.setParent("..")
    # cmds.setParent("..")

    # Utilities
    cmds.frameLayout(label="Utilities", collapsable=True, collapse=True, p=col, bgc=bg_color_03)
    cmds.columnLayout(adj=True, co=("both", 6), rowSpacing=7, columnWidth=250)
    cmds.text(l="", h=1)     
    cmds.button(w=winWidth, h=25, l="| Get Info SkinWeights |", c=getinfoskincluster_button_push, bgc=(0.7, 0.7, 0.53), annotation="Seleziona i joint che influenzano l'oggetto selezionato. Selezionare prima l'oggetto con lo skinCluster e lanciare lo script")
    cmds.button(w=winWidth, h=25, l="| Nurbs CopySkinWeights |", c=nurbs_copyskinweights_button_push, bgc=(0.7, 0.7, 0.53), annotation="CopySkinWieights ottimizzato per le nurbs. Selezionare prima la Mesh, poi le Nurbs e lanciare lo script")
    cmds.button(w=winWidth, h=25, l="| Bind PreMatrix Connector |", c=bind_prematrix_connector_button_push, bgc=(0.7, 0.7, 0.53), annotation="Tool per il BindPreMatrix. Selezionare l'oggetto che va in doppia (mesh, nurbs, curve) e lanciare lo script")
    cmds.text(l="", h=1)     
    cmds.setParent("..")

    # WIKI
    cmds.frameLayout(label="WIKI", labelVisible=False, p=col, bgc=(0.4, 0.4, 0.4))
    col_wiki = cmds.columnLayout(adj=True, co=("both", 6), rowSpacing=7, columnWidth=250)
    cmds.text(l="", h=1)     
    cmds.button(w=winWidth, h=30, l="| WIKI |", p=col_wiki, c=link_wiki_button_push, bgc=(0.2, 0.2, 0.2), annotation="Hai qualche dubbio? Vai alla Wiki di rigging!")
    cmds.text(l="", h=1)     
    cmds.setParent("..")
    
    cmds.showWindow(main_window)
