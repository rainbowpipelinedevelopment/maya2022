import pymel.core as pm

def copySkinWeights_nurbs(*args):
    """ 
    Select SOURCE first, and TARGETS last. 
    http://forum.mgear-framework.com/t/copy-skin-weights-from-poly-to-nurbs-mgear-4-0-9-maya-2022/3342/5 
    """
    if not pm.selected():
        cmds.error("Seleziona prima la surface nurbs, poi la mesh.")
    elif len(pm.selected()) == 1:
        cmds.error("Seleziona prima la surface nurbs, poi la mesh.")


    source = pm.selected()[0]
    targets = pm.selected()[1:]

    for target in targets:
        shapeType = pm.nodeType(target.getShape())
        if shapeType in ["nurbsSurface", "nurbsCurve"]:
            pm.copySkinWeights(source, target.cv, noMirror=True, surfaceAssociation='closestPoint', ia=['oneToOne','name'])
        elif shapeType  == "mesh":
            pm.copySkinWeights(source, target, noMirror=True, surfaceAssociation='closestPoint', ia=['oneToOne','name'])
        else:
            pass

    pm.select(clear=True)

    print("### CopySkinWeights tra '{}' e '{}' fatto! ###".format(source, targets))
    

# copySkinWeights_nurbs()