# -*- coding: utf-8 -*-

import maya.cmds as cmds
from maya import OpenMayaUI as omui
import os


# -----------------------------------------------------------------
# PySide
# -----------------------------------------------------------------
try:
    from shiboken2 import wrapInstance
except ImportError:
    from shiboken import wrapInstance

try:
    from PySide2.QtGui import QIcon
    from PySide2.QtWidgets import QWidget
except ImportError:
    from PySide.QtGui import QIcon, QWidget


class BreathAndBreastRig:
    """ Crea il sistema breath e il breast rig. """
    def __init__(self):
        self.locator_list = []
        self.tmp_nodes = []

        # -----------------------------------------------------------------
        # Path
        # -----------------------------------------------------------------
        localpipe = os.getenv("LOCALPY")
        myPath = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "Body_Rig")
        self.image_path = os.path.join(myPath, "icons")



    def create_locators(self, *args):
        locator_names = ('L_breast_TMP_locator', 'R_breast_TMP_locator')
        locator_list = []

        for idx, loc in enumerate(locator_names):
            if cmds.objExists(loc):
                cmds.error(f"### Attenzione!!! Esiste già un locator chiamato '{loc}'! ###")
            else:
                loc_breast = cmds.spaceLocator(name=loc)[0]
                locator_list.append(loc_breast)
                cmds.setAttr(f"{loc_breast}Shape.overrideEnabled", 1)
                cmds.setAttr(f"{loc_breast}Shape.overrideColor", 22)

                if idx == 0:
                    for attr, value in zip(('X', 'Y', 'Z'), (7, 130, 14)):
                       cmds.setAttr(f"{locator_list[0]}.translate{attr}", value)
                else:        
                    for attr, value in zip(('X', 'Y', 'Z'), (-7, 130, 14)):
                       cmds.setAttr(f"{locator_list[1]}.translate{attr}", value)

        # crea nodi multiply e crea connessioni per mirrorare locator di destra
        md_node_translate = cmds.createNode("multiplyDivide", n=f"MD_{locator_list[1]}_mirror_translate")
        md_node_rotate = cmds.createNode("multiplyDivide", n=f"MD_{locator_list[1]}_mirror_rotate")
        self.tmp_nodes.append(md_node_translate)
        self.tmp_nodes.append(md_node_rotate)
        
        cmds.setAttr(f"{md_node_translate}.input2X", -1)
        cmds.setAttr(f"{md_node_rotate}.input2Y", -1)
        cmds.setAttr(f"{md_node_rotate}.input2Z", -1)

        cmds.connectAttr(f"{locator_list[0]}.translate", f"{md_node_translate}.input1", f=True)
        cmds.connectAttr(f"{md_node_translate}.output", f"{locator_list[1]}.translate", f=True)
        
        cmds.connectAttr(f"{locator_list[0]}.rotate", f"{md_node_rotate}.input1", f=True)
        cmds.connectAttr(f"{md_node_rotate}.output", f"{locator_list[1]}.rotate", f=True)

        cmds.select(clear=True)
        self.locator_list = locator_list


    def _create_joints_breath(self):
        breath_list = ('spine_04_jnt', 'spine_05_jnt')
        breath_name_joints = ('Breath_01_jnt', 'Breath_02_jnt')
        jnt_list = []

        for item, name in zip(breath_list, breath_name_joints):
            cmds.select(clear=True)
            jnt = cmds.joint(name=name)
            cmds.matchTransform(jnt, item)
            cmds.makeIdentity(jnt, apply=True, r=True)
            cmds.parent(jnt, item)
            jnt_list.append(jnt)

        cmds.select(clear=True)

        return jnt_list
   
    
    def _create_joints_breast(self, locators):
        breast_name_joints = ('L_Breast_jnt', 'R_Breast_jnt')
        jnt_list = []

        for loc, name in zip(locators, breast_name_joints):
            piv = cmds.xform(loc, q=True, t=True, ws=True)
            print(f"loc: {loc} - piv: {piv[0:3]}")
            cmds.select(clear=True)
            jnt = cmds.joint(name=name, p=piv[0:3])
            cmds.parent(jnt, 'Breath_01_jnt')
            jnt_list.append(jnt)

        cmds.select(clear=True)
        cmds.setAttr(f"{jnt_list[1]}.rotateY", -180)
        cmds.makeIdentity(jnt_list[1], apply=True, t=False, r=True, s=False)

        for loc in locators:
            if cmds.objExists(loc):
                cmds.delete(loc)
            else:
                pass

        for node in self.tmp_nodes:
            if cmds.objExists(node):
                cmds.delete(node)
            else:
                pass

        return jnt_list


    def _make_orient_constraint(self, breath_jnts):
        cmds.orientConstraint(breath_jnts[1], breath_jnts[0], offset=[0,0,0], skip=["x", "z"], weight=1)
        cmds.select(clear=True)


    def _create_breath_attribute(self):
        if cmds.objExists('spineHigh_Ctrl'):
            cmds.select('spineHigh_Ctrl', replace=True)
            cmds.addAttr(ln="breath", at="float", min=0, max=10, dv=0, k=True)
            cmds.select(clear=True)
        else:
            cmds.error("### Il controllo 'spineHigh_Ctrl' non esiste. Impossibile aggiungere l'attributo 'breath'. ###")

    
    def _create_breath_groups(self):
        controls = ('clavicle_L_Ctrl', 'clavicle_R_Ctrl', 'spine_tweaker02_Ctrl')
        ctrl_grps = ('clavicle_L_00_Ctrl_space', 'clavicle_R_00_Ctrl_space', 'spine_tweaker02_Ctrl_srt')
        breath_grp_list = []

        for ctrl, ctrl_grp in zip(controls, ctrl_grps):
            cmds.select(clear=True)
            grp_tmp = cmds.group(name=f"{ctrl}_Breath_Grp", world=True, empty=True)
            breath_grp_list.append(grp_tmp)
            cmds.matchTransform(grp_tmp, ctrl_grp)
            cmds.parent(grp_tmp, ctrl_grp)
            cmds.parent(ctrl, grp_tmp)
            cmds.select(clear=True)

        return breath_grp_list


    def _create_nodes(self):
        name_list = ('Breath_Clavicles', 'Breath')
        sr_node_list = []
        md_node_list = []

        for idx, name in enumerate(name_list):
            # nodi setRange
            sr_node = cmds.createNode('setRange', name=f"SR_{name}")
            sr_node_list.append(sr_node)

            if idx == 0:
                valori_sr_max = (-1, 0.25, 2.5)
                valori_sr_min = (0, 0, 0)
                valori_sr_oldmax = (10, 10, 10)
            else:
                valori_sr_max = (1.025, 1, 1.013)
                valori_sr_min = (1, 1, 1)
                valori_sr_oldmax = (10, 10, 10)

            for attr, value_max, value_min, value_oldMax in zip(('X', 'Y', 'Z'), valori_sr_max, valori_sr_min, valori_sr_oldmax):
                cmds.setAttr(f"{sr_node}.max{attr}", value_max)
                cmds.setAttr(f"{sr_node}.min{attr}", value_min)
                cmds.setAttr(f"{sr_node}.oldMax{attr}", value_oldMax)
            
            # nodi multiplyDivide
            if idx == 0:
                md_node = cmds.createNode('multiplyDivide', name=f"MD_{name}")
                md_node_list.append(md_node)
                for attr in ('X', 'Y'):
                    cmds.setAttr(f"{md_node}.input2{attr}", -1)

        return sr_node_list, md_node_list
        

    def _make_connections(self, sr_node_list, md_node_list, breath_grp_list, breath_jnt_list):
        # connessioni da attributo 'breath' del controllo 'spineHigh_Ctrl' a nodi setRange
        for sr_node in sr_node_list:
            for attr in ('X', 'Y', 'Z'):
                cmds.connectAttr('spineHigh_Ctrl.breath', f"{sr_node}.value{attr}", force=True)

        # connessioni da nodi setRange a nodi multiplyDivide
        for sr_node, md_node in zip(sr_node_list, md_node_list):
            cmds.connectAttr(f"{sr_node}.outValue", f"{md_node}.input1", force=True)

        # connessioni ai gruppi dei controlli
        cmds.connectAttr(f"{sr_node_list[0]}.outValueX", f"{breath_grp_list[2]}.translateZ", force=True)
        cmds.connectAttr(f"{sr_node_list[0]}.outValueY", f"{breath_grp_list[0]}.translateY", force=True)
        cmds.connectAttr(f"{sr_node_list[0]}.outValueZ", f"{breath_grp_list[0]}.rotateY", force=True)
        cmds.connectAttr(f"{md_node_list[0]}.outputY", f"{breath_grp_list[1]}.translateY", force=True)
        cmds.connectAttr(f"{md_node_list[0]}.outputZ", f"{breath_grp_list[1]}.rotateY", force=True)

        # connessioni ai joints del breath
        cmds.connectAttr(f"{sr_node_list[1]}.outValue", f"{breath_jnt_list[0]}.scale", force=True)
        cmds.connectAttr(f"{sr_node_list[1]}.outValue", f"{breath_jnt_list[1]}.scale", force=True)


    def _utility_create_set(self, name_rig, type_set, obj_list):
        """Utility per creare un set."""

        name_rig_set = "{}_SET".format(name_rig)
        name_set = "{}_{}_SET".format(name_rig, type_set)
        
        if not cmds.objExists(name_rig_set):
            module_set = cmds.sets(empty=True, name=(name_rig_set))
        else:
            module_set = name_rig_set

        if not cmds.objExists(name_set):
            objects_set = cmds.sets(empty=True, name=name_set)
            # Parenta il set del modulo dentro al set globale
            cmds.sets(objects_set, edit=True, forceElement=module_set)
        else:
            objects_set = name_set

        for obj in obj_list:
            cmds.sets(obj, edit=True, forceElement=objects_set)


    def _create_undo_set(self, name_set, *args):
        for item in args:
            if cmds.objExists(name_set):
                cmds.sets(item, e=True, add=name_set)
            else:
                ctrl_set = cmds.sets(item, name=name_set)
                cmds.setAttr("{}.hiddenInOutliner".format(ctrl_set), True)


    def undo_rig(self, *args):
        """Cancella nodi e gruppi del rig."""
        name_set = 'BREAST_AND_BREATH_REMOVE_SET'

        if cmds.objExists(name_set):
            # spegni attributo 'breath'
            cmds.setAttr("spineHigh_Ctrl.breath", 0)

            # rimuovi attributo 'breath' da 'spineHigh_Ctrl'
            cmds.deleteAttr("spineHigh_Ctrl", attribute="breath")

            # sparenta controlli clavicle e 'spine_tweaker02_Ctrl' da gruppi breath
            cmds.parent('clavicle_L_Ctrl', 'clavicle_L_00_Ctrl_space')
            cmds.parent('clavicle_R_Ctrl', 'clavicle_R_00_Ctrl_space')
            cmds.parent('spine_tweaker02_Ctrl', 'spine_tweaker02_Ctrl_srt')

            # cancella jnt, grp e nodi 
            cmds.select(name_set)
            set_element = cmds.ls(sl=True)
            cmds.delete(set_element)

            print("### 'Breast and Breath Rig' have been successfully removed. ###")

        else:
            print("### Non è stato trovato nessun 'Breast and Breath Rig'! ###")


    def build_rig(self, *args):
        breath_jnt_list = self._create_joints_breath()
        breast_jnt_list = self._create_joints_breast(self.locator_list)
        self._make_orient_constraint(breath_jnt_list)
        self._create_breath_attribute()
        breath_grp_list = self._create_breath_groups()
        sr_node_list, md_node_list = self._create_nodes()
        self._make_connections(sr_node_list, md_node_list, breath_grp_list, breath_jnt_list)
        self._utility_create_set("Breast_and_Breath", "joints", [breath_jnt_list+breast_jnt_list])
        self._create_undo_set('BREAST_AND_BREATH_REMOVE_SET', breath_jnt_list, breast_jnt_list, breath_grp_list, sr_node_list, md_node_list)

        #reset variable
        self.locator_list[:] = []
        self.tmp_nodes[:] = []

        print("### 'Breast & Breath Rig' have been successfully rigged! ###")


    # ------------------------------------------------------------------------
    # UI
    # ------------------------------------------------------------------------
    def main(self):
        # Main window
        winWidth = 290
        color_label_01 = (0.5, 0.45, 0.15)
        color_label_02 = (.22, 0.22, 0.22)
        color_label_03 = (.15, 0.15, 0.15)
        
        mainWin = "RBW_Breath_and_Breast_Tool"

        if cmds.window(mainWin, exists = 1):
            cmds.deleteUI(mainWin, window = 1)

        ww = cmds.window(mainWin, title = "RBW Breath & Breast Tool", mxb=0, sizeable=True, resizeToFitChildren=True)

        # Get a pointer and convert it to Qt Widget object
        qw = omui.MQtUtil.findWindow(ww)
        widget = wrapInstance(int(qw), QWidget)

        # Create a QIcon object
        iconpath = os.path.join(self.image_path, "RainbowCGI_icona.ico")

        # Assign the icon
        icon = QIcon(iconpath)
        widget.setWindowIcon(icon)

        # Main layout
        col = cmds.columnLayout(co=("both", 5), adjustableColumn=True)

        cmds.frameLayout( label="Breast & Breath Rig", marginHeight=10, bgc=color_label_01)
        # Crea locators
        cmds.button("btn_create_locators", w=winWidth, h=50, l="Crea i locators per posizionare il 'Breast Rig'", c=self.create_locators, bgc=color_label_02)
        # Build rig
        cmds.button("btn_build_rig", w=winWidth, h=50, l="Crea il 'Breast e Breath Rig'", c=self.build_rig)
        #Undo Rig
        cmds.button("btn_undo_rig", w=winWidth, h=50, l="Rimuovi il 'Breast e Breath Rig'", c=self.undo_rig, bgc=color_label_02)
        cmds.setParent("..")

        cmds.showWindow(mainWin)


# BreathAndBreastRig().main()