#-*- coding: utf-8 -*-

import maya.cmds as cmds


def move_groups():
    grp_list = ('GRP_sqs_head', 'HeadSQS_Controls_Grp', 'Face_RIG_Grp')
    dl_grp = None

    # trova '<char>_deformLayer_Grp'
    children = cmds.listRelatives('CHAR')
    for item in children:
        if item.split('_')[1] == 'deformLayer':
            dl_grp = item
            print(dl_grp)

    if cmds.objExists('head_Ctrl') and cmds.objExists(dl_grp):
        for idx, grp in enumerate(grp_list):
            if idx == 2:
                try:
                    cmds.parent(grp, dl_grp)
                except Exception as e:
                    print(f"Errore: {str(e)}")
            else:
                try:
                    cmds.parent(grp, 'head_Ctrl')
                except Exception as e:
                    print(f"Errore: {str(e)}")


def unlock_attribute():
    check = cmds.getAttr('Transform_Ctrl.face_ctrl_vis', settable=False)
    if not check:
        cmds.setAttr('Transform_Ctrl.face_ctrl_vis', lock=False)


def connect_visibilities():
    try:
        # Verifica se il controllo "Transform_Ctrl" esiste
        if not cmds.objExists('Transform_Ctrl'):
            print("Attenzione: Il controllo Transform_Ctrl non esiste.")
            return

        # Connessione "face_ctrl_vis" di "Transform_Ctrl" a visibilità di "Face_RIG_Grp"
        if cmds.objExists('Face_RIG_Grp'):
            if cmds.objExists('Transform_Ctrl.face_ctrl_vis'):
                cmds.connectAttr('Transform_Ctrl.face_ctrl_vis', 'Face_RIG_Grp.visibility', force=True)
                print("Connessione riuscita: face_ctrl_vis di Transform_Ctrl con la visibilità di Face_RIG_Grp")
            else:
                print("Attenzione: L'attributo face_ctrl_vis non esiste su Transform_Ctrl.")
        else:
            print("Attenzione: Il gruppo Face_RIG_Grp non esiste in scena.")


        # Connessione "face_ctrl_vis" di "Transform_Ctrl" a visibilità di "GRP_sqs_head"
        if cmds.objExists('GRP_sqs_head'):
            if cmds.objExists('Transform_Ctrl.face_ctrl_vis'):
                cmds.connectAttr('Transform_Ctrl.face_ctrl_vis', 'GRP_sqs_head.visibility', force=True)
                print("Connessione riuscita: face_ctrl_vis di Transform_Ctrl con la visibilità di GRP_sqs_head")
            else:
                print("Attenzione: L'attributo face_ctrl_vis non esiste su Transform_Ctrl.")
        else:
            print("Attenzione: Il gruppo GRP_sqs_head non esiste in scena.")

        
        # Connessione "face_ctrl_vis" di "Transform_Ctrl" a visibilità di "HeadSQS_Controls_Grp"
        if cmds.objExists('HeadSQS_Controls_Grp'):
            if cmds.objExists('Transform_Ctrl.face_ctrl_vis'):
                cmds.connectAttr('Transform_Ctrl.face_ctrl_vis', 'HeadSQS_Controls_Grp.visibility', force=True)
                print("Connessione riuscita: face_ctrl_vis di Transform_Ctrl con la visibilità di HeadSQS_Controls_Grp")
            else:
                print("Attenzione: L'attributo face_ctrl_vis non esiste su Transform_Ctrl.")
        else:
            print("Attenzione: Il gruppo HeadSQS_Controls_Grp non esiste in scena.")

    except Exception as e:
        print(f"Errore durante la connessione: {str(e)}")


def move_face_controls_set():
    face_jnt_set = 'FACE_MODULES_Controls'
    sets_to_delete = []

    if cmds.objExists(face_jnt_set):
        list_set = cmds.sets(face_jnt_set, q=True)
        for item in list_set:
            if cmds.objectType(item) == 'objectSet':
                sets_to_delete.append(item)

    for item in sets_to_delete:
        cmds.sets(item, edit=True, fe='FACE_CONTROLS')
    
    cmds.delete(face_jnt_set)     
    cmds.select(clear=True)


def remove_face_set(face_set):
    sets_to_delete = []

    if cmds.objExists(face_set):
        list_set = cmds.sets(face_set, q=True)
        for item in list_set:
            if cmds.objectType(item) == 'objectSet':
                sets_to_delete.append(item)
                list_item = cmds.sets(item, q=True)
                for atom in list_item:
                    if cmds.objectType(atom) == 'objectSet':
                        sets_to_delete.append(atom)

    for item in sets_to_delete:
        cmds.delete(item)  

    cmds.select(clear=True)  


def main(*args):
    move_groups()
    unlock_attribute()
    connect_visibilities()
    move_face_controls_set()
    remove_face_set('FACE_MODULES_Controls')
    remove_face_set('FACE_MODULES_Joints')

    print("### 'Attach Head' done! ###")
