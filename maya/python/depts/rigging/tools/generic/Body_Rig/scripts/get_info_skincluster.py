import maya.cmds as cmds

def influenceJnts(*args):
    ''' Seleziona i joints facenti parte dello skincluster della mesh selezionata. '''
    sel = cmds.ls(sl=True)[0]
    if not sel:
        cmds.warning("Please select something.")
    else:
        for node in cmds.listHistory(sel, levels=4):
            if cmds.nodeType(node) == 'skinCluster':
                print(f"node: {node}")
                # break
                influenceList = cmds.skinCluster(node, q=True, inf=True)
                print(f"joints: {influenceList}")
                n_jnts = len(influenceList)
                print(f"joints selected: {n_jnts}")

                cmds.select(influenceList)
                return influenceList

