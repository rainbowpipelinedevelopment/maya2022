from maya import cmds


def fix_follow():
    selection = ('head_Ctrl', 'cog_Ctrl', 'Transform_Ctrl', 'EyeMaster_Ctrl')
    last = selection[-1]
    lenght = len(selection)
    attribute_name = "Follow"

    # metti posa occhi su 'default'
    cmds.setAttr("EyeMaster_Ctrl.eye_pose", 0)

    # elimina parentConstraint esistente
    conn_list = cmds.listConnections('EyeMaster_Ctrl.Follow', d=True)
    if cmds.objectType(conn_list[0]) == 'condition':
        node_conn = cmds.listConnections(conn_list[0], d=True)
        for item in node_conn:
            check = cmds.objectType(item)
            if check == 'parentConstraint':
                cmds.delete(item)
            else:
                pass

    # elimina attributo 'Follow' 
    cmds.deleteAttr("EyeMaster_Ctrl", attribute="Follow")

    # creo l'attributo enum all ultimo controllo selezionato, con i nomi switch dei controlli precedenti
    enum_attributes = ""
    i = 0
    while i <= lenght-2:
        enum_attributes += selection[i]+":"
        i += 1
    cmds.addAttr(last, ln=attribute_name, at="enum", en=enum_attributes, k=True)

    # tutti i controlli tranne l'ultimo, fanno il parent constrain sul gruppo padre dell ultimo controllo selezionato
    last_group = cmds.pickWalk(last, d="up")
    i = 0
    while i <= lenght-2:
        par_constr = cmds.parentConstraint(selection[i], last_group, mo=True)[0]

        # creo e setto un condition per ogni attributo
        cnd = cmds.createNode("condition", n="CND_"+attribute_name+""+last+"_Weigth"+selection[i])
        cmds.setAttr(cnd+".secondTerm", i)
        cmds.setAttr(cnd+".colorIfTrueR", 1)
        cmds.setAttr(cnd+".colorIfFalseR", 0)

        # connetto gli attributi ai condition e i condition al constrain
        cmds.connectAttr(last+"."+attribute_name, cnd+".firstTerm")
        cmds.connectAttr(cnd+".outColorR", par_constr+"."+selection[i]+"W"+str(i))

        i += 1
        
    # rimetti posa occhi su 'anim'
    cmds.setAttr("EyeMaster_Ctrl.eye_pose", 1)

    cmds.select(cl=True)

    print("### Fix 'follow' fatto ###")


def fix_aim():
    lista_grp = ('L_Eye_FK_Ctrl_Grp', 'R_Eye_FK_Ctrl_Grp')
    lista_ctrl = ('L_eye_Ctrl', 'R_eye_Ctrl')

    # spegni la posa degli occhi
    cmds.setAttr("EyeMaster_Ctrl.eye_pose", 0)

    # cancella aimConstraint già esistente
    for item in lista_grp:
        conn_list = cmds.listConnections(item, d=True)
        if conn_list[0]:
            if cmds.objectType(conn_list[0]) == 'aimConstraint':
                cmds.delete(conn_list[0])
            
    # crea aimC0ntraint nuovo con 'head_Ctrl' come WorldUpObject
    for grp, ctrl in zip(lista_grp, lista_ctrl):
        cmds.aimConstraint(ctrl, grp, mo=True, weight=1, aimVector=[0,0,1], upVector=[0,1,0], worldUpType="objectrotation", worldUpObject="head_Ctrl")
        
    # riaccendi la posa degli occhi
    cmds.setAttr("EyeMaster_Ctrl.eye_pose", 1)

    cmds.select(clear=True)

    print("### Fix 'aim' fatto ###")


def fix_eyes(*args):
    fix_follow()
    fix_aim()

    print("### Fix degli occhi andato a buon fine! ###")