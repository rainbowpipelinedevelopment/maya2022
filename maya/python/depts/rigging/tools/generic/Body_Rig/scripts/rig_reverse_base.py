import maya.cmds as cmds
import json

def get_offset_grp_values():
    grp_offset = 'god_C_offset_out'
    
    if cmds.objExists(grp_offset):
        values_dict = {}
        grp_parameters = {}
        values_dict.setdefault(grp_offset, [])
        for attr in ('translate', 'rotate', 'scale'):
            for axis in ('X', 'Y', 'Z'):
                value = cmds.getAttr(f"{grp_offset}.{attr}{axis}")
                grp_parameters.setdefault(f"{attr}{axis}", value)
        values_dict[grp_offset].append(grp_parameters)
            
    cmds.select(clear=True)

    return values_dict


def save_offset_grp_values(values_dict):
    saveFile = cmds.fileDialog2(caption='Save As', fileFilter='*.json', selectFileFilter='*.json', fileMode=0)
    
    try:
        with open(saveFile[0],'w') as storage:
            json.dump(values_dict, storage, indent=4, sort_keys=True)
    except:
        pass

    cmds.select(clear=True)


def grp_offset_check_save(values_dict):
    continue_check = True
    for grp, values in values_dict.items():
        if continue_check == True:
            for key, value in values[0].items():
                if key in ('translateX', 'translateY','translateZ', 'rotateX', 'rotateY', 'rotateZ'):
                    if value != 0:
                        continue_check = False
                        return True
                elif key in ('scaleX', 'scaleY','scaleZ'):
                    if value != 1:
                        continue_check = False
                        return True
                else:
                    pass
    return False


def reset_offset_grp():
    grp_offset = 'god_C_offset_out'
    if cmds.objExists(grp_offset):
        for attr in ('translate', 'rotate', 'scale'):
            for axis in ('X', 'Y', 'Z'):
                if attr == 'scale':
                    cmds.setAttr(f"{grp_offset}.{attr}{axis}", 1)
                else:
                    cmds.setAttr(f"{grp_offset}.{attr}{axis}", 0)
    cmds.select(clear=True)


def bringGroupsToTopOutliner(groups, move_children=False):
    for group in groups:
        if cmds.objExists(group):
            # Usa il comando parent per spostare il gruppo all'inizio dell'outliner
            cmds.parent(group, world=True)

            # Se richiesto, sposta anche i figli diretti all'inizio dell'outliner
            if move_children:
                children = cmds.listRelatives(group, children=True, fullPath=True) or []
                for child in children:
                    cmds.parent(child, world=True)


def deleteElements(elements_to_delete):
    for element in elements_to_delete:
        if cmds.objExists(element):
            cmds.delete(element)


def renameElement(old_name, new_name):
    if cmds.objExists(old_name):
        cmds.rename(old_name, new_name)


def renameCHAR():
    cmds.select(clear=True)
    grp = 'CHAR'
    
    if cmds.objExists(grp):
        children = cmds.listRelatives(grp)
        token = children[0].split('_')[0]
        cmds.rename(grp, f"{token}_Grp")
    
    cmds.select(clear=True)


def move_grp_xgen():
    sel = cmds.ls("GRP_*_Xgen")
    if len(sel) == 1:
        cmds.parent(sel[0], "Face_RIG_Grp")
    else:
        cmds.warning("### C'è più di un gruppo 'GRP_###_Xgen' nell'outliner! Inserire quello giusto in 'Face_RIG_Grp'. ###")

    cmds.select(clear=True)


def move_body_grp_rig():
    grp = 'CHAR'
    if cmds.objExists(grp):
        children = cmds.listRelatives(grp)
        token = children[0].split('_')[0]
        # char_name = f"{token[0].swapcase()}{token[1:]}"
        grp_char = f"{token}_componentLayer_Grp"
        if cmds.objExists('GRP_bodyRig'):
            grp_list = cmds.listRelatives('GRP_bodyRig', c=True)
            for item in grp_list:
                cmds.parent(item, grp_char)
            cmds.delete('GRP_bodyRig')
            cmds.select(clear=True)


def main(*args):
    groups_to_bring_to_top = ["GRP_sqs_head", "HeadSQS_Controls_Grp", "Face_RIG_Grp", "Hair_RIG_Grp", "Cloth_RIG_Grp", "GRP_CorrectiveJoints_RIG"]
    groups_to_move_children = ["GRP_Meshes_GRO", "GRP_Meshes_RM"]

    # azzerra trasformate gruppo offset dopo averle salvate su un file json
    offset_values = get_offset_grp_values()
    if grp_offset_check_save(offset_values):
        save_offset_grp_values(offset_values)
    reset_offset_grp()

    # Sposta i gruppi entro 'GRP_bodyRig' nel gruppo padre
    move_body_grp_rig()

    # Sposta i gruppi specificati all'inizio dell'outliner
    bringGroupsToTopOutliner(groups_to_bring_to_top, move_children=False)
    bringGroupsToTopOutliner(groups_to_move_children, move_children=True)

    # Elimina gli elementi specificati
    elements_to_delete = ["SETS", "Geo_Layer", "GRP_helper_floor", "hand_L_Ctrl_Grp", "hand_R_Ctrl_Grp", "GRP_Meshes_RM", "GRP_Meshes_RND", "GRP_Meshes_GRO", "GRP_Proxy_Geo", "ALL_CONTROLS", "BODY_CONTROLS", "MSH", "head_C_sSet", "leg_L_sSet", "leg_R_sSet", "spine_C_sSet", "god_C_sSet", "armClavHand_L_Set", "armClavHand_R_sSet", "arm_L_sSet", "arm_R_sSet", "clavicle_L_sSet", "clavicle_R_sSet", "hand_L_sSet", "fingersAndThumb_L_sSet", "fingers_L_sSet", "thumb_L_sSet", "index_L_sSet", "middle_L_sSet", "pinky_L_sSet", "ring_L_sSet", "hand_R_sSet", "fingersAndThumb_R_sSet", "fingers_R_sSet", "thumb_R_sSet", "index_R_sSet", "middle_R_sSet", "pinky_R_sSet", "ring_R_sSet", "LOC_Tracker_Grp"]
    deleteElements(elements_to_delete)

    # Rinomina il gruppo specificato
    renameElement("GRP_Meshes_UM", "GRP_Meshes_UM_OLD")
    renameElement("HAIR_CONTROLS", "HAIR_CONTROLS_OLD")
    renameElement("FACE_CONTROLS", "FACE_CONTROLS_OLD")
    renameElement("CLOTHES_CONTROLS", "CLOTHES_CONTROLS_OLD")
    renameElement("GROOMING", "GROOMING_OLD")
    renameCHAR()

    # Sposta il gruppo xgen in 'Face_GRP_Rig'
    move_grp_xgen()


    print("### 'Rig Reverse Base' done! ###")