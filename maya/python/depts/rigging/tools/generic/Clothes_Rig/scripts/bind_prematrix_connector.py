import maya.cmds as cmds

def _get_skincluster(sel):
    return cmds.listConnections(sel, type='skinCluster')


def _get_father(jnt):
    if not 'zero' in jnt:
        pcc = cmds.listConnections(cmds.listRelatives(jnt, c=1, ni=1, typ='parentConstraint'), plugs=1)
        return cmds.listRelatives([x for x in pcc if '.parentMatrix' in x][0].split('.')[0], p=1, ni=1)[0]
    else:
        return jnt


def bind_prematrix_connector(*args):
    selection = cmds.ls(sl=1, dag=1, shapes=1, ni=1)
    nType = cmds.nodeType (selection)
    if nType != "mesh" and nType != "nurbsSurface":
        print("bind_prematrix_connector: you have to select a mesh with a skinCluster.")
        return

    sc = _get_skincluster(selection)[0]
    jntSK = [x for x in cmds.skinCluster(_get_skincluster(selection)[0], q=1, influence=1)]
    connections = [(x,y) for x in cmds.listConnections(jntSK, plugs=1) if '.matrix[' in x for y in cmds.listConnections(x, plugs=1)]

    for info in connections:
        try:
            sk = info[0].split('.')[0]
            if sk == sc:
                min = info[0].split('matrix[')[1].split(']')[0]
                jnt = info[1].split('.')[0]
                ctrl = _get_father(info[1].split('.')[0])
                if not 'zero' in jnt:
                    cmds.connectAttr('%s.worldInverseMatrix[0]' % ctrl, '{0}.bindPreMatrix[{1}]'.format(sk, min), f=1)
                    print("Connected: {0}.worldInverseMatrix[0] ----> {1}.bindPreMatrix[{2}]".format(ctrl, sk, min))
                else:
                    continue
            print('')
        except:
            continue


# bind_prematrix_connector()