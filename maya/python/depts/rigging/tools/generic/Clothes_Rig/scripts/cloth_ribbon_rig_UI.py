# -*- coding: utf-8 -*-

import maya.cmds as cmds
from maya import OpenMayaUI as omui
import os


# -----------------------------------------------------------------
# PySide
# -----------------------------------------------------------------
try:
    from shiboken2 import wrapInstance
except ImportError:
    from shiboken import wrapInstance

try:
    from PySide2.QtGui import QIcon
    from PySide2.QtWidgets import QWidget
except ImportError:
    from PySide.QtGui import QIcon, QWidget


class ClothRibbon:
    """ Crea una ribbon con gli uvPin. """
    def __init__(self):
        self.cloth_nurbs = None
        self.controllo = None

        # -----------------------------------------------------------------
        # Path
        # -----------------------------------------------------------------
        localpipe = os.getenv("LOCALPY")
        self.image_path = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "Clothes_Rig", "icons")

    
    def _utility_make_ctrl(self, name):
        """Utility che crea un controllo con gruppo anchor e offset."""
        cmds.select(clear=True)

        ctrl_name = "{}_Ctrl".format(name)
        ctrl = cmds.curve(n=ctrl_name, d=1, k=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16],\
                          p=[(-0.5, 0.5, -0.5), (-0.5, 0.5, 0.5), (0.5, 0.5, 0.5), (0.5, 0.5, -0.5), \
                          (-0.5, 0.5, -0.5), (-0.5, -0.5, -0.5), (-0.5, -0.5, 0.5), (0.5, -0.5, 0.5), \
                          (0.5, 0.5, 0.5), (-0.5, 0.5, 0.5), (-0.5, -0.5, 0.5), (-0.5, -0.5, -0.5), \
                          (0.5, -0.5, -0.5), (0.5, 0.5, -0.5), (0.5, 0.5, 0.5), (0.5, -0.5, 0.5), \
                          (0.5, -0.5, -0.5)])

        for shape in cmds.listRelatives(ctrl, s=True, f=True) or []:
            shapeRenamed = cmds.rename(shape, "{}Shape".format(ctrl_name))

        cmds.setAttr("{}.v".format(ctrl), lock=True, keyable=False, channelBox=False)
        cmds.setAttr("{}Shape.overrideEnabled".format(ctrl), 1)
        cmds.setAttr("{}Shape.overrideColor".format(ctrl), 18)

        grp_anchor = cmds.group(name="{}_anchor_Grp".format(name), empty=True)
        grp_offset = cmds.group(name="{}_offset_Grp".format(name), empty=True)
        
        cmds.parent(ctrl, grp_offset)
        cmds.parent(grp_offset, grp_anchor)
        cmds.select(clear=True)

        return grp_anchor, grp_offset, ctrl
    
   
    def _utility_create_joints(self, control):
        """Crea un joint per locator."""
        name_jnt = control.replace("Ctrl", "jnt")
        piv = cmds.xform(control, q=True, piv=True, ws=True)
        cmds.select(clear=True)
        jnt = cmds.joint(name=name_jnt)
        cmds.setAttr("{}.visibility".format(jnt), 0)
        mtt = cmds.matchTransform(jnt, control)
        cmds.makeIdentity(jnt, apply=True, t=1, r=1, s=1, n=0)
        cmds.parent(jnt, control)
        cmds.select(clear=True)

        return jnt


    def _get_input_name_rig(self, name_attribute):
        string_input = cmds.textField(name_attribute, query=True, text=True)
        
        if not string_input:
            cmds.error("Please write rig name to add.")
        else:
            is_number = string_input[0].isdigit()
            obj_name = "{}_Rig_Grp".format(string_input)
            
            if is_number:
                # check se il nome inizia per un numero
                cmds.error("Please insert a string as the first character of the name, not a number.")
            
            elif cmds.objExists(obj_name):
                # check se esiste già un rig con lo stesso nome
                cmds.error("### Please choose a different name for this rig, '{}' already exists! ###".format(obj_name))
            
            else:
                return string_input


    def _get_input_intSliderGrp(self, name_attribute):
        # Retrieve text insert from user
        num_items = cmds.intSliderGrp(name_attribute, query=True, value=True)

        return num_items


    def get_nurbs(self, *args):
        selection = cmds.ls(sl=True)
        cmds.textField('txtf_nurbs', e=1, tx="")

        children = cmds.listHistory(selection[0], levels=1)
        is_nurbs = False
        for node in children:
            check_type = cmds.objectType(node)
            if check_type == "nurbsSurface":
                is_nurbs = True

        if not selection:
            cmds.error("Please select a nurbs.\n")
        elif len(selection) > 1:
            cmds.error("Please select only on nurbs.\n")
        elif not is_nurbs:
            cmds.error("Please select a nurbs.\n")
        else:
            cmds.textField('txtf_nurbs', e=1, tx=selection[0])
            cmds.button(self.btn_cloth_select_nurbs, e=True, en=False)
            cmds.button(self.btn_cloth_scale, e=True, en=True)
            self.cloth_nurbs = selection[0]

        cmds.select(clear=True)


        return selection[0]


    def _create_groups_hierarchy(self, rig_name, controls=True, joints=False, utility=False, **kwargs):
        """Crea la gerachia di gruppi del rig."""
        main_grp_name = "{}_Ribbon_Rig_Grp".format(rig_name)
        joints_grp_name = "{}_Ribbon_Joints_Grp".format(rig_name)
        ctrls_grp_name = "{}_Ribbon_Controls_Grp".format(rig_name)
        tmp_grp_name = "{}_Ribbon_TMP_Grp".format(rig_name)
        utility_grp_name = "{}_Ribbon_Utility_Grp".format(rig_name)
            
        if not cmds.objExists(main_grp_name):
            main_grp = cmds.group(name=main_grp_name, empty=True)
            
            if controls:
                ctrls_grp = cmds.group(name=ctrls_grp_name, empty=True)
                cmds.parent(ctrls_grp, main_grp)

            if joints:
                joints_grp = cmds.group(name=joints_grp_name, empty=True)
                # cmds.setAttr("{}.visibility".format(joint_grp), 0)
                cmds.parent(joints_grp, main_grp)

            if utility:
                utility_grp = cmds.group(name=utility_grp_name, empty=True)
                # cmds.setAttr("{}.visibility".format(utility_grp), 0)
                cmds.parent(utility_grp, main_grp)
            
            
            if joints and utility and controls:
                return main_grp, joints_grp, utility_grp, ctrls_grp
            elif joints and not utility and controls:
                return main_grp, joints_grp, ctrls_grp
            elif joints and not utility and not controls:
                return main_grp, joints_grp
            elif joints and utility and not controls:
                return main_grp, joints_grp, utility_grp
            elif utility and controls and not joints:
                return main_grp, utility_grp, ctrls_grp
            elif utility and not controls and not joints:
                return main_grp, utility_grp
            else:
                return main_grp

        else:
            cmds.error("Attenzione, esiste già un gruppo '{}', si prega di cambiare il nome del rig.".format(main_grp_name))


    def _create_joints(self, name_rig, n_ctrls, jnts_grp):
        """ Crea i bind joints. """
        joints_list = []

        for idx, jnt in enumerate(range(n_ctrls)):
            cmds.select(clear=True)
            jnt = cmds.joint(name="{}_ribbon_{:02d}_jnt".format(name_rig, idx+1), p=(0, 0, 0), radius=0.5)
            joints_list.append(jnt)
            
            cmds.parent(jnt, jnts_grp)
            cmds.select(cl=True)

        return joints_list


    def _rename_surface(self, name_rig, nurbs, utility_grp):
        """Rinomina la Nurbs in base al name_rig."""
        name_nurbs = f"NRB_{name_rig}_surface"

        if cmds.objExists(name_nurbs):
            cmds.warning(f"### '{name_nurbs}' already exists! Adding an '_' at the end of the name.")
            name_nurbs = f"NRB_{name_rig}_surface_"
            nurbs_renamed = cmds.rename(nurbs, name_nurbs)
        else:
            nurbs_renamed = cmds.rename(nurbs, name_nurbs)

        cmds.parent(nurbs_renamed, utility_grp)

        return nurbs_renamed


    def _create_uvpin_system(self, name_rig, nurbs, n_ctrls, grp):
        """Crea un nodo uvpin per surface patch."""
        num_spans = (n_ctrls * 2) - 1
        surf_shape = cmds.listRelatives(nurbs, s=True)[0]
        lista_controlli = []
        lista_joints = []
        lista_controlli_totali = []
        lista_locators = []

        # Crea un solo nodo uvpin da usare per questo rig
        uvpin_node = cmds.createNode("uvPin", name="UVP_{}".format(name_rig))
        cmds.connectAttr("{}.worldSpace[0]".format(surf_shape), "{}.deformedGeometry".format(uvpin_node), force=True)
        cmds.connectAttr("{}.local".format(surf_shape), "{}.originalGeometry".format(uvpin_node), force=True)

        # [ ] crea nodi plusMinus e fai connessioni
        counter_ctrl = 0
        for idx in range(num_spans):
            valueV = (1 / float(2 * num_spans)) + (2 * (1 / float(2 * num_spans)) * float(idx))
            # valueV =  ((1 / num_spans) * float(idx+1))
            cmds.setAttr("{}.coordinate[{}].coordinateU".format(uvpin_node, idx), 0.5)
            cmds.setAttr("{}.coordinate[{}].coordinateV".format(uvpin_node, idx), valueV)

            # se il controllo è visibile
            if idx % 2 != 1: 
                counter_ctrl += 1
                base_name = "{}_{:02d}".format(name_rig, counter_ctrl)

                # crea locator
                loc = cmds.spaceLocator(name="LOC_{}".format(base_name))[0]
                cmds.setAttr("{}Shape.visibility".format(loc), 0)
                cmds.parent(loc, grp)
                lista_locators.append(loc)
            
                # crea controllo
                grp_anchor, grp_offset, ctrl = self._utility_make_ctrl(base_name)
                lista_controlli.append(ctrl)
                lista_controlli_totali.append(ctrl)
                cmds.parent(grp_anchor, loc)

                # crea joint
                jnt = self._utility_create_joints(ctrl)
                lista_joints.append(jnt)

                # connetti locator al nodo uvpin
                cmds.connectAttr("{}.outputMatrix[{}]".format(uvpin_node, idx), "{}.offsetParentMatrix".format(loc), force=True)

            # se il controllo è nascosto
            else:
                base_name = "{}_{:02d}_hidden".format(name_rig, counter_ctrl)

                # crea locator
                loc = cmds.spaceLocator(name="LOC_{}".format(base_name))[0]
                cmds.setAttr("{}Shape.visibility".format(loc), 0)
                cmds.setAttr("{}.visibility".format(loc), 0)
                cmds.parent(loc, grp)
            
                # crea controllo
                grp_anchor, grp_offset, ctrl = self._utility_make_ctrl(base_name)
                lista_controlli_totali.append(ctrl)
                cmds.parent(grp_anchor, loc)

                # crea joint
                jnt = self._utility_create_joints(ctrl)
                lista_joints.append(jnt)

                # connetti locator al nodo uvpin
                cmds.connectAttr("{}.outputMatrix[{}]".format(uvpin_node, idx), "{}.offsetParentMatrix".format(loc), force=True)
                
            cmds.select(clear=True)

        return lista_controlli, lista_joints, lista_controlli_totali, lista_locators


    def _create_plusminus_node_system(self, lista_controlli_totali):
        """Crea sistema di nodi plusMinusAverage tra controlli visibili e nascosti."""
        for idx, controllo in enumerate(lista_controlli_totali):
            if idx % 2 == 1:
                # creo nodi e collego al ctrl driven
                nodo_pls_translate = cmds.createNode('plusMinusAverage', name="PLS_{}_translate".format(controllo))
                nodo_pls_rotate = cmds.createNode('plusMinusAverage', name="PLS_{}_rotate".format(controllo))

                # setto nodi su 'sum' per le traslate, 'average' per le rotate
                cmds.setAttr("{}.operation".format(nodo_pls_translate), 3)
                cmds.setAttr("{}.operation".format(nodo_pls_rotate), 3)

                # connessioni
                cmds.connectAttr("{}.output3D".format(nodo_pls_translate), "{}.translate".format(controllo), force=True)
                cmds.connectAttr("{}.output3D".format(nodo_pls_rotate), "{}.rotate".format(controllo), force=True)
                
                if idx == len(lista_controlli_totali)-1:
                    # connessione ctrl precedente
                    cmds.connectAttr("{}.translate".format(lista_controlli_totali[idx-1]), "{}.input3D[0]".format(nodo_pls_translate), force=True)
                    cmds.connectAttr("{}.rotate".format(lista_controlli_totali[idx-1]), "{}.input3D[0]".format(nodo_pls_rotate), force=True)

                    # connessione ctrl successivo
                    cmds.connectAttr("{}.translate".format(lista_controlli_totali[0]), "{}.input3D[1]".format(nodo_pls_translate), force=True)
                    cmds.connectAttr("{}.rotate".format(lista_controlli_totali[0]), "{}.input3D[1]".format(nodo_pls_rotate), force=True)

                else:
                    # connessione ctrl precedente
                    cmds.connectAttr("{}.translate".format(lista_controlli_totali[idx-1]), "{}.input3D[0]".format(nodo_pls_translate), force=True)
                    cmds.connectAttr("{}.rotate".format(lista_controlli_totali[idx-1]), "{}.input3D[0]".format(nodo_pls_rotate), force=True)

                    # connessione ctrl successivo
                    cmds.connectAttr("{}.translate".format(lista_controlli_totali[idx+1]), "{}.input3D[1]".format(nodo_pls_translate), force=True)
                    cmds.connectAttr("{}.rotate".format(lista_controlli_totali[idx+1]), "{}.input3D[1]".format(nodo_pls_rotate), force=True)


    def _cloth_make_scale_constraint(self, ctrl, lista_locators):
        """Fai lo scale contraint dal controllo selezionato ai locator."""
        for locator in lista_locators:
            cmds.scaleConstraint(ctrl, locator, mo=True, w=1)


    def _utility_create_set(self, name_rig, type_set, obj_list):
        """Utility per creare un set."""

        name_rig_set = "{}_SET".format(name_rig)
        name_set = "{}_{}_SET".format(name_rig, type_set)
        
        if not cmds.objExists(name_rig_set):
            module_set = cmds.sets(empty=True, name=(name_rig_set))
        else:
            module_set = name_rig_set

        if not cmds.objExists(name_set):
            objects_set = cmds.sets(empty=True, name=name_set)
            # Parenta il set del modulo dentro al set globale
            cmds.sets(objects_set, edit=True, forceElement=module_set)
        else:
            objects_set = name_set

        for obj in obj_list:
            cmds.sets(obj, edit=True, forceElement=objects_set)


    def get_ctrl_scale(self, *args):
        """Fai lo scale contraint dal controllo selezionato ai locator."""
        selection = cmds.ls(sl=True)[0]

        crv_shape = cmds.listRelatives(selection, shapes=True)[0]
        # print(crv_shape)

        check_type = cmds.objectType(crv_shape)
        # print(check_type)

        if check_type == "nurbsCurve":
            self.controllo = selection
            cmds.select(clear=True)
            
            # Update UI            
            cmds.textField(self.cloth_txtf_control, e=True, tx=self.controllo)
            cmds.button(self.btn_cloth_scale, e=True, en=False)
            cmds.button(self.btn_cloth_scale_undo, e=True, en=True)


    def ctrl_scale_undo(self, *args):
        """Resetta la variabile del controllo da cui scalare il rig."""
        self.controllo = None
        cmds.select(clear=True)

        # Update UI
        cmds.textField(self.cloth_txtf_control, e=True, tx="")
        cmds.button(self.btn_cloth_scale, e=True, en=True)
        cmds.button(self.btn_cloth_scale_undo, e=True, en=False)


    def build_ribbon(self, *args):  # DA SISTEMARE
        name_rig = self._get_input_name_rig('txtf_ribbon_name')
        n_ctrls = self._get_input_intSliderGrp('int_num_ctrls')
        main_grp, utility_grp, ctrls_grp = self._create_groups_hierarchy(name_rig, controls=True, joints=False, utility=True, tmp=False)
        nurbs_surf = self._rename_surface(name_rig, self.cloth_nurbs, utility_grp)
        lista_controlli, lista_joints, lista_controlli_totali, lista_locators = self._create_uvpin_system(name_rig, nurbs_surf, n_ctrls, ctrls_grp)
        self._create_plusminus_node_system(lista_controlli_totali)
        self._utility_create_set(name_rig, "controls", lista_controlli)
        self._utility_create_set(name_rig, "joints", lista_joints)
        if self.controllo:
            self._cloth_make_scale_constraint(self.controllo, lista_locators)

        cmds.select(clear=True)

        # Update Variable
        self.cloth_nurbs = None
        self.controllo = None

        # Update UI
        cmds.textField('txtf_ribbon_name', e=True, tx="")
        cmds.textField('txtf_nurbs', e=True, tx="")
        cmds.textField('txtf_control', e=True, tx="")
        cmds.intSliderGrp('int_num_ctrls', e=True, value=6)
        cmds.button(self.btn_cloth_select_nurbs, e=True, en=True)
        cmds.button(self.btn_cloth_scale, e=True, en=False)
        cmds.button(self.btn_cloth_scale_undo, e=True, en=False)
    
   
    # ------------------------------------------------------------------------
    # UI
    # ------------------------------------------------------------------------
    def main(self):
        # Main window
        win_width = 290
        color_label_01 = (0, 0.5, 0.6)
        color_label_02 = (0.4, 0.4, 0.4)
        
        mainWin = "RBW_Cloth_Ribbon_Tool"

        if cmds.window(mainWin, exists = 1):
            cmds.deleteUI(mainWin, window = 1)

        ww = cmds.window(mainWin, title = "RBW Cloth Ribbon Tool", mxb=0, sizeable=True, resizeToFitChildren=True)
        
        # Get a pointer and convert it to Qt Widget object
        qw = omui.MQtUtil.findWindow(ww)
        widget = wrapInstance(int(qw), QWidget)

        # Create a QIcon object
        iconpath = os.path.join(self.image_path, "RainbowCGI_icona.ico")

        # Assign the icon
        icon = QIcon(iconpath)
        widget.setWindowIcon(icon)

        # Main layout
        col = cmds.columnLayout(co=("both", 5), adjustableColumn=True)
        
        # Carica l'immagine delle brows
        img = os.path.join(self.image_path, "cloth.png")
        cmds.text(h = 10, l = "")
        cmds.iconTextButton(style="iconOnly", image1=img, p=col, w=100)
        cmds.text(h = 10, l = "")
        cmds.setParent("..")

        # Main layout
        col = cmds.columnLayout(co=("both", 5), adjustableColumn=True)

        # RIBBON RIG -----------------------------------------------------------
        cmds.frameLayout( label="CLOTH RIBBON RIG", bgc = color_label_02, font="boldLabelFont", marginHeight=5)
        
        # set the name
        cmds.frameLayout( label="1. Inserisci il nome del rig.", marginHeight=10, bgc=color_label_01)
        cmds.textField('txtf_ribbon_name')
        cmds.setParent("..")

        # selected nurbs
        cmds.frameLayout( label="2. Seleziona la nurbs.", marginHeight=10, bgc=color_label_01)
        cmds.textField("txtf_nurbs", editable=False)
        self.btn_cloth_select_nurbs = cmds.button("btn_select_nurbs", w=win_width, h=30, l="SELECT NURBS", c=self.get_nurbs)

        # num ctrls
        cmds.frameLayout( label="3. Inserisci il numero di controlli.", marginHeight=10, bgc = color_label_01)
        cmds.intSliderGrp("int_num_ctrls", field=True, label="n. Ctrls:", minValue=1, maxValue=60, value=2, cw=[(1, 50), (2, 40)], cat=[(2, 'both', 10), (3, 'right', 10)])
        cmds.setParent("..")

        # define the control where this rig will scale
        cmds.frameLayout( label="4. Seleziona il controllo da cui far scalare il rig (opzionale)", marginHeight=10, bgc = color_label_01)
        cmds.rowLayout(numberOfColumns = 2, adjustableColumn = 1)
        self.btn_cloth_scale = cmds.button(w = ((win_width / 2) - 2), h=30, l = "| Select Control |", en=False, c = self.get_ctrl_scale)
        self.btn_cloth_scale_undo = cmds.button(w = ((win_width / 2) - 2), h=30, l = "| Undo |", c = self.ctrl_scale_undo, en = 0)
        cmds.setParent("..")
        self.cloth_txtf_control = cmds.textField("txtf_control", w = win_width, h=30, ed = 0, ebg=True, bgc=(0.22, 0.22, 0.22), font="fixedWidthFont")
        cmds.setParent("..")
        
        # Build ribbon rig
        cmds.button("btn_build_rig", w=win_width, h=50, l="MAKE RIBBON", c=self.build_ribbon, bgc=color_label_02)
        cmds.setParent("..")

        cmds.showWindow(mainWin)
