# -*- coding: utf-8 -*-

import maya.cmds as cmds

class NurbsRibbon:
    """ Crea una ribbon con gli uvPin. """
    def __init__(self):
        self.nurbs = None


    def _get_input_name_rig(self, name_attribute):
        string_input = cmds.textField(name_attribute, query=True, text=True)
        
        if not string_input:
            cmds.error("Please write rig name to add.")
        else:
            is_number = string_input[0].isdigit()
            obj_name = "{}_Rig_Grp".format(string_input)
            
            if is_number:
                # check se il nome inizia per un numero
                cmds.error("Please insert a string as the first character of the name, not a number.")
            
            elif cmds.objExists(obj_name):
                # check se esiste già un rig con lo stesso nome
                cmds.error("### Please choose a different name for this rig, '{}' already exists! ###".format(obj_name))
            
            else:
                return string_input


    def _get_input_intSliderGrp(self, name_attribute):
        # Retrieve text insert from user
        num_items = cmds.intSliderGrp(name_attribute, query=True, value=True)

        return num_items


    def get_nurbs(self, *args):
        selection = cmds.ls(sl=True)
        cmds.textField('txtf_nurbs', e=1, tx="")

        children = cmds.listHistory(selection[0], levels=1)
        is_nurbs = False
        for node in children:
            check_type = cmds.objectType(node)
            if check_type == "nurbsSurface":
                is_nurbs = True

        if not selection:
            cmds.error("Please select a nurbs.\n")
        elif len(selection) > 1:
            cmds.error("Please select only on nurbs.\n")
        elif not is_nurbs:
            cmds.error("Please select a nurbs.\n")
        else:
            cmds.textField('txtf_nurbs', e=1, tx=selection[0])
            self.nurbs = selection[0]

        cmds.select(clear=True)

        return selection[0]


    def _rebuild_nurbs(self, nurbs):
        """ Ottimizza la nurbs con il rebuild. """
        spans_U = cmds.getAttr( f"{surface}.spansU")
        spans_V = cmds.getAttr( f"{surface}.spansV")
        cmds.rebuildSurface(self.nurbs, n=self.name, ch=False, rpo=True, rt=0, end=1, kr=0, kcp=0, kc=False, su=spans_U, du=3, sv=spans_V, dv=3, tol=0.01, fr=0, dir=2)

    
    def _create_groups_hierarchy(self, rig_name, controls=False, joints=False, utility=False, **kwargs):
        """Crea la gerachia di gruppi del rig."""
        main_grp_name = "{}_Ribbon_Rig_Grp".format(rig_name)
        joints_grp_name = "{}_Ribbon_Joints_Grp".format(rig_name)
        ctrls_grp_name = "{}_Ribbon_Controls_Grp".format(rig_name)
        tmp_grp_name = "{}_Ribbon_TMP_Grp".format(rig_name)
        utility_grp_name = "{}_Ribbon_Utility_Grp".format(rig_name)
            
        if not cmds.objExists(main_grp_name):
            main_grp = cmds.group(name=main_grp_name, empty=True)
            
            if controls:
                ctrls_grp = cmds.group(name=ctrls_grp_name, empty=True)
                cmds.parent(ctrls_grp, main_grp)

            if joints:
                joints_grp = cmds.group(name=joints_grp_name, empty=True)
                # cmds.setAttr("{}.visibility".format(joint_grp), 0)
                cmds.parent(joints_grp, main_grp)

            if utility:
                utility_grp = cmds.group(name=utility_grp_name, empty=True)
                # cmds.setAttr("{}.visibility".format(utility_grp), 0)
                cmds.parent(utility_grp, main_grp)
            
            
            if joints and utility and controls:
                return main_grp, joints_grp, utility_grp, ctrls_grp
            elif joints and not utility and controls:
                return main_grp, joints_grp, ctrls_grp
            elif joints and not utility and not controls:
                return main_grp, joints_grp
            elif joints and utility and not controls:
                return main_grp, joints_grp, utility_grp
            elif utility and controls and not joints:
                return main_grp, utility_grp, ctrls_grp
            elif utility and not controls and not joints:
                return main_grp, utility_grp
            else:
                return main_grp

        else:
            cmds.error("Attenzione, esiste già un gruppo '{}', si prega di cambiare il nome del rig.".format(main_grp_name))


    def _create_joints(self, name_rig, n_jnts, jnts_grp):
        """ Crea i bind joints. """
        joints_list = []

        for idx, jnt in enumerate(range(n_jnts)):
            cmds.select(clear=True)
            jnt = cmds.joint(name="{}_ribbon_{:02d}_jnt".format(name_rig, idx+1), p=(0, 0, 0), radius=0.5)
            joints_list.append(jnt)
            
            cmds.parent(jnt, jnts_grp)
            cmds.select(cl=True)

        return joints_list


    def _rename_surface(self, name_rig, nurbs, utility_grp):
        """Rinomina la Nurbs in base al name_rig."""
        name_nurbs = f"NRB_{name_rig}_surface"

        if cmds.objExists(name_nurbs):
            cmds.warning(f"### '{name_nurbs}' already exists! Adding an '_' at the end of the name.")
            name_nurbs = f"NRB_{name_rig}_surface_"
            nurbs_renamed = cmds.rename(nurbs, name_nurbs)
        else:
            nurbs_renamed = cmds.rename(nurbs, name_nurbs)

        cmds.parent(nurbs_renamed, utility_grp)

        return nurbs_renamed


    def _create_uvpun_system(self, name_rig, nurbs, n_jnts, grp):
        surf_shape = cmds.listRelatives(nurbs, s=True)[0]
        lista_joints = []
        lista_locators = []

        # Crea un solo nodo uvpin da usare per questo rig
        uvpin_node = cmds.createNode("uvPin", name="UVP_{}_ribbon".format(name_rig))
        cmds.connectAttr("{}.worldSpace[0]".format(surf_shape), "{}.deformedGeometry".format(uvpin_node), force=True)
        cmds.connectAttr("{}.local".format(surf_shape), "{}.originalGeometry".format(uvpin_node), force=True)

        counter_ctrl = 0
        for idx in range(n_jnts):
            valueU = (1 / float(2 * n_jnts)) + (2 * (1 / float(2 * n_jnts)) * float(idx))
            print(f"valueV: {valueU}")
            cmds.setAttr("{}.coordinate[{}].coordinateU".format(uvpin_node, idx), valueU)
            cmds.setAttr("{}.coordinate[{}].coordinateV".format(uvpin_node, idx), 0.5)

            counter_ctrl += 1
            base_name = "{}_{:02d}".format(name_rig, counter_ctrl)

            # crea locator
            loc = cmds.spaceLocator(name="LOC_{}".format(base_name))[0]
            cmds.setAttr("{}Shape.visibility".format(loc), 0)
            cmds.parent(loc, grp)
            lista_locators.append(loc)
        
            # crea joint
            cmds.select(clear=True)
            jnt = cmds.joint(name="{}_ribbon_{:02d}_jnt".format(name_rig, idx+1), p=(0, 0, 0), radius=0.5)
            lista_joints.append(jnt)
            cmds.parent(jnt, loc)
            cmds.select(cl=True)

            # connetti locator al nodo uvpin
            cmds.connectAttr("{}.outputMatrix[{}]".format(uvpin_node, idx), "{}.offsetParentMatrix".format(loc), force=True)

        return lista_joints


    def _utility_create_set(self, name_rig, type_set, obj_list):
        """Utility per creare un set."""

        name_rig_set = "{}_SET".format(name_rig)
        name_set = "{}_{}_SET".format(name_rig, type_set)
        
        if not cmds.objExists(name_rig_set):
            module_set = cmds.sets(empty=True, name=(name_rig_set))
        else:
            module_set = name_rig_set

        if not cmds.objExists(name_set):
            objects_set = cmds.sets(empty=True, name=name_set)
            # Parenta il set del modulo dentro al set globale
            cmds.sets(objects_set, edit=True, forceElement=module_set)
        else:
            objects_set = name_set

        for obj in obj_list:
            cmds.sets(obj, edit=True, forceElement=objects_set)


    def build_ribbon(self, *args):  # DA SISTEMARE
        name_rig = self._get_input_name_rig('txtf_ribbon_name')
        n_jnts = self._get_input_intSliderGrp('int_nun_jnts')
        main_grp, jnts_grp, utility_grp = self._create_groups_hierarchy(name_rig, controls=False, joints=True, utility=True, tmp=False)
        # joint_list = self._create_joints(name_rig, n_jnts, jnts_grp)
        nurbs_surf = self._rename_surface(name_rig, self.nurbs, utility_grp)
        lista_joints = self._create_uvpun_system(name_rig, nurbs_surf, n_jnts, jnts_grp)
        self._utility_create_set(name_rig, "joints", lista_joints)


        # Update Variable
        self.nurbs = None

        # Update UI
        cmds.textField('txtf_ribbon_name', e=True, tx="")
        cmds.textField('txtf_nurbs', e=True, tx="")
        cmds.intSliderGrp('int_nun_jnts', e=True, value=6)
    
   
    # ------------------------------------------------------------------------
    # UI
    # ------------------------------------------------------------------------
    def main(self):
        # Main window
        winWidth = 290
        color_label_01 = (.22, 0.22, 0.22)
        color_label_02 = (.15, 0.15, 0.15)
        
        mainWin = "RBW_Ribbon_Tool"

        if cmds.window(mainWin, exists = 1):
            cmds.deleteUI(mainWin, window = 1)

        cmds.window(mainWin, title = "RBW Ribbon Tool", mxb=0, sizeable=True, resizeToFitChildren=True)

        # Main layout
        col = cmds.columnLayout(co=("both", 5), adjustableColumn=True)

        # RIBBON RIG -----------------------------------------------------------
        cmds.frameLayout( label="RIBBON RIG", bgc = color_label_02, font="boldLabelFont", marginHeight=5)
        
        # set the name
        cmds.frameLayout( label="1. Inserisci il nome del rig.", marginHeight=10, bgc=color_label_01)
        cmds.textField('txtf_ribbon_name')
        cmds.setParent("..")

        # selected nurbs
        cmds.frameLayout( label="2. Seleziona la nurbs.", marginHeight=10, bgc=color_label_01)
        cmds.textField("txtf_nurbs", editable=False)
        cmds.button("btn_select_nurbs", w=winWidth, h=30, l="SELECT NURBS", c=self.get_nurbs)

        # num jnts
        cmds.frameLayout( label="3. Inserisci il numero di joints.", marginHeight=10, bgc = color_label_01)
        cmds.intSliderGrp("int_nun_jnts", field=True, label="n. Jnts:", minValue=2, maxValue=60, value=6, cw=[(1, 50), (2, 40)], cat=[(2, 'both', 10), (3, 'right', 10)])
        cmds.setParent("..")
        
        # Build ribbon rig
        cmds.button("btn_build_rig", w=winWidth, h=50, l="MAKE RIBBON", c=self.build_ribbon, bgc=color_label_02)
        cmds.setParent("..")

        cmds.showWindow(mainWin)
