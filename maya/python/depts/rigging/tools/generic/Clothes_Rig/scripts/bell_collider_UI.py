# -*- coding: utf-8 -*-

import pymel.core as pm
import maya.cmds as cmds
from maya import OpenMayaUI as omui
import os


# -----------------------------------------------------------------
# PySide
# -----------------------------------------------------------------
try:
    from shiboken2 import wrapInstance
except ImportError:
    from shiboken import wrapInstance

try:
    from PySide2.QtGui import QIcon
    from PySide2.QtWidgets import QWidget
except ImportError:
    from PySide.QtGui import QIcon, QWidget


class BellCollider:
    """Crea il sistema bell collider --> https://github.com/azagoruyko/colliders"""
    def __init__(self):
        # -----------------------------------------------------------------
        # Path
        # -----------------------------------------------------------------
        localpipe = os.getenv("LOCALPY")
        self.image_path = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "Clothes_Rig", "icons")


    # ------------------------------------------------------------------------
    # Utilitis
    # ------------------------------------------------------------------------
    def _utility_input_name_collider(self, name_attribute):
        """Memorizza il nome del rig."""
        string_input = cmds.textField(name_attribute, query=True, text=True)
        
        if not string_input:
            cmds.error("Please write rig name to add.")
        else:
            is_number = string_input[0].isdigit()
            obj_name = "{}_Grp".format(string_input)
            
            if is_number:
                # check se il nome inizia per un numero
                cmds.error("Please insert a string as the first character of the name, not a number.")
            
            elif cmds.objExists(obj_name):
                # check se esiste già un rig con lo stesso nome
                cmds.error("### Please choose a different name for this rig, '{}' already exists! ###".format(obj_name))
            
            else:
                return string_input


    def _utility_input_number(self, name_attribute):
        # Retrieve text insert from user
        num = cmds.intSliderGrp(name_attribute, query=True, value=True)

        return num


    # ------------------------------------------------------------------------
    # BELL COLLIDER
    # ------------------------------------------------------------------------
    def bell_collider(self, name_collider, num_rings, num_out_joints):
        """Script preso e adattato da Alexander Zagoruyko."""
        grp = pm.createNode("transform", n=name_collider + "_bellCollider_Grp")
        grp.addAttr("scaleFactor", min=0.001, dv=1, k=True)
        grp.scaleFactor >> grp.sx
        grp.scaleFactor >> grp.sy
        grp.scaleFactor >> grp.sz
        grp.sx.set(l=True, k=False)
        grp.sy.set(l=True, k=False)
        grp.sz.set(l=True, k=False)

        bellCollider = pm.createNode("bellCollider", n=name_collider + "_bellCollider")
        bellColliderParent = bellCollider.getParent()
        bellColliderParent.rename(name_collider + "_bellCollider_transform")
        bellColliderParent.t.lock()
        bellColliderParent.r.lock()
        bellColliderParent.s.lock()
        grp | bellColliderParent

        bell = pm.createNode("transform", n=name_collider + "_bell_transform", p=grp)
        bell.m >> bellCollider.bellMatrix

        for i in range(num_rings):
            ring = pm.createNode("transform", n=name_collider + "_ring_"+str(i+1)+"_transform", p=grp)
            ring.s.set([0.2, 2, 0.2])
            ring.m >> bellCollider.ringMatrix[i]

        curve = pm.createNode("nurbsCurve", n=name_collider + "_bellCollider_curveShape")
        curve.getParent().rename(name_collider + "_bellCollider_curve")
        grp | curve.getParent()
        bellCollider.outputCurve >> curve.create

        bellMesh = pm.createNode("mesh", n=name_collider + "_bellCollider_mesh")
        bellMesh.getParent().rename(name_collider + "_bellCollider_mesh")
        grp | bellMesh.getParent()
        bellCollider.outputBellMesh >> bellMesh.inMesh

        bellCollider.positionCount.set(num_out_joints)
        for i in range(num_out_joints):
            tr = pm.createNode("transform", n=name_collider + "_"+str(i+1)+"_transform", p=grp)
            bellCollider.outputPositions[i] >> tr.t
            bellCollider.outputRotations[i] >> tr.r


        # reset variable
        name_collider = None

    
    def make_collider(self, *args):
        name_rig = self._utility_input_name_collider(self.name_collider)
        num_rings = self._utility_input_number(self.slider_collider)
        num_out_joints = self._utility_input_number(self.slider_out_joints)
        self.bell_collider(name_rig, num_rings, num_out_joints)

        cmds.select(clear=True)

        # Reset variable
        # self.name_collider = None
        # self.slider_collider = None
        # self.slider_out_joints = None 

        # Update UI
        cmds.textField(self.name_collider, e=True, tx="")
        cmds.intSliderGrp(self.slider_collider, e=True, value=1)
        cmds.intSliderGrp(self.slider_out_joints, e=True, value=0)


    # ------------------------------------------------------------------------
    # UI
    # ------------------------------------------------------------------------
    def main(self):
        # Main window
        win_width = 290
        color_label_01 = (0.0, 0.6, 0.7)
        color_label_02 = (0.4, 0.4, 0.4)
        
        main_win = "RBW_Bell_Collider"

        if cmds.window(main_win, exists = 1):
            cmds.deleteUI(main_win, window = 1)

        ww = cmds.window(main_win, title = "RBW Bell Collider", mxb = 0, sizeable=True, resizeToFitChildren=True)

        # Get a pointer and convert it to Qt Widget object
        qw = omui.MQtUtil.findWindow(ww)
        widget = wrapInstance(int(qw), QWidget)

        # Create a QIcon object
        iconpath = os.path.join(self.image_path, "RainbowCGI_icona.ico")

        # Assign the icon
        icon = QIcon(iconpath)
        widget.setWindowIcon(icon)
        
        # Main layout
        col = cmds.columnLayout(co=("both", 5), adjustableColumn=True)
        
        # Carica l'immagine delle brows
        img = os.path.join(self.image_path, "cloth.png")
        cmds.text(h = 10, l = "")
        cmds.iconTextButton(style="iconOnly", image1=img, p=col, w=100)
        cmds.text(h = 10, l = "")
        cmds.setParent("..")

        # BELL COLLIDER SYSTEM ------------------------------------------------
        cmds.frameLayout( label="BELL COLLIDER RIG", bgc=color_label_02, font="boldLabelFont", marginHeight=5)
        
        # Set the name of the rig
        cmds.frameLayout( label="1. Inserisci il nome del rig", marginHeight=10, bgc=color_label_01)
        self.name_collider = cmds.textField('attribute_name_collider')
        cmds.setParent("..")

        # Set the number of rings/colliders
        cmds.frameLayout( label="2. Scegli il numero dei collisori", marginHeight=10, bgc = color_label_01)
        self.slider_collider = cmds.intSliderGrp("num_rings", field=True, label="Collisori:", minValue=1, maxValue=16, value=1, cw=[(1, 50), (2, 40)], cat=[(2, 'both', 10), (3, 'right', 10)])
        cmds.setParent("..")

        # Set the number of out_joints
        cmds.frameLayout( label="3. Scegli il numero di slot dove posizionare i joints", marginHeight=10, bgc = color_label_01)
        self.slider_out_joints = cmds.intSliderGrp("num_out_joints", field=True, label="Count:", minValue=0, maxValue=32, value=0, cw=[(1, 50), (2, 40)], cat=[(2, 'both', 10), (3, 'right', 10)])
        cmds.setParent("..")

        # Build final rig
        self.btn_collider_build = cmds.button(w = win_width, h = 40, l = "| MAKE COLLIDER |", en=True, c = self.make_collider, bgc=color_label_01)

        cmds.setParent("..")
        
        cmds.showWindow(main_win)