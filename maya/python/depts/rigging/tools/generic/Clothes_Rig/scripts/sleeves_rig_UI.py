# -*- coding: utf-8 -*-

import maya.cmds as cmds
import maya.mel as mel
from maya import OpenMayaUI as omui
import math, os, sys
from functools import partial
from imp import reload    

# -----------------------------------------------------------------
# PySide
# -----------------------------------------------------------------
try:
    from shiboken2 import wrapInstance
except ImportError:
    from shiboken import wrapInstance

try:
    from PySide2.QtGui import QIcon
    from PySide2.QtWidgets import QWidget
except ImportError:
    from PySide.QtGui import QIcon, QWidget


class Sleeves:
    """Automatically rig clothes like sleeves."""
    
    def __init__(self):
        # -----------------------------------------------------------------
        # Sleeves variables
        # -----------------------------------------------------------------
        self.sleeves_curves = None
        self.sleeves_locator = None
        self.controllo = None


        # -----------------------------------------------------------------
        # Path
        # -----------------------------------------------------------------
        localpipe = os.getenv("LOCALPY")
        self.image_path = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "Clothes_Rig", "icons")


    def _utility_make_ctrl(self, name):
        """Utility che crea un controllo con gruppo anchor e offset."""
        cmds.select(clear=True)

        ctrl_name = "{}_Ctrl".format(name)
        ctrl = cmds.curve(n=ctrl_name, d=1, k=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16],\
                          p=[(-0.5, 0.5, -0.5), (-0.5, 0.5, 0.5), (0.5, 0.5, 0.5), (0.5, 0.5, -0.5), \
                          (-0.5, 0.5, -0.5), (-0.5, -0.5, -0.5), (-0.5, -0.5, 0.5), (0.5, -0.5, 0.5), \
                          (0.5, 0.5, 0.5), (-0.5, 0.5, 0.5), (-0.5, -0.5, 0.5), (-0.5, -0.5, -0.5), \
                          (0.5, -0.5, -0.5), (0.5, 0.5, -0.5), (0.5, 0.5, 0.5), (0.5, -0.5, 0.5), \
                          (0.5, -0.5, -0.5)])

        for shape in cmds.listRelatives(ctrl, s=True, f=True) or []:
            shapeRenamed = cmds.rename(shape, "{}Shape".format(ctrl_name))

        cmds.setAttr("{}.v".format(ctrl), lock=True, keyable=False, channelBox=False)
        cmds.setAttr("{}Shape.overrideEnabled".format(ctrl), 1)
        cmds.setAttr("{}Shape.overrideColor".format(ctrl), 18)

        grp_anchor = cmds.group(name="{}_anchor_Grp".format(name), empty=True)
        grp_offset = cmds.group(name="{}_offset_Grp".format(name), empty=True)
        
        cmds.parent(ctrl, grp_offset)
        cmds.parent(grp_offset, grp_anchor)
        cmds.select(clear=True)

        return grp_anchor, grp_offset, ctrl

    
    def _utility_input_name_rig(self, name_attribute):
        """Memorizza il nome del rig."""
        string_input = cmds.textField(name_attribute, query=True, text=True)
        
        if not string_input:
            cmds.error("Please write rig name to add.")
        else:
            is_number = string_input[0].isdigit()
            obj_name = "{}_Rig_Grp".format(string_input)
            
            if is_number:
                # check se il nome inizia per un numero
                cmds.error("Please insert a string as the first character of the name, not a number.")
            
            elif cmds.objExists(obj_name):
                # check se esiste già un rig con lo stesso nome
                cmds.error("### Please choose a different name for this rig, '{}' already exists! ###".format(obj_name))
            
            else:
                return string_input

    
    def _utility_input_number_ctrls(self, name_attribute):
        # Retrieve text insert from user
        num_ctrls = cmds.intSliderGrp(name_attribute, query=True, value=True)

        return num_ctrls


    def _utility_create_groups_hierarchy(self, rig_name, joints=False, utility=False, **kwargs):
        """Crea la gerachia di gruppi del rig."""
        main_grp_name = "{}_Rig_Grp".format(rig_name)
        joints_grp_name = "{}_Joints_Grp".format(rig_name)
        ctrls_grp_name = "{}_Controls_Grp".format(rig_name)
        tmp_grp_name = "{}_TMP_Grp".format(rig_name)
        utility_grp_name = "{}_Utility_Grp".format(rig_name)
            
        if not cmds.objExists(main_grp_name):
            main_grp = cmds.group(name=main_grp_name, empty=True)
            ctrls_grp = cmds.group(name=ctrls_grp_name, empty=True)
            cmds.parent(ctrls_grp, main_grp)

            if joints:
                joint_grp = cmds.group(name=joint_grp_name, empty=True)
                cmds.setAttr("{}.visibility".format(joint_grp), 0)
                cmds.parent(joint_grp, main_grp)

            if utility:
                utility_grp = cmds.group(name=utility_grp_name, empty=True)
                cmds.setAttr("{}.visibility".format(utility_grp), 0)
                cmds.parent(utility_grp, main_grp)
            
            if joints and utility:
                return main_grp, ctrls_grp, joints_grp, utility_grp
            elif joints and not utility:
                return main_grp, ctrls_grp, joints_grp
            elif utility and not joints:
                return main_grp, ctrls_grp, utility_grp
            else:
                return main_grp, ctrls_grp

        else:
            cmds.error("Attenzione, esiste già un gruppo '{}', si prega di cambiare il nome del rig.".format(main_grp_name))
            

    def _utility_create_joints_from_list(self, controls, jnt_grp):
        """Crea un joint per locator."""
        lista_joints = []
        
        for ctrl in controls:
            name_jnt = ctrl.replace("Ctrl", "jnt")
            piv = cmds.xform(ctrl, q=True, piv=True, ws=True)
            cmds.select(clear=True)
            jnt = cmds.joint(name=name_jnt)
            mtt = cmds.matchTransform(jnt, ctrl)
            cmds.makeIdentity(jnt, apply=True, t=1, r=1, s=1, n=0)
            cmds.parent(jnt, jnt_grp)
            lista_joints.append(jnt)
            
            pac = cmds.parentConstraint(ctrl, jnt, mo=True, w=1)
            scc = cmds.scaleConstraint(ctrl, jnt, w=1)

            cmds.select(clear=True)

        return lista_joints


    def _utility_create_joints(self, control):
        """Crea un joint per locator."""
        name_jnt = control.replace("Ctrl", "jnt")
        piv = cmds.xform(control, q=True, piv=True, ws=True)
        cmds.select(clear=True)
        jnt = cmds.joint(name=name_jnt)
        cmds.setAttr("{}.visibility".format(jnt), 0)
        mtt = cmds.matchTransform(jnt, control)
        cmds.makeIdentity(jnt, apply=True, t=1, r=1, s=1, n=0)
        cmds.parent(jnt, control)
        cmds.select(clear=True)

        return jnt


    def _utility_create_set(self, name_rig, type_set, obj_list):
        """Utility per creare un set."""

        name_rig_set = "{}_SET".format(name_rig)
        name_set = "{}_{}_SET".format(name_rig, type_set)
        
        if not cmds.objExists(name_rig_set):
            module_set = cmds.sets(empty=True, name=(name_rig_set))
        else:
            module_set = name_rig_set

        if not cmds.objExists(name_set):
            objects_set = cmds.sets(empty=True, name=name_set)
            # Parenta il set del modulo dentro al set globale
            cmds.sets(objects_set, edit=True, forceElement=module_set)
        else:
            objects_set = name_set

        for obj in obj_list:
            cmds.sets(obj, edit=True, forceElement=objects_set)


    # -----------------------------------------------------------------
    # SLEEVES RIG
    # -----------------------------------------------------------------
    def sleeves_slider_callback(self, value, *args):
        """Imposta il valore del slider in modo che sia multiplo di 4"""
        new_value = round(value / 4) * 4
        cmds.intSliderGrp('num_ctrls', e=True, v=new_value)


    def sleeves_select_curve(self, *args):
        """Fa un check se l'oggetto selezionato è una curva e la stora se affermativo."""
        selection = cmds.filterExpand(sm = 9)
        
        if not selection:
            cmds.error("Please select two curves.\n")
        elif len(selection) == 1:
            cmds.error("Please select two curves.\n")
        elif len(selection) == 2:
            self.sleeves_curves = selection
            printed_curves = "{}, {}".format(selection[0], selection[1])
            # print("curves: {}".format(self.sleeves_curves))
            cmds.textField(self.sleeves_txtf_curve, e=1, it=printed_curves)
        else:
            # self.sleeves_curves = None
            cmds.error("Please select two curves.\n")

        cmds.select(cl=True)

        # Update UI
        cmds.textField(self.sleeves_txtf_curve, e=True, tx=printed_curves)
        cmds.button(self.btn_sleeves_select_curve, e=True, en=False)
        cmds.button(self.btn_sleeves_undo_curve, e=True, en=True)
        cmds.button(self.btn_sleeves_locator_01, e=True, en=True)
        cmds.button(self.btn_sleeves_locator_02, e=True, en=True)


    def sleeves_select_curve_undo(self, *args):
        """Resetta la variabile della curva."""
        self.sleeves_curves = None

        if self.sleeves_locator:
            cmds.delete(self.sleeves_locator)
            self.sleeves_locator = None

        cmds.select(clear=True)

        # Update UI
        cmds.textField(self.sleeves_txtf_curve, e=True, tx="")
        cmds.button(self.btn_sleeves_select_curve, e=True, en=True)
        cmds.button(self.btn_sleeves_undo_curve, e=True, en=False)
        cmds.button(self.btn_sleeves_locator_01, e=True, en=False)
        cmds.button(self.btn_sleeves_locator_02, e=True, en=False)
        cmds.button(self.btn_sleeves_build, e=True, en=False)


    def sleeves_create_locator(self, loc, *args):
        """Crea un locator sul cv selezionato."""
        selection = cmds.filterExpand(sm=28)

        if not selection:
            cmds.error("Please select one cv of the curve.")
        else:        
            tmp_cls = cmds.cluster(selection, n="tmp_cluster")
            tmp_loc = cmds.spaceLocator(n=f"tmp_{loc}")[0]
            cmds.matchTransform(tmp_loc, tmp_cls)
            cmds.delete(tmp_cls)
            if loc == 'loc_1':
                self.sleeves_locator_01 = tmp_loc
            elif loc == 'loc_2':
                self.sleeves_locator_02 = tmp_loc
            else:
                cmds.error("### ERROR ###")

        cmds.select(clear=True)

        # Update UI
        cmds.button(self.btn_sleeves_scale, e=True, en=True)
        cmds.button(self.btn_sleeves_build, e=True, en=True)


    def sleeves_get_ctrl_scale(self, *args):
        """Fai lo scale contraint dal controllo selezionato ai locator."""
        selection = cmds.ls(sl=True)[0]

        crv_shape = cmds.listRelatives(selection, shapes=True)[0]
        # print(crv_shape)

        check_type = cmds.objectType(crv_shape)
        # print(check_type)

        if check_type == "nurbsCurve":
            self.controllo = selection
            cmds.select(clear=True)
            
            # Update UI            
            cmds.textField(self.sleeves_txtf_control, e=True, tx=self.controllo)
            cmds.button(self.btn_sleeves_scale, e=True, en=False)
            cmds.button(self.btn_sleeves_scale_undo, e=True, en=True)


    def sleeves_ctrl_scale_undo(self, *args):
        """Resetta la variabile del controllo da cui scalare il rig."""
        self.controllo = None
        cmds.select(clear=True)

        # Update UI
        cmds.textField(self.sleeves_txtf_control, e=True, tx="")
        cmds.button(self.btn_sleeves_scale, e=True, en=True)
        cmds.button(self.btn_sleeves_scale_undo, e=True, en=False)


    def _sleeves_set_seam(self, curves_list, locators_list):
        """Make seam sulla curva in base alla posizione del locator."""
        for crv, loc in zip(curves_list, locators_list):
            print(f"crv: {crv}")
            print(f"loc: {loc}")
            crv_shape = cmds.listRelatives(crv, shapes=True)[0]
            print(f"crv_shape: {crv_shape}")

            npc_node = cmds.createNode("nearestPointOnCurve", name="NPC_temporany_01")
            cmds.connectAttr("{}.worldSpace[0]".format(crv_shape), "{}.inputCurve".format(npc_node), force=True)
            cmds.connectAttr("{}Shape.worldPosition[0]".format(loc), "{}.inPosition".format(npc_node), force=True)

            wsPos = cmds.getAttr("{}.position".format(npc_node))
            uParam = cmds.getAttr("{}.parameter".format(npc_node))

            cmds.select("{}.u[{}]".format(crv, uParam), r=True)

            try:
                mel.eval('moveNurbsCurveSeam')
                cmds.select(clear=True)
            except:
                cmds.warning("Seam is already at this location, no change will be made")
                cmds.select(clear=True)
                            
            try:
                cmds.delete(npc_node)
                cmds.select(clear=True)
            except:
                cmds.select(clear=True)
                print("### ATTENZIONE: non si è riusciti a eliminare il nodo {} ###".format(npc_node))

            cmds.delete(loc)


    def _sleeves_rebuild_curves(self, name_rig, curves_list, num_ctrls, utility_grp):
        """Fai il rebuild delle curve in base al input del numero dei controlli."""
        lista_curve_rebuildate = []
        num_spans = num_ctrls * 2

        for idx, curva in enumerate(curves_list):
            nome_curva = "CRV_{}_{:02d}".format(name_rig, idx+1)
            curva_tmp = cmds.rebuildCurve(curva, ch=True, rpo=True, rt=0, end=0, kr=0, kcp=False, kep=False, kt=False, s=num_spans, d=3, tol=0.01)
            cmds.delete(curva_tmp, ch=True)
            curva_new = cmds.rename(curva_tmp[0], nome_curva)
            lista_curve_rebuildate.append(curva_new)

            cmds.select(clear=True)

        for curva in lista_curve_rebuildate:   
            cmds.parent(curva, utility_grp)
            cmds.setAttr(f"{curva}.visibility", 0)

        cmds.select(clear=True)

        return lista_curve_rebuildate


    def _sleeves_set_seam_fix(self, curves_list):
        """Fix per piazzare il cv[0] nel posto giusto, ora lo shifta di un cv."""
        print("_sleeves_set_seam_fix()")
        lista_locators = []
        for crv in curves_list:
            tmp_cls = cmds.cluster("{}.cv[1:2]".format(crv), n="tmp_cluster")
            locator = cmds.spaceLocator(n="tmp_loc")[0]
            lista_locators.append(locator)
            cmds.matchTransform(locator, tmp_cls)
            cmds.delete(tmp_cls)
            cmds.select(clear=True)
            
        self._sleeves_set_seam(curves_list, lista_locators)


    def _sleeves_build_loft(self, name_rig, curves_list, utility_grp):
        """Fai il loft delle 2 curve"""
        surface = cmds.loft(curves_list[0], curves_list[1], name="NRB_{}_lofted".format(name_rig), ch=1, u=1, c=0, ar=0, d=3, ss=1, rn=0, po=0, rsn=True)[0]
        cmds.reverseSurface(surface, d=3, ch=0, rpo=1)

        cmds.parent(surface, utility_grp)

        # # rename nodo di loft
        # node_list = cmds.listHistory(surface)
        # for node in node_list:
        #     obj_type = cmds.objectType(node)
        #     if obj_type == "loft":
        #         loft_node = cmds.rename(node, "LFT_{}".format(name_rig))

        cmds.delete(surface, ch=True)
        cmds.select(clear=True)

        return surface


    def _fix_nurbs(self, surface_tmp, num_ctrls):
        num_spans = num_ctrls * 4

        nurbs_tmp = cmds.rebuildSurface(surface_tmp, ch=False, rpo=1, rt=0, end=1, kr=0, kcp=0, kc=0, su=0, du=3,  sv=num_spans, dv=3, tol=0.01, fr=0, dir=2)[0]
        u_param = 1 - (1 / num_spans)
        cmds.select(f"{nurbs_tmp}.v[{u_param}]", replace=True)
        mel.eval('MoveSurfaceSeam')
        surface = cmds.rebuildSurface(nurbs_tmp, ch=False, rpo=1, rt=0, end=1, kr=0, kcp=0, kc=0, su=0, du=3,  sv=num_spans/2, dv=3, tol=0.01, fr=0, dir=2)[0]
        cmds.select(clear=True)

        return surface


    def _sleeves_create_uvpin_system(self, name_rig, surface, num_ctrls, grp):
        """Crea un nodo uvpin per surface patch."""
        num_spans = num_ctrls * 2
        surf_shape = cmds.listRelatives(surface, s=True)[0]
        lista_controlli = []
        lista_joints = []
        lista_controlli_totali = []
        lista_locators = []

        # Crea un solo nodo uvpin da usare per questo rig
        uvpin_node = cmds.createNode("uvPin", name="UVP_{}".format(name_rig))
        cmds.connectAttr("{}.worldSpace[0]".format(surf_shape), "{}.deformedGeometry".format(uvpin_node), force=True)
        cmds.connectAttr("{}.local".format(surf_shape), "{}.originalGeometry".format(uvpin_node), force=True)

        # [ ] crea nodi plusMinus e fai connessioni
        counter_ctrl = 0
        for idx in range(num_spans):
            valueV = (1 / float(2 * num_spans)) + (2 * (1 / float(2 * num_spans)) * float(idx))
            cmds.setAttr("{}.coordinate[{}].coordinateU".format(uvpin_node, idx), 0.5)
            cmds.setAttr("{}.coordinate[{}].coordinateV".format(uvpin_node, idx), valueV)

            # se il controllo è visibile
            if idx % 2 != 1: 
                counter_ctrl += 1
                base_name = "{}_{:02d}".format(name_rig, counter_ctrl)

                # crea locator
                loc = cmds.spaceLocator(name="LOC_{}".format(base_name))[0]
                cmds.setAttr("{}Shape.visibility".format(loc), 0)
                cmds.parent(loc, grp)
                lista_locators.append(loc)
            
                # crea controllo
                grp_anchor, grp_offset, ctrl = self._utility_make_ctrl(base_name)
                lista_controlli.append(ctrl)
                lista_controlli_totali.append(ctrl)
                cmds.parent(grp_anchor, loc)

                # crea joint
                jnt = self._utility_create_joints(ctrl)
                lista_joints.append(jnt)

                # connetti locator al nodo uvpin
                cmds.connectAttr("{}.outputMatrix[{}]".format(uvpin_node, idx), "{}.offsetParentMatrix".format(loc), force=True)

            # se il controllo è nascosto
            else:
                base_name = "{}_{:02d}_hidden".format(name_rig, counter_ctrl)

                # crea locator
                loc = cmds.spaceLocator(name="LOC_{}".format(base_name))[0]
                cmds.setAttr("{}Shape.visibility".format(loc), 0)
                cmds.setAttr("{}.visibility".format(loc), 0)
                cmds.parent(loc, grp)
            
                # crea controllo
                grp_anchor, grp_offset, ctrl = self._utility_make_ctrl(base_name)
                lista_controlli_totali.append(ctrl)
                cmds.parent(grp_anchor, loc)

                # crea joint
                jnt = self._utility_create_joints(ctrl)
                lista_joints.append(jnt)

                # connetti locator al nodo uvpin
                cmds.connectAttr("{}.outputMatrix[{}]".format(uvpin_node, idx), "{}.offsetParentMatrix".format(loc), force=True)
                

            cmds.select(clear=True)

        return lista_controlli, lista_joints, lista_controlli_totali, lista_locators


    def _sleeves_create_plusminus_node_system(self, lista_controlli_totali):
        """Crea sistema di nodi plusMinusAverage tra controlli visibili e nascosti."""
        for idx, controllo in enumerate(lista_controlli_totali):
            if idx % 2 == 1:
                # creo nodi e collego al ctrl driven
                nodo_pls_translate = cmds.createNode('plusMinusAverage', name="PLS_{}_translate".format(controllo))
                nodo_pls_rotate = cmds.createNode('plusMinusAverage', name="PLS_{}_rotate".format(controllo))

                # setto nodi su 'sum' per le traslate, 'average' per le rotate
                cmds.setAttr("{}.operation".format(nodo_pls_translate), 3)
                cmds.setAttr("{}.operation".format(nodo_pls_rotate), 3)

                # connessioni
                cmds.connectAttr("{}.output3D".format(nodo_pls_translate), "{}.translate".format(controllo), force=True)
                cmds.connectAttr("{}.output3D".format(nodo_pls_rotate), "{}.rotate".format(controllo), force=True)
                
                if idx == len(lista_controlli_totali)-1:
                    # connessione ctrl precedente
                    cmds.connectAttr("{}.translate".format(lista_controlli_totali[idx-1]), "{}.input3D[0]".format(nodo_pls_translate), force=True)
                    cmds.connectAttr("{}.rotate".format(lista_controlli_totali[idx-1]), "{}.input3D[0]".format(nodo_pls_rotate), force=True)

                    # connessione ctrl successivo
                    cmds.connectAttr("{}.translate".format(lista_controlli_totali[0]), "{}.input3D[1]".format(nodo_pls_translate), force=True)
                    cmds.connectAttr("{}.rotate".format(lista_controlli_totali[0]), "{}.input3D[1]".format(nodo_pls_rotate), force=True)

                else:
                    # connessione ctrl precedente
                    cmds.connectAttr("{}.translate".format(lista_controlli_totali[idx-1]), "{}.input3D[0]".format(nodo_pls_translate), force=True)
                    cmds.connectAttr("{}.rotate".format(lista_controlli_totali[idx-1]), "{}.input3D[0]".format(nodo_pls_rotate), force=True)

                    # connessione ctrl successivo
                    cmds.connectAttr("{}.translate".format(lista_controlli_totali[idx+1]), "{}.input3D[1]".format(nodo_pls_translate), force=True)
                    cmds.connectAttr("{}.rotate".format(lista_controlli_totali[idx+1]), "{}.input3D[1]".format(nodo_pls_rotate), force=True)


    def _sleeves_make_scale_constraint(self, ctrl, lista_locators):
        """Fai lo scale contraint dal controllo selezionato ai locator."""
        for locator in lista_locators:
            cmds.scaleConstraint(ctrl, locator, mo=True, w=1)


    def sleeves_rig_build(self, *args):
        """Costruisci il rig delle sleeves."""
        name_rig = self._utility_input_name_rig("attribute_name_sleeves")
        input_num_ctrls = self._utility_input_number_ctrls("num_ctrls")
        main_grp, ctrls_grp, utility_grp = self._utility_create_groups_hierarchy(name_rig, controls=True, joints=False, utility=True, tmp=False)       
        self._sleeves_set_seam(self.sleeves_curves, (self.sleeves_locator_01, self.sleeves_locator_02))
        lista_curve = self._sleeves_rebuild_curves(name_rig, self.sleeves_curves, input_num_ctrls, utility_grp)
        self._sleeves_set_seam_fix(lista_curve)
        surface_tmp = self._sleeves_build_loft(name_rig, lista_curve, utility_grp)
        surface = self._fix_nurbs(surface_tmp, input_num_ctrls)
        lista_controlli, lista_joints, lista_controlli_totali, lista_locators = self._sleeves_create_uvpin_system(name_rig, surface, input_num_ctrls, ctrls_grp)
        self._utility_create_set(name_rig, "controls", lista_controlli)
        self._utility_create_set(name_rig, "joints", lista_joints)
        self._sleeves_create_plusminus_node_system(lista_controlli_totali)
        if self.controllo:
            self._sleeves_make_scale_constraint(self.controllo, lista_locators)
        
        cmds.select(clear=True)

        # Reset variable
        self.sleeves_curves = None
        self.sleeves_locator = None
        self.controllo = None

        # Update UI
        # cmds.textField('attribute_name_sleeves', e=True, tx="")
        cmds.textField(self.name_rig_sleeves, e=True, tx="")
        cmds.textField(self.sleeves_txtf_curve, e=True, tx="")
        cmds.textField(self.sleeves_txtf_control, e=True, tx="")
        cmds.button(self.btn_sleeves_select_curve, e=True, en=True)
        cmds.button(self.btn_sleeves_undo_curve, e=True, en=False)
        cmds.button(self.btn_sleeves_locator_01, e=True, en=False)
        cmds.button(self.btn_sleeves_locator_02, e=True, en=False)
        cmds.button(self.btn_sleeves_scale, e=True, en=False)
        cmds.button(self.btn_sleeves_scale_undo, e=True, en=False)
        cmds.button(self.btn_sleeves_build, e=True, en=False)
        cmds.intSliderGrp(self.slider_sleeves, e=True, value=4)

        print("### {} have been rigged. ###".format(name_rig))


    # ------------------------------------------------------------------------
    # UI
    # ------------------------------------------------------------------------
    def main(self):
        # Main window
        
        win_width = 290
        color_label_01 = (0.0, 0.3, 0.4)
        color_label_02 = (0.4, 0.4, 0.4)
        
        main_win = "RBW_Clothes_Sleeves_Rig"

        if cmds.window(main_win, exists = 1):
            cmds.deleteUI(main_win, window = 1)

        ww = cmds.window(main_win, title = "RBW Clothes Sleeves Rig", mxb = 0, sizeable=True, resizeToFitChildren=True)

        # Get a pointer and convert it to Qt Widget object
        qw = omui.MQtUtil.findWindow(ww)
        widget = wrapInstance(int(qw), QWidget)

        # Create a QIcon object
        iconpath = os.path.join(self.image_path, "RainbowCGI_icona.ico")

        # Assign the icon
        icon = QIcon(iconpath)
        widget.setWindowIcon(icon)

        # Main layout
        col = cmds.columnLayout(co=("both", 5), adjustableColumn=True)
        
        # Carica l'immagine delle brows
        img = os.path.join(self.image_path, "cloth.png")
        cmds.text(h = 10, l = "")
        cmds.iconTextButton(style="iconOnly", image1=img, p=col, w=100)
        cmds.text(h = 10, l = "")
        cmds.setParent("..")

        # SLEEVES RIG -----------------------------------------------------------
        cmds.frameLayout( label="SLEEVES RIG", bgc=color_label_02, font="boldLabelFont", marginHeight=5)
        
        # Set the name of the rig
        cmds.frameLayout( label="1. Inserisci il nome del rig", marginHeight=10, bgc = color_label_01)
        self.name_rig_sleeves = cmds.textField('attribute_name_sleeves')
        cmds.setParent("..")

        # Set the number of controls
        cmds.frameLayout( label="2. Scegli il numero di controlli", marginHeight=10, bgc = color_label_01)
        self.slider_sleeves = cmds.intSliderGrp("num_ctrls", field=True, label="n. Ctrls:", minValue=4, maxValue=16, value=4, cw=[(1, 50), (2, 40)], cat=[(2, 'both', 10), (3, 'right', 10)], dc=self.sleeves_slider_callback)
        cmds.setParent("..")

        # Define ribbon curves
        cmds.frameLayout(label="3. Seleziona le 2 curve rebuildate e orientate e premi 'Select curves'", marginHeight=10, bgc = color_label_01)
        cmds.rowLayout(numberOfColumns = 2, adjustableColumn = 1)
        self.btn_sleeves_select_curve = cmds.button(w = ((win_width / 2) - 2), h=30, l = "| Select Curve |", c = self.sleeves_select_curve)
        self.btn_sleeves_undo_curve = cmds.button(w = ((win_width / 2) - 2), h=30, l = "| Undo |", c = self.sleeves_select_curve_undo, en = 0)
        cmds.setParent("..")
        self.sleeves_txtf_curve = cmds.textField(w = win_width, h=30, ed = 0, ebg=True, bgc=(0.22, 0.22, 0.22), font="fixedWidthFont")
        cmds.setParent("..")

        # Define start/end of curve
        cmds.frameLayout( label="4. Per ogni curva seleziona un cv per impostare l'inizio della stessa.", marginHeight=10, bgc = color_label_01)
        self.btn_sleeves_locator_01 = cmds.button(w = win_width, h=30, l = "| Create Locator 1 |", en=False, c = partial(self.sleeves_create_locator, 'loc_1'))
        self.btn_sleeves_locator_02 = cmds.button(w = win_width, h=30, l = "| Create Locator 2 |", en=False, c = partial(self.sleeves_create_locator, 'loc_2'))
        cmds.setParent("..")

        # Define the control where this rig will scale
        cmds.frameLayout( label="5. Seleziona il controllo da cui far scalare il rig (opzionale)", marginHeight=10, bgc = color_label_01)
        cmds.rowLayout(numberOfColumns = 2, adjustableColumn = 1)
        self.btn_sleeves_scale = cmds.button(w = ((win_width / 2) - 2), h=30, l = "| Select Control |", en=False, c = self.sleeves_get_ctrl_scale)
        self.btn_sleeves_scale_undo = cmds.button(w = ((win_width / 2) - 2), h=30, l = "| Undo |", c = self.sleeves_ctrl_scale_undo, en = 0)
        cmds.setParent("..")
        self.sleeves_txtf_control = cmds.textField(w = win_width, h=30, ed = 0, ebg=True, bgc=(0.22, 0.22, 0.22), font="fixedWidthFont")
        cmds.setParent("..")

        # Build final rig
        self.btn_sleeves_build = cmds.button(w = win_width, h = 60, l = "| BUILD RIG |", en=False, c = self.sleeves_rig_build, bgc = color_label_01)

        cmds.setParent("..")
        
        cmds.showWindow(main_win)

