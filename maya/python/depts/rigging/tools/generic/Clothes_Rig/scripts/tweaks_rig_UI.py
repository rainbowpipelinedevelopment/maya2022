# -*- coding: utf-8 -*-

import maya.cmds as cmds
import maya.mel as mel
from maya import OpenMayaUI as omui
import math, os, sys
from functools import partial
from imp import reload    


# -----------------------------------------------------------------
# PySide
# -----------------------------------------------------------------
try:
    from shiboken2 import wrapInstance
except ImportError:
    from shiboken import wrapInstance

try:
    from PySide2.QtGui import QIcon
    from PySide2.QtWidgets import QWidget
except ImportError:
    from PySide.QtGui import QIcon, QWidget


class Tweaks:
    """Automatically rig clothes."""
    def __init__(self):
        # -----------------------------------------------------------------
        # Tweaks variables
        # -----------------------------------------------------------------
        self.string_input = None
        self.symmetrize = False
        self.components_updated = []
        self.locators_list = []
        self.msh = None
        self.controllo = None



        # -----------------------------------------------------------------
        # Path
        # -----------------------------------------------------------------
        localpipe = os.getenv("LOCALPY")
        self.image_path = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "Clothes_Rig", "icons")


    # ------------------------------------------------------------------------
    # SET MAYA PREFERENCES
    # ------------------------------------------------------------------------
    def _set_ordered_selection_on(self, *args):
        """Attiva la preferenza di selezione orderedSelection"""
        # assicurati di essere in object mode senza nulla selezionato
        mel.eval("maintainActiveChangeSelectMode lambert1 1;")       
        cmds.select(clear=True)

        # attiva preferenza 
        if (cmds.selectPref(tso=True, q=True)==0):
            cmds.selectPref(tso=True)
        if (cmds.selectPref(tso=True, q=True)==1):
            print("# Preferenza 'Track selection order' attivata. #")

   
    def _set_ordered_selection_off(self, *args):
        """Disattiva la preferenza di selezione orderedSelection"""
        # assicurati di essere in object mode senza nulla selezionato
        mel.eval("maintainActiveChangeSelectMode lambert1 1;")       
        cmds.select(clear=True)

        # disattiva preferenza
        if (cmds.selectPref(tso=True, q=True)==1):
            cmds.selectPref(tso=False)
        if (cmds.selectPref(tso=True, q=True)==0):
            print("# Preferenza 'Track selection order' disattivata. #")

    
    # ------------------------------------------------------------------------
    # NURBS
    # ------------------------------------------------------------------------
    def _utility_create_list_vectors(self, curva_tmp, idx, num_cv):
        """Crea lista di cv della curva_tmp da passare alla funzione che calcola l'angolo tra vettori/edges."""
        cv = cv_pre = "{}.cv[{}]".format(curva_tmp, idx)
        if idx == 0:
            cv_pre = "{}.cv[{}]".format(curva_tmp, num_cv-1)
            cv_post = "{}.cv[{}]".format(curva_tmp, idx+1)
        elif idx == num_cv-1:
            cv_pre = "{}.cv[{}]".format(curva_tmp, idx-1)
            cv_post = "{}.cv[{}]".format(curva_tmp, 0)
        else:
            cv_pre = "{}.cv[{}]".format(curva_tmp, idx-1)
            cv_post = "{}.cv[{}]".format(curva_tmp, idx+1)

        return cv, cv_pre, cv_post


    def _utility_calculate_angle_between_vectors(self, cv, cv_pre, cv_post):
        """Calcola l'angolo tra 2 vettori (in questo caso 2 edges che condividono un cv della curva)."""
        # recuperare posizione dei cv
        pos_cv = cmds.xform(cv, q=True, ws=True, t=True)
        pos_cv_pre = cmds.xform(cv_pre, q=True, ws=True, t=True)
        pos_cv_post = cmds.xform(cv_post, q=True, ws=True, t=True)

        # calcolare i 2 vettori v1 e v2
        v1 = [pos_cv_pre[0] - pos_cv[0], pos_cv_pre[1] - pos_cv[1], pos_cv_pre[2] - pos_cv[2]]
        v2 = [pos_cv_post[0] - pos_cv[0], pos_cv_post[1] - pos_cv[1], pos_cv_post[2] - pos_cv[2]]

        # calcolare magnitudine del vettore e vettore normalizzato
        v1_mag = math.sqrt((v1[0]**2) + (v1[1]**2) + (v1[2]**2))
        v1_norm = [v1[0]/v1_mag, v1[1]/v1_mag, v1[2]/v1_mag]
        
        v2_mag = math.sqrt((v2[0]**2) + (v2[1]**2) + (v2[2]**2))
        v2_norm = [v2[0]/v2_mag, v2[1]/v2_mag, v2[2]/v2_mag]
        
        # calcolare il dot product
        res = v1_norm[0]*v2_norm[0] + v1_norm[1]*v2_norm[1] + v1_norm[2]*v2_norm[2]

        # calcolare angolo
        angle_rad = math.acos(res)
        angle_deg = math.degrees(angle_rad)

        return angle_rad, angle_deg


    def _utility_ordina_lista(self, lista, operation=None):
        """Riordina la lista partendo da un indice dato."""
        lista_ordinata = []
        indici_ordinati = []

        # Trova il valore più basso nella lista
        if operation == "min" or operation == None:
            nuovo_inizio = min(lista)
        elif operation == "max":
            nuovo_inizio = max(lista)

        # Trova l'indice del nuovo_inizio
        indice = lista.index(nuovo_inizio)
        
        # Aggiungi gli elementi dalla posizione indice fino alla fine della lista
        for i, elem in enumerate(lista[indice:]):
            lista_ordinata.append(elem)
            indici_ordinati.append(indice + i)
        
        # Aggiungi gli elementi dalla posizione 0 fino alla posizione precedente all'indice
        for i, elem in enumerate(lista[:indice]):
            lista_ordinata.append(elem)
            indici_ordinati.append(i)
        
        return lista_ordinata, indici_ordinati


    def _utility_get_normal_values(self, component, nurbs_tmp):
        """Calcola le normali della mesh e della nurbs."""
        # da component seleziona le rispettive facce da calcolare le normali
        cmds.select(component, replace=True)
        cmds.ConvertSelectionToFaces()
        sel = cmds.ls(sl=True, fl=True)
        info = cmds.polyInfo(sel, faceNormals=True)[0]
        norm = info.split()
        faces_normals = round(float(norm[2]), 5), round(float(norm[3]), 5), round(float(norm[4]), 5)

        # calcola le normali della nurbs
        tmp_normals = cmds.pointOnSurface(nurbs_tmp, u=0.5, v=0.5, normal=True)
        nurbs_normals = round(tmp_normals[0], 5), round(tmp_normals[1], 5), round(tmp_normals[2], 5)

        return faces_normals, nurbs_normals


    def _utility_check_between_normals(self, faces_normals, nurbs_normals):
        """Confronta le normali della mesh con quelle della nurbs e ritorna True se va fatto il reverseSurface."""
        f_n = faces_normals
        n_n = nurbs_normals

        res_x = f_n[0] * n_n[0]
        res_y = f_n[1] * n_n[1]
        res_z = f_n[2] * n_n[2]

        if res_x >= 0 and (res_y > 0 and res_z > 0):
            return False
        elif res_x > 0 and (res_y > 0 and res_z < 0):
            return False # ??
        elif res_x > 0 and (res_y < 0 and res_z > 0):
            return False
        elif res_x > 0 and (res_y < 0 and res_z < 0):
            return True
        elif res_x == 0 and (res_y < 0 or res_z < 0):
            return True
        elif res_x < -0.00001 and (res_y > 0 and res_z > 0):
            return False
        elif res_x < -0.00001 and (res_y > 0 and res_z < 0):
            return False
        elif res_x < -0.00001 and (res_y < 0 or res_z > 0):
            return True
        elif res_x < -0.00001 and (res_y < 0 or res_z < 0):
            return True
        else:
            return False


    def _utility_get_type_component(self, component):
        """Determina se il component selezionato è un vertice, edge o faccia."""
        cmds.select(component, replace=True)
        if cmds.filterExpand(ex=True, sm=31):
            return "vertex"
        elif cmds.filterExpand(ex=True, sm=32):
            return "edge"
        elif cmds.filterExpand(ex=True, sm=34):
            return "face"
        else:
            return False

        cmds.select(clear=True)


    def _utility_create_nurbs_tmp(self, base_name, type_component):
        """ Crea una NURBS di grado 1, con spans in base al tipo di componente della selezione."""
        # print(f"{type_component}")
        if type_component == "vertex":
            # creare NURBS grado 1, con 2 spans per U e V
            nurbs_tmp = cmds.nurbsPlane(n=f"{base_name}_surface", d=1, u=2, v=2, ax=[0,1,0], ch=False)[0]
            num_cv = 8
            nurbs_cv_list = ["[0][0]", "[0][1]", "[0][2]", "[1][2]", "[2][2]", "[2][1]", "[2][0]", "[1][0]", "[1][1]"]
        elif type_component == "edge":
            # creare NURBS grado 1, con 1 spans per U e 2 per V
            nurbs_tmp = cmds.nurbsPlane(n=f"{base_name}_surface", d=1, u=1, v=2, ax=[0,1,0], ch=False)[0]
            num_cv = 6
            nurbs_cv_list = ["[0][1]", "[0][2]", "[1][2]", "[1][1]", "[1][0]", "[0][0]"]
        elif type_component == "face":
            # creare NURBS grado 1, con 1 span per U e V
            nurbs_tmp = cmds.nurbsPlane(n=f"{base_name}_surface", d=1, u=1, v=1, ax=[0,1,0], ch=False)[0]
            num_cv = 4
            nurbs_cv_list = ["[0][0]", "[0][1]", "[1][1]", "[1][0]"]
        else:
            pass

        return nurbs_tmp, num_cv, nurbs_cv_list


    def _utility_match_nurbs_to_component_selected(self, component, type_component, nurbs_tmp, num_cv, nurbs_cv_list, curva_tmp):
        """Calcola l'angolo tra i vettori dati dai vari cv della curva, così da definizre il cv della curva da cui iniziare a matchare i cv della nurbs."""
        cv_angle_list = []

        for idx in range(num_cv):
            cv, cv_pre, cv_post = self._utility_create_list_vectors(curva_tmp, idx, num_cv)
            angle_rad, angle_deg = self._utility_calculate_angle_between_vectors(cv, cv_pre, cv_post)
            cv_angle_list.append(angle_rad)

        if type_component == "vertex":
            # riordina indici della lista
            angoli_lista_ordinata, curva_indici_ordinati = self._utility_ordina_lista(cv_angle_list, 'min')

            # la nurbs ha 9 cv, la curva 8 ma va calcolato anche il vtx che sta al suo centro
            for cv_curve, cv_nurbs in zip(curva_indici_ordinati, nurbs_cv_list[:-1]):
                cv_pos = cmds.xform("{}.cv[{}]".format(curva_tmp, cv_curve), q=True, ws=True, t=True)
                # match cv nurbs con cv curva
                cmds.xform("{}.cv{}".format(nurbs_tmp, cv_nurbs), ws=True, t=cv_pos)
    
            # match cv nurbs con vtx
            vtx_pos = cmds.xform(component, q=True, ws=True, t=True)
            cmds.xform("{}.cv{}".format(nurbs_tmp, nurbs_cv_list[-1]), ws=True, t=vtx_pos)
    
            # check normals della nurbs confrontandole a un vertice della mesh
            faces_normals, nurbs_normals = self._utility_get_normal_values(component, nurbs_tmp)
            if self._utility_check_between_normals(faces_normals, nurbs_normals):
                cmds.reverseSurface(nurbs_tmp, d=3, ch=False, rpo=1)

            # rebuild nurbs
            nurbs_surf = cmds.rebuildSurface(nurbs_tmp, ch=False, rpo=1, rt=0, end=1, kr=0, kcp=0, kc=0, su=2, du=3, sv=2, dv=3, tol=0.01, fr=0,  dir=2)[0]

        elif type_component == "edge":
            # riordina indici della lista
            angoli_lista_ordinata, curva_indici_ordinati = self._utility_ordina_lista(cv_angle_list, 'max')
    
            # la nurbs ha 6 cv, come la curva
            for cv_curve, cv_nurbs in zip(curva_indici_ordinati, nurbs_cv_list):
                cv_pos = cmds.xform("{}.cv[{}]".format(curva_tmp, cv_curve), q=True, ws=True, t=True)
                # match cv nurbs con cv curva
                cmds.xform("{}.cv{}".format(nurbs_tmp, cv_nurbs), ws=True, t=cv_pos)

            # check normals della nurbs confrontandole a un vertice della mesh
            cmds.select(component, replace=True)
            cmds.ConvertSelectionToVertices()
            vtx = cmds.ls(sl=True, fl=True)[0]
            cmds.select(clear=True)
            faces_normals, nurbs_normals = self._utility_get_normal_values(vtx, nurbs_tmp)
            if self._utility_check_between_normals(faces_normals, nurbs_normals):
                cmds.reverseSurface(nurbs_tmp, d=3, ch=False, rpo=1)

            # rebuild nurbs
            nurbs_surf = cmds.rebuildSurface(nurbs_tmp, ch=False, rpo=1, rt=0, end=1, kr=0, kcp=0, kc=0, su=0, du=3, sv=0, dv=3, tol=0.01, fr=0, dir=2)[0]

        elif type_component == "face":
            # la nurbs ha 4 cv, come la curva
            for cv_curve, cv_nurbs in zip(range(4), nurbs_cv_list):
                cv_pos = cmds.xform("{}.cv[{}]".format(curva_tmp, cv_curve), q=True, ws=True, t=True)
                # match cv nurbs con cv curva
                cmds.xform("{}.cv{}".format(nurbs_tmp, cv_nurbs), ws=True, t=cv_pos)
    
            # check normals della nurbs confrontandole a un vertice della mesh
            cmds.select(component, replace=True)
            cmds.ConvertSelectionToVertices()
            vtx = cmds.ls(sl=True, fl=True)[0]
            cmds.select(clear=True)
            faces_normals, nurbs_normals = self._utility_get_normal_values(vtx, nurbs_tmp)
            if self._utility_check_between_normals(faces_normals, nurbs_normals):
                cmds.reverseSurface(nurbs_tmp, d=3, ch=False, rpo=1)

            # rebuild nurbs
            nurbs_surf = cmds.rebuildSurface(nurbs_tmp, ch=False, rpo=1, rt=0, end=1, kr=0, kcp=0, kc=0, su=1, du=3, sv=1, dv=3, tol=0.01, fr=0,  dir=2)[0]

        return nurbs_surf


    def _create_nurbs(self, selection, rig_name, locators_list, symmetrize):
        """Crea una nurbs partendo da un vertice selezionato e posizionala."""
        cmds.select(clear=True)
        nurbs_list = []    
        
        for counter, (component, locator) in enumerate(zip(selection, locators_list)):
            # seleziona da singolo component a edges perimetrali e converti selezione edge in curva
            cmds.select(component)
            cmds.ConvertSelectionToEdgePerimeter()
            base_name = locator.replace("_loc", "")
            curva_tmp = cmds.polyToCurve(n=f"{base_name}_TMP_curve", form=2, degree=1, conformToSmoothMeshPreview=False)[0]

            # vedi che tipo di componente è la selezione, crea nurbs_tmp e imposta alcune variabili
            type_component = self._utility_get_type_component(component)
            nurbs_tmp, num_cv, nurbs_cv_list = self._utility_create_nurbs_tmp(base_name, type_component)

            # metti nurbs in gerarchia
            cmds.parent(nurbs_tmp, self.nurbs_grp)

            # calcolare l'angolo tra i vettori dati dai vari cv della curva, necessario
            # per definire uno spigolo della curva da cui iniziare a matchare i cv della nurbs
            nurbs_surf = self._utility_match_nurbs_to_component_selected(component, type_component, nurbs_tmp, num_cv, nurbs_cv_list, curva_tmp)
            nurbs_list.append(nurbs_surf)

            # delete curve
            cmds.delete(curva_tmp)

            cmds.select(clear=True)

        return nurbs_list


    def _utility_mirror_nurbs(self, surface):
        """Utility che mirrora la nurbs."""
        
        # definisco nome nurbs da mirrorare
        old_side = surface.split("_")[0]
        if old_side == "L":
            new_name = surface.replace("L_", "R_")
        elif old_side == "R":
            new_name = surface.replace("R_", "L_")
        else:
            return

        # duplico nurbs, la rinomino e la scalo a -1 sulle X
        new_surf = cmds.duplicate(surface, name=new_name)[0]
        cmds.setAttr("{}.scaleX".format(new_surf), -1)
        cmds.makeIdentity(new_surf, apply=True, t =True, r=True, s=True, n=0, pn=1)
        cmds.reverseSurface(new_surf, d=3, ch=False, rpo=1)
       
        return new_surf


    def _symmetrize_nurbs(self, surface_list, lista_locator_pre_mirror):
        """Mirrora la nurbs sull'asse X, basandosi sulla lista dei locator da mirrorare."""
        nurbs_mirrored_list = []

        for surf in surface_list:
            name_loc = surf.replace("_surface", "_loc")
            if name_loc in lista_locator_pre_mirror:
                surf_mirrored = self._utility_mirror_nurbs(surf)
                nurbs_mirrored_list.append(surf_mirrored)

        return nurbs_mirrored_list


    # ------------------------------------------------------------------------
    # BUTTON SET (selezione vertici)
    # ------------------------------------------------------------------------
    def _get_input_simmetrize(self, *args):
        """Check se il rig va mirrorato"""
        if cmds.checkBox(self.is_simmetry, query=True, value=1) == 1:
            self.symmetrize = True
        else:
            self.symmetrize = False

        return self.symmetrize

    
    def _get_info_mesh(self, vertices):
        """ Estrapola mesh dalla selezione. """
        self.msh = vertices[0].split(".")[0]

        return self.msh


    def _utility_input_name_rig(self, name_attribute):
        """Memorizza il nome del rig."""
        string_input = cmds.textField(name_attribute, query=True, text=True)
        
        if not string_input:
            cmds.error("Please write rig name to add.")
        else:
            is_number = string_input[0].isdigit()
            obj_name = "{}_Rig_Grp".format(string_input)
            
            if is_number:
                # check se il nome inizia per un numero
                cmds.error("Please insert a string as the first character of the name, not a number.")
            
            elif cmds.objExists(obj_name):
                # check se esiste già un rig con lo stesso nome
                cmds.error("### Please choose a different name for this rig, '{}' already exists! ###".format(obj_name))
            
            else:
                return string_input


    def _create_groups_hierarchy(self, rig_name):
        """Crea la gerachia di gruppi del rig."""
        tweaks_main_grp_name = "{}_Tweaks_Rig_Grp".format(rig_name)
        tweaks_nurbs_grp_name = "{}_Tweaks_Surfaces_Grp".format(rig_name)
        tweaks_ctrls_grp_name = "{}_Tweaks_Controls_Grp".format(rig_name)
        tweaks_tmp_grp_name = "{}_Tweaks_TMP_Grp".format(rig_name)
        
        self.main_grp = cmds.group(name=tweaks_main_grp_name, empty=True)
        self.nurbs_grp = cmds.group(name=tweaks_nurbs_grp_name, empty=True)
        self.ctrls_grp = cmds.group(name=tweaks_ctrls_grp_name, empty=True)
        self.tmp_grp = cmds.group(name=tweaks_tmp_grp_name, empty=True)

        cmds.parent(self.nurbs_grp, self.main_grp)
        cmds.parent(self.ctrls_grp, self.main_grp)
        cmds.parent(self.tmp_grp, self.main_grp)


    def _cleanup_selection_from_duplicates(self, vertices):
        """Controlla se ci sono vertici che sarebbero duplicati se mirrorati, e nel caso rimuovili dalla lista."""
        vtx_pos_list_tmp = []
        vtx_pos_list = []

        for vtx in vertices:
            vtx_pos = cmds.xform(vtx, q=True, ws=True, t=True)
            vtx_pos_list_tmp.append(vtx_pos)

        for vtx, vtx_pos in zip(vertices, vtx_pos_list_tmp):
            vtx_pos_cleaned = [abs(round(vtx_pos[0], 2)), round(vtx_pos[1], 2), round(vtx_pos[2], 2)]

            # se non è un duplicato inseriscilo nella nuova lista
            if vtx_pos_cleaned not in vtx_pos_list:
                vtx_pos_list.append(vtx_pos_cleaned)
                self.components_updated.append(vtx)


    def _utility_create_locators(self, mesh, rig_name, main_grp, vertex, prefix, index):
        """"""
        cls_tmp = cmds.cluster(vertex)
        grp = cmds.group(name="{}{}_{:02d}_loc_Grp".format(prefix, rig_name, index), empty=True)
        loc = cmds.spaceLocator(name="{}{}_{:02d}_loc".format(prefix, rig_name, index))[0]
        cmds.parent(loc, grp)
        cmds.parent(grp, self.tmp_grp)
        cmds.matchTransform(grp, cls_tmp)
        cmds.delete(cls_tmp)
        # self.locators_list.append(loc)

        # orienta il gruppo/locator alle normali della mesh
        constr = cmds.normalConstraint(mesh, grp, aimVector = (1,0,0),  worldUpType= 0)
        cmds.delete(constr)

        return loc


    def _create_locators(self, mesh, rig_name, main_grp, vertices, symmetrize):
        """Crea un locator per vertice selezionato."""
        side_idx = 0
        center_idx = 0
        side_vtx_list = []
        side_pos_list = []
        center_vtx_list = []

        for idx, vtx in enumerate(vertices):
            if not symmetrize:
                # se non vanno mirrorati, usa l'ordine di selezione
                prefix = ""
                locator = self._utility_create_locators(mesh, rig_name, main_grp, vtx, prefix, idx+1)
                self.locators_list.append(locator)

            else:
                # se vanno mirrorati, crea una lista per i vertici da mirrorare e quelli centrali se presenti
                pos = cmds.xform(vtx, q=True, ws=True, t=True)
                pos_rounded = [round(pos[0], 2), round(pos[1], 2), round(pos[2], 2)]
                
                # definisci il prefix in base alla posizione sull'asse X
                if pos_rounded[0] > 0:
                    side_idx += 1
                    side_vtx_list.append(vtx)
                    locator = self._utility_create_locators(mesh, rig_name, main_grp, vtx, "L_", side_idx)
                    self.locators_list.append(locator)
                elif pos_rounded[0] < 0:
                    side_idx += 1
                    side_vtx_list.append(vtx)
                    locator = self._utility_create_locators(mesh, rig_name, main_grp, vtx, "R_", side_idx)
                    self.locators_list.append(locator)
                else:
                    center_idx += 1
                    center_vtx_list.append(vtx)
                    locator = self._utility_create_locators(mesh, rig_name, main_grp, vtx, "C_", center_idx)
                    self.locators_list.append(locator)


    def tweaks_components_select(self, *args):
        """Controlla se sono stati selezionati dei vertici e crea i locator dove creare i tweaks orientati."""
        
        # check se esiste già un rig con lo stesso nome
        self.string_input = self._utility_input_name_rig("attribute_name_tweaks")
        check_name = "{}_Tweaks_Rig_Grp".format(self.string_input)
        if cmds.objExists(check_name):
            cmds.error("Attenzione, esiste già un gruppo '{}', si prega di cambiare il nome del rig.\n".format(check_name))

        # check se è stato spuntato 'symmetrize'        
        self._get_input_simmetrize()

        # memorizza la selezione dei vertici nell'ordine in cui sono stati selezionati (flag -os)
        self.components = cmds.ls(os=True)
        
        # memorizza info riguardanti la mesh
        self._get_info_mesh(self.components)

        if self.components == None :
            cmds.scrollField(self.scroll_tweaks_select_vtx, e = 1, cl = 1)
            cmds.error("Please select vertices of the tweaks.\n")
        else :
            cmds.scrollField(self.scroll_tweaks_select_vtx, e = 1, cl = 1)
            
            for vtx in self.components :
                vtxNum = vtx.rpartition(".")[2] # <type 'unicode'>
                cmds.scrollField(self.scroll_tweaks_select_vtx, e = 1, it = (str(vtxNum) + " "))
                cmds.button(self.btn_tweaks_select_vtx, edit=True, enable=False)
                cmds.button(self.btn_tweaks_clear_vtx, edit=True, enable=True)
                cmds.button(self.btn_tweaks_build, edit=True, enable=True)

            # crea gerarchia di gruppi del rig e crea i locator con le curve nurbs
            self._create_groups_hierarchy(self.string_input)
            self._cleanup_selection_from_duplicates(self.components)
            self._create_locators(self.msh, self.string_input, self.tmp_grp, self.components_updated, self.symmetrize)
            self.surface_list = self._create_nurbs(self.components_updated, self.string_input, self.locators_list, self.symmetrize)

            cmds.select(clear=True)

        # Update UI
        cmds.button(self.btn_tweaks_scale, e=True, en=True)
        cmds.button(self.btn_tweaks_build, e=True, en=True)


    def tweaks_components_clear(self, *args): 
        """Resetta lista vertici selezionati e elimina gruppi e locator creati."""
        cmds.delete(self.main_grp)

        # Reset variables
        self.msh = None
        self.main_grp = None
        self.nurbs_grp = None
        self.ctrls_grp = None
        self.tmp_grp = None
        self.components[:] = []
        self.components_updated[:] = []
        self.locators_list[:] = []
        self.controllo = None

        # Clear UI
        cmds.scrollField(self.scroll_tweaks_select_vtx, edit=True, clear=True)
        cmds.button(self.btn_tweaks_select_vtx, edit=True, enable=True)
        cmds.button(self.btn_tweaks_clear_vtx, edit=True, enable=False)
        cmds.button(self.btn_tweaks_build, edit=True, enable=False)
        cmds.textField(self.tweaks_txtf_control, e=True, tx="")
        cmds.button(self.btn_tweaks_scale, e=True, en=False)
        cmds.button(self.btn_tweaks_scale_undo, e=True, en=False)

        cmds.select(clear=True)
    

    # ------------------------------------------------------------------------
    # BUTTON SCALE CONTROL
    # ------------------------------------------------------------------------
    def tweaks_get_ctrl_scale(self, *args):
        """Fai lo scale contraint dal controllo selezionato ai locator."""
        selection = cmds.ls(sl=True)[0]

        crv_shape = cmds.listRelatives(selection, shapes=True)[0]
        check_type = cmds.objectType(crv_shape)

        if check_type == "nurbsCurve":
            self.controllo = selection
            cmds.select(clear=True)
            
            # Update UI            
            cmds.textField(self.tweaks_txtf_control, e=True, tx=self.controllo)
            cmds.button(self.btn_tweaks_scale, e=True, en=False)
            cmds.button(self.btn_tweaks_scale_undo, e=True, en=True)


    def tweaks_ctrl_scale_undo(self, *args):
        """Resetta la variabile del controllo da cui scalare il rig."""
        self.controllo = None
        cmds.select(clear=True)

        # Update UI
        cmds.textField(self.tweaks_txtf_control, e=True, tx="")
        cmds.button(self.btn_tweaks_scale, e=True, en=True)
        cmds.button(self.btn_tweaks_scale_undo, e=True, en=False)


    # ------------------------------------------------------------------------
    # BUTTON BUILD RIG
    # ------------------------------------------------------------------------
    def _get_locators_pos_and_rot(self, locators):
        """Prendi e memorizza l'orientamento dei locator."""
        tweaks_locators_pos = []
        tweaks_locators_rot = []

        for loc in locators:
            loc_pos = cmds.xform(loc, q=True, ws=True, t=True)
            loc_rot = cmds.xform(loc, q=True, ws=True, ro=True)
            tweaks_locators_pos.append(loc_pos)
            tweaks_locators_rot.append(loc_rot)

        return tweaks_locators_pos, tweaks_locators_rot

    
    def _utility_mirror_locator(self, locator):
        """Utility che mirrora il locator."""
        # definisco nome locator da mirrorare
        old_side = locator.split("_")[0]
        if old_side == "L":
            new_name = locator.replace("L_", "R_")
        elif old_side == "R":
            new_name = locator.replace("R_", "L_")
        else:
            return

        # duplico locator, lo rinomino e lo scalo a -1 sulle X
        new_loc = cmds.duplicate(locator, name=new_name)[0]
        grp_tmp = cmds.group(name="{}_Grp".format(new_name), empty=True)
        cmds.parent(new_loc, grp_tmp)
        cmds.parent(grp_tmp, self.tmp_grp)
        cmds.setAttr("{}.scaleX".format(grp_tmp), -1)

        return new_loc

    
    def _symmetrize_locators(self, locators):
        """ Mirrora il locator con gruppo sull'asse X, previo check se non ha translateX a zero."""
        side_idx = 0
        center_idx = 0
        locator_mirrored = []
        side_loc_list = []
        center_loc_list = []

        lista_locator_pre_mirror = []

        for loc in locators:
            loc_pos = cmds.xform(loc, q=True, ws=True, t=True)

            pos = abs(round(loc_pos[0], 2))
            if pos != 0:
                # mirrora il locator ma prima crea lista per la numerazione ordinata 
                side_idx += 1
                side_loc_list.append(loc)

                loc_mirrored = self._utility_mirror_locator(loc)
                locator_mirrored.append(loc_mirrored)
                lista_locator_pre_mirror.append(loc)

            else:
                center_idx += 1
                center_loc_list.append(loc)

        return locator_mirrored, lista_locator_pre_mirror


    def _utility_make_ctrl(self, name):
        """Utility che crea un controllo con gruppo anchor e offset."""
        cmds.select(clear=True)

        ctrl_name = "{}_Ctrl".format(name)
        ctrl = cmds.curve(n=ctrl_name, d=1, k=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16],\
                          p=[(-0.5, 0.5, -0.5), (-0.5, 0.5, 0.5), (0.5, 0.5, 0.5), (0.5, 0.5, -0.5), \
                          (-0.5, 0.5, -0.5), (-0.5, -0.5, -0.5), (-0.5, -0.5, 0.5), (0.5, -0.5, 0.5), \
                          (0.5, 0.5, 0.5), (-0.5, 0.5, 0.5), (-0.5, -0.5, 0.5), (-0.5, -0.5, -0.5), \
                          (0.5, -0.5, -0.5), (0.5, 0.5, -0.5), (0.5, 0.5, 0.5), (0.5, -0.5, 0.5), \
                          (0.5, -0.5, -0.5)])

        for shape in cmds.listRelatives(ctrl, s=True, f=True) or []:
            shapeRenamed = cmds.rename(shape, "{}Shape".format(ctrl_name))

        cmds.setAttr("{}.v".format(ctrl), lock=True, keyable=False, channelBox=False)
        cmds.setAttr("{}Shape.overrideEnabled".format(ctrl), 1)
        cmds.setAttr("{}Shape.overrideColor".format(ctrl), 18)

        grp_anchor = cmds.group(name="{}_anchor_Grp".format(name), empty=True)
        grp_offset = cmds.group(name="{}_offset_Grp".format(name), empty=True)
        
        cmds.parent(ctrl, grp_offset)
        cmds.parent(grp_offset, grp_anchor)
        cmds.select(clear=True)

        return grp_anchor, grp_offset, ctrl


    def _utility_create_joints(self, control):
        """Crea un joint per locator."""
        name_jnt = control.replace("Ctrl", "jnt")
        piv = cmds.xform(control, q=True, piv=True, ws=True)
        cmds.select(clear=True)
        jnt = cmds.joint(name=name_jnt)
        cmds.setAttr("{}.visibility".format(jnt), 0)
        mtt = cmds.matchTransform(jnt, control)
        cmds.makeIdentity(jnt, apply=True, t=1, r=1, s=1, n=0)
        cmds.parent(jnt, control)
        cmds.select(clear=True)

        return jnt
    

    def _symmetrize_rig(self, symmetrize): # --> WHAT TO DO?
        """"""
        if not symmetrize:
            pass
        else:
            for loc, surf in zip(locators_mirrored, nurbs_mirrored):
                surf_name = loc.replace("_loc", "surface")

    
    def _reorder_items_in_group(self, grp):
        """Fix order position in group."""
        tmp = cmds.listRelatives(grp)
        tmp.sort()
        cmds.parent(tmp, world=True)
        cmds.parent(tmp, grp)
        cmds.select(clear=True)


    def _create_uvpin_system(self, surface_list, grp):
        """Crea un nodo uvpin per surface patch."""
        # num_spans = num_ctrls * 2
        lista_controlli = []
        lista_gruppi_ctrl = []
        lista_joints = []
        lista_locators = []

        # Crea un nodo uvpin per nurbs
        for surface in surface_list:
            surf_shape = cmds.listRelatives(surface, s=True)[0]
            uvpin_node = cmds.createNode("uvPin", name="UVP_{}".format(surface.replace("_surface", "")))
            cmds.connectAttr("{}.worldSpace[0]".format(surf_shape), "{}.deformedGeometry".format(uvpin_node), force=True)
            cmds.connectAttr("{}.local".format(surf_shape), "{}.originalGeometry".format(uvpin_node), force=True)

            cmds.setAttr("{}.coordinate[0].coordinateU".format(uvpin_node), 0.5)
            cmds.setAttr("{}.coordinate[0].coordinateV".format(uvpin_node), 0.5)

            # crea locator
            loc = cmds.spaceLocator(name="LOC_{}".format(surface.replace("_surface", "")))[0]
            cmds.setAttr("{}Shape.visibility".format(loc), 0)
            cmds.parent(loc, grp)
            lista_locators.append(loc)
        
            # crea controllo
            grp_anchor, grp_offset, ctrl = self._utility_make_ctrl(surface.replace("_surface", ""))
            lista_controlli.append(ctrl)
            lista_gruppi_ctrl.append(grp_anchor)
            cmds.parent(grp_anchor, loc)

            # crea joint
            jnt = self._utility_create_joints(ctrl)
            lista_joints.append(jnt)

            # connetti locator al nodo uvpin
            cmds.connectAttr("{}.outputMatrix[0]".format(uvpin_node), "{}.offsetParentMatrix".format(loc), force=True)

            cmds.select(clear=True)

            print(f"{surface}")
            print(f"{loc}")
            print(f"{ctrl}")

        return lista_controlli, lista_gruppi_ctrl, lista_joints, lista_locators


    def _orient_controls_locators(self, lista_locators_uvpin, locators_list_tmp, gruppi_controlli, mirror):
        """Orienta i locator dei controllli in base alla rotazione dei locator di costruzione."""
        print(f"lista_loc: {lista_locators_uvpin}")
        print(f"lista_tmp: {locators_list_tmp}")

        for loc_uvpin, loc_tmp, grp in zip(lista_locators_uvpin, locators_list_tmp, gruppi_controlli):
            if mirror and loc_tmp in mirror:
                cmds.matchTransform(loc_uvpin, loc_tmp, rot=True)
                cmds.setAttr("{}.rotateZ".format(grp), 180)
            else:    
                rot = cmds.xform(loc_tmp, q=True, ws=True, ro=True)
                cmds.xform(loc_uvpin, ws=True, ro=rot)


    def _delete_locators(self, tmp_list):
        """Elimina locators temporanei."""
        cmds.delete(tmp_list)
        cmds.select(clear=True)


    def _make_scale_constraint(self, ctrl, lista_locators):
        """Fai lo scale contraint dal controllo selezionato ai locator."""
        for locator in lista_locators:
            cmds.scaleConstraint(ctrl, locator, mo=True, w=1)


    def _utility_create_set(self, name_rig, type_set, obj_list):
        """Utility per creare un set."""

        name_rig_set = "{}_Tweaks_SET".format(name_rig)
        name_set = "{}_Tweaks_{}_SET".format(name_rig, type_set)
        
        if not cmds.objExists(name_rig_set):
            module_set = cmds.sets(empty=True, name=(name_rig_set))
        else:
            module_set = name_rig_set

        if not cmds.objExists(name_set):
            objects_set = cmds.sets(empty=True, name=name_set)
            # Parenta il set del modulo dentro al set globale
            cmds.sets(objects_set, edit=True, forceElement=module_set)
        else:
            objects_set = name_set

        for obj in obj_list:
            cmds.sets(obj, edit=True, forceElement=objects_set)


    def tweaks_rig_build(self, *args):
        """ Build the tweaks rig. """

        # # Step 1: memorizza trasformate locator da assegnare ai gruppi offset dei controlli e li mirrora
        # locators_pos, locators_rot = self._get_locators_pos_and_rot(self.locators_list)
        # Step 2: mirrora se l'opzione è stata attivata
        if self.symmetrize:
            locators_mirrored, lista_locator_pre_mirror = self._symmetrize_locators(self.locators_list)
            print(f"lista_locator_pre_mirror: {lista_locator_pre_mirror}")
            print(f"locators_mirrored: {locators_mirrored}")
            surface_mirrored = self._symmetrize_nurbs(self.surface_list, lista_locator_pre_mirror)
        # Step 3: creo sistema uvPin su nurbs selezionata con locator, controllo e joint
        if self.symmetrize:
            locators_list_tmp = self.locators_list + locators_mirrored
            surface_list = self.surface_list + surface_mirrored
        else: 
            locators_list_tmp = self.locators_list
            surface_list = self.surface_list
        lista_controlli, lista_gruppi_ctrl, lista_joints, lista_locators_uvpin = self._create_uvpin_system(surface_list, self.ctrls_grp)
        # Step 4: orienta i controlli in base ai rispettivi locator
        if self.symmetrize:
            self._orient_controls_locators(lista_locators_uvpin, locators_list_tmp, lista_gruppi_ctrl, mirror=locators_mirrored)
        else:
            self._orient_controls_locators(lista_locators_uvpin, locators_list_tmp, lista_gruppi_ctrl, mirror=None)
        # Step 5: Cancello i locator di costruzione
        self._delete_locators(self.tmp_grp)
        # Step 6: riodina elementi nei gruppi
        self._reorder_items_in_group(self.ctrls_grp)
        self._reorder_items_in_group(self.nurbs_grp)
        # Step 7: crea sets per joints e controlli
        self._utility_create_set(self.string_input, "controls", lista_controlli)
        self._utility_create_set(self.string_input, "joints", lista_joints)
        # Step 8: se è stato settato, fai scale contraint tra il controllo selezionato e i locator
        if self.controllo:
            self._make_scale_constraint(self.controllo, lista_locators_uvpin)

        print("### {} Tweaks Rig have been successfully rigged. ###".format(self.string_input))

        # Reset variables
        self.symmetrize = False
        self.msh = None
        self.string_input = None
        self.controllo = None

        self.components[:] = []
        self.components_updated[:] = []
        self.locators_list[:] = []
        self.surface_list[:] = []
        
        self.string_input = None
        self.symmetrize = False
        self.components_updated[:] = []
        self.locators_list[:] = []

        self.main_grp = None
        self.nurbs_grp = None
        self.ctrls_grp = None
        self.tmp_grp = None
    

        # Clear UI
        cmds.textField('attribute_name_tweaks', edit=True, text="")
        cmds.checkBox(self.is_simmetry, edit=True, value=0)
        cmds.scrollField(self.scroll_tweaks_select_vtx, edit=True, clear=True)
        cmds.button(self.btn_tweaks_select_vtx, edit=True, enable=True)
        cmds.button(self.btn_tweaks_clear_vtx, edit=True, enable=False)
        cmds.button(self.btn_tweaks_build, edit=True, enable=False)

        cmds.select(clear=True)


    # ------------------------------------------------------------------------
    # UI
    # ------------------------------------------------------------------------
    def main(self):
        # Main window
        
        win_width = 290
        color_label_01 = (0.0, 0.4, 0.5)
        color_label_02 = (0.4, 0.4, 0.4)
        
        main_win = "RBW_Clothes_Tweaks_Rig"

        if cmds.window(main_win, exists = 1):
            cmds.deleteUI(main_win, window = 1)

        ww = cmds.window(main_win, title = "RBW Clothes Tweaks Rig", mxb = 0, sizeable=True, resizeToFitChildren=True, closeCommand=self._set_ordered_selection_off)

        # Get a pointer and convert it to Qt Widget object
        qw = omui.MQtUtil.findWindow(ww)
        widget = wrapInstance(int(qw), QWidget)

        # Create a QIcon object
        iconpath = os.path.join(self.image_path, "RainbowCGI_icona.ico")

        # Assign the icon
        icon = QIcon(iconpath)
        widget.setWindowIcon(icon)

        # Main layout
        col = cmds.columnLayout(co=("both", 5), adjustableColumn=True)
        
        # Carica l'immagine delle brows
        img = os.path.join(self.image_path, "cloth.png")
        cmds.text(h = 10, l = "")
        cmds.iconTextButton(style="iconOnly", image1=img, p=col, w=100)
        cmds.text(h = 10, l = "")
        cmds.setParent("..")

        # ATTIVA Track Selection Order, che verrà disattivato alla chiusura della UI
        self._set_ordered_selection_on()

        # TWEAKS RIG -----------------------------------------------------------
        cmds.frameLayout(label='TWEAKS RIG', lw=60, bgc=color_label_02, font="boldLabelFont", marginHeight=5)
        
        # set the name
        cmds.frameLayout( label="1. Inserisci il nome del rig", marginHeight=10, bgc = color_label_01)

        # allow/disallow simmetry
        cmds.rowLayout(numberOfColumns = 2, adjustableColumn=1)
        name_rig_tweaks = cmds.textField('attribute_name_tweaks')
        self.is_simmetry =  cmds.checkBox(height=30, label="Symmetrize?", value=0)

        cmds.setParent("..")
        cmds.setParent("..")

        # List vertices selected
        cmds.frameLayout( label="2. Seleziona i vertici e premi 'Set''", marginHeight=10, bgc = color_label_01)
        cmds.rowLayout(numberOfColumns = 2, adjustableColumn = 1)
        self.btn_tweaks_select_vtx = cmds.button(w = ((win_width / 2) - 2), l = "Set", c = self.tweaks_components_select)
        self.btn_tweaks_clear_vtx = cmds.button(w = ((win_width / 2) - 2), l = "Clear", c = self.tweaks_components_clear, enable=False)
        cmds.setParent("..")
        self.scroll_tweaks_select_vtx = cmds.scrollField(w = win_width, h = 35, wordWrap = 1, ed = 0, enable=True)
        cmds.setParent("..")

        # Define the control where this rig will scale
        cmds.frameLayout( label="3. Seleziona il controllo da cui far scalare il rig (opzionale)", marginHeight=10, bgc = color_label_01)
        cmds.rowLayout(numberOfColumns = 2, adjustableColumn = 1)
        self.btn_tweaks_scale = cmds.button(w = ((win_width / 2) - 2), h=30, l = "| Select Control |", en=False, c = self.tweaks_get_ctrl_scale)
        self.btn_tweaks_scale_undo = cmds.button(w = ((win_width / 2) - 2), h=30, l = "| Undo |", c = self.tweaks_ctrl_scale_undo, en = 0)
        cmds.setParent("..")
        self.tweaks_txtf_control = cmds.textField(w = win_width, h=30, ed = 0, ebg=True, bgc=(0.22, 0.22, 0.22), font="fixedWidthFont")
        cmds.setParent("..")

        # Build final rig
        self.btn_tweaks_build = cmds.button(w=win_width, h = 40, l="BUILD", c=self.tweaks_rig_build, bgc = color_label_01, en=False)
        cmds.setParent("..")
        
        
        cmds.showWindow(main_win)
