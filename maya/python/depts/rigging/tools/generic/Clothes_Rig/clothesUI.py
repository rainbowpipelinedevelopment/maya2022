# -*- coding: utf-8 -*-

import maya.cmds as cmds
from maya import OpenMayaUI as omui
import os
from imp import reload

# -----------------------------------------------------------------
# Path
# -----------------------------------------------------------------
localpipe = os.getenv("LOCALPY")
myPath = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "Clothes_Rig")
image_path = os.path.join(myPath, "icons")


# -----------------------------------------------------------------
# PySide
# -----------------------------------------------------------------
try:
    from shiboken2 import wrapInstance
except ImportError:
    from shiboken import wrapInstance

try:
    from PySide2.QtGui import QIcon
    from PySide2.QtWidgets import QWidget
except ImportError:
    from PySide.QtGui import QIcon, QWidget


# -----------------------------------------------------------------
# Funzioni lanciate dalla UI
# -----------------------------------------------------------------
def sleeves_button_push(*args):
    import depts.rigging.tools.generic.Clothes_Rig.scripts.sleeves_rig_UI as sleeves_rig_UI
    reload(sleeves_rig_UI)
    sleevesRig = sleeves_rig_UI.Sleeves()
    sleevesRig.main()

def tweaks_button_push(*args):
    import depts.rigging.tools.generic.Clothes_Rig.scripts.tweaks_rig_UI as tweaks_rig_UI
    reload(tweaks_rig_UI)
    tweaksRig = tweaks_rig_UI.Tweaks()
    tweaksRig.main()

def cloth_ribbon_button_push(*args):
    import depts.rigging.tools.generic.Clothes_Rig.scripts.cloth_ribbon_rig_UI as cloth_ribbon_rig_UI
    reload(cloth_ribbon_rig_UI)
    clothRig = cloth_ribbon_rig_UI.ClothRibbon()
    clothRig.main()

def bell_collider_button_push(*args):
    import depts.rigging.tools.generic.Clothes_Rig.scripts.bell_collider_UI as bell_collider_UI
    reload(bell_collider_UI)
    bellColliderRig = bell_collider_UI.BellCollider()
    bellColliderRig.main()

# UTILITIES
def getinfoskincluster_button_push(*args):
    import depts.rigging.tools.generic.Clothes_Rig.scripts.get_info_skincluster as get_info_skincluster
    reload(get_info_skincluster)
    get_info_skincluster.influenceJnts()

def nurbs_copyskinweights_button_push(*args):
    import depts.rigging.tools.generic.Clothes_Rig.scripts.nurbs_copyskinweights as nurbs_copyskin
    reload(nurbs_copyskin)
    nurbs_copyskin.copySkinWeights_nurbs()

def nurbs_ribbon_button_push(*args):
    import depts.rigging.tools.generic.Clothes_Rig.scripts.nurbs_ribbon_UI as nurbs_ribbon_UI
    reload(nurbs_ribbon_UI)
    nurbsRibbon = nurbs_ribbon_UI.NurbsRibbon()
    nurbsRibbon.main()

def bind_prematrix_connector_button_push(*args):
    import depts.rigging.tools.generic.Clothes_Rig.scripts.bind_prematrix_connector as bind_prematrix_connector
    reload(bind_prematrix_connector)
    bind_prematrix_connector.bind_prematrix_connector()

def link_wiki_button_push(*args):
    import webbrowser
    pathToHelp="https://sites.google.com/rbw-cgi.it/rbw-rigging-team/home"
    webbrowser.open(pathToHelp) 

# -----------------------------------------------------------------
# UI
# -----------------------------------------------------------------
def main():
    winWidth = 280
    
    main_window = "RBW_Clothes_Module"
    title_window = "RBW Clothes Module"

    if cmds.window(main_window, exists = 1):
        cmds.deleteUI(main_window, window = 1)

    ww = cmds.window(main_window, title=title_window, mxb=False, sizeable=True, resizeToFitChildren=True, width=250, height=50)

    # Get a pointer and convert it to Qt Widget object
    qw = omui.MQtUtil.findWindow(ww)
    widget = wrapInstance(int(qw), QWidget)

    # Create a QIcon object
    iconpath = os.path.join(image_path, "RainbowCGI_icona.ico")

    # Assign the icon
    icon = QIcon(iconpath)
    widget.setWindowIcon(icon)

    # Main layout
    col = cmds.columnLayout(adjustableColumn=True, bgc=(0.225, 0.225, 0.225))
    
    # Carica l'immagine degli eyesockets
    img = os.path.join(image_path, "cloth.png")
    cmds.text(h = 10, l = "")
    cmds.iconTextButton(style="iconOnly", image1=img, p=col, w=100)
    cmds.text(h = 10, l = "")
    cmds.setParent("..")

    # FIELDS
    # Sleeves
    cmds.frameLayout( label="Clothes Module", marginHeight=10, p=col, bgc=(0.0, 0.2, 0.3))
    cmds.columnLayout(adj=True, co=("both", 6), rowSpacing=7, columnWidth=250)
    btn_sleeves_module = cmds.button(w=winWidth, h=40, l="| Sleeves Module |", c=sleeves_button_push, bgc=(0.0, 0.3, 0.4), annotation="Crea il sistema di rigging per maniche, gonne, colletti, etc. usando nurbs e uvpin.")
    # Tweaks
    btn_tweaks_module = cmds.button(w=winWidth, h=40, l="| Tweaks Module |", c=tweaks_button_push, bgc = (0.0, 0.4, 0.5), annotation="Crea il sistema di rigging per tweaks semplici, usando nurbs e uvpin.")
    # Cloth
    btn_cloth_ribbon_module = cmds.button(w=winWidth, h=40, l="| Cloth Ribbon Module |", c=cloth_ribbon_button_push, bgc=(0.0, 0.5, 0.6), annotation="Crea il sistema di rig con ribbon.")
    # Collider
    btn_collider_module = cmds.button(w=winWidth, h=40, l="| Collider Module |", c=bell_collider_button_push, bgc=(0.0, 0.6, 0.7), annotation="Crea il sistema di collider.")
    cmds.setParent("..")

    # Utilities
    cmds.frameLayout(label="Utilities", collapsable=True, collapse=True, p=col, bgc=(0.0, 0.15, 0.25))
    cmds.columnLayout(adj=True, co=("both", 6), rowSpacing=7, columnWidth=250)
    cmds.text(l="", h=1)     
    btn_nurbs_ribbon = cmds.button(w=winWidth, h=30, l="| Create Nurbs Ribbon |", c=nurbs_ribbon_button_push, bgc=(0.7, 0.7, 0.53), annotation="Tool per creare una NurbsRibbon pura e dura.")
    btn_getinfoskincluster = cmds.button(w=winWidth, h=30, l="| Get Info SkinWeights |", c=getinfoskincluster_button_push, bgc=(0.7, 0.7, 0.53), annotation="Seleziona i joint che influenzano l'oggetto selezionato. Selezionare prima l'oggetto con lo skinCluster e lanciare lo script.")
    btn_copyskinweight_nurbs = cmds.button(w=winWidth, h=30, l="| Nurbs CopySkinWeights |", c=nurbs_copyskinweights_button_push, bgc=(0.7, 0.7, 0.53), annotation="CopySkinWieights ottimizzato per le nurbs. Selezionare prima la Mesh, poi le Nurbs e lanciare lo script.")
    btn_bind_prematrix_connector = cmds.button(w=winWidth, h=30, l="| Bind PreMatrix Connector |", c=bind_prematrix_connector_button_push, bgc=(0.7, 0.7, 0.53), annotation="Tool per il BindPreMatrix. Selezionare l'oggetto che va in doppia (mesh, nurbs, curve) e lanciare lo script.")
    cmds.text(l="", h=1)     
    cmds.setParent("..")

    # WIKI
    cmds.frameLayout(label="WIKI", labelVisible=False, p=col, bgc=(0.4, 0.4, 0.4))
    col_wiki = cmds.columnLayout(adj=True, co=("both", 6), rowSpacing=7, columnWidth=250)
    cmds.text(l="", h=1)     
    btn_wiki = cmds.button(w=winWidth, h=30, l="| WIKI |", p=col_wiki, c=link_wiki_button_push, bgc=(0.2, 0.2, 0.2), annotation="Hai qualche dubbio? Vai alla Wiki di rigging!")
    cmds.text(l="", h=1)     
    cmds.setParent("..")
    
    cmds.showWindow(main_window)
