//Maya ASCII 2022 scene
//Name: default_vehicles.ma
//Last modified: Wed, Mar 27, 2024 11:56:50 AM
//Codeset: 1252
requires maya "2022";
requires -nodeType "renderSetup" "renderSetup.py" "1.0";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t pal;
fileInfo "application" "maya";
fileInfo "product" "Maya 2022";
fileInfo "version" "2022";
fileInfo "cutIdentifier" "202303271415-baa69b5798";
fileInfo "osv" "Windows 10 Pro v2009 (Build: 19045)";
fileInfo "UUID" "B4E6740E-45A4-0F7B-9D8F-FD92E2D62EAD";
createNode transform -s -n "persp";
	rename -uid "559E3B24-4075-D934-A24E-E1913093E1CD";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 323.64133655282296 253.50650224117231 406.19127331779083 ;
	setAttr ".r" -type "double3" -23.738352729311636 41.399999999999288 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "1A7328A9-4FD4-F7AB-65DD-86BA702120EE";
	setAttr -k off ".v";
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 614.23990295259114;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "4EB53D1C-403F-9AE9-F793-1A93E30EFDAD";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "4A82131B-45F3-FAF6-C404-129F79A7753C";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "146AFB5B-42D2-C774-6E87-EC83B9688921";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "D78498C2-4A40-9C43-B0E9-FDBBF679406F";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "77B31D95-4EE2-80B6-D89E-F6986CAB1EE0";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "65771971-4913-F53D-1BCD-798CA88F3C64";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "CTRL_set";
	rename -uid "3596A988-4078-3AFF-7EA7-E6AB68AD2971";
	addAttr -ci true -sn "Rattr" -ln "Rattr" -nn "reference" -dv 1 -min 0 -max 1 -at "bool";
	setAttr ".ctor" -type "string" "1493386226r";
	setAttr -cb on ".Rattr" no;
createNode locator -n "CTRL_setShape" -p "CTRL_set";
	rename -uid "9E5C2A23-4F22-70F4-5C57-589E59C79FB4";
	addAttr -ci true -sn "subC" -ln "subC" -nn "subControl" -dv 1 -min 0 -max 1 -at "bool";
	setAttr -k off ".v";
	setAttr -cb on ".subC" no;
createNode transform -n "CTRL_setsubControl" -p "CTRL_set";
	rename -uid "0FF5DA02-4157-9C0B-CDB7-58B5B35F56E7";
createNode locator -n "CTRL_setsubControlShape" -p "CTRL_setsubControl";
	rename -uid "AE66D1B4-4B01-ECF0-C9E5-0099DD894F44";
	setAttr -k off ".v";
createNode transform -n "GRP_Name_rig" -p "CTRL_setsubControl";
	rename -uid "882F6247-4BD1-0C93-117A-049512455AA6";
createNode transform -n "GRP_Controls" -p "GRP_Name_rig";
	rename -uid "B11037C8-4263-061B-C875-6FBB896FFB78";
createNode transform -n "GRP_constraint" -p "GRP_Controls";
	rename -uid "69F907B2-47AB-58AE-6CE8-37AC04320966";
	setAttr ".rp" -type "double3" 0 -2.4651903288156619e-32 4.4408920985006262e-16 ;
	setAttr ".sp" -type "double3" 0 -2.4651903288156619e-32 4.4408920985006262e-16 ;
createNode transform -n "Transform_Ctrl" -p "GRP_constraint";
	rename -uid "57087E12-4C7D-7D72-D8D9-4CA8080A9682";
	addAttr -ci true -k true -sn "ms" -ln "master_scale" -nn "Master Scale" -dv 1 -min 
		0 -max 10 -at "float";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr -k on ".ms";
createNode transform -n "GRP_main" -p "Transform_Ctrl";
	rename -uid "ED55A45F-4EBE-3BDC-F19A-5DB619509D68";
	setAttr ".rp" -type "double3" 0 -2.4651903288156619e-32 4.4408920985006262e-16 ;
	setAttr ".sp" -type "double3" 0 -2.4651903288156619e-32 4.4408920985006262e-16 ;
createNode transform -n "Main_Ctrl" -p "GRP_main";
	rename -uid "B1AF45FB-41B4-33EB-5A14-C4B7772E4B54";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "main_null" -p "Main_Ctrl";
	rename -uid "ADDC929E-4102-41AB-851D-FB9F33298EEE";
createNode transform -n "GRP_locDrive" -p "main_null";
	rename -uid "D49EC4FF-4C90-653A-D72E-CF950C6556AB";
createNode transform -n "LOC_drive" -p "GRP_locDrive";
	rename -uid "1A7AF749-40E0-3438-A4EB-49B0DF716D19";
createNode locator -n "LOC_driveShape" -p "LOC_drive";
	rename -uid "9C8D1A0B-4329-FE04-AFE4-1EA77721E3F2";
	setAttr -k off ".v";
createNode transform -n "CNT_drive" -p "main_null";
	rename -uid "987AD43C-4766-A26A-FAAB-A2A5C0CE0C2C";
	addAttr -ci true -sn "steering" -ln "steering" -at "double";
	addAttr -ci true -sn "frontWheelSpin" -ln "frontWheelSpin" -at "double";
	addAttr -ci true -sn "rearWheelSpin" -ln "rearWheelSpin" -at "double";
	addAttr -ci true -sn "front_wheel_auto_spin" -ln "front_wheel_auto_spin" -min 0 
		-max 1 -en "off:on" -at "enum";
	addAttr -ci true -sn "rear_wheel_auto_spin" -ln "rear_wheel_auto_spin" -min 0 -max 
		1 -en "off:on" -at "enum";
	addAttr -ci true -sn "wheels_controls_vis" -ln "wheels_controls_vis" -min 0 -max 
		1 -en "off:on" -at "enum";
	addAttr -ci true -sn "bank" -ln "bank" -at "double";
	addAttr -ci true -sn "RIG" -ln "RIG" -min 0 -max 0 -en "ADJUST" -at "enum";
	addAttr -ci true -sn "steerDrive" -ln "steerDrive" -at "double";
	addAttr -ci true -sn "wheelDrive" -ln "wheelDrive" -at "double";
	addAttr -ci true -sn "widthAdjust" -ln "widthAdjust" -at "double";
	addAttr -ci true -sn "lengthAdjust" -ln "lengthAdjust" -at "double";
	addAttr -ci true -sn "steerRadius" -ln "steerRadius" -at "double";
	addAttr -ci true -sn "wheelRadius" -ln "wheelRadius" -at "double";
	addAttr -ci true -sn "pivot_drive_adjust" -ln "pivot_drive_adjust" -at "double";
	addAttr -ci true -sn "bank_pivot_adjust" -ln "bank_pivot_adjust" -at "double";
	addAttr -ci true -sn "bank_pivot_adjust_vis" -ln "bank_pivot_adjust_vis" -min 0 
		-max 1 -en "off:on" -at "enum";
	addAttr -ci true -sn "wheels_latticeSize_adjust" -ln "wheels_latticeSize_adjust" 
		-at "double";
	addAttr -ci true -sn "wheels_lattice_vis" -ln "wheels_lattice_vis" -min 0 -max 1 
		-en "off:on" -at "enum";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -l on ".rptx";
	setAttr -l on ".rpty";
	setAttr -l on ".rptz";
	setAttr -k on ".steering";
	setAttr -k on ".frontWheelSpin";
	setAttr -k on ".rearWheelSpin";
	setAttr -k on ".front_wheel_auto_spin" 1;
	setAttr -k on ".rear_wheel_auto_spin" 1;
	setAttr -k on ".wheels_controls_vis" 1;
	setAttr -k on ".bank";
	setAttr -l on -k on ".RIG";
	setAttr -k on ".steerDrive";
	setAttr -k on ".wheelDrive";
	setAttr -k on ".widthAdjust" 20;
	setAttr -k on ".lengthAdjust";
	setAttr -k on ".steerRadius" 35;
	setAttr -k on ".wheelRadius" 35;
	setAttr -k on ".pivot_drive_adjust";
	setAttr -k on ".bank_pivot_adjust";
	setAttr -k on ".bank_pivot_adjust_vis" 1;
	setAttr -k on ".wheels_latticeSize_adjust" 1;
	setAttr -k on ".wheels_lattice_vis" 1;
createNode nurbsCurve -n "CNT_driveShape" -p "CNT_drive";
	rename -uid "04F16D5E-4626-568D-7142-2CB558BA5CB8";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 0.5810833 1 0 ;
	setAttr ".cc" -type "nurbsCurve" 
		2 14 0 no 3
		17 0 0 1 2 2.1000000000000001 4 4.0999999999999996 6 6.1000000000000005 8 8.0999999999999996
		 10 10.1 12 12.1 14 14
		16
		-15.034146011481386 19.722817656849827 231.94266599444012
		-15.034146011481386 19.722817656849827 231.94266599444012
		-56.894789562214982 19.722817656849827 231.94266599444012
		-56.894789562214982 19.722817656849827 231.94266599444012
		0.5350434685353086 19.722817656849827 331.53707215145113
		0.5350434685353086 19.722817656849827 331.53707215145113
		56.894789562214982 19.722817656849827 231.94266599444012
		56.894789562214982 19.722817656849827 231.94266599444012
		15.035696226415748 19.722817656849827 231.94266599444012
		15.035696226415748 19.722817656849827 231.94266599444012
		15.035696226415748 19.722817656849827 -261.98666082445686
		15.035696226415748 19.722817656849827 -261.98666082445686
		-15.034146011481386 19.722817656849827 -261.98666082445686
		-15.034146011481386 19.722817656849827 -261.98666082445686
		-15.034146011481386 19.722817656849827 231.94266599444012
		-15.034146011481386 19.722817656849827 231.94266599444012
		;
createNode transform -n "GRP_offset_Pivotdrive" -p "CNT_drive";
	rename -uid "F7DA918A-4BB4-34F8-BBEC-358F8F8E7ECB";
createNode transform -n "CNT_frontAxis" -p "GRP_offset_Pivotdrive";
	rename -uid "E03325D3-4BF0-EA5A-864D-CCB1C8D365E5";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0 0 121.40299999999988 ;
	setAttr ".sp" -type "double3" 0 0 121.40299999999988 ;
createNode nurbsCurve -n "CNT_frontAxisShape" -p "CNT_frontAxis";
	rename -uid "B0641F70-42B2-7D84-2783-C7893A49A571";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 20;
	setAttr ".cc" -type "nurbsCurve" 
		3 78 0 no 3
		83 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25
		 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52
		 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 78
		 78
		81
		-5.5958128169427479e-05 0 10.899363455181232
		-5.5958128169427479e-05 0 10.899363455181232
		-5.5958128169427479e-05 0 10.899363455181232
		-5.5958128169427479e-05 0 10.899363455181232
		-5.5958128169427479e-05 0 10.899363455181232
		-5.8409418798857135e-05 0 6.0586630874710181
		-5.8409418798857135e-05 0 6.0586630874710181
		-5.8409418798857135e-05 0 6.0586630874710181
		-45.04124729763933 0 12.66397151773343
		-83.225311313788694 0 38.177776378283312
		-108.73908109750964 0 76.361868455895504
		-115.28793365774006 0 121.40299656247072
		-115.28793365774006 0 121.40299656247072
		-115.28793365774006 0 121.40299656247072
		-110.44960038722326 0 121.40299670673484
		-110.44960038722324 0 121.40299670673484
		-110.44960038722324 0 121.40299670673484
		-104.17555916971804 0 164.55393009629731
		-79.732515460716272 0 201.13552554213999
		-43.150916654398323 0 225.57856253019233
		1.6458272977857394e-05 0 231.90663654481816
		1.6458272977857394e-05 0 231.90663654481816
		1.6458272977857394e-05 0 231.90663654481816
		1.6458272977857394e-05 0 231.90663654481816
		1.7179240845757755e-05 0 236.74733691252877
		1.7179240852151021e-05 0 236.74733691252877
		1.7179240852151021e-05 0 236.74733691252877
		45.041212220810586 0 230.1420425129979
		83.225290267691349 0 204.6282446678141
		108.73906706677798 0 166.44415259020184
		115.28793365774006 0 121.40295531212016
		115.28793365774006 0 121.40295531212016
		115.28793365774015 0 121.40295531212016
		110.44960038722324 0 121.40295718755382
		110.44960038722324 0 121.40295718755382
		110.44960038722324 0 121.40295718755382
		104.17554572781968 0 78.25202621753364
		79.732488576919494 0 41.670440853113789
		43.1508796891777 0 17.227417306959495
		-5.5958128169427479e-05 0 10.899363455181232
		0 0 10.899363455181232
		5.59581281755527e-05 0 10.899363455181232
		5.59581281755527e-05 0 10.899363455181232
		5.59581281755527e-05 0 10.899363455181232
		5.8409418856396557e-05 0 6.0586630874710181
		5.8409418856396557e-05 0 6.0586630874710181
		5.8409418856396557e-05 0 6.0586630874710181
		45.041247297639394 0 12.663971517733501
		83.225311313788779 0 38.177776378283312
		108.73908109750978 0 76.361868455895504
		115.28793365774006 0 121.40299656247072
		115.28793365774006 0 121.40299656247072
		115.28793365774015 0 121.40299656247072
		110.44960038722324 0 121.40299670673484
		110.44960038722326 0 121.40299670673484
		110.44960038722326 0 121.40299670673484
		104.17555916971816 0 164.55393009629731
		79.732515460716272 0 201.13552554213999
		43.150916654398245 0 225.57856253019244
		-1.6458273005419782e-05 0 231.90663654481816
		-1.6458273005419782e-05 0 231.90663654481816
		-1.6458273005419782e-05 0 231.90663654481816
		-1.645827300235729e-05 0 231.90663654481816
		-1.7179240820184679e-05 0 236.74733691252877
		-1.7179240820184679e-05 0 236.74733691252877
		-1.7179240820184679e-05 0 236.74733691252877
		-45.041212220810529 0 230.1420425129979
		-83.22529026769125 0 204.6282446678141
		-108.73906706677792 0 166.44415259020184
		-115.28793365774015 0 121.40295531212016
		-115.28793365774015 0 121.40295531212016
		-115.28793365774006 0 121.40295531212016
		-110.44960038722324 0 121.40295718755382
		-110.44960038722324 0 121.40295718755382
		-110.44960038722324 0 121.40295718755382
		-104.17554572781978 0 78.25202621753364
		-79.732488576919494 0 41.670440853113789
		-43.1508796891777 0 17.227417306959495
		5.59581281755527e-05 0 10.899363455181232
		5.59581281755527e-05 0 10.899363455181232
		5.59581281755527e-05 0 10.899363455181232
		;
createNode transform -n "GRP_rearAxis_reset" -p "CNT_frontAxis";
	rename -uid "D93B371E-43A3-EF86-0F1B-22A1913E144B";
	setAttr ".t" -type "double3" -6.1106675275368616e-13 -1.7763568394002505e-14 0 ;
	setAttr ".rp" -type "double3" 0 0 -137.036 ;
	setAttr ".sp" -type "double3" 0 0 -137.036 ;
createNode transform -n "CNT_rearAxis" -p "GRP_rearAxis_reset";
	rename -uid "934311E6-4154-6E82-4267-A28084DB4BC6";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0 0 -137.036 ;
	setAttr ".sp" -type "double3" 0 0 -137.036 ;
createNode nurbsCurve -n "CNT_rearAxisShape" -p "CNT_rearAxis";
	rename -uid "60B174FF-41D3-7DB7-8A88-E6A97A7FD29A";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 20;
	setAttr ".cc" -type "nurbsCurve" 
		3 78 0 no 3
		83 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25
		 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52
		 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 78
		 78
		81
		-5.5958128169427479e-05 0 -247.53963654481868
		-5.5958128169427479e-05 0 -247.53963654481868
		-5.5958128169427479e-05 0 -247.53963654481868
		-5.5958128169427479e-05 0 -247.53963654481868
		-5.5958128169427479e-05 0 -247.53963654481868
		-5.8409418798857135e-05 0 -252.38033691252889
		-5.8409418798857135e-05 0 -252.38033691252889
		-5.8409418798857135e-05 0 -252.38033691252889
		-45.04124729763933 0 -245.77502848226649
		-83.225311313788694 0 -220.2612236217166
		-108.73908109750964 0 -182.07713154410439
		-115.28793365774006 0 -137.03600343752919
		-115.28793365774006 0 -137.03600343752919
		-115.28793365774006 0 -137.03600343752919
		-110.44960038722326 0 -137.03600329326508
		-110.44960038722324 0 -137.03600329326508
		-110.44960038722324 0 -137.03600329326508
		-104.17555916971804 0 -93.8850699037026
		-79.732515460716272 0 -57.30347445785992
		-43.150916654398323 0 -32.860437469807579
		1.6458272977857394e-05 0 -26.532363455181724
		1.6458272977857394e-05 0 -26.532363455181724
		1.6458272977857394e-05 0 -26.532363455181724
		1.6458272977857394e-05 0 -26.532363455181724
		1.7179240845757755e-05 0 -21.691663087471145
		1.7179240852151021e-05 0 -21.691663087471145
		1.7179240852151021e-05 0 -21.691663087471145
		45.041212220810586 0 -28.296957487002004
		83.225290267691349 0 -53.810755332185806
		108.73906706677798 0 -91.994847409798069
		115.28793365774006 0 -137.03604468787975
		115.28793365774006 0 -137.03604468787975
		115.28793365774015 0 -137.03604468787975
		110.44960038722324 0 -137.03604281244608
		110.44960038722324 0 -137.03604281244608
		110.44960038722324 0 -137.03604281244608
		104.17554572781968 0 -180.18697378246628
		79.732488576919494 0 -216.7685591468861
		43.1508796891777 0 -241.21158269304041
		-5.5958128169427479e-05 0 -247.53963654481868
		0 0 -247.53963654481868
		5.59581281755527e-05 0 -247.53963654481868
		5.59581281755527e-05 0 -247.53963654481868
		5.59581281755527e-05 0 -247.53963654481868
		5.8409418856396557e-05 0 -252.38033691252889
		5.8409418856396557e-05 0 -252.38033691252889
		5.8409418856396557e-05 0 -252.38033691252889
		45.041247297639394 0 -245.77502848226641
		83.225311313788779 0 -220.2612236217166
		108.73908109750978 0 -182.07713154410439
		115.28793365774006 0 -137.03600343752919
		115.28793365774006 0 -137.03600343752919
		115.28793365774015 0 -137.03600343752919
		110.44960038722324 0 -137.03600329326508
		110.44960038722326 0 -137.03600329326508
		110.44960038722326 0 -137.03600329326508
		104.17555916971816 0 -93.8850699037026
		79.732515460716272 0 -57.30347445785992
		43.150916654398245 0 -32.860437469807465
		-1.6458273005419782e-05 0 -26.532363455181724
		-1.6458273005419782e-05 0 -26.532363455181724
		-1.6458273005419782e-05 0 -26.532363455181724
		-1.645827300235729e-05 0 -26.532363455181724
		-1.7179240820184679e-05 0 -21.691663087471145
		-1.7179240820184679e-05 0 -21.691663087471145
		-1.7179240820184679e-05 0 -21.691663087471145
		-45.041212220810529 0 -28.296957487002004
		-83.22529026769125 0 -53.810755332185806
		-108.73906706677792 0 -91.994847409798069
		-115.28793365774015 0 -137.03604468787975
		-115.28793365774015 0 -137.03604468787975
		-115.28793365774006 0 -137.03604468787975
		-110.44960038722324 0 -137.03604281244608
		-110.44960038722324 0 -137.03604281244608
		-110.44960038722324 0 -137.03604281244608
		-104.17554572781978 0 -180.18697378246628
		-79.732488576919494 0 -216.7685591468861
		-43.1508796891777 0 -241.21158269304041
		5.59581281755527e-05 0 -247.53963654481868
		5.59581281755527e-05 0 -247.53963654481868
		5.59581281755527e-05 0 -247.53963654481868
		;
createNode transform -n "GRP_bank_R" -p "CNT_rearAxis";
	rename -uid "68AF9757-4BAD-6080-4A2E-4FBDE2A0A1B4";
	setAttr ".t" -type "double3" 6.1106675275368616e-13 1.7763568394002505e-14 0 ;
	setAttr ".rp" -type "double3" -88.359001159667969 0.11792007088661194 121.64077758789062 ;
	setAttr -k on ".rpx";
	setAttr ".sp" -type "double3" -88.3594970703125 0.11792007088661194 121.64077758789062 ;
createNode transform -n "GRP_bank_L" -p "GRP_bank_R";
	rename -uid "5E556926-47C1-959D-F52C-138C3C32383A";
	setAttr ".rp" -type "double3" 88.359001159667969 0.11792066693305969 121.64077758789062 ;
	setAttr -k on ".rpx";
	setAttr ".sp" -type "double3" 88.3594970703125 0.11792066693305969 121.64077758789062 ;
	setAttr ".hdl" -type "double3" 88.359001159667969 0.11792066693305969 121.64077758789062 ;
createNode transform -n "GRP_offset_drive" -p "GRP_bank_L";
	rename -uid "541F0E52-4334-DF4B-458A-29856EC88BE5";
createNode transform -n "GRP_body_reset" -p "GRP_offset_drive";
	rename -uid "646BBF9C-453A-1969-3C4B-D5B6A5520C35";
	setAttr ".t" -type "double3" -1.2082557176995579e-12 -1.7763568394002505e-14 0 ;
	setAttr ".rp" -type "double3" 0 35 -17 ;
	setAttr -k on ".rpx";
	setAttr -k on ".rpz";
	setAttr ".sp" -type "double3" 0 35 -1.1368683772161603e-13 ;
createNode transform -n "CNT_body" -p "GRP_body_reset";
	rename -uid "2027ED8B-4F34-EE2D-312E-73A2C7F954AE";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".t" -type "double3" 0 -0.80639491357586479 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0 35 0 ;
	setAttr ".sp" -type "double3" 0 35 -1.1368683772161603e-13 ;
	setAttr ".mntl" -type "double3" -1 -10 -1 ;
	setAttr ".mxtl" -type "double3" 1 5 1 ;
	setAttr ".mtye" yes;
	setAttr ".xtye" yes;
	setAttr ".mrxe" yes;
	setAttr ".mrze" yes;
	setAttr ".xrxe" yes;
	setAttr ".xrze" yes;
createNode nurbsCurve -n "CNT_bodyShape" -p "CNT_body";
	rename -uid "57D0ADB6-440B-D56F-0CD5-8E9B76A7535D";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovc" 18;
	setAttr ".ovrgb" -type "float3" 1 0.4005 0 ;
	setAttr ".cc" -type "nurbsCurve" 
		3 22 0 no 3
		27 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 19 20 21 22 23 24 35 36 37 38 38 38
		25
		123.2161990941625 46.399771170441468 186.9665018174318
		-123.21619909416275 46.39977117044144 186.96650181743144
		-123.21619909416275 46.39977117044144 186.96650181743144
		-123.21619909416275 46.39977117044144 186.96650181743144
		-123.2161990941625 46.39977117044112 -214.08563329286849
		-123.2161990941625 46.39977117044112 -214.08563329286849
		-123.2161990941625 46.39977117044112 -214.08563329286849
		123.21619909416275 46.39977117044112 -214.08563329286844
		123.21619909416275 46.39977117044112 -214.08563329286844
		123.21619909416275 46.39977117044112 -214.08563329286844
		123.2161990941625 46.399771170441468 186.9665018174318
		123.2161990941625 46.399771170441468 186.9665018174318
		123.2161990941625 46.399771170441468 186.9665018174318
		-123.21619909416275 46.39977117044144 186.96650181743144
		-123.21619909416275 46.39977117044144 186.96650181743144
		-123.21619909416275 46.39977117044144 186.96650181743144
		-123.2161990941625 46.39977117044112 -214.08563329286849
		-123.2161990941625 46.39977117044112 -214.08563329286849
		-123.2161990941625 46.39977117044112 -214.08563329286849
		123.21619909416275 46.39977117044112 -214.08563329286844
		123.21619909416275 46.39977117044112 -214.08563329286844
		123.21619909416275 46.39977117044112 -214.08563329286844
		-123.2161990941625 46.39977117044112 -214.08563329286849
		-123.2161990941625 46.39977117044112 -214.08563329286849
		-123.2161990941625 46.39977117044112 -214.08563329286849
		;
createNode transform -n "GRP_frontWheelAdjust" -p "GRP_offset_drive";
	rename -uid "2670552D-4A59-F57E-D241-5C913A52BDA5";
	setAttr ".t" -type "double3" -5.8975047068088305e-13 1.7763568394002505e-14 0 ;
	setAttr ".rp" -type "double3" 0 0 121.40281850581466 ;
	setAttr ".sp" -type "double3" 0 0 121.40281850581466 ;
createNode transform -n "GRP_l_frontWheel_reset" -p "GRP_frontWheelAdjust";
	rename -uid "DDD8D568-49BF-6668-A012-7FB1772E9C8D";
	setAttr ".t" -type "double3" 20 35.000000000000007 121.403 ;
createNode transform -n "CNT_l_frontWheel" -p "GRP_l_frontWheel_reset";
	rename -uid "2495ACA4-452A-3116-8CE0-DD968DFCA06B";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 58.103362188599959 -35 -0.00045788570002969209 ;
	setAttr ".sp" -type "double3" 58.103362188599959 -35 -0.00045788570002969209 ;
createNode nurbsCurve -n "CNT_l_frontWheelShape" -p "CNT_l_frontWheel";
	rename -uid "57DF5489-44F4-D1A5-E6D7-29A88D4BCD82";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 0.095000029 0.095000029 1 ;
	setAttr ".cc" -type "nurbsCurve" 
		2 30 0 no 3
		33 0 0 1 2 2.1000000000000001 4 4.0999999999999996 6 6.0999999999999996 8 8.0999999999999996
		 10 10.1 12 12.1 14 14.1 16 16.100000000000001 18 18.100000000000001 20 20.100000000000001
		 22 22.100000000000001 24 24.100000000000001 26 26.100000000000001 28 28.100000000000001
		 30 30
		32
		45.054591724012845 -35 -27.645191207165837
		45.054591724012845 -35 -27.645191207165837
		45.054591724012845 -35 27.644275435681365
		45.054591724012845 -35 27.644275435681365
		45.054591724012823 -26.055909861039495 27.644275435681365
		45.054591724012823 -26.055909861039495 27.644275435681365
		45.054591724012845 -35 27.644275435681365
		45.054591724012845 -35 27.644275435681365
		87.037952231105152 -35 27.644275435681365
		87.037952231105152 -35 27.644275435681365
		87.037952231105152 -33.002994361689645 27.644275435681365
		87.037952231105152 -33.002994361689645 27.644275435681365
		87.037952231105152 -35 27.644275435681365
		87.037952231105152 -35 27.644275435681365
		87.037952231105166 -35 -27.645191207165823
		87.037952231105166 -35 -27.645191207165823
		87.037952231105166 -33.002994361689645 -27.645191207165851
		87.037952231105166 -33.002994361689645 -27.645191207165851
		87.037952231105166 -35 -27.645191207165823
		87.037952231105166 -35 -27.645191207165823
		45.054591724012845 -35 -27.645191207165837
		45.054591724012845 -35 -27.645191207165837
		45.054591724012838 -26.055909861039495 -27.645191207165851
		45.054591724012838 -26.055909861039495 -27.645191207165851
		87.037952231105166 -33.002994361689645 -27.645191207165851
		87.037952231105166 -33.002994361689645 -27.645191207165851
		87.037952231105152 -33.002994361689645 27.644275435681365
		87.037952231105152 -33.002994361689645 27.644275435681365
		45.054591724012823 -26.055909861039495 27.644275435681365
		45.054591724012823 -26.055909861039495 27.644275435681365
		45.054591724012838 -26.055909861039495 -27.645191207165851
		45.054591724012838 -26.055909861039495 -27.645191207165851
		;
createNode transform -n "GRP_deformer_l_frontWheel" -p "CNT_l_frontWheel";
	rename -uid "8AAC07D0-4598-D509-2ABA-40A29194CBE8";
	setAttr ".rp" -type "double3" 58.668001174927056 0.018816709518418406 0.0090178222656334128 ;
	setAttr ".sp" -type "double3" 58.668001174927056 0.018816709518418406 0.0090178222656334128 ;
createNode transform -n "GRP_ltc_l_frontWheel" -p "GRP_deformer_l_frontWheel";
	rename -uid "E0C2F87F-477C-2551-A225-BB82D8409D50";
	setAttr ".t" -type "double3" -20 -35.000000000000014 -121.403 ;
	setAttr ".rp" -type "double3" 78.668001174926758 35.018816709518433 121.41201782226562 ;
	setAttr ".sp" -type "double3" 78.668001174926758 35.018816709518433 121.41201782226562 ;
createNode transform -n "LTC_l_frontWheel" -p "GRP_ltc_l_frontWheel";
	rename -uid "835D01F8-4DBD-0E5E-301D-42B0515822A2";
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 0 1 1 ;
	setAttr ".t" -type "double3" 78.668001174926758 35.018816709518433 121.41201782226562 ;
	setAttr ".s" -type "double3" 36.896541595458984 80.992395877838135 80.992401123046875 ;
createNode lattice -n "LTC_l_frontWheelShape" -p "LTC_l_frontWheel";
	rename -uid "7AC0C993-4191-155B-EE90-C9A412C86B8F";
	setAttr -k off ".v";
	setAttr -s 16 ".iog[0].og";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 0 1 1 ;
	setAttr ".tw" yes;
	setAttr ".td" 2;
createNode lattice -n "LTC_l_frontWheelShapeOrig" -p "LTC_l_frontWheel";
	rename -uid "6BEECEB1-4C22-403B-156C-2FA8BEAD6C3F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".td" 2;
	setAttr ".cc" -type "lattice" 2 2 2 8 -0.5 -0.5 -0.5 0.5 -0.5
		 -0.5 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5 ;
createNode transform -n "LTC_l_frontWheelBase" -p "GRP_ltc_l_frontWheel";
	rename -uid "01FA11DF-4663-96B7-5A8E-A8BF2921A331";
	setAttr ".t" -type "double3" 78.668001174926758 35.018816709518433 121.41201782226562 ;
	setAttr ".s" -type "double3" 36.896541595458984 80.992395877838135 80.992401123046875 ;
createNode baseLattice -n "LTC_l_frontWheelBaseShape" -p "LTC_l_frontWheelBase";
	rename -uid "F315DBAE-4C6E-1652-55A6-5793802EC1AC";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
createNode transform -n "GRP_offset_l_frontWheel_top_01" -p "GRP_deformer_l_frontWheel";
	rename -uid "067128F9-4B51-0F62-A476-89ACA0DF6AF9";
	setAttr ".t" -type "double3" 40.219730377197855 40.515014648437486 40.505218383789085 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_l_frontWheel_top_01" -p "GRP_offset_l_frontWheel_top_01";
	rename -uid "B78B6437-4E86-926E-6611-E69B6EC63925";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_l_frontWheel_top_01Shape" -p "CNT_l_frontWheel_top_01";
	rename -uid "9C902413-4F3A-3F5D-AADD-668145EA7891";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 7.3288171367660118 8.249164751287541
		-2.0944897810900562e-15 -6.1941724750157157 5.1324091659419322
		0 4.2404843241591106 -5.2744526555926896
		5.2362244527251404e-16 7.3288171367660118 8.249164751287541
		;
createNode transform -n "GRP_offset_l_frontWheel_top_02" -p "GRP_deformer_l_frontWheel";
	rename -uid "4BA6BED4-4439-AEFC-2B1E-DABBE415233C";
	setAttr ".t" -type "double3" 77.116271972656847 40.515014648437486 40.505218383789057 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_l_frontWheel_top_02" -p "GRP_offset_l_frontWheel_top_02";
	rename -uid "D0139856-4AC5-576A-B951-C5AF1284F74E";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_l_frontWheel_top_02Shape" -p "CNT_l_frontWheel_top_02";
	rename -uid "2F21B21E-4610-4CDF-55BA-288F1EFF0E0E";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 7.3288171367660118 8.249164751287541
		-2.0944897810900562e-15 -6.1941724750157157 5.1324091659419322
		0 4.2404843241591106 -5.2744526555926896
		5.2362244527251404e-16 7.3288171367660118 8.249164751287541
		;
createNode transform -n "GRP_offset_l_frontWheel_top_03" -p "GRP_deformer_l_frontWheel";
	rename -uid "D6A16086-4175-59FC-6C1A-E0A655263BA1";
	setAttr ".t" -type "double3" 77.116271972656847 40.515014648437486 -40.487182739257818 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_l_frontWheel_top_03" -p "GRP_offset_l_frontWheel_top_03";
	rename -uid "5753750D-42F2-9EE7-E522-2884E09AE4B0";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_l_frontWheel_top_03Shape" -p "CNT_l_frontWheel_top_03";
	rename -uid "B9709853-4405-13D4-09F5-C5B97B16C589";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 8.249164751287541 -7.3288171367660127
		-2.0944897810900562e-15 5.1324091659419313 6.1941724750157157
		0 -5.2744526555926896 -4.2404843241591106
		5.2362244527251404e-16 8.249164751287541 -7.3288171367660127
		;
createNode transform -n "GRP_offset_l_frontWheel_top_04" -p "GRP_deformer_l_frontWheel";
	rename -uid "02B88A7B-4CAB-FE02-44EC-CD9318E206FC";
	setAttr ".t" -type "double3" 40.219730377197855 40.515014648437486 -40.487182739257818 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_l_frontWheel_top_04" -p "GRP_offset_l_frontWheel_top_04";
	rename -uid "0C4AE259-4386-F485-3D3D-8B9C59CA30CC";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_l_frontWheel_top_04Shape" -p "CNT_l_frontWheel_top_04";
	rename -uid "1C1AAFCD-405B-0DE6-9B08-13854D16FF03";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 8.249164751287541 -7.3288171367660127
		-2.0944897810900562e-15 5.1324091659419313 6.1941724750157157
		0 -5.2744526555926896 -4.2404843241591106
		5.2362244527251404e-16 8.249164751287541 -7.3288171367660127
		;
createNode transform -n "GRP_offset_l_frontWheel_bot_01" -p "GRP_deformer_l_frontWheel";
	rename -uid "35D0CCCA-413C-B3B0-5B90-DE907069DA83";
	setAttr ".t" -type "double3" 40.219730377197855 -40.477381229400649 40.505218383789057 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_l_frontWheel_bot_01" -p "GRP_offset_l_frontWheel_bot_01";
	rename -uid "347544ED-4990-7FF5-AF98-D1B28C9CFBD7";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_l_frontWheel_bot_01Shape" -p "CNT_l_frontWheel_bot_01";
	rename -uid "BF608413-40F9-716F-F14C-E98E27113D6A";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 -8.2491647512875392 7.3288171367660118
		-2.0944897810900562e-15 -5.1324091659419331 -6.1941724750157157
		0 5.2744526555926896 4.2404843241591106
		5.2362244527251404e-16 -8.2491647512875392 7.3288171367660118
		;
createNode transform -n "GRP_offset_l_frontWheel_bot_02" -p "GRP_deformer_l_frontWheel";
	rename -uid "B1DB9903-485C-19B3-83CA-16A5C69D822C";
	setAttr ".t" -type "double3" 77.116271972656847 -40.477381229400649 40.505218383789057 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_l_frontWheel_bot_02" -p "GRP_offset_l_frontWheel_bot_02";
	rename -uid "C5A88A8B-40CF-6DAF-C3AF-0793C1991595";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_l_frontWheel_bot_02Shape" -p "CNT_l_frontWheel_bot_02";
	rename -uid "2B1A2E2C-42C9-5701-8AC5-5D8C828E6471";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 -8.2491647512875392 7.3288171367660118
		-2.0944897810900562e-15 -5.1324091659419331 -6.1941724750157157
		0 5.2744526555926896 4.2404843241591106
		5.2362244527251404e-16 -8.2491647512875392 7.3288171367660118
		;
createNode transform -n "GRP_offset_l_frontWheel_bot_03" -p "GRP_deformer_l_frontWheel";
	rename -uid "26522486-4B7F-9D5C-7768-33AC4C14E058";
	setAttr ".t" -type "double3" 77.116271972656847 -40.477381229400649 -40.487182739257818 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_l_frontWheel_bot_03" -p "GRP_offset_l_frontWheel_bot_03";
	rename -uid "582EBCFC-47CD-06E0-F787-9F89340D8515";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_l_frontWheel_bot_03Shape" -p "CNT_l_frontWheel_bot_03";
	rename -uid "57F31F5F-4856-6132-CA46-559386E3223D";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 -7.3288171367660144 -8.249164751287541
		-2.0944897810900562e-15 6.1941724750157174 -5.1324091659419322
		0 -4.2404843241591106 5.2744526555926896
		5.2362244527251404e-16 -7.3288171367660144 -8.249164751287541
		;
createNode transform -n "GRP_offset_l_frontWheel_bot_04" -p "GRP_deformer_l_frontWheel";
	rename -uid "082E2AC9-4579-EBF3-5814-1EB453578E83";
	setAttr ".t" -type "double3" 40.219730377197855 -40.477381229400649 -40.487182739257818 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_l_frontWheel_bot_04" -p "GRP_offset_l_frontWheel_bot_04";
	rename -uid "6BFB773F-438C-F524-16F1-D8A07FA9751D";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_l_frontWheel_bot_04Shape" -p "CNT_l_frontWheel_bot_04";
	rename -uid "B3680B52-40B3-9DC3-D2AC-83B2B3810D32";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 -7.3288171367660144 -8.249164751287541
		-2.0944897810900562e-15 6.1941724750157174 -5.1324091659419322
		0 -4.2404843241591106 5.2744526555926896
		5.2362244527251404e-16 -7.3288171367660144 -8.249164751287541
		;
createNode transform -n "l_frontSteering_pt" -p "GRP_l_frontWheel_reset";
	rename -uid "DCEE5E00-4E02-5248-528D-5093EFA39DA9";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 33.13 -3.4199687988234473e-05 -0.00018149418529844752 ;
createNode locator -n "l_frontSteering_ptShape" -p "l_frontSteering_pt";
	rename -uid "DA82A479-444D-67E8-04FD-1686C4EFB120";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 10 10 10 ;
createNode transform -n "l_frontWheel_display" -p "GRP_l_frontWheel_reset";
	rename -uid "6262C5AD-4DE4-3CAD-D9BA-79BA04048FA1";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".s" -type "double3" 0.99999999999999978 1 0.99999999999999978 ;
createNode nurbsCurve -n "l_frontWheel_displayShape" -p "l_frontWheel_display";
	rename -uid "016C6AB7-4750-38C9-1F2E-D6823875F267";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode parentConstraint -n "l_frontWheel_display_parentConstraint1" -p "l_frontWheel_display";
	rename -uid "0A7EB431-403D-3DAD-31FE-92A3553D0F20";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "JNT_l_frontWheelW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -0.14961606931599647 -5.3269498117458625e-05 
		-0.00024426349695261251 ;
	setAttr ".tg[0].tor" -type "double3" -0.062346831539900997 88.007609872890711 0.11196722097120031 ;
	setAttr ".lr" -type "double3" -0.087149411369191548 88.00761137147849 0.087179635443923348 ;
	setAttr ".rst" -type "double3" 67.980426617571197 -1.9073486349441282e-05 -0.00024426269531829803 ;
	setAttr ".rsrr" -type "double3" -0.087149411369191548 88.007611371478504 0.087179635443923348 ;
	setAttr -k on ".w0";
createNode transform -n "GRP_r_frontWheel_reset" -p "GRP_frontWheelAdjust";
	rename -uid "4F8D7AA1-4E47-4B6D-13C9-1EB1B3F3C43A";
	setAttr ".t" -type "double3" -20 35.000000000000014 121.40300000000006 ;
createNode transform -n "r_frontSteering_pt" -p "GRP_r_frontWheel_reset";
	rename -uid "331E2CA1-48CC-2831-C3F6-39B9B8911FD7";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -33.13 -3.4199687988234473e-05 -0.00018149418529844752 ;
createNode locator -n "r_frontSteering_ptShape" -p "r_frontSteering_pt";
	rename -uid "CC3746DF-4381-E8C3-C5F9-FBA520E80AAB";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 10 10 10 ;
createNode transform -n "r_frontWheel_display" -p "GRP_r_frontWheel_reset";
	rename -uid "913599B5-4B11-41D7-AE0E-7C88A12C9652";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
createNode nurbsCurve -n "r_frontWheel_displayShape" -p "r_frontWheel_display";
	rename -uid "77765A6B-4403-BA22-B282-25A9B46ECFFC";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode parentConstraint -n "r_frontWheel_display_parentConstraint1" -p "r_frontWheel_display";
	rename -uid "F6D3B64B-44B2-C1D6-D6AC-8DB52FAF306E";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "JNT_r_frontWheelW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 0.021303145257519418 -1.906981010790787e-05 
		-0.00057593023176139013 ;
	setAttr ".tg[0].tor" -type "double3" 0.024723332774343226 87.999999812655858 0.024708271987065725 ;
	setAttr ".lr" -type "double3" 1.5060787255435109e-05 87.99999999869992 1.5051612630760566e-05 ;
	setAttr ".rst" -type "double3" -68.108696854742476 -1.9073486349441282e-05 -0.00024426269534671974 ;
	setAttr ".rsrr" -type "double3" 1.5060787255432915e-05 87.99999999869992 1.5051612630762765e-05 ;
	setAttr -k on ".w0";
createNode transform -n "CNT_r_frontWheel" -p "GRP_r_frontWheel_reset";
	rename -uid "F616AA72-4A08-8F06-0CF1-C48284FC36E9";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -58.103362188599974 -35 -0.00045788570002969209 ;
	setAttr ".sp" -type "double3" -58.103362188599974 -35 -0.00045788570002969209 ;
createNode nurbsCurve -n "CNT_r_frontWheelShape" -p "CNT_r_frontWheel";
	rename -uid "8632D454-4E6D-5665-CA00-85AE02AD89C7";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 0.095000029 0.095000029 1 ;
	setAttr ".cc" -type "nurbsCurve" 
		2 30 0 no 3
		33 0 0 1 2 2.1000000000000001 4 4.0999999999999996 6 6.0999999999999996 8 8.0999999999999996
		 10 10.1 12 12.1 14 14.1 16 16.100000000000001 18 18.100000000000001 20 20.100000000000001
		 22 22.100000000000001 24 24.100000000000001 26 26.100000000000001 28 28.100000000000001
		 30 30
		32
		-45.054591724012894 -35 -27.645191207165809
		-45.054591724012894 -35 -27.645191207165809
		-45.054591724012894 -35 27.644275435681426
		-45.054591724012894 -35 27.644275435681426
		-45.05459172401288 -26.055909861039495 27.644275435681426
		-45.05459172401288 -26.055909861039495 27.644275435681426
		-45.054591724012894 -35 27.644275435681426
		-45.054591724012894 -35 27.644275435681426
		-87.03795223110518 -35 27.644275435681426
		-87.03795223110518 -35 27.644275435681426
		-87.03795223110518 -33.002994361689645 27.644275435681426
		-87.03795223110518 -33.002994361689645 27.644275435681426
		-87.03795223110518 -35 27.644275435681426
		-87.03795223110518 -35 27.644275435681426
		-87.03795223110518 -35 -27.645191207165791
		-87.03795223110518 -35 -27.645191207165791
		-87.03795223110518 -33.002994361689645 -27.645191207165823
		-87.03795223110518 -33.002994361689645 -27.645191207165823
		-87.03795223110518 -35 -27.645191207165791
		-87.03795223110518 -35 -27.645191207165791
		-45.054591724012894 -35 -27.645191207165809
		-45.054591724012894 -35 -27.645191207165809
		-45.05459172401288 -26.055909861039495 -27.645191207165823
		-45.05459172401288 -26.055909861039495 -27.645191207165823
		-87.03795223110518 -33.002994361689645 -27.645191207165823
		-87.03795223110518 -33.002994361689645 -27.645191207165823
		-87.03795223110518 -33.002994361689645 27.644275435681426
		-87.03795223110518 -33.002994361689645 27.644275435681426
		-45.05459172401288 -26.055909861039495 27.644275435681426
		-45.05459172401288 -26.055909861039495 27.644275435681426
		-45.05459172401288 -26.055909861039495 -27.645191207165823
		-45.05459172401288 -26.055909861039495 -27.645191207165823
		;
createNode transform -n "GRP_deformer_r_frontWheel" -p "CNT_r_frontWheel";
	rename -uid "DC776BC9-40E4-3106-38AA-18937C905D50";
	setAttr ".rp" -type "double3" -58.371577579987211 0.018819451332078074 -0.00024426269538224687 ;
	setAttr ".sp" -type "double3" -58.371577579987211 0.018819451332078074 -0.00024426269538224687 ;
createNode transform -n "GRP_offset_r_frontwheel_top_01" -p "GRP_deformer_r_frontWheel";
	rename -uid "4FCA94E1-4AF8-016F-A147-7C9D107C2F48";
	setAttr ".t" -type "double3" -40.219500000000018 40.514819451332073 40.505021636962866 ;
createNode transform -n "CNT_r_frontWheel_top_01" -p "GRP_offset_r_frontwheel_top_01";
	rename -uid "8D8057CD-4DC3-392D-D8C6-C5AB9FF70668";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_r_frontWheel_top_01" -p "|CTRL_set|CTRL_setsubControl|GRP_Name_rig|GRP_Controls|GRP_constraint|Transform_Ctrl|GRP_main|Main_Ctrl|main_null|CNT_drive|GRP_offset_Pivotdrive|CNT_frontAxis|GRP_rearAxis_reset|CNT_rearAxis|GRP_bank_R|GRP_bank_L|GRP_offset_drive|GRP_frontWheelAdjust|GRP_r_frontWheel_reset|CNT_r_frontWheel|GRP_deformer_r_frontWheel|GRP_offset_r_frontwheel_top_01|CNT_r_frontWheel_top_01";
	rename -uid "FC02802B-4078-F38A-C076-0E815ED55E4A";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		-1.4953607214707176e-06 7.3123256366805105 8.223332772410048
		-1.4953607263556989e-06 -6.2047365548139313 5.1401596339253537
		-1.4953607196943608e-06 4.2291525169987585 -5.293729437887345
		-1.4953607214707176e-06 7.3123256366805105 8.223332772410048
		;
createNode transform -n "GRP_offset_r_frontwheel_top_02" -p "GRP_deformer_r_frontWheel";
	rename -uid "B080FF1E-4040-B070-49C4-9E8851ACEF3E";
	setAttr ".t" -type "double3" -77.1165 40.514819451332073 40.505021636962866 ;
createNode transform -n "CNT_r_frontWheel_top_02" -p "GRP_offset_r_frontwheel_top_02";
	rename -uid "E5CEB511-4859-A9DA-D37A-C1A515106BC3";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_r_frontWheel_top_2" -p "CNT_r_frontWheel_top_02";
	rename -uid "9D6144A5-4ED7-5B43-1A0F-7B9923B2FE3C";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		-8.5449160369677202e-07 7.3123256366805105 8.223332772410048
		-8.5449160858175333e-07 -6.2047365548139313 5.1401596339253537
		-8.5449160192041518e-07 4.2291525169987585 -5.293729437887345
		-8.5449160369677202e-07 7.3123256366805105 8.223332772410048
		;
createNode transform -n "GRP_offset_r_frontwheel_top_03" -p "GRP_deformer_r_frontWheel";
	rename -uid "B6FE7FF9-4B3F-39F0-610A-F9B0ABF9ED34";
	setAttr ".t" -type "double3" -77.1165 40.514819451332073 -40.486978363037153 ;
createNode transform -n "CNT_r_frontWheel_top_03" -p "GRP_offset_r_frontwheel_top_03";
	rename -uid "95753CDC-4EBD-0B86-071C-D68ABFBBD983";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_r_frontWheel_top_3" -p "CNT_r_frontWheel_top_03";
	rename -uid "0C2E2B06-40B2-477A-06C8-84AA647DF1C8";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		-8.5449160369677202e-07 8.2285695003067794 -7.3299069470119615
		-8.5449160858175333e-07 5.1453963618220833 6.1871552444824811
		-8.5449160192041518e-07 -5.2884927099906163 -4.2467338273302113
		-8.5449160369677202e-07 8.2285695003067794 -7.3299069470119615
		;
createNode transform -n "GRP_offset_r_frontwheel_top_04" -p "GRP_deformer_r_frontWheel";
	rename -uid "9BE5FA46-4C67-E310-A088-82890D4F0D0C";
	setAttr ".t" -type "double3" -40.219500000000018 40.514819451332073 -40.486978363037153 ;
createNode transform -n "CNT_r_frontWheel_top_04" -p "GRP_offset_r_frontwheel_top_04";
	rename -uid "66A65D80-4A84-7323-9E47-BC8A27FD61EF";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_r_frontWheel_top_4" -p "CNT_r_frontWheel_top_04";
	rename -uid "C1822A82-4D45-3163-77F4-42B01EC78EA2";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		-1.4953607214707176e-06 8.2285695003067794 -7.3299069470119615
		-1.4953607263556989e-06 5.1453963618220833 6.1871552444824811
		-1.4953607196943608e-06 -5.2884927099906163 -4.2467338273302113
		-1.4953607214707176e-06 8.2285695003067794 -7.3299069470119615
		;
createNode transform -n "GRP_offset_r_frontwheel_bot_02" -p "GRP_deformer_r_frontWheel";
	rename -uid "FB5B4F3E-4827-D893-2B22-A58013C0D2AF";
	setAttr ".t" -type "double3" -77.1165 -40.477180548667924 40.505021636962866 ;
createNode transform -n "CNT_r_frontWheel_bot_02" -p "GRP_offset_r_frontwheel_bot_02";
	rename -uid "26A13D03-43A1-B940-C1E7-EE99524EDB79";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_r_frontWheel_bot_Shape2" -p "CNT_r_frontWheel_bot_02";
	rename -uid "586A9FB9-48BA-B5BA-B00B-EA824D3913D5";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		-8.5449160369677202e-07 -8.3057163864016985 7.2729064107970753
		-8.5449160858175333e-07 -5.1559327070717673 -6.2287893961758094
		-8.5449160192041518e-07 5.2263835876967644 4.2564187887468421
		-8.5449160369677202e-07 -8.3057163864016985 7.2729064107970753
		;
createNode transform -n "GRP_offset_r_frontwheel_bot_01" -p "GRP_deformer_r_frontWheel";
	rename -uid "D7D0D7ED-4C7C-4B9C-6535-739EC7A47F2A";
	setAttr ".t" -type "double3" -40.219500000000018 -40.477180548667924 40.505021636962866 ;
createNode transform -n "CNT_r_frontWheel_bot_01" -p "GRP_offset_r_frontwheel_bot_01";
	rename -uid "F1F5CFC6-4048-9019-39AD-0798F0F57CC0";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_r_frontWheel_bot_Shape1" -p "CNT_r_frontWheel_bot_01";
	rename -uid "EFAD75CF-4305-92E6-CF33-49994F5A8521";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		-1.495360728576145e-06 -8.3057163864016985 7.2729064107970753
		-1.4953607334611263e-06 -5.1559327070717673 -6.2287893961758094
		-1.4953607267997882e-06 5.2263835876967644 4.2564187887468421
		-1.495360728576145e-06 -8.3057163864016985 7.2729064107970753
		;
createNode transform -n "GRP_offset_r_frontwheel_bot_03" -p "GRP_deformer_r_frontWheel";
	rename -uid "8906D330-45E0-69EB-75C6-47A6A64C2DF9";
	setAttr ".t" -type "double3" -77.1165 -40.477180548667924 -40.486978363037153 ;
createNode transform -n "CNT_r_frontWheel_bot_03" -p "GRP_offset_r_frontwheel_bot_03";
	rename -uid "88A23978-4464-2FCA-3CD7-60AFC12F1D4A";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_r_frontWheel_bot_Shape3" -p "CNT_r_frontWheel_bot_03";
	rename -uid "03CDF445-4B55-4D83-AC21-2893F9A30EF7";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		-8.5449160369677202e-07 -7.3274322013813853 -8.2244596904069738
		-8.5449160858175333e-07 6.1896299901130627 -5.1412865519222812
		-8.5449160192041518e-07 -4.2442590816996226 5.292602519890421
		-8.5449160369677202e-07 -7.3274322013813853 -8.2244596904069738
		;
createNode transform -n "GRP_offset_r_frontwheel_bot_04" -p "GRP_deformer_r_frontWheel";
	rename -uid "9CA3E982-4016-DE97-C54F-D3A62FE763AA";
	setAttr ".t" -type "double3" -40.219500000000018 -40.477180548667924 -40.486978363037153 ;
createNode transform -n "CNT_r_frontWheel_bot_04" -p "GRP_offset_r_frontwheel_bot_04";
	rename -uid "7F92314E-4F87-46E3-66FC-50B8534002BE";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_r_frontWheel_bot_Shape4" -p "CNT_r_frontWheel_bot_04";
	rename -uid "0B091A3C-40F2-626D-9D51-ABB5FFAACEFE";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		-1.495360728576145e-06 -7.3274322013813853 -8.2244596904069738
		-1.4953607334611263e-06 6.1896299901130627 -5.1412865519222812
		-1.4953607267997882e-06 -4.2442590816996226 5.292602519890421
		-1.495360728576145e-06 -7.3274322013813853 -8.2244596904069738
		;
createNode transform -n "GRP_ltc_r_frontWheel" -p "GRP_deformer_r_frontWheel";
	rename -uid "DF6424BB-41C4-457C-334F-5BB1A1CD345F";
	setAttr ".t" -type "double3" 20 -35.000000000000014 -121.40300000000002 ;
	setAttr ".rp" -type "double3" -77.734025955200195 35.018819451332092 121.41202163696289 ;
	setAttr ".sp" -type "double3" -77.734025955200195 35.018819451332092 121.41202163696289 ;
createNode transform -n "LTC_r_frontWheel" -p "GRP_ltc_r_frontWheel";
	rename -uid "F1FDBD00-419C-FBA4-3D8F-17B3BB433442";
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 0 1 1 ;
	setAttr ".t" -type "double3" -78.668 35.018819451332092 121.41202163696288 ;
	setAttr ".s" -type "double3" 36.897 80.992 80.992 ;
createNode lattice -n "LTC_r_frontWheelShape" -p "LTC_r_frontWheel";
	rename -uid "43A1A45A-4E91-A20B-20F5-96810BD8A65E";
	setAttr -k off ".v";
	setAttr -s 16 ".iog[0].og";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 0 1 1 ;
	setAttr ".tw" yes;
	setAttr ".td" 2;
createNode lattice -n "LTC_r_frontWheelShapeOrig" -p "LTC_r_frontWheel";
	rename -uid "9A09F94F-489A-BC5E-4EEE-F4BBF11D7155";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".td" 2;
	setAttr ".cc" -type "lattice" 2 2 2 8 -0.5 -0.5 -0.5 0.5 -0.5
		 -0.5 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5 ;
createNode transform -n "LTC_r_frontWheelBase" -p "GRP_ltc_r_frontWheel";
	rename -uid "522FCFBF-4E22-2AE3-5441-2B8012BE5274";
	setAttr ".t" -type "double3" -78.668 35.018819451332092 121.41202163696288 ;
	setAttr ".s" -type "double3" 36.897 80.992 80.992 ;
createNode baseLattice -n "LTC_r_frontWheelBaseShape" -p "LTC_r_frontWheelBase";
	rename -uid "C8840D40-447D-96BF-BF16-349AF0C9B5BE";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
createNode transform -n "GRP_r_rearWheel_reset" -p "GRP_offset_drive";
	rename -uid "EAB6271F-43A4-7766-84ED-BEBEE9D8C8BF";
	setAttr ".t" -type "double3" -20 34.999999999999986 -137.036 ;
createNode transform -n "CNT_r_rearWheel" -p "GRP_r_rearWheel_reset";
	rename -uid "869E6D47-42BB-7C77-CE0A-8F89ED539AE8";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -58.10336218860003 -35 0.00024937511926736988 ;
	setAttr ".sp" -type "double3" -58.10336218860003 -35 0.00024937511926736988 ;
createNode nurbsCurve -n "CNT_r_rearWheelShape" -p "CNT_r_rearWheel";
	rename -uid "0333CEF3-4A35-6005-48DD-9492BF4C7B8A";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 0.095000029 0.095000029 1 ;
	setAttr ".cc" -type "nurbsCurve" 
		2 30 0 no 3
		33 0 0 1 2 2.1000000000000001 4 4.0999999999999996 6 6.0999999999999996 8 8.0999999999999996
		 10 10.1 12 12.1 14 14.1 16 16.100000000000001 18 18.100000000000001 20 20.100000000000001
		 22 22.100000000000001 24 24.100000000000001 26 26.100000000000001 28 28.100000000000001
		 30 30
		32
		-45.054591724012937 -35 -27.644483946346529
		-45.054591724012937 -35 -27.644483946346529
		-45.054591724012937 -35 27.644982696500666
		-45.054591724012937 -35 27.644982696500666
		-45.054591724012923 -26.055909861039495 27.644982696500666
		-45.054591724012923 -26.055909861039495 27.644982696500666
		-45.054591724012937 -35 27.644982696500666
		-45.054591724012937 -35 27.644982696500666
		-87.037952231105237 -35 27.644982696500666
		-87.037952231105237 -35 27.644982696500666
		-87.037952231105237 -33.002994361689645 27.644982696500666
		-87.037952231105237 -33.002994361689645 27.644982696500666
		-87.037952231105237 -35 27.644982696500666
		-87.037952231105237 -35 27.644982696500666
		-87.037952231105237 -35 -27.644483946346529
		-87.037952231105237 -35 -27.644483946346529
		-87.037952231105237 -33.002994361689645 -27.644483946346551
		-87.037952231105237 -33.002994361689645 -27.644483946346551
		-87.037952231105237 -35 -27.644483946346529
		-87.037952231105237 -35 -27.644483946346529
		-45.054591724012937 -35 -27.644483946346529
		-45.054591724012937 -35 -27.644483946346529
		-45.054591724012923 -26.055909861039495 -27.644483946346551
		-45.054591724012923 -26.055909861039495 -27.644483946346551
		-87.037952231105237 -33.002994361689645 -27.644483946346551
		-87.037952231105237 -33.002994361689645 -27.644483946346551
		-87.037952231105237 -33.002994361689645 27.644982696500666
		-87.037952231105237 -33.002994361689645 27.644982696500666
		-45.054591724012923 -26.055909861039495 27.644982696500666
		-45.054591724012923 -26.055909861039495 27.644982696500666
		-45.054591724012923 -26.055909861039495 -27.644483946346551
		-45.054591724012923 -26.055909861039495 -27.644483946346551
		;
createNode transform -n "GRP_deformer_r_rearWheel" -p "CNT_r_rearWheel";
	rename -uid "763721B6-4D66-D016-4E8F-8BB224253161";
	setAttr ".rp" -type "double3" -58.668001174926452 0.018784284591689016 0.0092055664062229425 ;
	setAttr ".sp" -type "double3" -58.668001174926452 0.018784284591689016 0.0092055664062229425 ;
createNode transform -n "GRP_ltc_r_rearWheel" -p "GRP_deformer_r_rearWheel";
	rename -uid "F8A42048-4CD9-747C-F891-228565B758A9";
	setAttr ".t" -type "double3" 20 -34.999999999999986 137.036 ;
	setAttr ".rp" -type "double3" -78.668001174926758 35.018784284591675 -137.02679443359375 ;
	setAttr ".sp" -type "double3" -78.668001174926758 35.018784284591675 -137.02679443359375 ;
createNode transform -n "LTC_r_rearWheel" -p "GRP_ltc_r_rearWheel";
	rename -uid "A115E45A-4ED9-4A2D-B847-EDBDAA0D1D21";
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 0 1 1 ;
	setAttr ".t" -type "double3" -78.668001174926758 35.018784284591675 -137.02679443359375 ;
	setAttr ".s" -type "double3" 36.896541595458984 80.9923996925354 80.992401123046875 ;
createNode lattice -n "LTC_r_rearWheelShape" -p "LTC_r_rearWheel";
	rename -uid "1BAE5F2C-41F5-D475-692C-41A2EDD8EF80";
	setAttr -k off ".v";
	setAttr -s 16 ".iog[0].og";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 0 1 1 ;
	setAttr ".tw" yes;
	setAttr ".td" 2;
createNode lattice -n "LTC_r_rearWheelShapeOrig" -p "LTC_r_rearWheel";
	rename -uid "67862F87-4C71-B22B-2D16-62B25D7EC7EF";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".td" 2;
	setAttr ".cc" -type "lattice" 2 2 2 8 -0.5 -0.5 -0.5 0.5 -0.5
		 -0.5 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5 ;
createNode transform -n "LTC_r_rearWheelBase" -p "GRP_ltc_r_rearWheel";
	rename -uid "A2F7125A-4B3D-D1B7-DCF6-80878B4ACAAA";
	setAttr ".t" -type "double3" -78.668001174926758 35.018784284591675 -137.02679443359375 ;
	setAttr ".s" -type "double3" 36.896541595458984 80.9923996925354 80.992401123046875 ;
createNode baseLattice -n "LTC_r_rearWheelBaseShape" -p "LTC_r_rearWheelBase";
	rename -uid "72EBA5DB-4065-28C8-9297-F69A56F7DE40";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
createNode transform -n "GRP_offset_r_rearWheel_top_01" -p "GRP_deformer_r_rearWheel";
	rename -uid "A025A609-43D7-34AF-B429-C495D00224C0";
	setAttr ".t" -type "double3" -40.219730377196655 40.514984130859389 40.50540612792966 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_r_rearWheel_top_01" -p "GRP_offset_r_rearWheel_top_01";
	rename -uid "8C47DCC2-499C-C50F-7B75-AC99D553BB0F";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_r_rearWheel_top_01Shape" -p "CNT_r_rearWheel_top_01";
	rename -uid "98CA9D8B-4140-2B8C-97C0-10B674DEF473";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 7.3288171367660118 8.249164751287541
		-2.0944897810900562e-15 -6.1941724750157157 5.1324091659419322
		0 4.2404843241591106 -5.2744526555926896
		5.2362244527251404e-16 7.3288171367660118 8.249164751287541
		;
createNode transform -n "GRP_offset_r_rearWheel_top_02" -p "GRP_deformer_r_rearWheel";
	rename -uid "AE375B09-49D9-8DF6-1C39-C1BA098617A9";
	setAttr ".t" -type "double3" -77.116271972655639 40.514984130859389 40.50540612792966 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_r_rearWheel_top_02" -p "GRP_offset_r_rearWheel_top_02";
	rename -uid "368C118F-49AD-E681-9A63-75931E9E31A5";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_r_rearWheel_top_02Shape" -p "CNT_r_rearWheel_top_02";
	rename -uid "4E73AD84-4C00-CBA2-C770-3780E50BFB1F";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 7.3288171367660118 8.249164751287541
		-2.0944897810900562e-15 -6.1941724750157157 5.1324091659419322
		0 4.2404843241591106 -5.2744526555926896
		5.2362244527251404e-16 7.3288171367660118 8.249164751287541
		;
createNode transform -n "GRP_offset_r_rearWheel_top_03" -p "GRP_deformer_r_rearWheel";
	rename -uid "32F46652-4884-5F8B-5D4D-C3A10238F20C";
	setAttr ".t" -type "double3" -77.116271972655639 40.514984130859389 -40.486994995117215 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_r_rearWheel_top_03" -p "GRP_offset_r_rearWheel_top_03";
	rename -uid "C9FBE99A-4098-B366-B4F3-309E295A8867";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_r_rearWheel_top_03Shape" -p "CNT_r_rearWheel_top_03";
	rename -uid "855E0408-4C4A-D223-1EAF-B38CC8E0FB84";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 8.249164751287541 -7.3288171367660127
		-2.0944897810900562e-15 5.1324091659419313 6.1941724750157157
		0 -5.2744526555926896 -4.2404843241591106
		5.2362244527251404e-16 8.249164751287541 -7.3288171367660127
		;
createNode transform -n "GRP_offset_r_rearWheel_top_04" -p "GRP_deformer_r_rearWheel";
	rename -uid "FEE506EF-4386-CFFA-4027-949176B66D33";
	setAttr ".t" -type "double3" -40.219730377196655 40.514984130859389 -40.486994995117215 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_r_rearWheel_top_04" -p "GRP_offset_r_rearWheel_top_04";
	rename -uid "A21D8596-4004-075A-847F-F8A3BA233DAA";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_r_rearWheel_top_04Shape" -p "CNT_r_rearWheel_top_04";
	rename -uid "449EDC42-437F-C54F-EBCC-A59104B70B87";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 8.249164751287541 -7.3288171367660127
		-2.0944897810900562e-15 5.1324091659419313 6.1941724750157157
		0 -5.2744526555926896 -4.2404843241591106
		5.2362244527251404e-16 8.249164751287541 -7.3288171367660127
		;
createNode transform -n "GRP_offset_r_rearWheel_bot_01" -p "GRP_deformer_r_rearWheel";
	rename -uid "D615629B-4935-04B9-902A-52A72E28DBCB";
	setAttr ".t" -type "double3" -40.219730377196655 -40.477415561676011 40.50540612792966 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_r_rearWheel_bot_01" -p "GRP_offset_r_rearWheel_bot_01";
	rename -uid "1D468B44-4E4B-7535-B4C8-5C9A3E085E93";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_r_rearWheel_bot_01Shape" -p "CNT_r_rearWheel_bot_01";
	rename -uid "FAB871CA-473C-629F-DB03-E4BA4FCA5311";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 -8.2491647512875392 7.3288171367660118
		-2.0944897810900562e-15 -5.1324091659419331 -6.1941724750157157
		0 5.2744526555926896 4.2404843241591106
		5.2362244527251404e-16 -8.2491647512875392 7.3288171367660118
		;
createNode transform -n "GRP_offset_r_rearWheel_bot_02" -p "GRP_deformer_r_rearWheel";
	rename -uid "07F43D3D-4704-31D2-7E1E-198E5A21A9DB";
	setAttr ".t" -type "double3" -77.116271972655639 -40.477415561676011 40.50540612792966 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_r_rearWheel_bot_02" -p "GRP_offset_r_rearWheel_bot_02";
	rename -uid "A15776E3-41BA-0BAC-D284-26A23994A0B2";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_r_rearWheel_bot_02Shape" -p "CNT_r_rearWheel_bot_02";
	rename -uid "EB429854-4CE0-A382-91CB-C4900E7B8C36";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 -8.2491647512875392 7.3288171367660118
		-2.0944897810900562e-15 -5.1324091659419331 -6.1941724750157157
		0 5.2744526555926896 4.2404843241591106
		5.2362244527251404e-16 -8.2491647512875392 7.3288171367660118
		;
createNode transform -n "GRP_offset_r_rearWheel_bot_03" -p "GRP_deformer_r_rearWheel";
	rename -uid "FE02DBEE-4E93-848D-D510-45BEF80C9927";
	setAttr ".t" -type "double3" -77.116271972655639 -40.477415561676011 -40.486994995117215 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_r_rearWheel_bot_03" -p "GRP_offset_r_rearWheel_bot_03";
	rename -uid "FA795A06-4C98-9457-A8A0-79824DE32D74";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_r_rearWheel_bot_03Shape" -p "CNT_r_rearWheel_bot_03";
	rename -uid "1FB08665-41C1-6434-8FFB-DBB4C1B1E443";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 -7.3288171367660144 -8.249164751287541
		-2.0944897810900562e-15 6.1941724750157174 -5.1324091659419322
		0 -4.2404843241591106 5.2744526555926896
		5.2362244527251404e-16 -7.3288171367660144 -8.249164751287541
		;
createNode transform -n "GRP_offset_r_rearWheel_bot_04" -p "GRP_deformer_r_rearWheel";
	rename -uid "D4708C2C-4A47-DDA6-34D9-A7BE99F3FD89";
	setAttr ".t" -type "double3" -40.219730377196655 -40.477415561676011 -40.486994995117215 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_r_rearWheel_bot_04" -p "GRP_offset_r_rearWheel_bot_04";
	rename -uid "09359682-47AE-B5CA-8018-8D9AB53C222D";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_r_rearWheel_bot_04Shape" -p "CNT_r_rearWheel_bot_04";
	rename -uid "E0DD27EA-4134-85F5-DCD1-99A79FF3345B";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 -7.3288171367660144 -8.249164751287541
		-2.0944897810900562e-15 6.1941724750157174 -5.1324091659419322
		0 -4.2404843241591106 5.2744526555926896
		5.2362244527251404e-16 -7.3288171367660144 -8.249164751287541
		;
createNode transform -n "r_rearWheel_display" -p "GRP_r_rearWheel_reset";
	rename -uid "E7F1DA3E-44E4-B5CC-DE71-85A03E0602A3";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".s" -type "double3" 1.0000000000000011 1 1.0000000000000011 ;
createNode nurbsCurve -n "r_rearWheel_displayShape" -p "r_rearWheel_display";
	rename -uid "BBE68B53-4E41-4596-9F6B-01B1645CFBAF";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode parentConstraint -n "r_rearWheel_display_parentConstraint1" -p "r_rearWheel_display";
	rename -uid "15094D29-4FD5-F6D6-4178-9181CBA1B8E1";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_rearWheel_jtW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 1.4210854715202004e-14 -2.9842794901924208e-13 
		2.6830093702301383e-11 ;
	setAttr ".tg[0].tor" -type "double3" 0 89.999999999999986 0 ;
	setAttr ".lr" -type "double3" 0 89.999999999999986 0 ;
	setAttr ".rst" -type "double3" -88.13000000000001 35.000000000000028 -137.03599997810005 ;
	setAttr ".rsrr" -type "double3" 0 89.999999999999986 0 ;
	setAttr -k on ".w0";
createNode transform -n "GRP_l_rearWheel_reset" -p "GRP_offset_drive";
	rename -uid "946B14C2-4B83-52F3-4981-9F8AC9885BDC";
	setAttr ".t" -type "double3" 20 34.999999999999986 -137.036 ;
createNode transform -n "CNT_l_rearWheel" -p "GRP_l_rearWheel_reset";
	rename -uid "88E0F86D-44BA-7679-424A-E1905E7682AA";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 58.10336218860003 -35 0.00024937511926736988 ;
	setAttr ".sp" -type "double3" 58.10336218860003 -35 0.00024937511926736988 ;
createNode nurbsCurve -n "CNT_l_rearWheelShape" -p "CNT_l_rearWheel";
	rename -uid "561F2460-4469-4E2D-B0E3-82A98E16678C";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 0.095000029 0.095000029 1 ;
	setAttr ".cc" -type "nurbsCurve" 
		2 30 0 no 3
		33 0 0 1 2 2.1000000000000001 4 4.0999999999999996 6 6.0999999999999996 8 8.0999999999999996
		 10 10.1 12 12.1 14 14.1 16 16.100000000000001 18 18.100000000000001 20 20.100000000000001
		 22 22.100000000000001 24 24.100000000000001 26 26.100000000000001 28 28.100000000000001
		 30 30
		32
		45.054591724012937 -35 -27.644483946346529
		45.054591724012937 -35 -27.644483946346529
		45.054591724012937 -35 27.644982696500676
		45.054591724012937 -35 27.644982696500676
		45.054591724012923 -26.055909861039495 27.644982696500676
		45.054591724012923 -26.055909861039495 27.644982696500676
		45.054591724012937 -35 27.644982696500676
		45.054591724012937 -35 27.644982696500676
		87.037952231105237 -35 27.644982696500676
		87.037952231105237 -35 27.644982696500676
		87.037952231105237 -33.002994361689645 27.644982696500676
		87.037952231105237 -33.002994361689645 27.644982696500676
		87.037952231105237 -35 27.644982696500676
		87.037952231105237 -35 27.644982696500676
		87.037952231105237 -35 -27.644483946346529
		87.037952231105237 -35 -27.644483946346529
		87.037952231105237 -33.002994361689645 -27.644483946346551
		87.037952231105237 -33.002994361689645 -27.644483946346551
		87.037952231105237 -35 -27.644483946346529
		87.037952231105237 -35 -27.644483946346529
		45.054591724012937 -35 -27.644483946346529
		45.054591724012937 -35 -27.644483946346529
		45.054591724012923 -26.055909861039495 -27.644483946346551
		45.054591724012923 -26.055909861039495 -27.644483946346551
		87.037952231105237 -33.002994361689645 -27.644483946346551
		87.037952231105237 -33.002994361689645 -27.644483946346551
		87.037952231105237 -33.002994361689645 27.644982696500676
		87.037952231105237 -33.002994361689645 27.644982696500676
		45.054591724012923 -26.055909861039495 27.644982696500676
		45.054591724012923 -26.055909861039495 27.644982696500676
		45.054591724012923 -26.055909861039495 -27.644483946346551
		45.054591724012923 -26.055909861039495 -27.644483946346551
		;
createNode transform -n "GRP_deformer_l_rearWheel" -p "CNT_l_rearWheel";
	rename -uid "751F37F3-4FFB-0D75-5EB0-A493D2334457";
	setAttr ".rp" -type "double3" 58.668001174927063 0.018784284591689016 0.0092055664062229425 ;
	setAttr ".sp" -type "double3" 58.668001174927063 0.018784284591689016 0.0092055664062229425 ;
createNode transform -n "GRP_ltc_l_rearWheel" -p "GRP_deformer_l_rearWheel";
	rename -uid "61F2CD7A-4BDD-AA3D-360D-52AC65AE3309";
	setAttr ".t" -type "double3" -20 -34.999999999999986 137.036 ;
	setAttr ".rp" -type "double3" 78.668001174926758 35.018784284591675 -137.02679443359375 ;
	setAttr ".sp" -type "double3" 78.668001174926758 35.018784284591675 -137.02679443359375 ;
createNode transform -n "LTC_l_rearWheel" -p "GRP_ltc_l_rearWheel";
	rename -uid "D5CE7090-4C74-B95E-F241-0B84D6689B58";
	setAttr ".t" -type "double3" 78.668001174926758 35.018784284591675 -137.02679443359375 ;
	setAttr ".s" -type "double3" 36.896541595458984 80.9923996925354 80.992401123046875 ;
createNode lattice -n "LTC_l_rearWheelShape" -p "LTC_l_rearWheel";
	rename -uid "D3F16ACF-4B95-FF0D-D5DA-C6B908180930";
	setAttr -k off ".v";
	setAttr -s 16 ".iog[0].og";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 0 1 1 ;
	setAttr ".tw" yes;
	setAttr ".td" 2;
createNode lattice -n "LTC_l_rearWheelShapeOrig" -p "LTC_l_rearWheel";
	rename -uid "9EEE8CC7-4E9D-C34D-AE33-B28CDD2B6A32";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".td" 2;
	setAttr ".cc" -type "lattice" 2 2 2 8 -0.5 -0.5 -0.5 0.5 -0.5
		 -0.5 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5 ;
createNode transform -n "LTC_l_rearWheelBase" -p "GRP_ltc_l_rearWheel";
	rename -uid "1CB82BE6-4159-1BA5-93DE-FF99A6B54884";
	setAttr ".t" -type "double3" 78.668001174926758 35.018784284591675 -137.02679443359375 ;
	setAttr ".s" -type "double3" 36.896541595458984 80.9923996925354 80.992401123046875 ;
createNode baseLattice -n "LTC_l_rearWheelBaseShape" -p "LTC_l_rearWheelBase";
	rename -uid "4727A806-40EE-2A68-44C6-43AE3285964F";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
createNode transform -n "GRP_offset_l_rearWheel_top_01" -p "GRP_deformer_l_rearWheel";
	rename -uid "4B3CED29-4E2E-1B97-F3EA-FE80945749EF";
	setAttr ".t" -type "double3" 40.219730377197877 40.514984130859389 40.50540612792966 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_l_rearWheel_top_01" -p "GRP_offset_l_rearWheel_top_01";
	rename -uid "F7E1E0DD-4EAA-211A-B205-B79DB367271E";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_l_rearWheel_top_01Shape" -p "CNT_l_rearWheel_top_01";
	rename -uid "1896001E-4010-3818-720E-DE818879FCF1";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 7.3288171367660118 8.249164751287541
		-2.0944897810900562e-15 -6.1941724750157157 5.1324091659419322
		0 4.2404843241591106 -5.2744526555926896
		5.2362244527251404e-16 7.3288171367660118 8.249164751287541
		;
createNode transform -n "GRP_offset_l_rearWheel_top_02" -p "GRP_deformer_l_rearWheel";
	rename -uid "106B6361-48E9-D73E-8D68-5095952C982C";
	setAttr ".t" -type "double3" 77.116271972656861 40.514984130859389 40.50540612792966 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_l_rearWheel_top_02" -p "GRP_offset_l_rearWheel_top_02";
	rename -uid "C65D9E94-436A-AD25-42FE-AEA8591F99B3";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_l_rearWheel_top_02Shape" -p "CNT_l_rearWheel_top_02";
	rename -uid "CEBB29C1-4AD7-1A19-7465-78A7E128A333";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 7.3288171367660118 8.249164751287541
		-2.0944897810900562e-15 -6.1941724750157157 5.1324091659419322
		0 4.2404843241591106 -5.2744526555926896
		5.2362244527251404e-16 7.3288171367660118 8.249164751287541
		;
createNode transform -n "GRP_offset_l_rearWheel_top_03" -p "GRP_deformer_l_rearWheel";
	rename -uid "26E0D81F-487A-4024-3E66-B8963DC4BED6";
	setAttr ".t" -type "double3" 77.116271972656861 40.514984130859389 -40.486994995117215 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_l_rearWheel_top_03" -p "GRP_offset_l_rearWheel_top_03";
	rename -uid "92E8DC26-4E25-00FD-38E0-C6944D9BEAD0";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_l_rearWheel_top_03Shape" -p "CNT_l_rearWheel_top_03";
	rename -uid "87837028-4026-CE13-BD42-F8B97C7FD7EC";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 8.249164751287541 -7.3288171367660127
		-2.0944897810900562e-15 5.1324091659419313 6.1941724750157157
		0 -5.2744526555926896 -4.2404843241591106
		5.2362244527251404e-16 8.249164751287541 -7.3288171367660127
		;
createNode transform -n "GRP_offset_l_rearWheel_top_04" -p "GRP_deformer_l_rearWheel";
	rename -uid "CCCFACDF-46AB-98A3-A338-F6A8E741B0C7";
	setAttr ".t" -type "double3" 40.219730377197877 40.514984130859389 -40.486994995117215 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_l_rearWheel_top_04" -p "GRP_offset_l_rearWheel_top_04";
	rename -uid "99B6BE5A-424F-D9AD-9322-CB9C4E578946";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_l_rearWheel_top_04Shape" -p "CNT_l_rearWheel_top_04";
	rename -uid "AD2367BE-4786-5EF1-383E-338275E90C0F";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 8.249164751287541 -7.3288171367660127
		-2.0944897810900562e-15 5.1324091659419313 6.1941724750157157
		0 -5.2744526555926896 -4.2404843241591106
		5.2362244527251404e-16 8.249164751287541 -7.3288171367660127
		;
createNode transform -n "GRP_offset_l_rearWheel_bot_01" -p "GRP_deformer_l_rearWheel";
	rename -uid "E8F53401-4C86-65C4-C466-AC9CCB74528B";
	setAttr ".t" -type "double3" 40.219730377197877 -40.477415561676011 40.50540612792966 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_l_rearWheel_bot_01" -p "GRP_offset_l_rearWheel_bot_01";
	rename -uid "CEF65D17-4412-947E-9A21-73B0ED797646";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_l_rearWheel_bot_01Shape" -p "CNT_l_rearWheel_bot_01";
	rename -uid "1B53D32D-4F64-B6CD-A3FC-67A0A2A6B65A";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 -8.2491647512875392 7.3288171367660118
		-2.0944897810900562e-15 -5.1324091659419331 -6.1941724750157157
		0 5.2744526555926896 4.2404843241591106
		5.2362244527251404e-16 -8.2491647512875392 7.3288171367660118
		;
createNode transform -n "GRP_offset_l_rearWheel_bot_02" -p "GRP_deformer_l_rearWheel";
	rename -uid "C4AB471A-46FA-0CD2-5142-8387D1844CE4";
	setAttr ".t" -type "double3" 77.116271972656861 -40.477415561676011 40.50540612792966 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_l_rearWheel_bot_02" -p "GRP_offset_l_rearWheel_bot_02";
	rename -uid "FE887A96-4ADA-41C6-785F-A79525D7C5D8";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_l_rearWheel_bot_02Shape" -p "CNT_l_rearWheel_bot_02";
	rename -uid "EB2BFF40-4CD7-D8CC-A9A0-5FBDA1426C03";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 -8.2491647512875392 7.3288171367660118
		-2.0944897810900562e-15 -5.1324091659419331 -6.1941724750157157
		0 5.2744526555926896 4.2404843241591106
		5.2362244527251404e-16 -8.2491647512875392 7.3288171367660118
		;
createNode transform -n "GRP_offset_l_rearWheel_bot_03" -p "GRP_deformer_l_rearWheel";
	rename -uid "8741DE1C-419D-F2F4-4553-D3BED50895AE";
	setAttr ".t" -type "double3" 77.116271972656861 -40.477415561676011 -40.486994995117215 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_l_rearWheel_bot_03" -p "GRP_offset_l_rearWheel_bot_03";
	rename -uid "9E4A2ACF-4961-6A1A-9B08-07AA3623332D";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_l_rearWheel_bot_03Shape" -p "CNT_l_rearWheel_bot_03";
	rename -uid "835F0290-43BF-A48A-AB7C-4A991926A9D5";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 -7.3288171367660144 -8.249164751287541
		-2.0944897810900562e-15 6.1941724750157174 -5.1324091659419322
		0 -4.2404843241591106 5.2744526555926896
		5.2362244527251404e-16 -7.3288171367660144 -8.249164751287541
		;
createNode transform -n "GRP_offset_l_rearWheel_bot_04" -p "GRP_deformer_l_rearWheel";
	rename -uid "7E3BFF91-45F8-B50A-62AA-96A2F75A1813";
	setAttr ".t" -type "double3" 40.219730377197877 -40.477415561676011 -40.486994995117215 ;
	setAttr ".rp" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr ".sp" -type "double3" 0 -3.3306690738754696e-16 0 ;
createNode transform -n "CNT_l_rearWheel_bot_04" -p "GRP_offset_l_rearWheel_bot_04";
	rename -uid "9913F2FD-47C3-53AE-90D2-D88A7F2CC2E1";
	setAttr -l on -k off ".v";
createNode nurbsCurve -n "CNT_l_rearWheel_bot_04Shape" -p "CNT_l_rearWheel_bot_04";
	rename -uid "A7AF53B8-41A3-C8A7-0E3D-8AB792E5107A";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 6 7 15 16
		4
		5.2362244527251404e-16 -7.3288171367660144 -8.249164751287541
		-2.0944897810900562e-15 6.1941724750157174 -5.1324091659419322
		0 -4.2404843241591106 5.2744526555926896
		5.2362244527251404e-16 -7.3288171367660144 -8.249164751287541
		;
createNode transform -n "l_rearWheel_display" -p "GRP_l_rearWheel_reset";
	rename -uid "EF3FEBBD-4D2D-388D-CD1D-7CB3D8FBC24A";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".s" -type "double3" 1.0000000000000011 1 1.0000000000000011 ;
createNode nurbsCurve -n "l_rearWheel_displayShape" -p "l_rearWheel_display";
	rename -uid "2C99B515-4D77-A88C-7655-6F8FC4AB65FE";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode parentConstraint -n "l_rearWheel_display_parentConstraint1" -p "l_rearWheel_display";
	rename -uid "0E5C6BDA-4F17-7D90-4855-AFB60215FAF3";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_rearWheel_jtW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 1.2747136679536197e-11 -3.1810998279979685e-11 
		3.865352482534945e-11 ;
	setAttr ".tg[0].tor" -type "double3" 0 89.999999999999986 0 ;
	setAttr ".lr" -type "double3" 0 89.999999999999986 0 ;
	setAttr ".rst" -type "double3" 88.130042686900012 35.000000002700013 -137.03581848450006 ;
	setAttr ".rsrr" -type "double3" 0 89.999999999999986 0 ;
	setAttr -k on ".w0";
createNode transform -n "GRP_offset_pivot_locators" -p "GRP_offset_Pivotdrive";
	rename -uid "FC604393-4700-E2D7-C87D-78BCCF6CB0AE";
createNode transform -n "GRP_PivotBank_R" -p "GRP_offset_pivot_locators";
	rename -uid "D7C19691-473F-8B82-5C52-E6969C9C40DF";
	setAttr ".rp" -type "double3" -88.3594970703125 0.11792007088661194 121.64077758789062 ;
	setAttr ".sp" -type "double3" -88.3594970703125 0.11792007088661194 121.64077758789062 ;
createNode transform -n "LOC_PivotBank_R" -p "GRP_PivotBank_R";
	rename -uid "9AD2D673-4B79-160D-CDA5-81988A021D7A";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" -88.3594970703125 0.11792007088661194 121.64077758789062 ;
createNode locator -n "LOC_PivotBank_RShape" -p "LOC_PivotBank_R";
	rename -uid "040BA69C-48F4-C9A6-4301-9789C1456B8E";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 10 10 10 ;
createNode transform -n "GRP_PivotBank_L" -p "GRP_offset_pivot_locators";
	rename -uid "8DE6EAC6-4909-F57B-68E2-A482EE10A003";
	setAttr ".rp" -type "double3" 88.3594970703125 0.11792066693305969 121.64077758789062 ;
	setAttr ".sp" -type "double3" 88.3594970703125 0.11792066693305969 121.64077758789062 ;
createNode transform -n "LOC_PivotBank_L" -p "GRP_PivotBank_L";
	rename -uid "D70FF4A5-4968-5557-04F1-FB92D770A7A2";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 88.3594970703125 0.11792066693305969 121.64077758789062 ;
createNode locator -n "LOC_PivotBank_LShape" -p "LOC_PivotBank_L";
	rename -uid "1562CC21-4B1C-85B6-5FC9-308C39C0E1E1";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 10 10 10 ;
createNode parentConstraint -n "GRP_offset_pivot_locators_parentConstraint1" -p "GRP_offset_pivot_locators";
	rename -uid "CCA1158E-4E3E-CD32-2F35-4CB8E24FFBDE";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "CNT_rearAxisW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 6.1106675275368616e-13 1.7763568394002505e-14 
		137.03598474121094 ;
	setAttr ".rst" -type "double3" 0 0 -1.52587890625e-05 ;
	setAttr -k on ".w0";
createNode nurbsCurve -n "Main_CtrlShape" -p "Main_Ctrl";
	rename -uid "76417280-4773-CA9C-73BA-1C97D7EFB2EC";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovc" 17;
	setAttr ".ovrgb" -type "float3" 0.94729447 0.14126594 0.43414786 ;
	setAttr ".cc" -type "nurbsCurve" 
		3 13 0 no 3
		18 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 13 13
		16
		-132.97967688482197 0 -86.340153431297054
		-132.97967688482197 0 -86.340153431297054
		-166.20822715469575 0 -3.2012365102278283e-05
		-166.20822715469575 0 -3.2012365102278283e-05
		-99.697010245169011 0 172.68024053751628
		-99.697010245169011 0 172.68024053751628
		99.696980533151574 0 172.68025242232281
		99.696980533151574 0 172.68025242232281
		166.20822715469575 0 -2.3003477820163237e-06
		166.20822715469575 0 -2.3003477820163237e-06
		99.697034014782844 0 -172.68023325340459
		99.697034014782844 0 -172.68023325340459
		-99.696956763537742 0 -172.68026890782545
		-99.696956763537742 0 -172.68026890782545
		-132.97967688482197 0 -86.340153431297054
		-132.97967688482197 0 -86.340153431297054
		;
createNode nurbsCurve -n "Transform_CtrlShape" -p "Transform_Ctrl";
	rename -uid "81C99106-408B-2269-7D84-ECBAD41903A8";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovc" 13;
	setAttr ".ovrgb" -type "float3" 0.5810833 1 0 ;
	setAttr ".cc" -type "nurbsCurve" 
		3 13 0 no 3
		18 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 13 13
		16
		-166.03135553283283 0 -100.72256247220042
		-166.03135553283283 0 -100.72256247220042
		-214.51692234802184 0 -3.734493530246992e-05
		-214.51692234802184 0 -3.734493530246992e-05
		-116.30438380794743 0 201.44504757099438
		-116.30438380794743 0 201.44504757099438
		116.30434914654819 0 201.44506143555364
		116.30434914654819 0 201.44506143555364
		214.51692234802184 0 -2.6835361529244184e-06
		214.51692234802184 0 -2.6835361529244184e-06
		116.30441153706673 0 -201.44503907350668
		116.30441153706673 0 -201.44503907350668
		-116.30432141742874 0 -201.44508066718564
		-116.30432141742874 0 -201.44508066718564
		-166.03135553283283 0 -100.72256247220042
		-166.03135553283283 0 -100.72256247220042
		;
createNode transform -n "GRP_Joints" -p "CTRL_setsubControl";
	rename -uid "84B550F1-4B3E-3448-FF26-C2AE0DF3C4A4";
createNode joint -n "JNT_root" -p "GRP_Joints";
	rename -uid "5FFC7F0C-4AE0-1832-199F-158DB65CACD5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".radi" 2;
createNode parentConstraint -n "JNT_root_parentConstraint1" -p "JNT_root";
	rename -uid "90A482DE-436C-EB3B-B9AE-63987DC26EE6";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "CNT_driveW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode scaleConstraint -n "JNT_root_scaleConstraint1" -p "JNT_root";
	rename -uid "F7B2F4FD-4C9B-461D-110A-16B326F22F4D";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "CNT_driveW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode joint -n "JNT_chasis" -p "JNT_root";
	rename -uid "AF0DD297-4EE4-1A0B-DFC9-F6A4C8A2651A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr -k on ".ro";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -6.1106675275368616e-13 35 -1.1368683772161603e-13 1;
	setAttr ".radi" 2;
createNode transform -n "GRP_offset_joints" -p "JNT_chasis";
	rename -uid "0A591783-408F-094A-355D-0F8803AC296B";
createNode parentConstraint -n "GRP_offset_joints_parentConstraint1" -p "GRP_offset_joints";
	rename -uid "FD81BA9E-4D3F-33CC-0B1C-B3A81066C657";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "GRP_offset_driveW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 3.0292258760486853e-28 35 0 ;
	setAttr ".rst" -type "double3" 6.1106675275368646e-13 0 1.1368683772161603e-13 ;
	setAttr -k on ".w0";
createNode transform -n "GRP_offset_body" -p "GRP_offset_joints";
	rename -uid "CB95918D-4D16-98CF-EEDA-1BB3163E1925";
createNode joint -n "JNT_body" -p "GRP_offset_body";
	rename -uid "AB4A0C56-496B-0C13-9B60-9786AACC1BB0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.1106675275368626e-13 -0.80639491357586479 -1.1368683772161603e-13 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -6.1106675275368596e-13 23.370000839233398 61.189999999999884 1;
	setAttr ".otp" -type "string" "";
	setAttr ".radi" 2;
createNode joint -n "JNT_l_frontWidth" -p "JNT_body";
	rename -uid "C3542D5E-4E97-55A9-6FDB-D79CD3B8810C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 20 7.1054273576010019e-14 121.40299987792969 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 20 35 121.40299987792956 1;
	setAttr ".otp" -type "string" "";
	setAttr ".radi" 2;
createNode joint -n "JNT_l_frontUpperArm" -p "JNT_l_frontWidth";
	rename -uid "72C4C7D1-4A11-7403-7B31-AF9EDE43386B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 10.641914452457129 22.037624481183663 2.8421709430404007e-14 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 30.641914452457129 57.037624481183663 121.4029998779296 1;
	setAttr ".otp" -type "string" "";
	setAttr ".radi" 2;
createNode joint -n "JNT_l_frontUpperSpring" -p "JNT_l_frontUpperArm";
	rename -uid "ECF531DC-4457-1D59-035B-899552866FEB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 5.119346372595146e-05 -1.2047301972017976e-05 14.597000000000008 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.53148420859986079 -0.8470682003292177 0 0 0.8470682003292177 0.53148420859986079 0 0
		 0 0 1 0 30.641965645920855 57.037612433881691 135.99999987792961 1;
	setAttr ".radi" 2;
createNode aimConstraint -n "l_frontUpperSpring_jt_aimConstraint1" -p "JNT_l_frontUpperSpring";
	rename -uid "27ADEB68-4013-1438-2303-DDBD5DCD7323";
	addAttr -dcb 0 -ci true -sn "w0" -ln "l_frontLowerSpring_jtW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" 0 0 -57.894208223832258 ;
	setAttr -k on ".w0";
createNode joint -n "JNT_l_frontArm" -p "JNT_l_frontWidth";
	rename -uid "F765C448-4AC5-582A-0ECE-2E9C0990989F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 10.642 -3.4199688002445328e-05 -1.4210854715202004e-14 ;
	setAttr ".mntl" -type "double3" 53.13 -1 -1 ;
	setAttr ".mxtl" -type "double3" 53.13 1 1 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.00086230631076583599 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999988674748 -1.5050084283021111e-05 0
		 0 1.5050084283021111e-05 0.99999999988674748 0 30.641999999999999 34.999965800311998 121.40299987792956 1;
	setAttr ".otp" -type "string" "";
	setAttr ".radi" 2;
createNode aimConstraint -n "l_frontArm_jt_aimConstraint1" -p "JNT_l_frontArm";
	rename -uid "612AED39-4D80-58D8-1740-FB93DFE126D8";
	addAttr -dcb 0 -ci true -sn "w0" -ln "l_frontBall_jtW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" 0.00086230631076583599 0 0 ;
	setAttr -k on ".w0";
createNode joint -n "JNT_l_frontLowerArm" -p "JNT_l_frontWidth";
	rename -uid "23255046-4223-CAD2-AB97-499404A83105";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 10.641864776611328 -8.1809999999999974 2.8421709430404007e-14 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 30.641864776611328 26.819000000000003 121.4029998779296 1;
	setAttr ".radi" 2;
createNode joint -n "JNT_l_frontLowerBall" -p "JNT_l_frontLowerArm";
	rename -uid "86192355-4943-25A4-2AF2-F09F42D3676E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 22.408077239990234 -5.4945428314208975 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 53.049942016601562 21.324457168579105 121.4029998779296 1;
	setAttr ".radi" 2;
createNode joint -n "JNT_l_frontLowerSpring" -p "JNT_l_frontLowerBall";
	rename -uid "DDFF504B-4ECA-6D69-8028-01A6B6B608D1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 1.9234475558960185e-06 -0.0002027978223360094 14.597000000000008 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.53148420859986079 -0.8470682003292177 0 0 0.8470682003292177 0.53148420859986079 0 0
		 0 0 1 0 53.049943940049118 21.324254370756769 135.99999987792961 1;
	setAttr ".radi" 2;
	setAttr -k on ".liw";
createNode aimConstraint -n "l_frontLowerSpring_jt_aimConstraint1" -p "JNT_l_frontLowerSpring";
	rename -uid "B111698D-4E44-AFFF-EE0E-F39D2B8932A1";
	addAttr -dcb 0 -ci true -sn "w0" -ln "l_frontUpperSpring_jtW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" -1 0 0 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" 0 0 -57.894208223832258 ;
	setAttr -k on ".w0";
createNode joint -n "JNT_r_frontWidth" -p "JNT_body";
	rename -uid "66A2A823-42F2-6AA8-451E-21A59F5D7661";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "system" -ln "system" -dt "string";
	addAttr -ci true -sn "systemType" -ln "systemType" -dt "string";
	addAttr -ci true -sn "systemTier" -ln "systemTier" -dt "string";
	addAttr -ci true -sn "systemLabel" -ln "systemLabel" -dt "string";
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -20 7.1054273576010019e-14 121.40299987792969 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -20 35 121.40299987792956 1;
	setAttr ".otp" -type "string" "";
	setAttr ".radi" 2;
	setAttr ".system" -type "string" "";
	setAttr ".systemType" -type "string" "";
	setAttr ".systemTier" -type "string" "";
	setAttr ".systemLabel" -type "string" "";
createNode joint -n "JNT_r_frontUpperArm" -p "JNT_r_frontWidth";
	rename -uid "41AED9CA-4826-53CF-7856-BB9C5BF77DDF";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "system" -ln "system" -dt "string";
	addAttr -ci true -sn "systemType" -ln "systemType" -dt "string";
	addAttr -ci true -sn "systemTier" -ln "systemTier" -dt "string";
	addAttr -ci true -sn "systemLabel" -ln "systemLabel" -dt "string";
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -10.6419 22.037599999999998 -1.4210854715202004e-14 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -30.6419 57.037599999999998 121.40299987792956 1;
	setAttr ".otp" -type "string" "";
	setAttr ".radi" 2;
	setAttr ".system" -type "string" "";
	setAttr ".systemType" -type "string" "";
	setAttr ".systemTier" -type "string" "";
	setAttr ".systemLabel" -type "string" "";
createNode joint -n "JNT_r_frontUpperSpring" -p "JNT_r_frontUpperArm";
	rename -uid "1A39C20B-41A9-8F82-B822-FE91210A6235";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "system" -ln "system" -dt "string";
	addAttr -ci true -sn "systemType" -ln "systemType" -dt "string";
	addAttr -ci true -sn "systemTier" -ln "systemTier" -dt "string";
	addAttr -ci true -sn "systemLabel" -ln "systemLabel" -dt "string";
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -9.9999999999766942e-05 0 14.596999999999994 ;
	setAttr ".r" -type "double3" -7.0167092985348775e-15 7.016709298534876e-15 59.144186109545728 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.53148349614714974 0.84706864734990794 0 0 -0.84706864734990794 0.53148349614714974 0 0
		 0 0 1 0 -30.641999999999999 57.037599999999998 135.99999987792955 1;
	setAttr ".radi" 2;
	setAttr ".system" -type "string" "";
	setAttr ".systemType" -type "string" "";
	setAttr ".systemTier" -type "string" "";
	setAttr ".systemLabel" -type "string" "";
createNode aimConstraint -n "r_frontUpperSpring_jt_aimConstraint1" -p "JNT_r_frontUpperSpring";
	rename -uid "1B38CF1D-4DDA-7092-782E-AD908B47B12C";
	addAttr -dcb 0 -ci true -sn "w0" -ln "r_frontLowerSpring_jtW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" -1 0 0 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" 0 0 57.89425641419362 ;
	setAttr -k on ".w0";
createNode joint -n "JNT_r_frontArm" -p "JNT_r_frontWidth";
	rename -uid "697CA02E-4F5B-5EF3-FC53-789DC44F3E85";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "system" -ln "system" -dt "string";
	addAttr -ci true -sn "systemType" -ln "systemType" -dt "string";
	addAttr -ci true -sn "systemTier" -ln "systemTier" -dt "string";
	addAttr -ci true -sn "systemLabel" -ln "systemLabel" -dt "string";
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -10.642 0 -1.4210854715202004e-14 ;
	setAttr ".mntl" -type "double3" 53.13 -1 -1 ;
	setAttr ".mxtl" -type "double3" 53.13 1 1 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -30.641999999999999 35 121.40299987792956 1;
	setAttr ".otp" -type "string" "";
	setAttr ".radi" 2;
	setAttr ".system" -type "string" "";
	setAttr ".systemType" -type "string" "";
	setAttr ".systemTier" -type "string" "";
	setAttr ".systemLabel" -type "string" "";
createNode aimConstraint -n "r_frontArm_jt_aimConstraint1" -p "JNT_r_frontArm";
	rename -uid "B11322D1-4005-C15A-4902-369CD90705EB";
	addAttr -dcb 0 -ci true -sn "w0" -ln "r_frontBall_jtW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" -1 0 0 ;
	setAttr ".wut" 1;
	setAttr -k on ".w0";
createNode joint -n "JNT_r_frontLowerArm" -p "JNT_r_frontWidth";
	rename -uid "5291D312-4C25-9D4C-1E4A-0C8BE74929B5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "system" -ln "system" -dt "string";
	addAttr -ci true -sn "systemType" -ln "systemType" -dt "string";
	addAttr -ci true -sn "systemTier" -ln "systemTier" -dt "string";
	addAttr -ci true -sn "systemLabel" -ln "systemLabel" -dt "string";
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -10.6419 -8.1810000000000009 -1.4210854715202004e-14 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -30.6419 26.818999999999999 121.40299987792956 1;
	setAttr ".radi" 2;
	setAttr ".system" -type "string" "";
	setAttr ".systemType" -type "string" "";
	setAttr ".systemTier" -type "string" "";
	setAttr ".systemLabel" -type "string" "";
createNode joint -n "JNT_r_frontLowerBall" -p "JNT_r_frontLowerArm";
	rename -uid "DB2D3335-4729-15BE-2350-BC903B2E17FE";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "system" -ln "system" -dt "string";
	addAttr -ci true -sn "systemType" -ln "systemType" -dt "string";
	addAttr -ci true -sn "systemTier" -ln "systemTier" -dt "string";
	addAttr -ci true -sn "systemLabel" -ln "systemLabel" -dt "string";
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -22.408 -5.4944999999999986 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -53.049900000000001 21.3245 121.40299987792956 1;
	setAttr ".radi" 2;
	setAttr ".system" -type "string" "";
	setAttr ".systemType" -type "string" "";
	setAttr ".systemTier" -type "string" "";
	setAttr ".systemLabel" -type "string" "";
createNode joint -n "JNT_r_frontLowerSpring" -p "JNT_r_frontLowerBall";
	rename -uid "2C6CB955-42E3-FDDE-0D23-80AC4562C6F7";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "system" -ln "system" -dt "string";
	addAttr -ci true -sn "systemType" -ln "systemType" -dt "string";
	addAttr -ci true -sn "systemTier" -ln "systemTier" -dt "string";
	addAttr -ci true -sn "systemLabel" -ln "systemLabel" -dt "string";
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0 -0.00019999999999953388 14.596999999999994 ;
	setAttr ".r" -type "double3" 7.0167033969176964e-15 7.0167092985323958e-15 57.090502590182325 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.53148349614714974 0.84706864734990794 0 0 -0.84706864734990794 0.53148349614714974 0 0
		 0 0 1 0 -53.049900000000001 21.324300000000001 135.99999987792955 1;
	setAttr ".radi" 2;
	setAttr ".system" -type "string" "";
	setAttr ".systemType" -type "string" "";
	setAttr ".systemTier" -type "string" "";
	setAttr ".systemLabel" -type "string" "";
createNode aimConstraint -n "r_frontLowerSpring_jt_aimConstraint1" -p "JNT_r_frontLowerSpring";
	rename -uid "D483F935-44DA-0B04-FD5C-06A2E98C0926";
	addAttr -dcb 0 -ci true -sn "w0" -ln "r_frontUpperSpring_jtW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" 0 0 57.89425641419362 ;
	setAttr -k on ".w0";
createNode joint -n "JNT_l_rearWidth" -p "JNT_body";
	rename -uid "FB234596-42EE-E7B2-DDDD-418E8AA4D986";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 20 0 -137.036 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 20 35 -137.03600000000012 1;
	setAttr ".otp" -type "string" "";
	setAttr ".radi" 2;
createNode joint -n "JNT_l_rearUpperArm" -p "JNT_l_rearWidth";
	rename -uid "89FCD5E7-477C-3DC4-1E32-2B9548FB6FC1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 10.641914452457129 22.037624481183663 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 30.641914452457129 57.037624481183663 -137.03600000000012 1;
	setAttr ".otp" -type "string" "";
	setAttr ".radi" 2;
createNode joint -n "JNT_l_rearUpperSpring" -p "JNT_l_rearUpperArm";
	rename -uid "F8F8351E-4A72-E883-EAE7-AB98958C8A3F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 5.119346372595146e-05 -1.2047301972017976e-05 14.597000000000037 ;
	setAttr ".r" -type "double3" -1.201680184261872e-14 2.172616355175609e-14 -59.144185146033294 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.53148420859986079 -0.8470682003292177 0 0 0.8470682003292177 0.53148420859986079 0 0
		 0 0 1 0 30.641965645920855 57.037612433881691 -122.43900000000008 1;
	setAttr ".radi" 2;
createNode aimConstraint -n "l_rearUpperSpring_jt_aimConstraint1" -p "JNT_l_rearUpperSpring";
	rename -uid "E33AFC25-462D-8C5C-5A28-B38A90E5B8BB";
	addAttr -dcb 0 -ci true -sn "w0" -ln "l_rearLowerSpring_jtW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" -3.2044804913649911e-14 5.793643613801624e-14 -57.894208223832258 ;
	setAttr -k on ".w0";
createNode joint -n "JNT_l_rearArm" -p "JNT_l_rearWidth";
	rename -uid "5B3D1DE6-4AEC-E2C8-48D1-72B782CA4775";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 10.642 -3.4199688002445328e-05 0 ;
	setAttr ".r" -type "double3" 0.00086230631076583599 0 2.0537667318371104 ;
	setAttr ".mntl" -type "double3" 53.13 -1 -1 ;
	setAttr ".mxtl" -type "double3" 53.13 1 1 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.00086230631076583599 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 30.641999999999999 34.999965800311998 -137.03600000000012 1;
	setAttr ".otp" -type "string" "";
	setAttr ".radi" 2;
createNode aimConstraint -n "l_rearArm_jt_aimConstraint1" -p "JNT_l_rearArm";
	rename -uid "8AF29FA2-4689-E65D-29B7-E0B0A1B38804";
	addAttr -dcb 0 -ci true -sn "w0" -ln "l_reartBall_jtW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" 0.00086230631076583599 0 0 ;
	setAttr -k on ".w0";
createNode joint -n "JNT_l_rearLowerArm" -p "JNT_l_rearWidth";
	rename -uid "E3B11AE3-446A-96E9-F220-E0B6DDADE1EB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 10.641864776611328 -8.1809999999999974 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 30.641864776611328 26.819000000000003 -137.03600000000012 1;
	setAttr ".radi" 2;
createNode joint -n "JNT_l_rearLowerBall" -p "JNT_l_rearLowerArm";
	rename -uid "1CC181C6-41AA-538E-2440-D892AB30551A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 22.408077239990234 -5.4945428314208975 0 ;
	setAttr ".r" -type "double3" -1.330627941165119e-09 0 -2.0537667274475098 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 53.049942016601562 21.324457168579105 -137.03600000000012 1;
	setAttr ".radi" 2;
createNode joint -n "JNT_l_rearLowerSpring" -p "JNT_l_rearLowerBall";
	rename -uid "30C9FAE4-4E1F-9334-9D7C-BAB0D3707663";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 1.9234475558960185e-06 -0.0002027978223360094 14.596999999999994 ;
	setAttr ".r" -type "double3" -1.0681601637883305e-14 1.9312145379338743e-14 -57.090418418585806 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.53148420859986079 -0.8470682003292177 0 0 0.8470682003292177 0.53148420859986079 0 0
		 0 0 1 0 53.049943940049118 21.324254370756769 -122.43900000000012 1;
	setAttr ".radi" 2;
createNode aimConstraint -n "l_rearLowerSpring_jt_aimConstraint1" -p "JNT_l_rearLowerSpring";
	rename -uid "9A35E61A-4D72-72F8-AD62-4195E7893BBE";
	addAttr -dcb 0 -ci true -sn "w0" -ln "l_rearUpperSpring_jtW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" -1 0 0 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" -3.2044804913649911e-14 5.793643613801624e-14 -57.894208223832258 ;
	setAttr -k on ".w0";
createNode joint -n "JNT_r_rearWidth" -p "JNT_body";
	rename -uid "9BF96652-48CB-C690-49C1-0692386DF3D1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "system" -ln "system" -dt "string";
	addAttr -ci true -sn "systemType" -ln "systemType" -dt "string";
	addAttr -ci true -sn "systemTier" -ln "systemTier" -dt "string";
	addAttr -ci true -sn "systemLabel" -ln "systemLabel" -dt "string";
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -20 0 -137.03600000000009 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -20 35 -137.03600000000012 1;
	setAttr ".otp" -type "string" "";
	setAttr ".radi" 2;
	setAttr ".system" -type "string" "";
	setAttr ".systemType" -type "string" "";
	setAttr ".systemTier" -type "string" "";
	setAttr ".systemLabel" -type "string" "";
createNode joint -n "JNT_r_rearUpperArm" -p "JNT_r_rearWidth";
	rename -uid "B064F7B5-4A4A-05A6-69AC-728C774ACEFB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "system" -ln "system" -dt "string";
	addAttr -ci true -sn "systemType" -ln "systemType" -dt "string";
	addAttr -ci true -sn "systemTier" -ln "systemTier" -dt "string";
	addAttr -ci true -sn "systemLabel" -ln "systemLabel" -dt "string";
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -10.6419 22.037599999999998 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -30.6419 57.037599999999998 -137.03600000000012 1;
	setAttr ".otp" -type "string" "";
	setAttr ".radi" 2;
	setAttr ".system" -type "string" "";
	setAttr ".systemType" -type "string" "";
	setAttr ".systemTier" -type "string" "";
	setAttr ".systemLabel" -type "string" "";
createNode joint -n "JNT_r_rearUpperSpring" -p "JNT_r_rearUpperArm";
	rename -uid "89428FDE-48C8-889E-8B27-59BB34ADA1A4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "system" -ln "system" -dt "string";
	addAttr -ci true -sn "systemType" -ln "systemType" -dt "string";
	addAttr -ci true -sn "systemTier" -ln "systemTier" -dt "string";
	addAttr -ci true -sn "systemLabel" -ln "systemLabel" -dt "string";
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -9.9999999999766942e-05 0 14.596999999999994 ;
	setAttr ".r" -type "double3" -7.0167152001518954e-15 7.0167092985323942e-15 59.144186109545856 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.53148349614714974 0.84706864734990794 0 0 -0.84706864734990794 0.53148349614714974 0 0
		 0 0 1 0 -30.641999999999999 57.037599999999998 -122.43900000000012 1;
	setAttr ".radi" 2;
	setAttr ".system" -type "string" "";
	setAttr ".systemType" -type "string" "";
	setAttr ".systemTier" -type "string" "";
	setAttr ".systemLabel" -type "string" "";
createNode aimConstraint -n "r_rearUpperSpring_jt_aimConstraint1" -p "JNT_r_rearUpperSpring";
	rename -uid "98616A5F-421D-381B-A7D6-5C99F6F23D9C";
	addAttr -dcb 0 -ci true -sn "w0" -ln "r_rearLowerSpring_jtW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" -1 0 0 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" 0 0 57.89425641419362 ;
	setAttr -k on ".w0";
createNode joint -n "JNT_r_rearArm" -p "JNT_r_rearWidth";
	rename -uid "5037B54D-4D34-0513-1195-B2AE391F2D95";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "system" -ln "system" -dt "string";
	addAttr -ci true -sn "systemType" -ln "systemType" -dt "string";
	addAttr -ci true -sn "systemTier" -ln "systemTier" -dt "string";
	addAttr -ci true -sn "systemLabel" -ln "systemLabel" -dt "string";
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -10.642 0 0 ;
	setAttr ".mntl" -type "double3" 53.13 -1 -1 ;
	setAttr ".mxtl" -type "double3" 53.13 1 1 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -30.641999999999999 35 -137.03600000000012 1;
	setAttr ".otp" -type "string" "";
	setAttr ".radi" 2;
	setAttr ".system" -type "string" "";
	setAttr ".systemType" -type "string" "";
	setAttr ".systemTier" -type "string" "";
	setAttr ".systemLabel" -type "string" "";
createNode aimConstraint -n "r_rearArm_jt_aimConstraint1" -p "JNT_r_rearArm";
	rename -uid "7D451836-4956-802F-14E6-2CB294B9E9A9";
	addAttr -dcb 0 -ci true -sn "w0" -ln "r_rearBall_jtW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" -1 0 0 ;
	setAttr ".wut" 1;
	setAttr -k on ".w0";
createNode joint -n "JNT_r_rearLowerArm" -p "JNT_r_rearWidth";
	rename -uid "618FD994-4701-7BD2-1B93-2EBFF724DD0C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "system" -ln "system" -dt "string";
	addAttr -ci true -sn "systemType" -ln "systemType" -dt "string";
	addAttr -ci true -sn "systemTier" -ln "systemTier" -dt "string";
	addAttr -ci true -sn "systemLabel" -ln "systemLabel" -dt "string";
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -10.6419 -8.1810000000000009 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -30.6419 26.818999999999999 -137.03600000000012 1;
	setAttr ".radi" 2;
	setAttr ".system" -type "string" "";
	setAttr ".systemType" -type "string" "";
	setAttr ".systemTier" -type "string" "";
	setAttr ".systemLabel" -type "string" "";
createNode joint -n "JNT_r_rearLowerBall" -p "JNT_r_rearLowerArm";
	rename -uid "8BD7CE85-472A-C43C-0D93-A797168D0F58";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "system" -ln "system" -dt "string";
	addAttr -ci true -sn "systemType" -ln "systemType" -dt "string";
	addAttr -ci true -sn "systemTier" -ln "systemTier" -dt "string";
	addAttr -ci true -sn "systemLabel" -ln "systemLabel" -dt "string";
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -22.408 -5.4944999999999986 0 ;
	setAttr ".r" -type "double3" -1.330627941165119e-09 0 2.0536835193634033 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -53.049900000000001 21.3245 -137.03600000000012 1;
	setAttr ".radi" 2;
	setAttr ".system" -type "string" "";
	setAttr ".systemType" -type "string" "";
	setAttr ".systemTier" -type "string" "";
	setAttr ".systemLabel" -type "string" "";
createNode joint -n "JNT_r_rearLowerSpring" -p "JNT_r_rearLowerBall";
	rename -uid "E44F9E8F-4387-0D51-9BBD-649CBE4EB74D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "system" -ln "system" -dt "string";
	addAttr -ci true -sn "systemType" -ln "systemType" -dt "string";
	addAttr -ci true -sn "systemTier" -ln "systemTier" -dt "string";
	addAttr -ci true -sn "systemLabel" -ln "systemLabel" -dt "string";
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0 -0.00019999999999953388 14.596999999999994 ;
	setAttr ".r" -type "double3" 7.0167033969176964e-15 7.0167092985323958e-15 57.09050259018246 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.53148349614714974 0.84706864734990794 0 0 -0.84706864734990794 0.53148349614714974 0 0
		 0 0 1 0 -53.049900000000001 21.324300000000001 -122.43900000000012 1;
	setAttr ".radi" 2;
	setAttr ".system" -type "string" "";
	setAttr ".systemType" -type "string" "";
	setAttr ".systemTier" -type "string" "";
	setAttr ".systemLabel" -type "string" "";
createNode aimConstraint -n "r_rearLowerSpring_jt_aimConstraint1" -p "JNT_r_rearLowerSpring";
	rename -uid "482DB3BB-4908-2448-D230-70BD331C2152";
	addAttr -dcb 0 -ci true -sn "w0" -ln "r_rearUpperSpring_jtW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" 0 0 57.89425641419362 ;
	setAttr -k on ".w0";
createNode joint -n "JNT_l_frontBall" -p "GRP_offset_body";
	rename -uid "A807328F-4456-759A-CA6B-DAA0AF023F5F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".mntl" -type "double3" 53.13 -1 -1 ;
	setAttr ".mxtl" -type "double3" 53.13 1 1 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.00086230631076583599 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999988674748 -1.5050084283021111e-05 0
		 0 1.5050084283021111e-05 0.99999999988674748 0 53.130042686886618 35 121.40281850581465 1;
	setAttr ".otp" -type "string" "";
	setAttr ".radi" 2;
createNode joint -n "JNT_l_frontWheel" -p "JNT_l_frontBall";
	rename -uid "1485BDC7-42BF-F23F-C193-64B6AB3F9200";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 35 3.4196956470111672e-05 0.00018149469994455103 ;
	setAttr ".mnrl" -type "double3" -360 -360 0 ;
	setAttr ".mxrl" -type "double3" 360 360 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999988674748 -1.5050084283021111e-05 0
		 0 1.5050084283021111e-05 0.99999999988674748 0 88.130042686886611 35.000034199687974 121.40299999999991 1;
	setAttr ".radi" 2;
createNode joint -n "JNT_l_steering" -p "JNT_l_frontBall";
	rename -uid "BDF88D05-4780-7F2D-85DF-268B14D1C1D6";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -4.4570342936367524e-06 7.1054273576010019e-15 -26.639747619031908 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999988674748 -1.5050084283021112e-05 0
		 0 1.5050084283021112e-05 0.99999999988674748 0 53.130038229852921 34.999599069553057 94.763070889799778 1;
	setAttr ".radi" 2;
createNode parentConstraint -n "l_frontBall_jt_parentConstraint1" -p "JNT_l_frontBall";
	rename -uid "8A419E0A-48EC-715B-2C2B-8483FDCF553E";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_frontSteering_ptW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 4.2686887205434232e-05 3.4199687981129046e-05 
		-5.6843418860808015e-14 ;
	setAttr ".tg[0].tor" -type "double3" -0.00086230631076583599 0 0 ;
	setAttr ".rst" -type "double3" 53.130042686887208 -7.1054273576010019e-15 121.40281850581478 ;
	setAttr -k on ".w0";
createNode joint -n "JNT_r_frontBall" -p "GRP_offset_body";
	rename -uid "BD099E90-46B7-58E7-72BC-70BA91D674A3";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "system" -ln "system" -dt "string";
	addAttr -ci true -sn "systemType" -ln "systemType" -dt "string";
	addAttr -ci true -sn "systemTier" -ln "systemTier" -dt "string";
	addAttr -ci true -sn "systemLabel" -ln "systemLabel" -dt "string";
	setAttr ".mntl" -type "double3" 53.13 -1 -1 ;
	setAttr ".mxtl" -type "double3" 53.13 1 1 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.00086230631078468094 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999988674748 -1.5050084283350017e-05 0
		 0 1.5050084283350017e-05 0.99999999988674748 0 -53.130000000000578 35 121.40300000000005 1;
	setAttr ".otp" -type "string" "";
	setAttr ".radi" 2;
	setAttr ".system" -type "string" "";
	setAttr ".systemType" -type "string" "";
	setAttr ".systemTier" -type "string" "";
	setAttr ".systemLabel" -type "string" "";
createNode joint -n "JNT_r_frontWheel" -p "JNT_r_frontBall";
	rename -uid "5840444D-46A6-9669-26F4-04BF83B7E89A";
	addAttr -ci true -sn "system" -ln "system" -dt "string";
	addAttr -ci true -sn "systemType" -ln "systemType" -dt "string";
	addAttr -ci true -sn "systemTier" -ln "systemTier" -dt "string";
	addAttr -ci true -sn "systemLabel" -ln "systemLabel" -dt "string";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -35 -3.5526922909420271e-14 0.00033166724939803717 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999988674748 -1.5050084283350017e-05 0
		 0 1.5050084283350017e-05 0.99999999988674748 0 -88.130000000000578 35.000000004991584 121.4033316672494 1;
	setAttr ".radi" 2;
	setAttr ".system" -type "string" "";
	setAttr ".systemType" -type "string" "";
	setAttr ".systemTier" -type "string" "";
	setAttr ".systemLabel" -type "string" "";
createNode joint -n "JNT_r_steering" -p "JNT_r_frontBall";
	rename -uid "2C1BF521-4526-A8B6-6E8B-9FB0CCF321CB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "system" -ln "system" -dt "string";
	addAttr -ci true -sn "systemType" -ln "systemType" -dt "string";
	addAttr -ci true -sn "systemTier" -ln "systemTier" -dt "string";
	addAttr -ci true -sn "systemLabel" -ln "systemLabel" -dt "string";
	setAttr ".t" -type "double3" 2.1316282072803006e-14 9.3274034895785007e-07 -26.639900003003035 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999988674748 -1.5050084283350015e-05 0
		 0 1.5050084283350015e-05 0.99999999988674748 0 -53.129999999999981 34.999600000000001 94.76309999999998 1;
	setAttr ".radi" 2;
	setAttr ".system" -type "string" "";
	setAttr ".systemType" -type "string" "";
	setAttr ".systemTier" -type "string" "";
	setAttr ".systemLabel" -type "string" "";
createNode parentConstraint -n "r_frontBall_jt_parentConstraint1" -p "JNT_r_frontBall";
	rename -uid "7F2EABCA-4BB1-3DF2-63F6-46A9D925B8A7";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_frontSteering_ptW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 1.4210854715202004e-14 3.4199687981129046e-05 
		0.00018149418528423666 ;
	setAttr ".tg[0].tor" -type "double3" -0.00086230631078468094 0 0 ;
	setAttr ".rst" -type "double3" -53.129999999999988 -7.1054273576010019e-15 121.40300000000012 ;
	setAttr -k on ".w0";
createNode joint -n "JNT_l_reartBall" -p "GRP_offset_body";
	rename -uid "500F603E-40D1-BBC0-BC2A-A9A683DDFBE1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mntl" -type "double3" 53.13 -1 -1 ;
	setAttr ".mxtl" -type "double3" 53.13 1 1 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.00086230631076583599 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999988674748 -1.5050084283021111e-05 0
		 0 1.5050084283021111e-05 0.99999999988674748 0 53.130042686887258 35 -137.03600000000014 1;
	setAttr ".otp" -type "string" "";
	setAttr ".radi" 2;
createNode joint -n "JNT_l_rearWheel" -p "JNT_l_reartBall";
	rename -uid "08A06919-4770-E3FF-FA67-CEA72E98B9F8";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 35 -4.2774912239511623e-19 0.00018151546149169917 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0.00086230631076583599 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 88.130042686887265 35.000000002731824 -137.03581848453868 1;
	setAttr ".radi" 2;
createNode parentConstraint -n "l_reartBall_jt_parentConstraint1" -p "JNT_l_reartBall";
	rename -uid "879F3473-4A1F-C7FE-5FEA-C58D86F26062";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_rearWheel_ctrlW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -24.973319501712773 35 -0.00024937511940947843 ;
	setAttr ".tg[0].tor" -type "double3" -0.00086230631076583599 0 0 ;
	setAttr ".rst" -type "double3" 53.130042686887258 0 -137.03600000000003 ;
	setAttr -k on ".w0";
createNode joint -n "JNT_r_rearBall" -p "GRP_offset_body";
	rename -uid "FB1D5B3C-406E-AE44-433F-A29215E94A5F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "system" -ln "system" -dt "string";
	addAttr -ci true -sn "systemType" -ln "systemType" -dt "string";
	addAttr -ci true -sn "systemTier" -ln "systemTier" -dt "string";
	addAttr -ci true -sn "systemLabel" -ln "systemLabel" -dt "string";
	setAttr ".mntl" -type "double3" 53.13 -1 -1 ;
	setAttr ".mxtl" -type "double3" 53.13 1 1 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.00086230631078468094 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999988674748 -1.5050084283350017e-05 0
		 0 1.5050084283350017e-05 0.99999999988674748 0 -53.130000000000031 35 -137.03600000000014 1;
	setAttr ".otp" -type "string" "";
	setAttr ".radi" 2;
	setAttr ".system" -type "string" "";
	setAttr ".systemType" -type "string" "";
	setAttr ".systemTier" -type "string" "";
	setAttr ".systemLabel" -type "string" "";
createNode joint -n "JNT_r_rearWheel" -p "JNT_r_rearBall";
	rename -uid "D8E2B7EC-4A29-2AD3-FB1B-21AA567BBBA9";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "system" -ln "system" -dt "string";
	addAttr -ci true -sn "systemType" -ln "systemType" -dt "string";
	addAttr -ci true -sn "systemTier" -ln "systemTier" -dt "string";
	addAttr -ci true -sn "systemLabel" -ln "systemLabel" -dt "string";
	setAttr ".t" -type "double3" -35 0 2.1873262312510633e-08 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0.00086230631078468094 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -88.130000000000024 35.000000000000327 -137.03599997812688 1;
	setAttr ".radi" 2;
	setAttr ".system" -type "string" "";
	setAttr ".systemType" -type "string" "";
	setAttr ".systemTier" -type "string" "";
	setAttr ".systemLabel" -type "string" "";
createNode parentConstraint -n "r_rearBall_jt_parentConstraint1" -p "JNT_r_rearBall";
	rename -uid "A773810D-4F1D-BD0C-20EF-A29891A7F36B";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_rearWheel_ctrlW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 24.973362188599999 35 -0.00024937511940947843 ;
	setAttr ".tg[0].tor" -type "double3" -0.00086230631078468094 0 0 ;
	setAttr ".rst" -type "double3" -53.130000000000031 0 -137.03600000000003 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "JNT_chasis_parentConstraint1" -p "JNT_chasis";
	rename -uid "11226529-4F88-4EBD-0AC0-66B8549764F4";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "CNT_rearAxisW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 0 35.000000000000014 137.03599999999989 ;
	setAttr ".rst" -type "double3" -6.1106675275368616e-13 35 -1.1368683772161603e-13 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "JNT_chasis_scaleConstraint1" -p "JNT_chasis";
	rename -uid "822F905C-4933-AC6B-4CAB-C6AFC28BB9DF";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "CNT_rearAxisW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode joint -n "Car" -p "GRP_Joints";
	rename -uid "71A3C0A7-4BB4-36E4-7027-8181AEC57332";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -1.2082557176995579e-12 34.999999999999986 13.000001373291013 1;
createNode parentConstraint -n "Car_parentConstraint1" -p "Car";
	rename -uid "5B7DC514-4519-F921-E129-72A8CA1944B4";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "CNT_bodyW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -1.2082557176995579e-12 34.999999999999986 13.000001373291013 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "Car_scaleConstraint1" -p "Car";
	rename -uid "2161FD49-4AF4-E3EA-BFB8-219875088A50";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "CNT_bodyW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode transform -n "GRP_Meshes_grp" -p "CTRL_setsubControl";
	rename -uid "823E725C-4C01-F907-DCE1-579347F706AE";
	setAttr ".ovdt" 2;
createNode transform -n "GRP_Meshes_RM" -p "GRP_Meshes_grp";
	rename -uid "161BCA21-47EB-6DD7-0C8C-879C08AAE6A3";
	setAttr ".it" no;
createNode transform -n "GRP_Meshes_RND" -p "GRP_Meshes_grp";
	rename -uid "6FA06C94-4EA0-AABF-7368-F4897B5DFD7D";
	setAttr ".it" no;
createNode transform -n "Utilities" -p "CTRL_setsubControl";
	rename -uid "97A4599E-458C-0197-FC71-A68F43ECF10A";
	setAttr ".ove" yes;
	setAttr ".it" no;
createNode transform -n "GRP_utility_meshes" -p "Utilities";
	rename -uid "3E938AAD-4925-3BFC-FFB8-369DA1E1208C";
	setAttr ".ovdt" 2;
createNode transform -n "MSH_chassis_tutorial" -p "GRP_utility_meshes";
	rename -uid "F42CA253-4D5D-24E2-2988-D480BD2B3FFB";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" 0 0.903 0 ;
	setAttr ".sp" -type "double3" 0 0.903 0 ;
createNode mesh -n "MSH_chassis_tutorialShape" -p "MSH_chassis_tutorial";
	rename -uid "A7772484-4B71-54E1-37FB-A4B3B00E3811";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".sdt" 0;
	setAttr ".ugsdt" no;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
createNode mesh -n "MSH_chassis_tutorialShapeOrig" -p "MSH_chassis_tutorial";
	rename -uid "B0658C27-4ED0-21FE-83E1-8DB1949B2001";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 436 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.32603264 0.89621222 0.33253884
		 0.89621222 0.33253884 0.91110623 0.32603264 0.91110623 0.32603264 0.8935957 0.33253884
		 0.8935957 0.33253884 0.91372275 0.32603264 0.91372275 0.33856389 0.8962124 0.35046679
		 0.8962124 0.35046679 0.91110635 0.33856389 0.91110635 0.33856389 0.89359587 0.35046679
		 0.89359587 0.35046679 0.91372293 0.33856389 0.91372293 0.36297199 0.8962124 0.36297199
		 0.91110635 0.35646585 0.91110635 0.35646585 0.8962124 0.36297199 0.91372293 0.35646585
		 0.91372293 0.35646585 0.89359587 0.36297199 0.89359587 0.3809 0.8962124 0.3809 0.91110635
		 0.3689971 0.91110635 0.3689971 0.8962124 0.3809 0.91372293 0.3689971 0.91372293 0.3689971
		 0.89359587 0.3809 0.89359587 0.38689896 0.78366476 0.3934052 0.78366476 0.3934052
		 0.79855889 0.38689896 0.79855889 0.38689896 0.78104824 0.3934052 0.78104824 0.38689896
		 0.80117542 0.3934052 0.80117542 0.39943027 0.78366476 0.41133317 0.78366476 0.41133317
		 0.79855889 0.39943027 0.79855889 0.39943027 0.78104824 0.41133317 0.78104824 0.41133317
		 0.80117542 0.39943027 0.80117542 0.42383835 0.78366464 0.42383835 0.79855871 0.41733217
		 0.79855871 0.41733217 0.78366464 0.42383835 0.80117524 0.41733217 0.80117524 0.41733217
		 0.78104806 0.42383835 0.78104806 0.44176638 0.78366464 0.44176638 0.79855871 0.42986348
		 0.79855871 0.42986348 0.78366464 0.44176638 0.80117524 0.42986348 0.80117524 0.42986348
		 0.78104806 0.44176638 0.78104806 0.44776526 0.78366464 0.45427147 0.78366464 0.45427147
		 0.79855871 0.44776526 0.79855871 0.44776526 0.78104806 0.45427147 0.78104806 0.45427147
		 0.80117524 0.44776526 0.80117524 0.46029657 0.78366464 0.4721995 0.78366464 0.4721995
		 0.79855871 0.46029657 0.79855871 0.46029657 0.78104806 0.4721995 0.78104806 0.4721995
		 0.80117524 0.46029657 0.80117524 0.48470476 0.78366464 0.48470476 0.79855871 0.4781985
		 0.79855871 0.4781985 0.78366464 0.48470476 0.80117524 0.4781985 0.80117524 0.4781985
		 0.78104806 0.48470476 0.78104806 0.50263268 0.78366464 0.50263268 0.79855871 0.49072978
		 0.79855871 0.49072978 0.78366464 0.50263268 0.80117524 0.49072978 0.80117524 0.49072978
		 0.78104806 0.50263268 0.78104806 0.50863165 0.78366464 0.51513785 0.78366464 0.51513785
		 0.79855871 0.50863165 0.79855871 0.50863165 0.78104806 0.51513785 0.78104806 0.51513785
		 0.80117524 0.50863165 0.80117524 0.52116299 0.78366464 0.53306592 0.78366464 0.53306592
		 0.79855871 0.52116299 0.79855871 0.52116299 0.78104806 0.53306592 0.78104806 0.53306592
		 0.80117524 0.52116299 0.80117524 0.54557115 0.78366464 0.54557115 0.79855871 0.53906494
		 0.79855871 0.53906494 0.78366464 0.54557115 0.80117524 0.53906494 0.80117524 0.53906494
		 0.78104806 0.54557115 0.78104806 0.56349909 0.78366476 0.56349909 0.79855889 0.55159616
		 0.79855889 0.55159616 0.78366476 0.56349909 0.80117542 0.55159616 0.80117542 0.55159616
		 0.78104824 0.56349909 0.78104824 0.73027289 0.33626306 0.71092278 0.33626306 0.71092278
		 0.017691344 0.73027289 0.017691344 0.71092278 0.3519623 0.73027289 0.3519623 0.71092278
		 0.0019920322 0.73027289 0.0019920322 0.73598528 0.017691322 0.75533563 0.017691322
		 0.75533563 0.33626151 0.73598528 0.33626151 0.75533563 0.0019920322 0.73598528 0.0019920322
		 0.75533563 0.35196081 0.73598528 0.35196081 0.82970029 0.44747993 0.7475642 0.4770593
		 0.7475642 0.35562712 0.76691431 0.35562712 0.79923266 0.53961533 0.77988237 0.53961533
		 0.7610479 0.19670723 0.7610479 0.1159241 0.7610479 0.0033765247 0.79378515 0.0019920322
		 0.79378515 0.1159241 0.78039813 0.19670723 0.8424204 0.073472425 0.84225255 0.099337205
		 0.91670829 0.32360843 0.86807311 0.39508864 0.86807311 0.28115672 0.91654044 0.29774362
		 0.83533591 0.3937043 0.83533591 0.28115672 0.83533591 0.20037353 0.85468602 0.20037353
		 0.71146089 0.62040007 0.74192858 0.71253544 0.6597923 0.6829561 0.69211066 0.62040007
		 0.67914259 0.80438828 0.6597923 0.80438828 0.24195354 0.88469625 0.23544729 0.88469625
		 0.23544829 0.73294765 0.24195449 0.73294765 0.22981079 0.88469625 0.21790791 0.88469625
		 0.2179089 0.73294765 0.22981179 0.73294753 0.20576616 0.88469625 0.2057652 0.73294765
		 0.21227136 0.73294765 0.21227241 0.88469625 0.1882268 0.88469625 0.18822585 0.73294765
		 0.20012875 0.73294765 0.2001297 0.88469625 0.1825894 0.88469625 0.17608315 0.88469625
		 0.17608415 0.73294765 0.18259035 0.73294765 0.17044665 0.88469625 0.15854377 0.88469625
		 0.15854476 0.73294765 0.17044765 0.73294753 0.14640202 0.88469625 0.14640106 0.73294765
		 0.15290722 0.73294765 0.15290827 0.88469625 0.12886266 0.88469625 0.12886171 0.73294765
		 0.14076461 0.73294765 0.14076556 0.88469625 0.12322622 0.8846963 0.11672001 0.88469625
		 0.11671945 0.73294765 0.1232257 0.73294765 0.11108395 0.88469636 0.099181011 0.8846963
		 0.09918049 0.73294771 0.11108338 0.73294765 0.087038219 0.88469625 0.087038741 0.73294765
		 0.093544997 0.73294765 0.093544476 0.88469625 0.069499195 0.8846963 0.069499806 0.73294765
		 0.081402697 0.73294765 0.081402175 0.8846963 0.063863702 0.8846963 0.057357494 0.88469625
		 0.057356928 0.73294765 0.063863181 0.73294765 0.051721431 0.88469636 0.039818499
		 0.8846963 0.039817978 0.73294771 0.051720869 0.73294765 0.027675711 0.88469625 0.02767623
		 0.73294765 0.034182481 0.73294765 0.03418196 0.88469625 0.010136711 0.8846963 0.01013732
		 0.73294765 0.022040211 0.73294765 0.022039689 0.8846963 0.22409345 0.5359506 0.22409345
		 0.61673373 0.12717427 0.61673272 0.12717427 0.53594959 0.22409345 0.72928137 0.12717427
		 0.72928029 0.12717427 0.18598081 0.22409345 0.18598029 0.017529046 0.53594959 0.017529046
		 0.18598081 0.12717427 0.064548627 0.22409345 0.064548105 0.33373868 0.18598029 0.33373868
		 0.5359506;
	setAttr ".uvst[0].uvsp[250:435]" 0.001829757 0.52025032 0.001829757 0.20168012
		 0.12717427 0.0019925968 0.22409345 0.0019920755 0.34943798 0.20167956 0.34943798
		 0.52025133 0.57872945 0.43274552 0.4818103 0.43274501 0.4818103 0.082776219 0.57872945
		 0.082775265 0.57872945 0.52459842 0.4818103 0.52459776 0.37216505 0.43274501 0.37216505
		 0.082776219 0.4818103 0.0019931179 0.57872945 0.0019920755 0.6883747 0.082775265
		 0.6883747 0.43274552 0.57872945 0.61673373 0.4818103 0.61673325 0.35646579 0.41704568
		 0.35646579 0.098475531 0.70407403 0.098474488 0.70407403 0.41704622 0.1991784 0.98570776
		 0.10225923 0.98570871 0.10225923 0.95984399 0.1991784 0.95984292 0.10225923 0.88836354
		 0.1991784 0.88836271 0.65415674 0.77476525 0.63402963 0.77476531 0.63402963 0.6230166
		 0.65415674 0.62301666 0.65154022 0.77738178 0.63664615 0.7773819 0.63664615 0.62040007
		 0.65154022 0.62040013 0.62839413 0.77476525 0.60826701 0.77476531 0.60826701 0.6230166
		 0.62839413 0.62301666 0.6257776 0.77738178 0.61088353 0.7773819 0.61088353 0.62040007
		 0.6257776 0.62040013 0.60263151 0.62301683 0.58250439 0.62301683 0.58512098 0.62040013
		 0.60001493 0.62040013 0.58250439 0.77476543 0.60263151 0.77476525 0.60001493 0.77738196
		 0.58512098 0.77738196 0.57686889 0.62301683 0.55674183 0.62301683 0.55935836 0.62040013
		 0.57425237 0.62040013 0.55674183 0.77476543 0.57686889 0.77476525 0.57425237 0.77738196
		 0.55935836 0.77738196 0.55110633 0.77476525 0.53097922 0.77476531 0.53097922 0.6230166
		 0.55110633 0.62301666 0.54848981 0.77738178 0.53359574 0.7773819 0.53359574 0.62040007
		 0.54848981 0.62040013 0.52534372 0.77476525 0.5052166 0.77476531 0.5052166 0.6230166
		 0.52534372 0.62301666 0.52272713 0.77738178 0.50783312 0.7773819 0.50783312 0.62040007
		 0.52272713 0.62040013 0.49958107 0.62301648 0.47945383 0.62301648 0.48207036 0.62040013
		 0.49696437 0.62040013 0.47945383 0.77476525 0.49958107 0.77476525 0.49696437 0.7773816
		 0.48207036 0.77738178 0.4738183 0.62301648 0.45369107 0.62301648 0.45630762 0.62040013
		 0.4712016 0.62040013 0.45369107 0.77476525 0.4738183 0.77476525 0.4712016 0.7773816
		 0.45630762 0.77738178 0.44805557 0.77476525 0.42792845 0.77476531 0.42792845 0.6230166
		 0.44805557 0.62301666 0.44543901 0.77738178 0.43054497 0.7773819 0.43054497 0.62040007
		 0.44543901 0.62040013 0.42229292 0.77476525 0.40216583 0.77476531 0.40216583 0.6230166
		 0.42229292 0.62301666 0.41967639 0.77738178 0.40478235 0.7773819 0.40478235 0.62040007
		 0.41967639 0.62040013 0.37640324 0.77476543 0.3965303 0.77476525 0.39391375 0.77738196
		 0.37901977 0.77738196 0.37640324 0.62301683 0.3965303 0.62301683 0.37901977 0.62040013
		 0.39391375 0.62040013 0.35064068 0.88731295 0.37076774 0.88731277 0.36815122 0.88992947
		 0.35325721 0.88992947 0.35064068 0.73556435 0.37076774 0.73556435 0.35325721 0.73294765
		 0.36815122 0.73294765 0.34500518 0.88731277 0.32487807 0.88731277 0.32487807 0.73556417
		 0.34500518 0.73556417 0.34238863 0.88992929 0.32749459 0.88992929 0.32749459 0.73294765
		 0.34238863 0.73294765 0.31924257 0.88731277 0.29911545 0.88731277 0.29911545 0.73556417
		 0.301732 0.73294765 0.31662604 0.73294765 0.31924257 0.73556417 0.31662604 0.88992929
		 0.301732 0.88992929 0.27335271 0.88731277 0.29347995 0.88731277 0.29086325 0.88992912
		 0.27596927 0.88992929 0.27335271 0.73556399 0.29347995 0.73556399 0.27596927 0.73294765
		 0.29086325 0.73294765 0.24758999 0.88731277 0.26771724 0.88731277 0.26510051 0.88992912
		 0.25020653 0.88992929 0.24758999 0.73556399 0.26771724 0.73556399 0.25020653 0.73294765
		 0.26510051 0.73294765 0.20609006 0.98528171 0.20609006 0.88836259 0.23882727 0.88836259
		 0.23882727 0.98528171 0.26482451 0.99051476 0.24547419 0.99051476 0.24547419 0.8935957
		 0.26482451 0.8935957 0.021179991 0.99800789 0.001829757 0.99800789 0.001829757 0.88836259
		 0.021179991 0.88836259 0.046421513 0.99800795 0.02707137 0.99800795 0.02707137 0.88836271
		 0.046421513 0.88836271 0.052134018 0.88836259 0.071484253 0.88836259 0.071484253
		 0.99800789 0.052134018 0.99800789 0.07719662 0.88836271 0.096546777 0.88836271 0.096546777
		 0.99800795 0.07719662 0.99800795 0.31900418 0.8935957 0.31900418 0.99051476 0.27053681
		 0.99051476 0.27053681 0.8935957;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".sdt" 0;
	setAttr -s 172 ".vt";
	setAttr ".vt[0:165]"  -35.99382019 60.45261765 128.47329712 -34.7939415 61.65249634 128.47329712
		 -35.99382019 60.45261765 125.48970795 -34.7939415 61.65249634 125.48970795 -35.99382019 60.45261765 117.88563538
		 -34.7939415 61.65249634 117.88563538 -35.99382019 60.45261765 112.42728424 -34.7939415 61.65249634 112.42728424
		 -35.99381256 53.62261581 125.48970795 -34.79393387 52.42273712 125.48970795 -35.99382782 53.62261581 128.47329712
		 -34.79394913 52.42273712 128.47329712 -35.99381256 53.62261581 112.42728424 -34.79393387 52.42273712 112.42728424
		 -35.99382782 53.62261581 117.88563538 -34.79394913 52.42273712 117.88563538 34.79394913 52.42273712 128.47375488
		 34.79393387 52.42273712 125.49016571 34.7939415 61.65249634 125.49016571 34.7939415 61.65249634 128.47375488
		 35.99381256 53.62261581 125.49016571 35.99382782 53.62261581 128.47375488 35.99382019 60.45261765 128.47375488
		 35.99382019 60.45261765 125.49016571 34.79394913 52.42273712 117.88609314 34.79393387 52.42273712 112.427742
		 34.7939415 61.65249634 112.427742 34.7939415 61.65249634 117.88609314 35.99381256 53.62261581 112.427742
		 35.99382782 53.62261581 117.88609314 35.99382019 60.45261765 117.88609314 35.99382019 60.45261765 112.427742
		 -35.99382019 30.12638474 128.47329712 -34.7939415 31.32626343 128.47329712 -35.99382019 30.12638474 125.48970795
		 -34.7939415 31.32626343 125.48970795 -35.99382019 30.12638474 117.88563538 -34.7939415 31.32626343 117.88563538
		 -35.99382019 30.12638474 112.42728424 -34.7939415 31.32626343 112.42728424 -35.99381256 23.2963829 125.48970795
		 -34.79393387 22.096504211 125.48970795 -35.99382782 23.2963829 128.47329712 -34.79394913 22.096504211 128.47329712
		 -35.99381256 23.2963829 112.42728424 -34.79393387 22.096504211 112.42728424 -35.99382782 23.2963829 117.88563538
		 -34.79394913 22.096504211 117.88563538 34.79394913 22.096504211 128.47375488 34.79393387 22.096504211 125.49016571
		 34.7939415 31.32626343 125.49016571 34.7939415 31.32626343 128.47375488 35.99381256 23.2963829 125.49016571
		 35.99382782 23.2963829 128.47375488 35.99382019 30.12638474 128.47375488 35.99382019 30.12638474 125.49016571
		 34.79394913 22.096504211 117.88609314 34.79393387 22.096504211 112.427742 34.7939415 31.32626343 112.427742
		 34.7939415 31.32626343 117.88609314 35.99381256 23.2963829 112.427742 35.99382782 23.2963829 117.88609314
		 35.99382019 30.12638474 117.88609314 35.99382019 30.12638474 112.427742 -35.99382019 60.45261765 -129.96499634
		 -34.7939415 61.65249634 -129.96499634 -35.99382019 60.45261765 -132.94857788 -34.7939415 61.65249634 -132.94857788
		 -35.99382019 60.45261765 -140.55265808 -34.7939415 61.65249634 -140.55265808 -35.99382019 60.45261765 -146.011016846
		 -34.7939415 61.65249634 -146.011016846 -35.99381256 53.62261581 -132.94857788 -34.79393387 52.42273712 -132.94857788
		 -35.99382782 53.62261581 -129.96499634 -34.79394913 52.42273712 -129.96499634 -35.99381256 53.62261581 -146.011016846
		 -34.79393387 52.42273712 -146.011016846 -35.99382782 53.62261581 -140.55265808 -34.79394913 52.42273712 -140.55265808
		 34.79394913 52.42273712 -129.96524048 34.79393387 52.42273712 -132.94882202 34.7939415 61.65249634 -132.94882202
		 34.7939415 61.65249634 -129.96524048 35.99381256 53.62261581 -132.94882202 35.99382782 53.62261581 -129.96524048
		 35.99382019 60.45261765 -129.96524048 35.99382019 60.45261765 -132.94882202 34.79394913 52.42273712 -140.55290222
		 34.79393387 52.42273712 -146.011260986 34.7939415 61.65249634 -146.011260986 34.7939415 61.65249634 -140.55290222
		 35.99381256 53.62261581 -146.011260986 35.99382782 53.62261581 -140.55290222 35.99382019 60.45261765 -140.55290222
		 35.99382019 60.45261765 -146.011260986 -35.99382019 30.12638474 -129.96499634 -34.7939415 31.32626343 -129.96499634
		 -35.99382019 30.12638474 -132.94857788 -34.7939415 31.32626343 -132.94857788 -35.99382019 30.12638474 -140.55265808
		 -34.7939415 31.32626343 -140.55265808 -35.99382019 30.12638474 -146.011016846 -34.7939415 31.32626343 -146.011016846
		 -35.99381256 23.2963829 -132.94857788 -34.79393387 22.096504211 -132.94857788 -35.99382782 23.2963829 -129.96499634
		 -34.79394913 22.096504211 -129.96499634 -35.99381256 23.2963829 -146.011016846 -34.79393387 22.096504211 -146.011016846
		 -35.99382782 23.2963829 -140.55265808 -34.79394913 22.096504211 -140.55265808 34.79394913 22.096504211 -129.96524048
		 34.79393387 22.096504211 -132.94882202 34.7939415 31.32626343 -132.94882202 34.7939415 31.32626343 -129.96524048
		 35.99381256 23.2963829 -132.94882202 35.99382782 23.2963829 -129.96524048 35.99382019 30.12638474 -129.96524048
		 35.99382019 30.12638474 -132.94882202 34.79394913 22.096504211 -140.55290222 34.79393387 22.096504211 -146.011260986
		 34.7939415 31.32626343 -146.011260986 34.7939415 31.32626343 -140.55290222 35.99381256 23.2963829 -146.011260986
		 35.99382782 23.2963829 -140.55290222 35.99382019 30.12638474 -140.55290222 35.99382019 30.12638474 -146.011260986
		 -22.22227287 22.00044059753 159.11843872 22.22227287 22.00044059753 159.11889648
		 -22.22227287 59.66591644 -132.14556885 22.22227287 59.66591644 -132.14581299 -22.22227287 22.00044059753 70.4621048
		 22.22227287 22.00044059753 70.46256256 -22.22227287 30.87394905 70.4621048 22.22227287 30.87394905 70.46256256
		 -22.22227287 22.00044059753 -90.024291992 22.22227287 22.00044059753 -90.024543762
		 -22.22227287 30.87394905 -90.024291992 22.22227287 30.87394905 -90.024543762 22.22227287 37.012863159 159.75375366
		 -22.22227287 37.012863159 159.75332642 -22.22227287 37.012863159 107.50709534 22.22227287 37.012863159 107.5075531
		 -22.22227287 59.23871231 115.11342621 22.22227287 59.23871231 115.11388397 -22.22227287 59.31570053 126.97431946
		 22.22227287 59.31570053 126.97477722 -22.22227287 45.69425201 -174.39639282 -22.22227287 36.82074356 -174.39639282
		 22.22227287 45.69425201 -174.39663696 22.22227287 36.82074356 -174.39663696 79.70195007 22.00044059753 63.26328278
		 72.50267029 22.00044059753 70.46256256 79.70195007 30.87394905 63.26328278 72.50267029 30.87394905 70.46256256
		 79.70195007 22.00044059753 -82.82526398 72.50267029 22.00044059753 -90.024543762
		 79.70195007 30.87394905 -82.82526398 72.50267029 30.87394905 -90.024543762 -79.70195007 30.87394905 63.26282501
		 -72.50267029 30.87394905 70.4621048 -79.70195007 22.00044059753 63.26282501 -72.50267029 22.00044059753 70.4621048
		 -79.70195007 30.87394905 -82.82501221 -72.50267029 30.87394905 -90.024291992;
	setAttr ".vt[166:171]" -79.70195007 22.00044059753 -82.82501221 -72.50267029 22.00044059753 -90.024291992
		 -22.22227287 22.00044059753 107.50709534 22.22227287 22.00044059753 107.5075531 22.22227287 22.00043869019 -145.71012878
		 -22.22227287 22.00043869019 -145.70988464;
	setAttr -s 306 ".ed";
	setAttr ".ed[0:165]"  1 0 0 3 2 0 5 4 0 7 6 0 0 2 0 3 1 0 4 6 0 7 5 0 8 2 0
		 9 8 0 10 0 0 11 10 0 8 10 0 11 9 0 12 6 0 13 12 0 14 4 0 15 14 0 12 14 0 15 13 0
		 16 17 0 17 9 0 16 11 0 18 19 0 19 1 0 18 3 0 20 21 0 21 22 0 22 23 0 20 23 0 24 25 0
		 25 13 0 24 15 0 26 27 0 27 5 0 26 7 0 28 29 0 29 30 0 30 31 0 28 31 0 18 23 0 17 20 0
		 26 31 0 25 28 0 19 22 0 27 30 0 16 21 0 24 29 0 33 32 0 35 34 0 37 36 0 39 38 0 32 34 0
		 35 33 0 36 38 0 39 37 0 40 34 0 41 40 0 42 32 0 43 42 0 40 42 0 43 41 0 44 38 0 45 44 0
		 46 36 0 47 46 0 44 46 0 47 45 0 48 49 0 49 41 0 48 43 0 50 51 0 51 33 0 50 35 0 52 53 0
		 53 54 0 54 55 0 52 55 0 56 57 0 57 45 0 56 47 0 58 59 0 59 37 0 58 39 0 60 61 0 61 62 0
		 62 63 0 60 63 0 50 55 0 49 52 0 58 63 0 57 60 0 51 54 0 59 62 0 48 53 0 56 61 0 65 64 0
		 67 66 0 69 68 0 71 70 0 64 66 0 67 65 0 68 70 0 71 69 0 72 66 0 73 72 0 74 64 0 75 74 0
		 72 74 0 75 73 0 76 70 0 77 76 0 78 68 0 79 78 0 76 78 0 79 77 0 80 81 0 81 73 0 80 75 0
		 82 83 0 83 65 0 82 67 0 84 85 0 85 86 0 86 87 0 84 87 0 88 89 0 89 77 0 88 79 0 90 91 0
		 91 69 0 90 71 0 92 93 0 93 94 0 94 95 0 92 95 0 82 87 0 81 84 0 90 95 0 89 92 0 83 86 0
		 91 94 0 80 85 0 88 93 0 97 96 0 99 98 0 101 100 0 103 102 0 96 98 0 99 97 0 100 102 0
		 103 101 0 104 98 0 105 104 0 106 96 0 107 106 0 104 106 0 107 105 0 108 102 0 109 108 0
		 110 100 0 111 110 0 108 110 0 111 109 0 112 113 0 113 105 0;
	setAttr ".ed[166:305]" 112 107 0 114 115 0 115 97 0 114 99 0 116 117 0 117 118 0
		 118 119 0 116 119 0 120 121 0 121 109 0 120 111 0 122 123 0 123 101 0 122 103 0 124 125 0
		 125 126 0 126 127 0 124 127 0 114 119 0 113 116 0 122 127 0 121 124 0 115 118 0 123 126 0
		 112 117 0 120 125 0 129 128 0 130 131 0 132 133 1 133 135 0 135 134 1 134 132 0 136 132 1
		 137 133 1 138 134 1 139 135 1 136 137 1 137 139 0 139 138 0 138 136 0 133 153 0 135 155 0
		 137 157 0 139 159 0 134 161 0 132 163 0 138 165 0 136 167 0 129 140 0 141 128 0 141 140 0
		 142 144 0 143 145 0 142 143 0 144 146 0 145 147 0 146 141 0 147 140 0 144 145 0 147 146 0
		 130 148 0 136 171 0 148 149 0 131 150 0 148 150 0 137 170 0 150 151 0 151 149 0 134 142 0
		 135 143 0 152 153 0 155 154 0 156 152 0 157 156 0 158 154 0 158 159 0 160 161 0 163 162 0
		 164 160 0 165 164 0 166 162 0 166 167 0 153 155 0 154 152 0 156 158 0 159 157 0 161 163 0
		 162 160 0 164 166 0 167 165 0 163 167 1 153 157 1 155 159 1 161 165 1 140 143 1 141 142 1
		 132 168 0 133 169 0 48 51 1 49 50 1 56 59 1 57 58 1 16 19 1 17 18 1 24 27 1 25 26 1
		 1 11 1 3 9 1 5 15 1 7 13 1 65 75 1 67 73 1 69 79 1 71 77 1 97 107 1 99 105 1 101 111 1
		 103 109 1 112 115 1 113 114 1 121 122 1 80 83 1 81 82 1 88 91 1 89 90 1 33 43 1 35 41 1
		 37 47 1 39 45 1 168 128 0 169 129 0 143 169 1 169 168 1 130 138 0 131 139 0 170 151 0
		 171 149 0 170 171 1 130 171 0 131 170 0;
	setAttr -s 152 -ch 612 ".fc[0:151]" -type "polyFaces" 
		f 4 13 -22 -21 22
		mu 0 4 172 173 174 175
		f 4 12 10 4 -9
		mu 0 4 0 1 2 3
		f 4 19 -32 -31 32
		mu 0 4 176 177 178 179
		f 4 18 16 6 -15
		mu 0 4 8 9 10 11
		f 4 -1 -6 1 -5
		mu 0 4 2 6 7 3
		f 4 -3 -8 3 -7
		mu 0 4 10 14 15 11
		f 4 272 -23 268 24
		mu 0 4 280 281 282 283
		f 4 -10 -14 11 -13
		mu 0 4 0 4 5 1
		f 4 274 -33 270 34
		mu 0 4 288 289 290 291
		f 4 -16 -20 17 -19
		mu 0 4 8 12 13 9
		f 4 -25 -24 25 5
		mu 0 4 180 181 182 183
		f 4 29 -29 -28 -27
		mu 0 4 16 17 18 19
		f 4 -35 -34 35 7
		mu 0 4 184 185 186 187
		f 4 39 -39 -38 -37
		mu 0 4 24 25 26 27
		f 4 269 40 -30 -42
		mu 0 4 296 297 298 299
		f 4 271 42 -40 -44
		mu 0 4 304 305 306 307
		f 4 28 -41 23 44
		mu 0 4 18 17 20 21
		f 4 38 -43 33 45
		mu 0 4 26 25 28 29
		f 4 26 -47 20 41
		mu 0 4 16 19 22 23
		f 4 36 -48 30 43
		mu 0 4 24 27 30 31
		f 4 61 -70 -69 70
		mu 0 4 188 189 190 191
		f 4 60 58 52 -57
		mu 0 4 32 33 34 35
		f 4 67 -80 -79 80
		mu 0 4 192 193 194 195
		f 4 66 64 54 -63
		mu 0 4 40 41 42 43
		f 4 49 -53 -49 -54
		mu 0 4 38 35 34 39
		f 4 -51 -56 51 -55
		mu 0 4 42 46 47 43
		f 4 291 -71 264 72
		mu 0 4 312 313 314 315
		f 4 -58 -62 59 -61
		mu 0 4 32 36 37 33
		f 4 293 -81 266 82
		mu 0 4 320 321 322 323
		f 4 -64 -68 65 -67
		mu 0 4 40 44 45 41
		f 4 -73 -72 73 53
		mu 0 4 196 197 198 199
		f 4 77 -77 -76 -75
		mu 0 4 48 49 50 51
		f 4 -83 -82 83 55
		mu 0 4 200 201 202 203
		f 4 87 -87 -86 -85
		mu 0 4 56 57 58 59
		f 4 265 88 -78 -90
		mu 0 4 328 329 330 331
		f 4 267 90 -88 -92
		mu 0 4 336 337 338 339
		f 4 76 -89 71 92
		mu 0 4 50 49 52 53
		f 4 86 -91 81 93
		mu 0 4 58 57 60 61
		f 4 74 -95 68 89
		mu 0 4 48 51 54 55
		f 4 84 -96 78 91
		mu 0 4 56 59 62 63
		f 4 109 -118 -117 118
		mu 0 4 204 205 206 207
		f 4 108 106 100 -105
		mu 0 4 64 65 66 67
		f 4 115 -128 -127 128
		mu 0 4 208 209 210 211
		f 4 114 112 102 -111
		mu 0 4 72 73 74 75
		f 4 -97 -102 97 -101
		mu 0 4 66 70 71 67
		f 4 -99 -104 99 -103
		mu 0 4 74 78 79 75
		f 4 276 -119 287 120
		mu 0 4 344 345 346 347
		f 4 -106 -110 107 -109
		mu 0 4 64 68 69 65
		f 4 278 -129 289 130
		mu 0 4 352 353 354 355
		f 4 -112 -116 113 -115
		mu 0 4 72 76 77 73
		f 4 -121 -120 121 101
		mu 0 4 212 213 214 215
		f 4 125 -125 -124 -123
		mu 0 4 80 81 82 83
		f 4 -131 -130 131 103
		mu 0 4 216 217 218 219
		f 4 135 -135 -134 -133
		mu 0 4 88 89 90 91
		f 4 277 105 104 -98
		mu 0 4 360 361 362 363
		f 4 279 111 110 -100
		mu 0 4 368 369 370 371
		f 4 124 -137 119 140
		mu 0 4 82 81 84 85
		f 4 134 -139 129 141
		mu 0 4 90 89 92 93
		f 4 122 -143 116 137
		mu 0 4 80 83 86 87
		f 4 132 -144 126 139
		mu 0 4 88 91 94 95
		f 4 157 -166 -165 166
		mu 0 4 220 221 222 223
		f 4 156 154 148 -153
		mu 0 4 96 97 98 99
		f 4 163 -176 -175 176
		mu 0 4 224 225 226 227
		f 4 162 160 150 -159
		mu 0 4 104 105 106 107
		f 4 -145 -150 145 -149
		mu 0 4 98 102 103 99
		f 4 -147 -152 147 -151
		mu 0 4 106 110 111 107
		f 4 280 -167 284 168
		mu 0 4 376 377 378 379
		f 4 -154 -158 155 -157
		mu 0 4 96 100 101 97
		f 6 282 -177 191 181 -190 178
		mu 0 6 384 385 386 387 388 389
		f 4 -160 -164 161 -163
		mu 0 4 104 108 109 105
		f 4 -169 -168 169 149
		mu 0 4 228 229 230 231
		f 4 173 -173 -172 -171
		mu 0 4 112 113 114 115
		f 4 -179 -178 179 151
		mu 0 4 232 233 234 235
		f 4 183 -183 -182 -181
		mu 0 4 120 121 122 123
		f 4 281 153 152 -146
		mu 0 4 392 393 394 395
		f 4 283 159 158 -148
		mu 0 4 400 401 402 403
		f 4 172 -185 167 188
		mu 0 4 114 113 116 117
		f 4 182 -187 177 189
		mu 0 4 122 121 124 125
		f 4 170 -191 164 185
		mu 0 4 112 115 118 119
		f 4 180 -192 174 187
		mu 0 4 120 123 126 127
		f 4 263 298 -263 194
		mu 0 4 236 237 238 239
		f 4 -193 214 -217 215
		mu 0 4 408 409 410 411
		f 4 216 -224 225 222
		mu 0 4 278 279 277 276
		f 4 -229 230 232 233
		mu 0 4 412 413 414 415
		f 4 -195 -199 202 199
		mu 0 4 236 239 242 243
		f 4 249 -239 250 240
		mu 0 4 128 129 130 131
		f 4 204 200 -197 -202
		mu 0 4 256 257 258 259
		f 4 253 -245 254 246
		mu 0 4 136 137 138 139
		f 4 -205 -301 -194 299
		mu 0 4 257 256 260 261
		f 4 304 -228 -206 -300
		mu 0 4 144 145 146 147
		f 4 -196 206 248 -208
		mu 0 4 416 417 418 419
		f 4 257 239 238 236
		mu 0 4 249 248 254 255
		f 4 203 209 251 -209
		mu 0 4 420 421 422 423
		f 4 201 207 258 -210
		mu 0 4 256 259 266 267
		f 4 -198 210 252 -212
		mu 0 4 424 425 426 427
		f 4 259 245 244 242
		mu 0 4 263 262 270 271
		f 4 205 213 255 -213
		mu 0 4 428 429 430 431
		f 4 198 211 256 -214
		mu 0 4 242 239 244 245
		f 4 196 234 219 -236
		mu 0 4 259 258 264 265
		f 4 -225 220 -226 -222
		mu 0 4 274 275 276 277
		f 6 262 295 -216 261 -235 197
		mu 0 6 150 151 152 153 154 155
		f 4 -220 217 224 -219
		mu 0 4 432 433 434 435
		f 4 223 260 218 221
		mu 0 4 158 159 160 161
		f 4 193 229 -231 -227
		mu 0 4 261 260 268 269
		f 4 -230 305 301 -233
		mu 0 4 166 167 168 169
		f 4 302 -234 -302 303
		mu 0 4 246 252 253 247
		f 4 -237 -250 -238 -249
		mu 0 4 132 129 128 133
		f 4 -240 -252 -242 -251
		mu 0 4 130 134 135 131
		f 4 -243 -254 -244 -253
		mu 0 4 140 137 136 141
		f 4 -246 -256 -248 -255
		mu 0 4 138 142 143 139
		f 4 -247 247 -257 243
		mu 0 4 250 251 245 244
		f 4 -200 208 -258 -207
		mu 0 4 236 243 248 249
		f 4 -259 237 -241 241
		mu 0 4 267 266 272 273
		f 4 -201 212 -260 -211
		mu 0 4 258 257 262 263
		f 4 297 -264 195 235
		mu 0 4 160 163 164 165
		f 4 -262 -223 -221 -218
		mu 0 4 154 153 156 157
		f 4 -265 94 75 -93
		mu 0 4 315 314 318 319
		f 4 292 57 56 -50
		mu 0 4 332 333 334 335
		f 4 -267 95 85 -94
		mu 0 4 323 322 326 327
		f 4 294 63 62 -52
		mu 0 4 340 341 342 343
		f 4 -269 46 27 -45
		mu 0 4 283 282 286 287
		f 4 273 9 8 -2
		mu 0 4 300 301 302 303
		f 4 -271 47 37 -46
		mu 0 4 291 290 294 295
		f 4 275 15 14 -4
		mu 0 4 308 309 310 311
		f 4 0 -11 -12 -273
		mu 0 4 280 284 285 281
		f 4 -26 -270 21 -274
		mu 0 4 300 297 296 301
		f 4 2 -17 -18 -275
		mu 0 4 288 292 293 289
		f 4 -36 -272 31 -276
		mu 0 4 308 305 304 309
		f 4 96 -107 -108 -277
		mu 0 4 344 348 349 345
		f 4 288 136 -126 -138
		mu 0 4 365 364 366 367
		f 4 98 -113 -114 -279
		mu 0 4 352 356 357 353
		f 4 290 138 -136 -140
		mu 0 4 373 372 374 375
		f 4 144 -155 -156 -281
		mu 0 4 376 380 381 377
		f 4 285 184 -174 -186
		mu 0 4 397 396 398 399
		f 4 146 -161 -162 -283
		mu 0 4 384 390 391 385
		f 4 286 186 -184 -188
		mu 0 4 405 404 406 407
		f 4 -285 190 171 -189
		mu 0 4 379 378 382 383
		f 4 -170 -286 165 -282
		mu 0 4 392 396 397 393
		f 4 -180 -287 175 -284
		mu 0 4 400 404 405 401
		f 4 -288 142 123 -141
		mu 0 4 347 346 350 351
		f 4 -122 -289 117 -278
		mu 0 4 360 364 365 361
		f 4 -290 143 133 -142
		mu 0 4 355 354 358 359
		f 4 -132 -291 127 -280
		mu 0 4 368 372 373 369
		f 4 -59 -60 -292 48
		mu 0 4 316 317 313 312
		f 4 -74 -266 69 -293
		mu 0 4 332 329 328 333
		f 4 50 -65 -66 -294
		mu 0 4 320 324 325 321
		f 4 -84 -268 79 -295
		mu 0 4 340 337 336 341
		f 4 -261 -215 -297 -298
		mu 0 4 160 159 162 163
		f 4 -299 296 192 -296
		mu 0 4 238 237 240 241
		f 4 227 -304 -232 -203
		mu 0 4 242 246 247 243
		f 4 228 -303 -305 226
		mu 0 4 148 149 145 144
		f 4 -306 300 -204 231
		mu 0 4 168 167 170 171;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".vcs" 2;
createNode transform -n "MSH_tires_tutorial" -p "GRP_utility_meshes";
	rename -uid "98E048F5-4A3C-17DF-3FF8-E79C2D7AEBEB";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" 0 35.018801148980856 -7.8073883056640625 ;
	setAttr ".sp" -type "double3" 0 35.018801148980856 -7.8073883056640625 ;
createNode mesh -n "MSH_tires_tutorialShape" -p "MSH_tires_tutorial";
	rename -uid "6B3C73AF-4F59-69D5-5C2C-E1AAD632342B";
	setAttr -k off ".v";
	setAttr -s 14 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".sdt" 0;
	setAttr ".ugsdt" no;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
createNode mesh -n "MSH_tires_tutorialShapeOrig" -p "MSH_tires_tutorial";
	rename -uid "3C5D2216-4DF6-8478-CE95-2E85F4E38E90";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".sdt" 0;
	setAttr -s 840 ".vt";
	setAttr ".vt[0:165]"  -89.76550293 68.11952972 -137.23513794 -89.76550293 64.29701996 -137.22648621
		 -89.76550293 63.69873428 -131.13484192 -89.76550293 67.43951416 -130.34855652 -88.35949707 69.20457458 -129.99423218
		 -88.35949707 69.91967773 -137.2555542 -66.33499908 69.91967773 -137.2555542 -66.33499908 69.20457458 -129.99423218
		 -89.76550293 61.84700394 -125.30069733 -89.76550293 65.34255981 -123.75385284 -88.35949707 66.99537659 -123.040313721
		 -66.33499908 66.99537659 -123.040313721 -89.76550293 58.82275391 -119.97905731 -89.76550293 61.9203186 -117.73923492
		 -88.35949707 63.38866425 -116.69763947 -66.33499908 63.38866425 -116.69763947 -89.76550293 54.75815201 -115.40244293
		 -89.76550293 57.32234192 -112.56755829 -88.35949707 58.54204178 -111.24343872 -66.33499908 58.54204178 -111.24343872
		 -89.76550293 49.83084488 -111.77095795 -89.76550293 51.74959564 -108.4648819 -88.35949707 52.66734695 -106.91610718
		 -66.33499908 52.66734695 -106.91610718 -89.76550293 44.25618744 -109.24326324 -89.76550293 45.44563675 -105.61051178
		 -88.35949707 46.021316528 -103.90475464 -66.33499908 46.021316528 -103.90475464 -89.76550293 38.27780151 -107.9298172
		 -89.76550293 38.68596268 -104.12915802 -88.35949707 38.89442444 -102.34100342 -66.33499908 38.89442444 -102.34100342
		 -89.76550293 32.15697479 -107.88808441 -89.76550293 31.76601791 -104.085609436 -88.35949707 31.59814262 -102.29317474
		 -66.33499908 31.59814262 -102.29317474 -89.76550293 26.16123962 -109.1198349 -89.76550293 24.9882431 -105.4817276
		 -88.35949707 24.45137596 -103.76338196 -66.33499908 24.45137596 -103.76338196 -89.76550293 20.5526123 -111.57125092
		 -89.76550293 18.64884949 -108.25653839 -88.35949707 17.76644897 -106.68734741 -66.33499908 17.76644897 -106.68734741
		 -89.76550293 15.57623291 -115.13521576 -89.76550293 13.024894714 -112.28873444 -88.35949707 11.83552456 -110.93730927
		 -66.33499908 11.83552456 -110.93730927 -89.76550293 11.4495697 -119.65592194 -89.76550293 8.3621788 -117.40210724
		 -88.35949707 6.91781616 -116.32750702 -66.33499908 6.91781616 -116.32750702 -89.76550293 8.35301208 -124.93582916
		 -89.76550293 4.86448288 -123.37317657 -88.35949707 3.22827435 -122.62233734 -66.33499908 3.22827435 -122.62233734
		 -89.76550293 6.42187119 -130.74415588 -89.76550293 2.68468499 -129.94094849 -88.35949707 0.92811751 -129.54670715
		 -66.33499908 0.92811751 -129.54670715 -89.76550293 5.74054909 -136.8270874 -89.76550293 1.91803372 -136.81843567
		 -88.35949707 0.11788642 -136.79803467 -66.33499908 0.11788642 -136.79803467 -89.76550293 6.33882904 -142.91874695
		 -89.76550293 2.59804368 -143.70503235 -88.35949707 0.83298838 -144.059356689 -66.33499908 0.83298838 -144.059356689
		 -89.76550293 8.19056225 -148.75289917 -89.76550293 4.69500017 -150.29974365 -88.35949707 3.042184114 -151.013275146
		 -66.33499908 3.042184114 -151.013275146 -89.76550293 11.21481133 -154.074523926 -89.76550293 8.11724663 -156.31436157
		 -88.35949707 6.64889956 -157.35595703 -66.33499908 6.64889956 -157.35595703 -89.76550293 15.27940941 -158.65113831
		 -89.76550293 12.7152195 -161.48602295 -88.35949707 11.49552155 -162.81015015 -66.33499908 11.49552155 -162.81015015
		 -89.76550293 20.20671654 -162.28262329 -89.76550293 18.28796577 -165.58869934 -88.35949707 17.37021637 -167.13748169
		 -66.33499908 17.37021637 -167.13748169 -89.76550293 25.78137779 -164.81033325 -89.76550293 24.59192848 -168.44308472
		 -88.35949707 24.016246796 -170.14883423 -66.33499908 24.016246796 -170.14883423 -89.76550293 31.75976181 -166.12376404
		 -89.76550293 31.35160065 -169.92443848 -88.35949707 31.14313889 -171.71258545 -66.33499908 31.14313889 -171.71258545
		 -89.76550293 37.88058472 -166.16551208 -89.76550293 38.2715416 -169.96798706 -88.35949707 38.43941879 -171.76042175
		 -66.33499908 38.43941879 -171.76042175 -89.76550293 43.8763237 -164.9337616 -89.76550293 45.049320221 -168.5718689
		 -88.35949707 45.58618927 -170.29020691 -66.33499908 45.58618927 -170.29020691 -89.76550293 49.48495483 -162.48233032
		 -89.76550293 51.38871765 -165.79705811 -88.35949707 52.27111816 -167.3662262 -66.33499908 52.27111816 -167.3662262
		 -89.76550293 54.46133041 -158.91838074 -89.76550293 57.01266861 -161.76486206 -88.35949707 58.20203781 -163.11628723
		 -66.33499908 58.20203781 -163.11628723 -89.76550293 58.58798981 -154.3976593 -89.76550293 61.67538071 -156.65148926
		 -88.35949707 63.11974335 -157.72608948 -66.33499908 63.11974335 -157.72608948 -89.76550293 61.68454742 -149.11776733
		 -89.76550293 65.17308044 -150.68041992 -88.35949707 66.80929565 -151.43125916 -66.33499908 66.80929565 -151.43125916
		 -89.76550293 63.61569214 -143.30941772 -89.76550293 67.35288239 -144.11262512 -88.35949707 69.10944366 -144.5068512
		 -66.33499908 69.10944366 -144.5068512 -86.80031586 61.84566498 -131.51553345 -86.80031586 62.40529251 -137.21359253
		 -86.80031586 60.11358261 -126.05834198 -86.80031586 57.28473282 -121.080543518 -86.80031586 53.48275375 -116.79962921
		 -86.80031586 48.873806 -113.40279388 -86.80031586 43.6593399 -111.038414001 -86.80031586 38.067230225 -109.80983734
		 -86.80031586 32.34188461 -109.7707901 -86.80031586 26.7335434 -110.922966 -86.80031586 21.48729897 -113.21598053
		 -86.80031586 16.83245468 -116.54967499 -86.80031586 12.9724226 -120.77828217 -86.80031586 10.075940132 -125.71704865
		 -86.80031586 8.26957226 -131.15008545 -86.80031586 7.6322732 -136.83999634 -86.80031586 8.19189739 -142.53805542
		 -86.80031586 9.92398453 -147.99523926 -86.80031586 12.75283337 -152.97305298 -86.80031586 16.55480957 -157.25395203
		 -86.80031586 21.16375351 -160.65080261 -86.80031586 26.37822342 -163.015182495 -86.80031586 31.9703331 -164.2437439
		 -86.80031586 37.69567871 -164.2828064 -86.80031586 43.30402374 -163.13064575 -86.80031586 48.55026627 -160.83760071
		 -86.80031586 53.20510864 -157.50392151 -86.80031586 57.065135956 -153.27529907 -86.80031586 59.96162033 -148.33653259
		 -86.80031586 61.76799393 -142.90348816 -65.70256042 62.9355545 -131.28826904 -68.54407501 59.82168961 -131.93133545
		 -65.70256042 63.51861572 -137.21789551 -68.54407501 60.33909607 -137.19949341 -65.70256042 61.13240433 -125.60945892
		 -68.54407501 58.22028351 -126.88587189 -65.70256042 58.18795776 -120.42963409 -68.54407501 55.6048584 -122.28362274
		 -65.70256042 54.23091507 -115.97516632 -68.54407501 52.089725494 -118.32569122 -65.7025528 49.43420029 -112.44078827
		 -68.54407501 47.82850266 -115.18512726 -65.70256042 44.0074729919 -109.98090363 -68.54407501 43.0074462891 -112.99912262
		 -65.70256042 38.1878891 -108.70307159 -68.54407501 37.83723831 -111.8632431;
	setAttr ".vt[166:331]" -65.70256042 32.22979355 -108.66310883 -68.54407501 32.54384613 -111.82714081
		 -65.70256042 26.39360619 -109.8628006 -68.54407501 27.35862923 -112.89238739 -65.70256042 20.93437386 -112.24967194
		 -68.54407501 22.50819397 -115.012413025 -65.70256042 16.090698242 -115.71942902 -68.54407501 18.20453644 -118.094581604
		 -65.70256042 12.07425499 -120.12039948 -68.54407501 14.63572884 -122.00415802 -65.70256042 9.060622215 -125.26027679
		 -68.54407501 11.95777321 -126.57032013 -65.70256042 7.18147516 -130.91439819 -68.54407501 10.28768826 -131.59346008
		 -65.70256042 6.51895142 -136.83569336 -68.54407501 9.69847107 -136.85409546 -65.70256042 7.10200834 -142.76531982
		 -68.54407501 10.21587372 -142.12225342 -65.70256042 8.9051609 -148.44413757 -68.54407501 11.81728268 -147.16772461
		 -65.70256042 11.84960461 -153.6239624 -68.54407501 14.43270588 -151.7699585 -65.70256042 15.80664825 -158.078430176
		 -68.54407501 17.94783974 -155.72790527 -65.70256042 20.60335922 -161.61282349 -68.54407501 22.20905685 -158.86846924
		 -65.70256042 26.030092239 -164.072692871 -68.54407501 27.030117035 -161.054473877
		 -65.70256042 31.84967613 -165.3505249 -68.54407501 32.20032883 -162.19033813 -65.70256042 37.80776596 -165.39047241
		 -68.54407501 37.49371719 -162.22644043 -65.70256042 43.64395905 -164.1907959 -68.54407501 42.678936 -161.16122437
		 -65.70256042 49.10319519 -161.80392456 -68.54407501 47.52937317 -159.041168213 -65.70256042 53.94686508 -158.33416748
		 -68.54407501 51.83302689 -155.95901489 -65.70256042 57.9632988 -153.9331665 -68.54407501 55.40183258 -152.049407959
		 -65.70256042 60.97694016 -148.79330444 -68.54407501 58.079788208 -147.48326111 -65.70256042 62.85609055 -143.13917542
		 -68.54407501 59.74987793 -142.46011353 -89.76550293 68.11956787 121.20366669 -89.76550293 64.29705048 121.21233368
		 -89.76550293 63.69876862 127.30397034 -89.76550293 67.43954468 128.090255737 -88.35949707 69.20461273 128.44458008
		 -88.35949707 69.91971588 121.18325806 -66.33499908 69.91971588 121.18325806 -66.33499908 69.20461273 128.44458008
		 -89.76550293 61.84703827 133.13812256 -89.76550293 65.34259033 134.68496704 -88.35949707 66.99541473 135.39851379
		 -66.33499908 66.99541473 135.39851379 -89.76550293 58.82278824 138.45976257 -89.76550293 61.92035294 140.69958496
		 -88.35949707 63.38869858 141.74118042 -66.33499908 63.38869858 141.74118042 -89.76550293 54.75818634 143.036376953
		 -89.76550293 57.32237625 145.8712616 -88.35949707 58.54207611 147.19538879 -66.33499908 58.54207611 147.19538879
		 -89.76550293 49.83087921 146.66786194 -89.76550293 51.74962997 149.97393799 -88.35949707 52.66738129 151.52272034
		 -66.33499908 52.66738129 151.52272034 -89.76550293 44.25622177 149.19555664 -89.76550293 45.44567108 152.82830811
		 -88.35949707 46.021350861 154.53407288 -66.33499908 46.021350861 154.53407288 -89.76550293 38.27783585 150.50901794
		 -89.76550293 38.68599701 154.30966187 -88.35949707 38.89445877 156.097808838 -66.33499908 38.89445877 156.097808838
		 -89.76550293 32.15700912 150.55075073 -89.76550293 31.76605225 154.35321045 -88.35949707 31.59817696 156.14564514
		 -66.33499908 31.59817696 156.14564514 -89.76550293 26.16127396 149.31898499 -89.76550293 24.98827744 152.95709229
		 -88.35949707 24.45141029 154.6754303 -66.33499908 24.45141029 154.6754303 -89.76550293 20.55264664 146.86756897
		 -89.76550293 18.64888382 150.18228149 -88.35949707 17.76648331 151.75146484 -66.33499908 17.76648331 151.75146484
		 -89.76550293 15.57626724 143.30360413 -89.76550293 13.024929047 146.15008545 -88.35949707 11.83555889 147.50151062
		 -66.33499908 11.83555889 147.50151062 -89.76550293 11.44960403 138.78289795 -89.76550293 8.36221313 141.036712646
		 -88.35949707 6.91785049 142.11131287 -66.33499908 6.91785049 142.11131287 -89.76550293 8.35304642 133.50299072
		 -89.76550293 4.86451721 135.065643311 -88.35949707 3.2283082 135.81648254 -66.33499908 3.2283082 135.81648254
		 -89.76550293 6.42190552 127.69465637 -89.76550293 2.68471909 128.49786377 -88.35949707 0.92815173 128.89212036
		 -66.33499908 0.92815173 128.89212036 -89.76550293 5.74058342 121.61172485 -89.76550293 1.91806793 121.62037659
		 -88.35949707 0.11792064 121.64077759 -66.33499908 0.11792064 121.64077759 -89.76550293 6.33886337 115.52006531
		 -89.76550293 2.59807777 114.73377991 -88.35949707 0.83302259 114.3794632 -66.33499908 0.83302259 114.3794632
		 -89.76550293 8.19059563 109.68592072 -89.76550293 4.6950345 108.13907623 -88.35949707 3.042218208 107.42552948
		 -66.33499908 3.042218208 107.42552948 -89.76550293 11.21484566 104.3642807 -89.76550293 8.11728096 102.12446594
		 -88.35949707 6.64893389 101.082862854 -66.33499908 6.64893389 101.082862854 -89.76550293 15.27944374 99.78767395
		 -89.76550293 12.71525383 96.95278931 -88.35949707 11.49555588 95.62866974 -66.33499908 11.49555588 95.62866974
		 -89.76550293 20.20675087 96.15618896 -89.76550293 18.28800011 92.85011292 -88.35949707 17.3702507 91.3013382
		 -66.33499908 17.3702507 91.3013382 -89.76550293 25.78141212 93.62848663 -89.76550293 24.59196281 89.99572754
		 -88.35949707 24.016281128 88.28999329 -66.33499908 24.016281128 88.28999329 -89.76550293 31.75979614 92.31504822
		 -89.76550293 31.35163498 88.51438141 -88.35949707 31.14317322 86.72621918 -66.33499908 31.14317322 86.72621918
		 -89.76550293 37.88061905 92.27330017 -89.76550293 38.27157593 88.4708252 -88.35949707 38.43945313 86.67839813
		 -66.33499908 38.43945313 86.67839813 -89.76550293 43.87635803 93.50505066 -89.76550293 45.049354553 89.86695099
		 -88.35949707 45.5862236 88.14860535 -66.33499908 45.5862236 88.14860535 -89.76550293 49.48498917 95.95648193
		 -89.76550293 51.38875198 92.64175415 -88.35949707 52.2711525 91.07257843 -66.33499908 52.2711525 91.07257843
		 -89.76550293 54.46136475 99.52043152 -89.76550293 57.012702942 96.6739502 -88.35949707 58.20207214 95.32252502
		 -66.33499908 58.20207214 95.32252502 -89.76550293 58.58802414 104.041152954 -89.76550293 61.67541504 101.78733063
		 -88.35949707 63.11977768 100.71272278 -66.33499908 63.11977768 100.71272278 -89.76550293 61.68458176 109.32105255
		 -89.76550293 65.17311096 107.75839233 -88.35949707 66.80932617 107.00756073 -66.33499908 66.80932617 107.00756073
		 -89.76550293 63.61572647 115.12939453 -89.76550293 67.3529129 114.32619476 -88.35949707 69.10948181 113.93196106
		 -66.33499908 69.10948181 113.93196106 -86.80031586 61.84569931 126.92328644 -86.80031586 62.40532684 121.22522736;
	setAttr ".vt[332:497]" -86.80031586 60.11361694 132.38047791 -86.80031586 57.28476715 137.35827637
		 -86.80031586 53.48278809 141.63920593 -86.80031586 48.87384033 145.036026001 -86.80031586 43.65937424 147.40040588
		 -86.80031586 38.067264557 148.62898254 -86.80031586 32.34191513 148.66802979 -86.80031586 26.73357773 147.51585388
		 -86.80031586 21.4873333 145.22283936 -86.80031586 16.83248711 141.8891449 -86.80031586 12.97245693 137.66053772
		 -86.80031586 10.075974464 132.72177124 -86.80031586 8.26960659 127.28872681 -86.80031586 7.63230753 121.59882355
		 -86.80031586 8.19193172 115.90075684 -86.80031586 9.92401886 110.44356537 -86.80031586 12.7528677 105.46576691
		 -86.80031586 16.5548439 101.18486023 -86.80031586 21.16378784 97.78800964 -86.80031586 26.37825775 95.42362976
		 -86.80031586 31.97036743 94.19506073 -86.80031586 37.69571304 94.15600586 -86.80031586 43.30405807 95.30817413
		 -86.80031586 48.5503006 97.60121155 -86.80031586 53.20514297 100.93489838 -86.80031586 57.065170288 105.16352081
		 -86.80031586 59.96165466 110.10227966 -86.80031586 61.76802826 115.53533173 -65.70256042 62.93558884 127.15054321
		 -68.54407501 59.82172394 126.50748444 -65.70256042 63.51865005 121.22091675 -68.54407501 60.3391304 121.23932648
		 -65.70256042 61.13243866 132.82936096 -68.54407501 58.22031784 131.552948 -65.70256042 58.1879921 138.009185791
		 -68.54407501 55.60489273 136.15519714 -65.70256042 54.2309494 142.46365356 -68.54407501 52.089759827 140.11312866
		 -65.7025528 49.43423462 145.99804688 -68.54407501 47.82853699 143.25369263 -65.70256042 44.0075073242 148.45791626
		 -68.54407501 43.0074806213 145.43969727 -65.70256042 38.18792343 149.73576355 -68.54407501 37.83727264 146.57557678
		 -65.70256042 32.22982788 149.77571106 -68.54407501 32.54388046 146.61167908 -65.70256042 26.39364052 148.57601929
		 -68.54407501 27.35866356 145.5464325 -65.70256042 20.93440819 146.18914795 -68.54407501 22.5082283 143.42640686
		 -65.70256042 16.090730667 142.71939087 -68.54407501 18.20457077 140.34423828 -65.70256042 12.074289322 138.31842041
		 -68.54407501 14.63576317 136.43466187 -65.70256042 9.060656548 133.17854309 -68.54407501 11.95780754 131.86849976
		 -65.70256042 7.18150949 127.52441406 -68.54407501 10.28772259 126.84535217 -65.70256042 6.51898575 121.60313416
		 -68.54407501 9.6985054 121.58472443 -65.70256042 7.10204268 115.67350769 -68.54407501 10.21590805 116.31655884
		 -65.70256042 8.90519524 109.99467468 -68.54407501 11.81731701 111.27108765 -65.70256042 11.84963894 104.81485748
		 -68.54407501 14.43274021 106.66884613 -65.70256042 15.80668259 100.36038208 -68.54407501 17.94787407 102.71091461
		 -65.70256042 20.60339355 96.8259964 -68.54407501 22.20909119 99.57035065 -65.70256042 26.030126572 94.36612701
		 -68.54407501 27.030151367 97.38434601 -65.70256042 31.84971046 93.088294983 -68.54407501 32.20036316 96.24846649
		 -65.70256042 37.80780029 93.048339844 -68.54407501 37.49375153 96.21237183 -65.70256042 43.64399338 94.24800873
		 -68.54407501 42.67897034 97.27759552 -65.70256042 49.10322952 96.63489532 -68.54407501 47.5294075 99.39763641
		 -65.70256042 53.94689941 100.10464478 -68.54407501 51.83306122 102.47980499 -65.70256042 57.96333313 104.50563812
		 -68.54407501 55.40186691 106.3894043 -65.70256042 60.97697449 109.64550781 -68.54407501 58.07982254 110.95555115
		 -65.70256042 62.85612488 115.29964447 -68.54407501 59.74991226 115.97870636 89.76550293 68.11952972 -137.23513794
		 89.76550293 64.29701996 -137.22648621 89.76550293 63.69873428 -131.13484192 89.76550293 67.43951416 -130.34855652
		 88.35949707 69.20457458 -129.99423218 88.35949707 69.91967773 -137.2555542 66.33499908 69.91967773 -137.2555542
		 66.33499908 69.20457458 -129.99423218 89.76550293 61.84700394 -125.30069733 89.76550293 65.34255981 -123.75385284
		 88.35949707 66.99537659 -123.040313721 66.33499908 66.99537659 -123.040313721 89.76550293 58.82275391 -119.97905731
		 89.76550293 61.9203186 -117.73923492 88.35949707 63.38866425 -116.69763947 66.33499908 63.38866425 -116.69763947
		 89.76550293 54.75815201 -115.40244293 89.76550293 57.32234192 -112.56755829 88.35949707 58.54204178 -111.24343872
		 66.33499908 58.54204178 -111.24343872 89.76550293 49.83084488 -111.77095795 89.76550293 51.74959564 -108.4648819
		 88.35949707 52.66734695 -106.91610718 66.33499908 52.66734695 -106.91610718 89.76550293 44.25618744 -109.24326324
		 89.76550293 45.44563675 -105.61051178 88.35949707 46.021316528 -103.90475464 66.33499908 46.021316528 -103.90475464
		 89.76550293 38.27780151 -107.9298172 89.76550293 38.68596268 -104.12915802 88.35949707 38.89442444 -102.34100342
		 66.33499908 38.89442444 -102.34100342 89.76550293 32.15697479 -107.88808441 89.76550293 31.76601791 -104.085609436
		 88.35949707 31.59814262 -102.29317474 66.33499908 31.59814262 -102.29317474 89.76550293 26.16123962 -109.1198349
		 89.76550293 24.9882431 -105.4817276 88.35949707 24.45137596 -103.76338196 66.33499908 24.45137596 -103.76338196
		 89.76550293 20.5526123 -111.57125092 89.76550293 18.64884949 -108.25653839 88.35949707 17.76644897 -106.68734741
		 66.33499908 17.76644897 -106.68734741 89.76550293 15.57623291 -115.13521576 89.76550293 13.024894714 -112.28873444
		 88.35949707 11.83552456 -110.93730927 66.33499908 11.83552456 -110.93730927 89.76550293 11.4495697 -119.65592194
		 89.76550293 8.3621788 -117.40210724 88.35949707 6.91781616 -116.32750702 66.33499908 6.91781616 -116.32750702
		 89.76550293 8.35301208 -124.93582916 89.76550293 4.86448288 -123.37317657 88.35949707 3.22827435 -122.62233734
		 66.33499908 3.22827435 -122.62233734 89.76550293 6.42187119 -130.74415588 89.76550293 2.68468499 -129.94094849
		 88.35949707 0.92811751 -129.54670715 66.33499908 0.92811751 -129.54670715 89.76550293 5.74054909 -136.8270874
		 89.76550293 1.91803372 -136.81843567 88.35949707 0.11788642 -136.79803467 66.33499908 0.11788642 -136.79803467
		 89.76550293 6.33882904 -142.91874695 89.76550293 2.59804368 -143.70503235 88.35949707 0.83298838 -144.059356689
		 66.33499908 0.83298838 -144.059356689 89.76550293 8.19056225 -148.75289917 89.76550293 4.69500017 -150.29974365
		 88.35949707 3.042184114 -151.013275146 66.33499908 3.042184114 -151.013275146 89.76550293 11.21481133 -154.074523926
		 89.76550293 8.11724663 -156.31436157 88.35949707 6.64889956 -157.35595703 66.33499908 6.64889956 -157.35595703
		 89.76550293 15.27940941 -158.65113831 89.76550293 12.7152195 -161.48602295;
	setAttr ".vt[498:663]" 88.35949707 11.49552155 -162.81015015 66.33499908 11.49552155 -162.81015015
		 89.76550293 20.20671654 -162.28262329 89.76550293 18.28796577 -165.58869934 88.35949707 17.37021637 -167.13748169
		 66.33499908 17.37021637 -167.13748169 89.76550293 25.78137779 -164.81033325 89.76550293 24.59192848 -168.44308472
		 88.35949707 24.016246796 -170.14883423 66.33499908 24.016246796 -170.14883423 89.76550293 31.75976181 -166.12376404
		 89.76550293 31.35160065 -169.92443848 88.35949707 31.14313889 -171.71258545 66.33499908 31.14313889 -171.71258545
		 89.76550293 37.88058472 -166.16551208 89.76550293 38.2715416 -169.96798706 88.35949707 38.43941879 -171.76042175
		 66.33499908 38.43941879 -171.76042175 89.76550293 43.8763237 -164.9337616 89.76550293 45.049320221 -168.5718689
		 88.35949707 45.58618927 -170.29020691 66.33499908 45.58618927 -170.29020691 89.76550293 49.48495483 -162.48233032
		 89.76550293 51.38871765 -165.79705811 88.35949707 52.27111816 -167.3662262 66.33499908 52.27111816 -167.3662262
		 89.76550293 54.46133041 -158.91838074 89.76550293 57.01266861 -161.76486206 88.35949707 58.20203781 -163.11628723
		 66.33499908 58.20203781 -163.11628723 89.76550293 58.58798981 -154.3976593 89.76550293 61.67538071 -156.65148926
		 88.35949707 63.11974335 -157.72608948 66.33499908 63.11974335 -157.72608948 89.76550293 61.68454742 -149.11776733
		 89.76550293 65.17308044 -150.68041992 88.35949707 66.80929565 -151.43125916 66.33499908 66.80929565 -151.43125916
		 89.76550293 63.61569214 -143.30941772 89.76550293 67.35288239 -144.11262512 88.35949707 69.10944366 -144.5068512
		 66.33499908 69.10944366 -144.5068512 86.80031586 61.84566498 -131.51553345 86.80031586 62.40529251 -137.21359253
		 86.80031586 60.11358261 -126.05834198 86.80031586 57.28473282 -121.080543518 86.80031586 53.48275375 -116.79962921
		 86.80031586 48.873806 -113.40279388 86.80031586 43.6593399 -111.038414001 86.80031586 38.067230225 -109.80983734
		 86.80031586 32.34188461 -109.7707901 86.80031586 26.7335434 -110.922966 86.80031586 21.48729897 -113.21598053
		 86.80031586 16.83245468 -116.54967499 86.80031586 12.9724226 -120.77828217 86.80031586 10.075940132 -125.71704865
		 86.80031586 8.26957226 -131.15008545 86.80031586 7.6322732 -136.83999634 86.80031586 8.19189739 -142.53805542
		 86.80031586 9.92398453 -147.99523926 86.80031586 12.75283337 -152.97305298 86.80031586 16.55480957 -157.25395203
		 86.80031586 21.16375351 -160.65080261 86.80031586 26.37822342 -163.015182495 86.80031586 31.9703331 -164.2437439
		 86.80031586 37.69567871 -164.2828064 86.80031586 43.30402374 -163.13064575 86.80031586 48.55026627 -160.83760071
		 86.80031586 53.20510864 -157.50392151 86.80031586 57.065135956 -153.27529907 86.80031586 59.96162033 -148.33653259
		 86.80031586 61.76799393 -142.90348816 65.70256042 62.9355545 -131.28826904 68.54407501 59.82168961 -131.93133545
		 65.70256042 63.51861572 -137.21789551 68.54407501 60.33909607 -137.19949341 65.70256042 61.13240433 -125.60945892
		 68.54407501 58.22028351 -126.88587189 65.70256042 58.18795776 -120.42963409 68.54407501 55.6048584 -122.28362274
		 65.70256042 54.23091507 -115.97516632 68.54407501 52.089725494 -118.32569122 65.7025528 49.43420029 -112.44078827
		 68.54407501 47.82850266 -115.18512726 65.70256042 44.0074729919 -109.98090363 68.54407501 43.0074462891 -112.99912262
		 65.70256042 38.1878891 -108.70307159 68.54407501 37.83723831 -111.8632431 65.70256042 32.22979355 -108.66310883
		 68.54407501 32.54384613 -111.82714081 65.70256042 26.39360619 -109.8628006 68.54407501 27.35862923 -112.89238739
		 65.70256042 20.93437386 -112.24967194 68.54407501 22.50819397 -115.012413025 65.70256042 16.090698242 -115.71942902
		 68.54407501 18.20453644 -118.094581604 65.70256042 12.07425499 -120.12039948 68.54407501 14.63572884 -122.00415802
		 65.70256042 9.060622215 -125.26027679 68.54407501 11.95777321 -126.57032013 65.70256042 7.18147516 -130.91439819
		 68.54407501 10.28768826 -131.59346008 65.70256042 6.51895142 -136.83569336 68.54407501 9.69847107 -136.85409546
		 65.70256042 7.10200834 -142.76531982 68.54407501 10.21587372 -142.12225342 65.70256042 8.9051609 -148.44413757
		 68.54407501 11.81728268 -147.16772461 65.70256042 11.84960461 -153.6239624 68.54407501 14.43270588 -151.7699585
		 65.70256042 15.80664825 -158.078430176 68.54407501 17.94783974 -155.72790527 65.70256042 20.60335922 -161.61282349
		 68.54407501 22.20905685 -158.86846924 65.70256042 26.030092239 -164.072692871 68.54407501 27.030117035 -161.054473877
		 65.70256042 31.84967613 -165.3505249 68.54407501 32.20032883 -162.19033813 65.70256042 37.80776596 -165.39047241
		 68.54407501 37.49371719 -162.22644043 65.70256042 43.64395905 -164.1907959 68.54407501 42.678936 -161.16122437
		 65.70256042 49.10319519 -161.80392456 68.54407501 47.52937317 -159.041168213 65.70256042 53.94686508 -158.33416748
		 68.54407501 51.83302689 -155.95901489 65.70256042 57.9632988 -153.9331665 68.54407501 55.40183258 -152.049407959
		 65.70256042 60.97694016 -148.79330444 68.54407501 58.079788208 -147.48326111 65.70256042 62.85609055 -143.13917542
		 68.54407501 59.74987793 -142.46011353 89.76550293 68.11956787 121.20366669 89.76550293 64.29705048 121.21233368
		 89.76550293 63.69876862 127.30397034 89.76550293 67.43954468 128.090255737 88.35949707 69.20461273 128.44458008
		 88.35949707 69.91971588 121.18325806 66.33499908 69.91971588 121.18325806 66.33499908 69.20461273 128.44458008
		 89.76550293 61.84703827 133.13812256 89.76550293 65.34259033 134.68496704 88.35949707 66.99541473 135.39851379
		 66.33499908 66.99541473 135.39851379 89.76550293 58.82278824 138.45976257 89.76550293 61.92035294 140.69958496
		 88.35949707 63.38869858 141.74118042 66.33499908 63.38869858 141.74118042 89.76550293 54.75818634 143.036376953
		 89.76550293 57.32237625 145.8712616 88.35949707 58.54207611 147.19538879 66.33499908 58.54207611 147.19538879
		 89.76550293 49.83087921 146.66786194 89.76550293 51.74962997 149.97393799 88.35949707 52.66738129 151.52272034
		 66.33499908 52.66738129 151.52272034 89.76550293 44.25622177 149.19555664 89.76550293 45.44567108 152.82830811
		 88.35949707 46.021350861 154.53407288 66.33499908 46.021350861 154.53407288 89.76550293 38.27783585 150.50901794
		 89.76550293 38.68599701 154.30966187 88.35949707 38.89445877 156.097808838 66.33499908 38.89445877 156.097808838
		 89.76550293 32.15700912 150.55075073 89.76550293 31.76605225 154.35321045;
	setAttr ".vt[664:829]" 88.35949707 31.59817696 156.14564514 66.33499908 31.59817696 156.14564514
		 89.76550293 26.16127396 149.31898499 89.76550293 24.98827744 152.95709229 88.35949707 24.45141029 154.6754303
		 66.33499908 24.45141029 154.6754303 89.76550293 20.55264664 146.86756897 89.76550293 18.64888382 150.18228149
		 88.35949707 17.76648331 151.75146484 66.33499908 17.76648331 151.75146484 89.76550293 15.57626724 143.30360413
		 89.76550293 13.024929047 146.15008545 88.35949707 11.83555889 147.50151062 66.33499908 11.83555889 147.50151062
		 89.76550293 11.44960403 138.78289795 89.76550293 8.36221313 141.036712646 88.35949707 6.91785049 142.11131287
		 66.33499908 6.91785049 142.11131287 89.76550293 8.35304642 133.50299072 89.76550293 4.86451721 135.065643311
		 88.35949707 3.2283082 135.81648254 66.33499908 3.2283082 135.81648254 89.76550293 6.42190552 127.69465637
		 89.76550293 2.68471909 128.49786377 88.35949707 0.92815173 128.89212036 66.33499908 0.92815173 128.89212036
		 89.76550293 5.74058342 121.61172485 89.76550293 1.91806793 121.62037659 88.35949707 0.11792064 121.64077759
		 66.33499908 0.11792064 121.64077759 89.76550293 6.33886337 115.52006531 89.76550293 2.59807777 114.73377991
		 88.35949707 0.83302259 114.3794632 66.33499908 0.83302259 114.3794632 89.76550293 8.19059563 109.68592072
		 89.76550293 4.6950345 108.13907623 88.35949707 3.042218208 107.42552948 66.33499908 3.042218208 107.42552948
		 89.76550293 11.21484566 104.3642807 89.76550293 8.11728096 102.12446594 88.35949707 6.64893389 101.082862854
		 66.33499908 6.64893389 101.082862854 89.76550293 15.27944374 99.78767395 89.76550293 12.71525383 96.95278931
		 88.35949707 11.49555588 95.62866974 66.33499908 11.49555588 95.62866974 89.76550293 20.20675087 96.15618896
		 89.76550293 18.28800011 92.85011292 88.35949707 17.3702507 91.3013382 66.33499908 17.3702507 91.3013382
		 89.76550293 25.78141212 93.62848663 89.76550293 24.59196281 89.99572754 88.35949707 24.016281128 88.28999329
		 66.33499908 24.016281128 88.28999329 89.76550293 31.75979614 92.31504822 89.76550293 31.35163498 88.51438141
		 88.35949707 31.14317322 86.72621918 66.33499908 31.14317322 86.72621918 89.76550293 37.88061905 92.27330017
		 89.76550293 38.27157593 88.4708252 88.35949707 38.43945313 86.67839813 66.33499908 38.43945313 86.67839813
		 89.76550293 43.87635803 93.50505066 89.76550293 45.049354553 89.86695099 88.35949707 45.5862236 88.14860535
		 66.33499908 45.5862236 88.14860535 89.76550293 49.48498917 95.95648193 89.76550293 51.38875198 92.64175415
		 88.35949707 52.2711525 91.07257843 66.33499908 52.2711525 91.07257843 89.76550293 54.46136475 99.52043152
		 89.76550293 57.012702942 96.6739502 88.35949707 58.20207214 95.32252502 66.33499908 58.20207214 95.32252502
		 89.76550293 58.58802414 104.041152954 89.76550293 61.67541504 101.78733063 88.35949707 63.11977768 100.71272278
		 66.33499908 63.11977768 100.71272278 89.76550293 61.68458176 109.32105255 89.76550293 65.17311096 107.75839233
		 88.35949707 66.80932617 107.00756073 66.33499908 66.80932617 107.00756073 89.76550293 63.61572647 115.12939453
		 89.76550293 67.3529129 114.32619476 88.35949707 69.10948181 113.93196106 66.33499908 69.10948181 113.93196106
		 86.80031586 61.84569931 126.92328644 86.80031586 62.40532684 121.22522736 86.80031586 60.11361694 132.38047791
		 86.80031586 57.28476715 137.35827637 86.80031586 53.48278809 141.63920593 86.80031586 48.87384033 145.036026001
		 86.80031586 43.65937424 147.40040588 86.80031586 38.067264557 148.62898254 86.80031586 32.34191513 148.66802979
		 86.80031586 26.73357773 147.51585388 86.80031586 21.4873333 145.22283936 86.80031586 16.83248711 141.8891449
		 86.80031586 12.97245693 137.66053772 86.80031586 10.075974464 132.72177124 86.80031586 8.26960659 127.28872681
		 86.80031586 7.63230753 121.59882355 86.80031586 8.19193172 115.90075684 86.80031586 9.92401886 110.44356537
		 86.80031586 12.7528677 105.46576691 86.80031586 16.5548439 101.18486023 86.80031586 21.16378784 97.78800964
		 86.80031586 26.37825775 95.42362976 86.80031586 31.97036743 94.19506073 86.80031586 37.69571304 94.15600586
		 86.80031586 43.30405807 95.30817413 86.80031586 48.5503006 97.60121155 86.80031586 53.20514297 100.93489838
		 86.80031586 57.065170288 105.16352081 86.80031586 59.96165466 110.10227966 86.80031586 61.76802826 115.53533173
		 65.70256042 62.93558884 127.15054321 68.54407501 59.82172394 126.50748444 65.70256042 63.51865005 121.22091675
		 68.54407501 60.3391304 121.23932648 65.70256042 61.13243866 132.82936096 68.54407501 58.22031784 131.552948
		 65.70256042 58.1879921 138.009185791 68.54407501 55.60489273 136.15519714 65.70256042 54.2309494 142.46365356
		 68.54407501 52.089759827 140.11312866 65.7025528 49.43423462 145.99804688 68.54407501 47.82853699 143.25369263
		 65.70256042 44.0075073242 148.45791626 68.54407501 43.0074806213 145.43969727 65.70256042 38.18792343 149.73576355
		 68.54407501 37.83727264 146.57557678 65.70256042 32.22982788 149.77571106 68.54407501 32.54388046 146.61167908
		 65.70256042 26.39364052 148.57601929 68.54407501 27.35866356 145.5464325 65.70256042 20.93440819 146.18914795
		 68.54407501 22.5082283 143.42640686 65.70256042 16.090730667 142.71939087 68.54407501 18.20457077 140.34423828
		 65.70256042 12.074289322 138.31842041 68.54407501 14.63576317 136.43466187 65.70256042 9.060656548 133.17854309
		 68.54407501 11.95780754 131.86849976 65.70256042 7.18150949 127.52441406 68.54407501 10.28772259 126.84535217
		 65.70256042 6.51898575 121.60313416 68.54407501 9.6985054 121.58472443 65.70256042 7.10204268 115.67350769
		 68.54407501 10.21590805 116.31655884 65.70256042 8.90519524 109.99467468 68.54407501 11.81731701 111.27108765
		 65.70256042 11.84963894 104.81485748 68.54407501 14.43274021 106.66884613 65.70256042 15.80668259 100.36038208
		 68.54407501 17.94787407 102.71091461 65.70256042 20.60339355 96.8259964 68.54407501 22.20909119 99.57035065
		 65.70256042 26.030126572 94.36612701 68.54407501 27.030151367 97.38434601 65.70256042 31.84971046 93.088294983
		 68.54407501 32.20036316 96.24846649 65.70256042 37.80780029 93.048339844 68.54407501 37.49375153 96.21237183
		 65.70256042 43.64399338 94.24800873 68.54407501 42.67897034 97.27759552;
	setAttr ".vt[830:839]" 65.70256042 49.10322952 96.63489532 68.54407501 47.5294075 99.39763641
		 65.70256042 53.94689941 100.10464478 68.54407501 51.83306122 102.47980499 65.70256042 57.96333313 104.50563812
		 68.54407501 55.40186691 106.3894043 65.70256042 60.97697449 109.64550781 68.54407501 58.07982254 110.95555115
		 65.70256042 62.85612488 115.29964447 68.54407501 59.74991226 115.97870636;
	setAttr -s 1560 ".ed";
	setAttr ".ed[0:165]"  0 1 1 2 1 0 0 3 0 3 2 1 3 4 1 5 4 0 0 5 1 7 6 0 6 5 1
		 7 4 1 8 2 0 3 9 0 9 8 1 9 10 1 4 10 0 11 7 0 11 10 1 12 8 0 9 13 0 13 12 1 13 14 1
		 10 14 0 15 11 0 15 14 1 16 12 0 13 17 0 17 16 1 17 18 1 14 18 0 19 15 0 19 18 1 20 16 0
		 17 21 0 21 20 1 21 22 1 18 22 0 23 19 0 23 22 1 24 20 0 21 25 0 25 24 1 25 26 1 22 26 0
		 27 23 0 27 26 1 28 24 0 25 29 0 29 28 1 29 30 1 26 30 0 31 27 0 31 30 1 32 28 0 29 33 0
		 33 32 1 33 34 1 30 34 0 35 31 0 35 34 1 36 32 0 33 37 0 37 36 1 37 38 1 34 38 0 39 35 0
		 39 38 1 40 36 0 37 41 0 41 40 1 41 42 1 38 42 0 43 39 0 43 42 1 44 40 0 41 45 0 45 44 1
		 45 46 1 42 46 0 47 43 0 47 46 1 48 44 0 45 49 0 49 48 1 49 50 1 46 50 0 51 47 0 51 50 1
		 52 48 0 49 53 0 53 52 1 53 54 1 50 54 0 55 51 0 55 54 1 56 52 0 53 57 0 57 56 1 57 58 1
		 54 58 0 59 55 0 59 58 1 60 56 0 57 61 0 61 60 1 61 62 1 58 62 0 63 59 0 63 62 1 64 60 0
		 61 65 0 65 64 1 65 66 1 62 66 0 67 63 0 67 66 1 68 64 0 65 69 0 69 68 1 69 70 1 66 70 0
		 71 67 0 71 70 1 72 68 0 69 73 0 73 72 1 73 74 1 70 74 0 75 71 0 75 74 1 76 72 0 73 77 0
		 77 76 1 77 78 1 74 78 0 79 75 0 79 78 1 80 76 0 77 81 0 81 80 1 81 82 1 78 82 0 83 79 0
		 83 82 1 84 80 0 81 85 0 85 84 1 85 86 1 82 86 0 87 83 0 87 86 1 88 84 0 85 89 0 89 88 1
		 89 90 1 86 90 0 91 87 0 91 90 1 92 88 0 89 93 0 93 92 1 93 94 1 90 94 0 95 91 0 95 94 1
		 96 92 0 93 97 0;
	setAttr ".ed[166:331]" 97 96 1 97 98 1 94 98 0 99 95 0 99 98 1 100 96 0 97 101 0
		 101 100 1 101 102 1 98 102 0 103 99 0 103 102 1 104 100 0 101 105 0 105 104 1 105 106 1
		 102 106 0 107 103 0 107 106 1 108 104 0 105 109 0 109 108 1 109 110 1 106 110 0 111 107 0
		 111 110 1 112 108 0 109 113 0 113 112 1 113 114 1 110 114 0 115 111 0 115 114 1 116 112 0
		 113 117 0 117 116 1 117 118 1 114 118 0 119 115 0 119 118 1 1 116 0 117 0 0 118 5 0
		 6 119 0 2 120 1 1 121 1 120 121 0 8 122 1 122 120 0 12 123 1 123 122 0 16 124 1 124 123 0
		 20 125 1 125 124 0 24 126 1 126 125 0 28 127 1 127 126 0 32 128 1 128 127 0 36 129 1
		 129 128 0 40 130 1 130 129 0 44 131 1 131 130 0 48 132 1 132 131 0 52 133 1 133 132 0
		 56 134 1 134 133 0 60 135 1 135 134 0 64 136 1 136 135 0 68 137 1 137 136 0 72 138 1
		 138 137 0 76 139 1 139 138 0 80 140 1 140 139 0 84 141 1 141 140 0 88 142 1 142 141 0
		 92 143 1 143 142 0 96 144 1 144 143 0 100 145 1 145 144 0 104 146 1 146 145 0 108 147 1
		 147 146 0 112 148 1 148 147 0 116 149 1 149 148 0 121 149 0 7 150 1 6 152 1 119 208 1
		 115 206 1 111 204 1 107 202 1 103 200 1 99 198 1 95 196 1 91 194 1 87 192 1 83 190 1
		 79 188 1 75 186 1 71 184 1 67 182 1 63 180 1 59 178 1 55 176 1 51 174 1 47 172 1
		 43 170 1 39 168 1 35 166 1 31 164 1 27 162 1 23 160 1 19 158 1 15 156 1 11 154 1
		 151 153 0 150 154 0 153 209 0 152 150 0 155 151 0 154 156 0 157 155 0 156 158 0 159 157 0
		 158 160 0 161 159 0 160 162 0 163 161 0 162 164 0 165 163 0 164 166 0 167 165 0 166 168 0
		 169 167 0 168 170 0 171 169 0 170 172 0 173 171 0 172 174 0 175 173 0 174 176 0 177 175 0
		 176 178 0 179 177 0 178 180 0 181 179 0 180 182 0;
	setAttr ".ed[332:497]" 183 181 0 182 184 0 185 183 0 184 186 0 187 185 0 186 188 0
		 189 187 0 188 190 0 191 189 0 190 192 0 193 191 0 192 194 0 195 193 0 194 196 0 197 195 0
		 196 198 0 199 197 0 198 200 0 201 199 0 200 202 0 203 201 0 202 204 0 205 203 0 204 206 0
		 207 205 0 206 208 0 209 207 0 208 152 0 151 150 1 152 153 1 155 154 1 208 209 1 157 156 1
		 159 158 1 161 160 1 163 162 1 165 164 1 167 166 1 169 168 1 171 170 1 173 172 1 175 174 1
		 177 176 1 179 178 1 181 180 1 183 182 1 185 184 1 187 186 1 189 188 1 191 190 1 193 192 1
		 195 194 1 197 196 1 199 198 1 201 200 1 203 202 1 205 204 1 207 206 1 210 211 1 212 211 0
		 210 213 0 213 212 1 213 214 1 215 214 0 210 215 1 217 216 0 216 215 1 217 214 1 218 212 0
		 213 219 0 219 218 1 219 220 1 214 220 0 221 217 0 221 220 1 222 218 0 219 223 0 223 222 1
		 223 224 1 220 224 0 225 221 0 225 224 1 226 222 0 223 227 0 227 226 1 227 228 1 224 228 0
		 229 225 0 229 228 1 230 226 0 227 231 0 231 230 1 231 232 1 228 232 0 233 229 0 233 232 1
		 234 230 0 231 235 0 235 234 1 235 236 1 232 236 0 237 233 0 237 236 1 238 234 0 235 239 0
		 239 238 1 239 240 1 236 240 0 241 237 0 241 240 1 242 238 0 239 243 0 243 242 1 243 244 1
		 240 244 0 245 241 0 245 244 1 246 242 0 243 247 0 247 246 1 247 248 1 244 248 0 249 245 0
		 249 248 1 250 246 0 247 251 0 251 250 1 251 252 1 248 252 0 253 249 0 253 252 1 254 250 0
		 251 255 0 255 254 1 255 256 1 252 256 0 257 253 0 257 256 1 258 254 0 255 259 0 259 258 1
		 259 260 1 256 260 0 261 257 0 261 260 1 262 258 0 259 263 0 263 262 1 263 264 1 260 264 0
		 265 261 0 265 264 1 266 262 0 263 267 0 267 266 1 267 268 1 264 268 0 269 265 0 269 268 1
		 270 266 0 267 271 0 271 270 1 271 272 1 268 272 0 273 269 0 273 272 1;
	setAttr ".ed[498:663]" 274 270 0 271 275 0 275 274 1 275 276 1 272 276 0 277 273 0
		 277 276 1 278 274 0 275 279 0 279 278 1 279 280 1 276 280 0 281 277 0 281 280 1 282 278 0
		 279 283 0 283 282 1 283 284 1 280 284 0 285 281 0 285 284 1 286 282 0 283 287 0 287 286 1
		 287 288 1 284 288 0 289 285 0 289 288 1 290 286 0 287 291 0 291 290 1 291 292 1 288 292 0
		 293 289 0 293 292 1 294 290 0 291 295 0 295 294 1 295 296 1 292 296 0 297 293 0 297 296 1
		 298 294 0 295 299 0 299 298 1 299 300 1 296 300 0 301 297 0 301 300 1 302 298 0 299 303 0
		 303 302 1 303 304 1 300 304 0 305 301 0 305 304 1 306 302 0 303 307 0 307 306 1 307 308 1
		 304 308 0 309 305 0 309 308 1 310 306 0 307 311 0 311 310 1 311 312 1 308 312 0 313 309 0
		 313 312 1 314 310 0 311 315 0 315 314 1 315 316 1 312 316 0 317 313 0 317 316 1 318 314 0
		 315 319 0 319 318 1 319 320 1 316 320 0 321 317 0 321 320 1 322 318 0 319 323 0 323 322 1
		 323 324 1 320 324 0 325 321 0 325 324 1 326 322 0 323 327 0 327 326 1 327 328 1 324 328 0
		 329 325 0 329 328 1 211 326 0 327 210 0 328 215 0 216 329 0 212 330 1 211 331 1 330 331 0
		 218 332 1 332 330 0 222 333 1 333 332 0 226 334 1 334 333 0 230 335 1 335 334 0 234 336 1
		 336 335 0 238 337 1 337 336 0 242 338 1 338 337 0 246 339 1 339 338 0 250 340 1 340 339 0
		 254 341 1 341 340 0 258 342 1 342 341 0 262 343 1 343 342 0 266 344 1 344 343 0 270 345 1
		 345 344 0 274 346 1 346 345 0 278 347 1 347 346 0 282 348 1 348 347 0 286 349 1 349 348 0
		 290 350 1 350 349 0 294 351 1 351 350 0 298 352 1 352 351 0 302 353 1 353 352 0 306 354 1
		 354 353 0 310 355 1 355 354 0 314 356 1 356 355 0 318 357 1 357 356 0 322 358 1 358 357 0
		 326 359 1 359 358 0 331 359 0 217 360 1 216 362 1 329 418 1 325 416 1;
	setAttr ".ed[664:829]" 321 414 1 317 412 1 313 410 1 309 408 1 305 406 1 301 404 1
		 297 402 1 293 400 1 289 398 1 285 396 1 281 394 1 277 392 1 273 390 1 269 388 1 265 386 1
		 261 384 1 257 382 1 253 380 1 249 378 1 245 376 1 241 374 1 237 372 1 233 370 1 229 368 1
		 225 366 1 221 364 1 361 363 0 360 364 0 363 419 0 362 360 0 365 361 0 364 366 0 367 365 0
		 366 368 0 369 367 0 368 370 0 371 369 0 370 372 0 373 371 0 372 374 0 375 373 0 374 376 0
		 377 375 0 376 378 0 379 377 0 378 380 0 381 379 0 380 382 0 383 381 0 382 384 0 385 383 0
		 384 386 0 387 385 0 386 388 0 389 387 0 388 390 0 391 389 0 390 392 0 393 391 0 392 394 0
		 395 393 0 394 396 0 397 395 0 396 398 0 399 397 0 398 400 0 401 399 0 400 402 0 403 401 0
		 402 404 0 405 403 0 404 406 0 407 405 0 406 408 0 409 407 0 408 410 0 411 409 0 410 412 0
		 413 411 0 412 414 0 415 413 0 414 416 0 417 415 0 416 418 0 419 417 0 418 362 0 361 360 1
		 362 363 1 365 364 1 418 419 1 367 366 1 369 368 1 371 370 1 373 372 1 375 374 1 377 376 1
		 379 378 1 381 380 1 383 382 1 385 384 1 387 386 1 389 388 1 391 390 1 393 392 1 395 394 1
		 397 396 1 399 398 1 401 400 1 403 402 1 405 404 1 407 406 1 409 408 1 411 410 1 413 412 1
		 415 414 1 417 416 1 420 421 1 422 421 0 420 423 0 423 422 1 423 424 1 425 424 0 420 425 1
		 427 426 0 426 425 1 427 424 1 428 422 0 423 429 0 429 428 1 429 430 1 424 430 0 431 427 0
		 431 430 1 432 428 0 429 433 0 433 432 1 433 434 1 430 434 0 435 431 0 435 434 1 436 432 0
		 433 437 0 437 436 1 437 438 1 434 438 0 439 435 0 439 438 1 440 436 0 437 441 0 441 440 1
		 441 442 1 438 442 0 443 439 0 443 442 1 444 440 0 441 445 0 445 444 1 445 446 1 442 446 0
		 447 443 0 447 446 1 448 444 0 445 449 0 449 448 1 449 450 1 446 450 0;
	setAttr ".ed[830:995]" 451 447 0 451 450 1 452 448 0 449 453 0 453 452 1 453 454 1
		 450 454 0 455 451 0 455 454 1 456 452 0 453 457 0 457 456 1 457 458 1 454 458 0 459 455 0
		 459 458 1 460 456 0 457 461 0 461 460 1 461 462 1 458 462 0 463 459 0 463 462 1 464 460 0
		 461 465 0 465 464 1 465 466 1 462 466 0 467 463 0 467 466 1 468 464 0 465 469 0 469 468 1
		 469 470 1 466 470 0 471 467 0 471 470 1 472 468 0 469 473 0 473 472 1 473 474 1 470 474 0
		 475 471 0 475 474 1 476 472 0 473 477 0 477 476 1 477 478 1 474 478 0 479 475 0 479 478 1
		 480 476 0 477 481 0 481 480 1 481 482 1 478 482 0 483 479 0 483 482 1 484 480 0 481 485 0
		 485 484 1 485 486 1 482 486 0 487 483 0 487 486 1 488 484 0 485 489 0 489 488 1 489 490 1
		 486 490 0 491 487 0 491 490 1 492 488 0 489 493 0 493 492 1 493 494 1 490 494 0 495 491 0
		 495 494 1 496 492 0 493 497 0 497 496 1 497 498 1 494 498 0 499 495 0 499 498 1 500 496 0
		 497 501 0 501 500 1 501 502 1 498 502 0 503 499 0 503 502 1 504 500 0 501 505 0 505 504 1
		 505 506 1 502 506 0 507 503 0 507 506 1 508 504 0 505 509 0 509 508 1 509 510 1 506 510 0
		 511 507 0 511 510 1 512 508 0 509 513 0 513 512 1 513 514 1 510 514 0 515 511 0 515 514 1
		 516 512 0 513 517 0 517 516 1 517 518 1 514 518 0 519 515 0 519 518 1 520 516 0 517 521 0
		 521 520 1 521 522 1 518 522 0 523 519 0 523 522 1 524 520 0 521 525 0 525 524 1 525 526 1
		 522 526 0 527 523 0 527 526 1 528 524 0 525 529 0 529 528 1 529 530 1 526 530 0 531 527 0
		 531 530 1 532 528 0 529 533 0 533 532 1 533 534 1 530 534 0 535 531 0 535 534 1 536 532 0
		 533 537 0 537 536 1 537 538 1 534 538 0 539 535 0 539 538 1 421 536 0 537 420 0 538 425 0
		 426 539 0 422 540 1 421 541 1 540 541 0 428 542 1 542 540 0 432 543 1;
	setAttr ".ed[996:1161]" 543 542 0 436 544 1 544 543 0 440 545 1 545 544 0 444 546 1
		 546 545 0 448 547 1 547 546 0 452 548 1 548 547 0 456 549 1 549 548 0 460 550 1 550 549 0
		 464 551 1 551 550 0 468 552 1 552 551 0 472 553 1 553 552 0 476 554 1 554 553 0 480 555 1
		 555 554 0 484 556 1 556 555 0 488 557 1 557 556 0 492 558 1 558 557 0 496 559 1 559 558 0
		 500 560 1 560 559 0 504 561 1 561 560 0 508 562 1 562 561 0 512 563 1 563 562 0 516 564 1
		 564 563 0 520 565 1 565 564 0 524 566 1 566 565 0 528 567 1 567 566 0 532 568 1 568 567 0
		 536 569 1 569 568 0 541 569 0 427 570 1 426 572 1 539 628 1 535 626 1 531 624 1 527 622 1
		 523 620 1 519 618 1 515 616 1 511 614 1 507 612 1 503 610 1 499 608 1 495 606 1 491 604 1
		 487 602 1 483 600 1 479 598 1 475 596 1 471 594 1 467 592 1 463 590 1 459 588 1 455 586 1
		 451 584 1 447 582 1 443 580 1 439 578 1 435 576 1 431 574 1 571 573 0 570 574 0 573 629 0
		 572 570 0 575 571 0 574 576 0 577 575 0 576 578 0 579 577 0 578 580 0 581 579 0 580 582 0
		 583 581 0 582 584 0 585 583 0 584 586 0 587 585 0 586 588 0 589 587 0 588 590 0 591 589 0
		 590 592 0 593 591 0 592 594 0 595 593 0 594 596 0 597 595 0 596 598 0 599 597 0 598 600 0
		 601 599 0 600 602 0 603 601 0 602 604 0 605 603 0 604 606 0 607 605 0 606 608 0 609 607 0
		 608 610 0 611 609 0 610 612 0 613 611 0 612 614 0 615 613 0 614 616 0 617 615 0 616 618 0
		 619 617 0 618 620 0 621 619 0 620 622 0 623 621 0 622 624 0 625 623 0 624 626 0 627 625 0
		 626 628 0 629 627 0 628 572 0 571 570 1 572 573 1 575 574 1 628 629 1 577 576 1 579 578 1
		 581 580 1 583 582 1 585 584 1 587 586 1 589 588 1 591 590 1 593 592 1 595 594 1 597 596 1
		 599 598 1 601 600 1 603 602 1 605 604 1 607 606 1 609 608 1 611 610 1;
	setAttr ".ed[1162:1327]" 613 612 1 615 614 1 617 616 1 619 618 1 621 620 1 623 622 1
		 625 624 1 627 626 1 630 631 1 632 631 0 630 633 0 633 632 1 633 634 1 635 634 0 630 635 1
		 637 636 0 636 635 1 637 634 1 638 632 0 633 639 0 639 638 1 639 640 1 634 640 0 641 637 0
		 641 640 1 642 638 0 639 643 0 643 642 1 643 644 1 640 644 0 645 641 0 645 644 1 646 642 0
		 643 647 0 647 646 1 647 648 1 644 648 0 649 645 0 649 648 1 650 646 0 647 651 0 651 650 1
		 651 652 1 648 652 0 653 649 0 653 652 1 654 650 0 651 655 0 655 654 1 655 656 1 652 656 0
		 657 653 0 657 656 1 658 654 0 655 659 0 659 658 1 659 660 1 656 660 0 661 657 0 661 660 1
		 662 658 0 659 663 0 663 662 1 663 664 1 660 664 0 665 661 0 665 664 1 666 662 0 663 667 0
		 667 666 1 667 668 1 664 668 0 669 665 0 669 668 1 670 666 0 667 671 0 671 670 1 671 672 1
		 668 672 0 673 669 0 673 672 1 674 670 0 671 675 0 675 674 1 675 676 1 672 676 0 677 673 0
		 677 676 1 678 674 0 675 679 0 679 678 1 679 680 1 676 680 0 681 677 0 681 680 1 682 678 0
		 679 683 0 683 682 1 683 684 1 680 684 0 685 681 0 685 684 1 686 682 0 683 687 0 687 686 1
		 687 688 1 684 688 0 689 685 0 689 688 1 690 686 0 687 691 0 691 690 1 691 692 1 688 692 0
		 693 689 0 693 692 1 694 690 0 691 695 0 695 694 1 695 696 1 692 696 0 697 693 0 697 696 1
		 698 694 0 695 699 0 699 698 1 699 700 1 696 700 0 701 697 0 701 700 1 702 698 0 699 703 0
		 703 702 1 703 704 1 700 704 0 705 701 0 705 704 1 706 702 0 703 707 0 707 706 1 707 708 1
		 704 708 0 709 705 0 709 708 1 710 706 0 707 711 0 711 710 1 711 712 1 708 712 0 713 709 0
		 713 712 1 714 710 0 711 715 0 715 714 1 715 716 1 712 716 0 717 713 0 717 716 1 718 714 0
		 715 719 0 719 718 1 719 720 1 716 720 0 721 717 0 721 720 1 722 718 0;
	setAttr ".ed[1328:1493]" 719 723 0 723 722 1 723 724 1 720 724 0 725 721 0 725 724 1
		 726 722 0 723 727 0 727 726 1 727 728 1 724 728 0 729 725 0 729 728 1 730 726 0 727 731 0
		 731 730 1 731 732 1 728 732 0 733 729 0 733 732 1 734 730 0 731 735 0 735 734 1 735 736 1
		 732 736 0 737 733 0 737 736 1 738 734 0 735 739 0 739 738 1 739 740 1 736 740 0 741 737 0
		 741 740 1 742 738 0 739 743 0 743 742 1 743 744 1 740 744 0 745 741 0 745 744 1 746 742 0
		 743 747 0 747 746 1 747 748 1 744 748 0 749 745 0 749 748 1 631 746 0 747 630 0 748 635 0
		 636 749 0 632 750 1 631 751 1 750 751 0 638 752 1 752 750 0 642 753 1 753 752 0 646 754 1
		 754 753 0 650 755 1 755 754 0 654 756 1 756 755 0 658 757 1 757 756 0 662 758 1 758 757 0
		 666 759 1 759 758 0 670 760 1 760 759 0 674 761 1 761 760 0 678 762 1 762 761 0 682 763 1
		 763 762 0 686 764 1 764 763 0 690 765 1 765 764 0 694 766 1 766 765 0 698 767 1 767 766 0
		 702 768 1 768 767 0 706 769 1 769 768 0 710 770 1 770 769 0 714 771 1 771 770 0 718 772 1
		 772 771 0 722 773 1 773 772 0 726 774 1 774 773 0 730 775 1 775 774 0 734 776 1 776 775 0
		 738 777 1 777 776 0 742 778 1 778 777 0 746 779 1 779 778 0 751 779 0 637 780 1 636 782 1
		 749 838 1 745 836 1 741 834 1 737 832 1 733 830 1 729 828 1 725 826 1 721 824 1 717 822 1
		 713 820 1 709 818 1 705 816 1 701 814 1 697 812 1 693 810 1 689 808 1 685 806 1 681 804 1
		 677 802 1 673 800 1 669 798 1 665 796 1 661 794 1 657 792 1 653 790 1 649 788 1 645 786 1
		 641 784 1 781 783 0 780 784 0 783 839 0 782 780 0 785 781 0 784 786 0 787 785 0 786 788 0
		 789 787 0 788 790 0 791 789 0 790 792 0 793 791 0 792 794 0 795 793 0 794 796 0 797 795 0
		 796 798 0 799 797 0 798 800 0 801 799 0 800 802 0 803 801 0 802 804 0;
	setAttr ".ed[1494:1559]" 805 803 0 804 806 0 807 805 0 806 808 0 809 807 0 808 810 0
		 811 809 0 810 812 0 813 811 0 812 814 0 815 813 0 814 816 0 817 815 0 816 818 0 819 817 0
		 818 820 0 821 819 0 820 822 0 823 821 0 822 824 0 825 823 0 824 826 0 827 825 0 826 828 0
		 829 827 0 828 830 0 831 829 0 830 832 0 833 831 0 832 834 0 835 833 0 834 836 0 837 835 0
		 836 838 0 839 837 0 838 782 0 781 780 1 782 783 1 785 784 1 838 839 1 787 786 1 789 788 1
		 791 790 1 793 792 1 795 794 1 797 796 1 799 798 1 801 800 1 803 802 1 805 804 1 807 806 1
		 809 808 1 811 810 1 813 812 1 815 814 1 817 816 1 819 818 1 821 820 1 823 822 1 825 824 1
		 827 826 1 829 828 1 831 830 1 833 832 1 835 834 1 837 836 1;
	setAttr -s 720 ".n";
	setAttr ".n[0:165]" -type "float3"  1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20;
	setAttr ".n[166:331]" -type "float3"  1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20;
	setAttr ".n[332:497]" -type "float3"  1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20;
	setAttr ".n[498:663]" -type "float3"  1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20;
	setAttr ".n[664:719]" -type "float3"  1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20;
	setAttr -s 720 -ch 2880 ".fc";
	setAttr ".fc[0:499]" -type "polyFaces" 
		f 4 270 -304 -272 -8
		f 4 -4 -3 0 -2
		f 4 -7 2 4 -6
		f 4 5 -10 7 8
		f 4 299 -302 -271 -16
		f 4 -13 -12 3 -11
		f 4 -5 11 13 -15
		f 4 14 -17 15 9
		f 4 298 -306 -300 -23
		f 4 -20 -19 12 -18
		f 4 -14 18 20 -22
		f 4 21 -24 22 16
		f 4 297 -308 -299 -30
		f 4 -27 -26 19 -25
		f 4 -21 25 27 -29
		f 4 28 -31 29 23
		f 4 296 -310 -298 -37
		f 4 -34 -33 26 -32
		f 4 -28 32 34 -36
		f 4 35 -38 36 30
		f 4 295 -312 -297 -44
		f 4 -41 -40 33 -39
		f 4 -35 39 41 -43
		f 4 42 -45 43 37
		f 4 294 -314 -296 -51
		f 4 -48 -47 40 -46
		f 4 -42 46 48 -50
		f 4 49 -52 50 44
		f 4 293 -316 -295 -58
		f 4 -55 -54 47 -53
		f 4 -49 53 55 -57
		f 4 56 -59 57 51
		f 4 292 -318 -294 -65
		f 4 -62 -61 54 -60
		f 4 -56 60 62 -64
		f 4 63 -66 64 58
		f 4 291 -320 -293 -72
		f 4 -69 -68 61 -67
		f 4 -63 67 69 -71
		f 4 70 -73 71 65
		f 4 290 -322 -292 -79
		f 4 -76 -75 68 -74
		f 4 -70 74 76 -78
		f 4 77 -80 78 72
		f 4 289 -324 -291 -86
		f 4 -83 -82 75 -81
		f 4 -77 81 83 -85
		f 4 84 -87 85 79
		f 4 288 -326 -290 -93
		f 4 -90 -89 82 -88
		f 4 -84 88 90 -92
		f 4 91 -94 92 86
		f 4 287 -328 -289 -100
		f 4 -97 -96 89 -95
		f 4 -91 95 97 -99
		f 4 98 -101 99 93
		f 4 286 -330 -288 -107
		f 4 -104 -103 96 -102
		f 4 -98 102 104 -106
		f 4 105 -108 106 100
		f 4 285 -332 -287 -114
		f 4 -111 -110 103 -109
		f 4 -105 109 111 -113
		f 4 112 -115 113 107
		f 4 284 -334 -286 -121
		f 4 -118 -117 110 -116
		f 4 -112 116 118 -120
		f 4 119 -122 120 114
		f 4 283 -336 -285 -128
		f 4 -125 -124 117 -123
		f 4 -119 123 125 -127
		f 4 126 -129 127 121
		f 4 282 -338 -284 -135
		f 4 -132 -131 124 -130
		f 4 -126 130 132 -134
		f 4 133 -136 134 128
		f 4 281 -340 -283 -142
		f 4 -139 -138 131 -137
		f 4 -133 137 139 -141
		f 4 140 -143 141 135
		f 4 280 -342 -282 -149
		f 4 -146 -145 138 -144
		f 4 -140 144 146 -148
		f 4 147 -150 148 142
		f 4 279 -344 -281 -156
		f 4 -153 -152 145 -151
		f 4 -147 151 153 -155
		f 4 154 -157 155 149
		f 4 278 -346 -280 -163
		f 4 -160 -159 152 -158
		f 4 -154 158 160 -162
		f 4 161 -164 162 156
		f 4 277 -348 -279 -170
		f 4 -167 -166 159 -165
		f 4 -161 165 167 -169
		f 4 168 -171 169 163
		f 4 276 -350 -278 -177
		f 4 -174 -173 166 -172
		f 4 -168 172 174 -176
		f 4 175 -178 176 170
		f 4 275 -352 -277 -184
		f 4 -181 -180 173 -179
		f 4 -175 179 181 -183
		f 4 182 -185 183 177
		f 4 274 -354 -276 -191
		f 4 -188 -187 180 -186
		f 4 -182 186 188 -190
		f 4 189 -192 190 184
		f 4 273 -356 -275 -198
		f 4 -195 -194 187 -193
		f 4 -189 193 195 -197
		f 4 196 -199 197 191
		f 4 272 -358 -274 -205
		f 4 -202 -201 194 -200
		f 4 -196 200 202 -204
		f 4 203 -206 204 198
		f 4 271 -360 -273 -210
		f 4 -1 -208 201 -207
		f 4 -203 207 6 -209
		f 4 208 -9 209 205
		f 4 211 -213 -211 1
		f 4 210 -215 -214 10
		f 4 213 -217 -216 17
		f 4 215 -219 -218 24
		f 4 217 -221 -220 31
		f 4 219 -223 -222 38
		f 4 221 -225 -224 45
		f 4 223 -227 -226 52
		f 4 225 -229 -228 59
		f 4 227 -231 -230 66
		f 4 229 -233 -232 73
		f 4 231 -235 -234 80
		f 4 233 -237 -236 87
		f 4 235 -239 -238 94
		f 4 237 -241 -240 101
		f 4 239 -243 -242 108
		f 4 241 -245 -244 115
		f 4 243 -247 -246 122
		f 4 245 -249 -248 129
		f 4 247 -251 -250 136
		f 4 249 -253 -252 143
		f 4 251 -255 -254 150
		f 4 253 -257 -256 157
		f 4 255 -259 -258 164
		f 4 257 -261 -260 171
		f 4 259 -263 -262 178
		f 4 261 -265 -264 185
		f 4 263 -267 -266 192
		f 4 265 -269 -268 199
		f 4 267 -270 -212 206
		f 4 300 -362 303 -361
		f 4 301 -363 304 360
		f 4 302 -364 359 361
		f 4 305 -365 306 362
		f 4 307 -366 308 364
		f 4 309 -367 310 365
		f 4 311 -368 312 366
		f 4 313 -369 314 367
		f 4 315 -370 316 368
		f 4 317 -371 318 369
		f 4 319 -372 320 370
		f 4 321 -373 322 371
		f 4 323 -374 324 372
		f 4 325 -375 326 373
		f 4 327 -376 328 374
		f 4 329 -377 330 375
		f 4 331 -378 332 376
		f 4 333 -379 334 377
		f 4 335 -380 336 378
		f 4 337 -381 338 379
		f 4 339 -382 340 380
		f 4 341 -383 342 381
		f 4 343 -384 344 382
		f 4 345 -385 346 383
		f 4 347 -386 348 384
		f 4 349 -387 350 385
		f 4 351 -388 352 386
		f 4 353 -389 354 387
		f 4 355 -390 356 388
		f 4 357 363 358 389
		f 4 660 -694 -662 -398
		f 4 -394 -393 390 -392
		f 4 -397 392 394 -396
		f 4 395 -400 397 398
		f 4 689 -692 -661 -406
		f 4 -403 -402 393 -401
		f 4 -395 401 403 -405
		f 4 404 -407 405 399
		f 4 688 -696 -690 -413
		f 4 -410 -409 402 -408
		f 4 -404 408 410 -412
		f 4 411 -414 412 406
		f 4 687 -698 -689 -420
		f 4 -417 -416 409 -415
		f 4 -411 415 417 -419
		f 4 418 -421 419 413
		f 4 686 -700 -688 -427
		f 4 -424 -423 416 -422
		f 4 -418 422 424 -426
		f 4 425 -428 426 420
		f 4 685 -702 -687 -434
		f 4 -431 -430 423 -429
		f 4 -425 429 431 -433
		f 4 432 -435 433 427
		f 4 684 -704 -686 -441
		f 4 -438 -437 430 -436
		f 4 -432 436 438 -440
		f 4 439 -442 440 434
		f 4 683 -706 -685 -448
		f 4 -445 -444 437 -443
		f 4 -439 443 445 -447
		f 4 446 -449 447 441
		f 4 682 -708 -684 -455
		f 4 -452 -451 444 -450
		f 4 -446 450 452 -454
		f 4 453 -456 454 448
		f 4 681 -710 -683 -462
		f 4 -459 -458 451 -457
		f 4 -453 457 459 -461
		f 4 460 -463 461 455
		f 4 680 -712 -682 -469
		f 4 -466 -465 458 -464
		f 4 -460 464 466 -468
		f 4 467 -470 468 462
		f 4 679 -714 -681 -476
		f 4 -473 -472 465 -471
		f 4 -467 471 473 -475
		f 4 474 -477 475 469
		f 4 678 -716 -680 -483
		f 4 -480 -479 472 -478
		f 4 -474 478 480 -482
		f 4 481 -484 482 476
		f 4 677 -718 -679 -490
		f 4 -487 -486 479 -485
		f 4 -481 485 487 -489
		f 4 488 -491 489 483
		f 4 676 -720 -678 -497
		f 4 -494 -493 486 -492
		f 4 -488 492 494 -496
		f 4 495 -498 496 490
		f 4 675 -722 -677 -504
		f 4 -501 -500 493 -499
		f 4 -495 499 501 -503
		f 4 502 -505 503 497
		f 4 674 -724 -676 -511
		f 4 -508 -507 500 -506
		f 4 -502 506 508 -510
		f 4 509 -512 510 504
		f 4 673 -726 -675 -518
		f 4 -515 -514 507 -513
		f 4 -509 513 515 -517
		f 4 516 -519 517 511
		f 4 672 -728 -674 -525
		f 4 -522 -521 514 -520
		f 4 -516 520 522 -524
		f 4 523 -526 524 518
		f 4 671 -730 -673 -532
		f 4 -529 -528 521 -527
		f 4 -523 527 529 -531
		f 4 530 -533 531 525
		f 4 670 -732 -672 -539
		f 4 -536 -535 528 -534
		f 4 -530 534 536 -538
		f 4 537 -540 538 532
		f 4 669 -734 -671 -546
		f 4 -543 -542 535 -541
		f 4 -537 541 543 -545
		f 4 544 -547 545 539
		f 4 668 -736 -670 -553
		f 4 -550 -549 542 -548
		f 4 -544 548 550 -552
		f 4 551 -554 552 546
		f 4 667 -738 -669 -560
		f 4 -557 -556 549 -555
		f 4 -551 555 557 -559
		f 4 558 -561 559 553
		f 4 666 -740 -668 -567
		f 4 -564 -563 556 -562
		f 4 -558 562 564 -566
		f 4 565 -568 566 560
		f 4 665 -742 -667 -574
		f 4 -571 -570 563 -569
		f 4 -565 569 571 -573
		f 4 572 -575 573 567
		f 4 664 -744 -666 -581
		f 4 -578 -577 570 -576
		f 4 -572 576 578 -580
		f 4 579 -582 580 574
		f 4 663 -746 -665 -588
		f 4 -585 -584 577 -583
		f 4 -579 583 585 -587
		f 4 586 -589 587 581
		f 4 662 -748 -664 -595
		f 4 -592 -591 584 -590
		f 4 -586 590 592 -594
		f 4 593 -596 594 588
		f 4 661 -750 -663 -600
		f 4 -391 -598 591 -597
		f 4 -593 597 396 -599
		f 4 598 -399 599 595
		f 4 601 -603 -601 391
		f 4 600 -605 -604 400
		f 4 603 -607 -606 407
		f 4 605 -609 -608 414
		f 4 607 -611 -610 421
		f 4 609 -613 -612 428
		f 4 611 -615 -614 435
		f 4 613 -617 -616 442
		f 4 615 -619 -618 449
		f 4 617 -621 -620 456
		f 4 619 -623 -622 463
		f 4 621 -625 -624 470
		f 4 623 -627 -626 477
		f 4 625 -629 -628 484
		f 4 627 -631 -630 491
		f 4 629 -633 -632 498
		f 4 631 -635 -634 505
		f 4 633 -637 -636 512
		f 4 635 -639 -638 519
		f 4 637 -641 -640 526
		f 4 639 -643 -642 533
		f 4 641 -645 -644 540
		f 4 643 -647 -646 547
		f 4 645 -649 -648 554
		f 4 647 -651 -650 561
		f 4 649 -653 -652 568
		f 4 651 -655 -654 575
		f 4 653 -657 -656 582
		f 4 655 -659 -658 589
		f 4 657 -660 -602 596
		f 4 690 -752 693 -751
		f 4 691 -753 694 750
		f 4 692 -754 749 751
		f 4 695 -755 696 752
		f 4 697 -756 698 754
		f 4 699 -757 700 755
		f 4 701 -758 702 756
		f 4 703 -759 704 757
		f 4 705 -760 706 758
		f 4 707 -761 708 759
		f 4 709 -762 710 760
		f 4 711 -763 712 761
		f 4 713 -764 714 762
		f 4 715 -765 716 763
		f 4 717 -766 718 764
		f 4 719 -767 720 765
		f 4 721 -768 722 766
		f 4 723 -769 724 767
		f 4 725 -770 726 768
		f 4 727 -771 728 769
		f 4 729 -772 730 770
		f 4 731 -773 732 771
		f 4 733 -774 734 772
		f 4 735 -775 736 773
		f 4 737 -776 738 774
		f 4 739 -777 740 775
		f 4 741 -778 742 776
		f 4 743 -779 744 777
		f 4 745 -780 746 778
		f 4 747 753 748 779
		f 4 787 1051 1083 -1051
		f 4 781 -781 782 783
		f 4 785 -785 -783 786
		f 4 -789 -788 789 -786
		f 4 795 1050 1081 -1080
		f 4 790 -784 791 792
		f 4 794 -794 -792 784
		f 4 -790 -796 796 -795
		f 4 802 1079 1085 -1079
		f 4 797 -793 798 799
		f 4 801 -801 -799 793
		f 4 -797 -803 803 -802
		f 4 809 1078 1087 -1078
		f 4 804 -800 805 806
		f 4 808 -808 -806 800
		f 4 -804 -810 810 -809
		f 4 816 1077 1089 -1077
		f 4 811 -807 812 813
		f 4 815 -815 -813 807
		f 4 -811 -817 817 -816
		f 4 823 1076 1091 -1076
		f 4 818 -814 819 820
		f 4 822 -822 -820 814
		f 4 -818 -824 824 -823
		f 4 830 1075 1093 -1075
		f 4 825 -821 826 827
		f 4 829 -829 -827 821
		f 4 -825 -831 831 -830
		f 4 837 1074 1095 -1074
		f 4 832 -828 833 834
		f 4 836 -836 -834 828
		f 4 -832 -838 838 -837
		f 4 844 1073 1097 -1073
		f 4 839 -835 840 841
		f 4 843 -843 -841 835
		f 4 -839 -845 845 -844
		f 4 851 1072 1099 -1072
		f 4 846 -842 847 848
		f 4 850 -850 -848 842
		f 4 -846 -852 852 -851
		f 4 858 1071 1101 -1071
		f 4 853 -849 854 855
		f 4 857 -857 -855 849
		f 4 -853 -859 859 -858
		f 4 865 1070 1103 -1070
		f 4 860 -856 861 862
		f 4 864 -864 -862 856
		f 4 -860 -866 866 -865
		f 4 872 1069 1105 -1069
		f 4 867 -863 868 869
		f 4 871 -871 -869 863
		f 4 -867 -873 873 -872
		f 4 879 1068 1107 -1068
		f 4 874 -870 875 876
		f 4 878 -878 -876 870
		f 4 -874 -880 880 -879
		f 4 886 1067 1109 -1067
		f 4 881 -877 882 883
		f 4 885 -885 -883 877
		f 4 -881 -887 887 -886
		f 4 893 1066 1111 -1066
		f 4 888 -884 889 890
		f 4 892 -892 -890 884
		f 4 -888 -894 894 -893
		f 4 900 1065 1113 -1065
		f 4 895 -891 896 897
		f 4 899 -899 -897 891
		f 4 -895 -901 901 -900
		f 4 907 1064 1115 -1064
		f 4 902 -898 903 904
		f 4 906 -906 -904 898
		f 4 -902 -908 908 -907
		f 4 914 1063 1117 -1063
		f 4 909 -905 910 911
		f 4 913 -913 -911 905
		f 4 -909 -915 915 -914
		f 4 921 1062 1119 -1062
		f 4 916 -912 917 918
		f 4 920 -920 -918 912
		f 4 -916 -922 922 -921
		f 4 928 1061 1121 -1061
		f 4 923 -919 924 925
		f 4 927 -927 -925 919
		f 4 -923 -929 929 -928
		f 4 935 1060 1123 -1060
		f 4 930 -926 931 932
		f 4 934 -934 -932 926
		f 4 -930 -936 936 -935
		f 4 942 1059 1125 -1059
		f 4 937 -933 938 939
		f 4 941 -941 -939 933
		f 4 -937 -943 943 -942
		f 4 949 1058 1127 -1058
		f 4 944 -940 945 946
		f 4 948 -948 -946 940
		f 4 -944 -950 950 -949
		f 4 956 1057 1129 -1057
		f 4 951 -947 952 953
		f 4 955 -955 -953 947
		f 4 -951 -957 957 -956
		f 4 963 1056 1131 -1056
		f 4 958 -954 959 960
		f 4 962 -962 -960 954
		f 4 -958 -964 964 -963
		f 4 970 1055 1133 -1055
		f 4 965 -961 966 967
		f 4 969 -969 -967 961
		f 4 -965 -971 971 -970
		f 4 977 1054 1135 -1054
		f 4 972 -968 973 974
		f 4 976 -976 -974 968
		f 4 -972 -978 978 -977
		f 4 984 1053 1137 -1053
		f 4 979 -975 980 981
		f 4 983 -983 -981 975
		f 4 -979 -985 985 -984
		f 4 989 1052 1139 -1052
		f 4 986 -982 987 780
		f 4 988 -787 -988 982
		f 4 -986 -990 788 -989
		f 4 -782 990 992 -992
		f 4 -791 993 994 -991
		f 4 -798 995 996 -994
		f 4 -805 997 998 -996
		f 4 -812 999 1000 -998
		f 4 -819 1001 1002 -1000
		f 4 -826 1003 1004 -1002
		f 4 -833 1005 1006 -1004
		f 4 -840 1007 1008 -1006
		f 4 -847 1009 1010 -1008
		f 4 -854 1011 1012 -1010
		f 4 -861 1013 1014 -1012
		f 4 -868 1015 1016 -1014
		f 4 -875 1017 1018 -1016
		f 4 -882 1019 1020 -1018
		f 4 -889 1021 1022 -1020
		f 4 -896 1023 1024 -1022
		f 4 -903 1025 1026 -1024
		f 4 -910 1027 1028 -1026
		f 4 -917 1029 1030 -1028;
	setAttr ".fc[500:719]"
		f 4 -924 1031 1032 -1030
		f 4 -931 1033 1034 -1032
		f 4 -938 1035 1036 -1034
		f 4 -945 1037 1038 -1036
		f 4 -952 1039 1040 -1038
		f 4 -959 1041 1042 -1040
		f 4 -966 1043 1044 -1042
		f 4 -973 1045 1046 -1044
		f 4 -980 1047 1048 -1046
		f 4 -987 991 1049 -1048
		f 4 1140 -1084 1141 -1081
		f 4 -1141 -1085 1142 -1082
		f 4 -1142 -1140 1143 -1083
		f 4 -1143 -1087 1144 -1086
		f 4 -1145 -1089 1145 -1088
		f 4 -1146 -1091 1146 -1090
		f 4 -1147 -1093 1147 -1092
		f 4 -1148 -1095 1148 -1094
		f 4 -1149 -1097 1149 -1096
		f 4 -1150 -1099 1150 -1098
		f 4 -1151 -1101 1151 -1100
		f 4 -1152 -1103 1152 -1102
		f 4 -1153 -1105 1153 -1104
		f 4 -1154 -1107 1154 -1106
		f 4 -1155 -1109 1155 -1108
		f 4 -1156 -1111 1156 -1110
		f 4 -1157 -1113 1157 -1112
		f 4 -1158 -1115 1158 -1114
		f 4 -1159 -1117 1159 -1116
		f 4 -1160 -1119 1160 -1118
		f 4 -1161 -1121 1161 -1120
		f 4 -1162 -1123 1162 -1122
		f 4 -1163 -1125 1163 -1124
		f 4 -1164 -1127 1164 -1126
		f 4 -1165 -1129 1165 -1128
		f 4 -1166 -1131 1166 -1130
		f 4 -1167 -1133 1167 -1132
		f 4 -1168 -1135 1168 -1134
		f 4 -1169 -1137 1169 -1136
		f 4 -1170 -1139 -1144 -1138
		f 4 1177 1441 1473 -1441
		f 4 1171 -1171 1172 1173
		f 4 1175 -1175 -1173 1176
		f 4 -1179 -1178 1179 -1176
		f 4 1185 1440 1471 -1470
		f 4 1180 -1174 1181 1182
		f 4 1184 -1184 -1182 1174
		f 4 -1180 -1186 1186 -1185
		f 4 1192 1469 1475 -1469
		f 4 1187 -1183 1188 1189
		f 4 1191 -1191 -1189 1183
		f 4 -1187 -1193 1193 -1192
		f 4 1199 1468 1477 -1468
		f 4 1194 -1190 1195 1196
		f 4 1198 -1198 -1196 1190
		f 4 -1194 -1200 1200 -1199
		f 4 1206 1467 1479 -1467
		f 4 1201 -1197 1202 1203
		f 4 1205 -1205 -1203 1197
		f 4 -1201 -1207 1207 -1206
		f 4 1213 1466 1481 -1466
		f 4 1208 -1204 1209 1210
		f 4 1212 -1212 -1210 1204
		f 4 -1208 -1214 1214 -1213
		f 4 1220 1465 1483 -1465
		f 4 1215 -1211 1216 1217
		f 4 1219 -1219 -1217 1211
		f 4 -1215 -1221 1221 -1220
		f 4 1227 1464 1485 -1464
		f 4 1222 -1218 1223 1224
		f 4 1226 -1226 -1224 1218
		f 4 -1222 -1228 1228 -1227
		f 4 1234 1463 1487 -1463
		f 4 1229 -1225 1230 1231
		f 4 1233 -1233 -1231 1225
		f 4 -1229 -1235 1235 -1234
		f 4 1241 1462 1489 -1462
		f 4 1236 -1232 1237 1238
		f 4 1240 -1240 -1238 1232
		f 4 -1236 -1242 1242 -1241
		f 4 1248 1461 1491 -1461
		f 4 1243 -1239 1244 1245
		f 4 1247 -1247 -1245 1239
		f 4 -1243 -1249 1249 -1248
		f 4 1255 1460 1493 -1460
		f 4 1250 -1246 1251 1252
		f 4 1254 -1254 -1252 1246
		f 4 -1250 -1256 1256 -1255
		f 4 1262 1459 1495 -1459
		f 4 1257 -1253 1258 1259
		f 4 1261 -1261 -1259 1253
		f 4 -1257 -1263 1263 -1262
		f 4 1269 1458 1497 -1458
		f 4 1264 -1260 1265 1266
		f 4 1268 -1268 -1266 1260
		f 4 -1264 -1270 1270 -1269
		f 4 1276 1457 1499 -1457
		f 4 1271 -1267 1272 1273
		f 4 1275 -1275 -1273 1267
		f 4 -1271 -1277 1277 -1276
		f 4 1283 1456 1501 -1456
		f 4 1278 -1274 1279 1280
		f 4 1282 -1282 -1280 1274
		f 4 -1278 -1284 1284 -1283
		f 4 1290 1455 1503 -1455
		f 4 1285 -1281 1286 1287
		f 4 1289 -1289 -1287 1281
		f 4 -1285 -1291 1291 -1290
		f 4 1297 1454 1505 -1454
		f 4 1292 -1288 1293 1294
		f 4 1296 -1296 -1294 1288
		f 4 -1292 -1298 1298 -1297
		f 4 1304 1453 1507 -1453
		f 4 1299 -1295 1300 1301
		f 4 1303 -1303 -1301 1295
		f 4 -1299 -1305 1305 -1304
		f 4 1311 1452 1509 -1452
		f 4 1306 -1302 1307 1308
		f 4 1310 -1310 -1308 1302
		f 4 -1306 -1312 1312 -1311
		f 4 1318 1451 1511 -1451
		f 4 1313 -1309 1314 1315
		f 4 1317 -1317 -1315 1309
		f 4 -1313 -1319 1319 -1318
		f 4 1325 1450 1513 -1450
		f 4 1320 -1316 1321 1322
		f 4 1324 -1324 -1322 1316
		f 4 -1320 -1326 1326 -1325
		f 4 1332 1449 1515 -1449
		f 4 1327 -1323 1328 1329
		f 4 1331 -1331 -1329 1323
		f 4 -1327 -1333 1333 -1332
		f 4 1339 1448 1517 -1448
		f 4 1334 -1330 1335 1336
		f 4 1338 -1338 -1336 1330
		f 4 -1334 -1340 1340 -1339
		f 4 1346 1447 1519 -1447
		f 4 1341 -1337 1342 1343
		f 4 1345 -1345 -1343 1337
		f 4 -1341 -1347 1347 -1346
		f 4 1353 1446 1521 -1446
		f 4 1348 -1344 1349 1350
		f 4 1352 -1352 -1350 1344
		f 4 -1348 -1354 1354 -1353
		f 4 1360 1445 1523 -1445
		f 4 1355 -1351 1356 1357
		f 4 1359 -1359 -1357 1351
		f 4 -1355 -1361 1361 -1360
		f 4 1367 1444 1525 -1444
		f 4 1362 -1358 1363 1364
		f 4 1366 -1366 -1364 1358
		f 4 -1362 -1368 1368 -1367
		f 4 1374 1443 1527 -1443
		f 4 1369 -1365 1370 1371
		f 4 1373 -1373 -1371 1365
		f 4 -1369 -1375 1375 -1374
		f 4 1379 1442 1529 -1442
		f 4 1376 -1372 1377 1170
		f 4 1378 -1177 -1378 1372
		f 4 -1376 -1380 1178 -1379
		f 4 -1172 1380 1382 -1382
		f 4 -1181 1383 1384 -1381
		f 4 -1188 1385 1386 -1384
		f 4 -1195 1387 1388 -1386
		f 4 -1202 1389 1390 -1388
		f 4 -1209 1391 1392 -1390
		f 4 -1216 1393 1394 -1392
		f 4 -1223 1395 1396 -1394
		f 4 -1230 1397 1398 -1396
		f 4 -1237 1399 1400 -1398
		f 4 -1244 1401 1402 -1400
		f 4 -1251 1403 1404 -1402
		f 4 -1258 1405 1406 -1404
		f 4 -1265 1407 1408 -1406
		f 4 -1272 1409 1410 -1408
		f 4 -1279 1411 1412 -1410
		f 4 -1286 1413 1414 -1412
		f 4 -1293 1415 1416 -1414
		f 4 -1300 1417 1418 -1416
		f 4 -1307 1419 1420 -1418
		f 4 -1314 1421 1422 -1420
		f 4 -1321 1423 1424 -1422
		f 4 -1328 1425 1426 -1424
		f 4 -1335 1427 1428 -1426
		f 4 -1342 1429 1430 -1428
		f 4 -1349 1431 1432 -1430
		f 4 -1356 1433 1434 -1432
		f 4 -1363 1435 1436 -1434
		f 4 -1370 1437 1438 -1436
		f 4 -1377 1381 1439 -1438
		f 4 1530 -1474 1531 -1471
		f 4 -1531 -1475 1532 -1472
		f 4 -1532 -1530 1533 -1473
		f 4 -1533 -1477 1534 -1476
		f 4 -1535 -1479 1535 -1478
		f 4 -1536 -1481 1536 -1480
		f 4 -1537 -1483 1537 -1482
		f 4 -1538 -1485 1538 -1484
		f 4 -1539 -1487 1539 -1486
		f 4 -1540 -1489 1540 -1488
		f 4 -1541 -1491 1541 -1490
		f 4 -1542 -1493 1542 -1492
		f 4 -1543 -1495 1543 -1494
		f 4 -1544 -1497 1544 -1496
		f 4 -1545 -1499 1545 -1498
		f 4 -1546 -1501 1546 -1500
		f 4 -1547 -1503 1547 -1502
		f 4 -1548 -1505 1548 -1504
		f 4 -1549 -1507 1549 -1506
		f 4 -1550 -1509 1550 -1508
		f 4 -1551 -1511 1551 -1510
		f 4 -1552 -1513 1552 -1512
		f 4 -1553 -1515 1553 -1514
		f 4 -1554 -1517 1554 -1516
		f 4 -1555 -1519 1555 -1518
		f 4 -1556 -1521 1556 -1520
		f 4 -1557 -1523 1557 -1522
		f 4 -1558 -1525 1558 -1524
		f 4 -1559 -1527 1559 -1526
		f 4 -1560 -1529 -1534 -1528;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".vcs" 2;
createNode transform -n "MSH_wheels_tutorial" -p "GRP_utility_meshes";
	rename -uid "6DD322B8-4E2F-4AC3-F8C7-DDBFA9554E03";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode mesh -n "MSH_wheels_tutorialShape" -p "MSH_wheels_tutorial";
	rename -uid "9572B083-49EA-358D-2F88-CB9929FBCB6D";
	setAttr -k off ".v";
	setAttr -s 10 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".sdt" 0;
	setAttr ".ugsdt" no;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
createNode mesh -n "MSH_wheels_tutorialShapeOrig" -p "MSH_wheels_tutorial";
	rename -uid "75941B1B-4A71-026F-DC97-509E9D86CC91";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".sdt" 0;
	setAttr -s 1368 ".vt";
	setAttr ".vt[0:165]"  -77.0064620972 55.42156982 -129.13973999 -77.0064620972 50.84530258 -121.92578888
		 -77.0064620972 43.85671234 -117.012214661 -77.0064620972 35.51977539 -115.14710236
		 -77.0064620972 27.10368729 -116.61440277 -77.0064620972 19.88973999 -121.19072723
		 -77.0064620972 14.97619247 -128.17926025 -77.0064620972 13.11108208 -136.51617432
		 -77.0064620972 14.57834816 -144.93229675 -77.0064620972 19.15463448 -152.14620972
		 -77.0064620972 26.14322281 -157.059783936 -77.0064620972 34.48016739 -158.92488098
		 -77.0064620972 42.89624405 -157.45761108 -77.0064620972 50.11019135 -152.88134766
		 -77.0064620972 55.023742676 -145.89273071 -77.0064620972 56.88884735 -137.55578613
		 -81.83740234 55.42156982 -129.13973999 -81.83740234 50.84530258 -121.92578888 -81.83740234 43.85671234 -117.012214661
		 -81.83740234 35.51977539 -115.14710236 -81.83740234 27.10368729 -116.61440277 -81.83740234 19.88973999 -121.19072723
		 -81.83740234 14.97619247 -128.17926025 -81.83740234 13.11108208 -136.51617432 -81.83740234 14.57834816 -144.93229675
		 -81.83740234 19.15463448 -152.14620972 -81.83740234 26.14322281 -157.059783936 -81.83740234 34.48016739 -158.92488098
		 -81.83740234 42.89624405 -157.45761108 -81.83740234 50.11019135 -152.88134766 -81.83740234 55.023742676 -145.89273071
		 -81.83740234 56.88884735 -137.55578613 -82.29216003 46.3356781 -132.65292358 -82.29216003 43.79545593 -128.64857483
		 -82.29216003 39.91619873 -125.92111969 -82.29216003 35.28850174 -124.88584137 -82.29216003 30.61686325 -125.70029449
		 -82.29216003 26.61252594 -128.24052429 -82.29216003 23.88508224 -132.11975098 -82.29216003 22.8497963 -136.74746704
		 -82.29216003 23.66425323 -141.4190979 -82.29216003 26.2044754 -145.4234314 -82.29216003 30.083730698 -148.15087891
		 -82.29216003 34.71143341 -149.18617249 -82.29216003 39.38306427 -148.37171936 -82.29216003 43.38740921 -145.83148193
		 -82.29216003 46.11484528 -141.95223999 -82.29216003 47.15013504 -137.32452393 -87.89835358 45.20834351 -133.088806152
		 -87.89835358 42.92074203 -129.48268127 -87.89835358 39.42728424 -127.026481628 -87.89835358 35.25980759 -126.094169617
		 -87.89835358 31.052768707 -126.82761383 -87.89835358 27.44664955 -129.11523438 -87.89835358 24.99045181 -132.60868835
		 -87.89835358 24.058128357 -136.77618408 -87.89835358 24.79158401 -140.98320007 -87.89835358 27.079185486 -144.58932495
		 -87.89835358 30.57265091 -147.045501709 -87.89835358 34.74013138 -147.97784424 -87.89835358 38.94716263 -147.24438477
		 -87.89835358 42.55328369 -144.95678711 -87.89835358 45.009475708 -141.46331787 -87.89835358 45.94179535 -137.2958374
		 -65.50907898 45.98304367 -132.78924561 -65.50907898 43.52185822 -128.90945435 -65.50907898 39.76327515 -126.26688385
		 -65.50907898 35.27952576 -125.26377106 -65.50907898 30.7532196 -126.05292511 -65.50907898 26.87342453 -128.5141449
		 -65.50907898 24.23085022 -132.27270508 -65.50907898 23.22774506 -136.75642395 -65.50907898 24.016876221 -141.28276062
		 -65.50907898 26.47807693 -145.16253662 -65.50907898 30.23666191 -147.80511475 -65.50907898 34.72040939 -148.80821228
		 -65.50907898 39.24671173 -148.019073486 -65.50907898 43.12650299 -145.55789185 -65.50907898 45.76908875 -141.79930115
		 -65.50907898 46.77218246 -137.31555176 -89.17943573 31.28306198 -137.93795776 -89.17943573 32.231987 -139.67556763
		 -89.17943573 33.92258835 -140.70593262 -89.17943573 35.90186691 -140.75292969 -89.17943573 37.63947678 -139.80401611
		 -89.17943573 38.66983032 -138.11340332 -89.17943573 38.71684265 -136.13412476 -89.17943573 37.76791382 -134.39651489
		 -89.17943573 36.077312469 -133.3661499 -89.17943573 34.09803772 -133.31915283 -89.17943573 32.36042404 -134.26806641
		 -89.17943573 31.3300705 -135.9586792 -91.35694122 31.28306198 -137.93795776 -91.35694122 32.231987 -139.67556763
		 -91.35694122 33.92258835 -140.70593262 -91.35694122 35.90186691 -140.75292969 -91.35694122 37.63947678 -139.80401611
		 -91.35694122 38.66983032 -138.11340332 -91.35694122 38.71684265 -136.13412476 -91.35694122 37.76791382 -134.39651489
		 -91.35694122 36.077312469 -133.3661499 -91.35694122 34.09803772 -133.31915283 -91.35694122 32.36042404 -134.26806641
		 -91.35694122 31.3300705 -135.9586792 -91.82691956 31.91899109 -136.13156128 -91.82691956 31.87952614 -137.79321289
		 -91.82691193 32.67617798 -139.25198364 -91.82691193 32.7840004 -134.71226501 -91.82691193 35.7571373 -140.15646362
		 -91.82691193 35.90442276 -133.95507813 -91.82691193 37.21590805 -139.3598175 -91.82691193 37.32373047 -134.82008362
		 -91.82691193 34.095478058 -140.11700439 -91.82691193 34.24277115 -133.9156189 -91.82691193 38.080913544 -137.94052124
		 -91.82691193 38.1203804 -136.27886963 -89.31186676 40.94264603 -138.5531311 -89.31186676 42.1936264 -137.16377258
		 -89.31186676 41.25885391 -135.54470825 -89.31186676 39.43015671 -135.93339539 -89.31186676 39.2347374 -137.79272461
		 -90.90219879 40.94264603 -138.5531311 -90.90219879 42.1936264 -137.16377258 -90.90219879 41.25885391 -135.54470825
		 -90.90219879 39.43015671 -135.93339539 -90.90219879 39.2347374 -137.79272461 -91.3911972 40.84442139 -138.13320923
		 -91.3911972 41.75399399 -137.12301636 -91.3911972 41.074325562 -135.94580078 -91.3911972 39.74470139 -136.22842407
		 -91.3911972 39.60261917 -137.58029175 -89.31186676 38.2791748 -131.85299683 -89.31186676 37.34439087 -130.23391724
		 -89.31186676 35.51569366 -130.62261963 -89.31186676 35.32027435 -132.48191833 -89.31186676 37.028205872 -133.24234009
		 -90.90219879 38.2791748 -131.85299683 -90.90219879 37.34439087 -130.23391724 -90.90219879 35.51569366 -130.62261963
		 -90.90219879 35.32027435 -132.48191833 -90.90219879 37.028205872 -133.24234009 -91.3911972 37.84944153 -131.81665039
		 -91.3911972 37.16976929 -130.63943481 -91.3911972 35.84014893 -130.92205811 -91.3911972 35.69805908 -132.27394104
		 -91.3911972 36.93986511 -132.826828 -89.31186676 31.083930969 -132.31565857 -89.31186676 29.25521851 -132.70437622
		 -89.31186676 29.059799194 -134.56365967 -89.31186676 30.76771545 -135.32408142 -89.31186676 32.018692017 -133.9347229
		 -90.90219879 31.083930969 -132.31565857 -90.90219879 29.25521851 -132.70437622 -90.90219879 29.059799194 -134.56365967
		 -90.90219879 30.76771545 -135.32408142 -90.90219879 32.018692017 -133.9347229 -91.3911972 30.91656494 -132.71311951
		 -91.3911972 29.58693695 -132.9957428 -91.3911972 29.44485474 -134.34762573 -91.3911972 30.68666077 -134.9005127
		 -91.3911972 31.59622192 -133.89035034 -89.31186676 29.30048561 -139.30172729 -89.31186676 29.10506248 -141.16104126
		 -89.31186676 30.81297874 -141.92144775 -89.31186676 32.063957214 -140.53211975 -89.31186676 31.12917137 -138.9130249;
	setAttr ".vt[166:331]" -90.90219879 29.30048561 -139.30172729 -90.90219879 29.10506248 -141.16104126
		 -90.90219879 30.81297874 -141.92144775 -90.90219879 32.063957214 -140.53211975 -90.90219879 31.12917137 -138.9130249
		 -91.3911972 29.62677574 -139.58372498 -91.3911972 29.48468971 -140.93560791 -91.3911972 30.72649956 -141.48849487
		 -91.3911972 31.63606453 -140.47831726 -91.3911972 30.95640755 -139.30111694 -89.31186676 35.39350891 -143.1566925
		 -89.31186676 37.10144043 -143.91711426 -89.31186676 38.35241699 -142.527771 -89.31186676 37.41763306 -140.90869141
		 -89.31186676 35.58893585 -141.2973938 -90.90219879 35.39350891 -143.1566925 -90.90219879 37.10144043 -143.91711426
		 -90.90219879 38.35241699 -142.527771 -90.90219879 37.41763306 -140.90869141 -90.90219879 35.58893585 -141.2973938
		 -91.3911972 35.76252747 -142.9335022 -91.3911972 37.0043487549 -143.48638916 -91.3911972 37.91390991 -142.47621155
		 -91.3911972 37.23425293 -141.29901123 -91.3911972 35.90463257 -141.58163452 -89.48600769 35.066207886 -127.89911652
		 -89.48600769 40.32343292 -129.60638428 -88.092002869 34.32608032 -127.09299469 -86.81005859 38.90477753 -109.40110016
		 -87.68508911 40.39599609 -109.12595367 -87.68508911 47.050140381 -111.28800201 -88.092002869 41.39242554 -129.38897705
		 -86.81005859 48.087234497 -112.38465118 -89.48600769 43.76760101 -134.23953247 -89.48600769 43.76845169 -139.76702881
		 -88.092002869 44.30554581 -133.28652954 -86.81005859 62.5464325 -132.17404175 -87.68508911 63.26893234 -133.50724792
		 -87.68508911 63.26893234 -140.50382996 -88.092002869 44.30554581 -140.71653748 -86.81005859 62.5464325 -141.82904053
		 -89.48600769 40.42636871 -144.47436523 -89.48600769 35.16966248 -146.18325806 -88.092002869 41.49895477 -144.69148254
		 -86.81005859 48.19374847 -161.69581604 -87.68508911 47.14905548 -162.79493713 -87.68508911 40.49491119 -164.95700073
		 -88.092002869 34.43260956 -146.98748779 -86.81005859 39.011299133 -164.67938232 -89.48600769 29.65996552 -144.4593811
		 -89.48600769 26.41030121 -139.98803711 -88.092002869 29.78492355 -145.54656982 -86.81005859 15.68165302 -157.16833496
		 -87.68508911 14.31349277 -156.51441956 -87.68508911 10.20100498 -150.85406494 -88.092002869 25.41767502 -139.5355835
		 -86.81005859 10.0065813065 -149.35726929 -89.48600769 26.34721375 -134.21531677 -89.48600769 29.59550476 -129.74298096
		 -88.092002869 25.35183716 -134.67010498 -86.81005859 9.94073486 -124.84842682 -87.68508911 10.13986206 -123.34516144
		 -87.68508911 14.25234985 -117.68480682 -88.092002869 29.71908569 -128.65911865 -86.81005859 15.61580658 -117.037361145
		 -88.37075043 17.50684738 -128.48925781 -89.62237549 17.96696472 -127.72682953 -89.62237549 20.83357239 -123.78127289
		 -88.37075043 21.41650009 -123.10807037 -88.37075043 37.78019714 -117.72197723 -89.62237549 38.64748764 -117.92397308
		 -89.62237549 43.28575897 -119.4310379 -88.37075043 44.10614777 -119.77741241 -88.37075043 54.28529739 -133.67578125
		 -89.62237549 54.36119843 -134.56304932 -89.62237549 54.36119843 -139.4400177 -88.37075043 54.28529739 -140.32728577
		 -88.37075043 44.21266937 -154.30307007 -89.62237549 43.39228821 -154.64944458 -89.62237549 38.75402069 -156.1565094
		 -88.37075043 37.88672638 -156.35850525 -88.37075043 21.48233795 -151.097610474 -89.62237549 20.89941025 -150.42440796
		 -89.62237549 18.032802582 -146.47886658 -88.37075043 17.57268524 -145.71643066 -86.80031586 61.84566498 -131.51553345
		 -86.80031586 62.40529251 -137.21359253 -86.80031586 60.11358261 -126.05834198 -86.80031586 57.28473282 -121.080543518
		 -86.80031586 53.48275375 -116.79962921 -86.80031586 48.873806 -113.40279388 -86.80031586 43.6593399 -111.038414001
		 -86.80031586 38.067230225 -109.80983734 -86.80031586 32.34188461 -109.7707901 -86.80031586 26.7335434 -110.922966
		 -86.80031586 21.48729897 -113.21598053 -86.80031586 16.83245468 -116.54967499 -86.80031586 12.9724226 -120.77828217
		 -86.80031586 10.075940132 -125.71704865 -86.80031586 8.26957226 -131.15008545 -86.80031586 7.6322732 -136.83999634
		 -86.80031586 8.19189739 -142.53805542 -86.80031586 9.92398453 -147.99523926 -86.80031586 12.75283337 -152.97305298
		 -86.80031586 16.55480957 -157.25395203 -86.80031586 21.16375351 -160.65080261 -86.80031586 26.37822342 -163.015182495
		 -86.80031586 31.9703331 -164.2437439 -86.80031586 37.69567871 -164.2828064 -86.80031586 43.30402374 -163.13064575
		 -86.80031586 48.55026627 -160.83760071 -86.80031586 53.20510864 -157.50392151 -86.80031586 57.065135956 -153.27529907
		 -86.80031586 59.96162033 -148.33653259 -86.80031586 61.76799393 -142.90348816 -86.80031586 59.82168961 -131.93133545
		 -86.80031586 60.33909607 -137.19949341 -86.80031586 58.22028351 -126.88587189 -86.80031586 55.6048584 -122.28362274
		 -86.80031586 52.089725494 -118.32569122 -86.80031586 47.82850266 -115.18512726 -86.80031586 43.0074462891 -112.99912262
		 -86.80031586 37.83723831 -111.8632431 -86.80031586 32.54384613 -111.82714081 -86.80031586 27.35862923 -112.89238739
		 -86.80031586 22.50819397 -115.012413025 -86.80031586 18.20453644 -118.094581604 -86.80031586 14.63572884 -122.00415802
		 -86.80031586 11.95777321 -126.57032013 -86.80031586 10.28768826 -131.59346008 -86.80031586 9.69847107 -136.85409546
		 -86.80031586 10.21587372 -142.12225342 -86.80031586 11.81728268 -147.16772461 -86.80031586 14.43270588 -151.7699585
		 -86.80031586 17.94783974 -155.72790527 -86.80031586 22.20905685 -158.86846924 -86.80031586 27.030117035 -161.054473877
		 -86.80031586 32.20032883 -162.19033813 -86.80031586 37.49371719 -162.22644043 -86.80031586 42.678936 -161.16122437
		 -86.80031586 47.52937317 -159.041168213 -86.80031586 51.83302689 -155.95901489 -86.80031586 55.40183258 -152.049407959
		 -86.80031586 58.079788208 -147.48326111 -86.80031586 59.74987793 -142.46011353 -68.54407501 59.82168961 -131.93133545
		 -68.54407501 60.33909607 -137.19949341 -68.54407501 58.22028351 -126.88587189 -68.54407501 55.6048584 -122.28362274
		 -68.54407501 52.089725494 -118.32569122 -68.54407501 47.82850266 -115.18512726 -68.54407501 43.0074462891 -112.99912262
		 -68.54407501 37.83723831 -111.8632431 -68.54407501 32.54384613 -111.82714081 -68.54407501 27.35862923 -112.89238739
		 -68.54407501 22.50819397 -115.012413025 -68.54407501 18.20453644 -118.094581604 -68.54407501 14.63572884 -122.00415802
		 -68.54407501 11.95777321 -126.57032013 -68.54407501 10.28768826 -131.59346008 -68.54407501 9.69847107 -136.85409546
		 -68.54407501 10.21587372 -142.12225342 -68.54407501 11.81728268 -147.16772461 -68.54407501 14.43270588 -151.7699585
		 -68.54407501 17.94783974 -155.72790527 -68.54407501 22.20905685 -158.86846924;
	setAttr ".vt[332:497]" -68.54407501 27.030117035 -161.054473877 -68.54407501 32.20032883 -162.19033813
		 -68.54407501 37.49371719 -162.22644043 -68.54407501 42.678936 -161.16122437 -68.54407501 47.52937317 -159.041168213
		 -68.54407501 51.83302689 -155.95901489 -68.54407501 55.40183258 -152.049407959 -68.54407501 58.079788208 -147.48326111
		 -68.54407501 59.74987793 -142.46011353 -77.0064620972 55.42160416 129.29908752 -77.0064620972 50.84533691 136.51303101
		 -77.0064620972 43.85674667 141.42660522 -77.0064620972 35.51980972 143.29171753 -77.0064620972 27.10372162 141.82443237
		 -77.0064620972 19.88977432 137.24809265 -77.0064620972 14.97622681 130.259552 -77.0064620972 13.11111641 121.92264557
		 -77.0064620972 14.57838249 113.5065155 -77.0064620972 19.15466881 106.29259491 -77.0064620972 26.14325714 101.37902832
		 -77.0064620972 34.48020172 99.5139389 -77.0064620972 42.89627838 100.9812088 -77.0064620972 50.11022568 105.55747223
		 -77.0064620972 55.023777008 112.54607391 -77.0064620972 56.88888168 120.88301849
		 -81.83740234 55.42160416 129.29908752 -81.83740234 50.84533691 136.51303101 -81.83740234 43.85674667 141.42660522
		 -81.83740234 35.51980972 143.29171753 -81.83740234 27.10372162 141.82443237 -81.83740234 19.88977432 137.24809265
		 -81.83740234 14.97622681 130.259552 -81.83740234 13.11111641 121.92264557 -81.83740234 14.57838249 113.5065155
		 -81.83740234 19.15466881 106.29259491 -81.83740234 26.14325714 101.37902832 -81.83740234 34.48020172 99.5139389
		 -81.83740234 42.89627838 100.9812088 -81.83740234 50.11022568 105.55747223 -81.83740234 55.023777008 112.54607391
		 -81.83740234 56.88888168 120.88301849 -82.29216003 46.33571243 125.78588867 -82.29216003 43.79549026 129.79023743
		 -82.29216003 39.91623306 132.5177002 -82.29216003 35.28853607 133.55297852 -82.29216003 30.61689758 132.73852539
		 -82.29216003 26.61256027 130.19828796 -82.29216003 23.88511658 126.31905365 -82.29216003 22.84983063 121.69133759
		 -82.29216003 23.66428757 117.019706726 -82.29216003 26.20450974 113.01537323 -82.29216003 30.08376503 110.28793335
		 -82.29216003 34.71146774 109.25263977 -82.29216003 39.3830986 110.067092896 -82.29216003 43.38744354 112.60732269
		 -82.29216003 46.11487961 116.48658752 -82.29216003 47.15016937 121.11428833 -87.89835358 45.20837784 125.34999847
		 -87.89835358 42.92077637 128.95613098 -87.89835358 39.42731857 131.41233826 -87.89835358 35.25984192 132.34465027
		 -87.89835358 31.05280304 131.61120605 -87.89835358 27.44668388 129.32357788 -87.89835358 24.99048615 125.8301239
		 -87.89835358 24.058162689 121.66262817 -87.89835358 24.79161835 117.45561981 -87.89835358 27.079219818 113.84949493
		 -87.89835358 30.57268524 111.39330292 -87.89835358 34.74016571 110.46097565 -87.89835358 38.94719696 111.19442749
		 -87.89835358 42.55331802 113.48204041 -87.89835358 45.0095100403 116.97549438 -87.89835358 45.94182968 121.14297485
		 -65.50907898 45.983078 125.64955902 -65.50907898 43.52189255 129.52935791 -65.50907898 39.76330948 132.17193604
		 -65.50907898 35.27956009 133.17504883 -65.50907898 30.75325394 132.38589478 -65.50907898 26.87345886 129.92466736
		 -65.50907898 24.23088455 126.16611481 -65.50907898 23.22777939 121.68239594 -65.50907898 24.016910553 117.15605164
		 -65.50907898 26.47811127 113.27628326 -65.50907898 30.23669624 110.63368988 -65.50907898 34.72044373 109.63059998
		 -65.50907898 39.24674606 110.41973877 -65.50907898 43.12653732 112.88092041 -65.50907898 45.76912308 116.63951111
		 -65.50907898 46.7722168 121.1232605 -89.17943573 31.28309631 120.50085449 -89.17943573 32.23202133 118.763237
		 -89.17943573 33.92262268 117.7328949 -89.17943573 35.90190125 117.68589783 -89.17943573 37.63951111 118.63480377
		 -89.17943573 38.66986465 120.32540894 -89.17943573 38.71687698 122.3046875 -89.17943573 37.76794815 124.042304993
		 -89.17943573 36.077346802 125.072654724 -89.17943573 34.098072052 125.11966705 -89.17943573 32.36045837 124.17074585
		 -89.17943573 31.33010483 122.48013306 -91.35694122 31.28309631 120.50085449 -91.35694122 32.23202133 118.763237
		 -91.35694122 33.92262268 117.7328949 -91.35694122 35.90190125 117.68589783 -91.35694122 37.63951111 118.63480377
		 -91.35694122 38.66986465 120.32540894 -91.35694122 38.71687698 122.3046875 -91.35694122 37.76794815 124.042304993
		 -91.35694122 36.077346802 125.072654724 -91.35694122 34.098072052 125.11966705 -91.35694122 32.36045837 124.17074585
		 -91.35694122 31.33010483 122.48013306 -91.82691956 31.91902542 122.30725098 -91.82691956 31.87956047 120.64559937
		 -91.82691193 32.67621231 119.18682098 -91.82691193 32.78403091 123.72654724 -91.82691193 35.75717163 118.28234863
		 -91.82691193 35.90445709 124.48374176 -91.82691193 37.21594238 119.078994751 -91.82691193 37.3237648 123.61872864
		 -91.82691193 34.09551239 118.32180786 -91.82691193 34.24280548 124.52319336 -91.82691193 38.080947876 120.49829865
		 -91.82691193 38.12041473 122.15995026 -89.31186676 40.94268036 119.88568115 -89.31186676 42.19366074 121.27503967
		 -89.31186676 41.25888824 122.89411163 -89.31186676 39.43019104 122.50541687 -89.31186676 39.23477173 120.64609528
		 -90.90219879 40.94268036 119.88568115 -90.90219879 42.19366074 121.27503967 -90.90219879 41.25888824 122.89411163
		 -90.90219879 39.43019104 122.50541687 -90.90219879 39.23477173 120.64609528 -91.3911972 40.84445572 120.30561066
		 -91.3911972 41.75402832 121.3157959 -91.3911972 41.074359894 122.49301147 -91.3911972 39.74473572 122.21038818
		 -91.3911972 39.6026535 120.85851288 -89.31186676 38.27920914 126.58581543 -89.31186676 37.3444252 128.20489502
		 -89.31186676 35.515728 127.81619263 -89.31186676 35.32030869 125.95689392 -89.31186676 37.028240204 125.19647217
		 -90.90219879 38.27920914 126.58581543 -90.90219879 37.3444252 128.20489502 -90.90219879 35.515728 127.81619263
		 -90.90219879 35.32030869 125.95689392 -90.90219879 37.028240204 125.19647217 -91.3911972 37.84947586 126.62216187
		 -91.3911972 37.16980362 127.79937744 -91.3911972 35.84018326 127.51675415 -91.3911972 35.69809341 126.16487122
		 -91.3911972 36.93989944 125.61199188 -89.31186676 31.083965302 126.12315369 -89.31186676 29.25525284 125.73444366
		 -89.31186676 29.059833527 123.87514496 -89.31186676 30.76774979 123.11473083 -89.31186676 32.018726349 124.50408173
		 -90.90219879 31.083965302 126.12315369 -90.90219879 29.25525284 125.73444366 -90.90219879 29.059833527 123.87514496
		 -90.90219879 30.76774979 123.11473083 -90.90219879 32.018726349 124.50408173 -91.3911972 30.91659927 125.72570038;
	setAttr ".vt[498:663]" -91.3911972 29.58697128 125.44306946 -91.3911972 29.44488907 124.091186523
		 -91.3911972 30.6866951 123.53829956 -91.3911972 31.59625626 124.54846954 -89.31186676 29.30051994 119.13708496
		 -89.31186676 29.10509682 117.27776337 -89.31186676 30.81301308 116.51735687 -89.31186676 32.063987732 117.9066925
		 -89.31186676 31.1292057 119.52577972 -90.90219879 29.30051994 119.13708496 -90.90219879 29.10509682 117.27776337
		 -90.90219879 30.81301308 116.51735687 -90.90219879 32.063987732 117.9066925 -90.90219879 31.1292057 119.52577972
		 -91.3911972 29.62681007 118.85508728 -91.3911972 29.48472404 117.50320435 -91.3911972 30.72653389 116.95031738
		 -91.3911972 31.63609886 117.960495 -91.3911972 30.95644188 119.13770294 -89.31186676 35.39354324 115.28212738
		 -89.31186676 37.10147476 114.521698 -89.31186676 38.35245132 115.91104126 -89.31186676 37.41766739 117.53012848
		 -89.31186676 35.58897018 117.14141846 -90.90219879 35.39354324 115.28212738 -90.90219879 37.10147476 114.521698
		 -90.90219879 38.35245132 115.91104126 -90.90219879 37.41766739 117.53012848 -90.90219879 35.58897018 117.14141846
		 -91.3911972 35.7625618 115.50531006 -91.3911972 37.0043830872 114.9524231 -91.3911972 37.91394424 115.96260071
		 -91.3911972 37.23428726 117.13980865 -91.3911972 35.9046669 116.85719299 -89.48600769 35.066242218 130.53970337
		 -89.48600769 40.32346725 128.83242798 -88.092002869 34.32611465 131.3458252 -86.81005859 38.90481186 149.037719727
		 -87.68508911 40.39603043 149.31286621 -87.68508911 47.050174713 147.15081787 -88.092002869 41.39245987 129.049835205
		 -86.81005859 48.087268829 146.054168701 -89.48600769 43.76763535 124.19927216 -89.48600769 43.76848602 118.67177582
		 -88.092002869 44.30558014 125.15227509 -86.81005859 62.54646683 126.26477814 -87.68508911 63.26896667 124.93157196
		 -87.68508911 63.26896667 117.93498993 -88.092002869 44.30558014 117.72227478 -86.81005859 62.54646683 116.60977173
		 -89.48600769 40.42640305 113.96444702 -89.48600769 35.16969681 112.2555542 -88.092002869 41.49898911 113.74733734
		 -86.81005859 48.19378281 96.74299622 -87.68508911 47.14908981 95.64388275 -87.68508911 40.49494553 93.48181915
		 -88.092002869 34.43264389 111.45133972 -86.81005859 39.011333466 93.75943756 -89.48600769 29.65999985 113.97943115
		 -89.48600769 26.41033554 118.45076752 -88.092002869 29.78495789 112.8922348 -86.81005859 15.68168736 101.27048492
		 -87.68508911 14.31352711 101.9243927 -87.68508911 10.20103931 107.58474731 -88.092002869 25.41770935 118.90323639
		 -86.81005859 10.0066156387 109.081542969 -89.48600769 26.34724808 124.22350311 -89.48600769 29.59553909 128.6958313
		 -88.092002869 25.35187149 123.76870728 -86.81005859 9.9407692 133.59039307 -87.68508911 10.13989639 135.093658447
		 -87.68508911 14.25238419 140.75401306 -88.092002869 29.71912003 129.7796936 -86.81005859 15.61584091 141.40145874
		 -88.37075043 17.50688171 129.94955444 -89.62237549 17.96699905 130.71200562 -89.62237549 20.83360672 134.657547
		 -88.37075043 21.41653442 135.33074951 -88.37075043 37.78023148 140.71684265 -89.62237549 38.64752197 140.5148468
		 -89.62237549 43.2857933 139.0077972412 -88.37075043 44.1061821 138.66140747 -88.37075043 54.28533173 124.76302338
		 -89.62237549 54.36123276 123.87575531 -89.62237549 54.36123276 118.99879456 -88.37075043 54.28533173 118.11152649
		 -88.37075043 44.2127037 104.13573456 -89.62237549 43.39232254 103.78936768 -89.62237549 38.75405502 102.28230286
		 -88.37075043 37.88676071 102.080307007 -88.37075043 21.48237228 107.34120178 -89.62237549 20.89944458 108.014404297
		 -89.62237549 18.032836914 111.95994568 -88.37075043 17.57271957 112.72238159 -86.80031586 61.84569931 126.92328644
		 -86.80031586 62.40532684 121.22522736 -86.80031586 60.11361694 132.38047791 -86.80031586 57.28476715 137.35827637
		 -86.80031586 53.48278809 141.63920593 -86.80031586 48.87384033 145.036026001 -86.80031586 43.65937424 147.40040588
		 -86.80031586 38.067264557 148.62898254 -86.80031586 32.34191513 148.66802979 -86.80031586 26.73357773 147.51585388
		 -86.80031586 21.4873333 145.22283936 -86.80031586 16.83248711 141.8891449 -86.80031586 12.97245693 137.66053772
		 -86.80031586 10.075974464 132.72177124 -86.80031586 8.26960659 127.28872681 -86.80031586 7.63230753 121.59882355
		 -86.80031586 8.19193172 115.90075684 -86.80031586 9.92401886 110.44356537 -86.80031586 12.7528677 105.46576691
		 -86.80031586 16.5548439 101.18486023 -86.80031586 21.16378784 97.78800964 -86.80031586 26.37825775 95.42362976
		 -86.80031586 31.97036743 94.19506073 -86.80031586 37.69571304 94.15600586 -86.80031586 43.30405807 95.30817413
		 -86.80031586 48.5503006 97.60121155 -86.80031586 53.20514297 100.93489838 -86.80031586 57.065170288 105.16352081
		 -86.80031586 59.96165466 110.10227966 -86.80031586 61.76802826 115.53533173 -86.80031586 59.82172394 126.50748444
		 -86.80031586 60.3391304 121.23932648 -86.80031586 58.22031784 131.552948 -86.80031586 55.60489273 136.15519714
		 -86.80031586 52.089759827 140.11312866 -86.80031586 47.82853699 143.25369263 -86.80031586 43.0074806213 145.43969727
		 -86.80031586 37.83727264 146.57557678 -86.80031586 32.54388046 146.61167908 -86.80031586 27.35866356 145.5464325
		 -86.80031586 22.5082283 143.42640686 -86.80031586 18.20457077 140.34423828 -86.80031586 14.63576317 136.43466187
		 -86.80031586 11.95780754 131.86849976 -86.80031586 10.28772259 126.84535217 -86.80031586 9.6985054 121.58472443
		 -86.80031586 10.21590805 116.31655884 -86.80031586 11.81731701 111.27108765 -86.80031586 14.43274021 106.66884613
		 -86.80031586 17.94787407 102.71091461 -86.80031586 22.20909119 99.57035065 -86.80031586 27.030151367 97.38434601
		 -86.80031586 32.20036316 96.24846649 -86.80031586 37.49375153 96.21237183 -86.80031586 42.67897034 97.27759552
		 -86.80031586 47.5294075 99.39763641 -86.80031586 51.83306122 102.47980499 -86.80031586 55.40186691 106.3894043
		 -86.80031586 58.07982254 110.95555115 -86.80031586 59.74991226 115.97870636 -68.54407501 59.82172394 126.50748444
		 -68.54407501 60.3391304 121.23932648 -68.54407501 58.22031784 131.552948 -68.54407501 55.60489273 136.15519714
		 -68.54407501 52.089759827 140.11312866 -68.54407501 47.82853699 143.25369263 -68.54407501 43.0074806213 145.43969727
		 -68.54407501 37.83727264 146.57557678 -68.54407501 32.54388046 146.61167908 -68.54407501 27.35866356 145.5464325
		 -68.54407501 22.5082283 143.42640686 -68.54407501 18.20457077 140.34423828;
	setAttr ".vt[664:829]" -68.54407501 14.63576317 136.43466187 -68.54407501 11.95780754 131.86849976
		 -68.54407501 10.28772259 126.84535217 -68.54407501 9.6985054 121.58472443 -68.54407501 10.21590805 116.31655884
		 -68.54407501 11.81731701 111.27108765 -68.54407501 14.43274021 106.66884613 -68.54407501 17.94787407 102.71091461
		 -68.54407501 22.20909119 99.57035065 -68.54407501 27.030151367 97.38434601 -68.54407501 32.20036316 96.24846649
		 -68.54407501 37.49375153 96.21237183 -68.54407501 42.67897034 97.27759552 -68.54407501 47.5294075 99.39763641
		 -68.54407501 51.83306122 102.47980499 -68.54407501 55.40186691 106.3894043 -68.54407501 58.07982254 110.95555115
		 -68.54407501 59.74991226 115.97870636 77.0064620972 55.42156982 -129.13955688 77.0064620972 50.84530258 -121.92560577
		 77.0064620972 43.85671234 -117.012031555 77.0064620972 35.51977539 -115.14691925
		 77.0064620972 27.10368729 -116.61421967 77.0064620972 19.88973999 -121.19054413 77.0064620972 14.97619247 -128.17907715
		 77.0064620972 13.11108208 -136.51599121 77.0064620972 14.57834816 -144.93211365 77.0064620972 19.15463448 -152.14602661
		 77.0064620972 26.14322281 -157.05960083 77.0064620972 34.48016739 -158.92469788 77.0064620972 42.89624405 -157.45742798
		 77.0064620972 50.11019135 -152.88116455 77.0064620972 55.023742676 -145.89254761
		 77.0064620972 56.88884735 -137.55560303 81.83740234 55.42156982 -129.13955688 81.83740234 50.84530258 -121.92560577
		 81.83740234 43.85671234 -117.012031555 81.83740234 35.51977539 -115.14691925 81.83740234 27.10368729 -116.61421967
		 81.83740234 19.88973999 -121.19054413 81.83740234 14.97619247 -128.17907715 81.83740234 13.11108208 -136.51599121
		 81.83740234 14.57834816 -144.93211365 81.83740234 19.15463448 -152.14602661 81.83740234 26.14322281 -157.05960083
		 81.83740234 34.48016739 -158.92469788 81.83740234 42.89624405 -157.45742798 81.83740234 50.11019135 -152.88116455
		 81.83740234 55.023742676 -145.89254761 81.83740234 56.88884735 -137.55560303 82.29216003 46.3356781 -132.65274048
		 82.29216003 43.79545593 -128.64839172 82.29216003 39.91619873 -125.92093658 82.29216003 35.28850174 -124.88565826
		 82.29216003 30.61686325 -125.70011139 82.29216003 26.61252594 -128.24034119 82.29216003 23.88508224 -132.11956787
		 82.29216003 22.8497963 -136.74728394 82.29216003 23.66425323 -141.41891479 82.29216003 26.2044754 -145.42324829
		 82.29216003 30.083730698 -148.1506958 82.29216003 34.71143341 -149.18598938 82.29216003 39.38306427 -148.37153625
		 82.29216003 43.38740921 -145.83129883 82.29216003 46.11484528 -141.95205688 82.29216003 47.15013504 -137.32434082
		 87.89835358 45.20834351 -133.088623047 87.89835358 42.92074203 -129.48249817 87.89835358 39.42728424 -127.026298523
		 87.89835358 35.25980759 -126.093986511 87.89835358 31.052768707 -126.82743073 87.89835358 27.44664955 -129.11505127
		 87.89835358 24.99045181 -132.60850525 87.89835358 24.058128357 -136.77600098 87.89835358 24.79158401 -140.98301697
		 87.89835358 27.079185486 -144.58914185 87.89835358 30.57265091 -147.045318604 87.89835358 34.74013138 -147.97766113
		 87.89835358 38.94716263 -147.24420166 87.89835358 42.55328369 -144.956604 87.89835358 45.009475708 -141.46313477
		 87.89835358 45.94179535 -137.2956543 65.50907898 45.98304367 -132.7890625 65.50907898 43.52185822 -128.90927124
		 65.50907898 39.76327515 -126.26670074 65.50907898 35.27952576 -125.26358795 65.50907898 30.7532196 -126.052742004
		 65.50907898 26.87342453 -128.51396179 65.50907898 24.23085022 -132.27252197 65.50907898 23.22774506 -136.75624084
		 65.50907898 24.016876221 -141.28257751 65.50907898 26.47807693 -145.16235352 65.50907898 30.23666191 -147.80493164
		 65.50907898 34.72040939 -148.80802917 65.50907898 39.24671173 -148.018890381 65.50907898 43.12650299 -145.55770874
		 65.50907898 45.76908875 -141.79911804 65.50907898 46.77218246 -137.31536865 89.17943573 31.28306198 -137.93777466
		 89.17943573 32.231987 -139.67538452 89.17943573 33.92258835 -140.70574951 89.17943573 35.90186691 -140.75274658
		 89.17943573 37.63947678 -139.80383301 89.17943573 38.66983032 -138.11322021 89.17943573 38.71684265 -136.13394165
		 89.17943573 37.76791382 -134.39633179 89.17943573 36.077312469 -133.3659668 89.17943573 34.09803772 -133.31896973
		 89.17943573 32.36042404 -134.2678833 89.17943573 31.3300705 -135.95849609 91.35694122 31.28306198 -137.93777466
		 91.35694122 32.231987 -139.67538452 91.35694122 33.92258835 -140.70574951 91.35694122 35.90186691 -140.75274658
		 91.35694122 37.63947678 -139.80383301 91.35694122 38.66983032 -138.11322021 91.35694122 38.71684265 -136.13394165
		 91.35694122 37.76791382 -134.39633179 91.35694122 36.077312469 -133.3659668 91.35694122 34.09803772 -133.31896973
		 91.35694122 32.36042404 -134.2678833 91.35694122 31.3300705 -135.95849609 91.82691956 31.91899109 -136.13137817
		 91.82691956 31.87952614 -137.79302979 91.82691193 32.67617798 -139.25180054 91.82691193 32.7840004 -134.71208191
		 91.82691193 35.7571373 -140.15628052 91.82691193 35.90442276 -133.95489502 91.82691193 37.21590805 -139.3596344
		 91.82691193 37.32373047 -134.81990051 91.82691193 34.095478058 -140.11682129 91.82691193 34.24277115 -133.91543579
		 91.82691193 38.080913544 -137.94033813 91.82691193 38.1203804 -136.27868652 89.31186676 40.94264603 -138.552948
		 89.31186676 42.1936264 -137.16358948 89.31186676 41.25885391 -135.54452515 89.31186676 39.43015671 -135.93321228
		 89.31186676 39.2347374 -137.7925415 90.90219879 40.94264603 -138.552948 90.90219879 42.1936264 -137.16358948
		 90.90219879 41.25885391 -135.54452515 90.90219879 39.43015671 -135.93321228 90.90219879 39.2347374 -137.7925415
		 91.3911972 40.84442139 -138.13302612 91.3911972 41.75399399 -137.12283325 91.3911972 41.074325562 -135.94561768
		 91.3911972 39.74470139 -136.22824097 91.3911972 39.60261917 -137.58010864 89.31186676 38.2791748 -131.85281372
		 89.31186676 37.34439087 -130.23373413 89.31186676 35.51569366 -130.62243652 89.31186676 35.32027435 -132.48173523
		 89.31186676 37.028205872 -133.24215698 90.90219879 38.2791748 -131.85281372 90.90219879 37.34439087 -130.23373413
		 90.90219879 35.51569366 -130.62243652 90.90219879 35.32027435 -132.48173523 90.90219879 37.028205872 -133.24215698
		 91.3911972 37.84944153 -131.81646729 91.3911972 37.16976929 -130.63925171 91.3911972 35.84014893 -130.921875
		 91.3911972 35.69805908 -132.27375793 91.3911972 36.93986511 -132.8266449 89.31186676 31.083930969 -132.31547546
		 89.31186676 29.25521851 -132.70419312;
	setAttr ".vt[830:995]" 89.31186676 29.059799194 -134.56347656 89.31186676 30.76771545 -135.32389832
		 89.31186676 32.018692017 -133.93453979 90.90219879 31.083930969 -132.31547546 90.90219879 29.25521851 -132.70419312
		 90.90219879 29.059799194 -134.56347656 90.90219879 30.76771545 -135.32389832 90.90219879 32.018692017 -133.93453979
		 91.3911972 30.91656494 -132.7129364 91.3911972 29.58693695 -132.99555969 91.3911972 29.44485474 -134.34744263
		 91.3911972 30.68666077 -134.90032959 91.3911972 31.59622192 -133.89016724 89.31186676 29.30048561 -139.30154419
		 89.31186676 29.10506248 -141.16085815 89.31186676 30.81297874 -141.92126465 89.31186676 32.063957214 -140.53193665
		 89.31186676 31.12917137 -138.9128418 90.90219879 29.30048561 -139.30154419 90.90219879 29.10506248 -141.16085815
		 90.90219879 30.81297874 -141.92126465 90.90219879 32.063957214 -140.53193665 90.90219879 31.12917137 -138.9128418
		 91.3911972 29.62677574 -139.58354187 91.3911972 29.48468971 -140.9354248 91.3911972 30.72649956 -141.48831177
		 91.3911972 31.63606453 -140.47813416 91.3911972 30.95640755 -139.30093384 89.31186676 35.39350891 -143.1565094
		 89.31186676 37.10144043 -143.91693115 89.31186676 38.35241699 -142.52758789 89.31186676 37.41763306 -140.9085083
		 89.31186676 35.58893585 -141.29721069 90.90219879 35.39350891 -143.1565094 90.90219879 37.10144043 -143.91693115
		 90.90219879 38.35241699 -142.52758789 90.90219879 37.41763306 -140.9085083 90.90219879 35.58893585 -141.29721069
		 91.3911972 35.76252747 -142.93331909 91.3911972 37.0043487549 -143.48620605 91.3911972 37.91390991 -142.47602844
		 91.3911972 37.23425293 -141.29882813 91.3911972 35.90463257 -141.58145142 89.48600769 35.066207886 -127.89893341
		 89.48600769 40.32343292 -129.60620117 88.092002869 34.32608032 -127.092811584 86.81005859 38.90477753 -109.40091705
		 87.68508911 40.39599609 -109.12577057 87.68508911 47.050140381 -111.28781891 88.092002869 41.39242554 -129.38879395
		 86.81005859 48.087234497 -112.38446808 89.48600769 43.76760101 -134.23934937 89.48600769 43.76845169 -139.7668457
		 88.092002869 44.30554581 -133.28634644 86.81005859 62.5464325 -132.17385864 87.68508911 63.26893234 -133.50706482
		 87.68508911 63.26893234 -140.50364685 88.092002869 44.30554581 -140.71635437 86.81005859 62.5464325 -141.82885742
		 89.48600769 40.42636871 -144.47418213 89.48600769 35.16966248 -146.18307495 88.092002869 41.49895477 -144.69129944
		 86.81005859 48.19374847 -161.69563293 87.68508911 47.14905548 -162.79475403 87.68508911 40.49491119 -164.95681763
		 88.092002869 34.43260956 -146.98730469 86.81005859 39.011299133 -164.67919922 89.48600769 29.65996552 -144.459198
		 89.48600769 26.41030121 -139.987854 88.092002869 29.78492355 -145.54638672 86.81005859 15.68165302 -157.16815186
		 87.68508911 14.31349277 -156.51423645 87.68508911 10.20100498 -150.85388184 88.092002869 25.41767502 -139.53540039
		 86.81005859 10.0065813065 -149.35708618 89.48600769 26.34721375 -134.21513367 89.48600769 29.59550476 -129.74279785
		 88.092002869 25.35183716 -134.66992188 86.81005859 9.94073486 -124.84824371 87.68508911 10.13986206 -123.34497833
		 87.68508911 14.25234985 -117.68462372 88.092002869 29.71908569 -128.65893555 86.81005859 15.61580658 -117.03717804
		 88.37075043 17.50684738 -128.48907471 89.62237549 17.96696472 -127.72664642 89.62237549 20.83357239 -123.78108978
		 88.37075043 21.41650009 -123.10788727 88.37075043 37.78019714 -117.72179413 89.62237549 38.64748764 -117.92378998
		 89.62237549 43.28575897 -119.4308548 88.37075043 44.10614777 -119.77722931 88.37075043 54.28529739 -133.67559814
		 89.62237549 54.36119843 -134.56286621 89.62237549 54.36119843 -139.43983459 88.37075043 54.28529739 -140.32710266
		 88.37075043 44.21266937 -154.30288696 89.62237549 43.39228821 -154.64926147 89.62237549 38.75402069 -156.15632629
		 88.37075043 37.88672638 -156.35832214 88.37075043 21.48233795 -151.097427368 89.62237549 20.89941025 -150.42422485
		 89.62237549 18.032802582 -146.47868347 88.37075043 17.57268524 -145.71624756 86.80031586 61.84566498 -131.51535034
		 86.80031586 62.40529251 -137.21340942 86.80031586 60.11358261 -126.058158875 86.80031586 57.28473282 -121.080360413
		 86.80031586 53.48275375 -116.79944611 86.80031586 48.873806 -113.40261078 86.80031586 43.6593399 -111.038230896
		 86.80031586 38.067230225 -109.80965424 86.80031586 32.34188461 -109.77060699 86.80031586 26.7335434 -110.9227829
		 86.80031586 21.48729897 -113.21579742 86.80031586 16.83245468 -116.54949188 86.80031586 12.9724226 -120.77809906
		 86.80031586 10.075940132 -125.71686554 86.80031586 8.26957226 -131.14990234 86.80031586 7.6322732 -136.83981323
		 86.80031586 8.19189739 -142.53787231 86.80031586 9.92398453 -147.99505615 86.80031586 12.75283337 -152.97286987
		 86.80031586 16.55480957 -157.25376892 86.80031586 21.16375351 -160.65061951 86.80031586 26.37822342 -163.01499939
		 86.80031586 31.9703331 -164.24356079 86.80031586 37.69567871 -164.28262329 86.80031586 43.30402374 -163.13046265
		 86.80031586 48.55026627 -160.8374176 86.80031586 53.20510864 -157.5037384 86.80031586 57.065135956 -153.27511597
		 86.80031586 59.96162033 -148.33634949 86.80031586 61.76799393 -142.90330505 86.80031586 59.82168961 -131.93115234
		 86.80031586 60.33909607 -137.1993103 86.80031586 58.22028351 -126.88568878 86.80031586 55.6048584 -122.28343964
		 86.80031586 52.089725494 -118.32550812 86.80031586 47.82850266 -115.18494415 86.80031586 43.0074462891 -112.99893951
		 86.80031586 37.83723831 -111.86306 86.80031586 32.54384613 -111.8269577 86.80031586 27.35862923 -112.89220428
		 86.80031586 22.50819397 -115.012229919 86.80031586 18.20453644 -118.094398499 86.80031586 14.63572884 -122.0039749146
		 86.80031586 11.95777321 -126.57013702 86.80031586 10.28768826 -131.59327698 86.80031586 9.69847107 -136.85391235
		 86.80031586 10.21587372 -142.12207031 86.80031586 11.81728268 -147.1675415 86.80031586 14.43270588 -151.76977539
		 86.80031586 17.94783974 -155.72772217 86.80031586 22.20905685 -158.86828613 86.80031586 27.030117035 -161.054290771
		 86.80031586 32.20032883 -162.19015503 86.80031586 37.49371719 -162.22625732 86.80031586 42.678936 -161.16104126
		 86.80031586 47.52937317 -159.040985107 86.80031586 51.83302689 -155.95883179 86.80031586 55.40183258 -152.049224854
		 86.80031586 58.079788208 -147.483078 86.80031586 59.74987793 -142.45993042 68.54407501 59.82168961 -131.93115234
		 68.54407501 60.33909607 -137.1993103 68.54407501 58.22028351 -126.88568878;
	setAttr ".vt[996:1161]" 68.54407501 55.6048584 -122.28343964 68.54407501 52.089725494 -118.32550812
		 68.54407501 47.82850266 -115.18494415 68.54407501 43.0074462891 -112.99893951 68.54407501 37.83723831 -111.86306
		 68.54407501 32.54384613 -111.8269577 68.54407501 27.35862923 -112.89220428 68.54407501 22.50819397 -115.012229919
		 68.54407501 18.20453644 -118.094398499 68.54407501 14.63572884 -122.0039749146 68.54407501 11.95777321 -126.57013702
		 68.54407501 10.28768826 -131.59327698 68.54407501 9.69847107 -136.85391235 68.54407501 10.21587372 -142.12207031
		 68.54407501 11.81728268 -147.1675415 68.54407501 14.43270588 -151.76977539 68.54407501 17.94783974 -155.72772217
		 68.54407501 22.20905685 -158.86828613 68.54407501 27.030117035 -161.054290771 68.54407501 32.20032883 -162.19015503
		 68.54407501 37.49371719 -162.22625732 68.54407501 42.678936 -161.16104126 68.54407501 47.52937317 -159.040985107
		 68.54407501 51.83302689 -155.95883179 68.54407501 55.40183258 -152.049224854 68.54407501 58.079788208 -147.483078
		 68.54407501 59.74987793 -142.45993042 77.0064620972 55.42160416 129.29908752 77.0064620972 50.84533691 136.51303101
		 77.0064620972 43.85674667 141.42660522 77.0064620972 35.51980972 143.29171753 77.0064620972 27.10372162 141.82443237
		 77.0064620972 19.88977432 137.24809265 77.0064620972 14.97622681 130.259552 77.0064620972 13.11111641 121.92264557
		 77.0064620972 14.57838249 113.5065155 77.0064620972 19.15466881 106.29259491 77.0064620972 26.14325714 101.37902832
		 77.0064620972 34.48020172 99.5139389 77.0064620972 42.89627838 100.9812088 77.0064620972 50.11022568 105.55747223
		 77.0064620972 55.023777008 112.54607391 77.0064620972 56.88888168 120.88301849 81.83740234 55.42160416 129.29908752
		 81.83740234 50.84533691 136.51303101 81.83740234 43.85674667 141.42660522 81.83740234 35.51980972 143.29171753
		 81.83740234 27.10372162 141.82443237 81.83740234 19.88977432 137.24809265 81.83740234 14.97622681 130.259552
		 81.83740234 13.11111641 121.92264557 81.83740234 14.57838249 113.5065155 81.83740234 19.15466881 106.29259491
		 81.83740234 26.14325714 101.37902832 81.83740234 34.48020172 99.5139389 81.83740234 42.89627838 100.9812088
		 81.83740234 50.11022568 105.55747223 81.83740234 55.023777008 112.54607391 81.83740234 56.88888168 120.88301849
		 82.29216003 46.33571243 125.78588867 82.29216003 43.79549026 129.79023743 82.29216003 39.91623306 132.5177002
		 82.29216003 35.28853607 133.55297852 82.29216003 30.61689758 132.73852539 82.29216003 26.61256027 130.19828796
		 82.29216003 23.88511658 126.31905365 82.29216003 22.84983063 121.69133759 82.29216003 23.66428757 117.019706726
		 82.29216003 26.20450974 113.01537323 82.29216003 30.08376503 110.28793335 82.29216003 34.71146774 109.25263977
		 82.29216003 39.3830986 110.067092896 82.29216003 43.38744354 112.60732269 82.29216003 46.11487961 116.48658752
		 82.29216003 47.15016937 121.11428833 87.89835358 45.20837784 125.34999847 87.89835358 42.92077637 128.95613098
		 87.89835358 39.42731857 131.41233826 87.89835358 35.25984192 132.34465027 87.89835358 31.05280304 131.61120605
		 87.89835358 27.44668388 129.32357788 87.89835358 24.99048615 125.8301239 87.89835358 24.058162689 121.66262817
		 87.89835358 24.79161835 117.45561981 87.89835358 27.079219818 113.84949493 87.89835358 30.57268524 111.39330292
		 87.89835358 34.74016571 110.46097565 87.89835358 38.94719696 111.19442749 87.89835358 42.55331802 113.48204041
		 87.89835358 45.0095100403 116.97549438 87.89835358 45.94182968 121.14297485 65.50907898 45.983078 125.64955902
		 65.50907898 43.52189255 129.52935791 65.50907898 39.76330948 132.17193604 65.50907898 35.27956009 133.17504883
		 65.50907898 30.75325394 132.38589478 65.50907898 26.87345886 129.92466736 65.50907898 24.23088455 126.16611481
		 65.50907898 23.22777939 121.68239594 65.50907898 24.016910553 117.15605164 65.50907898 26.47811127 113.27628326
		 65.50907898 30.23669624 110.63368988 65.50907898 34.72044373 109.63059998 65.50907898 39.24674606 110.41973877
		 65.50907898 43.12653732 112.88092041 65.50907898 45.76912308 116.63951111 65.50907898 46.7722168 121.1232605
		 89.17943573 31.28309631 120.50085449 89.17943573 32.23202133 118.763237 89.17943573 33.92262268 117.7328949
		 89.17943573 35.90190125 117.68589783 89.17943573 37.63951111 118.63480377 89.17943573 38.66986465 120.32540894
		 89.17943573 38.71687698 122.3046875 89.17943573 37.76794815 124.042304993 89.17943573 36.077346802 125.072654724
		 89.17943573 34.098072052 125.11966705 89.17943573 32.36045837 124.17074585 89.17943573 31.33010483 122.48013306
		 91.35694122 31.28309631 120.50085449 91.35694122 32.23202133 118.763237 91.35694122 33.92262268 117.7328949
		 91.35694122 35.90190125 117.68589783 91.35694122 37.63951111 118.63480377 91.35694122 38.66986465 120.32540894
		 91.35694122 38.71687698 122.3046875 91.35694122 37.76794815 124.042304993 91.35694122 36.077346802 125.072654724
		 91.35694122 34.098072052 125.11966705 91.35694122 32.36045837 124.17074585 91.35694122 31.33010483 122.48013306
		 91.82691956 31.91902542 122.30725098 91.82691956 31.87956047 120.64559937 91.82691193 32.67621231 119.18682098
		 91.82691193 32.78403091 123.72654724 91.82691193 35.75717163 118.28234863 91.82691193 35.90445709 124.48374176
		 91.82691193 37.21594238 119.078994751 91.82691193 37.3237648 123.61872864 91.82691193 34.09551239 118.32180786
		 91.82691193 34.24280548 124.52319336 91.82691193 38.080947876 120.49829865 91.82691193 38.12041473 122.15995026
		 89.31186676 40.94268036 119.88568115 89.31186676 42.19366074 121.27503967 89.31186676 41.25888824 122.89411163
		 89.31186676 39.43019104 122.50541687 89.31186676 39.23477173 120.64609528 90.90219879 40.94268036 119.88568115
		 90.90219879 42.19366074 121.27503967 90.90219879 41.25888824 122.89411163 90.90219879 39.43019104 122.50541687
		 90.90219879 39.23477173 120.64609528 91.3911972 40.84445572 120.30561066 91.3911972 41.75402832 121.3157959
		 91.3911972 41.074359894 122.49301147 91.3911972 39.74473572 122.21038818 91.3911972 39.6026535 120.85851288
		 89.31186676 38.27920914 126.58581543 89.31186676 37.3444252 128.20489502 89.31186676 35.515728 127.81619263
		 89.31186676 35.32030869 125.95689392 89.31186676 37.028240204 125.19647217 90.90219879 38.27920914 126.58581543
		 90.90219879 37.3444252 128.20489502 90.90219879 35.515728 127.81619263;
	setAttr ".vt[1162:1327]" 90.90219879 35.32030869 125.95689392 90.90219879 37.028240204 125.19647217
		 91.3911972 37.84947586 126.62216187 91.3911972 37.16980362 127.79937744 91.3911972 35.84018326 127.51675415
		 91.3911972 35.69809341 126.16487122 91.3911972 36.93989944 125.61199188 89.31186676 31.083965302 126.12315369
		 89.31186676 29.25525284 125.73444366 89.31186676 29.059833527 123.87514496 89.31186676 30.76774979 123.11473083
		 89.31186676 32.018726349 124.50408173 90.90219879 31.083965302 126.12315369 90.90219879 29.25525284 125.73444366
		 90.90219879 29.059833527 123.87514496 90.90219879 30.76774979 123.11473083 90.90219879 32.018726349 124.50408173
		 91.3911972 30.91659927 125.72570038 91.3911972 29.58697128 125.44306946 91.3911972 29.44488907 124.091186523
		 91.3911972 30.6866951 123.53829956 91.3911972 31.59625626 124.54846954 89.31186676 29.30051994 119.13708496
		 89.31186676 29.10509682 117.27776337 89.31186676 30.81301308 116.51735687 89.31186676 32.063987732 117.9066925
		 89.31186676 31.1292057 119.52577972 90.90219879 29.30051994 119.13708496 90.90219879 29.10509682 117.27776337
		 90.90219879 30.81301308 116.51735687 90.90219879 32.063987732 117.9066925 90.90219879 31.1292057 119.52577972
		 91.3911972 29.62681007 118.85508728 91.3911972 29.48472404 117.50320435 91.3911972 30.72653389 116.95031738
		 91.3911972 31.63609886 117.960495 91.3911972 30.95644188 119.13770294 89.31186676 35.39354324 115.28212738
		 89.31186676 37.10147476 114.521698 89.31186676 38.35245132 115.91104126 89.31186676 37.41766739 117.53012848
		 89.31186676 35.58897018 117.14141846 90.90219879 35.39354324 115.28212738 90.90219879 37.10147476 114.521698
		 90.90219879 38.35245132 115.91104126 90.90219879 37.41766739 117.53012848 90.90219879 35.58897018 117.14141846
		 91.3911972 35.7625618 115.50531006 91.3911972 37.0043830872 114.9524231 91.3911972 37.91394424 115.96260071
		 91.3911972 37.23428726 117.13980865 91.3911972 35.9046669 116.85719299 89.48600769 35.066242218 130.53970337
		 89.48600769 40.32346725 128.83242798 88.092002869 34.32611465 131.3458252 86.81005859 38.90481186 149.037719727
		 87.68508911 40.39603043 149.31286621 87.68508911 47.050174713 147.15081787 88.092002869 41.39245987 129.049835205
		 86.81005859 48.087268829 146.054168701 89.48600769 43.76763535 124.19927216 89.48600769 43.76848602 118.67177582
		 88.092002869 44.30558014 125.15227509 86.81005859 62.54646683 126.26477814 87.68508911 63.26896667 124.93157196
		 87.68508911 63.26896667 117.93498993 88.092002869 44.30558014 117.72227478 86.81005859 62.54646683 116.60977173
		 89.48600769 40.42640305 113.96444702 89.48600769 35.16969681 112.2555542 88.092002869 41.49898911 113.74733734
		 86.81005859 48.19378281 96.74299622 87.68508911 47.14908981 95.64388275 87.68508911 40.49494553 93.48181915
		 88.092002869 34.43264389 111.45133972 86.81005859 39.011333466 93.75943756 89.48600769 29.65999985 113.97943115
		 89.48600769 26.41033554 118.45076752 88.092002869 29.78495789 112.8922348 86.81005859 15.68168736 101.27048492
		 87.68508911 14.31352711 101.9243927 87.68508911 10.20103931 107.58474731 88.092002869 25.41770935 118.90323639
		 86.81005859 10.0066156387 109.081542969 89.48600769 26.34724808 124.22350311 89.48600769 29.59553909 128.6958313
		 88.092002869 25.35187149 123.76870728 86.81005859 9.9407692 133.59039307 87.68508911 10.13989639 135.093658447
		 87.68508911 14.25238419 140.75401306 88.092002869 29.71912003 129.7796936 86.81005859 15.61584091 141.40145874
		 88.37075043 17.50688171 129.94955444 89.62237549 17.96699905 130.71200562 89.62237549 20.83360672 134.657547
		 88.37075043 21.41653442 135.33074951 88.37075043 37.78023148 140.71684265 89.62237549 38.64752197 140.5148468
		 89.62237549 43.2857933 139.0077972412 88.37075043 44.1061821 138.66140747 88.37075043 54.28533173 124.76302338
		 89.62237549 54.36123276 123.87575531 89.62237549 54.36123276 118.99879456 88.37075043 54.28533173 118.11152649
		 88.37075043 44.2127037 104.13573456 89.62237549 43.39232254 103.78936768 89.62237549 38.75405502 102.28230286
		 88.37075043 37.88676071 102.080307007 88.37075043 21.48237228 107.34120178 89.62237549 20.89944458 108.014404297
		 89.62237549 18.032836914 111.95994568 88.37075043 17.57271957 112.72238159 86.80031586 61.84569931 126.92328644
		 86.80031586 62.40532684 121.22522736 86.80031586 60.11361694 132.38047791 86.80031586 57.28476715 137.35827637
		 86.80031586 53.48278809 141.63920593 86.80031586 48.87384033 145.036026001 86.80031586 43.65937424 147.40040588
		 86.80031586 38.067264557 148.62898254 86.80031586 32.34191513 148.66802979 86.80031586 26.73357773 147.51585388
		 86.80031586 21.4873333 145.22283936 86.80031586 16.83248711 141.8891449 86.80031586 12.97245693 137.66053772
		 86.80031586 10.075974464 132.72177124 86.80031586 8.26960659 127.28872681 86.80031586 7.63230753 121.59882355
		 86.80031586 8.19193172 115.90075684 86.80031586 9.92401886 110.44356537 86.80031586 12.7528677 105.46576691
		 86.80031586 16.5548439 101.18486023 86.80031586 21.16378784 97.78800964 86.80031586 26.37825775 95.42362976
		 86.80031586 31.97036743 94.19506073 86.80031586 37.69571304 94.15600586 86.80031586 43.30405807 95.30817413
		 86.80031586 48.5503006 97.60121155 86.80031586 53.20514297 100.93489838 86.80031586 57.065170288 105.16352081
		 86.80031586 59.96165466 110.10227966 86.80031586 61.76802826 115.53533173 86.80031586 59.82172394 126.50748444
		 86.80031586 60.3391304 121.23932648 86.80031586 58.22031784 131.552948 86.80031586 55.60489273 136.15519714
		 86.80031586 52.089759827 140.11312866 86.80031586 47.82853699 143.25369263 86.80031586 43.0074806213 145.43969727
		 86.80031586 37.83727264 146.57557678 86.80031586 32.54388046 146.61167908 86.80031586 27.35866356 145.5464325
		 86.80031586 22.5082283 143.42640686 86.80031586 18.20457077 140.34423828 86.80031586 14.63576317 136.43466187
		 86.80031586 11.95780754 131.86849976 86.80031586 10.28772259 126.84535217 86.80031586 9.6985054 121.58472443
		 86.80031586 10.21590805 116.31655884 86.80031586 11.81731701 111.27108765 86.80031586 14.43274021 106.66884613
		 86.80031586 17.94787407 102.71091461 86.80031586 22.20909119 99.57035065 86.80031586 27.030151367 97.38434601
		 86.80031586 32.20036316 96.24846649 86.80031586 37.49375153 96.21237183;
	setAttr ".vt[1328:1367]" 86.80031586 42.67897034 97.27759552 86.80031586 47.5294075 99.39763641
		 86.80031586 51.83306122 102.47980499 86.80031586 55.40186691 106.3894043 86.80031586 58.07982254 110.95555115
		 86.80031586 59.74991226 115.97870636 68.54407501 59.82172394 126.50748444 68.54407501 60.3391304 121.23932648
		 68.54407501 58.22031784 131.552948 68.54407501 55.60489273 136.15519714 68.54407501 52.089759827 140.11312866
		 68.54407501 47.82853699 143.25369263 68.54407501 43.0074806213 145.43969727 68.54407501 37.83727264 146.57557678
		 68.54407501 32.54388046 146.61167908 68.54407501 27.35866356 145.5464325 68.54407501 22.5082283 143.42640686
		 68.54407501 18.20457077 140.34423828 68.54407501 14.63576317 136.43466187 68.54407501 11.95780754 131.86849976
		 68.54407501 10.28772259 126.84535217 68.54407501 9.6985054 121.58472443 68.54407501 10.21590805 116.31655884
		 68.54407501 11.81731701 111.27108765 68.54407501 14.43274021 106.66884613 68.54407501 17.94787407 102.71091461
		 68.54407501 22.20909119 99.57035065 68.54407501 27.030151367 97.38434601 68.54407501 32.20036316 96.24846649
		 68.54407501 37.49375153 96.21237183 68.54407501 42.67897034 97.27759552 68.54407501 47.5294075 99.39763641
		 68.54407501 51.83306122 102.47980499 68.54407501 55.40186691 106.3894043 68.54407501 58.07982254 110.95555115
		 68.54407501 59.74991226 115.97870636 91.8269043 34.99998093 121.40275574 -91.8269043 34.99998856 121.40276337
		 -91.8269043 34.99995422 -137.036026001 91.8269043 34.99995041 -137.035827637;
	setAttr -s 2452 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 0 0 16 17 0 17 18 0 18 19 0 19 20 0
		 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0 29 30 0 30 31 0
		 31 16 0 0 16 1 1 17 1 2 18 1 3 19 1 4 20 1 5 21 1 6 22 1 7 23 1 8 24 1 9 25 1 10 26 1
		 11 27 1 12 28 1 13 29 1 14 30 1 15 31 1 16 32 1 17 33 1 32 33 0 18 34 1 33 34 0 19 35 1
		 34 35 0 20 36 1 35 36 0 21 37 1 36 37 0 22 38 1 37 38 0 23 39 1 38 39 0 24 40 1 39 40 0
		 25 41 1 40 41 0 26 42 1 41 42 0 27 43 1 42 43 0 28 44 1 43 44 0 29 45 1 44 45 0 30 46 1
		 45 46 0 31 47 1 46 47 0 47 32 0 32 48 1 33 49 1 48 49 0 34 50 1 49 50 0 35 51 1 50 51 0
		 36 52 1 51 52 0 37 53 1 52 53 0 38 54 1 53 54 0 39 55 1 54 55 0 40 56 1 55 56 0 41 57 1
		 56 57 0 42 58 1 57 58 0 43 59 1 58 59 0 44 60 1 59 60 0 45 61 1 60 61 0 46 62 1 61 62 0
		 47 63 1 62 63 0 63 48 0 0 64 1 1 65 1 64 65 0 2 66 1 65 66 0 3 67 1 66 67 0 4 68 1
		 67 68 0 5 69 1 68 69 0 6 70 1 69 70 0 7 71 1 70 71 0 8 72 1 71 72 0 9 73 1 72 73 0
		 10 74 1 73 74 0 11 75 1 74 75 0 12 76 1 75 76 0 13 77 1 76 77 0 14 78 1 77 78 0 15 79 1
		 78 79 0 79 64 0 80 81 0 81 82 0 82 83 0 83 84 0 84 85 0 85 86 0 86 87 0 87 88 0 88 89 0
		 89 90 0 90 91 0 91 80 0 92 93 0 93 94 0 94 95 0 95 96 0 96 97 0 97 98 0 98 99 0 99 100 0
		 100 101 0 101 102 0;
	setAttr ".ed[166:331]" 102 103 0 103 92 0 80 92 0 81 93 0 82 94 0 83 95 0 84 96 0
		 85 97 0 86 98 0 87 99 0 88 100 0 89 101 0 90 102 0 91 103 0 103 104 1 92 105 1 104 105 0
		 93 106 1 105 106 0 102 107 1 107 104 0 95 108 1 100 109 1 96 110 1 108 110 0 99 111 1
		 111 109 0 94 112 1 101 113 1 112 108 0 109 113 0 106 112 0 113 107 0 97 114 1 110 114 0
		 98 115 1 114 115 0 115 111 0 116 117 0 117 118 0 118 119 0 119 120 0 120 116 0 121 122 0
		 122 123 0 123 124 0 124 125 0 125 121 0 116 121 0 117 122 0 118 123 0 119 124 0 120 125 0
		 121 126 0 122 127 0 126 127 0 123 128 0 127 128 0 124 129 0 128 129 0 125 130 0 129 130 0
		 130 126 0 128 130 1 126 128 1 131 132 0 132 133 0 133 134 0 134 135 0 135 131 0 136 137 0
		 137 138 0 138 139 0 139 140 0 140 136 0 131 136 0 132 137 0 133 138 0 134 139 0 135 140 0
		 136 141 0 137 142 0 141 142 0 138 143 0 142 143 0 139 144 0 143 144 0 140 145 0 144 145 0
		 145 141 0 143 145 1 141 143 1 146 147 0 147 148 0 148 149 0 149 150 0 150 146 0 151 152 0
		 152 153 0 153 154 0 154 155 0 155 151 0 146 151 0 147 152 0 148 153 0 149 154 0 150 155 0
		 151 156 0 152 157 0 156 157 0 153 158 0 157 158 0 154 159 0 158 159 0 155 160 0 159 160 0
		 160 156 0 158 160 1 156 158 1 161 162 0 162 163 0 163 164 0 164 165 0 165 161 0 166 167 0
		 167 168 0 168 169 0 169 170 0 170 166 0 161 166 0 162 167 0 163 168 0 164 169 0 165 170 0
		 166 171 0 167 172 0 171 172 0 168 173 0 172 173 0 169 174 0 173 174 0 170 175 0 174 175 0
		 175 171 0 173 175 1 171 173 1 176 177 0 177 178 0 178 179 0 179 180 0 180 176 0 181 182 0
		 182 183 0 183 184 0 184 185 0 185 181 0 176 181 0 177 182 0 178 183 0 179 184 0 180 185 0
		 181 186 0 182 187 0 186 187 0 183 188 0 187 188 0;
	setAttr ".ed[332:497]" 184 189 0 188 189 0 185 190 0 189 190 0 190 186 0 188 190 1
		 186 188 1 191 192 1 195 194 0 194 235 0 191 236 0 235 193 0 196 195 0 192 237 0 238 198 0
		 198 196 0 197 238 0 191 193 0 192 197 0 199 200 1 203 202 0 202 239 0 199 240 0 239 201 0
		 204 203 0 200 241 0 242 206 0 206 204 0 205 242 0 199 201 0 200 205 0 207 208 1 211 210 0
		 210 243 0 207 244 0 243 209 0 212 211 0 208 245 0 246 214 0 214 212 0 213 246 0 207 209 0
		 208 213 0 215 216 1 219 218 0 218 247 0 215 248 0 247 217 0 220 219 0 216 249 0 250 222 0
		 222 220 0 221 250 0 215 217 0 216 221 0 223 224 1 227 226 0 226 231 0 223 232 0 231 225 0
		 228 227 0 224 233 0 234 230 0 230 228 0 229 234 0 223 225 0 224 229 0 192 199 0 197 201 0
		 200 207 0 205 209 0 208 215 0 213 217 0 216 223 0 221 225 0 193 229 0 191 224 0 232 227 0
		 231 232 1 233 228 0 232 233 1 233 234 1 236 195 0 235 236 1 237 196 0 236 237 1 237 238 1
		 240 203 0 239 240 1 241 204 0 240 241 1 241 242 1 244 211 0 243 244 1 245 212 0 244 245 1
		 245 246 1 248 219 0 247 248 1 249 220 0 248 249 1 249 250 1 192 207 1 191 208 1 215 224 1
		 251 252 0 253 251 0 254 253 0 255 254 0 256 255 0 257 256 0 258 257 0 259 258 0 260 259 0
		 261 260 0 262 261 0 263 262 0 264 263 0 265 264 0 266 265 0 267 266 0 268 267 0 269 268 0
		 270 269 0 271 270 0 272 271 0 273 272 0 274 273 0 275 274 0 276 275 0 277 276 0 278 277 0
		 279 278 0 280 279 0 252 280 0 251 281 1 252 282 1 281 282 0 253 283 1 283 281 0 254 284 1
		 284 283 0 255 285 1 285 284 0 256 286 1 286 285 0 257 287 1 287 286 0 258 288 1 288 287 0
		 259 289 1 289 288 0 260 290 1 290 289 0 261 291 1 291 290 0 262 292 1 292 291 0 263 293 1
		 293 292 0 264 294 1 294 293 0 265 295 1 295 294 0 266 296 1 296 295 0;
	setAttr ".ed[498:663]" 267 297 1 297 296 0 268 298 1 298 297 0 269 299 1 299 298 0
		 270 300 1 300 299 0 271 301 1 301 300 0 272 302 1 302 301 0 273 303 1 303 302 0 274 304 1
		 304 303 0 275 305 1 305 304 0 276 306 1 306 305 0 277 307 1 307 306 0 278 308 1 308 307 0
		 279 309 1 309 308 0 280 310 1 310 309 0 282 310 0 281 311 1 282 312 1 283 313 1 284 314 1
		 285 315 1 286 316 1 287 317 1 288 318 1 289 319 1 290 320 1 291 321 1 292 322 1 293 323 1
		 294 324 1 295 325 1 296 326 1 297 327 1 298 328 1 299 329 1 300 330 1 301 331 1 302 332 1
		 303 333 1 304 334 1 305 335 1 306 336 1 307 337 1 308 338 1 309 339 1 310 340 1 311 312 0
		 312 340 0 313 311 0 314 313 0 315 314 0 316 315 0 317 316 0 318 317 0 319 318 0 320 319 0
		 321 320 0 322 321 0 323 322 0 324 323 0 325 324 0 326 325 0 327 326 0 328 327 0 329 328 0
		 330 329 0 331 330 0 332 331 0 333 332 0 334 333 0 335 334 0 336 335 0 337 336 0 338 337 0
		 339 338 0 340 339 0 67 75 1 66 76 1 65 77 1 64 78 1 70 72 1 69 73 1 68 74 1 51 59 1
		 50 60 1 49 61 1 48 62 1 52 58 1 53 57 1 54 56 1 341 342 0 342 343 0 343 344 0 344 345 0
		 345 346 0 346 347 0 347 348 0 348 349 0 349 350 0 350 351 0 351 352 0 352 353 0 353 354 0
		 354 355 0 355 356 0 356 341 0 357 358 0 358 359 0 359 360 0 360 361 0 361 362 0 362 363 0
		 363 364 0 364 365 0 365 366 0 366 367 0 367 368 0 368 369 0 369 370 0 370 371 0 371 372 0
		 372 357 0 341 357 1 342 358 1 343 359 1 344 360 1 345 361 1 346 362 1 347 363 1 348 364 1
		 349 365 1 350 366 1 351 367 1 352 368 1 353 369 1 354 370 1 355 371 1 356 372 1 357 373 1
		 358 374 1 373 374 0 359 375 1 374 375 0 360 376 1 375 376 0 361 377 1 376 377 0 362 378 1
		 377 378 0 363 379 1 378 379 0 364 380 1 379 380 0;
	setAttr ".ed[664:829]" 365 381 1 380 381 0 366 382 1 381 382 0 367 383 1 382 383 0
		 368 384 1 383 384 0 369 385 1 384 385 0 370 386 1 385 386 0 371 387 1 386 387 0 372 388 1
		 387 388 0 388 373 0 373 389 1 374 390 1 389 390 0 375 391 1 390 391 0 376 392 1 391 392 0
		 377 393 1 392 393 0 378 394 1 393 394 0 379 395 1 394 395 0 380 396 1 395 396 0 381 397 1
		 396 397 0 382 398 1 397 398 0 383 399 1 398 399 0 384 400 1 399 400 0 385 401 1 400 401 0
		 386 402 1 401 402 0 387 403 1 402 403 0 388 404 1 403 404 0 404 389 0 341 405 1 342 406 1
		 405 406 0 343 407 1 406 407 0 344 408 1 407 408 0 345 409 1 408 409 0 346 410 1 409 410 0
		 347 411 1 410 411 0 348 412 1 411 412 0 349 413 1 412 413 0 350 414 1 413 414 0 351 415 1
		 414 415 0 352 416 1 415 416 0 353 417 1 416 417 0 354 418 1 417 418 0 355 419 1 418 419 0
		 356 420 1 419 420 0 420 405 0 421 422 0 422 423 0 423 424 0 424 425 0 425 426 0 426 427 0
		 427 428 0 428 429 0 429 430 0 430 431 0 431 432 0 432 421 0 433 434 0 434 435 0 435 436 0
		 436 437 0 437 438 0 438 439 0 439 440 0 440 441 0 441 442 0 442 443 0 443 444 0 444 433 0
		 421 433 0 422 434 0 423 435 0 424 436 0 425 437 0 426 438 0 427 439 0 428 440 0 429 441 0
		 430 442 0 431 443 0 432 444 0 444 445 1 433 446 1 445 446 0 434 447 1 446 447 0 443 448 1
		 448 445 0 436 449 1 441 450 1 437 451 1 449 451 0 440 452 1 452 450 0 435 453 1 442 454 1
		 453 449 0 450 454 0 447 453 0 454 448 0 438 455 1 451 455 0 439 456 1 455 456 0 456 452 0
		 457 458 0 458 459 0 459 460 0 460 461 0 461 457 0 462 463 0 463 464 0 464 465 0 465 466 0
		 466 462 0 457 462 0 458 463 0 459 464 0 460 465 0 461 466 0 462 467 0 463 468 0 467 468 0
		 464 469 0 468 469 0 465 470 0 469 470 0 466 471 0 470 471 0 471 467 0;
	setAttr ".ed[830:995]" 469 471 1 467 469 1 472 473 0 473 474 0 474 475 0 475 476 0
		 476 472 0 477 478 0 478 479 0 479 480 0 480 481 0 481 477 0 472 477 0 473 478 0 474 479 0
		 475 480 0 476 481 0 477 482 0 478 483 0 482 483 0 479 484 0 483 484 0 480 485 0 484 485 0
		 481 486 0 485 486 0 486 482 0 484 486 1 482 484 1 487 488 0 488 489 0 489 490 0 490 491 0
		 491 487 0 492 493 0 493 494 0 494 495 0 495 496 0 496 492 0 487 492 0 488 493 0 489 494 0
		 490 495 0 491 496 0 492 497 0 493 498 0 497 498 0 494 499 0 498 499 0 495 500 0 499 500 0
		 496 501 0 500 501 0 501 497 0 499 501 1 497 499 1 502 503 0 503 504 0 504 505 0 505 506 0
		 506 502 0 507 508 0 508 509 0 509 510 0 510 511 0 511 507 0 502 507 0 503 508 0 504 509 0
		 505 510 0 506 511 0 507 512 0 508 513 0 512 513 0 509 514 0 513 514 0 510 515 0 514 515 0
		 511 516 0 515 516 0 516 512 0 514 516 1 512 514 1 517 518 0 518 519 0 519 520 0 520 521 0
		 521 517 0 522 523 0 523 524 0 524 525 0 525 526 0 526 522 0 517 522 0 518 523 0 519 524 0
		 520 525 0 521 526 0 522 527 0 523 528 0 527 528 0 524 529 0 528 529 0 525 530 0 529 530 0
		 526 531 0 530 531 0 531 527 0 529 531 1 527 529 1 532 533 1 536 535 0 535 576 0 532 577 0
		 576 534 0 537 536 0 533 578 0 579 539 0 539 537 0 538 579 0 532 534 0 533 538 0 540 541 1
		 544 543 0 543 580 0 540 581 0 580 542 0 545 544 0 541 582 0 583 547 0 547 545 0 546 583 0
		 540 542 0 541 546 0 548 549 1 552 551 0 551 584 0 548 585 0 584 550 0 553 552 0 549 586 0
		 587 555 0 555 553 0 554 587 0 548 550 0 549 554 0 556 557 1 560 559 0 559 588 0 556 589 0
		 588 558 0 561 560 0 557 590 0 591 563 0 563 561 0 562 591 0 556 558 0 557 562 0 564 565 1
		 568 567 0 567 572 0 564 573 0 572 566 0 569 568 0 565 574 0 575 571 0;
	setAttr ".ed[996:1161]" 571 569 0 570 575 0 564 566 0 565 570 0 533 540 0 538 542 0
		 541 548 0 546 550 0 549 556 0 554 558 0 557 564 0 562 566 0 534 570 0 532 565 0 573 568 0
		 572 573 1 574 569 0 573 574 1 574 575 1 577 536 0 576 577 1 578 537 0 577 578 1 578 579 1
		 581 544 0 580 581 1 582 545 0 581 582 1 582 583 1 585 552 0 584 585 1 586 553 0 585 586 1
		 586 587 1 589 560 0 588 589 1 590 561 0 589 590 1 590 591 1 533 548 1 532 549 1 556 565 1
		 592 593 0 594 592 0 595 594 0 596 595 0 597 596 0 598 597 0 599 598 0 600 599 0 601 600 0
		 602 601 0 603 602 0 604 603 0 605 604 0 606 605 0 607 606 0 608 607 0 609 608 0 610 609 0
		 611 610 0 612 611 0 613 612 0 614 613 0 615 614 0 616 615 0 617 616 0 618 617 0 619 618 0
		 620 619 0 621 620 0 593 621 0 592 622 1 593 623 1 622 623 0 594 624 1 624 622 0 595 625 1
		 625 624 0 596 626 1 626 625 0 597 627 1 627 626 0 598 628 1 628 627 0 599 629 1 629 628 0
		 600 630 1 630 629 0 601 631 1 631 630 0 602 632 1 632 631 0 603 633 1 633 632 0 604 634 1
		 634 633 0 605 635 1 635 634 0 606 636 1 636 635 0 607 637 1 637 636 0 608 638 1 638 637 0
		 609 639 1 639 638 0 610 640 1 640 639 0 611 641 1 641 640 0 612 642 1 642 641 0 613 643 1
		 643 642 0 614 644 1 644 643 0 615 645 1 645 644 0 616 646 1 646 645 0 617 647 1 647 646 0
		 618 648 1 648 647 0 619 649 1 649 648 0 620 650 1 650 649 0 621 651 1 651 650 0 623 651 0
		 622 652 1 623 653 1 624 654 1 625 655 1 626 656 1 627 657 1 628 658 1 629 659 1 630 660 1
		 631 661 1 632 662 1 633 663 1 634 664 1 635 665 1 636 666 1 637 667 1 638 668 1 639 669 1
		 640 670 1 641 671 1 642 672 1 643 673 1 644 674 1 645 675 1 646 676 1 647 677 1 648 678 1
		 649 679 1 650 680 1 651 681 1 652 653 0 653 681 0 654 652 0 655 654 0;
	setAttr ".ed[1162:1327]" 656 655 0 657 656 0 658 657 0 659 658 0 660 659 0 661 660 0
		 662 661 0 663 662 0 664 663 0 665 664 0 666 665 0 667 666 0 668 667 0 669 668 0 670 669 0
		 671 670 0 672 671 0 673 672 0 674 673 0 675 674 0 676 675 0 677 676 0 678 677 0 679 678 0
		 680 679 0 681 680 0 408 416 1 407 417 1 406 418 1 405 419 1 411 413 1 410 414 1 409 415 1
		 392 400 1 391 401 1 390 402 1 389 403 1 393 399 1 394 398 1 395 397 1 682 683 0 683 684 0
		 684 685 0 685 686 0 686 687 0 687 688 0 688 689 0 689 690 0 690 691 0 691 692 0 692 693 0
		 693 694 0 694 695 0 695 696 0 696 697 0 697 682 0 698 699 0 699 700 0 700 701 0 701 702 0
		 702 703 0 703 704 0 704 705 0 705 706 0 706 707 0 707 708 0 708 709 0 709 710 0 710 711 0
		 711 712 0 712 713 0 713 698 0 682 698 1 683 699 1 684 700 1 685 701 1 686 702 1 687 703 1
		 688 704 1 689 705 1 690 706 1 691 707 1 692 708 1 693 709 1 694 710 1 695 711 1 696 712 1
		 697 713 1 698 714 1 699 715 1 714 715 0 700 716 1 715 716 0 701 717 1 716 717 0 702 718 1
		 717 718 0 703 719 1 718 719 0 704 720 1 719 720 0 705 721 1 720 721 0 706 722 1 721 722 0
		 707 723 1 722 723 0 708 724 1 723 724 0 709 725 1 724 725 0 710 726 1 725 726 0 711 727 1
		 726 727 0 712 728 1 727 728 0 713 729 1 728 729 0 729 714 0 714 730 1 715 731 1 730 731 0
		 716 732 1 731 732 0 717 733 1 732 733 0 718 734 1 733 734 0 719 735 1 734 735 0 720 736 1
		 735 736 0 721 737 1 736 737 0 722 738 1 737 738 0 723 739 1 738 739 0 724 740 1 739 740 0
		 725 741 1 740 741 0 726 742 1 741 742 0 727 743 1 742 743 0 728 744 1 743 744 0 729 745 1
		 744 745 0 745 730 0 682 746 1 683 747 1 746 747 0 684 748 1 747 748 0 685 749 1 748 749 0
		 686 750 1 749 750 0 687 751 1 750 751 0 688 752 1 751 752 0 689 753 1;
	setAttr ".ed[1328:1493]" 752 753 0 690 754 1 753 754 0 691 755 1 754 755 0 692 756 1
		 755 756 0 693 757 1 756 757 0 694 758 1 757 758 0 695 759 1 758 759 0 696 760 1 759 760 0
		 697 761 1 760 761 0 761 746 0 762 763 0 763 764 0 764 765 0 765 766 0 766 767 0 767 768 0
		 768 769 0 769 770 0 770 771 0 771 772 0 772 773 0 773 762 0 774 775 0 775 776 0 776 777 0
		 777 778 0 778 779 0 779 780 0 780 781 0 781 782 0 782 783 0 783 784 0 784 785 0 785 774 0
		 762 774 0 763 775 0 764 776 0 765 777 0 766 778 0 767 779 0 768 780 0 769 781 0 770 782 0
		 771 783 0 772 784 0 773 785 0 785 786 1 774 787 1 786 787 0 775 788 1 787 788 0 784 789 1
		 789 786 0 777 790 1 782 791 1 778 792 1 790 792 0 781 793 1 793 791 0 776 794 1 783 795 1
		 794 790 0 791 795 0 788 794 0 795 789 0 779 796 1 792 796 0 780 797 1 796 797 0 797 793 0
		 798 799 0 799 800 0 800 801 0 801 802 0 802 798 0 803 804 0 804 805 0 805 806 0 806 807 0
		 807 803 0 798 803 0 799 804 0 800 805 0 801 806 0 802 807 0 803 808 0 804 809 0 808 809 0
		 805 810 0 809 810 0 806 811 0 810 811 0 807 812 0 811 812 0 812 808 0 810 812 1 808 810 1
		 813 814 0 814 815 0 815 816 0 816 817 0 817 813 0 818 819 0 819 820 0 820 821 0 821 822 0
		 822 818 0 813 818 0 814 819 0 815 820 0 816 821 0 817 822 0 818 823 0 819 824 0 823 824 0
		 820 825 0 824 825 0 821 826 0 825 826 0 822 827 0 826 827 0 827 823 0 825 827 1 823 825 1
		 828 829 0 829 830 0 830 831 0 831 832 0 832 828 0 833 834 0 834 835 0 835 836 0 836 837 0
		 837 833 0 828 833 0 829 834 0 830 835 0 831 836 0 832 837 0 833 838 0 834 839 0 838 839 0
		 835 840 0 839 840 0 836 841 0 840 841 0 837 842 0 841 842 0 842 838 0 840 842 1 838 840 1
		 843 844 0 844 845 0 845 846 0 846 847 0 847 843 0 848 849 0 849 850 0;
	setAttr ".ed[1494:1659]" 850 851 0 851 852 0 852 848 0 843 848 0 844 849 0 845 850 0
		 846 851 0 847 852 0 848 853 0 849 854 0 853 854 0 850 855 0 854 855 0 851 856 0 855 856 0
		 852 857 0 856 857 0 857 853 0 855 857 1 853 855 1 858 859 0 859 860 0 860 861 0 861 862 0
		 862 858 0 863 864 0 864 865 0 865 866 0 866 867 0 867 863 0 858 863 0 859 864 0 860 865 0
		 861 866 0 862 867 0 863 868 0 864 869 0 868 869 0 865 870 0 869 870 0 866 871 0 870 871 0
		 867 872 0 871 872 0 872 868 0 870 872 1 868 870 1 873 874 1 877 876 0 876 917 0 873 918 0
		 917 875 0 878 877 0 874 919 0 920 880 0 880 878 0 879 920 0 873 875 0 874 879 0 881 882 1
		 885 884 0 884 921 0 881 922 0 921 883 0 886 885 0 882 923 0 924 888 0 888 886 0 887 924 0
		 881 883 0 882 887 0 889 890 1 893 892 0 892 925 0 889 926 0 925 891 0 894 893 0 890 927 0
		 928 896 0 896 894 0 895 928 0 889 891 0 890 895 0 897 898 1 901 900 0 900 929 0 897 930 0
		 929 899 0 902 901 0 898 931 0 932 904 0 904 902 0 903 932 0 897 899 0 898 903 0 905 906 1
		 909 908 0 908 913 0 905 914 0 913 907 0 910 909 0 906 915 0 916 912 0 912 910 0 911 916 0
		 905 907 0 906 911 0 874 881 0 879 883 0 882 889 0 887 891 0 890 897 0 895 899 0 898 905 0
		 903 907 0 875 911 0 873 906 0 914 909 0 913 914 1 915 910 0 914 915 1 915 916 1 918 877 0
		 917 918 1 919 878 0 918 919 1 919 920 1 922 885 0 921 922 1 923 886 0 922 923 1 923 924 1
		 926 893 0 925 926 1 927 894 0 926 927 1 927 928 1 930 901 0 929 930 1 931 902 0 930 931 1
		 931 932 1 874 889 1 873 890 1 897 906 1 933 934 0 935 933 0 936 935 0 937 936 0 938 937 0
		 939 938 0 940 939 0 941 940 0 942 941 0 943 942 0 944 943 0 945 944 0 946 945 0 947 946 0
		 948 947 0 949 948 0 950 949 0 951 950 0 952 951 0 953 952 0 954 953 0;
	setAttr ".ed[1660:1825]" 955 954 0 956 955 0 957 956 0 958 957 0 959 958 0 960 959 0
		 961 960 0 962 961 0 934 962 0 933 963 1 934 964 1 963 964 0 935 965 1 965 963 0 936 966 1
		 966 965 0 937 967 1 967 966 0 938 968 1 968 967 0 939 969 1 969 968 0 940 970 1 970 969 0
		 941 971 1 971 970 0 942 972 1 972 971 0 943 973 1 973 972 0 944 974 1 974 973 0 945 975 1
		 975 974 0 946 976 1 976 975 0 947 977 1 977 976 0 948 978 1 978 977 0 949 979 1 979 978 0
		 950 980 1 980 979 0 951 981 1 981 980 0 952 982 1 982 981 0 953 983 1 983 982 0 954 984 1
		 984 983 0 955 985 1 985 984 0 956 986 1 986 985 0 957 987 1 987 986 0 958 988 1 988 987 0
		 959 989 1 989 988 0 960 990 1 990 989 0 961 991 1 991 990 0 962 992 1 992 991 0 964 992 0
		 963 993 1 964 994 1 965 995 1 966 996 1 967 997 1 968 998 1 969 999 1 970 1000 1
		 971 1001 1 972 1002 1 973 1003 1 974 1004 1 975 1005 1 976 1006 1 977 1007 1 978 1008 1
		 979 1009 1 980 1010 1 981 1011 1 982 1012 1 983 1013 1 984 1014 1 985 1015 1 986 1016 1
		 987 1017 1 988 1018 1 989 1019 1 990 1020 1 991 1021 1 992 1022 1 993 994 0 994 1022 0
		 995 993 0 996 995 0 997 996 0 998 997 0 999 998 0 1000 999 0 1001 1000 0 1002 1001 0
		 1003 1002 0 1004 1003 0 1005 1004 0 1006 1005 0 1007 1006 0 1008 1007 0 1009 1008 0
		 1010 1009 0 1011 1010 0 1012 1011 0 1013 1012 0 1014 1013 0 1015 1014 0 1016 1015 0
		 1017 1016 0 1018 1017 0 1019 1018 0 1020 1019 0 1021 1020 0 1022 1021 0 749 757 1
		 748 758 1 747 759 1 746 760 1 752 754 1 751 755 1 750 756 1 733 741 1 732 742 1 731 743 1
		 730 744 1 734 740 1 735 739 1 736 738 1 1023 1024 0 1024 1025 0 1025 1026 0 1026 1027 0
		 1027 1028 0 1028 1029 0 1029 1030 0 1030 1031 0 1031 1032 0 1032 1033 0 1033 1034 0
		 1034 1035 0 1035 1036 0 1036 1037 0 1037 1038 0 1038 1023 0 1039 1040 0 1040 1041 0
		 1041 1042 0 1042 1043 0 1043 1044 0 1044 1045 0 1045 1046 0;
	setAttr ".ed[1826:1991]" 1046 1047 0 1047 1048 0 1048 1049 0 1049 1050 0 1050 1051 0
		 1051 1052 0 1052 1053 0 1053 1054 0 1054 1039 0 1023 1039 1 1024 1040 1 1025 1041 1
		 1026 1042 1 1027 1043 1 1028 1044 1 1029 1045 1 1030 1046 1 1031 1047 1 1032 1048 1
		 1033 1049 1 1034 1050 1 1035 1051 1 1036 1052 1 1037 1053 1 1038 1054 1 1039 1055 1
		 1040 1056 1 1055 1056 0 1041 1057 1 1056 1057 0 1042 1058 1 1057 1058 0 1043 1059 1
		 1058 1059 0 1044 1060 1 1059 1060 0 1045 1061 1 1060 1061 0 1046 1062 1 1061 1062 0
		 1047 1063 1 1062 1063 0 1048 1064 1 1063 1064 0 1049 1065 1 1064 1065 0 1050 1066 1
		 1065 1066 0 1051 1067 1 1066 1067 0 1052 1068 1 1067 1068 0 1053 1069 1 1068 1069 0
		 1054 1070 1 1069 1070 0 1070 1055 0 1055 1071 1 1056 1072 1 1071 1072 0 1057 1073 1
		 1072 1073 0 1058 1074 1 1073 1074 0 1059 1075 1 1074 1075 0 1060 1076 1 1075 1076 0
		 1061 1077 1 1076 1077 0 1062 1078 1 1077 1078 0 1063 1079 1 1078 1079 0 1064 1080 1
		 1079 1080 0 1065 1081 1 1080 1081 0 1066 1082 1 1081 1082 0 1067 1083 1 1082 1083 0
		 1068 1084 1 1083 1084 0 1069 1085 1 1084 1085 0 1070 1086 1 1085 1086 0 1086 1071 0
		 1023 1087 1 1024 1088 1 1087 1088 0 1025 1089 1 1088 1089 0 1026 1090 1 1089 1090 0
		 1027 1091 1 1090 1091 0 1028 1092 1 1091 1092 0 1029 1093 1 1092 1093 0 1030 1094 1
		 1093 1094 0 1031 1095 1 1094 1095 0 1032 1096 1 1095 1096 0 1033 1097 1 1096 1097 0
		 1034 1098 1 1097 1098 0 1035 1099 1 1098 1099 0 1036 1100 1 1099 1100 0 1037 1101 1
		 1100 1101 0 1038 1102 1 1101 1102 0 1102 1087 0 1103 1104 0 1104 1105 0 1105 1106 0
		 1106 1107 0 1107 1108 0 1108 1109 0 1109 1110 0 1110 1111 0 1111 1112 0 1112 1113 0
		 1113 1114 0 1114 1103 0 1115 1116 0 1116 1117 0 1117 1118 0 1118 1119 0 1119 1120 0
		 1120 1121 0 1121 1122 0 1122 1123 0 1123 1124 0 1124 1125 0 1125 1126 0 1126 1115 0
		 1103 1115 0 1104 1116 0 1105 1117 0 1106 1118 0 1107 1119 0 1108 1120 0 1109 1121 0
		 1110 1122 0 1111 1123 0 1112 1124 0 1113 1125 0 1114 1126 0 1126 1127 1 1115 1128 1
		 1127 1128 0 1116 1129 1 1128 1129 0 1125 1130 1 1130 1127 0 1118 1131 1 1123 1132 1;
	setAttr ".ed[1992:2157]" 1119 1133 1 1131 1133 0 1122 1134 1 1134 1132 0 1117 1135 1
		 1124 1136 1 1135 1131 0 1132 1136 0 1129 1135 0 1136 1130 0 1120 1137 1 1133 1137 0
		 1121 1138 1 1137 1138 0 1138 1134 0 1139 1140 0 1140 1141 0 1141 1142 0 1142 1143 0
		 1143 1139 0 1144 1145 0 1145 1146 0 1146 1147 0 1147 1148 0 1148 1144 0 1139 1144 0
		 1140 1145 0 1141 1146 0 1142 1147 0 1143 1148 0 1144 1149 0 1145 1150 0 1149 1150 0
		 1146 1151 0 1150 1151 0 1147 1152 0 1151 1152 0 1148 1153 0 1152 1153 0 1153 1149 0
		 1151 1153 1 1149 1151 1 1154 1155 0 1155 1156 0 1156 1157 0 1157 1158 0 1158 1154 0
		 1159 1160 0 1160 1161 0 1161 1162 0 1162 1163 0 1163 1159 0 1154 1159 0 1155 1160 0
		 1156 1161 0 1157 1162 0 1158 1163 0 1159 1164 0 1160 1165 0 1164 1165 0 1161 1166 0
		 1165 1166 0 1162 1167 0 1166 1167 0 1163 1168 0 1167 1168 0 1168 1164 0 1166 1168 1
		 1164 1166 1 1169 1170 0 1170 1171 0 1171 1172 0 1172 1173 0 1173 1169 0 1174 1175 0
		 1175 1176 0 1176 1177 0 1177 1178 0 1178 1174 0 1169 1174 0 1170 1175 0 1171 1176 0
		 1172 1177 0 1173 1178 0 1174 1179 0 1175 1180 0 1179 1180 0 1176 1181 0 1180 1181 0
		 1177 1182 0 1181 1182 0 1178 1183 0 1182 1183 0 1183 1179 0 1181 1183 1 1179 1181 1
		 1184 1185 0 1185 1186 0 1186 1187 0 1187 1188 0 1188 1184 0 1189 1190 0 1190 1191 0
		 1191 1192 0 1192 1193 0 1193 1189 0 1184 1189 0 1185 1190 0 1186 1191 0 1187 1192 0
		 1188 1193 0 1189 1194 0 1190 1195 0 1194 1195 0 1191 1196 0 1195 1196 0 1192 1197 0
		 1196 1197 0 1193 1198 0 1197 1198 0 1198 1194 0 1196 1198 1 1194 1196 1 1199 1200 0
		 1200 1201 0 1201 1202 0 1202 1203 0 1203 1199 0 1204 1205 0 1205 1206 0 1206 1207 0
		 1207 1208 0 1208 1204 0 1199 1204 0 1200 1205 0 1201 1206 0 1202 1207 0 1203 1208 0
		 1204 1209 0 1205 1210 0 1209 1210 0 1206 1211 0 1210 1211 0 1207 1212 0 1211 1212 0
		 1208 1213 0 1212 1213 0 1213 1209 0 1211 1213 1 1209 1211 1 1214 1215 1 1218 1217 0
		 1217 1258 0 1214 1259 0 1258 1216 0 1219 1218 0 1215 1260 0 1261 1221 0 1221 1219 0
		 1220 1261 0 1214 1216 0 1215 1220 0 1222 1223 1 1226 1225 0 1225 1262 0 1222 1263 0;
	setAttr ".ed[2158:2323]" 1262 1224 0 1227 1226 0 1223 1264 0 1265 1229 0 1229 1227 0
		 1228 1265 0 1222 1224 0 1223 1228 0 1230 1231 1 1234 1233 0 1233 1266 0 1230 1267 0
		 1266 1232 0 1235 1234 0 1231 1268 0 1269 1237 0 1237 1235 0 1236 1269 0 1230 1232 0
		 1231 1236 0 1238 1239 1 1242 1241 0 1241 1270 0 1238 1271 0 1270 1240 0 1243 1242 0
		 1239 1272 0 1273 1245 0 1245 1243 0 1244 1273 0 1238 1240 0 1239 1244 0 1246 1247 1
		 1250 1249 0 1249 1254 0 1246 1255 0 1254 1248 0 1251 1250 0 1247 1256 0 1257 1253 0
		 1253 1251 0 1252 1257 0 1246 1248 0 1247 1252 0 1215 1222 0 1220 1224 0 1223 1230 0
		 1228 1232 0 1231 1238 0 1236 1240 0 1239 1246 0 1244 1248 0 1216 1252 0 1214 1247 0
		 1255 1250 0 1254 1255 1 1256 1251 0 1255 1256 1 1256 1257 1 1259 1218 0 1258 1259 1
		 1260 1219 0 1259 1260 1 1260 1261 1 1263 1226 0 1262 1263 1 1264 1227 0 1263 1264 1
		 1264 1265 1 1267 1234 0 1266 1267 1 1268 1235 0 1267 1268 1 1268 1269 1 1271 1242 0
		 1270 1271 1 1272 1243 0 1271 1272 1 1272 1273 1 1215 1230 1 1214 1231 1 1238 1247 1
		 1274 1275 0 1276 1274 0 1277 1276 0 1278 1277 0 1279 1278 0 1280 1279 0 1281 1280 0
		 1282 1281 0 1283 1282 0 1284 1283 0 1285 1284 0 1286 1285 0 1287 1286 0 1288 1287 0
		 1289 1288 0 1290 1289 0 1291 1290 0 1292 1291 0 1293 1292 0 1294 1293 0 1295 1294 0
		 1296 1295 0 1297 1296 0 1298 1297 0 1299 1298 0 1300 1299 0 1301 1300 0 1302 1301 0
		 1303 1302 0 1275 1303 0 1274 1304 1 1275 1305 1 1304 1305 0 1276 1306 1 1306 1304 0
		 1277 1307 1 1307 1306 0 1278 1308 1 1308 1307 0 1279 1309 1 1309 1308 0 1280 1310 1
		 1310 1309 0 1281 1311 1 1311 1310 0 1282 1312 1 1312 1311 0 1283 1313 1 1313 1312 0
		 1284 1314 1 1314 1313 0 1285 1315 1 1315 1314 0 1286 1316 1 1316 1315 0 1287 1317 1
		 1317 1316 0 1288 1318 1 1318 1317 0 1289 1319 1 1319 1318 0 1290 1320 1 1320 1319 0
		 1291 1321 1 1321 1320 0 1292 1322 1 1322 1321 0 1293 1323 1 1323 1322 0 1294 1324 1
		 1324 1323 0 1295 1325 1 1325 1324 0 1296 1326 1 1326 1325 0 1297 1327 1 1327 1326 0
		 1298 1328 1 1328 1327 0 1299 1329 1 1329 1328 0 1300 1330 1 1330 1329 0 1301 1331 1;
	setAttr ".ed[2324:2451]" 1331 1330 0 1302 1332 1 1332 1331 0 1303 1333 1 1333 1332 0
		 1305 1333 0 1304 1334 1 1305 1335 1 1306 1336 1 1307 1337 1 1308 1338 1 1309 1339 1
		 1310 1340 1 1311 1341 1 1312 1342 1 1313 1343 1 1314 1344 1 1315 1345 1 1316 1346 1
		 1317 1347 1 1318 1348 1 1319 1349 1 1320 1350 1 1321 1351 1 1322 1352 1 1323 1353 1
		 1324 1354 1 1325 1355 1 1326 1356 1 1327 1357 1 1328 1358 1 1329 1359 1 1330 1360 1
		 1331 1361 1 1332 1362 1 1333 1363 1 1334 1335 0 1335 1363 0 1336 1334 0 1337 1336 0
		 1338 1337 0 1339 1338 0 1340 1339 0 1341 1340 0 1342 1341 0 1343 1342 0 1344 1343 0
		 1345 1344 0 1346 1345 0 1347 1346 0 1348 1347 0 1349 1348 0 1350 1349 0 1351 1350 0
		 1352 1351 0 1353 1352 0 1354 1353 0 1355 1354 0 1356 1355 0 1357 1356 0 1358 1357 0
		 1359 1358 0 1360 1359 0 1361 1360 0 1362 1361 0 1363 1362 0 1090 1098 1 1089 1099 1
		 1088 1100 1 1087 1101 1 1093 1095 1 1092 1096 1 1091 1097 1 1074 1082 1 1073 1083 1
		 1072 1084 1 1071 1085 1 1075 1081 1 1076 1080 1 1077 1079 1 1136 1364 1 1364 1131 1
		 1130 1364 1 1127 1364 1 1128 1364 1 1129 1364 1 1135 1364 1 1133 1364 1 1137 1364 1
		 1138 1364 1 1134 1364 1 1132 1364 1 454 1365 1 1365 449 1 450 1365 1 452 1365 1 456 1365 1
		 455 1365 1 451 1365 1 453 1365 1 447 1365 1 446 1365 1 445 1365 1 448 1365 1 113 1366 1
		 1366 108 1 109 1366 1 111 1366 1 115 1366 1 114 1366 1 110 1366 1 112 1366 1 106 1366 1
		 105 1366 1 104 1366 1 107 1366 1 795 1367 1 1367 790 1 789 1367 1 786 1367 1 787 1367 1
		 788 1367 1 794 1367 1 792 1367 1 796 1367 1 797 1367 1 793 1367 1 791 1367 1;
	setAttr -s 1120 -ch 4356 ".fc";
	setAttr ".fc[0:499]" -type "polyFaces" 
		f 4 32 16 -34 -1
		f 4 33 17 -35 -2
		f 4 34 18 -36 -3
		f 4 35 19 -37 -4
		f 4 36 20 -38 -5
		f 4 37 21 -39 -6
		f 4 38 22 -40 -7
		f 4 39 23 -41 -8
		f 4 40 24 -42 -9
		f 4 41 25 -43 -10
		f 4 42 26 -44 -11
		f 4 43 27 -45 -12
		f 4 44 28 -46 -13
		f 4 45 29 -47 -14
		f 4 46 30 -48 -15
		f 4 47 31 -33 -16
		f 3 126 128 -592
		f 3 -111 -598 -112
		f 4 48 50 -50 -17
		f 4 49 52 -52 -18
		f 4 51 54 -54 -19
		f 4 53 56 -56 -20
		f 4 55 58 -58 -21
		f 4 57 60 -60 -22
		f 4 59 62 -62 -23
		f 4 61 64 -64 -24
		f 4 63 66 -66 -25
		f 4 65 68 -68 -26
		f 4 67 70 -70 -27
		f 4 69 72 -72 -28
		f 4 71 74 -74 -29
		f 4 73 76 -76 -30
		f 4 75 78 -78 -31
		f 4 77 79 -49 -32
		f 4 80 82 -82 -51
		f 4 81 84 -84 -53
		f 4 83 86 -86 -55
		f 4 85 88 -88 -57
		f 4 87 90 -90 -59
		f 4 89 92 -92 -61
		f 4 91 94 -94 -63
		f 4 93 96 -96 -65
		f 4 95 98 -98 -67
		f 4 97 100 -100 -69
		f 4 99 102 -102 -71
		f 4 101 104 -104 -73
		f 4 103 106 -106 -75
		f 4 105 108 -108 -77
		f 4 107 110 -110 -79
		f 4 109 111 -81 -80
		f 4 113 -115 -113 0
		f 4 115 -117 -114 1
		f 4 117 -119 -116 2
		f 4 119 -121 -118 3
		f 4 121 -123 -120 4
		f 4 123 -125 -122 5
		f 4 125 -127 -124 6
		f 4 127 -129 -126 7
		f 4 129 -131 -128 8
		f 4 131 -133 -130 9
		f 4 133 -135 -132 10
		f 4 135 -137 -134 11
		f 4 137 -139 -136 12
		f 4 139 -141 -138 13
		f 4 141 -143 -140 14
		f 4 112 -144 -142 15
		f 4 168 156 -170 -145
		f 4 169 157 -171 -146
		f 4 170 158 -172 -147
		f 4 171 159 -173 -148
		f 4 172 160 -174 -149
		f 4 173 161 -175 -150
		f 4 174 162 -176 -151
		f 4 175 163 -177 -152
		f 4 176 164 -178 -153
		f 4 177 165 -179 -154
		f 4 178 166 -180 -155
		f 4 179 167 -169 -156
		f 3 2435 2429 -196
		f 4 180 182 -182 -168
		f 4 181 184 -184 -157
		f 4 185 186 -181 -167
		f 4 187 190 -190 -160
		f 4 191 192 -189 -164
		f 4 193 195 -188 -159
		f 4 188 196 -195 -165
		f 4 183 197 -194 -158
		f 4 194 198 -186 -166
		f 4 189 200 -200 -161
		f 4 199 202 -202 -162
		f 4 201 203 -192 -163
		f 4 214 209 -216 -205
		f 4 215 210 -217 -206
		f 4 216 211 -218 -207
		f 4 217 212 -219 -208
		f 4 218 213 -215 -209
		f 3 -230 -231 -229
		f 4 219 221 -221 -210
		f 4 220 223 -223 -211
		f 4 222 225 -225 -212
		f 4 224 227 -227 -213
		f 4 226 228 -220 -214
		f 3 -228 -226 229
		f 3 -224 -222 230
		f 4 241 236 -243 -232
		f 4 242 237 -244 -233
		f 4 243 238 -245 -234
		f 4 244 239 -246 -235
		f 4 245 240 -242 -236
		f 3 -257 -258 -256
		f 4 246 248 -248 -237
		f 4 247 250 -250 -238
		f 4 249 252 -252 -239
		f 4 251 254 -254 -240
		f 4 253 255 -247 -241
		f 3 -255 -253 256
		f 3 -251 -249 257
		f 4 268 263 -270 -259
		f 4 269 264 -271 -260
		f 4 270 265 -272 -261
		f 4 271 266 -273 -262
		f 4 272 267 -269 -263
		f 3 -284 -285 -283
		f 4 273 275 -275 -264
		f 4 274 277 -277 -265
		f 4 276 279 -279 -266
		f 4 278 281 -281 -267
		f 4 280 282 -274 -268
		f 3 -282 -280 283
		f 3 -278 -276 284
		f 4 295 290 -297 -286
		f 4 296 291 -298 -287
		f 4 297 292 -299 -288
		f 4 298 293 -300 -289
		f 4 299 294 -296 -290
		f 3 -311 -312 -310
		f 4 300 302 -302 -291
		f 4 301 304 -304 -292
		f 4 303 306 -306 -293
		f 4 305 308 -308 -294
		f 4 307 309 -301 -295
		f 3 -309 -307 310
		f 3 -305 -303 311
		f 4 322 317 -324 -313
		f 4 323 318 -325 -314
		f 4 324 319 -326 -315
		f 4 325 320 -327 -316
		f 4 326 321 -323 -317
		f 3 -338 -339 -337
		f 4 327 329 -329 -318
		f 4 328 331 -331 -319
		f 4 330 333 -333 -320
		f 4 332 335 -335 -321
		f 4 334 336 -328 -322
		f 3 -336 -334 337
		f 3 -332 -330 338
		f 4 417 -346 -340 342
		f 4 -415 -416 -342 -341
		f 4 -417 -418 414 -345
		f 4 -419 416 -348 -347
		f 4 422 -358 -352 354
		f 4 -420 -421 -354 -353
		f 4 -422 -423 419 -357
		f 4 -424 421 -360 -359
		f 4 427 -370 -364 366
		f 4 -425 -426 -366 -365
		f 4 -427 -428 424 -369
		f 4 -429 426 -372 -371
		f 4 432 -382 -376 378
		f 4 -430 -431 -378 -377
		f 4 -432 -433 429 -381
		f 4 -434 431 -384 -383
		f 4 412 -394 -388 390
		f 4 -410 -411 -390 -389
		f 4 -412 -413 409 -393
		f 4 -414 411 -396 -395
		f 4 400 -362 -400 350
		f 4 402 -374 -402 362
		f 4 404 -386 -404 374
		f 4 406 -398 -406 386
		f 4 408 398 -408 -350
		f 4 399 351 401 -435
		f 4 -391 397 -392 410
		f 4 -399 393 413 -397
		f 4 -343 349 -344 415
		f 4 -351 345 418 -349
		f 4 -355 361 -356 420
		f 4 -363 357 423 -361
		f 4 -367 373 -368 425
		f 4 -375 369 428 -373
		f 4 -379 385 -380 430
		f 4 -387 381 433 -385
		f 4 434 363 -436 339
		f 4 375 405 387 -437
		f 4 403 436 -409 435
		f 4 468 -470 -468 437
		f 4 467 -472 -471 438
		f 4 470 -474 -473 439
		f 4 472 -476 -475 440
		f 4 474 -478 -477 441
		f 4 476 -480 -479 442
		f 4 478 -482 -481 443
		f 4 480 -484 -483 444
		f 4 482 -486 -485 445
		f 4 484 -488 -487 446
		f 4 486 -490 -489 447
		f 4 488 -492 -491 448
		f 4 490 -494 -493 449
		f 4 492 -496 -495 450
		f 4 494 -498 -497 451
		f 4 496 -500 -499 452
		f 4 498 -502 -501 453
		f 4 500 -504 -503 454
		f 4 502 -506 -505 455
		f 4 504 -508 -507 456
		f 4 506 -510 -509 457
		f 4 508 -512 -511 458
		f 4 510 -514 -513 459
		f 4 512 -516 -515 460
		f 4 514 -518 -517 461
		f 4 516 -520 -519 462
		f 4 518 -522 -521 463
		f 4 520 -524 -523 464
		f 4 522 -526 -525 465
		f 4 524 -527 -469 466
		f 4 528 -558 -528 469
		f 4 527 -560 -530 471
		f 4 529 -561 -531 473
		f 4 530 -562 -532 475
		f 4 531 -563 -533 477
		f 4 532 -564 -534 479
		f 4 533 -565 -535 481
		f 4 534 -566 -536 483
		f 4 535 -567 -537 485
		f 4 536 -568 -538 487
		f 4 537 -569 -539 489
		f 4 538 -570 -540 491
		f 4 539 -571 -541 493
		f 4 540 -572 -542 495
		f 4 541 -573 -543 497
		f 4 542 -574 -544 499
		f 4 543 -575 -545 501
		f 4 544 -576 -546 503
		f 4 545 -577 -547 505
		f 4 546 -578 -548 507
		f 4 547 -579 -549 509
		f 4 548 -580 -550 511
		f 4 549 -581 -551 513
		f 4 550 -582 -552 515
		f 4 551 -583 -553 517
		f 4 552 -584 -554 519
		f 4 553 -585 -555 521
		f 4 554 -586 -556 523
		f 4 555 -587 -557 525
		f 4 556 -559 -529 526
		f 4 118 587 136 -589
		f 4 116 588 138 -590
		f 4 -591 114 589 140
		f 3 142 143 590
		f 4 124 591 130 -593
		f 4 122 592 132 -594
		f 4 120 593 134 -588
		f 4 -103 -599 -89 594
		f 4 -105 -595 -87 595
		f 4 -107 -596 -85 596
		f 4 -109 -597 -83 597
		f 4 -101 -600 -91 598
		f 4 -99 -601 -93 599
		f 3 -97 -95 600
		f 4 633 617 -635 -602
		f 4 634 618 -636 -603
		f 4 635 619 -637 -604
		f 4 636 620 -638 -605
		f 4 637 621 -639 -606
		f 4 638 622 -640 -607
		f 4 639 623 -641 -608
		f 4 640 624 -642 -609
		f 4 641 625 -643 -610
		f 4 642 626 -644 -611
		f 4 643 627 -645 -612
		f 4 644 628 -646 -613
		f 4 645 629 -647 -614
		f 4 646 630 -648 -615
		f 4 647 631 -649 -616
		f 4 648 632 -634 -617
		f 3 727 729 -1193
		f 3 -712 -1199 -713
		f 4 649 651 -651 -618
		f 4 650 653 -653 -619
		f 4 652 655 -655 -620
		f 4 654 657 -657 -621
		f 4 656 659 -659 -622
		f 4 658 661 -661 -623
		f 4 660 663 -663 -624
		f 4 662 665 -665 -625
		f 4 664 667 -667 -626
		f 4 666 669 -669 -627
		f 4 668 671 -671 -628
		f 4 670 673 -673 -629
		f 4 672 675 -675 -630
		f 4 674 677 -677 -631
		f 4 676 679 -679 -632
		f 4 678 680 -650 -633
		f 4 681 683 -683 -652
		f 4 682 685 -685 -654
		f 4 684 687 -687 -656
		f 4 686 689 -689 -658
		f 4 688 691 -691 -660
		f 4 690 693 -693 -662
		f 4 692 695 -695 -664
		f 4 694 697 -697 -666
		f 4 696 699 -699 -668
		f 4 698 701 -701 -670
		f 4 700 703 -703 -672
		f 4 702 705 -705 -674
		f 4 704 707 -707 -676
		f 4 706 709 -709 -678
		f 4 708 711 -711 -680
		f 4 710 712 -682 -681
		f 4 714 -716 -714 601
		f 4 716 -718 -715 602
		f 4 718 -720 -717 603
		f 4 720 -722 -719 604
		f 4 722 -724 -721 605
		f 4 724 -726 -723 606
		f 4 726 -728 -725 607
		f 4 728 -730 -727 608
		f 4 730 -732 -729 609
		f 4 732 -734 -731 610
		f 4 734 -736 -733 611
		f 4 736 -738 -735 612
		f 4 738 -740 -737 613
		f 4 740 -742 -739 614
		f 4 742 -744 -741 615
		f 4 713 -745 -743 616
		f 4 769 757 -771 -746
		f 4 770 758 -772 -747
		f 4 771 759 -773 -748
		f 4 772 760 -774 -749
		f 4 773 761 -775 -750
		f 4 774 762 -776 -751
		f 4 775 763 -777 -752
		f 4 776 764 -778 -753
		f 4 777 765 -779 -754
		f 4 778 766 -780 -755
		f 4 779 767 -781 -756
		f 4 780 768 -770 -757
		f 3 2423 2417 -797
		f 4 781 783 -783 -769
		f 4 782 785 -785 -758
		f 4 786 787 -782 -768
		f 4 788 791 -791 -761
		f 4 792 793 -790 -765
		f 4 794 796 -789 -760
		f 4 789 797 -796 -766
		f 4 784 798 -795 -759
		f 4 795 799 -787 -767
		f 4 790 801 -801 -762
		f 4 800 803 -803 -763
		f 4 802 804 -793 -764
		f 4 815 810 -817 -806
		f 4 816 811 -818 -807
		f 4 817 812 -819 -808
		f 4 818 813 -820 -809
		f 4 819 814 -816 -810
		f 3 -831 -832 -830
		f 4 820 822 -822 -811
		f 4 821 824 -824 -812
		f 4 823 826 -826 -813
		f 4 825 828 -828 -814
		f 4 827 829 -821 -815
		f 3 -829 -827 830
		f 3 -825 -823 831
		f 4 842 837 -844 -833
		f 4 843 838 -845 -834
		f 4 844 839 -846 -835
		f 4 845 840 -847 -836
		f 4 846 841 -843 -837
		f 3 -858 -859 -857
		f 4 847 849 -849 -838
		f 4 848 851 -851 -839
		f 4 850 853 -853 -840
		f 4 852 855 -855 -841
		f 4 854 856 -848 -842
		f 3 -856 -854 857
		f 3 -852 -850 858
		f 4 869 864 -871 -860
		f 4 870 865 -872 -861
		f 4 871 866 -873 -862
		f 4 872 867 -874 -863
		f 4 873 868 -870 -864
		f 3 -885 -886 -884
		f 4 874 876 -876 -865
		f 4 875 878 -878 -866
		f 4 877 880 -880 -867
		f 4 879 882 -882 -868
		f 4 881 883 -875 -869
		f 3 -883 -881 884
		f 3 -879 -877 885
		f 4 896 891 -898 -887
		f 4 897 892 -899 -888
		f 4 898 893 -900 -889
		f 4 899 894 -901 -890
		f 4 900 895 -897 -891
		f 3 -912 -913 -911
		f 4 901 903 -903 -892
		f 4 902 905 -905 -893
		f 4 904 907 -907 -894
		f 4 906 909 -909 -895
		f 4 908 910 -902 -896
		f 3 -910 -908 911
		f 3 -906 -904 912
		f 4 923 918 -925 -914
		f 4 924 919 -926 -915
		f 4 925 920 -927 -916
		f 4 926 921 -928 -917
		f 4 927 922 -924 -918
		f 3 -939 -940 -938
		f 4 928 930 -930 -919
		f 4 929 932 -932 -920
		f 4 931 934 -934 -921
		f 4 933 936 -936 -922
		f 4 935 937 -929 -923
		f 3 -937 -935 938
		f 3 -933 -931 939
		f 4 1018 -947 -941 943
		f 4 -1016 -1017 -943 -942
		f 4 -1018 -1019 1015 -946
		f 4 -1020 1017 -949 -948
		f 4 1023 -959 -953 955
		f 4 -1021 -1022 -955 -954
		f 4 -1023 -1024 1020 -958
		f 4 -1025 1022 -961 -960
		f 4 1028 -971 -965 967
		f 4 -1026 -1027 -967 -966
		f 4 -1028 -1029 1025 -970
		f 4 -1030 1027 -973 -972
		f 4 1033 -983 -977 979
		f 4 -1031 -1032 -979 -978
		f 4 -1033 -1034 1030 -982
		f 4 -1035 1032 -985 -984
		f 4 1013 -995 -989 991
		f 4 -1011 -1012 -991 -990
		f 4 -1013 -1014 1010 -994
		f 4 -1015 1012 -997 -996
		f 4 1001 -963 -1001 951
		f 4 1003 -975 -1003 963
		f 4 1005 -987 -1005 975
		f 4 1007 -999 -1007 987
		f 4 1009 999 -1009 -951
		f 4 1000 952 1002 -1036
		f 4 -992 998 -993 1011
		f 4 -1000 994 1014 -998
		f 4 -944 950 -945 1016
		f 4 -952 946 1019 -950
		f 4 -956 962 -957 1021
		f 4 -964 958 1024 -962
		f 4 -968 974 -969 1026
		f 4 -976 970 1029 -974
		f 4 -980 986 -981 1031
		f 4 -988 982 1034 -986
		f 4 1035 964 -1037 940
		f 4 976 1006 988 -1038
		f 4 1004 1037 -1010 1036
		f 4 1069 -1071 -1069 1038
		f 4 1068 -1073 -1072 1039
		f 4 1071 -1075 -1074 1040
		f 4 1073 -1077 -1076 1041
		f 4 1075 -1079 -1078 1042
		f 4 1077 -1081 -1080 1043
		f 4 1079 -1083 -1082 1044
		f 4 1081 -1085 -1084 1045
		f 4 1083 -1087 -1086 1046
		f 4 1085 -1089 -1088 1047
		f 4 1087 -1091 -1090 1048
		f 4 1089 -1093 -1092 1049
		f 4 1091 -1095 -1094 1050
		f 4 1093 -1097 -1096 1051
		f 4 1095 -1099 -1098 1052
		f 4 1097 -1101 -1100 1053
		f 4 1099 -1103 -1102 1054
		f 4 1101 -1105 -1104 1055
		f 4 1103 -1107 -1106 1056
		f 4 1105 -1109 -1108 1057
		f 4 1107 -1111 -1110 1058
		f 4 1109 -1113 -1112 1059
		f 4 1111 -1115 -1114 1060
		f 4 1113 -1117 -1116 1061
		f 4 1115 -1119 -1118 1062
		f 4 1117 -1121 -1120 1063
		f 4 1119 -1123 -1122 1064
		f 4 1121 -1125 -1124 1065
		f 4 1123 -1127 -1126 1066
		f 4 1125 -1128 -1070 1067
		f 4 1129 -1159 -1129 1070
		f 4 1128 -1161 -1131 1072
		f 4 1130 -1162 -1132 1074
		f 4 1131 -1163 -1133 1076
		f 4 1132 -1164 -1134 1078
		f 4 1133 -1165 -1135 1080;
	setAttr ".fc[500:999]"
		f 4 1134 -1166 -1136 1082
		f 4 1135 -1167 -1137 1084
		f 4 1136 -1168 -1138 1086
		f 4 1137 -1169 -1139 1088
		f 4 1138 -1170 -1140 1090
		f 4 1139 -1171 -1141 1092
		f 4 1140 -1172 -1142 1094
		f 4 1141 -1173 -1143 1096
		f 4 1142 -1174 -1144 1098
		f 4 1143 -1175 -1145 1100
		f 4 1144 -1176 -1146 1102
		f 4 1145 -1177 -1147 1104
		f 4 1146 -1178 -1148 1106
		f 4 1147 -1179 -1149 1108
		f 4 1148 -1180 -1150 1110
		f 4 1149 -1181 -1151 1112
		f 4 1150 -1182 -1152 1114
		f 4 1151 -1183 -1153 1116
		f 4 1152 -1184 -1154 1118
		f 4 1153 -1185 -1155 1120
		f 4 1154 -1186 -1156 1122
		f 4 1155 -1187 -1157 1124
		f 4 1156 -1188 -1158 1126
		f 4 1157 -1160 -1130 1127
		f 4 719 1188 737 -1190
		f 4 717 1189 739 -1191
		f 4 -1192 715 1190 741
		f 3 743 744 1191
		f 4 725 1192 731 -1194
		f 4 723 1193 733 -1195
		f 4 721 1194 735 -1189
		f 4 -704 -1200 -690 1195
		f 4 -706 -1196 -688 1196
		f 4 -708 -1197 -686 1197
		f 4 -710 -1198 -684 1198
		f 4 -702 -1201 -692 1199
		f 4 -700 -1202 -694 1200
		f 3 -698 -696 1201
		f 4 1202 1235 -1219 -1235
		f 4 1203 1236 -1220 -1236
		f 4 1204 1237 -1221 -1237
		f 4 1205 1238 -1222 -1238
		f 4 1206 1239 -1223 -1239
		f 4 1207 1240 -1224 -1240
		f 4 1208 1241 -1225 -1241
		f 4 1209 1242 -1226 -1242
		f 4 1210 1243 -1227 -1243
		f 4 1211 1244 -1228 -1244
		f 4 1212 1245 -1229 -1245
		f 4 1213 1246 -1230 -1246
		f 4 1214 1247 -1231 -1247
		f 4 1215 1248 -1232 -1248
		f 4 1216 1249 -1233 -1249
		f 4 1217 1234 -1234 -1250
		f 3 1793 -1331 -1329
		f 3 1313 1799 1312
		f 4 1218 1251 -1253 -1251
		f 4 1219 1253 -1255 -1252
		f 4 1220 1255 -1257 -1254
		f 4 1221 1257 -1259 -1256
		f 4 1222 1259 -1261 -1258
		f 4 1223 1261 -1263 -1260
		f 4 1224 1263 -1265 -1262
		f 4 1225 1265 -1267 -1264
		f 4 1226 1267 -1269 -1266
		f 4 1227 1269 -1271 -1268
		f 4 1228 1271 -1273 -1270
		f 4 1229 1273 -1275 -1272
		f 4 1230 1275 -1277 -1274
		f 4 1231 1277 -1279 -1276
		f 4 1232 1279 -1281 -1278
		f 4 1233 1250 -1282 -1280
		f 4 1252 1283 -1285 -1283
		f 4 1254 1285 -1287 -1284
		f 4 1256 1287 -1289 -1286
		f 4 1258 1289 -1291 -1288
		f 4 1260 1291 -1293 -1290
		f 4 1262 1293 -1295 -1292
		f 4 1264 1295 -1297 -1294
		f 4 1266 1297 -1299 -1296
		f 4 1268 1299 -1301 -1298
		f 4 1270 1301 -1303 -1300
		f 4 1272 1303 -1305 -1302
		f 4 1274 1305 -1307 -1304
		f 4 1276 1307 -1309 -1306
		f 4 1278 1309 -1311 -1308
		f 4 1280 1311 -1313 -1310
		f 4 1281 1282 -1314 -1312
		f 4 -1203 1314 1316 -1316
		f 4 -1204 1315 1318 -1318
		f 4 -1205 1317 1320 -1320
		f 4 -1206 1319 1322 -1322
		f 4 -1207 1321 1324 -1324
		f 4 -1208 1323 1326 -1326
		f 4 -1209 1325 1328 -1328
		f 4 -1210 1327 1330 -1330
		f 4 -1211 1329 1332 -1332
		f 4 -1212 1331 1334 -1334
		f 4 -1213 1333 1336 -1336
		f 4 -1214 1335 1338 -1338
		f 4 -1215 1337 1340 -1340
		f 4 -1216 1339 1342 -1342
		f 4 -1217 1341 1344 -1344
		f 4 -1218 1343 1345 -1315
		f 4 1346 1371 -1359 -1371
		f 4 1347 1372 -1360 -1372
		f 4 1348 1373 -1361 -1373
		f 4 1349 1374 -1362 -1374
		f 4 1350 1375 -1363 -1375
		f 4 1351 1376 -1364 -1376
		f 4 1352 1377 -1365 -1377
		f 4 1353 1378 -1366 -1378
		f 4 1354 1379 -1367 -1379
		f 4 1355 1380 -1368 -1380
		f 4 1356 1381 -1369 -1381
		f 4 1357 1370 -1370 -1382
		f 3 2447 2441 1392
		f 4 1369 1383 -1385 -1383
		f 4 1358 1385 -1387 -1384
		f 4 1368 1382 -1389 -1388
		f 4 1361 1391 -1393 -1390
		f 4 1365 1390 -1395 -1394
		f 4 1360 1389 -1398 -1396
		f 4 1366 1396 -1399 -1391
		f 4 1359 1395 -1400 -1386
		f 4 1367 1387 -1401 -1397
		f 4 1362 1401 -1403 -1392
		f 4 1363 1403 -1405 -1402
		f 4 1364 1393 -1406 -1404
		f 4 1406 1417 -1412 -1417
		f 4 1407 1418 -1413 -1418
		f 4 1408 1419 -1414 -1419
		f 4 1409 1420 -1415 -1420
		f 4 1410 1416 -1416 -1421
		f 3 1430 1432 1431
		f 4 1411 1422 -1424 -1422
		f 4 1412 1424 -1426 -1423
		f 4 1413 1426 -1428 -1425
		f 4 1414 1428 -1430 -1427
		f 4 1415 1421 -1431 -1429
		f 3 -1432 1427 1429
		f 3 -1433 1423 1425
		f 4 1433 1444 -1439 -1444
		f 4 1434 1445 -1440 -1445
		f 4 1435 1446 -1441 -1446
		f 4 1436 1447 -1442 -1447
		f 4 1437 1443 -1443 -1448
		f 3 1457 1459 1458
		f 4 1438 1449 -1451 -1449
		f 4 1439 1451 -1453 -1450
		f 4 1440 1453 -1455 -1452
		f 4 1441 1455 -1457 -1454
		f 4 1442 1448 -1458 -1456
		f 3 -1459 1454 1456
		f 3 -1460 1450 1452
		f 4 1460 1471 -1466 -1471
		f 4 1461 1472 -1467 -1472
		f 4 1462 1473 -1468 -1473
		f 4 1463 1474 -1469 -1474
		f 4 1464 1470 -1470 -1475
		f 3 1484 1486 1485
		f 4 1465 1476 -1478 -1476
		f 4 1466 1478 -1480 -1477
		f 4 1467 1480 -1482 -1479
		f 4 1468 1482 -1484 -1481
		f 4 1469 1475 -1485 -1483
		f 3 -1486 1481 1483
		f 3 -1487 1477 1479
		f 4 1487 1498 -1493 -1498
		f 4 1488 1499 -1494 -1499
		f 4 1489 1500 -1495 -1500
		f 4 1490 1501 -1496 -1501
		f 4 1491 1497 -1497 -1502
		f 3 1511 1513 1512
		f 4 1492 1503 -1505 -1503
		f 4 1493 1505 -1507 -1504
		f 4 1494 1507 -1509 -1506
		f 4 1495 1509 -1511 -1508
		f 4 1496 1502 -1512 -1510
		f 3 -1513 1508 1510
		f 3 -1514 1504 1506
		f 4 1514 1525 -1520 -1525
		f 4 1515 1526 -1521 -1526
		f 4 1516 1527 -1522 -1527
		f 4 1517 1528 -1523 -1528
		f 4 1518 1524 -1524 -1529
		f 3 1538 1540 1539
		f 4 1519 1530 -1532 -1530
		f 4 1520 1532 -1534 -1531
		f 4 1521 1534 -1536 -1533
		f 4 1522 1536 -1538 -1535
		f 4 1523 1529 -1539 -1537
		f 3 -1540 1535 1537
		f 3 -1541 1531 1533
		f 4 -1545 1541 1547 -1620
		f 4 1542 1543 1617 1616
		f 4 1546 -1617 1619 1618
		f 4 1548 1549 -1619 1620
		f 4 -1557 1553 1559 -1625
		f 4 1554 1555 1622 1621
		f 4 1558 -1622 1624 1623
		f 4 1560 1561 -1624 1625
		f 4 -1569 1565 1571 -1630
		f 4 1566 1567 1627 1626
		f 4 1570 -1627 1629 1628
		f 4 1572 1573 -1629 1630
		f 4 -1581 1577 1583 -1635
		f 4 1578 1579 1632 1631
		f 4 1582 -1632 1634 1633
		f 4 1584 1585 -1634 1635
		f 4 -1593 1589 1595 -1615
		f 4 1590 1591 1612 1611
		f 4 1594 -1612 1614 1613
		f 4 1596 1597 -1614 1615
		f 4 -1553 1601 1563 -1603
		f 4 -1565 1603 1575 -1605
		f 4 -1577 1605 1587 -1607
		f 4 -1589 1607 1599 -1609
		f 4 1551 1609 -1601 -1611
		f 4 1636 -1604 -1554 -1602
		f 4 -1613 1593 -1600 1592
		f 4 1598 -1616 -1596 1600
		f 4 -1618 1545 -1552 1544
		f 4 1550 -1621 -1548 1552
		f 4 -1623 1557 -1564 1556
		f 4 1562 -1626 -1560 1564
		f 4 -1628 1569 -1576 1568
		f 4 1574 -1631 -1572 1576
		f 4 -1633 1581 -1588 1580
		f 4 1586 -1636 -1584 1588
		f 4 -1542 1637 -1566 -1637
		f 4 1638 -1590 -1608 -1578
		f 4 -1638 1610 -1639 -1606
		f 4 -1640 1669 1671 -1671
		f 4 -1641 1672 1673 -1670
		f 4 -1642 1674 1675 -1673
		f 4 -1643 1676 1677 -1675
		f 4 -1644 1678 1679 -1677
		f 4 -1645 1680 1681 -1679
		f 4 -1646 1682 1683 -1681
		f 4 -1647 1684 1685 -1683
		f 4 -1648 1686 1687 -1685
		f 4 -1649 1688 1689 -1687
		f 4 -1650 1690 1691 -1689
		f 4 -1651 1692 1693 -1691
		f 4 -1652 1694 1695 -1693
		f 4 -1653 1696 1697 -1695
		f 4 -1654 1698 1699 -1697
		f 4 -1655 1700 1701 -1699
		f 4 -1656 1702 1703 -1701
		f 4 -1657 1704 1705 -1703
		f 4 -1658 1706 1707 -1705
		f 4 -1659 1708 1709 -1707
		f 4 -1660 1710 1711 -1709
		f 4 -1661 1712 1713 -1711
		f 4 -1662 1714 1715 -1713
		f 4 -1663 1716 1717 -1715
		f 4 -1664 1718 1719 -1717
		f 4 -1665 1720 1721 -1719
		f 4 -1666 1722 1723 -1721
		f 4 -1667 1724 1725 -1723
		f 4 -1668 1726 1727 -1725
		f 4 -1669 1670 1728 -1727
		f 4 -1672 1729 1759 -1731
		f 4 -1674 1731 1761 -1730
		f 4 -1676 1732 1762 -1732
		f 4 -1678 1733 1763 -1733
		f 4 -1680 1734 1764 -1734
		f 4 -1682 1735 1765 -1735
		f 4 -1684 1736 1766 -1736
		f 4 -1686 1737 1767 -1737
		f 4 -1688 1738 1768 -1738
		f 4 -1690 1739 1769 -1739
		f 4 -1692 1740 1770 -1740
		f 4 -1694 1741 1771 -1741
		f 4 -1696 1742 1772 -1742
		f 4 -1698 1743 1773 -1743
		f 4 -1700 1744 1774 -1744
		f 4 -1702 1745 1775 -1745
		f 4 -1704 1746 1776 -1746
		f 4 -1706 1747 1777 -1747
		f 4 -1708 1748 1778 -1748
		f 4 -1710 1749 1779 -1749
		f 4 -1712 1750 1780 -1750
		f 4 -1714 1751 1781 -1751
		f 4 -1716 1752 1782 -1752
		f 4 -1718 1753 1783 -1753
		f 4 -1720 1754 1784 -1754
		f 4 -1722 1755 1785 -1755
		f 4 -1724 1756 1786 -1756
		f 4 -1726 1757 1787 -1757
		f 4 -1728 1758 1788 -1758
		f 4 -1729 1730 1760 -1759
		f 4 1790 -1339 -1790 -1321
		f 4 1791 -1341 -1791 -1319
		f 4 -1343 -1792 -1317 1792
		f 3 -1793 -1346 -1345
		f 4 1794 -1333 -1794 -1327
		f 4 1795 -1335 -1795 -1325
		f 4 1789 -1337 -1796 -1323
		f 4 -1797 1290 1800 1304
		f 4 -1798 1288 1796 1306
		f 4 -1799 1286 1797 1308
		f 4 -1800 1284 1798 1310
		f 4 -1801 1292 1801 1302
		f 4 -1802 1294 1802 1300
		f 3 -1803 1296 1298
		f 4 1803 1836 -1820 -1836
		f 4 1804 1837 -1821 -1837
		f 4 1805 1838 -1822 -1838
		f 4 1806 1839 -1823 -1839
		f 4 1807 1840 -1824 -1840
		f 4 1808 1841 -1825 -1841
		f 4 1809 1842 -1826 -1842
		f 4 1810 1843 -1827 -1843
		f 4 1811 1844 -1828 -1844
		f 4 1812 1845 -1829 -1845
		f 4 1813 1846 -1830 -1846
		f 4 1814 1847 -1831 -1847
		f 4 1815 1848 -1832 -1848
		f 4 1816 1849 -1833 -1849
		f 4 1817 1850 -1834 -1850
		f 4 1818 1835 -1835 -1851
		f 3 2394 -1932 -1930
		f 3 1914 2400 1913
		f 4 1819 1852 -1854 -1852
		f 4 1820 1854 -1856 -1853
		f 4 1821 1856 -1858 -1855
		f 4 1822 1858 -1860 -1857
		f 4 1823 1860 -1862 -1859
		f 4 1824 1862 -1864 -1861
		f 4 1825 1864 -1866 -1863
		f 4 1826 1866 -1868 -1865
		f 4 1827 1868 -1870 -1867
		f 4 1828 1870 -1872 -1869
		f 4 1829 1872 -1874 -1871
		f 4 1830 1874 -1876 -1873
		f 4 1831 1876 -1878 -1875
		f 4 1832 1878 -1880 -1877
		f 4 1833 1880 -1882 -1879
		f 4 1834 1851 -1883 -1881
		f 4 1853 1884 -1886 -1884
		f 4 1855 1886 -1888 -1885
		f 4 1857 1888 -1890 -1887
		f 4 1859 1890 -1892 -1889
		f 4 1861 1892 -1894 -1891
		f 4 1863 1894 -1896 -1893
		f 4 1865 1896 -1898 -1895
		f 4 1867 1898 -1900 -1897
		f 4 1869 1900 -1902 -1899
		f 4 1871 1902 -1904 -1901
		f 4 1873 1904 -1906 -1903
		f 4 1875 1906 -1908 -1905
		f 4 1877 1908 -1910 -1907
		f 4 1879 1910 -1912 -1909
		f 4 1881 1912 -1914 -1911
		f 4 1882 1883 -1915 -1913
		f 4 -1804 1915 1917 -1917
		f 4 -1805 1916 1919 -1919
		f 4 -1806 1918 1921 -1921
		f 4 -1807 1920 1923 -1923
		f 4 -1808 1922 1925 -1925
		f 4 -1809 1924 1927 -1927
		f 4 -1810 1926 1929 -1929
		f 4 -1811 1928 1931 -1931
		f 4 -1812 1930 1933 -1933
		f 4 -1813 1932 1935 -1935
		f 4 -1814 1934 1937 -1937
		f 4 -1815 1936 1939 -1939
		f 4 -1816 1938 1941 -1941
		f 4 -1817 1940 1943 -1943
		f 4 -1818 1942 1945 -1945
		f 4 -1819 1944 1946 -1916
		f 4 1947 1972 -1960 -1972
		f 4 1948 1973 -1961 -1973
		f 4 1949 1974 -1962 -1974
		f 4 1950 1975 -1963 -1975
		f 4 1951 1976 -1964 -1976
		f 4 1952 1977 -1965 -1977
		f 4 1953 1978 -1966 -1978
		f 4 1954 1979 -1967 -1979
		f 4 1955 1980 -1968 -1980
		f 4 1956 1981 -1969 -1981
		f 4 1957 1982 -1970 -1982
		f 4 1958 1971 -1971 -1983
		f 3 2411 2405 1993
		f 4 1970 1984 -1986 -1984
		f 4 1959 1986 -1988 -1985
		f 4 1969 1983 -1990 -1989
		f 4 1962 1992 -1994 -1991
		f 4 1966 1991 -1996 -1995
		f 4 1961 1990 -1999 -1997
		f 4 1967 1997 -2000 -1992
		f 4 1960 1996 -2001 -1987
		f 4 1968 1988 -2002 -1998
		f 4 1963 2002 -2004 -1993
		f 4 1964 2004 -2006 -2003
		f 4 1965 1994 -2007 -2005
		f 4 2007 2018 -2013 -2018
		f 4 2008 2019 -2014 -2019
		f 4 2009 2020 -2015 -2020
		f 4 2010 2021 -2016 -2021
		f 4 2011 2017 -2017 -2022
		f 3 2031 2033 2032
		f 4 2012 2023 -2025 -2023
		f 4 2013 2025 -2027 -2024
		f 4 2014 2027 -2029 -2026
		f 4 2015 2029 -2031 -2028
		f 4 2016 2022 -2032 -2030
		f 3 -2033 2028 2030
		f 3 -2034 2024 2026
		f 4 2034 2045 -2040 -2045
		f 4 2035 2046 -2041 -2046
		f 4 2036 2047 -2042 -2047
		f 4 2037 2048 -2043 -2048
		f 4 2038 2044 -2044 -2049
		f 3 2058 2060 2059
		f 4 2039 2050 -2052 -2050
		f 4 2040 2052 -2054 -2051
		f 4 2041 2054 -2056 -2053
		f 4 2042 2056 -2058 -2055
		f 4 2043 2049 -2059 -2057
		f 3 -2060 2055 2057
		f 3 -2061 2051 2053
		f 4 2061 2072 -2067 -2072
		f 4 2062 2073 -2068 -2073
		f 4 2063 2074 -2069 -2074
		f 4 2064 2075 -2070 -2075
		f 4 2065 2071 -2071 -2076
		f 3 2085 2087 2086
		f 4 2066 2077 -2079 -2077
		f 4 2067 2079 -2081 -2078
		f 4 2068 2081 -2083 -2080
		f 4 2069 2083 -2085 -2082
		f 4 2070 2076 -2086 -2084
		f 3 -2087 2082 2084
		f 3 -2088 2078 2080
		f 4 2088 2099 -2094 -2099
		f 4 2089 2100 -2095 -2100
		f 4 2090 2101 -2096 -2101
		f 4 2091 2102 -2097 -2102
		f 4 2092 2098 -2098 -2103
		f 3 2112 2114 2113
		f 4 2093 2104 -2106 -2104
		f 4 2094 2106 -2108 -2105
		f 4 2095 2108 -2110 -2107
		f 4 2096 2110 -2112 -2109
		f 4 2097 2103 -2113 -2111
		f 3 -2114 2109 2111
		f 3 -2115 2105 2107
		f 4 2115 2126 -2121 -2126
		f 4 2116 2127 -2122 -2127
		f 4 2117 2128 -2123 -2128
		f 4 2118 2129 -2124 -2129
		f 4 2119 2125 -2125 -2130
		f 3 2139 2141 2140
		f 4 2120 2131 -2133 -2131
		f 4 2121 2133 -2135 -2132
		f 4 2122 2135 -2137 -2134
		f 4 2123 2137 -2139 -2136
		f 4 2124 2130 -2140 -2138
		f 3 -2141 2136 2138
		f 3 -2142 2132 2134
		f 4 -2146 2142 2148 -2221
		f 4 2143 2144 2218 2217
		f 4 2147 -2218 2220 2219
		f 4 2149 2150 -2220 2221
		f 4 -2158 2154 2160 -2226
		f 4 2155 2156 2223 2222
		f 4 2159 -2223 2225 2224
		f 4 2161 2162 -2225 2226
		f 4 -2170 2166 2172 -2231
		f 4 2167 2168 2228 2227
		f 4 2171 -2228 2230 2229
		f 4 2173 2174 -2230 2231
		f 4 -2182 2178 2184 -2236
		f 4 2179 2180 2233 2232
		f 4 2183 -2233 2235 2234
		f 4 2185 2186 -2235 2236
		f 4 -2194 2190 2196 -2216
		f 4 2191 2192 2213 2212
		f 4 2195 -2213 2215 2214
		f 4 2197 2198 -2215 2216
		f 4 -2154 2202 2164 -2204
		f 4 -2166 2204 2176 -2206
		f 4 -2178 2206 2188 -2208
		f 4 -2190 2208 2200 -2210
		f 4 2152 2210 -2202 -2212
		f 4 2237 -2205 -2155 -2203
		f 4 -2214 2194 -2201 2193
		f 4 2199 -2217 -2197 2201
		f 4 -2219 2146 -2153 2145
		f 4 2151 -2222 -2149 2153
		f 4 -2224 2158 -2165 2157
		f 4 2163 -2227 -2161 2165
		f 4 -2229 2170 -2177 2169
		f 4 2175 -2232 -2173 2177
		f 4 -2234 2182 -2189 2181
		f 4 2187 -2237 -2185 2189
		f 4 -2143 2238 -2167 -2238;
	setAttr ".fc[1000:1119]"
		f 4 2239 -2191 -2209 -2179
		f 4 -2239 2211 -2240 -2207
		f 4 -2241 2270 2272 -2272
		f 4 -2242 2273 2274 -2271
		f 4 -2243 2275 2276 -2274
		f 4 -2244 2277 2278 -2276
		f 4 -2245 2279 2280 -2278
		f 4 -2246 2281 2282 -2280
		f 4 -2247 2283 2284 -2282
		f 4 -2248 2285 2286 -2284
		f 4 -2249 2287 2288 -2286
		f 4 -2250 2289 2290 -2288
		f 4 -2251 2291 2292 -2290
		f 4 -2252 2293 2294 -2292
		f 4 -2253 2295 2296 -2294
		f 4 -2254 2297 2298 -2296
		f 4 -2255 2299 2300 -2298
		f 4 -2256 2301 2302 -2300
		f 4 -2257 2303 2304 -2302
		f 4 -2258 2305 2306 -2304
		f 4 -2259 2307 2308 -2306
		f 4 -2260 2309 2310 -2308
		f 4 -2261 2311 2312 -2310
		f 4 -2262 2313 2314 -2312
		f 4 -2263 2315 2316 -2314
		f 4 -2264 2317 2318 -2316
		f 4 -2265 2319 2320 -2318
		f 4 -2266 2321 2322 -2320
		f 4 -2267 2323 2324 -2322
		f 4 -2268 2325 2326 -2324
		f 4 -2269 2327 2328 -2326
		f 4 -2270 2271 2329 -2328
		f 4 -2273 2330 2360 -2332
		f 4 -2275 2332 2362 -2331
		f 4 -2277 2333 2363 -2333
		f 4 -2279 2334 2364 -2334
		f 4 -2281 2335 2365 -2335
		f 4 -2283 2336 2366 -2336
		f 4 -2285 2337 2367 -2337
		f 4 -2287 2338 2368 -2338
		f 4 -2289 2339 2369 -2339
		f 4 -2291 2340 2370 -2340
		f 4 -2293 2341 2371 -2341
		f 4 -2295 2342 2372 -2342
		f 4 -2297 2343 2373 -2343
		f 4 -2299 2344 2374 -2344
		f 4 -2301 2345 2375 -2345
		f 4 -2303 2346 2376 -2346
		f 4 -2305 2347 2377 -2347
		f 4 -2307 2348 2378 -2348
		f 4 -2309 2349 2379 -2349
		f 4 -2311 2350 2380 -2350
		f 4 -2313 2351 2381 -2351
		f 4 -2315 2352 2382 -2352
		f 4 -2317 2353 2383 -2353
		f 4 -2319 2354 2384 -2354
		f 4 -2321 2355 2385 -2355
		f 4 -2323 2356 2386 -2356
		f 4 -2325 2357 2387 -2357
		f 4 -2327 2358 2388 -2358
		f 4 -2329 2359 2389 -2359
		f 4 -2330 2331 2361 -2360
		f 4 2391 -1940 -2391 -1922
		f 4 2392 -1942 -2392 -1920
		f 4 -1944 -2393 -1918 2393
		f 3 -2394 -1947 -1946
		f 4 2395 -1934 -2395 -1928
		f 4 2396 -1936 -2396 -1926
		f 4 2390 -1938 -2397 -1924
		f 4 -2398 1891 2401 1905
		f 4 -2399 1889 2397 1907
		f 4 -2400 1887 2398 1909
		f 4 -2401 1885 2399 1911
		f 4 -2402 1893 2402 1903
		f 4 -2403 1895 2403 1901
		f 3 -2404 1897 1899
		f 3 2001 2406 -2405
		f 3 -2407 1989 2407
		f 3 -2408 1985 2408
		f 3 -2409 1987 2409
		f 3 -2410 2000 2410
		f 3 -2411 1998 -2406
		f 3 2412 -2412 2003
		f 3 2413 -2413 2005
		f 3 2414 -2414 2006
		f 3 2415 -2415 1995
		f 3 2404 -2416 1999
		f 3 -798 2418 -2417
		f 3 -2419 -794 2419
		f 3 -2420 -805 2420
		f 3 -2421 -804 2421
		f 3 -2422 -802 2422
		f 3 -2423 -792 -2418
		f 3 2424 -2424 -799
		f 3 2425 -2425 -786
		f 3 2426 -2426 -784
		f 3 2427 -2427 -788
		f 3 2416 -2428 -800
		f 3 -197 2430 -2429
		f 3 -2431 -193 2431
		f 3 -2432 -204 2432
		f 3 -2433 -203 2433
		f 3 -2434 -201 2434
		f 3 -2435 -191 -2430
		f 3 2436 -2436 -198
		f 3 2437 -2437 -185
		f 3 2438 -2438 -183
		f 3 2439 -2439 -187
		f 3 2428 -2440 -199
		f 3 1400 2442 -2441
		f 3 -2443 1388 2443
		f 3 -2444 1384 2444
		f 3 -2445 1386 2445
		f 3 -2446 1399 2446
		f 3 -2447 1397 -2442
		f 3 2448 -2448 1402
		f 3 2449 -2449 1404
		f 3 2450 -2450 1405
		f 3 2451 -2451 1394
		f 3 2440 -2452 1398;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".vcs" 2;
createNode transform -n "MSH_wheelMount_tutorial" -p "GRP_utility_meshes";
	rename -uid "F330298F-4E26-FDC0-5C25-558BE6537C04";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode mesh -n "MSH_wheelMount_tutorialShape" -p "MSH_wheelMount_tutorial";
	rename -uid "39A9A748-418C-D7F7-4EA5-4B9633E29004";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".sdt" 0;
	setAttr ".ugsdt" no;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
createNode mesh -n "MSH_wheelMount_tutorialShapeOrig" -p "MSH_wheelMount_tutorial";
	rename -uid "E882EE90-4CD6-B2D2-47F3-8191407DFAC7";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 366 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.49999997 0.68843985 0.49999997
		 0.3125 0.54166663 0.68843985 0.375 0.3125 0.41666666 0.3125 0.41666666 0.68843985
		 0.375 0.68843985 0.45833331 0.3125 0.45833331 0.68843985 0.54166663 0.3125 0.58333331
		 0.3125 0.58333331 0.68843985 0.625 0.3125 0.625 0.68843985 0.42187503 0.020933509
		 0.57812506 0.020933539 0.5 0.15000001 0.34375 0.15624997 0.421875 0.29156646 0.578125
		 0.29156649 0.65625 0.15625 0.65625 0.84375 0.578125 0.97906649 0.421875 0.97906649
		 0.34375 0.84375 0.42187503 0.70843351 0.57812506 0.70843351 0.375 0.3125 0.41666666
		 0.3125 0.41666666 0.68843985 0.375 0.68843985 0.45833331 0.3125 0.45833331 0.68843985
		 0.49999997 0.3125 0.49999997 0.68843985 0.54166663 0.3125 0.54166663 0.68843985 0.58333331
		 0.3125 0.58333331 0.68843985 0.625 0.3125 0.625 0.68843985 0.375 0.3125 0.41666666
		 0.3125 0.41666666 0.68843985 0.375 0.68843985 0.45833331 0.3125 0.45833331 0.68843985
		 0.58333331 0.3125 0.625 0.3125 0.625 0.68843985 0.58333331 0.68843985 0.34375 0.15624997
		 0.42187503 0.020933509 0.57812506 0.020933539 0.65625 0.15625 0.65625 0.84375 0.578125
		 0.97906649 0.421875 0.97906649 0.34375 0.84375 0.375 0.3125 0.41666666 0.3125 0.41666666
		 0.68843985 0.375 0.68843985 0.45833331 0.3125 0.45833331 0.68843985 0.49999997 0.3125
		 0.49999997 0.68843985 0.54166663 0.3125 0.54166663 0.68843985 0.58333331 0.3125 0.58333331
		 0.68843985 0.625 0.3125 0.625 0.68843985 0.34375 0.15624997 0.42187503 0.020933509
		 0.57812506 0.020933539 0.65625 0.15625 0.65625 0.84375 0.578125 0.97906649 0.421875
		 0.97906649 0.34375 0.84375 0.42187503 0.70843351 0.57812506 0.70843351 0.578125 0.29156649
		 0.421875 0.29156646 0.375 0.3125 0.41666666 0.3125 0.41666666 0.68843985 0.375 0.68843985
		 0.45833331 0.3125 0.45833331 0.68843985 0.49999997 0.3125 0.49999997 0.68843985 0.54166663
		 0.3125 0.54166663 0.68843985 0.58333331 0.3125 0.58333331 0.68843985 0.625 0.3125
		 0.625 0.68843985 0.375 0.3125 0.41666666 0.3125 0.41666666 0.68843985 0.375 0.68843985
		 0.45833331 0.3125 0.45833331 0.68843985 0.58333331 0.3125 0.625 0.3125 0.625 0.68843985
		 0.58333331 0.68843985 0.34375 0.15624997 0.42187503 0.020933509 0.57812506 0.020933539
		 0.65625 0.15625 0.65625 0.84375 0.578125 0.97906649 0.421875 0.97906649 0.34375 0.84375
		 0.375 0.3125 0.41666666 0.3125 0.41666666 0.68843985 0.375 0.68843985 0.45833331
		 0.3125 0.45833331 0.68843985 0.49999997 0.3125 0.49999997 0.68843985 0.54166663 0.3125
		 0.54166663 0.68843985 0.58333331 0.3125 0.58333331 0.68843985 0.625 0.3125 0.625
		 0.68843985 0.375 0.3125 0.41666666 0.3125 0.41666666 0.68843985 0.375 0.68843985
		 0.45833331 0.3125 0.45833331 0.68843985 0.49999997 0.3125 0.49999997 0.68843985 0.54166663
		 0.3125 0.54166663 0.68843985 0.58333331 0.3125 0.58333331 0.68843985 0.625 0.3125
		 0.625 0.68843985 0.34375 0.15624997 0.42187503 0.020933509 0.57812506 0.020933539
		 0.65625 0.15625 0.65625 0.84375 0.578125 0.97906649 0.421875 0.97906649 0.34375 0.84375
		 0.42187503 0.70843351 0.57812506 0.70843351 0.578125 0.29156649 0.421875 0.29156646
		 0.375 0.3125 0.41666666 0.3125 0.41666666 0.68843985 0.375 0.68843985 0.45833331
		 0.3125 0.45833331 0.68843985 0.49999997 0.3125 0.49999997 0.68843985 0.54166663 0.3125
		 0.54166663 0.68843985 0.58333331 0.3125 0.58333331 0.68843985 0.625 0.3125 0.625
		 0.68843985 0.34375 0.15624997 0.42187503 0.020933509 0.57812506 0.020933539 0.65625
		 0.15625 0.65625 0.84375 0.578125 0.97906649 0.421875 0.97906649 0.34375 0.84375 0.42187503
		 0.70843351 0.57812506 0.70843351 0.578125 0.29156649 0.421875 0.29156646 0.49999997
		 0.68843985 0.49999997 0.3125 0.54166663 0.68843985 0.375 0.3125 0.41666666 0.3125
		 0.41666666 0.68843985 0.375 0.68843985 0.45833331 0.3125 0.45833331 0.68843985 0.54166663
		 0.3125 0.58333331 0.3125 0.58333331 0.68843985 0.625 0.3125 0.625 0.68843985 0.42187503
		 0.020933509 0.57812506 0.020933539 0.5 0.15000001 0.34375 0.15624997 0.421875 0.29156646
		 0.578125 0.29156649 0.65625 0.15625 0.65625 0.84375 0.578125 0.97906649 0.421875
		 0.97906649 0.34375 0.84375 0.42187503 0.70843351 0.57812506 0.70843351 0.375 0.3125
		 0.41666666 0.3125 0.41666666 0.68843985 0.375 0.68843985 0.45833331 0.3125 0.45833331
		 0.68843985 0.49999997 0.3125 0.49999997 0.68843985 0.54166663 0.3125 0.54166663 0.68843985
		 0.58333331 0.3125 0.58333331 0.68843985 0.625 0.3125 0.625 0.68843985 0.375 0.3125
		 0.41666666 0.3125 0.41666666 0.68843985 0.375 0.68843985 0.45833331 0.3125 0.45833331
		 0.68843985 0.58333331 0.3125 0.625 0.3125 0.625 0.68843985 0.58333331 0.68843985
		 0.34375 0.15624997 0.42187503 0.020933509 0.57812506 0.020933539 0.65625 0.15625
		 0.65625 0.84375 0.578125 0.97906649 0.421875 0.97906649 0.34375 0.84375 0.375 0.3125
		 0.41666666 0.3125 0.41666666 0.68843985 0.375 0.68843985 0.45833331 0.3125 0.45833331
		 0.68843985 0.49999997 0.3125 0.49999997 0.68843985;
	setAttr ".uvst[0].uvsp[250:365]" 0.54166663 0.3125 0.54166663 0.68843985 0.58333331
		 0.3125 0.58333331 0.68843985 0.625 0.3125 0.625 0.68843985 0.34375 0.15624997 0.42187503
		 0.020933509 0.57812506 0.020933539 0.65625 0.15625 0.65625 0.84375 0.578125 0.97906649
		 0.421875 0.97906649 0.34375 0.84375 0.42187503 0.70843351 0.57812506 0.70843351 0.578125
		 0.29156649 0.421875 0.29156646 0.375 0.3125 0.41666666 0.3125 0.41666666 0.68843985
		 0.375 0.68843985 0.45833331 0.3125 0.45833331 0.68843985 0.49999997 0.3125 0.49999997
		 0.68843985 0.54166663 0.3125 0.54166663 0.68843985 0.58333331 0.3125 0.58333331 0.68843985
		 0.625 0.3125 0.625 0.68843985 0.375 0.3125 0.41666666 0.3125 0.41666666 0.68843985
		 0.375 0.68843985 0.45833331 0.3125 0.45833331 0.68843985 0.58333331 0.3125 0.625
		 0.3125 0.625 0.68843985 0.58333331 0.68843985 0.34375 0.15624997 0.42187503 0.020933509
		 0.57812506 0.020933539 0.65625 0.15625 0.65625 0.84375 0.578125 0.97906649 0.421875
		 0.97906649 0.34375 0.84375 0.375 0.3125 0.41666666 0.3125 0.41666666 0.68843985 0.375
		 0.68843985 0.45833331 0.3125 0.45833331 0.68843985 0.49999997 0.3125 0.49999997 0.68843985
		 0.54166663 0.3125 0.54166663 0.68843985 0.58333331 0.3125 0.58333331 0.68843985 0.625
		 0.3125 0.625 0.68843985 0.375 0.3125 0.41666666 0.3125 0.41666666 0.68843985 0.375
		 0.68843985 0.45833331 0.3125 0.45833331 0.68843985 0.49999997 0.3125 0.49999997 0.68843985
		 0.54166663 0.3125 0.54166663 0.68843985 0.58333331 0.3125 0.58333331 0.68843985 0.625
		 0.3125 0.625 0.68843985 0.34375 0.15624997 0.42187503 0.020933509 0.57812506 0.020933539
		 0.65625 0.15625 0.65625 0.84375 0.578125 0.97906649 0.421875 0.97906649 0.34375 0.84375
		 0.42187503 0.70843351 0.57812506 0.70843351 0.578125 0.29156649 0.421875 0.29156646
		 0.375 0.3125 0.41666666 0.3125 0.41666666 0.68843985 0.375 0.68843985 0.45833331
		 0.3125 0.45833331 0.68843985 0.49999997 0.3125 0.49999997 0.68843985 0.54166663 0.3125
		 0.54166663 0.68843985 0.58333331 0.3125 0.58333331 0.68843985 0.625 0.3125 0.625
		 0.68843985 0.34375 0.15624997 0.42187503 0.020933509 0.57812506 0.020933539 0.65625
		 0.15625 0.65625 0.84375 0.578125 0.97906649 0.421875 0.97906649 0.34375 0.84375 0.42187503
		 0.70843351 0.57812506 0.70843351 0.578125 0.29156649 0.421875 0.29156646;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".sdt" 0;
	setAttr -s 202 ".vt";
	setAttr ".vt[0:165]"  55.78562927 45.099544525 96.29663086 55.78562164 45.099494934 93.23023987
		 55.78562164 50.021793365 93.23016357 55.78562927 50.021835327 96.29656982 53.1300354 45.099472046 91.69703674
		 53.1300354 50.021766663 91.69696045 50.47444916 45.099494934 93.23023987 50.47445679 50.021793365 93.23016357
		 50.4744606 45.099544525 96.29663086 53.13003922 45.099567413 97.82981873 53.13003922 50.021858215 97.82975006
		 50.47445297 50.021835327 96.29656982 53.1300354 45.099517822 94.76344299 61.50404358 31.36269569 -135.01473999
		 61.50402832 31.36269379 -139.21463013 61.50403976 34.99995041 -141.31469727 61.50403214 38.63721085 -139.21463013
		 61.50402069 38.63721085 -135.014770508 61.50404358 34.99994659 -132.91470337 70.1939621 31.36269569 -135.01473999
		 70.19394684 31.36269569 -139.21464539 70.1939621 34.99995041 -141.31469727 70.19393921 38.63721085 -139.21463013
		 70.1939621 38.63721085 -135.014755249 70.19393921 34.99994659 -132.91470337 62.081745148 30.76428413 -131.94647217
		 62.081737518 30.76428032 -142.28292847 53.13004303 30.76428032 -147.45120239 53.1300354 30.76428032 -126.77824402
		 62.081741333 39.23563385 -131.94644165 62.081729889 39.23564529 -142.28294373 53.13003922 39.23564529 -147.45120239
		 53.13003159 39.23563766 -126.77822876 59.93870544 28.68452454 -133.18371582 59.93870544 28.68451881 -141.045684814
		 53.13004303 28.68451881 -144.97665405 46.32137299 28.68451881 -141.045684814 46.32138062 28.68452072 -133.18370056
		 53.13004303 28.68451881 -129.25273132 59.93870926 41.46411133 -133.1836853 59.93870544 41.46412659 -141.045684814
		 53.13003922 41.46412277 -144.97665405 46.32138062 41.46412659 -141.045684814 46.32136536 41.46411133 -133.1836853
		 53.1300354 41.46411514 -129.25271606 61.50404358 31.36272621 123.42440033 61.50402832 31.36266136 119.22450256
		 61.50403976 34.99988556 117.12438965 61.50403214 38.63717651 119.22438812 61.50402069 38.63724136 123.424263
		 61.50404358 35.000007629395 125.52437592 70.1939621 31.36272621 123.4243927 70.19394684 31.36266327 119.22449493
		 70.1939621 34.99988556 117.12438202 70.19393921 38.63717651 119.22438812 70.1939621 38.63724136 123.42427063
		 70.19393921 35.000007629395 125.52437592 62.081745148 30.76433945 126.49269867 62.081737518 30.76422119 116.15623474
		 53.13004303 30.76416397 110.98796082 53.1300354 30.76439667 131.66091919 62.081741333 39.23569107 126.49255371
		 62.081729889 39.23558807 116.15605164 53.13003922 39.23552704 110.98778534 53.13003159 39.23575211 131.6607666
		 57.096595764 32.14298248 114.38000488 54.89950562 32.14299774 114.38001251 53.80096436 34.99995804 114.37999725
		 54.89950562 37.85694122 114.38001251 57.09658432 37.85694122 114.38001251 58.19512558 34.99995804 114.38000488
		 57.096603394 32.14298248 97.21422577 54.89951324 32.14299393 97.2142334 53.80097961 34.99995804 97.2142334
		 54.89951706 37.85694122 97.21422577 57.096599579 37.85694122 97.2142334 58.19513321 34.99995804 97.2142334
		 58.62298584 30.050359726 97.93500519 58.62298584 30.050264359 91.5923233 53.13004303 30.050216675 88.4209671
		 47.63709259 30.050264359 91.59231567 47.63710022 30.050359726 97.93500519 53.13004303 30.05040741 101.10631561
		 58.62298965 45.20827484 97.93479156 58.62297821 45.20819473 91.59208679 53.13003922 45.20814514 88.42073822
		 47.63710785 45.20819473 91.59208679 47.63708496 45.20827484 97.93478394 53.1300354 45.20832443 101.10611725
		 59.93870544 28.68457222 125.25549316 59.93870544 28.68446732 117.39351654 53.13004303 28.68441772 113.46253967
		 46.32137299 28.68446732 117.39351654 46.32138062 28.68456841 125.25550079 53.13004303 28.68461418 129.1864624
		 59.93870926 41.4641571 125.25527191 59.93870544 41.464077 117.39328003 53.13003922 41.4640274 113.46230316
		 46.32138062 41.464077 117.39328003 46.32136536 41.4641571 125.25527191 53.1300354 41.46421051 129.18624878
		 -50.47445679 50.021793365 93.23016357 -50.47444916 45.099494934 93.23023987 -50.47445297 50.021835327 96.29656982
		 -55.78562927 45.099544525 96.29663086 -55.78562164 45.099494934 93.23023987 -55.78562164 50.021793365 93.23016357
		 -55.78562927 50.021835327 96.29656982 -53.1300354 45.099472046 91.69703674 -53.1300354 50.021766663 91.69696045
		 -50.4744606 45.099544525 96.29663086 -53.13003922 45.099567413 97.82981873 -53.13003922 50.021858215 97.82975006
		 -53.1300354 45.099517822 94.76344299 -61.50404358 31.36269569 -135.01473999 -61.50402832 31.36269379 -139.21463013
		 -70.19394684 31.36269569 -139.21464539 -70.1939621 31.36269569 -135.01473999 -61.50403976 34.99995041 -141.31469727
		 -70.1939621 34.99995041 -141.31469727 -61.50403214 38.63721085 -139.21463013 -70.19393921 38.63721085 -139.21463013
		 -61.50402069 38.63721085 -135.014770508 -70.1939621 38.63721085 -135.014755249 -61.50404358 34.99994659 -132.91470337
		 -70.19393921 34.99994659 -132.91470337 -62.081745148 30.76428413 -131.94647217 -62.081737518 30.76428032 -142.28292847
		 -62.081729889 39.23564529 -142.28294373 -62.081741333 39.23563385 -131.94644165 -53.13004303 30.76428032 -147.45120239
		 -53.13003922 39.23564529 -147.45120239 -53.1300354 30.76428032 -126.77824402 -53.13003159 39.23563766 -126.77822876
		 -59.93870544 28.68452454 -133.18371582 -59.93870544 28.68451881 -141.045684814 -59.93870544 41.46412659 -141.045684814
		 -59.93870926 41.46411133 -133.1836853 -53.13004303 28.68451881 -144.97665405 -53.13003922 41.46412277 -144.97665405
		 -46.32137299 28.68451881 -141.045684814 -46.32138062 41.46412659 -141.045684814 -46.32138062 28.68452072 -133.18370056
		 -46.32136536 41.46411133 -133.1836853 -53.13004303 28.68451881 -129.25273132 -53.1300354 41.46411514 -129.25271606
		 -61.50404358 31.36272621 123.42440033 -61.50402832 31.36266136 119.22450256 -70.19394684 31.36266327 119.22449493
		 -70.1939621 31.36272621 123.4243927 -61.50403976 34.99988556 117.12438965 -70.1939621 34.99988556 117.12438202
		 -61.50403214 38.63717651 119.22438812 -70.19393921 38.63717651 119.22438812 -61.50402069 38.63724136 123.424263
		 -70.1939621 38.63724136 123.42427063 -61.50404358 35.000007629395 125.52437592 -70.19393921 35.000007629395 125.52437592
		 -62.081745148 30.76433945 126.49269867 -62.081737518 30.76422119 116.15623474 -62.081729889 39.23558807 116.15605164
		 -62.081741333 39.23569107 126.49255371 -53.13004303 30.76416397 110.98796082 -53.13003922 39.23552704 110.98778534
		 -53.1300354 30.76439667 131.66091919 -53.13003159 39.23575211 131.6607666;
	setAttr ".vt[166:201]" -57.096595764 32.14298248 114.38000488 -54.89950562 32.14299774 114.38001251
		 -54.89951324 32.14299393 97.2142334 -57.096603394 32.14298248 97.21422577 -53.80096436 34.99995804 114.37999725
		 -53.80097961 34.99995804 97.2142334 -54.89950562 37.85694122 114.38001251 -54.89951706 37.85694122 97.21422577
		 -57.09658432 37.85694122 114.38001251 -57.096599579 37.85694122 97.2142334 -58.19512558 34.99995804 114.38000488
		 -58.19513321 34.99995804 97.2142334 -58.62298584 30.050359726 97.93500519 -58.62298584 30.050264359 91.5923233
		 -58.62297821 45.20819473 91.59208679 -58.62298965 45.20827484 97.93479156 -53.13004303 30.050216675 88.4209671
		 -53.13003922 45.20814514 88.42073822 -47.63709259 30.050264359 91.59231567 -47.63710785 45.20819473 91.59208679
		 -47.63710022 30.050359726 97.93500519 -47.63708496 45.20827484 97.93478394 -53.13004303 30.05040741 101.10631561
		 -53.1300354 45.20832443 101.10611725 -59.93870544 28.68457222 125.25549316 -59.93870544 28.68446732 117.39351654
		 -59.93870544 41.464077 117.39328003 -59.93870926 41.4641571 125.25527191 -53.13004303 28.68441772 113.46253967
		 -53.13003922 41.4640274 113.46230316 -46.32137299 28.68446732 117.39351654 -46.32138062 41.464077 117.39328003
		 -46.32138062 28.68456841 125.25550079 -46.32136536 41.4641571 125.25527191 -53.13004303 28.68461418 129.1864624
		 -53.1300354 41.46421051 129.18624878;
	setAttr -s 330 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 3 2 0 0 3 0 1 4 0 4 5 0 2 5 0 4 6 0 6 7 0
		 5 7 0 8 9 0 9 10 0 11 10 0 8 11 0 9 0 0 10 3 0 12 0 1 12 1 1 12 4 1 12 6 1 6 8 0
		 12 8 1 12 9 1 5 10 0 7 11 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0 18 13 0 19 20 0
		 20 21 0 21 22 0 22 23 0 23 24 0 24 19 0 13 19 1 14 20 1 15 21 0 16 22 1 17 23 1 18 24 0
		 25 26 0 26 27 0 28 25 0 29 30 0 30 31 0 32 29 0 25 29 0 26 30 0 27 31 0 28 32 0 31 32 0
		 27 28 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 33 0 39 40 0 40 41 0 41 42 0 43 44 0
		 44 39 0 33 39 0 34 40 0 35 41 0 36 42 0 37 43 0 38 44 0 41 44 1 42 43 0 35 38 1 45 46 0
		 46 47 0 47 48 0 48 49 0 49 50 0 50 45 0 51 52 0 52 53 0 53 54 0 54 55 0 55 56 0 56 51 0
		 45 51 0 46 52 0 47 53 0 48 54 0 49 55 0 50 56 0 57 58 0 58 59 0 60 57 0 61 62 0 62 63 0
		 64 61 0 57 61 0 58 62 0 59 63 0 60 64 0 63 64 0 65 66 0 66 67 0 67 68 0 68 69 0 69 70 0
		 70 65 0 71 72 0 72 73 0 73 74 0 74 75 0 75 76 0 76 71 0 65 71 0 66 72 0 67 73 0 68 74 0
		 69 75 0 70 76 0 77 78 0 78 79 0 79 80 0 80 81 0 81 82 0 82 77 0 83 84 0 84 85 0 85 86 0
		 87 88 0 88 83 0 77 83 0 78 84 0 79 85 0 80 86 0 81 87 0 82 88 0 85 88 0 86 87 0 79 82 0
		 59 60 0 89 90 0 90 91 0 91 92 0 92 93 0 93 94 0 94 89 0 95 96 0 96 97 0 97 98 0 99 100 0
		 100 95 0 89 95 0 90 96 0 91 97 0 92 98 0 93 99 0 94 100 0 97 100 1 98 99 0 91 94 1
		 101 7 0 102 101 0 102 6 0;
	setAttr ".ed[166:329]" 103 11 0 101 103 0 104 105 0 105 106 0 107 106 0 104 107 0
		 105 108 0 108 109 0 106 109 0 108 102 0 109 101 0 110 111 0 111 112 0 103 112 0 110 103 0
		 111 104 0 112 107 0 113 104 1 113 105 1 113 108 1 113 102 1 102 110 0 113 110 1 113 111 1
		 109 112 0 110 8 0 114 115 0 115 116 1 117 116 0 114 117 1 115 118 0 118 119 0 116 119 0
		 118 120 0 120 121 1 119 121 0 120 122 0 122 123 1 121 123 0 122 124 0 124 125 0 123 125 0
		 124 114 0 125 117 0 126 127 0 127 128 0 129 128 0 126 129 0 127 130 0 130 131 0 128 131 0
		 132 126 0 133 129 0 132 133 0 130 132 0 131 133 0 134 135 0 135 136 0 137 136 0 134 137 0
		 135 138 0 138 139 0 136 139 0 138 140 0 140 141 0 139 141 0 140 142 0 142 143 0 141 143 0
		 142 144 0 144 145 0 143 145 0 144 134 0 145 137 0 138 144 1 139 145 1 146 147 0 147 148 0
		 149 148 0 146 149 0 147 150 0 150 151 0 148 151 0 150 152 0 152 153 0 151 153 0 152 154 0
		 154 155 0 153 155 0 154 156 0 156 157 0 155 157 0 156 146 0 157 149 0 158 159 0 159 160 0
		 161 160 0 158 161 0 159 162 0 162 163 0 160 163 0 164 158 0 165 161 0 164 165 0 162 164 0
		 163 165 0 166 167 0 167 168 0 169 168 0 166 169 0 167 170 0 170 171 0 168 171 0 170 172 0
		 172 173 0 171 173 0 172 174 0 174 175 0 173 175 0 174 176 0 176 177 0 175 177 0 176 166 0
		 177 169 0 178 179 0 179 180 0 181 180 0 178 181 0 179 182 0 182 183 0 180 183 0 182 184 0
		 184 185 0 183 185 0 184 186 0 186 187 0 185 187 0 186 188 0 188 189 0 187 189 0 188 178 0
		 189 181 0 182 188 0 183 189 0 190 191 0 191 192 0 193 192 0 190 193 0 191 194 0 194 195 0
		 192 195 0 194 196 0 196 197 0 195 197 0 196 198 0 198 199 0 197 199 0 198 200 0 200 201 0
		 199 201 0 200 190 0 201 193 0 194 200 1 195 201 1;
	setAttr -s 150 -ch 588 ".fc[0:149]" -type "polyFaces" 
		f 4 -9 -166 164 163
		mu 0 4 0 1 184 183
		f 4 -25 -164 167 166
		mu 0 4 2 0 183 185
		f 4 0 1 -3 -4
		mu 0 4 3 4 5 6
		f 4 4 5 -7 -2
		mu 0 4 4 7 8 5
		f 4 7 8 -10 -6
		mu 0 4 7 1 0 8
		f 4 10 11 -13 -14
		mu 0 4 9 10 11 2
		f 4 14 3 -16 -12
		mu 0 4 10 12 13 11
		f 3 -1 -17 17
		mu 0 3 14 15 16
		f 3 -5 -18 18
		mu 0 3 17 14 16
		f 3 -8 -19 19
		mu 0 3 18 17 16
		f 3 -21 -20 21
		mu 0 3 19 18 16
		f 3 -11 -22 22
		mu 0 3 20 19 16
		f 3 -15 -23 16
		mu 0 3 15 20 16
		f 4 15 2 6 23
		mu 0 4 21 22 23 24
		f 4 -24 9 24 12
		mu 0 4 21 24 25 26
		f 4 13 -167 -181 191
		mu 0 4 9 2 185 192
		f 4 20 -192 -188 165
		mu 0 4 1 9 192 184
		f 4 25 38 -32 -38
		mu 0 4 27 28 29 30
		f 4 26 39 -33 -39
		mu 0 4 28 31 32 29
		f 4 27 40 -34 -40
		mu 0 4 31 33 34 32
		f 4 28 41 -35 -41
		mu 0 4 33 35 36 34
		f 4 29 42 -36 -42
		mu 0 4 35 37 38 36
		f 4 30 37 -37 -43
		mu 0 4 37 39 40 38
		f 4 43 50 -47 -50
		mu 0 4 41 42 43 44
		f 4 44 51 -48 -51
		mu 0 4 42 45 46 43
		f 4 45 49 -49 -53
		mu 0 4 47 48 49 50
		f 4 -45 -44 -46 -55
		mu 0 4 51 52 53 54
		f 4 48 46 47 53
		mu 0 4 55 56 57 58
		f 4 55 67 -62 -67
		mu 0 4 59 60 61 62
		f 4 56 68 -63 -68
		mu 0 4 60 63 64 61
		f 4 57 69 -64 -69
		mu 0 4 63 65 66 64
		f 4 58 70 -74 -70
		mu 0 4 65 67 68 66
		f 4 59 71 -65 -71
		mu 0 4 67 69 70 68
		f 4 60 66 -66 -72
		mu 0 4 69 71 72 70
		f 4 -57 -56 -61 -75
		mu 0 4 73 74 75 76
		f 4 65 61 62 72
		mu 0 4 77 78 79 80
		f 4 -73 63 73 64
		mu 0 4 77 80 81 82
		f 4 -59 -58 74 -60
		mu 0 4 83 84 73 76
		f 4 75 88 -82 -88
		mu 0 4 85 86 87 88
		f 4 76 89 -83 -89
		mu 0 4 86 89 90 87
		f 4 77 90 -84 -90
		mu 0 4 89 91 92 90
		f 4 78 91 -85 -91
		mu 0 4 91 93 94 92
		f 4 79 92 -86 -92
		mu 0 4 93 95 96 94
		f 4 80 87 -87 -93
		mu 0 4 95 97 98 96
		f 4 93 100 -97 -100
		mu 0 4 99 100 101 102
		f 4 94 101 -98 -101
		mu 0 4 100 103 104 101
		f 4 95 99 -99 -103
		mu 0 4 105 106 107 108
		f 4 -95 -94 -96 -143
		mu 0 4 109 110 111 112
		f 4 98 96 97 103
		mu 0 4 113 114 115 116
		f 4 104 117 -111 -117
		mu 0 4 117 118 119 120
		f 4 105 118 -112 -118
		mu 0 4 118 121 122 119
		f 4 106 119 -113 -119
		mu 0 4 121 123 124 122
		f 4 107 120 -114 -120
		mu 0 4 123 125 126 124
		f 4 108 121 -115 -121
		mu 0 4 125 127 128 126
		f 4 109 116 -116 -122
		mu 0 4 127 129 130 128
		f 4 122 134 -129 -134
		mu 0 4 131 132 133 134
		f 4 123 135 -130 -135
		mu 0 4 132 135 136 133
		f 4 124 136 -131 -136
		mu 0 4 135 137 138 136
		f 4 125 137 -141 -137
		mu 0 4 137 139 140 138
		f 4 126 138 -132 -138
		mu 0 4 139 141 142 140
		f 4 127 133 -133 -139
		mu 0 4 141 143 144 142
		f 4 -124 -123 -128 -142
		mu 0 4 145 146 147 148
		f 4 132 128 129 139
		mu 0 4 149 150 151 152
		f 4 -140 130 140 131
		mu 0 4 149 152 153 154
		f 4 -126 -125 141 -127
		mu 0 4 155 156 145 148
		f 4 143 155 -150 -155
		mu 0 4 157 158 159 160
		f 4 144 156 -151 -156
		mu 0 4 158 161 162 159
		f 4 145 157 -152 -157
		mu 0 4 161 163 164 162
		f 4 146 158 -162 -158
		mu 0 4 163 165 166 164
		f 4 147 159 -153 -159
		mu 0 4 165 167 168 166
		f 4 148 154 -154 -160
		mu 0 4 167 169 170 168
		f 4 -145 -144 -149 -163
		mu 0 4 171 172 173 174
		f 4 153 149 150 160
		mu 0 4 175 176 177 178
		f 4 -161 151 161 152
		mu 0 4 175 178 179 180
		f 4 -147 -146 162 -148
		mu 0 4 181 182 171 174
		f 4 -52 54 52 -54
		mu 0 4 46 51 47 55
		f 4 -102 142 102 -104
		mu 0 4 104 109 105 113
		f 4 171 170 -170 -169
		mu 0 4 186 189 188 187
		f 4 169 174 -174 -173
		mu 0 4 187 188 191 190
		f 4 173 176 -165 -176
		mu 0 4 190 191 183 184
		f 4 180 179 -179 -178
		mu 0 4 192 185 194 193
		f 4 178 182 -172 -182
		mu 0 4 193 194 196 195
		f 3 -185 183 168
		mu 0 3 197 199 198
		f 3 -186 184 172
		mu 0 3 200 199 197
		f 3 -187 185 175
		mu 0 3 201 199 200
		f 3 -189 186 187
		mu 0 3 202 199 201
		f 3 -190 188 177
		mu 0 3 203 199 202
		f 3 -184 189 181
		mu 0 3 198 199 203
		f 4 -191 -175 -171 -183
		mu 0 4 204 207 206 205
		f 4 -180 -168 -177 190
		mu 0 4 204 209 208 207
		f 4 195 194 -194 -193
		mu 0 4 210 213 212 211
		f 4 193 198 -198 -197
		mu 0 4 211 212 215 214
		f 4 197 201 -201 -200
		mu 0 4 214 215 217 216
		f 4 200 204 -204 -203
		mu 0 4 216 217 219 218
		f 4 203 207 -207 -206
		mu 0 4 218 219 221 220
		f 4 206 209 -196 -209
		mu 0 4 220 221 223 222
		f 4 213 212 -212 -211
		mu 0 4 224 227 226 225
		f 4 211 216 -216 -215
		mu 0 4 225 226 229 228
		f 4 219 218 -214 -218
		mu 0 4 230 233 232 231
		f 4 220 217 210 214
		mu 0 4 234 237 236 235
		f 4 -222 -217 -213 -219
		mu 0 4 238 241 240 239
		f 4 225 224 -224 -223
		mu 0 4 242 245 244 243
		f 4 223 228 -228 -227
		mu 0 4 243 244 247 246
		f 4 227 231 -231 -230
		mu 0 4 246 247 249 248
		f 4 230 234 -234 -233
		mu 0 4 248 249 251 250
		f 4 233 237 -237 -236
		mu 0 4 250 251 253 252
		f 4 236 239 -226 -239
		mu 0 4 252 253 255 254
		f 4 240 238 222 226
		mu 0 4 256 259 258 257
		f 4 -242 -229 -225 -240
		mu 0 4 260 263 262 261
		f 4 -238 -235 -232 241
		mu 0 4 260 265 264 263
		f 4 235 -241 229 232
		mu 0 4 266 259 256 267
		f 4 245 244 -244 -243
		mu 0 4 268 271 270 269
		f 4 243 248 -248 -247
		mu 0 4 269 270 273 272
		f 4 247 251 -251 -250
		mu 0 4 272 273 275 274
		f 4 250 254 -254 -253
		mu 0 4 274 275 277 276
		f 4 253 257 -257 -256
		mu 0 4 276 277 279 278
		f 4 256 259 -246 -259
		mu 0 4 278 279 281 280
		f 4 263 262 -262 -261
		mu 0 4 282 285 284 283
		f 4 261 266 -266 -265
		mu 0 4 283 284 287 286
		f 4 269 268 -264 -268
		mu 0 4 288 291 290 289
		f 4 270 267 260 264
		mu 0 4 292 295 294 293
		f 4 -272 -267 -263 -269
		mu 0 4 296 299 298 297
		f 4 275 274 -274 -273
		mu 0 4 300 303 302 301
		f 4 273 278 -278 -277
		mu 0 4 301 302 305 304
		f 4 277 281 -281 -280
		mu 0 4 304 305 307 306
		f 4 280 284 -284 -283
		mu 0 4 306 307 309 308
		f 4 283 287 -287 -286
		mu 0 4 308 309 311 310
		f 4 286 289 -276 -289
		mu 0 4 310 311 313 312
		f 4 293 292 -292 -291
		mu 0 4 314 317 316 315
		f 4 291 296 -296 -295
		mu 0 4 315 316 319 318
		f 4 295 299 -299 -298
		mu 0 4 318 319 321 320
		f 4 298 302 -302 -301
		mu 0 4 320 321 323 322
		f 4 301 305 -305 -304
		mu 0 4 322 323 325 324
		f 4 304 307 -294 -307
		mu 0 4 324 325 327 326
		f 4 308 306 290 294
		mu 0 4 328 331 330 329
		f 4 -310 -297 -293 -308
		mu 0 4 332 335 334 333
		f 4 -306 -303 -300 309
		mu 0 4 332 337 336 335
		f 4 303 -309 297 300
		mu 0 4 338 331 328 339
		f 4 313 312 -312 -311
		mu 0 4 340 343 342 341
		f 4 311 316 -316 -315
		mu 0 4 341 342 345 344
		f 4 315 319 -319 -318
		mu 0 4 344 345 347 346
		f 4 318 322 -322 -321
		mu 0 4 346 347 349 348
		f 4 321 325 -325 -324
		mu 0 4 348 349 351 350
		f 4 324 327 -314 -327
		mu 0 4 350 351 353 352
		f 4 328 326 310 314
		mu 0 4 354 357 356 355
		f 4 -330 -317 -313 -328
		mu 0 4 358 361 360 359
		f 4 -326 -323 -320 329
		mu 0 4 358 363 362 361
		f 4 323 -329 317 320
		mu 0 4 364 357 354 365
		f 4 221 -220 -221 215
		mu 0 4 229 238 230 234
		f 4 271 -270 -271 265
		mu 0 4 287 296 288 292;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".vcs" 2;
createNode transform -n "MSH_arms_tutorial" -p "GRP_utility_meshes";
	rename -uid "9997314A-4F91-A859-86B7-99B2D9ACA858";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode mesh -n "MSH_arms_tutorialShape" -p "MSH_arms_tutorial";
	rename -uid "D32B1B4A-49E1-54B9-CC5B-2A98A8E7F2BC";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".sdt" 0;
	setAttr ".ugsdt" no;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
createNode mesh -n "MSH_arms_tutorialShapeOrig" -p "MSH_arms_tutorial";
	rename -uid "6801D90E-4FB1-2D70-100B-5BBDAB99A149";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 1216 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.54166663 0.3125 0.54166663
		 0.3125 0.49999997 0.31250018 0.49999997 0.31250018 0.49999997 0.68843985 0.49999997
		 0.68843985 0.54166663 0.68843985 0.54166663 0.68843985 0.49999994 0.40210003 0.49999997
		 0.59883988 0.54166663 0.59883988 0.54166669 0.40209976 0.49999994 0.40210003 0.49999997
		 0.59883988 0.54166663 0.68843985 0.49999997 0.68843985 0.54166663 0.59883988 0.54166669
		 0.40209976 0.49999997 0.31250018 0.54166663 0.3125 0.54166663 0.68843985 0.49999997
		 0.68843985 0.49999997 0.31250018 0.54166663 0.3125 0.375 0.3125 0.41666666 0.3125
		 0.41666666 0.68843985 0.375 0.68843985 0.45833331 0.3125 0.45833331 0.68843985 0.49999997
		 0.3125 0.49999997 0.68843985 0.54166663 0.3125 0.54166663 0.68843985 0.58333331 0.3125
		 0.58333331 0.68843985 0.625 0.3125 0.625 0.68843985 0.34375 0.15624997 0.42187503
		 0.020933509 0.57812506 0.020933539 0.65625 0.15625 0.65625 0.84375 0.578125 0.97906649
		 0.421875 0.97906649 0.34375 0.84375 0.42187503 0.70843351 0.57812506 0.70843351 0.578125
		 0.29156649 0.421875 0.29156646 0.65625 0.15625 0.57812506 0.020933539 0.42187503
		 0.020933509 0.34375 0.15624997 0.34375 0.84375 0.421875 0.97906649 0.578125 0.97906649
		 0.65625 0.84375 0.57812506 0.70843351 0.42187503 0.70843351 0.578125 0.29156649 0.421875
		 0.29156646 0.34375 0.84375 0.421875 0.97906649 0.421875 0.97906649 0.421875 0.97906649
		 0.578125 0.97906649 0.578125 0.97906649 0.65625 0.84375 0.65625 0.84375 0.65625 0.84375
		 0.57812506 0.70843351 0.57812506 0.70843351 0.42187503 0.70843351 0.42187503 0.70843351
		 0.34375 0.84375 0.34375 0.84375 0.578125 0.29156649 0.65625 0.15625 0.65625 0.15625
		 0.34375 0.15624997 0.34375 0.15624997 0.421875 0.29156646 0.421875 0.29156646 0.578125
		 0.29156649 0.578125 0.29156649 0.375 0.3125 0.41666666 0.3125 0.41666666 0.68843985
		 0.375 0.68843985 0.45833331 0.3125 0.45833331 0.68843985 0.49999997 0.3125 0.49999997
		 0.68843985 0.54166663 0.3125 0.54166663 0.68843985 0.58333331 0.3125 0.58333331 0.68843985
		 0.625 0.3125 0.625 0.68843985 0.34375 0.15624997 0.42187503 0.020933509 0.57812506
		 0.020933539 0.65625 0.15625 0.65625 0.84375 0.578125 0.97906649 0.421875 0.97906649
		 0.34375 0.84375 0.42187503 0.70843351 0.57812506 0.70843351 0.578125 0.29156649 0.421875
		 0.29156646 0.65625 0.15625 0.57812506 0.020933539 0.42187503 0.020933509 0.34375
		 0.15624997 0.34375 0.84375 0.34375 0.84375 0.421875 0.97906649 0.421875 0.97906649
		 0.421875 0.97906649 0.421875 0.97906649 0.578125 0.97906649 0.578125 0.97906649 0.578125
		 0.97906649 0.65625 0.84375 0.65625 0.84375 0.65625 0.84375 0.65625 0.84375 0.57812506
		 0.70843351 0.57812506 0.70843351 0.57812506 0.70843351 0.42187503 0.70843351 0.42187503
		 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.34375 0.84375 0.578125 0.29156649
		 0.578125 0.29156649 0.65625 0.15625 0.65625 0.15625 0.34375 0.15624997 0.34375 0.15624997
		 0.421875 0.29156646 0.421875 0.29156646 0.421875 0.29156646 0.578125 0.29156649 0.578125
		 0.29156649 0.54166663 0.3125 0.54166663 0.3125 0.5 0.31250015 0.49999997 0.31250018
		 0.49999997 0.68843985 0.49999997 0.68843991 0.54166663 0.68843943 0.54166663 0.68843985
		 0.49999994 0.40210003 0.49999997 0.59883988 0.54166663 0.59883988 0.54166669 0.40209976
		 0.54166663 0.40209997 0.54166663 0.59883976 0.49999997 0.59883982 0.49999994 0.40210009
		 0.49999997 0.68843985 0.54166663 0.68843985 0.54166663 0.68843985 0.49999997 0.68843985
		 0.49999997 0.31250018 0.54166663 0.3125 0.54166663 0.3125 0.49999997 0.31250015 0.375
		 0.3125 0.375 0.68843985 0.41666666 0.68843985 0.41666666 0.3125 0.45833331 0.68843985
		 0.45833331 0.3125 0.49999997 0.68843985 0.49999997 0.3125 0.54166663 0.68843985 0.54166663
		 0.3125 0.58333331 0.68843985 0.58333331 0.3125 0.625 0.68843985 0.625 0.3125 0.578125
		 0.29156649 0.65625 0.15625 0.34375 0.15624997 0.421875 0.29156646 0.65625 0.84375
		 0.34375 0.84375 0.421875 0.97906649 0.578125 0.97906649 0.57812506 0.020933539 0.42187503
		 0.020933509 0.57812506 0.70843351 0.42187503 0.70843351 0.578125 0.29156649 0.421875
		 0.29156646 0.34375 0.15624997 0.65625 0.15625 0.65625 0.84375 0.65625 0.84375 0.578125
		 0.97906649 0.578125 0.97906649 0.578125 0.97906649 0.421875 0.97906649 0.421875 0.97906649
		 0.421875 0.97906649 0.34375 0.84375 0.34375 0.84375 0.34375 0.15624997 0.34375 0.15624997
		 0.42187503 0.020933509 0.42187503 0.020933509 0.42187503 0.020933509 0.57812506 0.020933539
		 0.57812506 0.020933539 0.57812506 0.020933539 0.65625 0.15625 0.65625 0.15625 0.34375
		 0.84375 0.42187503 0.70843351 0.57812506 0.70843351 0.65625 0.84375 0.54166663 0.3125
		 0.54166663 0.3125 0.5 0.31250015 0.49999997 0.31250018 0.49999997 0.68843985 0.49999997
		 0.68843991 0.54166663 0.68843943 0.54166663 0.68843985 0.49999994 0.40210003 0.49999997
		 0.59883988 0.54166663 0.59883988 0.54166669 0.40209976 0.54166663 0.40209997 0.54166663
		 0.59883976 0.49999997 0.59883982 0.49999994 0.40210009 0.49999997 0.68843985 0.54166663
		 0.68843985 0.54166663 0.68843985 0.49999997 0.68843985 0.49999997 0.31250018 0.54166663
		 0.3125 0.54166663 0.3125 0.49999997 0.31250015;
	setAttr ".uvst[0].uvsp[250:499]" 0.375 0.3125 0.375 0.68843985 0.41666666 0.68843985
		 0.41666666 0.3125 0.45833331 0.68843985 0.45833331 0.3125 0.49999997 0.68843985 0.49999997
		 0.3125 0.54166663 0.68843985 0.54166663 0.3125 0.58333331 0.68843985 0.58333331 0.3125
		 0.625 0.68843985 0.625 0.3125 0.578125 0.29156649 0.65625 0.15625 0.34375 0.15624997
		 0.421875 0.29156646 0.65625 0.84375 0.34375 0.84375 0.421875 0.97906649 0.578125
		 0.97906649 0.57812506 0.020933539 0.42187503 0.020933509 0.57812506 0.70843351 0.42187503
		 0.70843351 0.578125 0.29156649 0.421875 0.29156646 0.34375 0.15624997 0.65625 0.15625
		 0.65625 0.84375 0.65625 0.84375 0.578125 0.97906649 0.578125 0.97906649 0.578125
		 0.97906649 0.421875 0.97906649 0.421875 0.97906649 0.421875 0.97906649 0.34375 0.84375
		 0.34375 0.84375 0.34375 0.15624997 0.34375 0.15624997 0.42187503 0.020933509 0.42187503
		 0.020933509 0.42187503 0.020933509 0.57812506 0.020933539 0.57812506 0.020933539
		 0.57812506 0.020933539 0.65625 0.15625 0.65625 0.15625 0.34375 0.84375 0.42187503
		 0.70843351 0.57812506 0.70843351 0.65625 0.84375 0.54166663 0.3125 0.54166663 0.3125
		 0.49999997 0.31250018 0.49999997 0.31250018 0.49999997 0.68843985 0.49999997 0.68843985
		 0.54166663 0.68843985 0.54166663 0.68843985 0.49999994 0.40210003 0.49999997 0.59883988
		 0.54166663 0.59883988 0.54166669 0.40209976 0.49999994 0.40210003 0.49999997 0.59883988
		 0.54166663 0.68843985 0.49999997 0.68843985 0.54166663 0.59883988 0.54166669 0.40209976
		 0.49999997 0.31250018 0.54166663 0.3125 0.54166663 0.68843985 0.49999997 0.68843985
		 0.49999997 0.31250018 0.54166663 0.3125 0.375 0.3125 0.41666666 0.3125 0.41666666
		 0.68843985 0.375 0.68843985 0.45833331 0.3125 0.45833331 0.68843985 0.49999997 0.3125
		 0.49999997 0.68843985 0.54166663 0.3125 0.54166663 0.68843985 0.58333331 0.3125 0.58333331
		 0.68843985 0.625 0.3125 0.625 0.68843985 0.34375 0.15624997 0.42187503 0.020933509
		 0.57812506 0.020933539 0.65625 0.15625 0.65625 0.84375 0.578125 0.97906649 0.421875
		 0.97906649 0.34375 0.84375 0.42187503 0.70843351 0.57812506 0.70843351 0.578125 0.29156649
		 0.421875 0.29156646 0.65625 0.15625 0.57812506 0.020933539 0.42187503 0.020933509
		 0.34375 0.15624997 0.34375 0.84375 0.34375 0.84375 0.421875 0.97906649 0.421875 0.97906649
		 0.421875 0.97906649 0.421875 0.97906649 0.578125 0.97906649 0.578125 0.97906649 0.578125
		 0.97906649 0.65625 0.84375 0.65625 0.84375 0.65625 0.84375 0.65625 0.84375 0.57812506
		 0.70843351 0.57812506 0.70843351 0.57812506 0.70843351 0.42187503 0.70843351 0.42187503
		 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.34375 0.84375 0.578125 0.29156649
		 0.578125 0.29156649 0.65625 0.15625 0.65625 0.15625 0.34375 0.15624997 0.34375 0.15624997
		 0.421875 0.29156646 0.421875 0.29156646 0.421875 0.29156646 0.578125 0.29156649 0.578125
		 0.29156649 0.54166663 0.3125 0.54166663 0.3125 0.5 0.31250015 0.49999997 0.31250018
		 0.49999997 0.68843985 0.49999997 0.68843991 0.54166663 0.68843943 0.54166663 0.68843985
		 0.49999994 0.40210003 0.49999997 0.59883988 0.54166663 0.59883988 0.54166669 0.40209976
		 0.54166663 0.40209997 0.54166663 0.59883976 0.49999997 0.59883982 0.49999994 0.40210009
		 0.49999997 0.68843985 0.54166663 0.68843985 0.54166663 0.68843985 0.49999997 0.68843985
		 0.49999997 0.31250018 0.54166663 0.3125 0.54166663 0.3125 0.49999997 0.31250015 0.375
		 0.3125 0.375 0.68843985 0.41666666 0.68843985 0.41666666 0.3125 0.45833331 0.68843985
		 0.45833331 0.3125 0.49999997 0.68843985 0.49999997 0.3125 0.54166663 0.68843985 0.54166663
		 0.3125 0.58333331 0.68843985 0.58333331 0.3125 0.625 0.68843985 0.625 0.3125 0.578125
		 0.29156649 0.65625 0.15625 0.34375 0.15624997 0.421875 0.29156646 0.65625 0.84375
		 0.34375 0.84375 0.421875 0.97906649 0.578125 0.97906649 0.57812506 0.020933539 0.42187503
		 0.020933509 0.57812506 0.70843351 0.42187503 0.70843351 0.578125 0.29156649 0.421875
		 0.29156646 0.34375 0.15624997 0.65625 0.15625 0.65625 0.84375 0.65625 0.84375 0.578125
		 0.97906649 0.578125 0.97906649 0.578125 0.97906649 0.421875 0.97906649 0.421875 0.97906649
		 0.421875 0.97906649 0.34375 0.84375 0.34375 0.84375 0.34375 0.15624997 0.34375 0.15624997
		 0.42187503 0.020933509 0.42187503 0.020933509 0.42187503 0.020933509 0.57812506 0.020933539
		 0.57812506 0.020933539 0.57812506 0.020933539 0.65625 0.15625 0.65625 0.15625 0.34375
		 0.84375 0.42187503 0.70843351 0.57812506 0.70843351 0.65625 0.84375 0.375 0.3125
		 0.41666666 0.3125 0.41666666 0.68843985 0.375 0.68843985 0.45833331 0.3125 0.45833331
		 0.68843985 0.49999997 0.3125 0.49999997 0.68843985 0.54166663 0.3125 0.54166663 0.68843985
		 0.58333331 0.3125 0.58333331 0.68843985 0.625 0.3125 0.625 0.68843985 0.34375 0.15624997
		 0.42187503 0.020933509 0.57812506 0.020933539 0.65625 0.15625 0.65625 0.84375 0.578125
		 0.97906649 0.421875 0.97906649 0.34375 0.84375 0.42187503 0.70843351 0.57812506 0.70843351
		 0.578125 0.29156649 0.421875 0.29156646 0.65625 0.15625 0.57812506 0.020933539 0.42187503
		 0.020933509 0.34375 0.15624997 0.34375 0.84375 0.34375 0.84375;
	setAttr ".uvst[0].uvsp[500:749]" 0.421875 0.97906649 0.421875 0.97906649 0.421875
		 0.97906649 0.421875 0.97906649 0.578125 0.97906649 0.578125 0.97906649 0.578125 0.97906649
		 0.65625 0.84375 0.65625 0.84375 0.65625 0.84375 0.65625 0.84375 0.57812506 0.70843351
		 0.57812506 0.70843351 0.57812506 0.70843351 0.42187503 0.70843351 0.42187503 0.70843351
		 0.42187503 0.70843351 0.34375 0.84375 0.34375 0.84375 0.578125 0.29156649 0.578125
		 0.29156649 0.65625 0.15625 0.65625 0.15625 0.34375 0.15624997 0.34375 0.15624997
		 0.421875 0.29156646 0.421875 0.29156646 0.421875 0.29156646 0.578125 0.29156649 0.578125
		 0.29156649 0.54166663 0.3125 0.54166663 0.3125 0.5 0.31250015 0.49999997 0.31250018
		 0.49999997 0.68843985 0.49999997 0.68843991 0.54166663 0.68843943 0.54166663 0.68843985
		 0.49999994 0.40210003 0.49999997 0.59883988 0.54166663 0.59883988 0.54166669 0.40209976
		 0.54166663 0.40209997 0.54166663 0.59883976 0.49999997 0.59883982 0.49999994 0.40210009
		 0.49999997 0.68843985 0.54166663 0.68843985 0.54166663 0.68843985 0.49999997 0.68843985
		 0.49999997 0.31250018 0.54166663 0.3125 0.54166663 0.3125 0.49999997 0.31250015 0.375
		 0.3125 0.375 0.68843985 0.41666666 0.68843985 0.41666666 0.3125 0.45833331 0.68843985
		 0.45833331 0.3125 0.49999997 0.68843985 0.49999997 0.3125 0.54166663 0.68843985 0.54166663
		 0.3125 0.58333331 0.68843985 0.58333331 0.3125 0.625 0.68843985 0.625 0.3125 0.578125
		 0.29156649 0.65625 0.15625 0.34375 0.15624997 0.421875 0.29156646 0.65625 0.84375
		 0.34375 0.84375 0.421875 0.97906649 0.578125 0.97906649 0.57812506 0.020933539 0.42187503
		 0.020933509 0.57812506 0.70843351 0.42187503 0.70843351 0.578125 0.29156649 0.421875
		 0.29156646 0.34375 0.15624997 0.65625 0.15625 0.65625 0.84375 0.65625 0.84375 0.578125
		 0.97906649 0.578125 0.97906649 0.578125 0.97906649 0.421875 0.97906649 0.421875 0.97906649
		 0.421875 0.97906649 0.34375 0.84375 0.34375 0.84375 0.34375 0.15624997 0.34375 0.15624997
		 0.42187503 0.020933509 0.42187503 0.020933509 0.42187503 0.020933509 0.57812506 0.020933539
		 0.57812506 0.020933539 0.57812506 0.020933539 0.65625 0.15625 0.65625 0.15625 0.34375
		 0.84375 0.42187503 0.70843351 0.57812506 0.70843351 0.65625 0.84375 0.54166663 0.3125
		 0.54166663 0.3125 0.49999997 0.31250018 0.49999997 0.31250018 0.49999997 0.68843985
		 0.49999997 0.68843985 0.54166663 0.68843985 0.54166663 0.68843985 0.49999994 0.40210003
		 0.49999997 0.59883988 0.54166663 0.59883988 0.54166669 0.40209976 0.49999994 0.40210003
		 0.49999997 0.59883988 0.54166663 0.68843985 0.49999997 0.68843985 0.54166663 0.59883988
		 0.54166669 0.40209976 0.49999997 0.31250018 0.54166663 0.3125 0.54166663 0.68843985
		 0.49999997 0.68843985 0.49999997 0.31250018 0.54166663 0.3125 0.375 0.3125 0.41666666
		 0.3125 0.41666666 0.68843985 0.375 0.68843985 0.45833331 0.3125 0.45833331 0.68843985
		 0.49999997 0.3125 0.49999997 0.68843985 0.54166663 0.3125 0.54166663 0.68843985 0.58333331
		 0.3125 0.58333331 0.68843985 0.625 0.3125 0.625 0.68843985 0.34375 0.15624997 0.42187503
		 0.020933509 0.57812506 0.020933539 0.65625 0.15625 0.65625 0.84375 0.578125 0.97906649
		 0.421875 0.97906649 0.34375 0.84375 0.42187503 0.70843351 0.57812506 0.70843351 0.578125
		 0.29156649 0.421875 0.29156646 0.65625 0.15625 0.57812506 0.020933539 0.42187503
		 0.020933509 0.34375 0.15624997 0.34375 0.84375 0.34375 0.84375 0.421875 0.97906649
		 0.421875 0.97906649 0.421875 0.97906649 0.421875 0.97906649 0.578125 0.97906649 0.578125
		 0.97906649 0.578125 0.97906649 0.65625 0.84375 0.65625 0.84375 0.65625 0.84375 0.65625
		 0.84375 0.57812506 0.70843351 0.57812506 0.70843351 0.57812506 0.70843351 0.42187503
		 0.70843351 0.42187503 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.34375 0.84375
		 0.578125 0.29156649 0.578125 0.29156649 0.65625 0.15625 0.65625 0.15625 0.34375 0.15624997
		 0.34375 0.15624997 0.421875 0.29156646 0.421875 0.29156646 0.421875 0.29156646 0.578125
		 0.29156649 0.578125 0.29156649 0.375 0.3125 0.41666666 0.3125 0.41666666 0.68843985
		 0.375 0.68843985 0.45833331 0.3125 0.45833331 0.68843985 0.49999997 0.3125 0.49999997
		 0.68843985 0.54166663 0.3125 0.54166663 0.68843985 0.58333331 0.3125 0.58333331 0.68843985
		 0.625 0.3125 0.625 0.68843985 0.34375 0.15624997 0.42187503 0.020933509 0.57812506
		 0.020933539 0.65625 0.15625 0.65625 0.84375 0.578125 0.97906649 0.421875 0.97906649
		 0.34375 0.84375 0.42187503 0.70843351 0.57812506 0.70843351 0.578125 0.29156649 0.421875
		 0.29156646 0.65625 0.15625 0.57812506 0.020933539 0.42187503 0.020933509 0.34375
		 0.15624997 0.34375 0.84375 0.34375 0.84375 0.421875 0.97906649 0.421875 0.97906649
		 0.421875 0.97906649 0.421875 0.97906649 0.578125 0.97906649 0.578125 0.97906649 0.578125
		 0.97906649 0.65625 0.84375 0.65625 0.84375 0.65625 0.84375 0.65625 0.84375 0.57812506
		 0.70843351 0.57812506 0.70843351 0.57812506 0.70843351 0.42187503 0.70843351 0.42187503
		 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.34375 0.84375 0.578125 0.29156649
		 0.578125 0.29156649 0.65625 0.15625 0.65625 0.15625 0.34375 0.15624997;
	setAttr ".uvst[0].uvsp[750:999]" 0.34375 0.15624997 0.421875 0.29156646 0.421875
		 0.29156646 0.421875 0.29156646 0.578125 0.29156649 0.578125 0.29156649 0.54166663
		 0.3125 0.54166663 0.3125 0.5 0.31250015 0.49999997 0.31250018 0.49999997 0.68843985
		 0.49999997 0.68843991 0.54166663 0.68843943 0.54166663 0.68843985 0.49999994 0.40210003
		 0.49999997 0.59883988 0.54166663 0.59883988 0.54166669 0.40209976 0.54166663 0.40209997
		 0.54166663 0.59883976 0.49999997 0.59883982 0.49999994 0.40210009 0.49999997 0.68843985
		 0.54166663 0.68843985 0.54166663 0.68843985 0.49999997 0.68843985 0.49999997 0.31250018
		 0.54166663 0.3125 0.54166663 0.3125 0.49999997 0.31250015 0.375 0.3125 0.375 0.68843985
		 0.41666666 0.68843985 0.41666666 0.3125 0.45833331 0.68843985 0.45833331 0.3125 0.49999997
		 0.68843985 0.49999997 0.3125 0.54166663 0.68843985 0.54166663 0.3125 0.58333331 0.68843985
		 0.58333331 0.3125 0.625 0.68843985 0.625 0.3125 0.578125 0.29156649 0.65625 0.15625
		 0.34375 0.15624997 0.421875 0.29156646 0.65625 0.84375 0.34375 0.84375 0.421875 0.97906649
		 0.578125 0.97906649 0.57812506 0.020933539 0.42187503 0.020933509 0.57812506 0.70843351
		 0.42187503 0.70843351 0.578125 0.29156649 0.421875 0.29156646 0.34375 0.15624997
		 0.65625 0.15625 0.65625 0.84375 0.65625 0.84375 0.578125 0.97906649 0.578125 0.97906649
		 0.578125 0.97906649 0.421875 0.97906649 0.421875 0.97906649 0.421875 0.97906649 0.34375
		 0.84375 0.34375 0.84375 0.34375 0.15624997 0.34375 0.15624997 0.42187503 0.020933509
		 0.42187503 0.020933509 0.42187503 0.020933509 0.57812506 0.020933539 0.57812506 0.020933539
		 0.57812506 0.020933539 0.65625 0.15625 0.65625 0.15625 0.34375 0.84375 0.42187503
		 0.70843351 0.57812506 0.70843351 0.65625 0.84375 0.54166663 0.3125 0.54166663 0.3125
		 0.5 0.31250015 0.49999997 0.31250018 0.49999997 0.68843985 0.49999997 0.68843991
		 0.54166663 0.68843943 0.54166663 0.68843985 0.49999994 0.40210003 0.49999997 0.59883988
		 0.54166663 0.59883988 0.54166669 0.40209976 0.54166663 0.40209997 0.54166663 0.59883976
		 0.49999997 0.59883982 0.49999994 0.40210009 0.49999997 0.68843985 0.54166663 0.68843985
		 0.54166663 0.68843985 0.49999997 0.68843985 0.49999997 0.31250018 0.54166663 0.3125
		 0.54166663 0.3125 0.49999997 0.31250015 0.375 0.3125 0.375 0.68843985 0.41666666
		 0.68843985 0.41666666 0.3125 0.45833331 0.68843985 0.45833331 0.3125 0.49999997 0.68843985
		 0.49999997 0.3125 0.54166663 0.68843985 0.54166663 0.3125 0.58333331 0.68843985 0.58333331
		 0.3125 0.625 0.68843985 0.625 0.3125 0.578125 0.29156649 0.65625 0.15625 0.34375
		 0.15624997 0.421875 0.29156646 0.65625 0.84375 0.34375 0.84375 0.421875 0.97906649
		 0.578125 0.97906649 0.57812506 0.020933539 0.42187503 0.020933509 0.57812506 0.70843351
		 0.42187503 0.70843351 0.578125 0.29156649 0.421875 0.29156646 0.34375 0.15624997
		 0.65625 0.15625 0.65625 0.84375 0.65625 0.84375 0.578125 0.97906649 0.578125 0.97906649
		 0.578125 0.97906649 0.421875 0.97906649 0.421875 0.97906649 0.421875 0.97906649 0.34375
		 0.84375 0.34375 0.84375 0.34375 0.15624997 0.34375 0.15624997 0.42187503 0.020933509
		 0.42187503 0.020933509 0.42187503 0.020933509 0.57812506 0.020933539 0.57812506 0.020933539
		 0.57812506 0.020933539 0.65625 0.15625 0.65625 0.15625 0.34375 0.84375 0.42187503
		 0.70843351 0.57812506 0.70843351 0.65625 0.84375 0.54166663 0.3125 0.54166663 0.3125
		 0.49999997 0.31250018 0.49999997 0.31250018 0.49999997 0.68843985 0.49999997 0.68843985
		 0.54166663 0.68843985 0.54166663 0.68843985 0.49999994 0.40210003 0.49999997 0.59883988
		 0.54166663 0.59883988 0.54166669 0.40209976 0.49999994 0.40210003 0.49999997 0.59883988
		 0.54166663 0.68843985 0.49999997 0.68843985 0.54166663 0.59883988 0.54166669 0.40209976
		 0.49999997 0.31250018 0.54166663 0.3125 0.54166663 0.68843985 0.49999997 0.68843985
		 0.49999997 0.31250018 0.54166663 0.3125 0.375 0.3125 0.41666666 0.3125 0.41666666
		 0.68843985 0.375 0.68843985 0.45833331 0.3125 0.45833331 0.68843985 0.49999997 0.3125
		 0.49999997 0.68843985 0.54166663 0.3125 0.54166663 0.68843985 0.58333331 0.3125 0.58333331
		 0.68843985 0.625 0.3125 0.625 0.68843985 0.34375 0.15624997 0.42187503 0.020933509
		 0.57812506 0.020933539 0.65625 0.15625 0.65625 0.84375 0.578125 0.97906649 0.421875
		 0.97906649 0.34375 0.84375 0.42187503 0.70843351 0.57812506 0.70843351 0.578125 0.29156649
		 0.421875 0.29156646 0.65625 0.15625 0.57812506 0.020933539 0.42187503 0.020933509
		 0.34375 0.15624997 0.34375 0.84375 0.34375 0.84375 0.421875 0.97906649 0.421875 0.97906649
		 0.421875 0.97906649 0.421875 0.97906649 0.578125 0.97906649 0.578125 0.97906649 0.578125
		 0.97906649 0.65625 0.84375 0.65625 0.84375 0.65625 0.84375 0.65625 0.84375 0.57812506
		 0.70843351 0.57812506 0.70843351 0.57812506 0.70843351 0.42187503 0.70843351 0.42187503
		 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.34375 0.84375 0.578125 0.29156649
		 0.578125 0.29156649 0.65625 0.15625 0.65625 0.15625 0.34375 0.15624997 0.34375 0.15624997
		 0.421875 0.29156646 0.421875 0.29156646 0.421875 0.29156646 0.578125 0.29156649 0.578125
		 0.29156649 0.54166663 0.3125 0.54166663 0.3125;
	setAttr ".uvst[0].uvsp[1000:1215]" 0.5 0.31250015 0.49999997 0.31250018 0.49999997
		 0.68843985 0.49999997 0.68843991 0.54166663 0.68843943 0.54166663 0.68843985 0.49999994
		 0.40210003 0.49999997 0.59883988 0.54166663 0.59883988 0.54166669 0.40209976 0.54166663
		 0.40209997 0.54166663 0.59883976 0.49999997 0.59883982 0.49999994 0.40210009 0.49999997
		 0.68843985 0.54166663 0.68843985 0.54166663 0.68843985 0.49999997 0.68843985 0.49999997
		 0.31250018 0.54166663 0.3125 0.54166663 0.3125 0.49999997 0.31250015 0.375 0.3125
		 0.375 0.68843985 0.41666666 0.68843985 0.41666666 0.3125 0.45833331 0.68843985 0.45833331
		 0.3125 0.49999997 0.68843985 0.49999997 0.3125 0.54166663 0.68843985 0.54166663 0.3125
		 0.58333331 0.68843985 0.58333331 0.3125 0.625 0.68843985 0.625 0.3125 0.578125 0.29156649
		 0.65625 0.15625 0.34375 0.15624997 0.421875 0.29156646 0.65625 0.84375 0.34375 0.84375
		 0.421875 0.97906649 0.578125 0.97906649 0.57812506 0.020933539 0.42187503 0.020933509
		 0.57812506 0.70843351 0.42187503 0.70843351 0.578125 0.29156649 0.421875 0.29156646
		 0.34375 0.15624997 0.65625 0.15625 0.65625 0.84375 0.65625 0.84375 0.578125 0.97906649
		 0.578125 0.97906649 0.578125 0.97906649 0.421875 0.97906649 0.421875 0.97906649 0.421875
		 0.97906649 0.34375 0.84375 0.34375 0.84375 0.34375 0.15624997 0.34375 0.15624997
		 0.42187503 0.020933509 0.42187503 0.020933509 0.42187503 0.020933509 0.57812506 0.020933539
		 0.57812506 0.020933539 0.57812506 0.020933539 0.65625 0.15625 0.65625 0.15625 0.34375
		 0.84375 0.42187503 0.70843351 0.57812506 0.70843351 0.65625 0.84375 0.375 0.3125
		 0.41666666 0.3125 0.41666666 0.68843985 0.375 0.68843985 0.45833331 0.3125 0.45833331
		 0.68843985 0.49999997 0.3125 0.49999997 0.68843985 0.54166663 0.3125 0.54166663 0.68843985
		 0.58333331 0.3125 0.58333331 0.68843985 0.625 0.3125 0.625 0.68843985 0.34375 0.15624997
		 0.42187503 0.020933509 0.57812506 0.020933539 0.65625 0.15625 0.65625 0.84375 0.578125
		 0.97906649 0.421875 0.97906649 0.34375 0.84375 0.42187503 0.70843351 0.57812506 0.70843351
		 0.578125 0.29156649 0.421875 0.29156646 0.65625 0.15625 0.57812506 0.020933539 0.42187503
		 0.020933509 0.34375 0.15624997 0.34375 0.84375 0.34375 0.84375 0.421875 0.97906649
		 0.421875 0.97906649 0.421875 0.97906649 0.421875 0.97906649 0.578125 0.97906649 0.578125
		 0.97906649 0.578125 0.97906649 0.65625 0.84375 0.65625 0.84375 0.65625 0.84375 0.65625
		 0.84375 0.57812506 0.70843351 0.57812506 0.70843351 0.57812506 0.70843351 0.42187503
		 0.70843351 0.42187503 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.34375 0.84375
		 0.578125 0.29156649 0.578125 0.29156649 0.65625 0.15625 0.65625 0.15625 0.34375 0.15624997
		 0.34375 0.15624997 0.421875 0.29156646 0.421875 0.29156646 0.421875 0.29156646 0.578125
		 0.29156649 0.578125 0.29156649 0.54166663 0.3125 0.54166663 0.3125 0.5 0.31250015
		 0.49999997 0.31250018 0.49999997 0.68843985 0.49999997 0.68843991 0.54166663 0.68843943
		 0.54166663 0.68843985 0.49999994 0.40210003 0.49999997 0.59883988 0.54166663 0.59883988
		 0.54166669 0.40209976 0.54166663 0.40209997 0.54166663 0.59883976 0.49999997 0.59883982
		 0.49999994 0.40210009 0.49999997 0.68843985 0.54166663 0.68843985 0.54166663 0.68843985
		 0.49999997 0.68843985 0.49999997 0.31250018 0.54166663 0.3125 0.54166663 0.3125 0.49999997
		 0.31250015 0.375 0.3125 0.375 0.68843985 0.41666666 0.68843985 0.41666666 0.3125
		 0.45833331 0.68843985 0.45833331 0.3125 0.49999997 0.68843985 0.49999997 0.3125 0.54166663
		 0.68843985 0.54166663 0.3125 0.58333331 0.68843985 0.58333331 0.3125 0.625 0.68843985
		 0.625 0.3125 0.578125 0.29156649 0.65625 0.15625 0.34375 0.15624997 0.421875 0.29156646
		 0.65625 0.84375 0.34375 0.84375 0.421875 0.97906649 0.578125 0.97906649 0.57812506
		 0.020933539 0.42187503 0.020933509 0.57812506 0.70843351 0.42187503 0.70843351 0.578125
		 0.29156649 0.421875 0.29156646 0.34375 0.15624997 0.65625 0.15625 0.65625 0.84375
		 0.65625 0.84375 0.578125 0.97906649 0.578125 0.97906649 0.578125 0.97906649 0.421875
		 0.97906649 0.421875 0.97906649 0.421875 0.97906649 0.34375 0.84375 0.34375 0.84375
		 0.34375 0.15624997 0.34375 0.15624997 0.42187503 0.020933509 0.42187503 0.020933509
		 0.42187503 0.020933509 0.57812506 0.020933539 0.57812506 0.020933539 0.57812506 0.020933539
		 0.65625 0.15625 0.65625 0.15625 0.34375 0.84375 0.42187503 0.70843351 0.57812506
		 0.70843351 0.65625 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".sdt" 0;
	setAttr -s 576 ".vt";
	setAttr ".vt[0:165]"  51.64831161 54.48753357 -131.58007813 50.24591827 53.085147858 -131.58007813
		 51.64831161 54.48737717 -141.85423279 50.24591446 53.084991455 -141.85421753 54.72763062 54.48736954 -141.85424805
		 56.13002396 53.08498764 -141.85421753 54.72763062 54.48753738 -131.58010864 56.13002014 53.085147858 -131.58006287
		 56.13002396 19.13417816 -131.57949829 56.13002777 19.13417816 -141.8536377 50.24591827 19.13402557 -141.8536377
		 50.24591827 19.13417816 -131.57949829 54.72763824 17.67677498 -141.85362244 54.72763443 17.67677498 -131.57945251
		 51.64831543 17.67677498 -131.57946777 51.64831543 17.67677498 -141.85357666 55.050460815 22.4793911 -115.89472961
		 55.050460815 20.16939354 -115.89476013 53.049942017 19.014398575 -115.89472961 51.049430847 20.16939354 -115.89476013
		 51.049430847 22.4793911 -115.89476013 53.049942017 23.63438416 -115.89476013 55.050460815 22.47900963 -159.3288269
		 55.050460815 20.16900826 -159.3288269 53.049942017 19.014009476 -159.3288269 51.049430847 20.16900444 -159.3288269
		 51.049430847 22.47900581 -159.3288269 53.049942017 23.63400078 -159.3288269 53.049942017 19.54047012 -114.93708801
		 53.049942017 23.10833168 -114.93708801 54.59487915 22.21636772 -114.93708801 54.59487915 20.43243408 -114.93708801
		 51.50501251 22.21636772 -114.93708801 51.50501251 20.43243217 -114.93708801 53.049942017 19.54006386 -160.28646851
		 53.049942017 23.10793114 -160.28646851 54.59487915 20.43202782 -160.28646851 54.59487915 22.21597099 -160.28646851
		 51.50501251 22.21596718 -160.28646851 51.50501251 20.43202782 -160.28646851 55.1305542 52.74762344 -130.88845825
		 55.1305542 50.43762589 -130.88845825 53.13003922 49.28263092 -130.88845825 51.12952423 50.43762589 -130.88845825
		 51.12952423 52.74762344 -130.88845825 53.13003922 53.90261841 -130.88845825 55.1305542 52.74763107 -142.74794006
		 55.1305542 50.43762589 -142.74795532 53.13003922 49.28263092 -142.74795532 51.12952423 50.43762589 -142.74795532
		 51.12952423 52.74762726 -142.74794006 53.1300354 53.90262222 -142.74794006 53.13003922 49.80869293 -130.36422729
		 53.13003922 53.3765564 -130.36422729 54.67497253 52.48459244 -130.36422729 54.67497253 50.70065689 -130.36422729
		 51.5851059 52.48459244 -130.36422729 51.5851059 50.70065689 -130.36422729 53.13003922 49.80869293 -143.2477417
		 53.1300354 53.37656021 -143.24772644 54.67497253 50.70065689 -143.2477417 54.67497253 52.48460007 -143.24772644
		 51.5851059 52.48459625 -143.24772644 51.5851059 50.70065689 -143.2477417 29.68078613 62.48414612 -133.32003784
		 26.98661041 60.92865753 -133.32003784 29.68078232 62.48413849 -140.19560242 26.98661041 60.92865372 -140.19560242
		 58.005027771 52.61729813 -133.32009888 56.44956207 55.31147385 -133.32009888 58.005027771 52.61729431 -140.19566345
		 56.44956207 55.31147003 -140.19567871 56.75487137 47.95169449 -140.19567871 54.060722351 46.39621353 -140.19566345
		 56.75488663 47.95169067 -133.32009888 54.060726166 46.39620972 -133.32009888 27.29195404 53.5688858 -140.19561768
		 25.7364769 56.26306152 -140.19560242 27.29195404 53.56889343 -133.32003784 25.73647881 56.26306915 -133.32003784
		 32.91646957 58.35099411 -159.3288269 32.91646957 55.72453308 -159.3288269 30.64188957 54.41130829 -159.3288269
		 28.36730766 55.7245369 -159.3288269 28.36730576 58.3509903 -159.3288269 30.64188766 59.66422272 -159.3288269
		 32.9164505 58.35099792 -115.89477539 32.9164505 55.72453308 -115.89477539 30.64186668 54.41130447 -115.89476013
		 28.36728859 55.72453308 -115.89477539 28.36728668 58.35099411 -115.89477539 30.64186859 59.66422272 -115.89477539
		 28.73632813 55.93758392 -160.28646851 28.73632622 58.13793564 -160.28646851 30.64188576 54.83740616 -160.28646851
		 30.64188957 59.2381134 -160.28648376 32.5474472 55.93759155 -160.28648376 32.5474472 58.13793945 -160.28646851
		 30.6418705 59.2381134 -114.93710327 32.54742813 58.13794327 -114.93711853 32.54743195 55.93759155 -114.93711853
		 30.64186668 54.83740997 -114.93710327 28.73631096 55.93758774 -114.93711853 28.73630714 58.13793945 -114.93711853
		 29.68078995 32.26572037 -133.31997681 26.98661804 30.71023178 -133.32000732 29.68078613 32.26570892 -140.19558716
		 26.98661423 30.71023178 -140.19558716 58.0050315857 22.39886856 -133.32006836 56.44956589 25.09305191 -133.32006836
		 58.0050315857 22.39886856 -140.19564819 56.44956589 25.093044281 -140.19564819 56.75487137 17.73326492 -140.19564819
		 54.060726166 16.17778778 -140.19564819 56.75488663 17.73326492 -133.32006836 54.060726166 16.17778397 -133.32003784
		 27.29195786 23.35046005 -140.19558716 25.73648071 26.044631958 -140.19558716 27.29195786 23.35046768 -133.32000732
		 25.73648453 26.044643402 -133.32000732 27.83374405 25.19792557 -146.6857605 27.83374405 28.44046783 -146.6857605
		 30.64186859 30.061737061 -146.6857605 33.44999313 28.44046402 -146.6857605 33.45000076 25.19792938 -146.6857605
		 30.64187241 23.57665634 -146.6857605 27.83374405 25.19792175 -128.96548462 27.83374405 28.44046783 -128.96548462
		 30.64187241 30.061740875 -128.96548462 33.44999313 28.44047165 -128.96551514 33.44999313 25.19792938 -128.96551514
		 30.64186859 23.57665253 -128.96551514 32.99441147 28.17744064 -147.6434021 32.99441528 25.46096039 -147.6434021
		 30.64186859 29.53568649 -147.6434021 30.64186859 24.10272217 -147.6434021 28.28932571 28.17743301 -147.6434021
		 28.28932571 25.46095657 -147.6434021 30.64186859 24.10271454 -128.0078125 28.28932571 25.46095657 -128.0078430176
		 28.28932571 28.17743301 -128.0078430176 30.64186859 29.53568268 -128.0078125 32.99441147 28.17744064 -128.0078125
		 32.99441528 25.46096039 -128.0078125 51.64831161 54.48759079 126.85869598 50.24591446 53.085205078 126.85870361
		 51.64831161 54.48743439 116.58454895 50.24591446 53.085048676 116.58456421 54.72763062 54.48742676 116.58454132
		 56.13002014 53.085044861 116.58457184 54.72763062 54.4875946 126.85868073 56.13002014 53.085205078 126.85871887
		 56.13002396 19.13423538 126.85929108 56.13002396 19.13423538 116.58514404 50.24591827 19.13408279 116.58513641
		 50.24591827 19.13423538 126.85927582 54.72763443 17.6768322 116.5851593 54.72763443 17.6768322 126.85929871
		 51.64831543 17.6768322 126.85931396 51.64831543 17.6768322 116.58516693 55.1305542 52.747715 127.550354
		 55.1305542 50.43771744 127.55039215 53.13003922 49.28272247 127.55040741 51.12952423 50.43771744 127.55039215
		 51.12952423 52.747715 127.550354 53.13003922 53.90270996 127.55033875;
	setAttr ".vt[166:331]" 55.1305542 52.74754333 115.69087219 55.1305542 50.43753815 115.69089508
		 53.13003922 49.28254318 115.69091034 51.12952423 50.43753815 115.69089508 51.12952423 52.74753952 115.69087219
		 53.1300354 53.90253448 115.69085693 53.13003922 49.80879211 128.074630737 53.13003922 53.37665558 128.074584961
		 54.67497253 52.48469162 128.074584961 54.67497253 50.70075607 128.074615479 51.5851059 52.48469162 128.074584961
		 51.5851059 50.70075607 128.074615479 53.13003922 49.80859756 115.19111633 53.1300354 53.37646484 115.19107819
		 54.67497253 50.70056152 115.1911087 54.67497253 52.4845047 115.19109344 51.5851059 52.48450089 115.19109344
		 51.5851059 50.70056152 115.1911087 29.68078613 62.48420334 125.11862946 26.98661041 60.92871475 125.11865234
		 29.68078232 62.48409271 118.24306488 26.98661041 60.92860794 118.24308777 58.005027771 52.61735535 125.11872101
		 56.44956207 55.31153107 125.1186676 58.005027771 52.61724854 118.24315643 56.44956207 55.31142426 118.24310303
		 56.75487137 47.95164871 118.24321747 54.060722351 46.39616776 118.24324799 56.75488663 47.95174789 125.11878204
		 54.060726166 46.39626694 125.11881256 27.29195404 53.56884003 118.24319458 25.7364769 56.26301575 118.24316406
		 27.29195404 53.56895065 125.11875916 25.73647881 56.26312637 125.11872864 32.91646957 58.35112762 110.38816833
		 32.91646957 55.7246666 110.38812256 30.64188957 54.41143799 110.3881073 28.36730766 55.72467041 110.38813019
		 28.36730576 58.35112381 110.38817596 30.64188766 59.66435623 110.38819122 32.9164505 58.35073853 142.54400635
		 32.9164505 55.7242775 142.54394531 30.64186668 54.41104507 142.54394531 28.36728859 55.7242775 142.54394531
		 28.36728668 58.35073853 142.54400635 30.64186859 59.66396713 142.54402161 28.73632813 55.93772888 109.43048096
		 28.73632622 58.1380806 109.43052673 30.64188576 54.83755112 109.4304657 30.64188957 59.23825836 109.43053436
		 32.5474472 55.93773651 109.43047333 32.5474472 58.13808441 109.43052673 30.6418705 59.23784637 143.50169373
		 32.54742813 58.13767242 143.50164795 32.54743195 55.93732452 143.50161743 30.64186668 54.83714294 143.50160217
		 28.73631096 55.93731689 143.50161743 28.73630714 58.13766861 143.50164795 55.050460815 22.47944832 142.54402161
		 55.050460815 20.16945076 142.54402161 53.049942017 19.014455795 142.54402161 51.049430847 20.16945076 142.54402161
		 51.049430847 22.47944832 142.54402161 53.049942017 23.63444328 142.54402161 55.050460815 22.47906685 115.51576233
		 55.050460815 20.16906166 115.51576233 53.049942017 19.014066696 115.51576233 51.049430847 20.16906166 115.51576233
		 51.049430847 22.47906303 115.51576233 53.049942017 23.634058 115.51576233 53.049942017 19.54052544 143.50169373
		 53.049942017 23.1083889 143.50169373 54.59487915 22.21642494 143.50169373 54.59487915 20.4324894 143.50169373
		 51.50501251 22.21642494 143.50169373 51.50501251 20.4324894 143.50169373 53.049942017 19.54012108 114.55812073
		 53.049942017 23.10798836 114.55812073 54.59487915 20.43208504 114.55812073 54.59487915 22.21602821 114.55812073
		 51.50501251 22.2160244 114.55812073 51.50501251 20.43208504 114.55812073 29.68078995 32.26572037 125.11875916
		 26.98661804 30.71023178 125.11875153 29.68078613 32.26570892 118.24317169 26.98661423 30.71023178 118.24317169
		 58.0050315857 22.39886856 125.11869049 56.44956589 25.09305191 125.11868286 58.0050315857 22.39886856 118.24311066
		 56.44956589 25.093044281 118.24310303 56.75487137 17.73326492 118.24310303 54.060726166 16.17778778 118.24311066
		 56.75488663 17.73326492 125.11868286 54.060726166 16.17778397 125.11869812 27.29195786 23.35046005 118.24316406
		 25.73648071 26.044631958 118.24317169 27.29195786 23.35046768 125.11873627 25.73648453 26.044643402 125.11875153
		 27.83374405 25.19792557 111.75299072 27.83374405 28.44046783 111.75299072 30.64186859 30.061737061 111.75299835
		 33.44999313 28.44046402 111.75299835 33.45000076 25.19792938 111.75299835 30.64187241 23.57665634 111.75299072
		 27.83374405 25.19792175 129.4732666 27.83374405 28.44046783 129.4732666 30.64187241 30.061740875 129.4732666
		 33.44999313 28.44047165 129.47323608 33.44999313 25.19792938 129.47323608 30.64186859 23.57665253 129.47323608
		 32.99441147 28.17744064 110.79534912 32.99441528 25.46096039 110.79534912 30.64186859 29.53568649 110.79534912
		 30.64186859 24.10272217 110.79534149 28.28932571 28.17743301 110.79534149 28.28932571 25.46095657 110.79534912
		 30.64186859 24.10271454 130.43093872 28.28932571 25.46095657 130.4309082 28.28932571 28.17743301 130.4309082
		 30.64186859 29.53568268 130.43093872 32.99441147 28.17744064 130.43093872 32.99441528 25.46096039 130.43093872
		 -56.13002014 53.085147858 -131.58006287 -56.13002396 19.13417816 -131.57949829 -56.13002777 19.13417816 -141.8536377
		 -56.13002396 53.08498764 -141.85421753 -50.24591446 53.084991455 -141.85421753 -50.24591827 19.13402557 -141.8536377
		 -50.24591827 19.13417816 -131.57949829 -50.24591827 53.085147858 -131.58007813 -54.72763062 54.48736954 -141.85424805
		 -51.64831161 54.48737717 -141.85423279 -51.64831161 54.48753357 -131.58007813 -54.72763062 54.48753738 -131.58010864
		 -54.72763824 17.67677498 -141.85362244 -51.64831543 17.67677498 -141.85357666 -51.64831543 17.67677498 -131.57946777
		 -54.72763443 17.67677498 -131.57945251 -55.050460815 22.4793911 -115.89472961 -55.050460815 20.16939354 -115.89476013
		 -55.050460815 20.16900826 -159.3288269 -55.050460815 22.47900963 -159.3288269 -53.049942017 19.014398575 -115.89472961
		 -53.049942017 19.014009476 -159.3288269 -51.049430847 20.16939354 -115.89476013 -51.049430847 20.16900444 -159.3288269
		 -51.049430847 22.4793911 -115.89476013 -51.049430847 22.47900581 -159.3288269 -53.049942017 23.63438416 -115.89476013
		 -53.049942017 23.63400078 -159.3288269 -53.049942017 19.54047012 -114.93708801 -54.59487915 20.43243408 -114.93708801
		 -54.59487915 22.21636772 -114.93708801 -53.049942017 23.10833168 -114.93708801 -53.049942017 23.10793114 -160.28646851
		 -54.59487915 22.21597099 -160.28646851 -54.59487915 20.43202782 -160.28646851 -53.049942017 19.54006386 -160.28646851
		 -51.50501251 20.43202782 -160.28646851 -51.50501251 22.21596718 -160.28646851 -51.50501251 22.21636772 -114.93708801
		 -51.50501251 20.43243217 -114.93708801 -55.1305542 52.74762344 -130.88845825 -55.1305542 50.43762589 -130.88845825
		 -55.1305542 50.43762589 -142.74795532 -55.1305542 52.74763107 -142.74794006;
	setAttr ".vt[332:497]" -53.13003922 49.28263092 -130.88845825 -53.13003922 49.28263092 -142.74795532
		 -51.12952423 50.43762589 -130.88845825 -51.12952423 50.43762589 -142.74795532 -51.12952423 52.74762344 -130.88845825
		 -51.12952423 52.74762726 -142.74794006 -53.13003922 53.90261841 -130.88845825 -53.1300354 53.90262222 -142.74794006
		 -53.13003922 49.80869293 -130.36422729 -54.67497253 50.70065689 -130.36422729 -54.67497253 52.48459244 -130.36422729
		 -53.13003922 53.3765564 -130.36422729 -53.1300354 53.37656021 -143.24772644 -54.67497253 52.48460007 -143.24772644
		 -54.67497253 50.70065689 -143.2477417 -53.13003922 49.80869293 -143.2477417 -51.5851059 50.70065689 -143.2477417
		 -51.5851059 52.48459625 -143.24772644 -51.5851059 52.48459244 -130.36422729 -51.5851059 50.70065689 -130.36422729
		 -54.060726166 46.39620972 -133.32009888 -27.29195404 53.56889343 -133.32003784 -27.29195404 53.5688858 -140.19561768
		 -54.060722351 46.39621353 -140.19566345 -56.44956207 55.31147003 -140.19567871 -29.68078232 62.48413849 -140.19560242
		 -29.68078613 62.48414612 -133.32003784 -56.44956207 55.31147385 -133.32009888 -56.75487137 47.95169449 -140.19567871
		 -58.005027771 52.61729431 -140.19566345 -58.005027771 52.61729813 -133.32009888 -56.75488663 47.95169067 -133.32009888
		 -25.73647881 56.26306915 -133.32003784 -26.98661041 60.92865753 -133.32003784 -26.98661041 60.92865372 -140.19560242
		 -25.7364769 56.26306152 -140.19560242 -32.91646957 58.35099411 -159.3288269 -32.9164505 58.35099792 -115.89477539
		 -32.9164505 55.72453308 -115.89477539 -32.91646957 55.72453308 -159.3288269 -30.64186668 54.41130447 -115.89476013
		 -30.64188957 54.41130829 -159.3288269 -28.36728859 55.72453308 -115.89477539 -28.36730766 55.7245369 -159.3288269
		 -28.36728668 58.35099411 -115.89477539 -28.36730576 58.3509903 -159.3288269 -30.64186859 59.66422272 -115.89477539
		 -30.64188766 59.66422272 -159.3288269 -28.73632622 58.13793564 -160.28646851 -30.64188957 59.2381134 -160.28648376
		 -30.64188576 54.83740616 -160.28646851 -28.73632813 55.93758392 -160.28646851 -30.6418705 59.2381134 -114.93710327
		 -30.64186668 54.83740997 -114.93710327 -32.54743195 55.93759155 -114.93711853 -32.54742813 58.13794327 -114.93711853
		 -32.5474472 58.13793945 -160.28646851 -32.5474472 55.93759155 -160.28648376 -28.73630714 58.13793945 -114.93711853
		 -28.73631096 55.93758774 -114.93711853 -54.060726166 16.17778397 -133.32003784 -27.29195786 23.35046768 -133.32000732
		 -27.29195786 23.35046005 -140.19558716 -54.060726166 16.17778778 -140.19564819 -56.44956589 25.093044281 -140.19564819
		 -29.68078613 32.26570892 -140.19558716 -29.68078995 32.26572037 -133.31997681 -56.44956589 25.09305191 -133.32006836
		 -56.75487137 17.73326492 -140.19564819 -58.0050315857 22.39886856 -140.19564819 -58.0050315857 22.39886856 -133.32006836
		 -56.75488663 17.73326492 -133.32006836 -25.73648453 26.044643402 -133.32000732 -26.98661804 30.71023178 -133.32000732
		 -26.98661423 30.71023178 -140.19558716 -25.73648071 26.044631958 -140.19558716 -27.83374405 25.19792557 -146.6857605
		 -27.83374405 25.19792175 -128.96548462 -27.83374405 28.44046783 -128.96548462 -27.83374405 28.44046783 -146.6857605
		 -30.64187241 30.061740875 -128.96548462 -30.64186859 30.061737061 -146.6857605 -33.44999313 28.44047165 -128.96551514
		 -33.44999313 28.44046402 -146.6857605 -33.44999313 25.19792938 -128.96551514 -33.45000076 25.19792938 -146.6857605
		 -30.64186859 23.57665253 -128.96551514 -30.64187241 23.57665634 -146.6857605 -32.99441528 25.46096039 -147.6434021
		 -30.64186859 24.10272217 -147.6434021 -30.64186859 29.53568649 -147.6434021 -32.99441147 28.17744064 -147.6434021
		 -30.64186859 24.10271454 -128.0078125 -30.64186859 29.53568268 -128.0078125 -28.28932571 28.17743301 -128.0078430176
		 -28.28932571 25.46095657 -128.0078430176 -28.28932571 25.46095657 -147.6434021 -28.28932571 28.17743301 -147.6434021
		 -32.99441528 25.46096039 -128.0078125 -32.99441147 28.17744064 -128.0078125 -56.13002014 53.085205078 126.85871887
		 -56.13002396 19.13423538 126.85929108 -56.13002396 19.13423538 116.58514404 -56.13002014 53.085044861 116.58457184
		 -50.24591446 53.085048676 116.58456421 -50.24591827 19.13408279 116.58513641 -50.24591827 19.13423538 126.85927582
		 -50.24591446 53.085205078 126.85870361 -54.72763062 54.48742676 116.58454132 -51.64831161 54.48743439 116.58454895
		 -51.64831161 54.48759079 126.85869598 -54.72763062 54.4875946 126.85868073 -54.72763443 17.6768322 116.5851593
		 -51.64831543 17.6768322 116.58516693 -51.64831543 17.6768322 126.85931396 -54.72763443 17.6768322 126.85929871
		 -55.1305542 52.747715 127.550354 -55.1305542 50.43771744 127.55039215 -55.1305542 50.43753815 115.69089508
		 -55.1305542 52.74754333 115.69087219 -53.13003922 49.28272247 127.55040741 -53.13003922 49.28254318 115.69091034
		 -51.12952423 50.43771744 127.55039215 -51.12952423 50.43753815 115.69089508 -51.12952423 52.747715 127.550354
		 -51.12952423 52.74753952 115.69087219 -53.13003922 53.90270996 127.55033875 -53.1300354 53.90253448 115.69085693
		 -53.13003922 49.80879211 128.074630737 -54.67497253 50.70075607 128.074615479 -54.67497253 52.48469162 128.074584961
		 -53.13003922 53.37665558 128.074584961 -53.1300354 53.37646484 115.19107819 -54.67497253 52.4845047 115.19109344
		 -54.67497253 50.70056152 115.1911087 -53.13003922 49.80859756 115.19111633 -51.5851059 50.70056152 115.1911087
		 -51.5851059 52.48450089 115.19109344 -51.5851059 52.48469162 128.074584961 -51.5851059 50.70075607 128.074615479
		 -54.060726166 46.39626694 125.11881256 -27.29195404 53.56895065 125.11875916 -27.29195404 53.56884003 118.24319458
		 -54.060722351 46.39616776 118.24324799 -56.44956207 55.31142426 118.24310303 -29.68078232 62.48409271 118.24306488
		 -29.68078613 62.48420334 125.11862946 -56.44956207 55.31153107 125.1186676 -56.75487137 47.95164871 118.24321747
		 -58.005027771 52.61724854 118.24315643 -58.005027771 52.61735535 125.11872101 -56.75488663 47.95174789 125.11878204
		 -25.73647881 56.26312637 125.11872864 -26.98661041 60.92871475 125.11865234 -26.98661041 60.92860794 118.24308777
		 -25.7364769 56.26301575 118.24316406 -32.91646957 58.35112762 110.38816833 -32.9164505 58.35073853 142.54400635
		 -32.9164505 55.7242775 142.54394531 -32.91646957 55.7246666 110.38812256 -30.64186668 54.41104507 142.54394531
		 -30.64188957 54.41143799 110.3881073 -28.36728859 55.7242775 142.54394531 -28.36730766 55.72467041 110.38813019
		 -28.36728668 58.35073853 142.54400635 -28.36730576 58.35112381 110.38817596;
	setAttr ".vt[498:575]" -30.64186859 59.66396713 142.54402161 -30.64188766 59.66435623 110.38819122
		 -28.73632622 58.1380806 109.43052673 -30.64188957 59.23825836 109.43053436 -30.64188576 54.83755112 109.4304657
		 -28.73632813 55.93772888 109.43048096 -30.6418705 59.23784637 143.50169373 -30.64186668 54.83714294 143.50160217
		 -32.54743195 55.93732452 143.50161743 -32.54742813 58.13767242 143.50164795 -32.5474472 58.13808441 109.43052673
		 -32.5474472 55.93773651 109.43047333 -28.73630714 58.13766861 143.50164795 -28.73631096 55.93731689 143.50161743
		 -55.050460815 22.47944832 142.54402161 -55.050460815 20.16945076 142.54402161 -55.050460815 20.16906166 115.51576233
		 -55.050460815 22.47906685 115.51576233 -53.049942017 19.014455795 142.54402161 -53.049942017 19.014066696 115.51576233
		 -51.049430847 20.16945076 142.54402161 -51.049430847 20.16906166 115.51576233 -51.049430847 22.47944832 142.54402161
		 -51.049430847 22.47906303 115.51576233 -53.049942017 23.63444328 142.54402161 -53.049942017 23.634058 115.51576233
		 -53.049942017 19.54052544 143.50169373 -54.59487915 20.4324894 143.50169373 -54.59487915 22.21642494 143.50169373
		 -53.049942017 23.1083889 143.50169373 -53.049942017 23.10798836 114.55812073 -54.59487915 22.21602821 114.55812073
		 -54.59487915 20.43208504 114.55812073 -53.049942017 19.54012108 114.55812073 -51.50501251 20.43208504 114.55812073
		 -51.50501251 22.2160244 114.55812073 -51.50501251 22.21642494 143.50169373 -51.50501251 20.4324894 143.50169373
		 -54.060726166 16.17778397 125.11869812 -27.29195786 23.35046768 125.11873627 -27.29195786 23.35046005 118.24316406
		 -54.060726166 16.17778778 118.24311066 -56.44956589 25.093044281 118.24310303 -29.68078613 32.26570892 118.24317169
		 -29.68078995 32.26572037 125.11875916 -56.44956589 25.09305191 125.11868286 -56.75487137 17.73326492 118.24310303
		 -58.0050315857 22.39886856 118.24311066 -58.0050315857 22.39886856 125.11869049 -56.75488663 17.73326492 125.11868286
		 -25.73648453 26.044643402 125.11875153 -26.98661804 30.71023178 125.11875153 -26.98661423 30.71023178 118.24317169
		 -25.73648071 26.044631958 118.24317169 -27.83374405 25.19792557 111.75299072 -27.83374405 25.19792175 129.4732666
		 -27.83374405 28.44046783 129.4732666 -27.83374405 28.44046783 111.75299072 -30.64187241 30.061740875 129.4732666
		 -30.64186859 30.061737061 111.75299835 -33.44999313 28.44047165 129.47323608 -33.44999313 28.44046402 111.75299835
		 -33.44999313 25.19792938 129.47323608 -33.45000076 25.19792938 111.75299835 -30.64186859 23.57665253 129.47323608
		 -30.64187241 23.57665634 111.75299072 -32.99441528 25.46096039 110.79534912 -30.64186859 24.10272217 110.79534149
		 -30.64186859 29.53568649 110.79534912 -32.99441147 28.17744064 110.79534912 -30.64186859 24.10271454 130.43093872
		 -30.64186859 29.53568268 130.43093872 -28.28932571 28.17743301 130.4309082 -28.28932571 25.46095657 130.4309082
		 -28.28932571 25.46095657 110.79534912 -28.28932571 28.17743301 110.79534149 -32.99441528 25.46096039 130.43093872
		 -32.99441147 28.17744064 130.43093872;
	setAttr -s 992 ".ed";
	setAttr ".ed[0:165]"  1 0 0 3 2 0 0 2 1 3 1 1 4 2 0 5 4 0 6 0 0 7 6 0 4 6 1
		 7 5 1 8 9 1 9 5 0 8 7 0 10 11 1 11 1 0 10 3 0 12 13 1 13 14 0 14 15 1 12 15 0 10 15 0
		 9 12 0 11 14 0 8 13 0 16 17 0 17 18 0 18 19 0 19 20 0 20 21 0 21 16 0 22 23 0 23 24 0
		 24 25 0 26 27 0 27 22 0 16 22 0 17 23 0 18 24 0 19 25 0 20 26 0 21 27 0 25 26 0 18 28 0
		 21 29 0 16 30 0 29 30 0 17 31 0 30 31 0 31 28 0 20 32 0 32 29 0 19 33 0 28 33 0 33 32 0
		 24 34 0 27 35 0 34 35 0 23 36 0 36 34 0 22 37 0 37 36 0 35 37 0 26 38 0 38 35 0 25 39 0
		 39 38 0 34 39 0 28 29 0 40 41 1 41 42 1 42 43 1 43 44 1 44 45 1 45 40 1 46 47 1 47 48 1
		 48 49 1 50 51 1 51 46 1 40 46 0 41 47 0 42 48 0 43 49 0 44 50 0 45 51 0 49 50 1 42 52 1
		 45 53 1 52 53 1 40 54 1 53 54 1 41 55 1 54 55 1 55 52 1 44 56 1 56 53 1 43 57 1 52 57 1
		 57 56 1 48 58 1 51 59 1 58 59 1 47 60 1 60 58 1 46 61 1 61 60 1 59 61 1 50 62 1 62 59 1
		 49 63 1 63 62 1 58 63 1 65 64 0 66 67 0 64 66 1 67 65 1 69 64 0 69 68 0 71 66 0 71 70 0
		 68 70 1 71 69 1 72 70 0 73 76 0 73 72 0 74 68 0 75 78 0 75 74 0 72 74 1 75 73 1 77 67 0
		 77 76 0 79 65 0 78 79 0 76 78 1 79 77 1 80 81 1 81 82 1 82 83 1 83 84 1 84 85 1 85 80 1
		 86 87 1 87 88 1 88 89 1 89 90 1 90 91 1 91 86 1 80 86 0 81 87 0 82 88 0 83 89 0 84 90 0
		 85 91 0 83 92 1 84 93 1 92 93 0 82 94 1 94 92 0 85 95 1 94 95 1 93 95 0 81 96 1 96 94 0
		 80 97 1 97 96 0;
	setAttr ".ed[166:331]" 95 97 0 91 98 1 86 99 1 98 99 0 87 100 1 99 100 0 88 101 1
		 100 101 0 101 98 1 89 102 1 101 102 0 90 103 1 102 103 0 103 98 0 105 104 0 106 107 0
		 104 106 1 107 105 1 109 104 0 109 108 0 111 106 0 111 110 0 108 110 1 111 109 1 112 110 0
		 113 116 0 113 112 0 114 108 0 115 118 0 115 114 0 112 114 1 115 113 1 117 107 0 117 116 0
		 119 105 0 118 119 0 116 118 1 119 117 1 120 121 1 121 122 1 122 123 1 123 124 1 124 125 1
		 125 120 1 126 127 1 127 128 1 128 129 1 129 130 1 130 131 1 131 126 1 120 126 0 121 127 0
		 122 128 0 123 129 0 124 130 0 125 131 0 123 132 1 124 133 1 132 133 0 122 134 1 134 132 0
		 125 135 1 134 135 1 133 135 0 121 136 1 136 134 0 120 137 1 137 136 0 135 137 0 131 138 1
		 126 139 1 138 139 0 127 140 1 139 140 0 128 141 1 140 141 0 141 138 1 129 142 1 141 142 0
		 130 143 1 142 143 0 143 138 0 145 144 0 147 146 0 144 146 1 147 145 1 148 146 0 149 148 0
		 150 144 0 151 150 0 148 150 1 151 149 1 152 153 1 153 149 0 152 151 0 154 155 1 155 145 0
		 154 147 0 156 157 1 157 158 0 158 159 1 156 159 0 154 159 0 153 156 0 155 158 0 152 157 0
		 160 161 0 161 162 0 162 163 0 163 164 0 164 165 0 165 160 0 166 167 0 167 168 0 168 169 0
		 170 171 0 171 166 0 160 166 0 161 167 0 162 168 0 163 169 0 164 170 0 165 171 0 169 170 0
		 162 172 0 165 173 0 172 173 0 160 174 0 173 174 0 161 175 0 174 175 0 175 172 0 164 176 0
		 176 173 0 163 177 0 172 177 0 177 176 0 168 178 0 171 179 0 178 179 0 167 180 0 180 178 0
		 166 181 0 181 180 0 179 181 0 170 182 0 182 179 0 169 183 0 183 182 0 178 183 0 185 184 0
		 186 187 0 184 186 0 187 185 0 189 184 0 189 188 0 191 186 0 191 190 0 188 190 0 191 189 0
		 192 190 0 193 196 0 193 192 0 194 188 0 195 198 0 195 194 0;
	setAttr ".ed[332:497]" 192 194 0 195 193 0 197 187 0 197 196 0 199 185 0 198 199 0
		 196 198 0 199 197 0 200 201 0 201 202 0 202 203 0 203 204 0 204 205 0 205 200 0 206 207 0
		 207 208 0 208 209 0 209 210 0 210 211 0 211 206 0 200 206 0 201 207 0 202 208 0 203 209 0
		 204 210 0 205 211 0 203 212 0 204 213 0 212 213 0 202 214 0 214 212 0 205 215 0 214 215 0
		 213 215 0 201 216 0 216 214 0 200 217 0 217 216 0 215 217 0 211 218 0 206 219 0 218 219 0
		 207 220 0 219 220 0 208 221 0 220 221 0 221 218 0 209 222 0 221 222 0 210 223 0 222 223 0
		 223 218 0 224 225 0 225 226 0 226 227 0 227 228 0 228 229 0 229 224 0 230 231 0 231 232 0
		 232 233 0 234 235 0 235 230 0 224 230 0 225 231 0 226 232 0 227 233 0 228 234 0 229 235 0
		 233 234 0 226 236 0 229 237 0 224 238 0 237 238 0 225 239 0 238 239 0 239 236 0 228 240 0
		 240 237 0 227 241 0 236 241 0 241 240 0 232 242 0 235 243 0 242 243 0 231 244 0 244 242 0
		 230 245 0 245 244 0 243 245 0 234 246 0 246 243 0 233 247 0 247 246 0 242 247 0 249 248 0
		 250 251 0 248 250 0 251 249 0 253 248 0 253 252 0 255 250 0 255 254 0 252 254 0 255 253 0
		 256 254 0 257 260 0 257 256 0 258 252 0 259 262 0 259 258 0 256 258 0 259 257 0 261 251 0
		 261 260 0 263 249 0 262 263 0 260 262 0 263 261 0 264 265 0 265 266 0 266 267 0 267 268 0
		 268 269 0 269 264 0 270 271 0 271 272 0 272 273 0 273 274 0 274 275 0 275 270 0 264 270 0
		 265 271 0 266 272 0 267 273 0 268 274 0 269 275 0 267 276 0 268 277 0 276 277 0 266 278 0
		 278 276 0 269 279 0 278 279 0 277 279 0 265 280 0 280 278 0 264 281 0 281 280 0 279 281 0
		 275 282 0 270 283 0 282 283 0 271 284 0 283 284 0 272 285 0 284 285 0 285 282 0 273 286 0
		 285 286 0 274 287 0 286 287 0 287 282 0 236 237 0 289 288 0 289 290 1;
	setAttr ".ed[498:663]" 290 291 0 288 291 1 293 292 0 293 294 1 294 295 0 292 295 1
		 296 297 0 298 297 1 299 298 0 296 299 1 290 300 0 300 301 0 293 301 0 292 297 0 291 296 0
		 295 298 0 294 302 0 303 302 0 289 303 0 288 299 0 300 303 1 302 301 1 304 305 0 305 306 0
		 307 306 0 304 307 0 305 308 0 308 309 0 306 309 0 308 310 0 310 311 0 309 311 0 310 312 0
		 312 313 0 311 313 0 312 314 0 314 315 0 313 315 0 314 304 0 315 307 0 317 316 0 318 317 0
		 319 318 0 316 319 0 320 321 0 321 322 0 322 323 0 323 320 0 323 324 0 324 325 0 325 320 0
		 327 326 0 316 327 0 326 319 0 314 319 0 304 318 0 305 317 0 308 316 0 312 326 0 310 327 0
		 309 323 0 306 322 0 307 321 0 315 320 0 313 325 0 311 324 0 328 329 1 329 330 0 331 330 1
		 328 331 0 329 332 1 332 333 0 330 333 1 332 334 1 334 335 0 333 335 1 334 336 1 336 337 0
		 335 337 1 336 338 1 338 339 0 337 339 1 338 328 1 339 331 1 341 340 1 342 341 1 343 342 1
		 340 343 1 344 345 1 345 346 1 346 347 1 347 344 1 347 348 1 348 349 1 349 344 1 351 350 1
		 340 351 1 350 343 1 338 343 1 328 342 1 329 341 1 332 340 1 336 350 1 334 351 1 333 347 1
		 330 346 1 331 345 1 339 344 1 337 349 1 335 348 1 352 353 0 354 353 1 355 354 0 352 355 1
		 356 357 0 358 357 1 359 358 0 356 359 1 360 361 0 362 361 1 363 362 0 360 363 1 364 365 0
		 366 365 1 367 366 0 364 367 1 357 366 0 365 358 0 355 360 0 367 354 0 356 361 0 359 362 0
		 353 364 0 352 363 0 368 369 0 369 370 1 371 370 0 368 371 1 370 372 1 373 372 0 371 373 1
		 372 374 1 375 374 0 373 375 1 374 376 1 377 376 0 375 377 1 376 378 1 379 378 0 377 379 1
		 378 369 1 379 368 1 380 381 0 382 381 1 382 383 0 383 380 0 385 384 1 386 385 0 387 386 0
		 384 387 0 381 388 0 388 389 0 389 382 0 390 384 0 391 390 0 385 391 0;
	setAttr ".ed[664:829]" 377 380 1 375 383 1 373 382 1 379 381 1 371 389 1 368 388 1
		 378 384 1 369 387 1 370 386 1 372 385 1 374 391 1 376 390 1 392 393 0 394 393 1 395 394 0
		 392 395 1 396 397 0 398 397 1 399 398 0 396 399 1 400 401 0 402 401 1 403 402 0 400 403 1
		 404 405 0 406 405 1 407 406 0 404 407 1 397 406 0 405 398 0 395 400 0 407 394 0 396 401 0
		 399 402 0 393 404 0 392 403 0 408 409 0 409 410 1 411 410 0 408 411 1 410 412 1 413 412 0
		 411 413 1 412 414 1 415 414 0 413 415 1 414 416 1 417 416 0 415 417 1 416 418 1 419 418 0
		 417 419 1 418 409 1 419 408 1 420 421 0 422 421 1 422 423 0 423 420 0 425 424 1 426 425 0
		 427 426 0 424 427 0 421 428 0 428 429 0 429 422 0 430 424 0 431 430 0 425 431 0 417 420 1
		 415 423 1 413 422 1 419 421 1 411 429 1 408 428 1 418 424 1 409 427 1 410 426 1 412 425 1
		 414 431 1 416 430 1 433 432 0 433 434 1 434 435 0 432 435 1 437 436 0 437 438 1 438 439 0
		 436 439 1 440 441 0 442 441 1 443 442 0 440 443 1 434 444 0 444 445 0 437 445 0 436 441 0
		 435 440 0 439 442 0 438 446 0 447 446 0 433 447 0 432 443 0 444 447 1 446 445 1 448 449 0
		 449 450 0 451 450 0 448 451 0 449 452 0 452 453 0 450 453 0 452 454 0 454 455 0 453 455 0
		 454 456 0 456 457 0 455 457 0 456 458 0 458 459 0 457 459 0 458 448 0 459 451 0 461 460 0
		 462 461 0 463 462 0 460 463 0 464 465 0 465 466 0 466 467 0 467 464 0 467 468 0 468 469 0
		 469 464 0 471 470 0 460 471 0 470 463 0 458 463 0 448 462 0 449 461 0 452 460 0 456 470 0
		 454 471 0 453 467 0 450 466 0 451 465 0 459 464 0 457 469 0 455 468 0 472 473 0 474 473 0
		 475 474 0 472 475 0 476 477 0 478 477 0 479 478 0 476 479 0 480 481 0 482 481 0 483 482 0
		 480 483 0 484 485 0 486 485 0 487 486 0 484 487 0 477 486 0 485 478 0;
	setAttr ".ed[830:991]" 475 480 0 487 474 0 476 481 0 479 482 0 473 484 0 472 483 0
		 488 489 0 489 490 0 491 490 0 488 491 0 490 492 0 493 492 0 491 493 0 492 494 0 495 494 0
		 493 495 0 494 496 0 497 496 0 495 497 0 496 498 0 499 498 0 497 499 0 498 489 0 499 488 0
		 500 501 0 502 501 0 502 503 0 503 500 0 505 504 0 506 505 0 507 506 0 504 507 0 501 508 0
		 508 509 0 509 502 0 510 504 0 511 510 0 505 511 0 497 500 0 495 503 0 493 502 0 499 501 0
		 491 509 0 488 508 0 498 504 0 489 507 0 490 506 0 492 505 0 494 511 0 496 510 0 512 513 0
		 513 514 0 515 514 0 512 515 0 513 516 0 516 517 0 514 517 0 516 518 0 518 519 0 517 519 0
		 518 520 0 520 521 0 519 521 0 520 522 0 522 523 0 521 523 0 522 512 0 523 515 0 525 524 0
		 526 525 0 527 526 0 524 527 0 528 529 0 529 530 0 530 531 0 531 528 0 531 532 0 532 533 0
		 533 528 0 535 534 0 524 535 0 534 527 0 522 527 0 512 526 0 513 525 0 516 524 0 520 534 0
		 518 535 0 517 531 0 514 530 0 515 529 0 523 528 0 521 533 0 519 532 0 536 537 0 538 537 0
		 539 538 0 536 539 0 540 541 0 542 541 0 543 542 0 540 543 0 544 545 0 546 545 0 547 546 0
		 544 547 0 548 549 0 550 549 0 551 550 0 548 551 0 541 550 0 549 542 0 539 544 0 551 538 0
		 540 545 0 543 546 0 537 548 0 536 547 0 552 553 0 553 554 0 555 554 0 552 555 0 554 556 0
		 557 556 0 555 557 0 556 558 0 559 558 0 557 559 0 558 560 0 561 560 0 559 561 0 560 562 0
		 563 562 0 561 563 0 562 553 0 563 552 0 564 565 0 566 565 0 566 567 0 567 564 0 569 568 0
		 570 569 0 571 570 0 568 571 0 565 572 0 572 573 0 573 566 0 574 568 0 575 574 0 569 575 0
		 561 564 0 559 567 0 557 566 0 563 565 0 555 573 0 552 572 0 562 568 0 553 571 0 554 570 0
		 556 569 0 558 575 0 560 574 0;
	setAttr -s 472 -ch 1984 ".fc[0:471]" -type "polyFaces" 
		f 4 -13 10 11 -10
		mu 0 4 0 1 2 3
		f 4 -16 13 14 -4
		mu 0 4 4 5 6 7
		f 4 4 -3 -7 -9
		mu 0 4 8 9 10 11
		f 8 -12 21 19 -21 15 1 -5 -6
		mu 0 8 3 2 12 13 5 4 9 8
		f 4 2 -2 3 0
		mu 0 4 14 15 4 7
		f 8 -15 22 -18 -24 12 7 6 -1
		mu 0 8 7 6 16 17 1 0 11 10
		f 4 8 -8 9 5
		mu 0 4 18 19 0 3
		f 4 16 17 18 -20
		mu 0 4 12 17 16 13
		f 4 -23 -14 20 -19
		mu 0 4 20 6 5 21
		f 4 -22 -11 23 -17
		mu 0 4 22 2 1 23
		f 4 24 36 -31 -36
		mu 0 4 24 25 26 27
		f 4 25 37 -32 -37
		mu 0 4 25 28 29 26
		f 4 26 38 -33 -38
		mu 0 4 28 30 31 29
		f 4 27 39 -42 -39
		mu 0 4 30 32 33 31
		f 4 28 40 -34 -40
		mu 0 4 32 34 35 33
		f 4 29 35 -35 -41
		mu 0 4 34 36 37 35
		f 4 -49 -48 -46 -68
		mu 0 4 38 39 40 41
		f 4 61 60 58 56
		mu 0 4 42 43 44 45
		f 4 -57 66 65 63
		mu 0 4 42 45 46 47
		f 4 -54 -53 67 -51
		mu 0 4 48 49 38 41
		f 4 43 45 -45 -30
		mu 0 4 50 41 40 51
		f 4 44 47 -47 -25
		mu 0 4 51 40 39 52
		f 4 46 48 -43 -26
		mu 0 4 52 39 38 53
		f 4 49 50 -44 -29
		mu 0 4 54 62 63 55
		f 4 42 52 -52 -27
		mu 0 4 64 65 66 56
		f 4 51 53 -50 -28
		mu 0 4 56 67 68 57
		f 4 54 -59 -58 31
		mu 0 4 69 70 71 58
		f 4 57 -61 -60 30
		mu 0 4 58 72 73 59
		f 4 59 -62 -56 34
		mu 0 4 59 74 75 76
		f 4 55 -64 -63 33
		mu 0 4 60 77 78 79
		f 4 62 -66 -65 41
		mu 0 4 80 81 82 61
		f 4 64 -67 -55 32
		mu 0 4 61 83 84 85
		f 4 68 80 -75 -80
		mu 0 4 86 87 88 89
		f 4 69 81 -76 -81
		mu 0 4 87 90 91 88
		f 4 70 82 -77 -82
		mu 0 4 90 92 93 91
		f 4 71 83 -86 -83
		mu 0 4 92 94 95 93
		f 4 72 84 -78 -84
		mu 0 4 94 96 97 95
		f 4 73 79 -79 -85
		mu 0 4 96 98 99 97
		f 4 -94 -93 -91 -89
		mu 0 4 100 101 102 103
		f 4 106 105 103 101
		mu 0 4 104 105 106 107
		f 4 -102 111 110 108
		mu 0 4 104 107 108 109
		f 4 -99 -98 88 -96
		mu 0 4 110 111 100 103
		f 4 87 90 -90 -74
		mu 0 4 112 103 102 113
		f 4 89 92 -92 -69
		mu 0 4 113 102 101 114
		f 4 91 93 -87 -70
		mu 0 4 114 101 100 115
		f 4 94 95 -88 -73
		mu 0 4 116 117 118 119
		f 4 86 97 -97 -71
		mu 0 4 120 121 122 123
		f 4 96 98 -95 -72
		mu 0 4 123 124 125 126
		f 4 99 -104 -103 75
		mu 0 4 127 128 129 130
		f 4 102 -106 -105 74
		mu 0 4 130 131 132 133
		f 4 104 -107 -101 78
		mu 0 4 133 134 135 136
		f 4 100 -109 -108 77
		mu 0 4 137 138 139 140
		f 4 107 -111 -110 85
		mu 0 4 141 142 143 144
		f 4 109 -112 -100 76
		mu 0 4 144 145 146 147
		f 4 126 -135 -124 -130
		mu 0 4 148 149 150 151
		f 4 118 -115 -117 -122
		mu 0 4 152 153 154 155
		f 4 122 -121 -126 -129
		mu 0 4 156 157 158 159
		f 4 132 -116 -131 -136
		mu 0 4 160 161 162 163
		f 4 114 113 115 112
		mu 0 4 154 153 164 165
		f 8 -123 -125 123 -132 130 -114 -119 119
		mu 0 8 157 156 151 150 163 162 153 152
		f 4 120 -120 121 117
		mu 0 4 166 167 152 155
		f 8 125 -118 116 -113 -133 -134 -127 127
		mu 0 8 159 158 155 154 161 160 149 148
		f 4 128 -128 129 124
		mu 0 4 168 169 148 151
		f 4 134 133 135 131
		mu 0 4 150 149 170 171
		f 4 148 142 -150 -137
		mu 0 4 172 173 174 175
		f 4 149 143 -151 -138
		mu 0 4 175 174 176 177
		f 4 150 144 -152 -139
		mu 0 4 177 176 178 179
		f 4 151 145 -153 -140
		mu 0 4 179 178 180 181
		f 4 152 146 -154 -141
		mu 0 4 181 180 182 183
		f 4 153 147 -149 -142
		mu 0 4 183 182 184 185
		f 4 161 -161 158 156
		mu 0 4 186 187 188 189
		f 4 -175 -174 -172 -170
		mu 0 4 190 191 192 193
		f 4 166 165 163 160
		mu 0 4 187 194 195 188
		f 4 -180 -179 -177 174
		mu 0 4 190 196 197 191
		f 4 155 -157 -155 139
		mu 0 4 198 186 189 199
		f 4 154 -159 -158 138
		mu 0 4 199 189 188 200
		f 4 159 -162 -156 140
		mu 0 4 201 187 186 198
		f 4 157 -164 -163 137
		mu 0 4 202 203 204 205
		f 4 162 -166 -165 136
		mu 0 4 205 206 207 208
		f 4 164 -167 -160 141
		mu 0 4 208 209 210 211
		f 4 167 169 -169 -148
		mu 0 4 212 213 214 215
		f 4 168 171 -171 -143
		mu 0 4 215 216 217 218
		f 4 170 173 -173 -144
		mu 0 4 218 219 220 221
		f 4 172 176 -176 -145
		mu 0 4 222 191 197 223
		f 4 175 178 -178 -146
		mu 0 4 223 197 196 224
		f 4 177 179 -168 -147
		mu 0 4 224 196 190 225
		f 4 194 -203 -192 -198
		mu 0 4 226 227 228 229
		f 4 186 -183 -185 -190
		mu 0 4 230 231 232 233
		f 4 190 -189 -194 -197
		mu 0 4 234 235 236 237
		f 4 200 -184 -199 -204
		mu 0 4 238 239 240 241
		f 4 182 181 183 180
		mu 0 4 232 231 242 243
		f 8 -191 -193 191 -200 198 -182 -187 187
		mu 0 8 235 234 229 228 241 240 231 230
		f 4 188 -188 189 185
		mu 0 4 244 245 230 233
		f 8 193 -186 184 -181 -201 -202 -195 195
		mu 0 8 237 236 233 232 239 238 227 226
		f 4 196 -196 197 192
		mu 0 4 246 247 226 229
		f 4 202 201 203 199
		mu 0 4 228 227 248 249
		f 4 216 210 -218 -205
		mu 0 4 250 251 252 253
		f 4 217 211 -219 -206
		mu 0 4 253 252 254 255
		f 4 218 212 -220 -207
		mu 0 4 255 254 256 257
		f 4 219 213 -221 -208
		mu 0 4 257 256 258 259
		f 4 220 214 -222 -209
		mu 0 4 259 258 260 261
		f 4 221 215 -217 -210
		mu 0 4 261 260 262 263
		f 4 229 -229 226 224
		mu 0 4 264 265 266 267
		f 4 -243 -242 -240 -238
		mu 0 4 268 269 270 271
		f 4 234 233 231 228
		mu 0 4 265 272 273 266
		f 4 -248 -247 -245 242
		mu 0 4 268 274 275 269
		f 4 223 -225 -223 207
		mu 0 4 276 264 267 277
		f 4 222 -227 -226 206
		mu 0 4 277 267 266 278
		f 4 227 -230 -224 208
		mu 0 4 279 265 264 276
		f 4 225 -232 -231 205
		mu 0 4 280 281 282 283
		f 4 230 -234 -233 204
		mu 0 4 283 284 285 286
		f 4 232 -235 -228 209
		mu 0 4 286 287 288 289
		f 4 235 237 -237 -216
		mu 0 4 290 291 292 293
		f 4 236 239 -239 -211
		mu 0 4 293 294 295 296
		f 4 238 241 -241 -212
		mu 0 4 296 297 298 299
		f 4 240 244 -244 -213
		mu 0 4 300 269 275 301
		f 4 243 246 -246 -214
		mu 0 4 301 275 274 302
		f 4 245 247 -236 -215
		mu 0 4 302 274 268 303
		f 4 -261 258 259 -258
		mu 0 4 304 305 306 307
		f 4 -264 261 262 -252
		mu 0 4 308 309 310 311
		f 4 252 -251 -255 -257
		mu 0 4 312 313 314 315
		f 8 -260 269 267 -269 263 249 -253 -254
		mu 0 8 307 306 316 317 309 308 313 312
		f 4 250 -250 251 248
		mu 0 4 318 319 308 311
		f 8 -263 270 -266 -272 260 255 254 -249
		mu 0 8 311 310 320 321 305 304 315 314
		f 4 256 -256 257 253
		mu 0 4 322 323 304 307
		f 4 264 265 266 -268
		mu 0 4 316 321 320 317
		f 4 -271 -262 268 -267
		mu 0 4 324 310 309 325
		f 4 -270 -259 271 -265
		mu 0 4 326 306 305 327
		f 4 272 284 -279 -284
		mu 0 4 328 329 330 331
		f 4 273 285 -280 -285
		mu 0 4 329 332 333 330
		f 4 274 286 -281 -286
		mu 0 4 332 334 335 333
		f 4 275 287 -290 -287
		mu 0 4 334 336 337 335
		f 4 276 288 -282 -288
		mu 0 4 336 338 339 337
		f 4 277 283 -283 -289
		mu 0 4 338 340 341 339
		f 4 -298 -297 -295 -293
		mu 0 4 342 343 344 345
		f 4 310 309 307 305
		mu 0 4 346 347 348 349
		f 4 -306 315 314 312
		mu 0 4 346 349 350 351
		f 4 -303 -302 292 -300
		mu 0 4 352 353 342 345
		f 4 291 294 -294 -278
		mu 0 4 354 345 344 355
		f 4 293 296 -296 -273
		mu 0 4 355 344 343 356
		f 4 295 297 -291 -274
		mu 0 4 356 343 342 357
		f 4 298 299 -292 -277
		mu 0 4 358 359 360 361
		f 4 290 301 -301 -275
		mu 0 4 362 363 364 365
		f 4 300 302 -299 -276
		mu 0 4 365 366 367 368
		f 4 303 -308 -307 279
		mu 0 4 369 370 371 372
		f 4 306 -310 -309 278
		mu 0 4 372 373 374 375
		f 4 308 -311 -305 282
		mu 0 4 375 376 377 378
		f 4 304 -313 -312 281
		mu 0 4 379 380 381 382
		f 4 311 -315 -314 289
		mu 0 4 383 384 385 386
		f 4 313 -316 -304 280
		mu 0 4 386 387 388 389
		f 4 330 -339 -328 -334
		mu 0 4 390 391 392 393
		f 4 322 -319 -321 -326
		mu 0 4 394 395 396 397
		f 4 326 -325 -330 -333
		mu 0 4 398 399 400 401
		f 4 336 -320 -335 -340
		mu 0 4 402 403 404 405
		f 4 318 317 319 316
		mu 0 4 396 395 406 407
		f 8 -327 -329 327 -336 334 -318 -323 323
		mu 0 8 399 398 393 392 405 404 395 394
		f 4 324 -324 325 321
		mu 0 4 408 409 394 397
		f 8 329 -322 320 -317 -337 -338 -331 331
		mu 0 8 401 400 397 396 403 402 391 390
		f 4 332 -332 333 328
		mu 0 4 410 411 390 393
		f 4 338 337 339 335
		mu 0 4 392 391 412 413
		f 4 352 346 -354 -341
		mu 0 4 414 415 416 417
		f 4 353 347 -355 -342
		mu 0 4 417 416 418 419
		f 4 354 348 -356 -343
		mu 0 4 419 418 420 421
		f 4 355 349 -357 -344
		mu 0 4 421 420 422 423
		f 4 356 350 -358 -345
		mu 0 4 423 422 424 425
		f 4 357 351 -353 -346
		mu 0 4 425 424 426 427
		f 4 365 -365 362 360
		mu 0 4 428 429 430 431
		f 4 -379 -378 -376 -374
		mu 0 4 432 433 434 435
		f 4 370 369 367 364
		mu 0 4 429 436 437 430
		f 4 -384 -383 -381 378
		mu 0 4 432 438 439 433
		f 4 359 -361 -359 343
		mu 0 4 440 428 431 441
		f 4 358 -363 -362 342
		mu 0 4 441 431 430 442
		f 4 363 -366 -360 344
		mu 0 4 443 429 428 440
		f 4 361 -368 -367 341
		mu 0 4 444 445 446 447
		f 4 366 -370 -369 340
		mu 0 4 447 448 449 450
		f 4 368 -371 -364 345
		mu 0 4 450 451 452 453
		f 4 371 373 -373 -352
		mu 0 4 454 455 456 457
		f 4 372 375 -375 -347
		mu 0 4 457 458 459 460
		f 4 374 377 -377 -348
		mu 0 4 460 461 462 463
		f 4 376 380 -380 -349
		mu 0 4 464 433 439 465
		f 4 379 382 -382 -350
		mu 0 4 465 439 438 466
		f 4 381 383 -372 -351
		mu 0 4 466 438 432 467
		f 4 384 396 -391 -396
		mu 0 4 468 469 470 471
		f 4 385 397 -392 -397
		mu 0 4 469 472 473 470
		f 4 386 398 -393 -398
		mu 0 4 472 474 475 473
		f 4 387 399 -402 -399
		mu 0 4 474 476 477 475
		f 4 388 400 -394 -400
		mu 0 4 476 478 479 477
		f 4 389 395 -395 -401
		mu 0 4 478 480 481 479
		f 4 -409 -408 -406 -496
		mu 0 4 482 483 484 485
		f 4 421 420 418 416
		mu 0 4 486 487 488 489
		f 4 -417 426 425 423
		mu 0 4 486 489 490 491
		f 4 -414 -413 495 -411
		mu 0 4 492 493 482 485
		f 4 403 405 -405 -390
		mu 0 4 494 485 484 495
		f 4 404 407 -407 -385
		mu 0 4 495 484 483 496
		f 4 406 408 -403 -386
		mu 0 4 496 483 482 497
		f 4 409 410 -404 -389
		mu 0 4 498 499 500 501
		f 4 402 412 -412 -387
		mu 0 4 502 503 504 505
		f 4 411 413 -410 -388
		mu 0 4 505 506 507 508
		f 4 414 -419 -418 391
		mu 0 4 509 510 511 512
		f 4 417 -421 -420 390
		mu 0 4 512 513 514 515
		f 4 419 -422 -416 394
		mu 0 4 515 516 517 518
		f 4 415 -424 -423 393
		mu 0 4 519 520 521 522
		f 4 422 -426 -425 401
		mu 0 4 523 524 525 526
		f 4 424 -427 -415 392
		mu 0 4 526 527 528 529
		f 4 441 -450 -439 -445
		mu 0 4 530 531 532 533
		f 4 433 -430 -432 -437
		mu 0 4 534 535 536 537
		f 4 437 -436 -441 -444
		mu 0 4 538 539 540 541
		f 4 447 -431 -446 -451
		mu 0 4 542 543 544 545
		f 4 429 428 430 427
		mu 0 4 536 535 546 547
		f 8 -438 -440 438 -447 445 -429 -434 434
		mu 0 8 539 538 533 532 545 544 535 534
		f 4 435 -435 436 432
		mu 0 4 548 549 534 537
		f 8 440 -433 431 -428 -448 -449 -442 442
		mu 0 8 541 540 537 536 543 542 531 530
		f 4 443 -443 444 439
		mu 0 4 550 551 530 533
		f 4 449 448 450 446
		mu 0 4 532 531 552 553
		f 4 463 457 -465 -452
		mu 0 4 554 555 556 557
		f 4 464 458 -466 -453
		mu 0 4 557 556 558 559
		f 4 465 459 -467 -454
		mu 0 4 559 558 560 561
		f 4 466 460 -468 -455
		mu 0 4 561 560 562 563
		f 4 467 461 -469 -456
		mu 0 4 563 562 564 565
		f 4 468 462 -464 -457
		mu 0 4 565 564 566 567
		f 4 476 -476 473 471
		mu 0 4 568 569 570 571
		f 4 -490 -489 -487 -485
		mu 0 4 572 573 574 575
		f 4 481 480 478 475
		mu 0 4 569 576 577 570
		f 4 -495 -494 -492 489
		mu 0 4 572 578 579 573
		f 4 470 -472 -470 454
		mu 0 4 580 568 571 581
		f 4 469 -474 -473 453
		mu 0 4 581 571 570 582
		f 4 474 -477 -471 455
		mu 0 4 583 569 568 580
		f 4 472 -479 -478 452
		mu 0 4 584 585 586 587
		f 4 477 -481 -480 451
		mu 0 4 587 588 589 590
		f 4 479 -482 -475 456
		mu 0 4 590 591 592 593
		f 4 482 484 -484 -463
		mu 0 4 594 595 596 597
		f 4 483 486 -486 -458
		mu 0 4 597 598 599 600
		f 4 485 488 -488 -459
		mu 0 4 600 601 602 603
		f 4 487 491 -491 -460
		mu 0 4 604 573 579 605
		f 4 490 493 -493 -461
		mu 0 4 605 579 578 606
		f 4 492 494 -483 -462
		mu 0 4 606 578 572 607
		f 4 499 -499 -498 496
		mu 0 4 608 611 610 609
		f 4 503 -503 -502 500
		mu 0 4 612 615 614 613
		f 4 507 506 505 -505
		mu 0 4 616 619 618 617
		f 8 512 504 -512 -501 510 -510 -509 498
		mu 0 8 611 616 617 612 613 621 620 610
		f 4 -514 -504 511 -506
		mu 0 4 622 615 612 623
		f 8 513 -507 -518 -497 516 515 -515 502
		mu 0 8 615 618 619 608 609 625 624 614
		f 4 -513 -500 517 -508
		mu 0 4 626 611 608 627
		f 4 509 -520 -516 -519
		mu 0 4 620 621 624 625
		f 4 519 -511 501 514
		mu 0 4 628 629 613 614
		f 4 518 -517 497 508
		mu 0 4 630 631 609 610
		f 4 523 522 -522 -521
		mu 0 4 632 635 634 633
		f 4 521 526 -526 -525
		mu 0 4 633 634 637 636
		f 4 525 529 -529 -528
		mu 0 4 636 637 639 638
		f 4 528 532 -532 -531
		mu 0 4 638 639 641 640
		f 4 531 535 -535 -534
		mu 0 4 640 641 643 642
		f 4 534 537 -524 -537
		mu 0 4 642 643 645 644
		f 4 541 540 539 538
		mu 0 4 646 649 648 647
		f 4 -546 -545 -544 -543
		mu 0 4 650 653 652 651
		f 4 -549 -548 -547 545
		mu 0 4 650 655 654 653
		f 4 551 -542 550 549
		mu 0 4 656 649 646 657
		f 4 536 553 -541 -553
		mu 0 4 658 659 648 649
		f 4 520 554 -540 -554
		mu 0 4 659 660 647 648
		f 4 524 555 -539 -555
		mu 0 4 660 661 646 647
		f 4 533 552 -552 -557
		mu 0 4 662 665 664 663
		f 4 527 557 -551 -556
		mu 0 4 666 669 668 667
		f 4 530 556 -550 -558
		mu 0 4 669 672 671 670
		f 4 -527 559 544 -559
		mu 0 4 673 676 675 674
		f 4 -523 560 543 -560
		mu 0 4 676 679 678 677
		f 4 -538 561 542 -561
		mu 0 4 679 682 681 680
		f 4 -536 562 548 -562
		mu 0 4 683 686 685 684
		f 4 -533 563 547 -563
		mu 0 4 687 690 689 688
		f 4 -530 558 546 -564
		mu 0 4 690 693 692 691
		f 4 567 566 -566 -565
		mu 0 4 694 697 696 695
		f 4 565 570 -570 -569
		mu 0 4 695 696 699 698
		f 4 569 573 -573 -572
		mu 0 4 698 699 701 700
		f 4 572 576 -576 -575
		mu 0 4 700 701 703 702
		f 4 575 579 -579 -578
		mu 0 4 702 703 705 704
		f 4 578 581 -568 -581
		mu 0 4 704 705 707 706
		f 4 585 584 583 582
		mu 0 4 708 711 710 709
		f 4 -590 -589 -588 -587
		mu 0 4 712 715 714 713
		f 4 -593 -592 -591 589
		mu 0 4 712 717 716 715
		f 4 595 -586 594 593
		mu 0 4 718 711 708 719
		f 4 580 597 -585 -597
		mu 0 4 720 721 710 711
		f 4 564 598 -584 -598
		mu 0 4 721 722 709 710
		f 4 568 599 -583 -599
		mu 0 4 722 723 708 709
		f 4 577 596 -596 -601
		mu 0 4 724 727 726 725
		f 4 571 601 -595 -600
		mu 0 4 728 731 730 729
		f 4 574 600 -594 -602
		mu 0 4 731 734 733 732
		f 4 -571 603 588 -603
		mu 0 4 735 738 737 736
		f 4 -567 604 587 -604
		mu 0 4 738 741 740 739
		f 4 -582 605 586 -605
		mu 0 4 741 744 743 742
		f 4 -580 606 592 -606
		mu 0 4 745 748 747 746
		f 4 -577 607 591 -607
		mu 0 4 749 752 751 750
		f 4 -574 602 590 -608
		mu 0 4 752 755 754 753
		f 4 611 610 609 -609
		mu 0 4 756 759 758 757
		f 4 615 614 613 -613
		mu 0 4 760 763 762 761
		f 4 619 618 617 -617
		mu 0 4 764 767 766 765
		f 4 623 622 621 -621
		mu 0 4 768 771 770 769
		f 4 -626 -622 -625 -614
		mu 0 4 762 773 772 761
		f 8 -629 612 624 -623 627 -611 626 616
		mu 0 8 765 760 761 770 771 758 759 764
		f 4 -630 -616 628 -618
		mu 0 4 774 763 760 775
		f 8 -632 608 630 620 625 -615 629 -619
		mu 0 8 767 756 757 768 769 762 763 766
		f 4 -627 -612 631 -620
		mu 0 4 776 759 756 777
		f 4 -628 -624 -631 -610
		mu 0 4 758 779 778 757
		f 4 635 634 -634 -633
		mu 0 4 780 783 782 781
		f 4 638 637 -637 -635
		mu 0 4 783 785 784 782
		f 4 641 640 -640 -638
		mu 0 4 785 787 786 784
		f 4 644 643 -643 -641
		mu 0 4 787 789 788 786
		f 4 647 646 -646 -644
		mu 0 4 789 791 790 788
		f 4 649 632 -649 -647
		mu 0 4 791 793 792 790
		f 4 -654 -653 651 -651
		mu 0 4 794 797 796 795
		f 4 657 656 655 654
		mu 0 4 798 801 800 799
		f 4 -652 -661 -660 -659
		mu 0 4 795 796 803 802
		f 4 -655 663 662 661
		mu 0 4 798 799 805 804
		f 4 -645 665 653 -665
		mu 0 4 806 807 797 794
		f 4 -642 666 652 -666
		mu 0 4 807 808 796 797
		f 4 -648 664 650 -668
		mu 0 4 809 806 794 795
		f 4 -639 668 660 -667
		mu 0 4 810 813 812 811
		f 4 -636 669 659 -669
		mu 0 4 813 816 815 814
		f 4 -650 667 658 -670
		mu 0 4 816 819 818 817
		f 4 648 671 -658 -671
		mu 0 4 820 823 822 821
		f 4 633 672 -657 -672
		mu 0 4 823 826 825 824
		f 4 636 673 -656 -673
		mu 0 4 826 829 828 827
		f 4 639 674 -664 -674
		mu 0 4 830 831 805 799
		f 4 642 675 -663 -675
		mu 0 4 831 832 804 805
		f 4 645 670 -662 -676
		mu 0 4 832 833 798 804
		f 4 679 678 677 -677
		mu 0 4 834 837 836 835
		f 4 683 682 681 -681
		mu 0 4 838 841 840 839
		f 4 687 686 685 -685
		mu 0 4 842 845 844 843
		f 4 691 690 689 -689
		mu 0 4 846 849 848 847
		f 4 -694 -690 -693 -682
		mu 0 4 840 851 850 839
		f 8 -697 680 692 -691 695 -679 694 684
		mu 0 8 843 838 839 848 849 836 837 842
		f 4 -698 -684 696 -686
		mu 0 4 852 841 838 853
		f 8 -700 676 698 688 693 -683 697 -687
		mu 0 8 845 834 835 846 847 840 841 844
		f 4 -695 -680 699 -688
		mu 0 4 854 837 834 855
		f 4 -696 -692 -699 -678
		mu 0 4 836 857 856 835
		f 4 703 702 -702 -701
		mu 0 4 858 861 860 859
		f 4 706 705 -705 -703
		mu 0 4 861 863 862 860
		f 4 709 708 -708 -706
		mu 0 4 863 865 864 862
		f 4 712 711 -711 -709
		mu 0 4 865 867 866 864
		f 4 715 714 -714 -712
		mu 0 4 867 869 868 866
		f 4 717 700 -717 -715
		mu 0 4 869 871 870 868
		f 4 -722 -721 719 -719
		mu 0 4 872 875 874 873
		f 4 725 724 723 722
		mu 0 4 876 879 878 877
		f 4 -720 -729 -728 -727
		mu 0 4 873 874 881 880
		f 4 -723 731 730 729
		mu 0 4 876 877 883 882
		f 4 -713 733 721 -733
		mu 0 4 884 885 875 872
		f 4 -710 734 720 -734
		mu 0 4 885 886 874 875
		f 4 -716 732 718 -736
		mu 0 4 887 884 872 873
		f 4 -707 736 728 -735
		mu 0 4 888 891 890 889
		f 4 -704 737 727 -737
		mu 0 4 891 894 893 892
		f 4 -718 735 726 -738
		mu 0 4 894 897 896 895
		f 4 716 739 -726 -739
		mu 0 4 898 901 900 899
		f 4 701 740 -725 -740
		mu 0 4 901 904 903 902
		f 4 704 741 -724 -741
		mu 0 4 904 907 906 905
		f 4 707 742 -732 -742
		mu 0 4 908 909 883 877
		f 4 710 743 -731 -743
		mu 0 4 909 910 882 883
		f 4 713 738 -730 -744
		mu 0 4 910 911 876 882
		f 4 747 -747 -746 744
		mu 0 4 912 915 914 913
		f 4 751 -751 -750 748
		mu 0 4 916 919 918 917
		f 4 755 754 753 -753
		mu 0 4 920 923 922 921
		f 8 760 752 -760 -749 758 -758 -757 746
		mu 0 8 915 920 921 916 917 925 924 914
		f 4 -762 -752 759 -754
		mu 0 4 926 919 916 927
		f 8 761 -755 -766 -745 764 763 -763 750
		mu 0 8 919 922 923 912 913 929 928 918
		f 4 -761 -748 765 -756
		mu 0 4 930 915 912 931
		f 4 757 -768 -764 -767
		mu 0 4 924 925 928 929
		f 4 767 -759 749 762
		mu 0 4 932 933 917 918
		f 4 766 -765 745 756
		mu 0 4 934 935 913 914
		f 4 771 770 -770 -769
		mu 0 4 936 939 938 937
		f 4 769 774 -774 -773
		mu 0 4 937 938 941 940
		f 4 773 777 -777 -776
		mu 0 4 940 941 943 942
		f 4 776 780 -780 -779
		mu 0 4 942 943 945 944
		f 4 779 783 -783 -782
		mu 0 4 944 945 947 946
		f 4 782 785 -772 -785
		mu 0 4 946 947 949 948
		f 4 789 788 787 786
		mu 0 4 950 953 952 951
		f 4 -794 -793 -792 -791
		mu 0 4 954 957 956 955
		f 4 -797 -796 -795 793
		mu 0 4 954 959 958 957
		f 4 799 -790 798 797
		mu 0 4 960 953 950 961
		f 4 784 801 -789 -801
		mu 0 4 962 963 952 953
		f 4 768 802 -788 -802
		mu 0 4 963 964 951 952
		f 4 772 803 -787 -803
		mu 0 4 964 965 950 951
		f 4 781 800 -800 -805
		mu 0 4 966 969 968 967
		f 4 775 805 -799 -804
		mu 0 4 970 973 972 971
		f 4 778 804 -798 -806
		mu 0 4 973 976 975 974
		f 4 -775 807 792 -807
		mu 0 4 977 980 979 978
		f 4 -771 808 791 -808
		mu 0 4 980 983 982 981
		f 4 -786 809 790 -809
		mu 0 4 983 986 985 984
		f 4 -784 810 796 -810
		mu 0 4 987 990 989 988
		f 4 -781 811 795 -811
		mu 0 4 991 994 993 992
		f 4 -778 806 794 -812
		mu 0 4 994 997 996 995
		f 4 815 814 813 -813
		mu 0 4 998 1001 1000 999
		f 4 819 818 817 -817
		mu 0 4 1002 1005 1004 1003
		f 4 823 822 821 -821
		mu 0 4 1006 1009 1008 1007
		f 4 827 826 825 -825
		mu 0 4 1010 1013 1012 1011
		f 4 -830 -826 -829 -818
		mu 0 4 1004 1015 1014 1003
		f 8 -833 816 828 -827 831 -815 830 820
		mu 0 8 1007 1002 1003 1012 1013 1000 1001 1006
		f 4 -834 -820 832 -822
		mu 0 4 1016 1005 1002 1017
		f 8 -836 812 834 824 829 -819 833 -823
		mu 0 8 1009 998 999 1010 1011 1004 1005 1008
		f 4 -831 -816 835 -824
		mu 0 4 1018 1001 998 1019
		f 4 -832 -828 -835 -814
		mu 0 4 1000 1021 1020 999
		f 4 839 838 -838 -837
		mu 0 4 1022 1025 1024 1023
		f 4 842 841 -841 -839
		mu 0 4 1025 1027 1026 1024
		f 4 845 844 -844 -842
		mu 0 4 1027 1029 1028 1026
		f 4 848 847 -847 -845
		mu 0 4 1029 1031 1030 1028
		f 4 851 850 -850 -848
		mu 0 4 1031 1033 1032 1030
		f 4 853 836 -853 -851
		mu 0 4 1033 1035 1034 1032
		f 4 -858 -857 855 -855
		mu 0 4 1036 1039 1038 1037
		f 4 861 860 859 858
		mu 0 4 1040 1043 1042 1041
		f 4 -856 -865 -864 -863
		mu 0 4 1037 1038 1045 1044
		f 4 -859 867 866 865
		mu 0 4 1040 1041 1047 1046
		f 4 -849 869 857 -869
		mu 0 4 1048 1049 1039 1036
		f 4 -846 870 856 -870
		mu 0 4 1049 1050 1038 1039
		f 4 -852 868 854 -872
		mu 0 4 1051 1048 1036 1037
		f 4 -843 872 864 -871
		mu 0 4 1052 1055 1054 1053
		f 4 -840 873 863 -873
		mu 0 4 1055 1058 1057 1056
		f 4 -854 871 862 -874
		mu 0 4 1058 1061 1060 1059
		f 4 852 875 -862 -875
		mu 0 4 1062 1065 1064 1063
		f 4 837 876 -861 -876
		mu 0 4 1065 1068 1067 1066
		f 4 840 877 -860 -877
		mu 0 4 1068 1071 1070 1069
		f 4 843 878 -868 -878
		mu 0 4 1072 1073 1047 1041
		f 4 846 879 -867 -879
		mu 0 4 1073 1074 1046 1047
		f 4 849 874 -866 -880
		mu 0 4 1074 1075 1040 1046
		f 4 883 882 -882 -881
		mu 0 4 1076 1079 1078 1077
		f 4 881 886 -886 -885
		mu 0 4 1077 1078 1081 1080
		f 4 885 889 -889 -888
		mu 0 4 1080 1081 1083 1082
		f 4 888 892 -892 -891
		mu 0 4 1082 1083 1085 1084
		f 4 891 895 -895 -894
		mu 0 4 1084 1085 1087 1086
		f 4 894 897 -884 -897
		mu 0 4 1086 1087 1089 1088
		f 4 901 900 899 898
		mu 0 4 1090 1093 1092 1091
		f 4 -906 -905 -904 -903
		mu 0 4 1094 1097 1096 1095
		f 4 -909 -908 -907 905
		mu 0 4 1094 1099 1098 1097
		f 4 911 -902 910 909
		mu 0 4 1100 1093 1090 1101
		f 4 896 913 -901 -913
		mu 0 4 1102 1103 1092 1093
		f 4 880 914 -900 -914
		mu 0 4 1103 1104 1091 1092
		f 4 884 915 -899 -915
		mu 0 4 1104 1105 1090 1091
		f 4 893 912 -912 -917
		mu 0 4 1106 1109 1108 1107
		f 4 887 917 -911 -916
		mu 0 4 1110 1113 1112 1111
		f 4 890 916 -910 -918
		mu 0 4 1113 1116 1115 1114
		f 4 -887 919 904 -919
		mu 0 4 1117 1120 1119 1118
		f 4 -883 920 903 -920
		mu 0 4 1120 1123 1122 1121
		f 4 -898 921 902 -921
		mu 0 4 1123 1126 1125 1124
		f 4 -896 922 908 -922
		mu 0 4 1127 1130 1129 1128
		f 4 -893 923 907 -923
		mu 0 4 1131 1134 1133 1132
		f 4 -890 918 906 -924
		mu 0 4 1134 1137 1136 1135
		f 4 927 926 925 -925
		mu 0 4 1138 1141 1140 1139
		f 4 931 930 929 -929
		mu 0 4 1142 1145 1144 1143
		f 4 935 934 933 -933
		mu 0 4 1146 1149 1148 1147
		f 4 939 938 937 -937
		mu 0 4 1150 1153 1152 1151
		f 4 -942 -938 -941 -930
		mu 0 4 1144 1155 1154 1143
		f 8 -945 928 940 -939 943 -927 942 932
		mu 0 8 1147 1142 1143 1152 1153 1140 1141 1146
		f 4 -946 -932 944 -934
		mu 0 4 1156 1145 1142 1157
		f 8 -948 924 946 936 941 -931 945 -935
		mu 0 8 1149 1138 1139 1150 1151 1144 1145 1148
		f 4 -943 -928 947 -936
		mu 0 4 1158 1141 1138 1159
		f 4 -944 -940 -947 -926
		mu 0 4 1140 1161 1160 1139
		f 4 951 950 -950 -949
		mu 0 4 1162 1165 1164 1163
		f 4 954 953 -953 -951
		mu 0 4 1165 1167 1166 1164
		f 4 957 956 -956 -954
		mu 0 4 1167 1169 1168 1166
		f 4 960 959 -959 -957
		mu 0 4 1169 1171 1170 1168
		f 4 963 962 -962 -960
		mu 0 4 1171 1173 1172 1170
		f 4 965 948 -965 -963
		mu 0 4 1173 1175 1174 1172
		f 4 -970 -969 967 -967
		mu 0 4 1176 1179 1178 1177
		f 4 973 972 971 970
		mu 0 4 1180 1183 1182 1181
		f 4 -968 -977 -976 -975
		mu 0 4 1177 1178 1185 1184
		f 4 -971 979 978 977
		mu 0 4 1180 1181 1187 1186
		f 4 -961 981 969 -981
		mu 0 4 1188 1189 1179 1176
		f 4 -958 982 968 -982
		mu 0 4 1189 1190 1178 1179
		f 4 -964 980 966 -984
		mu 0 4 1191 1188 1176 1177
		f 4 -955 984 976 -983
		mu 0 4 1192 1195 1194 1193
		f 4 -952 985 975 -985
		mu 0 4 1195 1198 1197 1196
		f 4 -966 983 974 -986
		mu 0 4 1198 1201 1200 1199
		f 4 964 987 -974 -987
		mu 0 4 1202 1205 1204 1203
		f 4 949 988 -973 -988
		mu 0 4 1205 1208 1207 1206
		f 4 952 989 -972 -989
		mu 0 4 1208 1211 1210 1209
		f 4 955 990 -980 -990
		mu 0 4 1212 1213 1187 1181
		f 4 958 991 -979 -991
		mu 0 4 1213 1214 1186 1187
		f 4 961 986 -978 -992
		mu 0 4 1214 1215 1180 1186;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".vcs" 2;
createNode transform -n "MSH_springs_tutorial" -p "GRP_utility_meshes";
	rename -uid "B725C4DB-4BC7-918F-9F38-9AA1ECCA5516";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode mesh -n "MSH_springs_tutorialShape" -p "MSH_springs_tutorial";
	rename -uid "C5629490-47FC-B396-AE1A-EB93AE12F377";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".sdt" 0;
	setAttr ".ugsdt" no;
	setAttr ".vcs" 2;
createNode mesh -n "MSH_springs_tutorialShapeOrig" -p "MSH_springs_tutorial";
	rename -uid "1E2065DE-462C-2353-51D5-60AF0F107FAE";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".sdt" 0;
	setAttr -s 972 ".vt";
	setAttr ".vt[0:165]"  -29.24106407 50.8389473 -148.043167114 -35.90217972 55.016765594 -148.043151855
		 -29.24106789 50.83895111 -155.90594482 -35.9021759 55.016788483 -155.90594482 -54.5196991 21.59993744 -153.90634155
		 -52.88311768 20.57347107 -154.70655823 -51.24654388 19.54700851 -153.90638733 -50.56864166 19.12183762 -151.97447205
		 -51.24654388 19.5470047 -150.042663574 -52.88311768 20.57347488 -149.24250793 -54.5196991 21.59993744 -150.042694092
		 -55.19758987 22.025112152 -151.97447205 -34.36645508 53.73210144 -153.90632629 -32.72986603 52.70567322 -154.70654297
		 -31.093309402 51.67921448 -153.90634155 -30.41540337 51.25401688 -151.9744873 -31.093305588 51.67921448 -150.042709351
		 -32.72988129 52.70568848 -149.24247742 -34.36646271 53.73213959 -150.042709351 -35.04434967 54.15730286 -151.97451782
		 -40.69882965 40.000022888184 -158.98306274 -36.50046921 37.36684418 -156.93032837
		 -34.76145172 36.27613068 -151.97453308 -36.50046158 37.36684799 -147.018722534 -40.69881821 40.000026702881 -144.96600342
		 -44.89717102 42.63321686 -147.018692017 -46.63619232 43.72396088 -151.9745636 -44.89718246 42.63323593 -156.93035889
		 -39.26735687 42.28232193 -158.98309326 -35.069015503 39.6491127 -156.93032837 -33.3299942 38.55844116 -151.97453308
		 -35.069007874 39.64915085 -147.018722534 -39.26737213 42.28233719 -144.96598816 -43.46572113 44.91553879 -147.018722534
		 -45.20471954 46.0062217712 -151.97451782 -43.4657135 44.91550446 -156.93032837 -36.081466675 53.29201508 -156.011306763
		 -38.74759674 54.96417999 -152.45707703 -36.49902344 53.55387878 -152.27340698 -37.94907379 53.77619934 -152.38606262
		 -37.38196182 54.10766983 -147.63046265 -35.67964935 53.039978027 -149.37739563 -37.27384186 52.46386719 -148.54658508
		 -33.11206055 52.0963974 -146.64924622 -33.81941986 50.96850586 -147.9805603 -34.52682877 49.84067917 -146.64923096
		 -33.81943512 50.96851349 -145.31790161 -42.45738602 44.2831192 -155.74008179 -39.26735687 42.28232574 -158.63114929
		 -39.2673645 42.28232193 -155.96847534 -39.0068626404 42.69765854 -157.29980469 -35.27984238 39.78138351 -156.681427
		 -36.87483978 40.78174973 -154.79862976 -35.52210236 41.16682053 -155.74008179 -35.32077408 38.55231857 -151.97451782
		 -35.7412262 40.3875351 -151.97454834 -33.90600967 40.80799866 -151.97451782 -33.48555374 38.97277451 -151.97451782
		 -36.34738922 39.85100937 -148.20899963 -36.43750763 41.4790535 -149.15036011 -34.93263245 42.10668564 -148.20899963
		 -34.84250259 40.47865677 -147.26757813 -39.24266815 42.32171631 -146.64926147 -38.53529358 43.44953918 -147.98051453
		 -37.82791138 44.57738495 -146.64923096 -38.53529358 43.44957733 -145.31790161 -42.13794708 44.79242706 -148.20896912
		 -40.6330719 45.42006302 -149.15034485 -40.72317886 47.048130035 -148.20899963 -42.22807312 46.42046738 -147.26760864
		 -43.16455078 46.091083527 -151.97451782 -41.32933426 46.51156616 -151.97450256 -41.74979401 48.34677887 -151.97454834
		 -43.58501434 47.92633057 -151.97447205 -41.54845428 45.7322998 -155.74008179 -40.043571472 46.35995865 -154.79867554
		 -40.13370514 47.98799133 -155.74005127 -41.63859558 47.36032867 -156.68147278 -38.063697815 44.20148087 -157.29980469
		 -37.35631561 45.32932663 -155.96850586 -36.64894104 46.45714951 -157.29980469 -37.3563118 45.32932663 -158.63111877
		 -34.57892609 42.67059708 -155.74008179 -34.66906738 44.29866028 -154.79867554 -33.16416931 44.92630386 -155.74008179
		 -33.074050903 43.29827118 -156.681427 -32.96283722 42.3117981 -151.97451782 -33.38330841 44.14702225 -151.97451782
		 -31.54807854 44.56750488 -151.97451782 -31.1276207 42.73225403 -151.97451782 -33.98945618 43.61048508 -148.20899963
		 -34.079589844 45.23852539 -149.15036011 -32.57469177 45.86619186 -148.20896912 -32.48457336 44.23812485 -147.26757813
		 -36.88472748 46.081203461 -146.64923096 -36.17735672 47.20904922 -147.98054504 -35.4699707 48.33687973 -146.64923096
		 -36.17736053 47.20905304 -145.31790161 -39.78001404 48.5519104 -148.20896912 -38.27513504 49.17957687 -149.15036011
		 -38.36525345 50.80758667 -148.20896912 -39.87014771 50.17995071 -147.26757813 -40.80661774 49.85055161 -151.97451782
		 -38.97141266 50.2710495 -151.97451782 -39.39186096 52.10626984 -151.97453308 -41.22708893 51.68580627 -151.97450256
		 -39.19052887 49.49176407 -155.74008179 -37.68563461 50.11944199 -154.79870605 -37.77576447 51.74747467 -155.74008179
		 -39.28064728 51.11979675 -156.68145752 -35.70573425 47.9609108 -157.29980469 -34.99837494 49.08877182 -155.96847534
		 -34.29098511 50.21662521 -157.29980469 -34.99835968 49.088764191 -158.63113403 -32.22099686 46.43009949 -155.74005127
		 -32.31111908 48.058132172 -154.79867554 -30.80623055 48.68577576 -155.74005127 -30.7161026 47.057735443 -156.68145752
		 -30.60490036 46.071281433 -151.97450256 -31.025373459 47.90649414 -151.97450256 -29.19015503 48.3269577 -151.9744873
		 -28.76968193 46.49176788 -151.97453308 -31.6315155 47.36997223 -148.20896912 -31.72166252 48.9980011 -149.15037537
		 -30.21677208 49.6256485 -148.20895386 -30.12664795 47.99764633 -147.26754761 -33.41217804 51.61779785 -158.98304749
		 -29.21380806 48.98463821 -156.93032837 -27.474823 47.89389801 -151.97447205 -29.21383476 48.98461151 -147.018722534
		 -33.41217804 51.61783981 -144.9659729 -37.6105423 54.25101089 -147.018722534 -39.34954071 55.34172821 -151.97454834
		 -37.61051941 54.25102997 -156.93032837 -32.0080795288 53.85653687 -158.98306274 -27.80970955 51.22335434 -156.93032837
		 -26.070697784 50.13262939 -151.9744873 -27.80971718 51.22335434 -147.018692017 -32.0080718994 53.85652924 -144.96595764
		 -36.20642853 56.48974228 -147.018707275 -37.94542694 57.58043289 -151.97454834 -36.20640564 56.48973465 -156.93032837
		 -48.35235977 21.48641968 -148.62130737 -54.033760071 25.049793243 -148.62130737 -48.35234451 21.48641586 -155.32771301
		 -54.033752441 25.049804688 -155.32774353 -25.97711563 56.042945862 -148.043151855
		 -26.52030945 58.4139061 -148.043151855 -25.97711563 56.042964935 -155.90591431 -26.52030754 58.41389084 -155.90594482
		 -30.26727867 60.76399231 -148.043151855 -32.63822937 60.22078705 -148.043151855 -32.63822174 60.22079086 -155.90594482
		 -30.26727104 60.76399231 -155.90594482 -53.52400589 16.8137207 -148.62130737 -50.90700531 17.41327286 -148.62130737
		 -50.90699768 17.41327667 -155.32774353 -53.52398682 16.8137207 -155.32771301 -56.58840942 20.97666168 -148.62132263
		 -55.98884583 18.35964584 -148.62130737 -55.98884201 18.35968018 -155.32774353 -56.58840942 20.9766655 -155.32771301
		 -29.24106026 50.8389473 -118.19664001 -35.90217209 55.016765594 -118.19664001 -29.24106407 50.83895111 -126.059402466
		 -35.90218353 55.016788483 -126.059463501;
	setAttr ".vt[166:331]" -54.5196991 21.59993744 -124.059814453 -52.88311768 20.57347107 -124.86003113
		 -51.24654388 19.54700851 -124.059844971 -50.56864166 19.12183762 -122.12796021 -51.24653625 19.5470047 -120.19613647
		 -52.88311768 20.57347488 -119.39598083 -54.5196991 21.59993744 -120.19616699 -55.19758987 22.025112152 -122.12796021
		 -34.36645508 53.73213196 -124.059844971 -32.72986984 52.70567322 -124.86003113 -31.093305588 51.67921448 -124.059814453
		 -30.41540337 51.25404739 -122.12797546 -31.093301773 51.67921448 -120.19616699 -32.72987747 52.70565796 -119.39595032
		 -34.36646271 53.73213959 -120.19613647 -35.044345856 54.15730286 -122.12799072 -40.69882584 40.000022888184 -129.13653564
		 -36.50046539 37.36684418 -127.083786011 -34.76145172 36.27613068 -122.12799072 -36.50046539 37.36684799 -117.17221069
		 -40.69882202 40.000026702881 -115.11946106 -44.89717484 42.63324738 -117.17222595
		 -46.63619232 43.72396088 -122.1280365 -44.89717865 42.63323593 -127.08380127 -39.26735687 42.28232193 -129.13656616
		 -35.069015503 39.6491127 -127.083786011 -33.33000183 38.55847168 -122.12799072 -35.069007874 39.64915085 -117.17219543
		 -39.26737213 42.28233719 -115.1194458 -43.46572113 44.91553879 -117.17221069 -45.2047348 46.0062217712 -122.12797546
		 -43.4657135 44.91553497 -127.08380127 -36.081459045 53.29201508 -126.16473389 -38.74760056 54.96417999 -122.61053467
		 -36.49902344 53.55387878 -122.42689514 -37.94908142 53.77619934 -122.53953552 -37.38195801 54.10766983 -117.78392029
		 -35.67964172 53.039978027 -119.53088379 -37.27384949 52.46389771 -118.70004272 -33.11205292 52.096366882 -116.80270386
		 -33.81943512 50.96850586 -118.13404846 -34.52682495 49.84067917 -116.80271912 -33.81944275 50.96851349 -115.47138977
		 -42.45737839 44.2831192 -125.89353943 -39.26735687 42.28232574 -128.78462219 -39.2673645 42.28232193 -126.12193298
		 -39.0068664551 42.69765854 -127.45326233 -35.27983856 39.781353 -126.83488464 -36.87484741 40.78174973 -124.95211792
		 -35.52211761 41.16682053 -125.89355469 -35.32077408 38.55231857 -122.12800598 -35.74123383 40.3875351 -122.12800598
		 -33.90601349 40.80799866 -122.12800598 -33.48554993 38.97277451 -122.12799072 -36.34738159 39.85100937 -118.36245728
		 -36.43751144 41.4790535 -119.30383301 -34.93263245 42.10668564 -118.36245728 -34.84250259 40.47865677 -117.42103577
		 -39.24266815 42.32171631 -116.80271912 -38.53528976 43.44953918 -118.13400269 -37.82791138 44.57738495 -116.80271912
		 -38.53530121 43.44957733 -115.47135925 -42.13794708 44.79242706 -118.36244202 -40.6330719 45.42006302 -119.30380249
		 -40.72318268 47.048130035 -118.36244202 -42.22807312 46.42046738 -117.42106628 -43.16455078 46.091114044 -122.12799072
		 -41.32933044 46.51156616 -122.12797546 -41.74979401 48.34677887 -122.12800598 -43.58501434 47.92633057 -122.12796021
		 -41.5484581 45.7322998 -125.89356995 -40.043571472 46.35992813 -124.95213318 -40.13370514 47.98799133 -125.89350891
		 -41.63860321 47.36032867 -126.83493042 -38.063697815 44.20148087 -127.45329285 -37.35631943 45.32929611 -126.1219635
		 -36.64894104 46.45714951 -127.45326233 -37.35631943 45.32929611 -128.78456116 -34.57894135 42.67062759 -125.89355469
		 -34.66906357 44.29866028 -124.95214844 -33.16417694 44.92630386 -125.89353943 -33.074043274 43.29827118 -126.83491516
		 -32.96283722 42.31182861 -122.12800598 -33.38330841 44.14702225 -122.12797546 -31.54807854 44.56750488 -122.12800598
		 -31.12761688 42.73225403 -122.12799072 -33.98945618 43.61045456 -118.36242676 -34.079586029 45.23852539 -119.30383301
		 -32.5746994 45.86619186 -118.36245728 -32.48457336 44.23812485 -117.42103577 -36.88473511 46.081203461 -116.8026886
		 -36.17735291 47.20904922 -118.13401794 -35.4699707 48.33687973 -116.8026886 -36.17736435 47.20905304 -115.47135925
		 -39.78001022 48.5519104 -118.36244202 -38.27513123 49.17957687 -119.30384827 -38.36525345 50.80758667 -118.36244202
		 -39.87015533 50.17995071 -117.42103577 -40.80661774 49.85055161 -122.12797546 -38.97141266 50.2710495 -122.12797546
		 -39.39186096 52.10626984 -122.12802124 -41.22708893 51.68580627 -122.12799072 -39.19052124 49.49176407 -125.89353943
		 -37.68563461 50.11944199 -124.95219421 -37.77576447 51.74744415 -125.89353943 -39.28064728 51.11979675 -126.83494568
		 -35.70574188 47.9609108 -127.45326233 -34.99837112 49.08877182 -126.12193298 -34.29099274 50.21662521 -127.45326233
		 -34.99837112 49.088794708 -128.78460693 -32.22099304 46.43006897 -125.89350891 -32.31111908 48.058132172 -124.95214844
		 -30.80623055 48.68577576 -125.89350891 -30.71611404 47.057735443 -126.83494568 -30.60490417 46.071281433 -122.12796021
		 -31.025369644 47.90649414 -122.12797546 -29.19015121 48.3269577 -122.12797546 -28.76967812 46.49173737 -122.12796021
		 -31.63151932 47.36997223 -118.36245728 -31.72165871 48.9980011 -119.30383301 -30.21676826 49.6256485 -118.3624115
		 -30.12664413 47.99761581 -117.42100525 -33.41218567 51.61782837 -129.13652039 -29.2138195 48.98463821 -127.083816528
		 -27.47481918 47.89389801 -122.12796021 -29.21383095 48.98461151 -117.17221069 -33.41217041 51.61783981 -115.11946106
		 -37.61053467 54.25101089 -117.17219543 -39.34955597 55.34172821 -122.12802124 -37.61052704 54.25099945 -127.083786011
		 -32.0080718994 53.85656738 -129.13653564 -27.80971718 51.22335434 -127.083831787
		 -26.070705414 50.13262939 -122.12794495 -27.80971336 51.22332382 -117.17221069 -32.0080718994 53.85652924 -115.11941528
		 -36.2064209 56.48971176 -117.17214966 -37.94543457 57.58043289 -122.12802124 -36.20640564 56.48973465 -127.083786011
		 -48.35235596 21.48641968 -118.77476501 -54.033760071 25.049793243 -118.77479553 -48.3523407 21.48638535 -125.4811554
		 -54.033744812 25.049804688 -125.48120117 -25.97711945 56.042945862 -118.19664001
		 -26.52030563 58.4139061 -118.1966095 -25.97712326 56.042964935 -126.059402466 -26.52030373 58.41392136 -126.059432983
		 -30.26727104 60.76399231 -118.19664001 -32.63822937 60.22078705 -118.19662476 -32.63822174 60.22079086 -126.059417725
		 -30.26727104 60.76399231 -126.059432983 -53.52400208 16.8137207 -118.77479553 -50.90700531 17.41327286 -118.77476501
		 -50.90700531 17.413311 -125.48120117 -53.52399063 16.8137207 -125.48117065 -56.58841705 20.97666168 -118.77478027
		 -55.98884583 18.35964584 -118.77478027 -55.98884583 18.35968018 -125.48120117 -56.58840942 20.9766655 -125.48117065
		 -29.30729485 51.7395134 139.93135071 -35.96840668 55.91734314 139.93136597 -29.30729866 51.7395134 132.068572998
		 -35.96841431 55.91733932 132.068572998 -54.58592987 22.50050354 134.06817627 -52.94935989 21.47405243 133.26795959
		 -51.31277466 20.44758606 134.068161011 -50.63487244 20.022403717 136.000015258789;
	setAttr ".vt[332:497]" -51.31277084 20.44758224 137.93185425 -52.94934845 21.47405243 138.73200989
		 -54.5859375 22.50051117 137.93182373 -55.26382446 22.92567825 136.000015258789 -34.43270111 54.63268661 134.068161011
		 -32.79611206 53.6062355 133.26797485 -31.15954018 52.57977676 134.06817627 -30.48163795 52.15460587 136
		 -31.15953636 52.57977676 137.93183899 -32.79611206 53.60624695 138.73204041 -34.43269348 54.63269424 137.93183899
		 -35.11058044 55.057857513 136 -40.76506805 40.90058899 128.99145508 -36.5667038 38.26739883 131.044189453
		 -34.8276825 37.17669296 136.000015258789 -36.56669617 38.26739502 140.95579529 -40.7650528 40.9005928 143.0085449219
		 -44.96340942 43.5337944 140.95579529 -46.7024231 44.62450027 135.99998474 -44.96341705 43.53378296 131.044189453
		 -39.33360291 43.18290329 128.99142456 -35.13524628 40.54970169 131.044189453 -33.39622879 39.45899963 136.000015258789
		 -35.13525391 40.54971313 140.95579529 -39.33360291 43.18291092 143.0085601807 -43.5319519 45.81609344 140.95576477
		 -45.27097321 46.90679932 136 -43.53194427 45.816082 131.044189453 -36.14769745 54.19256592 131.96324158
		 -38.81383896 55.86475754 135.51747131 -36.56526184 54.45445633 135.70111084 -38.015319824 54.67677689 135.5884552
		 -37.4481926 55.0082321167 140.34405518 -35.74588013 53.9405365 138.59709167 -37.34009171 53.36444092 139.42796326
		 -33.17829132 52.9969368 141.32530212 -33.88566589 51.86909103 139.99395752 -34.59305954 50.74124146 141.32528687
		 -33.88568115 51.86909866 142.65661621 -42.52362061 45.18367767 132.23443604 -39.33360291 43.18290329 129.34339905
		 -39.33359528 43.18289566 132.0060424805 -39.0730896 43.59822083 130.67471313 -35.34607697 40.68193054 131.29309082
		 -36.94107437 41.68231583 133.17585754 -35.58834839 42.067394257 132.23443604 -35.3870163 39.4528923 136
		 -35.80747223 41.28811264 136 -33.97225189 41.70858383 136 -33.55178833 39.87334824 136
		 -36.41362762 40.7515831 139.76554871 -36.50374985 42.3796196 138.82415771 -34.99887085 43.0072631836 139.76554871
		 -34.90872955 41.37922668 140.7069397 -39.30891418 43.22228622 141.32528687 -38.60152435 44.35012054 139.99397278
		 -37.89415741 45.47796631 141.32528687 -38.60153961 44.3501358 142.65661621 -42.20417786 45.69300461 139.76554871
		 -40.69930267 46.32064438 138.82417297 -40.78942108 47.94868088 139.76554871 -42.29431152 47.32104111 140.7069397
		 -43.23078918 46.99168015 136 -41.39556122 47.41213226 136.000015258789 -41.81603241 49.24735641 136
		 -43.65124893 48.82689667 136.000015258789 -41.61468124 46.63287354 132.23443604 -40.10980606 47.26051712 133.17584229
		 -40.19994736 48.8885498 132.23446655 -41.70484161 48.26091385 131.29307556 -38.12992096 45.10203171 130.67471313
		 -37.42255783 46.22986984 132.0060119629 -36.71517181 47.35770416 130.67471313 -37.42255402 46.22986603 129.34339905
		 -34.64517593 43.57118607 132.23443604 -34.73529816 45.19921112 133.17584229 -33.23041153 45.82687378 132.23443604
		 -33.14028931 44.19882965 131.29309082 -33.029067993 43.21236038 136 -33.44953918 45.047588348 136
		 -31.61432076 45.46805573 136 -31.19385338 43.63282776 136 -34.055698395 44.51105118 139.76554871
		 -34.14582062 46.13909149 138.82415771 -32.64094162 46.76673889 139.76554871 -32.55080414 45.13869476 140.7069397
		 -36.95097351 46.98177338 141.32528687 -36.24358749 48.10960388 139.99397278 -35.53620911 49.23745346 141.32528687
		 -36.24358749 48.1096077 142.65661621 -39.84624481 49.45247269 139.76554871 -38.34136581 50.080120087 138.82415771
		 -38.43148804 51.7081604 139.76554871 -39.93639374 51.080524445 140.7069397 -40.87286377 50.75114822 136
		 -39.037643433 51.17162323 136 -39.45809555 53.0068435669 135.99998474 -41.29333115 52.58637619 135.99998474
		 -39.25675201 50.39234543 132.23443604 -37.75186539 51.020000458 133.17581177 -37.84199524 52.64802551 132.23443604
		 -39.34688568 52.020366669 131.2930603 -35.7719841 48.86149216 130.67471313 -35.064605713 49.98934174 132.0060424805
		 -34.35723114 51.11719131 130.67471313 -35.064605713 49.98933792 129.34338379 -32.28722763 47.33065033 132.23446655
		 -32.37734985 48.95868683 133.17584229 -30.87246513 49.58633804 132.23446655 -30.78234673 47.95830154 131.2930603
		 -30.67114449 46.97185516 136.000015258789 -31.091604233 48.80706406 136.000015258789
		 -29.25638771 49.22752762 136.000030517578 -28.83592796 47.39231873 136.000015258789
		 -31.69776344 48.27053452 139.76554871 -31.78788948 49.89855957 138.82417297 -30.28300285 50.5262146 139.76556396
		 -30.19288063 48.89818192 140.70697021 -33.47842407 52.51838684 128.99147034 -29.28004837 49.88518524 131.044189453
		 -27.54105186 48.79447937 136.000015258789 -29.28006744 49.88518143 140.95579529 -33.47840881 52.51839828 143.0085449219
		 -37.67677307 55.15158081 140.95579529 -39.41578674 56.24229431 135.99996948 -37.67676544 55.15158081 131.044189453
		 -32.074306488 54.75710678 128.99145508 -27.87595177 52.123909 131.044189453 -26.13693619 51.033195496 136.000030517578
		 -27.87594414 52.12390518 140.95579529 -32.074302673 54.75709534 143.0085601807 -36.2726593 57.39029312 140.95581055
		 -38.011672974 58.48101044 136 -36.27264023 57.39027786 131.044189453 -48.41859436 22.38697815 139.35321045
		 -54.099990845 25.9503479 139.35321045 -48.41858673 22.38698196 132.64680481 -54.099983215 25.95035934 132.64680481
		 -26.043346405 56.94352722 139.93136597 -26.58654022 59.31446838 139.93136597 -26.043357849 56.94352341 132.068603516
		 -26.58653831 59.31445694 132.068572998 -30.33350563 61.66456604 139.93136597 -32.70446014 61.12135696 139.93136597
		 -32.70446014 61.12136841 132.068572998 -30.33350945 61.66456985 132.068572998 -53.59023285 17.7142868 139.35321045
		 -50.97325134 18.3138504 139.35321045 -50.97324371 18.31385803 132.64680481 -53.59023285 17.71429443 132.64680481
		 -56.6546402 21.87722397 139.35319519 -56.055084229 19.26023865 139.35321045 -56.055084229 19.26024246 132.64680481
		 -56.6546402 21.87722397 132.64680481 29.24106407 50.8389473 -148.043167114 35.90217972 55.016765594 -148.043136597
		 29.24106789 50.83895111 -155.90592957 35.9021759 55.016788483 -155.90596008 54.5196991 21.59993744 -153.90634155
		 52.88311768 20.57347107 -154.70655823 51.24654388 19.54700851 -153.90637207 50.56864166 19.12183762 -151.9744873
		 51.24654388 19.5470047 -150.042663574 52.88311768 20.57347488 -149.24250793 54.5196991 21.59993744 -150.042694092
		 55.19758987 22.025112152 -151.9744873;
	setAttr ".vt[498:663]" 34.36645508 53.73210144 -153.90634155 32.72986603 52.70567322 -154.70655823
		 31.093309402 51.67921448 -153.90634155 30.41540337 51.25401688 -151.9744873 31.093305588 51.67921448 -150.042694092
		 32.72988129 52.70568848 -149.24247742 34.36646271 53.73213959 -150.042694092 35.04434967 54.15730286 -151.97451782
		 40.69882965 40.000022888184 -158.98306274 36.50046921 37.36684418 -156.93031311 34.76145172 36.27613068 -151.97451782
		 36.50046158 37.36684799 -147.018722534 40.69881821 40.000026702881 -144.96600342
		 44.89717102 42.63321686 -147.018707275 46.63619232 43.72396088 -151.9745636 44.89718246 42.63323593 -156.93035889
		 39.26735687 42.28232193 -158.98309326 35.069015503 39.6491127 -156.93031311 33.3299942 38.55844116 -151.97451782
		 35.069007874 39.64915085 -147.018722534 39.26737213 42.28233719 -144.9659729 43.46572113 44.91553879 -147.018737793
		 45.20471954 46.0062217712 -151.97453308 43.4657135 44.91550446 -156.93032837 36.081466675 53.29201508 -156.011291504
		 38.74759674 54.96417999 -152.45706177 36.49902344 53.55387878 -152.27342224 37.94907379 53.77619934 -152.38606262
		 37.38196182 54.10766983 -147.63044739 35.67964935 53.039978027 -149.37741089 37.27384186 52.46386719 -148.54658508
		 33.11206055 52.0963974 -146.64924622 33.81941986 50.96850586 -147.98054504 34.52682877 49.84067917 -146.64924622
		 33.81943512 50.96851349 -145.31788635 42.45738602 44.2831192 -155.74006653 39.26735687 42.28232574 -158.63113403
		 39.2673645 42.28232193 -155.96847534 39.0068626404 42.69765854 -157.29978943 35.27984238 39.78138351 -156.68144226
		 36.87483978 40.78174973 -154.79864502 35.52210236 41.16682053 -155.74008179 35.32077408 38.55231857 -151.97453308
		 35.7412262 40.3875351 -151.97454834 33.90600967 40.80799866 -151.97450256 33.48555374 38.97277451 -151.97451782
		 36.34738922 39.85100937 -148.20898438 36.43750763 41.4790535 -149.15036011 34.93263245 42.10668564 -148.20898438
		 34.84250259 40.47865677 -147.26756287 39.24266815 42.32171631 -146.64926147 38.53529358 43.44953918 -147.98051453
		 37.82791138 44.57738495 -146.64924622 38.53529358 43.44957733 -145.31788635 42.13794708 44.79242706 -148.20896912
		 40.6330719 45.42006302 -149.15034485 40.72317886 47.048130035 -148.20899963 42.22807312 46.42046738 -147.26760864
		 43.16455078 46.091083527 -151.97451782 41.32933426 46.51156616 -151.97450256 41.74979401 48.34677887 -151.97454834
		 43.58501434 47.92633057 -151.9744873 41.54845428 45.7322998 -155.74009705 40.043571472 46.35995865 -154.79866028
		 40.13370514 47.98799133 -155.74003601 41.63859558 47.36032867 -156.68145752 38.063697815 44.20148087 -157.29978943
		 37.35631561 45.32932663 -155.96852112 36.64894104 46.45714951 -157.29978943 37.3563118 45.32932663 -158.63111877
		 34.57892609 42.67059708 -155.74008179 34.66906738 44.29866028 -154.79867554 33.16416931 44.92630386 -155.74006653
		 33.074050903 43.29827118 -156.68144226 32.96283722 42.3117981 -151.97453308 33.38330841 44.14702225 -151.97450256
		 31.54807854 44.56750488 -151.97453308 31.1276207 42.73225403 -151.97451782 33.98945618 43.61048508 -148.20898438
		 34.079589844 45.23852539 -149.15036011 32.57469177 45.86619186 -148.20896912 32.48457336 44.23812485 -147.26756287
		 36.88472748 46.081203461 -146.6492157 36.17735672 47.20904922 -147.98054504 35.4699707 48.33687973 -146.6492157
		 36.17736053 47.20905304 -145.31788635 39.78001404 48.5519104 -148.20896912 38.27513504 49.17957687 -149.15036011
		 38.36525345 50.80758667 -148.20896912 39.87014771 50.17995071 -147.26756287 40.80661774 49.85055161 -151.97451782
		 38.97141266 50.2710495 -151.97450256 39.39186096 52.10626984 -151.97453308 41.22708893 51.68580627 -151.97451782
		 39.19052887 49.49176407 -155.74006653 37.68563461 50.11944199 -154.79870605 37.77576447 51.74747467 -155.74009705
		 39.28064728 51.11979675 -156.68147278 35.70573425 47.9609108 -157.29981995 34.99837494 49.08877182 -155.96846008
		 34.29098511 50.21662521 -157.29978943 34.99835968 49.088764191 -158.63111877 32.22099686 46.43009949 -155.74003601
		 32.31111908 48.058132172 -154.79867554 30.80623055 48.68577576 -155.74003601 30.7161026 47.057735443 -156.68147278
		 30.60490036 46.071281433 -151.97450256 31.025373459 47.90649414 -151.97450256 29.19015503 48.3269577 -151.9744873
		 28.76968193 46.49176788 -151.97451782 31.6315155 47.36997223 -148.20896912 31.72166252 48.9980011 -149.15036011
		 30.21677208 49.6256485 -148.2089386 30.12664795 47.99764633 -147.26756287 33.41217804 51.61779785 -158.98304749
		 29.21380806 48.98463821 -156.93034363 27.474823 47.89389801 -151.9744873 29.21383476 48.98461151 -147.018722534
		 33.41217804 51.61783981 -144.96598816 37.6105423 54.25101089 -147.018722534 39.34954071 55.34172821 -151.97454834
		 37.61051941 54.25102997 -156.93034363 32.0080795288 53.85653687 -158.98304749 27.80970955 51.22335434 -156.93032837
		 26.070697784 50.13262939 -151.97447205 27.80971718 51.22335434 -147.018707275 32.0080718994 53.85652924 -144.96595764
		 36.20642853 56.48974228 -147.018722534 37.94542694 57.58043289 -151.97454834 36.20640564 56.48973465 -156.93034363
		 48.35235977 21.48641968 -148.62129211 54.033760071 25.049793243 -148.62130737 48.35234451 21.48641586 -155.32771301
		 54.033752441 25.049804688 -155.32772827 25.97711563 56.042945862 -148.043136597 26.52030945 58.4139061 -148.043136597
		 25.97711563 56.042964935 -155.90589905 26.52030754 58.41389084 -155.90592957 30.26727867 60.76399231 -148.043167114
		 32.63822937 60.22078705 -148.043151855 32.63822174 60.22079086 -155.90594482 30.26727104 60.76399231 -155.90596008
		 53.52400589 16.8137207 -148.62132263 50.90700531 17.41327286 -148.62130737 50.90699768 17.41327667 -155.32772827
		 53.52398682 16.8137207 -155.32771301 56.58840942 20.97666168 -148.62132263 55.98884583 18.35964584 -148.62130737
		 55.98884201 18.35968018 -155.32772827 56.58840942 20.9766655 -155.32771301 29.24106026 50.8389473 -118.19664001
		 35.90217209 55.016765594 -118.19664001 29.24106407 50.83895111 -126.059402466 35.90218353 55.016788483 -126.059463501
		 54.5196991 21.59993744 -124.059814453 52.88311768 20.57347107 -124.86003113 51.24654388 19.54700851 -124.059844971
		 50.56864166 19.12183762 -122.12796021 51.24653625 19.5470047 -120.19613647 52.88311768 20.57347488 -119.39598083
		 54.5196991 21.59993744 -120.19616699 55.19758987 22.025112152 -122.12796021 34.36645508 53.73213196 -124.059844971
		 32.72986984 52.70567322 -124.86003113 31.093305588 51.67921448 -124.059814453 30.41540337 51.25404739 -122.12797546;
	setAttr ".vt[664:829]" 31.093301773 51.67921448 -120.19616699 32.72987747 52.70565796 -119.39595032
		 34.36646271 53.73213959 -120.19613647 35.044345856 54.15730286 -122.12799072 40.69882584 40.000022888184 -129.13653564
		 36.50046539 37.36684418 -127.083786011 34.76145172 36.27613068 -122.12799072 36.50046539 37.36684799 -117.17221069
		 40.69882202 40.000026702881 -115.11946106 44.89717484 42.63324738 -117.17222595 46.63619232 43.72396088 -122.1280365
		 44.89717865 42.63323593 -127.08380127 39.26735687 42.28232193 -129.13656616 35.069015503 39.6491127 -127.083786011
		 33.33000183 38.55847168 -122.12799072 35.069007874 39.64915085 -117.17219543 39.26737213 42.28233719 -115.1194458
		 43.46572113 44.91553879 -117.17221069 45.2047348 46.0062217712 -122.12797546 43.4657135 44.91553497 -127.08380127
		 36.081459045 53.29201508 -126.16473389 38.74760056 54.96417999 -122.61053467 36.49902344 53.55387878 -122.42689514
		 37.94908142 53.77619934 -122.53953552 37.38195801 54.10766983 -117.78392029 35.67964172 53.039978027 -119.53088379
		 37.27384949 52.46389771 -118.70004272 33.11205292 52.096366882 -116.80270386 33.81943512 50.96850586 -118.13404846
		 34.52682495 49.84067917 -116.80271912 33.81944275 50.96851349 -115.47138977 42.45737839 44.2831192 -125.89353943
		 39.26735687 42.28232574 -128.78460693 39.2673645 42.28232193 -126.12193298 39.0068664551 42.69765854 -127.45326233
		 35.27983856 39.781353 -126.83488464 36.87484741 40.78174973 -124.95211792 35.52211761 41.16682053 -125.89355469
		 35.32077408 38.55231857 -122.12800598 35.74123383 40.3875351 -122.12800598 33.90601349 40.80799866 -122.12800598
		 33.48554993 38.97277451 -122.12799072 36.34738159 39.85100937 -118.36245728 36.43751144 41.4790535 -119.30383301
		 34.93263245 42.10668564 -118.36245728 34.84250259 40.47865677 -117.42103577 39.24266815 42.32171631 -116.80271912
		 38.53528976 43.44953918 -118.13400269 37.82791138 44.57738495 -116.80271912 38.53530121 43.44957733 -115.47135925
		 42.13794708 44.79242706 -118.36244202 40.6330719 45.42006302 -119.30380249 40.72318268 47.048130035 -118.36244202
		 42.22807312 46.42046738 -117.42106628 43.16455078 46.091114044 -122.12799072 41.32933044 46.51156616 -122.12797546
		 41.74979401 48.34677887 -122.12800598 43.58501434 47.92633057 -122.12796021 41.5484581 45.7322998 -125.89356995
		 40.043571472 46.35992813 -124.95213318 40.13370514 47.98799133 -125.89350891 41.63860321 47.36032867 -126.83493042
		 38.063697815 44.20148087 -127.45329285 37.35631943 45.32929611 -126.1219635 36.64894104 46.45714951 -127.45326233
		 37.35631943 45.32929611 -128.78456116 34.57894135 42.67062759 -125.89355469 34.66906357 44.29866028 -124.95214844
		 33.16417694 44.92630386 -125.89353943 33.074043274 43.29827118 -126.83491516 32.96283722 42.31182861 -122.12800598
		 33.38330841 44.14702225 -122.12797546 31.54807854 44.56750488 -122.12800598 31.12761688 42.73225403 -122.12799072
		 33.98945618 43.61045456 -118.36242676 34.079586029 45.23852539 -119.30383301 32.5746994 45.86619186 -118.36245728
		 32.48457336 44.23812485 -117.42103577 36.88473511 46.081203461 -116.8026886 36.17735291 47.20904922 -118.13401794
		 35.4699707 48.33687973 -116.8026886 36.17736435 47.20905304 -115.47135925 39.78001022 48.5519104 -118.36244202
		 38.27513123 49.17957687 -119.30384827 38.36525345 50.80758667 -118.36244202 39.87015533 50.17995071 -117.42103577
		 40.80661774 49.85055161 -122.12797546 38.97141266 50.2710495 -122.12797546 39.39186096 52.10626984 -122.12802124
		 41.22708893 51.68580627 -122.12799072 39.19052124 49.49176407 -125.89353943 37.68563461 50.11944199 -124.95219421
		 37.77576447 51.74744415 -125.89353943 39.28064728 51.11979675 -126.83494568 35.70574188 47.9609108 -127.45326233
		 34.99837112 49.08877182 -126.12193298 34.29099274 50.21662521 -127.45326233 34.99837112 49.088794708 -128.78462219
		 32.22099304 46.43006897 -125.89350891 32.31111908 48.058132172 -124.95214844 30.80623055 48.68577576 -125.89350891
		 30.71611404 47.057735443 -126.83494568 30.60490417 46.071281433 -122.12796021 31.025369644 47.90649414 -122.12797546
		 29.19015121 48.3269577 -122.12797546 28.76967812 46.49173737 -122.12796021 31.63151932 47.36997223 -118.36245728
		 31.72165871 48.9980011 -119.30383301 30.21676826 49.6256485 -118.3624115 30.12664413 47.99761581 -117.42100525
		 33.41218567 51.61782837 -129.13652039 29.2138195 48.98463821 -127.083816528 27.47481918 47.89389801 -122.12796021
		 29.21383095 48.98461151 -117.17221069 33.41217041 51.61783981 -115.11946106 37.61053467 54.25101089 -117.17219543
		 39.34955597 55.34172821 -122.12802124 37.61052704 54.25099945 -127.083786011 32.0080718994 53.85656738 -129.1365509
		 27.80971718 51.22335434 -127.083831787 26.070705414 50.13262939 -122.12794495 27.80971336 51.22332382 -117.17221069
		 32.0080718994 53.85652924 -115.11941528 36.2064209 56.48971176 -117.17214966 37.94543457 57.58043289 -122.12802124
		 36.20640564 56.48973465 -127.083786011 48.35235596 21.48641968 -118.77476501 54.033760071 25.049793243 -118.77479553
		 48.3523407 21.48638535 -125.4811554 54.033744812 25.049804688 -125.48120117 25.97711945 56.042945862 -118.19664001
		 26.52030563 58.4139061 -118.1966095 25.97712326 56.042964935 -126.059402466 26.52030373 58.41392136 -126.059432983
		 30.26727104 60.76399231 -118.19664001 32.63822937 60.22078705 -118.19662476 32.63822174 60.22079086 -126.059417725
		 30.26727104 60.76399231 -126.059432983 53.52400208 16.8137207 -118.77479553 50.90700531 17.41327286 -118.77476501
		 50.90700531 17.413311 -125.48120117 53.52399063 16.8137207 -125.48117065 56.58841705 20.97666168 -118.77478027
		 55.98884583 18.35964584 -118.77478027 55.98884583 18.35968018 -125.48120117 56.58840942 20.9766655 -125.48117065
		 29.30729485 51.7395134 139.93135071 35.96840668 55.91734314 139.93136597 29.30729866 51.7395134 132.068572998
		 35.96841431 55.91733932 132.068572998 54.58592987 22.50050354 134.06817627 52.94935989 21.47405243 133.26795959
		 51.31277466 20.44758606 134.068161011 50.63487244 20.022403717 136.000015258789 51.31277084 20.44758224 137.93185425
		 52.94934845 21.47405243 138.73200989 54.5859375 22.50051117 137.93182373 55.26382446 22.92567825 136.000015258789
		 34.43270111 54.63268661 134.068161011 32.79611206 53.6062355 133.26797485 31.15954018 52.57977676 134.06817627
		 30.48163795 52.15460587 136 31.15953636 52.57977676 137.93183899 32.79611206 53.60624695 138.73204041
		 34.43269348 54.63269424 137.93183899 35.11058044 55.057857513 136;
	setAttr ".vt[830:971]" 40.76506805 40.90058899 128.99145508 36.5667038 38.26739883 131.044189453
		 34.8276825 37.17669296 136.000015258789 36.56669617 38.26739502 140.95579529 40.7650528 40.9005928 143.0085449219
		 44.96340942 43.5337944 140.95579529 46.7024231 44.62450027 135.99998474 44.96341705 43.53378296 131.044189453
		 39.33360291 43.18290329 128.99142456 35.13524628 40.54970169 131.044189453 33.39622879 39.45899963 136.000015258789
		 35.13525391 40.54971313 140.95579529 39.33360291 43.18291092 143.0085601807 43.5319519 45.81609344 140.95576477
		 45.27097321 46.90679932 136 43.53194427 45.816082 131.044189453 36.14769745 54.19256592 131.96324158
		 38.81383896 55.86475754 135.51747131 36.56526184 54.45445633 135.70111084 38.015319824 54.67677689 135.5884552
		 37.4481926 55.0082321167 140.34405518 35.74588013 53.9405365 138.59709167 37.34009171 53.36444092 139.42796326
		 33.17829132 52.9969368 141.32530212 33.88566589 51.86909103 139.99395752 34.59305954 50.74124146 141.32528687
		 33.88568115 51.86909866 142.65661621 42.52362061 45.18367767 132.23443604 39.33360291 43.18290329 129.34339905
		 39.33359528 43.18289566 132.0060424805 39.0730896 43.59822083 130.67471313 35.34607697 40.68193054 131.29309082
		 36.94107437 41.68231583 133.17585754 35.58834839 42.067394257 132.23443604 35.3870163 39.4528923 136
		 35.80747223 41.28811264 136 33.97225189 41.70858383 136 33.55178833 39.87334824 136
		 36.41362762 40.7515831 139.76554871 36.50374985 42.3796196 138.82415771 34.99887085 43.0072631836 139.76554871
		 34.90872955 41.37922668 140.7069397 39.30891418 43.22228622 141.32528687 38.60152435 44.35012054 139.99397278
		 37.89415741 45.47796631 141.32528687 38.60153961 44.3501358 142.65661621 42.20417786 45.69300461 139.76554871
		 40.69930267 46.32064438 138.82417297 40.78942108 47.94868088 139.76554871 42.29431152 47.32104111 140.7069397
		 43.23078918 46.99168015 136 41.39556122 47.41213226 136.000015258789 41.81603241 49.24735641 136
		 43.65124893 48.82689667 136.000015258789 41.61468124 46.63287354 132.23443604 40.10980606 47.26051712 133.17584229
		 40.19994736 48.8885498 132.23446655 41.70484161 48.26091385 131.29307556 38.12992096 45.10203171 130.67471313
		 37.42255783 46.22986984 132.0060119629 36.71517181 47.35770416 130.67471313 37.42255402 46.22986603 129.34339905
		 34.64517593 43.57118607 132.23443604 34.73529816 45.19921112 133.17584229 33.23041153 45.82687378 132.23443604
		 33.14028931 44.19882965 131.29309082 33.029067993 43.21236038 136 33.44953918 45.047588348 136
		 31.61432076 45.46805573 136 31.19385338 43.63282776 136 34.055698395 44.51105118 139.76554871
		 34.14582062 46.13909149 138.82415771 32.64094162 46.76673889 139.76554871 32.55080414 45.13869476 140.7069397
		 36.95097351 46.98177338 141.32528687 36.24358749 48.10960388 139.99397278 35.53620911 49.23745346 141.32528687
		 36.24358749 48.1096077 142.65661621 39.84624481 49.45247269 139.76554871 38.34136581 50.080120087 138.82415771
		 38.43148804 51.7081604 139.76554871 39.93639374 51.080524445 140.7069397 40.87286377 50.75114822 136
		 39.037643433 51.17162323 136 39.45809555 53.0068435669 135.99998474 41.29333115 52.58637619 135.99998474
		 39.25675201 50.39234543 132.23443604 37.75186539 51.020000458 133.17581177 37.84199524 52.64802551 132.23443604
		 39.34688568 52.020366669 131.2930603 35.7719841 48.86149216 130.67471313 35.064605713 49.98934174 132.0060424805
		 34.35723114 51.11719131 130.67471313 35.064605713 49.98933792 129.34338379 32.28722763 47.33065033 132.23446655
		 32.37734985 48.95868683 133.17584229 30.87246513 49.58633804 132.23446655 30.78234673 47.95830154 131.2930603
		 30.67114449 46.97185516 136.000015258789 31.091604233 48.80706406 136.000015258789
		 29.25638771 49.22752762 136.000030517578 28.83592796 47.39231873 136.000015258789
		 31.69776344 48.27053452 139.76554871 31.78788948 49.89855957 138.82417297 30.28300285 50.5262146 139.76556396
		 30.19288063 48.89818192 140.70697021 33.47842407 52.51838684 128.99147034 29.28004837 49.88518524 131.044189453
		 27.54105186 48.79447937 136.000015258789 29.28006744 49.88518143 140.95579529 33.47840881 52.51839828 143.0085449219
		 37.67677307 55.15158081 140.95579529 39.41578674 56.24229431 135.99996948 37.67676544 55.15158081 131.044189453
		 32.074306488 54.75710678 128.99145508 27.87595177 52.123909 131.044189453 26.13693619 51.033195496 136.000030517578
		 27.87594414 52.12390518 140.95579529 32.074302673 54.75709534 143.0085601807 36.2726593 57.39029312 140.95581055
		 38.011672974 58.48101044 136 36.27264023 57.39027786 131.044189453 48.41859436 22.38697815 139.35321045
		 54.099990845 25.9503479 139.35321045 48.41858673 22.38698196 132.64680481 54.099983215 25.95035934 132.64680481
		 26.043346405 56.94352722 139.93136597 26.58654022 59.31446838 139.93136597 26.043357849 56.94352341 132.068603516
		 26.58653831 59.31445694 132.068572998 30.33350563 61.66456604 139.93136597 32.70446014 61.12135696 139.93136597
		 32.70446014 61.12136841 132.068572998 30.33350945 61.66456985 132.068572998 53.59023285 17.7142868 139.35321045
		 50.97325134 18.3138504 139.35321045 50.97324371 18.31385803 132.64680481 53.59023285 17.71429443 132.64680481
		 56.6546402 21.87722397 139.35319519 56.055084229 19.26023865 139.35321045 56.055084229 19.26024246 132.64680481
		 56.6546402 21.87722397 132.64680481;
	setAttr -s 1836 ".ed";
	setAttr ".ed[0:165]"  0 1 0 2 3 0 0 146 0 1 151 0 2 0 0 3 1 0 4 5 0 5 6 0
		 6 7 0 7 8 0 8 9 0 9 10 0 10 11 0 11 4 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 12 0 4 12 1 5 13 1 6 14 1 7 15 1 8 16 1 9 17 1 10 18 1 11 19 1 20 21 0
		 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 20 0 28 29 0 29 30 0 30 31 0 31 32 0
		 32 33 0 33 34 0 34 35 0 35 28 0 20 28 1 21 29 1 22 30 1 23 31 1 24 32 1 25 33 1 26 34 1
		 27 35 1 30 34 1 29 35 1 31 33 1 23 25 1 22 26 1 21 27 1 37 38 1 38 39 1 39 37 1 40 41 1
		 41 42 1 42 40 1 43 44 1 44 45 1 45 46 1 46 43 1 36 37 0 36 38 0 36 39 1 37 40 0 38 41 0
		 39 42 0 40 43 1 41 44 0 42 45 0 40 46 0 41 43 1 48 49 1 49 50 1 50 48 1 51 52 1 52 53 1
		 53 51 1 54 55 1 55 56 1 56 57 1 57 54 1 58 59 1 59 60 1 60 61 1 61 58 1 62 63 1 63 64 1
		 64 65 1 65 62 1 66 67 1 67 68 1 68 69 1 69 66 1 70 71 1 71 72 1 72 73 1 73 70 1 74 75 1
		 75 76 1 76 77 1 77 74 1 78 79 1 79 80 1 80 81 1 81 78 1 82 83 1 83 84 1 84 85 1 85 82 1
		 86 87 1 87 88 1 88 89 1 89 86 1 90 91 1 91 92 1 92 93 1 93 90 1 94 95 1 95 96 1 96 97 1
		 97 94 1 98 99 1 99 100 1 100 101 1 101 98 1 102 103 1 103 104 1 104 105 1 105 102 1
		 106 107 1 107 108 1 108 109 1 109 106 1 110 111 1 111 112 1 112 113 1 113 110 1 114 115 1
		 115 116 1 116 117 1 117 114 1 118 119 1 119 120 1 120 121 1 121 118 1 122 123 1 123 124 1
		 124 125 1 125 122 1 47 48 0 47 49 0 47 50 1 48 51 0 49 52 0 50 53 0 51 54 1;
	setAttr ".ed[166:331]" 52 55 0 53 56 0 51 57 0 54 58 0 55 59 0 56 60 0 57 61 0
		 58 62 0 59 63 0 60 64 0 61 65 0 62 66 0 63 67 0 64 68 0 65 69 0 66 70 0 67 71 0 68 72 0
		 69 73 0 70 74 0 71 75 0 72 76 0 73 77 0 74 78 0 75 79 0 76 80 0 77 81 0 78 82 0 79 83 0
		 80 84 0 81 85 0 82 86 0 83 87 0 84 88 0 85 89 0 86 90 0 87 91 0 88 92 0 89 93 0 90 94 0
		 91 95 0 92 96 0 93 97 0 94 98 0 95 99 0 96 100 0 97 101 0 98 102 0 99 103 0 100 104 0
		 101 105 0 102 106 0 103 107 0 104 108 0 105 109 0 106 110 0 107 111 0 108 112 0 109 113 0
		 110 114 0 111 115 0 112 116 0 113 117 0 114 118 0 115 119 0 116 120 0 117 121 0 118 122 0
		 119 123 0 120 124 0 121 125 0 122 45 0 123 44 0 124 43 0 125 46 0 52 54 1 126 127 0
		 127 128 0 128 129 0 129 130 0 130 131 0 131 132 0 132 133 0 133 126 0 134 135 0 135 136 0
		 136 137 0 137 138 0 138 139 0 139 140 0 140 141 0 141 134 0 126 134 1 127 135 1 128 136 1
		 129 137 1 130 138 1 131 139 1 132 140 1 133 141 1 128 132 1 136 140 1 129 131 1 137 139 1
		 127 133 1 135 141 1 142 143 0 144 145 0 142 144 0 143 145 0 144 156 0 145 161 0 147 150 0
		 147 146 0 148 2 0 149 153 0 149 148 0 151 150 0 152 3 0 153 152 0 146 148 1 149 147 1
		 150 153 1 152 151 1 154 159 0 155 142 0 154 155 0 157 160 0 157 156 0 158 143 0 159 158 0
		 161 160 0 154 157 1 156 155 1 158 161 1 160 159 1 146 151 0 155 158 0 156 161 0 148 152 0
		 162 163 0 164 165 0 162 308 0 163 313 0 164 162 0 165 163 0 166 167 0 167 168 0 168 169 0
		 169 170 0 170 171 0 171 172 0 172 173 0 173 166 0 174 175 0 175 176 0 176 177 0 177 178 0
		 178 179 0 179 180 0 180 181 0 181 174 0 166 174 1 167 175 1 168 176 1 169 177 1;
	setAttr ".ed[332:497]" 170 178 1 171 179 1 172 180 1 173 181 1 182 183 0 183 184 0
		 184 185 0 185 186 0 186 187 0 187 188 0 188 189 0 189 182 0 190 191 0 191 192 0 192 193 0
		 193 194 0 194 195 0 195 196 0 196 197 0 197 190 0 182 190 1 183 191 1 184 192 1 185 193 1
		 186 194 1 187 195 1 188 196 1 189 197 1 192 196 1 191 197 1 193 195 1 185 187 1 184 188 1
		 183 189 1 199 200 1 200 201 1 201 199 1 202 203 1 203 204 1 204 202 1 205 206 1 206 207 1
		 207 208 1 208 205 1 198 199 0 198 200 0 198 201 1 199 202 0 200 203 0 201 204 0 202 205 1
		 203 206 0 204 207 0 202 208 0 203 205 1 210 211 1 211 212 1 212 210 1 213 214 1 214 215 1
		 215 213 1 216 217 1 217 218 1 218 219 1 219 216 1 220 221 1 221 222 1 222 223 1 223 220 1
		 224 225 1 225 226 1 226 227 1 227 224 1 228 229 1 229 230 1 230 231 1 231 228 1 232 233 1
		 233 234 1 234 235 1 235 232 1 236 237 1 237 238 1 238 239 1 239 236 1 240 241 1 241 242 1
		 242 243 1 243 240 1 244 245 1 245 246 1 246 247 1 247 244 1 248 249 1 249 250 1 250 251 1
		 251 248 1 252 253 1 253 254 1 254 255 1 255 252 1 256 257 1 257 258 1 258 259 1 259 256 1
		 260 261 1 261 262 1 262 263 1 263 260 1 264 265 1 265 266 1 266 267 1 267 264 1 268 269 1
		 269 270 1 270 271 1 271 268 1 272 273 1 273 274 1 274 275 1 275 272 1 276 277 1 277 278 1
		 278 279 1 279 276 1 280 281 1 281 282 1 282 283 1 283 280 1 284 285 1 285 286 1 286 287 1
		 287 284 1 209 210 0 209 211 0 209 212 1 210 213 0 211 214 0 212 215 0 213 216 1 214 217 0
		 215 218 0 213 219 0 216 220 0 217 221 0 218 222 0 219 223 0 220 224 0 221 225 0 222 226 0
		 223 227 0 224 228 0 225 229 0 226 230 0 227 231 0 228 232 0 229 233 0 230 234 0 231 235 0
		 232 236 0 233 237 0 234 238 0 235 239 0 236 240 0 237 241 0 238 242 0;
	setAttr ".ed[498:663]" 239 243 0 240 244 0 241 245 0 242 246 0 243 247 0 244 248 0
		 245 249 0 246 250 0 247 251 0 248 252 0 249 253 0 250 254 0 251 255 0 252 256 0 253 257 0
		 254 258 0 255 259 0 256 260 0 257 261 0 258 262 0 259 263 0 260 264 0 261 265 0 262 266 0
		 263 267 0 264 268 0 265 269 0 266 270 0 267 271 0 268 272 0 269 273 0 270 274 0 271 275 0
		 272 276 0 273 277 0 274 278 0 275 279 0 276 280 0 277 281 0 278 282 0 279 283 0 280 284 0
		 281 285 0 282 286 0 283 287 0 284 207 0 285 206 0 286 205 0 287 208 0 214 216 1 288 289 0
		 289 290 0 290 291 0 291 292 0 292 293 0 293 294 0 294 295 0 295 288 0 296 297 0 297 298 0
		 298 299 0 299 300 0 300 301 0 301 302 0 302 303 0 303 296 0 288 296 1 289 297 1 290 298 1
		 291 299 1 292 300 1 293 301 1 294 302 1 295 303 1 290 294 1 298 302 1 291 293 1 299 301 1
		 289 295 1 297 303 1 304 305 0 306 307 0 304 306 0 305 307 0 306 318 0 307 323 0 309 312 0
		 309 308 0 310 164 0 311 315 0 311 310 0 313 312 0 314 165 0 315 314 0 308 310 1 311 309 1
		 312 315 1 314 313 1 316 321 0 317 304 0 316 317 0 319 322 0 319 318 0 320 305 0 321 320 0
		 323 322 0 316 319 1 318 317 1 320 323 1 322 321 1 308 313 0 317 320 0 318 323 0 310 314 0
		 324 325 0 326 327 0 324 470 0 325 475 0 326 324 0 327 325 0 328 329 0 329 330 0 330 331 0
		 331 332 0 332 333 0 333 334 0 334 335 0 335 328 0 336 337 0 337 338 0 338 339 0 339 340 0
		 340 341 0 341 342 0 342 343 0 343 336 0 328 336 1 329 337 1 330 338 1 331 339 1 332 340 1
		 333 341 1 334 342 1 335 343 1 344 345 0 345 346 0 346 347 0 347 348 0 348 349 0 349 350 0
		 350 351 0 351 344 0 352 353 0 353 354 0 354 355 0 355 356 0 356 357 0 357 358 0 358 359 0
		 359 352 0 344 352 1 345 353 1 346 354 1 347 355 1 348 356 1 349 357 1;
	setAttr ".ed[664:829]" 350 358 1 351 359 1 354 358 1 353 359 1 355 357 1 347 349 1
		 346 350 1 345 351 1 361 362 1 362 363 1 363 361 1 364 365 1 365 366 1 366 364 1 367 368 1
		 368 369 1 369 370 1 370 367 1 360 361 0 360 362 0 360 363 1 361 364 0 362 365 0 363 366 0
		 364 367 1 365 368 0 366 369 0 364 370 0 365 367 1 372 373 1 373 374 1 374 372 1 375 376 1
		 376 377 1 377 375 1 378 379 1 379 380 1 380 381 1 381 378 1 382 383 1 383 384 1 384 385 1
		 385 382 1 386 387 1 387 388 1 388 389 1 389 386 1 390 391 1 391 392 1 392 393 1 393 390 1
		 394 395 1 395 396 1 396 397 1 397 394 1 398 399 1 399 400 1 400 401 1 401 398 1 402 403 1
		 403 404 1 404 405 1 405 402 1 406 407 1 407 408 1 408 409 1 409 406 1 410 411 1 411 412 1
		 412 413 1 413 410 1 414 415 1 415 416 1 416 417 1 417 414 1 418 419 1 419 420 1 420 421 1
		 421 418 1 422 423 1 423 424 1 424 425 1 425 422 1 426 427 1 427 428 1 428 429 1 429 426 1
		 430 431 1 431 432 1 432 433 1 433 430 1 434 435 1 435 436 1 436 437 1 437 434 1 438 439 1
		 439 440 1 440 441 1 441 438 1 442 443 1 443 444 1 444 445 1 445 442 1 446 447 1 447 448 1
		 448 449 1 449 446 1 371 372 0 371 373 0 371 374 1 372 375 0 373 376 0 374 377 0 375 378 1
		 376 379 0 377 380 0 375 381 0 378 382 0 379 383 0 380 384 0 381 385 0 382 386 0 383 387 0
		 384 388 0 385 389 0 386 390 0 387 391 0 388 392 0 389 393 0 390 394 0 391 395 0 392 396 0
		 393 397 0 394 398 0 395 399 0 396 400 0 397 401 0 398 402 0 399 403 0 400 404 0 401 405 0
		 402 406 0 403 407 0 404 408 0 405 409 0 406 410 0 407 411 0 408 412 0 409 413 0 410 414 0
		 411 415 0 412 416 0 413 417 0 414 418 0 415 419 0 416 420 0 417 421 0 418 422 0 419 423 0
		 420 424 0 421 425 0 422 426 0 423 427 0 424 428 0 425 429 0 426 430 0;
	setAttr ".ed[830:995]" 427 431 0 428 432 0 429 433 0 430 434 0 431 435 0 432 436 0
		 433 437 0 434 438 0 435 439 0 436 440 0 437 441 0 438 442 0 439 443 0 440 444 0 441 445 0
		 442 446 0 443 447 0 444 448 0 445 449 0 446 369 0 447 368 0 448 367 0 449 370 0 376 378 1
		 450 451 0 451 452 0 452 453 0 453 454 0 454 455 0 455 456 0 456 457 0 457 450 0 458 459 0
		 459 460 0 460 461 0 461 462 0 462 463 0 463 464 0 464 465 0 465 458 0 450 458 1 451 459 1
		 452 460 1 453 461 1 454 462 1 455 463 1 456 464 1 457 465 1 452 456 1 460 464 1 453 455 1
		 461 463 1 451 457 1 459 465 1 466 467 0 468 469 0 466 468 0 467 469 0 468 480 0 469 485 0
		 471 474 0 471 470 0 472 326 0 473 477 0 473 472 0 475 474 0 476 327 0 477 476 0 470 472 1
		 473 471 1 474 477 1 476 475 1 478 483 0 479 466 0 478 479 0 481 484 0 481 480 0 482 467 0
		 483 482 0 485 484 0 478 481 1 480 479 1 482 485 1 484 483 1 470 475 0 479 482 0 480 485 0
		 472 476 0 486 487 0 488 489 0 486 632 0 487 637 0 488 486 0 489 487 0 490 491 0 491 492 0
		 492 493 0 493 494 0 494 495 0 495 496 0 496 497 0 497 490 0 498 499 0 499 500 0 500 501 0
		 501 502 0 502 503 0 503 504 0 504 505 0 505 498 0 490 498 1 491 499 1 492 500 1 493 501 1
		 494 502 1 495 503 1 496 504 1 497 505 1 506 507 0 507 508 0 508 509 0 509 510 0 510 511 0
		 511 512 0 512 513 0 513 506 0 514 515 0 515 516 0 516 517 0 517 518 0 518 519 0 519 520 0
		 520 521 0 521 514 0 506 514 1 507 515 1 508 516 1 509 517 1 510 518 1 511 519 1 512 520 1
		 513 521 1 516 520 1 515 521 1 517 519 1 509 511 1 508 512 1 507 513 1 523 524 1 524 525 1
		 525 523 1 526 527 1 527 528 1 528 526 1 529 530 1 530 531 1 531 532 1 532 529 1 522 523 0
		 522 524 0 522 525 1 523 526 0 524 527 0 525 528 0 526 529 1 527 530 0;
	setAttr ".ed[996:1161]" 528 531 0 526 532 0 527 529 1 534 535 1 535 536 1 536 534 1
		 537 538 1 538 539 1 539 537 1 540 541 1 541 542 1 542 543 1 543 540 1 544 545 1 545 546 1
		 546 547 1 547 544 1 548 549 1 549 550 1 550 551 1 551 548 1 552 553 1 553 554 1 554 555 1
		 555 552 1 556 557 1 557 558 1 558 559 1 559 556 1 560 561 1 561 562 1 562 563 1 563 560 1
		 564 565 1 565 566 1 566 567 1 567 564 1 568 569 1 569 570 1 570 571 1 571 568 1 572 573 1
		 573 574 1 574 575 1 575 572 1 576 577 1 577 578 1 578 579 1 579 576 1 580 581 1 581 582 1
		 582 583 1 583 580 1 584 585 1 585 586 1 586 587 1 587 584 1 588 589 1 589 590 1 590 591 1
		 591 588 1 592 593 1 593 594 1 594 595 1 595 592 1 596 597 1 597 598 1 598 599 1 599 596 1
		 600 601 1 601 602 1 602 603 1 603 600 1 604 605 1 605 606 1 606 607 1 607 604 1 608 609 1
		 609 610 1 610 611 1 611 608 1 533 534 0 533 535 0 533 536 1 534 537 0 535 538 0 536 539 0
		 537 540 1 538 541 0 539 542 0 537 543 0 540 544 0 541 545 0 542 546 0 543 547 0 544 548 0
		 545 549 0 546 550 0 547 551 0 548 552 0 549 553 0 550 554 0 551 555 0 552 556 0 553 557 0
		 554 558 0 555 559 0 556 560 0 557 561 0 558 562 0 559 563 0 560 564 0 561 565 0 562 566 0
		 563 567 0 564 568 0 565 569 0 566 570 0 567 571 0 568 572 0 569 573 0 570 574 0 571 575 0
		 572 576 0 573 577 0 574 578 0 575 579 0 576 580 0 577 581 0 578 582 0 579 583 0 580 584 0
		 581 585 0 582 586 0 583 587 0 584 588 0 585 589 0 586 590 0 587 591 0 588 592 0 589 593 0
		 590 594 0 591 595 0 592 596 0 593 597 0 594 598 0 595 599 0 596 600 0 597 601 0 598 602 0
		 599 603 0 600 604 0 601 605 0 602 606 0 603 607 0 604 608 0 605 609 0 606 610 0 607 611 0
		 608 531 0 609 530 0 610 529 0 611 532 0 538 540 1 612 613 0 613 614 0;
	setAttr ".ed[1162:1327]" 614 615 0 615 616 0 616 617 0 617 618 0 618 619 0 619 612 0
		 620 621 0 621 622 0 622 623 0 623 624 0 624 625 0 625 626 0 626 627 0 627 620 0 612 620 1
		 613 621 1 614 622 1 615 623 1 616 624 1 617 625 1 618 626 1 619 627 1 614 618 1 622 626 1
		 615 617 1 623 625 1 613 619 1 621 627 1 628 629 0 630 631 0 628 630 0 629 631 0 630 642 0
		 631 647 0 633 636 0 633 632 0 634 488 0 635 639 0 635 634 0 637 636 0 638 489 0 639 638 0
		 632 634 1 635 633 1 636 639 1 638 637 1 640 645 0 641 628 0 640 641 0 643 646 0 643 642 0
		 644 629 0 645 644 0 647 646 0 640 643 1 642 641 1 644 647 1 646 645 1 632 637 0 641 644 0
		 642 647 0 634 638 0 648 649 0 650 651 0 648 794 0 649 799 0 650 648 0 651 649 0 652 653 0
		 653 654 0 654 655 0 655 656 0 656 657 0 657 658 0 658 659 0 659 652 0 660 661 0 661 662 0
		 662 663 0 663 664 0 664 665 0 665 666 0 666 667 0 667 660 0 652 660 1 653 661 1 654 662 1
		 655 663 1 656 664 1 657 665 1 658 666 1 659 667 1 668 669 0 669 670 0 670 671 0 671 672 0
		 672 673 0 673 674 0 674 675 0 675 668 0 676 677 0 677 678 0 678 679 0 679 680 0 680 681 0
		 681 682 0 682 683 0 683 676 0 668 676 1 669 677 1 670 678 1 671 679 1 672 680 1 673 681 1
		 674 682 1 675 683 1 678 682 1 677 683 1 679 681 1 671 673 1 670 674 1 669 675 1 685 686 1
		 686 687 1 687 685 1 688 689 1 689 690 1 690 688 1 691 692 1 692 693 1 693 694 1 694 691 1
		 684 685 0 684 686 0 684 687 1 685 688 0 686 689 0 687 690 0 688 691 1 689 692 0 690 693 0
		 688 694 0 689 691 1 696 697 1 697 698 1 698 696 1 699 700 1 700 701 1 701 699 1 702 703 1
		 703 704 1 704 705 1 705 702 1 706 707 1 707 708 1 708 709 1 709 706 1 710 711 1 711 712 1
		 712 713 1 713 710 1 714 715 1 715 716 1 716 717 1 717 714 1 718 719 1;
	setAttr ".ed[1328:1493]" 719 720 1 720 721 1 721 718 1 722 723 1 723 724 1 724 725 1
		 725 722 1 726 727 1 727 728 1 728 729 1 729 726 1 730 731 1 731 732 1 732 733 1 733 730 1
		 734 735 1 735 736 1 736 737 1 737 734 1 738 739 1 739 740 1 740 741 1 741 738 1 742 743 1
		 743 744 1 744 745 1 745 742 1 746 747 1 747 748 1 748 749 1 749 746 1 750 751 1 751 752 1
		 752 753 1 753 750 1 754 755 1 755 756 1 756 757 1 757 754 1 758 759 1 759 760 1 760 761 1
		 761 758 1 762 763 1 763 764 1 764 765 1 765 762 1 766 767 1 767 768 1 768 769 1 769 766 1
		 770 771 1 771 772 1 772 773 1 773 770 1 695 696 0 695 697 0 695 698 1 696 699 0 697 700 0
		 698 701 0 699 702 1 700 703 0 701 704 0 699 705 0 702 706 0 703 707 0 704 708 0 705 709 0
		 706 710 0 707 711 0 708 712 0 709 713 0 710 714 0 711 715 0 712 716 0 713 717 0 714 718 0
		 715 719 0 716 720 0 717 721 0 718 722 0 719 723 0 720 724 0 721 725 0 722 726 0 723 727 0
		 724 728 0 725 729 0 726 730 0 727 731 0 728 732 0 729 733 0 730 734 0 731 735 0 732 736 0
		 733 737 0 734 738 0 735 739 0 736 740 0 737 741 0 738 742 0 739 743 0 740 744 0 741 745 0
		 742 746 0 743 747 0 744 748 0 745 749 0 746 750 0 747 751 0 748 752 0 749 753 0 750 754 0
		 751 755 0 752 756 0 753 757 0 754 758 0 755 759 0 756 760 0 757 761 0 758 762 0 759 763 0
		 760 764 0 761 765 0 762 766 0 763 767 0 764 768 0 765 769 0 766 770 0 767 771 0 768 772 0
		 769 773 0 770 693 0 771 692 0 772 691 0 773 694 0 700 702 1 774 775 0 775 776 0 776 777 0
		 777 778 0 778 779 0 779 780 0 780 781 0 781 774 0 782 783 0 783 784 0 784 785 0 785 786 0
		 786 787 0 787 788 0 788 789 0 789 782 0 774 782 1 775 783 1 776 784 1 777 785 1 778 786 1
		 779 787 1 780 788 1 781 789 1 776 780 1 784 788 1 777 779 1 785 787 1;
	setAttr ".ed[1494:1659]" 775 781 1 783 789 1 790 791 0 792 793 0 790 792 0 791 793 0
		 792 804 0 793 809 0 795 798 0 795 794 0 796 650 0 797 801 0 797 796 0 799 798 0 800 651 0
		 801 800 0 794 796 1 797 795 1 798 801 1 800 799 1 802 807 0 803 790 0 802 803 0 805 808 0
		 805 804 0 806 791 0 807 806 0 809 808 0 802 805 1 804 803 1 806 809 1 808 807 1 794 799 0
		 803 806 0 804 809 0 796 800 0 810 811 0 812 813 0 810 956 0 811 961 0 812 810 0 813 811 0
		 814 815 0 815 816 0 816 817 0 817 818 0 818 819 0 819 820 0 820 821 0 821 814 0 822 823 0
		 823 824 0 824 825 0 825 826 0 826 827 0 827 828 0 828 829 0 829 822 0 814 822 1 815 823 1
		 816 824 1 817 825 1 818 826 1 819 827 1 820 828 1 821 829 1 830 831 0 831 832 0 832 833 0
		 833 834 0 834 835 0 835 836 0 836 837 0 837 830 0 838 839 0 839 840 0 840 841 0 841 842 0
		 842 843 0 843 844 0 844 845 0 845 838 0 830 838 1 831 839 1 832 840 1 833 841 1 834 842 1
		 835 843 1 836 844 1 837 845 1 840 844 1 839 845 1 841 843 1 833 835 1 832 836 1 831 837 1
		 847 848 1 848 849 1 849 847 1 850 851 1 851 852 1 852 850 1 853 854 1 854 855 1 855 856 1
		 856 853 1 846 847 0 846 848 0 846 849 1 847 850 0 848 851 0 849 852 0 850 853 1 851 854 0
		 852 855 0 850 856 0 851 853 1 858 859 1 859 860 1 860 858 1 861 862 1 862 863 1 863 861 1
		 864 865 1 865 866 1 866 867 1 867 864 1 868 869 1 869 870 1 870 871 1 871 868 1 872 873 1
		 873 874 1 874 875 1 875 872 1 876 877 1 877 878 1 878 879 1 879 876 1 880 881 1 881 882 1
		 882 883 1 883 880 1 884 885 1 885 886 1 886 887 1 887 884 1 888 889 1 889 890 1 890 891 1
		 891 888 1 892 893 1 893 894 1 894 895 1 895 892 1 896 897 1 897 898 1 898 899 1 899 896 1
		 900 901 1 901 902 1 902 903 1 903 900 1 904 905 1 905 906 1 906 907 1;
	setAttr ".ed[1660:1825]" 907 904 1 908 909 1 909 910 1 910 911 1 911 908 1 912 913 1
		 913 914 1 914 915 1 915 912 1 916 917 1 917 918 1 918 919 1 919 916 1 920 921 1 921 922 1
		 922 923 1 923 920 1 924 925 1 925 926 1 926 927 1 927 924 1 928 929 1 929 930 1 930 931 1
		 931 928 1 932 933 1 933 934 1 934 935 1 935 932 1 857 858 0 857 859 0 857 860 1 858 861 0
		 859 862 0 860 863 0 861 864 1 862 865 0 863 866 0 861 867 0 864 868 0 865 869 0 866 870 0
		 867 871 0 868 872 0 869 873 0 870 874 0 871 875 0 872 876 0 873 877 0 874 878 0 875 879 0
		 876 880 0 877 881 0 878 882 0 879 883 0 880 884 0 881 885 0 882 886 0 883 887 0 884 888 0
		 885 889 0 886 890 0 887 891 0 888 892 0 889 893 0 890 894 0 891 895 0 892 896 0 893 897 0
		 894 898 0 895 899 0 896 900 0 897 901 0 898 902 0 899 903 0 900 904 0 901 905 0 902 906 0
		 903 907 0 904 908 0 905 909 0 906 910 0 907 911 0 908 912 0 909 913 0 910 914 0 911 915 0
		 912 916 0 913 917 0 914 918 0 915 919 0 916 920 0 917 921 0 918 922 0 919 923 0 920 924 0
		 921 925 0 922 926 0 923 927 0 924 928 0 925 929 0 926 930 0 927 931 0 928 932 0 929 933 0
		 930 934 0 931 935 0 932 855 0 933 854 0 934 853 0 935 856 0 862 864 1 936 937 0 937 938 0
		 938 939 0 939 940 0 940 941 0 941 942 0 942 943 0 943 936 0 944 945 0 945 946 0 946 947 0
		 947 948 0 948 949 0 949 950 0 950 951 0 951 944 0 936 944 1 937 945 1 938 946 1 939 947 1
		 940 948 1 941 949 1 942 950 1 943 951 1 938 942 1 946 950 1 939 941 1 947 949 1 937 943 1
		 945 951 1 952 953 0 954 955 0 952 954 0 953 955 0 954 966 0 955 971 0 957 960 0 957 956 0
		 958 812 0 959 963 0 959 958 0 961 960 0 962 813 0 963 962 0 956 958 1 959 957 1 960 963 1
		 962 961 1 964 969 0 965 952 0 964 965 0 967 970 0 967 966 0 968 953 0;
	setAttr ".ed[1826:1835]" 969 968 0 971 970 0 964 967 1 966 965 1 968 971 1 970 969 1
		 956 961 0 965 968 0 966 971 0 958 962 0;
	setAttr -s 948 ".n";
	setAttr ".n[0:165]" -type "float3"  1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20;
	setAttr ".n[166:331]" -type "float3"  1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20;
	setAttr ".n[332:497]" -type "float3"  1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20;
	setAttr ".n[498:663]" -type "float3"  1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20;
	setAttr ".n[664:829]" -type "float3"  1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20;
	setAttr ".n[830:947]" -type "float3"  1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20;
	setAttr -s 918 -ch 3552 ".fc";
	setAttr ".fc[0:499]" -type "polyFaces" 
		f 4 -280 278 -284 -303
		f 4 281 -289 -279 -288
		f 4 3 -290 284 5
		f 4 -281 -287 -3 -5
		f 4 22 14 -24 -7
		f 4 23 15 -25 -8
		f 4 24 16 -26 -9
		f 4 25 17 -27 -10
		f 4 26 18 -28 -11
		f 4 27 19 -29 -12
		f 4 28 20 -30 -13
		f 4 29 21 -23 -14
		f 4 46 38 -48 -31
		f 4 47 39 -49 -32
		f 4 48 40 -50 -33
		f 4 49 41 -51 -34
		f 4 50 42 -52 -35
		f 4 51 43 -53 -36
		f 4 52 44 -54 -37
		f 4 53 45 -47 -38
		f 3 33 34 -58
		f 3 -56 -39 -46
		f 4 -44 -57 -41 54
		f 4 -45 -55 -40 55
		f 3 -43 -42 56
		f 4 -59 32 57 35
		f 4 36 -60 31 58
		f 3 37 30 59
		f 3 70 60 -72
		f 3 71 61 -73
		f 3 72 62 -71
		f 4 73 63 -75 -61
		f 4 74 64 -76 -62
		f 4 75 65 -74 -63
		f 3 76 -81 -64
		f 4 77 67 -79 -65
		f 4 78 68 -80 -66
		f 3 79 69 -77
		f 3 66 -78 80
		f 3 159 81 -161
		f 3 160 82 -162
		f 3 161 83 -160
		f 4 162 84 -164 -82
		f 4 163 85 -165 -83
		f 4 164 86 -163 -84
		f 3 165 -242 -85
		f 4 166 88 -168 -86
		f 4 167 89 -169 -87
		f 3 168 90 -166
		f 4 169 91 -171 -88
		f 4 170 92 -172 -89
		f 4 171 93 -173 -90
		f 4 172 94 -170 -91
		f 4 173 95 -175 -92
		f 4 174 96 -176 -93
		f 4 175 97 -177 -94
		f 4 176 98 -174 -95
		f 4 177 99 -179 -96
		f 4 178 100 -180 -97
		f 4 179 101 -181 -98
		f 4 180 102 -178 -99
		f 4 181 103 -183 -100
		f 4 182 104 -184 -101
		f 4 183 105 -185 -102
		f 4 184 106 -182 -103
		f 4 185 107 -187 -104
		f 4 186 108 -188 -105
		f 4 187 109 -189 -106
		f 4 188 110 -186 -107
		f 4 189 111 -191 -108
		f 4 190 112 -192 -109
		f 4 191 113 -193 -110
		f 4 192 114 -190 -111
		f 4 193 115 -195 -112
		f 4 194 116 -196 -113
		f 4 195 117 -197 -114
		f 4 196 118 -194 -115
		f 4 197 119 -199 -116
		f 4 198 120 -200 -117
		f 4 199 121 -201 -118
		f 4 200 122 -198 -119
		f 4 201 123 -203 -120
		f 4 202 124 -204 -121
		f 4 203 125 -205 -122
		f 4 204 126 -202 -123
		f 4 205 127 -207 -124
		f 4 206 128 -208 -125
		f 4 207 129 -209 -126
		f 4 208 130 -206 -127
		f 4 209 131 -211 -128
		f 4 210 132 -212 -129
		f 4 211 133 -213 -130
		f 4 212 134 -210 -131
		f 4 213 135 -215 -132
		f 4 214 136 -216 -133
		f 4 215 137 -217 -134
		f 4 216 138 -214 -135
		f 4 217 139 -219 -136
		f 4 218 140 -220 -137
		f 4 219 141 -221 -138
		f 4 220 142 -218 -139
		f 4 221 143 -223 -140
		f 4 222 144 -224 -141
		f 4 223 145 -225 -142
		f 4 224 146 -222 -143
		f 4 225 147 -227 -144
		f 4 226 148 -228 -145
		f 4 227 149 -229 -146
		f 4 228 150 -226 -147
		f 4 229 151 -231 -148
		f 4 230 152 -232 -149
		f 4 231 153 -233 -150
		f 4 232 154 -230 -151
		f 4 233 155 -235 -152
		f 4 234 156 -236 -153
		f 4 235 157 -237 -154
		f 4 236 158 -234 -155
		f 4 237 -68 -239 -156
		f 4 238 -67 -240 -157
		f 4 239 -70 -241 -158
		f 4 240 -69 -238 -159
		f 3 87 -167 241
		f 4 258 250 -260 -243
		f 4 259 251 -261 -244
		f 4 260 252 -262 -245
		f 4 261 253 -263 -246
		f 4 262 254 -264 -247
		f 4 263 255 -265 -248
		f 4 264 256 -266 -249
		f 4 265 257 -259 -250
		f 3 245 246 -269
		f 3 -272 -251 -258
		f 4 248 -271 243 266
		f 4 -256 -270 -253 267
		f 4 -267 244 268 247
		f 3 -255 -254 269
		f 3 249 242 270
		f 4 -257 -268 -252 271
		f 4 274 273 -276 -273
		f 4 -295 293 -298 -305
		f 4 290 -302 -294 -299
		f 4 295 275 277 -301
		f 4 -277 -275 -292 -300
		f 4 280 1 -285 -306
		f 4 286 -283 287 279
		f 4 288 285 289 283
		f 4 291 272 -296 -304
		f 4 298 294 299 -293
		f 4 300 297 301 296
		f 4 2 302 -4 -1
		f 4 303 -297 -291 292
		f 4 276 304 -278 -274
		f 4 305 -286 -282 282
		f 4 -586 584 -590 -609
		f 4 587 -595 -585 -594
		f 4 309 -596 590 311
		f 4 -587 -593 -309 -311
		f 4 328 320 -330 -313
		f 4 329 321 -331 -314
		f 4 330 322 -332 -315
		f 4 331 323 -333 -316
		f 4 332 324 -334 -317
		f 4 333 325 -335 -318
		f 4 334 326 -336 -319
		f 4 335 327 -329 -320
		f 4 352 344 -354 -337
		f 4 353 345 -355 -338
		f 4 354 346 -356 -339
		f 4 355 347 -357 -340
		f 4 356 348 -358 -341
		f 4 357 349 -359 -342
		f 4 358 350 -360 -343
		f 4 359 351 -353 -344
		f 3 339 340 -364
		f 3 -362 -345 -352
		f 4 -350 -363 -347 360
		f 4 -351 -361 -346 361
		f 3 -349 -348 362
		f 4 -365 338 363 341
		f 4 342 -366 337 364
		f 3 343 336 365
		f 3 376 366 -378
		f 3 377 367 -379
		f 3 378 368 -377
		f 4 379 369 -381 -367
		f 4 380 370 -382 -368
		f 4 381 371 -380 -369
		f 3 382 -387 -370
		f 4 383 373 -385 -371
		f 4 384 374 -386 -372
		f 3 385 375 -383
		f 3 372 -384 386
		f 3 465 387 -467
		f 3 466 388 -468
		f 3 467 389 -466
		f 4 468 390 -470 -388
		f 4 469 391 -471 -389
		f 4 470 392 -469 -390
		f 3 471 -548 -391
		f 4 472 394 -474 -392
		f 4 473 395 -475 -393
		f 3 474 396 -472
		f 4 475 397 -477 -394
		f 4 476 398 -478 -395
		f 4 477 399 -479 -396
		f 4 478 400 -476 -397
		f 4 479 401 -481 -398
		f 4 480 402 -482 -399
		f 4 481 403 -483 -400
		f 4 482 404 -480 -401
		f 4 483 405 -485 -402
		f 4 484 406 -486 -403
		f 4 485 407 -487 -404
		f 4 486 408 -484 -405
		f 4 487 409 -489 -406
		f 4 488 410 -490 -407
		f 4 489 411 -491 -408
		f 4 490 412 -488 -409
		f 4 491 413 -493 -410
		f 4 492 414 -494 -411
		f 4 493 415 -495 -412
		f 4 494 416 -492 -413
		f 4 495 417 -497 -414
		f 4 496 418 -498 -415
		f 4 497 419 -499 -416
		f 4 498 420 -496 -417
		f 4 499 421 -501 -418
		f 4 500 422 -502 -419
		f 4 501 423 -503 -420
		f 4 502 424 -500 -421
		f 4 503 425 -505 -422
		f 4 504 426 -506 -423
		f 4 505 427 -507 -424
		f 4 506 428 -504 -425
		f 4 507 429 -509 -426
		f 4 508 430 -510 -427
		f 4 509 431 -511 -428
		f 4 510 432 -508 -429
		f 4 511 433 -513 -430
		f 4 512 434 -514 -431
		f 4 513 435 -515 -432
		f 4 514 436 -512 -433
		f 4 515 437 -517 -434
		f 4 516 438 -518 -435
		f 4 517 439 -519 -436
		f 4 518 440 -516 -437
		f 4 519 441 -521 -438
		f 4 520 442 -522 -439
		f 4 521 443 -523 -440
		f 4 522 444 -520 -441
		f 4 523 445 -525 -442
		f 4 524 446 -526 -443
		f 4 525 447 -527 -444
		f 4 526 448 -524 -445
		f 4 527 449 -529 -446
		f 4 528 450 -530 -447
		f 4 529 451 -531 -448
		f 4 530 452 -528 -449
		f 4 531 453 -533 -450
		f 4 532 454 -534 -451
		f 4 533 455 -535 -452
		f 4 534 456 -532 -453
		f 4 535 457 -537 -454
		f 4 536 458 -538 -455
		f 4 537 459 -539 -456
		f 4 538 460 -536 -457
		f 4 539 461 -541 -458
		f 4 540 462 -542 -459
		f 4 541 463 -543 -460
		f 4 542 464 -540 -461
		f 4 543 -374 -545 -462
		f 4 544 -373 -546 -463
		f 4 545 -376 -547 -464
		f 4 546 -375 -544 -465
		f 3 393 -473 547
		f 4 564 556 -566 -549
		f 4 565 557 -567 -550
		f 4 566 558 -568 -551
		f 4 567 559 -569 -552
		f 4 568 560 -570 -553
		f 4 569 561 -571 -554
		f 4 570 562 -572 -555
		f 4 571 563 -565 -556
		f 3 551 552 -575
		f 3 -578 -557 -564
		f 4 554 -577 549 572
		f 4 -562 -576 -559 573
		f 4 -573 550 574 553
		f 3 -561 -560 575
		f 3 555 548 576
		f 4 -563 -574 -558 577
		f 4 580 579 -582 -579
		f 4 -601 599 -604 -611
		f 4 596 -608 -600 -605
		f 4 601 581 583 -607
		f 4 -583 -581 -598 -606
		f 4 586 307 -591 -612
		f 4 592 -589 593 585
		f 4 594 591 595 589
		f 4 597 578 -602 -610
		f 4 604 600 605 -599
		f 4 606 603 607 602
		f 4 308 608 -310 -307
		f 4 609 -603 -597 598
		f 4 582 610 -584 -580
		f 4 611 -592 -588 588
		f 4 -892 890 -896 -915
		f 4 893 -901 -891 -900
		f 4 615 -902 896 617
		f 4 -893 -899 -615 -617
		f 4 634 626 -636 -619
		f 4 635 627 -637 -620
		f 4 636 628 -638 -621
		f 4 637 629 -639 -622
		f 4 638 630 -640 -623
		f 4 639 631 -641 -624
		f 4 640 632 -642 -625
		f 4 641 633 -635 -626
		f 4 658 650 -660 -643
		f 4 659 651 -661 -644
		f 4 660 652 -662 -645
		f 4 661 653 -663 -646
		f 4 662 654 -664 -647
		f 4 663 655 -665 -648
		f 4 664 656 -666 -649
		f 4 665 657 -659 -650
		f 3 645 646 -670
		f 3 -668 -651 -658
		f 4 -656 -669 -653 666
		f 4 -657 -667 -652 667
		f 3 -655 -654 668
		f 4 -671 644 669 647
		f 4 648 -672 643 670
		f 3 649 642 671
		f 3 682 672 -684
		f 3 683 673 -685
		f 3 684 674 -683
		f 4 685 675 -687 -673
		f 4 686 676 -688 -674
		f 4 687 677 -686 -675
		f 3 688 -693 -676
		f 4 689 679 -691 -677
		f 4 690 680 -692 -678
		f 3 691 681 -689
		f 3 678 -690 692
		f 3 771 693 -773
		f 3 772 694 -774
		f 3 773 695 -772
		f 4 774 696 -776 -694
		f 4 775 697 -777 -695
		f 4 776 698 -775 -696
		f 3 777 -854 -697
		f 4 778 700 -780 -698
		f 4 779 701 -781 -699
		f 3 780 702 -778
		f 4 781 703 -783 -700
		f 4 782 704 -784 -701
		f 4 783 705 -785 -702
		f 4 784 706 -782 -703
		f 4 785 707 -787 -704
		f 4 786 708 -788 -705
		f 4 787 709 -789 -706
		f 4 788 710 -786 -707
		f 4 789 711 -791 -708
		f 4 790 712 -792 -709
		f 4 791 713 -793 -710
		f 4 792 714 -790 -711
		f 4 793 715 -795 -712
		f 4 794 716 -796 -713
		f 4 795 717 -797 -714
		f 4 796 718 -794 -715
		f 4 797 719 -799 -716
		f 4 798 720 -800 -717
		f 4 799 721 -801 -718
		f 4 800 722 -798 -719
		f 4 801 723 -803 -720
		f 4 802 724 -804 -721
		f 4 803 725 -805 -722
		f 4 804 726 -802 -723
		f 4 805 727 -807 -724
		f 4 806 728 -808 -725
		f 4 807 729 -809 -726
		f 4 808 730 -806 -727
		f 4 809 731 -811 -728
		f 4 810 732 -812 -729
		f 4 811 733 -813 -730
		f 4 812 734 -810 -731
		f 4 813 735 -815 -732
		f 4 814 736 -816 -733
		f 4 815 737 -817 -734
		f 4 816 738 -814 -735
		f 4 817 739 -819 -736
		f 4 818 740 -820 -737
		f 4 819 741 -821 -738
		f 4 820 742 -818 -739
		f 4 821 743 -823 -740
		f 4 822 744 -824 -741
		f 4 823 745 -825 -742
		f 4 824 746 -822 -743
		f 4 825 747 -827 -744
		f 4 826 748 -828 -745
		f 4 827 749 -829 -746
		f 4 828 750 -826 -747
		f 4 829 751 -831 -748
		f 4 830 752 -832 -749
		f 4 831 753 -833 -750
		f 4 832 754 -830 -751
		f 4 833 755 -835 -752
		f 4 834 756 -836 -753
		f 4 835 757 -837 -754
		f 4 836 758 -834 -755
		f 4 837 759 -839 -756
		f 4 838 760 -840 -757
		f 4 839 761 -841 -758
		f 4 840 762 -838 -759
		f 4 841 763 -843 -760
		f 4 842 764 -844 -761
		f 4 843 765 -845 -762
		f 4 844 766 -842 -763
		f 4 845 767 -847 -764
		f 4 846 768 -848 -765
		f 4 847 769 -849 -766
		f 4 848 770 -846 -767
		f 4 849 -680 -851 -768
		f 4 850 -679 -852 -769
		f 4 851 -682 -853 -770
		f 4 852 -681 -850 -771
		f 3 699 -779 853
		f 4 870 862 -872 -855
		f 4 871 863 -873 -856
		f 4 872 864 -874 -857
		f 4 873 865 -875 -858
		f 4 874 866 -876 -859
		f 4 875 867 -877 -860
		f 4 876 868 -878 -861
		f 4 877 869 -871 -862
		f 3 857 858 -881
		f 3 -884 -863 -870
		f 4 860 -883 855 878
		f 4 -868 -882 -865 879
		f 4 -879 856 880 859
		f 3 -867 -866 881
		f 3 861 854 882
		f 4 -869 -880 -864 883
		f 4 886 885 -888 -885
		f 4 -907 905 -910 -917
		f 4 902 -914 -906 -911
		f 4 907 887 889 -913
		f 4 -889 -887 -904 -912
		f 4 892 613 -897 -918
		f 4 898 -895 899 891
		f 4 900 897 901 895
		f 4 903 884 -908 -916
		f 4 910 906 911 -905
		f 4 912 909 913 908
		f 4 614 914 -616 -613
		f 4 915 -909 -903 904
		f 4 888 916 -890 -886
		f 4 917 -898 -894 894
		f 4 1220 1201 -1197 1197
		f 4 1205 1196 1206 -1200
		f 4 -924 -1203 1207 -922
		f 4 922 920 1204 1198
		f 4 924 941 -933 -941
		f 4 925 942 -934 -942
		f 4 926 943 -935 -943
		f 4 927 944 -936 -944
		f 4 928 945 -937 -945
		f 4 929 946 -938 -946
		f 4 930 947 -939 -947
		f 4 931 940 -940 -948
		f 4 948 965 -957 -965
		f 4 949 966 -958 -966
		f 4 950 967 -959 -967
		f 4 951 968 -960 -968
		f 4 952 969 -961 -969
		f 4 953 970 -962 -970
		f 4 954 971 -963 -971
		f 4 955 964 -964 -972
		f 3 975 -953 -952
		f 3 963 956 973
		f 4 -973 958 974 961
		f 4 -974 957 972 962
		f 3 -975 959 960
		f 4 -954 -976 -951 976
		f 4 -977 -950 977 -955
		f 3 -978 -949 -956
		f 3 989 -979 -989
		f 3 990 -980 -990
		f 3 988 -981 -991
		f 4 978 992 -982 -992
		f 4 979 993 -983 -993
		f 4 980 991 -984 -994
		f 3 981 998 -995
		f 4 982 996 -986 -996
		f 4 983 997 -987 -997
		f 3 994 -988 -998
		f 3 -999 995 -985
		f 3 1078 -1000 -1078
		f 3 1079 -1001 -1079;
	setAttr ".fc[500:917]"
		f 3 1077 -1002 -1080
		f 4 999 1081 -1003 -1081
		f 4 1000 1082 -1004 -1082
		f 4 1001 1080 -1005 -1083
		f 3 1002 1159 -1084
		f 4 1003 1085 -1007 -1085
		f 4 1004 1086 -1008 -1086
		f 3 1083 -1009 -1087
		f 4 1005 1088 -1010 -1088
		f 4 1006 1089 -1011 -1089
		f 4 1007 1090 -1012 -1090
		f 4 1008 1087 -1013 -1091
		f 4 1009 1092 -1014 -1092
		f 4 1010 1093 -1015 -1093
		f 4 1011 1094 -1016 -1094
		f 4 1012 1091 -1017 -1095
		f 4 1013 1096 -1018 -1096
		f 4 1014 1097 -1019 -1097
		f 4 1015 1098 -1020 -1098
		f 4 1016 1095 -1021 -1099
		f 4 1017 1100 -1022 -1100
		f 4 1018 1101 -1023 -1101
		f 4 1019 1102 -1024 -1102
		f 4 1020 1099 -1025 -1103
		f 4 1021 1104 -1026 -1104
		f 4 1022 1105 -1027 -1105
		f 4 1023 1106 -1028 -1106
		f 4 1024 1103 -1029 -1107
		f 4 1025 1108 -1030 -1108
		f 4 1026 1109 -1031 -1109
		f 4 1027 1110 -1032 -1110
		f 4 1028 1107 -1033 -1111
		f 4 1029 1112 -1034 -1112
		f 4 1030 1113 -1035 -1113
		f 4 1031 1114 -1036 -1114
		f 4 1032 1111 -1037 -1115
		f 4 1033 1116 -1038 -1116
		f 4 1034 1117 -1039 -1117
		f 4 1035 1118 -1040 -1118
		f 4 1036 1115 -1041 -1119
		f 4 1037 1120 -1042 -1120
		f 4 1038 1121 -1043 -1121
		f 4 1039 1122 -1044 -1122
		f 4 1040 1119 -1045 -1123
		f 4 1041 1124 -1046 -1124
		f 4 1042 1125 -1047 -1125
		f 4 1043 1126 -1048 -1126
		f 4 1044 1123 -1049 -1127
		f 4 1045 1128 -1050 -1128
		f 4 1046 1129 -1051 -1129
		f 4 1047 1130 -1052 -1130
		f 4 1048 1127 -1053 -1131
		f 4 1049 1132 -1054 -1132
		f 4 1050 1133 -1055 -1133
		f 4 1051 1134 -1056 -1134
		f 4 1052 1131 -1057 -1135
		f 4 1053 1136 -1058 -1136
		f 4 1054 1137 -1059 -1137
		f 4 1055 1138 -1060 -1138
		f 4 1056 1135 -1061 -1139
		f 4 1057 1140 -1062 -1140
		f 4 1058 1141 -1063 -1141
		f 4 1059 1142 -1064 -1142
		f 4 1060 1139 -1065 -1143
		f 4 1061 1144 -1066 -1144
		f 4 1062 1145 -1067 -1145
		f 4 1063 1146 -1068 -1146
		f 4 1064 1143 -1069 -1147
		f 4 1065 1148 -1070 -1148
		f 4 1066 1149 -1071 -1149
		f 4 1067 1150 -1072 -1150
		f 4 1068 1147 -1073 -1151
		f 4 1069 1152 -1074 -1152
		f 4 1070 1153 -1075 -1153
		f 4 1071 1154 -1076 -1154
		f 4 1072 1151 -1077 -1155
		f 4 1073 1156 985 -1156
		f 4 1074 1157 984 -1157
		f 4 1075 1158 987 -1158
		f 4 1076 1155 986 -1159
		f 3 -1160 1084 -1006
		f 4 1160 1177 -1169 -1177
		f 4 1161 1178 -1170 -1178
		f 4 1162 1179 -1171 -1179
		f 4 1163 1180 -1172 -1180
		f 4 1164 1181 -1173 -1181
		f 4 1165 1182 -1174 -1182
		f 4 1166 1183 -1175 -1183
		f 4 1167 1176 -1176 -1184
		f 3 1186 -1165 -1164
		f 3 1175 1168 1189
		f 4 -1185 -1162 1188 -1167
		f 4 -1186 1170 1187 1173
		f 4 -1166 -1187 -1163 1184
		f 3 -1188 1171 1172
		f 3 -1189 -1161 -1168
		f 4 -1190 1169 1185 1174
		f 4 1190 1193 -1192 -1193
		f 4 1222 1215 -1212 1212
		f 4 1216 1211 1219 -1209
		f 4 1218 -1196 -1194 -1214
		f 4 1217 1209 1192 1194
		f 4 1223 1202 -920 -1199
		f 4 -1198 -1206 1200 -1205
		f 4 -1202 -1208 -1204 -1207
		f 4 1221 1213 -1191 -1210
		f 4 1210 -1218 -1213 -1217
		f 4 -1215 -1220 -1216 -1219
		f 4 918 921 -1221 -921
		f 4 -1211 1208 1214 -1222
		f 4 1191 1195 -1223 -1195
		f 4 -1201 1199 1203 -1224
		f 4 1526 1507 -1503 1503
		f 4 1511 1502 1512 -1506
		f 4 -1230 -1509 1513 -1228
		f 4 1228 1226 1510 1504
		f 4 1230 1247 -1239 -1247
		f 4 1231 1248 -1240 -1248
		f 4 1232 1249 -1241 -1249
		f 4 1233 1250 -1242 -1250
		f 4 1234 1251 -1243 -1251
		f 4 1235 1252 -1244 -1252
		f 4 1236 1253 -1245 -1253
		f 4 1237 1246 -1246 -1254
		f 4 1254 1271 -1263 -1271
		f 4 1255 1272 -1264 -1272
		f 4 1256 1273 -1265 -1273
		f 4 1257 1274 -1266 -1274
		f 4 1258 1275 -1267 -1275
		f 4 1259 1276 -1268 -1276
		f 4 1260 1277 -1269 -1277
		f 4 1261 1270 -1270 -1278
		f 3 1281 -1259 -1258
		f 3 1269 1262 1279
		f 4 -1279 1264 1280 1267
		f 4 -1280 1263 1278 1268
		f 3 -1281 1265 1266
		f 4 -1260 -1282 -1257 1282
		f 4 -1283 -1256 1283 -1261
		f 3 -1284 -1255 -1262
		f 3 1295 -1285 -1295
		f 3 1296 -1286 -1296
		f 3 1294 -1287 -1297
		f 4 1284 1298 -1288 -1298
		f 4 1285 1299 -1289 -1299
		f 4 1286 1297 -1290 -1300
		f 3 1287 1304 -1301
		f 4 1288 1302 -1292 -1302
		f 4 1289 1303 -1293 -1303
		f 3 1300 -1294 -1304
		f 3 -1305 1301 -1291
		f 3 1384 -1306 -1384
		f 3 1385 -1307 -1385
		f 3 1383 -1308 -1386
		f 4 1305 1387 -1309 -1387
		f 4 1306 1388 -1310 -1388
		f 4 1307 1386 -1311 -1389
		f 3 1308 1465 -1390
		f 4 1309 1391 -1313 -1391
		f 4 1310 1392 -1314 -1392
		f 3 1389 -1315 -1393
		f 4 1311 1394 -1316 -1394
		f 4 1312 1395 -1317 -1395
		f 4 1313 1396 -1318 -1396
		f 4 1314 1393 -1319 -1397
		f 4 1315 1398 -1320 -1398
		f 4 1316 1399 -1321 -1399
		f 4 1317 1400 -1322 -1400
		f 4 1318 1397 -1323 -1401
		f 4 1319 1402 -1324 -1402
		f 4 1320 1403 -1325 -1403
		f 4 1321 1404 -1326 -1404
		f 4 1322 1401 -1327 -1405
		f 4 1323 1406 -1328 -1406
		f 4 1324 1407 -1329 -1407
		f 4 1325 1408 -1330 -1408
		f 4 1326 1405 -1331 -1409
		f 4 1327 1410 -1332 -1410
		f 4 1328 1411 -1333 -1411
		f 4 1329 1412 -1334 -1412
		f 4 1330 1409 -1335 -1413
		f 4 1331 1414 -1336 -1414
		f 4 1332 1415 -1337 -1415
		f 4 1333 1416 -1338 -1416
		f 4 1334 1413 -1339 -1417
		f 4 1335 1418 -1340 -1418
		f 4 1336 1419 -1341 -1419
		f 4 1337 1420 -1342 -1420
		f 4 1338 1417 -1343 -1421
		f 4 1339 1422 -1344 -1422
		f 4 1340 1423 -1345 -1423
		f 4 1341 1424 -1346 -1424
		f 4 1342 1421 -1347 -1425
		f 4 1343 1426 -1348 -1426
		f 4 1344 1427 -1349 -1427
		f 4 1345 1428 -1350 -1428
		f 4 1346 1425 -1351 -1429
		f 4 1347 1430 -1352 -1430
		f 4 1348 1431 -1353 -1431
		f 4 1349 1432 -1354 -1432
		f 4 1350 1429 -1355 -1433
		f 4 1351 1434 -1356 -1434
		f 4 1352 1435 -1357 -1435
		f 4 1353 1436 -1358 -1436
		f 4 1354 1433 -1359 -1437
		f 4 1355 1438 -1360 -1438
		f 4 1356 1439 -1361 -1439
		f 4 1357 1440 -1362 -1440
		f 4 1358 1437 -1363 -1441
		f 4 1359 1442 -1364 -1442
		f 4 1360 1443 -1365 -1443
		f 4 1361 1444 -1366 -1444
		f 4 1362 1441 -1367 -1445
		f 4 1363 1446 -1368 -1446
		f 4 1364 1447 -1369 -1447
		f 4 1365 1448 -1370 -1448
		f 4 1366 1445 -1371 -1449
		f 4 1367 1450 -1372 -1450
		f 4 1368 1451 -1373 -1451
		f 4 1369 1452 -1374 -1452
		f 4 1370 1449 -1375 -1453
		f 4 1371 1454 -1376 -1454
		f 4 1372 1455 -1377 -1455
		f 4 1373 1456 -1378 -1456
		f 4 1374 1453 -1379 -1457
		f 4 1375 1458 -1380 -1458
		f 4 1376 1459 -1381 -1459
		f 4 1377 1460 -1382 -1460
		f 4 1378 1457 -1383 -1461
		f 4 1379 1462 1291 -1462
		f 4 1380 1463 1290 -1463
		f 4 1381 1464 1293 -1464
		f 4 1382 1461 1292 -1465
		f 3 -1466 1390 -1312
		f 4 1466 1483 -1475 -1483
		f 4 1467 1484 -1476 -1484
		f 4 1468 1485 -1477 -1485
		f 4 1469 1486 -1478 -1486
		f 4 1470 1487 -1479 -1487
		f 4 1471 1488 -1480 -1488
		f 4 1472 1489 -1481 -1489
		f 4 1473 1482 -1482 -1490
		f 3 1492 -1471 -1470
		f 3 1481 1474 1495
		f 4 -1491 -1468 1494 -1473
		f 4 -1492 1476 1493 1479
		f 4 -1472 -1493 -1469 1490
		f 3 -1494 1477 1478
		f 3 -1495 -1467 -1474
		f 4 -1496 1475 1491 1480
		f 4 1496 1499 -1498 -1499
		f 4 1528 1521 -1518 1518
		f 4 1522 1517 1525 -1515
		f 4 1524 -1502 -1500 -1520
		f 4 1523 1515 1498 1500
		f 4 1529 1508 -1226 -1505
		f 4 -1504 -1512 1506 -1511
		f 4 -1508 -1514 -1510 -1513
		f 4 1527 1519 -1497 -1516
		f 4 1516 -1524 -1519 -1523
		f 4 -1521 -1526 -1522 -1525
		f 4 1224 1227 -1527 -1227
		f 4 -1517 1514 1520 -1528
		f 4 1497 1501 -1529 -1501
		f 4 -1507 1505 1509 -1530
		f 4 1832 1813 -1809 1809
		f 4 1817 1808 1818 -1812
		f 4 -1536 -1815 1819 -1534
		f 4 1534 1532 1816 1810
		f 4 1536 1553 -1545 -1553
		f 4 1537 1554 -1546 -1554
		f 4 1538 1555 -1547 -1555
		f 4 1539 1556 -1548 -1556
		f 4 1540 1557 -1549 -1557
		f 4 1541 1558 -1550 -1558
		f 4 1542 1559 -1551 -1559
		f 4 1543 1552 -1552 -1560
		f 4 1560 1577 -1569 -1577
		f 4 1561 1578 -1570 -1578
		f 4 1562 1579 -1571 -1579
		f 4 1563 1580 -1572 -1580
		f 4 1564 1581 -1573 -1581
		f 4 1565 1582 -1574 -1582
		f 4 1566 1583 -1575 -1583
		f 4 1567 1576 -1576 -1584
		f 3 1587 -1565 -1564
		f 3 1575 1568 1585
		f 4 -1585 1570 1586 1573
		f 4 -1586 1569 1584 1574
		f 3 -1587 1571 1572
		f 4 -1566 -1588 -1563 1588
		f 4 -1589 -1562 1589 -1567
		f 3 -1590 -1561 -1568
		f 3 1601 -1591 -1601
		f 3 1602 -1592 -1602
		f 3 1600 -1593 -1603
		f 4 1590 1604 -1594 -1604
		f 4 1591 1605 -1595 -1605
		f 4 1592 1603 -1596 -1606
		f 3 1593 1610 -1607
		f 4 1594 1608 -1598 -1608
		f 4 1595 1609 -1599 -1609
		f 3 1606 -1600 -1610
		f 3 -1611 1607 -1597
		f 3 1690 -1612 -1690
		f 3 1691 -1613 -1691
		f 3 1689 -1614 -1692
		f 4 1611 1693 -1615 -1693
		f 4 1612 1694 -1616 -1694
		f 4 1613 1692 -1617 -1695
		f 3 1614 1771 -1696
		f 4 1615 1697 -1619 -1697
		f 4 1616 1698 -1620 -1698
		f 3 1695 -1621 -1699
		f 4 1617 1700 -1622 -1700
		f 4 1618 1701 -1623 -1701
		f 4 1619 1702 -1624 -1702
		f 4 1620 1699 -1625 -1703
		f 4 1621 1704 -1626 -1704
		f 4 1622 1705 -1627 -1705
		f 4 1623 1706 -1628 -1706
		f 4 1624 1703 -1629 -1707
		f 4 1625 1708 -1630 -1708
		f 4 1626 1709 -1631 -1709
		f 4 1627 1710 -1632 -1710
		f 4 1628 1707 -1633 -1711
		f 4 1629 1712 -1634 -1712
		f 4 1630 1713 -1635 -1713
		f 4 1631 1714 -1636 -1714
		f 4 1632 1711 -1637 -1715
		f 4 1633 1716 -1638 -1716
		f 4 1634 1717 -1639 -1717
		f 4 1635 1718 -1640 -1718
		f 4 1636 1715 -1641 -1719
		f 4 1637 1720 -1642 -1720
		f 4 1638 1721 -1643 -1721
		f 4 1639 1722 -1644 -1722
		f 4 1640 1719 -1645 -1723
		f 4 1641 1724 -1646 -1724
		f 4 1642 1725 -1647 -1725
		f 4 1643 1726 -1648 -1726
		f 4 1644 1723 -1649 -1727
		f 4 1645 1728 -1650 -1728
		f 4 1646 1729 -1651 -1729
		f 4 1647 1730 -1652 -1730
		f 4 1648 1727 -1653 -1731
		f 4 1649 1732 -1654 -1732
		f 4 1650 1733 -1655 -1733
		f 4 1651 1734 -1656 -1734
		f 4 1652 1731 -1657 -1735
		f 4 1653 1736 -1658 -1736
		f 4 1654 1737 -1659 -1737
		f 4 1655 1738 -1660 -1738
		f 4 1656 1735 -1661 -1739
		f 4 1657 1740 -1662 -1740
		f 4 1658 1741 -1663 -1741
		f 4 1659 1742 -1664 -1742
		f 4 1660 1739 -1665 -1743
		f 4 1661 1744 -1666 -1744
		f 4 1662 1745 -1667 -1745
		f 4 1663 1746 -1668 -1746
		f 4 1664 1743 -1669 -1747
		f 4 1665 1748 -1670 -1748
		f 4 1666 1749 -1671 -1749
		f 4 1667 1750 -1672 -1750
		f 4 1668 1747 -1673 -1751
		f 4 1669 1752 -1674 -1752
		f 4 1670 1753 -1675 -1753
		f 4 1671 1754 -1676 -1754
		f 4 1672 1751 -1677 -1755
		f 4 1673 1756 -1678 -1756
		f 4 1674 1757 -1679 -1757
		f 4 1675 1758 -1680 -1758
		f 4 1676 1755 -1681 -1759
		f 4 1677 1760 -1682 -1760
		f 4 1678 1761 -1683 -1761
		f 4 1679 1762 -1684 -1762
		f 4 1680 1759 -1685 -1763
		f 4 1681 1764 -1686 -1764
		f 4 1682 1765 -1687 -1765
		f 4 1683 1766 -1688 -1766
		f 4 1684 1763 -1689 -1767
		f 4 1685 1768 1597 -1768
		f 4 1686 1769 1596 -1769
		f 4 1687 1770 1599 -1770
		f 4 1688 1767 1598 -1771
		f 3 -1772 1696 -1618
		f 4 1772 1789 -1781 -1789
		f 4 1773 1790 -1782 -1790
		f 4 1774 1791 -1783 -1791
		f 4 1775 1792 -1784 -1792
		f 4 1776 1793 -1785 -1793
		f 4 1777 1794 -1786 -1794
		f 4 1778 1795 -1787 -1795
		f 4 1779 1788 -1788 -1796
		f 3 1798 -1777 -1776
		f 3 1787 1780 1801
		f 4 -1797 -1774 1800 -1779
		f 4 -1798 1782 1799 1785
		f 4 -1778 -1799 -1775 1796
		f 3 -1800 1783 1784
		f 3 -1801 -1773 -1780
		f 4 -1802 1781 1797 1786
		f 4 1802 1805 -1804 -1805
		f 4 1834 1827 -1824 1824
		f 4 1828 1823 1831 -1821
		f 4 1830 -1808 -1806 -1826
		f 4 1829 1821 1804 1806
		f 4 1835 1814 -1532 -1811
		f 4 -1810 -1818 1812 -1817
		f 4 -1814 -1820 -1816 -1819
		f 4 1833 1825 -1803 -1822
		f 4 1822 -1830 -1825 -1829
		f 4 -1827 -1832 -1828 -1831
		f 4 1530 1533 -1833 -1533
		f 4 -1823 1820 1826 -1834
		f 4 1803 1807 -1835 -1807
		f 4 -1813 1811 1815 -1836;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".vcs" 2;
createNode transform -n "MSH_springs_tutorial_skinMesh" -p "GRP_utility_meshes";
	rename -uid "A38776E4-4AE2-BCC7-CAB7-BEAD344B1A87";
	setAttr ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode mesh -n "MSH_springs_tutorial_skinMeshShape" -p "MSH_springs_tutorial_skinMesh";
	rename -uid "49F47ED0-4719-B014-9339-DAB71E97693A";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".sdt" 0;
	setAttr ".ugsdt" no;
	setAttr ".vcs" 2;
createNode mesh -n "MSH_springs_tutorial_skinMeshShapeOrig" -p "MSH_springs_tutorial_skinMesh";
	rename -uid "601DC572-4301-ABFC-698C-96928437923C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".sdt" 0;
	setAttr -s 624 ".vt";
	setAttr ".vt[0:165]"  -40.69884109 40.000026702881 -155.11117554 -38.81987762 38.82155991 -154.19248962
		 -38.041576385 38.33343124 -151.9744873 -38.81987381 38.82156754 -149.75657654 -40.69882202 40.000003814697 -148.83786011
		 -42.57778168 41.17851257 -149.75657654 -43.35607147 41.66664124 -151.97453308 -42.57778549 41.17850113 -154.19245911
		 -39.26737213 42.28232574 -155.11117554 -37.38842773 41.10385513 -154.19248962 -36.61011124 40.61574173 -151.9744873
		 -37.3884201 41.10386658 -149.75657654 -39.2673645 42.282341 -148.83789063 -41.14633179 43.46083832 -149.75660706
		 -41.92461395 43.94896317 -151.97450256 -41.14632416 43.4608345 -154.19248962 -33.4121933 51.61779404 -155.11112976
		 -31.53321648 50.43935013 -154.19247437 -30.75494766 49.95121765 -151.97450256 -31.53323174 50.43935013 -149.75656128
		 -33.41216278 51.61783981 -148.83787537 -35.2911377 52.79629898 -149.75656128 -36.06942749 53.28442764 -151.97451782
		 -35.2911377 52.79628754 -154.19244385 -32.00806427 53.85652924 -155.11116028 -30.12911034 52.67806244 -154.19247437
		 -29.35083199 52.18989182 -151.97447205 -30.12911034 52.67806244 -149.75656128 -32.0080566406 53.85652924 -148.83787537
		 -33.88702393 55.035003662 -149.75656128 -34.66530991 55.52313614 -151.97451782 -33.88702393 55.035030365 -154.19247437
		 -29.24106598 50.8389473 -148.043167114 -35.90217972 55.016765594 -148.043136597 -29.24106979 50.83895111 -155.90592957
		 -35.90217209 55.016788483 -155.90596008 -54.5196991 21.59993744 -153.90634155 -52.88312149 20.57347107 -154.70655823
		 -51.2465477 19.54700851 -153.90637207 -50.56864929 19.12183762 -151.9744873 -51.2465477 19.5470047 -150.042663574
		 -52.88312531 20.57347488 -149.24250793 -54.5196991 21.59993744 -150.042694092 -55.19760132 22.025112152 -151.9744873
		 -34.36645508 53.73210144 -153.90634155 -32.72986221 52.70567322 -154.70655823 -31.093313217 51.67921448 -153.90634155
		 -30.41540527 51.25401688 -151.9744873 -31.093305588 51.67921448 -150.042694092 -32.72988129 52.70568848 -149.24247742
		 -34.36646271 53.73213959 -150.042694092 -35.04434967 54.15730286 -151.97451782 -40.69882965 40.000022888184 -158.98306274
		 -36.50046921 37.36684418 -156.93031311 -34.76145172 36.27613068 -151.97451782 -36.50046158 37.36684799 -147.018722534
		 -40.69881821 40.000026702881 -144.96600342 -44.89717102 42.63321686 -147.018707275
		 -46.63618851 43.72396088 -151.9745636 -44.89718246 42.63323593 -156.93035889 -39.26735687 42.28232193 -158.98309326
		 -35.069015503 39.6491127 -156.93031311 -33.3299942 38.55844116 -151.97451782 -35.069007874 39.64915085 -147.018722534
		 -39.26737213 42.28233719 -144.9659729 -43.46572113 44.91553879 -147.018737793 -45.2047348 46.0062217712 -151.97453308
		 -43.4657135 44.91550446 -156.93032837 -33.41217804 51.61779785 -158.98304749 -29.21380806 48.98463821 -156.93034363
		 -27.47482109 47.89389801 -151.9744873 -29.21383286 48.98461151 -147.018722534 -33.41217804 51.61783981 -144.96598816
		 -37.61052704 54.25101089 -147.018722534 -39.34954834 55.34172821 -151.97454834 -37.61051941 54.25102997 -156.93034363
		 -32.0080795288 53.85653687 -158.98304749 -27.80970955 51.22335434 -156.93032837 -26.070695877 50.13262939 -151.97447205
		 -27.80971336 51.22335434 -147.018707275 -32.0080718994 53.85652924 -144.96595764
		 -36.20642853 56.48974228 -147.018722534 -37.94542694 57.58046341 -151.97454834 -36.20640564 56.48970413 -156.93034363
		 -48.35236359 21.48641968 -148.62129211 -54.0337677 25.049762726 -148.62130737 -48.35234833 21.48641586 -155.32771301
		 -54.033752441 25.049804688 -155.32772827 -25.97711563 56.042945862 -148.043136597
		 -26.52030945 58.4139061 -148.043136597 -25.97711563 56.042964935 -155.90589905 -26.52030563 58.41389084 -155.90592957
		 -30.26727104 60.76402664 -148.043167114 -32.63822937 60.22078705 -148.043151855 -32.63822174 60.22079086 -155.90594482
		 -30.26727104 60.76399231 -155.90596008 -53.5240097 16.8137207 -148.62132263 -50.90700531 17.41327286 -148.62130737
		 -50.9070015 17.41327667 -155.32772827 -53.52398682 16.8137207 -155.32771301 -56.58841705 20.97663116 -148.62132263
		 -55.98884964 18.35964584 -148.62130737 -55.98884583 18.35964966 -155.32772827 -56.58840942 20.9766655 -155.32771301
		 -40.69883728 40.000026702881 -125.26464844 -38.81988144 38.82155991 -124.34596252
		 -38.0415802 38.33343124 -122.12796021 -38.81987762 38.82156754 -119.91003418 -40.69882202 40.000034332275 -118.99136353
		 -42.57778168 41.17851257 -119.91003418 -43.35607147 41.66664124 -122.12800598 -42.57778931 41.17850113 -124.34593201
		 -39.26737213 42.28232574 -125.26463318 -37.3884201 41.10385513 -124.34594727 -36.61011505 40.61574173 -122.12796021
		 -37.38841248 41.10386658 -119.91003418 -39.2673645 42.282341 -118.99134827 -41.14633179 43.46083832 -119.91007996
		 -41.92461395 43.94896317 -122.12797546 -41.14632416 43.46080399 -124.34596252 -33.41218567 51.61782455 -125.26460266
		 -31.53322029 50.43935013 -124.34594727 -30.75494385 49.95121765 -122.12799072 -31.53322792 50.43935013 -119.91003418
		 -33.41217041 51.61783981 -118.99134827 -35.2911377 52.79629898 -119.91003418 -36.06942749 53.28442764 -122.12800598
		 -35.2911377 52.79628754 -124.34591675 -32.00806427 53.85652924 -125.26461792 -30.12910652 52.67806244 -124.34594727
		 -29.35082817 52.18989182 -122.12796021 -30.12911034 52.67806244 -119.91001892 -32.00806427 53.85652924 -118.99134827
		 -33.88702011 55.035003662 -119.91001892 -34.66530991 55.52313614 -122.12799072 -33.8870163 55.034999847 -124.34591675
		 -29.24106216 50.8389473 -118.19664001 -35.90217209 55.016765594 -118.19664001 -29.24106598 50.83895111 -126.059402466
		 -35.9021759 55.016788483 -126.059463501 -54.5196991 21.59993744 -124.059814453 -52.88312531 20.57347107 -124.86003113
		 -51.24654388 19.54700851 -124.059844971 -50.56864166 19.12183762 -122.12796021 -51.24655151 19.5470047 -120.19613647
		 -52.88311768 20.57347488 -119.39598083 -54.5196991 21.59993744 -120.19616699 -55.19759369 22.025112152 -122.12796021
		 -34.36645508 53.73213196 -124.059844971 -32.72986984 52.70567322 -124.86003113 -31.093309402 51.67921448 -124.059814453
		 -30.41540146 51.25404739 -122.12797546 -31.093301773 51.67921448 -120.19616699 -32.72987747 52.70565796 -119.39595032
		 -34.36646271 53.73213959 -120.19613647 -35.044345856 54.15730286 -122.12799072 -40.69882584 40.000022888184 -129.13653564
		 -36.50046539 37.36684418 -127.083786011 -34.76145172 36.27613068 -122.12799072 -36.50046539 37.36684799 -117.17221069
		 -40.69882202 40.000026702881 -115.11946106 -44.89717102 42.63324738 -117.17219543
		 -46.63618469 43.72396088 -122.1280365 -44.89717865 42.63323593 -127.08380127 -39.26735687 42.28232193 -129.13656616
		 -35.069015503 39.6491127 -127.083786011;
	setAttr ".vt[166:331]" -33.33000183 38.55847168 -122.12799072 -35.069007874 39.64915085 -117.17219543
		 -39.26737213 42.28233719 -115.1194458 -43.46572113 44.91553879 -117.17221069 -45.2047348 46.0062217712 -122.12800598
		 -43.4657135 44.91553497 -127.08380127 -33.41217804 51.61779785 -129.13652039 -29.2138195 48.98463821 -127.083816528
		 -27.47481728 47.89389801 -122.12796021 -29.21383286 48.98461151 -117.17221069 -33.41217041 51.61783981 -115.11946106
		 -37.61052704 54.25101089 -117.17219543 -39.34954834 55.34172821 -122.12802124 -37.61052704 54.25099945 -127.083786011
		 -32.0080718994 53.85656738 -129.1365509 -27.80971718 51.22335434 -127.083831787 -26.070703506 50.13262939 -122.12794495
		 -27.80970955 51.22332382 -117.17221069 -32.0080718994 53.85652924 -115.11941528 -36.2064209 56.48971176 -117.17214966
		 -37.94541931 57.58046341 -122.12802124 -36.20641327 56.48973465 -127.083786011 -48.35235596 21.48641968 -118.77476501
		 -54.0337677 25.049762726 -118.77479553 -48.3523407 21.48638535 -125.4811554 -54.033752441 25.049804688 -125.48120117
		 -25.97711945 56.042945862 -118.19664001 -26.52030563 58.4139061 -118.1966095 -25.97712326 56.042964935 -126.059402466
		 -26.52030182 58.41389084 -126.059432983 -30.26727104 60.76399612 -118.19664001 -32.63822937 60.22078705 -118.19662476
		 -32.63822174 60.22079086 -126.059417725 -30.26727104 60.76399231 -126.059432983 -53.52400589 16.8137207 -118.77479553
		 -50.90700912 17.41327286 -118.77476501 -50.90701294 17.41328049 -125.48120117 -53.52399063 16.8137207 -125.48117065
		 -56.58841705 20.97663116 -118.77478027 -55.98885345 18.35964584 -118.77478027 -55.98884583 18.35964966 -125.48120117
		 -56.58840942 20.9766655 -125.48117065 -40.76507568 40.90059662 132.86335754 -38.8861084 39.72212219 133.78204346
		 -38.1078186 39.2339859 136.000015258789 -38.88610077 39.72213364 138.21794128 -40.76506805 40.90060425 139.13664246
		 -42.64402008 42.079086304 138.21794128 -43.42230988 42.5672226 136 -42.64403534 42.079071045 133.78204346
		 -39.33361053 43.1829071 132.86334229 -37.45465851 42.0044212341 133.78205872 -36.67636108 41.5163002 136.000015258789
		 -37.45465088 42.0044364929 138.21794128 -39.33360291 43.18291855 139.13665771 -41.21257019 44.36138535 138.21792603
		 -41.99085999 44.84952545 136 -41.21257019 44.36138153 133.78204346 -33.47842407 52.51838303 132.8633728
		 -31.59945869 51.33991241 133.78205872 -30.82117653 50.85176849 136.000015258789 -31.59946251 51.3399086 138.21795654
		 -33.47840881 52.5184021 139.13665771 -35.35736465 53.69686127 138.21795654 -36.13565445 54.18499374 136
		 -35.35737228 53.69685364 133.78205872 -32.074306488 54.75710297 132.86335754 -30.19534492 53.57862473 133.78205872
		 -29.41706085 53.09047699 136.000015258789 -30.19534492 53.57862473 138.21795654 -32.074306488 54.75709915 139.13665771
		 -33.95325089 55.93556976 138.21795654 -34.73153687 56.42370987 136.000015258789 -33.9532547 55.93556595 133.78205872
		 -29.30729485 51.7395134 139.93135071 -35.96840668 55.91734314 139.93136597 -29.30729866 51.7395134 132.068572998
		 -35.96841431 55.91733932 132.068572998 -54.58592987 22.50049591 134.06817627 -52.94936371 21.4740448 133.26795959
		 -51.31277466 20.44757843 134.068161011 -50.63487625 20.022399902 136.000015258789
		 -51.31277466 20.44757462 137.93185425 -52.94935226 21.47404861 138.73200989 -54.5859375 22.50050735 137.93182373
		 -55.26382828 22.92567444 136.000015258789 -34.43270111 54.63268661 134.068161011
		 -32.79611206 53.6062355 133.26797485 -31.15954018 52.57977676 134.06817627 -30.48163795 52.15460587 136
		 -31.15953636 52.57977676 137.93183899 -32.79611206 53.60624695 138.73204041 -34.43269348 54.63269424 137.93183899
		 -35.11058044 55.057857513 136 -40.76506042 40.9005928 128.99145508 -36.5667038 38.26740265 131.044189453
		 -34.8276825 37.17669678 136.000015258789 -36.56669617 38.26739883 140.95579529 -40.7650528 40.9005928 143.0085449219
		 -44.96341705 43.53380585 140.95579529 -46.7024231 44.62450027 135.99998474 -44.96341705 43.53379059 131.044189453
		 -39.33360291 43.18289948 128.99142456 -35.13525391 40.54970932 131.044189453 -33.39622498 39.45900726 136.000015258789
		 -35.13524628 40.54971695 140.95579529 -39.33360291 43.18291473 143.0085601807 -43.5319519 45.81609344 140.95576477
		 -45.27096558 46.90680695 136 -43.5319519 45.81609726 131.044189453 -33.47842407 52.51838684 128.99147034
		 -29.28004837 49.88518524 131.044189453 -27.54105186 48.79447937 136.000015258789
		 -29.28006744 49.88518143 140.95579529 -33.47840881 52.51839828 143.0085449219 -37.67677307 55.15158081 140.95579529
		 -39.41578674 56.24229431 135.99996948 -37.67676544 55.15158081 131.044189453 -32.074306488 54.75710678 128.99145508
		 -27.87595177 52.123909 131.044189453 -26.13693619 51.033195496 136.000030517578 -27.87594414 52.12390518 140.95579529
		 -32.074302673 54.75709534 143.0085601807 -36.2726593 57.39029312 140.95581055 -38.011672974 58.48101044 136
		 -36.27264023 57.39027786 131.044189453 -48.41859818 22.38697052 139.35321045 -54.099994659 25.95034027 139.35321045
		 -48.41859436 22.38697815 132.64680481 -54.09998703 25.95035172 132.64680481 -26.043346405 56.94352722 139.93136597
		 -26.58654022 59.31446838 139.93136597 -26.043357849 56.94352341 132.068603516 -26.58653831 59.31445694 132.068572998
		 -30.33350563 61.66456604 139.93136597 -32.70446014 61.12135696 139.93136597 -32.70446014 61.12136841 132.068572998
		 -30.33350945 61.66456985 132.068572998 -53.59024048 17.71428299 139.35321045 -50.97325134 18.3138504 139.35321045
		 -50.97324371 18.31385803 132.64680481 -53.59023285 17.7142868 132.64680481 -56.65464783 21.87722015 139.35319519
		 -56.055088043 19.26023102 139.35321045 -56.055084229 19.26023483 132.64680481 -56.65464401 21.87722015 132.64680481
		 40.69884109 40.000026702881 -155.11117554 38.81987762 38.82155991 -154.19248962 38.041576385 38.33343124 -151.9744873
		 38.81987381 38.82156754 -149.75657654 40.69882202 40.000003814697 -148.83786011 42.57778168 41.17851257 -149.75657654
		 43.35607147 41.66664124 -151.97453308 42.57778549 41.17850113 -154.19245911 39.26737213 42.28232574 -155.11117554
		 37.38842773 41.10385513 -154.19248962 36.61011124 40.61574173 -151.9744873 37.3884201 41.10386658 -149.75657654
		 39.2673645 42.282341 -148.83789063 41.14633179 43.46083832 -149.75660706 41.92461395 43.94896317 -151.97450256
		 41.14632416 43.4608345 -154.19248962 33.4121933 51.61779404 -155.11112976 31.53321648 50.43935013 -154.19247437
		 30.75494766 49.95121765 -151.97450256 31.53323174 50.43935013 -149.75656128;
	setAttr ".vt[332:497]" 33.41216278 51.61783981 -148.83787537 35.2911377 52.79629898 -149.75656128
		 36.06942749 53.28442764 -151.97451782 35.2911377 52.79628754 -154.19244385 32.00806427 53.85652924 -155.11116028
		 30.12911034 52.67806244 -154.19247437 29.35083199 52.18989182 -151.97447205 30.12911034 52.67806244 -149.75656128
		 32.0080566406 53.85652924 -148.83787537 33.88702393 55.035003662 -149.75656128 34.66530991 55.52313614 -151.97451782
		 33.88702393 55.035030365 -154.19247437 29.24106598 50.8389473 -148.043167114 35.90217972 55.016765594 -148.043136597
		 29.24106979 50.83895111 -155.90592957 35.90217209 55.016788483 -155.90596008 54.5196991 21.59993744 -153.90634155
		 52.88312149 20.57347107 -154.70655823 51.2465477 19.54700851 -153.90637207 50.56864929 19.12183762 -151.9744873
		 51.2465477 19.5470047 -150.042663574 52.88312531 20.57347488 -149.24250793 54.5196991 21.59993744 -150.042694092
		 55.19760132 22.025112152 -151.9744873 34.36645508 53.73210144 -153.90634155 32.72986221 52.70567322 -154.70655823
		 31.093313217 51.67921448 -153.90634155 30.41540527 51.25401688 -151.9744873 31.093305588 51.67921448 -150.042694092
		 32.72988129 52.70568848 -149.24247742 34.36646271 53.73213959 -150.042694092 35.04434967 54.15730286 -151.97451782
		 40.69882965 40.000022888184 -158.98306274 36.50046921 37.36684418 -156.93031311 34.76145172 36.27613068 -151.97451782
		 36.50046158 37.36684799 -147.018722534 40.69881821 40.000026702881 -144.96600342
		 44.89717102 42.63321686 -147.018707275 46.63618851 43.72396088 -151.9745636 44.89718246 42.63323593 -156.93035889
		 39.26735687 42.28232193 -158.98309326 35.069015503 39.6491127 -156.93031311 33.3299942 38.55844116 -151.97451782
		 35.069007874 39.64915085 -147.018722534 39.26737213 42.28233719 -144.9659729 43.46572113 44.91553879 -147.018737793
		 45.2047348 46.0062217712 -151.97453308 43.4657135 44.91550446 -156.93032837 33.41217804 51.61779785 -158.98304749
		 29.21380806 48.98463821 -156.93034363 27.47482109 47.89389801 -151.9744873 29.21383286 48.98461151 -147.018722534
		 33.41217804 51.61783981 -144.96598816 37.61052704 54.25101089 -147.018722534 39.34954834 55.34172821 -151.97454834
		 37.61051941 54.25102997 -156.93034363 32.0080795288 53.85653687 -158.98304749 27.80970955 51.22335434 -156.93032837
		 26.070695877 50.13262939 -151.97447205 27.80971336 51.22335434 -147.018707275 32.0080718994 53.85652924 -144.96595764
		 36.20642853 56.48974228 -147.018722534 37.94542694 57.58046341 -151.97454834 36.20640564 56.48970413 -156.93034363
		 48.35236359 21.48641968 -148.62129211 54.0337677 25.049762726 -148.62130737 48.35234833 21.48641586 -155.32771301
		 54.033752441 25.049804688 -155.32772827 25.97711563 56.042945862 -148.043136597 26.52030945 58.4139061 -148.043136597
		 25.97711563 56.042964935 -155.90589905 26.52030563 58.41389084 -155.90592957 30.26727104 60.76402664 -148.043167114
		 32.63822937 60.22078705 -148.043151855 32.63822174 60.22079086 -155.90594482 30.26727104 60.76399231 -155.90596008
		 53.5240097 16.8137207 -148.62132263 50.90700531 17.41327286 -148.62130737 50.9070015 17.41327667 -155.32772827
		 53.52398682 16.8137207 -155.32771301 56.58841705 20.97663116 -148.62132263 55.98884964 18.35964584 -148.62130737
		 55.98884583 18.35964966 -155.32772827 56.58840942 20.9766655 -155.32771301 40.69883728 40.000026702881 -125.26464844
		 38.81988144 38.82155991 -124.34596252 38.0415802 38.33343124 -122.12796021 38.81987762 38.82156754 -119.91003418
		 40.69882202 40.000034332275 -118.99136353 42.57778168 41.17851257 -119.91003418 43.35607147 41.66664124 -122.12800598
		 42.57778931 41.17850113 -124.34593201 39.26737213 42.28232574 -125.26463318 37.3884201 41.10385513 -124.34594727
		 36.61011505 40.61574173 -122.12796021 37.38841248 41.10386658 -119.91003418 39.2673645 42.282341 -118.99134827
		 41.14633179 43.46083832 -119.91007996 41.92461395 43.94896317 -122.12797546 41.14632416 43.46080399 -124.34596252
		 33.41218567 51.61782455 -125.26460266 31.53322029 50.43935013 -124.34594727 30.75494385 49.95121765 -122.12799072
		 31.53322792 50.43935013 -119.91003418 33.41217041 51.61783981 -118.99134827 35.2911377 52.79629898 -119.91003418
		 36.06942749 53.28442764 -122.12800598 35.2911377 52.79628754 -124.34591675 32.00806427 53.85652924 -125.26461792
		 30.12910652 52.67806244 -124.34594727 29.35082817 52.18989182 -122.12796021 30.12911034 52.67806244 -119.91001892
		 32.00806427 53.85652924 -118.99134827 33.88702011 55.035003662 -119.91001892 34.66530991 55.52313614 -122.12799072
		 33.8870163 55.034999847 -124.34591675 29.24106216 50.8389473 -118.19664001 35.90217209 55.016765594 -118.19664001
		 29.24106598 50.83895111 -126.059402466 35.9021759 55.016788483 -126.059463501 54.5196991 21.59993744 -124.059814453
		 52.88312531 20.57347107 -124.86003113 51.24654388 19.54700851 -124.059844971 50.56864166 19.12183762 -122.12796021
		 51.24655151 19.5470047 -120.19613647 52.88311768 20.57347488 -119.39598083 54.5196991 21.59993744 -120.19616699
		 55.19759369 22.025112152 -122.12796021 34.36645508 53.73213196 -124.059844971 32.72986984 52.70567322 -124.86003113
		 31.093309402 51.67921448 -124.059814453 30.41540146 51.25404739 -122.12797546 31.093301773 51.67921448 -120.19616699
		 32.72987747 52.70565796 -119.39595032 34.36646271 53.73213959 -120.19613647 35.044345856 54.15730286 -122.12799072
		 40.69882584 40.000022888184 -129.13653564 36.50046539 37.36684418 -127.083786011
		 34.76145172 36.27613068 -122.12799072 36.50046539 37.36684799 -117.17221069 40.69882202 40.000026702881 -115.11946106
		 44.89717102 42.63324738 -117.17219543 46.63618469 43.72396088 -122.1280365 44.89717865 42.63323593 -127.08380127
		 39.26735687 42.28232193 -129.13656616 35.069015503 39.6491127 -127.083786011 33.33000183 38.55847168 -122.12799072
		 35.069007874 39.64915085 -117.17219543 39.26737213 42.28233719 -115.1194458 43.46572113 44.91553879 -117.17221069
		 45.2047348 46.0062217712 -122.12800598 43.4657135 44.91553497 -127.08380127 33.41217804 51.61779785 -129.13652039
		 29.2138195 48.98463821 -127.083816528 27.47481728 47.89389801 -122.12796021 29.21383286 48.98461151 -117.17221069
		 33.41217041 51.61783981 -115.11946106 37.61052704 54.25101089 -117.17219543 39.34954834 55.34172821 -122.12802124
		 37.61052704 54.25099945 -127.083786011 32.0080718994 53.85656738 -129.1365509 27.80971718 51.22335434 -127.083831787
		 26.070703506 50.13262939 -122.12794495 27.80970955 51.22332382 -117.17221069 32.0080718994 53.85652924 -115.11941528
		 36.2064209 56.48971176 -117.17214966;
	setAttr ".vt[498:623]" 37.94541931 57.58046341 -122.12802124 36.20641327 56.48973465 -127.083786011
		 48.35235596 21.48641968 -118.77476501 54.0337677 25.049762726 -118.77479553 48.3523407 21.48638535 -125.4811554
		 54.033752441 25.049804688 -125.48120117 25.97711945 56.042945862 -118.19664001 26.52030563 58.4139061 -118.1966095
		 25.97712326 56.042964935 -126.059402466 26.52030182 58.41389084 -126.059432983 30.26727104 60.76399612 -118.19664001
		 32.63822937 60.22078705 -118.19662476 32.63822174 60.22079086 -126.059417725 30.26727104 60.76399231 -126.059432983
		 53.52400589 16.8137207 -118.77479553 50.90700912 17.41327286 -118.77476501 50.90701294 17.41328049 -125.48120117
		 53.52399063 16.8137207 -125.48117065 56.58841705 20.97663116 -118.77478027 55.98885345 18.35964584 -118.77478027
		 55.98884583 18.35964966 -125.48120117 56.58840942 20.9766655 -125.48117065 40.76507568 40.90059662 132.86335754
		 38.8861084 39.72212219 133.78204346 38.1078186 39.2339859 136.000015258789 38.88610077 39.72213364 138.21794128
		 40.76506805 40.90060425 139.13664246 42.64402008 42.079086304 138.21794128 43.42230988 42.5672226 136
		 42.64403534 42.079071045 133.78204346 39.33361053 43.1829071 132.86334229 37.45465851 42.0044212341 133.78205872
		 36.67636108 41.5163002 136.000015258789 37.45465088 42.0044364929 138.21794128 39.33360291 43.18291855 139.13665771
		 41.21257019 44.36138535 138.21792603 41.99085999 44.84952545 136 41.21257019 44.36138153 133.78204346
		 33.47842407 52.51838303 132.8633728 31.59945869 51.33991241 133.78205872 30.82117653 50.85176849 136.000015258789
		 31.59946251 51.3399086 138.21795654 33.47840881 52.5184021 139.13665771 35.35736465 53.69686127 138.21795654
		 36.13565445 54.18499374 136 35.35737228 53.69685364 133.78205872 32.074306488 54.75710297 132.86335754
		 30.19534492 53.57862473 133.78205872 29.41706085 53.09047699 136.000015258789 30.19534492 53.57862473 138.21795654
		 32.074306488 54.75709915 139.13665771 33.95325089 55.93556976 138.21795654 34.73153687 56.42370987 136.000015258789
		 33.9532547 55.93556595 133.78205872 29.30729485 51.7395134 139.93135071 35.96840668 55.91734314 139.93136597
		 29.30729866 51.7395134 132.068572998 35.96841431 55.91733932 132.068572998 54.58592987 22.50049591 134.06817627
		 52.94936371 21.4740448 133.26795959 51.31277466 20.44757843 134.068161011 50.63487625 20.022399902 136.000015258789
		 51.31277466 20.44757462 137.93185425 52.94935226 21.47404861 138.73200989 54.5859375 22.50050735 137.93182373
		 55.26382828 22.92567444 136.000015258789 34.43270111 54.63268661 134.068161011 32.79611206 53.6062355 133.26797485
		 31.15954018 52.57977676 134.06817627 30.48163795 52.15460587 136 31.15953636 52.57977676 137.93183899
		 32.79611206 53.60624695 138.73204041 34.43269348 54.63269424 137.93183899 35.11058044 55.057857513 136
		 40.76506042 40.9005928 128.99145508 36.5667038 38.26740265 131.044189453 34.8276825 37.17669678 136.000015258789
		 36.56669617 38.26739883 140.95579529 40.7650528 40.9005928 143.0085449219 44.96341705 43.53380585 140.95579529
		 46.7024231 44.62450027 135.99998474 44.96341705 43.53379059 131.044189453 39.33360291 43.18289948 128.99142456
		 35.13525391 40.54970932 131.044189453 33.39622498 39.45900726 136.000015258789 35.13524628 40.54971695 140.95579529
		 39.33360291 43.18291473 143.0085601807 43.5319519 45.81609344 140.95576477 45.27096558 46.90680695 136
		 43.5319519 45.81609726 131.044189453 33.47842407 52.51838684 128.99147034 29.28004837 49.88518524 131.044189453
		 27.54105186 48.79447937 136.000015258789 29.28006744 49.88518143 140.95579529 33.47840881 52.51839828 143.0085449219
		 37.67677307 55.15158081 140.95579529 39.41578674 56.24229431 135.99996948 37.67676544 55.15158081 131.044189453
		 32.074306488 54.75710678 128.99145508 27.87595177 52.123909 131.044189453 26.13693619 51.033195496 136.000030517578
		 27.87594414 52.12390518 140.95579529 32.074302673 54.75709534 143.0085601807 36.2726593 57.39029312 140.95581055
		 38.011672974 58.48101044 136 36.27264023 57.39027786 131.044189453 48.41859818 22.38697052 139.35321045
		 54.099994659 25.95034027 139.35321045 48.41859436 22.38697815 132.64680481 54.09998703 25.95035172 132.64680481
		 26.043346405 56.94352722 139.93136597 26.58654022 59.31446838 139.93136597 26.043357849 56.94352341 132.068603516
		 26.58653831 59.31445694 132.068572998 30.33350563 61.66456604 139.93136597 32.70446014 61.12135696 139.93136597
		 32.70446014 61.12136841 132.068572998 30.33350945 61.66456985 132.068572998 53.59024048 17.71428299 139.35321045
		 50.97325134 18.3138504 139.35321045 50.97324371 18.31385803 132.64680481 53.59023285 17.7142868 132.64680481
		 56.65464783 21.87722015 139.35319519 56.055088043 19.26023102 139.35321045 56.055084229 19.26023483 132.64680481
		 56.65464401 21.87722015 132.64680481;
	setAttr -s 1128 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 0 0 8 9 1
		 9 10 1 10 11 1 11 12 1 12 13 1 13 14 1 14 15 1 15 8 1 0 8 1 1 9 1 2 10 1 3 11 1 4 12 1
		 5 13 1 6 14 1 7 15 1 3 5 1 2 6 1 1 7 1 16 17 1 17 18 1 18 19 1 19 20 1 20 21 1 21 22 1
		 22 23 1 23 16 1 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0 29 30 0 30 31 0 31 24 0 16 24 1
		 17 25 1 18 26 1 19 27 1 20 28 1 21 29 1 22 30 1 23 31 1 26 30 1 27 29 1 25 31 1 9 17 1
		 10 18 1 11 19 1 12 20 1 13 21 1 14 22 1 15 23 1 8 16 1 32 33 0 34 35 0 32 88 0 33 93 0
		 34 32 0 35 33 0 36 37 0 37 38 0 38 39 0 39 40 0 40 41 0 41 42 0 42 43 0 43 36 0 44 45 0
		 45 46 0 46 47 0 47 48 0 48 49 0 49 50 0 50 51 0 51 44 0 36 44 1 37 45 1 38 46 1 39 47 1
		 40 48 1 41 49 1 42 50 1 43 51 1 52 53 0 53 54 0 54 55 0 55 56 0 56 57 0 57 58 0 58 59 0
		 59 52 0 60 61 1 61 62 1 62 63 1 63 64 1 64 65 1 65 66 1 66 67 1 67 60 1 52 60 1 53 61 1
		 54 62 1 55 63 1 56 64 1 57 65 1 58 66 1 59 67 1 55 57 1 54 58 1 53 59 1 68 69 1 69 70 1
		 70 71 1 71 72 1 72 73 1 73 74 1 74 75 1 75 68 1 76 77 0 77 78 0 78 79 0 79 80 0 80 81 0
		 81 82 0 82 83 0 83 76 0 68 76 1 69 77 1 70 78 1 71 79 1 72 80 1 73 81 1 74 82 1 75 83 1
		 78 82 1 79 81 1 77 83 1 84 85 0 86 87 0 84 86 0 85 87 0 86 98 0 87 103 0 89 92 0
		 89 88 0 90 34 0 91 95 0 91 90 0 93 92 0 94 35 0 95 94 0 88 90 1 91 89 1 92 95 1 94 93 1
		 96 101 0 97 84 0;
	setAttr ".ed[166:331]" 96 97 0 99 102 0 99 98 0 100 85 0 101 100 0 103 102 0
		 96 99 1 98 97 1 100 103 1 102 101 1 88 93 1 97 100 1 98 103 1 90 94 1 61 69 1 62 70 1
		 63 71 1 64 72 1 65 73 1 66 74 1 67 75 1 60 68 1 104 105 0 105 106 0 106 107 0 107 108 0
		 108 109 0 109 110 0 110 111 0 111 104 0 112 113 1 113 114 1 114 115 1 115 116 1 116 117 1
		 117 118 1 118 119 1 119 112 1 104 112 1 105 113 1 106 114 1 107 115 1 108 116 1 109 117 1
		 110 118 1 111 119 1 107 109 1 106 110 1 105 111 1 120 121 1 121 122 1 122 123 1 123 124 1
		 124 125 1 125 126 1 126 127 1 127 120 1 128 129 0 129 130 0 130 131 0 131 132 0 132 133 0
		 133 134 0 134 135 0 135 128 0 120 128 1 121 129 1 122 130 1 123 131 1 124 132 1 125 133 1
		 126 134 1 127 135 1 130 134 1 131 133 1 129 135 1 113 121 1 114 122 1 115 123 1 116 124 1
		 117 125 1 118 126 1 119 127 1 112 120 1 136 137 0 138 139 0 136 192 0 137 197 0 138 136 0
		 139 137 0 140 141 0 141 142 0 142 143 0 143 144 0 144 145 0 145 146 0 146 147 0 147 140 0
		 148 149 0 149 150 0 150 151 0 151 152 0 152 153 0 153 154 0 154 155 0 155 148 0 140 148 1
		 141 149 1 142 150 1 143 151 1 144 152 1 145 153 1 146 154 1 147 155 1 156 157 0 157 158 0
		 158 159 0 159 160 0 160 161 0 161 162 0 162 163 0 163 156 0 164 165 1 165 166 1 166 167 1
		 167 168 1 168 169 1 169 170 1 170 171 1 171 164 1 156 164 1 157 165 1 158 166 1 159 167 1
		 160 168 1 161 169 1 162 170 1 163 171 1 159 161 1 158 162 1 157 163 1 172 173 1 173 174 1
		 174 175 1 175 176 1 176 177 1 177 178 1 178 179 1 179 172 1 180 181 0 181 182 0 182 183 0
		 183 184 0 184 185 0 185 186 0 186 187 0 187 180 0 172 180 1 173 181 1 174 182 1 175 183 1
		 176 184 1 177 185 1 178 186 1 179 187 1 182 186 1;
	setAttr ".ed[332:497]" 183 185 1 181 187 1 188 189 0 190 191 0 188 190 0 189 191 0
		 190 202 0 191 207 0 193 196 0 193 192 0 194 138 0 195 199 0 195 194 0 197 196 0 198 139 0
		 199 198 0 192 194 1 195 193 1 196 199 1 198 197 1 200 205 0 201 188 0 200 201 0 203 206 0
		 203 202 0 204 189 0 205 204 0 207 206 0 200 203 1 202 201 1 204 207 1 206 205 1 192 197 1
		 201 204 1 202 207 1 194 198 1 165 173 1 166 174 1 167 175 1 168 176 1 169 177 1 170 178 1
		 171 179 1 164 172 1 208 209 0 209 210 0 210 211 0 211 212 0 212 213 0 213 214 0 214 215 0
		 215 208 0 216 217 1 217 218 1 218 219 1 219 220 1 220 221 1 221 222 1 222 223 1 223 216 1
		 208 216 1 209 217 1 210 218 1 211 219 1 212 220 1 213 221 1 214 222 1 215 223 1 211 213 1
		 210 214 1 209 215 1 224 225 1 225 226 1 226 227 1 227 228 1 228 229 1 229 230 1 230 231 1
		 231 224 1 232 233 0 233 234 0 234 235 0 235 236 0 236 237 0 237 238 0 238 239 0 239 232 0
		 224 232 1 225 233 1 226 234 1 227 235 1 228 236 1 229 237 1 230 238 1 231 239 1 234 238 1
		 235 237 1 233 239 1 217 225 1 218 226 1 219 227 1 220 228 1 221 229 1 222 230 1 223 231 1
		 216 224 1 240 241 0 242 243 0 240 296 0 241 301 0 242 240 0 243 241 0 244 245 0 245 246 0
		 246 247 0 247 248 0 248 249 0 249 250 0 250 251 0 251 244 0 252 253 0 253 254 0 254 255 0
		 255 256 0 256 257 0 257 258 0 258 259 0 259 252 0 244 252 1 245 253 1 246 254 1 247 255 1
		 248 256 1 249 257 1 250 258 1 251 259 1 260 261 0 261 262 0 262 263 0 263 264 0 264 265 0
		 265 266 0 266 267 0 267 260 0 268 269 1 269 270 1 270 271 1 271 272 1 272 273 1 273 274 1
		 274 275 1 275 268 1 260 268 1 261 269 1 262 270 1 263 271 1 264 272 1 265 273 1 266 274 1
		 267 275 1 263 265 1 262 266 1 261 267 1 276 277 1 277 278 1 278 279 1;
	setAttr ".ed[498:663]" 279 280 1 280 281 1 281 282 1 282 283 1 283 276 1 284 285 0
		 285 286 0 286 287 0 287 288 0 288 289 0 289 290 0 290 291 0 291 284 0 276 284 1 277 285 1
		 278 286 1 279 287 1 280 288 1 281 289 1 282 290 1 283 291 1 286 290 1 287 289 1 285 291 1
		 292 293 0 294 295 0 292 294 0 293 295 0 294 306 0 295 311 0 297 300 0 297 296 0 298 242 0
		 299 303 0 299 298 0 301 300 0 302 243 0 303 302 0 296 298 1 299 297 1 300 303 1 302 301 1
		 304 309 0 305 292 0 304 305 0 307 310 0 307 306 0 308 293 0 309 308 0 311 310 0 304 307 1
		 306 305 1 308 311 1 310 309 1 296 301 1 305 308 1 306 311 1 298 302 1 269 277 1 270 278 1
		 271 279 1 272 280 1 273 281 1 274 282 1 275 283 1 268 276 1 312 313 0 313 314 0 314 315 0
		 315 316 0 316 317 0 317 318 0 318 319 0 319 312 0 320 321 1 321 322 1 322 323 1 323 324 1
		 324 325 1 325 326 1 326 327 1 327 320 1 312 320 1 313 321 1 314 322 1 315 323 1 316 324 1
		 317 325 1 318 326 1 319 327 1 315 317 1 314 318 1 313 319 1 328 329 1 329 330 1 330 331 1
		 331 332 1 332 333 1 333 334 1 334 335 1 335 328 1 336 337 0 337 338 0 338 339 0 339 340 0
		 340 341 0 341 342 0 342 343 0 343 336 0 328 336 1 329 337 1 330 338 1 331 339 1 332 340 1
		 333 341 1 334 342 1 335 343 1 338 342 1 339 341 1 337 343 1 321 329 1 322 330 1 323 331 1
		 324 332 1 325 333 1 326 334 1 327 335 1 320 328 1 344 345 0 346 347 0 344 400 0 345 405 0
		 346 344 0 347 345 0 348 349 0 349 350 0 350 351 0 351 352 0 352 353 0 353 354 0 354 355 0
		 355 348 0 356 357 0 357 358 0 358 359 0 359 360 0 360 361 0 361 362 0 362 363 0 363 356 0
		 348 356 1 349 357 1 350 358 1 351 359 1 352 360 1 353 361 1 354 362 1 355 363 1 364 365 0
		 365 366 0 366 367 0 367 368 0 368 369 0 369 370 0 370 371 0 371 364 0;
	setAttr ".ed[664:829]" 372 373 1 373 374 1 374 375 1 375 376 1 376 377 1 377 378 1
		 378 379 1 379 372 1 364 372 1 365 373 1 366 374 1 367 375 1 368 376 1 369 377 1 370 378 1
		 371 379 1 367 369 1 366 370 1 365 371 1 380 381 1 381 382 1 382 383 1 383 384 1 384 385 1
		 385 386 1 386 387 1 387 380 1 388 389 0 389 390 0 390 391 0 391 392 0 392 393 0 393 394 0
		 394 395 0 395 388 0 380 388 1 381 389 1 382 390 1 383 391 1 384 392 1 385 393 1 386 394 1
		 387 395 1 390 394 1 391 393 1 389 395 1 396 397 0 398 399 0 396 398 0 397 399 0 398 410 0
		 399 415 0 401 404 0 401 400 0 402 346 0 403 407 0 403 402 0 405 404 0 406 347 0 407 406 0
		 400 402 1 403 401 1 404 407 1 406 405 1 408 413 0 409 396 0 408 409 0 411 414 0 411 410 0
		 412 397 0 413 412 0 415 414 0 408 411 1 410 409 1 412 415 1 414 413 1 400 405 1 409 412 1
		 410 415 1 402 406 1 373 381 1 374 382 1 375 383 1 376 384 1 377 385 1 378 386 1 379 387 1
		 372 380 1 416 417 0 417 418 0 418 419 0 419 420 0 420 421 0 421 422 0 422 423 0 423 416 0
		 424 425 1 425 426 1 426 427 1 427 428 1 428 429 1 429 430 1 430 431 1 431 424 1 416 424 1
		 417 425 1 418 426 1 419 427 1 420 428 1 421 429 1 422 430 1 423 431 1 419 421 1 418 422 1
		 417 423 1 432 433 1 433 434 1 434 435 1 435 436 1 436 437 1 437 438 1 438 439 1 439 432 1
		 440 441 0 441 442 0 442 443 0 443 444 0 444 445 0 445 446 0 446 447 0 447 440 0 432 440 1
		 433 441 1 434 442 1 435 443 1 436 444 1 437 445 1 438 446 1 439 447 1 442 446 1 443 445 1
		 441 447 1 425 433 1 426 434 1 427 435 1 428 436 1 429 437 1 430 438 1 431 439 1 424 432 1
		 448 449 0 450 451 0 448 504 0 449 509 0 450 448 0 451 449 0 452 453 0 453 454 0 454 455 0
		 455 456 0 456 457 0 457 458 0 458 459 0 459 452 0 460 461 0 461 462 0;
	setAttr ".ed[830:995]" 462 463 0 463 464 0 464 465 0 465 466 0 466 467 0 467 460 0
		 452 460 1 453 461 1 454 462 1 455 463 1 456 464 1 457 465 1 458 466 1 459 467 1 468 469 0
		 469 470 0 470 471 0 471 472 0 472 473 0 473 474 0 474 475 0 475 468 0 476 477 1 477 478 1
		 478 479 1 479 480 1 480 481 1 481 482 1 482 483 1 483 476 1 468 476 1 469 477 1 470 478 1
		 471 479 1 472 480 1 473 481 1 474 482 1 475 483 1 471 473 1 470 474 1 469 475 1 484 485 1
		 485 486 1 486 487 1 487 488 1 488 489 1 489 490 1 490 491 1 491 484 1 492 493 0 493 494 0
		 494 495 0 495 496 0 496 497 0 497 498 0 498 499 0 499 492 0 484 492 1 485 493 1 486 494 1
		 487 495 1 488 496 1 489 497 1 490 498 1 491 499 1 494 498 1 495 497 1 493 499 1 500 501 0
		 502 503 0 500 502 0 501 503 0 502 514 0 503 519 0 505 508 0 505 504 0 506 450 0 507 511 0
		 507 506 0 509 508 0 510 451 0 511 510 0 504 506 1 507 505 1 508 511 1 510 509 1 512 517 0
		 513 500 0 512 513 0 515 518 0 515 514 0 516 501 0 517 516 0 519 518 0 512 515 1 514 513 1
		 516 519 1 518 517 1 504 509 1 513 516 1 514 519 1 506 510 1 477 485 1 478 486 1 479 487 1
		 480 488 1 481 489 1 482 490 1 483 491 1 476 484 1 520 521 0 521 522 0 522 523 0 523 524 0
		 524 525 0 525 526 0 526 527 0 527 520 0 528 529 1 529 530 1 530 531 1 531 532 1 532 533 1
		 533 534 1 534 535 1 535 528 1 520 528 1 521 529 1 522 530 1 523 531 1 524 532 1 525 533 1
		 526 534 1 527 535 1 523 525 1 522 526 1 521 527 1 536 537 1 537 538 1 538 539 1 539 540 1
		 540 541 1 541 542 1 542 543 1 543 536 1 544 545 0 545 546 0 546 547 0 547 548 0 548 549 0
		 549 550 0 550 551 0 551 544 0 536 544 1 537 545 1 538 546 1 539 547 1 540 548 1 541 549 1
		 542 550 1 543 551 1 546 550 1 547 549 1 545 551 1 529 537 1 530 538 1;
	setAttr ".ed[996:1127]" 531 539 1 532 540 1 533 541 1 534 542 1 535 543 1 528 536 1
		 552 553 0 554 555 0 552 608 0 553 613 0 554 552 0 555 553 0 556 557 0 557 558 0 558 559 0
		 559 560 0 560 561 0 561 562 0 562 563 0 563 556 0 564 565 0 565 566 0 566 567 0 567 568 0
		 568 569 0 569 570 0 570 571 0 571 564 0 556 564 1 557 565 1 558 566 1 559 567 1 560 568 1
		 561 569 1 562 570 1 563 571 1 572 573 0 573 574 0 574 575 0 575 576 0 576 577 0 577 578 0
		 578 579 0 579 572 0 580 581 1 581 582 1 582 583 1 583 584 1 584 585 1 585 586 1 586 587 1
		 587 580 1 572 580 1 573 581 1 574 582 1 575 583 1 576 584 1 577 585 1 578 586 1 579 587 1
		 575 577 1 574 578 1 573 579 1 588 589 1 589 590 1 590 591 1 591 592 1 592 593 1 593 594 1
		 594 595 1 595 588 1 596 597 0 597 598 0 598 599 0 599 600 0 600 601 0 601 602 0 602 603 0
		 603 596 0 588 596 1 589 597 1 590 598 1 591 599 1 592 600 1 593 601 1 594 602 1 595 603 1
		 598 602 1 599 601 1 597 603 1 604 605 0 606 607 0 604 606 0 605 607 0 606 618 0 607 623 0
		 609 612 0 609 608 0 610 554 0 611 615 0 611 610 0 613 612 0 614 555 0 615 614 0 608 610 1
		 611 609 1 612 615 1 614 613 1 616 621 0 617 604 0 616 617 0 619 622 0 619 618 0 620 605 0
		 621 620 0 623 622 0 616 619 1 618 617 1 620 623 1 622 621 1 608 613 1 617 620 1 618 623 1
		 610 614 1 581 589 1 582 590 1 583 591 1 584 592 1 585 593 1 586 594 1 587 595 1 580 588 1;
	setAttr -s 492 ".n";
	setAttr ".n[0:165]" -type "float3"  1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20;
	setAttr ".n[166:331]" -type "float3"  1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20;
	setAttr ".n[332:491]" -type "float3"  1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20;
	setAttr -s 546 -ch 2136 ".fc";
	setAttr ".fc[0:499]" -type "polyFaces" 
		f 4 16 8 -18 -1
		f 4 17 9 -19 -2
		f 4 18 10 -20 -3
		f 4 19 11 -21 -4
		f 4 20 12 -22 -5
		f 4 21 13 -23 -6
		f 4 22 14 -24 -7
		f 4 23 15 -17 -8
		f 3 3 4 -25
		f 4 -26 2 24 5
		f 4 6 -27 1 25
		f 3 7 0 26
		f 4 43 35 -45 -28
		f 4 44 36 -46 -29
		f 4 45 37 -47 -30
		f 4 46 38 -48 -31
		f 4 47 39 -49 -32
		f 4 48 40 -50 -33
		f 4 49 41 -51 -34
		f 4 50 42 -44 -35
		f 3 -54 -36 -43
		f 4 -41 -53 -38 51
		f 3 -40 -39 52
		f 4 -42 -52 -37 53
		f 4 54 28 -56 -10
		f 4 55 29 -57 -11
		f 4 56 30 -58 -12
		f 4 57 31 -59 -13
		f 4 58 32 -60 -14
		f 4 59 33 -61 -15
		f 4 60 34 -62 -16
		f 4 61 27 -55 -9
		f 4 -154 152 -158 -177
		f 4 155 -163 -153 -162
		f 4 65 -164 158 67
		f 4 -155 -161 -65 -67
		f 4 84 76 -86 -69
		f 4 85 77 -87 -70
		f 4 86 78 -88 -71
		f 4 87 79 -89 -72
		f 4 88 80 -90 -73
		f 4 89 81 -91 -74
		f 4 90 82 -92 -75
		f 4 91 83 -85 -76
		f 4 108 100 -110 -93
		f 4 109 101 -111 -94
		f 4 110 102 -112 -95
		f 4 111 103 -113 -96
		f 4 112 104 -114 -97
		f 4 113 105 -115 -98
		f 4 114 106 -116 -99
		f 4 115 107 -109 -100
		f 3 95 96 -117
		f 4 -118 94 116 97
		f 4 98 -119 93 117
		f 3 99 92 118
		f 4 135 127 -137 -120
		f 4 136 128 -138 -121
		f 4 137 129 -139 -122
		f 4 138 130 -140 -123
		f 4 139 131 -141 -124
		f 4 140 132 -142 -125
		f 4 141 133 -143 -126
		f 4 142 134 -136 -127
		f 3 -146 -128 -135
		f 4 -133 -145 -130 143
		f 3 -132 -131 144
		f 4 -134 -144 -129 145
		f 4 148 147 -150 -147
		f 4 -169 167 -172 -179
		f 4 164 -176 -168 -173
		f 4 169 149 151 -175
		f 4 -151 -149 -166 -174
		f 4 154 63 -159 -180
		f 4 160 -157 161 153
		f 4 162 159 163 157
		f 4 165 146 -170 -178
		f 4 172 168 173 -167
		f 4 174 171 175 170
		f 4 64 176 -66 -63
		f 4 177 -171 -165 166
		f 4 150 178 -152 -148
		f 4 179 -160 -156 156
		f 4 180 120 -182 -102
		f 4 181 121 -183 -103
		f 4 182 122 -184 -104
		f 4 183 123 -185 -105
		f 4 184 124 -186 -106
		f 4 185 125 -187 -107
		f 4 186 126 -188 -108
		f 4 187 119 -181 -101
		f 4 204 196 -206 -189
		f 4 205 197 -207 -190
		f 4 206 198 -208 -191
		f 4 207 199 -209 -192
		f 4 208 200 -210 -193
		f 4 209 201 -211 -194
		f 4 210 202 -212 -195
		f 4 211 203 -205 -196
		f 3 191 192 -213
		f 4 -214 190 212 193
		f 4 194 -215 189 213
		f 3 195 188 214
		f 4 231 223 -233 -216
		f 4 232 224 -234 -217
		f 4 233 225 -235 -218
		f 4 234 226 -236 -219
		f 4 235 227 -237 -220
		f 4 236 228 -238 -221
		f 4 237 229 -239 -222
		f 4 238 230 -232 -223
		f 3 -242 -224 -231
		f 4 -229 -241 -226 239
		f 3 -228 -227 240
		f 4 -230 -240 -225 241
		f 4 242 216 -244 -198
		f 4 243 217 -245 -199
		f 4 244 218 -246 -200
		f 4 245 219 -247 -201
		f 4 246 220 -248 -202
		f 4 247 221 -249 -203
		f 4 248 222 -250 -204
		f 4 249 215 -243 -197
		f 4 -342 340 -346 -365
		f 4 343 -351 -341 -350
		f 4 253 -352 346 255
		f 4 -343 -349 -253 -255
		f 4 272 264 -274 -257
		f 4 273 265 -275 -258
		f 4 274 266 -276 -259
		f 4 275 267 -277 -260
		f 4 276 268 -278 -261
		f 4 277 269 -279 -262
		f 4 278 270 -280 -263
		f 4 279 271 -273 -264
		f 4 296 288 -298 -281
		f 4 297 289 -299 -282
		f 4 298 290 -300 -283
		f 4 299 291 -301 -284
		f 4 300 292 -302 -285
		f 4 301 293 -303 -286
		f 4 302 294 -304 -287
		f 4 303 295 -297 -288
		f 3 283 284 -305
		f 4 -306 282 304 285
		f 4 286 -307 281 305
		f 3 287 280 306
		f 4 323 315 -325 -308
		f 4 324 316 -326 -309
		f 4 325 317 -327 -310
		f 4 326 318 -328 -311
		f 4 327 319 -329 -312
		f 4 328 320 -330 -313
		f 4 329 321 -331 -314
		f 4 330 322 -324 -315
		f 3 -334 -316 -323
		f 4 -321 -333 -318 331
		f 3 -320 -319 332
		f 4 -322 -332 -317 333
		f 4 336 335 -338 -335
		f 4 -357 355 -360 -367
		f 4 352 -364 -356 -361
		f 4 357 337 339 -363
		f 4 -339 -337 -354 -362
		f 4 342 251 -347 -368
		f 4 348 -345 349 341
		f 4 350 347 351 345
		f 4 353 334 -358 -366
		f 4 360 356 361 -355
		f 4 362 359 363 358
		f 4 252 364 -254 -251
		f 4 365 -359 -353 354
		f 4 338 366 -340 -336
		f 4 367 -348 -344 344
		f 4 368 308 -370 -290
		f 4 369 309 -371 -291
		f 4 370 310 -372 -292
		f 4 371 311 -373 -293
		f 4 372 312 -374 -294
		f 4 373 313 -375 -295
		f 4 374 314 -376 -296
		f 4 375 307 -369 -289
		f 4 392 384 -394 -377
		f 4 393 385 -395 -378
		f 4 394 386 -396 -379
		f 4 395 387 -397 -380
		f 4 396 388 -398 -381
		f 4 397 389 -399 -382
		f 4 398 390 -400 -383
		f 4 399 391 -393 -384
		f 3 379 380 -401
		f 4 -402 378 400 381
		f 4 382 -403 377 401
		f 3 383 376 402
		f 4 419 411 -421 -404
		f 4 420 412 -422 -405
		f 4 421 413 -423 -406
		f 4 422 414 -424 -407
		f 4 423 415 -425 -408
		f 4 424 416 -426 -409
		f 4 425 417 -427 -410
		f 4 426 418 -420 -411
		f 3 -430 -412 -419
		f 4 -417 -429 -414 427
		f 3 -416 -415 428
		f 4 -418 -428 -413 429
		f 4 430 404 -432 -386
		f 4 431 405 -433 -387
		f 4 432 406 -434 -388
		f 4 433 407 -435 -389
		f 4 434 408 -436 -390
		f 4 435 409 -437 -391
		f 4 436 410 -438 -392
		f 4 437 403 -431 -385
		f 4 -530 528 -534 -553
		f 4 531 -539 -529 -538
		f 4 441 -540 534 443
		f 4 -531 -537 -441 -443
		f 4 460 452 -462 -445
		f 4 461 453 -463 -446
		f 4 462 454 -464 -447
		f 4 463 455 -465 -448
		f 4 464 456 -466 -449
		f 4 465 457 -467 -450
		f 4 466 458 -468 -451
		f 4 467 459 -461 -452
		f 4 484 476 -486 -469
		f 4 485 477 -487 -470
		f 4 486 478 -488 -471
		f 4 487 479 -489 -472
		f 4 488 480 -490 -473
		f 4 489 481 -491 -474
		f 4 490 482 -492 -475
		f 4 491 483 -485 -476
		f 3 471 472 -493
		f 4 -494 470 492 473
		f 4 474 -495 469 493
		f 3 475 468 494
		f 4 511 503 -513 -496
		f 4 512 504 -514 -497
		f 4 513 505 -515 -498
		f 4 514 506 -516 -499
		f 4 515 507 -517 -500
		f 4 516 508 -518 -501
		f 4 517 509 -519 -502
		f 4 518 510 -512 -503
		f 3 -522 -504 -511
		f 4 -509 -521 -506 519
		f 3 -508 -507 520
		f 4 -510 -520 -505 521
		f 4 524 523 -526 -523
		f 4 -545 543 -548 -555
		f 4 540 -552 -544 -549
		f 4 545 525 527 -551
		f 4 -527 -525 -542 -550
		f 4 530 439 -535 -556
		f 4 536 -533 537 529
		f 4 538 535 539 533
		f 4 541 522 -546 -554
		f 4 548 544 549 -543
		f 4 550 547 551 546
		f 4 440 552 -442 -439
		f 4 553 -547 -541 542
		f 4 526 554 -528 -524
		f 4 555 -536 -532 532
		f 4 556 496 -558 -478
		f 4 557 497 -559 -479
		f 4 558 498 -560 -480
		f 4 559 499 -561 -481
		f 4 560 500 -562 -482
		f 4 561 501 -563 -483
		f 4 562 502 -564 -484
		f 4 563 495 -557 -477
		f 4 564 581 -573 -581
		f 4 565 582 -574 -582
		f 4 566 583 -575 -583
		f 4 567 584 -576 -584
		f 4 568 585 -577 -585
		f 4 569 586 -578 -586
		f 4 570 587 -579 -587
		f 4 571 580 -580 -588
		f 3 588 -569 -568
		f 4 -570 -589 -567 589
		f 4 -590 -566 590 -571
		f 3 -591 -565 -572
		f 4 591 608 -600 -608
		f 4 592 609 -601 -609
		f 4 593 610 -602 -610
		f 4 594 611 -603 -611
		f 4 595 612 -604 -612
		f 4 596 613 -605 -613
		f 4 597 614 -606 -614
		f 4 598 607 -607 -615
		f 3 606 599 617
		f 4 -616 601 616 604
		f 3 -617 602 603
		f 4 -618 600 615 605
		f 4 573 619 -593 -619
		f 4 574 620 -594 -620
		f 4 575 621 -595 -621
		f 4 576 622 -596 -622
		f 4 577 623 -597 -623
		f 4 578 624 -598 -624
		f 4 579 625 -599 -625
		f 4 572 618 -592 -626
		f 4 740 721 -717 717
		f 4 725 716 726 -720
		f 4 -632 -723 727 -630
		f 4 630 628 724 718
		f 4 632 649 -641 -649
		f 4 633 650 -642 -650
		f 4 634 651 -643 -651
		f 4 635 652 -644 -652
		f 4 636 653 -645 -653
		f 4 637 654 -646 -654
		f 4 638 655 -647 -655
		f 4 639 648 -648 -656
		f 4 656 673 -665 -673
		f 4 657 674 -666 -674
		f 4 658 675 -667 -675
		f 4 659 676 -668 -676
		f 4 660 677 -669 -677
		f 4 661 678 -670 -678
		f 4 662 679 -671 -679
		f 4 663 672 -672 -680
		f 3 680 -661 -660
		f 4 -662 -681 -659 681
		f 4 -682 -658 682 -663
		f 3 -683 -657 -664
		f 4 683 700 -692 -700
		f 4 684 701 -693 -701
		f 4 685 702 -694 -702
		f 4 686 703 -695 -703
		f 4 687 704 -696 -704
		f 4 688 705 -697 -705
		f 4 689 706 -698 -706
		f 4 690 699 -699 -707
		f 3 698 691 709
		f 4 -708 693 708 696
		f 3 -709 694 695
		f 4 -710 692 707 697
		f 4 710 713 -712 -713
		f 4 742 735 -732 732
		f 4 736 731 739 -729
		f 4 738 -716 -714 -734
		f 4 737 729 712 714
		f 4 743 722 -628 -719
		f 4 -718 -726 720 -725
		f 4 -722 -728 -724 -727
		f 4 741 733 -711 -730
		f 4 730 -738 -733 -737
		f 4 -735 -740 -736 -739
		f 4 626 629 -741 -629
		f 4 -731 728 734 -742
		f 4 711 715 -743 -715
		f 4 -721 719 723 -744
		f 4 665 745 -685 -745
		f 4 666 746 -686 -746
		f 4 667 747 -687 -747
		f 4 668 748 -688 -748
		f 4 669 749 -689 -749
		f 4 670 750 -690 -750
		f 4 671 751 -691 -751
		f 4 664 744 -684 -752
		f 4 752 769 -761 -769
		f 4 753 770 -762 -770
		f 4 754 771 -763 -771
		f 4 755 772 -764 -772
		f 4 756 773 -765 -773
		f 4 757 774 -766 -774
		f 4 758 775 -767 -775
		f 4 759 768 -768 -776
		f 3 776 -757 -756
		f 4 -758 -777 -755 777
		f 4 -778 -754 778 -759
		f 3 -779 -753 -760
		f 4 779 796 -788 -796
		f 4 780 797 -789 -797
		f 4 781 798 -790 -798
		f 4 782 799 -791 -799
		f 4 783 800 -792 -800
		f 4 784 801 -793 -801
		f 4 785 802 -794 -802
		f 4 786 795 -795 -803
		f 3 794 787 805
		f 4 -804 789 804 792
		f 3 -805 790 791
		f 4 -806 788 803 793
		f 4 761 807 -781 -807
		f 4 762 808 -782 -808
		f 4 763 809 -783 -809
		f 4 764 810 -784 -810
		f 4 765 811 -785 -811
		f 4 766 812 -786 -812
		f 4 767 813 -787 -813
		f 4 760 806 -780 -814
		f 4 928 909 -905 905
		f 4 913 904 914 -908
		f 4 -820 -911 915 -818
		f 4 818 816 912 906
		f 4 820 837 -829 -837
		f 4 821 838 -830 -838
		f 4 822 839 -831 -839
		f 4 823 840 -832 -840
		f 4 824 841 -833 -841
		f 4 825 842 -834 -842
		f 4 826 843 -835 -843
		f 4 827 836 -836 -844
		f 4 844 861 -853 -861
		f 4 845 862 -854 -862
		f 4 846 863 -855 -863
		f 4 847 864 -856 -864
		f 4 848 865 -857 -865
		f 4 849 866 -858 -866
		f 4 850 867 -859 -867
		f 4 851 860 -860 -868
		f 3 868 -849 -848
		f 4 -850 -869 -847 869
		f 4 -870 -846 870 -851
		f 3 -871 -845 -852
		f 4 871 888 -880 -888
		f 4 872 889 -881 -889
		f 4 873 890 -882 -890
		f 4 874 891 -883 -891
		f 4 875 892 -884 -892
		f 4 876 893 -885 -893
		f 4 877 894 -886 -894
		f 4 878 887 -887 -895
		f 3 886 879 897
		f 4 -896 881 896 884
		f 3 -897 882 883
		f 4 -898 880 895 885
		f 4 898 901 -900 -901
		f 4 930 923 -920 920
		f 4 924 919 927 -917
		f 4 926 -904 -902 -922
		f 4 925 917 900 902
		f 4 931 910 -816 -907
		f 4 -906 -914 908 -913
		f 4 -910 -916 -912 -915
		f 4 929 921 -899 -918
		f 4 918 -926 -921 -925
		f 4 -923 -928 -924 -927
		f 4 814 817 -929 -817
		f 4 -919 916 922 -930
		f 4 899 903 -931 -903
		f 4 -909 907 911 -932
		f 4 853 933 -873 -933
		f 4 854 934 -874 -934
		f 4 855 935 -875 -935
		f 4 856 936 -876 -936
		f 4 857 937 -877 -937
		f 4 858 938 -878 -938
		f 4 859 939 -879 -939
		f 4 852 932 -872 -940
		f 4 940 957 -949 -957
		f 4 941 958 -950 -958
		f 4 942 959 -951 -959
		f 4 943 960 -952 -960
		f 4 944 961 -953 -961
		f 4 945 962 -954 -962
		f 4 946 963 -955 -963
		f 4 947 956 -956 -964
		f 3 964 -945 -944
		f 4 -946 -965 -943 965
		f 4 -966 -942 966 -947
		f 3 -967 -941 -948
		f 4 967 984 -976 -984
		f 4 968 985 -977 -985
		f 4 969 986 -978 -986
		f 4 970 987 -979 -987
		f 4 971 988 -980 -988
		f 4 972 989 -981 -989
		f 4 973 990 -982 -990
		f 4 974 983 -983 -991
		f 3 982 975 993
		f 4 -992 977 992 980
		f 3 -993 978 979
		f 4 -994 976 991 981
		f 4 949 995 -969 -995
		f 4 950 996 -970 -996
		f 4 951 997 -971 -997
		f 4 952 998 -972 -998
		f 4 953 999 -973 -999
		f 4 954 1000 -974 -1000
		f 4 955 1001 -975 -1001
		f 4 948 994 -968 -1002
		f 4 1116 1097 -1093 1093
		f 4 1101 1092 1102 -1096
		f 4 -1008 -1099 1103 -1006
		f 4 1006 1004 1100 1094
		f 4 1008 1025 -1017 -1025
		f 4 1009 1026 -1018 -1026
		f 4 1010 1027 -1019 -1027
		f 4 1011 1028 -1020 -1028
		f 4 1012 1029 -1021 -1029
		f 4 1013 1030 -1022 -1030
		f 4 1014 1031 -1023 -1031
		f 4 1015 1024 -1024 -1032
		f 4 1032 1049 -1041 -1049;
	setAttr ".fc[500:545]"
		f 4 1033 1050 -1042 -1050
		f 4 1034 1051 -1043 -1051
		f 4 1035 1052 -1044 -1052
		f 4 1036 1053 -1045 -1053
		f 4 1037 1054 -1046 -1054
		f 4 1038 1055 -1047 -1055
		f 4 1039 1048 -1048 -1056
		f 3 1056 -1037 -1036
		f 4 -1038 -1057 -1035 1057
		f 4 -1058 -1034 1058 -1039
		f 3 -1059 -1033 -1040
		f 4 1059 1076 -1068 -1076
		f 4 1060 1077 -1069 -1077
		f 4 1061 1078 -1070 -1078
		f 4 1062 1079 -1071 -1079
		f 4 1063 1080 -1072 -1080
		f 4 1064 1081 -1073 -1081
		f 4 1065 1082 -1074 -1082
		f 4 1066 1075 -1075 -1083
		f 3 1074 1067 1085
		f 4 -1084 1069 1084 1072
		f 3 -1085 1070 1071
		f 4 -1086 1068 1083 1073
		f 4 1086 1089 -1088 -1089
		f 4 1118 1111 -1108 1108
		f 4 1112 1107 1115 -1105
		f 4 1114 -1092 -1090 -1110
		f 4 1113 1105 1088 1090
		f 4 1119 1098 -1004 -1095
		f 4 -1094 -1102 1096 -1101
		f 4 -1098 -1104 -1100 -1103
		f 4 1117 1109 -1087 -1106
		f 4 1106 -1114 -1109 -1113
		f 4 -1111 -1116 -1112 -1115
		f 4 1002 1005 -1117 -1005
		f 4 -1107 1104 1110 -1118
		f 4 1087 1091 -1119 -1091
		f 4 -1097 1095 1099 -1120
		f 4 1041 1121 -1061 -1121
		f 4 1042 1122 -1062 -1122
		f 4 1043 1123 -1063 -1123
		f 4 1044 1124 -1064 -1124
		f 4 1045 1125 -1065 -1125
		f 4 1046 1126 -1066 -1126
		f 4 1047 1127 -1067 -1127
		f 4 1040 1120 -1060 -1128;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".vcs" 2;
createNode transform -n "upVectors" -p "Utilities";
	rename -uid "B4B2C808-415F-BEF0-9FF1-349DC628CEE0";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 -1.1368683772161603e-13 ;
	setAttr ".rp" -type "double3" 0 35 0 ;
	setAttr ".sp" -type "double3" 0 35 0 ;
createNode transform -n "l_rearWheel_upvector_pt" -p "upVectors";
	rename -uid "8C8B3135-42FA-7C58-6166-A5AD15EF8C80";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
createNode locator -n "l_rearWheel_upvector_ptShape" -p "l_rearWheel_upvector_pt";
	rename -uid "250FF937-486B-3A4D-2137-8BB45ADFCB33";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 10 10 10 ;
createNode parentConstraint -n "l_rearWheel_upvector_pt_parentConstraint1" -p "l_rearWheel_upvector_pt";
	rename -uid "793C4E78-48D9-6185-8CCE-FEA60CB9BBF4";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_rearWidth_jtW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 10.641914452457126 40.000000000000014 8.5265128291212022e-14 ;
	setAttr ".rst" -type "double3" 30.641914452457126 75.000000000000014 -137.03600000000003 ;
	setAttr -k on ".w0";
createNode transform -n "r_rearWheel_upvector_pt" -p "upVectors";
	rename -uid "4DC881A7-4B0E-BFD5-5356-C3B6266ED7FF";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
createNode locator -n "r_rearWheel_upvector_ptShape" -p "r_rearWheel_upvector_pt";
	rename -uid "570DC727-4F0F-4E61-43DE-799E262B1C5D";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 10 10 10 ;
createNode parentConstraint -n "r_rearWheer_upvector_pt_parentConstraint1" -p "r_rearWheel_upvector_pt";
	rename -uid "0581B476-43E0-EE7B-B407-43AAC7777834";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_rearWidth_jtW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -10.642000000000007 40.000000000000014 8.5265128291212022e-14 ;
	setAttr ".rst" -type "double3" -30.642000000000007 75.000000000000014 -137.03600000000003 ;
	setAttr -k on ".w0";
createNode transform -n "r_frontWheel_upvector_pt" -p "upVectors";
	rename -uid "623C5135-4C53-EA22-9D4C-F199A2ED5AF5";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
createNode locator -n "r_frontWheel_upvector_ptShape" -p "r_frontWheel_upvector_pt";
	rename -uid "E47B1C22-4D3F-DFF5-C9C1-18BAFD642CF4";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 10 10 10 ;
createNode parentConstraint -n "r_frontWheer_upvector_pt_parentConstraint1" -p "r_frontWheel_upvector_pt";
	rename -uid "02EF736A-4360-3D96-9CA9-4BB3C426EDE0";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_frontWidth_jtW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -10.64200000000001 40.000000000000028 1.2207048882828531e-07 ;
	setAttr ".rst" -type "double3" -30.64200000000001 75.000000000000028 121.40300000000006 ;
	setAttr -k on ".w0";
createNode transform -n "l_frontWheel_upvector_pt" -p "upVectors";
	rename -uid "C6B14CBE-4722-BAB3-D890-299948AC39F2";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
createNode locator -n "l_frontWheel_upvector_ptShape" -p "l_frontWheel_upvector_pt";
	rename -uid "A5A48266-4FA4-B59E-F6A9-15A627F13078";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 10 10 10 ;
createNode parentConstraint -n "l_frontWheel_upvector_pt_parentConstraint1" -p "l_frontWheel_upvector_pt";
	rename -uid "B0FBA96C-497C-6E81-A3F9-BEA19E133F10";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_frontWidth_jtW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 10.641914452457129 40.000000000000028 1.2207048882828531e-07 ;
	setAttr ".rst" -type "double3" 30.641914452457129 75.000000000000028 121.40300000000006 ;
	setAttr -k on ".w0";
createNode transform -n "l_frontSpring_upvector_pt" -p "upVectors";
	rename -uid "44A38CCF-4C81-0B72-FDA4-B6862D56AAF9";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
createNode locator -n "l_frontSpring_upvector_ptShape" -p "l_frontSpring_upvector_pt";
	rename -uid "DF1495AD-4AA8-0609-7A5D-7994197F943E";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 10 10 10 ;
createNode parentConstraint -n "l_frontSpring_upvector_pt_parentConstraint1" -p "l_frontSpring_upvector_pt";
	rename -uid "350C3D5F-4F99-ACEB-45F9-6E8205E6E5C7";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_frontWidth_jtW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 33.130000000000024 22.037612433881712 14.597000122070497 ;
	setAttr ".rst" -type "double3" 53.130000000000024 57.037612433881712 136.00000000000006 ;
	setAttr -k on ".w0";
createNode transform -n "r_frontSpring_upvector_pt" -p "upVectors";
	rename -uid "90CB1904-40F1-1E54-A42A-18A335E56DD7";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
createNode locator -n "r_frontSpring_upvector_ptShape" -p "r_frontSpring_upvector_pt";
	rename -uid "053F7BCF-4562-B07B-9CD0-849FBE88C0EB";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 10 10 10 ;
createNode parentConstraint -n "r_frontSpring_upvector_pt_parentConstraint1" -p "r_frontSpring_upvector_pt";
	rename -uid "E08EFD59-4114-C185-5D5D-B2A57AD9C577";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_frontWidth_jtW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -33.130000000000024 22.037612433881712 14.597000122070497 ;
	setAttr ".rst" -type "double3" -53.130000000000024 57.037612433881712 136.00000000000006 ;
	setAttr -k on ".w0";
createNode transform -n "r_rearSpring_upvector_pt" -p "upVectors";
	rename -uid "4BCF7691-4721-E694-A817-6DBC8A20CE77";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
createNode locator -n "r_rearSpring_upvector_ptShape" -p "r_rearSpring_upvector_pt";
	rename -uid "14EA8849-43E8-4E79-58C3-BBA6ABFF1E5A";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 10 10 10 ;
createNode parentConstraint -n "r_rearSpring_upvector_pt_parentConstraint1" -p "r_rearSpring_upvector_pt";
	rename -uid "C964A522-4910-F64F-5025-9EA65DE6A6DC";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_rearWidth_jtW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -33.13000000000001 22.037612433881705 14.597000000000065 ;
	setAttr ".rst" -type "double3" -53.13000000000001 57.037612433881705 -122.43900000000005 ;
	setAttr -k on ".w0";
createNode transform -n "l_rearSpring_upvector_pt" -p "upVectors";
	rename -uid "00F8713D-4CF3-8270-FDA1-18B5D5BACA66";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
createNode locator -n "l_rearSpring_upvector_ptShape" -p "l_rearSpring_upvector_pt";
	rename -uid "76D28A5F-433E-28E4-3540-329F6F6E9521";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 10 10 10 ;
createNode parentConstraint -n "l_rearSpring_upvector_pt_parentConstraint1" -p "l_rearSpring_upvector_pt";
	rename -uid "A0A04AB8-439A-73A6-1FDE-A9A98B469A84";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_rearWidth_jtW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 33.130000000000024 22.037612433881705 14.597000000000065 ;
	setAttr ".rst" -type "double3" 53.130000000000024 57.037612433881705 -122.43900000000005 ;
	setAttr -k on ".w0";
createNode transform -n "GRP_clusters_Wheels" -p "Utilities";
	rename -uid "98BB5E6F-4451-9338-660F-2481E51CF411";
	setAttr ".v" no;
	setAttr ".it" no;
createNode transform -n "CLS_l_frontWheel_top_01" -p "GRP_clusters_Wheels";
	rename -uid "0DFE689D-4862-50E7-470B-26A693BF0548";
	setAttr ".rp" -type "double3" 60.219730377197266 75.5150146484375 161.90821838378906 ;
	setAttr ".sp" -type "double3" 60.219730377197266 75.5150146484375 161.90821838378906 ;
createNode clusterHandle -n "CLS_l_frontWheel_top_01Shape" -p "CLS_l_frontWheel_top_01";
	rename -uid "A9E2AD57-4F08-0617-DD97-4E84566B3B2C";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" 60.219730377197266 75.5150146484375 161.90821838378906 ;
createNode transform -n "CLS_l_frontWheel_top_02" -p "GRP_clusters_Wheels";
	rename -uid "C80889E6-4487-B282-EBA8-7982FCCA7548";
	setAttr ".rp" -type "double3" 60.219730377197266 75.5150146484375 80.915817260742188 ;
	setAttr ".sp" -type "double3" 60.219730377197266 75.5150146484375 80.915817260742188 ;
createNode clusterHandle -n "CLS_l_frontWheel_top_02Shape" -p "CLS_l_frontWheel_top_02";
	rename -uid "C365B303-48B4-D933-BD9A-3BBB7B13FC4C";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" 60.219730377197266 75.5150146484375 80.915817260742188 ;
createNode transform -n "CLS_l_frontWheel_top_03" -p "GRP_clusters_Wheels";
	rename -uid "65931BFD-4DC6-99DC-DACC-D6B2CEDA3748";
	setAttr ".rp" -type "double3" 97.11627197265625 75.5150146484375 80.915817260742188 ;
	setAttr ".sp" -type "double3" 97.11627197265625 75.5150146484375 80.915817260742188 ;
createNode clusterHandle -n "CLS_l_frontWheel_top_03Shape" -p "CLS_l_frontWheel_top_03";
	rename -uid "FEC45ACE-4AE1-1B6F-2CDD-61AF103BE6C2";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" 97.11627197265625 75.5150146484375 80.915817260742188 ;
createNode transform -n "CLS_l_frontWheel_top_04" -p "GRP_clusters_Wheels";
	rename -uid "128BD6BE-4911-9D96-C9D8-E5860BC06DE9";
	setAttr ".rp" -type "double3" 97.11627197265625 75.5150146484375 161.90821838378906 ;
	setAttr ".sp" -type "double3" 97.11627197265625 75.5150146484375 161.90821838378906 ;
createNode clusterHandle -n "CLS_l_frontWheel_top_04Shape" -p "CLS_l_frontWheel_top_04";
	rename -uid "F3B7C684-44FC-8B71-CD40-C4AC46EE593A";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" 97.11627197265625 75.5150146484375 161.90821838378906 ;
createNode transform -n "CLS_l_frontWheel_bot_01" -p "GRP_clusters_Wheels";
	rename -uid "0C0B0E99-4203-624B-8750-52933B8FA5D9";
	setAttr ".rp" -type "double3" 60.219730377197266 -5.4773812294006348 161.90821838378906 ;
	setAttr ".sp" -type "double3" 60.219730377197266 -5.4773812294006348 161.90821838378906 ;
createNode clusterHandle -n "CLS_l_frontWheel_bot_01Shape" -p "CLS_l_frontWheel_bot_01";
	rename -uid "F646D6E8-4320-9255-FFF4-B3AAEB691201";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" 60.219730377197266 -5.4773812294006348 161.90821838378906 ;
createNode transform -n "CLS_l_frontWheel_bot_02" -p "GRP_clusters_Wheels";
	rename -uid "619AAE7E-4DD1-B363-FC66-528735BF9146";
	setAttr ".rp" -type "double3" 60.219730377197266 -5.4773812294006348 80.915817260742188 ;
	setAttr ".sp" -type "double3" 60.219730377197266 -5.4773812294006348 80.915817260742188 ;
createNode clusterHandle -n "CLS_l_frontWheel_bot_02Shape" -p "CLS_l_frontWheel_bot_02";
	rename -uid "CFC85170-42E0-3073-8ED3-0ABF20B088E2";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" 60.219730377197266 -5.4773812294006348 80.915817260742188 ;
createNode transform -n "CLS_l_frontWheel_bot_03" -p "GRP_clusters_Wheels";
	rename -uid "368B0567-46B9-012F-A7AC-D1B197EBD005";
	setAttr ".rp" -type "double3" 97.11627197265625 -5.4773812294006348 80.915817260742188 ;
	setAttr ".sp" -type "double3" 97.11627197265625 -5.4773812294006348 80.915817260742188 ;
createNode clusterHandle -n "CLS_l_frontWheel_bot_03Shape" -p "CLS_l_frontWheel_bot_03";
	rename -uid "7D258F2D-4A1D-AF75-AA92-7B97F152CB56";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" 97.11627197265625 -5.4773812294006348 80.915817260742188 ;
createNode transform -n "CLS_l_frontWheel_bot_04" -p "GRP_clusters_Wheels";
	rename -uid "853AD1EF-413F-FA50-E236-2BA8935DE18D";
	setAttr ".rp" -type "double3" 97.11627197265625 -5.4773812294006348 161.90821838378906 ;
	setAttr ".sp" -type "double3" 97.11627197265625 -5.4773812294006348 161.90821838378906 ;
createNode clusterHandle -n "CLS_l_frontWheel_bot_04Shape" -p "CLS_l_frontWheel_bot_04";
	rename -uid "6B0254C4-4B05-4FC6-46E3-2E8C1B4B4D9D";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" 97.11627197265625 -5.4773812294006348 161.90821838378906 ;
createNode transform -n "CLS_l_rearWheel_top_01" -p "GRP_clusters_Wheels";
	rename -uid "7CE9F9F5-4A55-FAEA-13C1-47BCE5919D19";
	setAttr ".rp" -type "double3" 60.219730377197266 75.514984130859375 -96.530593872070341 ;
	setAttr ".sp" -type "double3" 60.219730377197266 75.514984130859375 -96.530593872070341 ;
createNode clusterHandle -n "CLS_l_rearWheel_top_01Shape" -p "CLS_l_rearWheel_top_01";
	rename -uid "6A1DF3CD-42D3-4250-6DEB-7E8D702639D6";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" 60.219730377197266 75.514984130859375 -96.530593872070341 ;
createNode transform -n "CLS_l_rearWheel_top_02" -p "GRP_clusters_Wheels";
	rename -uid "CEAD31BD-4D40-EBBD-1A9D-6F95F331B46B";
	setAttr ".rp" -type "double3" 60.219730377197266 75.514984130859375 -177.52299499511722 ;
	setAttr ".sp" -type "double3" 60.219730377197266 75.514984130859375 -177.52299499511722 ;
createNode clusterHandle -n "CLS_l_rearWheel_top_02Shape" -p "CLS_l_rearWheel_top_02";
	rename -uid "5F80BF3E-4825-2B41-C56F-45A740A13AE8";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" 60.219730377197266 75.514984130859375 -177.52299499511722 ;
createNode transform -n "CLS_l_rearWheel_top_03" -p "GRP_clusters_Wheels";
	rename -uid "2EEC8993-4063-750F-4F8D-A0A5AE79DB0C";
	setAttr ".rp" -type "double3" 97.11627197265625 75.514984130859375 -177.52299499511722 ;
	setAttr ".sp" -type "double3" 97.11627197265625 75.514984130859375 -177.52299499511722 ;
createNode clusterHandle -n "CLS_l_rearWheel_top_03Shape" -p "CLS_l_rearWheel_top_03";
	rename -uid "54724FC6-4E80-83F3-F95F-24B09BB340B2";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" 97.11627197265625 75.514984130859375 -177.52299499511722 ;
createNode transform -n "CLS_l_rearWheel_top_04" -p "GRP_clusters_Wheels";
	rename -uid "E7C35E56-44F8-D995-98FA-DC9540ECD5BF";
	setAttr ".rp" -type "double3" 97.11627197265625 75.514984130859375 -96.530593872070341 ;
	setAttr ".sp" -type "double3" 97.11627197265625 75.514984130859375 -96.530593872070341 ;
createNode clusterHandle -n "CLS_l_rearWheel_top_04Shape" -p "CLS_l_rearWheel_top_04";
	rename -uid "CC4DEA94-4739-F39C-2A9E-FCAA275011F0";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" 97.11627197265625 75.514984130859375 -96.530593872070341 ;
createNode transform -n "CLS_l_rearWheel_bot_01" -p "GRP_clusters_Wheels";
	rename -uid "27F72D8D-408A-3696-264A-1CA2DC5F85E3";
	setAttr ".rp" -type "double3" 60.219730377197266 -5.4774155616760254 -96.530593872070341 ;
	setAttr ".sp" -type "double3" 60.219730377197266 -5.4774155616760254 -96.530593872070341 ;
createNode clusterHandle -n "CLS_l_rearWheel_bot_01Shape" -p "CLS_l_rearWheel_bot_01";
	rename -uid "154CEAE0-491F-D6A3-967D-2FB008977307";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" 60.219730377197266 -5.4774155616760254 -96.530593872070341 ;
createNode transform -n "CLS_l_rearWheel_bot_02" -p "GRP_clusters_Wheels";
	rename -uid "864B48C8-4DE2-BDFD-D105-FA9D1FA815A4";
	setAttr ".rp" -type "double3" 60.219730377197266 -5.4774155616760254 -177.52299499511722 ;
	setAttr ".sp" -type "double3" 60.219730377197266 -5.4774155616760254 -177.52299499511722 ;
createNode clusterHandle -n "CLS_l_rearWheel_bot_02Shape" -p "CLS_l_rearWheel_bot_02";
	rename -uid "098E69BF-419E-BBC5-DDB2-82987A70BD42";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" 60.219730377197266 -5.4774155616760254 -177.52299499511722 ;
createNode transform -n "CLS_l_rearWheel_bot_03" -p "GRP_clusters_Wheels";
	rename -uid "EB5CF525-4F92-FCA2-54C6-9CA8209FED37";
	setAttr ".rp" -type "double3" 97.11627197265625 -5.4774155616760254 -177.52299499511722 ;
	setAttr ".sp" -type "double3" 97.11627197265625 -5.4774155616760254 -177.52299499511722 ;
createNode clusterHandle -n "CLS_l_rearWheel_bot_03Shape" -p "CLS_l_rearWheel_bot_03";
	rename -uid "6EAF3B0C-402C-5C23-D9AD-61844DBFA06E";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" 97.11627197265625 -5.4774155616760254 -177.52299499511722 ;
createNode transform -n "CLS_l_rearWheel_bot_04" -p "GRP_clusters_Wheels";
	rename -uid "935072BA-455D-08B4-64BF-23B06617C0BA";
	setAttr ".rp" -type "double3" 97.11627197265625 -5.4774155616760254 -96.530593872070341 ;
	setAttr ".sp" -type "double3" 97.11627197265625 -5.4774155616760254 -96.530593872070341 ;
createNode clusterHandle -n "CLS_l_rearWheel_bot_04Shape" -p "CLS_l_rearWheel_bot_04";
	rename -uid "577B8CE7-41B8-99FC-468F-D690B03DF423";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" 97.11627197265625 -5.4774155616760254 -96.530593872070341 ;
createNode transform -n "CLS_r_rearWheel_top_01" -p "GRP_clusters_Wheels";
	rename -uid "F7354BBA-4D20-F490-CE04-45BBC2F16E84";
	setAttr ".rp" -type "double3" -60.219730377197266 75.514984130859375 -96.530593872070341 ;
	setAttr ".sp" -type "double3" -60.219730377197266 75.514984130859375 -96.530593872070341 ;
createNode clusterHandle -n "CLS_r_rearWheel_top_01Shape" -p "CLS_r_rearWheel_top_01";
	rename -uid "D05D000D-4E7E-1A91-615C-3A984D49C3B1";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" -60.219730377197266 75.514984130859375 -96.530593872070341 ;
createNode transform -n "CLS_r_rearWheel_top_02" -p "GRP_clusters_Wheels";
	rename -uid "58C743E8-4C36-916A-C2A3-9D87D84FC411";
	setAttr ".rp" -type "double3" -97.11627197265625 75.514984130859375 -96.530593872070341 ;
	setAttr ".sp" -type "double3" -97.11627197265625 75.514984130859375 -96.530593872070341 ;
createNode clusterHandle -n "CLS_r_rearWheel_top_02Shape" -p "CLS_r_rearWheel_top_02";
	rename -uid "17EB9093-4909-F52E-303C-4EADD39D0BB7";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" -97.11627197265625 75.514984130859375 -96.530593872070341 ;
createNode transform -n "CLS_r_rearWheel_top_03" -p "GRP_clusters_Wheels";
	rename -uid "C3ECC65B-4272-CA53-A4B9-348B1EBA68EF";
	setAttr ".rp" -type "double3" -97.11627197265625 75.514984130859375 -177.52299499511722 ;
	setAttr ".sp" -type "double3" -97.11627197265625 75.514984130859375 -177.52299499511722 ;
createNode clusterHandle -n "CLS_r_rearWheel_top_03Shape" -p "CLS_r_rearWheel_top_03";
	rename -uid "24688F26-4049-13E2-2B18-0CB9C852CD3A";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" -97.11627197265625 75.514984130859375 -177.52299499511722 ;
createNode transform -n "CLS_r_rearWheel_top_04" -p "GRP_clusters_Wheels";
	rename -uid "398F9CFD-436B-6044-603A-7F81F414C773";
	setAttr ".rp" -type "double3" -60.219730377197266 75.514984130859375 -177.52299499511722 ;
	setAttr ".sp" -type "double3" -60.219730377197266 75.514984130859375 -177.52299499511722 ;
createNode clusterHandle -n "CLS_r_rearWheel_top_04Shape" -p "CLS_r_rearWheel_top_04";
	rename -uid "EDDC41B2-4C00-FCD0-310F-77B9EDD3834D";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" -60.219730377197266 75.514984130859375 -177.52299499511722 ;
createNode transform -n "CLS_r_rearWheel_bot_01" -p "GRP_clusters_Wheels";
	rename -uid "0ED4139E-4AA7-407D-269D-FBB9EC8DA796";
	setAttr ".rp" -type "double3" -60.219730377197266 -5.4774155616760254 -96.530593872070341 ;
	setAttr ".sp" -type "double3" -60.219730377197266 -5.4774155616760254 -96.530593872070341 ;
createNode clusterHandle -n "CLS_r_rearWheel_bot_01Shape" -p "CLS_r_rearWheel_bot_01";
	rename -uid "14150198-4BF3-8F2B-7705-B3AD663A9946";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" -60.219730377197266 -5.4774155616760254 -96.530593872070341 ;
createNode transform -n "CLS_r_rearWheel_bot_02" -p "GRP_clusters_Wheels";
	rename -uid "47A4E8BB-4BD2-C06E-A5F2-518150350203";
	setAttr ".rp" -type "double3" -97.11627197265625 -5.4774155616760254 -96.530593872070341 ;
	setAttr ".sp" -type "double3" -97.11627197265625 -5.4774155616760254 -96.530593872070341 ;
createNode clusterHandle -n "CLS_r_rearWheel_bot_02Shape" -p "CLS_r_rearWheel_bot_02";
	rename -uid "6F503F74-488A-E5CD-C77E-F29AB897F9FE";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" -97.11627197265625 -5.4774155616760254 -96.530593872070341 ;
createNode transform -n "CLS_r_rearWheel_bot_03" -p "GRP_clusters_Wheels";
	rename -uid "B4E6276A-47EF-17F9-3E21-8EBA22E14F3B";
	setAttr ".rp" -type "double3" -97.11627197265625 -5.4774155616760254 -177.52299499511722 ;
	setAttr ".sp" -type "double3" -97.11627197265625 -5.4774155616760254 -177.52299499511722 ;
createNode clusterHandle -n "CLS_r_rearWheel_bot_03Shape" -p "CLS_r_rearWheel_bot_03";
	rename -uid "8F8F2169-4AE5-59AA-FFA1-59BFF22BA8C8";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" -97.11627197265625 -5.4774155616760254 -177.52299499511722 ;
createNode transform -n "CLS_r_rearWheel_bot_04" -p "GRP_clusters_Wheels";
	rename -uid "9B454025-437C-077A-0CEB-4294705D97F0";
	setAttr ".rp" -type "double3" -60.219730377197266 -5.4774155616760254 -177.52299499511722 ;
	setAttr ".sp" -type "double3" -60.219730377197266 -5.4774155616760254 -177.52299499511722 ;
createNode clusterHandle -n "CLS_r_rearWheel_bot_04Shape" -p "CLS_r_rearWheel_bot_04";
	rename -uid "7B5D7A1E-4980-D00E-E221-678CEA4D0547";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" -60.219730377197266 -5.4774155616760254 -177.52299499511722 ;
createNode transform -n "CLS_r_frontWheel_top_01" -p "GRP_clusters_Wheels";
	rename -uid "35E85E8F-4088-6925-5280-449D30D10BC9";
	setAttr ".rp" -type "double3" -60.219500000000608 75.514819451332102 161.90802163696293 ;
	setAttr ".sp" -type "double3" -60.219500000000608 75.514819451332102 161.90802163696293 ;
createNode clusterHandle -n "CLS_r_frontWheel_top_01Shape" -p "CLS_r_frontWheel_top_01";
	rename -uid "794D89D8-4563-C004-9018-BE8F0102DD84";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" -60.219500000000608 75.514819451332102 161.90802163696293 ;
createNode transform -n "CLS_r_frontWheel_top_02" -p "GRP_clusters_Wheels";
	rename -uid "59D687D2-4FCA-C7C4-5FED-7DAED3AC78B1";
	setAttr ".rp" -type "double3" -97.116500000000599 75.514819451332102 161.90802163696293 ;
	setAttr ".sp" -type "double3" -97.116500000000599 75.514819451332102 161.90802163696293 ;
createNode clusterHandle -n "CLS_r_frontWheel_top_02Shape" -p "CLS_r_frontWheel_top_02";
	rename -uid "BAB47CE9-45B9-ED41-EACB-248DBFBA7E89";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" -97.116500000000599 75.514819451332102 161.90802163696293 ;
createNode transform -n "CLS_r_frontWheel_top_03" -p "GRP_clusters_Wheels";
	rename -uid "99050AF5-4DCF-2F31-F6DB-749E1504D3C3";
	setAttr ".rp" -type "double3" -97.116500000000599 75.514819451332102 80.91602163696291 ;
	setAttr ".sp" -type "double3" -97.116500000000599 75.514819451332102 80.91602163696291 ;
createNode clusterHandle -n "CLS_r_frontWheel_top_03Shape" -p "CLS_r_frontWheel_top_03";
	rename -uid "DF1A25EB-41F0-6EEC-526A-AA9CB9FD7692";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" -97.116500000000599 75.514819451332102 80.91602163696291 ;
createNode transform -n "CLS_r_frontWheel_top_04" -p "GRP_clusters_Wheels";
	rename -uid "25D1C875-4D78-5B82-C57A-88900C8F5246";
	setAttr ".rp" -type "double3" -60.219500000000608 75.514819451332102 80.91602163696291 ;
	setAttr ".sp" -type "double3" -60.219500000000608 75.514819451332102 80.91602163696291 ;
createNode clusterHandle -n "CLS_r_frontWheel_top_04Shape" -p "CLS_r_frontWheel_top_04";
	rename -uid "8FC498DD-4DDF-7A7D-A4C8-3FAAE403BE5A";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" -60.219500000000608 75.514819451332102 80.91602163696291 ;
createNode transform -n "CLS_r_frontWheel_bot_02" -p "GRP_clusters_Wheels";
	rename -uid "F6A3E497-4FC4-E4EE-14F3-27A83F90D6FC";
	setAttr ".rp" -type "double3" -97.116500000000599 -5.4771805486678957 161.90802163696293 ;
	setAttr ".sp" -type "double3" -97.116500000000599 -5.4771805486678957 161.90802163696293 ;
createNode clusterHandle -n "CLS_r_frontWheel_bot_02Shape" -p "CLS_r_frontWheel_bot_02";
	rename -uid "82C00316-46E8-8007-CF46-2BBA8463F77F";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" -97.116500000000599 -5.4771805486678957 161.90802163696293 ;
createNode transform -n "CLS_r_frontWheel_bot_01" -p "GRP_clusters_Wheels";
	rename -uid "B99B0732-4C91-CC34-FC2E-8B80D7094274";
	setAttr ".rp" -type "double3" -60.219500000000608 -5.4771805486678957 161.90802163696293 ;
	setAttr ".sp" -type "double3" -60.219500000000608 -5.4771805486678957 161.90802163696293 ;
createNode clusterHandle -n "CLS_r_frontWheel_bot_01Shape" -p "CLS_r_frontWheel_bot_01";
	rename -uid "FA32C822-4222-E0FA-4A18-81A5C392B5DA";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" -60.219500000000608 -5.4771805486678957 161.90802163696293 ;
createNode transform -n "CLS_r_frontWheel_bot_03" -p "GRP_clusters_Wheels";
	rename -uid "11DE5250-43EB-7156-9E69-A8BEA1ECDF1A";
	setAttr ".rp" -type "double3" -97.116500000000599 -5.4771805486678957 80.91602163696291 ;
	setAttr ".sp" -type "double3" -97.116500000000599 -5.4771805486678957 80.91602163696291 ;
createNode clusterHandle -n "CLS_r_frontWheel_bot_03Shape" -p "CLS_r_frontWheel_bot_03";
	rename -uid "404751B9-4E52-167C-120C-B8B1AA61CC0C";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" -97.116500000000599 -5.4771805486678957 80.91602163696291 ;
createNode transform -n "CLS_r_frontWheel_bot_04" -p "GRP_clusters_Wheels";
	rename -uid "115ED41E-40A1-69E0-4ECD-88BD6D231D5F";
	setAttr ".rp" -type "double3" -60.219500000000608 -5.4771805486678957 80.91602163696291 ;
	setAttr ".sp" -type "double3" -60.219500000000608 -5.4771805486678957 80.91602163696291 ;
createNode clusterHandle -n "CLS_r_frontWheel_bot_04Shape" -p "CLS_r_frontWheel_bot_04";
	rename -uid "07CC4D72-48BE-C6E9-6489-5A92361F36B5";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".or" -type "double3" -60.219500000000608 -5.4771805486678957 80.91602163696291 ;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "B4FF1D03-44B4-0145-E03B-FBB5B60A62A8";
	setAttr -s 17 ".lnk";
	setAttr -s 9 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "3DCC2779-4CD7-6510-1CC2-94B7834A8446";
	setAttr ".bsdt[0].bscd" -type "Int32Array" 0 ;
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "AE052B11-460F-5116-C1CF-B9A305D84532";
createNode displayLayerManager -n "layerManager";
	rename -uid "0511CF91-486C-0916-068E-C1A3C1D959B3";
createNode displayLayer -n "defaultLayer";
	rename -uid "BAA8D82C-48D1-ECA2-60F0-D8AB8D6FEE15";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "AEF56422-4ADA-0BAE-DB3D-D3943B3C6D71";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "97C024C4-4ADD-F9C9-6FE9-21824E721D76";
	setAttr ".g" yes;
createNode renderSetup -n "renderSetup";
	rename -uid "F56D242B-45DB-FD71-6377-8D95E054C73D";
createNode objectSet -n "ALL_CONTROLS";
	rename -uid "AD952234-4A19-E5E6-7F54-04A1BC7936DB";
	setAttr ".ihi" 0;
	setAttr -s 44 ".dsm";
createNode objectSet -n "All_Rig_Driver_Meshes_RM";
	rename -uid "AC5B6C90-4A2D-1A1F-571C-E0ABA3591B77";
	setAttr ".ihi" 0;
createNode objectSet -n "MSH";
	rename -uid "F1BE59F3-4E3D-92F8-8D7B-0A9793F753B5";
	setAttr ".ihi" 0;
createNode expression -n "car_expression";
	rename -uid "D645A994-4442-B857-0341-7299A58A9103";
	setAttr -k on ".nds";
	setAttr -s 8 ".in[5:7]"  0 0 0;
	setAttr -s 5 ".in";
	setAttr -s 4 ".out";
	setAttr ".ixp" -type "string" (
		"vector $vPos = << .O[0], .O[1], .O[2] >>;\nfloat $distance = 0.0;\nint $direction = 1;\nvector $vPosChange = `getAttr CNT_drive.translate`;\nfloat $FirstFrame = `findKeyframe -time 0 -which first CNT_drive`;\nfloat $cx = $vPosChange.x - $vPos.x;\nfloat $cy = $vPosChange.y - $vPos.y;\nfloat $cz = $vPosChange.z - $vPos.z;\nfloat $distance = sqrt( `pow $cx 2` + `pow $cy 2` + `pow $cz 2` );\nfloat $angle = .I[0]%360;\n\n\nif ( ( $vPosChange.x == $vPos.x ) && ( $vPosChange.y != $vPos.y ) && ( $vPosChange.z == $vPos.z ) ){}\nelse {\n\tif ( $angle == 0 ){ \n\t\tif ( $vPosChange.z > $vPos.z ) $direction = 1;\n\t\telse $direction=-1;}\n\tif ( ( $angle > 0 && $angle <= 90 ) || ( $angle <- 180 && $angle >= -270 ) ){ \n\t\tif ( $vPosChange.x > $vPos.x ) $direction = 1 * $direction;\n\t\telse $direction = -1 * $direction; }\n\tif ( ( $angle > 90 && $angle <= 180 ) || ( $angle < -90 && $angle >= -180 ) ){\n\t\tif ( $vPosChange.z > $vPos.z ) $direction = -1 * $direction;\n\t\telse $direction = 1 * $direction; }\n\tif ( ( $angle > 180 && $angle <= 270 ) || ( $angle < 0 && $angle >= -90 ) ){\n"
		+ "\t\tif ( $vPosChange.x > $vPos.x ) $direction = -1 * $direction;\n\t\telse $direction = 1 * $direction; }\n\tif ( ( $angle > 270 && $angle <= 360 ) || ( $angle < -270 && $angle >= -360 ) ) {\n\t\tif ( $vPosChange.z > $vPos.z ) $direction = 1 * $direction;\n\t\telse $direction = -1 * $direction; }\n\t.O[3] = .O[3] + ( ( $direction * ( ( $distance / ( 6.283185 * .I[1] ) ) * 360.0 ) ) ) ; }\n\n.O[0] = .I[2];\n.O[1] = .I[3];\n.O[2] = .I[4];\n\n\nif ( ( .I[2] == 0) && ( .I[3] == 0) && ( .I[4] == 0) ) {\n    .O[3] = 0;\n\n}");
createNode unitConversion -n "unitConversion507";
	rename -uid "335A225B-4786-95C8-DBD1-85A056BFCA90";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "steerDriveDistance_MD";
	rename -uid "456343D0-4D35-A873-D418-769AA3223145";
	setAttr ".i2" -type "float3" -360 360 1 ;
createNode multiplyDivide -n "steerDriveCircumferenceFraction_MD";
	rename -uid "58366F92-4147-943F-D6D7-A9889CEED4B8";
	setAttr ".op" 2;
createNode multiplyDivide -n "steerDistance_and_invert_MD";
	rename -uid "429EE63B-4F65-FF1D-FC14-A2A59F6CF1E8";
	setAttr ".i2" -type "float3" 219.91147 -1 -1 ;
createNode multiplyDivide -n "steerCircumferenceFraction_MD";
	rename -uid "C0985B8E-4B46-518D-32B5-25A048C38D96";
	setAttr ".op" 2;
	setAttr ".i2" -type "float3" 360 1 1 ;
createNode multiplyDivide -n "steerCircumferenceCalc_MD";
	rename -uid "CFA822F6-49F8-A605-E500-EF9671D3D85E";
	setAttr ".i2" -type "float3" 6.283185 1 1 ;
createNode multiplyDivide -n "wheelCircumferenceCalc_MD";
	rename -uid "654B0ABF-4ED2-5BA7-94E8-C29946DC1160";
	setAttr ".i2" -type "float3" 6.283 1 1 ;
createNode multiplyDivide -n "wheel_MD";
	rename -uid "7D554A89-485C-CF05-8CBC-C2AD506031B1";
	setAttr ".i2" -type "float3" -1 -1 -1 ;
createNode ffd -n "ffd_L_Rear1";
	rename -uid "1EFFFA04-4219-38FE-F342-49BEBEFDB369";
	setAttr -s 2 ".ip";
	setAttr ".ip[1].gtg" -type "string" "";
	setAttr ".ip[2].gtg" -type "string" "";
	setAttr -s 2 ".og";
	setAttr ".lo" yes;
	setAttr ".ot" 1;
	setAttr ".ofd" -3.2489276499486265e-21;
createNode objectSet -n "ffd2Set";
	rename -uid "99F8A1BD-4551-396A-C8B2-23B21875C52A";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".vo" yes;
	setAttr -s 2 ".gn";
createNode groupId -n "groupId38";
	rename -uid "A1BB30AD-423F-ACE1-8EDF-50BD980C0886";
	setAttr ".ihi" 0;
createNode groupId -n "groupId39";
	rename -uid "80EE897F-497A-1CD1-6DC8-83AABDE91B0D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts37";
	rename -uid "166558EB-4FE2-1CAC-1681-1A95E62B6E4B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[1023:1364]";
createNode ffd -n "ffd_R_Front";
	rename -uid "5EE702C9-489E-B101-A901-0BA38BFCA4A5";
	setAttr -s 2 ".ip";
	setAttr ".ip[5].gtg" -type "string" "";
	setAttr ".ip[6].gtg" -type "string" "";
	setAttr -s 2 ".og";
	setAttr ".lo" yes;
	setAttr ".ot" 1;
	setAttr ".ofd" -3.2489276499486265e-21;
createNode objectSet -n "ffd1Set";
	rename -uid "468A2DE9-4165-1125-401B-698BC8D66B4F";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".vo" yes;
	setAttr -s 2 ".gn";
createNode groupParts -n "groupParts16";
	rename -uid "AB1CD8C9-456E-F621-2F82-34997BAD89D9";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 3 "f[648:651]" "f[687]" "f[718]";
createNode groupParts -n "groupParts15";
	rename -uid "F2A53C1E-43D2-CE8C-88C3-D3A4AF97B5BC";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 4 "f[0:647]" "f[652:686]" "f[688:717]" "f[719]";
	setAttr ".irc" -type "componentList" 3 "f[648:651]" "f[687]" "f[718]";
createNode groupId -n "groupId15";
	rename -uid "1AA5C900-41B4-CE9A-6C8C-5C9DC32584FD";
	setAttr ".ihi" 0;
createNode groupId -n "groupId17";
	rename -uid "9CE66FCA-4F13-02C3-30AC-0985A8A0BF90";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts38";
	rename -uid "5AB50B75-4CCF-85FB-DF51-84A10310A735";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[630:839]";
createNode cluster -n "CLS_l_frontWheel_bot_02Cluster";
	rename -uid "E85C34D4-4A15-F730-9722-D7853B2C1A6F";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.992395877838135 0 0
		 0 0 80.992401123046875 0 78.668001174926758 35.018816709518433 121.41201782226562 1;
createNode objectSet -n "cluster16Set";
	rename -uid "6D660C66-47CC-06BF-3A95-76A6A1B0A4A0";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster16GroupId";
	rename -uid "9766AEF1-445F-9CBA-FDBD-CAA9D3F07428";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster16GroupParts";
	rename -uid "F1AC37E4-4204-A3E8-C8D7-2C8876D9595C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[0][0][0]";
createNode cluster -n "CLS_l_frontWheel_bot_03Cluster";
	rename -uid "A95F5143-4093-DF6A-0FB9-6683E83EEFF9";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.992395877838135 0 0
		 0 0 80.992401123046875 0 78.668001174926758 35.018816709518433 121.41201782226562 1;
createNode objectSet -n "cluster15Set";
	rename -uid "5B048576-4202-B704-0825-B28A6390ADE6";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster15GroupId";
	rename -uid "7A54E77B-453C-24D8-E617-3ABB6176E954";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster15GroupParts";
	rename -uid "F4DB67A1-49F4-1B2E-0BA4-01B32D2A507C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[1][0][0]";
createNode cluster -n "CLS_l_frontWheel_bot_01Cluster";
	rename -uid "EE93D415-4B7C-5224-CE69-F0874F49BABE";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.992395877838135 0 0
		 0 0 80.992401123046875 0 78.668001174926758 35.018816709518433 121.41201782226562 1;
createNode objectSet -n "cluster14Set";
	rename -uid "173BF5B2-4BBE-97A9-7615-648EA72D4CED";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster14GroupId";
	rename -uid "FAD213EC-42B3-6CF5-ABB8-D2855CF59594";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster14GroupParts";
	rename -uid "BAA7D338-406F-283D-0ED7-F3A9DB7B0D6A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[0][0][1]";
createNode cluster -n "CLS_l_frontWheel_bot_04Cluster";
	rename -uid "95A1A79E-4F94-7B52-DA0E-FBAB363F5D46";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.992395877838135 0 0
		 0 0 80.992401123046875 0 78.668001174926758 35.018816709518433 121.41201782226562 1;
createNode objectSet -n "cluster13Set";
	rename -uid "6BCE7257-46BE-DB9F-52BC-4F92D044FD35";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster13GroupId";
	rename -uid "4172B5C8-4BA0-19BD-737F-4C9AD45E7B61";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster13GroupParts";
	rename -uid "DB6522C4-4BF8-1C88-BCB7-82B80BA936B9";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[1][0][1]";
createNode cluster -n "CLS_r_frontWheel_top_02Cluster1";
	rename -uid "0A29B33A-4B37-8E1A-8F08-73A4D1C5838D";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.992395877838135 0 0
		 0 0 80.992401123046875 0 78.668001174926758 35.018816709518433 121.41201782226562 1;
createNode objectSet -n "cluster12Set";
	rename -uid "3F3305C9-45F8-BC77-7F71-6B8FE55ED6AD";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster12GroupId";
	rename -uid "957179E0-4AF7-B34F-57A7-428E52288DB4";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster12GroupParts";
	rename -uid "A41CADE0-433A-C78B-AAD5-05ADDC245492";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[1][1][0]";
createNode cluster -n "CLS_r_frontWheel_top_01Cluster1";
	rename -uid "45D04E74-4607-D762-E5F3-9D8CCE653D6C";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.992395877838135 0 0
		 0 0 80.992401123046875 0 78.668001174926758 35.018816709518433 121.41201782226562 1;
createNode objectSet -n "cluster11Set";
	rename -uid "6C546B73-4FD3-55AB-5831-FA9CB81C057D";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster11GroupId";
	rename -uid "886FC0B4-4778-BF16-3FC8-739C1595CB47";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster11GroupParts";
	rename -uid "14F285EA-47D3-1099-FE51-F7998D30295D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[0][1][0]";
createNode cluster -n "CLS_r_frontWheel_top_03Cluster1";
	rename -uid "D5C9CE14-41F4-CB56-C0F8-62B2BD7903A5";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.992395877838135 0 0
		 0 0 80.992401123046875 0 78.668001174926758 35.018816709518433 121.41201782226562 1;
createNode objectSet -n "cluster10Set";
	rename -uid "24E66026-4A2F-5A1E-24AF-31A92A9EE30D";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster10GroupId";
	rename -uid "4A46E153-4878-6F53-6F28-04AAE928DA8C";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster10GroupParts";
	rename -uid "D37ECB1C-47AB-7AF1-FF40-2884B5E17559";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[0][1][1]";
createNode cluster -n "CLS_r_frontWheel_top_04Cluster1";
	rename -uid "978D8D64-4D7F-9C70-A384-DCAF71AAA963";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.992395877838135 0 0
		 0 0 80.992401123046875 0 78.668001174926758 35.018816709518433 121.41201782226562 1;
createNode objectSet -n "cluster9Set";
	rename -uid "55BC6BEE-44D9-31C0-794A-B19BE0201C4B";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster9GroupId";
	rename -uid "50A22359-4800-D86E-9BB2-1E9A5DF93E5C";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster9GroupParts";
	rename -uid "8341A6FB-49CC-8B66-DB38-218887FE718A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[1][1][1]";
createNode unitConversion -n "unitConversion487";
	rename -uid "3AF9EC32-43F5-0A0A-9B02-15B862C562A8";
	setAttr ".cf" 0.017453292519943295;
createNode makeNurbCircle -n "wheelDisplay1";
	rename -uid "5375549B-4734-369A-A653-4A86E924BD27";
createNode unitConversion -n "unitConversion488";
	rename -uid "D8293529-4AA8-773A-6B47-7D9AC38E247D";
	setAttr ".cf" 0.017453292519943295;
createNode makeNurbCircle -n "wheelDisplay3";
	rename -uid "E78DB459-48A2-492E-C74D-6BA35B441304";
createNode ffd -n "ffd_R_Rear";
	rename -uid "D11BA183-432D-29D7-A6B9-18B0BB9F0374";
	setAttr -s 2 ".ip";
	setAttr ".ip[1].gtg" -type "string" "";
	setAttr ".ip[2].gtg" -type "string" "";
	setAttr -s 2 ".og";
	setAttr ".lo" yes;
	setAttr ".ot" 1;
	setAttr ".ofd" 1.2428132117781826e-311;
createNode objectSet -n "ffd4Set";
	rename -uid "E9570F37-4EE4-0D54-8140-8CB955EDBC9D";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".vo" yes;
	setAttr -s 2 ".gn";
createNode groupId -n "groupId42";
	rename -uid "B6AC8F9D-4455-2786-B2F2-6EBDE4729903";
	setAttr ".ihi" 0;
createNode groupId -n "groupId43";
	rename -uid "E829A757-4130-F666-4B0E-878AB67E984F";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts41";
	rename -uid "7CA25070-4137-4F67-5B42-8E8FD9EBB37F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "vtx[0:340]" "vtx[1366]";
createNode ffd -n "ffd_L_Rear";
	rename -uid "CB01A2AF-49D0-58C3-CECA-88AB402C8165";
	setAttr -s 2 ".ip";
	setAttr ".ip[1].gtg" -type "string" "";
	setAttr ".ip[2].gtg" -type "string" "";
	setAttr -s 2 ".og";
	setAttr ".lo" yes;
	setAttr ".ot" 1;
	setAttr ".ofd" 2.7813847636974126e-308;
createNode objectSet -n "ffd3Set";
	rename -uid "81177EF7-4CF9-4A54-3376-10B54CDF3C6B";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".vo" yes;
	setAttr -s 2 ".gn";
createNode groupId -n "groupId40";
	rename -uid "BE7F2B48-4718-5D34-E417-44B3896C2D06";
	setAttr ".ihi" 0;
createNode groupId -n "groupId41";
	rename -uid "5770E61D-4760-9C12-C7ED-96A199804D29";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts39";
	rename -uid "F1F85F11-4CAD-8A36-4B76-D0BEBCA67EA2";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "vtx[682:1022]" "vtx[1367]";
createNode groupParts -n "groupParts40";
	rename -uid "D5DFD6B2-48D6-1643-A423-7BB0EE472852";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[420:629]";
createNode groupParts -n "groupParts42";
	rename -uid "C8905237-468D-F2F9-EA9D-648741400110";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[0:209]";
createNode cluster -n "CLS_r_rearWheel_bot_02Cluster";
	rename -uid "0AE57122-4327-6B58-5A8D-9FBAABFCB79F";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.9923996925354 0 0 0 0 80.992401123046875 0
		 -78.668001174926758 35.018784284591675 -137.02679443359375 1;
createNode objectSet -n "cluster32Set";
	rename -uid "D36B644D-42D9-5314-9CC0-969BF8A0E177";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster32GroupId";
	rename -uid "E093A9F8-4210-1E4C-7007-07B2C55E331E";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster32GroupParts";
	rename -uid "377952C7-4E8A-458C-B850-23A96791C6ED";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[0][0][1]";
createNode cluster -n "CLS_r_rearWheel_top_04Cluster";
	rename -uid "912510BC-4A13-C288-9B88-F1A71D0DFBB0";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.9923996925354 0 0 0 0 80.992401123046875 0
		 -78.668001174926758 35.018784284591675 -137.02679443359375 1;
createNode objectSet -n "cluster31Set";
	rename -uid "6F48361A-403F-B15E-2A58-BDA810304D1A";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster31GroupId";
	rename -uid "F310DD4B-4AB2-B3B1-5D56-CA8BD42E6F18";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster31GroupParts";
	rename -uid "94E6C3A7-4639-004C-9FAD-25B1EE6AF91F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[1][1][0]";
createNode cluster -n "CLS_r_rearWheel_top_03Cluster";
	rename -uid "F3095588-4E30-6AE4-9D11-CCB07EE54E8F";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.9923996925354 0 0 0 0 80.992401123046875 0
		 -78.668001174926758 35.018784284591675 -137.02679443359375 1;
createNode objectSet -n "cluster30Set";
	rename -uid "53334178-4BD5-89FD-DD0F-5A8619A5D615";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster30GroupId";
	rename -uid "F8F8151B-4842-BD1E-90B8-D081BB8BEC7B";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster30GroupParts";
	rename -uid "33ED99BE-4350-9CD6-16F8-2089A693D525";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[0][1][0]";
createNode cluster -n "CLS_r_rearWheel_bot_03Cluster";
	rename -uid "A0AF726C-4A1C-B706-B76D-009F81DAB91F";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.9923996925354 0 0 0 0 80.992401123046875 0
		 -78.668001174926758 35.018784284591675 -137.02679443359375 1;
createNode objectSet -n "cluster29Set";
	rename -uid "B8021F63-4800-B463-565E-26B1E5D7149A";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster29GroupId";
	rename -uid "079B1D1F-4465-AF87-B253-BB9A706C7D97";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster29GroupParts";
	rename -uid "F946BA3F-4CB0-7610-030D-AF92946A38E0";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[0][0][0]";
createNode cluster -n "CLS_r_rearWheel_bot_04Cluster";
	rename -uid "00228229-4266-F5F4-DFF9-2A937AD9F65E";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.9923996925354 0 0 0 0 80.992401123046875 0
		 -78.668001174926758 35.018784284591675 -137.02679443359375 1;
createNode objectSet -n "cluster28Set";
	rename -uid "4E37D154-4D81-84C0-E006-658ABF97C680";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster28GroupId";
	rename -uid "3DAC30AE-49DC-BBDA-2E34-418E8FACB6C9";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster28GroupParts";
	rename -uid "17BAA63B-4474-9BC5-7CC4-0298396390A3";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[1][0][0]";
createNode cluster -n "CLS_r_rearWheel_bot_01Cluster";
	rename -uid "0E3638F2-4888-8B40-07DC-3C8589BA59BD";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.9923996925354 0 0 0 0 80.992401123046875 0
		 -78.668001174926758 35.018784284591675 -137.02679443359375 1;
createNode objectSet -n "cluster27Set";
	rename -uid "20288D68-4350-2548-57D5-88975C6112EA";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster27GroupId";
	rename -uid "51693FD5-42CD-1567-EB1A-C984CCCA52C3";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster27GroupParts";
	rename -uid "786F86FC-4874-A5C9-1E60-D7B420C874A5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[1][0][1]";
createNode cluster -n "CLS_r_rearWheel_top_02Cluster";
	rename -uid "A09DFD15-4925-BEAC-54EF-25AB27354FA6";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.9923996925354 0 0 0 0 80.992401123046875 0
		 -78.668001174926758 35.018784284591675 -137.02679443359375 1;
createNode objectSet -n "cluster26Set";
	rename -uid "E5093994-4CEC-989A-95D1-139FF7F3BFF6";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster26GroupId";
	rename -uid "223432E6-4971-FA34-6C02-F986A2DCCBC4";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster26GroupParts";
	rename -uid "EBD284F3-443A-D7E6-B9C1-BDBC984F4E86";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[0][1][1]";
createNode cluster -n "CLS_r_rearWheel_top_01Cluster";
	rename -uid "D64F652A-4FF8-3213-27AA-568380156C15";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.9923996925354 0 0 0 0 80.992401123046875 0
		 -78.668001174926758 35.018784284591675 -137.02679443359375 1;
createNode objectSet -n "cluster25Set";
	rename -uid "F28FA06F-4128-ECDB-D6E6-BB85F4005C67";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster25GroupId";
	rename -uid "CF5A75F2-4AB8-4D92-E2CC-ACB58FFBFF1F";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster25GroupParts";
	rename -uid "8A5611F4-4A3D-38B8-65CA-D2AA02649B7C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[1][1][1]";
createNode makeNurbCircle -n "wheelDisplay4";
	rename -uid "1D5BB593-4B5C-DAAE-D1D4-819F323CD1F1";
createNode cluster -n "CLS_l_rearWheel_top_02Cluster";
	rename -uid "0A8304EE-4DFF-4DE5-39F5-9EA9DECF200F";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.9923996925354 0 0 0 0 80.992401123046875 0
		 78.668001174926758 35.018784284591675 -137.02679443359375 1;
createNode objectSet -n "cluster24Set";
	rename -uid "914717BC-4536-8AF1-E6C3-AE92270EF1FA";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster24GroupId";
	rename -uid "363E2D58-4A77-B5F2-5DEE-80BE96DA9C42";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster24GroupParts";
	rename -uid "46601947-4FA0-5E18-4BCC-C481B08500C5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[0][1][0]";
createNode cluster -n "CLS_l_rearWheel_top_03Cluster";
	rename -uid "F9D606C2-485A-76AA-9B7F-9E97BE18207E";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.9923996925354 0 0 0 0 80.992401123046875 0
		 78.668001174926758 35.018784284591675 -137.02679443359375 1;
createNode objectSet -n "cluster23Set";
	rename -uid "E6F77428-4F6D-1F10-16C5-149044161246";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster23GroupId";
	rename -uid "A76FCDD4-462B-C279-3BE2-66870B468A78";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster23GroupParts";
	rename -uid "E65A9CB7-484D-6658-6512-DB90219F3DD7";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[1][1][0]";
createNode cluster -n "CLS_l_rearWheel_bot_02Cluster";
	rename -uid "DB335F2C-4A3A-8F3E-88E8-B38EB610A289";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.9923996925354 0 0 0 0 80.992401123046875 0
		 78.668001174926758 35.018784284591675 -137.02679443359375 1;
createNode objectSet -n "cluster22Set";
	rename -uid "52915422-454E-3618-31AF-1599316C4EE0";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster22GroupId";
	rename -uid "482FE742-44BD-0840-AB3F-3F8A07BD7FD8";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster22GroupParts";
	rename -uid "B541980E-4E41-78B1-5428-FD9622C06C11";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[0][0][0]";
createNode cluster -n "CLS_l_rearWheel_bot_03Cluster";
	rename -uid "D15F5C5C-42A8-6DE1-9AED-19AD40F01EA6";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.9923996925354 0 0 0 0 80.992401123046875 0
		 78.668001174926758 35.018784284591675 -137.02679443359375 1;
createNode objectSet -n "cluster21Set";
	rename -uid "63C65C5E-4092-2F52-2C6C-CAB64AD27708";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster21GroupId";
	rename -uid "D527C155-4282-D47A-89AC-E0B416F6F090";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster21GroupParts";
	rename -uid "666B929D-460B-9C35-3708-44AD656AE780";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[1][0][0]";
createNode cluster -n "CLS_l_rearWheel_bot_01Cluster";
	rename -uid "1091A161-4877-E114-C82D-5CB7F96C5E3E";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.9923996925354 0 0 0 0 80.992401123046875 0
		 78.668001174926758 35.018784284591675 -137.02679443359375 1;
createNode objectSet -n "cluster20Set";
	rename -uid "B2A1477F-40A9-CC00-829A-7884C1DB2050";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster20GroupId";
	rename -uid "53AF8BE3-45E7-796D-8BEB-119533D54E20";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster20GroupParts";
	rename -uid "F96D7964-4B76-3DCC-1FD7-4DB3A9EDB1E9";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[0][0][1]";
createNode cluster -n "CLS_l_rearWheel_bot_04Cluster";
	rename -uid "6D3D3F1A-4C9D-C23D-BDCE-15A60416544D";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.9923996925354 0 0 0 0 80.992401123046875 0
		 78.668001174926758 35.018784284591675 -137.02679443359375 1;
createNode objectSet -n "cluster19Set";
	rename -uid "324D7D04-415D-16F3-66CE-E58F90908DA3";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster19GroupId";
	rename -uid "E1E8B04F-41D6-9F09-28DD-67A5483B931C";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster19GroupParts";
	rename -uid "6C3AFBE5-4C73-B91C-1B73-D3B1D26D207B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[1][0][1]";
createNode cluster -n "CLS_l_rearWheel_top_01Cluster";
	rename -uid "B92CA881-4086-ADAC-AA69-40876C3EB790";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.9923996925354 0 0 0 0 80.992401123046875 0
		 78.668001174926758 35.018784284591675 -137.02679443359375 1;
createNode objectSet -n "cluster18Set";
	rename -uid "F579129A-4A71-D98F-5541-EABF668EA7D2";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster18GroupId";
	rename -uid "BF28CD0C-4DC9-E97C-AEA4-C5AA4ADD8835";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster18GroupParts";
	rename -uid "FA3E8538-4230-0E90-50FF-92B03AE0D113";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[0][1][1]";
createNode cluster -n "CLS_l_rearWheel_top_04Cluster";
	rename -uid "1E489A37-46A2-5387-F2CF-99B438E76707";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896541595458984 0 0 0 0 80.9923996925354 0 0 0 0 80.992401123046875 0
		 78.668001174926758 35.018784284591675 -137.02679443359375 1;
createNode objectSet -n "cluster17Set";
	rename -uid "2D44DB1B-4233-339B-B4DF-9ABC09A99073";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster17GroupId";
	rename -uid "5D3CC51E-43E8-2839-2E59-EDBBA4A5044B";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster17GroupParts";
	rename -uid "69452568-4333-49F6-5EED-D39A1859BBEF";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[1][1][1]";
createNode makeNurbCircle -n "wheelDisplay2";
	rename -uid "C03808C1-46FC-BF6A-9AF2-EB9760552838";
createNode unitConversion -n "unitConversion502";
	rename -uid "0E846BF2-4562-6932-B692-0B9F68B70FD0";
	setAttr ".cf" 0.017453292519943295;
createNode condition -n "CND_l_frontWheel_spin";
	rename -uid "5D691BE4-4A9B-9AC8-E2B0-97A2678E21F0";
	setAttr ".st" 1;
	setAttr ".cf" -type "float3" 0 0 0 ;
createNode plusMinusAverage -n "l_frontDrive_PMA";
	rename -uid "FF43A7B6-4755-AF0D-64CD-C79C486631C2";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode plusMinusAverage -n "frontWheelSpin_PMA";
	rename -uid "17BA9FEE-4468-F149-272B-26BA252157C1";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode unitConversion -n "unitConversion503";
	rename -uid "6188E26C-4336-B793-D542-3CB106F30865";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion505";
	rename -uid "6793D6C1-4CEB-CF57-4ADF-E587ED1BDBDC";
	setAttr ".cf" 0.017453292519943295;
createNode condition -n "CND_rearWheel_spin";
	rename -uid "321CC7A1-455D-898B-70E1-9293C41C85BB";
	setAttr ".st" 1;
createNode plusMinusAverage -n "rearWheelSpin_PMA";
	rename -uid "6999E84D-4A06-C882-8E77-269D441AA5DA";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode unitConversion -n "unitConversion506";
	rename -uid "260F3125-4A0E-2561-66D4-2BB44CD6CEC6";
	setAttr ".cf" 0.017453292519943295;
createNode plusMinusAverage -n "frontAxleAdjust_PMA";
	rename -uid "BEF88464-437A-98B6-11E2-18BA9E6B9C5C";
	setAttr ".op" 2;
	setAttr -s 2 ".i1[0:1]"  121.40299988 0;
createNode unitConversion -n "unitConversion494";
	rename -uid "C69DC630-4199-FB85-0DE3-219D05866FFC";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "frontArm_MD";
	rename -uid "46E8546F-4CAF-63FD-8D8A-D0B30534766A";
	setAttr ".i2" -type "float3" -1 -1 1 ;
createNode unitConversion -n "unitConversion493";
	rename -uid "8B2F68BD-4DEE-9233-BE41-879029105553";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion495";
	rename -uid "F0C52DA1-475A-71FF-837E-B6806E5DBA86";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion496";
	rename -uid "4AB019B7-45C1-548C-7EBC-9791A2A7B7A6";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion498";
	rename -uid "7FD2D93A-4D0A-9C3E-BD19-5F87A4FBBC44";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "rearArm_MD";
	rename -uid "F5D1AACD-47C1-84DA-49CB-678E4713A139";
	setAttr ".i2" -type "float3" -1 -1 1 ;
createNode unitConversion -n "unitConversion497";
	rename -uid "6D555ECB-4B56-533F-2261-28B6E1BAB5D8";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion499";
	rename -uid "5095E45C-487F-38D0-E532-FBA575CDDCDB";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion500";
	rename -uid "42CE0DAC-4FE3-9004-4DEE-8CBDCE16E2D3";
	setAttr ".cf" 0.017453292519943295;
createNode groupId -n "skinCluster1GroupId";
	rename -uid "DF4A8E58-4724-DB88-8507-23865EACF747";
	setAttr ".ihi" 0;
createNode objectSet -n "SK_chassis_tutorialSet";
	rename -uid "D18941F4-47CB-2F49-1E77-37ABF2965851";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode skinCluster -n "SK_chassis_tutorial";
	rename -uid "63147FCE-4E82-0F3E-2C15-97B4085187CD";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr -s 172 ".wl";
	setAttr ".wl[0:171].w"
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 3 1
		1 2 1
		1 5 1
		1 4 1
		1 3 1
		1 2 1
		1 3 1
		1 2 1
		1 5 1
		1 4 1
		1 5 1
		1 4 1
		1 2 1
		1 3 1
		1 3 1
		1 2 1
		1 3 1
		1 2 1
		1 3 1
		1 2 1
		1 5 1
		1 5 1
		1 4 1
		1 4 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 3 1
		1 2 1
		1 4 1
		1 5 1;
	setAttr -s 6 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".pm[1]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -35 1.1368683772161603e-13 1;
	setAttr ".pm[2]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -20 -35 -121.40299987792956 1;
	setAttr ".pm[3]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 20 -35 -121.40299987792956 1;
	setAttr ".pm[4]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -20 -35 137.03600000000012 1;
	setAttr ".pm[5]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 20 -35 137.03600000000012 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 6 ".ma";
	setAttr -s 6 ".dpf[0:5]"  4 4 4 4 4 4;
	setAttr -s 6 ".lw";
	setAttr -s 6 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 5;
	setAttr ".ucm" yes;
	setAttr -s 6 ".ifcl";
	setAttr -s 6 ".ifcl";
createNode groupParts -n "skinCluster1GroupParts";
	rename -uid "27497D20-481A-C232-B91F-54B0EDB7344B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode materialInfo -n "materialInfo3";
	rename -uid "8BA08105-49FD-9BCF-64ED-11B6188F3FDC";
createNode shadingEngine -n "chassis_tutorial_shdr_SE";
	rename -uid "A6BA6234-4008-25A5-6F66-D8967F2C152D";
	setAttr ".ihi" 0;
	setAttr -s 3 ".dsm";
	setAttr ".ro" yes;
createNode phong -n "chassis_tutorial_shdr";
	rename -uid "CD53FCB7-4665-90C5-47C7-2DACDC71EFD6";
	setAttr ".rdl" 10;
	setAttr ".dc" 1;
	setAttr ".c" -type "float3" 0.64999998 1 0.44999999 ;
	setAttr ".cp" 5;
createNode shadingEngine -n "tire_tutorial_shdr_SE";
	rename -uid "0C2DC4B3-497A-1903-E7DE-7994468B6CAD";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 2 ".gn";
createNode materialInfo -n "materialInfo1";
	rename -uid "F64A96C7-4F0B-A938-E340-93934C3279C9";
createNode phong -n "tire_tutorial_shdr";
	rename -uid "9852A36D-40E2-DC86-13E6-CEB79EAF220D";
	setAttr ".rdl" 10;
	setAttr ".dc" 1;
	setAttr ".c" -type "float3" 0.25 0.25 0.25 ;
	setAttr ".cp" 5;
createNode shadingEngine -n "lambert2SG";
	rename -uid "DAB9037C-4F10-ADE1-5907-1B82C263AE1C";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo5";
	rename -uid "F506A057-4697-ACB5-5603-A1A9D9D13248";
createNode lambert -n "lambert2";
	rename -uid "F57BA77D-4AB0-5232-2A1C-E6A2281058B6";
	setAttr ".c" -type "float3" 1 1 1 ;
createNode groupId -n "groupId16";
	rename -uid "F129FAD0-4DFD-E009-5BC0-C2A76E20C0DA";
	setAttr ".ihi" 0;
createNode materialInfo -n "materialInfo2";
	rename -uid "E2C0C2F6-460E-DAD3-C89A-0487341A2526";
createNode shadingEngine -n "wheels_tutorial_shdr_SE";
	rename -uid "D851FD08-4EDF-391D-7948-70BA816BE698";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode phong -n "wheels_tutorial_shdr";
	rename -uid "8E8C2A32-4657-9F9B-DAA4-93972F7786DD";
	setAttr ".rdl" 10;
	setAttr ".dc" 1;
	setAttr ".c" -type "float3" 0.44999999 0.64999998 1 ;
	setAttr ".cp" 5;
createNode groupId -n "skinCluster4GroupId";
	rename -uid "23907496-46BB-EBC8-B452-A2862519ECDF";
	setAttr ".ihi" 0;
createNode objectSet -n "SK_wheelMount_tutorialSet";
	rename -uid "C91E36AA-482B-E788-1A91-10846008291B";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode skinCluster -n "SK_wheelMount_tutorial";
	rename -uid "4A12A63F-40B6-C66F-BEBA-B49C8109176D";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr -s 202 ".wl";
	setAttr ".wl[0:201].w"
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1;
	setAttr -s 6 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 0 0 0 0 0.99999999988674748 1.5050084283021111e-05 0
		 0 -1.5050084283021111e-05 0.99999999988674748 0 -53.130042686887215 -34.99817287338545 -121.4033452450154 1;
	setAttr ".pm[1]" -type "matrix" 1 0 0 0 0 0.99999999988674748 1.5050084283350017e-05 0
		 0 -1.5050084283350017e-05 0.99999999988674748 0 53.130000000000003 -34.99817287065391 -121.40352673920073 1;
	setAttr ".pm[2]" -type "matrix" 1 0 0 0 0 0.99999999988674748 1.5050084283021111e-05 0
		 0 -1.5050084283021111e-05 0.99999999988674748 0 -53.130042686887258 -35.00206239938597 137.03547323153057 1;
	setAttr ".pm[3]" -type "matrix" 1 0 0 0 0 0.99999999988674748 1.5050084283350017e-05 0
		 0 -1.5050084283350017e-05 0.99999999988674748 0 53.130000000000038 -35.002062399386013 137.03547323153055 1;
	setAttr ".pm[4]" -type "matrix" 1 0 0 0 0 0.99999999988674748 1.5050084283021111e-05 0
		 0 -1.5050084283021111e-05 0.99999999988674748 0 -53.130038229852921 -34.998172873385457 -94.763597625983493 1;
	setAttr ".pm[5]" -type "matrix" 1 0 0 0 0 0.99999999988674748 1.5050084283350017e-05 0
		 0 -1.5050084283350017e-05 0.99999999988674748 0 53.129999999999988 -34.998173803394259 -94.763626736197693 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 6 ".ma";
	setAttr -s 6 ".dpf[0:5]"  4 4 4 4 4 4;
	setAttr -s 6 ".lw";
	setAttr -s 6 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 1;
	setAttr ".ucm" yes;
	setAttr -s 6 ".ifcl";
	setAttr -s 6 ".ifcl";
createNode groupParts -n "skinCluster4GroupParts";
	rename -uid "C0D7C47F-424C-CEA7-BF95-479D835C85C1";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode groupId -n "skinCluster5GroupId";
	rename -uid "A38425C0-442A-15F9-E1EF-569CA99BF82B";
	setAttr ".ihi" 0;
createNode objectSet -n "SK_arms_tutorialSet";
	rename -uid "5A00A329-4A87-6381-3F77-7EA0912CF85E";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode skinCluster -n "SK_arms_tutorial";
	rename -uid "1163B2A1-4AAA-3571-7743-36AADE72F377";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr -s 576 ".wl";
	setAttr ".wl[0:499].w"
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 15 1
		1 15 1
		1 15 1
		1 15 1
		1 15 1
		1 15 1
		1 15 1
		1 15 1
		1 15 1
		1 15 1
		1 15 1
		1 15 1
		1 15 1
		1 15 1
		1 15 1
		1 15 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1;
	setAttr ".wl[500:575].w"
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1
		1 8 1;
	setAttr -s 16 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 30.6419 -26.818999999999999 137.03600000000012 1;
	setAttr ".pm[1]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 53.049900000000001 -21.3245 137.03600000000012 1;
	setAttr ".pm[2]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 30.641999999999999 -35 137.03600000000012 1;
	setAttr ".pm[3]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 30.6419 -57.037599999999998 137.03600000000012 1;
	setAttr ".pm[4]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -30.641999999999999 -34.999965800311998 137.03600000000012 1;
	setAttr ".pm[5]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -30.641914452457129 -57.037624481183663 137.03600000000012 1;
	setAttr ".pm[6]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -30.641864776611328 -26.819000000000003 137.03600000000012 1;
	setAttr ".pm[7]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -53.049942016601562 -21.324457168579105 137.03600000000012 1;
	setAttr ".pm[8]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 30.6419 -26.818999999999999 -121.40299987792956 1;
	setAttr ".pm[9]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 53.049900000000001 -21.3245 -121.40299987792956 1;
	setAttr ".pm[10]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 30.641999999999999 -35 -121.40299987792956 1;
	setAttr ".pm[11]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 30.6419 -57.037599999999998 -121.40299987792956 1;
	setAttr ".pm[12]" -type "matrix" 1 0 0 0 0 0.99999999988674748 1.5050084283021111e-05 0
		 0 -1.5050084283021111e-05 0.99999999988674748 0 -30.641999999999999 -34.998138670967791 -121.40352661661555 1;
	setAttr ".pm[13]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -30.641914452457129 -57.037624481183663 -121.4029998779296 1;
	setAttr ".pm[14]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -30.641864776611328 -26.819000000000003 -121.4029998779296 1;
	setAttr ".pm[15]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -53.049942016601562 -21.324457168579105 -121.4029998779296 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 16 ".ma";
	setAttr -s 16 ".dpf[0:15]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 16 ".lw";
	setAttr -s 16 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 1;
	setAttr ".ucm" yes;
	setAttr -s 16 ".ifcl";
	setAttr -s 16 ".ifcl";
createNode groupParts -n "skinCluster5GroupParts";
	rename -uid "49A1FEE4-44E8-7BF4-65DA-F5A812A19740";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode groupId -n "skinCluster7GroupId";
	rename -uid "7FB39A1E-4A2E-7E85-1F33-E1A51843DF1A";
	setAttr ".ihi" 0;
createNode objectSet -n "SK_springs_tutorialSet";
	rename -uid "DB3E4156-4220-301B-F935-6F83955FEDA6";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode skinCluster -n "SK_springs_tutorial";
	rename -uid "D7896621-435D-3B43-8FAE-69BA9576AE2E";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr -s 972 ".wl";
	setAttr ".wl[0:319].w"
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 0 1
		2 0 0.99999994656113245 1 5.343886755326821e-08
		2 0 0.99999994656169733 1 5.3438302671793281e-08
		2 0 0.99999989312141224 1 1.0687858775781933e-07
		2 0 0.99999994656137758 1 5.3438622416024373e-08
		2 0 0.99999989312276227 1 1.0687723772662139e-07
		1 0 1
		2 0 0.99999983968481843 1 1.6031518157433311e-07
		2 0 5.9604644775390625e-08 1 0.99999994039535522
		2 0 5.9604644775390625e-08 1 0.99999994039535522
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		2 0 0.9999999999961654 1 3.8346392033927756e-12
		2 0 0.99999999999211153 1 7.8884763304865047e-12
		1 0 1
		2 0 0.99999982118606567 1 1.7881393432617188e-07
		2 0 0.99999999999808264 1 1.9173378162928856e-12
		2 0 0.99999926427437913 1 7.3572562087065307e-07
		1 0 1
		1 1 1
		1 1 1
		1 1 1
		2 0 0.05282699167721093 1 0.94717300832278917
		1 1 1
		1 1 1
		2 0 0.12115598196640612 1 0.87884401803359391
		1 1 1
		2 0 0.069555044174194336 1 0.93044495582580566
		2 0 0.19036662632360812 1 0.80963337367639188
		2 0 0.069554649000527199 1 0.93044535099947279
		2 0 0.99999763402388453 1 2.3659761154704029e-06
		1 0 1
		1 0 1
		2 0 0.95551013633774406 1 0.044489863662255984
		1 0 1
		2 0 0.99999880790710449 1 1.1920928955078125e-06
		2 0 0.90516982701052462 1 0.094830172989475353
		1 0 1
		2 0 0.97564895264804363 1 0.024351047351956367
		2 0 0.85483494850008812 1 0.14516505149991188
		2 0 0.97564798283570331 1 0.024352017164296677
		1 0 0.99999999999999989
		2 0 0.92530678957700729 1 0.074693210422992706
		2 0 0.80449657999768887 1 0.19550342000231116
		2 0 0.92530874184802436 1 0.074691258151975526
		2 0 0.995781513748348 1 0.004218486251652066
		2 0 0.87497159838676453 1 0.12502840161323547
		2 0 0.75415825291572791 1 0.24584174708427209
		2 0 0.87496825950612944 1 0.12503174049387059
		2 0 0.94544303856406986 1 0.054556961435930168
		2 0 0.82463306188583374 1 0.17536693811416626
		2 0 0.70381628248461081 1 0.29618371751538919
		2 0 0.8246303387208187 1 0.1753696612791813
		2 0 0.89510482989835738 1 0.10489517010164265
		2 0 0.7742927074432373 1 0.2257072925567627
		2 0 0.65348011612512469 1 0.34651988387487537
		2 0 0.77429112857604998 1 0.22570887142394999
		2 0 0.84476409889762727 1 0.15523590110237268
		2 0 0.7239532470703125 1 0.2760467529296875
		2 0 0.60313987823899118 1 0.39686012176100888
		2 0 0.72395295758318756 1 0.27604704241681238
		2 0 0.79442383253796645 1 0.20557616746203355
		2 0 0.67361125349998474 1 0.32638874650001526
		2 0 0.5528003476483534 1 0.4471996523516466
		2 0 0.67361092503039688 1 0.32638907496960318
		2 0 0.74408913298191659 1 0.25591086701808341
		2 0 0.62327563762664795 1 0.37672436237335205
		2 0 0.50246339111062532 1 0.49753660888937462
		2 0 0.62327548063146365 1 0.3767245193685364
		2 0 0.69374998516941278 1 0.30625001483058722
		2 0 0.57293987274169922 1 0.42706012725830078
		2 0 0.45212297597198725 1 0.54787702402801275
		2 0 0.57293842668668093 1 0.42706157331331912
		2 0 0.64341219665708216 1 0.35658780334291784
		2 0 0.52259960770606995 1 0.47740039229393005
		2 0 0.4017850466638464 1 0.5982149533361536
		2 0 0.5226005565493117 1 0.47739944345068824
		2 0 0.59307245709776402 1 0.40692754290223598
		2 0 0.47226130962371826 1 0.52773869037628174
		2 0 0.35144875033810569 1 0.64855124966189437
		2 0 0.47226076339761991 1 0.52773923660238009
		2 0 0.5427344325751875 1 0.45726556742481245
		2 0 0.42192113399505615 1 0.57807886600494385
		2 0 0.30111032552799938 1 0.69888967447200068
		2 0 0.42192179487926229 1 0.57807820512073771
		2 0 0.49239822587447535 1 0.50760177412552465
		2 0 0.37158381938934326 1 0.62841618061065674
		2 0 0.25077117005449628 1 0.74922882994550366
		2 0 0.37158367759523514 1 0.62841632240476497
		2 0 0.44205870457787599 1 0.55794129542212401
		2 0 0.3212432861328125 1 0.6787567138671875
		2 0 0.20043202285268974 1 0.79956797714731032
		2 0 0.3212459114622418 1 0.6787540885377582
		2 0 0.39171761141666278 1 0.60828238858333716
		2 0 0.27090436220169067 1 0.72909563779830933
		2 0 0.15009057332908107 1 0.84990942667091895
		2 0 0.27090467260007328 1 0.72909532739992677
		2 0 0.34138024085662033 1 0.65861975914337967
		2 0 0.22056782245635986 1 0.77943217754364014
		2 0 0.099756178178621188 1 0.90024382182137885
		2 0 0.22056894573724148 1 0.77943105426275849
		2 0 0.29104052305433287 1 0.70895947694566719
		2 0 0.17023110389709473 1 0.82976889610290527
		2 0 0.049416104433095569 1 0.95058389556690437
		2 0 0.17022580258118133 1 0.82977419741881864
		2 0 0.24070185790196605 1 0.75929814209803403
		2 0 0.11989158391952515 1 0.88010841608047485
		1 1 1
		2 0 0.11988798085649488 1 0.88011201914350512
		1 1 1
		1 1 1
		2 0 9.0421281129238196e-08 1 0.99999990957871887
		2 0 1.1168958735652268e-07 1 0.99999988831041264
		1 1 1
		2 0 7.6720220931747463e-07 1 0.99999923279779068
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 0 1
		2 0 0.99999989312256332 1 1.068774366785874e-07
		1 0 1
		1 0 1
		2 0 0.99999978624548191 1 2.1375451808580692e-07
		1 0 1
		1 0 1
		2 0 0.99999994656134916 1 5.3438650837733803e-08
		1 1 1
		1 1 1
		1 1 1
		2 0 -3.0589319521823199e-08 1 1.0000000305893195
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 1 1
		2 0 3.4272670745849609e-07 1 0.99999965727329254
		1 1 1
		2 0 0.052826089831071801 1 0.94717391016892816
		1 1 1
		1 1 1
		2 0 0.12115408596966407 1 0.87884591403033596
		1 1 1
		2 0 0.069555580615997314 1 0.93044441938400269
		2 0 0.19036655615058268 1 0.80963344384941727
		2 0 0.069555177577666966 1 0.93044482242233306
		1 0 1
		1 0 1
		1 0 1
		2 0 0.95550999652038471 1 0.044490003479615209
		2 0 0.99999913971038268 1 8.602896173215413e-07
		2 0 0.9999995231628418 1 4.76837158203125e-07
		2 0 0.90517082765010881 1 0.094829172349891175
		1 0 1
		2 0 0.97564909979701042 1 0.024350900202989578
		2 0 0.8548360357483743 1 0.14516396425162564
		2 0 0.97564970362252013 1 0.024350296377479836
		1 0 1
		2 0 0.92530741542577744 1 0.074692584574222565
		2 0 0.80449668119876727 1 0.19550331880123281
		2 0 0.92530857570147573 1 0.074691424298524267
		2 0 0.99578146176346327 1 0.0042185382365367632
		2 0 0.87497147917747498 1 0.12502852082252502
		2 0 0.75415828995508483 1 0.2458417100449152
		2 0 0.87496844333251322 1 0.12503155666748678
		2 0 0.94544304305889093 1 0.054556956941109055
		2 0 0.82463306188583374 1 0.17536693811416626
		2 0 0.70381684019500979 1 0.29618315980499016
		2 0 0.82463020513719221 1 0.17536979486280788
		2 0 0.89510267043107772 1 0.10489732956892223
		2 0 0.77429251372814178 1 0.22570748627185822
		2 0 0.65348020859069345 1 0.34651979140930655
		2 0 0.77429119301326199 1 0.22570880698673809
		2 0 0.84476502269131193 1 0.15523497730868807
		2 0 0.72395381331443787 1 0.27604618668556213
		2 0 0.60313976257654167 1 0.39686023742345833
		2 0 0.72395452278731764 1 0.2760454772126823
		2 0 0.79442398931050784 1 0.20557601068949213
		2 0 0.67361453175544739 1 0.32638546824455261
		2 0 0.55280033602333045 1 0.44719966397666955
		2 0 0.67361365895988112 1 0.32638634104011888
		2 0 0.74408736997354952 1 0.25591263002645043
		2 0 0.62327563762664795 1 0.37672436237335205
		2 0 0.5024633703738165 1 0.49753662962618356
		2 0 0.62327458661554569 1 0.37672541338445426
		2 0 0.693749197361473 1 0.306250802638527
		2 0 0.57293978333473206 1 0.42706021666526794
		2 0 0.45212424986207861 1 0.54787575013792134
		2 0 0.57293944579833933 1 0.42706055420166072
		2 0 0.64341466190236218 1 0.35658533809763793
		2 0 0.52259978652000427 1 0.47740021347999573
		2 0 0.40178493005848404 1 0.59821506994151608
		2 0 0.52260003454940718 1 0.47739996545059282
		2 0 0.59307269239677851 1 0.40692730760322143
		2 0 0.47226101160049438 1 0.52773898839950562
		2 0 0.35144906698687056 1 0.64855093301312938
		2 0 0.47226049131504411 1 0.52773950868495589
		2 0 0.54273381487892047 1 0.45726618512107958
		2 0 0.42192095518112183 1 0.57807904481887817
		2 0 0.30111014201874825 1 0.69888985798125169
		2 0 0.42192199965244886 1 0.57807800034755119
		2 0 0.49239807068960295 1 0.50760192931039705
		2 0 0.37158381938934326 1 0.62841618061065674
		2 0 0.25077132270745212 1 0.74922867729254794
		2 0 0.37158358974175903 1 0.62841641025824102
		2 0 0.4420570115688035 1 0.5579429884311965
		2 0 0.32124245166778564 1 0.67875754833221436
		2 0 0.20043222317951626 1 0.79956777682048374
		2 0 0.32124494615769478 1 0.67875505384230517
		2 0 0.39171774691094918 1 0.60828225308905082
		2 0 0.27090603113174438 1 0.72909396886825562
		2 0 0.15009102870189014 1 0.84990897129810983
		2 0 0.27090321611408541 1 0.72909678388591459
		2 0 0.34138206730711451 1 0.65861793269288549
		2 0 0.22056770324707031 1 0.77943229675292969
		2 0 0.099756045943534738 1 0.90024395405646529
		2 0 0.22056886667147993 1 0.7794311333285201
		2 0 0.29104135895219252 1 0.70895864104780748
		2 0 0.17023104429244995 1 0.82976895570755005
		2 0 0.049416550765568903 1 0.95058344923443105
		2 0 0.17022850526714156 1 0.82977149473285849
		2 0 0.2407019419612117 1 0.75929805803878836
		2 0 0.11989176273345947 1 0.88010823726654053
		1 1 1
		2 0 0.11988999928757078 1 0.88011000071242917
		1 1 1
		1 1 1
		2 0 9.5898421648143994e-08 1 0.99999990410157835
		1 1 1
		1 1 1
		2 0 2.6439181510795606e-07 1 0.99999973560818489
		2 0 1.7881393432617188e-07 1 0.99999982118606567
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1;
	setAttr ".wl[320:642].w"
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		2 4 0.99999982118606567 5 1.7881393432617188e-07
		2 4 0.9999997761568693 5 2.2384313069778727e-07
		2 4 0.99999982959548106 5 1.7040451893990394e-07
		2 4 0.99999986135912877 5 1.3864087122783531e-07
		2 4 0.99999977615702562 5 2.2384297437838541e-07
		2 4 0.99999986135911456 5 1.3864088543869002e-07
		2 4 0.99999991479771921 5 8.5202280786234041e-08
		2 4 0.99999986135907193 5 1.3864092807125417e-07
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		2 4 0.99999970673840721 5 2.9326159278753039e-07
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 5 1
		1 5 1
		1 5 1
		2 4 0.052825529512011069 5 0.94717447048798897
		1 5 1
		1 5 1
		2 4 0.12115574520216055 5 0.87884425479783945
		1 5 1
		2 4 0.069553554058074951 5 0.93044644594192505
		2 4 0.19036659279565354 5 0.80963340720434651
		2 4 0.06955319146893349 5 0.93044680853106654
		1 4 1
		1 4 1
		1 4 1
		2 4 0.95550971974156174 5 0.044490280258438292
		1 4 1
		2 4 0.99999898672103882 5 1.0132789611816406e-06
		2 4 0.90517256195002516 5 0.094827438049974844
		1 4 1
		2 4 0.97564715892076492 5 0.024352841079235077
		2 4 0.85483385533576706 5 0.14516614466423294
		2 4 0.97564738800692941 5 0.024352611993070497
		1 4 1
		2 4 0.92530753463506699 5 0.074692465364933014
		2 4 0.8044960264313803 5 0.19550397356861965
		2 4 0.92530796893417344 5 0.074692031065826558
		2 4 0.99578245696297651 5 0.004217543037023503
		2 4 0.87497086822986603 5 0.12502913177013397
		2 4 0.75415817468671098 5 0.24584182531328902
		2 4 0.87496975716599557 5 0.12503024283400438
		2 4 0.94544166695549992 5 0.054558333044500112
		2 4 0.82463045418262482 5 0.17536954581737518
		2 4 0.70381802177756236 5 0.29618197822243764
		2 4 0.82462945081209404 5 0.17537054918790596
		2 4 0.89510456662768068 5 0.10489543337231938
		2 4 0.77429187297821045 5 0.22570812702178955
		2 4 0.65348006039590012 5 0.34651993960409988
		2 4 0.7742919276560245 5 0.22570807234397547
		2 4 0.84476429880665482 5 0.15523570119334518
		2 4 0.72395220398902893 5 0.27604779601097107
		2 4 0.60314139249391319 5 0.39685860750608692
		2 4 0.72395320004951424 5 0.27604679995048587
		2 4 0.79442501097376361 5 0.20557498902623642
		2 4 0.67361459136009216 5 0.32638540863990784
		2 4 0.55280170141394602 5 0.44719829858605392
		2 4 0.67361449782112059 5 0.32638550217887946
		2 4 0.74408889161218084 5 0.25591110838781911
		2 4 0.6232764720916748 5 0.3767235279083252
		2 4 0.50246352582604448 5 0.49753647417395552
		2 4 0.62327657785326807 5 0.37672342214673193
		2 4 0.6937510221748725 5 0.3062489778251275
		2 4 0.57293838262557983 5 0.42706161737442017
		2 4 0.45212522831899765 5 0.54787477168100229
		2 4 0.57293831868750955 5 0.4270616813124905
		2 4 0.64341271059635752 5 0.35658728940364248
		2 4 0.52259939908981323 5 0.47740060091018677
		2 4 0.40178709292014647 5 0.59821290707985342
		2 4 0.52259992405922806 5 0.477400075940772
		2 4 0.59307306291998363 5 0.40692693708001637
		2 4 0.47226196527481079 5 0.52773803472518921
		2 4 0.3514484084161833 5 0.6485515915838167
		2 4 0.4722612764000379 5 0.5277387235999621
		2 4 0.54273436745059334 5 0.45726563254940666
		2 4 0.42192214727401733 5 0.57807785272598267
		2 4 0.30110944138823692 5 0.69889055861176308
		2 4 0.42192133367364898 5 0.57807866632635108
		2 4 0.49239675123016807 5 0.50760324876983187
		2 4 0.37158310413360596 5 0.62841689586639404
		2 4 0.25077064456307829 5 0.74922935543692171
		2 4 0.3715841360205504 5 0.6284158639794496
		2 4 0.44205650841009958 5 0.55794349158990042
		2 4 0.32124286890029907 5 0.67875713109970093
		2 4 0.20043203313810273 5 0.79956796686189724
		2 4 0.32124551920878441 5 0.67875448079121559
		2 4 0.39171794641161195 5 0.60828205358838816
		2 4 0.27090549468994141 5 0.72909450531005859
		2 4 0.15009216420911201 5 0.84990783579088802
		2 4 0.27090560090677118 5 0.72909439909322893
		2 4 0.34138099419980433 5 0.65861900580019572
		2 4 0.22056800127029419 5 0.77943199872970581
		2 4 0.099755488795793032 5 0.90024451120420701
		2 4 0.22056848306255458 5 0.77943151693744539
		2 4 0.2910409821120285 5 0.70895901788797144
		2 4 0.17022949457168579 5 0.82977050542831421
		2 4 0.049416970265742849 5 0.95058302973425712
		2 4 0.17022851660491792 5 0.82977148339508211
		2 4 0.24070296285635101 5 0.75929703714364893
		2 4 0.11989152431488037 5 0.88010847568511963
		1 5 1
		2 4 0.11989034805352397 5 0.88010965194647606
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 3 1
		2 2 6.2252183852251619e-08 3 0.99999993774781615
		2 2 6.4234768615278881e-08 3 0.99999993576523138
		2 2 1.1920928955078125e-07 3 0.99999988079071045
		2 2 5.9604644775390625e-08 3 0.99999994039535522
		2 2 1.1920928955078125e-07 3 0.99999988079071045
		1 3 1
		2 2 1.9264894035586622e-07 3 0.99999980735105964
		1 2 1
		2 2 0.9999999510496842 3 4.8950315800766475e-08
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		2 2 8.3446502685546875e-07 3 0.99999916553497314
		1 3 1
		1 2 1
		2 2 0.99999930128745973 3 6.9871254027020768e-07
		1 2 1
		2 2 0.9471734851895528 3 0.05282651481044718
		1 2 1
		1 2 1
		2 2 0.87884401803359391 3 0.12115598196640612
		1 2 1
		2 2 0.93044495582580566 3 0.069555044174194336
		2 2 0.80963333737070275 3 0.19036666262929725
		2 2 0.93044525850852089 3 0.069554741491479097
		2 2 2.3692846298217773e-06 3 0.99999763071537018
		1 3 1
		1 3 1
		2 2 0.044489674783140448 3 0.95551032521685952
		2 2 3.2130628824234009e-06 3 0.99999678693711758
		2 2 1.1920928955078125e-06 3 0.99999880790710449
		2 2 0.094829882824136902 3 0.90517011717586304
		1 3 1
		2 2 0.024351060390472412 3 0.97564893960952759
		2 2 0.1451651382532001 3 0.8548348617467999
		2 2 0.024352020186912543 3 0.97564797981308748
		1 3 1
		2 2 0.074693202972412109 3 0.92530679702758789
		2 2 0.1955032081706575 3 0.80449679182934242
		2 2 0.074691418343213647 3 0.92530858165678631
		2 2 0.0042181913555534032 3 0.99578180864444654
		2 2 0.12502837181091309 3 0.87497162818908691
		2 2 0.24584161843283808 3 0.75415838156716186
		2 2 0.12503189403823575 3 0.87496810596176422
		2 2 0.054556961435930168 3 0.94544303856406986
		2 2 0.17536687850952148 3 0.82463312149047852
		2 2 0.29618371751538919 3 0.70381628248461081
		2 2 0.1753696612791813 3 0.8246303387208187
		2 2 0.10489489075885766 3 0.89510510924114228
		2 2 0.2257072925567627 3 0.7742927074432373
		2 2 0.34651992982225449 3 0.65348007017774556
		2 2 0.22570916738654379 3 0.77429083261345621
		2 2 0.15523558346928862 3 0.84476441653071133
		2 2 0.2760467529296875 3 0.7239532470703125
		2 2 0.3968598046789904 3 0.60314019532100971
		2 2 0.27604696043135446 3 0.72395303956864554
		2 2 0.20557593373804672 3 0.79442406626195328
		2 2 0.32638871669769287 3 0.67361128330230713
		2 2 0.44719976193158639 3 0.55280023806841361
		2 2 0.32638907496960318 3 0.67361092503039688
		2 2 0.25591086701808341 3 0.74408913298191659
		2 2 0.37672436237335205 3 0.62327563762664795
		2 2 0.49753655237489169 3 0.50246344762510831
		2 2 0.37672456138357258 3 0.62327543861642742
		2 2 0.3062500713738065 3 0.69374992862619356
		2 2 0.42706012725830078 3 0.57293987274169922
		2 2 0.54787679124803979 3 0.45212320875196016
		2 2 0.42706157331331912 3 0.57293842668668093
		2 2 0.3565879220578323 3 0.6434120779421677
		2 2 0.47740042209625244 3 0.52259957790374756
		2 2 0.59821479814245138 3 0.40178520185754862
		2 2 0.47739996724650086 3 0.52260003275349909
		2 2 0.40692741349276967 3 0.59307258650723038
		2 2 0.52773869037628174 3 0.47226130962371826
		2 2 0.64855134959309035 3 0.35144865040690954
		2 2 0.52773960403260078 3 0.47226039596739922
		2 2 0.45726580437946929 3 0.54273419562053071
		2 2 0.57807886600494385 3 0.42192113399505615
		2 2 0.69888991372015219 3 0.30111008627984781
		2 2 0.57807791422955279 3 0.42192208577044726
		2 2 0.50760177412552465 3 0.49239822587447535
		2 2 0.62841618061065674 3 0.37158381938934326
		2 2 0.74922882994550366 3 0.25077117005449628
		2 2 0.62841640229160634 3 0.37158359770839366
		2 2 0.55794162450193985 3 0.44205837549806015
		2 2 0.6787567138671875 3 0.3212432861328125
		2 2 0.79956789975657394 3 0.20043210024342611
		2 2 0.67875383487667929 3 0.32124616512332071
		2 2 0.60828254262951109 3 0.39171745737048891
		2 2 0.72909563779830933 3 0.27090436220169067
		2 2 0.84990928714559866 3 0.1500907128544014
		2 2 0.72909553348260325 3 0.27090446651739669
		2 2 0.65861969267659448 3 0.34138030732340552
		2 2 0.77943217754364014 3 0.22056782245635986
		2 2 0.90024381216706151 3 0.099756187832938478
		2 2 0.77943079340693255 3 0.22056920659306739
		2 2 0.70895963032846865 3 0.29104036967153135
		2 2 0.82976889610290527 3 0.17023110389709473
		2 2 0.95058389556690437 3 0.049416104433095569
		2 2 0.82977412512838544 3 0.17022587487161453
		2 2 0.75929785616212031 3 0.24070214383787975
		2 2 0.88010841608047485 3 0.11989158391952515
		1 2 1
		2 2 0.88011197575682243 3 0.11988802424317761
		1 2 1
		1 2 1
		2 2 0.99999990803333816 3 9.196666184152491e-08
		2 2 0.99999990803306815 3 9.1966931847764499e-08
		1 2 1
		2 2 0.99999916553497314 3 8.3446502685546875e-07
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 3 1
		1 3 1
		1 3 1;
	setAttr ".wl[643:966].w"
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 3 1
		2 2 1.2451141628844198e-07 3 0.99999987548858371
		1 3 1
		1 3 1
		2 2 2.384185791015625e-07 3 0.9999997615814209
		1 3 1
		1 3 1
		2 2 6.4219705109280767e-08 3 0.99999993578029489
		1 2 1
		1 2 1
		1 2 1
		2 2 0.9999999732807634 3 2.6719236601024932e-08
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 2 1
		2 2 0.99999961570074447 3 3.8429925552918576e-07
		1 2 1
		2 2 0.94717401184157979 3 0.052825988158420226
		1 2 1
		1 2 1
		2 2 0.87884591403033596 3 0.12115408596966407
		1 2 1
		2 2 0.93044441938400269 3 0.069555580615997314
		2 2 0.80963373432930741 3 0.19036626567069254
		2 2 0.93044482242233306 3 0.069555177577666966
		1 3 1
		1 3 1
		1 3 1
		2 2 0.04448970813879357 3 0.9555102918612064
		2 2 8.4936618804931641e-07 3 0.99999915063381195
		2 2 4.76837158203125e-07 3 0.9999995231628418
		2 2 0.094829167255676472 3 0.90517083274432353
		1 3 1.0000000000000002
		2 2 0.024350881576538086 3 0.97564911842346191
		2 2 0.14516344319016286 3 0.85483655680983717
		2 2 0.02435000366357308 3 0.97564999633642702
		1 3 1
		2 2 0.074692606925964355 3 0.92530739307403564
		2 2 0.19550331880123281 3 0.80449668119876727
		2 2 0.074691424298524267 3 0.92530857570147573
		2 2 0.0042185382365367632 3 0.99578146176346327
		2 2 0.12502849102020264 3 0.87497150897979736
		2 2 0.2458417100449152 3 0.75415828995508483
		2 2 0.12503155666748678 3 0.87496844333251322
		2 2 0.054556956941109055 3 0.94544304305889093
		2 2 0.17536693811416626 3 0.82463306188583374
		2 2 0.29618315980499016 3 0.70381684019500979
		2 2 0.17536979486280788 3 0.82463020513719221
		2 2 0.1048972021532957 3 0.89510279784670432
		2 2 0.22570747137069702 3 0.77429252862930298
		2 2 0.34652011329655991 3 0.65347988670344015
		2 2 0.22570916738654379 3 0.77429083261345621
		2 2 0.15523497730868807 3 0.84476502269131193
		2 2 0.27604615688323975 3 0.72395384311676025
		2 2 0.39686023742345833 3 0.60313976257654167
		2 2 0.2760454772126823 3 0.72395452278731764
		2 2 0.20557601068949213 3 0.79442398931050784
		2 2 0.326385498046875 3 0.673614501953125
		2 2 0.44719966397666955 3 0.55280033602333045
		2 2 0.32638634104011888 3 0.67361365895988112
		2 2 0.25591249971303931 3 0.74408750028696069
		2 2 0.37672436237335205 3 0.62327563762664795
		2 2 0.49753630991422049 3 0.50246369008577951
		2 2 0.37672540075171124 3 0.62327459924828876
		2 2 0.306250802638527 3 0.693749197361473
		2 2 0.42706024646759033 3 0.57293975353240967
		2 2 0.54787575013792134 3 0.45212424986207861
		2 2 0.42706059297545673 3 0.57293940702454327
		2 2 0.35658564989105695 3 0.6434143501089431
		2 2 0.47740018367767334 3 0.52259981632232666
		2 2 0.59821477192908734 3 0.40178522807091271
		2 2 0.47739996545059282 3 0.52260003454940718
		2 2 0.40692689688050288 3 0.59307310311949701
		2 2 0.52773898839950562 3 0.47226101160049438
		2 2 0.64855093301312938 3 0.35144906698687056
		2 2 0.52773920538517816 3 0.4722607946148219
		2 2 0.45726568259639555 3 0.54273431740360445
		2 2 0.57807904481887817 3 0.42192095518112183
		2 2 0.69888987698732308 3 0.30111012301267698
		2 2 0.57807800034755119 3 0.42192199965244886
		2 2 0.50760164941874886 3 0.4923983505812512
		2 2 0.62841618061065674 3 0.37158381938934326
		2 2 0.74922867729254794 3 0.25077132270745212
		2 2 0.62841641025824102 3 0.37158358974175903
		2 2 0.55794290567255844 3 0.44205709432744161
		2 2 0.67875754833221436 3 0.32124245166778564
		2 2 0.79956777682048374 3 0.20043222317951626
		2 2 0.67875505384230517 3 0.32124494615769478
		2 2 0.60828225308905082 3 0.39171774691094918
		2 2 0.72909396886825562 3 0.27090603113174438
		2 2 0.84990866920466623 3 0.15009133079533368
		2 2 0.7290972210623663 3 0.27090277893763365
		2 2 0.65861807783637294 3 0.34138192216362706
		2 2 0.77943229675292969 3 0.22056770324707031
		2 2 0.90024395405646529 3 0.099756045943534738
		2 2 0.7794311333285201 3 0.22056886667147993
		2 2 0.70895864104780748 3 0.29104135895219252
		2 2 0.82976895570755005 3 0.17023104429244995
		2 2 0.95058344718136512 3 0.049416552818634878
		2 2 0.82977149473285849 3 0.17022850526714156
		2 2 0.75929804166769266 3 0.24070195833230737
		2 2 0.88010823726654053 3 0.11989176273345947
		1 2 1
		2 2 0.88010998400104712 3 0.11989001599895284
		1 2 1
		1 2 1
		2 2 0.99999990803340921 3 9.1966590787251334e-08
		1 2 1
		1 2 1
		2 2 0.99999963213525689 3 3.678647431115678e-07
		2 2 0.99999979093101388 3 2.0906898612338409e-07
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		2 6 1.7881393432617188e-07 7 0.99999982118606567
		2 6 2.384185791015625e-07 7 0.9999997615814209
		2 6 1.7881393432617188e-07 7 0.99999982118606567
		2 6 1.1920928955078125e-07 7 0.99999988079071045
		2 6 2.328011703411903e-07 7 0.99999976719882966
		2 6 1.1920928955078125e-07 7 0.99999988079071045
		2 6 4.6274237774923677e-08 7 0.99999995372576223
		2 6 1.3713784596802725e-07 7 0.99999986286215403
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		2 6 2.9802322387695312e-07 7 0.99999970197677612
		1 7 1
		1 7 1
		2 6 1.0759492852230323e-08 7 0.99999998924050715
		1 7 1
		1 7 1
		1 7 1
		2 6 -6.1968421505298465e-09 7 1.0000000061968422
		1 6 1
		1 6 1
		1 6 1
		2 6 0.94717429061256642 7 0.052825709387433611
		1 6 1
		1 6 1
		2 6 0.87884425479783945 7 0.12115574520216055
		1 6 1
		2 6 0.93044644594192505 7 0.069553554058074951
		2 6 0.80963374616947692 7 0.19036625383052302
		2 6 0.93044690603252944 7 0.069553093967470558
		1 7 1
		2 6 2.8125941753387451e-07 7 0.99999971874058247
		1 7 1
		2 6 0.044490280258438292 7 0.95550971974156174
		2 6 2.7939677238464355e-08 7 0.99999997206032276
		2 6 1.0132789611816406e-06 7 0.99999898672103882
		2 6 0.094827438049974844 7 0.90517256195002516
		1 7 1
		2 6 0.024352848529815674 7 0.97564715147018433
		2 6 0.14516614466423294 7 0.85483385533576706
		2 6 0.024352439855075327 7 0.9756475601449246
		1 7 1
		2 6 0.074692487716674805 7 0.9253075122833252
		2 6 0.19550397356861965 7 0.8044960264313803
		2 6 0.074692031065826558 7 0.92530796893417344
		2 6 0.004217543037023503 7 0.99578245696297651
		2 6 0.12502914667129517 7 0.87497085332870483
		2 6 0.24584182531328902 7 0.75415817468671098
		2 6 0.12503024283400438 7 0.87496975716599557
		2 6 0.054558333044500112 7 0.94544166695549992
		2 6 0.17536956071853638 7 0.82463043928146362
		2 6 0.29618234877762828 7 0.70381765122237172
		2 6 0.17537054918790596 7 0.82462945081209404
		2 6 0.10489545157722191 7 0.89510454842277809
		2 6 0.22570812702178955 7 0.77429187297821045
		2 6 0.34651993960409988 7 0.65348006039590012
		2 6 0.22570807234397547 7 0.7742919276560245
		2 6 0.15523570119334518 7 0.84476429880665482
		2 6 0.27604782581329346 7 0.72395217418670654
		2 6 0.3968588125666791 7 0.6031411874333209
		2 6 0.27604682492514293 7 0.72395317507485712
		2 6 0.20557498902623642 7 0.79442501097376361
		2 6 0.32638537883758545 7 0.67361462116241455
		2 6 0.44719829858605392 7 0.55280170141394602
		2 6 0.32638550217887946 7 0.67361449782112059
		2 6 0.25591110838781911 7 0.74408889161218084
		2 6 0.3767235279083252 7 0.6232764720916748
		2 6 0.49753647417395552 7 0.50246352582604448
		2 6 0.37672342214673193 7 0.62327657785326807
		2 6 0.30624934629008477 7 0.69375065370991518
		2 6 0.42706161737442017 7 0.57293838262557983
		2 6 0.54787469597626559 7 0.45212530402373441
		2 6 0.4270616813124905 7 0.57293831868750955
		2 6 0.35658728940364248 7 0.64341271059635752
		2 6 0.47740060091018677 7 0.52259939908981323
		2 6 0.59821269934674337 7 0.40178730065325663
		2 6 0.47740005691124948 7 0.52259994308875057
		2 6 0.40692673452779654 7 0.5930732654722034
		2 6 0.52773803472518921 7 0.47226196527481079
		2 6 0.64855152364624968 7 0.35144847635375026
		2 6 0.527738460641067 7 0.47226153935893306
		2 6 0.45726563254940666 7 0.54273436745059334
		2 6 0.57807785272598267 7 0.42192214727401733
		2 6 0.69889055861176308 7 0.30110944138823692
		2 6 0.57807841118064218 7 0.42192158881935776
		2 6 0.50760299188779701 7 0.49239700811220299
		2 6 0.62841689586639404 7 0.37158310413360596
		2 6 0.74922935543692171 7 0.25077064456307829
		2 6 0.62841566587555497 7 0.37158433412444503
		2 6 0.55794349158990042 7 0.44205650841009958
		2 6 0.67875713109970093 7 0.32124286890029907
		2 6 0.79956812618892803 7 0.20043187381107189
		2 6 0.67875448079121559 7 0.32124551920878441
		2 6 0.60828205358838816 7 0.39171794641161195
		2 6 0.72909450531005859 7 0.27090549468994141
		2 6 0.84990810761951308 7 0.15009189238048695
		2 6 0.72909439909322893 7 0.27090560090677118
		2 6 0.65861900580019572 7 0.34138099419980433
		2 6 0.77943199872970581 7 0.22056800127029419
		2 6 0.90024451120420701 7 0.099755488795793032
		2 6 0.77943151693744539 7 0.22056848306255458
		2 6 0.70895921298152476 7 0.29104078701847519
		2 6 0.82977050542831421 7 0.17022949457168579
		2 6 0.95058302973425712 7 0.049416970265742849
		2 6 0.82977148339508211 7 0.17022851660491792
		2 6 0.75929703714364893 7 0.24070296285635101
		2 6 0.88010847568511963 7 0.11989152431488037
		1 6 0.99999999999999989
		2 6 0.8801093906727655 7 0.11989060932723454
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 7 1
		1 7 1
		1 7 1;
	setAttr ".wl[967:971].w"
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1;
	setAttr -s 8 ".pm";
	setAttr ".pm[0]" -type "matrix" 0.53148349614714974 -0.84706864734990794 0 0 0.84706864734990794 0.53148349614714974 0 0
		 0 0 1 0 10.132000365573038 -56.270420551938543 122.43900000000014 1;
	setAttr ".pm[1]" -type "matrix" 0.53148349614714974 -0.84706864734990794 0 0 0.84706864734990794 0.53148349614714974 0 0
		 0 0 1 0 -32.029045391144152 -56.270420551938543 122.43900000000014 1;
	setAttr ".pm[2]" -type "matrix" 0.53148420859986079 0.8470682003292177 0 0 -0.8470682003292177 0.53148420859986079 0 0
		 0 0 1 0 32.029026854177204 -56.270424999087084 122.43900000000008 1;
	setAttr ".pm[3]" -type "matrix" 0.53148420859986079 0.8470682003292177 0 0 -0.8470682003292177 0.53148420859986079 0 0
		 0 0 1 0 -10.132109698044594 -56.270424999087084 122.43900000000014 1;
	setAttr ".pm[4]" -type "matrix" 0.53148349614714974 -0.84706864734990794 0 0 0.84706864734990794 0.53148349614714974 0 0
		 0 0 1 0 10.132000365573038 -56.270420551938543 -135.99999987792955 1;
	setAttr ".pm[5]" -type "matrix" 0.53148349614714974 -0.84706864734990794 0 0 0.84706864734990794 0.53148349614714974 0 0
		 0 0 1 0 -32.029045391144152 -56.270420551938543 -135.99999987792955 1;
	setAttr ".pm[6]" -type "matrix" 0.53148420859986079 0.8470682003292177 0 0 -0.8470682003292177 0.53148420859986079 0 0
		 0 0 1 0 32.029026854177204 -56.270424999087084 -135.99999987792961 1;
	setAttr ".pm[7]" -type "matrix" 0.53148420859986079 0.8470682003292177 0 0 -0.8470682003292177 0.53148420859986079 0 0
		 0 0 1 0 -10.132109698044594 -56.270424999087084 -135.99999987792961 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 8 ".ma";
	setAttr -s 8 ".dpf[0:7]"  4 4 4 4 4 4 4 4;
	setAttr -s 8 ".lw";
	setAttr -s 8 ".lw";
	setAttr ".mmi" yes;
	setAttr ".ucm" yes;
	setAttr -s 8 ".ifcl";
	setAttr -s 8 ".ifcl";
createNode groupParts -n "skinCluster7GroupParts";
	rename -uid "61051266-48E8-9008-80F7-C5A71B3FC816";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode materialInfo -n "materialInfo4";
	rename -uid "359CACCB-4D8A-352C-63FD-7A82CBDA0DB8";
createNode shadingEngine -n "springs_tutorial_shdr_SE";
	rename -uid "4F32BFA0-4E6B-B6C4-50DA-94AE4CA64EF8";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
createNode phong -n "springs_tutorial_shdr";
	rename -uid "A085C08A-4B1F-C312-6ED0-C589E0508E4C";
	setAttr ".rdl" 10;
	setAttr ".dc" 1;
	setAttr ".c" -type "float3" 1 0.44999999 0.44999999 ;
	setAttr ".cp" 5;
createNode groupId -n "skinCluster6GroupId";
	rename -uid "D2F353FF-4922-959C-21FC-A08FF2E425B2";
	setAttr ".ihi" 0;
createNode objectSet -n "skinCluster6Set";
	rename -uid "950D560D-4699-383D-A5A3-0C8DC1CB0709";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode skinCluster -n "skinCluster6";
	rename -uid "EB382989-41FE-3EA9-6EAB-18B948D71BFE";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr -s 624 ".wl";
	setAttr ".wl[0:499].w"
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 4 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1;
	setAttr ".wl[500:623].w"
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1;
	setAttr -s 8 ".pm";
	setAttr ".pm[0]" -type "matrix" 0.53148349614714974 -0.84706864734990794 0 0 0.84706864734990794 0.53148349614714974 0 0
		 0 0 1 0 10.132000365573038 -56.270420551938543 122.43900000000014 1;
	setAttr ".pm[1]" -type "matrix" 0.53148349614714974 -0.84706864734990794 0 0 0.84706864734990794 0.53148349614714974 0 0
		 0 0 1 0 -32.029045391144152 -56.270420551938543 122.43900000000014 1;
	setAttr ".pm[2]" -type "matrix" 0.53148420859986079 0.8470682003292177 0 0 -0.8470682003292177 0.53148420859986079 0 0
		 0 0 1 0 32.029026854177204 -56.270424999087084 122.43900000000008 1;
	setAttr ".pm[3]" -type "matrix" 0.53148420859986079 0.8470682003292177 0 0 -0.8470682003292177 0.53148420859986079 0 0
		 0 0 1 0 -10.132109698044594 -56.270424999087084 122.43900000000014 1;
	setAttr ".pm[4]" -type "matrix" 0.53148349614714974 -0.84706864734990794 0 0 0.84706864734990794 0.53148349614714974 0 0
		 0 0 1 0 10.132000365573038 -56.270420551938543 -135.99999987792955 1;
	setAttr ".pm[5]" -type "matrix" 0.53148349614714974 -0.84706864734990794 0 0 0.84706864734990794 0.53148349614714974 0 0
		 0 0 1 0 -32.029045391144152 -56.270420551938543 -135.99999987792955 1;
	setAttr ".pm[6]" -type "matrix" 0.53148420859986079 0.8470682003292177 0 0 -0.8470682003292177 0.53148420859986079 0 0
		 0 0 1 0 32.029026854177204 -56.270424999087084 -135.99999987792961 1;
	setAttr ".pm[7]" -type "matrix" 0.53148420859986079 0.8470682003292177 0 0 -0.8470682003292177 0.53148420859986079 0 0
		 0 0 1 0 -10.132109698044594 -56.270424999087084 -135.99999987792961 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 8 ".ma";
	setAttr -s 8 ".dpf[0:7]"  4 4 4 4 4 4 4 4;
	setAttr -s 8 ".lw";
	setAttr -s 8 ".lw";
	setAttr ".mmi" yes;
	setAttr ".ucm" yes;
	setAttr -s 8 ".ifcl";
	setAttr -s 8 ".ifcl";
createNode groupParts -n "skinCluster6GroupParts";
	rename -uid "8F08BE80-49B7-79DE-E359-D98876ED2610";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode script -n "uiConfigurationScriptNode";
	rename -uid "79F5C2A2-4FD5-4591-83E8-3F819A87FE9E";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n"
		+ "            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1162\n            -height 708\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n"
		+ "            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -selectCommand \"print(\\\"\\\")\" \n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n"
		+ "            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n"
		+ "            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n"
		+ "            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n"
		+ "                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n"
		+ "                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showPlayRangeShades \"on\" \n                -lockPlayRangeShades \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n"
		+ "                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -keyMinScale 1\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -valueLinesToggle 1\n                -highlightAffectedCurves 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n"
		+ "                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n"
		+ "                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n"
		+ "            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n"
		+ "                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit 1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n"
		+ "                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n"
		+ "                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit 1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n{ string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"|persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n"
		+ "                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n"
		+ "                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName; };\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1162\\n    -height 708\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1162\\n    -height 708\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "3DE731DD-49E5-61B9-4E94-5AAB531D971F";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode multiplyDivide -n "MLD_bank";
	rename -uid "E35CD1CC-49FA-D9A2-A9AD-E1A0F4A3BB1F";
	setAttr ".i2" -type "float3" -1 -1 1 ;
createNode condition -n "CND_bank_L";
	rename -uid "5539C5C7-4AB7-57C0-8A71-798874F4C4C5";
	setAttr ".op" 2;
	setAttr ".cf" -type "float3" 0 0 0 ;
createNode condition -n "CND_bank_R";
	rename -uid "02F71335-4793-5EBC-5200-A6AE2336D3F1";
	setAttr ".op" 4;
	setAttr ".cf" -type "float3" 0 0 0 ;
createNode unitConversion -n "unitConversion508";
	rename -uid "8DBA1AF5-4FC4-12CB-076C-7CA2FE5453E3";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion509";
	rename -uid "B3B7C94B-413C-1F6C-9105-FC87F9BA1D61";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "MLD_structure_adjust_01";
	rename -uid "87FFCD87-4604-01B7-69FE-8BAD4167EFFE";
	setAttr ".i2" -type "float3" 0.057 1 0.057 ;
createNode plusMinusAverage -n "PLS_structureAdjust_lenght";
	rename -uid "672BDA28-43D4-40A2-2219-759643A5B782";
	setAttr -s 2 ".i1[0:1]"  5 0;
createNode multiplyDivide -n "MLD_structure_adjust_03";
	rename -uid "E8E8DFF5-44A0-91CB-49ED-BEB3E4B2846D";
	setAttr ".i2" -type "float3" -1 1 -1 ;
createNode unitConversion -n "unitConversion510";
	rename -uid "E8FD7518-4874-F6D4-0EA3-538C85286EC8";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion512";
	rename -uid "41ADEDC4-46DD-7057-0A85-BDADE42FA99E";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "MLD_structure_adjust_02";
	rename -uid "CAB36C00-42C9-CEF0-C75B-BEB9696A6BAF";
	setAttr ".op" 2;
	setAttr ".i2" -type "float3" 10 1 10 ;
createNode plusMinusAverage -n "PLS_structureAdjust_width";
	rename -uid "37588A27-47F9-9E03-F04D-3ABEFEBC014C";
	setAttr ".op" 2;
	setAttr -s 2 ".i1[0:1]"  5 0;
createNode unitConversion -n "unitConversion514";
	rename -uid "B8090868-4B37-149A-500A-609EA75C3BE4";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion515";
	rename -uid "6A68401E-4865-3971-3DAC-07A7B6D8E9A6";
	setAttr ".cf" 0.017453292519943295;
createNode setRange -n "SR_structure_adjust";
	rename -uid "D53B0BF4-483A-EE9A-2BB2-3DB71075B04B";
	setAttr ".m" -type "float3" 750 0 0 ;
	setAttr ".on" -type "float3" 20 0 0 ;
	setAttr ".om" -type "float3" 750 0 0 ;
createNode multiplyDivide -n "MLD_pivotBodyCtrl";
	rename -uid "84B5EDC2-48B7-0399-D7CB-BB92CDB11C83";
	setAttr ".i2" -type "float3" -0.5 -0.5 1 ;
createNode multiplyDivide -n "MLD_bankPivot";
	rename -uid "3EF9998C-42E9-8772-41A7-B98565BDB0C6";
	setAttr ".i2" -type "float3" 1 -1 1 ;
createNode plusMinusAverage -n "PMA_bankPivot_L";
	rename -uid "0834E99E-47D9-9D75-11F9-0491072ECA36";
	setAttr -s 2 ".i1[1]"  88.35900116;
createNode plusMinusAverage -n "PMA_bankPivot_R";
	rename -uid "63300705-4509-85DF-E30B-54A48386C389";
	setAttr -s 2 ".i1[1]"  -88.35900116;
createNode plusMinusAverage -n "plusMinusAverage2";
	rename -uid "F2BAF748-40AD-0519-A5B0-6A89918C3FB7";
	setAttr ".op" 2;
	setAttr -s 2 ".i1[1]"  35;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "150650E7-4F15-B955-758A-C6A2325CAC0B";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -348.8095099490792 -217.85713420027813 ;
	setAttr ".tgi[0].vh" -type "double2" 334.52379623102826 227.38094334564548 ;
createNode plusMinusAverage -n "plusMinusAverage3";
	rename -uid "30AAD56C-43D5-15E9-C540-688EB6960205";
	setAttr ".op" 2;
	setAttr -s 2 ".i1[1]"  35;
createNode plusMinusAverage -n "plusMinusAverage4";
	rename -uid "32B790AC-4408-7EB0-1385-1AB1E87ED72E";
	setAttr ".op" 2;
	setAttr -s 2 ".i1[1]"  35;
createNode skinCluster -n "skinCluster8";
	rename -uid "346D74CD-42B7-9CD5-0BE7-B49495602689";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr -s 1368 ".wl";
	setAttr ".wl[0:499].w"
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1;
	setAttr ".wl[500:999].w"
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1;
	setAttr ".wl[1000:1367].w"
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 1 1
		1 3 1
		1 2 1;
	setAttr -s 4 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 0 0 0 0 0.99999999988674748 1.5050084283021111e-05 0
		 0 -1.5050084283021111e-05 0.99999999988674748 0 -88.130042686886611 -34.99820707034192 -121.40352673971532 1;
	setAttr ".pm[1]" -type "matrix" 1 0 0 0 0 0.99999999988674748 1.5050084283350017e-05 0
		 0 -1.5050084283350017e-05 0.99999999988674748 0 88.130000000000564 -34.998172870653875 -121.40385840645017 1;
	setAttr ".pm[2]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -88.130042686887265 -35.000000002731824 137.03581848453868 1;
	setAttr ".pm[3]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 88.130000000000024 -35.000000000000327 137.03599997812688 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 4 ".ma";
	setAttr -s 4 ".dpf[0:3]"  4 4 4 4;
	setAttr -s 4 ".lw";
	setAttr -s 4 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 5;
	setAttr ".ucm" yes;
	setAttr -s 4 ".ifcl";
	setAttr -s 4 ".ifcl";
createNode objectSet -n "skinCluster8Set";
	rename -uid "87D24E15-4D83-F55D-B2AF-69A25116F34A";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster8GroupId";
	rename -uid "55DFC272-4225-51BE-195B-5A943734BBB5";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster8GroupParts";
	rename -uid "7A7689C2-495F-B18E-241F-31A46ED92C53";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster9";
	rename -uid "39D36989-4E6F-D754-9CC4-51BB50F37871";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr -s 840 ".wl";
	setAttr ".wl[0:499].w"
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 3 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1;
	setAttr ".wl[500:839].w"
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1
		1 0 1;
	setAttr -s 4 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 0 0 0 0 0.99999999988674748 1.5050084283021111e-05 0
		 0 -1.5050084283021111e-05 0.99999999988674748 0 -88.130042686886611 -34.99820707034192 -121.40352673971532 1;
	setAttr ".pm[1]" -type "matrix" 1 0 0 0 0 0.99999999988674748 1.5050084283350017e-05 0
		 0 -1.5050084283350017e-05 0.99999999988674748 0 88.130000000000564 -34.998172870653875 -121.40385840645017 1;
	setAttr ".pm[2]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -88.130042686887265 -35.000000002731824 137.03581848453868 1;
	setAttr ".pm[3]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 88.130000000000024 -35.000000000000327 137.03599997812688 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 4 ".ma";
	setAttr -s 4 ".dpf[0:3]"  4 4 4 4;
	setAttr -s 4 ".lw";
	setAttr -s 4 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 5;
	setAttr ".ucm" yes;
	setAttr -s 4 ".ifcl";
	setAttr -s 4 ".ifcl";
createNode objectSet -n "skinCluster9Set";
	rename -uid "4E59CFDC-4372-5FD1-5F74-A7ABF9A683B8";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster9GroupId";
	rename -uid "FC82CC1D-4636-5428-A9E5-C7AD4503EE5B";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster9GroupParts";
	rename -uid "E6B9F35F-4885-5B4D-65C2-05A99EB3F2D9";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode cluster -n "CLS_r_frontWheel_top_02Cluster";
	rename -uid "A64E7856-4DEC-85EB-52F2-67AE3EDB2C3B";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896999999999998 0 0 0 0 80.992000000000004 0 0
		 0 0 80.992000000000004 0 -78.668000000000603 35.018819451332106 121.41202163696292 1;
createNode objectSet -n "cluster33Set";
	rename -uid "4F29BD33-45D3-9EB8-4C86-9EB8F91E3E02";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster33GroupId";
	rename -uid "EA32B4CA-4378-3A0B-2791-DBBC7CAE47A9";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster33GroupParts";
	rename -uid "B7FBF299-4EA8-87BD-35F2-70BC9192A660";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[0][1][1]";
createNode cluster -n "CLS_r_frontWheel_top_01Cluster";
	rename -uid "5DB96991-4C89-0D5B-CEC3-9BA6FBBE99BC";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896999999999998 0 0 0 0 80.992000000000004 0 0
		 0 0 80.992000000000004 0 -78.668000000000603 35.018819451332106 121.41202163696292 1;
createNode objectSet -n "cluster34Set";
	rename -uid "FD141807-4223-318A-13EB-1E9BCE4C270D";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster34GroupId";
	rename -uid "ECE2693F-4F1C-82C3-F1A4-07B3D61FC3B3";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster34GroupParts";
	rename -uid "309D0BC0-4FE9-32DE-9EB5-14BED1B3EF31";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[1][1][1]";
createNode cluster -n "CLS_r_frontWheel_top_03Cluster";
	rename -uid "9D0EE8F1-4136-EB6C-05A2-F9A14CC1F903";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896999999999998 0 0 0 0 80.992000000000004 0 0
		 0 0 80.992000000000004 0 -78.668000000000603 35.018819451332106 121.41202163696292 1;
createNode objectSet -n "cluster35Set";
	rename -uid "E910E018-48C5-BF00-325F-48B37A2DC7A5";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster35GroupId";
	rename -uid "1ED7EA2A-448D-1F1F-5C07-0292EEB56014";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster35GroupParts";
	rename -uid "6B1E7FBE-4C26-1ECE-EFCA-8C8DEAFBE9D1";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[0][1][0]";
createNode cluster -n "CLS_r_frontWheel_top_04Cluster";
	rename -uid "6D851F5A-4456-0A23-ACD1-ECBA7586B09B";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896999999999998 0 0 0 0 80.992000000000004 0 0
		 0 0 80.992000000000004 0 -78.668000000000603 35.018819451332106 121.41202163696292 1;
createNode objectSet -n "cluster36Set";
	rename -uid "DEFB7034-4B44-313D-3744-2287AD9D7A78";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster36GroupId";
	rename -uid "EC2A81F8-4A2D-1E35-5E06-24A2AB69207A";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster36GroupParts";
	rename -uid "465D58AD-4B3D-E5AF-62D8-ADAEF7597D63";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[1][1][0]";
createNode cluster -n "CLS_r_frontWheel_bot_02Cluster";
	rename -uid "A05204DD-44BE-9948-E1FE-48AC252D0593";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896999999999998 0 0 0 0 80.992000000000004 0 0
		 0 0 80.992000000000004 0 -78.668000000000603 35.018819451332106 121.41202163696292 1;
createNode objectSet -n "cluster37Set";
	rename -uid "A12AFB6A-45DF-9AD9-488E-13B2620A0046";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster37GroupId";
	rename -uid "99B2C9D6-4D39-315D-BB2E-B298F9B70207";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster37GroupParts";
	rename -uid "04780D8C-4161-0F13-4D47-7F91796F028E";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[0][0][1]";
createNode cluster -n "CLS_r_frontWheel_bot_01Cluster";
	rename -uid "935E6332-4478-0F94-72EA-C7B2212BFE1E";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896999999999998 0 0 0 0 80.992000000000004 0 0
		 0 0 80.992000000000004 0 -78.668000000000603 35.018819451332106 121.41202163696292 1;
createNode objectSet -n "cluster38Set";
	rename -uid "55E06217-4B5E-1643-2E06-668BA32118C2";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster38GroupId";
	rename -uid "9E13089D-458E-CDA0-5F93-1D812B5745AD";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster38GroupParts";
	rename -uid "4AF54EC9-476E-DBF4-0751-139C18C4E1E0";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[1][0][1]";
createNode cluster -n "CLS_r_frontWheel_bot_03Cluster";
	rename -uid "2C97EE82-4921-9636-61F4-C0B1DC9AC802";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896999999999998 0 0 0 0 80.992000000000004 0 0
		 0 0 80.992000000000004 0 -78.668000000000603 35.018819451332106 121.41202163696292 1;
createNode objectSet -n "cluster39Set";
	rename -uid "D97B1FBB-4674-AF98-C01B-2FA8EF4A7959";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster39GroupId";
	rename -uid "0235B870-42CE-4CAD-A6E7-868AFBF35004";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster39GroupParts";
	rename -uid "6CB37673-4CA8-D393-AE16-10BDDA66A9F8";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[0][0][0]";
createNode cluster -n "CLS_r_frontWheel_bot_04Cluster";
	rename -uid "CA5F55D1-4BBB-C11E-196E-ADB9F0BA3109";
	setAttr ".ip[0].gtg" -type "string" "";
	setAttr ".gm[0]" -type "matrix" 36.896999999999998 0 0 0 0 80.992000000000004 0 0
		 0 0 80.992000000000004 0 -78.668000000000603 35.018819451332106 121.41202163696292 1;
createNode objectSet -n "cluster40Set";
	rename -uid "2C977B7F-492A-F1FC-6304-089F2B908AE2";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "cluster40GroupId";
	rename -uid "804D40C5-4137-03A7-C6E2-05902F510431";
	setAttr ".ihi" 0;
createNode groupParts -n "cluster40GroupParts";
	rename -uid "0FDD75C3-43D1-A6D5-0BEB-FD899D532261";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "pt[1][0][0]";
createNode groupId -n "groupId50";
	rename -uid "B4B30191-408E-47B2-C3AF-EAAEE00CD006";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts49";
	rename -uid "34BB66CE-4FA8-3B8D-9F0E-20B3DD3D3F18";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[210:419]";
createNode groupId -n "groupId51";
	rename -uid "32F5F624-45D9-35DD-4112-BF8919D51FF8";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts50";
	rename -uid "059F17E7-4B64-2E57-4E12-7F876ED719A5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "vtx[341:681]" "vtx[1365]";
createNode multiplyDivide -n "MLD_FrontWheelsSpin";
	rename -uid "BA05155B-4C6B-588E-C976-11A4EBD5F45D";
createNode multiplyDivide -n "MLD_RearWheelsSpin";
	rename -uid "CDF3B72A-472E-2E69-DBA1-B496FAC8A201";
createNode objectSet -n "SETS";
	rename -uid "8CA3452F-4C27-2167-402F-91ADA96978D4";
	setAttr ".ihi" 0;
	setAttr -s 3 ".dnsm";
createNode nodeGraphEditorInfo -n "MayaNodeEditorSavedTabsInfo";
	rename -uid "EB24E0B7-4400-19AF-5C21-23A2A39A3CE6";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -612.61208074062199 -262.65893846196013 ;
	setAttr ".tgi[0].vh" -type "double2" 340.97192131666475 207.06199475481779 ;
	setAttr -s 18 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" -115.71428680419922;
	setAttr ".tgi[0].ni[0].y" -504.28570556640625;
	setAttr ".tgi[0].ni[0].nvs" 18304;
	setAttr ".tgi[0].ni[1].x" -115.71428680419922;
	setAttr ".tgi[0].ni[1].y" 240;
	setAttr ".tgi[0].ni[1].nvs" 18304;
	setAttr ".tgi[0].ni[2].x" -422.85714721679688;
	setAttr ".tgi[0].ni[2].y" -454.28570556640625;
	setAttr ".tgi[0].ni[2].nvs" 18304;
	setAttr ".tgi[0].ni[3].x" 191.42857360839844;
	setAttr ".tgi[0].ni[3].y" 341.42855834960938;
	setAttr ".tgi[0].ni[3].nvs" 18304;
	setAttr ".tgi[0].ni[4].x" -424.28570556640625;
	setAttr ".tgi[0].ni[4].y" 8.5714282989501953;
	setAttr ".tgi[0].ni[4].nvs" 18304;
	setAttr ".tgi[0].ni[5].x" -422.85714721679688;
	setAttr ".tgi[0].ni[5].y" -555.71429443359375;
	setAttr ".tgi[0].ni[5].nvs" 18304;
	setAttr ".tgi[0].ni[6].x" 191.42857360839844;
	setAttr ".tgi[0].ni[6].y" 240;
	setAttr ".tgi[0].ni[6].nvs" 18304;
	setAttr ".tgi[0].ni[7].x" -115.71428680419922;
	setAttr ".tgi[0].ni[7].y" -272.85714721679688;
	setAttr ".tgi[0].ni[7].nvs" 18304;
	setAttr ".tgi[0].ni[8].x" 191.42857360839844;
	setAttr ".tgi[0].ni[8].y" -272.85714721679688;
	setAttr ".tgi[0].ni[8].nvs" 18304;
	setAttr ".tgi[0].ni[9].x" -422.85714721679688;
	setAttr ".tgi[0].ni[9].y" -222.85714721679688;
	setAttr ".tgi[0].ni[9].nvs" 18304;
	setAttr ".tgi[0].ni[10].x" 191.42857360839844;
	setAttr ".tgi[0].ni[10].y" 138.57142639160156;
	setAttr ".tgi[0].ni[10].nvs" 18304;
	setAttr ".tgi[0].ni[11].x" -422.85714721679688;
	setAttr ".tgi[0].ni[11].y" -324.28570556640625;
	setAttr ".tgi[0].ni[11].nvs" 18304;
	setAttr ".tgi[0].ni[12].x" 191.42857360839844;
	setAttr ".tgi[0].ni[12].y" -504.28570556640625;
	setAttr ".tgi[0].ni[12].nvs" 18304;
	setAttr ".tgi[0].ni[13].x" -424.28570556640625;
	setAttr ".tgi[0].ni[13].y" -92.857139587402344;
	setAttr ".tgi[0].ni[13].nvs" 18304;
	setAttr ".tgi[0].ni[14].x" -425.71429443359375;
	setAttr ".tgi[0].ni[14].y" 190;
	setAttr ".tgi[0].ni[14].nvs" 18304;
	setAttr ".tgi[0].ni[15].x" -115.71428680419922;
	setAttr ".tgi[0].ni[15].y" -41.428569793701172;
	setAttr ".tgi[0].ni[15].nvs" 18304;
	setAttr ".tgi[0].ni[16].x" -425.71429443359375;
	setAttr ".tgi[0].ni[16].y" 291.42855834960938;
	setAttr ".tgi[0].ni[16].nvs" 18304;
	setAttr ".tgi[0].ni[17].x" 191.42857360839844;
	setAttr ".tgi[0].ni[17].y" -41.428569793701172;
	setAttr ".tgi[0].ni[17].nvs" 18304;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".msaa" yes;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 7 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 10 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 33 ".u";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr ".fs" 1;
	setAttr ".ef" 10;
	setAttr ".dss" -type "string" "lambert1";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :defaultColorMgtGlobals;
	setAttr ".cfe" yes;
	setAttr ".cfp" -type "string" "<MAYA_RESOURCES>/OCIO-configs/Maya-legacy/config.ocio";
	setAttr ".vtn" -type "string" "sRGB gamma (legacy)";
	setAttr ".vn" -type "string" "sRGB gamma";
	setAttr ".dn" -type "string" "legacy";
	setAttr ".wsn" -type "string" "scene-linear Rec 709/sRGB";
	setAttr ".ovt" no;
	setAttr ".povt" no;
	setAttr ".otn" -type "string" "sRGB gamma (legacy)";
	setAttr ".potn" -type "string" "sRGB gamma (legacy)";
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "CTRL_setShape.subC" "CTRL_setsubControlShape.v";
connectAttr "Transform_Ctrl.ms" "Transform_Ctrl.sx" -l on;
connectAttr "Transform_Ctrl.ms" "Transform_Ctrl.sy" -l on;
connectAttr "Transform_Ctrl.ms" "Transform_Ctrl.sz" -l on;
connectAttr "car_expression.out[0]" "LOC_drive.tx";
connectAttr "car_expression.out[1]" "LOC_drive.ty";
connectAttr "car_expression.out[2]" "LOC_drive.tz";
connectAttr "steerDriveDistance_MD.ox" "CNT_drive.steerDrive";
connectAttr "car_expression.out[3]" "CNT_drive.wheelDrive";
connectAttr "CNT_drive.pivot_drive_adjust" "GRP_offset_Pivotdrive.tz";
connectAttr "CNT_drive.lengthAdjust" "GRP_rearAxis_reset.tz";
connectAttr "PMA_bankPivot_R.o1" "GRP_bank_R.rpx";
connectAttr "unitConversion509.o" "GRP_bank_R.rz";
connectAttr "PMA_bankPivot_L.o1" "GRP_bank_L.rpx";
connectAttr "unitConversion508.o" "GRP_bank_L.rz";
connectAttr "MLD_pivotBodyCtrl.ox" "GRP_body_reset.tz";
connectAttr "unitConversion510.o" "CNT_body.mrxl";
connectAttr "unitConversion515.o" "CNT_body.mrzl";
connectAttr "unitConversion512.o" "CNT_body.xrxl";
connectAttr "unitConversion514.o" "CNT_body.xrzl";
connectAttr "wheel_MD.oz" "GRP_frontWheelAdjust.tz";
connectAttr "CNT_drive.widthAdjust" "GRP_l_frontWheel_reset.tx";
connectAttr "CNT_drive.wheels_controls_vis" "GRP_deformer_l_frontWheel.v";
connectAttr "plusMinusAverage3.o1" "GRP_deformer_l_frontWheel.tx";
connectAttr "plusMinusAverage4.o1" "GRP_deformer_l_frontWheel.ty";
connectAttr "CNT_drive.wheels_latticeSize_adjust" "GRP_deformer_l_frontWheel.sx"
		;
connectAttr "CNT_drive.wheels_latticeSize_adjust" "GRP_deformer_l_frontWheel.sy"
		;
connectAttr "CNT_drive.wheels_latticeSize_adjust" "GRP_deformer_l_frontWheel.sz"
		;
connectAttr "CNT_drive.wheels_lattice_vis" "GRP_ltc_l_frontWheel.v";
connectAttr "CLS_l_frontWheel_bot_02Cluster.og[0]" "LTC_l_frontWheelShape.li";
connectAttr "cluster9GroupId.id" "LTC_l_frontWheelShape.iog.og[0].gid";
connectAttr "cluster9Set.mwc" "LTC_l_frontWheelShape.iog.og[0].gco";
connectAttr "cluster10GroupId.id" "LTC_l_frontWheelShape.iog.og[2].gid";
connectAttr "cluster10Set.mwc" "LTC_l_frontWheelShape.iog.og[2].gco";
connectAttr "cluster11GroupId.id" "LTC_l_frontWheelShape.iog.og[3].gid";
connectAttr "cluster11Set.mwc" "LTC_l_frontWheelShape.iog.og[3].gco";
connectAttr "cluster12GroupId.id" "LTC_l_frontWheelShape.iog.og[4].gid";
connectAttr "cluster12Set.mwc" "LTC_l_frontWheelShape.iog.og[4].gco";
connectAttr "cluster13GroupId.id" "LTC_l_frontWheelShape.iog.og[5].gid";
connectAttr "cluster13Set.mwc" "LTC_l_frontWheelShape.iog.og[5].gco";
connectAttr "cluster14GroupId.id" "LTC_l_frontWheelShape.iog.og[6].gid";
connectAttr "cluster14Set.mwc" "LTC_l_frontWheelShape.iog.og[6].gco";
connectAttr "cluster15GroupId.id" "LTC_l_frontWheelShape.iog.og[7].gid";
connectAttr "cluster15Set.mwc" "LTC_l_frontWheelShape.iog.og[7].gco";
connectAttr "cluster16GroupId.id" "LTC_l_frontWheelShape.iog.og[8].gid";
connectAttr "cluster16Set.mwc" "LTC_l_frontWheelShape.iog.og[8].gco";
connectAttr "unitConversion487.o" "l_frontSteering_pt.ry";
connectAttr "l_frontWheel_display_parentConstraint1.ctx" "l_frontWheel_display.tx"
		;
connectAttr "l_frontWheel_display_parentConstraint1.cty" "l_frontWheel_display.ty"
		;
connectAttr "l_frontWheel_display_parentConstraint1.ctz" "l_frontWheel_display.tz"
		;
connectAttr "l_frontWheel_display_parentConstraint1.crx" "l_frontWheel_display.rx"
		;
connectAttr "l_frontWheel_display_parentConstraint1.cry" "l_frontWheel_display.ry"
		;
connectAttr "l_frontWheel_display_parentConstraint1.crz" "l_frontWheel_display.rz"
		;
connectAttr "wheelDisplay1.oc" "l_frontWheel_displayShape.cr";
connectAttr "l_frontWheel_display.ro" "l_frontWheel_display_parentConstraint1.cro"
		;
connectAttr "l_frontWheel_display.pim" "l_frontWheel_display_parentConstraint1.cpim"
		;
connectAttr "l_frontWheel_display.rp" "l_frontWheel_display_parentConstraint1.crp"
		;
connectAttr "l_frontWheel_display.rpt" "l_frontWheel_display_parentConstraint1.crt"
		;
connectAttr "JNT_l_frontWheel.t" "l_frontWheel_display_parentConstraint1.tg[0].tt"
		;
connectAttr "JNT_l_frontWheel.rp" "l_frontWheel_display_parentConstraint1.tg[0].trp"
		;
connectAttr "JNT_l_frontWheel.rpt" "l_frontWheel_display_parentConstraint1.tg[0].trt"
		;
connectAttr "JNT_l_frontWheel.r" "l_frontWheel_display_parentConstraint1.tg[0].tr"
		;
connectAttr "JNT_l_frontWheel.ro" "l_frontWheel_display_parentConstraint1.tg[0].tro"
		;
connectAttr "JNT_l_frontWheel.s" "l_frontWheel_display_parentConstraint1.tg[0].ts"
		;
connectAttr "JNT_l_frontWheel.pm" "l_frontWheel_display_parentConstraint1.tg[0].tpm"
		;
connectAttr "JNT_l_frontWheel.jo" "l_frontWheel_display_parentConstraint1.tg[0].tjo"
		;
connectAttr "JNT_l_frontWheel.ssc" "l_frontWheel_display_parentConstraint1.tg[0].tsc"
		;
connectAttr "JNT_l_frontWheel.is" "l_frontWheel_display_parentConstraint1.tg[0].tis"
		;
connectAttr "l_frontWheel_display_parentConstraint1.w0" "l_frontWheel_display_parentConstraint1.tg[0].tw"
		;
connectAttr "wheel_MD.ox" "GRP_r_frontWheel_reset.tx";
connectAttr "unitConversion488.o" "r_frontSteering_pt.ry";
connectAttr "r_frontWheel_display_parentConstraint1.ctx" "r_frontWheel_display.tx"
		;
connectAttr "r_frontWheel_display_parentConstraint1.cty" "r_frontWheel_display.ty"
		;
connectAttr "r_frontWheel_display_parentConstraint1.ctz" "r_frontWheel_display.tz"
		;
connectAttr "r_frontWheel_display_parentConstraint1.crx" "r_frontWheel_display.rx"
		;
connectAttr "r_frontWheel_display_parentConstraint1.cry" "r_frontWheel_display.ry"
		;
connectAttr "r_frontWheel_display_parentConstraint1.crz" "r_frontWheel_display.rz"
		;
connectAttr "wheelDisplay3.oc" "r_frontWheel_displayShape.cr";
connectAttr "r_frontWheel_display.ro" "r_frontWheel_display_parentConstraint1.cro"
		;
connectAttr "r_frontWheel_display.pim" "r_frontWheel_display_parentConstraint1.cpim"
		;
connectAttr "r_frontWheel_display.rp" "r_frontWheel_display_parentConstraint1.crp"
		;
connectAttr "r_frontWheel_display.rpt" "r_frontWheel_display_parentConstraint1.crt"
		;
connectAttr "JNT_r_frontWheel.t" "r_frontWheel_display_parentConstraint1.tg[0].tt"
		;
connectAttr "JNT_r_frontWheel.rp" "r_frontWheel_display_parentConstraint1.tg[0].trp"
		;
connectAttr "JNT_r_frontWheel.rpt" "r_frontWheel_display_parentConstraint1.tg[0].trt"
		;
connectAttr "JNT_r_frontWheel.r" "r_frontWheel_display_parentConstraint1.tg[0].tr"
		;
connectAttr "JNT_r_frontWheel.ro" "r_frontWheel_display_parentConstraint1.tg[0].tro"
		;
connectAttr "JNT_r_frontWheel.s" "r_frontWheel_display_parentConstraint1.tg[0].ts"
		;
connectAttr "JNT_r_frontWheel.pm" "r_frontWheel_display_parentConstraint1.tg[0].tpm"
		;
connectAttr "JNT_r_frontWheel.jo" "r_frontWheel_display_parentConstraint1.tg[0].tjo"
		;
connectAttr "JNT_r_frontWheel.ssc" "r_frontWheel_display_parentConstraint1.tg[0].tsc"
		;
connectAttr "JNT_r_frontWheel.is" "r_frontWheel_display_parentConstraint1.tg[0].tis"
		;
connectAttr "r_frontWheel_display_parentConstraint1.w0" "r_frontWheel_display_parentConstraint1.tg[0].tw"
		;
connectAttr "CNT_drive.wheels_controls_vis" "GRP_deformer_r_frontWheel.v";
connectAttr "steerDistance_and_invert_MD.oz" "GRP_deformer_r_frontWheel.tx";
connectAttr "plusMinusAverage4.o1" "GRP_deformer_r_frontWheel.ty";
connectAttr "CNT_drive.wheels_latticeSize_adjust" "GRP_deformer_r_frontWheel.sx"
		;
connectAttr "CNT_drive.wheels_latticeSize_adjust" "GRP_deformer_r_frontWheel.sy"
		;
connectAttr "CNT_drive.wheels_latticeSize_adjust" "GRP_deformer_r_frontWheel.sz"
		;
connectAttr "CNT_drive.wheels_lattice_vis" "GRP_ltc_r_frontWheel.v";
connectAttr "cluster33GroupId.id" "LTC_r_frontWheelShape.iog.og[9].gid";
connectAttr "cluster33Set.mwc" "LTC_r_frontWheelShape.iog.og[9].gco";
connectAttr "cluster34GroupId.id" "LTC_r_frontWheelShape.iog.og[11].gid";
connectAttr "cluster34Set.mwc" "LTC_r_frontWheelShape.iog.og[11].gco";
connectAttr "cluster35GroupId.id" "LTC_r_frontWheelShape.iog.og[12].gid";
connectAttr "cluster35Set.mwc" "LTC_r_frontWheelShape.iog.og[12].gco";
connectAttr "cluster36GroupId.id" "LTC_r_frontWheelShape.iog.og[13].gid";
connectAttr "cluster36Set.mwc" "LTC_r_frontWheelShape.iog.og[13].gco";
connectAttr "cluster37GroupId.id" "LTC_r_frontWheelShape.iog.og[14].gid";
connectAttr "cluster37Set.mwc" "LTC_r_frontWheelShape.iog.og[14].gco";
connectAttr "cluster38GroupId.id" "LTC_r_frontWheelShape.iog.og[15].gid";
connectAttr "cluster38Set.mwc" "LTC_r_frontWheelShape.iog.og[15].gco";
connectAttr "cluster39GroupId.id" "LTC_r_frontWheelShape.iog.og[16].gid";
connectAttr "cluster39Set.mwc" "LTC_r_frontWheelShape.iog.og[16].gco";
connectAttr "cluster40GroupId.id" "LTC_r_frontWheelShape.iog.og[17].gid";
connectAttr "cluster40Set.mwc" "LTC_r_frontWheelShape.iog.og[17].gco";
connectAttr "CLS_r_frontWheel_bot_04Cluster.og[0]" "LTC_r_frontWheelShape.li";
connectAttr "wheel_MD.ox" "GRP_r_rearWheel_reset.tx";
connectAttr "CNT_drive.wheels_controls_vis" "GRP_deformer_r_rearWheel.v";
connectAttr "steerDistance_and_invert_MD.oz" "GRP_deformer_r_rearWheel.tx";
connectAttr "plusMinusAverage4.o1" "GRP_deformer_r_rearWheel.ty";
connectAttr "CNT_drive.wheels_latticeSize_adjust" "GRP_deformer_r_rearWheel.sx";
connectAttr "CNT_drive.wheels_latticeSize_adjust" "GRP_deformer_r_rearWheel.sy";
connectAttr "CNT_drive.wheels_latticeSize_adjust" "GRP_deformer_r_rearWheel.sz";
connectAttr "CNT_drive.wheels_lattice_vis" "GRP_ltc_r_rearWheel.v";
connectAttr "CLS_r_rearWheel_bot_02Cluster.og[0]" "LTC_r_rearWheelShape.li";
connectAttr "cluster25GroupId.id" "LTC_r_rearWheelShape.iog.og[0].gid";
connectAttr "cluster25Set.mwc" "LTC_r_rearWheelShape.iog.og[0].gco";
connectAttr "cluster26GroupId.id" "LTC_r_rearWheelShape.iog.og[2].gid";
connectAttr "cluster26Set.mwc" "LTC_r_rearWheelShape.iog.og[2].gco";
connectAttr "cluster27GroupId.id" "LTC_r_rearWheelShape.iog.og[3].gid";
connectAttr "cluster27Set.mwc" "LTC_r_rearWheelShape.iog.og[3].gco";
connectAttr "cluster28GroupId.id" "LTC_r_rearWheelShape.iog.og[4].gid";
connectAttr "cluster28Set.mwc" "LTC_r_rearWheelShape.iog.og[4].gco";
connectAttr "cluster29GroupId.id" "LTC_r_rearWheelShape.iog.og[5].gid";
connectAttr "cluster29Set.mwc" "LTC_r_rearWheelShape.iog.og[5].gco";
connectAttr "cluster30GroupId.id" "LTC_r_rearWheelShape.iog.og[6].gid";
connectAttr "cluster30Set.mwc" "LTC_r_rearWheelShape.iog.og[6].gco";
connectAttr "cluster31GroupId.id" "LTC_r_rearWheelShape.iog.og[7].gid";
connectAttr "cluster31Set.mwc" "LTC_r_rearWheelShape.iog.og[7].gco";
connectAttr "cluster32GroupId.id" "LTC_r_rearWheelShape.iog.og[8].gid";
connectAttr "cluster32Set.mwc" "LTC_r_rearWheelShape.iog.og[8].gco";
connectAttr "r_rearWheel_display_parentConstraint1.ctx" "r_rearWheel_display.tx"
		;
connectAttr "r_rearWheel_display_parentConstraint1.cty" "r_rearWheel_display.ty"
		;
connectAttr "r_rearWheel_display_parentConstraint1.ctz" "r_rearWheel_display.tz"
		;
connectAttr "r_rearWheel_display_parentConstraint1.crx" "r_rearWheel_display.rx"
		;
connectAttr "r_rearWheel_display_parentConstraint1.cry" "r_rearWheel_display.ry"
		;
connectAttr "r_rearWheel_display_parentConstraint1.crz" "r_rearWheel_display.rz"
		;
connectAttr "wheelDisplay4.oc" "r_rearWheel_displayShape.cr";
connectAttr "r_rearWheel_display.ro" "r_rearWheel_display_parentConstraint1.cro"
		;
connectAttr "r_rearWheel_display.pim" "r_rearWheel_display_parentConstraint1.cpim"
		;
connectAttr "r_rearWheel_display.rp" "r_rearWheel_display_parentConstraint1.crp"
		;
connectAttr "r_rearWheel_display.rpt" "r_rearWheel_display_parentConstraint1.crt"
		;
connectAttr "JNT_r_rearWheel.t" "r_rearWheel_display_parentConstraint1.tg[0].tt"
		;
connectAttr "JNT_r_rearWheel.rp" "r_rearWheel_display_parentConstraint1.tg[0].trp"
		;
connectAttr "JNT_r_rearWheel.rpt" "r_rearWheel_display_parentConstraint1.tg[0].trt"
		;
connectAttr "JNT_r_rearWheel.r" "r_rearWheel_display_parentConstraint1.tg[0].tr"
		;
connectAttr "JNT_r_rearWheel.ro" "r_rearWheel_display_parentConstraint1.tg[0].tro"
		;
connectAttr "JNT_r_rearWheel.s" "r_rearWheel_display_parentConstraint1.tg[0].ts"
		;
connectAttr "JNT_r_rearWheel.pm" "r_rearWheel_display_parentConstraint1.tg[0].tpm"
		;
connectAttr "JNT_r_rearWheel.jo" "r_rearWheel_display_parentConstraint1.tg[0].tjo"
		;
connectAttr "JNT_r_rearWheel.ssc" "r_rearWheel_display_parentConstraint1.tg[0].tsc"
		;
connectAttr "JNT_r_rearWheel.is" "r_rearWheel_display_parentConstraint1.tg[0].tis"
		;
connectAttr "r_rearWheel_display_parentConstraint1.w0" "r_rearWheel_display_parentConstraint1.tg[0].tw"
		;
connectAttr "CNT_drive.widthAdjust" "GRP_l_rearWheel_reset.tx";
connectAttr "CNT_drive.wheels_controls_vis" "GRP_deformer_l_rearWheel.v";
connectAttr "plusMinusAverage3.o1" "GRP_deformer_l_rearWheel.tx";
connectAttr "plusMinusAverage4.o1" "GRP_deformer_l_rearWheel.ty";
connectAttr "CNT_drive.wheels_latticeSize_adjust" "GRP_deformer_l_rearWheel.sx";
connectAttr "CNT_drive.wheels_latticeSize_adjust" "GRP_deformer_l_rearWheel.sy";
connectAttr "CNT_drive.wheels_latticeSize_adjust" "GRP_deformer_l_rearWheel.sz";
connectAttr "CNT_drive.wheels_lattice_vis" "GRP_ltc_l_rearWheel.v";
connectAttr "CLS_l_rearWheel_top_02Cluster.og[0]" "LTC_l_rearWheelShape.li";
connectAttr "cluster17GroupId.id" "LTC_l_rearWheelShape.iog.og[0].gid";
connectAttr "cluster17Set.mwc" "LTC_l_rearWheelShape.iog.og[0].gco";
connectAttr "cluster18GroupId.id" "LTC_l_rearWheelShape.iog.og[2].gid";
connectAttr "cluster18Set.mwc" "LTC_l_rearWheelShape.iog.og[2].gco";
connectAttr "cluster19GroupId.id" "LTC_l_rearWheelShape.iog.og[3].gid";
connectAttr "cluster19Set.mwc" "LTC_l_rearWheelShape.iog.og[3].gco";
connectAttr "cluster20GroupId.id" "LTC_l_rearWheelShape.iog.og[4].gid";
connectAttr "cluster20Set.mwc" "LTC_l_rearWheelShape.iog.og[4].gco";
connectAttr "cluster21GroupId.id" "LTC_l_rearWheelShape.iog.og[5].gid";
connectAttr "cluster21Set.mwc" "LTC_l_rearWheelShape.iog.og[5].gco";
connectAttr "cluster22GroupId.id" "LTC_l_rearWheelShape.iog.og[6].gid";
connectAttr "cluster22Set.mwc" "LTC_l_rearWheelShape.iog.og[6].gco";
connectAttr "cluster23GroupId.id" "LTC_l_rearWheelShape.iog.og[7].gid";
connectAttr "cluster23Set.mwc" "LTC_l_rearWheelShape.iog.og[7].gco";
connectAttr "cluster24GroupId.id" "LTC_l_rearWheelShape.iog.og[8].gid";
connectAttr "cluster24Set.mwc" "LTC_l_rearWheelShape.iog.og[8].gco";
connectAttr "l_rearWheel_display_parentConstraint1.ctx" "l_rearWheel_display.tx"
		;
connectAttr "l_rearWheel_display_parentConstraint1.cty" "l_rearWheel_display.ty"
		;
connectAttr "l_rearWheel_display_parentConstraint1.ctz" "l_rearWheel_display.tz"
		;
connectAttr "l_rearWheel_display_parentConstraint1.crx" "l_rearWheel_display.rx"
		;
connectAttr "l_rearWheel_display_parentConstraint1.cry" "l_rearWheel_display.ry"
		;
connectAttr "l_rearWheel_display_parentConstraint1.crz" "l_rearWheel_display.rz"
		;
connectAttr "wheelDisplay2.oc" "l_rearWheel_displayShape.cr";
connectAttr "l_rearWheel_display.ro" "l_rearWheel_display_parentConstraint1.cro"
		;
connectAttr "l_rearWheel_display.pim" "l_rearWheel_display_parentConstraint1.cpim"
		;
connectAttr "l_rearWheel_display.rp" "l_rearWheel_display_parentConstraint1.crp"
		;
connectAttr "l_rearWheel_display.rpt" "l_rearWheel_display_parentConstraint1.crt"
		;
connectAttr "JNT_l_rearWheel.t" "l_rearWheel_display_parentConstraint1.tg[0].tt"
		;
connectAttr "JNT_l_rearWheel.rp" "l_rearWheel_display_parentConstraint1.tg[0].trp"
		;
connectAttr "JNT_l_rearWheel.rpt" "l_rearWheel_display_parentConstraint1.tg[0].trt"
		;
connectAttr "JNT_l_rearWheel.r" "l_rearWheel_display_parentConstraint1.tg[0].tr"
		;
connectAttr "JNT_l_rearWheel.ro" "l_rearWheel_display_parentConstraint1.tg[0].tro"
		;
connectAttr "JNT_l_rearWheel.s" "l_rearWheel_display_parentConstraint1.tg[0].ts"
		;
connectAttr "JNT_l_rearWheel.pm" "l_rearWheel_display_parentConstraint1.tg[0].tpm"
		;
connectAttr "JNT_l_rearWheel.jo" "l_rearWheel_display_parentConstraint1.tg[0].tjo"
		;
connectAttr "JNT_l_rearWheel.ssc" "l_rearWheel_display_parentConstraint1.tg[0].tsc"
		;
connectAttr "JNT_l_rearWheel.is" "l_rearWheel_display_parentConstraint1.tg[0].tis"
		;
connectAttr "l_rearWheel_display_parentConstraint1.w0" "l_rearWheel_display_parentConstraint1.tg[0].tw"
		;
connectAttr "GRP_offset_pivot_locators_parentConstraint1.ctx" "GRP_offset_pivot_locators.tx"
		;
connectAttr "GRP_offset_pivot_locators_parentConstraint1.cty" "GRP_offset_pivot_locators.ty"
		;
connectAttr "GRP_offset_pivot_locators_parentConstraint1.ctz" "GRP_offset_pivot_locators.tz"
		;
connectAttr "GRP_offset_pivot_locators_parentConstraint1.crx" "GRP_offset_pivot_locators.rx"
		;
connectAttr "GRP_offset_pivot_locators_parentConstraint1.cry" "GRP_offset_pivot_locators.ry"
		;
connectAttr "GRP_offset_pivot_locators_parentConstraint1.crz" "GRP_offset_pivot_locators.rz"
		;
connectAttr "CNT_drive.bank_pivot_adjust_vis" "GRP_offset_pivot_locators.v";
connectAttr "MLD_bankPivot.oy" "GRP_PivotBank_R.tx";
connectAttr "wheel_MD.oz" "GRP_PivotBank_R.tz";
connectAttr "MLD_bankPivot.ox" "GRP_PivotBank_L.tx";
connectAttr "wheel_MD.oz" "GRP_PivotBank_L.tz";
connectAttr "GRP_offset_pivot_locators.ro" "GRP_offset_pivot_locators_parentConstraint1.cro"
		;
connectAttr "GRP_offset_pivot_locators.pim" "GRP_offset_pivot_locators_parentConstraint1.cpim"
		;
connectAttr "GRP_offset_pivot_locators.rp" "GRP_offset_pivot_locators_parentConstraint1.crp"
		;
connectAttr "GRP_offset_pivot_locators.rpt" "GRP_offset_pivot_locators_parentConstraint1.crt"
		;
connectAttr "CNT_rearAxis.t" "GRP_offset_pivot_locators_parentConstraint1.tg[0].tt"
		;
connectAttr "CNT_rearAxis.rp" "GRP_offset_pivot_locators_parentConstraint1.tg[0].trp"
		;
connectAttr "CNT_rearAxis.rpt" "GRP_offset_pivot_locators_parentConstraint1.tg[0].trt"
		;
connectAttr "CNT_rearAxis.r" "GRP_offset_pivot_locators_parentConstraint1.tg[0].tr"
		;
connectAttr "CNT_rearAxis.ro" "GRP_offset_pivot_locators_parentConstraint1.tg[0].tro"
		;
connectAttr "CNT_rearAxis.s" "GRP_offset_pivot_locators_parentConstraint1.tg[0].ts"
		;
connectAttr "CNT_rearAxis.pm" "GRP_offset_pivot_locators_parentConstraint1.tg[0].tpm"
		;
connectAttr "GRP_offset_pivot_locators_parentConstraint1.w0" "GRP_offset_pivot_locators_parentConstraint1.tg[0].tw"
		;
connectAttr "JNT_root_parentConstraint1.ctx" "JNT_root.tx";
connectAttr "JNT_root_parentConstraint1.cty" "JNT_root.ty";
connectAttr "JNT_root_parentConstraint1.ctz" "JNT_root.tz";
connectAttr "JNT_root_parentConstraint1.crx" "JNT_root.rx";
connectAttr "JNT_root_parentConstraint1.cry" "JNT_root.ry";
connectAttr "JNT_root_parentConstraint1.crz" "JNT_root.rz";
connectAttr "JNT_root_scaleConstraint1.csx" "JNT_root.sx";
connectAttr "JNT_root_scaleConstraint1.csy" "JNT_root.sy";
connectAttr "JNT_root_scaleConstraint1.csz" "JNT_root.sz";
connectAttr "JNT_root.ro" "JNT_root_parentConstraint1.cro";
connectAttr "JNT_root.pim" "JNT_root_parentConstraint1.cpim";
connectAttr "JNT_root.rp" "JNT_root_parentConstraint1.crp";
connectAttr "JNT_root.rpt" "JNT_root_parentConstraint1.crt";
connectAttr "JNT_root.jo" "JNT_root_parentConstraint1.cjo";
connectAttr "CNT_drive.t" "JNT_root_parentConstraint1.tg[0].tt";
connectAttr "CNT_drive.rp" "JNT_root_parentConstraint1.tg[0].trp";
connectAttr "CNT_drive.rpt" "JNT_root_parentConstraint1.tg[0].trt";
connectAttr "CNT_drive.r" "JNT_root_parentConstraint1.tg[0].tr";
connectAttr "CNT_drive.ro" "JNT_root_parentConstraint1.tg[0].tro";
connectAttr "CNT_drive.s" "JNT_root_parentConstraint1.tg[0].ts";
connectAttr "CNT_drive.pm" "JNT_root_parentConstraint1.tg[0].tpm";
connectAttr "JNT_root_parentConstraint1.w0" "JNT_root_parentConstraint1.tg[0].tw"
		;
connectAttr "JNT_root.pim" "JNT_root_scaleConstraint1.cpim";
connectAttr "CNT_drive.s" "JNT_root_scaleConstraint1.tg[0].ts";
connectAttr "CNT_drive.pm" "JNT_root_scaleConstraint1.tg[0].tpm";
connectAttr "JNT_root_scaleConstraint1.w0" "JNT_root_scaleConstraint1.tg[0].tw";
connectAttr "JNT_root.s" "JNT_chasis.is";
connectAttr "CNT_drive.wheelRadius" "JNT_chasis.ty";
connectAttr "JNT_chasis_parentConstraint1.ctx" "JNT_chasis.tx";
connectAttr "JNT_chasis_parentConstraint1.ctz" "JNT_chasis.tz";
connectAttr "JNT_chasis_parentConstraint1.crx" "JNT_chasis.rx";
connectAttr "JNT_chasis_parentConstraint1.cry" "JNT_chasis.ry";
connectAttr "JNT_chasis_parentConstraint1.crz" "JNT_chasis.rz";
connectAttr "JNT_chasis_scaleConstraint1.csx" "JNT_chasis.sx";
connectAttr "JNT_chasis_scaleConstraint1.csy" "JNT_chasis.sy";
connectAttr "JNT_chasis_scaleConstraint1.csz" "JNT_chasis.sz";
connectAttr "GRP_offset_joints_parentConstraint1.ctx" "GRP_offset_joints.tx";
connectAttr "GRP_offset_joints_parentConstraint1.cty" "GRP_offset_joints.ty";
connectAttr "GRP_offset_joints_parentConstraint1.ctz" "GRP_offset_joints.tz";
connectAttr "GRP_offset_joints_parentConstraint1.crx" "GRP_offset_joints.rx";
connectAttr "GRP_offset_joints_parentConstraint1.cry" "GRP_offset_joints.ry";
connectAttr "GRP_offset_joints_parentConstraint1.crz" "GRP_offset_joints.rz";
connectAttr "GRP_offset_joints.ro" "GRP_offset_joints_parentConstraint1.cro";
connectAttr "GRP_offset_joints.pim" "GRP_offset_joints_parentConstraint1.cpim";
connectAttr "GRP_offset_joints.rp" "GRP_offset_joints_parentConstraint1.crp";
connectAttr "GRP_offset_joints.rpt" "GRP_offset_joints_parentConstraint1.crt";
connectAttr "GRP_offset_drive.t" "GRP_offset_joints_parentConstraint1.tg[0].tt";
connectAttr "GRP_offset_drive.rp" "GRP_offset_joints_parentConstraint1.tg[0].trp"
		;
connectAttr "GRP_offset_drive.rpt" "GRP_offset_joints_parentConstraint1.tg[0].trt"
		;
connectAttr "GRP_offset_drive.r" "GRP_offset_joints_parentConstraint1.tg[0].tr";
connectAttr "GRP_offset_drive.ro" "GRP_offset_joints_parentConstraint1.tg[0].tro"
		;
connectAttr "GRP_offset_drive.s" "GRP_offset_joints_parentConstraint1.tg[0].ts";
connectAttr "GRP_offset_drive.pm" "GRP_offset_joints_parentConstraint1.tg[0].tpm"
		;
connectAttr "GRP_offset_joints_parentConstraint1.w0" "GRP_offset_joints_parentConstraint1.tg[0].tw"
		;
connectAttr "plusMinusAverage2.o1" "GRP_offset_body.ty";
connectAttr "CNT_body.ty" "JNT_body.ty";
connectAttr "CNT_body.rx" "JNT_body.rx";
connectAttr "CNT_body.rz" "JNT_body.rz";
connectAttr "JNT_body.s" "JNT_l_frontWidth.is";
connectAttr "frontAxleAdjust_PMA.o1" "JNT_l_frontWidth.tz";
connectAttr "CNT_drive.widthAdjust" "JNT_l_frontWidth.tx";
connectAttr "JNT_l_frontWidth.s" "JNT_l_frontUpperArm.is";
connectAttr "JNT_l_frontArm.rz" "JNT_l_frontUpperArm.rz";
connectAttr "l_frontUpperSpring_jt_aimConstraint1.crz" "JNT_l_frontUpperSpring.rz"
		;
connectAttr "JNT_l_frontUpperArm.s" "JNT_l_frontUpperSpring.is";
connectAttr "JNT_l_frontUpperSpring.pim" "l_frontUpperSpring_jt_aimConstraint1.cpim"
		;
connectAttr "JNT_l_frontUpperSpring.t" "l_frontUpperSpring_jt_aimConstraint1.ct"
		;
connectAttr "JNT_l_frontUpperSpring.rp" "l_frontUpperSpring_jt_aimConstraint1.crp"
		;
connectAttr "JNT_l_frontUpperSpring.rpt" "l_frontUpperSpring_jt_aimConstraint1.crt"
		;
connectAttr "JNT_l_frontUpperSpring.ro" "l_frontUpperSpring_jt_aimConstraint1.cro"
		;
connectAttr "JNT_l_frontUpperSpring.jo" "l_frontUpperSpring_jt_aimConstraint1.cjo"
		;
connectAttr "JNT_l_frontUpperSpring.is" "l_frontUpperSpring_jt_aimConstraint1.is"
		;
connectAttr "JNT_l_frontLowerSpring.t" "l_frontUpperSpring_jt_aimConstraint1.tg[0].tt"
		;
connectAttr "JNT_l_frontLowerSpring.rp" "l_frontUpperSpring_jt_aimConstraint1.tg[0].trp"
		;
connectAttr "JNT_l_frontLowerSpring.rpt" "l_frontUpperSpring_jt_aimConstraint1.tg[0].trt"
		;
connectAttr "JNT_l_frontLowerSpring.pm" "l_frontUpperSpring_jt_aimConstraint1.tg[0].tpm"
		;
connectAttr "l_frontUpperSpring_jt_aimConstraint1.w0" "l_frontUpperSpring_jt_aimConstraint1.tg[0].tw"
		;
connectAttr "l_frontSpring_upvector_pt.wm" "l_frontUpperSpring_jt_aimConstraint1.wum"
		;
connectAttr "l_frontArm_jt_aimConstraint1.crz" "JNT_l_frontArm.rz";
connectAttr "JNT_l_frontWidth.s" "JNT_l_frontArm.is";
connectAttr "JNT_l_frontArm.pim" "l_frontArm_jt_aimConstraint1.cpim";
connectAttr "JNT_l_frontArm.t" "l_frontArm_jt_aimConstraint1.ct";
connectAttr "JNT_l_frontArm.rp" "l_frontArm_jt_aimConstraint1.crp";
connectAttr "JNT_l_frontArm.rpt" "l_frontArm_jt_aimConstraint1.crt";
connectAttr "JNT_l_frontArm.ro" "l_frontArm_jt_aimConstraint1.cro";
connectAttr "JNT_l_frontArm.jo" "l_frontArm_jt_aimConstraint1.cjo";
connectAttr "JNT_l_frontArm.is" "l_frontArm_jt_aimConstraint1.is";
connectAttr "JNT_l_frontBall.t" "l_frontArm_jt_aimConstraint1.tg[0].tt";
connectAttr "JNT_l_frontBall.rp" "l_frontArm_jt_aimConstraint1.tg[0].trp";
connectAttr "JNT_l_frontBall.rpt" "l_frontArm_jt_aimConstraint1.tg[0].trt";
connectAttr "JNT_l_frontBall.pm" "l_frontArm_jt_aimConstraint1.tg[0].tpm";
connectAttr "l_frontArm_jt_aimConstraint1.w0" "l_frontArm_jt_aimConstraint1.tg[0].tw"
		;
connectAttr "l_frontWheel_upvector_pt.wm" "l_frontArm_jt_aimConstraint1.wum";
connectAttr "JNT_l_frontWidth.s" "JNT_l_frontLowerArm.is";
connectAttr "JNT_l_frontArm.rz" "JNT_l_frontLowerArm.rz";
connectAttr "JNT_l_frontLowerArm.s" "JNT_l_frontLowerBall.is";
connectAttr "unitConversion494.o" "JNT_l_frontLowerBall.rz";
connectAttr "l_frontLowerSpring_jt_aimConstraint1.crz" "JNT_l_frontLowerSpring.rz"
		;
connectAttr "JNT_l_frontLowerBall.s" "JNT_l_frontLowerSpring.is";
connectAttr "JNT_l_frontLowerSpring.pim" "l_frontLowerSpring_jt_aimConstraint1.cpim"
		;
connectAttr "JNT_l_frontLowerSpring.t" "l_frontLowerSpring_jt_aimConstraint1.ct"
		;
connectAttr "JNT_l_frontLowerSpring.rp" "l_frontLowerSpring_jt_aimConstraint1.crp"
		;
connectAttr "JNT_l_frontLowerSpring.rpt" "l_frontLowerSpring_jt_aimConstraint1.crt"
		;
connectAttr "JNT_l_frontLowerSpring.ro" "l_frontLowerSpring_jt_aimConstraint1.cro"
		;
connectAttr "JNT_l_frontLowerSpring.jo" "l_frontLowerSpring_jt_aimConstraint1.cjo"
		;
connectAttr "JNT_l_frontLowerSpring.is" "l_frontLowerSpring_jt_aimConstraint1.is"
		;
connectAttr "JNT_l_frontUpperSpring.t" "l_frontLowerSpring_jt_aimConstraint1.tg[0].tt"
		;
connectAttr "JNT_l_frontUpperSpring.rp" "l_frontLowerSpring_jt_aimConstraint1.tg[0].trp"
		;
connectAttr "JNT_l_frontUpperSpring.rpt" "l_frontLowerSpring_jt_aimConstraint1.tg[0].trt"
		;
connectAttr "JNT_l_frontUpperSpring.pm" "l_frontLowerSpring_jt_aimConstraint1.tg[0].tpm"
		;
connectAttr "l_frontLowerSpring_jt_aimConstraint1.w0" "l_frontLowerSpring_jt_aimConstraint1.tg[0].tw"
		;
connectAttr "l_frontSpring_upvector_pt.wm" "l_frontLowerSpring_jt_aimConstraint1.wum"
		;
connectAttr "JNT_body.s" "JNT_r_frontWidth.is";
connectAttr "frontAxleAdjust_PMA.o1" "JNT_r_frontWidth.tz";
connectAttr "wheel_MD.ox" "JNT_r_frontWidth.tx";
connectAttr "JNT_r_frontWidth.s" "JNT_r_frontUpperArm.is";
connectAttr "JNT_r_frontArm.rz" "JNT_r_frontUpperArm.rz";
connectAttr "r_frontUpperSpring_jt_aimConstraint1.crz" "JNT_r_frontUpperSpring.rz"
		;
connectAttr "JNT_r_frontUpperArm.s" "JNT_r_frontUpperSpring.is";
connectAttr "JNT_r_frontUpperSpring.pim" "r_frontUpperSpring_jt_aimConstraint1.cpim"
		;
connectAttr "JNT_r_frontUpperSpring.t" "r_frontUpperSpring_jt_aimConstraint1.ct"
		;
connectAttr "JNT_r_frontUpperSpring.rp" "r_frontUpperSpring_jt_aimConstraint1.crp"
		;
connectAttr "JNT_r_frontUpperSpring.rpt" "r_frontUpperSpring_jt_aimConstraint1.crt"
		;
connectAttr "JNT_r_frontUpperSpring.ro" "r_frontUpperSpring_jt_aimConstraint1.cro"
		;
connectAttr "JNT_r_frontUpperSpring.jo" "r_frontUpperSpring_jt_aimConstraint1.cjo"
		;
connectAttr "JNT_r_frontUpperSpring.is" "r_frontUpperSpring_jt_aimConstraint1.is"
		;
connectAttr "JNT_r_frontLowerSpring.t" "r_frontUpperSpring_jt_aimConstraint1.tg[0].tt"
		;
connectAttr "JNT_r_frontLowerSpring.rp" "r_frontUpperSpring_jt_aimConstraint1.tg[0].trp"
		;
connectAttr "JNT_r_frontLowerSpring.rpt" "r_frontUpperSpring_jt_aimConstraint1.tg[0].trt"
		;
connectAttr "JNT_r_frontLowerSpring.pm" "r_frontUpperSpring_jt_aimConstraint1.tg[0].tpm"
		;
connectAttr "r_frontUpperSpring_jt_aimConstraint1.w0" "r_frontUpperSpring_jt_aimConstraint1.tg[0].tw"
		;
connectAttr "r_frontSpring_upvector_pt.wm" "r_frontUpperSpring_jt_aimConstraint1.wum"
		;
connectAttr "r_frontArm_jt_aimConstraint1.crz" "JNT_r_frontArm.rz";
connectAttr "JNT_r_frontWidth.s" "JNT_r_frontArm.is";
connectAttr "JNT_r_frontArm.pim" "r_frontArm_jt_aimConstraint1.cpim";
connectAttr "JNT_r_frontArm.t" "r_frontArm_jt_aimConstraint1.ct";
connectAttr "JNT_r_frontArm.rp" "r_frontArm_jt_aimConstraint1.crp";
connectAttr "JNT_r_frontArm.rpt" "r_frontArm_jt_aimConstraint1.crt";
connectAttr "JNT_r_frontArm.ro" "r_frontArm_jt_aimConstraint1.cro";
connectAttr "JNT_r_frontArm.jo" "r_frontArm_jt_aimConstraint1.cjo";
connectAttr "JNT_r_frontArm.is" "r_frontArm_jt_aimConstraint1.is";
connectAttr "JNT_r_frontBall.t" "r_frontArm_jt_aimConstraint1.tg[0].tt";
connectAttr "JNT_r_frontBall.rp" "r_frontArm_jt_aimConstraint1.tg[0].trp";
connectAttr "JNT_r_frontBall.rpt" "r_frontArm_jt_aimConstraint1.tg[0].trt";
connectAttr "JNT_r_frontBall.pm" "r_frontArm_jt_aimConstraint1.tg[0].tpm";
connectAttr "r_frontArm_jt_aimConstraint1.w0" "r_frontArm_jt_aimConstraint1.tg[0].tw"
		;
connectAttr "r_frontWheel_upvector_pt.wm" "r_frontArm_jt_aimConstraint1.wum";
connectAttr "JNT_r_frontWidth.s" "JNT_r_frontLowerArm.is";
connectAttr "JNT_r_frontArm.rz" "JNT_r_frontLowerArm.rz";
connectAttr "JNT_r_frontLowerArm.s" "JNT_r_frontLowerBall.is";
connectAttr "unitConversion496.o" "JNT_r_frontLowerBall.rz";
connectAttr "r_frontLowerSpring_jt_aimConstraint1.crz" "JNT_r_frontLowerSpring.rz"
		;
connectAttr "JNT_r_frontLowerBall.s" "JNT_r_frontLowerSpring.is";
connectAttr "JNT_r_frontLowerSpring.pim" "r_frontLowerSpring_jt_aimConstraint1.cpim"
		;
connectAttr "JNT_r_frontLowerSpring.t" "r_frontLowerSpring_jt_aimConstraint1.ct"
		;
connectAttr "JNT_r_frontLowerSpring.rp" "r_frontLowerSpring_jt_aimConstraint1.crp"
		;
connectAttr "JNT_r_frontLowerSpring.rpt" "r_frontLowerSpring_jt_aimConstraint1.crt"
		;
connectAttr "JNT_r_frontLowerSpring.ro" "r_frontLowerSpring_jt_aimConstraint1.cro"
		;
connectAttr "JNT_r_frontLowerSpring.jo" "r_frontLowerSpring_jt_aimConstraint1.cjo"
		;
connectAttr "JNT_r_frontLowerSpring.is" "r_frontLowerSpring_jt_aimConstraint1.is"
		;
connectAttr "JNT_r_frontUpperSpring.t" "r_frontLowerSpring_jt_aimConstraint1.tg[0].tt"
		;
connectAttr "JNT_r_frontUpperSpring.rp" "r_frontLowerSpring_jt_aimConstraint1.tg[0].trp"
		;
connectAttr "JNT_r_frontUpperSpring.rpt" "r_frontLowerSpring_jt_aimConstraint1.tg[0].trt"
		;
connectAttr "JNT_r_frontUpperSpring.pm" "r_frontLowerSpring_jt_aimConstraint1.tg[0].tpm"
		;
connectAttr "r_frontLowerSpring_jt_aimConstraint1.w0" "r_frontLowerSpring_jt_aimConstraint1.tg[0].tw"
		;
connectAttr "r_frontSpring_upvector_pt.wm" "r_frontLowerSpring_jt_aimConstraint1.wum"
		;
connectAttr "JNT_body.s" "JNT_l_rearWidth.is";
connectAttr "CNT_drive.widthAdjust" "JNT_l_rearWidth.tx";
connectAttr "JNT_l_rearWidth.s" "JNT_l_rearUpperArm.is";
connectAttr "JNT_l_rearArm.rz" "JNT_l_rearUpperArm.rz";
connectAttr "l_rearUpperSpring_jt_aimConstraint1.crz" "JNT_l_rearUpperSpring.rz"
		;
connectAttr "JNT_l_rearUpperArm.s" "JNT_l_rearUpperSpring.is";
connectAttr "JNT_l_rearUpperSpring.pim" "l_rearUpperSpring_jt_aimConstraint1.cpim"
		;
connectAttr "JNT_l_rearUpperSpring.t" "l_rearUpperSpring_jt_aimConstraint1.ct";
connectAttr "JNT_l_rearUpperSpring.rp" "l_rearUpperSpring_jt_aimConstraint1.crp"
		;
connectAttr "JNT_l_rearUpperSpring.rpt" "l_rearUpperSpring_jt_aimConstraint1.crt"
		;
connectAttr "JNT_l_rearUpperSpring.ro" "l_rearUpperSpring_jt_aimConstraint1.cro"
		;
connectAttr "JNT_l_rearUpperSpring.jo" "l_rearUpperSpring_jt_aimConstraint1.cjo"
		;
connectAttr "JNT_l_rearUpperSpring.is" "l_rearUpperSpring_jt_aimConstraint1.is";
connectAttr "JNT_l_rearLowerSpring.t" "l_rearUpperSpring_jt_aimConstraint1.tg[0].tt"
		;
connectAttr "JNT_l_rearLowerSpring.rp" "l_rearUpperSpring_jt_aimConstraint1.tg[0].trp"
		;
connectAttr "JNT_l_rearLowerSpring.rpt" "l_rearUpperSpring_jt_aimConstraint1.tg[0].trt"
		;
connectAttr "JNT_l_rearLowerSpring.pm" "l_rearUpperSpring_jt_aimConstraint1.tg[0].tpm"
		;
connectAttr "l_rearUpperSpring_jt_aimConstraint1.w0" "l_rearUpperSpring_jt_aimConstraint1.tg[0].tw"
		;
connectAttr "l_rearSpring_upvector_pt.wm" "l_rearUpperSpring_jt_aimConstraint1.wum"
		;
connectAttr "l_rearArm_jt_aimConstraint1.crz" "JNT_l_rearArm.rz";
connectAttr "JNT_l_rearWidth.s" "JNT_l_rearArm.is";
connectAttr "JNT_l_rearArm.pim" "l_rearArm_jt_aimConstraint1.cpim";
connectAttr "JNT_l_rearArm.t" "l_rearArm_jt_aimConstraint1.ct";
connectAttr "JNT_l_rearArm.rp" "l_rearArm_jt_aimConstraint1.crp";
connectAttr "JNT_l_rearArm.rpt" "l_rearArm_jt_aimConstraint1.crt";
connectAttr "JNT_l_rearArm.ro" "l_rearArm_jt_aimConstraint1.cro";
connectAttr "JNT_l_rearArm.jo" "l_rearArm_jt_aimConstraint1.cjo";
connectAttr "JNT_l_rearArm.is" "l_rearArm_jt_aimConstraint1.is";
connectAttr "JNT_l_reartBall.t" "l_rearArm_jt_aimConstraint1.tg[0].tt";
connectAttr "JNT_l_reartBall.rp" "l_rearArm_jt_aimConstraint1.tg[0].trp";
connectAttr "JNT_l_reartBall.rpt" "l_rearArm_jt_aimConstraint1.tg[0].trt";
connectAttr "JNT_l_reartBall.pm" "l_rearArm_jt_aimConstraint1.tg[0].tpm";
connectAttr "l_rearArm_jt_aimConstraint1.w0" "l_rearArm_jt_aimConstraint1.tg[0].tw"
		;
connectAttr "l_rearWheel_upvector_pt.wm" "l_rearArm_jt_aimConstraint1.wum";
connectAttr "JNT_l_rearWidth.s" "JNT_l_rearLowerArm.is";
connectAttr "JNT_l_rearArm.rz" "JNT_l_rearLowerArm.rz";
connectAttr "JNT_l_rearLowerArm.s" "JNT_l_rearLowerBall.is";
connectAttr "unitConversion498.o" "JNT_l_rearLowerBall.rz";
connectAttr "l_rearLowerSpring_jt_aimConstraint1.crz" "JNT_l_rearLowerSpring.rz"
		;
connectAttr "JNT_l_rearLowerBall.s" "JNT_l_rearLowerSpring.is";
connectAttr "JNT_l_rearLowerSpring.pim" "l_rearLowerSpring_jt_aimConstraint1.cpim"
		;
connectAttr "JNT_l_rearLowerSpring.t" "l_rearLowerSpring_jt_aimConstraint1.ct";
connectAttr "JNT_l_rearLowerSpring.rp" "l_rearLowerSpring_jt_aimConstraint1.crp"
		;
connectAttr "JNT_l_rearLowerSpring.rpt" "l_rearLowerSpring_jt_aimConstraint1.crt"
		;
connectAttr "JNT_l_rearLowerSpring.ro" "l_rearLowerSpring_jt_aimConstraint1.cro"
		;
connectAttr "JNT_l_rearLowerSpring.jo" "l_rearLowerSpring_jt_aimConstraint1.cjo"
		;
connectAttr "JNT_l_rearLowerSpring.is" "l_rearLowerSpring_jt_aimConstraint1.is";
connectAttr "JNT_l_rearUpperSpring.t" "l_rearLowerSpring_jt_aimConstraint1.tg[0].tt"
		;
connectAttr "JNT_l_rearUpperSpring.rp" "l_rearLowerSpring_jt_aimConstraint1.tg[0].trp"
		;
connectAttr "JNT_l_rearUpperSpring.rpt" "l_rearLowerSpring_jt_aimConstraint1.tg[0].trt"
		;
connectAttr "JNT_l_rearUpperSpring.pm" "l_rearLowerSpring_jt_aimConstraint1.tg[0].tpm"
		;
connectAttr "l_rearLowerSpring_jt_aimConstraint1.w0" "l_rearLowerSpring_jt_aimConstraint1.tg[0].tw"
		;
connectAttr "l_rearSpring_upvector_pt.wm" "l_rearLowerSpring_jt_aimConstraint1.wum"
		;
connectAttr "JNT_body.s" "JNT_r_rearWidth.is";
connectAttr "wheel_MD.ox" "JNT_r_rearWidth.tx";
connectAttr "JNT_r_rearWidth.s" "JNT_r_rearUpperArm.is";
connectAttr "JNT_r_rearArm.rz" "JNT_r_rearUpperArm.rz";
connectAttr "r_rearUpperSpring_jt_aimConstraint1.crz" "JNT_r_rearUpperSpring.rz"
		;
connectAttr "JNT_r_rearUpperArm.s" "JNT_r_rearUpperSpring.is";
connectAttr "JNT_r_rearUpperSpring.pim" "r_rearUpperSpring_jt_aimConstraint1.cpim"
		;
connectAttr "JNT_r_rearUpperSpring.t" "r_rearUpperSpring_jt_aimConstraint1.ct";
connectAttr "JNT_r_rearUpperSpring.rp" "r_rearUpperSpring_jt_aimConstraint1.crp"
		;
connectAttr "JNT_r_rearUpperSpring.rpt" "r_rearUpperSpring_jt_aimConstraint1.crt"
		;
connectAttr "JNT_r_rearUpperSpring.ro" "r_rearUpperSpring_jt_aimConstraint1.cro"
		;
connectAttr "JNT_r_rearUpperSpring.jo" "r_rearUpperSpring_jt_aimConstraint1.cjo"
		;
connectAttr "JNT_r_rearUpperSpring.is" "r_rearUpperSpring_jt_aimConstraint1.is";
connectAttr "JNT_r_rearLowerSpring.t" "r_rearUpperSpring_jt_aimConstraint1.tg[0].tt"
		;
connectAttr "JNT_r_rearLowerSpring.rp" "r_rearUpperSpring_jt_aimConstraint1.tg[0].trp"
		;
connectAttr "JNT_r_rearLowerSpring.rpt" "r_rearUpperSpring_jt_aimConstraint1.tg[0].trt"
		;
connectAttr "JNT_r_rearLowerSpring.pm" "r_rearUpperSpring_jt_aimConstraint1.tg[0].tpm"
		;
connectAttr "r_rearUpperSpring_jt_aimConstraint1.w0" "r_rearUpperSpring_jt_aimConstraint1.tg[0].tw"
		;
connectAttr "r_rearSpring_upvector_pt.wm" "r_rearUpperSpring_jt_aimConstraint1.wum"
		;
connectAttr "r_rearArm_jt_aimConstraint1.crz" "JNT_r_rearArm.rz";
connectAttr "JNT_r_rearWidth.s" "JNT_r_rearArm.is";
connectAttr "JNT_r_rearArm.pim" "r_rearArm_jt_aimConstraint1.cpim";
connectAttr "JNT_r_rearArm.t" "r_rearArm_jt_aimConstraint1.ct";
connectAttr "JNT_r_rearArm.rp" "r_rearArm_jt_aimConstraint1.crp";
connectAttr "JNT_r_rearArm.rpt" "r_rearArm_jt_aimConstraint1.crt";
connectAttr "JNT_r_rearArm.ro" "r_rearArm_jt_aimConstraint1.cro";
connectAttr "JNT_r_rearArm.jo" "r_rearArm_jt_aimConstraint1.cjo";
connectAttr "JNT_r_rearArm.is" "r_rearArm_jt_aimConstraint1.is";
connectAttr "JNT_r_rearBall.t" "r_rearArm_jt_aimConstraint1.tg[0].tt";
connectAttr "JNT_r_rearBall.rp" "r_rearArm_jt_aimConstraint1.tg[0].trp";
connectAttr "JNT_r_rearBall.rpt" "r_rearArm_jt_aimConstraint1.tg[0].trt";
connectAttr "JNT_r_rearBall.pm" "r_rearArm_jt_aimConstraint1.tg[0].tpm";
connectAttr "r_rearArm_jt_aimConstraint1.w0" "r_rearArm_jt_aimConstraint1.tg[0].tw"
		;
connectAttr "r_rearWheel_upvector_pt.wm" "r_rearArm_jt_aimConstraint1.wum";
connectAttr "JNT_r_rearWidth.s" "JNT_r_rearLowerArm.is";
connectAttr "JNT_r_rearArm.rz" "JNT_r_rearLowerArm.rz";
connectAttr "JNT_r_rearLowerArm.s" "JNT_r_rearLowerBall.is";
connectAttr "unitConversion500.o" "JNT_r_rearLowerBall.rz";
connectAttr "r_rearLowerSpring_jt_aimConstraint1.crz" "JNT_r_rearLowerSpring.rz"
		;
connectAttr "JNT_r_rearLowerBall.s" "JNT_r_rearLowerSpring.is";
connectAttr "JNT_r_rearLowerSpring.pim" "r_rearLowerSpring_jt_aimConstraint1.cpim"
		;
connectAttr "JNT_r_rearLowerSpring.t" "r_rearLowerSpring_jt_aimConstraint1.ct";
connectAttr "JNT_r_rearLowerSpring.rp" "r_rearLowerSpring_jt_aimConstraint1.crp"
		;
connectAttr "JNT_r_rearLowerSpring.rpt" "r_rearLowerSpring_jt_aimConstraint1.crt"
		;
connectAttr "JNT_r_rearLowerSpring.ro" "r_rearLowerSpring_jt_aimConstraint1.cro"
		;
connectAttr "JNT_r_rearLowerSpring.jo" "r_rearLowerSpring_jt_aimConstraint1.cjo"
		;
connectAttr "JNT_r_rearLowerSpring.is" "r_rearLowerSpring_jt_aimConstraint1.is";
connectAttr "JNT_r_rearUpperSpring.t" "r_rearLowerSpring_jt_aimConstraint1.tg[0].tt"
		;
connectAttr "JNT_r_rearUpperSpring.rp" "r_rearLowerSpring_jt_aimConstraint1.tg[0].trp"
		;
connectAttr "JNT_r_rearUpperSpring.rpt" "r_rearLowerSpring_jt_aimConstraint1.tg[0].trt"
		;
connectAttr "JNT_r_rearUpperSpring.pm" "r_rearLowerSpring_jt_aimConstraint1.tg[0].tpm"
		;
connectAttr "r_rearLowerSpring_jt_aimConstraint1.w0" "r_rearLowerSpring_jt_aimConstraint1.tg[0].tw"
		;
connectAttr "r_rearSpring_upvector_pt.wm" "r_rearLowerSpring_jt_aimConstraint1.wum"
		;
connectAttr "l_frontBall_jt_parentConstraint1.ctx" "JNT_l_frontBall.tx";
connectAttr "l_frontBall_jt_parentConstraint1.ctz" "JNT_l_frontBall.tz";
connectAttr "CNT_l_frontWheel.ty" "JNT_l_frontBall.ty";
connectAttr "l_frontBall_jt_parentConstraint1.crx" "JNT_l_frontBall.rx";
connectAttr "l_frontBall_jt_parentConstraint1.cry" "JNT_l_frontBall.ry";
connectAttr "l_frontBall_jt_parentConstraint1.crz" "JNT_l_frontBall.rz";
connectAttr "CNT_drive.steerRadius" "JNT_l_frontWheel.tx";
connectAttr "unitConversion502.o" "JNT_l_frontWheel.rx";
connectAttr "JNT_l_frontBall.s" "JNT_l_frontWheel.is";
connectAttr "JNT_l_frontBall.s" "JNT_l_steering.is";
connectAttr "JNT_l_frontBall.ro" "l_frontBall_jt_parentConstraint1.cro";
connectAttr "JNT_l_frontBall.pim" "l_frontBall_jt_parentConstraint1.cpim";
connectAttr "JNT_l_frontBall.rp" "l_frontBall_jt_parentConstraint1.crp";
connectAttr "JNT_l_frontBall.rpt" "l_frontBall_jt_parentConstraint1.crt";
connectAttr "JNT_l_frontBall.jo" "l_frontBall_jt_parentConstraint1.cjo";
connectAttr "l_frontSteering_pt.t" "l_frontBall_jt_parentConstraint1.tg[0].tt";
connectAttr "l_frontSteering_pt.rp" "l_frontBall_jt_parentConstraint1.tg[0].trp"
		;
connectAttr "l_frontSteering_pt.rpt" "l_frontBall_jt_parentConstraint1.tg[0].trt"
		;
connectAttr "l_frontSteering_pt.r" "l_frontBall_jt_parentConstraint1.tg[0].tr";
connectAttr "l_frontSteering_pt.ro" "l_frontBall_jt_parentConstraint1.tg[0].tro"
		;
connectAttr "l_frontSteering_pt.s" "l_frontBall_jt_parentConstraint1.tg[0].ts";
connectAttr "l_frontSteering_pt.pm" "l_frontBall_jt_parentConstraint1.tg[0].tpm"
		;
connectAttr "l_frontBall_jt_parentConstraint1.w0" "l_frontBall_jt_parentConstraint1.tg[0].tw"
		;
connectAttr "r_frontBall_jt_parentConstraint1.ctx" "JNT_r_frontBall.tx";
connectAttr "r_frontBall_jt_parentConstraint1.ctz" "JNT_r_frontBall.tz";
connectAttr "CNT_r_frontWheel.ty" "JNT_r_frontBall.ty";
connectAttr "r_frontBall_jt_parentConstraint1.crx" "JNT_r_frontBall.rx";
connectAttr "r_frontBall_jt_parentConstraint1.cry" "JNT_r_frontBall.ry";
connectAttr "r_frontBall_jt_parentConstraint1.crz" "JNT_r_frontBall.rz";
connectAttr "steerDistance_and_invert_MD.oy" "JNT_r_frontWheel.tx";
connectAttr "unitConversion503.o" "JNT_r_frontWheel.rx";
connectAttr "JNT_r_frontBall.s" "JNT_r_frontWheel.is";
connectAttr "JNT_r_frontBall.s" "JNT_r_steering.is";
connectAttr "JNT_r_frontBall.ro" "r_frontBall_jt_parentConstraint1.cro";
connectAttr "JNT_r_frontBall.pim" "r_frontBall_jt_parentConstraint1.cpim";
connectAttr "JNT_r_frontBall.rp" "r_frontBall_jt_parentConstraint1.crp";
connectAttr "JNT_r_frontBall.rpt" "r_frontBall_jt_parentConstraint1.crt";
connectAttr "JNT_r_frontBall.jo" "r_frontBall_jt_parentConstraint1.cjo";
connectAttr "r_frontSteering_pt.t" "r_frontBall_jt_parentConstraint1.tg[0].tt";
connectAttr "r_frontSteering_pt.rp" "r_frontBall_jt_parentConstraint1.tg[0].trp"
		;
connectAttr "r_frontSteering_pt.rpt" "r_frontBall_jt_parentConstraint1.tg[0].trt"
		;
connectAttr "r_frontSteering_pt.r" "r_frontBall_jt_parentConstraint1.tg[0].tr";
connectAttr "r_frontSteering_pt.ro" "r_frontBall_jt_parentConstraint1.tg[0].tro"
		;
connectAttr "r_frontSteering_pt.s" "r_frontBall_jt_parentConstraint1.tg[0].ts";
connectAttr "r_frontSteering_pt.pm" "r_frontBall_jt_parentConstraint1.tg[0].tpm"
		;
connectAttr "r_frontBall_jt_parentConstraint1.w0" "r_frontBall_jt_parentConstraint1.tg[0].tw"
		;
connectAttr "l_reartBall_jt_parentConstraint1.ctx" "JNT_l_reartBall.tx";
connectAttr "l_reartBall_jt_parentConstraint1.ctz" "JNT_l_reartBall.tz";
connectAttr "CNT_l_rearWheel.ty" "JNT_l_reartBall.ty";
connectAttr "l_reartBall_jt_parentConstraint1.crx" "JNT_l_reartBall.rx";
connectAttr "l_reartBall_jt_parentConstraint1.cry" "JNT_l_reartBall.ry";
connectAttr "l_reartBall_jt_parentConstraint1.crz" "JNT_l_reartBall.rz";
connectAttr "CNT_drive.steerRadius" "JNT_l_rearWheel.tx";
connectAttr "unitConversion505.o" "JNT_l_rearWheel.rx";
connectAttr "JNT_l_reartBall.s" "JNT_l_rearWheel.is";
connectAttr "JNT_l_reartBall.ro" "l_reartBall_jt_parentConstraint1.cro";
connectAttr "JNT_l_reartBall.pim" "l_reartBall_jt_parentConstraint1.cpim";
connectAttr "JNT_l_reartBall.rp" "l_reartBall_jt_parentConstraint1.crp";
connectAttr "JNT_l_reartBall.rpt" "l_reartBall_jt_parentConstraint1.crt";
connectAttr "JNT_l_reartBall.jo" "l_reartBall_jt_parentConstraint1.cjo";
connectAttr "CNT_l_rearWheel.t" "l_reartBall_jt_parentConstraint1.tg[0].tt";
connectAttr "CNT_l_rearWheel.rp" "l_reartBall_jt_parentConstraint1.tg[0].trp";
connectAttr "CNT_l_rearWheel.rpt" "l_reartBall_jt_parentConstraint1.tg[0].trt";
connectAttr "CNT_l_rearWheel.r" "l_reartBall_jt_parentConstraint1.tg[0].tr";
connectAttr "CNT_l_rearWheel.ro" "l_reartBall_jt_parentConstraint1.tg[0].tro";
connectAttr "CNT_l_rearWheel.s" "l_reartBall_jt_parentConstraint1.tg[0].ts";
connectAttr "CNT_l_rearWheel.pm" "l_reartBall_jt_parentConstraint1.tg[0].tpm";
connectAttr "l_reartBall_jt_parentConstraint1.w0" "l_reartBall_jt_parentConstraint1.tg[0].tw"
		;
connectAttr "r_rearBall_jt_parentConstraint1.ctx" "JNT_r_rearBall.tx";
connectAttr "r_rearBall_jt_parentConstraint1.ctz" "JNT_r_rearBall.tz";
connectAttr "CNT_r_rearWheel.ty" "JNT_r_rearBall.ty";
connectAttr "r_rearBall_jt_parentConstraint1.crx" "JNT_r_rearBall.rx";
connectAttr "r_rearBall_jt_parentConstraint1.cry" "JNT_r_rearBall.ry";
connectAttr "r_rearBall_jt_parentConstraint1.crz" "JNT_r_rearBall.rz";
connectAttr "steerDistance_and_invert_MD.oy" "JNT_r_rearWheel.tx";
connectAttr "unitConversion506.o" "JNT_r_rearWheel.rx";
connectAttr "JNT_r_rearBall.s" "JNT_r_rearWheel.is";
connectAttr "JNT_r_rearBall.ro" "r_rearBall_jt_parentConstraint1.cro";
connectAttr "JNT_r_rearBall.pim" "r_rearBall_jt_parentConstraint1.cpim";
connectAttr "JNT_r_rearBall.rp" "r_rearBall_jt_parentConstraint1.crp";
connectAttr "JNT_r_rearBall.rpt" "r_rearBall_jt_parentConstraint1.crt";
connectAttr "JNT_r_rearBall.jo" "r_rearBall_jt_parentConstraint1.cjo";
connectAttr "CNT_r_rearWheel.t" "r_rearBall_jt_parentConstraint1.tg[0].tt";
connectAttr "CNT_r_rearWheel.rp" "r_rearBall_jt_parentConstraint1.tg[0].trp";
connectAttr "CNT_r_rearWheel.rpt" "r_rearBall_jt_parentConstraint1.tg[0].trt";
connectAttr "CNT_r_rearWheel.r" "r_rearBall_jt_parentConstraint1.tg[0].tr";
connectAttr "CNT_r_rearWheel.ro" "r_rearBall_jt_parentConstraint1.tg[0].tro";
connectAttr "CNT_r_rearWheel.s" "r_rearBall_jt_parentConstraint1.tg[0].ts";
connectAttr "CNT_r_rearWheel.pm" "r_rearBall_jt_parentConstraint1.tg[0].tpm";
connectAttr "r_rearBall_jt_parentConstraint1.w0" "r_rearBall_jt_parentConstraint1.tg[0].tw"
		;
connectAttr "JNT_chasis.ro" "JNT_chasis_parentConstraint1.cro";
connectAttr "JNT_chasis.pim" "JNT_chasis_parentConstraint1.cpim";
connectAttr "JNT_chasis.rp" "JNT_chasis_parentConstraint1.crp";
connectAttr "JNT_chasis.rpt" "JNT_chasis_parentConstraint1.crt";
connectAttr "JNT_chasis.jo" "JNT_chasis_parentConstraint1.cjo";
connectAttr "CNT_rearAxis.t" "JNT_chasis_parentConstraint1.tg[0].tt";
connectAttr "CNT_rearAxis.rp" "JNT_chasis_parentConstraint1.tg[0].trp";
connectAttr "CNT_rearAxis.rpt" "JNT_chasis_parentConstraint1.tg[0].trt";
connectAttr "CNT_rearAxis.r" "JNT_chasis_parentConstraint1.tg[0].tr";
connectAttr "CNT_rearAxis.ro" "JNT_chasis_parentConstraint1.tg[0].tro";
connectAttr "CNT_rearAxis.s" "JNT_chasis_parentConstraint1.tg[0].ts";
connectAttr "CNT_rearAxis.pm" "JNT_chasis_parentConstraint1.tg[0].tpm";
connectAttr "JNT_chasis_parentConstraint1.w0" "JNT_chasis_parentConstraint1.tg[0].tw"
		;
connectAttr "JNT_chasis.ssc" "JNT_chasis_scaleConstraint1.tsc";
connectAttr "JNT_chasis.pim" "JNT_chasis_scaleConstraint1.cpim";
connectAttr "CNT_rearAxis.s" "JNT_chasis_scaleConstraint1.tg[0].ts";
connectAttr "CNT_rearAxis.pm" "JNT_chasis_scaleConstraint1.tg[0].tpm";
connectAttr "JNT_chasis_scaleConstraint1.w0" "JNT_chasis_scaleConstraint1.tg[0].tw"
		;
connectAttr "Car_parentConstraint1.ctx" "Car.tx";
connectAttr "Car_parentConstraint1.cty" "Car.ty";
connectAttr "Car_parentConstraint1.ctz" "Car.tz";
connectAttr "Car_parentConstraint1.crx" "Car.rx";
connectAttr "Car_parentConstraint1.cry" "Car.ry";
connectAttr "Car_parentConstraint1.crz" "Car.rz";
connectAttr "Car_scaleConstraint1.csx" "Car.sx";
connectAttr "Car_scaleConstraint1.csy" "Car.sy";
connectAttr "Car_scaleConstraint1.csz" "Car.sz";
connectAttr "Car.ro" "Car_parentConstraint1.cro";
connectAttr "Car.pim" "Car_parentConstraint1.cpim";
connectAttr "Car.rp" "Car_parentConstraint1.crp";
connectAttr "Car.rpt" "Car_parentConstraint1.crt";
connectAttr "Car.jo" "Car_parentConstraint1.cjo";
connectAttr "CNT_body.t" "Car_parentConstraint1.tg[0].tt";
connectAttr "CNT_body.rp" "Car_parentConstraint1.tg[0].trp";
connectAttr "CNT_body.rpt" "Car_parentConstraint1.tg[0].trt";
connectAttr "CNT_body.r" "Car_parentConstraint1.tg[0].tr";
connectAttr "CNT_body.ro" "Car_parentConstraint1.tg[0].tro";
connectAttr "CNT_body.s" "Car_parentConstraint1.tg[0].ts";
connectAttr "CNT_body.pm" "Car_parentConstraint1.tg[0].tpm";
connectAttr "Car_parentConstraint1.w0" "Car_parentConstraint1.tg[0].tw";
connectAttr "Car.pim" "Car_scaleConstraint1.cpim";
connectAttr "CNT_body.s" "Car_scaleConstraint1.tg[0].ts";
connectAttr "CNT_body.pm" "Car_scaleConstraint1.tg[0].tpm";
connectAttr "Car_scaleConstraint1.w0" "Car_scaleConstraint1.tg[0].tw";
connectAttr "CTRL_set.Rattr" "GRP_Meshes_grp.ove";
connectAttr "CTRL_set.Rattr" "GRP_utility_meshes.ove";
connectAttr "skinCluster1GroupId.id" "MSH_chassis_tutorialShape.iog.og[0].gid";
connectAttr "SK_chassis_tutorialSet.mwc" "MSH_chassis_tutorialShape.iog.og[0].gco"
		;
connectAttr "SK_chassis_tutorial.og[0]" "MSH_chassis_tutorialShape.i";
connectAttr "groupId15.id" "MSH_tires_tutorialShape.iog.og[2].gid";
connectAttr "tire_tutorial_shdr_SE.mwc" "MSH_tires_tutorialShape.iog.og[2].gco";
connectAttr "groupId17.id" "MSH_tires_tutorialShape.iog.og[3].gid";
connectAttr "lambert2SG.mwc" "MSH_tires_tutorialShape.iog.og[3].gco";
connectAttr "groupId39.id" "MSH_tires_tutorialShape.iog.og[18].gid";
connectAttr "ffd2Set.mwc" "MSH_tires_tutorialShape.iog.og[18].gco";
connectAttr "groupId41.id" "MSH_tires_tutorialShape.iog.og[19].gid";
connectAttr "ffd3Set.mwc" "MSH_tires_tutorialShape.iog.og[19].gco";
connectAttr "groupId43.id" "MSH_tires_tutorialShape.iog.og[20].gid";
connectAttr "ffd4Set.mwc" "MSH_tires_tutorialShape.iog.og[20].gco";
connectAttr "skinCluster9GroupId.id" "MSH_tires_tutorialShape.iog.og[21].gid";
connectAttr "skinCluster9Set.mwc" "MSH_tires_tutorialShape.iog.og[21].gco";
connectAttr "groupId50.id" "MSH_tires_tutorialShape.iog.og[27].gid";
connectAttr "ffd1Set.mwc" "MSH_tires_tutorialShape.iog.og[27].gco";
connectAttr "ffd_R_Front.og[5]" "MSH_tires_tutorialShape.i";
connectAttr "groupId16.id" "MSH_tires_tutorialShape.ciog.cog[0].cgid";
connectAttr "groupId38.id" "MSH_wheels_tutorialShape.iog.og[9].gid";
connectAttr "ffd2Set.mwc" "MSH_wheels_tutorialShape.iog.og[9].gco";
connectAttr "groupId40.id" "MSH_wheels_tutorialShape.iog.og[10].gid";
connectAttr "ffd3Set.mwc" "MSH_wheels_tutorialShape.iog.og[10].gco";
connectAttr "groupId42.id" "MSH_wheels_tutorialShape.iog.og[11].gid";
connectAttr "ffd4Set.mwc" "MSH_wheels_tutorialShape.iog.og[11].gco";
connectAttr "skinCluster8GroupId.id" "MSH_wheels_tutorialShape.iog.og[12].gid";
connectAttr "skinCluster8Set.mwc" "MSH_wheels_tutorialShape.iog.og[12].gco";
connectAttr "groupId51.id" "MSH_wheels_tutorialShape.iog.og[18].gid";
connectAttr "ffd1Set.mwc" "MSH_wheels_tutorialShape.iog.og[18].gco";
connectAttr "ffd_R_Front.og[6]" "MSH_wheels_tutorialShape.i";
connectAttr "skinCluster4GroupId.id" "MSH_wheelMount_tutorialShape.iog.og[0].gid"
		;
connectAttr "SK_wheelMount_tutorialSet.mwc" "MSH_wheelMount_tutorialShape.iog.og[0].gco"
		;
connectAttr "SK_wheelMount_tutorial.og[0]" "MSH_wheelMount_tutorialShape.i";
connectAttr "skinCluster5GroupId.id" "MSH_arms_tutorialShape.iog.og[0].gid";
connectAttr "SK_arms_tutorialSet.mwc" "MSH_arms_tutorialShape.iog.og[0].gco";
connectAttr "SK_arms_tutorial.og[0]" "MSH_arms_tutorialShape.i";
connectAttr "skinCluster7GroupId.id" "MSH_springs_tutorialShape.iog.og[0].gid";
connectAttr "SK_springs_tutorialSet.mwc" "MSH_springs_tutorialShape.iog.og[0].gco"
		;
connectAttr "SK_springs_tutorial.og[0]" "MSH_springs_tutorialShape.i";
connectAttr "skinCluster6GroupId.id" "MSH_springs_tutorial_skinMeshShape.iog.og[2].gid"
		;
connectAttr "skinCluster6Set.mwc" "MSH_springs_tutorial_skinMeshShape.iog.og[2].gco"
		;
connectAttr "skinCluster6.og[0]" "MSH_springs_tutorial_skinMeshShape.i";
connectAttr "l_rearWheel_upvector_pt_parentConstraint1.ctx" "l_rearWheel_upvector_pt.tx"
		;
connectAttr "l_rearWheel_upvector_pt_parentConstraint1.cty" "l_rearWheel_upvector_pt.ty"
		;
connectAttr "l_rearWheel_upvector_pt_parentConstraint1.ctz" "l_rearWheel_upvector_pt.tz"
		;
connectAttr "l_rearWheel_upvector_pt_parentConstraint1.crx" "l_rearWheel_upvector_pt.rx"
		;
connectAttr "l_rearWheel_upvector_pt_parentConstraint1.cry" "l_rearWheel_upvector_pt.ry"
		;
connectAttr "l_rearWheel_upvector_pt_parentConstraint1.crz" "l_rearWheel_upvector_pt.rz"
		;
connectAttr "l_rearWheel_upvector_pt.ro" "l_rearWheel_upvector_pt_parentConstraint1.cro"
		;
connectAttr "l_rearWheel_upvector_pt.pim" "l_rearWheel_upvector_pt_parentConstraint1.cpim"
		;
connectAttr "l_rearWheel_upvector_pt.rp" "l_rearWheel_upvector_pt_parentConstraint1.crp"
		;
connectAttr "l_rearWheel_upvector_pt.rpt" "l_rearWheel_upvector_pt_parentConstraint1.crt"
		;
connectAttr "JNT_l_rearWidth.t" "l_rearWheel_upvector_pt_parentConstraint1.tg[0].tt"
		;
connectAttr "JNT_l_rearWidth.rp" "l_rearWheel_upvector_pt_parentConstraint1.tg[0].trp"
		;
connectAttr "JNT_l_rearWidth.rpt" "l_rearWheel_upvector_pt_parentConstraint1.tg[0].trt"
		;
connectAttr "JNT_l_rearWidth.r" "l_rearWheel_upvector_pt_parentConstraint1.tg[0].tr"
		;
connectAttr "JNT_l_rearWidth.ro" "l_rearWheel_upvector_pt_parentConstraint1.tg[0].tro"
		;
connectAttr "JNT_l_rearWidth.s" "l_rearWheel_upvector_pt_parentConstraint1.tg[0].ts"
		;
connectAttr "JNT_l_rearWidth.pm" "l_rearWheel_upvector_pt_parentConstraint1.tg[0].tpm"
		;
connectAttr "JNT_l_rearWidth.jo" "l_rearWheel_upvector_pt_parentConstraint1.tg[0].tjo"
		;
connectAttr "JNT_l_rearWidth.ssc" "l_rearWheel_upvector_pt_parentConstraint1.tg[0].tsc"
		;
connectAttr "JNT_l_rearWidth.is" "l_rearWheel_upvector_pt_parentConstraint1.tg[0].tis"
		;
connectAttr "l_rearWheel_upvector_pt_parentConstraint1.w0" "l_rearWheel_upvector_pt_parentConstraint1.tg[0].tw"
		;
connectAttr "r_rearWheer_upvector_pt_parentConstraint1.ctx" "r_rearWheel_upvector_pt.tx"
		;
connectAttr "r_rearWheer_upvector_pt_parentConstraint1.cty" "r_rearWheel_upvector_pt.ty"
		;
connectAttr "r_rearWheer_upvector_pt_parentConstraint1.ctz" "r_rearWheel_upvector_pt.tz"
		;
connectAttr "r_rearWheer_upvector_pt_parentConstraint1.crx" "r_rearWheel_upvector_pt.rx"
		;
connectAttr "r_rearWheer_upvector_pt_parentConstraint1.cry" "r_rearWheel_upvector_pt.ry"
		;
connectAttr "r_rearWheer_upvector_pt_parentConstraint1.crz" "r_rearWheel_upvector_pt.rz"
		;
connectAttr "r_rearWheel_upvector_pt.ro" "r_rearWheer_upvector_pt_parentConstraint1.cro"
		;
connectAttr "r_rearWheel_upvector_pt.pim" "r_rearWheer_upvector_pt_parentConstraint1.cpim"
		;
connectAttr "r_rearWheel_upvector_pt.rp" "r_rearWheer_upvector_pt_parentConstraint1.crp"
		;
connectAttr "r_rearWheel_upvector_pt.rpt" "r_rearWheer_upvector_pt_parentConstraint1.crt"
		;
connectAttr "JNT_r_rearWidth.t" "r_rearWheer_upvector_pt_parentConstraint1.tg[0].tt"
		;
connectAttr "JNT_r_rearWidth.rp" "r_rearWheer_upvector_pt_parentConstraint1.tg[0].trp"
		;
connectAttr "JNT_r_rearWidth.rpt" "r_rearWheer_upvector_pt_parentConstraint1.tg[0].trt"
		;
connectAttr "JNT_r_rearWidth.r" "r_rearWheer_upvector_pt_parentConstraint1.tg[0].tr"
		;
connectAttr "JNT_r_rearWidth.ro" "r_rearWheer_upvector_pt_parentConstraint1.tg[0].tro"
		;
connectAttr "JNT_r_rearWidth.s" "r_rearWheer_upvector_pt_parentConstraint1.tg[0].ts"
		;
connectAttr "JNT_r_rearWidth.pm" "r_rearWheer_upvector_pt_parentConstraint1.tg[0].tpm"
		;
connectAttr "JNT_r_rearWidth.jo" "r_rearWheer_upvector_pt_parentConstraint1.tg[0].tjo"
		;
connectAttr "JNT_r_rearWidth.ssc" "r_rearWheer_upvector_pt_parentConstraint1.tg[0].tsc"
		;
connectAttr "JNT_r_rearWidth.is" "r_rearWheer_upvector_pt_parentConstraint1.tg[0].tis"
		;
connectAttr "r_rearWheer_upvector_pt_parentConstraint1.w0" "r_rearWheer_upvector_pt_parentConstraint1.tg[0].tw"
		;
connectAttr "r_frontWheer_upvector_pt_parentConstraint1.ctx" "r_frontWheel_upvector_pt.tx"
		;
connectAttr "r_frontWheer_upvector_pt_parentConstraint1.cty" "r_frontWheel_upvector_pt.ty"
		;
connectAttr "r_frontWheer_upvector_pt_parentConstraint1.ctz" "r_frontWheel_upvector_pt.tz"
		;
connectAttr "r_frontWheer_upvector_pt_parentConstraint1.crx" "r_frontWheel_upvector_pt.rx"
		;
connectAttr "r_frontWheer_upvector_pt_parentConstraint1.cry" "r_frontWheel_upvector_pt.ry"
		;
connectAttr "r_frontWheer_upvector_pt_parentConstraint1.crz" "r_frontWheel_upvector_pt.rz"
		;
connectAttr "r_frontWheel_upvector_pt.ro" "r_frontWheer_upvector_pt_parentConstraint1.cro"
		;
connectAttr "r_frontWheel_upvector_pt.pim" "r_frontWheer_upvector_pt_parentConstraint1.cpim"
		;
connectAttr "r_frontWheel_upvector_pt.rp" "r_frontWheer_upvector_pt_parentConstraint1.crp"
		;
connectAttr "r_frontWheel_upvector_pt.rpt" "r_frontWheer_upvector_pt_parentConstraint1.crt"
		;
connectAttr "JNT_r_frontWidth.t" "r_frontWheer_upvector_pt_parentConstraint1.tg[0].tt"
		;
connectAttr "JNT_r_frontWidth.rp" "r_frontWheer_upvector_pt_parentConstraint1.tg[0].trp"
		;
connectAttr "JNT_r_frontWidth.rpt" "r_frontWheer_upvector_pt_parentConstraint1.tg[0].trt"
		;
connectAttr "JNT_r_frontWidth.r" "r_frontWheer_upvector_pt_parentConstraint1.tg[0].tr"
		;
connectAttr "JNT_r_frontWidth.ro" "r_frontWheer_upvector_pt_parentConstraint1.tg[0].tro"
		;
connectAttr "JNT_r_frontWidth.s" "r_frontWheer_upvector_pt_parentConstraint1.tg[0].ts"
		;
connectAttr "JNT_r_frontWidth.pm" "r_frontWheer_upvector_pt_parentConstraint1.tg[0].tpm"
		;
connectAttr "JNT_r_frontWidth.jo" "r_frontWheer_upvector_pt_parentConstraint1.tg[0].tjo"
		;
connectAttr "JNT_r_frontWidth.ssc" "r_frontWheer_upvector_pt_parentConstraint1.tg[0].tsc"
		;
connectAttr "JNT_r_frontWidth.is" "r_frontWheer_upvector_pt_parentConstraint1.tg[0].tis"
		;
connectAttr "r_frontWheer_upvector_pt_parentConstraint1.w0" "r_frontWheer_upvector_pt_parentConstraint1.tg[0].tw"
		;
connectAttr "l_frontWheel_upvector_pt_parentConstraint1.ctx" "l_frontWheel_upvector_pt.tx"
		;
connectAttr "l_frontWheel_upvector_pt_parentConstraint1.cty" "l_frontWheel_upvector_pt.ty"
		;
connectAttr "l_frontWheel_upvector_pt_parentConstraint1.ctz" "l_frontWheel_upvector_pt.tz"
		;
connectAttr "l_frontWheel_upvector_pt_parentConstraint1.crx" "l_frontWheel_upvector_pt.rx"
		;
connectAttr "l_frontWheel_upvector_pt_parentConstraint1.cry" "l_frontWheel_upvector_pt.ry"
		;
connectAttr "l_frontWheel_upvector_pt_parentConstraint1.crz" "l_frontWheel_upvector_pt.rz"
		;
connectAttr "l_frontWheel_upvector_pt.ro" "l_frontWheel_upvector_pt_parentConstraint1.cro"
		;
connectAttr "l_frontWheel_upvector_pt.pim" "l_frontWheel_upvector_pt_parentConstraint1.cpim"
		;
connectAttr "l_frontWheel_upvector_pt.rp" "l_frontWheel_upvector_pt_parentConstraint1.crp"
		;
connectAttr "l_frontWheel_upvector_pt.rpt" "l_frontWheel_upvector_pt_parentConstraint1.crt"
		;
connectAttr "JNT_l_frontWidth.t" "l_frontWheel_upvector_pt_parentConstraint1.tg[0].tt"
		;
connectAttr "JNT_l_frontWidth.rp" "l_frontWheel_upvector_pt_parentConstraint1.tg[0].trp"
		;
connectAttr "JNT_l_frontWidth.rpt" "l_frontWheel_upvector_pt_parentConstraint1.tg[0].trt"
		;
connectAttr "JNT_l_frontWidth.r" "l_frontWheel_upvector_pt_parentConstraint1.tg[0].tr"
		;
connectAttr "JNT_l_frontWidth.ro" "l_frontWheel_upvector_pt_parentConstraint1.tg[0].tro"
		;
connectAttr "JNT_l_frontWidth.s" "l_frontWheel_upvector_pt_parentConstraint1.tg[0].ts"
		;
connectAttr "JNT_l_frontWidth.pm" "l_frontWheel_upvector_pt_parentConstraint1.tg[0].tpm"
		;
connectAttr "JNT_l_frontWidth.jo" "l_frontWheel_upvector_pt_parentConstraint1.tg[0].tjo"
		;
connectAttr "JNT_l_frontWidth.ssc" "l_frontWheel_upvector_pt_parentConstraint1.tg[0].tsc"
		;
connectAttr "JNT_l_frontWidth.is" "l_frontWheel_upvector_pt_parentConstraint1.tg[0].tis"
		;
connectAttr "l_frontWheel_upvector_pt_parentConstraint1.w0" "l_frontWheel_upvector_pt_parentConstraint1.tg[0].tw"
		;
connectAttr "l_frontSpring_upvector_pt_parentConstraint1.ctx" "l_frontSpring_upvector_pt.tx"
		;
connectAttr "l_frontSpring_upvector_pt_parentConstraint1.cty" "l_frontSpring_upvector_pt.ty"
		;
connectAttr "l_frontSpring_upvector_pt_parentConstraint1.ctz" "l_frontSpring_upvector_pt.tz"
		;
connectAttr "l_frontSpring_upvector_pt_parentConstraint1.crx" "l_frontSpring_upvector_pt.rx"
		;
connectAttr "l_frontSpring_upvector_pt_parentConstraint1.cry" "l_frontSpring_upvector_pt.ry"
		;
connectAttr "l_frontSpring_upvector_pt_parentConstraint1.crz" "l_frontSpring_upvector_pt.rz"
		;
connectAttr "l_frontSpring_upvector_pt.ro" "l_frontSpring_upvector_pt_parentConstraint1.cro"
		;
connectAttr "l_frontSpring_upvector_pt.pim" "l_frontSpring_upvector_pt_parentConstraint1.cpim"
		;
connectAttr "l_frontSpring_upvector_pt.rp" "l_frontSpring_upvector_pt_parentConstraint1.crp"
		;
connectAttr "l_frontSpring_upvector_pt.rpt" "l_frontSpring_upvector_pt_parentConstraint1.crt"
		;
connectAttr "JNT_l_frontWidth.t" "l_frontSpring_upvector_pt_parentConstraint1.tg[0].tt"
		;
connectAttr "JNT_l_frontWidth.rp" "l_frontSpring_upvector_pt_parentConstraint1.tg[0].trp"
		;
connectAttr "JNT_l_frontWidth.rpt" "l_frontSpring_upvector_pt_parentConstraint1.tg[0].trt"
		;
connectAttr "JNT_l_frontWidth.r" "l_frontSpring_upvector_pt_parentConstraint1.tg[0].tr"
		;
connectAttr "JNT_l_frontWidth.ro" "l_frontSpring_upvector_pt_parentConstraint1.tg[0].tro"
		;
connectAttr "JNT_l_frontWidth.s" "l_frontSpring_upvector_pt_parentConstraint1.tg[0].ts"
		;
connectAttr "JNT_l_frontWidth.pm" "l_frontSpring_upvector_pt_parentConstraint1.tg[0].tpm"
		;
connectAttr "JNT_l_frontWidth.jo" "l_frontSpring_upvector_pt_parentConstraint1.tg[0].tjo"
		;
connectAttr "JNT_l_frontWidth.ssc" "l_frontSpring_upvector_pt_parentConstraint1.tg[0].tsc"
		;
connectAttr "JNT_l_frontWidth.is" "l_frontSpring_upvector_pt_parentConstraint1.tg[0].tis"
		;
connectAttr "l_frontSpring_upvector_pt_parentConstraint1.w0" "l_frontSpring_upvector_pt_parentConstraint1.tg[0].tw"
		;
connectAttr "r_frontSpring_upvector_pt_parentConstraint1.ctx" "r_frontSpring_upvector_pt.tx"
		;
connectAttr "r_frontSpring_upvector_pt_parentConstraint1.cty" "r_frontSpring_upvector_pt.ty"
		;
connectAttr "r_frontSpring_upvector_pt_parentConstraint1.ctz" "r_frontSpring_upvector_pt.tz"
		;
connectAttr "r_frontSpring_upvector_pt_parentConstraint1.crx" "r_frontSpring_upvector_pt.rx"
		;
connectAttr "r_frontSpring_upvector_pt_parentConstraint1.cry" "r_frontSpring_upvector_pt.ry"
		;
connectAttr "r_frontSpring_upvector_pt_parentConstraint1.crz" "r_frontSpring_upvector_pt.rz"
		;
connectAttr "r_frontSpring_upvector_pt.ro" "r_frontSpring_upvector_pt_parentConstraint1.cro"
		;
connectAttr "r_frontSpring_upvector_pt.pim" "r_frontSpring_upvector_pt_parentConstraint1.cpim"
		;
connectAttr "r_frontSpring_upvector_pt.rp" "r_frontSpring_upvector_pt_parentConstraint1.crp"
		;
connectAttr "r_frontSpring_upvector_pt.rpt" "r_frontSpring_upvector_pt_parentConstraint1.crt"
		;
connectAttr "JNT_r_frontWidth.t" "r_frontSpring_upvector_pt_parentConstraint1.tg[0].tt"
		;
connectAttr "JNT_r_frontWidth.rp" "r_frontSpring_upvector_pt_parentConstraint1.tg[0].trp"
		;
connectAttr "JNT_r_frontWidth.rpt" "r_frontSpring_upvector_pt_parentConstraint1.tg[0].trt"
		;
connectAttr "JNT_r_frontWidth.r" "r_frontSpring_upvector_pt_parentConstraint1.tg[0].tr"
		;
connectAttr "JNT_r_frontWidth.ro" "r_frontSpring_upvector_pt_parentConstraint1.tg[0].tro"
		;
connectAttr "JNT_r_frontWidth.s" "r_frontSpring_upvector_pt_parentConstraint1.tg[0].ts"
		;
connectAttr "JNT_r_frontWidth.pm" "r_frontSpring_upvector_pt_parentConstraint1.tg[0].tpm"
		;
connectAttr "JNT_r_frontWidth.jo" "r_frontSpring_upvector_pt_parentConstraint1.tg[0].tjo"
		;
connectAttr "JNT_r_frontWidth.ssc" "r_frontSpring_upvector_pt_parentConstraint1.tg[0].tsc"
		;
connectAttr "JNT_r_frontWidth.is" "r_frontSpring_upvector_pt_parentConstraint1.tg[0].tis"
		;
connectAttr "r_frontSpring_upvector_pt_parentConstraint1.w0" "r_frontSpring_upvector_pt_parentConstraint1.tg[0].tw"
		;
connectAttr "r_rearSpring_upvector_pt_parentConstraint1.ctx" "r_rearSpring_upvector_pt.tx"
		;
connectAttr "r_rearSpring_upvector_pt_parentConstraint1.cty" "r_rearSpring_upvector_pt.ty"
		;
connectAttr "r_rearSpring_upvector_pt_parentConstraint1.ctz" "r_rearSpring_upvector_pt.tz"
		;
connectAttr "r_rearSpring_upvector_pt_parentConstraint1.crx" "r_rearSpring_upvector_pt.rx"
		;
connectAttr "r_rearSpring_upvector_pt_parentConstraint1.cry" "r_rearSpring_upvector_pt.ry"
		;
connectAttr "r_rearSpring_upvector_pt_parentConstraint1.crz" "r_rearSpring_upvector_pt.rz"
		;
connectAttr "r_rearSpring_upvector_pt.ro" "r_rearSpring_upvector_pt_parentConstraint1.cro"
		;
connectAttr "r_rearSpring_upvector_pt.pim" "r_rearSpring_upvector_pt_parentConstraint1.cpim"
		;
connectAttr "r_rearSpring_upvector_pt.rp" "r_rearSpring_upvector_pt_parentConstraint1.crp"
		;
connectAttr "r_rearSpring_upvector_pt.rpt" "r_rearSpring_upvector_pt_parentConstraint1.crt"
		;
connectAttr "JNT_r_rearWidth.t" "r_rearSpring_upvector_pt_parentConstraint1.tg[0].tt"
		;
connectAttr "JNT_r_rearWidth.rp" "r_rearSpring_upvector_pt_parentConstraint1.tg[0].trp"
		;
connectAttr "JNT_r_rearWidth.rpt" "r_rearSpring_upvector_pt_parentConstraint1.tg[0].trt"
		;
connectAttr "JNT_r_rearWidth.r" "r_rearSpring_upvector_pt_parentConstraint1.tg[0].tr"
		;
connectAttr "JNT_r_rearWidth.ro" "r_rearSpring_upvector_pt_parentConstraint1.tg[0].tro"
		;
connectAttr "JNT_r_rearWidth.s" "r_rearSpring_upvector_pt_parentConstraint1.tg[0].ts"
		;
connectAttr "JNT_r_rearWidth.pm" "r_rearSpring_upvector_pt_parentConstraint1.tg[0].tpm"
		;
connectAttr "JNT_r_rearWidth.jo" "r_rearSpring_upvector_pt_parentConstraint1.tg[0].tjo"
		;
connectAttr "JNT_r_rearWidth.ssc" "r_rearSpring_upvector_pt_parentConstraint1.tg[0].tsc"
		;
connectAttr "JNT_r_rearWidth.is" "r_rearSpring_upvector_pt_parentConstraint1.tg[0].tis"
		;
connectAttr "r_rearSpring_upvector_pt_parentConstraint1.w0" "r_rearSpring_upvector_pt_parentConstraint1.tg[0].tw"
		;
connectAttr "l_rearSpring_upvector_pt_parentConstraint1.ctx" "l_rearSpring_upvector_pt.tx"
		;
connectAttr "l_rearSpring_upvector_pt_parentConstraint1.cty" "l_rearSpring_upvector_pt.ty"
		;
connectAttr "l_rearSpring_upvector_pt_parentConstraint1.ctz" "l_rearSpring_upvector_pt.tz"
		;
connectAttr "l_rearSpring_upvector_pt_parentConstraint1.crx" "l_rearSpring_upvector_pt.rx"
		;
connectAttr "l_rearSpring_upvector_pt_parentConstraint1.cry" "l_rearSpring_upvector_pt.ry"
		;
connectAttr "l_rearSpring_upvector_pt_parentConstraint1.crz" "l_rearSpring_upvector_pt.rz"
		;
connectAttr "l_rearSpring_upvector_pt.ro" "l_rearSpring_upvector_pt_parentConstraint1.cro"
		;
connectAttr "l_rearSpring_upvector_pt.pim" "l_rearSpring_upvector_pt_parentConstraint1.cpim"
		;
connectAttr "l_rearSpring_upvector_pt.rp" "l_rearSpring_upvector_pt_parentConstraint1.crp"
		;
connectAttr "l_rearSpring_upvector_pt.rpt" "l_rearSpring_upvector_pt_parentConstraint1.crt"
		;
connectAttr "JNT_l_rearWidth.t" "l_rearSpring_upvector_pt_parentConstraint1.tg[0].tt"
		;
connectAttr "JNT_l_rearWidth.rp" "l_rearSpring_upvector_pt_parentConstraint1.tg[0].trp"
		;
connectAttr "JNT_l_rearWidth.rpt" "l_rearSpring_upvector_pt_parentConstraint1.tg[0].trt"
		;
connectAttr "JNT_l_rearWidth.r" "l_rearSpring_upvector_pt_parentConstraint1.tg[0].tr"
		;
connectAttr "JNT_l_rearWidth.ro" "l_rearSpring_upvector_pt_parentConstraint1.tg[0].tro"
		;
connectAttr "JNT_l_rearWidth.s" "l_rearSpring_upvector_pt_parentConstraint1.tg[0].ts"
		;
connectAttr "JNT_l_rearWidth.pm" "l_rearSpring_upvector_pt_parentConstraint1.tg[0].tpm"
		;
connectAttr "JNT_l_rearWidth.jo" "l_rearSpring_upvector_pt_parentConstraint1.tg[0].tjo"
		;
connectAttr "JNT_l_rearWidth.ssc" "l_rearSpring_upvector_pt_parentConstraint1.tg[0].tsc"
		;
connectAttr "JNT_l_rearWidth.is" "l_rearSpring_upvector_pt_parentConstraint1.tg[0].tis"
		;
connectAttr "l_rearSpring_upvector_pt_parentConstraint1.w0" "l_rearSpring_upvector_pt_parentConstraint1.tg[0].tw"
		;
connectAttr "CNT_l_frontWheel_top_01.t" "CLS_l_frontWheel_top_01.t";
connectAttr "CNT_l_frontWheel_top_04.t" "CLS_l_frontWheel_top_02.t";
connectAttr "CNT_l_frontWheel_top_03.t" "CLS_l_frontWheel_top_03.t";
connectAttr "CNT_l_frontWheel_top_02.t" "CLS_l_frontWheel_top_04.t";
connectAttr "CNT_l_frontWheel_bot_01.t" "CLS_l_frontWheel_bot_01.t";
connectAttr "CNT_l_frontWheel_bot_04.t" "CLS_l_frontWheel_bot_02.t";
connectAttr "CNT_l_frontWheel_bot_03.t" "CLS_l_frontWheel_bot_03.t";
connectAttr "CNT_l_frontWheel_bot_02.t" "CLS_l_frontWheel_bot_04.t";
connectAttr "CNT_l_rearWheel_top_01.t" "CLS_l_rearWheel_top_01.t";
connectAttr "CNT_l_rearWheel_top_04.t" "CLS_l_rearWheel_top_02.t";
connectAttr "CNT_l_rearWheel_top_03.t" "CLS_l_rearWheel_top_03.t";
connectAttr "CNT_l_rearWheel_top_02.t" "CLS_l_rearWheel_top_04.t";
connectAttr "CNT_l_rearWheel_bot_01.t" "CLS_l_rearWheel_bot_01.t";
connectAttr "CNT_l_rearWheel_bot_04.t" "CLS_l_rearWheel_bot_02.t";
connectAttr "CNT_l_rearWheel_bot_03.t" "CLS_l_rearWheel_bot_03.t";
connectAttr "CNT_l_rearWheel_bot_02.t" "CLS_l_rearWheel_bot_04.t";
connectAttr "CNT_r_rearWheel_top_01.t" "CLS_r_rearWheel_top_01.t";
connectAttr "CNT_r_rearWheel_top_02.t" "CLS_r_rearWheel_top_02.t";
connectAttr "CNT_r_rearWheel_top_03.t" "CLS_r_rearWheel_top_03.t";
connectAttr "CNT_r_rearWheel_top_04.t" "CLS_r_rearWheel_top_04.t";
connectAttr "CNT_r_rearWheel_bot_01.t" "CLS_r_rearWheel_bot_01.t";
connectAttr "CNT_r_rearWheel_bot_02.t" "CLS_r_rearWheel_bot_02.t";
connectAttr "CNT_r_rearWheel_bot_03.t" "CLS_r_rearWheel_bot_03.t";
connectAttr "CNT_r_rearWheel_bot_04.t" "CLS_r_rearWheel_bot_04.t";
connectAttr "|CTRL_set|CTRL_setsubControl|GRP_Name_rig|GRP_Controls|GRP_constraint|Transform_Ctrl|GRP_main|Main_Ctrl|main_null|CNT_drive|GRP_offset_Pivotdrive|CNT_frontAxis|GRP_rearAxis_reset|CNT_rearAxis|GRP_bank_R|GRP_bank_L|GRP_offset_drive|GRP_frontWheelAdjust|GRP_r_frontWheel_reset|CNT_r_frontWheel|GRP_deformer_r_frontWheel|GRP_offset_r_frontwheel_top_01|CNT_r_frontWheel_top_01.t" "CLS_r_frontWheel_top_01.t"
		;
connectAttr "CNT_r_frontWheel_top_02.t" "CLS_r_frontWheel_top_02.t";
connectAttr "CNT_r_frontWheel_top_03.t" "CLS_r_frontWheel_top_03.t";
connectAttr "CNT_r_frontWheel_top_04.t" "CLS_r_frontWheel_top_04.t";
connectAttr "CNT_r_frontWheel_bot_02.t" "CLS_r_frontWheel_bot_02.t";
connectAttr "CNT_r_frontWheel_bot_01.t" "CLS_r_frontWheel_bot_01.t";
connectAttr "CNT_r_frontWheel_bot_03.t" "CLS_r_frontWheel_bot_03.t";
connectAttr "CNT_r_frontWheel_bot_04.t" "CLS_r_frontWheel_bot_04.t";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "wheels_tutorial_shdr_SE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "chassis_tutorial_shdr_SE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "springs_tutorial_shdr_SE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "tire_tutorial_shdr_SE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":defaultObjectSet.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":ikSystem.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":hyperGraphLayout.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":defaultHardwareRenderGlobals.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":defaultRenderGlobals.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":defaultRenderGlobals.message" ":initialParticleSE.message";
relationship "link" ":lightLinker1" ":defaultLightSet.message" ":initialParticleSE.message";
relationship "link" ":lightLinker1" ":defaultLightSet.message" ":initialShadingGroup.message";
relationship "link" ":lightLinker1" ":defaultRenderGlobals.message" ":initialShadingGroup.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":initialShadingGroup.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "wheels_tutorial_shdr_SE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "chassis_tutorial_shdr_SE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "springs_tutorial_shdr_SE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "tire_tutorial_shdr_SE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":defaultHardwareRenderGlobals.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":ikSystem.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "CTRL_setsubControl.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "Main_Ctrl.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "Transform_Ctrl.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CTRL_set.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_rearAxis.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_body.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_drive.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_frontAxis.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_l_frontWheel.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_l_rearWheel.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_r_frontWheel.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_r_rearWheel.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_r_rearWheel_bot_04.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_r_rearWheel_bot_03.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_r_rearWheel_bot_02.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_r_rearWheel_bot_01.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_r_rearWheel_top_04.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_r_rearWheel_top_03.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_r_rearWheel_top_02.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_r_rearWheel_top_01.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_l_rearWheel_bot_04.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_l_rearWheel_bot_03.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_l_rearWheel_bot_02.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_l_rearWheel_bot_01.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_l_rearWheel_top_04.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_l_rearWheel_top_03.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_l_rearWheel_top_02.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_l_rearWheel_top_01.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_l_frontWheel_bot_04.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_l_frontWheel_bot_03.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_l_frontWheel_bot_02.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_l_frontWheel_bot_01.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_l_frontWheel_top_04.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_l_frontWheel_top_03.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_l_frontWheel_top_02.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_l_frontWheel_top_01.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_r_frontWheel_bot_04.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_r_frontWheel_bot_03.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_r_frontWheel_bot_01.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_r_frontWheel_bot_02.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_r_frontWheel_top_04.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_r_frontWheel_top_03.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "CNT_r_frontWheel_top_02.iog" "ALL_CONTROLS.dsm" -na;
connectAttr "|CTRL_set|CTRL_setsubControl|GRP_Name_rig|GRP_Controls|GRP_constraint|Transform_Ctrl|GRP_main|Main_Ctrl|main_null|CNT_drive|GRP_offset_Pivotdrive|CNT_frontAxis|GRP_rearAxis_reset|CNT_rearAxis|GRP_bank_R|GRP_bank_L|GRP_offset_drive|GRP_frontWheelAdjust|GRP_r_frontWheel_reset|CNT_r_frontWheel|GRP_deformer_r_frontWheel|GRP_offset_r_frontwheel_top_01|CNT_r_frontWheel_top_01.iog" "ALL_CONTROLS.dsm"
		 -na;
connectAttr "unitConversion507.o" "car_expression.in[0]";
connectAttr "CNT_drive.wheelRadius" "car_expression.in[1]";
connectAttr "CNT_drive.tx" "car_expression.in[2]";
connectAttr "CNT_drive.ty" "car_expression.in[3]";
connectAttr "CNT_drive.tz" "car_expression.in[4]";
connectAttr ":time1.o" "car_expression.tim";
connectAttr "CNT_drive.ry" "unitConversion507.i";
connectAttr "steerDriveCircumferenceFraction_MD.ox" "steerDriveDistance_MD.i1x";
connectAttr "steerDistance_and_invert_MD.ox" "steerDriveCircumferenceFraction_MD.i1x"
		;
connectAttr "wheelCircumferenceCalc_MD.ox" "steerDriveCircumferenceFraction_MD.i2x"
		;
connectAttr "steerCircumferenceFraction_MD.ox" "steerDistance_and_invert_MD.i1x"
		;
connectAttr "CNT_drive.steerRadius" "steerDistance_and_invert_MD.i1y";
connectAttr "plusMinusAverage3.o1" "steerDistance_and_invert_MD.i1z";
connectAttr "steerCircumferenceCalc_MD.ox" "steerDistance_and_invert_MD.i2x";
connectAttr "CNT_drive.steering" "steerCircumferenceFraction_MD.i1x";
connectAttr "CNT_drive.steerRadius" "steerCircumferenceCalc_MD.i1x";
connectAttr "CNT_drive.wheelRadius" "wheelCircumferenceCalc_MD.i1x";
connectAttr "CNT_drive.widthAdjust" "wheel_MD.i1x";
connectAttr "CNT_drive.lengthAdjust" "wheel_MD.i1z";
connectAttr "groupParts37.og" "ffd_L_Rear1.ip[1].ig";
connectAttr "groupId38.id" "ffd_L_Rear1.ip[1].gi";
connectAttr "groupParts38.og" "ffd_L_Rear1.ip[2].ig";
connectAttr "groupId39.id" "ffd_L_Rear1.ip[2].gi";
connectAttr "LTC_l_frontWheelShape.wm" "ffd_L_Rear1.dlm";
connectAttr "LTC_l_frontWheelShape.lo" "ffd_L_Rear1.dlp";
connectAttr "LTC_l_frontWheelBaseShape.wm" "ffd_L_Rear1.blm";
connectAttr "groupId38.msg" "ffd2Set.gn" -na;
connectAttr "groupId39.msg" "ffd2Set.gn" -na;
connectAttr "MSH_wheels_tutorialShape.iog.og[9]" "ffd2Set.dsm" -na;
connectAttr "MSH_tires_tutorialShape.iog.og[18]" "ffd2Set.dsm" -na;
connectAttr "ffd_L_Rear1.msg" "ffd2Set.ub[0]";
connectAttr "skinCluster8.og[0]" "groupParts37.ig";
connectAttr "groupId38.id" "groupParts37.gi";
connectAttr "groupParts49.og" "ffd_R_Front.ip[5].ig";
connectAttr "groupId50.id" "ffd_R_Front.ip[5].gi";
connectAttr "groupParts50.og" "ffd_R_Front.ip[6].ig";
connectAttr "groupId51.id" "ffd_R_Front.ip[6].gi";
connectAttr "LTC_r_frontWheelShape.wm" "ffd_R_Front.dlm";
connectAttr "LTC_r_frontWheelShape.lo" "ffd_R_Front.dlp";
connectAttr "LTC_r_frontWheelBaseShape.wm" "ffd_R_Front.blm";
connectAttr "groupId50.msg" "ffd1Set.gn" -na;
connectAttr "groupId51.msg" "ffd1Set.gn" -na;
connectAttr "MSH_tires_tutorialShape.iog.og[27]" "ffd1Set.dsm" -na;
connectAttr "MSH_wheels_tutorialShape.iog.og[18]" "ffd1Set.dsm" -na;
connectAttr "ffd_R_Front.msg" "ffd1Set.ub[0]";
connectAttr "groupParts15.og" "groupParts16.ig";
connectAttr "groupId17.id" "groupParts16.gi";
connectAttr "MSH_tires_tutorialShapeOrig.w" "groupParts15.ig";
connectAttr "groupId15.id" "groupParts15.gi";
connectAttr "skinCluster9.og[0]" "groupParts38.ig";
connectAttr "groupId39.id" "groupParts38.gi";
connectAttr "cluster16GroupParts.og" "CLS_l_frontWheel_bot_02Cluster.ip[0].ig";
connectAttr "cluster16GroupId.id" "CLS_l_frontWheel_bot_02Cluster.ip[0].gi";
connectAttr "CLS_l_frontWheel_bot_02.wm" "CLS_l_frontWheel_bot_02Cluster.ma";
connectAttr "CLS_l_frontWheel_bot_02Shape.x" "CLS_l_frontWheel_bot_02Cluster.x";
connectAttr "cluster16GroupId.msg" "cluster16Set.gn" -na;
connectAttr "LTC_l_frontWheelShape.iog.og[8]" "cluster16Set.dsm" -na;
connectAttr "CLS_l_frontWheel_bot_02Cluster.msg" "cluster16Set.ub[0]";
connectAttr "CLS_l_frontWheel_bot_03Cluster.og[0]" "cluster16GroupParts.ig";
connectAttr "cluster16GroupId.id" "cluster16GroupParts.gi";
connectAttr "cluster15GroupParts.og" "CLS_l_frontWheel_bot_03Cluster.ip[0].ig";
connectAttr "cluster15GroupId.id" "CLS_l_frontWheel_bot_03Cluster.ip[0].gi";
connectAttr "CLS_l_frontWheel_bot_03.wm" "CLS_l_frontWheel_bot_03Cluster.ma";
connectAttr "CLS_l_frontWheel_bot_03Shape.x" "CLS_l_frontWheel_bot_03Cluster.x";
connectAttr "cluster15GroupId.msg" "cluster15Set.gn" -na;
connectAttr "LTC_l_frontWheelShape.iog.og[7]" "cluster15Set.dsm" -na;
connectAttr "CLS_l_frontWheel_bot_03Cluster.msg" "cluster15Set.ub[0]";
connectAttr "CLS_l_frontWheel_bot_01Cluster.og[0]" "cluster15GroupParts.ig";
connectAttr "cluster15GroupId.id" "cluster15GroupParts.gi";
connectAttr "cluster14GroupParts.og" "CLS_l_frontWheel_bot_01Cluster.ip[0].ig";
connectAttr "cluster14GroupId.id" "CLS_l_frontWheel_bot_01Cluster.ip[0].gi";
connectAttr "CLS_l_frontWheel_bot_01.wm" "CLS_l_frontWheel_bot_01Cluster.ma";
connectAttr "CLS_l_frontWheel_bot_01Shape.x" "CLS_l_frontWheel_bot_01Cluster.x";
connectAttr "cluster14GroupId.msg" "cluster14Set.gn" -na;
connectAttr "LTC_l_frontWheelShape.iog.og[6]" "cluster14Set.dsm" -na;
connectAttr "CLS_l_frontWheel_bot_01Cluster.msg" "cluster14Set.ub[0]";
connectAttr "CLS_l_frontWheel_bot_04Cluster.og[0]" "cluster14GroupParts.ig";
connectAttr "cluster14GroupId.id" "cluster14GroupParts.gi";
connectAttr "cluster13GroupParts.og" "CLS_l_frontWheel_bot_04Cluster.ip[0].ig";
connectAttr "cluster13GroupId.id" "CLS_l_frontWheel_bot_04Cluster.ip[0].gi";
connectAttr "CLS_l_frontWheel_bot_04.wm" "CLS_l_frontWheel_bot_04Cluster.ma";
connectAttr "CLS_l_frontWheel_bot_04Shape.x" "CLS_l_frontWheel_bot_04Cluster.x";
connectAttr "cluster13GroupId.msg" "cluster13Set.gn" -na;
connectAttr "LTC_l_frontWheelShape.iog.og[5]" "cluster13Set.dsm" -na;
connectAttr "CLS_l_frontWheel_bot_04Cluster.msg" "cluster13Set.ub[0]";
connectAttr "CLS_r_frontWheel_top_02Cluster1.og[0]" "cluster13GroupParts.ig";
connectAttr "cluster13GroupId.id" "cluster13GroupParts.gi";
connectAttr "cluster12GroupParts.og" "CLS_r_frontWheel_top_02Cluster1.ip[0].ig";
connectAttr "cluster12GroupId.id" "CLS_r_frontWheel_top_02Cluster1.ip[0].gi";
connectAttr "CLS_l_frontWheel_top_03.wm" "CLS_r_frontWheel_top_02Cluster1.ma";
connectAttr "CLS_l_frontWheel_top_03Shape.x" "CLS_r_frontWheel_top_02Cluster1.x"
		;
connectAttr "cluster12GroupId.msg" "cluster12Set.gn" -na;
connectAttr "LTC_l_frontWheelShape.iog.og[4]" "cluster12Set.dsm" -na;
connectAttr "CLS_r_frontWheel_top_02Cluster1.msg" "cluster12Set.ub[0]";
connectAttr "CLS_r_frontWheel_top_01Cluster1.og[0]" "cluster12GroupParts.ig";
connectAttr "cluster12GroupId.id" "cluster12GroupParts.gi";
connectAttr "cluster11GroupParts.og" "CLS_r_frontWheel_top_01Cluster1.ip[0].ig";
connectAttr "cluster11GroupId.id" "CLS_r_frontWheel_top_01Cluster1.ip[0].gi";
connectAttr "CLS_l_frontWheel_top_02.wm" "CLS_r_frontWheel_top_01Cluster1.ma";
connectAttr "CLS_l_frontWheel_top_02Shape.x" "CLS_r_frontWheel_top_01Cluster1.x"
		;
connectAttr "cluster11GroupId.msg" "cluster11Set.gn" -na;
connectAttr "LTC_l_frontWheelShape.iog.og[3]" "cluster11Set.dsm" -na;
connectAttr "CLS_r_frontWheel_top_01Cluster1.msg" "cluster11Set.ub[0]";
connectAttr "CLS_r_frontWheel_top_03Cluster1.og[0]" "cluster11GroupParts.ig";
connectAttr "cluster11GroupId.id" "cluster11GroupParts.gi";
connectAttr "cluster10GroupParts.og" "CLS_r_frontWheel_top_03Cluster1.ip[0].ig";
connectAttr "cluster10GroupId.id" "CLS_r_frontWheel_top_03Cluster1.ip[0].gi";
connectAttr "CLS_l_frontWheel_top_01.wm" "CLS_r_frontWheel_top_03Cluster1.ma";
connectAttr "CLS_l_frontWheel_top_01Shape.x" "CLS_r_frontWheel_top_03Cluster1.x"
		;
connectAttr "cluster10GroupId.msg" "cluster10Set.gn" -na;
connectAttr "LTC_l_frontWheelShape.iog.og[2]" "cluster10Set.dsm" -na;
connectAttr "CLS_r_frontWheel_top_03Cluster1.msg" "cluster10Set.ub[0]";
connectAttr "CLS_r_frontWheel_top_04Cluster1.og[0]" "cluster10GroupParts.ig";
connectAttr "cluster10GroupId.id" "cluster10GroupParts.gi";
connectAttr "cluster9GroupParts.og" "CLS_r_frontWheel_top_04Cluster1.ip[0].ig";
connectAttr "cluster9GroupId.id" "CLS_r_frontWheel_top_04Cluster1.ip[0].gi";
connectAttr "CLS_l_frontWheel_top_04.wm" "CLS_r_frontWheel_top_04Cluster1.ma";
connectAttr "CLS_l_frontWheel_top_04Shape.x" "CLS_r_frontWheel_top_04Cluster1.x"
		;
connectAttr "cluster9GroupId.msg" "cluster9Set.gn" -na;
connectAttr "LTC_l_frontWheelShape.iog.og[0]" "cluster9Set.dsm" -na;
connectAttr "CLS_r_frontWheel_top_04Cluster1.msg" "cluster9Set.ub[0]";
connectAttr "LTC_l_frontWheelShapeOrig.wl" "cluster9GroupParts.ig";
connectAttr "cluster9GroupId.id" "cluster9GroupParts.gi";
connectAttr "CNT_drive.steering" "unitConversion487.i";
connectAttr "CNT_drive.wheelRadius" "wheelDisplay1.r";
connectAttr "CNT_drive.steering" "unitConversion488.i";
connectAttr "CNT_drive.wheelRadius" "wheelDisplay3.r";
connectAttr "groupParts41.og" "ffd_R_Rear.ip[1].ig";
connectAttr "groupId42.id" "ffd_R_Rear.ip[1].gi";
connectAttr "groupParts42.og" "ffd_R_Rear.ip[2].ig";
connectAttr "groupId43.id" "ffd_R_Rear.ip[2].gi";
connectAttr "LTC_r_rearWheelShape.wm" "ffd_R_Rear.dlm";
connectAttr "LTC_r_rearWheelShape.lo" "ffd_R_Rear.dlp";
connectAttr "LTC_r_rearWheelBaseShape.wm" "ffd_R_Rear.blm";
connectAttr "groupId42.msg" "ffd4Set.gn" -na;
connectAttr "groupId43.msg" "ffd4Set.gn" -na;
connectAttr "MSH_wheels_tutorialShape.iog.og[11]" "ffd4Set.dsm" -na;
connectAttr "MSH_tires_tutorialShape.iog.og[20]" "ffd4Set.dsm" -na;
connectAttr "ffd_R_Rear.msg" "ffd4Set.ub[0]";
connectAttr "ffd_L_Rear.og[1]" "groupParts41.ig";
connectAttr "groupId42.id" "groupParts41.gi";
connectAttr "groupParts39.og" "ffd_L_Rear.ip[1].ig";
connectAttr "groupId40.id" "ffd_L_Rear.ip[1].gi";
connectAttr "groupParts40.og" "ffd_L_Rear.ip[2].ig";
connectAttr "groupId41.id" "ffd_L_Rear.ip[2].gi";
connectAttr "LTC_l_rearWheelShape.wm" "ffd_L_Rear.dlm";
connectAttr "LTC_l_rearWheelShape.lo" "ffd_L_Rear.dlp";
connectAttr "LTC_l_rearWheelBaseShape.wm" "ffd_L_Rear.blm";
connectAttr "groupId40.msg" "ffd3Set.gn" -na;
connectAttr "groupId41.msg" "ffd3Set.gn" -na;
connectAttr "MSH_wheels_tutorialShape.iog.og[10]" "ffd3Set.dsm" -na;
connectAttr "MSH_tires_tutorialShape.iog.og[19]" "ffd3Set.dsm" -na;
connectAttr "ffd_L_Rear.msg" "ffd3Set.ub[0]";
connectAttr "ffd_L_Rear1.og[1]" "groupParts39.ig";
connectAttr "groupId40.id" "groupParts39.gi";
connectAttr "ffd_L_Rear1.og[2]" "groupParts40.ig";
connectAttr "groupId41.id" "groupParts40.gi";
connectAttr "ffd_L_Rear.og[2]" "groupParts42.ig";
connectAttr "groupId43.id" "groupParts42.gi";
connectAttr "cluster32GroupParts.og" "CLS_r_rearWheel_bot_02Cluster.ip[0].ig";
connectAttr "cluster32GroupId.id" "CLS_r_rearWheel_bot_02Cluster.ip[0].gi";
connectAttr "CLS_r_rearWheel_bot_02.wm" "CLS_r_rearWheel_bot_02Cluster.ma";
connectAttr "CLS_r_rearWheel_bot_02Shape.x" "CLS_r_rearWheel_bot_02Cluster.x";
connectAttr "cluster32GroupId.msg" "cluster32Set.gn" -na;
connectAttr "LTC_r_rearWheelShape.iog.og[8]" "cluster32Set.dsm" -na;
connectAttr "CLS_r_rearWheel_bot_02Cluster.msg" "cluster32Set.ub[0]";
connectAttr "CLS_r_rearWheel_top_04Cluster.og[0]" "cluster32GroupParts.ig";
connectAttr "cluster32GroupId.id" "cluster32GroupParts.gi";
connectAttr "cluster31GroupParts.og" "CLS_r_rearWheel_top_04Cluster.ip[0].ig";
connectAttr "cluster31GroupId.id" "CLS_r_rearWheel_top_04Cluster.ip[0].gi";
connectAttr "CLS_r_rearWheel_top_04.wm" "CLS_r_rearWheel_top_04Cluster.ma";
connectAttr "CLS_r_rearWheel_top_04Shape.x" "CLS_r_rearWheel_top_04Cluster.x";
connectAttr "cluster31GroupId.msg" "cluster31Set.gn" -na;
connectAttr "LTC_r_rearWheelShape.iog.og[7]" "cluster31Set.dsm" -na;
connectAttr "CLS_r_rearWheel_top_04Cluster.msg" "cluster31Set.ub[0]";
connectAttr "CLS_r_rearWheel_top_03Cluster.og[0]" "cluster31GroupParts.ig";
connectAttr "cluster31GroupId.id" "cluster31GroupParts.gi";
connectAttr "cluster30GroupParts.og" "CLS_r_rearWheel_top_03Cluster.ip[0].ig";
connectAttr "cluster30GroupId.id" "CLS_r_rearWheel_top_03Cluster.ip[0].gi";
connectAttr "CLS_r_rearWheel_top_03.wm" "CLS_r_rearWheel_top_03Cluster.ma";
connectAttr "CLS_r_rearWheel_top_03Shape.x" "CLS_r_rearWheel_top_03Cluster.x";
connectAttr "cluster30GroupId.msg" "cluster30Set.gn" -na;
connectAttr "LTC_r_rearWheelShape.iog.og[6]" "cluster30Set.dsm" -na;
connectAttr "CLS_r_rearWheel_top_03Cluster.msg" "cluster30Set.ub[0]";
connectAttr "CLS_r_rearWheel_bot_03Cluster.og[0]" "cluster30GroupParts.ig";
connectAttr "cluster30GroupId.id" "cluster30GroupParts.gi";
connectAttr "cluster29GroupParts.og" "CLS_r_rearWheel_bot_03Cluster.ip[0].ig";
connectAttr "cluster29GroupId.id" "CLS_r_rearWheel_bot_03Cluster.ip[0].gi";
connectAttr "CLS_r_rearWheel_bot_03.wm" "CLS_r_rearWheel_bot_03Cluster.ma";
connectAttr "CLS_r_rearWheel_bot_03Shape.x" "CLS_r_rearWheel_bot_03Cluster.x";
connectAttr "cluster29GroupId.msg" "cluster29Set.gn" -na;
connectAttr "LTC_r_rearWheelShape.iog.og[5]" "cluster29Set.dsm" -na;
connectAttr "CLS_r_rearWheel_bot_03Cluster.msg" "cluster29Set.ub[0]";
connectAttr "CLS_r_rearWheel_bot_04Cluster.og[0]" "cluster29GroupParts.ig";
connectAttr "cluster29GroupId.id" "cluster29GroupParts.gi";
connectAttr "cluster28GroupParts.og" "CLS_r_rearWheel_bot_04Cluster.ip[0].ig";
connectAttr "cluster28GroupId.id" "CLS_r_rearWheel_bot_04Cluster.ip[0].gi";
connectAttr "CLS_r_rearWheel_bot_04.wm" "CLS_r_rearWheel_bot_04Cluster.ma";
connectAttr "CLS_r_rearWheel_bot_04Shape.x" "CLS_r_rearWheel_bot_04Cluster.x";
connectAttr "cluster28GroupId.msg" "cluster28Set.gn" -na;
connectAttr "LTC_r_rearWheelShape.iog.og[4]" "cluster28Set.dsm" -na;
connectAttr "CLS_r_rearWheel_bot_04Cluster.msg" "cluster28Set.ub[0]";
connectAttr "CLS_r_rearWheel_bot_01Cluster.og[0]" "cluster28GroupParts.ig";
connectAttr "cluster28GroupId.id" "cluster28GroupParts.gi";
connectAttr "cluster27GroupParts.og" "CLS_r_rearWheel_bot_01Cluster.ip[0].ig";
connectAttr "cluster27GroupId.id" "CLS_r_rearWheel_bot_01Cluster.ip[0].gi";
connectAttr "CLS_r_rearWheel_bot_01.wm" "CLS_r_rearWheel_bot_01Cluster.ma";
connectAttr "CLS_r_rearWheel_bot_01Shape.x" "CLS_r_rearWheel_bot_01Cluster.x";
connectAttr "cluster27GroupId.msg" "cluster27Set.gn" -na;
connectAttr "LTC_r_rearWheelShape.iog.og[3]" "cluster27Set.dsm" -na;
connectAttr "CLS_r_rearWheel_bot_01Cluster.msg" "cluster27Set.ub[0]";
connectAttr "CLS_r_rearWheel_top_02Cluster.og[0]" "cluster27GroupParts.ig";
connectAttr "cluster27GroupId.id" "cluster27GroupParts.gi";
connectAttr "cluster26GroupParts.og" "CLS_r_rearWheel_top_02Cluster.ip[0].ig";
connectAttr "cluster26GroupId.id" "CLS_r_rearWheel_top_02Cluster.ip[0].gi";
connectAttr "CLS_r_rearWheel_top_02.wm" "CLS_r_rearWheel_top_02Cluster.ma";
connectAttr "CLS_r_rearWheel_top_02Shape.x" "CLS_r_rearWheel_top_02Cluster.x";
connectAttr "cluster26GroupId.msg" "cluster26Set.gn" -na;
connectAttr "LTC_r_rearWheelShape.iog.og[2]" "cluster26Set.dsm" -na;
connectAttr "CLS_r_rearWheel_top_02Cluster.msg" "cluster26Set.ub[0]";
connectAttr "CLS_r_rearWheel_top_01Cluster.og[0]" "cluster26GroupParts.ig";
connectAttr "cluster26GroupId.id" "cluster26GroupParts.gi";
connectAttr "cluster25GroupParts.og" "CLS_r_rearWheel_top_01Cluster.ip[0].ig";
connectAttr "cluster25GroupId.id" "CLS_r_rearWheel_top_01Cluster.ip[0].gi";
connectAttr "CLS_r_rearWheel_top_01.wm" "CLS_r_rearWheel_top_01Cluster.ma";
connectAttr "CLS_r_rearWheel_top_01Shape.x" "CLS_r_rearWheel_top_01Cluster.x";
connectAttr "cluster25GroupId.msg" "cluster25Set.gn" -na;
connectAttr "LTC_r_rearWheelShape.iog.og[0]" "cluster25Set.dsm" -na;
connectAttr "CLS_r_rearWheel_top_01Cluster.msg" "cluster25Set.ub[0]";
connectAttr "LTC_r_rearWheelShapeOrig.wl" "cluster25GroupParts.ig";
connectAttr "cluster25GroupId.id" "cluster25GroupParts.gi";
connectAttr "CNT_drive.wheelRadius" "wheelDisplay4.r";
connectAttr "cluster24GroupParts.og" "CLS_l_rearWheel_top_02Cluster.ip[0].ig";
connectAttr "cluster24GroupId.id" "CLS_l_rearWheel_top_02Cluster.ip[0].gi";
connectAttr "CLS_l_rearWheel_top_02.wm" "CLS_l_rearWheel_top_02Cluster.ma";
connectAttr "CLS_l_rearWheel_top_02Shape.x" "CLS_l_rearWheel_top_02Cluster.x";
connectAttr "cluster24GroupId.msg" "cluster24Set.gn" -na;
connectAttr "LTC_l_rearWheelShape.iog.og[8]" "cluster24Set.dsm" -na;
connectAttr "CLS_l_rearWheel_top_02Cluster.msg" "cluster24Set.ub[0]";
connectAttr "CLS_l_rearWheel_top_03Cluster.og[0]" "cluster24GroupParts.ig";
connectAttr "cluster24GroupId.id" "cluster24GroupParts.gi";
connectAttr "cluster23GroupParts.og" "CLS_l_rearWheel_top_03Cluster.ip[0].ig";
connectAttr "cluster23GroupId.id" "CLS_l_rearWheel_top_03Cluster.ip[0].gi";
connectAttr "CLS_l_rearWheel_top_03.wm" "CLS_l_rearWheel_top_03Cluster.ma";
connectAttr "CLS_l_rearWheel_top_03Shape.x" "CLS_l_rearWheel_top_03Cluster.x";
connectAttr "cluster23GroupId.msg" "cluster23Set.gn" -na;
connectAttr "LTC_l_rearWheelShape.iog.og[7]" "cluster23Set.dsm" -na;
connectAttr "CLS_l_rearWheel_top_03Cluster.msg" "cluster23Set.ub[0]";
connectAttr "CLS_l_rearWheel_bot_02Cluster.og[0]" "cluster23GroupParts.ig";
connectAttr "cluster23GroupId.id" "cluster23GroupParts.gi";
connectAttr "cluster22GroupParts.og" "CLS_l_rearWheel_bot_02Cluster.ip[0].ig";
connectAttr "cluster22GroupId.id" "CLS_l_rearWheel_bot_02Cluster.ip[0].gi";
connectAttr "CLS_l_rearWheel_bot_02.wm" "CLS_l_rearWheel_bot_02Cluster.ma";
connectAttr "CLS_l_rearWheel_bot_02Shape.x" "CLS_l_rearWheel_bot_02Cluster.x";
connectAttr "cluster22GroupId.msg" "cluster22Set.gn" -na;
connectAttr "LTC_l_rearWheelShape.iog.og[6]" "cluster22Set.dsm" -na;
connectAttr "CLS_l_rearWheel_bot_02Cluster.msg" "cluster22Set.ub[0]";
connectAttr "CLS_l_rearWheel_bot_03Cluster.og[0]" "cluster22GroupParts.ig";
connectAttr "cluster22GroupId.id" "cluster22GroupParts.gi";
connectAttr "cluster21GroupParts.og" "CLS_l_rearWheel_bot_03Cluster.ip[0].ig";
connectAttr "cluster21GroupId.id" "CLS_l_rearWheel_bot_03Cluster.ip[0].gi";
connectAttr "CLS_l_rearWheel_bot_03.wm" "CLS_l_rearWheel_bot_03Cluster.ma";
connectAttr "CLS_l_rearWheel_bot_03Shape.x" "CLS_l_rearWheel_bot_03Cluster.x";
connectAttr "cluster21GroupId.msg" "cluster21Set.gn" -na;
connectAttr "LTC_l_rearWheelShape.iog.og[5]" "cluster21Set.dsm" -na;
connectAttr "CLS_l_rearWheel_bot_03Cluster.msg" "cluster21Set.ub[0]";
connectAttr "CLS_l_rearWheel_bot_01Cluster.og[0]" "cluster21GroupParts.ig";
connectAttr "cluster21GroupId.id" "cluster21GroupParts.gi";
connectAttr "cluster20GroupParts.og" "CLS_l_rearWheel_bot_01Cluster.ip[0].ig";
connectAttr "cluster20GroupId.id" "CLS_l_rearWheel_bot_01Cluster.ip[0].gi";
connectAttr "CLS_l_rearWheel_bot_01.wm" "CLS_l_rearWheel_bot_01Cluster.ma";
connectAttr "CLS_l_rearWheel_bot_01Shape.x" "CLS_l_rearWheel_bot_01Cluster.x";
connectAttr "cluster20GroupId.msg" "cluster20Set.gn" -na;
connectAttr "LTC_l_rearWheelShape.iog.og[4]" "cluster20Set.dsm" -na;
connectAttr "CLS_l_rearWheel_bot_01Cluster.msg" "cluster20Set.ub[0]";
connectAttr "CLS_l_rearWheel_bot_04Cluster.og[0]" "cluster20GroupParts.ig";
connectAttr "cluster20GroupId.id" "cluster20GroupParts.gi";
connectAttr "cluster19GroupParts.og" "CLS_l_rearWheel_bot_04Cluster.ip[0].ig";
connectAttr "cluster19GroupId.id" "CLS_l_rearWheel_bot_04Cluster.ip[0].gi";
connectAttr "CLS_l_rearWheel_bot_04.wm" "CLS_l_rearWheel_bot_04Cluster.ma";
connectAttr "CLS_l_rearWheel_bot_04Shape.x" "CLS_l_rearWheel_bot_04Cluster.x";
connectAttr "cluster19GroupId.msg" "cluster19Set.gn" -na;
connectAttr "LTC_l_rearWheelShape.iog.og[3]" "cluster19Set.dsm" -na;
connectAttr "CLS_l_rearWheel_bot_04Cluster.msg" "cluster19Set.ub[0]";
connectAttr "CLS_l_rearWheel_top_01Cluster.og[0]" "cluster19GroupParts.ig";
connectAttr "cluster19GroupId.id" "cluster19GroupParts.gi";
connectAttr "cluster18GroupParts.og" "CLS_l_rearWheel_top_01Cluster.ip[0].ig";
connectAttr "cluster18GroupId.id" "CLS_l_rearWheel_top_01Cluster.ip[0].gi";
connectAttr "CLS_l_rearWheel_top_01.wm" "CLS_l_rearWheel_top_01Cluster.ma";
connectAttr "CLS_l_rearWheel_top_01Shape.x" "CLS_l_rearWheel_top_01Cluster.x";
connectAttr "cluster18GroupId.msg" "cluster18Set.gn" -na;
connectAttr "LTC_l_rearWheelShape.iog.og[2]" "cluster18Set.dsm" -na;
connectAttr "CLS_l_rearWheel_top_01Cluster.msg" "cluster18Set.ub[0]";
connectAttr "CLS_l_rearWheel_top_04Cluster.og[0]" "cluster18GroupParts.ig";
connectAttr "cluster18GroupId.id" "cluster18GroupParts.gi";
connectAttr "cluster17GroupParts.og" "CLS_l_rearWheel_top_04Cluster.ip[0].ig";
connectAttr "cluster17GroupId.id" "CLS_l_rearWheel_top_04Cluster.ip[0].gi";
connectAttr "CLS_l_rearWheel_top_04.wm" "CLS_l_rearWheel_top_04Cluster.ma";
connectAttr "CLS_l_rearWheel_top_04Shape.x" "CLS_l_rearWheel_top_04Cluster.x";
connectAttr "cluster17GroupId.msg" "cluster17Set.gn" -na;
connectAttr "LTC_l_rearWheelShape.iog.og[0]" "cluster17Set.dsm" -na;
connectAttr "CLS_l_rearWheel_top_04Cluster.msg" "cluster17Set.ub[0]";
connectAttr "LTC_l_rearWheelShapeOrig.wl" "cluster17GroupParts.ig";
connectAttr "cluster17GroupId.id" "cluster17GroupParts.gi";
connectAttr "CNT_drive.wheelRadius" "wheelDisplay2.r";
connectAttr "CND_l_frontWheel_spin.ocr" "unitConversion502.i";
connectAttr "CNT_drive.front_wheel_auto_spin" "CND_l_frontWheel_spin.ft";
connectAttr "l_frontDrive_PMA.o1" "CND_l_frontWheel_spin.ctr";
connectAttr "CNT_drive.frontWheelSpin" "CND_l_frontWheel_spin.cfr";
connectAttr "frontWheelSpin_PMA.o1" "l_frontDrive_PMA.i1[0]";
connectAttr "CNT_drive.steerDrive" "l_frontDrive_PMA.i1[1]";
connectAttr "CNT_drive.frontWheelSpin" "frontWheelSpin_PMA.i1[0]";
connectAttr "MLD_FrontWheelsSpin.ox" "frontWheelSpin_PMA.i1[1]";
connectAttr "CND_l_frontWheel_spin.ocr" "unitConversion503.i";
connectAttr "CND_rearWheel_spin.ocr" "unitConversion505.i";
connectAttr "rearWheelSpin_PMA.o1" "CND_rearWheel_spin.ctr";
connectAttr "CNT_drive.rear_wheel_auto_spin" "CND_rearWheel_spin.ft";
connectAttr "CNT_drive.rearWheelSpin" "CND_rearWheel_spin.cfr";
connectAttr "CNT_drive.rearWheelSpin" "rearWheelSpin_PMA.i1[0]";
connectAttr "MLD_RearWheelsSpin.ox" "rearWheelSpin_PMA.i1[1]";
connectAttr "CND_rearWheel_spin.ocr" "unitConversion506.i";
connectAttr "CNT_drive.lengthAdjust" "frontAxleAdjust_PMA.i1[1]";
connectAttr "frontArm_MD.ox" "unitConversion494.i";
connectAttr "unitConversion493.o" "frontArm_MD.i1x";
connectAttr "unitConversion495.o" "frontArm_MD.i1y";
connectAttr "JNT_l_frontArm.rz" "unitConversion493.i";
connectAttr "JNT_r_frontArm.rz" "unitConversion495.i";
connectAttr "frontArm_MD.oy" "unitConversion496.i";
connectAttr "rearArm_MD.ox" "unitConversion498.i";
connectAttr "unitConversion497.o" "rearArm_MD.i1x";
connectAttr "unitConversion499.o" "rearArm_MD.i1y";
connectAttr "JNT_l_rearArm.rz" "unitConversion497.i";
connectAttr "JNT_r_rearArm.rz" "unitConversion499.i";
connectAttr "rearArm_MD.oy" "unitConversion500.i";
connectAttr "skinCluster1GroupId.msg" "SK_chassis_tutorialSet.gn" -na;
connectAttr "MSH_chassis_tutorialShape.iog.og[0]" "SK_chassis_tutorialSet.dsm" -na
		;
connectAttr "SK_chassis_tutorial.msg" "SK_chassis_tutorialSet.ub[0]";
connectAttr "skinCluster1GroupParts.og" "SK_chassis_tutorial.ip[0].ig";
connectAttr "skinCluster1GroupId.id" "SK_chassis_tutorial.ip[0].gi";
connectAttr "JNT_root.wm" "SK_chassis_tutorial.ma[0]";
connectAttr "JNT_chasis.wm" "SK_chassis_tutorial.ma[1]";
connectAttr "JNT_l_frontWidth.wm" "SK_chassis_tutorial.ma[2]";
connectAttr "JNT_r_frontWidth.wm" "SK_chassis_tutorial.ma[3]";
connectAttr "JNT_l_rearWidth.wm" "SK_chassis_tutorial.ma[4]";
connectAttr "JNT_r_rearWidth.wm" "SK_chassis_tutorial.ma[5]";
connectAttr "JNT_root.liw" "SK_chassis_tutorial.lw[0]";
connectAttr "JNT_chasis.liw" "SK_chassis_tutorial.lw[1]";
connectAttr "JNT_l_frontWidth.liw" "SK_chassis_tutorial.lw[2]";
connectAttr "JNT_r_frontWidth.liw" "SK_chassis_tutorial.lw[3]";
connectAttr "JNT_l_rearWidth.liw" "SK_chassis_tutorial.lw[4]";
connectAttr "JNT_r_rearWidth.liw" "SK_chassis_tutorial.lw[5]";
connectAttr "JNT_root.obcc" "SK_chassis_tutorial.ifcl[0]";
connectAttr "JNT_chasis.obcc" "SK_chassis_tutorial.ifcl[1]";
connectAttr "JNT_l_frontWidth.obcc" "SK_chassis_tutorial.ifcl[2]";
connectAttr "JNT_r_frontWidth.obcc" "SK_chassis_tutorial.ifcl[3]";
connectAttr "JNT_l_rearWidth.obcc" "SK_chassis_tutorial.ifcl[4]";
connectAttr "JNT_r_rearWidth.obcc" "SK_chassis_tutorial.ifcl[5]";
connectAttr "JNT_r_rearWidth.msg" "SK_chassis_tutorial.ptt";
connectAttr "MSH_chassis_tutorialShapeOrig.w" "skinCluster1GroupParts.ig";
connectAttr "skinCluster1GroupId.id" "skinCluster1GroupParts.gi";
connectAttr "chassis_tutorial_shdr_SE.msg" "materialInfo3.sg";
connectAttr "chassis_tutorial_shdr.msg" "materialInfo3.m";
connectAttr "chassis_tutorial_shdr.oc" "chassis_tutorial_shdr_SE.ss";
connectAttr "MSH_wheelMount_tutorialShape.iog" "chassis_tutorial_shdr_SE.dsm" -na
		;
connectAttr "MSH_chassis_tutorialShape.iog" "chassis_tutorial_shdr_SE.dsm" -na;
connectAttr "MSH_arms_tutorialShape.iog" "chassis_tutorial_shdr_SE.dsm" -na;
connectAttr "tire_tutorial_shdr.oc" "tire_tutorial_shdr_SE.ss";
connectAttr "MSH_tires_tutorialShape.iog.og[2]" "tire_tutorial_shdr_SE.dsm" -na;
connectAttr "MSH_tires_tutorialShape.ciog.cog[0]" "tire_tutorial_shdr_SE.dsm" -na
		;
connectAttr "groupId15.msg" "tire_tutorial_shdr_SE.gn" -na;
connectAttr "groupId16.msg" "tire_tutorial_shdr_SE.gn" -na;
connectAttr "tire_tutorial_shdr_SE.msg" "materialInfo1.sg";
connectAttr "tire_tutorial_shdr.msg" "materialInfo1.m";
connectAttr "lambert2.oc" "lambert2SG.ss";
connectAttr "groupId17.msg" "lambert2SG.gn" -na;
connectAttr "MSH_tires_tutorialShape.iog.og[3]" "lambert2SG.dsm" -na;
connectAttr "lambert2SG.msg" "materialInfo5.sg";
connectAttr "lambert2.msg" "materialInfo5.m";
connectAttr "wheels_tutorial_shdr_SE.msg" "materialInfo2.sg";
connectAttr "wheels_tutorial_shdr.msg" "materialInfo2.m";
connectAttr "wheels_tutorial_shdr.oc" "wheels_tutorial_shdr_SE.ss";
connectAttr "MSH_wheels_tutorialShape.iog" "wheels_tutorial_shdr_SE.dsm" -na;
connectAttr "skinCluster4GroupId.msg" "SK_wheelMount_tutorialSet.gn" -na;
connectAttr "MSH_wheelMount_tutorialShape.iog.og[0]" "SK_wheelMount_tutorialSet.dsm"
		 -na;
connectAttr "SK_wheelMount_tutorial.msg" "SK_wheelMount_tutorialSet.ub[0]";
connectAttr "skinCluster4GroupParts.og" "SK_wheelMount_tutorial.ip[0].ig";
connectAttr "skinCluster4GroupId.id" "SK_wheelMount_tutorial.ip[0].gi";
connectAttr "JNT_l_frontBall.wm" "SK_wheelMount_tutorial.ma[0]";
connectAttr "JNT_r_frontBall.wm" "SK_wheelMount_tutorial.ma[1]";
connectAttr "JNT_l_reartBall.wm" "SK_wheelMount_tutorial.ma[2]";
connectAttr "JNT_r_rearBall.wm" "SK_wheelMount_tutorial.ma[3]";
connectAttr "JNT_l_steering.wm" "SK_wheelMount_tutorial.ma[4]";
connectAttr "JNT_r_steering.wm" "SK_wheelMount_tutorial.ma[5]";
connectAttr "JNT_l_frontBall.liw" "SK_wheelMount_tutorial.lw[0]";
connectAttr "JNT_r_frontBall.liw" "SK_wheelMount_tutorial.lw[1]";
connectAttr "JNT_l_reartBall.liw" "SK_wheelMount_tutorial.lw[2]";
connectAttr "JNT_r_rearBall.liw" "SK_wheelMount_tutorial.lw[3]";
connectAttr "JNT_l_steering.liw" "SK_wheelMount_tutorial.lw[4]";
connectAttr "JNT_r_steering.liw" "SK_wheelMount_tutorial.lw[5]";
connectAttr "JNT_l_frontBall.obcc" "SK_wheelMount_tutorial.ifcl[0]";
connectAttr "JNT_r_frontBall.obcc" "SK_wheelMount_tutorial.ifcl[1]";
connectAttr "JNT_l_reartBall.obcc" "SK_wheelMount_tutorial.ifcl[2]";
connectAttr "JNT_r_rearBall.obcc" "SK_wheelMount_tutorial.ifcl[3]";
connectAttr "JNT_l_steering.obcc" "SK_wheelMount_tutorial.ifcl[4]";
connectAttr "JNT_r_steering.obcc" "SK_wheelMount_tutorial.ifcl[5]";
connectAttr "MSH_wheelMount_tutorialShapeOrig.w" "skinCluster4GroupParts.ig";
connectAttr "skinCluster4GroupId.id" "skinCluster4GroupParts.gi";
connectAttr "skinCluster5GroupId.msg" "SK_arms_tutorialSet.gn" -na;
connectAttr "MSH_arms_tutorialShape.iog.og[0]" "SK_arms_tutorialSet.dsm" -na;
connectAttr "SK_arms_tutorial.msg" "SK_arms_tutorialSet.ub[0]";
connectAttr "skinCluster5GroupParts.og" "SK_arms_tutorial.ip[0].ig";
connectAttr "skinCluster5GroupId.id" "SK_arms_tutorial.ip[0].gi";
connectAttr "JNT_r_rearLowerArm.wm" "SK_arms_tutorial.ma[0]";
connectAttr "JNT_r_rearLowerBall.wm" "SK_arms_tutorial.ma[1]";
connectAttr "JNT_r_rearArm.wm" "SK_arms_tutorial.ma[2]";
connectAttr "JNT_r_rearUpperArm.wm" "SK_arms_tutorial.ma[3]";
connectAttr "JNT_l_rearArm.wm" "SK_arms_tutorial.ma[4]";
connectAttr "JNT_l_rearUpperArm.wm" "SK_arms_tutorial.ma[5]";
connectAttr "JNT_l_rearLowerArm.wm" "SK_arms_tutorial.ma[6]";
connectAttr "JNT_l_rearLowerBall.wm" "SK_arms_tutorial.ma[7]";
connectAttr "JNT_r_frontLowerArm.wm" "SK_arms_tutorial.ma[8]";
connectAttr "JNT_r_frontLowerBall.wm" "SK_arms_tutorial.ma[9]";
connectAttr "JNT_r_frontArm.wm" "SK_arms_tutorial.ma[10]";
connectAttr "JNT_r_frontUpperArm.wm" "SK_arms_tutorial.ma[11]";
connectAttr "JNT_l_frontArm.wm" "SK_arms_tutorial.ma[12]";
connectAttr "JNT_l_frontUpperArm.wm" "SK_arms_tutorial.ma[13]";
connectAttr "JNT_l_frontLowerArm.wm" "SK_arms_tutorial.ma[14]";
connectAttr "JNT_l_frontLowerBall.wm" "SK_arms_tutorial.ma[15]";
connectAttr "JNT_r_rearLowerArm.liw" "SK_arms_tutorial.lw[0]";
connectAttr "JNT_r_rearLowerBall.liw" "SK_arms_tutorial.lw[1]";
connectAttr "JNT_r_rearArm.liw" "SK_arms_tutorial.lw[2]";
connectAttr "JNT_r_rearUpperArm.liw" "SK_arms_tutorial.lw[3]";
connectAttr "JNT_l_rearArm.liw" "SK_arms_tutorial.lw[4]";
connectAttr "JNT_l_rearUpperArm.liw" "SK_arms_tutorial.lw[5]";
connectAttr "JNT_l_rearLowerArm.liw" "SK_arms_tutorial.lw[6]";
connectAttr "JNT_l_rearLowerBall.liw" "SK_arms_tutorial.lw[7]";
connectAttr "JNT_r_frontLowerArm.liw" "SK_arms_tutorial.lw[8]";
connectAttr "JNT_r_frontLowerBall.liw" "SK_arms_tutorial.lw[9]";
connectAttr "JNT_r_frontArm.liw" "SK_arms_tutorial.lw[10]";
connectAttr "JNT_r_frontUpperArm.liw" "SK_arms_tutorial.lw[11]";
connectAttr "JNT_l_frontArm.liw" "SK_arms_tutorial.lw[12]";
connectAttr "JNT_l_frontUpperArm.liw" "SK_arms_tutorial.lw[13]";
connectAttr "JNT_l_frontLowerArm.liw" "SK_arms_tutorial.lw[14]";
connectAttr "JNT_l_frontLowerBall.liw" "SK_arms_tutorial.lw[15]";
connectAttr "JNT_r_rearLowerArm.obcc" "SK_arms_tutorial.ifcl[0]";
connectAttr "JNT_r_rearLowerBall.obcc" "SK_arms_tutorial.ifcl[1]";
connectAttr "JNT_r_rearArm.obcc" "SK_arms_tutorial.ifcl[2]";
connectAttr "JNT_r_rearUpperArm.obcc" "SK_arms_tutorial.ifcl[3]";
connectAttr "JNT_l_rearArm.obcc" "SK_arms_tutorial.ifcl[4]";
connectAttr "JNT_l_rearUpperArm.obcc" "SK_arms_tutorial.ifcl[5]";
connectAttr "JNT_l_rearLowerArm.obcc" "SK_arms_tutorial.ifcl[6]";
connectAttr "JNT_l_rearLowerBall.obcc" "SK_arms_tutorial.ifcl[7]";
connectAttr "JNT_r_frontLowerArm.obcc" "SK_arms_tutorial.ifcl[8]";
connectAttr "JNT_r_frontLowerBall.obcc" "SK_arms_tutorial.ifcl[9]";
connectAttr "JNT_r_frontArm.obcc" "SK_arms_tutorial.ifcl[10]";
connectAttr "JNT_r_frontUpperArm.obcc" "SK_arms_tutorial.ifcl[11]";
connectAttr "JNT_l_frontArm.obcc" "SK_arms_tutorial.ifcl[12]";
connectAttr "JNT_l_frontUpperArm.obcc" "SK_arms_tutorial.ifcl[13]";
connectAttr "JNT_l_frontLowerArm.obcc" "SK_arms_tutorial.ifcl[14]";
connectAttr "JNT_l_frontLowerBall.obcc" "SK_arms_tutorial.ifcl[15]";
connectAttr "MSH_arms_tutorialShapeOrig.w" "skinCluster5GroupParts.ig";
connectAttr "skinCluster5GroupId.id" "skinCluster5GroupParts.gi";
connectAttr "skinCluster7GroupId.msg" "SK_springs_tutorialSet.gn" -na;
connectAttr "MSH_springs_tutorialShape.iog.og[0]" "SK_springs_tutorialSet.dsm" -na
		;
connectAttr "SK_springs_tutorial.msg" "SK_springs_tutorialSet.ub[0]";
connectAttr "skinCluster7GroupParts.og" "SK_springs_tutorial.ip[0].ig";
connectAttr "skinCluster7GroupId.id" "SK_springs_tutorial.ip[0].gi";
connectAttr "JNT_r_rearLowerSpring.wm" "SK_springs_tutorial.ma[0]";
connectAttr "JNT_r_rearUpperSpring.wm" "SK_springs_tutorial.ma[1]";
connectAttr "JNT_l_rearUpperSpring.wm" "SK_springs_tutorial.ma[2]";
connectAttr "JNT_l_rearLowerSpring.wm" "SK_springs_tutorial.ma[3]";
connectAttr "JNT_r_frontLowerSpring.wm" "SK_springs_tutorial.ma[4]";
connectAttr "JNT_r_frontUpperSpring.wm" "SK_springs_tutorial.ma[5]";
connectAttr "JNT_l_frontUpperSpring.wm" "SK_springs_tutorial.ma[6]";
connectAttr "JNT_l_frontLowerSpring.wm" "SK_springs_tutorial.ma[7]";
connectAttr "JNT_r_rearLowerSpring.liw" "SK_springs_tutorial.lw[0]";
connectAttr "JNT_r_rearUpperSpring.liw" "SK_springs_tutorial.lw[1]";
connectAttr "JNT_l_rearUpperSpring.liw" "SK_springs_tutorial.lw[2]";
connectAttr "JNT_l_rearLowerSpring.liw" "SK_springs_tutorial.lw[3]";
connectAttr "JNT_r_frontLowerSpring.liw" "SK_springs_tutorial.lw[4]";
connectAttr "JNT_r_frontUpperSpring.liw" "SK_springs_tutorial.lw[5]";
connectAttr "JNT_l_frontUpperSpring.liw" "SK_springs_tutorial.lw[6]";
connectAttr "JNT_l_frontLowerSpring.liw" "SK_springs_tutorial.lw[7]";
connectAttr "JNT_r_rearLowerSpring.obcc" "SK_springs_tutorial.ifcl[0]";
connectAttr "JNT_r_rearUpperSpring.obcc" "SK_springs_tutorial.ifcl[1]";
connectAttr "JNT_l_rearUpperSpring.obcc" "SK_springs_tutorial.ifcl[2]";
connectAttr "JNT_l_rearLowerSpring.obcc" "SK_springs_tutorial.ifcl[3]";
connectAttr "JNT_r_frontLowerSpring.obcc" "SK_springs_tutorial.ifcl[4]";
connectAttr "JNT_r_frontUpperSpring.obcc" "SK_springs_tutorial.ifcl[5]";
connectAttr "JNT_l_frontUpperSpring.obcc" "SK_springs_tutorial.ifcl[6]";
connectAttr "JNT_l_frontLowerSpring.obcc" "SK_springs_tutorial.ifcl[7]";
connectAttr "MSH_springs_tutorialShapeOrig.w" "skinCluster7GroupParts.ig";
connectAttr "skinCluster7GroupId.id" "skinCluster7GroupParts.gi";
connectAttr "springs_tutorial_shdr_SE.msg" "materialInfo4.sg";
connectAttr "springs_tutorial_shdr.msg" "materialInfo4.m";
connectAttr "springs_tutorial_shdr.oc" "springs_tutorial_shdr_SE.ss";
connectAttr "MSH_springs_tutorialShape.iog" "springs_tutorial_shdr_SE.dsm" -na;
connectAttr "MSH_springs_tutorial_skinMeshShape.iog" "springs_tutorial_shdr_SE.dsm"
		 -na;
connectAttr "skinCluster6GroupId.msg" "skinCluster6Set.gn" -na;
connectAttr "MSH_springs_tutorial_skinMeshShape.iog.og[2]" "skinCluster6Set.dsm"
		 -na;
connectAttr "skinCluster6.msg" "skinCluster6Set.ub[0]";
connectAttr "skinCluster6GroupParts.og" "skinCluster6.ip[0].ig";
connectAttr "skinCluster6GroupId.id" "skinCluster6.ip[0].gi";
connectAttr "JNT_r_rearLowerSpring.wm" "skinCluster6.ma[0]";
connectAttr "JNT_r_rearUpperSpring.wm" "skinCluster6.ma[1]";
connectAttr "JNT_l_rearUpperSpring.wm" "skinCluster6.ma[2]";
connectAttr "JNT_l_rearLowerSpring.wm" "skinCluster6.ma[3]";
connectAttr "JNT_r_frontLowerSpring.wm" "skinCluster6.ma[4]";
connectAttr "JNT_r_frontUpperSpring.wm" "skinCluster6.ma[5]";
connectAttr "JNT_l_frontUpperSpring.wm" "skinCluster6.ma[6]";
connectAttr "JNT_l_frontLowerSpring.wm" "skinCluster6.ma[7]";
connectAttr "JNT_r_rearLowerSpring.liw" "skinCluster6.lw[0]";
connectAttr "JNT_r_rearUpperSpring.liw" "skinCluster6.lw[1]";
connectAttr "JNT_l_rearUpperSpring.liw" "skinCluster6.lw[2]";
connectAttr "JNT_l_rearLowerSpring.liw" "skinCluster6.lw[3]";
connectAttr "JNT_r_frontLowerSpring.liw" "skinCluster6.lw[4]";
connectAttr "JNT_r_frontUpperSpring.liw" "skinCluster6.lw[5]";
connectAttr "JNT_l_frontUpperSpring.liw" "skinCluster6.lw[6]";
connectAttr "JNT_l_frontLowerSpring.liw" "skinCluster6.lw[7]";
connectAttr "JNT_r_rearLowerSpring.obcc" "skinCluster6.ifcl[0]";
connectAttr "JNT_r_rearUpperSpring.obcc" "skinCluster6.ifcl[1]";
connectAttr "JNT_l_rearUpperSpring.obcc" "skinCluster6.ifcl[2]";
connectAttr "JNT_l_rearLowerSpring.obcc" "skinCluster6.ifcl[3]";
connectAttr "JNT_r_frontLowerSpring.obcc" "skinCluster6.ifcl[4]";
connectAttr "JNT_r_frontUpperSpring.obcc" "skinCluster6.ifcl[5]";
connectAttr "JNT_l_frontUpperSpring.obcc" "skinCluster6.ifcl[6]";
connectAttr "JNT_l_frontLowerSpring.obcc" "skinCluster6.ifcl[7]";
connectAttr "MSH_springs_tutorial_skinMeshShapeOrig.w" "skinCluster6GroupParts.ig"
		;
connectAttr "skinCluster6GroupId.id" "skinCluster6GroupParts.gi";
connectAttr "CNT_drive.bank" "MLD_bank.i1x";
connectAttr "CNT_drive.bank" "MLD_bank.i1y";
connectAttr "CNT_drive.bank" "CND_bank_L.ft";
connectAttr "MLD_bank.oy" "CND_bank_L.ctr";
connectAttr "CNT_drive.bank" "CND_bank_R.ft";
connectAttr "MLD_bank.ox" "CND_bank_R.ctr";
connectAttr "CND_bank_L.ocr" "unitConversion508.i";
connectAttr "CND_bank_R.ocr" "unitConversion509.i";
connectAttr "CNT_drive.lengthAdjust" "MLD_structure_adjust_01.i1x";
connectAttr "SR_structure_adjust.ox" "MLD_structure_adjust_01.i1z";
connectAttr "MLD_structure_adjust_02.ox" "PLS_structureAdjust_lenght.i1[1]";
connectAttr "PLS_structureAdjust_lenght.o1" "MLD_structure_adjust_03.i1x";
connectAttr "PLS_structureAdjust_width.o1" "MLD_structure_adjust_03.i1z";
connectAttr "MLD_structure_adjust_03.ox" "unitConversion510.i";
connectAttr "PLS_structureAdjust_lenght.o1" "unitConversion512.i";
connectAttr "MLD_structure_adjust_01.ox" "MLD_structure_adjust_02.i1x";
connectAttr "MLD_structure_adjust_01.oz" "MLD_structure_adjust_02.i1z";
connectAttr "MLD_structure_adjust_02.oz" "PLS_structureAdjust_width.i1[1]";
connectAttr "PLS_structureAdjust_width.o1" "unitConversion514.i";
connectAttr "MLD_structure_adjust_03.oz" "unitConversion515.i";
connectAttr "CNT_drive.widthAdjust" "SR_structure_adjust.vx";
connectAttr "CNT_drive.lengthAdjust" "MLD_pivotBodyCtrl.i1x";
connectAttr "CNT_drive.bank_pivot_adjust" "MLD_bankPivot.i1x";
connectAttr "CNT_drive.bank_pivot_adjust" "MLD_bankPivot.i1y";
connectAttr "GRP_PivotBank_L.tx" "PMA_bankPivot_L.i1[0]";
connectAttr "GRP_PivotBank_R.tx" "PMA_bankPivot_R.i1[0]";
connectAttr "CNT_drive.wheelRadius" "plusMinusAverage2.i1[0]";
connectAttr "CNT_drive.steerRadius" "plusMinusAverage3.i1[0]";
connectAttr "CNT_drive.wheelRadius" "plusMinusAverage4.i1[0]";
connectAttr "skinCluster8GroupParts.og" "skinCluster8.ip[0].ig";
connectAttr "skinCluster8GroupId.id" "skinCluster8.ip[0].gi";
connectAttr "JNT_l_frontWheel.wm" "skinCluster8.ma[0]";
connectAttr "JNT_r_frontWheel.wm" "skinCluster8.ma[1]";
connectAttr "JNT_l_rearWheel.wm" "skinCluster8.ma[2]";
connectAttr "JNT_r_rearWheel.wm" "skinCluster8.ma[3]";
connectAttr "JNT_l_frontWheel.liw" "skinCluster8.lw[0]";
connectAttr "JNT_r_frontWheel.liw" "skinCluster8.lw[1]";
connectAttr "JNT_l_rearWheel.liw" "skinCluster8.lw[2]";
connectAttr "JNT_r_rearWheel.liw" "skinCluster8.lw[3]";
connectAttr "JNT_l_frontWheel.obcc" "skinCluster8.ifcl[0]";
connectAttr "JNT_r_frontWheel.obcc" "skinCluster8.ifcl[1]";
connectAttr "JNT_l_rearWheel.obcc" "skinCluster8.ifcl[2]";
connectAttr "JNT_r_rearWheel.obcc" "skinCluster8.ifcl[3]";
connectAttr "JNT_r_rearWheel.msg" "skinCluster8.ptt";
connectAttr "skinCluster8GroupId.msg" "skinCluster8Set.gn" -na;
connectAttr "MSH_wheels_tutorialShape.iog.og[12]" "skinCluster8Set.dsm" -na;
connectAttr "skinCluster8.msg" "skinCluster8Set.ub[0]";
connectAttr "MSH_wheels_tutorialShapeOrig.w" "skinCluster8GroupParts.ig";
connectAttr "skinCluster8GroupId.id" "skinCluster8GroupParts.gi";
connectAttr "skinCluster9GroupParts.og" "skinCluster9.ip[0].ig";
connectAttr "skinCluster9GroupId.id" "skinCluster9.ip[0].gi";
connectAttr "JNT_l_frontWheel.wm" "skinCluster9.ma[0]";
connectAttr "JNT_r_frontWheel.wm" "skinCluster9.ma[1]";
connectAttr "JNT_l_rearWheel.wm" "skinCluster9.ma[2]";
connectAttr "JNT_r_rearWheel.wm" "skinCluster9.ma[3]";
connectAttr "JNT_l_frontWheel.liw" "skinCluster9.lw[0]";
connectAttr "JNT_r_frontWheel.liw" "skinCluster9.lw[1]";
connectAttr "JNT_l_rearWheel.liw" "skinCluster9.lw[2]";
connectAttr "JNT_r_rearWheel.liw" "skinCluster9.lw[3]";
connectAttr "JNT_l_frontWheel.obcc" "skinCluster9.ifcl[0]";
connectAttr "JNT_r_frontWheel.obcc" "skinCluster9.ifcl[1]";
connectAttr "JNT_l_rearWheel.obcc" "skinCluster9.ifcl[2]";
connectAttr "JNT_r_rearWheel.obcc" "skinCluster9.ifcl[3]";
connectAttr "JNT_r_rearWheel.msg" "skinCluster9.ptt";
connectAttr "skinCluster9GroupId.msg" "skinCluster9Set.gn" -na;
connectAttr "MSH_tires_tutorialShape.iog.og[21]" "skinCluster9Set.dsm" -na;
connectAttr "skinCluster9.msg" "skinCluster9Set.ub[0]";
connectAttr "groupParts16.og" "skinCluster9GroupParts.ig";
connectAttr "skinCluster9GroupId.id" "skinCluster9GroupParts.gi";
connectAttr "cluster33GroupParts.og" "CLS_r_frontWheel_top_02Cluster.ip[0].ig";
connectAttr "cluster33GroupId.id" "CLS_r_frontWheel_top_02Cluster.ip[0].gi";
connectAttr "CLS_r_frontWheel_top_02.wm" "CLS_r_frontWheel_top_02Cluster.ma";
connectAttr "CLS_r_frontWheel_top_02Shape.x" "CLS_r_frontWheel_top_02Cluster.x";
connectAttr "cluster33GroupId.msg" "cluster33Set.gn" -na;
connectAttr "LTC_r_frontWheelShape.iog.og[9]" "cluster33Set.dsm" -na;
connectAttr "CLS_r_frontWheel_top_02Cluster.msg" "cluster33Set.ub[0]";
connectAttr "LTC_r_frontWheelShapeOrig.wl" "cluster33GroupParts.ig";
connectAttr "cluster33GroupId.id" "cluster33GroupParts.gi";
connectAttr "cluster34GroupParts.og" "CLS_r_frontWheel_top_01Cluster.ip[0].ig";
connectAttr "cluster34GroupId.id" "CLS_r_frontWheel_top_01Cluster.ip[0].gi";
connectAttr "CLS_r_frontWheel_top_01.wm" "CLS_r_frontWheel_top_01Cluster.ma";
connectAttr "CLS_r_frontWheel_top_01Shape.x" "CLS_r_frontWheel_top_01Cluster.x";
connectAttr "cluster34GroupId.msg" "cluster34Set.gn" -na;
connectAttr "LTC_r_frontWheelShape.iog.og[11]" "cluster34Set.dsm" -na;
connectAttr "CLS_r_frontWheel_top_01Cluster.msg" "cluster34Set.ub[0]";
connectAttr "CLS_r_frontWheel_top_02Cluster.og[0]" "cluster34GroupParts.ig";
connectAttr "cluster34GroupId.id" "cluster34GroupParts.gi";
connectAttr "cluster35GroupParts.og" "CLS_r_frontWheel_top_03Cluster.ip[0].ig";
connectAttr "cluster35GroupId.id" "CLS_r_frontWheel_top_03Cluster.ip[0].gi";
connectAttr "CLS_r_frontWheel_top_03.wm" "CLS_r_frontWheel_top_03Cluster.ma";
connectAttr "CLS_r_frontWheel_top_03Shape.x" "CLS_r_frontWheel_top_03Cluster.x";
connectAttr "cluster35GroupId.msg" "cluster35Set.gn" -na;
connectAttr "LTC_r_frontWheelShape.iog.og[12]" "cluster35Set.dsm" -na;
connectAttr "CLS_r_frontWheel_top_03Cluster.msg" "cluster35Set.ub[0]";
connectAttr "CLS_r_frontWheel_top_01Cluster.og[0]" "cluster35GroupParts.ig";
connectAttr "cluster35GroupId.id" "cluster35GroupParts.gi";
connectAttr "cluster36GroupParts.og" "CLS_r_frontWheel_top_04Cluster.ip[0].ig";
connectAttr "cluster36GroupId.id" "CLS_r_frontWheel_top_04Cluster.ip[0].gi";
connectAttr "CLS_r_frontWheel_top_04.wm" "CLS_r_frontWheel_top_04Cluster.ma";
connectAttr "CLS_r_frontWheel_top_04Shape.x" "CLS_r_frontWheel_top_04Cluster.x";
connectAttr "cluster36GroupId.msg" "cluster36Set.gn" -na;
connectAttr "LTC_r_frontWheelShape.iog.og[13]" "cluster36Set.dsm" -na;
connectAttr "CLS_r_frontWheel_top_04Cluster.msg" "cluster36Set.ub[0]";
connectAttr "CLS_r_frontWheel_top_03Cluster.og[0]" "cluster36GroupParts.ig";
connectAttr "cluster36GroupId.id" "cluster36GroupParts.gi";
connectAttr "cluster37GroupParts.og" "CLS_r_frontWheel_bot_02Cluster.ip[0].ig";
connectAttr "cluster37GroupId.id" "CLS_r_frontWheel_bot_02Cluster.ip[0].gi";
connectAttr "CLS_r_frontWheel_bot_02.wm" "CLS_r_frontWheel_bot_02Cluster.ma";
connectAttr "CLS_r_frontWheel_bot_02Shape.x" "CLS_r_frontWheel_bot_02Cluster.x";
connectAttr "cluster37GroupId.msg" "cluster37Set.gn" -na;
connectAttr "LTC_r_frontWheelShape.iog.og[14]" "cluster37Set.dsm" -na;
connectAttr "CLS_r_frontWheel_bot_02Cluster.msg" "cluster37Set.ub[0]";
connectAttr "CLS_r_frontWheel_top_04Cluster.og[0]" "cluster37GroupParts.ig";
connectAttr "cluster37GroupId.id" "cluster37GroupParts.gi";
connectAttr "cluster38GroupParts.og" "CLS_r_frontWheel_bot_01Cluster.ip[0].ig";
connectAttr "cluster38GroupId.id" "CLS_r_frontWheel_bot_01Cluster.ip[0].gi";
connectAttr "CLS_r_frontWheel_bot_01.wm" "CLS_r_frontWheel_bot_01Cluster.ma";
connectAttr "CLS_r_frontWheel_bot_01Shape.x" "CLS_r_frontWheel_bot_01Cluster.x";
connectAttr "cluster38GroupId.msg" "cluster38Set.gn" -na;
connectAttr "LTC_r_frontWheelShape.iog.og[15]" "cluster38Set.dsm" -na;
connectAttr "CLS_r_frontWheel_bot_01Cluster.msg" "cluster38Set.ub[0]";
connectAttr "CLS_r_frontWheel_bot_02Cluster.og[0]" "cluster38GroupParts.ig";
connectAttr "cluster38GroupId.id" "cluster38GroupParts.gi";
connectAttr "cluster39GroupParts.og" "CLS_r_frontWheel_bot_03Cluster.ip[0].ig";
connectAttr "cluster39GroupId.id" "CLS_r_frontWheel_bot_03Cluster.ip[0].gi";
connectAttr "CLS_r_frontWheel_bot_03.wm" "CLS_r_frontWheel_bot_03Cluster.ma";
connectAttr "CLS_r_frontWheel_bot_03Shape.x" "CLS_r_frontWheel_bot_03Cluster.x";
connectAttr "cluster39GroupId.msg" "cluster39Set.gn" -na;
connectAttr "LTC_r_frontWheelShape.iog.og[16]" "cluster39Set.dsm" -na;
connectAttr "CLS_r_frontWheel_bot_03Cluster.msg" "cluster39Set.ub[0]";
connectAttr "CLS_r_frontWheel_bot_01Cluster.og[0]" "cluster39GroupParts.ig";
connectAttr "cluster39GroupId.id" "cluster39GroupParts.gi";
connectAttr "cluster40GroupParts.og" "CLS_r_frontWheel_bot_04Cluster.ip[0].ig";
connectAttr "cluster40GroupId.id" "CLS_r_frontWheel_bot_04Cluster.ip[0].gi";
connectAttr "CLS_r_frontWheel_bot_04.wm" "CLS_r_frontWheel_bot_04Cluster.ma";
connectAttr "CLS_r_frontWheel_bot_04Shape.x" "CLS_r_frontWheel_bot_04Cluster.x";
connectAttr "cluster40GroupId.msg" "cluster40Set.gn" -na;
connectAttr "LTC_r_frontWheelShape.iog.og[17]" "cluster40Set.dsm" -na;
connectAttr "CLS_r_frontWheel_bot_04Cluster.msg" "cluster40Set.ub[0]";
connectAttr "CLS_r_frontWheel_bot_03Cluster.og[0]" "cluster40GroupParts.ig";
connectAttr "cluster40GroupId.id" "cluster40GroupParts.gi";
connectAttr "ffd_R_Rear.og[2]" "groupParts49.ig";
connectAttr "groupId50.id" "groupParts49.gi";
connectAttr "ffd_R_Rear.og[1]" "groupParts50.ig";
connectAttr "groupId51.id" "groupParts50.gi";
connectAttr "CNT_drive.wheelDrive" "MLD_FrontWheelsSpin.i1x";
connectAttr "CNT_drive.wheelDrive" "MLD_RearWheelsSpin.i1x";
connectAttr "ALL_CONTROLS.msg" "SETS.dnsm" -na;
connectAttr "All_Rig_Driver_Meshes_RM.msg" "SETS.dnsm" -na;
connectAttr "MSH.msg" "SETS.dnsm" -na;
connectAttr "ffd_L_Rear.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[0].dn";
connectAttr "ffd_R_Front.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[1].dn";
connectAttr "LTC_l_rearWheelBaseShape.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[2].dn"
		;
connectAttr "MSH_wheels_tutorialShape.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[3].dn"
		;
connectAttr "LTC_l_frontWheelBaseShape.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[4].dn"
		;
connectAttr "LTC_l_rearWheelShape.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[5].dn"
		;
connectAttr "ffd1Set.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[6].dn";
connectAttr "ffd_R_Rear.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[7].dn";
connectAttr "ffd4Set.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[8].dn";
connectAttr "LTC_r_rearWheelBaseShape.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[9].dn"
		;
connectAttr "MSH_tires_tutorialShape.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[10].dn"
		;
connectAttr "LTC_r_rearWheelShape.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[11].dn"
		;
connectAttr "ffd3Set.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[12].dn";
connectAttr "LTC_l_frontWheelShape.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[13].dn"
		;
connectAttr "LTC_r_frontWheelShape.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[14].dn"
		;
connectAttr "ffd_L_Rear1.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[15].dn";
connectAttr "LTC_r_frontWheelBaseShape.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[16].dn"
		;
connectAttr "ffd2Set.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[17].dn";
connectAttr "tire_tutorial_shdr_SE.pa" ":renderPartition.st" -na;
connectAttr "wheels_tutorial_shdr_SE.pa" ":renderPartition.st" -na;
connectAttr "chassis_tutorial_shdr_SE.pa" ":renderPartition.st" -na;
connectAttr "springs_tutorial_shdr_SE.pa" ":renderPartition.st" -na;
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "wheels_tutorial_shdr.msg" ":defaultShaderList1.s" -na;
connectAttr "chassis_tutorial_shdr.msg" ":defaultShaderList1.s" -na;
connectAttr "springs_tutorial_shdr.msg" ":defaultShaderList1.s" -na;
connectAttr "tire_tutorial_shdr.msg" ":defaultShaderList1.s" -na;
connectAttr "lambert2.msg" ":defaultShaderList1.s" -na;
connectAttr "wheel_MD.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "l_frontDrive_PMA.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "steerCircumferenceFraction_MD.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "steerCircumferenceCalc_MD.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "steerDistance_and_invert_MD.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "steerDriveCircumferenceFraction_MD.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "wheelCircumferenceCalc_MD.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "steerDriveDistance_MD.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "frontAxleAdjust_PMA.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "frontArm_MD.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "rearArm_MD.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "rearWheelSpin_PMA.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "frontWheelSpin_PMA.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "CND_l_frontWheel_spin.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "CND_rearWheel_spin.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "MLD_bank.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "CND_bank_L.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "CND_bank_R.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "MLD_structure_adjust_01.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "PLS_structureAdjust_lenght.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "MLD_structure_adjust_03.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "MLD_structure_adjust_02.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "PLS_structureAdjust_width.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "SR_structure_adjust.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "MLD_pivotBodyCtrl.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "MLD_bankPivot.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "PMA_bankPivot_L.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "PMA_bankPivot_R.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "plusMinusAverage2.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "plusMinusAverage3.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "plusMinusAverage4.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "MLD_FrontWheelsSpin.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "MLD_RearWheelsSpin.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
dataStructure -fmt "raw" -as "name=notes_pPlane5:string=value";
dataStructure -fmt "raw" -as "name=notes_groundA_parShape:string=value";
dataStructure -fmt "raw" -as "name=mapManager_snapshot_Combined:string=value";
dataStructure -fmt "raw" -as "name=IdStruct:int32=ID";
dataStructure -fmt "raw" -as "name=notes_groundD_parShape:string=value";
dataStructure -fmt "raw" -as "name=faceConnectMarkerStructure:bool=faceConnectMarker:string[200]=faceConnectOutputGroups";
dataStructure -fmt "raw" -as "name=mapManager_slopesGroundGrassC_Combined:string=value";
dataStructure -fmt "raw" -as "name=mapManager_polySurface56:string=value";
dataStructure -fmt "raw" -as "name=notes_wildPatchC_parShape:string=value";
dataStructure -fmt "raw" -as "name=mapManager_grassBase:string=value";
dataStructure -fmt "raw" -as "name=faceConnectOutputStructure:bool=faceConnectOutput:string[200]=faceConnectOutputAttributes:string[200]=faceConnectOutputGroups";
dataStructure -fmt "raw" -as "name=idStructure:int32=ID";
dataStructure -fmt "raw" -as "name=notes_mountains_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_slopesGroundGrassA_Combined:string=value";
dataStructure -fmt "raw" -as "name=mapManager_pPlane4:string=value";
dataStructure -fmt "raw" -as "name=notes_wildPatchDegraded_parShape:string=value";
dataStructure -fmt "raw" -as "name=mapManager_base_hojas:string=value";
dataStructure -fmt "raw" -as "name=mapManager_slopesGroundGrassA_Combined:string=value";
dataStructure -fmt "raw" -as "name=FBXFastExportSetting_MB:string=19424";
dataStructure -fmt "raw" -as "name=mapManager_slopesGroundGrassB_Combined:string=value";
dataStructure -fmt "raw" -as "name=externalContentTablZ:string=nodZ:string=key:string=upath:uint32=upathcrc:string=rpath:string=roles";
dataStructure -fmt "raw" -as "name=OffStruct:float=Offset";
dataStructure -fmt "raw" -as "name=mapManager_groundWoods_c_geo1:string=value";
dataStructure -fmt "raw" -as "name=notes_baseScatter:string=value";
dataStructure -fmt "raw" -as "name=Offset:float[3]=value";
dataStructure -fmt "raw" -as "name=notes_groundC_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_pPlane1:string=value";
dataStructure -fmt "raw" -as "name=notes_decayGrassPatchC_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_decayLeaves_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_grassBase:string=value";
dataStructure -fmt "raw" -as "name=mapManager_pPlane6:string=value";
dataStructure -fmt "raw" -as "name=notes_groundWoods_c_geo1:string=value";
dataStructure -fmt "raw" -as "name=notes_bushes_parShape:string=value";
dataStructure -fmt "raw" -as "name=mapManager_floorOrangeConcrete_c_geo:string=value";
dataStructure -fmt "raw" -as "name=notes_polySurface56:string=value";
dataStructure -fmt "raw" -as "name=notes_decayGrassPatchA_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_floorOrangeConcrete_c_geo:string=value";
dataStructure -fmt "raw" -as "name=mapManager_ground:string=value";
dataStructure -fmt "raw" -as "name=notes_grassJuneBackYard_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_decayGrassPatchD_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_baseScatt:string=value";
dataStructure -fmt "raw" -as "name=notes_ground:string=value";
dataStructure -fmt "raw" -as "name=mapManager_baseScatt:string=value";
dataStructure -fmt "raw" -as "name=notes_decayGrassPatchB_parShape:string=value";
dataStructure -fmt "raw" -as "name=mapManager_slopesGroundGrassD_Combined:string=value";
dataStructure -fmt "raw" -as "name=notes_slopes_parShape:string=value";
dataStructure -fmt "raw" -as "name=FBXFastExportSetting_FBX:string=54";
dataStructure -fmt "raw" -as "name=mapManager_baseLeaves:string=value";
dataStructure -fmt "raw" -as "name=notes_trees_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_suelo:string=value";
dataStructure -fmt "raw" -as "name=mapManager_degraded:string=value";
dataStructure -fmt "raw" -as "name=mapManager_ground_c_geo:string=value";
dataStructure -fmt "raw" -as "name=mapManager_snapshot_floor:string=value";
dataStructure -fmt "raw" -as "name=notes_degraded:string=value";
dataStructure -fmt "raw" -as "name=notes_wildPatchG_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_pPlane2:string=value";
dataStructure -fmt "raw" -as "name=notes_pPlane6:string=value";
dataStructure -fmt "raw" -as "name=mapManager_suelo:string=value";
dataStructure -fmt "raw" -as "name=mapManager_grass_c_geo:string=value";
dataStructure -fmt "raw" -as "name=notes_trees_left:string=value";
dataStructure -fmt "raw" -as "name=notes_trees_left1:string=value";
dataStructure -fmt "raw" -as "name=Curvature:float=mean:float=gaussian:float=ABS:float=RMS";
dataStructure -fmt "raw" -as "name=notes_pPlane4:string=value";
dataStructure -fmt "raw" -as "name=mapManager_pPlane2:string=value";
dataStructure -fmt "raw" -as "name=notes_wildPatchF_parShape:string=value";
dataStructure -fmt "raw" -as "name=mapManager_baseScatter:string=value";
dataStructure -fmt "raw" -as "name=mapManager_slopesMountainsGrass_Combined:string=value";
dataStructure -fmt "raw" -as "name=notes_snapshot_Combined:string=value";
dataStructure -fmt "raw" -as "name=mapManager_trees_left:string=value";
dataStructure -fmt "raw" -as "name=notes_grass_c_geo1:string=value";
dataStructure -fmt "raw" -as "name=mapManager_base_right:string=value";
dataStructure -fmt "raw" -as "name=mapManager_trees_left1:string=value";
dataStructure -fmt "raw" -as "name=notes_groundB_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_right_parShape:string=value";
dataStructure -fmt "raw" -as "name=mapManager_juneBackYard:string=value";
dataStructure -fmt "raw" -as "name=notes_wildPatchD_parShape:string=value";
dataStructure -fmt "raw" -as "name=Blur3dMetaData:string=Blur3dValue";
dataStructure -fmt "raw" -as "name=notes_wildPatchE_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_baseLeaves:string=value";
dataStructure -fmt "raw" -as "name=notes_slopesGroundGrassD_Combined:string=value";
dataStructure -fmt "raw" -as "name=notes_grass_c_geo:string=value";
dataStructure -fmt "raw" -as "name=OrgStruct:float[3]=Origin Point";
dataStructure -fmt "raw" -as "name=notes_snapshot_floor:string=value";
dataStructure -fmt "raw" -as "name=mapManager_base_left:string=value";
dataStructure -fmt "raw" -as "name=notes_wildPatchA_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_decayLeavesCarousel_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_slopesMountainsGrass_Combined:string=value";
dataStructure -fmt "raw" -as "name=notes_pPlane3:string=value";
dataStructure -fmt "raw" -as "name=DiffEdge:float=value";
dataStructure -fmt "raw" -as "name=notes_snapshot_CombinedGrass:string=value";
dataStructure -fmt "raw" -as "name=notes_widlPatchB_parShape:string=value";
dataStructure -fmt "raw" -as "name=mapManager_snapshot_CombinedGrass:string=value";
dataStructure -fmt "raw" -as "name=notes_base_hojas:string=value";
dataStructure -fmt "raw" -as "name=notes_ferns_parShape:string=value";
dataStructure -fmt "raw" -as "name=mapManager_grass_c_geo1:string=value";
dataStructure -fmt "raw" -as "name=notes_slopesGroundGrassB_Combined:string=value";
dataStructure -fmt "raw" -as "name=keyValueStructure:string=value";
dataStructure -fmt "raw" -as "name=notes_base_right:string=value";
dataStructure -fmt "raw" -as "name=notes_wildPatchH_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_slopesGroundGrassC_Combined:string=value";
dataStructure -fmt "raw" -as "name=DiffArea:float=value";
dataStructure -fmt "raw" -as "name=notes_left_parShape:string=value";
dataStructure -fmt "raw" -as "name=mapManager_pPlane1:string=value";
dataStructure -fmt "raw" -as "name=notes_decayGrassesCenter_parShape:string=value";
dataStructure -fmt "raw" -as "name=notes_base_left:string=value";
dataStructure -fmt "raw" -as "name=mapManager_pPlane5:string=value";
dataStructure -fmt "raw" -as "name=notes_juneBackYard:string=value";
dataStructure -fmt "raw" -as "name=notes_ground_c_geo:string=value";
dataStructure -fmt "raw" -as "name=mapManager_pPlane3:string=value";
// End of default_vehicles.ma
