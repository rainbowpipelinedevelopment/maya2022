import importlib
import os

import maya.cmds as cmd

import api.log
import api.json
import api.database
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.log)
importlib.reload(api.json)
importlib.reload(api.database)
# importlib.reload(rbwUI)


################################################################################
# @brief      Create a Meshcut Set
#
# @return     True if the set is created, False otherwise
#
def meshcutSet():
    api.log.logger().debug('Updating meshcut set')
    assetId = cmd.getAttr('rigging_saveNode.asset_id')
    pathQuery = "SELECT path FROM `savedAsset` WHERE `assetId` = {} AND `dept` = 'grooming' AND `visible`= 1 ORDER BY `version` DESC".format(assetId)
    paths = api.database.selectQuery(pathQuery)
    if len(paths) > 0:
        # find the latest version of the groom folder
        pipeMayaProject = os.getenv('MC_FOLDER').replace('\\', '/')
        folderPath = os.path.dirname(paths[0][0])
        groomMayaProject = os.path.join(pipeMayaProject, folderPath).replace('\\', '/')

        # search for the JSON file containing the Mesh set information
        groomMeshes = []
        for file in os.listdir(groomMayaProject):
            if file.endswith('_meshcut.json'):
                groomMeshes = api.json.json_read(os.path.join(groomMayaProject, file))["meshes"]

        # meshcut set
        if groomMeshes:
            meshcutMeshes = []
            missingMeshes = []
            for mesh in groomMeshes:
                if cmd.objExists(mesh):
                    meshcutMeshes.append(mesh)
                else:
                    missingMeshes.append(mesh)

            if not missingMeshes:
                try:
                    cmd.select('meshcut', noExpand=True)
                    cmd.delete()
                except ValueError:
                    pass
                cmd.sets(meshcutMeshes, name='meshcut')
            else:
                # if some of the meshes are missing the procedure is interrupted
                # LIST OF THE MISSING MESHES AND THE AVAILABLE MESHES
                api.log.logger().debug('AVAILABLE MESHES:')
                for x in meshcutMeshes:
                    api.log.logger().debug(x)
                api.log.logger().debug('MISSING MESHES:')
                for y in missingMeshes:
                    api.log.logger().debug(y)

                missingMeshesMessage = 'Some meshes are missing and the procedure is cancelled!\nCheck your script editor for more informations.'
                rbwUI.RBWConfirm(text=missingMeshesMessage, title='Missing Meshes', defCancel=None)
        else:
            # if the json file is missing the procedure is interrupted
            api.log.logger().debug('NO JSON FILE FOUND!')
            api.log.logger().debug('Path of the latest version grooming folder: {}'.format(groomMayaProject))
    else:
        # no grooming def save in content
        api.log.logger().debug('No grooming def save in content')
