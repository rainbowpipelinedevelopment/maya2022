import maya.cmds as cmds
import os
import re

def create_ui():
    if cmds.window("phonemeSwitchUI", exists=True):
        cmds.deleteUI("phonemeSwitchUI")

    window = cmds.window("phonemeSwitchUI", title="Phoneme Switch Tool", widthHeight=(400, 150))
    cmds.columnLayout(adjustableColumn=True)

    cmds.text(label="Path:")
    path_field = cmds.textField(text=r"\\speed.nas\dev24\02_production\01_content\mc_dev24\sourceimages\character\g\gil\df\anim\Fonemi")

    cmds.text(label="Texture Prefix:")
    prefix_field = cmds.textField(text="PBL_gil_")

    def execute():
        path = cmds.textField(path_field, query=True, text=True)
        texture_prefix = cmds.textField(prefix_field, query=True, text=True)
        create_phoneme_switch_from_ui(path, texture_prefix)

    cmds.button(label="Create Phoneme Switch", command=lambda x: execute())

    cmds.showWindow(window)
    cmds.window(window, edit=True, widthHeight=(400, 150))  # Fissa l'altezza a 150

def create_phoneme_switch_from_ui(path, texture_prefix):
    # Ottieni i nomi dei file nel percorso
    files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]

    # Filtra e modifica i nomi dei file per ottenere i fonemi
    enum_options = ['off']  # La prima scelta è sempre "off"
    for file in files:
        match = re.match(rf"^{texture_prefix}fonemi_(.+?)_C\.png$", file)
        if match:
            enum_options.append(match.group(1))

    # Converti le opzioni in una stringa separata da due punti, come richiesto da Maya
    enum_string = ":".join(enum_options)

    # Ottieni i controlli selezionati
    selected_controls = cmds.ls(selection=True)

    # Aggiungi l'attributo enumerativo ai controlli selezionati
    for control in selected_controls:
        if not cmds.attributeQuery('phonemes', node=control, exists=True):
            cmds.addAttr(control, longName='phonemes', niceName='Phonemes', attributeType='enum', enumName=enum_string)
            cmds.setAttr('{}.phonemes'.format(control), edit=True, keyable=True)

    print(f"Attributo 'phonemes' aggiunto ai controlli selezionati con le opzioni: {enum_options}")

    # Funzione per creare nodi Condition, Layered Texture e Materiale Lambert
    def create_phoneme_switch_for_controls(controls):
        for control in controls:
            # Leggi l'attributo "phonemes"
            phonemes_attr = control + '.phonemes'
            if not cmds.objExists(phonemes_attr):
                cmds.warning(f"{phonemes_attr} does not exist. Skipping control {control}.")
                continue

            # Ottieni le scelte enum dell'attributo "phonemes"
            enum_choices = cmds.attributeQuery('phonemes', node=control, listEnum=True)[0].split(':')
            
            condition_nodes = []
            textures = []
            
            for i, choice in enumerate(enum_choices):
                # Crea un nodo Condition per ogni scelta enum, con il nome del controllo come suffisso
                condition_node = cmds.createNode('condition', name=f"{choice}_{control}_condition")
                condition_nodes.append(condition_node)
                
                # Configura il nodo Condition
                cmds.connectAttr(phonemes_attr, condition_node + '.firstTerm')
                cmds.setAttr(condition_node + '.secondTerm', i)
                cmds.setAttr(condition_node + '.colorIfTrueR', 1)
                cmds.setAttr(condition_node + '.colorIfTrueG', 1)
                cmds.setAttr(condition_node + '.colorIfTrueB', 1)
                cmds.setAttr(condition_node + '.colorIfFalseR', 0)
                cmds.setAttr(condition_node + '.colorIfFalseG', 0)
                cmds.setAttr(condition_node + '.colorIfFalseB', 0)
                
                # Cerca e importa la texture corrispondente se la scelta non è "off"
                if choice != 'off':
                    texture_path = os.path.join(path, f"{texture_prefix}fonemi_{choice}_C.png")
                    if os.path.isfile(texture_path):
                        file_node = cmds.shadingNode('file', asTexture=True, name=f"{choice}_{control}_file")
                        cmds.setAttr(f"{file_node}.fileTextureName", texture_path, type="string")
                        textures.append(file_node)
                    else:
                        textures.append(None)  # Nessuna texture trovata
                else:
                    textures.append(None)  # Nessuna texture per "off"

            # Crea un Layered Texture "_C"
            layered_texture_c = cmds.shadingNode('layeredTexture', asTexture=True, name=f"{control}_C")
            
            for i, (condition_node, texture) in enumerate(zip(condition_nodes, textures)):
                # Aggiungi una nuova istanza al Layered Texture con blend mode "over" (1)
                cmds.setAttr(layered_texture_c + f".inputs[{i}].blendMode", 1)  # Over modalità di miscelazione
                # Connette l'output del nodo condition all'alpha del Layered Texture
                cmds.connectAttr(condition_node + '.outColorR', layered_texture_c + f".inputs[{i}].alpha")
                if texture:
                    # Connette l'output color della texture all'input color dell'istanza del Layered Texture
                    cmds.connectAttr(texture + '.outColor', layered_texture_c + f".inputs[{i}].color")
                else:
                    # Lascia il colore vuoto per "off"
                    cmds.setAttr(layered_texture_c + f".inputs[{i}].color", 0, 0, 0, type="double3")

            # Crea un nuovo materiale Lambert chiamato "fonemi_material"
            phoneme_material = cmds.shadingNode('lambert', asShader=True, name="fonemi_material")
            
            # Connette l'output color del Layered Texture "_C" all'input color del Lambert
            cmds.connectAttr(layered_texture_c + '.outColor', phoneme_material + '.color')

            # Crea un secondo Layered Texture "_M"
            layered_texture_m = cmds.shadingNode('layeredTexture', asTexture=True, name=f"{control}_M")
            
            for i, (condition_node, texture) in enumerate(zip(condition_nodes, textures)):
                # Aggiungi una nuova istanza al Layered Texture con blend mode "over" (1)
                cmds.setAttr(layered_texture_m + f".inputs[{i}].blendMode", 1)  # Over modalità di miscelazione
                # Connette l'output del nodo condition all'alpha del Layered Texture "_M"
                cmds.connectAttr(condition_node + '.outColorR', layered_texture_m + f".inputs[{i}].alpha")
                if texture:
                    # Connette l'output alpha della texture all'input color R,G,B dell'istanza del Layered Texture "_M"
                    cmds.connectAttr(texture + '.outAlpha', layered_texture_m + f".inputs[{i}].colorR")
                    cmds.connectAttr(texture + '.outAlpha', layered_texture_m + f".inputs[{i}].colorG")
                    cmds.connectAttr(texture + '.outAlpha', layered_texture_m + f".inputs[{i}].colorB")
                else:
                    # Lascia il colore vuoto per "off"
                    cmds.setAttr(layered_texture_m + f".inputs[{i}].color", 0, 0, 0, type="double3")

            # Crea un nodo Reverse
            reverse_node = cmds.shadingNode('reverse', asUtility=True, name=f"{control}_reverse")
            
            # Connette l'output color del Layered Texture "_M" all'input del nodo Reverse
            cmds.connectAttr(layered_texture_m + '.outColor', reverse_node + '.input')

            # Connette l'output del nodo Reverse alla transparency del materiale Lambert
            cmds.connectAttr(reverse_node + '.output', phoneme_material + '.transparency')

            # Creazione del materiale "mouth_final_material"
            final_material = cmds.shadingNode('lambert', asShader=True, name="mouth_final_material")

            # Creazione di un nodo Condition per i colori
            color_condition = cmds.shadingNode('condition', asUtility=True, name="color_condition")
            cmds.connectAttr(phonemes_attr, color_condition + '.firstTerm')
            cmds.setAttr(color_condition + '.secondTerm', 0)
            cmds.setAttr(color_condition + '.colorIfTrueR', 1)
            cmds.setAttr(color_condition + '.colorIfTrueG', 1)
            cmds.setAttr(color_condition + '.colorIfTrueB', 1)
            cmds.setAttr(color_condition + '.colorIfFalseR', 0)
            cmds.setAttr(color_condition + '.colorIfFalseG', 0)
            cmds.setAttr(color_condition + '.colorIfFalseB', 0)

            # Connessione degli output color dei materiali
            cmds.connectAttr("fonemi_material.outColor", color_condition + '.colorIfFalse')
            cmds.connectAttr("Mouth_ctrl_material.outColor", color_condition + '.colorIfTrue')

            # Connessione del nodo condition al materiale finale
            cmds.connectAttr(color_condition + '.outColor', final_material + '.color')

            # Creazione di un nodo Condition per le transparency
            transparency_condition = cmds.shadingNode('condition', asUtility=True, name="transparency_condition")
            cmds.connectAttr(phonemes_attr, transparency_condition + '.firstTerm')
            cmds.setAttr(transparency_condition + '.secondTerm', 0)
            cmds.setAttr(transparency_condition + '.colorIfTrueR', 1)
            cmds.setAttr(transparency_condition + '.colorIfTrueG', 1)
            cmds.setAttr(transparency_condition + '.colorIfTrueB', 1)
            cmds.setAttr(transparency_condition + '.colorIfFalseR', 0)
            cmds.setAttr(transparency_condition + '.colorIfFalseG', 0)
            cmds.setAttr(transparency_condition + '.colorIfFalseB', 0)

            # Connessione delle transparency dei materiali
            cmds.connectAttr("fonemi_material.transparency", transparency_condition + '.colorIfFalse')
            cmds.connectAttr("Mouth_ctrl_material.transparency", transparency_condition + '.colorIfTrue')

            # Connessione del nodo condition al materiale finale
            cmds.connectAttr(transparency_condition + '.outColor', final_material + '.transparency')

    # Esegui la funzione sui controlli selezionati
    create_phoneme_switch_for_controls(selected_controls)

# Crea l'interfaccia utente
create_ui()
