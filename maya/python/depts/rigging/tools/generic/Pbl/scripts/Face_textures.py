import maya.cmds as cmds
import os
import re

def create_ui():
    if cmds.window("textureSwitchUI", exists=True):
        cmds.deleteUI("textureSwitchUI")

    window = cmds.window("textureSwitchUI", title="PBL Texture Switch Tool", widthHeight=(400, 150))
    cmds.columnLayout(adjustableColumn=True)

    cmds.text(label="Path:")
    path_field = cmds.textField(text=r"\\speed.nas\dev24\02_production\01_content\mc_dev24\sourceimages\character\g\gil\df\anim")

    cmds.text(label="Filter:")
    filter_field = cmds.textField(text="_Gil_")

    cmds.text(label="Texture Prefix:")
    prefix_field = cmds.textField(text="PBL_gil_")

    def execute():
        path = cmds.textField(path_field, query=True, text=True)
        filter_pattern = cmds.textField(filter_field, query=True, text=True)
        texture_prefix = cmds.textField(prefix_field, query=True, text=True)
        create_texture_switch_from_ui(path, filter_pattern, texture_prefix)

    cmds.button(label="Create Texture Switch", command=lambda x: execute())

    cmds.showWindow(window)
    cmds.window(window, edit=True, widthHeight=(400, 150))  # Fissa l'altezza a 150

def create_texture_switch_from_ui(path, filter_pattern, texture_prefix):
    # Ottieni i nomi delle cartelle nel percorso
    folders = [d for d in os.listdir(path) if os.path.isdir(os.path.join(path, d))]

    # Filtra e modifica i nomi delle cartelle
    enum_options = []
    for folder in folders:
        match = re.match(rf"^\d+{filter_pattern}(.+)$", folder)
        if match:
            enum_options.append(match.group(1))

    # Converti le opzioni in una stringa separata da due punti, come richiesto da Maya
    enum_string = ":".join(enum_options)

    # Ottieni i controlli selezionati
    selected_controls = cmds.ls(selection=True)

    # Aggiungi l'attributo enumerativo ai controlli selezionati
    for control in selected_controls:
        if not cmds.attributeQuery('multiswitch', node=control, exists=True):
            cmds.addAttr(control, longName='multiswitch', niceName='Expression', attributeType='enum', enumName=enum_string)
            cmds.setAttr('{}.multiswitch'.format(control), edit=True, keyable=True)

    print(f"Attributo 'multiswitch' aggiunto ai controlli selezionati con le opzioni: {enum_options}")

    # Funzione per creare nodi Condition, Layered Texture e Materiale Lambert
    def create_texture_switch_for_controls(controls):
        for control in controls:
            # Leggi l'attributo "multiswitch"
            expressions_attr = control + '.multiswitch'
            if not cmds.objExists(expressions_attr):
                cmds.warning(f"{expressions_attr} does not exist. Skipping control {control}.")
                continue

            # Ottieni le scelte enum dell'attributo "multiswitch"
            enum_choices = cmds.attributeQuery('multiswitch', node=control, listEnum=True)[0].split(':')
            
            condition_nodes = []
            textures = []
            
            for i, choice in enumerate(enum_choices):
                # Crea un nodo Condition per ogni scelta enum, con il nome del controllo come suffisso
                condition_node = cmds.createNode('condition', name=f"{choice}_{control}_condition")
                condition_nodes.append(condition_node)
                
                # Configura il nodo Condition
                cmds.connectAttr(expressions_attr, condition_node + '.firstTerm')
                cmds.setAttr(condition_node + '.secondTerm', i)
                cmds.setAttr(condition_node + '.colorIfTrueR', 1)
                cmds.setAttr(condition_node + '.colorIfTrueG', 1)
                cmds.setAttr(condition_node + '.colorIfTrueB', 1)
                cmds.setAttr(condition_node + '.colorIfFalseR', 0)
                cmds.setAttr(condition_node + '.colorIfFalseG', 0)
                cmds.setAttr(condition_node + '.colorIfFalseB', 0)
                
                # Cerca e importa la texture corrispondente
                root_name = re.sub(r'_ctrl$', '', control)
                texture_path = None
                for folder in folders:
                    potential_texture_path = os.path.join(path, folder, f"{texture_prefix}{root_name}_{choice}_C.png")
                    if os.path.isfile(potential_texture_path):
                        texture_path = potential_texture_path
                        break
                
                if texture_path:
                    file_node = cmds.shadingNode('file', asTexture=True, name=f"{choice}_{control}_file")
                    cmds.setAttr(f"{file_node}.fileTextureName", texture_path, type="string")
                    textures.append(file_node)

            # Crea un Layered Texture "_C"
            layered_texture_c = cmds.shadingNode('layeredTexture', asTexture=True, name=f"{control}_C")
            
            for i, (condition_node, texture) in enumerate(zip(condition_nodes, textures)):
                # Aggiungi una nuova istanza al Layered Texture con blend mode "over" (1)
                cmds.setAttr(layered_texture_c + f".inputs[{i}].blendMode", 1)  # Over modalità di miscelazione
                # Connette l'output del nodo condition all'alpha del Layered Texture
                cmds.connectAttr(condition_node + '.outColorR', layered_texture_c + f".inputs[{i}].alpha")
                # Connette l'output color della texture all'input color dell'istanza del Layered Texture
                cmds.connectAttr(texture + '.outColor', layered_texture_c + f".inputs[{i}].color")

            # Crea un nuovo materiale Lambert
            lambert_material = cmds.shadingNode('lambert', asShader=True, name=f"{control}_material")
            
            # Connette l'output color del Layered Texture "_C" all'input color del Lambert
            cmds.connectAttr(layered_texture_c + '.outColor', lambert_material + '.color')

            # Crea un secondo Layered Texture "_M"
            layered_texture_m = cmds.shadingNode('layeredTexture', asTexture=True, name=f"{control}_M")
            
            for i, (condition_node, texture) in enumerate(zip(condition_nodes, textures)):
                # Aggiungi una nuova istanza al Layered Texture con blend mode "over" (1)
                cmds.setAttr(layered_texture_m + f".inputs[{i}].blendMode", 1)  # Over modalità di miscelazione
                # Connette l'output del nodo condition all'alpha del Layered Texture "_M"
                cmds.connectAttr(condition_node + '.outColorR', layered_texture_m + f".inputs[{i}].alpha")
                # Connette l'output alpha della texture all'input color R,G,B dell'istanza del Layered Texture "_M"
                cmds.connectAttr(texture + '.outAlpha', layered_texture_m + f".inputs[{i}].colorR")
                cmds.connectAttr(texture + '.outAlpha', layered_texture_m + f".inputs[{i}].colorG")
                cmds.connectAttr(texture + '.outAlpha', layered_texture_m + f".inputs[{i}].colorB")

            # Crea un nodo Reverse
            reverse_node = cmds.shadingNode('reverse', asUtility=True, name=f"{control}_reverse")
            
            # Connette l'output color del Layered Texture "_M" all'input del nodo Reverse
            cmds.connectAttr(layered_texture_m + '.outColor', reverse_node + '.input')

            # Connette l'output del nodo Reverse alla transparency del materiale Lambert
            cmds.connectAttr(reverse_node + '.output', lambert_material + '.transparency')

    # Esegui la funzione sui controlli selezionati
    create_texture_switch_for_controls(selected_controls)

# Crea l'interfaccia utente
create_ui()
