#-*- coding: utf-8 -*-

import maya.cmds as cmds
from maya import OpenMayaUI as omui
import os, sys
from imp import reload

# -----------------------------------------------------------------
# Path
# -----------------------------------------------------------------
localpipe = os.getenv("LOCALPY")
myPath = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "Pbl")
script_path = os.path.join(myPath, "scripts")
scene_path = os.path.join(myPath, "scenes")
image_path = os.path.join(myPath, "icons")


# -----------------------------------------------------------------
# PySide
# -----------------------------------------------------------------
try:
    from shiboken2 import wrapInstance
except ImportError:
    from shiboken import wrapInstance

try:
    from PySide2.QtGui import QIcon
    from PySide2.QtWidgets import QWidget
except ImportError:
    from PySide.QtGui import QIcon, QWidget


# -----------------------------------------------------------------
# Funzioni da associare ai singoli pulsanti della UI
# -----------------------------------------------------------------
# FACE BASE
def face_textures_button_push(*args):
    scriptPath = os.path.join(script_path, "Face_textures.py")
    print("Attempting to run script: {}".format(scriptPath))
    try:
        with open(scriptPath, 'r') as file:
            scriptContent = file.read()
            exec(scriptContent, globals())  # Usa globals() per mantenere il contesto globale
        print("Script executed successfully.")
    except Exception as e:
        print("Error executing script: {}".format(str(e)))

def phonemes_textures_button_push(*args):
    scriptPath = os.path.join(script_path, "Phonemes_textures.py")
    print("Attempting to run script: {}".format(scriptPath))

    try:
        with open(scriptPath, 'r') as file:
            scriptContent = file.read()
            exec(scriptContent, globals())  # Usa globals() per mantenere il contesto globale
        print("Script executed successfully.")
    except Exception as e:
        print("Error executing script: {}".format(str(e)))


# UTILITIES
def import_export_ctrl_shapes_button_push(*args):
    scriptPath = os.path.join(script_path, "import_export_ctrl_shapes.py")
    print("Attempting to run script: {}".format(scriptPath))

    try:
        with open(scriptPath, 'r') as file:
            scriptContent = file.read()
            exec(scriptContent, globals())  # Usa globals() per mantenere il contesto globale
        print("Script executed successfully.")
    except Exception as e:
        print("Error executing script: {}".format(str(e)))

def mirror_skinning_driver_curves_button_push(*args):
    scriptPath = os.path.join(script_path, "mirror_skinning_driver_curves.py")
    print("Attempting to run script: {}".format(scriptPath))

    try:
        with open(scriptPath, 'r') as file:
            scriptContent = file.read()
            exec(scriptContent, globals())  # Usa globals() per mantenere il contesto globale
        print("Script executed successfully.")
    except Exception as e:
        print("Error executing script: {}".format(str(e)))

def joint_visibility_button_push(*args):
    scriptPath = os.path.join(script_path, "joint_visibility.py")
    print("Attempting to run script: {}".format(scriptPath))

    try:
        with open(scriptPath, 'r') as file:
            scriptContent = file.read()
            exec(scriptContent, globals())  # Usa globals() per mantenere il contesto globale
        print("Script executed successfully.")
    except Exception as e:
        print("Error executing script: {}".format(str(e)))

def joint_visibility_on_button_push(*args):
    scriptPath = os.path.join(script_path, "joint_visibility_on.py")
    print("Attempting to run script: {}".format(scriptPath))

    try:
        with open(scriptPath, 'r') as file:
            scriptContent = file.read()
            exec(scriptContent, globals())  # Usa globals() per mantenere il contesto globale
        print("Script executed successfully.")
    except Exception as e:
        print("Error executing script: {}".format(str(e)))
        
def joint_segmentscale_compensate_off(*args):
    scriptPath = os.path.join(script_path, "DisableSegmentScaleCompensate.py")
    print("Attempting to run script: {}".format(scriptPath))

    try:
        with open(scriptPath, 'r') as file:
            scriptContent = file.read()
            exec(scriptContent, globals())  # Usa globals() per mantenere il contesto globale
        print("Script executed successfully.")
    except Exception as e:
        print("Error executing script: {}".format(str(e)))
    

def link_wiki(*args):
    import webbrowser
    pathToHelp="https://sites.google.com/rbw-cgi.it/rbw-rigging-team/home"
    webbrowser.open(pathToHelp) 


# -----------------------------------------------------------------
# UI
# -----------------------------------------------------------------
def main():
    winWidth = 280   
    """Funzione che lancia la UI dei moduli del facciale."""
    if cmds.window("PblUI", q=True, exists=True):
         cmds.deleteUI("pbl_icon")
    ww = cmds.window("PblUI" ,t="Rainbow CGI - PBL Face Rig",  mxb=False, width=250, height=50)
    
    # Get a pointer and convert it to Qt Widget object
    qw = omui.MQtUtil.findWindow(ww)
    widget = wrapInstance(int(qw), QWidget)

    # Create a QIcon object
    iconpath = os.path.join(image_path, "RainbowCGI_icona.ico")

    # Assign the icon
    icon = QIcon(iconpath)
    widget.setWindowIcon(icon)

    # Main Layout
    col = cmds.columnLayout(adjustableColumn=True, bgc=(0.225, 0.225, 0.225))
    
    # Carica l'immagine del Face Rig
    img = os.path.join(image_path, "pbl_icon.png")
    cmds.iconTextButton(style="iconOnly", image1=img, w=80, h=250, useAlpha=True, p=col)
    cmds.text("PBL FACE RIG", h=20, font='boldLabelFont')
    cmds.setParent("..")

    # Face Base
    cmds.frameLayout(label="Face Base", collapsable=False, collapse=False, p=col, bgc=[0.5, 0.4, 0.8])
    cmds.columnLayout(adj=True, co=("both", 6), rowSpacing=7, columnWidth=250)
    cmds.text(l="", h=1)   
    cmds.button(l="FACE TEXTURE", command=face_textures_button_push, h=30, bgc=[0.3, 0.3, 0.3], w=140)
    cmds.button(l="PHONEMES", command=phonemes_textures_button_push, h=30, bgc=[0.3, 0.3, 0.3], w=140)
    cmds.text(l="", h=1)     
    cmds.setParent("..")

    # Utilities
    cmds.frameLayout(label="Utilities", collapsable=True, collapse=True, p=col, bgc=[0.5, 0.5, 0.6])
    cmds.columnLayout(adj=True, co=("both", 6), rowSpacing=7, columnWidth=250)
    cmds.text(l="", h=1)     
    cmds.button(w=winWidth, l="| Import/Export Ctrl Shapes |", command = import_export_ctrl_shapes_button_push, h=30, bgc=[0.8, 0.8, 0.6])
    cmds.button(w=winWidth, l="| Mirrora influenze curve driver |", command = mirror_skinning_driver_curves_button_push, h=30, bgc=[0.8, 0.8, 0.6])
    cmds.button(w=winWidth, l="| Joint Visibility off |", command = joint_visibility_button_push, h=30, bgc=[0.8, 0.8, 0.6])
    cmds.button(w=winWidth, l="| Joint Visibility on |", command = joint_visibility_on_button_push, h=30, bgc=[0.8, 0.8, 0.6])
    cmds.button(w=winWidth, l="| Segment Scale Compensate Off |", command = joint_segmentscale_compensate_off, h=30, bgc=[0.8, 0.8, 0.6])
    cmds.text(l="", h=1)     
    cmds.setParent("..")

    # WIKI
    cmds.frameLayout(label="WIKI", labelVisible=False, p=col, bgc=(0.4, 0.4, 0.4))
    col_wiki = cmds.columnLayout(adj=True, co=("both", 6), rowSpacing=7, columnWidth=250)
    cmds.text(l="", h=1)     
    cmds.button(h=30, l="| WIKI |", p=col_wiki, c=link_wiki, bgc=(0.2, 0.2, 0.2), annotation="Hai qualche dubbio? Vai alla Wiki di rigging!")
    cmds.text(l="", h=1)     
    cmds.setParent("..")

    cmds.showWindow()
    
