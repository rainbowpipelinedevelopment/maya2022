# -*- coding: utf-8 -*-

import maya.cmds as cmds
from maya import OpenMayaUI as omui
import os
from imp import reload

# -----------------------------------------------------------------
# Path
# -----------------------------------------------------------------
localpipe = os.getenv("LOCALPY")
myPath = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "Finalize_Rig")
image_path = os.path.join(myPath, "icons")


# -----------------------------------------------------------------
# PySide
# -----------------------------------------------------------------
try:
    from shiboken2 import wrapInstance
except ImportError:
    from shiboken import wrapInstance

try:
    from PySide2.QtGui import QIcon
    from PySide2.QtWidgets import QWidget
except ImportError:
    from PySide.QtGui import QIcon, QWidget


# -----------------------------------------------------------------
# Funzioni lanciate dalla UI
# -----------------------------------------------------------------
def delete_tweaks_button_push(*args):
    import depts.rigging.tools.generic.Finalize_Rig.scripts.delete_tweaks as delete_tweaks
    reload(delete_tweaks)
    delete_tweaks.delete_tweak_nodes()


def delete_bindPoses_button_push(*args):
    import depts.rigging.tools.generic.Finalize_Rig.scripts.delete_bindPoses as delete_bindPoses
    reload(delete_bindPoses)
    delete_bindPoses.delAllBind()


def delete_arnold_button_push(*args):
    import depts.rigging.tools.generic.Finalize_Rig.scripts.delete_arnold as delete_arnold
    reload(delete_arnold)
    delete_arnold.delete_arnold_nodes()


def delete_Vray_button_push(*args):
    import depts.rigging.tools.generic.Finalize_Rig.scripts.delete_Vray as delete_Vray
    reload(delete_Vray)
    delete_Vray.delete_vray_nodes()

def delete_ngskin_button_push(*args):
    import depts.rigging.tools.generic.Finalize_Rig.scripts.delete_ngskin as delete_ngskin
    reload(delete_ngskin)
    delete_ngskin.delete_ngSkinTools_nodes()
   

# UTILITIES
def hideFromChannelBox_button_push(*args):
    import depts.rigging.tools.generic.Finalize_Rig.scripts.hide_From_ChannelBox as hide_From_ChannelBox
    reload(hide_From_ChannelBox)
    hide_From_ChannelBox.hideFromChannelBox()

def lock_attributes_groups_button_push(*args):
    import depts.rigging.tools.generic.Finalize_Rig.scripts.lock_attributes_groups as lock_attributes_groups
    reload(lock_attributes_groups)
    lock_attributes_groups.lockAllGroups()

# WIKI
def link_wiki_button_push(*args):
    import webbrowser
    pathToHelp="https://sites.google.com/rbw-cgi.it/rbw-rigging-team/home"
    webbrowser.open(pathToHelp) 

# -----------------------------------------------------------------
# UI
# -----------------------------------------------------------------
def main():
    winWidth = 280
    bg_color_01 = (0.35, 0.35, 0.35)
    bg_color_02 = (0.6, 0.5, 0.2)
    bg_color_03 = (0.4, 0.3, 0.2)
    
    main_window = "RBW_Finalize_Rig"
    main_title = "RBW Finalize Rig"

    if cmds.window(main_window, exists = 1):
        cmds.deleteUI(main_window, window = 1)

    ww = cmds.window(main_window, title=main_title, mxb=False, sizeable=True, resizeToFitChildren=True, width=250, height=50)

    # Get a pointer and convert it to Qt Widget object
    qw = omui.MQtUtil.findWindow(ww)
    widget = wrapInstance(int(qw), QWidget)

    # Create a QIcon object
    iconpath = os.path.join(image_path, "RainbowCGI_icona.ico")

    # Assign the icon
    icon = QIcon(iconpath)
    widget.setWindowIcon(icon)

    # Main layout
    col = cmds.columnLayout(co=("both", 5), adjustableColumn=True, bgc=(0.225, 0.225, 0.225))
    
    # Carica l'immagine degli eyesockets
    img = os.path.join(image_path, "finalize.png")
    cmds.text(h = 10, l = "")
    cmds.iconTextButton(style="iconOnly", image1=img, p=col, w=100)
    cmds.text(h = 10, l = "")
    cmds.setParent("..")

    # FIELDS
    cmds.frameLayout( label="Finalize", marginHeight=10, p=col, bgc=bg_color_02)
    cmds.columnLayout(adj=True, co=("both", 6), rowSpacing=5, columnWidth=250)
    cmds.button(w=winWidth, h=30, l="| Delete Tweaks |", c=delete_tweaks_button_push, bgc=bg_color_01, annotation="Crea il sistema per il Breast e Breath Rig")
    cmds.button(w=winWidth, h=30, l="| Delete Bind Poses |", c=delete_bindPoses_button_push, bgc=bg_color_01, annotation="Crea una ribbon usando una nurbs e gli uvpin")
    cmds.button(w=winWidth, h=30, l="| Delete Arnold |", c=delete_arnold_button_push, bgc=bg_color_01, annotation="Tool per ritornare alle guide di 'Hive'")
    cmds.button(w=winWidth, h=30, l="| Delete Vray |", c=delete_Vray_button_push, bgc=bg_color_01, annotation="Automatiza l'attacco del rig della faccia al rig del body")
    cmds.button(w=winWidth, h=30, l="| Delete nginSkinTool |", c=delete_ngskin_button_push, bgc=bg_color_01, annotation="Automatiza i fix del 'follow', 'aim' e 'scale' degli occhi")
    cmds.setParent("..")
    # cmds.setParent("..")

    # Delivery
    cmds.frameLayout(label="Delivery", collapsable=True, collapse=True, p=col, bgc=bg_color_03)
    cmds.columnLayout(adj=True, co=("both", 6), rowSpacing=7, columnWidth=250)
    cmds.text(l="", h=1)     
    cmds.button(w=winWidth, h=25, l="| Hide Inputs |", c=hideFromChannelBox_button_push, bgc=(0.7, 0.7, 0.53), annotation="Nasconde tutti gli input dal ChannelBox")
    cmds.button(w=winWidth, h=25, l="| Lock Attributes |", c=lock_attributes_groups_button_push, bgc=(0.7, 0.7, 0.53), annotation="Esegue Lock and Hide degli Attributi dei gruppi")
    cmds.text(l="", h=1)     
    cmds.setParent("..")

    # WIKI
    cmds.frameLayout(label="WIKI", labelVisible=False, p=col, bgc=(0.4, 0.4, 0.4))
    col_wiki = cmds.columnLayout(adj=True, co=("both", 6), rowSpacing=7, columnWidth=250)
    cmds.text(l="", h=1)     
    cmds.button(w=winWidth, h=30, l="| WIKI |", p=col_wiki, c=link_wiki_button_push, bgc=(0.2, 0.2, 0.2), annotation="Hai qualche dubbio? Vai alla Wiki di rigging!")
    cmds.text(l="", h=1)     
    cmds.setParent("..")
    
    cmds.showWindow(main_window)