import maya.OpenMayaUI as omui
import maya.cmds as mc
import maya.api.OpenMaya as om2

def hideFromChannelBox():
    mc.undoInfo(openChunk=True)
    allItem = mc.ls()

    for item in allItem:
        try:

            mc.setAttr("{}.isHistoricallyInteresting".format(item), 0)

        except:
            pass

    om2.MGlobal.displayInfo("--- Every Input node was hidden from the channel box ---")

    mc.undoInfo(closeChunk=True)

