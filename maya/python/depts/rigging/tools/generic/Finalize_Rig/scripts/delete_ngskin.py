import maya.cmds as cmds

def delete_ngSkinTools_nodes():
    # Ottieni tutti i nodi nella scena
    all_nodes = cmds.ls()

    # Trova e elimina i nodi di ngSkinTools
    ngSkinTools_nodes = [node for node in all_nodes if "ngSkinToolsData" in node]
    if ngSkinTools_nodes:
        cmds.delete(ngSkinTools_nodes)
        print("Eliminati tutti i nodi di ngSkinTools presenti nella scena.")
    else:
        print("Nessun nodo di ngSkinTools trovato nella scena.")