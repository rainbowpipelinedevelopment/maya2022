from maya import cmds

def delAllBind(*args):
    sel = cmds.ls("bindPose*")
    if not sel:
        cmds.warning("Nessun nodo di bindPose trovato.")
    else:
        cmds.delete(sel)
        print("Tutti i nodi di bindPose sono stati eliminati correttamente.")