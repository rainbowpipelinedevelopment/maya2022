import maya.cmds as cmds

def delete_arnold_nodes():
    # Ottieni tutti i nodi nella scena
    all_nodes = cmds.ls()

    # Trova e elimina tutti i nodi di Arnold
    arnold_nodes = [node for node in all_nodes if cmds.nodeType(node).startswith("ai") or cmds.nodeType(node).startswith("mtoa")]
    if arnold_nodes:
        cmds.delete(arnold_nodes)
        print("Eliminati tutti i nodi di Arnold presenti nella scena.")
    else:
        print("Nessun nodo di Arnold trovato nella scena.")