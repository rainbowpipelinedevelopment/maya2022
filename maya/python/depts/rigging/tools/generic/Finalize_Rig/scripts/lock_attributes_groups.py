import maya.cmds as mc

def lockAllGroups():
    mc.undoInfo(openChunk=True)
    
    # Ottieni una lista di tutti gli oggetti nella scena
    all_objects = mc.ls()

    # Itera su ogni oggetto nella scena
    for item in all_objects:
        # Controlla se il nome dell'oggetto soddisfa i criteri specificati
        if (("CHAR" in item) or ("grp" in item) or ("Grp" in item) or ("GRP" in item)):
            objectType = mc.objectType(item)
            
            # Controlla se l'oggetto è un trasform
            if objectType == 'transform':
                attributes = mc.listAttr(item, keyable=True)
                
                # Se l'oggetto ha attributi keyable, bloccali
                if attributes:
                    for attr in attributes:
                        mc.setAttr("{}.{}".format(item, attr), lock=True, keyable=False)

    mc.undoInfo(closeChunk=True)

# Chiama la funzione per bloccare tutti i gruppi che soddisfano i criteri specificati
lockAllGroups()
