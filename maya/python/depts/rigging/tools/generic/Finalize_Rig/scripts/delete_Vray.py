import maya.cmds as cmds

def delete_vray_nodes():
    # Lista dei tipi di nodo di V-Ray
    vray_node_types = ['VRaySettingsNode', 'VRayRenderElement', 'VRayObjectProperties', 'VRayMeshMaterial', 'VRayLightRectShape', 'VRayLightIESShape', 'VRaySunShape', 'VRayDisplacement']

    # Trova e elimina tutti i nodi di V-Ray
    for node_type in vray_node_types:
        # Ottieni tutti i nodi del tipo specificato
        vray_nodes = cmds.ls(type=node_type)
        if vray_nodes:
            # Elimina tutti i nodi di V-Ray trovati
            cmds.delete(vray_nodes)
        else:
            # Se il tipo di nodo di V-Ray non è presente nella scena, stampa un messaggio
            print("Nessun nodo di tipo", node_type, "trovato in scena.")