import maya.cmds as cmds

def delete_tweak_nodes():
    # Lista dei tipi di nodo di tweak
    tweak_node_types = ['tweak', 'tweakUV']

    # Trova e elimina tutti i nodi di tweak
    for node_type in tweak_node_types:
        # Ottieni tutti i nodi del tipo specificato
        tweak_nodes = cmds.ls(type=node_type)
        if tweak_nodes:
            # Elimina tutti i nodi di tweak trovati
            cmds.delete(tweak_nodes)
        else:
            # Se il tipo di nodo di tweak non è presente nella scena, stampa un messaggio
            print("Nessun nodo di tipo", node_type, "trovato in scena.")