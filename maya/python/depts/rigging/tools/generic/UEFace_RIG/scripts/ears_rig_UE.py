import maya.cmds as cmds

def main_rig_process():
    def create_and_parent_joints(locator_names):
        joint_names = {}
        for loc in locator_names:
            position = cmds.xform(loc, query=True, worldSpace=True, translation=True)
            joint_name = loc.replace("LOC_", "") + "_jnt"
            cmds.select(clear=True)
            joint_names[loc] = cmds.joint(name=joint_name, position=position)
            temp_constraint = cmds.orientConstraint(loc, joint_name)
            cmds.delete(temp_constraint)
        
        # Assuming locator_names has at least 3 elements
        if len(locator_names) >= 3:
            cmds.parent(joint_names[locator_names[1]], joint_names[locator_names[0]])
            cmds.parent(joint_names[locator_names[2]], joint_names[locator_names[0]])
        cmds.select(clear=True)

    def create_control_and_group(joint_name, control_name, parent):
        cmds.select(clear=True)
        control = cmds.circle(name=control_name, normal=(0, 1, 0), radius=1.5)[0]
        cmds.setAttr("{}.visibility".format(control), lock=True, keyable=False, channelBox=False)

        cmds.delete(cmds.parentConstraint(joint_name, control))
        grp = cmds.group(name=control_name + "_Grp", empty=True)
        cmds.delete(cmds.parentConstraint(control, grp))
        cmds.parent(control, grp)
        if parent:
            cmds.parentConstraint(control, joint_name, maintainOffset=True)
            cmds.scaleConstraint(control, joint_name)
        return control, grp

    def create_controls_for_joints(joint_names):
        control_dict = {}
        for jnt in joint_names:
            control_name = jnt.replace("_jnt", "_Ctrl")
            control, grp = create_control_and_group(jnt, control_name, parent=True)
            control_dict[jnt] = {"control": (control, grp)}
        parent_controls(control_dict)
        return control_dict

    def parent_controls(control_dict):
        ear_joints = ["L_Ear_jnt", "L_EarLow_jnt", "L_EarTop_jnt", "R_Ear_jnt", "R_EarLow_jnt", "R_EarTop_jnt"]
        for ear in ear_joints:
            if ear in control_dict:
                top = ear.replace("Ear", "EarTop")
                low = ear.replace("Ear", "EarLow")
                if top in control_dict and low in control_dict:
                    cmds.parent(control_dict[top]["control"][1], control_dict[ear]["control"][0])
                    cmds.parent(control_dict[low]["control"][1], control_dict[ear]["control"][0])
                    
        for ear in ear_joints:
            cmds.setAttr(f"{ear}.segmentScaleCompensate", 0)
            
    def organize_ears_rig():
        cmds.setAttr("L_Ear_jnt.visibility", 0)
        cmds.setAttr("R_Ear_jnt.visibility", 0)
        ears_controls_grp = cmds.group("L_Ear_Ctrl_Grp", "R_Ear_Ctrl_Grp", name="Ears_Controls_Grp")
        cmds.parent("L_Ear_jnt", "Head_SQSUp_02_jnt")
        cmds.parent("R_Ear_jnt", "Head_SQSUp_02_jnt")
        cmds.parent(ears_controls_grp, "Head_SQSUp_02_jnt")
        if cmds.objExists("LOC_Ears_Grp"):
            cmds.delete("LOC_Ears_Grp")
        # Imparenta 'Ears_RIG_Static_Grp' sotto 'Face_RIG_Grp'
        if cmds.objExists("Ears_RIG_Static_Grp") and cmds.objExists("Face_RIG_Grp"):
            cmds.parent("Ears_RIG_Static_Grp", "Face_RIG_Grp")

    def process_multiple_pairs(control_pairs):
        for source, target in control_pairs:
            replace_and_remove(source, target)
        if cmds.objExists("Ear_Controls_Grp_REF"):
            cmds.delete("Ear_Controls_Grp_REF")

    def process_multiple_pairs(control_pairs):
        for source, target in control_pairs:
            replace_and_remove(source, target)
        if cmds.objExists("Ear_Controls_Grp_REF"):
            cmds.delete("Ear_Controls_Grp_REF")

    def clean_rig():
        # Elimina il gruppo "Jaw_Chin_Joints_Grp" se esiste
        if cmds.objExists("Ear_Controls_Grp_REF"):
            cmds.delete("Ear_Controls_Grp_REF")

    def replace_and_remove(source_ctrl, target_ctrl):
        # Verifica se i due controlli esistono
        if not cmds.objExists(source_ctrl):
            cmds.warning("Il controllo sorgente '{}' non esiste.".format(source_ctrl))
            return
        
        if not cmds.objExists(target_ctrl):
            cmds.warning("Il controllo di destinazione '{}' non esiste.".format(target_ctrl))
            return

        # Ottieni e elimina le shape esistenti nel controllo di destinazione
        target_shapes = cmds.listRelatives(target_ctrl, shapes=True)
        if target_shapes:
            cmds.delete(target_shapes)

        # Ottieni la shape del controllo sorgente
        source_shapes = cmds.listRelatives(source_ctrl, shapes=True)
        if not source_shapes:
            cmds.warning("Il controllo sorgente '{}' non ha shape associate.".format(source_ctrl))
            return

        # Sposta ogni shape del controllo sorgente al controllo di destinazione
        for source_shape in source_shapes:
            cmds.parent(source_shape, target_ctrl, r=True, shape=True)

        # Trova il gruppo padre del controllo sorgente, se esiste
        source_parent = cmds.listRelatives(source_ctrl, parent=True)

        # Elimina il controllo sorgente
        cmds.delete(source_ctrl)

        # Elimina anche il gruppo padre del controllo sorgente, se non ha altri figli
        if source_parent and not cmds.listRelatives(source_parent[0], children=True):
            cmds.delete(source_parent)

        cmds.warning("Shape sostituita con successo da '{}' a '{}'.".format(source_ctrl, target_ctrl))
 
    control_pairs = [
        ("L_EarTop_Ctrl_REF", "L_EarTop_Ctrl"),
        ("R_EarTop_Ctrl_REF", "R_EarTop_Ctrl"),
        ("L_EarLow_Ctrl_REF", "L_EarLow_Ctrl"),
        ("R_EarLow_Ctrl_REF", "R_EarLow_Ctrl"),
        ("L_Ear_Ctrl_REF", "L_Ear_Ctrl"),
        ("R_Ear_Ctrl_REF", "R_Ear_Ctrl"),
        # Altre coppie qui se necessario
    ]

    # Creazione e imparentamento dei Joint
    locator_names_left = ["LOC_L_Ear", "LOC_L_EarLow", "LOC_L_EarTop"]
    locator_names_right = ["LOC_R_Ear", "LOC_R_EarLow", "LOC_R_EarTop"]
    create_and_parent_joints(locator_names_left)
    create_and_parent_joints(locator_names_right)

    # Creazione dei controlli per i Joint
    joint_names_left = ["L_Ear_jnt", "L_EarLow_jnt", "L_EarTop_jnt"]
    joint_names_right = ["R_Ear_jnt", "R_EarLow_jnt", "R_EarTop_jnt"]
    controls_left = create_controls_for_joints(joint_names_left)
    controls_right = create_controls_for_joints(joint_names_right)
    organize_ears_rig()
    process_multiple_pairs(control_pairs)
    clean_rig()
    
    # Funzioni di Utility per creazione set
    def create_joints_set(module, jnt_list):
        # Creates a set containing the joints for skinning
        joints_for_skin = []
        joints_for_skin.extend(jnt_list)
        
        if not cmds.objExists("FACE_MODULES_Joints"):
            module_joints_set = cmds.sets(empty=True, name=("FACE_MODULES_Joints"))
        else:
            module_joints_set = "FACE_MODULES_Joints"

        # Controlla se il set del modulo di rig dei joints esiste, altrimenti crealo
        if not cmds.objExists("{}_Joints".format(module)): # 'HeadEyes' -'HeadMouth' - 'HeadSquash'
            section_joints_set = cmds.sets(empty=True, name=("{}_Joints".format(module)))
            # Parenta il set della parte di rig dentro al set globale
            cmds.sets(section_joints_set, edit=True, forceElement=module_joints_set)
        else:
            section_joints_set = "{}_Joints".format(module)

        for jnt in joints_for_skin:
            cmds.sets(jnt, edit=True, forceElement=section_joints_set)

    def create_controls_set(module, ctrl_list):
        # Crea il set contenente i controlli creati
        if not cmds.objExists("FACE_MODULES_Controls"):
            module_controls_set = cmds.sets(empty=True, name=("FACE_MODULES_Controls"))
        else:
            module_controls_set = "FACE_MODULES_Controls"

        if not cmds.objExists("{}_Controls".format(module)):
            module_ctrl_set = cmds.sets(empty=True, name=("{}_Controls".format(module)))
            cmds.sets(module_ctrl_set, edit=True, forceElement=module_controls_set)
        else:
            module_ctrl_set = "{}_Controls".format(module)

        for ctrl in ctrl_list:
            cmds.sets(ctrl, edit=True, forceElement=module_ctrl_set)

        return module_ctrl_set

    def rename_ctrl_shapes(set_ctrl):
        """Data una selezione di controlli, rinomina le shapes in base al nome del controllo."""
        if not cmds.objExists(set_ctrl):
            print(f"Il set {set_ctrl} non esiste.")
            return

        cmds.select(clear=True)
        cmds.select(set_ctrl)
        sel = cmds.ls(sl=True, long=True)  # Usa `long=True` per ottenere il percorso completo degli oggetti

        if not sel:
            print("Nessun controllo trovato nella selezione.")
            return

        for ctrl in sel:
            shapes = cmds.listRelatives(ctrl, shapes=True, fullPath=True)
            if shapes:
                for shape in shapes:
                    new_name = "{}Shape".format(ctrl.split('|')[-1])  # Usa solo il nome dell'oggetto senza il percorso
                    if cmds.objExists(new_name):
                        print(f"Un oggetto con il nome {new_name} esiste già. Non posso rinominare {shape}.")
                    else:
                        cmds.rename(shape, new_name)
            else:
                print(f"Il controllo {ctrl} non ha forme (shapes) associate.")
        
        print("### Le shapes dei controlli sono state rinominate correttamente. ###")

    # Esegui la creazione dei set
    create_joints_set("Ears", ["L_Ear_jnt", "L_EarTop_jnt", "L_EarLow_jnt", "R_Ear_jnt", "R_EarTop_jnt", "R_EarLow_jnt"])
    controls_set = create_controls_set("Ears", ["L_Ear_Ctrl", "L_EarTop_Ctrl", "L_EarLow_Ctrl", "R_Ear_Ctrl", "R_EarTop_Ctrl", "R_EarLow_Ctrl"])
    rename_ctrl_shapes(controls_set)

# Esecuzione della funzione principale
main_rig_process()
