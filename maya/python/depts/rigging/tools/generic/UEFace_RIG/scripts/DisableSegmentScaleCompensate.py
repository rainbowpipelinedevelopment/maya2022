def enable_segment_scale_compensate_on_all_joints():
    # Trova tutti i joint nella scena
    all_joints = cmds.ls(type='joint')

    # Itera su tutti i joint trovati
    for joint in all_joints:
        # Abilita il Segment Scale Compensate per ogni joint
        cmds.setAttr(joint + ".segmentScaleCompensate", 0)

    print(f"Segment Scale Compensate abilitato per {len(all_joints)} joint.")

# Esegui la funzione
disabled_segment_scale_compensate_on_all_joints()
