import maya.cmds as cmds

def ParentMatrix():
    
    def create_offset_matrix_attributes(obj, target_name, suffix):
        attr_name = "{}_{}_OffsetMatrix_01".format(target_name, suffix)
    
        if cmds.attributeQuery(attr_name, node=obj, exists=True):
            cmds.warning("L'attributo {} esiste già.".format(attr_name))
            return attr_name
    
        cmds.addAttr(obj, longName=attr_name, attributeType='matrix', usedAsColor=False, keyable=False)
    
        return attr_name
        
    def calculate_offset_matrix(source_obj, target_obj, result_obj, attr_name, suffix):
        # Creazione del primo nodo multMatrix
        mult_matrix_node_1 = cmds.createNode('multMatrix', name="{}_{}_MultMatrix1".format(source_obj, suffix))
    
        # Connessione degli input al primo nodo multMatrix
        cmds.connectAttr("{}.worldMatrix[0]".format(target_obj), "{}.matrixIn[0]".format(mult_matrix_node_1))
        cmds.connectAttr("{}.worldInverseMatrix[0]".format(source_obj), "{}.matrixIn[1]".format(mult_matrix_node_1))
        cmds.connectAttr("{}.matrixSum".format(mult_matrix_node_1), "{}.{}".format(result_obj, attr_name))

        
        # Creazione del secondo nodo multMatrix
        mult_matrix_node_2 = cmds.createNode('multMatrix', name="{}_{}_MultMatrix2".format(target_obj, suffix))
    
        # Connessione degli input al secondo nodo multMatrix
        cmds.connectAttr("{}.{}".format(result_obj, attr_name), "{}.matrixIn[0]".format(mult_matrix_node_2))
        cmds.connectAttr("{}.worldMatrix[0]".format(source_obj), "{}.matrixIn[1]".format(mult_matrix_node_2))
        cmds.connectAttr("{}.parentInverseMatrix[0]".format(target_object), "{}.matrixIn[2]".format(mult_matrix_node_2))
        
        cmds.disconnectAttr("{}.matrixSum".format(mult_matrix_node_1), "{}.{}".format(result_object, attr_name))        
        cmds.delete(mult_matrix_node_1)        
        
        decompose_matrix_node = cmds.createNode('decomposeMatrix', name="{}_{}DecomposeMatrix".format(target_obj, suffix))
        cmds.connectAttr("{}.matrixSum".format(mult_matrix_node_2), "{}.inputMatrix".format(decompose_matrix_node))
        
        # Connessione del risultato al translate e rotate del secondo oggetto
        cmds.connectAttr("{}.outputTranslate".format(decompose_matrix_node), "{}.translate".format(target_obj))
        cmds.connectAttr("{}.outputRotate".format(decompose_matrix_node), "{}.rotate".format(target_obj))

    selected_objects = cmds.ls(selection=True, type='transform')
    
    if len(selected_objects) == 3:
        source_object = selected_objects[0]  # L'oggetto che influisce sugli altri
        target_object = selected_objects[1]  # L'oggetto che riceve la trasformazione
        result_object = selected_objects[2]  # L'oggetto su cui si crea l'attributo di offset
        suffix = "Offset"
    
        attr_name = create_offset_matrix_attributes(result_object, target_object, suffix)
    
        calculate_offset_matrix(source_object, target_object, result_object, attr_name, suffix)
    
        print("Attributo di offset matrix creato su {} basato su {}. MultMatrix creato e collegato al secondo oggetto.".format(result_object, source_object))
    else:
        cmds.warning("Seleziona esattamente tre oggetti prima di eseguire lo script.")
    
    return


ParentMatrix()
