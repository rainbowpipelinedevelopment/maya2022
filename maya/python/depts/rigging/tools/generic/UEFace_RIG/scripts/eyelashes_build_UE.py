# -*- coding: utf-8 -*-

from maya import cmds, OpenMaya
import math, os, sys
from functools import partial
from imp import reload
import depts.rigging.tools.generic.Face_RIG.scripts.matrix_module as mtx
reload(mtx)


class AER:
    """Automatically rig eyelashes."""
    
    def __init__(self):
        self.eyeSide = None
        self.eyelashes_module = None
        self.joints_grp = None
        self.controls_grp = None
        self.joints_list = []
        self.ctrls_list = []
        self.master_grp_list = []
        self.head_joint = "Head_SQSUp_02_jnt"  # Joint in cui parentare tutti i joints delle eyelashes
        self.eyelashes_rig_grp = None  # Variabile per tenere traccia del gruppo _Eyelashes_RIG_GRP
        self.ctrls_eyelid_list = ["Eyelid_UpInn_Ctrl", "Eyelid_Up_Ctrl", "Eyelid_UpOut_Ctrl", "Eyelid_LowOut_Ctrl", "Eyelid_Low_Ctrl", "Eyelid_LowInn_Ctrl"]
        # -----------------------------------------------------------------
        # Path
        # -----------------------------------------------------------------
        localpipe = os.getenv("LOCALPY")
        myPath = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "Face_RIG")
        scene_path = os.path.join(myPath, "scenes", "controls_shape.ma")
        self.my_file_path = scene_path


    # -----------------------------------------------------------------
    # Parte di costruzione del modulo eyelashes
    # -----------------------------------------------------------------
    def _check_selection(self):
        """Check iniziale sulla selezione."""
        self.sel = cmds.ls(sl=True)
        
        if not self.sel:
            cmds.error("Seleziona il modulo delle eyelid.")
        elif self.sel[0] not in ("L_Eyelid_RIG_GRP", "R_Eyelid_RIG_GRP"):
            cmds.warning("Seleziona il modulo delle eyelid.")


    def _duplicate_eyelid_module(self, selection):
        """Duplica il modulo delle eyelids selezionato e rinominalo come eyelashes,
        duplicando anche il gruppo Eyelid_CTRL_JNT_GRP associato al lato e rinominando tutto al suo interno."""
        
        if selection:
            # Determina il lato dall'oggetto selezionato
            self.eyeSide = selection[0].split("_")[0]
            
            # Duplica il modulo Eyelid e rinomina come Eyelashes
            name_grp = selection[0].replace("Eyelid", "Eyelashes")
            self.eyelashes_module = cmds.duplicate(selection[0], name=name_grp)[0]
            self.eyelashes_rig_grp = f"{self.eyeSide}_Eyelashes_RIG_GRP"  # Memorizza il gruppo RIG

            # Cerca e duplica il gruppo Eyelid_CTRL_JNT_GRP associato al lato
            eyelid_ctrl_jnt_grp = f"{self.eyeSide}_Eyelid_CTRL_JNT_GRP"
            if cmds.objExists(eyelid_ctrl_jnt_grp):
                # Duplica il gruppo Eyelid_CTRL_JNT_GRP
                duplicated_eyelashes_ctrl_jnt_grp = cmds.duplicate(eyelid_ctrl_jnt_grp)[0]
                
                # Rinomina il gruppo duplicato, rimuovendo l'eventuale suffisso '1' aggiunto automaticamente da Maya
                new_group_name = duplicated_eyelashes_ctrl_jnt_grp.replace("Eyelid", "Eyelashes").rstrip("1")
                duplicated_eyelashes_ctrl_jnt_grp = cmds.rename(duplicated_eyelashes_ctrl_jnt_grp, new_group_name)
                
                # Rinomina tutti gli oggetti all'interno del gruppo duplicato
                all_descendants = cmds.listRelatives(duplicated_eyelashes_ctrl_jnt_grp, allDescendents=True, fullPath=True)
                if all_descendants:
                    for obj in all_descendants:
                        short_name = obj.split("|")[-1]  # Ottieni il nome dell'oggetto senza il percorso completo
                        if "Eyelid" in short_name:
                            new_name = short_name.replace("Eyelid", "Eyelashes")
                            cmds.rename(obj, new_name)
                        
                print(f"Duplicated and renamed group and contents: {duplicated_eyelashes_ctrl_jnt_grp}")
            else:
                print(f"Group {eyelid_ctrl_jnt_grp} not found in the scene.")




    def _delete_unused_groups(self, side, module):
        """Elimina tutti gruppi tranne quello dei ctrl joints."""
        children = cmds.listRelatives(module, fullPath=True, allDescendents=True)
        
        for item in children:
            token = item.split("|")[-1]
            name = token.replace("Eyelid", "Eyelashes")
            cmds.rename(item, name)

        cmds.parent("{}_Eyelashes_CTRL_JNT_GRP".format(side), "{}_Eyelashes_RIG_GRP".format(side))
        self.joints_grp = cmds.rename("{}_Eyelashes_CTRL_JNT_GRP".format(side), "{}_Eyelashes_JNT_GRP".format(side))
        cmds.delete("{}_Eyelashes_Base_RIG_GRP".format(side))


    def _delete_unused_joints(self, side, joints_grp):
        """Pulisci il gruppo dai joint dai corner e dai constraint."""
        children = cmds.listRelatives(joints_grp, allDescendents=True)
        for item in children:
            check_type = cmds.nodeType(item)
            if item in ("{}_Eyelashes_CornerInn_jnt".format(side), "{}_Eyelashes_CornerOut_jnt".format(side)):
                cmds.delete(item)
            if 'Constraint' in check_type:
                cmds.delete(item)


    def _rename_joints(self, side, joints_grp):
        """Rinomina i joint."""
        name_list = ("Eyelashes_Up_01", "Eyelashes_Up_02", "Eyelashes_Up_03", "Eyelashes_Low_03", "Eyelashes_Low_02", "Eyelashes_Low_01")

        children = cmds.listRelatives(joints_grp, allDescendents=True)
        
        for jnt, name in zip(children, name_list):
            new_jnt = cmds.rename(jnt, "{}_{}_jnt".format(side, name))
            self.joints_list.append(new_jnt)


    def _create_controls(self, side, module, joints_list):
        """Crea i controlli e li mette in gerarchia."""
        self.controls_grp = cmds.group(name="{}_Eyelashes_CTRL_GRP".format(side), em=True)
        cmds.parent(self.controls_grp, module)

        ctrl_eyelid_list = (f"{side}_Eyelid_UpInn_Ctrl", f"{side}_Eyelid_Up_Ctrl", f"{side}_Eyelid_UpOut_Ctrl", f"{side}_Eyelid_LowOut_Ctrl", f"{side}_Eyelid_Low_Ctrl", f"{side}_Eyelid_LowInn_Ctrl")

        for jnt, ctrl_eyelid in zip(joints_list, ctrl_eyelid_list):
            ctrl_name = jnt.replace("jnt", "Ctrl")
            ctrl = cmds.circle(r=0.15, n=ctrl_name)[0]
            self.ctrls_list.append(ctrl)
            for shape in cmds.listRelatives(ctrl, s=True, f=True) or []:
                shapeRenamed = cmds.rename(shape, "{}Shape".format(ctrl_name))

            master_grp = cmds.group(name="{}_MASTER_Grp".format(ctrl_name), em=True)
            self.master_grp_list.append(master_grp)
            matrix_grp = cmds.group(name="{}_MATRIX_Grp".format(ctrl_name), em=True)

            cmds.parent(ctrl, matrix_grp)
            cmds.parent(matrix_grp, master_grp)
            cmds.parent(master_grp, self.controls_grp)
            
            # orienta il gruppo master al controllo delle eyelid corrispondente
            cmds.matchTransform(master_grp, ctrl_eyelid)
            
            cmds.setAttr(ctrl + ".visibility", lock=True, keyable=False, channelBox=False)
            cmds.setAttr(ctrl + "Shape.overrideEnabled", 1)
            cmds.setAttr(ctrl + "Shape.overrideColor", 18)


    def _replace_and_remove_shapes(self, path):
        """Sostituisce e rimuove le shapes dei controlli."""
        # lista contenente i controlli da sostituire
        control_pairs = [
            ('L_Eyelashes_Up_01_Ctrl_REF', 'L_Eyelashes_Up_01_Ctrl'),
            ('L_Eyelashes_Up_02_Ctrl_REF', 'L_Eyelashes_Up_02_Ctrl'),
            ('L_Eyelashes_Up_03_Ctrl_REF', 'L_Eyelashes_Up_03_Ctrl'),
            ('L_Eyelashes_Low_03_Ctrl_REF', 'L_Eyelashes_Low_03_Ctrl'),
            ('L_Eyelashes_Low_02_Ctrl_REF', 'L_Eyelashes_Low_02_Ctrl'),
            ('L_Eyelashes_Low_01_Ctrl_REF', 'L_Eyelashes_Low_01_Ctrl'),
            ('R_Eyelashes_Up_01_Ctrl_REF', 'R_Eyelashes_Up_01_Ctrl'),
            ('R_Eyelashes_Up_02_Ctrl_REF', 'R_Eyelashes_Up_02_Ctrl'),
            ('R_Eyelashes_Up_03_Ctrl_REF', 'R_Eyelashes_Up_03_Ctrl'),
            ('R_Eyelashes_Low_03_Ctrl_REF', 'R_Eyelashes_Low_03_Ctrl'),
            ('R_Eyelashes_Low_02_Ctrl_REF', 'R_Eyelashes_Low_02_Ctrl'),
            ('R_Eyelashes_Low_01_Ctrl_REF', 'R_Eyelashes_Low_01_Ctrl')
        ]

        # importa scena con i controlli
        cmds.file(path, i=True, mergeNamespacesOnClash=True, namespace=':', gr=True, gn="GRP_shapes_REF")

        # itera attraverso le coppie di controlli e esegui la sostituzione
        for source_ctrl, target_ctrl in control_pairs:
            try:
                # Ottieni e elimina le shape esistenti nel controllo di destinazione
                target_shapes = cmds.listRelatives(target_ctrl, shapes=True)
                name_target_shape = target_shapes
                if target_shapes:
                    cmds.delete(target_shapes)

                # Ottieni la shape del controllo sorgente
                new_source_ctrl = cmds.duplicate(source_ctrl, n="{}_NEW".format(source_ctrl))
                source_shapes = cmds.listRelatives(source_ctrl, shapes=True)
                if source_shapes:
                    for source_shape in source_shapes:
                        cmds.parent(source_shape, target_ctrl, r=True, shape=True)
                        cmds.rename(source_shape, name_target_shape[0])
            except:
                pass

        # elimina il gruppo dei controlli importati
        cmds.delete("GRP_shapes_REF")


    def _parent_joints_to_controls(self, joints_list, ctrl_list):
        """Crea i constraint (parent e scale) tra i controlli e i joint e parenta i joint al joint principale."""
        for jnt, ctrl in zip(joints_list, ctrl_list):
            # Crea il parentConstraint e il scaleConstraint
            cmds.parentConstraint(ctrl, jnt, maintainOffset=True)
            cmds.scaleConstraint(ctrl, jnt, maintainOffset=True)

            # Parenta i joint al joint principale
            cmds.parent(jnt, self.head_joint)

        # Elimina il gruppo che conteneva precedentemente i joint
        if cmds.objExists(self.joints_grp):
            cmds.delete(self.joints_grp)


    def _parent_lashes_and_eyelids(self, side, master_grp_list, ctrls_eyelid_list):
        """Parenta i controlli eyelashes dentro i controlli eyelids."""
        for grp_master, ctrl_eyelid in zip(master_grp_list, ctrls_eyelid_list):
            cmds.parent(grp_master, "{}_{}".format(side, ctrl_eyelid))


    def _populate_sets(self, type_item, side, obj_list):
        """Crea i set relativi alle eyelashes."""
        if type_item == "joints":
            module = "Joints"
            part_name = "Joints"
            set_name = "jointsForSkin"
        elif type_item == "controls":
            module = "Controls"
            part_name = "controls"
            set_name = "controls"
        else:
            cmds.error("Attibuto errato passato a _populate_sets.")

        # Controlla se il set contenente i controlli esiste, altrimenti crealo
        if not cmds.objExists("FACE_MODULES_{}".format(module)):
            module_set = cmds.sets(empty=True, name=("FACE_MODULES_{}".format(module)))
        else:
            module_set = "FACE_MODULES_{}".format(module)

        # Controlla se il set della parte di rig esiste, altrimenti crealo
        if not cmds.objExists("HeadEyes_{}".format(part_name)):
            rigpart_set = cmds.sets(empty=True, name=("HeadEyes_{}".format(part_name)))
            # Parenta il set della parte di rig dentro al set globale
            cmds.sets(rigpart_set, edit=True, forceElement=module_set)
        else:
            rigpart_set = "HeadEyes_{}".format(part_name)

        # Controlla se il set del modulo esiste, altrimenti crealo
        if not cmds.objExists("{}_Eyelashes_{}".format(side, set_name)):
            set_items = cmds.sets(empty=True, name=("{}_Eyelashes_{}".format(side, set_name)))
            # Parenta il set del modulo dentro al set globale
            cmds.sets(set_items, edit=True, forceElement=rigpart_set)
        else:
            set_items = "{}_Eyelashes_{}".format(side, set_name)

        for obj in obj_list:
            cmds.sets(obj, edit=True, forceElement=set_items)

        return set_items


    def _cleanup_rig(self, eyelashes_module):
        """Pulizia dei gruppi vuoti del modulo eyelashes."""
        children = cmds.listRelatives(eyelashes_module)
        for grp in children:
            token = grp.split("_")
            if token[-2] in ['JNT', 'CTRL']:
                print(token)
                cmds.delete(grp)


    def _rename_ctrl_shapes(self, set_ctrl):
        """Data una selezione di controlli, rinomina le shapes in base al nome del controllo."""
        cmds.select(clear=True)
        cmds.select(set_ctrl)
        sel = cmds.ls(sl=True)
        cmds.select(clear=True)
        
        for ctrl in sel:
            shapes = cmds.listRelatives(ctrl, shapes=True, pa=True)
            for shape in shapes:
                new_name = "{}Shape".format(ctrl)
                cmds.rename(shape, new_name)

        print("### Le shapes dei controlli sono state rinominate correttamente. ###")
    

    # -----------------------------------------------------------------
    # Funzioni lanciate dalla UI
    # -----------------------------------------------------------------
    def buildRig(self, *args):
        """Build eyelashes rig."""
        self._check_selection()
        self._duplicate_eyelid_module(self.sel)
        self._delete_unused_groups(self.eyeSide, self.eyelashes_module)
        self._delete_unused_joints(self.eyeSide, self.joints_grp)
        self._rename_joints(self.eyeSide, self.joints_grp)
        self._create_controls(self.eyeSide, self.eyelashes_module, self.joints_list)
        self._replace_and_remove_shapes(self.my_file_path)
        self._parent_joints_to_controls(self.joints_list, self.ctrls_list)
        self._parent_lashes_and_eyelids(self.eyeSide, self.master_grp_list, self.ctrls_eyelid_list)
        self._populate_sets("joints", self.eyeSide, self.joints_list)
        set_ctrls = self._populate_sets("controls", self.eyeSide, self.ctrls_list)
        self._cleanup_rig(self.eyelashes_module)
        self._rename_ctrl_shapes(set_ctrls)

        # Elimina il gruppo _Eyelashes_RIG_GRP se esiste
        if cmds.objExists(self.eyelashes_rig_grp):
            cmds.delete(self.eyelashes_rig_grp)

        # Clear scene & script variables #
        self.eyelashes_module = None
        self.joints_grp = None
        self.controls_grp = None
        self.joints_list[:] = []
        self.ctrls_list[:] = []
        self.master_grp_list[:] = []

        cmds.select(clear=True)
    
        # End message
        print("### {}_Eyelashes have been successfully rigged. ###".format(self.eyeSide))
        
        self.eyeSide = None
