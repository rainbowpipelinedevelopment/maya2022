# -*- coding: utf-8 -*-

from maya import cmds, OpenMaya
import os, sys


def finalize_eye_pose_UE(*args):
    """
    Sistema da attributo EyeMaster_Ctrl.eye_pose per compensare deformazione se 'default'.
    Prima di lanciarlo vanno creati i moduli Eyelid e Eyesocket, sia L che R.
    """

    # -----------------------------------------------------------------
    # Accendi autolid
    # -----------------------------------------------------------------
    cmds.setAttr("EyeMaster_Ctrl.auto_eyelid", 1)


    # -----------------------------------------------------------------
    # Ciclo for
    # -----------------------------------------------------------------
    values_eye_list = []
    grp_pose_list = []

    
    # check iniziale
    for ctrl in ("L_eye_Ctrl", "R_eye_Ctrl"):
        if cmds.objExists(ctrl):
            pass
        else:
            cmds.error("### {} cannot be found! ###".format(ctrl))


    for side in ("L", "R"):
        # Crea gruppo padre di eye_Ctrl e mettilo in gerarchia
        if cmds.objExists("{}_eye_Ctrl_Grp_POSE".format(side)):
            cmds.error("### {}_eye_Ctrl_Grp_POSE exists ###".format(side))
            return

        if cmds.objExists("{}_eye_Ctrl".format(side)):
            grp_pose = cmds.group(name="{}_eye_Ctrl_Grp_POSE".format(side), empty=True)
            grp_pose_list.append(grp_pose)
            cmds.matchTransform(grp_pose, "{}_eye_Ctrl".format(side))
            cmds.parent(grp_pose, "{}_eye_Ctrl_Grp".format(side))
            cmds.parent("{}_eye_Ctrl".format(side), grp_pose)

            value_eye = cmds.getAttr("{}.translateX".format(grp_pose))
            values_eye_list.append(value_eye)
        else:
            cmds.error("### '{}_eye_Ctrl' cannot be found! ###".format(side))

    
    for side, value_eye, grp_pose in zip(("L", "R"), values_eye_list, grp_pose_list):
        # valore condition
        value_pose_up = cmds.getAttr("{}_Eyelid_Up_Ctrl_MATRIX_Grp.translateX".format(side))
        value_pose_low = cmds.getAttr("{}_Eyelid_Low_Ctrl_MATRIX_Grp.translateX".format(side))
        # print("value_pose_up: {}".format(value_pose_up))
        # print("value_pose_low: {}".format(value_pose_low))

        # Creiamo condition
        cnd_node_eyemaster = cmds.createNode('condition', n="CND_{}_eye_Ctrl_Grp_POSE".format(side))
        
        cnd_node_rig_up = cmds.createNode('condition', n="CND_{}_Eyelid_Up_Ctrl_Compensate_Pose".format(side))
        cnd_node_rig_low = cmds.createNode('condition', n="CND_{}_Eyelid_Low_Ctrl_Compensate_Pose".format(side))


        # -----------------------------------------------------------------
        # POSE EYE CTRL 
        # -----------------------------------------------------------------
        # Setting condition
        cmds.setAttr("{}.secondTerm".format(cnd_node_eyemaster), 1)
        cmds.setAttr("{}.colorIfTrueR".format(cnd_node_eyemaster), value_eye)
        for attr in ("R", "G", "B"):
            cmds.setAttr("{}.colorIfFalse{}".format(cnd_node_eyemaster, attr), 0)

        # Connessioni condition
        cmds.connectAttr("EyeMaster_Ctrl.eye_pose", "{}.firstTerm".format(cnd_node_eyemaster), force=True)
        cmds.connectAttr("{}.outColorR".format(cnd_node_eyemaster), "{}.translateX".format(grp_pose), force=True)


        # -----------------------------------------------------------------
        # COMPENSATE POSE EYE
        # -----------------------------------------------------------------
        # Connessione condition da controllo
        cmds.connectAttr("EyeMaster_Ctrl.eye_pose", "{}.firstTerm".format(cnd_node_rig_up), force=True)
        cmds.connectAttr("EyeMaster_Ctrl.eye_pose", "{}.firstTerm".format(cnd_node_rig_low), force=True)

        # Crea nodo PlusMinusAverage
        pls_node_up = cmds.createNode('plusMinusAverage', n="PLS_{}_Eyelid_Up_Compensate_Pose".format(side))
        pls_node_low = cmds.createNode('plusMinusAverage', n="PLS_{}_Eyelid_Low_Compensate_Pose".format(side))

        # Settiamo valori condition
        cmds.setAttr("{}.secondTerm".format(cnd_node_rig_up), 1)
        cmds.setAttr("{}.colorIfTrueR".format(cnd_node_rig_up), -value_pose_up)
        for attr in ("R", "G", "B"):
            cmds.setAttr("{}.colorIfFalse{}".format(cnd_node_rig_up, attr), 0)

        cmds.setAttr("{}.secondTerm".format(cnd_node_rig_low), 1)
        cmds.setAttr("{}.colorIfTrueR".format(cnd_node_rig_low), -value_pose_low)
        for attr in ("R", "G", "B"):
            cmds.setAttr("{}.colorIfFalse{}".format(cnd_node_rig_low, attr), 0)
        
        # Connessioni plusMinus
        cmds.connectAttr("MLP_{}_Eyelid_Up_AutoEyelid_translate.output".format(side), "{}.input3D[0]".format(pls_node_up), force=True)
        cmds.connectAttr("{}.output3Dx".format(pls_node_up), "{}.translateY".format("{}_Eyelid_Up_Ctrl_MATRIX_Grp".format(side)), force=True)
        cmds.connectAttr("{}.output3Dy".format(pls_node_up), "{}.translateX".format("{}_Eyelid_Up_Ctrl_MATRIX_Grp".format(side)), force=True)
        
        cmds.connectAttr("MLP_{}_Eyelid_Low_AutoEyelid_translate.output".format(side), "{}.input3D[0]".format(pls_node_low), force=True)
        cmds.connectAttr("{}.output3Dx".format(pls_node_low), "{}.translateY".format("{}_Eyelid_Low_Ctrl_MATRIX_Grp".format(side)), force=True)
        cmds.connectAttr("{}.output3Dy".format(pls_node_low), "{}.translateX".format("{}_Eyelid_Low_Ctrl_MATRIX_Grp".format(side)), force=True)

        # Creiamo multiply
        mlp_node_auto_up = cmds.createNode('multiplyDivide', n="MLP_{}_Eyelid_Up_EyePose".format(side))
        mlp_node_auto_low = cmds.createNode('multiplyDivide', n="MLP_{}_Eyelid_Low_EyePose".format(side))

        # Connessioni finali
        cmds.connectAttr("EyeMaster_Ctrl.auto_eyelid", "{}.input2X".format(mlp_node_auto_up), force=True)
        cmds.connectAttr("{}.outColorR".format(cnd_node_rig_up),  "{}.input1X".format(mlp_node_auto_up), force=True)
        cmds.connectAttr("{}.outputX".format(mlp_node_auto_up), "{}.input3D[1].input3Dy".format(pls_node_up), force=True)

        cmds.connectAttr("EyeMaster_Ctrl.auto_eyelid", "{}.input2X".format(mlp_node_auto_low), force=True)
        cmds.connectAttr("{}.outColorR".format(cnd_node_rig_low), "{}.input1X".format(mlp_node_auto_low), force=True)
        cmds.connectAttr("{}.outputX".format(mlp_node_auto_low), "{}.input3D[1].input3Dy".format(pls_node_low), force=True)


    # -----------------------------------------------------------------
    # End message
    # -----------------------------------------------------------------
    print("EyePose have been successfully finalized.")


# # Lancia script
# finalize_eye_pose()