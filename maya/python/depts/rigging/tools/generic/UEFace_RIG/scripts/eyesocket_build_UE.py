# -*- coding: utf-8 -*-

import maya.cmds as cmds
from maya import OpenMaya
from maya import OpenMayaUI as omui
import math, os, sys
from functools import partial
from imp import reload
import depts.rigging.tools.generic.UEFace_RIG.scripts.matrix_module as mtx
reload(mtx)


# -----------------------------------------------------------------
# PySide
# -----------------------------------------------------------------
try:
    from shiboken2 import wrapInstance
except ImportError:
    from shiboken import wrapInstance

try:
    from PySide2.QtGui import QIcon
    from PySide2.QtWidgets import QWidget
except ImportError:
    from PySide.QtGui import QIcon, QWidget


class AER :
    '''Automatically rig eyesockets.'''
    
    def __init__(self):
        self.eyeLoc = None
        self.eyeName = None
        self.upperLidVtx = None
        self.lowerLidVtx = None
        self.grpAllRig = None
        self.grpBaseRig = None
        self.hierarchyMainGrpCTRL = None
        self.hierarchyMainGrpJNT = None
        self.parent_rig = ["L_Eyemover_Ctrl", "R_Eyemover_Ctrl"]
        self.set_sliders = []
        # -----------------------------------------------------------------
        # Path
        # -----------------------------------------------------------------
        localpipe = os.getenv("LOCALPY")
        myPath = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "UEFace_RIG")
        script_path = os.path.join(myPath, "scripts")
        scene_path = os.path.join(myPath, "scenes", "controls_shape.ma")
        image_path = os.path.join(myPath, "icons")

        self.my_file_path = scene_path
        self.image_path = image_path
    
    
    # -----------------------------------------------------------------
    # Prerequisiti per i rig
    # -----------------------------------------------------------------  
    def sideEye(self, side, *args):
        if side == None :
            self.eyeSide = None
            cmds.error("Please select a side.\n")
        else:
            self.side = side
            self.nameRig = "{}_EyeSocket".format(self.side)


    def placeEyeCenter(self, *args):
        '''Place locator in the center of the eyeball.
        
        Called by 'UI' function.
        Call functions: None '''
        
        selection = cmds.filterExpand(sm = 12)
        
        if selection == None :
            self.eyeLoc = None
            cmds.error("Please select the eyeball.\n")
        else :
            eyeball = selection [0]

            
            eyeTemp = cmds.duplicate(eyeball) [0]
            cmds.xform(eyeTemp, cp = 1)
            self.eyeLoc = cmds.spaceLocator(n = (self.nameRig + "_Center_locator")) [0]
            cnstrTemp = cmds.pointConstraint(eyeTemp, self.eyeLoc)
            cmds.delete(cnstrTemp)
            cmds.delete(eyeTemp)
            # lock locator
            cmds.setAttr(self.eyeLoc + ".overrideEnabled", 1)
            cmds.setAttr(self.eyeLoc + ".overrideDisplayType", 2)
            
            cmds.select(cl = 1)

            # Update UI
            cmds.textField(self.txtfLoc, e = 1, tx = self.eyeLoc)
            cmds.button(self.btnPlaceCenter, e = 1, en = 0)
            cmds.button(self.btnUndoPlaceCenter, e = 1, en = 1)
    
    
    def placeEyeCenterUndo(self, *args):
        '''Undo 'placeEyeCenter' function.
        
        Called by 'UI' function.
        Call functions: None '''
        
        try :
            cmds.delete(self.eyeLoc)
        except ValueError :
            cmds.warning("'" + self.eyeLoc + "'" + " doesn't exists.\n")
        finally :
            self.eyeLoc = None
            self.eyeName = None
            cmds.textField(self.txtfLoc, e = 1, tx = "")
            cmds.button(self.btnPlaceCenter, e = 1, en = 1)
            cmds.button(self.btnUndoPlaceCenter, e = 1, en = 0)
    
    
    def upLidVtxSet(self, *args):
        '''List selected vertices as upper lid vertices.
        
        Called by 'UI' function.
        Call functions: None '''
        
        self.upperLidVtx = cmds.filterExpand(sm = 31)
        
        if self.upperLidVtx == None :
            cmds.scrollField(self.scrollfUpLid, e = 1, cl = 1)
            cmds.error("Please select vertices of the upper lid.\n")
        else :
            cmds.scrollField(self.scrollfUpLid, e = 1, cl = 1)
            for vtx in self.upperLidVtx :
                vtxNum = vtx.rpartition(".")[2] # <type 'unicode'>
                cmds.scrollField(self.scrollfUpLid, e = 1, it = (str(vtxNum) + " "))
    
    
    def lowLidVtxSet(self, *args):
        '''List selected vertices as lower lid vertices.
        
        Called by 'UI' function.
        Call functions: None '''
        
        self.lowerLidVtx = cmds.filterExpand(sm = 31)
        
        if self.lowerLidVtx == None :
            cmds.scrollField(self.scrollfLowLid, e = 1, cl = 1)
            cmds.error("Please select vertices of the lower lid.\n")
        else :
            cmds.scrollField(self.scrollfLowLid, e = 1, cl = 1)
            for vtx in self.lowerLidVtx :
                vtxNum = vtx.rpartition(".")[2] # <type 'unicode'>
                cmds.scrollField(self.scrollfLowLid, e = 1, it=(str(vtxNum) + " "))
    

    # -----------------------------------------------------------------
    # Parte di costruzione del modulo eyesockets
    # -----------------------------------------------------------------
    def _ordinate_cv_base_curve(self, curva):
        """Ordina cv base curve per permettere il mirroring delle influenze dei joint."""

        last_ep = cmds.getAttr(f"{curva}.spans")
        pos_ep_start = cmds.xform(f"{curva}.ep[0]", q=True, ws=True, t=True)
        pos_ep_end = cmds.xform(f"{curva}.ep[{last_ep}]", q=True, ws=True, t=True)


        if self.side == "L":
            if pos_ep_start[0] > pos_ep_end[0]:
                base_curve = cmds.reverseCurve(curva, ch=False, rpo=1)
            else:
                base_curve = curva
            
        elif self.side == "R":
            if pos_ep_start[0] < pos_ep_end[0]:
                base_curve = cmds.reverseCurve(curva, ch=False, rpo=1)
            else:
                base_curve = curva

        return base_curve




    def createEyelidsCrv(self, nameRig, upperLidVtx, lowerLidVtx, rigGrp):
        '''Creates nurbsCurve out of each lid vertices.
        
        Called by 'buildRig' function.
        Call functions: None '''
        
        cmds.select(cl = 1)
        
        # Organize rig hierarchy
        self.grpBaseRig = cmds.group(n = (nameRig + "_Base_RIG_GRP"), em = 1)
        self.hierarchyCrvGrp = cmds.group(n = (nameRig + "_curves_GRP"), em = 1)
        self.hierarchyUpCrvGrp = cmds.group(n = (nameRig + "_Up_curves_GRP"), em = 1)
        self.hierarchyLowCrvGrp = cmds.group(n = (nameRig + "_Low_curves_GRP"), em = 1)
        
        cmds.parent(self.hierarchyUpCrvGrp, self.hierarchyCrvGrp)
        cmds.parent(self.hierarchyLowCrvGrp, self.hierarchyCrvGrp)
        cmds.parent(self.hierarchyCrvGrp, self.grpBaseRig)
        
        # Upper eyelid
        cmds.select(upperLidVtx)
        edgeUpLid = cmds.polyListComponentConversion(fv = 1, te = 1, internal = 1)
        cmds.select(edgeUpLid)
        tempCrvUp = cmds.polyToCurve(form = 0, degree = 1, conformToSmoothMeshPreview=0)[0]
        cmds.select(tempCrvUp)
        cmds.delete(ch = 1)
        up_base_crv = self._ordinate_cv_base_curve(tempCrvUp)
        upLidCrvName = nameRig + "_Up_BASE_curve"
        self.upLidCrv = cmds.rename(up_base_crv, upLidCrvName)
        cmds.parent(self.upLidCrv, self.hierarchyUpCrvGrp)
        
        # Lower eyelid
        cmds.select(lowerLidVtx)
        edgeLowLid = cmds.polyListComponentConversion(fv = 1, te = 1, internal = 1)
        cmds.select(edgeLowLid)
        tempCrvLow = cmds.polyToCurve(form = 0, degree = 1, conformToSmoothMeshPreview=0)[0]
        cmds.select(tempCrvLow)
        cmds.delete(ch = 1)
        low_base_crv = self._ordinate_cv_base_curve(tempCrvLow)
        lowLidCrvName = nameRig + "_Low_BASE_curve"
        self.lowLidCrv = cmds.rename(low_base_crv, lowLidCrvName)
        cmds.parent(self.lowLidCrv, self.hierarchyLowCrvGrp)

    
    def _jntOrientZeroed(self, jnt, *args):
        '''Zeroed the Joint Orient of the joint child.
        
        Called by 'vtxToJnt' function.
        Call functions: None '''

        for axis in ['X', 'Y', 'Z']:
            cmds.setAttr("{}.jointOrient{}".format(jnt, axis), 0)


    def cv_base_curve_to_joint(self, eyeCenter, up_base_crv, low_base_crv):
        '''Creates one joint per cv of the base curve and parent it to the center of the eye.
        
        Called by 'buildRig' function.
        Call functions: None '''
        
        cmds.select(cl = 1)
        
        self.upLidJntList = []
        self.lowLidJntList = []
        
        # Organize rig hierarchy
        hierarchySecondGrp = cmds.group(n = (self.nameRig + "_Base_JNT_GRP"), em = 1)
        hierarchyUpGrp = cmds.group(n = (self.nameRig + "_Up_joints_GRP"), em = 1)
        hierarchyLowGrp = cmds.group(n = (self.nameRig + "_Low_joints_GRP"), em = 1)
        
        cmds.parent(hierarchyUpGrp, hierarchySecondGrp)
        cmds.parent(hierarchyLowGrp, hierarchySecondGrp)
        
        self.hierarchyMainGrpJNT = self.nameRig + "_JNT_GRP"
        
        if cmds.objExists("|" + self.hierarchyMainGrpJNT):
            cmds.parent(hierarchySecondGrp, ("|" + self.hierarchyMainGrpJNT))
        else:
            cmds.group(n = self.hierarchyMainGrpJNT, em = 1)
            cmds.parent(hierarchySecondGrp, ("|" + self.hierarchyMainGrpJNT))
        
        cmds.setAttr(self.hierarchyMainGrpJNT + ".visibility", 0)
        
        # Calcola il numero di vertici nella curva
        upper_numCVs = cmds.getAttr(up_base_crv + '.spans') + cmds.getAttr(up_base_crv + '.degree')
        lower_numCVs = cmds.getAttr(low_base_crv + '.spans') + cmds.getAttr(low_base_crv + '.degree')

        # Upper eyelid
        for idx, cv in enumerate(range(upper_numCVs)):
            if idx == 0 or idx == (upper_numCVs - 1):
                pass
            else:
                cmds.select(cl = 1)
                jnt = cmds.joint(rad = 0.01, n = (self.nameRig + "_Up_Skin_%d_jnt" % idx))
                self.upLidJntList.append(jnt)
                position = cmds.xform("{}.cv[{}]".format(up_base_crv, cv), q = 1, ws = 1, t = 1)
                cmds.xform(jnt, ws = 1, t = position)
                centerPosition = cmds.xform(eyeCenter, q = 1, ws = 1, t = 1)
                cmds.select(cl = 1)
                centerJnt = cmds.joint(rad = 0.01, n = (self.nameRig + "_Up_Base_%d_jnt" % idx))
                cmds.xform(centerJnt, ws = 1, t = centerPosition)
                cmds.parent(jnt, centerJnt)
                cmds.joint(centerJnt, e = 1, oj = "xyz", secondaryAxisOrient = "yup", ch = 1, zso = 1)
                cmds.parent(centerJnt, hierarchyUpGrp)
                # reset joint orient
                self._jntOrientZeroed(jnt)
        
        # Lower eyelid
        for idx, cv in enumerate(range(lower_numCVs)):
            if idx == 0 or idx == (lower_numCVs - 1):
                pass
            else:
                cmds.select(cl = 1)
                jnt = cmds.joint(rad = 0.01, n = (self.nameRig + "_Low_Skin_%d_jnt" % idx))
                self.lowLidJntList.append(jnt)
                position = cmds.xform("{}.cv[{}]".format(low_base_crv, cv), q = 1, ws = 1, t = 1)
                cmds.xform(jnt, ws = 1, t = position)
                centerPosition = cmds.xform(eyeCenter, q = 1, ws = 1, t = 1)
                cmds.select(cl = 1)
                centerJnt = cmds.joint(rad = 0.01, n = (self.nameRig + "_Low_Base_%d_jnt" % idx))
                cmds.xform(centerJnt, ws = 1, t = centerPosition)
                cmds.parent(jnt, centerJnt)
                cmds.joint(centerJnt, e = 1, oj = "xyz", secondaryAxisOrient = "yup", ch = 1, zso = 1)
                cmds.parent(centerJnt, hierarchyLowGrp)
                # reset joint orient
                self._jntOrientZeroed(jnt) 
        
        # Creates a set containing the joints for skinning
        jntsForSkin = []
        jntsForSkin.extend(self.upLidJntList)
        jntsForSkin.extend(self.lowLidJntList)
        
        if not cmds.objExists("FACE_MODULES_Joints"):
            module_joints_set = cmds.sets(empty=True, name=("FACE_MODULES_Joints"))
        else:
            module_joints_set = "FACE_MODULES_Joints"

        # Controlla se il set della parte di rig dei joints esiste, altrimenti crealo
        if not cmds.objExists("HeadEyes_Joints"):
            rigpart_joints_set = cmds.sets(empty=True, name=("HeadEyes_Joints"))
            # Parenta il set della parte di rig dentro al set globale
            cmds.sets(rigpart_joints_set, edit=True, forceElement=module_joints_set)
        else:
            rigpart_joints_set = "HeadEyes_Joints"

        # Controlla se il set dei joints del modulo esiste, altrimenti crealo
        if not cmds.objExists("{}_jointsForSkin".format(self.nameRig)):
            setSkinJnts = cmds.sets(empty=True, name=("{}_jointsForSkin".format(self.nameRig)))
            # Parenta il set del modulo dentro al set globale
            cmds.sets(setSkinJnts, edit=True, forceElement=rigpart_joints_set)
        else:
            setSkinJnts = "{}_jointsForSkin".format(self.nameRig)

        for jnt in jntsForSkin:
            cmds.sets(jnt, edit=True, forceElement=setSkinJnts)
    
    
    def place_rig_locators(self, nameRig, upLidJntList, lowLidJntList):
        '''Creates one locator per eyelid vertex and constrain each joint to it (aim).
        
        Called by 'buildRig' function.
        Call functions: None '''
        
        self.upLidLocList = []
        self.lowLidLocList = []
        
        # Organize rig hierarchy
        hierarchyThirdGrp = cmds.group(n = (nameRig + "_locator_GRP"), em = 1)
        hierarchyUpGrp = cmds.group(n = (nameRig + "_Up_locator_GRP"), em = 1)
        hierarchyLowGrp = cmds.group(n = (nameRig + "_Low_locator_GRP"), em = 1)
        
        cmds.parent(hierarchyUpGrp, hierarchyThirdGrp)
        cmds.parent(hierarchyLowGrp, hierarchyThirdGrp)
        
        self.grpBaseRig = (nameRig + "_Base_RIG_GRP")
        if cmds.objExists(nameRig + "_Base_RIG_GRP"):
            cmds.setAttr(self.grpBaseRig + ".visibility", 0)
            cmds.parent(hierarchyThirdGrp, self.grpBaseRig)
        else:
            self.grpBaseRig = cmds.group(n = (nameRig + "_Base_RIG_GRP"), em = 1)
            cmds.setAttr(self.grpBaseRig + ".visibility", 0)
            cmds.parent(hierarchyThirdGrp, self.grpBaseRig)
        
        self.grpAllRig = nameRig + "_RIG_GRP"
        if cmds.objExists(nameRig + "_RIG_GRP"):
            cmds.parent(self.grpBaseRig, self.grpAllRig)
            cmds.parent(self.hierarchyMainGrpJNT, self.grpAllRig)
        else:
            self.grpAllRig = cmds.group(n = (nameRig + "_RIG_GRP"), em = 1)
            cmds.parent(self.grpBaseRig, self.grpAllRig)
            cmds.parent(self.hierarchyMainGrpJNT, self.grpAllRig)

        # Imparenta tutto sotto "Face_RIG_Grp"
            if cmds.objExists(self.grpAllRig) and cmds.objExists("Face_RIG_Grp"):
                cmds.parent(self.grpAllRig, "Face_RIG_Grp")
        
        # Upper eyelid
        for upLidJnt in upLidJntList :
            locName = upLidJnt.replace("_Skin", "")
            locName = locName.replace("_jnt", "_loc")
            loc = cmds.spaceLocator(n = locName)[0]
            self.upLidLocList.append(loc)
            cmds.setAttr(loc + "Shape.localScaleX", 0.025)
            cmds.setAttr(loc + "Shape.localScaleY", 0.025)
            cmds.setAttr(loc + "Shape.localScaleZ", 0.025)
            positionLoc = cmds.xform(upLidJnt, q = 1, ws = 1, t = 1)
            cmds.xform(loc, ws = 1, t = positionLoc)
            parentJnt = cmds.listRelatives(upLidJnt, p = 1)[0]
            aim = cmds.aimConstraint(loc, parentJnt, weight = 1, aimVector = (1,0,0), upVector = (0,1,0), worldUpType = "vector", worldUpVector = (0,1,0))
            cmds.parent(loc, hierarchyUpGrp)
            cmds.delete(aim)
            jntGrp = "{}_Up_joints_GRP".format(nameRig)
            cmds.parent(upLidJnt, jntGrp)
            cmds.delete(parentJnt)
            pac = cmds.parentConstraint(loc, upLidJnt, mo=True, w=1, skipRotate=["x", "y", "z"])
        
        # Lower eyelid
        for lowLidJnt in lowLidJntList :
            locName = lowLidJnt.replace("_Skin", "")
            locName = locName.replace("_jnt", "_loc")
            loc = cmds.spaceLocator(n = locName) [0]
            self.lowLidLocList.append(loc)
            cmds.setAttr(loc + "Shape.localScaleX", 0.025)
            cmds.setAttr(loc + "Shape.localScaleY", 0.025)
            cmds.setAttr(loc + "Shape.localScaleZ", 0.025)
            positionLoc = cmds.xform(lowLidJnt, q = 1, ws = 1, t = 1)
            cmds.xform(loc, ws = 1, t = positionLoc)
            parentJnt = cmds.listRelatives(lowLidJnt, p = 1) [0]
            aim = cmds.aimConstraint(loc, parentJnt, weight = 1, aimVector = (1,0,0), upVector = (0,1,0), worldUpType = "vector", worldUpVector = (0,1,0))
            cmds.parent(loc, hierarchyLowGrp)
            cmds.delete(aim)
            jntGrp = "{}_Low_joints_GRP".format(nameRig)
            cmds.parent(lowLidJnt, jntGrp)
            cmds.delete(parentJnt)
            pac = cmds.parentConstraint(loc, lowLidJnt, mo=True, w=1,skipRotate=["x", "y", "z"])
                
    
    def _getDagPath(self, objectName):
        '''MARCO GIORDANO'S CODE (http://www.marcogiordanotd.com/)
        
        Called by '_getUParam' function.
        Call functions: None '''
    
        if isinstance(objectName, list)==True:
            oNodeList=[]
            for o in objectName:
                selectionList = OpenMaya.MSelectionList()
                selectionList.add(o)
                oNode = OpenMaya.MDagPath()
                selectionList.getDagPath(0, oNode)
                oNodeList.append(oNode)
            return oNodeList
        else:
            selectionList = OpenMaya.MSelectionList()
            selectionList.add(objectName)
            oNode = OpenMaya.MDagPath()
            selectionList.getDagPath(0, oNode)
            return oNode
    
    
    def _getUParam(self, pnt = [], crv = None, *args):
        '''MARCO GIORDANO'S CODE (http://www.marcogiordanotd.com/) - Function called by 
        
        Called by 'connectLocToCrv' function.
        Call functions: '_getDagPath' '''
        
        point = OpenMaya.MPoint(pnt[0],pnt[1],pnt[2])
        curveFn = OpenMaya.MFnNurbsCurve(self._getDagPath(crv))
        paramUtill=OpenMaya.MScriptUtil()
        paramPtr=paramUtill.asDoublePtr()
        isOnCurve = curveFn.isPointOnCurve(point)
        if isOnCurve == True:
            curveFn.getParamAtPoint(point , paramPtr,0.001,OpenMaya.MSpace.kObject )
        else :
            point = curveFn.closestPoint(point,paramPtr,0.001,OpenMaya.MSpace.kObject)
            curveFn.getParamAtPoint(point , paramPtr,0.001,OpenMaya.MSpace.kObject )
        
        param = paramUtill.getDouble(paramPtr)  
        return param
    
    
    def connectLocToCrv(self, upLidLocList, upLidCrv, lowLidLocList, lowLidCrv):
        '''Connect locators to lid curves via pointOnCurveInfo nodes.
        
        Called by 'buildRig' function.
        Call functions: None '''
        
        # Upper eyelid
        for upLidLoc in self.upLidLocList :
            position = cmds.xform(upLidLoc, q = 1, ws = 1, t = 1)
            u = self._getUParam(position, upLidCrv)
            name = upLidLoc.replace("_locator", "_pointOnCurveInfo")
            ptOnCrvInfo = cmds.createNode("pointOnCurveInfo", n = name)
            cmds.connectAttr(upLidCrv + ".worldSpace", ptOnCrvInfo + ".inputCurve")
            cmds.setAttr(ptOnCrvInfo + ".parameter", u)
            cmds.connectAttr(ptOnCrvInfo + ".position", upLidLoc + ".t")
        
        # Lower eyelid
        for lowLidLoc in self.lowLidLocList :
            position = cmds.xform(lowLidLoc, q = 1, ws = 1, t = 1)
            u = self._getUParam(position, lowLidCrv)
            name = lowLidLoc.replace("_locator", "_pointOnCurveInfo")
            ptOnCrvInfo = cmds.createNode("pointOnCurveInfo", n = name)
            cmds.connectAttr(lowLidCrv + ".worldSpace", ptOnCrvInfo + ".inputCurve")
            cmds.setAttr(ptOnCrvInfo + ".parameter", u)
            cmds.connectAttr(ptOnCrvInfo + ".position", lowLidLoc + ".t")
    
    
    def eyelidsCorners(self, upLidEpCrvPos, lowLidEpCrvPos):
        '''Define eye corners position.
        
        Called by 'createDriverCrv' function.
        Call functions: None '''
        
        cornerUp1 = upLidEpCrvPos[0]
        cornerUp2 = upLidEpCrvPos[-1]
        cornerLow1 = lowLidEpCrvPos[0]
        cornerLow2 = lowLidEpCrvPos[-1]

        
        if self.side == "L":
            if cornerUp1[0] > cornerUp2[0]:
                self.cornerAPos = cornerUp2
                self.cornerBPos = cornerUp1
            else:
                self.cornerAPos = cornerUp1
                self.cornerBPos = cornerUp2

        elif self.side == "R":
            if cornerUp1[0] > cornerUp2[0]:
                self.cornerAPos = cornerUp1
                self.cornerBPos = cornerUp2
            else:
                self.cornerAPos = cornerUp2
                self.cornerBPos = cornerUp1

        return self.cornerAPos, self.cornerBPos
    
    
    def eyelidsCrvCVs(self, upLidCrv, lowLidCrv):
        '''List CVs of each guide curve.
        
        Called by 'createDriverCrv' function.
        Call functions: None '''
        
        upLidCVs = []
        x = 0
        while x < 7 :
            posCv = cmds.xform ((upLidCrv + ".cv[%d]" % x), q = 1, ws = 1, t = 1)
            upLidCVs.append (posCv)
            x += 1
        
        lowLidCVs = []
        y = 0
        while y < 7 :
            posCv = cmds.xform ((lowLidCrv + ".cv[%d]" % y), q = 1, ws = 1, t = 1)
            lowLidCVs.append (posCv)
            y += 1
        
        return upLidCVs, lowLidCVs
    
    
    def eyelidsMatchTopology(self, cornerAPos, cornerBPos, upLidCVsPos, lowLidCVsPos):
        '''Reorganise the CVs of each curve so they have the same topology.
        
        Called by 'createDriverCrv' function.
        Call functions: None '''
        
        # Order of CVs in base lists:           Order of CVs in ordered lists:
        # (upLidCVsPos, lowLidCVsPos)           (upLidCVsOrdered, lowLidCVsOrdered)
        # -----------------------               -------------------------
        # INDEX |       CV      |               | INDEX |       CV      |
        # ------|---------------|               |-------|---------------|
        #   0   | corner: ?     |               |   0   | corner A      |
        #   1   |               |               |   1   |               |
        #   2   |               |               |   2   |               |
        #   3   | middle of crv |               |   3   | middle of crv |
        #   4   |               |               |   4   |               |
        #   5   |               |               |   5   |               |
        #   6   | other corner  |               |   6   | corner B      |
        # -----------------------               -------------------------
        
        # - measure dist between first_CV of baseList and cornerAPos
        # - measure dist between first_CV of baseList and cornerBPos
        # - if CV is closer to cornerA append baseList to orderedList from beginning to end
        # - else (CV closer to cornerB) append baseList to orderedList from end to beginning
        # return orderedLists
        
        # distance formula is: d = sqrt((Ax-Bx)**2 + (Ay-By)**2 + (Az-Bz)**2)
        distTEMP1 = math.sqrt ( ( ((upLidCVsPos [0])[0]) - cornerAPos[0] )**2 + ( ((upLidCVsPos [0])[1]) - cornerAPos[1] )**2 + ( ((upLidCVsPos [0])[2]) - cornerAPos[2] )**2 )
        distTEMP2 = math.sqrt ( ( ((upLidCVsPos [0])[0]) - cornerBPos[0] )**2 + ( ((upLidCVsPos [0])[1]) - cornerBPos[1] )**2 + ( ((upLidCVsPos [0])[2]) - cornerBPos[2] )**2 )
        if distTEMP1 < distTEMP2 :
            upLidCVsOrdered = upLidCVsPos
        else:
            upLidCVsOrdered = upLidCVsPos[::-1] # reversed 'upLidCVsPos'
        
        distTEMP3 = math.sqrt ( ( ((lowLidCVsPos [0])[0]) - cornerAPos[0] )**2 + ( ((lowLidCVsPos [0])[1]) - cornerAPos[1] )**2 + ( ((lowLidCVsPos [0])[2]) - cornerAPos[2] )**2 )
        distTEMP4 = math.sqrt ( ( ((lowLidCVsPos [0])[0]) - cornerBPos[0] )**2 + ( ((lowLidCVsPos [0])[1]) - cornerBPos[1] )**2 + ( ((lowLidCVsPos [0])[2]) - cornerBPos[2] )**2 )
        if distTEMP3 < distTEMP4 :
            lowLidCVsOrdered = lowLidCVsPos
        else:
            lowLidCVsOrdered = lowLidCVsPos[::-1] # reversed 'lowLidCVsPos'
            
        return upLidCVsOrdered, lowLidCVsOrdered
    
    
    def createDriverCrv(self, upLidBaseCrv, upRigGrp, lowLidBaseCrv, lowRigGrp):
        '''Create a driver curve for each lid curve and connect it to the base curve with a wire deformer.
        
        Called by 'buildRig' function.
        Call functions: 'eyelidsCorners', 'eyeLidsLeftAndRight' (unused), 'eyelidsCrvCVs', 'eyelidsMatchTopology' '''
        
        ## Upper eyelid ##
        upLidDriverCrvTEMP = cmds.duplicate(upLidBaseCrv)[0]
        cmds.delete(upLidDriverCrvTEMP, ch = 1) # delete history
        cmds.rebuildCurve(upLidDriverCrvTEMP, rpo = 1, end = 1, kr = 2, kcp = 0, kep = 1, kt = 0, s = 4, d = 7, tol = 0.01)
        
        # list the position of the EPs of the upper lid driver curve
        upLidEpPosTEMP = []
        x = 0
        while x < 5:
            posEp = cmds.xform((upLidDriverCrvTEMP + ".ep[%d]" % x), q = 1, ws = 1, t = 1)
            upLidEpPosTEMP.append(posEp)
            x += 1
        cmds.delete(upLidDriverCrvTEMP)
        
        # Create the upLid 'guide' curve for corner placement and query CVs positions and indexes
        upLidGuideCrv = cmds.curve(d = 3, ep =(upLidEpPosTEMP[0], upLidEpPosTEMP[1], upLidEpPosTEMP[2], upLidEpPosTEMP[3], upLidEpPosTEMP[4]))
        
        ## Lower eyelid ##
        lowLidDriverCrvTEMP = cmds.duplicate(lowLidBaseCrv)[0]
        cmds.delete(lowLidDriverCrvTEMP, ch = 1) # delete history
        cmds.rebuildCurve(lowLidDriverCrvTEMP, rpo = 1, end = 1, kr = 2, kcp = 0, kep = 1, kt = 0, s = 4, d = 7, tol = 0.01)
        
        # list the position of the EPs of the lower lid driver curve
        lowLidEpPosTEMP = []
        x = 0
        while x < 5 :
            posEp = cmds.xform((lowLidDriverCrvTEMP + ".ep[%d]" % x), q = 1, ws = 1, t = 1)
            lowLidEpPosTEMP.append(posEp)
            x += 1
        cmds.delete(lowLidDriverCrvTEMP)
        
        # Create the lowLid 'guide' curve for corner placement and query CVs positions and indexes
        lowLidGuideCrv = cmds.curve(d = 3, ep = (lowLidEpPosTEMP[0], lowLidEpPosTEMP[1], lowLidEpPosTEMP[2], lowLidEpPosTEMP[3], lowLidEpPosTEMP[4]))
        
        ##
        
        # Find position of eye corners
        self.cornerAPos, self.cornerBPos = self.eyelidsCorners(upLidEpPosTEMP, lowLidEpPosTEMP)
        
        # List CVs positions of upLidGuideCrv and lowLidGuideCrv
        upLidCVsPos, lowLidCVsPos = self.eyelidsCrvCVs(upLidGuideCrv, lowLidGuideCrv)
        
        # List CVs positions in the right order(to match topology)
        upLidCVsOrdered, lowLidCVsOrdered = self.eyelidsMatchTopology(self.cornerAPos, self.cornerBPos, upLidCVsPos, lowLidCVsPos)
        
        ##
        
        # Create upper driver curve
        self.upLidDriverCrv = cmds.curve(d = 3, p =(upLidCVsOrdered[0], upLidCVsOrdered[1], upLidCVsOrdered[2], upLidCVsOrdered[3], upLidCVsOrdered[4], upLidCVsOrdered[5], upLidCVsOrdered[6]))
        upLidDriverCrvName = upLidBaseCrv.replace("_BASE_", "_DRIVER_")
        self.upLidDriverCrv = cmds.rename(self.upLidDriverCrv, upLidDriverCrvName)
        cmds.parent(self.upLidDriverCrv, upRigGrp)
        
        cmds.delete(upLidGuideCrv)
        
        # Create lower driver curve
        lowCrvTEMP = cmds.curve(d = 3, p =(lowLidCVsOrdered[0], lowLidCVsOrdered[1], lowLidCVsOrdered[2], lowLidCVsOrdered[3], lowLidCVsOrdered[4], lowLidCVsOrdered[5], lowLidCVsOrdered[6]))
        lowLidDriverCrvName = lowLidBaseCrv.replace("_BASE_", "_DRIVER_")
        self.lowLidDriverCrv = cmds.rename(lowCrvTEMP, lowLidDriverCrvName)
        cmds.parent(self.lowLidDriverCrv, lowRigGrp)
        
        cmds.delete(lowLidGuideCrv)
        
        ##
        
        cmds.select(cl = 1)
        wireNodeUpLidName = upLidBaseCrv.replace("_BASE_curve", "_controlCurve_wire")
        wireUpLid = cmds.wire(upLidBaseCrv, n = wireNodeUpLidName, w = self.upLidDriverCrv, gw = 0, en = 1, ce = 0, li = 0, dds=[0, 1000])
        
        cmds.select(cl = 1)
        wireNodeLowLidName = lowLidBaseCrv.replace("_BASE_curve", "_controlCurve_wire")
        wireUpLid = cmds.wire(lowLidBaseCrv, n = wireNodeLowLidName, w = self.lowLidDriverCrv, gw = 0, en = 1, ce = 0, li = 0, dds=[0, 1000])
    
    
    def _fixOrientCtrls(self, loc, jnt, *args):
        '''Fix the orienty of the joint controls.
        
        Called by 'createCrvCtrls' function.
        Call functions: None '''

        piv = cmds.xform(loc, q=True, piv=True, ws=True)
        cmds.select(clear=True)
        jntAim = cmds.joint(n="JNT_ToAim", p=piv[0:3])
        aim = cmds.aimConstraint(jntAim, jnt, weight = 1, aimVector = (0,0,-1), upVector = (0,1,0), worldUpType = "vector", worldUpVector = (0,1,0))

        cmds.delete(aim)
        cmds.delete(jntAim)


    def createJntCtrls(self, nameRig, cornerAPos, cornerBPos, upLidDriverCrv, lowLidDriverCrv, rigGrp):
        '''Creates controller joints for each point of the eyelids driver curves.
        
        Called by 'buildRig' function.
        Call functions: None '''
        
        # Find position of EPs of each driver curve for joint placement
        # upper driver
        upLidEpDriverCrvPos = []
        # self.upper_ep_ordered_list = []
        for idx in range(5):
            ep = "{}.ep[{}]".format(upLidDriverCrv, idx)
            posEp = cmds.xform ("{}.ep[{}]".format(upLidDriverCrv, idx), q = 1, ws = 1, t = 1)
            upLidEpDriverCrvPos.append(posEp)
            # self.upper_ep_ordered_list.append(ep)

        # if upLidEpDriverCrvPos[0] > upLidEpDriverCrvPos[4]:
            # self.upper_ep_ordered_list.reverse()

        # lower driver
        lowLidEpDriverCrvPos = []
        # self.lower_ep_ordered_list = []
        for idx in range(5):
            ep = "{}.ep[{}]".format(lowLidDriverCrv, idx)
            posEp = cmds.xform ("{}.ep[{}]".format(lowLidDriverCrv, idx), q = 1, ws = 1, t = 1)
            lowLidEpDriverCrvPos.append(posEp)
            # self.lower_ep_ordered_list.append(ep)

        # if lowLidEpDriverCrvPos[0] < lowLidEpDriverCrvPos[4]:
            # self.lower_ep_ordered_list.reverse()
        
        # Place controller joints
        self.nameEyelid = ['CornerInn', 'UpInn', 'Up', 'UpOut', 'CornerOut', 'LowOut', 'Low', 'LowInn']
        self.ctrlJnts = []
        posList = [self.cornerAPos, upLidEpDriverCrvPos[1], upLidEpDriverCrvPos[2], upLidEpDriverCrvPos[3], self.cornerBPos, lowLidEpDriverCrvPos[3], lowLidEpDriverCrvPos[2], lowLidEpDriverCrvPos[1]]

        for item, (value, pos) in enumerate(zip(self.nameEyelid, posList)):
            jnt = cmds.joint(rad=0.05, p=pos, n="{}_{}_jnt".format(nameRig, value))
            self.ctrlJnts.append(jnt)
            # fix orient jnt
            self._fixOrientCtrls(self.eyeLoc, jnt)

        # Organise rig hierarchy
        hierarchyCtrlJntGrp = cmds.group(n =(nameRig + "_CTRL_JNT_GRP"), em = 1)
        cmds.parent(hierarchyCtrlJntGrp, rigGrp)
        for jnt in self.ctrlJnts:
            cmds.parent(jnt, hierarchyCtrlJntGrp)
        

    def bindskin_ctrljnts_to_curve(self, upLidDriverCrv, lowLidDriverCrv, ctrlJnts):
        # Skin controllers joints to each driver curve
        kwargs = {
            'toSelectedBones': True,
            'bindMethod': 0,
            'normalizeWeights': 1,
            'weightDistribution': 0,
            'maximumInfluences': 2,
            'obeyMaxInfluences': True,
            'dropoffRate': 4,
            'removeUnusedInfluence': False
        }

        cmds.select(cl = 1)
        # Upper eyelid
        upper_jnt = ["{}_EyeSocket_CornerInn_jnt".format(self.side), "{}_EyeSocket_UpInn_jnt".format(self.side), "{}_EyeSocket_Up_jnt".format(self.side), "{}_EyeSocket_UpOut_jnt".format(self.side), "{}_EyeSocket_CornerOut_jnt".format(self.side)]  
        cmds.skinCluster(upper_jnt, upLidDriverCrv, n=f"SK_{self.side}_eyesocket_driver_upper", **kwargs)

        # Lower eyelid
        lower_jnt = ["{}_EyeSocket_CornerInn_jnt".format(self.side), "{}_EyeSocket_CornerOut_jnt".format(self.side), "{}_EyeSocket_LowOut_jnt".format(self.side), "{}_EyeSocket_Low_jnt".format(self.side), "{}_EyeSocket_LowInn_jnt".format(self.side)]
        cmds.skinCluster(lower_jnt, lowLidDriverCrv, n=f"SK_{self.side}_eyesocket_driver_lower", **kwargs)
    
   
    def makeCtrl(self, sel, name, *args):
        '''Creates the controls.
        
        Called by 'createCrvCtrls' function.
        Call functions: None '''

        cmds.select(cl = 1)
        TEMP_CTRL1 = cmds.circle(r = 0.15, n=name)[0]
        TEMP_CTRL2 = cmds.duplicate()[0]
        cmds.setAttr(TEMP_CTRL2 + ".rotateY", 90)
        TEMP_CTRL3 = cmds.duplicate()[0]
        cmds.setAttr(TEMP_CTRL3 + ".rotateX", 90)
        cmds.parent(TEMP_CTRL2, TEMP_CTRL3, TEMP_CTRL1)
        cmds.makeIdentity(apply = 1, t = 1, r = 1, s = 1, n = 0, pn = 1)
        cmds.pickWalk(d = "down")
        cmds.select(TEMP_CTRL1, tgl = 1)
        cmds.parent(r = 1, s = 1)
        cmds.delete(TEMP_CTRL2, TEMP_CTRL3)
        cmds.select(cl = 1)

        return TEMP_CTRL1


    def createCrvCtrls(self, side, nameRig, ctrlJnts):
        '''Creates controller curve for each controller joint.
        
        Called by 'buildRig' function.
        Call functions: makeCtrl '''
        
        # Organize rig hierarchy
        hierarchySecondGrp = cmds.group(n="{}_Base_Ctrl_Grp".format(nameRig), em = 1)
        
        self.hierarchyMainGrpCTRL = "{}_CTRL_GRP".format(nameRig)
        
        if cmds.objExists("|" + self.hierarchyMainGrpCTRL) :
            cmds.parent(hierarchySecondGrp, ("|" + self.hierarchyMainGrpCTRL))
        else :
            cmds.group(n = self.hierarchyMainGrpCTRL, em = 1)
            cmds.parent(hierarchySecondGrp, ("|" + self.hierarchyMainGrpCTRL))
        
        if cmds.objExists(nameRig + "_RIG_GRP"):
            cmds.parent(self.hierarchyMainGrpCTRL, self.grpAllRig)

        # Create the controls and constrain the joints
        self.ctrlList = []
        ctrlOffsetGrpList = []
        ctrlSecList = []

        for index, value in enumerate(self.nameEyelid):
            if int(index) % 2 == 1:
                tmp = "{}_{}_Ctrl".format(nameRig, value)
                ctrlSecList.append(tmp)

        for idx, jnt in enumerate(ctrlJnts):
            ctrlName = jnt.replace("jnt", "Ctrl")
            ctrl = self.makeCtrl(jnt, ctrlName)
            self.ctrlList.append(ctrl)
            pointC_TEMP = cmds.pointConstraint(jnt, ctrl)
            cmds.delete(pointC_TEMP)

            # fix orient ctrl
            self._fixOrientCtrls(self.eyeLoc, ctrl)
            
            origName = "{}_MASTER_Grp".format(ctrlName)
            origGrp = cmds.group(n=origName, em=1)
            parentC_TEMP = cmds.parentConstraint(ctrl, origGrp)
            cmds.delete(parentC_TEMP)
            if ctrl in ctrlSecList: # If controller is 'secondary'
                offsetGrpName = origName.replace("MASTER", "MATRIX")
                offsetGrp = cmds.duplicate(origGrp, n = offsetGrpName)
                cmds.parent(ctrl, offsetGrp)
                cmds.parent(offsetGrp, origGrp)
                ctrlOffsetGrpList.extend(offsetGrp)
                cmds.setAttr("{}.visibility".format(origName), 0)
            else:
                offsetGrpName = origName.replace("MASTER", "MATRIX")
                offsetGrp = cmds.duplicate(origGrp, n = offsetGrpName)
                cmds.parent(ctrl, offsetGrp)
                cmds.parent(offsetGrp, origGrp)

            # Match posizione con controlli eyelid e aggiungere attributo Follow
            if idx == 2:
                cmds.matchTransform(origGrp, "{}_Eyelid_Up_Ctrl".format(side), rot=True)
                cmds.addAttr(ctrl, ln="follow", at="float", min=0, max=1, dv=0.5, k=True)
            elif idx == 6:
                cmds.matchTransform(origGrp, "{}_Eyelid_Low_Ctrl".format(side), rot=True)
                cmds.addAttr(ctrl, ln="follow", at="float", min=0, max=1, dv=0.5, k=True)

            cmds.parent(origGrp, hierarchySecondGrp)
            cmds.parentConstraint(ctrl, jnt, mo=True)

        cmds.select (cl = 1)

        # visibility corners ctrl
        cmds.setAttr("{}_CornerInn_Ctrl_MASTER_Grp.visibility".format(nameRig), 0)
        cmds.setAttr("{}_CornerOut_Ctrl_MASTER_Grp.visibility".format(nameRig), 0)

        # Lock and hide unused channels
        for ctrl in self.ctrlList :
            cmds.setAttr((ctrl + ".sx"), lock = 1, keyable = 0, channelBox = 0)
            cmds.setAttr((ctrl + ".sy"), lock = 1, keyable = 0, channelBox = 0)
            cmds.setAttr((ctrl + ".sz"), lock = 1, keyable = 0, channelBox = 0)
            cmds.setAttr((ctrl + ".v"), lock = 1, keyable = 0, channelBox = 0)

        # Controlla se il set contenente i controlli esiste, altrimenti crealo
        if not cmds.objExists("FACE_MODULES_Controls"):
            module_controls_set = cmds.sets(empty=True, name=("FACE_MODULES_Controls"))
        else:
            module_controls_set = "FACE_MODULES_Controls"

        # Controlla se il set della parte di rig dei joints esiste, altrimenti crealo
        if not cmds.objExists("HeadEyes_controls"):
            rigpart_controls_set = cmds.sets(empty=True, name=("HeadEyes_controls"))
            # Parenta il set della parte di rig dentro al set globale
            cmds.sets(rigpart_controls_set, edit=True, forceElement=module_controls_set)
        else:
            rigpart_controls_set = "HeadEyes_controls"

        # Controlla se il set dei controlli del modulo esiste, altrimenti crealo
        if not cmds.objExists("{}_controls".format(self.nameRig)):
            self.set_eyesocket_ctrls = cmds.sets(empty=True, name=("{}_controls".format(self.nameRig)))
            # Parenta il set del modulo dentro al set rigpart
            cmds.sets(self.set_eyesocket_ctrls, edit=True, forceElement=rigpart_controls_set)
        else:
            self.set_eyesocket_ctrls = "{}_controls".format(self.nameRig)

        for ctrl in self.ctrlList:
            cmds.sets(ctrl, edit=True, forceElement=self.set_eyesocket_ctrls)


    def add_squint_follow(self, side):
        ctrl_list = ("{}_EyeSocket_Up_Ctrl".format(side), "{}_EyeSocket_Low_Ctrl".format(side))

        # Aggiungi gruppo SQUINT sopra il MATRIX
        for ctrl in ctrl_list:
            grp = cmds.listRelatives(ctrl, p=True)[0]
            name = grp.replace("MATRIX", "SQUINT")
            cmds.group(grp, n=name)


    def replace_and_remove_shapes(self, *args):
        # lista contenente i controlli da sostituire
        control_pairs = [
                ('L_EyeSocket_Up_Ctrl_REF', 'L_EyeSocket_Up_Ctrl'),
                ('L_EyeSocket_Low_Ctrl_REF', 'L_EyeSocket_Low_Ctrl'),
                ('R_EyeSocket_Up_Ctrl_REF', 'R_EyeSocket_Up_Ctrl'),
                ('R_EyeSocket_Low_Ctrl_REF', 'R_EyeSocket_Low_Ctrl')
            ]

        # importa scena con i controlli
        cmds.file(self.my_file_path, i=True, mergeNamespacesOnClash=True, namespace=':', gr=True, gn="GRP_shapes_REF")

        # itera attraverso le coppie di controlli e esegui la sostituzione
        for source_ctrl, target_ctrl in control_pairs:
            try:
                # Ottieni e elimina le shape esistenti nel controllo di destinazione
                target_shapes = cmds.listRelatives(target_ctrl, shapes=True)
                name_target_shape = target_shapes
                if target_shapes:
                    cmds.delete(target_shapes)

                # Ottieni la shape del controllo sorgente
                new_source_ctrl = cmds.duplicate(source_ctrl, n="{}_NEW".format(source_ctrl))
                source_shapes = cmds.listRelatives(source_ctrl, shapes=True)
                if source_shapes:
                    for source_shape in source_shapes:
                        cmds.parent(source_shape, target_ctrl, r=True, shape=True)
                        cmds.rename(source_shape, name_target_shape[0])
            except:
                pass

        # elimina il gruppo dei controlli importati
        grp_del = "GRP_shapes_REF"
        cmds.delete("GRP_shapes_REF")
    

    def create_matrix_system(self):
        EyeSocket_UpInn_list = [['{}_EyeSocket_Up_Ctrl'.format(self.side), '{}_EyeSocket_UpInn_Ctrl_MATRIX_Grp'.format(self.side), '{}_EyeSocket_UpInn_Ctrl_MASTER_Grp'.format(self.side)]]
        EyeSocket_UpOut_list = [['{}_EyeSocket_Up_Ctrl'.format(self.side), '{}_EyeSocket_UpOut_Ctrl_MATRIX_Grp'.format(self.side), '{}_EyeSocket_UpOut_Ctrl_MASTER_Grp'.format(self.side)]]
        EyeSocket_LowInn_list = [['{}_EyeSocket_Low_Ctrl'.format(self.side), '{}_EyeSocket_LowInn_Ctrl_MATRIX_Grp'.format(self.side), '{}_EyeSocket_LowInn_Ctrl_MASTER_Grp'.format(self.side)]]
        EyeSocket_LowOut_list = [['{}_EyeSocket_Low_Ctrl'.format(self.side), '{}_EyeSocket_LowOut_Ctrl_MATRIX_Grp'.format(self.side), '{}_EyeSocket_LowOut_Ctrl_MASTER_Grp'.format(self.side)]]
    
        mtx_EyeSocket_UpInn = mtx.MatrixConstraint(EyeSocket_UpInn_list, t=[(1, 1, 1)], r=[(1, 1, 1)]).matrix_constraint()
        mtx_EyeSocket_UpOut = mtx.MatrixConstraint(EyeSocket_UpOut_list, t=[(1, 1, 1)], r=[(1, 1, 1)]).matrix_constraint()
        mtx_EyeSocket_LowInn = mtx.MatrixConstraint(EyeSocket_LowInn_list, t=[(1, 1, 1)], r=[(1, 1, 1)]).matrix_constraint()
        mtx_EyeSocket_LowOut = mtx.MatrixConstraint(EyeSocket_LowOut_list, t=[(1, 1, 1)], r=[(1, 1, 1)]).matrix_constraint()


    # -----------------------------------------------------------------
    # Finalizza il modulo
    # -----------------------------------------------------------------
    def duplicate_and_rename_groups_and_controls(self, base_group, rig_hierarchy):
        """Fa un duplicato di 'base_group' e rinominalo con tutto il suo contenuto col suffisso '_Static'."""
        # Rinomina il gruppo dei controlli 'base_group' col suffisso '_Static'.
        self.static_grp = cmds.rename(base_group, "{}_Static".format(base_group))
        
        # Duplicalo come 'driver_grp'
        driver_grp = cmds.duplicate(self.static_grp, n=base_group)[0]

        # Pulisci il driver_grp da eventuali constraint
        children = cmds.listRelatives(driver_grp, allDescendents=True, fullPath=True, shapes=False)
        for item in children:
            check_type = cmds.nodeType(item)
            if 'Constraint' in check_type:
                # print("Constraint detected, deleting '{}'".format(item))
                cmds.delete(item)

        # Rinomina tutti i gruppi e controlli del gruppo 'static_grp' col suffisso '_Static', saltando le shapes.
        children = cmds.listRelatives(self.static_grp, allDescendents=True, fullPath=True, shapes=False)
        for item in children:
            check_type = cmds.nodeType(item)
            if check_type == 'transform':
                tmp = item.split("|")[-1]
                cmds.rename(item, "{}_Static".format(tmp))

        # Metti in gerachia driver_grp e nascondi lo static_grp
        print("sideEye: {}".format(self.side))
        if self.side == "L":
            cmds.parent(driver_grp, rig_hierarchy[0])
        elif self.side == "R":
            cmds.parent(driver_grp, rig_hierarchy[1])
        
        cmds.setAttr("{}.visibility".format(self.static_grp), 0)

        # # Aggiungere attributo Follow
        # ctrl_list = ["{}_EyeSocket_Up_Ctrl".format(self.side), "{}_EyeSocket_Low_Ctrl".format(self.side)]
        # for ctrl in ctrl_list:
        #     cmds.addAttr(ctrl, ln="follow", at="float", min=0, max=1, dv=1, k=True)

        print("# Duplicato e rinominato tutto il contenuto di '{}' col suffisso '_Static'. #".format(base_group))



    def connect_driver_to_static(self, driver_grp, static_grp):
        """Crea connessioni dirette tra i controlli e gruppi Matrix di driver e static """
        children_static = cmds.listRelatives(static_grp, allDescendents=True, fullPath=False, shapes=False)
        children_driver = cmds.listRelatives(driver_grp, allDescendents=True, fullPath=False, shapes=False)

        # '_Ctrl' --> '_Ctrl_Static'
        for static_ctrl, driver_ctrl in zip(children_static, children_driver):
            if cmds.nodeType(driver_ctrl) == 'transform':
                name_tokens = driver_ctrl.split("_")[-1]
                if name_tokens == "Ctrl":
                    # print("{} --> {}".format(driver_ctrl, static_ctrl))
                    driver_attrs = cmds.listAttr(driver_ctrl, keyable=True) or []
                    for attr in driver_attrs:
                        if cmds.attributeQuery(attr, node=static_ctrl, exists=True) and cmds.getAttr(driver_ctrl + '.' + attr, keyable=True):
                            cmds.connectAttr("{}.{}".format(driver_ctrl, attr), "{}.{}".format(static_ctrl, attr), force=True)

        # '_Ctrl_MATRIX_Grp_Static' --> '_Ctrl_MATRIX_Grp' valido per [MATRIX, FOLLOW, SQUARE, COMPENSATE]
        lista_gruppi = ('MATRIX', 'FOLLOW', 'SQUARE', 'COMPENSATE', 'SQUINT')
        for static_grp, driver_grp in zip(children_static, children_driver):
            if cmds.nodeType(driver_grp) == 'transform':
                name_tokens = driver_grp.split("_")
                for grp in lista_gruppi:
                    if grp in name_tokens:
                        # print("{} --> {}".format(static_grp, driver_grp))
                        driver_attrs = cmds.listAttr(static_grp, keyable=True) or []
                        for attr in driver_attrs:
                            if cmds.attributeQuery(attr, node=driver_grp, exists=True) and cmds.getAttr("{}.{}".format(static_grp, attr), keyable=True):
                                cmds.connectAttr("{}.{}".format(static_grp, attr), "{}.{}".format(driver_grp, attr), force=True)

        print("# Connessioni dirette completate tra driver e static. #")

    
    def autosocket_system(self):
        """Connessione diretta da Eyelid_Up_Ctrl_Static, il resto verrà gestito dallo squint dall Brow_Master_Ctrl."""
        # Crea multiplyDivide node per l'attributo Follow
        mlp_follow_up = cmds.createNode('multiplyDivide', n="MLP_{}_EyeSocket_Up_FOLLOW".format(self.side))
        mlp_follow_low = cmds.createNode('multiplyDivide', n="MLP_{}_EyeSocket_Low_FOLLOW".format(self.side))

        # Crea multiplyDivide node SLIDER per il follow
        mlp_slider_up = cmds.createNode('multiplyDivide', n="MLP_{}_Eyelid_Up_Ctrl_Static_to_{}_EyeSocket_Up_Ctrl_MATRIX_Grp_Static_Translate_Follow".format(self.side, self.side))
        mlp_slider_low = cmds.createNode('multiplyDivide', n="MLP_{}_Eyelid_Low_Ctrl_Static_to_{}_EyeSocket_Low_Ctrl_MATRIX_Grp_Static_Translate_Follow".format(self.side, self.side))
        self.set_sliders.append(mlp_slider_up)
        self.set_sliders.append(mlp_slider_low)
        
        for attr in ("X", "Y", "Z"):
            cmds.setAttr("{}.input2{}".format(mlp_slider_up, attr), 0.5)
            cmds.setAttr("{}.input2{}".format(mlp_slider_low, attr), 0.5)

        # UP --> Connetti Eyelid_Up_Ctrl_Static a EyeSocket_Up_Ctrl_MATRIX_Grp_Static
        cmds.connectAttr("{}_Eyelid_Up_Ctrl.rotate".format(self.side), "{}_EyeSocket_Up_Ctrl_MATRIX_Grp_Static.rotate".format(self.side), force=True)
        cmds.connectAttr("{}_Eyelid_Up_Ctrl.scale".format(self.side), "{}_EyeSocket_Up_Ctrl_MATRIX_Grp_Static.scale".format(self.side), force=True)

        cmds.connectAttr("{}_Eyelid_Up_Ctrl.translate".format(self.side), "{}.input1".format(mlp_slider_up), force=True)
        cmds.connectAttr("{}.output".format(mlp_slider_up), "{}.input1".format(mlp_follow_up), force=True)
        
        for attr in ('X', 'Y', 'Z'):
            cmds.connectAttr("{}_EyeSocket_Up_Ctrl_Static.follow".format(self.side), "{}.input2{}".format(mlp_follow_up, attr), force=True)
    
        cmds.connectAttr("{}.output".format(mlp_follow_up), "{}_EyeSocket_Up_Ctrl_MATRIX_Grp_Static.translate".format(self.side), force=True)

        # LOW --> Connetti Eyelid_Low_Ctrl_Static a EyeSocket_Low_Ctrl_MATRIX_Grp_Static
        cmds.connectAttr("{}_Eyelid_Low_Ctrl.rotate".format(self.side), "{}_EyeSocket_Low_Ctrl_MATRIX_Grp_Static.rotate".format(self.side), force=True)
        cmds.connectAttr("{}_Eyelid_Low_Ctrl.scale".format(self.side), "{}_EyeSocket_Low_Ctrl_MATRIX_Grp_Static.scale".format(self.side), force=True)

        cmds.connectAttr("{}_Eyelid_Low_Ctrl.translate".format(self.side), "{}.input1".format(mlp_slider_low), force=True)
        cmds.connectAttr("{}.output".format(mlp_slider_low), "{}.input1".format(mlp_follow_low), force=True)
        
        for attr in ('X', 'Y', 'Z'):
            cmds.connectAttr("{}_EyeSocket_Low_Ctrl_Static.follow".format(self.side), "{}.input2{}".format(mlp_follow_low, attr), force=True)
    
        cmds.connectAttr("{}.output".format(mlp_follow_low), "{}_EyeSocket_Low_Ctrl_MATRIX_Grp_Static.translate".format(self.side), force=True)


    def make_sliders(self, nameRig, nodi):
        """Rinomina i nodi passati come input con il suffisso 'SLIDER' e se non esiste crea il set slider corrispondente."""
        sliders = []

        for item in nodi:
            nodo = cmds.rename(item, "{}_SLIDER".format(item))
            sliders.append(nodo)

        # Controlla se il set globale degli sliders esiste, altrimenti crealo
        if not cmds.objExists("SLIDERS"):
            sliders_set = cmds.sets(empty=True, name=("SLIDERS"))
        else:
            sliders_set = "SLIDERS"

        # Controlla se il set della parte di rig degli sliders esiste, altrimenti crealo
        if not cmds.objExists("SLIDERS_HeadEyes"):
            rigpart_sliders_set = cmds.sets(empty=True, name=("SLIDERS_HeadEyes"))
            # Parenta il set della parte di rig dentro al set globale
            cmds.sets(rigpart_sliders_set, edit=True, forceElement=sliders_set)
        else:
            rigpart_sliders_set = "SLIDERS_HeadEyes"

        # crea sotto-set del singolo modulo
        if not cmds.objExists("{}_sliders".format(nameRig)):
            set_module_sliders = cmds.sets(empty=True, name=("{}_sliders".format(nameRig)))
            # Parenta il set del modulo dentro al set globale
            cmds.sets(set_module_sliders, edit=True, forceElement=rigpart_sliders_set)
        else:
            set_module_sliders = "{}_sliders".format(nameRig)

        # Popola il set
        for nodo in sliders:
            cmds.sets(nodo, edit=True, forceElement=set_module_sliders)

        print("# Sliders rinominati e set creato. #")
    

    def insert_driver_crv_in_set_slider(self, nameRig, up_curve, low_curve):
        set_module_sliders = "{}_sliders".format(nameRig)
        
        for curve in (up_curve, low_curve):
            cmds.sets(curve, edit=True, forceElement=set_module_sliders)


    def rename_ctrl_shapes(self, set_ctrl):
        """Data una selezione di controlli, rinomina le shapes in base al nome del controllo."""
        cmds.select(clear=True)
        cmds.select(set_ctrl)
        sel = cmds.ls(sl=True)
        cmds.select(clear=True)
        
        for ctrl in sel:
            shapes = cmds.listRelatives(ctrl, shapes=True, pa=True)
            for shape in shapes:
                new_name = "{}Shape".format(ctrl)
                cmds.rename(shape, new_name)

        print("### Le shapes dei controlli sono state rinominate correttamente. ###")


    def EyesSocketFix(self, side):
        group_name = f"{side}_EyeSocket_JNT_GRP"

        # Nome del world up object
        world_up_object = "Eyelid_LOC"

        # Recupera tutti gli aim constraint all'interno del gruppo
        constraints = cmds.listRelatives(group_name, allDescendents=True, type="aimConstraint")

        if constraints:
            for constraint in constraints:
                # Imposta il world up type su "Object rotation up"
                cmds.setAttr(f"{constraint}.worldUpType", 2)  # 2 corrisponde a "Object Up (Object Rotation Up)"
                
                # Disconnette eventuali connessioni esistenti prima di effettuare la nuova connessione
                connected_attrs = cmds.listConnections(f"{constraint}.worldUpMatrix", plugs=True)
                if connected_attrs:
                    for attr in connected_attrs:
                        cmds.disconnectAttr(attr, f"{constraint}.worldUpMatrix")
                
                # Connette il world up object
                cmds.connectAttr(f"{world_up_object}.worldMatrix[0]", f"{constraint}.worldUpMatrix", force=True)

            print(f"Aim constraints aggiornati con successo per il lato {side}.")
        else:
            print(f"Nessun aim constraint trovato all'interno del gruppo {group_name}.")

        # Nome dei gruppi coinvolti
        eyesocket_jnt_grp = f"{side}_EyeSocket_JNT_GRP"
        eye_driver_grp = f"{side}_Eye_driver"
        eyesocket_ctrl_grp_static = f"{side}_EyeSocket_CTRL_GRP_Static"
        eyelid_ctrl_grp = f"{side}_Eyesocket_CTRL_GRP"
        eyemover_ctrl = f"{side}_Eyemover_Ctrl"
        eyesocket_ctrl_grp_to_remove = f"{side}_EyeSocket_CTRL_GRP"
        eyesocket_up_joints_grp = f"{side}_EyeSocket_Up_joints_GRP"
        eyesocket_low_joints_grp = f"{side}_EyeSocket_Low_joints_GRP"
        eyesocket_Ctrl_joints = f"{side}_EyeSocket_CTRL_JNT_GRP"

        # Funzione per rimuovere il suffisso "_Static"
        def remove_suffix(name, suffix="_Static"):
            if name.endswith(suffix):
                return name[:-len(suffix)]
            return name

        # Posizionare "Eyelid_JNT_GRP" come figlio di "Eye_driver"
        cmds.parent(eyesocket_jnt_grp, eye_driver_grp)
        cmds.parent(eyesocket_Ctrl_joints, "Head_jnt")

        # Posizionare "Eyelid_CTRL_GRP_Static" come figlio di "Eyemover_Ctrl"
        cmds.parent(eyesocket_ctrl_grp_static, eyemover_ctrl)

        # Eliminare "Eyelid_CTRL_GRP"
        if cmds.objExists(eyesocket_ctrl_grp_to_remove):
            cmds.delete(eyesocket_ctrl_grp_to_remove)

        # Funzione per rinominare il gruppo e i suoi figli
        def rename_group_and_children(group):
            # Rimuovere il suffisso dal nome del gruppo principale
            new_name = remove_suffix(group)
            if new_name != group:
                group = cmds.rename(group, new_name)
            
            # Recuperare tutti i figli del gruppo
            children = cmds.listRelatives(group, allDescendents=True, fullPath=True) or []
            
            # Rinominare tutti i figli
            for child in children:
                new_child_name = remove_suffix(child.split('|')[-1])  # Prendi solo il nome dell'oggetto, non il percorso completo
                cmds.rename(child, new_child_name)

        if cmds.objExists(eyesocket_up_joints_grp) and cmds.objExists(eyesocket_low_joints_grp):
            # Recupera i figli del gruppo up_joints e low_joints
            up_joints_children = cmds.listRelatives(eyesocket_up_joints_grp, children=True, fullPath=True) or []
            low_joints_children = cmds.listRelatives(eyesocket_low_joints_grp, children=True, fullPath=True) or []

            # Sposta i figli sotto il driver principale
            for child in up_joints_children + low_joints_children:
                cmds.parent(child, eye_driver_grp)

            # Cancella i gruppi padre ormai vuoti
            cmds.delete(eyesocket_up_joints_grp)
            cmds.delete(eyesocket_low_joints_grp)

        if cmds.objExists(eyesocket_jnt_grp):
            # Recupera i figli del gruppo eyelid_jnt_grp
            children = cmds.listRelatives(eyesocket_jnt_grp, children=True, fullPath=True) or []
            
            if children:
                # Sposta i figli sotto il driver principale
                for child in children:
                    cmds.parent(child, eye_driver_grp)
            
            # Cancella il gruppo se non ha più figli
            cmds.delete(eyesocket_jnt_grp)

        # Esegui il rinominare del gruppo e dei figli
        rename_group_and_children(eyesocket_ctrl_grp_static)

        # Cambia l'attributo visibility del gruppo L_Eyelid_CTRL_GRP a 1
        if cmds.objExists(f"{side}_EyeSocket_CTRL_GRP"):
            cmds.setAttr(f"{side}_EyeSocket_CTRL_GRP.visibility", 1)

        print(f"Script eseguito con successo per il lato {side}.")


    # -----------------------------------------------------------------
    # Funzioni lanciate dalla UI
    # -----------------------------------------------------------------
    def buildRig (self, *args):
        '''Build eyesockets rig.
        
        Called by 'UI' function.
        Call functions: 'vtxToJnt', 'placeRigLoc', 'createEyelidsCrv', 'connectLocToCrv', 
                        'createDriverCrv', 'createJntCtrls', 'createCrvCtrls', 'addSmartBlink' '''
        
        if self.eyeLoc == None or self.upperLidVtx == None or self.lowerLidVtx == None :
            cmds.error ("Please define eye center and eyelids vertices.")
        else : # Call functions to build rig #
            # Step 1: creates a "high-res" curve for each lid (each vertex is a point of the curve)
            self.createEyelidsCrv(self.nameRig, self.upperLidVtx, self.lowerLidVtx, self.grpBaseRig)
            # Step 2: places one joint per curve cv
            self.cv_base_curve_to_joint (self.eyeLoc, self.upLidCrv, self.lowLidCrv)
            # Step 3: places one locator per curve cv and constrain-aim each joint to it (so as it acts like an IK)
            self.place_rig_locators(self.nameRig, self.upLidJntList, self.lowLidJntList)
            # Step 4: connects each locator to the curve with a pointOnCurve node, so when the CVs of the curve move, the corresponding locator follows (and so does the joint)
            self.connectLocToCrv(self.upLidLocList, self.upLidCrv, self.lowLidLocList, self.lowLidCrv)
            # Step 5: creates a "low-res" curve with only 5 control points and makes it drive the high-res curve with a wire deformer
            self.createDriverCrv(self.upLidCrv, self.hierarchyUpCrvGrp, self.lowLidCrv, self.hierarchyLowCrvGrp)
            # Step 6: creates controller joints to drive the 'driver curve'
            self.createJntCtrls(self.nameRig, self.cornerAPos, self.cornerBPos, self.upLidDriverCrv, self.lowLidDriverCrv, self.grpBaseRig)
            # Step 7: creates curve controllers, and attached the corresponding joints to them
            self.createCrvCtrls(self.side, self.nameRig, self.ctrlJnts)
            # Step 8: add group SQUINT on ctrl Up and Low
            self.add_squint_follow(self.side)
            # Step 9: skinna la curva driver con i ctrlJnts 
            self.bindskin_ctrljnts_to_curve(self.upLidDriverCrv, self.lowLidDriverCrv, self.ctrlJnts)
            # Step 10: import the new controls_shapes and replace them
            self.replace_and_remove_shapes()
            # Step 11: crea tutte le connessioni via matrici
            self.create_matrix_system()
            # Step 12: finalizza il modulo
            self.duplicate_and_rename_groups_and_controls(self.hierarchyMainGrpCTRL, self.parent_rig)
            self.connect_driver_to_static(self.hierarchyMainGrpCTRL, self.static_grp)
            self.rename_ctrl_shapes(self.set_eyesocket_ctrls)
            # Step 13: sistema auto_eyelid
            self.autosocket_system()
            self.make_sliders(self.nameRig, self.set_sliders)
            self.insert_driver_crv_in_set_slider(self.nameRig, self.upLidDriverCrv, self.lowLidDriverCrv)
            
            self.EyesSocketFix(self.side)

            # Clear scene & script variables #
            cmds.delete (self.eyeLoc)
            cmds.select (cl = 1)
            
            self.eyeLoc = None
            self.eyeName = None
            self.upperLidVtx = None
            self.lowerLidVtx = None
            self.set_sliders[:] = []
            
            # Update UI #
            cmds.textField (self.txtfLoc, e = 1, tx = "")
            cmds.button (self.btnPlaceCenter, e = 1, en = 1)
            cmds.button (self.btnUndoPlaceCenter, e = 1, en = 0)
            cmds.scrollField (self.scrollfUpLid, e = 1, cl = 1)
            cmds.scrollField (self.scrollfLowLid, e = 1, cl = 1)
            
            # End message
            print("### {}_EyeSocket have been successfully rigged. ###".format(self.side))
    
    
    # -----------------------------------------------------------------
    # UI
    # -----------------------------------------------------------------
    def UI (self):
        '''Creates UI - Main function
        
        Call functions: 'placeEyeCenter', 'placeEyeCenterUndo', 
                        'upLidVtxSet', 'lowLidVtxSet', 'buildRig' '''

        # Main window
        
        winWidth = 290
        
        UKDP_mainWin = "RBW_EyeSockets_Rig"

        if cmds.window("RBW_EyeSockets_Rig", exists = 1):
            cmds.deleteUI("RBW_EyeSockets_Rig", window = 1)

        ww = cmds.window("RBW_EyeSockets_Rig", title = "RBW EyeSockets Rig", mxb = 0, sizeable=True, resizeToFitChildren=True)

        # Get a pointer and convert it to Qt Widget object
        qw = omui.MQtUtil.findWindow(ww)
        widget = wrapInstance(int(qw), QWidget)

        # Create a QIcon object
        iconpath = os.path.join(self.image_path, "RainbowCGI_icona.ico")

        # Assign the icon
        icon = QIcon(iconpath)
        widget.setWindowIcon(icon)
        
        # Main layout
        col = cmds.columnLayout(co=("both", 5), adjustableColumn=True)
        
        # Carica l'immagine degli eyesockets
        img = os.path.join(self.image_path, "eyesocket.png")
        cmds.text(h = 10, l = "")
        cmds.iconTextButton(style="iconOnly", image1=img, p=col, w=100)
        cmds.text(h = 10, l = "")
        cmds.setParent("..")

        # FIELDS
        cmds.frameLayout( label='1. Select a side', marginHeight=10, bgc = (0.8, 0.50, 0.0))
        cmds.columnLayout( adjustableColumn=True )
        self.radioChecked = cmds.radioButtonGrp(labelArray2=['Left', 'Right'], on1=partial(self.sideEye, 'L') , on2=partial(self.sideEye, 'R') , numberOfRadioButtons=2, columnWidth2=(150, 150), columnAttach2=('left', 'right'), columnOffset2=(80, 80))
        cmds.setParent("..")
        cmds.setParent("..")

        cmds.separator(h = 15, w = winWidth, style = "in")

        # Define eyeball center
        cmds.frameLayout( label="2. Select eyeball, then click 'Place center'", marginHeight=10, bgc = (0.8, 0.50, 0.0))
        cmds.rowLayout(numberOfColumns = 2, adjustableColumn = 1)
        self.btnPlaceCenter = cmds.button(w = ((winWidth / 2) - 2), l = "Place center", c = self.placeEyeCenter)
        self.btnUndoPlaceCenter = cmds.button(w = ((winWidth / 2) - 2), l = "Undo", c = self.placeEyeCenterUndo, en = 0)

        cmds.setParent("..")
        cmds.setParent("..")
        cmds.text(h = 5, l = "")
        self.txtfLoc = cmds.textField(w = winWidth, ed = 0)
        
        cmds.separator(h = 15, w = winWidth, style = "in")

        # List upper lid vertices
        cmds.frameLayout( label="3. Select vertices of upper eye socket, then click 'Set''", marginHeight=10, bgc = (0.8, 0.50, 0.0))
        self.btnUpLid = cmds.button(w = winWidth, l = "Set", c = self.upLidVtxSet)
        self.scrollfUpLid = cmds.scrollField(w = winWidth, h = 35, wordWrap = 1, ed = 0, en = 0)
        cmds.setParent("..")

        cmds.separator (h = 15, w = winWidth, style = "in")

        # List lower lid vertices
        cmds.frameLayout( label="4. Select vertices of lower eye socket, then click 'Set''", marginHeight=10, bgc = (0.8, 0.50, 0.0))
        self.btnLowLid = cmds.button(w = winWidth, l = "Set", c = self.lowLidVtxSet)
        self.scrollfLowLid = cmds.scrollField(w = winWidth, h = 35, wordWrap = 1, ed = 0, en = 0)
        cmds.setParent("..")

        cmds.separator(h = 15, w = winWidth, style = "in")
        cmds.text(h = 5, l = "")

        # Build final rig
        self.btnBuild = cmds.button(w = winWidth, h = 60, l = "BUILD RIG", c = self.buildRig, bgc = (0.8, 0.50, 0.0))
        
        cmds.text(h = 10, l = "")
        
        cmds.showWindow("RBW_EyeSockets_Rig")


# autoEyeSocketsRig = AER()
# autoEyeSocketsRig.UI() # load UI