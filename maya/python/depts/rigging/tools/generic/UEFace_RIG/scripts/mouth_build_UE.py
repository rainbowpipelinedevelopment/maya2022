# -*- coding: utf-8 -*-

import maya.cmds as cmds
import maya.mel as mel
from maya import OpenMaya
from maya import OpenMayaUI as omui
import math, os, sys
from imp import reload
import depts.rigging.tools.generic.UEFace_RIG.scripts.matrix_module as mtx
reload(mtx)


# -----------------------------------------------------------------
# PySide
# -----------------------------------------------------------------
try:
    from shiboken2 import wrapInstance
except ImportError:
    from shiboken import wrapInstance

try:
    from PySide2.QtGui import QIcon
    from PySide2.QtWidgets import QWidget
except ImportError:
    from PySide.QtGui import QIcon, QWidget


class AER :
    '''Automatically rig mouth.'''
    
    def __init__(self):
        self.eyeLoc = None
        self.nameRig = "Mouth"
        self.upperLidVtx = None
        self.lowerLidVtx = None
        self.grpAllRig = None
        self.grpBaseRig = None
        self.hierarchyMainGrpCTRL = None
        self.hierarchyMainGrpJNT = None
        self.modulo = "Mouth_RIG_GRP"
        self.jaw_ctrl = "Jaw_Ctrl"
        self.loc_nose_jaw = "LOC_Nose"
        self.parent_rig = "Head_SQSLow_03_jnt"
        self.set_sliders = []
        self.locator_ctrls = ['R_CornerMouth_loc', 'R_UpperMouth_02_loc', 'R_UpperMouth_01_loc', 'UpperMouth_Center_loc', 'L_UpperMouth_01_loc', 'L_UpperMouth_02_loc',
                              'L_CornerMouth_loc', 'L_LowerMouth_02_loc', 'L_LowerMouth_01_loc', 'LowerMouth_Center_loc', 'R_LowerMouth_01_loc', 'R_LowerMouth_02_loc']
        self.upper_jnt = ['L_CornerMouth_jnt', 'L_UpperMouth_01_jnt', 'L_UpperMouth_02_jnt', 'UpperMouth_Center_jnt', 'R_UpperMouth_02_jnt', 'R_UpperMouth_01_jnt', 'R_CornerMouth_jnt']
        self.lower_jnt = ['L_CornerMouth_jnt', 'L_LowerMouth_01_jnt', 'L_LowerMouth_02_jnt', 'LowerMouth_Center_jnt', 'R_LowerMouth_02_jnt', 'R_LowerMouth_01_jnt', 'R_CornerMouth_jnt']

        # -----------------------------------------------------------------
        # Path
        # -----------------------------------------------------------------
        localpipe = os.getenv("LOCALPY")
        myPath = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "UEFace_RIG")
        script_path = os.path.join(myPath, "scripts")
        scene_path = os.path.join(myPath, "scenes", "controls_shape.ma")
        image_path = os.path.join(myPath, "icons")

        self.my_file_path = scene_path
        self.image_path = image_path


    # -----------------------------------------------------------------
    # Prerequisiti per i rig
    # -----------------------------------------------------------------  
    def placeEyeCenter(self, *args):
        '''Place locator in the center of the eyeball.
        
        Called by 'UI' function.
        Call functions: None '''
        
        selection = cmds.filterExpand(sm = 12) or cmds.filterExpand(sm = 22)
        
        if selection == None :
            self.eyeLoc = None
            cmds.error("Please select the mouth pivot.\n")
        else :
            eyeball = selection[0]
            name = self.nameRig
            
            eyeTemp = cmds.duplicate(eyeball) [0]
            cmds.xform(eyeTemp, cp = 1)
            self.eyeLoc = cmds.spaceLocator(n = (name + "_Center_locator")) [0]
            cnstrTemp = cmds.pointConstraint(eyeTemp, self.eyeLoc)
            cmds.delete(cnstrTemp)
            cmds.delete(eyeTemp)
            # # lock locator
            # cmds.setAttr(self.eyeLoc + ".overrideEnabled", 1)
            # cmds.setAttr(self.eyeLoc + ".overrideDisplayType", 2)
            
            cmds.select(cl = 1)

            # Update UI
            cmds.textField(self.txtfLoc, e = 1, tx = self.eyeLoc)
            cmds.button(self.btnPlaceCenter, e = 1, en = 0)
            cmds.button(self.btnUndoPlaceCenter, e = 1, en = 1)
    
    
    def placeEyeCenterUndo(self, *args):
        '''Undo 'placeEyeCenter' function.
        
        Called by 'UI' function.
        Call functions: None '''
        
        try :
            cmds.delete(self.eyeLoc)
        except ValueError :
            cmds.warning ("'" + self.eyeLoc + "'" + " doesn't exists.\n")
        finally :
            self.eyeLoc = None
            # self.nameRig = None
            cmds.textField(self.txtfLoc, e = 1, tx = "")
            cmds.button(self.btnPlaceCenter, e = 1, en = 1)
            cmds.button(self.btnUndoPlaceCenter, e = 1, en = 0)
    
    
    def upLidVtxSet(self, *args):
        '''List selected vertices as upper lip vertices.
        
        Called by 'UI' function.
        Call functions: None '''
        
        self.upperLidVtx = cmds.filterExpand(sm = 31)

        if self.upperLidVtx == None :
            cmds.scrollField(self.scrollfUpLid, e = 1, cl = 1)
            cmds.error("Please select vertices of on lip.\n")
        else :
            cmds.scrollField(self.scrollfUpLid, e = 1, cl = 1)
            for vtx in self.upperLidVtx :
                vtxNum = vtx.rpartition(".")[2] # <type 'unicode'>
                cmds.scrollField(self.scrollfUpLid, e = 1, it = (str(vtxNum) + " "))
        
        self.lowLidVtxSet()
    
    
    def lowLidVtxSet(self, *args):
        '''List selected vertices as lower lid vertices.
        
        Called by 'UI' function.
        Call functions: None '''
        
        self.lowerLidVtx = self.upperLidVtx
    

    # -----------------------------------------------------------------
    # Fix fatti allo script originale
    # -----------------------------------------------------------------
    def jntOrientZeroed(self, jnt, *args):
        '''Zeroed the Joint Orient of the joint child.
        
        Called by 'cv_base_curve_to_joint' function.
        Call functions: None '''

        for axis in ['X', 'Y', 'Z']:
            cmds.setAttr("{}.jointOrient{}".format(jnt, axis), 0)

    
    def fixOrientCtrls(self, loc, jnt, *args):
        '''Fix the orient of the joint controls.
        
        Called by 'createCrvCtrls' function.
        Call functions: None '''

        piv = cmds.xform(loc, q=True, piv=True, ws=True)
        cmds.select(clear=True)
        jntAim = cmds.joint(n="JNT_ToAim", p=piv[0:3])
        
        side = jnt.split("_")
        
        if len(side) == 3:
            if side[0] == "UpperMouth" or (side[0] == "L" and side[1] == "CornerMouth"):
                aim = cmds.aimConstraint(jntAim, jnt, weight = 1, aimVector = (0,0,-1), upVector = (0,1,0), worldUpType = "vector", worldUpVector = (0,1,0))
            elif side[0] == "LowerMouth":
                aim = cmds.aimConstraint(jntAim, jnt, weight = 1, aimVector = (0,0,1), upVector = (0,-1,0), worldUpType = "vector", worldUpVector = (0,1,0))
            elif side[0] == "R" and side[1] == "CornerMouth":
                aim = cmds.aimConstraint(jntAim, jnt, weight = 1, aimVector = (0,0,1), upVector = (0,1,0), worldUpType = "vector", worldUpVector = (0,1,0))

        else:
            if side[1] == "UpperMouth":
                aim = cmds.aimConstraint(jntAim, jnt, weight = 1, aimVector = (0,0,-1), upVector = (0,1,0), worldUpType = "vector", worldUpVector = (0,1,0))
            elif side[1] == "LowerMouth":
                aim = cmds.aimConstraint(jntAim, jnt, weight = 1, aimVector = (0,0,1), upVector = (0,-1,0), worldUpType = "vector", worldUpVector = (0,1,0))

        cmds.delete(aim)
        cmds.delete(jntAim)


    def makeCtrl(self, sel, name, *args):
        # Creates the controller object
        cmds.select(cl = 1)
        TEMP_CTRL1 = cmds.circle(r = 0.15, n=name)[0]
        TEMP_CTRL2 = cmds.duplicate()[0]
        cmds.setAttr(TEMP_CTRL2 + ".rotateY", 90)
        TEMP_CTRL3 = cmds.duplicate()[0]
        cmds.setAttr(TEMP_CTRL3 + ".rotateX", 90)
        cmds.parent(TEMP_CTRL2, TEMP_CTRL3, TEMP_CTRL1)
        cmds.makeIdentity(apply = 1, t = 1, r = 1, s = 1, n = 0, pn = 1)
        cmds.pickWalk(d = "down")
        cmds.select(TEMP_CTRL1, tgl = 1)
        cmds.parent(r = 1, s = 1)
        cmds.delete(TEMP_CTRL2, TEMP_CTRL3)
        cmds.select(cl = 1)

        return TEMP_CTRL1
        

    def createTransformOffsetGroup(self, sel, *args):
        '''Creates offset group for each item selected.
        
        Called by 'createCrvCtrls' function.
        Call functions: None '''
        
        for item in sel:
            parent_group = cmds.listRelatives(item,p=True);
            cmds.select(cl=True);
            offset_group = cmds.group(em=True, n="GRP_"+item+"_transform_offset")
            cmds.parent(offset_group, item)
            cmds.setAttr(offset_group+".tx",0)
            cmds.setAttr(offset_group+".ty",0)
            cmds.setAttr(offset_group+".tz",0)
            cmds.setAttr(offset_group+".rx",0)
            cmds.setAttr(offset_group+".ry",0)
            cmds.setAttr(offset_group+".rz",0)
            cmds.parent(offset_group, parent_group)
            cmds.parent(item, offset_group)


    # -----------------------------------------------------------------
    # Parte di costruzione del modulo mouth
    # -----------------------------------------------------------------
    def _ordinate_cv_base_curve(self, curva):
        """Ordina cv base curve per permettere il mirroring delle influenze dei joint."""

        last_ep = cmds.getAttr(f"{curva}.spans")
        pos_ep_start = cmds.xform(f"{curva}.ep[0]", q=True, ws=True, t=True)
        pos_ep_end = cmds.xform(f"{curva}.ep[{last_ep}]", q=True, ws=True, t=True)

        if pos_ep_start[0] < pos_ep_end[0]:
            base_curve = cmds.reverseCurve(curva, ch=False, rpo=1)
        else:
            base_curve = curva
            
        return base_curve


    def rename_joints_skin(self, joint_list, part):
        lista_jnt_rinominati = []

        half = len(joint_list) // 2
        
        left_jnts = joint_list[:half]
        centre_jnt = joint_list[half]
        right_jnts = joint_list[-1:half:-1]

        for idx, jnt in enumerate(left_jnts):
            L_jnt_renamed = cmds.rename(jnt, f"L_Mouth_{part}_Skin_{idx:02}_jnt")
            lista_jnt_rinominati.append(L_jnt_renamed)

        C_jnt_renamed = cmds.rename(centre_jnt, f"C_Mouth_{part}_Skin_jnt")
        lista_jnt_rinominati.append(C_jnt_renamed)

        tmp_r_list = []
        for idx, jnt in enumerate(right_jnts):
            R_jnt_renamed = cmds.rename(jnt, f"R_Mouth_{part}_Skin_{idx:02}_jnt")
            tmp_r_list.append(R_jnt_renamed)
        lista_jnt_ordinati = list(reversed(tmp_r_list))
        for jnt in lista_jnt_ordinati:
            lista_jnt_rinominati.append(jnt)

        return lista_jnt_rinominati


    def cv_base_curve_to_joint(self, eyeCenter, base_crv):
        """Creates one joint per cv of the base curve and parent it to the center of the eye.
        
        Called by 'buildRig' function.
        Call functions: 'jntOrientZeroed' """
        
        cmds.select(cl = 1)
    
        self.upLidJntList = []
        self.lowLidJntList = []
        
        # Organize rig hierarchy
        hierarchySecondGrp = cmds.group(n = (self.nameRig + "_Base_JNT_GRP"), em = 1)
        hierarchyUpGrp = cmds.group(n = (self.nameRig + "_Up_joints_GRP"), em = 1)
        hierarchyLowGrp = cmds.group(n = (self.nameRig + "_Low_joints_GRP"), em = 1)
        
        cmds.parent(hierarchyUpGrp, hierarchySecondGrp)
        cmds.parent(hierarchyLowGrp, hierarchySecondGrp)
        
        self.hierarchyMainGrpJNT = self.nameRig + "_JNT_GRP"
        
        if cmds.objExists("|" + self.hierarchyMainGrpJNT) :
            cmds.parent(hierarchySecondGrp, ("|" + self.hierarchyMainGrpJNT))
        else :
            cmds.group(n = self.hierarchyMainGrpJNT, em = 1)
            cmds.parent(hierarchySecondGrp, ("|" + self.hierarchyMainGrpJNT))

        cmds.setAttr(self.hierarchyMainGrpJNT + ".visibility", 0)
        
        # Calcola il numero di vertici nella curva
        numCVs = cmds.getAttr(base_crv + '.spans') + cmds.getAttr(base_crv + '.degree')
        
        # Upper mouth, per ogni cv crea un jnt
        for idx, cv in enumerate(range(numCVs)):
            cmds.select(cl = 1)
            jnt = cmds.joint(rad = 0.01, n = (self.nameRig + "_Up_Skin_%d_jnt" % idx))
            self.upLidJntList.append(jnt)
            position = cmds.xform("{}.cv[{}]".format(base_crv, cv), q = 1, ws = 1, t = 1)
            cmds.xform(jnt, ws = 1, t = position)
            centerPosition = cmds.xform(eyeCenter, q = 1, ws = 1, t = 1)
            cmds.select(cl = 1)
            centerJnt = cmds.joint(rad = 0.01, n = (self.nameRig + "_Up_Base_%d_jnt" % idx))
            cmds.xform(centerJnt, ws = 1, t = centerPosition)
            cmds.parent(jnt, centerJnt)
            cmds.joint(centerJnt, e = 1, oj = "xyz", secondaryAxisOrient = "yup", ch = 1, zso = 1)
            cmds.parent(centerJnt, hierarchyUpGrp)
            self.jntOrientZeroed(jnt) # reset joint orient
        
        # Lower mouth
        for idx, cv in enumerate(range(numCVs)):
            # if idx == 0 or idx == (numCVs - 1):
            #     pass
            # else:
            cmds.select(cl = 1)
            jnt = cmds.joint(rad = 0.01, n = (self.nameRig + "_Low_Skin_%d_jnt" % idx))
            self.lowLidJntList.append(jnt)
            position = cmds.xform("{}.cv[{}]".format(base_crv, cv), q = 1, ws = 1, t = 1)
            cmds.xform(jnt, ws = 1, t = position)
            centerPosition = cmds.xform(eyeCenter, q = 1, ws = 1, t = 1)
            cmds.select(cl = 1)
            centerJnt = cmds.joint(rad = 0.01, n = (self.nameRig + "_Low_Base_%d_jnt" % idx))
            cmds.xform(centerJnt, ws = 1, t = centerPosition)
            cmds.parent (jnt, centerJnt)
            cmds.joint(centerJnt, e = 1, oj = "xyz", secondaryAxisOrient = "yup", ch = 1, zso = 1)
            cmds.parent (centerJnt, hierarchyLowGrp)
            self.jntOrientZeroed(jnt) # reset joint orient
        
        
        # Creates a set containing the joints for skinning
        jntsForSkin = []
        jntsForSkin.extend(self.upLidJntList)
        jntsForSkin.extend(self.lowLidJntList)
        
        if not cmds.objExists("FACE_MODULES_Joints"):
            module_joints_set = cmds.sets(empty=True, name=("FACE_MODULES_Joints"))
        else:
            module_joints_set = "FACE_MODULES_Joints"

        # Controlla se il set della parte di rig dei joints esiste, altrimenti crealo
        if not cmds.objExists("HeadMouth_Joints"):
            rigpart_joints_set = cmds.sets(empty=True, name=("HeadMouth_Joints"))
            # Parenta il set della parte di rig dentro al set globale
            cmds.sets(rigpart_joints_set, edit=True, forceElement=module_joints_set)
        else:
            rigpart_joints_set = "HeadMouth_Joints"

        # Controlla se il set dei joints del modulo esiste, altrimenti crealo
        if not cmds.objExists("{}_jointsForSkin".format(self.nameRig)):
            setSkinJnts = cmds.sets(empty=True, name=("{}_jointsForSkin".format(self.nameRig)))
            # Parenta il set del modulo dentro al set globale
            cmds.sets(setSkinJnts, edit=True, forceElement=rigpart_joints_set)
        else:
            setSkinJnts = "{}_jointsForSkin".format(self.nameRig)

        for jnt in jntsForSkin:
            cmds.sets(jnt, edit=True, forceElement=setSkinJnts)


    def create_groups_for_locators(self, locator):
        
        master_grp_name = "{}_MASTER_Grp".format(locator)
        master_grp = cmds.group(n=master_grp_name, em=1)
        parentC_TEMP = cmds.parentConstraint(locator, master_grp)
        cmds.delete(parentC_TEMP)

        matrix_grp_name = master_grp_name.replace("MASTER", "MATRIX")
        matrix_grp = cmds.duplicate(master_grp, n = matrix_grp_name)[0]
        cmds.parent(locator, matrix_grp)
        cmds.parent(matrix_grp, master_grp)

        return master_grp, matrix_grp


    def place_rig_locators(self, nameRig, upLidJntList, lowLidJntList):
        '''Creates one locator per base curve cv and constrain each joint to it (aim).
        
        Called by 'buildRig' function.
        Call functions: None '''
        
        self.upLidLocList = []
        self.lowLidLocList = []
        
        # Organize rig hierarchy
        hierarchyThirdGrp = cmds.group(n = (nameRig + "_locator_GRP"), em = 1)
        hierarchyUpGrp = cmds.group(n = (nameRig + "_Up_locator_GRP"), em = 1)
        hierarchyLowGrp = cmds.group(n = (nameRig + "_Low_locator_GRP"), em = 1)
        
        cmds.parent(hierarchyUpGrp, hierarchyThirdGrp)
        cmds.parent(hierarchyLowGrp, hierarchyThirdGrp)
        
        self.grpBaseRig = (nameRig + "_Base_RIG_GRP")
        if cmds.objExists(nameRig + "_Base_RIG_GRP"):
            cmds.setAttr(self.grpBaseRig + ".visibility", 0)
            cmds.parent(hierarchyThirdGrp, self.grpBaseRig)
        else:
            self.grpBaseRig = cmds.group(n = (nameRig + "_Base_RIG_GRP"), em = 1)
            cmds.setAttr(self.grpBaseRig + ".visibility", 0)
            cmds.parent(hierarchyThirdGrp, self.grpBaseRig)
        
        self.grpAllRig = nameRig + "_RIG_GRP"
        if cmds.objExists(nameRig + "_RIG_GRP"):
            cmds.parent(self.grpBaseRig, self.grpAllRig)
            cmds.parent(self.hierarchyMainGrpJNT, self.grpAllRig)
        else:
            self.grpAllRig = cmds.group(n = (nameRig + "_RIG_GRP"), em = 1)
            cmds.parent(self.grpBaseRig, self.grpAllRig)
            cmds.parent(self.hierarchyMainGrpJNT, self.grpAllRig)
        
        # Imparenta tutto sotto 'Face_RIG_Grp'
        if cmds.objExists(self.grpAllRig) and cmds.objExists("Face_RIG_Grp"):
            cmds.parent(self.grpAllRig, "Face_RIG_Grp")

        # Upper mouth
        self.grp_upper_locator = []
        for upLidJnt in upLidJntList :
            locName = upLidJnt.replace("_Skin", "")
            locName = locName.replace("_jnt", "_loc")
            loc = cmds.spaceLocator(n = locName) [0]
            self.upLidLocList.append(loc)
            cmds.setAttr(loc + "Shape.localScaleX", 0.025)
            cmds.setAttr(loc + "Shape.localScaleY", 0.025)
            cmds.setAttr(loc + "Shape.localScaleZ", 0.025)
            positionLoc = cmds.xform(upLidJnt, q = 1, ws = 1, t = 1)
            cmds.xform(loc, ws = 1, t = positionLoc)
            grp_loc = self.create_groups_for_locators(loc)
            self.grp_upper_locator.append(grp_loc)
            parentJnt = cmds.listRelatives(upLidJnt, p = 1) [0]
            aim = cmds.aimConstraint(loc, parentJnt, weight = 1, aimVector = (1,0,0), upVector = (0,1,0), worldUpType = "vector", worldUpVector = (0,1,0))
            cmds.parent(grp_loc[0], hierarchyUpGrp)
            cmds.delete(aim)
            jntGrp = "{}_Up_joints_GRP".format(nameRig)
            cmds.parent(upLidJnt, jntGrp)
            cmds.delete(parentJnt)
            pac = cmds.parentConstraint(loc, upLidJnt, mo=True, w=1)
        
        # Lower mouth
        self.grp_lower_locator = []
        for lowLidJnt in lowLidJntList :
            locName = lowLidJnt.replace("_Skin", "")
            locName = locName.replace("_jnt", "_loc")
            loc = cmds.spaceLocator(n = locName) [0]
            self.lowLidLocList.append(loc)
            cmds.setAttr(loc + "Shape.localScaleX", 0.025)
            cmds.setAttr(loc + "Shape.localScaleY", 0.025)
            cmds.setAttr(loc + "Shape.localScaleZ", 0.025)
            positionLoc = cmds.xform(lowLidJnt, q = 1, ws = 1, t = 1)
            cmds.xform(loc, ws = 1, t = positionLoc)
            grp_loc = self.create_groups_for_locators(loc)
            self.grp_lower_locator.append(grp_loc)
            parentJnt = cmds.listRelatives(lowLidJnt, p = 1) [0]
            aim = cmds.aimConstraint(loc, parentJnt, weight = 1, aimVector = (1,0,0), upVector = (0,1,0), worldUpType = "vector", worldUpVector = (0,1,0))
            cmds.parent(grp_loc[0], hierarchyLowGrp)
            cmds.delete(aim)
            jntGrp = "{}_Low_joints_GRP".format(nameRig)
            cmds.parent(lowLidJnt, jntGrp)
            cmds.delete(parentJnt)
            pac = cmds.parentConstraint(loc, lowLidJnt, mo=True, w=1)


    def createMouthCrv(self, nameRig, upperLidVtx, lowerLidVtx):
        '''Creates nurbsCurve out of each lid vertices.
        
        Called by 'buildRig' function.
        Call functions: None '''
        
        cmds.select (cl = 1)

        # Organize rig hierarchy
        self.grpBaseRig = cmds.group(n = (nameRig + "_Base_RIG_GRP"), em = 1)
        self.hierarchyCrvGrp = cmds.group(n = (nameRig + "_curves_GRP"), em = 1)
        self.hierarchyUpCrvGrp = cmds.group(n = (nameRig + "_Up_curves_GRP"), em = 1)
        self.hierarchyLowCrvGrp = cmds.group(n = (nameRig + "_Low_curves_GRP"), em = 1)
        
        cmds.parent(self.hierarchyUpCrvGrp, self.hierarchyCrvGrp)
        cmds.parent(self.hierarchyLowCrvGrp, self.hierarchyCrvGrp)
        cmds.parent(self.hierarchyCrvGrp, self.grpBaseRig)
        
        # Upper mouth
        cmds.select(upperLidVtx)
        edgeUpLid = cmds.polyListComponentConversion(fv=1, te=1, internal=1)
        cmds.select(edgeUpLid)
        tempCrvUp = cmds.polyToCurve(form=0, degree=3, conformToSmoothMeshPreview=0)[0]
        cmds.delete(tempCrvUp, ch=1)
        up_base_crv = self._ordinate_cv_base_curve(tempCrvUp)
        upLidCrvName = nameRig + "_Up_BASE_curve"
        self.upLidCrv = cmds.rename(up_base_crv, upLidCrvName)
        cmds.parent(self.upLidCrv, self.hierarchyUpCrvGrp)
        
        # Lower mouth
        lowLidCrvName = nameRig + "_Low_BASE_curve"
        self.lowLidCrvTMP = cmds.duplicate(self.upLidCrv)
        self.lowLidCrv = cmds.rename(self.lowLidCrvTMP, lowLidCrvName)
        cmds.parent(self.lowLidCrv, self.hierarchyLowCrvGrp)


    def getDagPath(self, objectName):
        '''MARCO GIORDANO'S CODE (http://www.marcogiordanotd.com/)
        
        Called by 'getUParam' function.
        Call functions: None '''
    
        if isinstance(objectName, list)==True:
            oNodeList=[]
            for o in objectName:
                selectionList = OpenMaya.MSelectionList()
                selectionList.add(o)
                oNode = OpenMaya.MDagPath()
                selectionList.getDagPath(0, oNode)
                oNodeList.append(oNode)
            return oNodeList
        else:
            selectionList = OpenMaya.MSelectionList()
            selectionList.add(objectName)
            oNode = OpenMaya.MDagPath()
            selectionList.getDagPath(0, oNode)
            return oNode
        
    
    def getUParam(self, pnt = [], crv = None, *args):
        '''MARCO GIORDANO'S CODE (http://www.marcogiordanotd.com/) 
        
        Called by 'connectLocToCrv' function.
        Call functions: 'getDagPath' '''
        
        point = OpenMaya.MPoint(pnt[0],pnt[1],pnt[2])
        curveFn = OpenMaya.MFnNurbsCurve(self.getDagPath(crv))
        paramUtill=OpenMaya.MScriptUtil()
        paramPtr=paramUtill.asDoublePtr()
        isOnCurve = curveFn.isPointOnCurve(point)
        if isOnCurve == True:
            curveFn.getParamAtPoint(point , paramPtr,0.001,OpenMaya.MSpace.kObject )
        else :
            point = curveFn.closestPoint(point,paramPtr,0.001,OpenMaya.MSpace.kObject)
            curveFn.getParamAtPoint(point , paramPtr,0.001,OpenMaya.MSpace.kObject )
        
        param = paramUtill.getDouble(paramPtr)
        return param
    
    
    def connectLocToCrv(self, upLidLocList, upLidCrv, lowLidLocList, lowLidCrv):
        '''Connect locators to lid curves via pointOnCurveInfo nodes.
        
        Called by 'buildRig' function.
        Call functions: None '''
        
        # Upper lip master grp
        for upLidLoc in self.grp_upper_locator:
            position = cmds.xform(upLidLoc[0], q = 1, ws = 1, t = 1)
            u = self.getUParam (position, upLidCrv)
            name = upLidLoc[0].replace ("_locator", "_pointOnCurveInfo")
            ptOnCrvInfo = cmds.createNode("pointOnCurveInfo", n = name)
            cmds.connectAttr (upLidCrv + ".worldSpace", ptOnCrvInfo + ".inputCurve")
            cmds.setAttr (ptOnCrvInfo + ".parameter", u)
            cmds.connectAttr (ptOnCrvInfo + ".position", upLidLoc[0] + ".t")
        
        # Lower lip master grp
        for lowLidLoc in self.grp_lower_locator :
            position = cmds.xform (lowLidLoc[0], q = 1, ws = 1, t = 1)
            u = self.getUParam (position, lowLidCrv)
            name = lowLidLoc[0].replace ("_locator", "_pointOnCurveInfo")
            ptOnCrvInfo = cmds.createNode ("pointOnCurveInfo", n = name)
            cmds.connectAttr (lowLidCrv + ".worldSpace", ptOnCrvInfo + ".inputCurve")
            cmds.setAttr (ptOnCrvInfo + ".parameter", u)
            cmds.connectAttr (ptOnCrvInfo + ".position", lowLidLoc[0] + ".t")

    
    def mouthCorners(self, upLidEpCrvPos, lowLidEpCrvPos):
        '''Define eye corners position.
        
        Called by 'createDriverCrv' function.
        Call functions: None '''
        
        cornerUp1 = upLidEpCrvPos[0]
        cornerUp2 = upLidEpCrvPos[4]
        cornerLow1 = lowLidEpCrvPos[0]
        cornerLow2 = lowLidEpCrvPos[4]

        if cornerUp1[0] > cornerUp2[0]:
            self.cornerAPos = cornerUp1
            self.cornerBPos = cornerUp2
        else:
            self.cornerAPos = cornerUp2
            self.cornerBPos = cornerUp1

        return self.cornerAPos, self.cornerBPos
    
    
    def mouthCrvCVs(self, upLidCrv, lowLidCrv):
        '''List CVs of each guide curve.
        
        Called by 'createDriverCrv' function.
        Call functions: None '''
        
        upLidCVs = []
        x = 0
        while x < 7 :
            posCv = cmds.xform((upLidCrv + ".cv[%d]" % x), q = 1, ws = 1, t = 1)
            upLidCVs.append(posCv)
            x += 1
        
        lowLidCVs = []
        y = 0
        while y < 7 :
            posCv = cmds.xform((lowLidCrv + ".cv[%d]" % y), q = 1, ws = 1, t = 1)
            lowLidCVs.append(posCv)
            y += 1
        
        return upLidCVs, lowLidCVs
    
    
    def mouthMatchTopology(self, cornerAPos, cornerBPos, upLidCVsPos, lowLidCVsPos):
        '''Reorganise the CVs of each curve so they have the same topology.
        
        Called by 'createDriverCrv' function.
        Call functions: None '''
        
        # Order of CVs in base lists:           Order of CVs in ordered lists:
        # (upLidCVsPos, lowLidCVsPos)           (upLidCVsOrdered, lowLidCVsOrdered)
        # -----------------------               -------------------------
        # INDEX |       CV      |               | INDEX |       CV      |
        # ------|---------------|               |-------|---------------|
        #   0   | corner: ?     |               |   0   | corner A      |
        #   1   |               |               |   1   |               |
        #   2   |               |               |   2   |               |
        #   3   | middle of crv |               |   3   | middle of crv |
        #   4   |               |               |   4   |               |
        #   5   |               |               |   5   |               |
        #   6   | other corner  |               |   6   | corner B      |
        # -----------------------               -------------------------
        
        # - measure dist between first_CV of baseList and cornerAPos
        # - measure dist between first_CV of baseList and cornerBPos
        # - if CV is closer to cornerA append baseList to orderedList from beginning to end
        # - else (CV closer to cornerB) append baseList to orderedList from end to beginning
        # return orderedLists
        
        # distance formula is: d = sqrt((Ax-Bx)**2 + (Ay-By)**2 + (Az-Bz)**2)
        distTEMP1 = math.sqrt( ( ((upLidCVsPos [0])[0]) - cornerAPos[0] )**2 + ( ((upLidCVsPos [0])[1]) - cornerAPos[1] )**2 + ( ((upLidCVsPos [0])[2]) - cornerAPos[2] )**2 )
        distTEMP2 = math.sqrt( ( ((upLidCVsPos [0])[0]) - cornerBPos[0] )**2 + ( ((upLidCVsPos [0])[1]) - cornerBPos[1] )**2 + ( ((upLidCVsPos [0])[2]) - cornerBPos[2] )**2 )
        if distTEMP1 < distTEMP2 :
            upLidCVsOrdered = upLidCVsPos
        else:
            upLidCVsOrdered = upLidCVsPos[::-1] # reversed 'upLidCVsPos'
        
        distTEMP3 = math.sqrt( ( ((lowLidCVsPos [0])[0]) - cornerAPos[0] )**2 + ( ((lowLidCVsPos [0])[1]) - cornerAPos[1] )**2 + ( ((lowLidCVsPos [0])[2]) - cornerAPos[2] )**2 )
        distTEMP4 = math.sqrt( ( ((lowLidCVsPos [0])[0]) - cornerBPos[0] )**2 + ( ((lowLidCVsPos [0])[1]) - cornerBPos[1] )**2 + ( ((lowLidCVsPos [0])[2]) - cornerBPos[2] )**2 )
        if distTEMP3 < distTEMP4 :
            lowLidCVsOrdered = lowLidCVsPos
        else:
            lowLidCVsOrdered = lowLidCVsPos[::-1] # reversed 'lowLidCVsPos'

        return upLidCVsOrdered, lowLidCVsOrdered
    
    
    def createDriverCrv(self, upLidBaseCrv, upRigGrp, lowLidBaseCrv, lowRigGrp):
        '''Create a driver curve for each lid curve and connect it to the base curve with a wire deformer.
        
        Called by 'buildRig' function.
        Call functions: 'mouthCorners', 'mouthLeftAndRight' (unused), 'mouthCrvCVs', 'mouthMatchTopology' '''
        
        ## Upper eyelid ##
        upLidDriverCrvTEMP = cmds.duplicate(upLidBaseCrv)[0]
        cmds.delete(upLidDriverCrvTEMP, ch = 1) # delete history
        cmds.rebuildCurve(upLidDriverCrvTEMP, rpo = 1, end = 1, kr = 2, kcp = 0, kep = 1, kt = 0, s = 4, d = 7, tol = 0.01)

        # list the position of the EPs of the upper lid driver curve
        upLidEpPosTEMP = []
        x = 0
        while x < 5 :
            posEp = cmds.xform((upLidDriverCrvTEMP + ".ep[%d]" % x), q = 1, ws = 1, t = 1)
            upLidEpPosTEMP.append(posEp)
            x += 1
        cmds.delete(upLidDriverCrvTEMP)
        
        # Create the upLid 'guide' curve for corner placement and query CVs positions and indexes
        upLidGuideCrv = cmds.curve(d = 3, ep = (upLidEpPosTEMP[0], upLidEpPosTEMP[1], upLidEpPosTEMP[2], upLidEpPosTEMP[3], upLidEpPosTEMP[4]))
        
        ## Lower eyelid ##
        lowLidDriverCrvTEMP = cmds.duplicate(lowLidBaseCrv)[0]
        cmds.delete(lowLidDriverCrvTEMP, ch = 1) # delete history
        cmds.rebuildCurve(lowLidDriverCrvTEMP, rpo = 1, end = 1, kr = 2, kcp = 0, kep = 1, kt = 0, s = 4, d = 7, tol = 0.01)
        
        # list the position of the EPs of the lower lid driver curve
        lowLidEpPosTEMP = []
        x = 0
        while x < 5 :
            posEp = cmds.xform((lowLidDriverCrvTEMP + ".ep[%d]" % x), q = 1, ws = 1, t = 1)
            lowLidEpPosTEMP.append(posEp)
            x += 1
        cmds.delete(lowLidDriverCrvTEMP)
        
        # Create the lowLid 'guide' curve for corner placement and query CVs positions and indexes
        lowLidGuideCrv = cmds.curve(d = 3, ep = (lowLidEpPosTEMP[0], lowLidEpPosTEMP[1], lowLidEpPosTEMP[2], lowLidEpPosTEMP[3], lowLidEpPosTEMP[4]))
        
        ##
        
        # Find position of eye corners
        self.cornerAPos, self.cornerBPos = self.mouthCorners(upLidEpPosTEMP, lowLidEpPosTEMP)

        # List CVs positions of upLidGuideCrv and lowLidGuideCrv
        upLidCVsPos, lowLidCVsPos = self.mouthCrvCVs(upLidGuideCrv, lowLidGuideCrv)
        
        # List CVs positions in the right order (to match topology)
        upLidCVsOrdered, lowLidCVsOrdered = self.mouthMatchTopology(self.cornerAPos, self.cornerBPos, upLidCVsPos, lowLidCVsPos)

        
        ##
        
        # Create upper driver curve
        upCrvTEMP = cmds.curve(d = 3, p = (upLidCVsOrdered[0], upLidCVsOrdered[1], upLidCVsOrdered[2], upLidCVsOrdered[3], upLidCVsOrdered[4], upLidCVsOrdered[5], upLidCVsOrdered[6]))
        # cmds.insertKnotCurve(upCrvTEMP, ch=False, rpo=1, p=(1.5, 2.5), nk=1, ib=True)
        upLidDriverCrvName = upLidBaseCrv.replace("_BASE_", "_DRIVER_")
        self.upLidDriverCrv = cmds.rename(upCrvTEMP, upLidDriverCrvName)
        cmds.parent(self.upLidDriverCrv, upRigGrp)
        
        cmds.delete(upLidGuideCrv)
        
        # Create lower driver curve
        lowCrvTEMP = cmds.curve(d = 3, p = (lowLidCVsOrdered[0], lowLidCVsOrdered[1], lowLidCVsOrdered[2], lowLidCVsOrdered[3], lowLidCVsOrdered[4], lowLidCVsOrdered[5], lowLidCVsOrdered[6]))
        # cmds.insertKnotCurve(lowCrvTEMP, ch=False, rpo=1, p=(1.5, 2.5), nk=1, ib=True)
        lowLidDriverCrvName = lowLidBaseCrv.replace("_BASE_", "_DRIVER_")
        self.lowLidDriverCrv = cmds.rename(lowCrvTEMP, lowLidDriverCrvName)
        cmds.parent(self.lowLidDriverCrv, lowRigGrp)
        
        cmds.delete(lowLidGuideCrv)
        
        ##
        
        cmds.select(cl = 1)
        wireNodeUpLidName = upLidBaseCrv.replace("_BASE_curve", "_controlCurve_wire")
        wireUpLid = cmds.wire(upLidBaseCrv, n = wireNodeUpLidName, w = self.upLidDriverCrv, gw = 0, en = 1, ce = 0, li = 0, dds=[0, 1000])
        cmds.setAttr("{}.scale[0]".format(wireUpLid[0]), 0)
        
        cmds.select(cl = 1)
        wireNodeLowLidName = lowLidBaseCrv.replace("_BASE_curve", "_controlCurve_wire")
        wireLowLid = cmds.wire(lowLidBaseCrv, n = wireNodeLowLidName, w = self.lowLidDriverCrv, gw = 0, en = 1, ce = 0, li = 0, dds=[0, 1000])
        cmds.setAttr("{}.scale[0]".format(wireLowLid[0]), 0)
    
    
    def createJntCtrls(self, nameRig, cornerAPos, cornerBPos, upLidDriverCrv, lowLidDriverCrv, rigGrp):
        '''Creates controller joints for each point of the mouth driver curves.
        
        Called by 'buildRig' function.
        Call functions: None '''
        
        # Find position of EPs of each driver curve for joint placement
        upLidEpDriverCrvPos = []
        x = 0
        while x < 5 :
            posEp = cmds.xform((upLidDriverCrv + ".ep[%d]" % x), q = 1, ws = 1, t = 1)
            upLidEpDriverCrvPos.append(posEp)
            x += 1
        
        lowLidEpDriverCrvPos = []
        y = 0
        while y < 5 :
            posEp = cmds.xform((lowLidDriverCrv + ".ep[%d]" % y), q = 1, ws = 1, t = 1)
            lowLidEpDriverCrvPos.append(posEp)
            y += 1
        
        # Place controller joints
        self.nameMouth = ['R_CornerMouth', 'R_UpperMouth_02', 'R_UpperMouth_01', 'UpperMouth_Center', 'L_UpperMouth_01', 'L_UpperMouth_02',
                          'L_CornerMouth', 'L_LowerMouth_02', 'L_LowerMouth_01', 'LowerMouth_Center', 'R_LowerMouth_01', 'R_LowerMouth_02']
        self.ctrlJnts = []
        
        # UPDATE: added L_UpperMouth_02, R_UpperMouth_02, L_LowerMouth_02, R_LowerMouth_02
        idx = [1, 5]
        upLidEpDriverCrvPos_new = []
        lowLidEpDriverCrvPos_new = []

        for i in idx:
            posEp = cmds.xform(("{}_Up_DRIVER_curve.cv[{}]".format(nameRig, i)), q = 1, ws = 1, t = 1)
            lowLidEpDriverCrvPos_new.append(posEp)
        
        for i in idx:
            posEp = cmds.xform(("{}_Low_DRIVER_curve.cv[{}]".format(nameRig, i)), q = 1, ws = 1, t = 1)
            upLidEpDriverCrvPos_new.append(posEp)

        posList = [self.cornerBPos, upLidEpDriverCrvPos_new[1], upLidEpDriverCrvPos[3], upLidEpDriverCrvPos[2], upLidEpDriverCrvPos[1], upLidEpDriverCrvPos_new[0], 
                   self.cornerAPos, lowLidEpDriverCrvPos_new[0], lowLidEpDriverCrvPos[1], lowLidEpDriverCrvPos[2], lowLidEpDriverCrvPos[3], lowLidEpDriverCrvPos_new[1]]

        for item, (value, pos) in enumerate(zip(self.nameMouth, posList)):
            jnt = cmds.joint(rad=0.05, p=pos, n="{}_jnt".format(value))
            self.ctrlJnts.append(jnt)
            # fix orient jnt
            self.fixOrientCtrls(self.eyeLoc, jnt)

        # Organise rig hierarchy
        hierarchyCtrlJntGrp = cmds.group (n = (nameRig + "_CTRL_JNT_GRP"), em = 1)
        cmds.parent (hierarchyCtrlJntGrp, rigGrp)
        for jnt in self.ctrlJnts:
            cmds.parent (jnt, hierarchyCtrlJntGrp)


    def bindskin_ctrljnts_to_curve(self, upLidDriverCrv, lowLidDriverCrv, ctrlJnts):
        # Skin controllers joints to each driver curve
        kwargs = {
            'toSelectedBones': True,
            'bindMethod': 0,
            'normalizeWeights': 1,
            'weightDistribution': 0,
            'maximumInfluences': 1,
            'obeyMaxInfluences': True,
            'dropoffRate': 0.1,
            'removeUnusedInfluence': False
        }

        cmds.select(cl = 1)
        # Upper mouth
        upper_jnt = ["R_CornerMouth_jnt", "R_UpperMouth_02_jnt", "R_UpperMouth_01_jnt", "UpperMouth_Center_jnt", "L_UpperMouth_01_jnt", "L_UpperMouth_02_jnt", "L_CornerMouth_jnt"]  
        cmds.skinCluster(upper_jnt, upLidDriverCrv, n="SK_mouth_driver_upper", **kwargs)
        # editiamo le influenze
        history_upper = cmds.listHistory(upLidDriverCrv, pruneDagObjects=True)
        skinCluster_upper = cmds.ls(history_upper, type='skinCluster')[0]

        for idx, jnt in enumerate(reversed(upper_jnt)):
            cmds.skinPercent(skinCluster_upper, "{}.cv[{}]".format(upLidDriverCrv, idx), transformValue=[(jnt, 1.0)])

        # Lower mouth
        lower_jnt = ["R_CornerMouth_jnt", "R_LowerMouth_02_jnt", "R_LowerMouth_01_jnt", "LowerMouth_Center_jnt", "L_LowerMouth_01_jnt", "L_LowerMouth_02_jnt", "L_CornerMouth_jnt"]  
        cmds.skinCluster(lower_jnt, lowLidDriverCrv, n="SK_mouth_driver_lower", **kwargs)
        # editiamo le influenze
        history_lower = cmds.listHistory(lowLidDriverCrv, pruneDagObjects=True)
        skinCluster_lower = cmds.ls(history_lower, type='skinCluster')[0]
        for idx, jnt in enumerate(reversed((lower_jnt))):
            cmds.skinPercent(skinCluster_lower, "{}.cv[{}]".format(lowLidDriverCrv, idx), transformValue=[(jnt, 1.0)])
    
    
    def createCrvCtrls(self, nameRig, ctrlJnts):
        '''Creates controller curve for each controller joint.
        
        Called by 'buildRig' function.
        Call functions: None '''
        
        # Organize rig hierarchy
        hierarchySecondGrp = cmds.group (n="{}_Base_Ctrl_Grp".format(nameRig), em = 1)
        
        self.hierarchyMainGrpCTRL = "{}_CTRL_GRP".format(nameRig)
        
        if cmds.objExists ("|" + self.hierarchyMainGrpCTRL) :
            cmds.parent (hierarchySecondGrp, ("|" + self.hierarchyMainGrpCTRL))
        else :
            cmds.group (n = self.hierarchyMainGrpCTRL, em = 1)
            cmds.parent (hierarchySecondGrp, ("|" + self.hierarchyMainGrpCTRL))
        
        if cmds.objExists(nameRig + "_RIG_GRP"):
            cmds.parent(self.hierarchyMainGrpCTRL, self.grpAllRig)

        # Create the controls and constrain the joints
        self.ctrlList = []
        ctrlSecList = []
        self.master_grp_list = []
        self.matrix_grp_list = []

        for index, value in enumerate(self.nameMouth):
            if int(index) % 3 != 0:
                tmp = "{}_Ctrl".format(value)
                ctrlSecList.append(tmp)

        for jnt in ctrlJnts:
            ctrlName = jnt.replace("jnt", "Ctrl")
            ctrl = self.makeCtrl(jnt, ctrlName)
            self.ctrlList.append(ctrl)
            pointC_TEMP = cmds.pointConstraint(jnt, ctrl)
            cmds.delete(pointC_TEMP)

            # fix orient ctrl
            tokens = jnt.split("_")
            if tokens[1] == "CornerMouth":  
                if tokens[0] == "R":
                    loc_rotate = cmds.xform("R_CornerMouth_loc", q=True, ws=True, ro=True)
                    cmds.xform(ctrl, ws=True, ro=loc_rotate)
                elif tokens[0] == "L":
                    loc_rotate = cmds.xform("L_CornerMouth_loc", q=True, ws=True, ro=True)
                    cmds.xform(ctrl, ws=True, ro=loc_rotate)

            else:
                self.fixOrientCtrls(self.eyeLoc, ctrl)
            
            master_grp_name = "{}_MASTER_Grp".format(ctrlName)
            master_grp = cmds.group(n=master_grp_name, em=1)
            parentC_TEMP = cmds.parentConstraint(ctrl, master_grp)
            cmds.delete(parentC_TEMP)
            self.master_grp_list.append(master_grp)

            matrix_grp_name = master_grp_name.replace("MASTER", "MATRIX")
            matrix_grp = cmds.duplicate(master_grp, n = matrix_grp_name)
            cmds.parent(ctrl, matrix_grp)
            cmds.parent(matrix_grp, master_grp)
            cmds.parent(master_grp, hierarchySecondGrp)
            cmds.parentConstraint(ctrl, jnt)
            self.matrix_grp_list.append(matrix_grp[0])

        cmds.select (cl = 1)
        
        # Crea gruppi SQUARE per i controlli_02 (nella lista self.ctrlList sono idx: 1, 5, 7, 11)
        self.square_grps = [cmds.group(ctrl, n="{}_SQUARE_Grp".format(ctrl)) for idx, ctrl in enumerate(self.ctrlList) if idx in [1, 5, 7, 11]]
        
        # Lock and hide unused channels
        for ctrl in self.ctrlList :
            # cmds.setAttr ((ctrl + ".rx"), lock = 1, keyable = 0, channelBox = 0)
            # cmds.setAttr ((ctrl + ".ry"), lock = 1, keyable = 0, channelBox = 0)
            # cmds.setAttr ((ctrl + ".rz"), lock = 1, keyable = 0, channelBox = 0)
            cmds.setAttr ((ctrl + ".sx"), lock = 1, keyable = 0, channelBox = 0)
            cmds.setAttr ((ctrl + ".sy"), lock = 1, keyable = 0, channelBox = 0)
            cmds.setAttr ((ctrl + ".sz"), lock = 1, keyable = 0, channelBox = 0)
            cmds.setAttr ((ctrl + ".v"), lock = 1, keyable = 0, channelBox = 0)

        # Controlla se il set contenente i controlli esiste, altrimenti crealo
        if not cmds.objExists("FACE_MODULES_Controls"):
            module_controls_set = cmds.sets(empty=True, name=("FACE_MODULES_Controls"))
        else:
            module_controls_set = "FACE_MODULES_Controls"

        # Controlla se il set della parte di rig dei joints esiste, altrimenti crealo
        if not cmds.objExists("HeadMouth_controls"):
            rigpart_controls_set = cmds.sets(empty=True, name=("HeadMouth_controls"))
            # Parenta il set della parte di rig dentro al set globale
            cmds.sets(rigpart_controls_set, edit=True, forceElement=module_controls_set)
        else:
            rigpart_controls_set = "HeadMouth_controls"

        # Controlla se il set dei controlli del modulo esiste, altrimenti crealo
        if not cmds.objExists("{}_controls".format(self.nameRig)):
            self.set_mouth_ctrls = cmds.sets(empty=True, name=("{}_controls".format(self.nameRig)))
            # Parenta il set del modulo dentro al set rigpart
            cmds.sets(self.set_mouth_ctrls, edit=True, forceElement=rigpart_controls_set)
        else:
            self.set_mouth_ctrls = "{}_controls".format(self.nameRig)

        for ctrl in self.ctrlList:
            cmds.sets(ctrl, edit=True, forceElement=self.set_mouth_ctrls)
    
    
    def add_master_ctrl(self, nameRig, ctrlList, master_grp_list, matrix_grp_list):
        
        self.mouth_master_ctrls = []

        # seleziono gruppo matrix agli idx 3 e 9
        for idx, (grp_master, grp_offset, ctrl) in enumerate(zip(master_grp_list, matrix_grp_list, ctrlList)):
            if idx == 3 or idx == 9:
                
                # creo il controllo master all'origine degli assi
                ctrl_master_name = ctrl.replace("Center", "Master")
                ctrl_master = cmds.circle(n=ctrl_master_name, c=[0,0,0], nr=[0,1,0], sw=360, r=2, d=3, ut=0, tol=0.01, s=8, ch=0)[0]
                self.mouth_master_ctrls.append(ctrl_master)
                
                # creo il relativo gruppo MATRIX
                grp_matrix_name = "{}_MATRIX_Grp".format(ctrl_master_name)
                grp_matrix = cmds.group(name = grp_matrix_name, em=True)
                
                # parento il controllo al gruppo e faccio il matching delle coordinate col gruppo del controllo Center                
                cmds.parent(ctrl_master, grp_matrix)
                cmds.matchTransform(grp_matrix, grp_offset)

                # rendo il gruppo MATRIX figlio del gruppo master e il gruppo offset del center figlio del controllo Master
                cmds.parent(grp_matrix, grp_master)
                cmds.parent(grp_offset, ctrl_master)

                # rinomino gruppi offest del ctrl center
                new_name_offset_grp = grp_offset.replace("MATRIX", "offset")
                cmds.rename(grp_offset, new_name_offset_grp)

                # rinomino gruppo MASTER da Center a Master
                new_name_master_grp = grp_master.replace("Center", "Master")
                cmds.rename(grp_master, new_name_master_grp)

            if idx == 3:
                # creo gruppi FOLLOW e COMPENSATE
                grp_follow = cmds.group(ctrl_master, n="{}_FOLLOW_Grp".format(ctrl_master))
                grp_compensate = cmds.group(grp_follow, n="{}_COMPENSATE_Grp".format(ctrl_master))

        # Lock and hide unused channels
        for ctrl in self.mouth_master_ctrls:
            # cmds.setAttr ((ctrl + ".rx"), lock = 1, keyable = 0, channelBox = 0)
            # cmds.setAttr ((ctrl + ".ry"), lock = 1, keyable = 0, channelBox = 0)
            # cmds.setAttr ((ctrl + ".rz"), lock = 1, keyable = 0, channelBox = 0)
            cmds.setAttr ((ctrl + ".sx"), lock = 1, keyable = 0, channelBox = 0)
            cmds.setAttr ((ctrl + ".sy"), lock = 1, keyable = 0, channelBox = 0)
            cmds.setAttr ((ctrl + ".sz"), lock = 1, keyable = 0, channelBox = 0)
            cmds.setAttr ((ctrl + ".v"), lock = 1, keyable = 0, channelBox = 0)

        # Aggiungi controlli al set contenente i controlli creati
        if cmds.objExists(self.set_mouth_ctrls) and cmds.sets(self.set_mouth_ctrls, q=True, size=True):
            cmds.sets(self.mouth_master_ctrls, e = 1, forceElement = self.set_mouth_ctrls)  
    

    def addSmartClose(self, nameRig, upLidBaseCrv, upLidDriverCrv, lowLidBaseCrv, lowLidDriverCrv, rigGrp, upCrvRigGrp, lowCrvRigGrp, mouth_master_ctrls):
        '''Add a 'smart close' feature to the eyelid rig, allowing to close wherever the controllers are (blendshapes + wire deformers system).
        
        Called by 'buildRig' function.
        Call functions: None '''
        
        # Variables names containing 'SB' = smartClose
        
        up_master_ctrl = mouth_master_ctrls[0]
        low_master_ctrl = mouth_master_ctrls[1]

        # - STEP 1:
        bothLidsSB_Crv = cmds.duplicate(upLidDriverCrv, n = (nameRig + "_Mouth_smartClose_curve"))[0]
        cmds.parent(bothLidsSB_Crv, rigGrp)
        self.bothLidsSB_BlndShp = cmds.blendShape(upLidDriverCrv, lowLidDriverCrv, bothLidsSB_Crv, n = (nameRig + "_Mouth_smartClose_BLENDSHAPE"))[0] # duplicare 7 volte ???
        cmds.select(cl = 1)
        # crea attributo SmartClose 
        cmds.select(up_master_ctrl, low_master_ctrl)
        cmds.addAttr(ln = "SmartClose", at = "float", min = 0, max = 1, k = 1)
        cmds.select(cl = 1)
        # crea attributo SmartCloseHeight
        cmds.select(up_master_ctrl)
        cmds.addAttr(ln = "SmartCloseHeight", at = "float", min = 0, max = 1, defaultValue = 0.5, k = 1)
        cmds.connectAttr((up_master_ctrl + ".SmartCloseHeight"), (self.bothLidsSB_BlndShp + "." + upLidDriverCrv), f = 1)
        SBReverse = cmds.shadingNode("reverse", asUtility = 1, n = (nameRig + "_Mouth_smartClose_reverse"))
        cmds.connectAttr((up_master_ctrl + ".SmartCloseHeight"), (SBReverse + ".inputX"), f = 1)
        cmds.connectAttr((SBReverse + ".outputX"), (self.bothLidsSB_BlndShp + "." + lowLidDriverCrv), f = 1)
        cmds.select(cl = 1)
        # crea attributo JawFollow
        cmds.select(up_master_ctrl)
        cmds.addAttr(ln = "JawFollow", at = "float", min = 0, max = 1, defaultValue = 0.5, k = 1)

        # STEP 2:
        upLidSB_Crv = cmds.duplicate(self.upLidCrv, n = (nameRig + "_Up_smartClose_curve")) [0]
        lowLidSB_Crv = cmds.duplicate(self.lowLidCrv, n = (nameRig + "_Low_smartClose_curve")) [0]
        cmds.setAttr((up_master_ctrl + ".SmartCloseHeight"), 1)
        cmds.select(cl = 1)
        wireUpLid = cmds.wire(upLidSB_Crv, n = (nameRig + "_Up_smartClose_wire"), w = bothLidsSB_Crv, gw = 0, en = 1, ce = 0, li = 0, dds=[0, 1000])
        cmds.setAttr("{}.scale[0]".format(wireUpLid[0]), 0)
        cmds.setAttr((up_master_ctrl + ".SmartCloseHeight"), 0)
        cmds.select(cl = 1)
        wireLowLid = cmds.wire(lowLidSB_Crv, n = (nameRig + "_Low_smartClose_wire"), w = bothLidsSB_Crv, gw = 0, en = 1, ce = 0, li = 0, dds=[0, 1000])
        cmds.setAttr("{}.scale[0]".format(wireLowLid[0]), 0)
        
        # STEP 3:
        self.upLidSB_BlndShp = cmds.blendShape(upLidSB_Crv, self.upLidCrv, n = (nameRig + "_Up_smartClose_BLENDSHAPE"))[0]
        self.lowLidSB_BlndShp = cmds.blendShape(lowLidSB_Crv, self.lowLidCrv, n = (nameRig + "_Low_smartClose_BLENDSHAPE"))[0]
        
        # FINAL STEP:
        cmds.connectAttr((up_master_ctrl + ".SmartClose"), (self.upLidSB_BlndShp + "." + upLidSB_Crv), f = 1)
        cmds.connectAttr((low_master_ctrl + ".SmartClose"), (self.lowLidSB_BlndShp + "." + lowLidSB_Crv), f = 1)
        cmds.setAttr((up_master_ctrl + ".SmartCloseHeight"), 0.50)
    

    def copyWeightToBs(self, curva_skinnata, joints_interessati, blendshape_target=None):
        # Trova lo skin cluster associato alla curva
        history = cmds.listHistory(curva_skinnata, pruneDagObjects=True)
        skinClusters = cmds.ls(history, type='skinCluster')

        if not skinClusters:
            cmds.error("Skin cluster non trovato per la curva data.")

        skinCluster = skinClusters[0]

        # Verifica se i joints specificati sono effettivamente nello skin cluster
        joints = cmds.skinCluster(skinCluster, q=True, inf=True)
        for joint in joints_interessati:
            if joint not in joints:
                cmds.error("Joint " + joint + " non trovato nello skin cluster.")

        # Calcola il numero di vertici nella curva
        numCVs = cmds.getAttr(curva_skinnata + '.spans') + cmds.getAttr(curva_skinnata + '.degree')

        # Calcola la somma dei pesi per ogni vertice
        somma_pesi = [0] * numCVs
        for i in range(numCVs):
            for joint in joints_interessati:
                peso = cmds.skinPercent(skinCluster, curva_skinnata + ".cv[" + str(i) + "]", query=True, transform=joint)
                somma_pesi[i] += peso

        # FIX per smoothare i valori nei cv dispari
        for idx, value in enumerate(somma_pesi):
            if idx == 11:
                pass
            elif idx % 2 == 1:
                somma_pesi[idx] = (somma_pesi[idx-1] + somma_pesi[idx+1]) / 2
            else:
                pass

        if not blendshape_target:
            pass
        else:
            # Applica i pesi sommati al nodo di BlendShape
            for i, peso in enumerate(somma_pesi):
                cmds.setAttr(blendshape_target + ".inputTarget[0].inputTargetGroup[0].targetWeights[" + str(i) + "]", peso)

        return somma_pesi


    def addStickyLips(self, nameRig, bs_node):
        ctrlCorners = [self.ctrlList[0], self.ctrlList[6]]
        srNodeList = []

        # create attribute
        for ctrl in ctrlCorners:
            cmds.addAttr(ctrl, ln="Sticky", at="float", min=0, max=5, k=True)
            # cmds.addAttr(ctrl, ln="NarrowWide", at="float", min=-10, max=10, defaultValue=0, k=True)
        
        # create 2 setRange per lato
        for lip, ctrl in zip(['R', 'L'], ctrlCorners):
            for side in ['Up', 'Low']:
                node1 = cmds.createNode('setRange', n="SR_StickyLips_{}_{}_A".format(lip, side))
                node2 = cmds.createNode('setRange', n="SR_StickyLips_{}_{}_B".format(lip, side))
                # set attributes node1
                for attr in ['X', 'Y', 'Z']:
                    cmds.setAttr("{}.max{}".format(node1, attr), 1)
                for attr, value in zip(['X', 'Y', 'Z'], [0, 1, 2]):
                    cmds.setAttr("{}.oldMin{}".format(node1, attr), value)
                for attr, value in zip(['X', 'Y', 'Z'], [1, 2, 3]):
                    cmds.setAttr("{}.oldMax{}".format(node1, attr), value)
                srNodeList.append(node1)

                # set attributes node2
                for attr in ['X', 'Y', 'Z']:
                    cmds.setAttr("{}.max{}".format(node2, attr), 1)
                for attr, value in zip(['X', 'Y'], [3, 4]):
                    cmds.setAttr("{}.oldMin{}".format(node2, attr), value)
                for attr, value in zip(['X', 'Y'], [4, 5]):
                    cmds.setAttr("{}.oldMax{}".format(node2, attr), value)
                srNodeList.append(node2)
                
                # connect ctrls to SetRange nodes
                for attr in ['X', 'Y', 'Z']:
                    cmds.connectAttr("{}.Sticky".format(ctrl), "{}.value{}".format(node1, attr), f=True)
                    cmds.connectAttr("{}.Sticky".format(ctrl), "{}.value{}".format(node2, attr), f=True)

        # passaggio per le blendshapes
        bs_node_up = []
        bs_node_low = []
        bs_node_up_set = []
        bs_node_low_set = []
        node_name_up = ['BS_Sticky_Up_R_01','BS_Sticky_Up_L_01','BS_Sticky_Up_R_02','BS_Sticky_Up_L_02','BS_Sticky_Up_R_03','BS_Sticky_Up_L_03', 'BS_Sticky_Up_R_04','BS_Sticky_Up_L_04', 'BS_Sticky_Up_R_05','BS_Sticky_Up_L_05'] 
        node_name_low = ['BS_Sticky_Low_R_01','BS_Sticky_Low_L_01','BS_Sticky_Low_R_02','BS_Sticky_Low_L_02','BS_Sticky_Low_R_03','BS_Sticky_Low_L_03', 'BS_Sticky_Low_R_04','BS_Sticky_Low_L_04', 'BS_Sticky_Low_R_05', 'BS_Sticky_Low_L_05']
        jnts_lists = []

        jnt_01_L_up = ['Mouth_Up_DRIVER_SK_curve_0_jnt', 'Mouth_Up_DRIVER_SK_curve_1_jnt']
        jnt_02_L_up = ['Mouth_Up_DRIVER_SK_curve_0_jnt', 'Mouth_Up_DRIVER_SK_curve_1_jnt', 'Mouth_Up_DRIVER_SK_curve_2_jnt']
        jnt_03_L_up = ['Mouth_Up_DRIVER_SK_curve_0_jnt', 'Mouth_Up_DRIVER_SK_curve_1_jnt', 'Mouth_Up_DRIVER_SK_curve_2_jnt', 'Mouth_Up_DRIVER_SK_curve_3_jnt']
        jnt_04_L_up = ['Mouth_Up_DRIVER_SK_curve_0_jnt', 'Mouth_Up_DRIVER_SK_curve_1_jnt', 'Mouth_Up_DRIVER_SK_curve_2_jnt', 'Mouth_Up_DRIVER_SK_curve_3_jnt', 'Mouth_Up_DRIVER_SK_curve_4_jnt']
        jnt_05_L_up = ['Mouth_Up_DRIVER_SK_curve_0_jnt', 'Mouth_Up_DRIVER_SK_curve_1_jnt', 'Mouth_Up_DRIVER_SK_curve_2_jnt', 'Mouth_Up_DRIVER_SK_curve_3_jnt', 'Mouth_Up_DRIVER_SK_curve_4_jnt', 'Mouth_Up_DRIVER_SK_curve_5_jnt']
        jnt_01_R_up = ['Mouth_Up_DRIVER_SK_curve_10_jnt', 'Mouth_Up_DRIVER_SK_curve_9_jnt']
        jnt_02_R_up = ['Mouth_Up_DRIVER_SK_curve_10_jnt', 'Mouth_Up_DRIVER_SK_curve_9_jnt', 'Mouth_Up_DRIVER_SK_curve_8_jnt']
        jnt_03_R_up = ['Mouth_Up_DRIVER_SK_curve_10_jnt', 'Mouth_Up_DRIVER_SK_curve_9_jnt', 'Mouth_Up_DRIVER_SK_curve_8_jnt', 'Mouth_Up_DRIVER_SK_curve_7_jnt']
        jnt_04_R_up = ['Mouth_Up_DRIVER_SK_curve_10_jnt', 'Mouth_Up_DRIVER_SK_curve_9_jnt', 'Mouth_Up_DRIVER_SK_curve_8_jnt', 'Mouth_Up_DRIVER_SK_curve_7_jnt', 'Mouth_Up_DRIVER_SK_curve_6_jnt']
        jnt_05_R_up = ['Mouth_Up_DRIVER_SK_curve_10_jnt', 'Mouth_Up_DRIVER_SK_curve_9_jnt', 'Mouth_Up_DRIVER_SK_curve_8_jnt', 'Mouth_Up_DRIVER_SK_curve_7_jnt', 'Mouth_Up_DRIVER_SK_curve_6_jnt', 'Mouth_Up_DRIVER_SK_curve_5_jnt']
        jnt_up = [jnt_01_R_up, jnt_01_L_up, jnt_02_R_up, jnt_02_L_up, jnt_03_R_up, jnt_03_L_up, jnt_04_R_up, jnt_04_L_up, jnt_05_R_up, jnt_05_L_up]        

        jnt_01_L_low = ['Mouth_Low_DRIVER_SK_curve_0_jnt', 'Mouth_Low_DRIVER_SK_curve_1_jnt']
        jnt_02_L_low = ['Mouth_Low_DRIVER_SK_curve_0_jnt', 'Mouth_Low_DRIVER_SK_curve_1_jnt', 'Mouth_Low_DRIVER_SK_curve_2_jnt']
        jnt_03_L_low = ['Mouth_Low_DRIVER_SK_curve_0_jnt', 'Mouth_Low_DRIVER_SK_curve_1_jnt', 'Mouth_Low_DRIVER_SK_curve_2_jnt', 'Mouth_Low_DRIVER_SK_curve_3_jnt']
        jnt_04_L_low = ['Mouth_Low_DRIVER_SK_curve_0_jnt', 'Mouth_Low_DRIVER_SK_curve_1_jnt', 'Mouth_Low_DRIVER_SK_curve_2_jnt', 'Mouth_Low_DRIVER_SK_curve_3_jnt', 'Mouth_Low_DRIVER_SK_curve_4_jnt']
        jnt_05_L_low = ['Mouth_Low_DRIVER_SK_curve_0_jnt', 'Mouth_Low_DRIVER_SK_curve_1_jnt', 'Mouth_Low_DRIVER_SK_curve_2_jnt', 'Mouth_Low_DRIVER_SK_curve_3_jnt', 'Mouth_Low_DRIVER_SK_curve_4_jnt', 'Mouth_Low_DRIVER_SK_curve_5_jnt']
        jnt_01_R_low = ['Mouth_Low_DRIVER_SK_curve_10_jnt', 'Mouth_Low_DRIVER_SK_curve_9_jnt']
        jnt_02_R_low = ['Mouth_Low_DRIVER_SK_curve_10_jnt', 'Mouth_Low_DRIVER_SK_curve_9_jnt', 'Mouth_Low_DRIVER_SK_curve_8_jnt']
        jnt_03_R_low = ['Mouth_Low_DRIVER_SK_curve_10_jnt', 'Mouth_Low_DRIVER_SK_curve_9_jnt', 'Mouth_Low_DRIVER_SK_curve_8_jnt', 'Mouth_Low_DRIVER_SK_curve_7_jnt']
        jnt_04_R_low = ['Mouth_Low_DRIVER_SK_curve_10_jnt', 'Mouth_Low_DRIVER_SK_curve_9_jnt', 'Mouth_Low_DRIVER_SK_curve_8_jnt', 'Mouth_Low_DRIVER_SK_curve_7_jnt', 'Mouth_Low_DRIVER_SK_curve_6_jnt']
        jnt_05_R_low = ['Mouth_Low_DRIVER_SK_curve_10_jnt', 'Mouth_Low_DRIVER_SK_curve_9_jnt', 'Mouth_Low_DRIVER_SK_curve_8_jnt', 'Mouth_Low_DRIVER_SK_curve_7_jnt', 'Mouth_Low_DRIVER_SK_curve_6_jnt', 'Mouth_Low_DRIVER_SK_curve_5_jnt']
        jnt_low = [jnt_01_R_low, jnt_01_L_low, jnt_02_R_low, jnt_02_L_low, jnt_03_R_low, jnt_03_L_low, jnt_04_R_low, jnt_04_L_low, jnt_05_R_low, jnt_05_L_low]        

        # aggiungo passaggio in cui skinno copia curva BASE e copio le pesature della driver, e usare questa curva per il passaggio successivo del copyWeightToBs
        for crv_base, crv_driver in zip(['Mouth_Up_BASE_curve', 'Mouth_Low_BASE_curve'], ['Mouth_Up_DRIVER_curve', 'Mouth_Low_DRIVER_curve']):
            crv_base_sk = cmds.duplicate(crv_base, n=crv_base.replace("BASE", "BASE_SK"))[0]

            # rebuild curva driver con 11 cv
            crv_driver_copy = cmds.duplicate(crv_driver, n=crv_driver.replace("DRIVER", "DRIVER_SK"))[0]
            cmds.rebuildCurve(crv_driver_copy, ch=True, rpo=1, rt=0, end=1, kr=2, kcp=0, kep=1, kt=0, s=10, d=3, tol=0.01)
            cmds.delete("{}.cv[11]".format(crv_driver_copy))
            cmds.delete("{}.cv[1]".format(crv_driver_copy))

            # Ottieni i CV della curva
            cvs = cmds.ls(crv_driver_copy + '.cv[*]', fl=True)

            # Crea un joint per ogni CV
            jnt_new_list = []
            for idx, cv in enumerate(cvs):
                pos = cmds.pointPosition(cv)
                jnt = cmds.joint(position=pos, n="{}_{}_jnt".format(crv_driver_copy, idx))
                jnt_new_list.append(jnt)
                cmds.select(cl=True)
            # skinnare curva
            cmds.skinCluster(jnt_new_list, crv_driver_copy)
            jnts_lists.append(jnt_new_list)

            cmds.setAttr("{}.visibility".format(crv_base_sk), 0)
            influenceList = cmds.skinCluster(crv_driver_copy, q=1, inf=1)
            
            # bindskin baseSK e copy da drive
            kwargs = {
                'toSelectedBones': True,
                'bindMethod': 0,
                'normalizeWeights': 1,
                'weightDistribution': 0,
                'maximumInfluences': 1,
                'obeyMaxInfluences': True,
                'dropoffRate': 4,
                'removeUnusedInfluence': False
            }

            # name_sk = "SK_{}".format(crv_base_sk)
            cmds.skinCluster(influenceList, crv_base_sk, **kwargs)

            history_from = cmds.listHistory(crv_driver_copy, pruneDagObjects=True)
            skinCluster_from = cmds.ls(history_from, type='skinCluster')[0]
           
            history_to = cmds.listHistory(crv_base_sk, pruneDagObjects=True)
            skinCluster_to = cmds.ls(history_to, type='skinCluster')[0]

            cmds.copySkinWeights(ss=skinCluster_from, ds=skinCluster_to, smooth=True, noMirror=True, sa="closestPoint",  ia=["closestJoint", "oneToOne", "label"])

        # duplica la curva driver 10 volte (5 sx, 5 dx) e falle entrare in blendshape sulla curva base, e passa le pesature dello sk dalla curva driver alle curve bs
        for crv_driver, nodeList, jnts_list, crv_base_sk in zip(['Mouth_Up_DRIVER_curve', 'Mouth_Low_DRIVER_curve'], [node_name_up, node_name_low], [jnt_up, jnt_low], ['Mouth_Up_BASE_SK_curve', 'Mouth_Low_BASE_SK_curve']):
            for idx, (bs_node, jnts) in enumerate(zip(nodeList, jnts_list)):
                crv_base = crv_driver.replace('DRIVER', 'BASE')
                crv_bs = cmds.duplicate(crv_base, n="CRV_{}".format(bs_node))[0]    
                cmds.setAttr("{}.visibility".format(crv_bs), 0)
                nodeBS = cmds.blendShape(crv_bs, crv_base, name=bs_node)
                weightCvs = self.copyWeightToBs(crv_base_sk , jnts, bs_node)
        
        # make connection
        # SR_StickyLips_R_Up
        for nodeBs, attr in zip([node_name_up[0], node_name_up[2], node_name_up[4]], ['X', 'Y', 'Z']):
            cmds.connectAttr("{}.outValue{}".format(srNodeList[0], attr), "{}.CRV_{}".format(nodeBs, nodeBs), f=True)
            cmds.connectAttr("{}_Up_smartClose_curveShape.worldSpace[0]".format(nameRig), "{}.inputTarget[0].inputTargetGroup[0].inputTargetItem[6000].inputGeomTarget".format(nodeBs), f=True)
        for nodeBs, attr in zip([node_name_up[6], node_name_up[8]], ['X', 'Y']):
            cmds.connectAttr("{}.outValue{}".format(srNodeList[1], attr), "{}.CRV_{}".format(nodeBs, nodeBs), f=True)
            cmds.connectAttr("{}_Up_smartClose_curveShape.worldSpace[0]".format(nameRig), "{}.inputTarget[0].inputTargetGroup[0].inputTargetItem[6000].inputGeomTarget".format(nodeBs), f=True)

        # SR_StickyLips_R_Low  
        for nodeBs, attr in zip([node_name_low[0], node_name_low[2], node_name_low[4]], ['X', 'Y', 'Z']):
            cmds.connectAttr("{}.outValue{}".format(srNodeList[2], attr), "{}.CRV_{}".format(nodeBs, nodeBs), f=True)
            cmds.connectAttr("{}_Low_smartClose_curveShape.worldSpace[0]".format(nameRig), "{}.inputTarget[0].inputTargetGroup[0].inputTargetItem[6000].inputGeomTarget".format(nodeBs), f=True)
        for nodeBs, attr in zip([node_name_low[6], node_name_low[8]], ['X', 'Y']):
            cmds.connectAttr("{}.outValue{}".format(srNodeList[3], attr), "{}.CRV_{}".format(nodeBs, nodeBs), f=True)
            cmds.connectAttr("{}_Low_smartClose_curveShape.worldSpace[0]".format(nameRig), "{}.inputTarget[0].inputTargetGroup[0].inputTargetItem[6000].inputGeomTarget".format(nodeBs), f=True)

        # SR_StickyLips_L_Up
        for nodeBs, attr in zip([node_name_up[1], node_name_up[3], node_name_up[5]], ['X', 'Y', 'Z']):
            cmds.connectAttr("{}.outValue{}".format(srNodeList[4], attr), "{}.CRV_{}".format(nodeBs, nodeBs), f=True)
            cmds.connectAttr("{}_Up_smartClose_curveShape.worldSpace[0]".format(nameRig), "{}.inputTarget[0].inputTargetGroup[0].inputTargetItem[6000].inputGeomTarget".format(nodeBs), f=True)
        for nodeBs, attr in zip([node_name_up[7], node_name_up[9]], ['X', 'Y']):
            cmds.connectAttr("{}.outValue{}".format(srNodeList[5], attr), "{}.CRV_{}".format(nodeBs, nodeBs), f=True)
            cmds.connectAttr("{}_Up_smartClose_curveShape.worldSpace[0]".format(nameRig), "{}.inputTarget[0].inputTargetGroup[0].inputTargetItem[6000].inputGeomTarget".format(nodeBs), f=True)

        # SR_StickyLips_L_Low  
        for nodeBs, attr in zip([node_name_low[1], node_name_low[3], node_name_low[5]], ['X', 'Y', 'Z']):
            cmds.connectAttr("{}.outValue{}".format(srNodeList[6], attr), "{}.CRV_{}".format(nodeBs, nodeBs), f=True)
            cmds.connectAttr("{}_Low_smartClose_curveShape.worldSpace[0]".format(nameRig), "{}.inputTarget[0].inputTargetGroup[0].inputTargetItem[6000].inputGeomTarget".format(nodeBs), f=True)
        for nodeBs, attr in zip([node_name_low[7], node_name_low[9]], ['X', 'Y']):
            cmds.connectAttr("{}.outValue{}".format(srNodeList[7], attr), "{}.CRV_{}".format(nodeBs, nodeBs), f=True)
            cmds.connectAttr("{}_Low_smartClose_curveShape.worldSpace[0]".format(nameRig), "{}.inputTarget[0].inputTargetGroup[0].inputTargetItem[6000].inputGeomTarget".format(nodeBs), f=True)

        # Add BASE curves to the BS sets
        for i in bs_node_up_set:
            cmds.sets("Mouth_Up_BASE_curve", include=i)
        for i in bs_node_low_set:
            cmds.sets("Mouth_Low_BASE_curve", include=i)

        # Reorder deformer
        for crv, node in zip(["Mouth_Up_BASE_curve", "Mouth_Low_BASE_curve"], ['BS_UpperMouth_curve_Sticky', 'BS_LowerMouth_curve_Sticky']):
            bs_node = []
            info = cmds.listHistory(crv, pruneDagObjects=True, interestLevel=1)

            for i in info:
                check = cmds.objectType(i)
                if check == 'blendShape':
                    bs_node.append(i)
            cmds.reorderDeformers(bs_node[0], bs_node[-1], crv)
            cmds.reorderDeformers(bs_node[1], bs_node[0], crv)
            
            # rename original bs node
            info = cmds.listHistory(crv, pruneDagObjects=True, interestLevel=1)
            cmds.rename(info[0], node)

        # delete the tmp joints created for the new curve
        for jnt in jnts_lists:
            cmds.delete(jnt)
    

    def replace_and_remove_shapes(self):
        # lista contenente i controlli da sostituire
        control_pairs = [
                ('L_CornerMouth_Ctrl_REF', 'L_CornerMouth_Ctrl'),
                ('L_UpperMouth_02_Ctrl_REF', 'L_UpperMouth_02_Ctrl'),
                ('L_UpperMouth_01_Ctrl_REF', 'L_UpperMouth_01_Ctrl'),
                ('UpperMouth_Center_Ctrl_REF', 'UpperMouth_Center_Ctrl'),
                ('R_UpperMouth_01_Ctrl_REF', 'R_UpperMouth_01_Ctrl'),
                ('R_UpperMouth_02_Ctrl_REF', 'R_UpperMouth_02_Ctrl'),
                ('R_CornerMouth_Ctrl_REF', 'R_CornerMouth_Ctrl'),
                ('R_LowerMouth_02_Ctrl_REF', 'R_LowerMouth_02_Ctrl'),
                ('R_LowerMouth_01_Ctrl_REF', 'R_LowerMouth_01_Ctrl'),
                ('LowerMouth_Center_Ctrl_REF', 'LowerMouth_Center_Ctrl'),
                ('L_LowerMouth_01_Ctrl_REF', 'L_LowerMouth_01_Ctrl'),
                ('L_LowerMouth_02_Ctrl_REF', 'L_LowerMouth_02_Ctrl'),
                ('UpperMouth_Master_Ctrl_REF', 'UpperMouth_Master_Ctrl'),
                ('LowerMouth_Master_Ctrl_REF', 'LowerMouth_Master_Ctrl')
            ]

        # importa scena con i controlli
        cmds.file(self.my_file_path, i=True, mergeNamespacesOnClash=True, namespace=':', gr=True, gn="GRP_shapes_REF")

        # itera attraverso le coppie di controlli e esegui la sostituzione
        for source_ctrl, target_ctrl in control_pairs:
            try:
                # Ottieni e elimina le shape esistenti nel controllo di destinazione
                target_shapes = cmds.listRelatives(target_ctrl, shapes=True)
                name_target_shape = target_shapes
                if target_shapes:
                    cmds.delete(target_shapes)

                # Ottieni la shape del controllo sorgente
                new_source_ctrl = cmds.duplicate(source_ctrl, n="{}_NEW".format(source_ctrl))
                source_shapes = cmds.listRelatives(source_ctrl, shapes=True)
                if source_shapes:
                    for source_shape in source_shapes:
                        cmds.parent(source_shape, target_ctrl, r=True, shape=True)
                        cmds.rename(source_shape, name_target_shape[0])
            except:
                pass

        # elimina il gruppo dei controlli importati
        grp_del = "GRP_shapes_REF"
        cmds.delete("GRP_shapes_REF")
    

    def _make_aim_locator(self, loc_center, grp_loc):
        '''Fix the orient of the joint controls.
        
        Called by 'createCrvCtrls' function.
        Call functions: None '''

        piv = cmds.xform(loc_center, q=True, piv=True, ws=True)
        cmds.select(clear=True)
        # jntAim = cmds.joint(n="JNT_ToAim", p=piv[0:3])
        
        tokens = grp_loc.split("_")
        
        if len(tokens) == 4:
            if tokens[0] == "UpperMouth" or (tokens[0] == "L" and tokens[1] == "CornerMouth"):
                aim = cmds.aimConstraint (loc_center, grp_loc, weight = 1, aimVector = (0,0,-1), upVector = (0,1,0), worldUpType = "vector", worldUpVector = (0,1,0))
            elif tokens[0] == "LowerMouth":
                aim = cmds.aimConstraint (loc_center, grp_loc, weight = 1, aimVector = (0,0,1), upVector = (0,-1,0), worldUpType = "vector", worldUpVector = (0,1,0))
            elif tokens[0] == "R" and tokens[1] == "CornerMouth":
                aim = cmds.aimConstraint (loc_center, grp_loc, weight = 1, aimVector = (0,0,1), upVector = (0,1,0), worldUpType = "vector", worldUpVector = (0,1,0))

        else:
            if tokens[1] == "UpperMouth":
                aim = cmds.aimConstraint (loc_center, grp_loc, weight = 1, aimVector = (0,0,-1), upVector = (0,1,0), worldUpType = "vector", worldUpVector = (0,1,0))
            elif tokens[1] == "LowerMouth":
                aim = cmds.aimConstraint (loc_center, grp_loc, weight = 1, aimVector = (0,0,1), upVector = (0,-1,0), worldUpType = "vector", worldUpVector = (0,1,0))

        # cmds.delete(aim)
        # cmds.delete(jntAim)


    def create_locator_ctrl(self, ctrlJnts, locator_ctrls):
        """ Crea i locator per posizionare i controlli."""
        loc_list = []
        grp_list = []

        for jnt, loc in zip(ctrlJnts, locator_ctrls):
            # crea locator rispetto al ctrl jnt
            pos = cmds.xform(jnt, q=True, ws=True, t=True)
            rot = cmds.xform(jnt, q=True, ws=True, ro=True)
            locator = cmds.spaceLocator(name=loc)[0]
            grp_loc = cmds.group(name="{}_Grp".format(loc), empty=True)
            cmds.matchTransform(grp_loc, locator)
            cmds.parent(locator, grp_loc)
            cmds.setAttr("{}.translate".format(grp_loc), pos[0], pos[1], pos[2])
            loc_list.append(locator)
            grp_list.append(grp_loc)
            
            # cambia size
            for attr in ['X', 'Y', 'Z']:
                cmds.setAttr("{}.localScale{}".format(locator, attr), 0.25)

            # nascondi attributi rotate
            for attr in ("rx", "ry", "rz"):
                cmds.setAttr("{}.{}".format(locator, attr), keyable=False, channelBox=False)

            # nascondi e locka attributi scale-visibility
            for attr in ("sx", "sy", "sz", "v"):
                cmds.setAttr("{}.{}".format(locator, attr), lock=True, keyable=False, channelBox=False)
                        
            cmds.select(clear=True)

        # crea gruppi per i locator
        self.loc_grp = cmds.group(n="Ctrl_Locators_Grp", em=True)
        upper_loc_grp = cmds.group(n="Upper_Ctrl_Locators_Grp", em=True)
        lower_loc_grp = cmds.group(n="Lower_Ctrl_Locators_Grp", em=True)
        corner_loc_grp = cmds.group(n="Corner_Ctrl_Locators_Grp", em=True)
        
        # mettili in gerarchia i gruppi
        cmds.parent(upper_loc_grp, self.loc_grp)
        cmds.parent(lower_loc_grp, self.loc_grp)
        cmds.parent(corner_loc_grp, self.loc_grp)
        
        # parenta i locator ai rispettivi gruppi
        for idx, (loc, grp) in enumerate(zip(loc_list, grp_list)):
            if idx in [0,6]:
                cmds.parent(grp, corner_loc_grp)
                cmds.setAttr("{}.overrideEnabled".format(loc), 1)
                cmds.setAttr("{}.overrideColor".format(loc), 18)
                for attr in ['rx', 'ry', 'rz']:
                    cmds.setAttr("{}.{}".format(loc, attr), lock=False)
                    cmds.setAttr("{}.{}".format(loc, attr), channelBox=True)
            elif idx in [1,2,3,4,5]:
                cmds.parent(grp, upper_loc_grp)
                cmds.setAttr("{}.overrideEnabled".format(loc), 1)
                cmds.setAttr("{}.overrideColor".format(loc), 17)
            elif idx in [7,8,9,10,11]:
                cmds.parent(grp, lower_loc_grp)
                cmds.setAttr("{}.overrideEnabled".format(loc), 1)
                cmds.setAttr("{}.overrideColor".format(loc), 6)
    
        # posiziona i locator spostando i gruppi padre
        cmds.setAttr("{}.translateY".format(upper_loc_grp), 0.25)
        cmds.setAttr("{}.translateZ".format(upper_loc_grp), 0.75)
        cmds.setAttr("{}.translateY".format(lower_loc_grp), -0.25)
        cmds.setAttr("{}.translateZ".format(lower_loc_grp), 0.50)

        # crea nodi multiply e crea connessioni per mirrorare locator di destra
        for idx, loc in enumerate(locator_ctrls):
            if idx == 0:
                mlp_node_translate = cmds.createNode("multiplyDivide", n="MLP_{}_mirror_translate".format(loc))
                mlp_node_rotate = cmds.createNode("multiplyDivide", n="MLP_{}_mirror_rotate".format(loc))
                cmds.setAttr("{}.input2Z".format(mlp_node_translate), -1)
                cmds.setAttr("{}.input2X".format(mlp_node_rotate), -1)
                cmds.setAttr("{}.input2Y".format(mlp_node_rotate), -1)

                left_loc = loc.replace("R_", "L_") 
                cmds.connectAttr("{}.translate".format(left_loc), "{}.input1".format(mlp_node_translate), f=True)
                cmds.connectAttr("{}.output".format(mlp_node_translate), "{}.translate".format(loc), f=True)
                
                cmds.connectAttr("{}.rotate".format(left_loc), "{}.input1".format(mlp_node_rotate), f=True)
                cmds.connectAttr("{}.output".format(mlp_node_rotate), "{}.rotate".format(loc), f=True)

            if idx in [1, 2, 10, 11]:
                mlp_node_translate = cmds.createNode("multiplyDivide", n="MLP_{}_mirror_translate".format(loc))
                mlp_node_rotate = cmds.createNode("multiplyDivide", n="MLP_{}_mirror_rotate".format(loc))
                cmds.setAttr("{}.input2X".format(mlp_node_translate), -1)
                cmds.setAttr("{}.input2Y".format(mlp_node_rotate), -1)
                cmds.setAttr("{}.input2Z".format(mlp_node_rotate), -1)

                left_loc = loc.replace("R_", "L_") 
                cmds.connectAttr("{}.translate".format(left_loc), "{}.input1".format(mlp_node_translate), f=True)
                cmds.connectAttr("{}.output".format(mlp_node_translate), "{}.translate".format(loc), f=True)
                
                cmds.connectAttr("{}.rotate".format(left_loc), "{}.input1".format(mlp_node_rotate), f=True)
                cmds.connectAttr("{}.output".format(mlp_node_rotate), "{}.rotate".format(loc), f=True)

        # crea aim contraint tra mouth_center_locator e i locator di sinistra/centrali
        for grp_loc in grp_list:
            self._make_aim_locator(self.eyeLoc, grp_loc)

        cmds.select(clear=True)


    def check_locators(self, locator_ctrls):
        for locator in locator_ctrls:
            if not cmds.objExists(locator):
                cmds.error("Non trovo {}. Risolvi il problema e rilancia il 'Finalize Rig'.".format(locator))


    def match_joints_to_locators(self, locator_ctrls, ctrlJnts):
        for idx, (jnt, loc) in enumerate(zip(ctrlJnts, locator_ctrls)):
            # Ottieni la posizione del locator
            loc_position = cmds.xform(loc, q=True, ws=True, t=True)
            
            # Matcha la posizione del joint al locator
            cmds.xform(jnt, ws=True, t=loc_position)

    
    # -----------------------------------------------------------------
    # Parte di connessioni per i vari attributi dei controlli mouth
    # -----------------------------------------------------------------
    def jaw_ctrl_square_attribute(self, jaw_ctrl, square_grp_list):
        """Crea il sistema dello 'Square' sui controlli '02' guidato dal Jaw_Ctrl."""
        # creo nodo di condition
        cnd_node = cmds.createNode("condition", name="CND_Compensate_Jaw")
        cmds.setAttr("{}.operation".format(cnd_node), 3) # 3 -> greater or equal

        # creo nodi di multiplyDivide
        mlp_jaw_node = cmds.createNode("multiplyDivide", name="MLP_Jaw_Square")
        mlp_compensate_node = cmds.createNode("multiplyDivide", name="MLP_Compensate_Square")
        cmds.setAttr("{}.input2X".format(mlp_compensate_node), 0.002)
        cmds.setAttr("{}.input2Y".format(mlp_compensate_node), -0.002)
        
        # Inserisci nodo nel set SLIDERS
        self.set_sliders.append(mlp_compensate_node)

        # connessioni varie
        cmds.connectAttr("{}.square".format(jaw_ctrl), "{}.colorIfTrueR".format(cnd_node), f=True)
        cmds.connectAttr("{}.rotateX".format(jaw_ctrl), "{}.firstTerm".format(cnd_node), f=True)

        for attr in ['X', 'Y', 'Z']:
            cmds.connectAttr("{}.rotateX".format(jaw_ctrl), "{}.input1{}".format(mlp_jaw_node, attr), f=True)

        for attr in ['X', 'Y', 'Z']:
            cmds.connectAttr("{}.outColorR".format(cnd_node), "{}.input2{}".format(mlp_jaw_node, attr), f=True)

        cmds.connectAttr("{}.output".format(mlp_jaw_node), "{}.input1".format(mlp_compensate_node), f=True)

        for grp in square_grp_list:
            cmds.connectAttr("{}.outputX".format(mlp_compensate_node), "{}.translateY".format(grp), f=True)



    def uppermouth_master_ctrl_jaw_follow_attribute(self, loc_nose):
        """Crea il sistema del 'Jaw Follow' presente sul controllo 'UpperMouth_Master_Ctrl'."""
        
        # Crea locator LOC_UpperMouth_Jaw, matchalo a UpperMouth_Center_loc e mettilo in gerarchia
        loc_uppermouth_jaw = cmds.spaceLocator(name="LOC_UpperMouth_Jaw")[0]
        cmds.setAttr("{}.visibility".format(loc_uppermouth_jaw), 0)
        loc_pos = cmds.xform("UpperMouth_Center_jnt", q=True, ws=True, t=True)
        cmds.xform(loc_uppermouth_jaw, ws=True, t=loc_pos)
        cmds.parent(loc_uppermouth_jaw, "Mouth_locator_GRP")
        cmds.parentConstraint("Jaw_jnt", loc_uppermouth_jaw, mo=True, w=1, st=["x", "z"], sr=["x", "y", "z"])

        # Crea nodo distance e connetti in input i 2 locator
        self.dist_node_shape = cmds.createNode("distanceDimShape", name="DIS_Nose_JawShape")
        cmds.connectAttr("{}Shape.worldPosition[0]".format(loc_uppermouth_jaw), "{}.startPoint".format(self.dist_node_shape), f=True)
        cmds.connectAttr("{}Shape.worldPosition[0]".format(loc_nose), "{}.endPoint".format(self.dist_node_shape), f=True)

        # Metti dist_node_transform in gerarchia
        dist_node_transform = cmds.listRelatives(self.dist_node_shape, allParents=True)[0]
        nose_utility_grp = cmds.group(dist_node_transform, n="Nose_Utilities_Grp")
        cmds.setAttr("{}.visibility".format(nose_utility_grp), 0)
        cmds.parent(nose_utility_grp, "Face_RIG_Grp")

        # Crea nodo setRange e connetti in input il self.dist_node_shape
        sr_node = cmds.createNode('setRange', n="SR_Nose_Jaw")
        for attr in ['X', 'Y', 'Z']:
            cmds.connectAttr("{}.distance".format(self.dist_node_shape), "{}.value{}".format(sr_node, attr), f=True)

        # Setta i valori per oldMin e oldMax tramite multiplyDivide
        dist_value = cmds.getAttr("{}.distance".format(self.dist_node_shape))
        md_oldMin = cmds.createNode('multiplyDivide', n="MD_SR_Nose_Jaw_oldMin")
        md_oldMax = cmds.createNode('multiplyDivide', n="MD_SR_Nose_Jaw_oldMax")

        # Connetti l'attributo masterScale al posto della scala dell'head joint
        for attr in ['X', 'Y', 'Z']:
            cmds.setAttr(f"{md_oldMin}.input1{attr}", dist_value)
            cmds.setAttr(f"{md_oldMax}.input1{attr}", dist_value * 10)

            cmds.connectAttr(f"Head_jnt.masterScale", f"{md_oldMin}.input2{attr}", f=True)
            cmds.connectAttr(f"Head_jnt.masterScale", f"{md_oldMax}.input2{attr}", f=True)

            cmds.connectAttr(f"{md_oldMin}.output{attr}", f"{sr_node}.oldMin{attr}", f=True)
            cmds.connectAttr(f"{md_oldMax}.output{attr}", f"{sr_node}.oldMax{attr}", f=True)

        # Ripristina i valori originali per min e max direttamente nel setRange
        cmds.setAttr(f"{sr_node}.maxX", dist_value * 5)
        cmds.setAttr(f"{sr_node}.maxZ", 1)
        cmds.setAttr(f"{sr_node}.minX", 0)
        cmds.setAttr(f"{sr_node}.minY", 0)
        cmds.setAttr(f"{sr_node}.minZ", 0)

        # Crea i nodi di multiplyDivide per il Jaw Follow
        mlp_nose = cmds.createNode('multiplyDivide', n="MLP_Nose_Jaw")
        cmds.connectAttr(f"{sr_node}.outValue", f"{mlp_nose}.input1", f=True)
        cmds.setAttr(f"{mlp_nose}.input2X", -1)

        mlp_uppermouth = cmds.createNode('multiplyDivide', n="MLP_UpperMouth_Jaw")
        cmds.connectAttr(f"{mlp_nose}.output", f"{mlp_uppermouth}.input1", f=True)
        for attr in ['X', 'Y', 'Z']:
            cmds.connectAttr(f"UpperMouth_Master_Ctrl.JawFollow", f"{mlp_uppermouth}.input2{attr}", f=True)

        # Connetti mlp_uppermouth al gruppo UpperMouth_Master_Ctrl_FOLLOW_Grp
        cmds.connectAttr(f"{mlp_uppermouth}.outputX", "UpperMouth_Master_Ctrl_FOLLOW_Grp.translateY", f=True)



    def uppermouth_master_ctrl_nose_compensate_attribute(self, loc_nose):
        """Crea il sistema di compensazione per il controllo 'UpperMouth_Master_Ctrl' basato sul movimento del naso."""
        
        # Crea nodo setRange e connetti in input il self.dist_node_shape
        sr_node = cmds.createNode('setRange', n="SR_Nose_UpperMouth")
        for attr in ['X', 'Y', 'Z']:
            cmds.connectAttr("{}.distance".format(self.dist_node_shape), "{}.value{}".format(sr_node, attr), f=True)

        # Setta i valori per oldMin e oldMax tramite multiplyDivide
        dist_value = cmds.getAttr("{}.distance".format(self.dist_node_shape))
        md_oldMin = cmds.createNode('multiplyDivide', n="MD_SR_Nose_UpperMouth_oldMin")
        md_oldMax = cmds.createNode('multiplyDivide', n="MD_SR_Nose_UpperMouth_oldMax")

        for attr in ['X', 'Y', 'Z']:
            cmds.setAttr(f"{md_oldMin}.input1{attr}", dist_value / 20)
            cmds.setAttr(f"{md_oldMax}.input1{attr}", dist_value)

            cmds.connectAttr(f"Head_jnt.scale{attr}", f"{md_oldMin}.input2{attr}", f=True)
            cmds.connectAttr(f"Head_jnt.scale{attr}", f"{md_oldMax}.input2{attr}", f=True)

            cmds.connectAttr(f"{md_oldMin}.output{attr}", f"{sr_node}.oldMin{attr}", f=True)
            cmds.connectAttr(f"{md_oldMax}.output{attr}", f"{sr_node}.oldMax{attr}", f=True)

        # Ripristina i valori originali per min e max direttamente nel setRange
        cmds.setAttr(f"{sr_node}.minX", 1.56)
        cmds.setAttr(f"{sr_node}.minY", -0.06)
        cmds.setAttr(f"{sr_node}.minZ", 10)

        # Crea i nodi di multiplyDivide per il compensatore
        mlp_nose = cmds.createNode('multiplyDivide', n="MLP_Nose_UpperMouth")
        cmds.connectAttr(f"{sr_node}.outValue", f"{mlp_nose}.input1", f=True)
        cmds.setAttr(f"{mlp_nose}.input2X", 1.13)
        cmds.setAttr(f"{mlp_nose}.input2Y", 0.24)
        cmds.setAttr(f"{mlp_nose}.input2Z", 1)

        mlp_uppermouth = cmds.createNode('multiplyDivide', n="MLP_UpperMouth_Jaw_Compensate")
        self.set_sliders.append(mlp_uppermouth)

        cmds.connectAttr(f"{mlp_nose}.output", f"{mlp_uppermouth}.input1", f=True)
        
        # Connetti mlp_uppermouth al gruppo UpperMouth_Master_Ctrl_COMPENSATE_Grp
        cmds.connectAttr(f"{mlp_uppermouth}.outputX", "UpperMouth_Master_Ctrl_COMPENSATE_Grp.translateY", f=True)
        cmds.connectAttr(f"{mlp_uppermouth}.outputY", "UpperMouth_Master_Ctrl_COMPENSATE_Grp.translateZ", f=True)



    # -----------------------------------------------------------------
    # Sistema per creare matrici che guidano in rotate i locator/joint che skinnano la mesh
    # -----------------------------------------------------------------
    def driver_locator(self, upLidDriverCrv, lowLidDriverCrv):
        """Crea un locator per ogni cv della curva driver."""
        num_cv_upper = cmds.getAttr(upLidDriverCrv + '.spans') + cmds.getAttr(upLidDriverCrv + '.degree')
        num_cv_lower = cmds.getAttr(lowLidDriverCrv + '.spans') + cmds.getAttr(lowLidDriverCrv + '.degree')

        self.loc_upper_list = []
        self.loc_lower_list = []

        for cv in range(num_cv_upper):
            pos = cmds.xform("{}.cv[{}]".format(upLidDriverCrv, cv), q = 1, ws = 1, t = 1)
            loc = cmds.spaceLocator(n="LOC_{}_{}".format(upLidDriverCrv, cv))
            cmds.xform(loc, ws = 1, t = pos)
            self.loc_upper_list.append(loc)

        for cv in range(num_cv_lower):
            pos = cmds.xform("{}.cv[{}]".format(lowLidDriverCrv, cv), q = 1, ws = 1, t = 1)
            loc = cmds.spaceLocator(n="LOC_{}_{}".format(lowLidDriverCrv, cv))
            cmds.xform(loc, ws = 1, t = pos)
            self.loc_lower_list.append(loc)


    def distanza_da_cv_a_locator(self, curva_nome, lista_locator):
        def distanza_euclidea(p1, p2):
            return math.sqrt(sum((a - b) ** 2 for a, b in zip(p1, p2)))

        def trova_locator_vicini(cv_index, locator):
            cv_position = cmds.pointPosition(curva_nome + '.cv[' + str(cv_index) + ']')
            locator_vicini = sorted(locator, key=lambda x: distanza_euclidea(cv_position, cmds.pointPosition(x)))
            return locator_vicini[0], locator_vicini[1]

        # Calcola il numero totale di CV sulla curva
        num_cv = cmds.getAttr(curva_nome + '.spans') + cmds.getAttr(curva_nome + '.degree')

        # Calcola la distanza per ogni CV
        lista_selezione = []
        lista_distanze = []

        for cv in range(num_cv):  # Include tutti i CV
            pos_cv = cmds.pointPosition(curva_nome + '.cv[' + str(cv) + ']')
            locator1, locator2 = trova_locator_vicini(cv, lista_locator)

            # Calcola le distanze
            pos_locator1 = cmds.pointPosition(locator1)
            pos_locator2 = cmds.pointPosition(locator2)

            dist_locator1 = distanza_euclidea(pos_cv, pos_locator1)
            dist_locator2 = distanza_euclidea(pos_cv, pos_locator2)
            
            somma_distanze = dist_locator1 + dist_locator2

            dist_normalizzata1 = round(dist_locator1 / somma_distanze, 2)
            dist_normalizzata2 = round(1 - dist_normalizzata1, 2)

            # Crea lista selezione
            lista_selezione_tmp = [[locator1[0], "{}.cv[{}]".format(curva_nome, cv)], [locator2[0], "{}.cv[{}]".format(curva_nome, cv)]]

            # Crea lista valori
            x = round(dist_normalizzata1, 1)
            y = round(dist_normalizzata2, 1)
            lista_distanze_tmp = [(x, x, x), (y, y, y)]    

            lista_selezione.append(lista_selezione_tmp)
            lista_distanze.append(lista_distanze_tmp)

        return lista_selezione, lista_distanze   

 
    def unlock_rotate_on_mouth_controls(self, nameRig):
        
        # otteniamo self.loc_upper_list e self.loc_lower_list
        self.driver_locator(self.upLidDriverCrv, self.lowLidDriverCrv)

        # trova distanza cv/locator
        self.lista_selezione_upper, self.lista_valori_upper = self.distanza_da_cv_a_locator(self.upLidCrv, self.loc_upper_list)
        self.lista_selezione_lower, self.lista_valori_lower = self.distanza_da_cv_a_locator(self.lowLidCrv, self.loc_lower_list)

    
    def trasforma_liste(self, lista_info_cv, lista_gruppi, curva):
        lista_target = []
        lista_valori = []

        numCVs = cmds.getAttr(curva + '.spans') + cmds.getAttr(curva + '.degree')

        ckeck_jnts_name = [["LOC_Mouth_Low_DRIVER_curve_0", "L_CornerMouth_jnt"],
                           ["LOC_Mouth_Low_DRIVER_curve_1", "L_LowerMouth_02_jnt"],
                           ["LOC_Mouth_Low_DRIVER_curve_2", "L_LowerMouth_01_jnt"],
                           ["LOC_Mouth_Low_DRIVER_curve_3", "LowerMouth_Center_jnt"],
                           ["LOC_Mouth_Low_DRIVER_curve_4", "R_LowerMouth_01_jnt"],
                           ["LOC_Mouth_Low_DRIVER_curve_5", "R_LowerMouth_02_jnt"],
                           ["LOC_Mouth_Low_DRIVER_curve_6", "R_CornerMouth_jnt"],
                           ["LOC_Mouth_Up_DRIVER_curve_0", "L_CornerMouth_jnt"],
                           ["LOC_Mouth_Up_DRIVER_curve_1", "L_UpperMouth_02_jnt"],
                           ["LOC_Mouth_Up_DRIVER_curve_2", "L_UpperMouth_01_jnt"],
                           ["LOC_Mouth_Up_DRIVER_curve_3", "UpperMouth_Center_jnt"],
                           ["LOC_Mouth_Up_DRIVER_curve_4", "R_UpperMouth_01_jnt"],
                           ["LOC_Mouth_Up_DRIVER_curve_5", "R_UpperMouth_02_jnt"],
                           ["LOC_Mouth_Up_DRIVER_curve_6", "R_CornerMouth_jnt"]]

        tokens = curva.split("_")
        if tokens[1] == 'Up':
            side = "Up"
        else:
            side = "Low"

        for idx, lista in enumerate(lista_gruppi):
            tmp_target = []
            for i, v in enumerate(lista):
                jnt = []
                for item in ckeck_jnts_name:
                    if lista[i][0] == item[0]:
                        jnt.append(item[1])

                tmp_selezione = [jnt[0], "Mouth_{}_{}_loc_MATRIX_Grp".format(side, idx), "Mouth_{}_{}_loc_MASTER_Grp".format(side, idx)]
                tmp_target.append(tmp_selezione)
            lista_target.append(tmp_target)

        for valori in lista_info_cv:
            tmp_valori = [valori[1], valori[0]]
            valori = tmp_valori 

            lista_valori.append(valori)

        return lista_target, lista_valori
    

    def matrici_locator(self, lista_valori, lista_grp, curva): 
        selezione, valori = self.trasforma_liste(lista_valori, lista_grp, curva)
        # print("selezione: {}".format(selezione))
        # print("valori: {}".format(valori))

        half = len(lista_valori) / 2

        for idx, (lista_selezione, valori_rotate) in enumerate(zip(selezione, valori)):
            # print(idx)
            # print("Elaborating: {}".format(lista_selezione))
            # print("Elaborating: {}".format(valori_rotate))
            if idx == half:
                # fixa valori locator al centro
                matrice =  mtx.MatrixConstraint(lista_selezione, r=[(1,1,1), (0,0,0)]).matrix_constraint()
            else:
                matrice =  mtx.MatrixConstraint(lista_selezione, r=valori_rotate).matrix_constraint()


    def delete_driver_locator(self, loc_upper_list, loc_lower_list):
        for x,y in zip(loc_upper_list, loc_lower_list):
            cmds.delete(x)
            cmds.delete(y)



    # -----------------------------------------------------------------
    # Crea il sistema con le matrici
    # -----------------------------------------------------------------
    def create_matrix_system(self):

        duplicated_obj = cmds.duplicate("Jaw_Ctrl", n="Jaw_Ctrl_Static")[0]

        # Ottieni tutti i figli (discendenti) di Jaw_Ctrl_Static
        children = cmds.listRelatives(duplicated_obj, children=True, fullPath=True)

        # Se ci sono figli, cancellali
        if children:
            cmds.delete(children)
            
        duplicated_obj02 = cmds.duplicate("MouthMaster_Driver_Ctrl", n="MouthMaster_Driver_Ctrl_Static")[0]

        # Ottieni tutti i figli (discendenti) di Jaw_Ctrl_Static
        children = cmds.listRelatives(duplicated_obj02, children=True, fullPath=True)

        # Se ci sono figli, cancellali
        if children:
            cmds.delete(children)

        print(f"Duplicated {duplicated_obj} and deleted its children.")
        cmds.group("Jaw_Ctrl_Static", n = "Jaw_Ctrl_Static_Offset")
        cmds.parent("Jaw_Ctrl_Static_Offset", w = True)
        cmds.group("Jaw_Ctrl_Static_Offset", n = "Grp_Head_Utilities")
        cmds.parent("Grp_Head_Utilities", "Face_RIG_Grp")
        cmds.connectAttr("Jaw_Ctrl.translate", "Jaw_Ctrl_Static.translate")
        cmds.connectAttr("Jaw_Ctrl.rotate", "Jaw_Ctrl_Static.rotate")  
        cmds.connectAttr("Jaw_Ctrl.scale", "Jaw_Ctrl_Static.scale")
        cmds.group("MouthMaster_Driver_Ctrl_Static", n = "MouthMaster_Driver_Ctrl_Static_Offset")
        cmds.parent("MouthMaster_Driver_Ctrl_Static_Offset", w = True)
        cmds.parent("MouthMaster_Driver_Ctrl_Static_Offset", "Grp_Head_Utilities")

    
        L_corner_mouth_list = [['MouthMaster_Driver_Ctrl_Static', 'L_CornerMouth_Ctrl_MATRIX_Grp', 'L_CornerMouth_Ctrl_MASTER_Grp'],
                               ['Jaw_Ctrl_Static', 'L_CornerMouth_Ctrl_MATRIX_Grp', 'L_CornerMouth_Ctrl_MASTER_Grp']]
        R_corner_mouth_list = [['MouthMaster_Driver_Ctrl_Static', 'R_CornerMouth_Ctrl_MATRIX_Grp', 'R_CornerMouth_Ctrl_MASTER_Grp'],
                               ['Jaw_Ctrl_Static', 'R_CornerMouth_Ctrl_MATRIX_Grp', 'R_CornerMouth_Ctrl_MASTER_Grp']]
        L_UpperMouth_02_list = [['L_CornerMouth_Ctrl', 'L_UpperMouth_02_Ctrl_MATRIX_Grp', 'L_UpperMouth_02_Ctrl_MASTER_Grp'],
                                ['L_UpperMouth_01_Ctrl', 'L_UpperMouth_02_Ctrl_MATRIX_Grp', 'L_UpperMouth_02_Ctrl_MASTER_Grp']]
        R_UpperMouth_02_list = [['R_CornerMouth_Ctrl', 'R_UpperMouth_02_Ctrl_MATRIX_Grp', 'R_UpperMouth_02_Ctrl_MASTER_Grp'],
                                ['R_UpperMouth_01_Ctrl', 'R_UpperMouth_02_Ctrl_MATRIX_Grp', 'R_UpperMouth_02_Ctrl_MASTER_Grp']]
        L_LowerMouth_02_list = [['L_CornerMouth_Ctrl', 'L_LowerMouth_02_Ctrl_MATRIX_Grp', 'L_LowerMouth_02_Ctrl_MASTER_Grp'],
                                ['L_LowerMouth_01_Ctrl', 'L_LowerMouth_02_Ctrl_MATRIX_Grp', 'L_LowerMouth_02_Ctrl_MASTER_Grp']]
        R_LowerMouth_02_list = [['R_CornerMouth_Ctrl', 'R_LowerMouth_02_Ctrl_MATRIX_Grp', 'R_LowerMouth_02_Ctrl_MASTER_Grp'],
                                ['R_LowerMouth_01_Ctrl', 'R_LowerMouth_02_Ctrl_MATRIX_Grp', 'R_LowerMouth_02_Ctrl_MASTER_Grp']]
        L_UpperMouth_01_list = [['L_CornerMouth_Ctrl', 'L_UpperMouth_01_Ctrl_MATRIX_Grp', 'L_UpperMouth_01_Ctrl_MASTER_Grp'],
                                ['UpperMouth_Master_Ctrl', 'L_UpperMouth_01_Ctrl_MATRIX_Grp', 'L_UpperMouth_01_Ctrl_MASTER_Grp']]
        R_UpperMouth_01_list = [['R_CornerMouth_Ctrl', 'R_UpperMouth_01_Ctrl_MATRIX_Grp', 'R_UpperMouth_01_Ctrl_MASTER_Grp'],
                                ['UpperMouth_Master_Ctrl', 'R_UpperMouth_01_Ctrl_MATRIX_Grp', 'R_UpperMouth_01_Ctrl_MASTER_Grp']]
        L_LowerMouth_01_list = [['L_CornerMouth_Ctrl', 'L_LowerMouth_01_Ctrl_MATRIX_Grp', 'L_LowerMouth_01_Ctrl_MASTER_Grp'],
                                ['LowerMouth_Master_Ctrl', 'L_LowerMouth_01_Ctrl_MATRIX_Grp', 'L_LowerMouth_01_Ctrl_MASTER_Grp']]
        R_LowerMouth_01_list = [['R_CornerMouth_Ctrl', 'R_LowerMouth_01_Ctrl_MATRIX_Grp', 'R_LowerMouth_01_Ctrl_MASTER_Grp'],
                                ['LowerMouth_Master_Ctrl', 'R_LowerMouth_01_Ctrl_MATRIX_Grp', 'R_LowerMouth_01_Ctrl_MASTER_Grp']]
        UpperMouth_Master_list = [['MouthMaster_Driver_Ctrl_Static', 'UpperMouth_Master_Ctrl_MATRIX_Grp', 'UpperMouth_Master_Ctrl_MASTER_Grp']]
        LowerMouth_Master_list = [['MouthMaster_Driver_Ctrl_Static', 'LowerMouth_Master_Ctrl_MATRIX_Grp', 'LowerMouth_Master_Ctrl_MASTER_Grp'],
                                  ['Jaw_Ctrl_Static', 'LowerMouth_Master_Ctrl_MATRIX_Grp', 'LowerMouth_Master_Ctrl_MASTER_Grp']]
        
        # STEP 1. matrix Corner_Mouth
        mtx_L_corner_mouth = mtx.MatrixConstraint(L_corner_mouth_list, t=[(1, 1, 1), (0.5, 0.5, 0.5)], r=[(1, 1, 1), (0.5, 0.5, 0.5)]).matrix_constraint()
        mtx_R_corner_mouth = mtx.MatrixConstraint(R_corner_mouth_list, t=[(1, 1, 1), (0.5, 0.5, 0.5)], r=[(1, 1, 1), (0.5, 0.5, 0.5)]).matrix_constraint()
        # STEP 2. matrix Mouth_02
        mtx_L_UpperMouth_02 = mtx.MatrixConstraint(L_UpperMouth_02_list, t=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)], r=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)]).matrix_constraint()
        mtx_R_UpperMouth_02 = mtx.MatrixConstraint(R_UpperMouth_02_list, t=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)], r=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)]).matrix_constraint()
        mtx_L_LowerMouth_02 = mtx.MatrixConstraint(L_LowerMouth_02_list, t=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)], r=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)]).matrix_constraint()
        mtx_R_LowerMouth_02 = mtx.MatrixConstraint(R_LowerMouth_02_list, t=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)], r=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)]).matrix_constraint()
        # STEP 3. matrix Mouth_01
        mtx_L_UpperMouth_01 = mtx.MatrixConstraint(L_UpperMouth_01_list, t=[(0.5, 0, 0.25), (0.5, 1, 0.75)], r=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)]).matrix_constraint()
        mtx_R_UpperMouth_01 = mtx.MatrixConstraint(R_UpperMouth_01_list, t=[(0.5, 0, 0.25), (0.5, 1, 0.75)], r=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)]).matrix_constraint()
        mtx_L_LowerMouth_01 = mtx.MatrixConstraint(L_LowerMouth_01_list, t=[(0.5, 0, 0.25), (0.5, 1, 0.75)], r=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)]).matrix_constraint()
        mtx_R_LowerMouth_01 = mtx.MatrixConstraint(R_LowerMouth_01_list, t=[(0.5, 0, 0.25), (0.5, 1, 0.75)], r=[(0.5, 0.5, 0.5), (0.5, 0.5, 0.5)]).matrix_constraint()
        # STEP 4. matrix Mouth_Master
        mtx_UpperMouth_Master = mtx.MatrixConstraint(UpperMouth_Master_list, t=[(1, 1, 1)], r=[(1, 1, 1)]).matrix_constraint()
        mtx_LowerMouth_Master = mtx.MatrixConstraint(LowerMouth_Master_list, t=[(1, 1, 1), (1, 1, 1)], r=[(1, 1, 1), (1, 1, 1)]).matrix_constraint()




    
    # -----------------------------------------------------------------
    # Finalizza il modulo
    # -----------------------------------------------------------------
    def follow_nostril(self):
    
        cmds.duplicate("L_CornerMouth_Ctrl", n = "L_CornerMouth_Static_Ctrl")
        cmds.group("L_CornerMouth_Static_Ctrl", n = "L_CornerMouth_Static_Ctrl_Offset")
        cmds.connectAttr("L_CornerMouth_Ctrl.translate", "L_CornerMouth_Static_Ctrl.translate")
        cmds.connectAttr("L_CornerMouth_Ctrl.rotate", "L_CornerMouth_Static_Ctrl.rotate")
        cmds.connectAttr("L_CornerMouth_Ctrl.scale", "L_CornerMouth_Static_Ctrl.scale")
        cmds.duplicate("R_CornerMouth_Ctrl", n = "R_CornerMouth_Static_Ctrl")
        cmds.group("R_CornerMouth_Static_Ctrl", n = "R_CornerMouth_Static_Ctrl_Offset")
        translate_mld = cmds.shadingNode('multiplyDivide', asUtility=True, name='MLD_R_CornerMouth_translate')

        cmds.connectAttr('R_CornerMouth_Ctrl.translate', translate_mld + '.input1')

        cmds.setAttr(translate_mld + '.input2X', -1)
        cmds.setAttr(translate_mld + '.input2Y', 1)
        cmds.setAttr(translate_mld + '.input2Z', -1)

        cmds.connectAttr(translate_mld + '.output', 'R_CornerMouth_Static_Ctrl.translate')
        cmds.connectAttr("R_CornerMouth_Ctrl.rotate", "R_CornerMouth_Static_Ctrl.rotate")
        cmds.connectAttr("R_CornerMouth_Ctrl.scale", "R_CornerMouth_Static_Ctrl.scale")        
        cmds.parent("L_CornerMouth_Static_Ctrl_Offset", "R_CornerMouth_Static_Ctrl_Offset", "Jaw_Ctrl_Static_Offset", "Grp_Head_Utilities")
        cmds.parentConstraint("Jaw_Ctrl", "L_CornerMouth_Static_Ctrl_Offset")
        cmds.parentConstraint("Jaw_Ctrl", "R_CornerMouth_Static_Ctrl_Offset")
        cmds.scaleConstraint("Jaw_Ctrl", "L_CornerMouth_Static_Ctrl_Offset")
        cmds.scaleConstraint("Jaw_Ctrl", "R_CornerMouth_Static_Ctrl_Offset")
        cmds.parent("LOC_UpperMouth_Jaw","Head_jnt")

        L_notril_list = [['L_CornerMouth_Static_Ctrl', 'L_Nostril_Ctrl_Matrix_Grp', 'L_Nostril_Ctrl_Grp']]
        R_notril_list = [['R_CornerMouth_Static_Ctrl', 'R_Nostril_Ctrl_Matrix_Grp', 'R_Nostril_Ctrl_Grp']]

        # Creo matrici
        mtx_L_nostril = mtx.MatrixConstraint(L_notril_list, t=[(0.1, 0.1, 0.1)]).matrix_constraint()
        mtx_R_nostril = mtx.MatrixConstraint(R_notril_list, t=[(0.1, 0.1, 0.1)]).matrix_constraint()

        for side, mtx_node in zip(['L', 'R'], (mtx_L_nostril, mtx_R_nostril)):
            # Inserisci nodi multiply translate nel set SLIDERS
            mlp_slider = mtx_node[0][0][0]
            self.set_sliders.append(mlp_slider)

            # Creo multiply con connessioni da L/R_Nostril_Ctrl e MLP_SLIDER
            mlp_follow = cmds.createNode("multiplyDivide", n="MLP_{}_Nostril_Follow".format(side))
            cmds.connectAttr("{}.output".format(mlp_slider), "{}.input1".format(mlp_follow), force=True)
            for attr in ['X', 'Y', 'Z']:
                cmds.connectAttr("{}_Nostril_Ctrl.follow".format(side), "{}.input2{}".format(mlp_follow, attr), force=True)

            # Connetti questo multiply al PLS_translate al posto dello slider
            cmds.connectAttr("{}.output".format(mlp_follow), "{}.input3D[0]".format(mtx_node[1][0]), force=True)

        print("# Connessioni dei follow nostril fatte. #")


    def duplicate_and_rename_groups_and_controls(self, base_group, rig_hierarchy):
        """Fa un duplicato di 'base_group' e rinominalo con tutto il suo contenuto col suffisso '_Static'."""
        # Rinomina il gruppo dei controlli 'base_group' col suffisso '_Static'.
        self.static_grp = cmds.rename(base_group, "{}_Static".format(base_group))
        
        # Duplicalo come 'driver_grp'
        driver_grp = cmds.duplicate(self.static_grp, n=base_group)[0]

        # Pulisci il driver_grp da eventuali constraint
        children = cmds.listRelatives(driver_grp, allDescendents=True, fullPath=True, shapes=False)
        for item in children:
            check_type = cmds.nodeType(item)
            if 'Constraint' in check_type:
                cmds.delete(item)

        # Rinomina tutti i gruppi e controlli del gruppo 'static_grp' col suffisso '_Static', saltando le shapes.
        children = cmds.listRelatives(self.static_grp, allDescendents=True, fullPath=True, shapes=False)
        for item in children:
            check_type = cmds.nodeType(item)
            if check_type == 'transform':
                tmp = item.split("|")[-1]
                cmds.rename(item, "{}_Static".format(tmp))

        # Metti in gerachia driver_grp e nascondi lo static_grp
        cmds.parent(driver_grp, rig_hierarchy)
        cmds.setAttr("{}.visibility".format(self.static_grp), 0)

        print("# Duplicato e rinominato tutto il contenuto di '{}' col suffisso '_Static'. #".format(base_group))




    def connect_driver_to_static(self, driver_grp, static_grp):
        """Crea connessioni dirette tra i controlli e gruppi Matrix di driver e static """
        children_static = cmds.listRelatives(static_grp, allDescendents=True, fullPath=False, shapes=False)
        children_driver = cmds.listRelatives(driver_grp, allDescendents=True, fullPath=False, shapes=False)

        # '_Ctrl' --> '_Ctrl_Static'
        for static_ctrl, driver_ctrl in zip(children_static, children_driver):
            if cmds.nodeType(driver_ctrl) == 'transform':
                name_tokens = driver_ctrl.split("_")[-1]
                if name_tokens == "Ctrl":
                    # print("{} --> {}".format(driver_ctrl, static_ctrl))
                    driver_attrs = cmds.listAttr(driver_ctrl, keyable=True) or []
                    for attr in driver_attrs:
                        if cmds.attributeQuery(attr, node=static_ctrl, exists=True) and cmds.getAttr(driver_ctrl + '.' + attr, keyable=True):
                            cmds.connectAttr("{}.{}".format(driver_ctrl, attr), "{}.{}".format(static_ctrl, attr), force=True)

        # '_Ctrl_MATRIX_Grp_Static' --> '_Ctrl_MATRIX_Grp' valido per [MATRIX, FOLLOW, SQUARE, COMPENSATE]
        lista_gruppi = ('MATRIX', 'FOLLOW', 'SQUARE', 'COMPENSATE')
        for static_grp, driver_grp in zip(children_static, children_driver):
            if cmds.nodeType(driver_grp) == 'transform':
                name_tokens = driver_grp.split("_")
                for grp in lista_gruppi:
                    if grp in name_tokens:
                        # print("{} --> {}".format(static_grp, driver_grp))
                        driver_attrs = cmds.listAttr(static_grp, keyable=True) or []
                        for attr in driver_attrs:
                            if cmds.attributeQuery(attr, node=driver_grp, exists=True) and cmds.getAttr("{}.{}".format(static_grp, attr), keyable=True):
                                cmds.connectAttr("{}.{}".format(static_grp, attr), "{}.{}".format(driver_grp, attr), force=True)
        
        # aggiunta multiply per square
        mlp_node_up_square = cmds.createNode("multiplyDivide", name="MLP_UpperMouth_02_Ctrl_SQUARE_Grp")
        mlp_node_low_square = cmds.createNode("multiplyDivide", name="MLP_LowerMouth_02_Ctrl_SQUARE_Grp")

        for node in (mlp_node_up_square, mlp_node_low_square):
            cmds.setAttr("{}.input2X".format(node), 0)
            cmds.setAttr("{}.input2Z".format(node), 0)
        
        # Inserisci nodo nel set SLIDERS
        self.set_sliders.append(mlp_node_up_square)
        self.set_sliders.append(mlp_node_low_square)

        for attr in ("X", "Y", "Z"):    
            cmds.connectAttr("L_UpperMouth_02_Ctrl_SQUARE_Grp_Static.translate{}".format(attr), "{}.input1{}".format(mlp_node_up_square, attr), force=True)
            cmds.connectAttr("R_UpperMouth_02_Ctrl_SQUARE_Grp_Static.translate{}".format(attr), "{}.input1{}".format(mlp_node_up_square, attr), force=True)
            cmds.connectAttr("L_LowerMouth_02_Ctrl_SQUARE_Grp_Static.translate{}".format(attr), "{}.input1{}".format(mlp_node_low_square, attr), force=True)
            cmds.connectAttr("R_LowerMouth_02_Ctrl_SQUARE_Grp_Static.translate{}".format(attr), "{}.input1{}".format(mlp_node_low_square, attr), force=True)

            cmds.connectAttr("{}.output{}".format(mlp_node_up_square, attr), "L_UpperMouth_02_Ctrl_SQUARE_Grp.translate{}".format(attr), force=True)
            cmds.connectAttr("{}.output{}".format(mlp_node_up_square, attr), "R_UpperMouth_02_Ctrl_SQUARE_Grp.translate{}".format(attr), force=True)
            cmds.connectAttr("{}.output{}".format(mlp_node_low_square, attr), "L_LowerMouth_02_Ctrl_SQUARE_Grp.translate{}".format(attr), force=True)
            cmds.connectAttr("{}.output{}".format(mlp_node_low_square, attr), "R_LowerMouth_02_Ctrl_SQUARE_Grp.translate{}".format(attr), force=True)

        print("# Connessioni dirette completate tra driver e static. #")


    def make_sliders(self, nameRig, nodi):
        """Rinomina i nodi passati come input con il suffisso 'SLIDER' e se non esiste crea il set slider corrispondente."""
        sliders = []

        for item in nodi:
            nodo = cmds.rename(item, "{}_SLIDER".format(item))
            sliders.append(nodo)

        # Controlla se il set globale degli sliders esiste, altrimenti crealo
        if not cmds.objExists("SLIDERS"):
            sliders_set = cmds.sets(empty=True, name=("SLIDERS"))
        else:
            sliders_set = "SLIDERS"

        # Controlla se il set della parte di rig degli sliders esiste, altrimenti crealo
        if not cmds.objExists("SLIDERS_HeadMouth"):
            rigpart_sliders_set = cmds.sets(empty=True, name=("SLIDERS_HeadMouth"))
            # Parenta il set della parte di rig dentro al set globale
            cmds.sets(rigpart_sliders_set, edit=True, forceElement=sliders_set)
        else:
            rigpart_sliders_set = "SLIDERS_HeadMouth"

        # crea sotto-set del singolo modulo
        if not cmds.objExists("{}_sliders".format(nameRig)):
            set_module_sliders = cmds.sets(empty=True, name=("{}_sliders".format(nameRig)))
            # Parenta il set del modulo dentro al set globale
            cmds.sets(set_module_sliders, edit=True, forceElement=rigpart_sliders_set)
        else:
            set_module_sliders = "{}_sliders".format(nameRig)

        # Popola il set
        for nodo in sliders:
            cmds.sets(nodo, edit=True, forceElement=set_module_sliders)

        print("# Sliders rinominati e set creato. #")


    def insert_driver_crv_in_set_slider(self, nameRig, up_curve, low_curve):
        set_module_sliders = "{}_sliders".format(nameRig)
        
        for curve in (up_curve, low_curve):
            cmds.sets(curve, edit=True, forceElement=set_module_sliders)

    
    def rename_ctrl_shapes(self, set_ctrl):
        """Data una selezione di controlli, rinomina le shapes in base al nome del controllo."""
        cmds.select(clear=True)
        cmds.select(set_ctrl)
        sel = cmds.ls(sl=True)
        cmds.select(clear=True)
        
        for ctrl in sel:
            shapes = cmds.listRelatives(ctrl, shapes=True, pa=True)
            for shape in shapes:
                new_name = "{}Shape".format(ctrl)
                cmds.rename(shape, new_name)

        print("### Le shapes dei controlli sono state rinominate correttamente. ###")

    
    def create_joint_mouthMaster_driver(self):
        """Crea gruppc con joint nella posizione di 'MouthMaster_Driver_Ctrl'"""
        ctrl_driver = 'MouthMaster_Driver_Ctrl'
        grp_driver = 'MouthMaster_Driver_Ctrl_Grp'
        
        cmds.select(clear=True)
        
        if cmds.objExists(ctrl_driver):
            grp = cmds.group(name="GRP_MouthMaster_Driver_joint", world=True, empty=True)
            cmds.select(clear=True)
            jnt = cmds.joint(name="MouthMaster_Driver_jnt")
            cmds.select(clear=True)
            cmds.parent(jnt, grp)
            cmds.matchTransform(grp, ctrl_driver)
            cmds.parent(grp, grp_driver)
            cmds.parentConstraint(ctrl_driver, jnt, mo=True, w=1)
            cmds.scaleConstraint(ctrl_driver, jnt, w=1)
            cmds.select(clear=True)
            
        # Controlla se il set dei joints del modulo esiste, altrimenti crealo
        if not cmds.objExists("{}_jointsForSkin".format(self.nameRig)):
            setSkinJnts = cmds.sets(empty=True, name=("{}_jointsForSkin".format(self.nameRig)))
            # Parenta il set del modulo dentro al set globale
            cmds.sets(setSkinJnts, edit=True, forceElement=rigpart_joints_set)
        else:
            setSkinJnts = "{}_jointsForSkin".format(self.nameRig)

        cmds.sets(jnt, edit=True, forceElement=setSkinJnts)
    
    
    def mouthFixUe(self):
        
        mouth_control_jnt = "Mouth_CTRL_JNT_GRP"
        mouth_joint = "Mouth_JNT_GRP"
        mouth_Controls_Static = "Mouth_CTRL_GRP_Static"
        mouthUtilities = "GRP_Mouth_Controls_Utilities"
        mouth_Controls_toRemove = "Mouth_CTRL_GRP"
        source_group_Up = "Mouth_Up_joints_GRP"
        source_group_Low = "Mouth_Low_joints_GRP"
        
        # Funzione per rimuovere il suffisso "_Static"
        def remove_suffix(name, suffix="_Static"):
            if name.endswith(suffix):
                return name[:-len(suffix)]
            return name

        # Creazione del driver joint per la bocca
        mouth_Driver_jnt = cmds.joint(n="Mouth_Master_Joints_jnt")
        cmds.matchTransform(mouth_Driver_jnt, "Jaw_Ctrl")
        cmds.parent(mouth_joint, mouth_Driver_jnt)
        cmds.connectAttr("MouthMaster_Driver_jnt.translate", "MouthMaster_Driver_Ctrl_Static.translate")
        cmds.connectAttr("MouthMaster_Driver_jnt.rotate", "MouthMaster_Driver_Ctrl_Static.rotate")  
        cmds.connectAttr("MouthMaster_Driver_jnt.scale", "MouthMaster_Driver_Ctrl_Static.scale")
        
        # Parent dei figli del gruppo superiore e inferiore sotto il driver joint della bocca
        children_Up = cmds.listRelatives(source_group_Up, children=True, fullPath=True)
        if children_Up:
            for child in children_Up:
                cmds.parent(child, mouth_Driver_jnt)
            print(f"Tutti i figli di {source_group_Up} sono stati parentati a {mouth_Driver_jnt}")
        else:
            print(f"{source_group_Up} non ha figli o non esiste.")

        children_Low = cmds.listRelatives(source_group_Low, children=True, fullPath=True)
        if children_Low:
            for child in children_Low:
                cmds.parent(child, mouth_Driver_jnt)
            print(f"Tutti i figli di {source_group_Low} sono stati parentati a {mouth_Driver_jnt}")
        else:
            print(f"{source_group_Low} non ha figli o non esiste.")
        
        
        # Rimuovere tutti i parent constraint esistenti nei figli di Mouth_CTRL_JNT_GRP
        children_ctrl_jnt_grp = cmds.listRelatives(mouth_control_jnt, children=True, fullPath=True)
        if children_ctrl_jnt_grp:
            for child in children_ctrl_jnt_grp:
                constraints = cmds.listRelatives(child, type='parentConstraint', fullPath=True)
                if constraints:
                    cmds.delete(constraints)
        
        # Ricreare i parent constraint in base alla lista fornita
        controls = [
            "R_CornerMouth_Ctrl", "R_UpperMouth_02_Ctrl", "R_UpperMouth_01_Ctrl", "UpperMouth_Center_Ctrl",
            "L_UpperMouth_01_Ctrl", "L_UpperMouth_02_Ctrl", "L_CornerMouth_Ctrl", "L_LowerMouth_02_Ctrl",
            "L_LowerMouth_01_Ctrl", "LowerMouth_Center_Ctrl", "R_LowerMouth_01_Ctrl", "R_LowerMouth_02_Ctrl"
        ]
        
        joints = [
            "R_CornerMouth_jnt", "R_UpperMouth_02_jnt", "R_UpperMouth_01_jnt", "UpperMouth_Center_jnt",
            "L_UpperMouth_01_jnt", "L_UpperMouth_02_jnt", "L_CornerMouth_jnt", "L_LowerMouth_02_jnt",
            "L_LowerMouth_01_jnt", "LowerMouth_Center_jnt", "R_LowerMouth_01_jnt", "R_LowerMouth_02_jnt"
        ]
        
        for ctrl, jnt in zip(controls, joints):
            cmds.parentConstraint(ctrl, jnt, maintainOffset=True)
        
        # Rimozione del gruppo Mouth_JNT_GRP
        cmds.delete(mouth_joint)
        
        # Settaggio delle visibilità e parentaggi finali
        cmds.setAttr("Mouth_CTRL_GRP.visibility", 1)
        cmds.setAttr("Grp_Head_Utilities.visibility", 0)
        cmds.parent("Mouth_Master_Joints_jnt", "Head_SQSLow_03_jnt")
        cmds.parent("Mouth_CTRL_JNT_GRP", "Head_jnt")
        
        # Cancellazione e settaggio di attributi aggiuntivi
        cmds.delete("NoseLow_jnt")
        cmds.setAttr("Nose_Ctrl.follow", 0.5)
        cmds.setAttr("L_Nostril_Ctrl.follow", 0.5)
        cmds.setAttr("R_Nostril_Ctrl.follow", 0.5)
        cmds.connectAttr("MouthMaster_Ctrl.translateZ", "MouthMaster_Driver_Ctrl.translateZ")

        
    # -----------------------------------------------------------------
    # Funzioni lanciate dalla UI
    # -----------------------------------------------------------------
    def buildRig(self, *args):
        """Build mouth rig.
        
        Called by 'UI' function.
        Call functions: 'createMouthCrv', 'cv_base_curve_to_joint', 'place_rig_locators', 'connectLocToCrv', 
                        'createDriverCrv', 'createJntCtrls', 'createCrvCtrls', 'create_locator_ctrl' """
        
        if self.eyeLoc == None or self.upperLidVtx == None or self.lowerLidVtx == None :
            cmds.error("Please define eye center and mouth vertices.")
        else : # Call functions to build rig #
            # Step 1: creates a "high-res" curve for each lid (each vertex is a point of the curve)
            self.createMouthCrv(self.nameRig, self.upperLidVtx, self.lowerLidVtx)
            # Step 2: places one joint per curve cv
            self.cv_base_curve_to_joint(self.eyeLoc, self.upLidCrv)
            # Step 3: places one locator per curve cv and constrain-aim each joint to it (so as it acts like an IK)
            self.place_rig_locators(self.nameRig, self.upLidJntList, self.lowLidJntList)
            # Step 4: connects each locator to the curve with a pointOnCurve node, so when the CVs of the curve move, the corresponding locator follows (and so does the joint)
            self.connectLocToCrv(self.upLidLocList, self.upLidCrv, self.lowLidLocList, self.lowLidCrv)
            # Step 5: creates a "low-res" curve with only 7 control points and makes it drive the high-res curve with a wire deformer
            self.createDriverCrv(self.upLidCrv, self.hierarchyUpCrvGrp, self.lowLidCrv, self.hierarchyLowCrvGrp)
            # Step 6: creates controller joints to drive the 'driver curve'
            self.createJntCtrls(self.nameRig, self.cornerAPos, self.cornerBPos, self.upLidDriverCrv, self.lowLidDriverCrv, self.grpBaseRig)
            # Step 7: crea i locator per posizionare i controlli
            self.create_locator_ctrl(self.ctrlJnts, self.locator_ctrls)

        # Update button
        cmds.button(self.btnFinalize, e=1, enable=True)

        # End message
        print("### First Step has been successfully completed ###.")
    

    def cleanup_set_controls(self, set_name):
        """Elimina controlli 'Static' dal set dei controlli."""
        set_tmp = cmds.sets(set_name, q=True)

        for item in set_tmp:
            tokens = item.split("_")
            if tokens[-1] == "Static":
                cmds.sets(item, remove=set_name) 

    def finalizeRig(self, *args):
        """Finalize mouth rig.
        
        Called by 'UI' function.
        Call functions: 'check_locators', 'match_joints_to_locators', 'bindskin_ctrljnts_to_curve', 
                        'createCrvCtrls', 'add_master_ctrl', 'addSmartClose', 'addStickyLips', 
                        'replace_and_remove_shapes', create_matrix_system, 'jaw_ctrl_square_attribute', 
                        'uppermouth_master_ctrl_jaw_follow_attribute', 'unlock_rotate_on_mouth_controls',
                        'matrici_locator' """

        # Step 0: check esistenza dei locator 
        self.check_locators(self.locator_ctrls)     
        # Step 1: matcho ctrlJnts ai translate dei locator 
        self.match_joints_to_locators(self.locator_ctrls, self.ctrlJnts)
        # Step 2: creates curve controllers, and attached the corresponding joints to them
        self.createCrvCtrls(self.nameRig, self.ctrlJnts)
        # Step 3: crea controlli mouth_master con relativa gerarchia di gruppi
        self.add_master_ctrl(self.nameRig, self.ctrlList, self.master_grp_list, self.matrix_grp_list)
        # Step 4: if smart close check box is checked, add smart close feature
        self.addSmartClose(self.nameRig, self.upLidCrv, self.upLidDriverCrv, self.lowLidCrv, self.lowLidDriverCrv, self.hierarchyCrvGrp, self.hierarchyUpCrvGrp, self.hierarchyLowCrvGrp, self.mouth_master_ctrls)
        # Step 5: add sticky lips feature
        self.addStickyLips(self.nameRig, self.bothLidsSB_BlndShp)
        # Step 6: import the new controls_shapes and replace them
        self.replace_and_remove_shapes()
        # Step 7: crea tutte le connessioni via matrici e il sistema per i vari attributi aggiuntivi
        self.create_matrix_system()
        # Step 8: attributo Square di Jaw_Ctrl
        self.jaw_ctrl_square_attribute(self.jaw_ctrl, self.square_grps)
        # Step 9: attributo JawFollow di UpperMouth_Master_Ctrl
        self.uppermouth_master_ctrl_jaw_follow_attribute(self.loc_nose_jaw)
        # Step 10: sistema per far seguire l'uppermouth in su verso il naso
        self.uppermouth_master_ctrl_nose_compensate_attribute(self.loc_nose_jaw)
        # Step 11: skinna ctrlJnts alle rispettive curve driver 
        self.bindskin_ctrljnts_to_curve(self.upLidDriverCrv, self.lowLidDriverCrv, self.ctrlJnts)
        # Step 12: matrici only rotate per i locator
        self.unlock_rotate_on_mouth_controls(self.nameRig)
        self.matrici_locator(self.lista_valori_upper, self.lista_selezione_upper, self.upLidCrv)
        self.matrici_locator(self.lista_valori_lower, self.lista_selezione_lower, self.lowLidCrv)
        self.delete_driver_locator(self.loc_upper_list, self.loc_lower_list)
        self.duplicate_and_rename_groups_and_controls(self.hierarchyMainGrpCTRL, self.parent_rig)
        self.cleanup_set_controls(self.set_mouth_ctrls)
        self.connect_driver_to_static(self.hierarchyMainGrpCTRL, self.static_grp)
        self.create_joint_mouthMaster_driver()
        self.make_sliders(self.nameRig, self.set_sliders)
        self.insert_driver_crv_in_set_slider(self.nameRig, self.upLidDriverCrv, self.lowLidDriverCrv)
        self.rename_ctrl_shapes(self.set_mouth_ctrls)
        self.rename_joints_skin(self.upLidJntList, "Up")
        self.rename_joints_skin(self.lowLidJntList, "Low")
        self.mouthFixUe()
        self.follow_nostril()

        # Clear scene & script variables #
        cmds.delete(self.eyeLoc)
        cmds.delete(self.loc_grp)
        cmds.select(cl = 1)
        
        self.eyeLoc = None
        self.nameRig = None
        self.upperLidVtx = None
        self.lowerLidVtx = None
        self.set_sliders[:] = []
        
        # Update UI #
        cmds.textField(self.txtfLoc, e = 1, tx = "")
        cmds.button(self.btnPlaceCenter, e = 1, en = 1)
        cmds.button(self.btnUndoPlaceCenter, e = 1, en = 0)
        cmds.scrollField(self.scrollfUpLid, e = 1, cl = 1)
        cmds.button(self.btnFinalize, e=1, enable=False)
        
        # End message
        print("### Mouth has been successfully rigged. ###")

    
    # -----------------------------------------------------------------
    # UI
    # -----------------------------------------------------------------
    def UI(self):
        '''Creates UI - Main function
        
        Call functions: 'eyeParentJnt', 'eyeParentCtrl', 'placeEyeCenter', 'placeEyeCenterUndo', 
                        'upLidVtxSet', 'lowLidVtxSet', 'buildRig' '''

        # Main window
        
        winWidth = 290
        
        UKDP_mainWin = "RBW_Mouth_Rig"

        if cmds.window("RBW_Mouth_Rig", exists = 1):
            cmds.deleteUI("RBW_Mouth_Rig", window = 1)

        ww = cmds.window("RBW_Mouth_Rig", title = "RBW Mouth Rig", mxb = 0, sizeable=True, resizeToFitChildren=True)

        # Get a pointer and convert it to Qt Widget object
        qw = omui.MQtUtil.findWindow(ww)
        widget = wrapInstance(int(qw), QWidget)

        # Create a QIcon object
        iconpath = os.path.join(self.image_path, "RainbowCGI_icona.ico")

        # Assign the icon
        icon = QIcon(iconpath)
        widget.setWindowIcon(icon)

        # Main layout
        col = cmds.columnLayout(adjustableColumn=True, co=("both", 5))
        
        # Carica l'immagine della mouth
        img = os.path.join(self.image_path, "mouth.png")
        cmds.text(h = 10, l = "")
        cmds.iconTextButton(style="iconOnly", image1=img, p=col, w=100)
        cmds.text(h = 10, l = "")
        cmds.setParent("..")

        # Define mouth center
        cmds.frameLayout( label="1. Select mouth pivot, then click 'Place center'", marginHeight=10, bgc = (0.3,0.3,1.0))
        cmds.rowLayout(numberOfColumns = 2, adjustableColumn = 1)
        self.btnPlaceCenter = cmds.button(w = ((winWidth / 2) - 2), l = "Place center", c = self.placeEyeCenter)
        self.btnUndoPlaceCenter = cmds.button(w = ((winWidth / 2) - 2), l = "Undo", c = self.placeEyeCenterUndo, en = 0)

        cmds.setParent("..")
        cmds.setParent("..")
        cmds.text(h = 5, l = "")
        self.txtfLoc = cmds.textField(w = winWidth, ed = 0)
        
        cmds.separator(h = 15, w = winWidth, style = "in")

        # List upper lid vertices
        cmds.frameLayout( label="2. Select vertices (upper or lower lip), then click 'Set'", marginHeight=10, bgc = (0.3,0.3,1.0))
        self.btnUpLid = cmds.button(w = winWidth, l = "Set", c = self.upLidVtxSet)
        self.scrollfUpLid = cmds.scrollField(w = winWidth, h = 35, wordWrap = 1, ed = 0, en = 0)
        cmds.setParent("..")
        cmds.separator (h = 15, w = winWidth, style = "in")

        # Allow/disallow smart close
        cmds.rowLayout(numberOfColumns = 2)
        cmds.button(l = "", io=True, w=100, isObscured=True, manage=False)
        # self.isSmartClose =  cmds.checkBox(h = 30, l = "Add smart close?", v = 1)
        
        cmds.setParent("..")
        
        # Build first step
        self.btnBuild = cmds.button(w = winWidth, h = 30, l = "BUILD FIRST STEP", c = self.buildRig, bgc = (0.3,0.3,1))
        
        cmds.separator (h = 15, w = winWidth, style = "in")
        cmds.text("Usa i locator per posizionare i controlli.", h=40, align='center', font='boldLabelFont')
        cmds.text(" ", h=10)

        # Build final rig
        self.btnFinalize = cmds.button(w = winWidth, h = 60, l = "BUILD RIG", c = self.finalizeRig, bgc = (0.3,0.3,1), enable=False)
        
        cmds.text(h = 10, l = "")
        
        cmds.showWindow("RBW_Mouth_Rig")

# launch
autoMouthRig = AER()
autoMouthRig.UI() # load UI