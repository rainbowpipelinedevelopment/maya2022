# -*- coding: utf-8 -*-
import maya.cmds as cmds

# Funzioni di Utility per creazione set
def create_joints_set(module, jnt_list):
    # Creates a set containing the joints for skinning
    joints_for_skin = []
    joints_for_skin.extend(jnt_list)
    
    if not cmds.objExists("FACE_MODULES_Joints"):
        module_joints_set = cmds.sets(empty=True, name=("FACE_MODULES_Joints"))
    else:
        module_joints_set = "FACE_MODULES_Joints"

    # Controlla se il set della parte di rig dei joints esiste, altrimenti crealo
    if not cmds.objExists("{}_Joints".format(module)): # 'HeadEyes' -'HeadMouth' - 'HeadSquash'
        rigpart_joints_set = cmds.sets(empty=True, name=("{}_Joints".format(module)))
        # Parenta il set della parte di rig dentro al set globale
        cmds.sets(rigpart_joints_set, edit=True, forceElement=module_joints_set)
    else:
        rigpart_joints_set = "{}_Joints".format(module)

    for jnt in joints_for_skin:
        cmds.sets(jnt, edit=True, forceElement=rigpart_joints_set)

def create_head_joint():
    """Crea un joint nella posizione di 'LOC_Head', lo rinomina 'Head_jnt' e aggiunge l'attributo 'masterScale'."""
    if cmds.objExists("LOC_Head"):
        head_pos = cmds.xform("LOC_Head", q=True, ws=True, t=True)
        cmds.select(clear=True)
        head_joint = cmds.joint(name="Head_jnt", p=head_pos)

        # Aggiungi l'attributo masterScale
        if not cmds.attributeQuery("masterScale", node=head_joint, exists=True):
            cmds.addAttr(head_joint, longName="masterScale", attributeType="float", min=0, max=10, defaultValue=1, keyable=True)
            print("Attributo 'masterScale' aggiunto a 'Head_jnt'.")
        
        print("Head_jnt creato nella posizione di LOC_Head.")
    else:
        print("LOC_Head non esiste.")


def create_controls_set(module, ctrl_list):
    # use only the controls
    ctrl_list_pruned = []

    for idx, obj in enumerate(ctrl_list):
        children = cmds.listRelatives(obj[0], s=True)
        for check in children:
            check_type = cmds.objectType(check, isType="nurbsCurve")
            if check_type == True:
                if obj[0] not in ctrl_list_pruned:
                    ctrl_list_pruned.append(obj[0])

    # Crea il set contenente i controlli creati
    if not cmds.objExists("FACE_MODULES_Controls"):
        module_controls_set = cmds.sets(empty=True, name=("FACE_MODULES_Controls"))
    else:
        module_controls_set = "FACE_MODULES_Controls"

    if not cmds.objExists("{}_controls".format(module)):
        module_ctrl_set = cmds.sets(empty=True, name=("{}_controls".format(module)))
        # Parenta il set del modulo dentro al set globale
        cmds.sets(module_ctrl_set, edit=True, forceElement=module_controls_set)
    else:
        module_ctrl_set = "{}_controls".format(module)

    for ctrl in ctrl_list_pruned:
        cmds.sets(ctrl, edit=True, forceElement=module_ctrl_set)

    return module_ctrl_set

def rename_ctrl_shapes(set_ctrl):
    """Data una selezione di controlli, rinomina le shapes in base al nome del controllo."""
    cmds.select(clear=True)
    cmds.select(set_ctrl)
    sel = cmds.ls(sl=True)
    cmds.select(clear=True)
    
    for ctrl in sel:
        shapes = cmds.listRelatives(ctrl, shapes=True, pa=True)
        for shape in shapes:
            new_name = "{}Shape".format(ctrl)
            cmds.rename(shape, new_name)

    print("### Le shapes dei controlli sono state rinominate correttamente. ###")


# FASE 1: Creazione della Joint Chain superiore
# Funzione modificata per creare una catena di joint con un prefisso opzionale
def create_joint_chain(locator_names, prefix=""):
    joint_chain = []
    for loc_name in locator_names:
        joint_name = prefix + loc_name.replace("LOC_", "") + "_jnt"
        locator_position = cmds.pointPosition(loc_name, w=True)
        joint = cmds.joint(p=locator_position, name=joint_name)
        joint_chain.append(joint)
    
    return joint_chain

# Creazione Joint Chain inferiore con prefisso opzionale
def create_low_joint_chain(prefix=""):
    low_locator_names = ["LOC_SQSLow_01", "LOC_SQSLow_02", "LOC_SQSLow_03", "LOC_SQSLow_04"]
    low_joint_chain = create_joint_chain(low_locator_names, prefix)
    cmds.select(clear=True)
    return low_joint_chain

# Creazione Joint Chain per il lato destro con prefisso opzionale
def create_up_joint_chain(prefix=""):
    up_locator_names = ["LOC_SQSUp_01", "LOC_SQSUp_02", "LOC_SQSUp_03", "LOC_SQSUp_04"]
    up_joint_chain = create_joint_chain(up_locator_names, prefix)
    cmds.select(clear=True)
    return up_joint_chain

def connect_joint_chains(source_chain, target_chain):
    for source_joint, target_joint in zip(source_chain, target_chain):
        # Connette translate, rotate e scale
        for attr in ['translate', 'rotate', 'scale']:
            for axis in ['X', 'Y', 'Z']:
                source_attr = source_joint + '.' + attr + axis
                target_attr = target_joint + '.' + attr + axis
                cmds.connectAttr(source_attr, target_attr, force=True)

def set_draw_style_to_none(joint_chain):
    for joint in joint_chain:
        cmds.setAttr(joint + ".drawStyle", 2)  # 2 corrisponde a "none" nel drawStyle


# FASE 2: Creazione delle curve per il lato sinistro
def create_low_linear_curves():
    low_joint_names = ["SQSLow_01_jnt", "SQSLow_02_jnt", "SQSLow_03_jnt", "SQSLow_04_jnt"]
    create_linear_curve_with_spans([cmds.xform(joint, query=True, translation=True, worldSpace=True) for joint in low_joint_names], "Low_SQS_cv")
    cmds.select(clear=True)

# Creazione delle curve per il lato destro
def create_up_linear_curves():
    up_joint_names = ["SQSUp_01_jnt", "SQSUp_02_jnt", "SQSUp_03_jnt", "SQSUp_04_jnt"]
    create_linear_curve_with_spans([cmds.xform(joint, query=True, translation=True, worldSpace=True) for joint in up_joint_names], "Up_SQS_cv")
    cmds.select(clear=True)

# Creazione delle curve lineari
def create_linear_curve_with_spans(points, curve_name):
    num_spans = len(points) - 1
    curve = cmds.curve(d=1, p=points)
    cmds.rebuildCurve(curve, ch=1, rpo=1, rt=0, end=1, kr=0, s=num_spans, d=3, tol=0.01)
    cmds.rename(curve, curve_name)
    

# FASE 3: Creazione degli IK Spline Handle
def create_ik_spline_handles(low_joint_chain, up_joint_chain):
    # Creazione degli IK Spline Handle per il lato inferiore
    create_ik_spline_handle(low_joint_chain, "Low_SQS_cv")

    # Creazione degli IK Spline Handle per il lato superiore
    create_ik_spline_handle(up_joint_chain, "Up_SQS_cv")

# Funzione per la creazione degli IK Spline Handle
def create_ik_spline_handle(joint_chain, curve_name):
    ik_handle = cmds.ikHandle(solver="ikSplineSolver", sj=joint_chain[0], ee=joint_chain[-1])
    cmds.rename(ik_handle[0], curve_name + "_ikh")
    cmds.rename(ik_handle[1], curve_name + "_effector")
    cmds.ikHandle(curve_name + "_ikh", edit=True, curve=curve_name)


# FASE 4: Rimozione delle curve generate automaticamente
def remove_generated_curves():
    if cmds.objExists("curve1"):
        cmds.delete("curve1")
    if cmds.objExists("curve2"):
        cmds.delete("curve2")


# FASE 5: Creare i Control Joints

def midpoint(loc1, loc2):
    """Calcola il punto medio tra due locator."""
    pos1 = cmds.xform(loc1, q=True, ws=True, t=True)
    pos2 = cmds.xform(loc2, q=True, ws=True, t=True)
    return [(p1 + p2) / 2 for p1, p2 in zip(pos1, pos2)]


def create_joints():
    """Crea i tre joint nelle posizioni specificate."""
    # Crea il joint "TopSQS_Jnt" nella posizione di "LOC_SQSUp_04"
    top_pos = cmds.xform("LOC_SQSUp_04", q=True, ws=True, t=True)
    cmds.select(clear=True)
    cmds.joint(name="TopSQS_Jnt", p=top_pos)

    # Crea il joint "MidSQS_Jnt" a metÃ  tra "LOC_SQSUp_01" e "LOC_SQSLow_01"
    mid_pos = midpoint("LOC_SQSUp_01", "LOC_SQSLow_01")
    cmds.select(clear=True)
    cmds.joint(name="MidSQS_Jnt", p=mid_pos)

    # Crea il joint "LowSQS_Jnt" nella posizione di "LOC_SQSLow_04"
    low_pos = cmds.xform("LOC_SQSLow_04", q=True, ws=True, t=True)
    cmds.select(clear=True)
    cmds.joint(name="LowSQS_Jnt", p=low_pos)
    
    # Crea il joint "ZeroSQS_Jnt" nella posizione di "LOC_SQSLow_04"
    cmds.select(clear=True)
    cmds.joint(name="ZeroSQS_Jnt", p=low_pos)


def create_control_for_joint(jnt):
    """
    Crea un controllo NURBS e un gruppo di offset per il joint specificato.
    Restituisce il nome del controllo creato.

    :param jnt: Nome del joint per cui creare il controllo.
    :return: Nome del controllo creato.
    """
    # Creazione del nome del controllo senza "_Jnt" e aggiunta di "_Ctrl"
    ctrl_name = jnt.replace("_Jnt", "") + "_Ctrl"

    # Crea un controllo NURBS (ad esempio, un cerchio)
    ctrl = cmds.circle(name=ctrl_name, nr=(1, 0, 0))[0]

    # Ottieni la posizione del joint
    joint_pos = cmds.xform(jnt, q=True, ws=True, t=True)

    # Crea un gruppo di offset per il controllo
    offset_grp = cmds.group(name=ctrl_name + "_Grp_Offset", em=True)
    cmds.parent(ctrl, offset_grp)

    # Crea un gruppo esterno per il gruppo di offset
    external_grp = cmds.group(name=ctrl_name + "_Grp", em=True)
    cmds.parent(offset_grp, external_grp)

    # Sposta il gruppo esterno alla posizione del joint
    cmds.xform(external_grp, ws=True, t=joint_pos)

    return ctrl


def create_controls():
    """Crea i controlli, i relativi gruppi e i parent constraint per i tre joint."""
    joints = ["TopSQS_Jnt", "MidSQS_Jnt", "LowSQS_Jnt"]
    created_controls = []

    for jnt in joints:
        # Crea il controllo per il joint e lo aggiunge alla lista
        ctrl = create_control_for_joint(jnt)
        created_controls.append(ctrl)

        # Aggiungi un parent constraint tra il controllo e il joint
        cmds.parentConstraint(ctrl, jnt, mo=True)

    # Ottieni la posizione del pivot di "LowSQS_Ctrl_Grp_Offset"
    low_pivot_pos = cmds.xform("LowSQS_Ctrl_Grp_Offset", q=True, ws=True, rp=True)

    # Imposta il pivot di "TopSQS_Ctrl_Grp_Offset" alla stessa posizione
    cmds.xform("TopSQS_Ctrl_Grp_Offset", ws=True, piv=low_pivot_pos)


# FASE 7: Creo la curva per lo squash totale
def create_cubic_curve():
    """Crea una curva EP Cubica basata sulla posizione di tre joint specificati e la nomina 'SQS_cv'."""

    # Ottieni le posizioni dei joint
    low_jnt_pos = cmds.xform("LowSQS_Jnt", q=True, ws=True, t=True)
    mid_jnt_pos = cmds.xform("MidSQS_Jnt", q=True, ws=True, t=True)
    top_jnt_pos = cmds.xform("TopSQS_Jnt", q=True, ws=True, t=True)

    # Crea la curva
    curve = cmds.curve(d=2, p=[low_jnt_pos, mid_jnt_pos, top_jnt_pos], n="SQS_cv")

    # Ricostruisci la curva come curva cubica con 8 spans
    cmds.rebuildCurve(curve, d=3, s=8, replaceOriginal=True)

    return curve


# FASE 8: Skinno la curva dello Squash
def bind_skin_to_curve():
    """Esegue il bind skin della curva 'SQS_cv' con i joint specificati."""

    # Seleziona la curva e i joint
    cmds.select(["SQS_cv", "TopSQS_Jnt", "MidSQS_Jnt", "LowSQS_Jnt"])

    # Esegue il bind skin
    cmds.skinCluster(toSelectedBones=True)


# FASE 9: Creo un Wire deformer dalla curva totale a quelle Up e Low dello SQS
def create_single_wire_deformer():
    """Crea un singolo Wire deformer che utilizza 'SQS_cv' per deformare 'Low_SQS_cv' e 'Up_SQS_cv'."""

    # Crea il wire deformer usando 'SQS_cv' come base
    wire_deformer = cmds.wire(["Low_SQS_cv", "Up_SQS_cv"], w="SQS_cv", dds=[(0, 9999)], n="SQS_Wire")[0]

    # Imposta lo scale del wire a 0
    cmds.setAttr(wire_deformer + ".scale[0]", 0)


# FASE 10: Calcolo squash and stretch

def setup_eye_sqs():
    # Nome delle shape delle curve di input
    curve_L_shape_name = "Low_SQS_cvShape"
    curve_R_shape_name = "Up_SQS_cvShape"

    # Funzioni di supporto per creare nodi e collegamenti
    def create_curve_info_node(name, shape_name):
        node = cmds.createNode("curveInfo", name=name)
        cmds.connectAttr(shape_name + ".worldSpace[0]", node + ".inputCurve")
        return node

    def create_multiply_divide_node(name, operation):
        node = cmds.createNode("multiplyDivide", name=name)
        cmds.setAttr(node + ".operation", operation)
        return node

    def setup_curve(curve_shape_name, side):
        # Crea il nodo CurveInfo
        curve_info_node = create_curve_info_node("CI_" + side + "_SQS", curve_shape_name)

        # Crea e configura il nodo MultiplyDivide (Distance)
        multiply_divide_distance = create_multiply_divide_node("MD_" + side + "_SQS_Distance", 2)
        cmds.connectAttr(curve_info_node + ".arcLength", multiply_divide_distance + ".input1X")

        input1X_value = cmds.getAttr(curve_info_node + ".arcLength")
        cmds.setAttr(multiply_divide_distance + ".input2X", input1X_value)

        # Crea e configura il nodo MultiplyDivide (Power)
        multiply_divide_power = create_multiply_divide_node("MD_" + side + "_SQS_Power", 3)
        cmds.setAttr(multiply_divide_power + ".input2X", 0.5)
        cmds.connectAttr(multiply_divide_distance + ".outputX", multiply_divide_power + ".input1X")

        # Crea e configura il nodo MultiplyDivide (Compensate)
        multiply_divide_compensate = create_multiply_divide_node("MD_" + side + "_SQS_Compensate", 2)
        cmds.connectAttr(multiply_divide_power + ".outputX", multiply_divide_compensate + ".input2X")
        cmds.setAttr(multiply_divide_compensate + ".input1X", 1)

        return multiply_divide_distance, multiply_divide_compensate

    # Configura le curve sinistra e destra
    md_node_L, mdc_node_L = setup_curve(curve_L_shape_name, "Low")
    md_node_R, mdc_node_R = setup_curve(curve_R_shape_name, "Up")

    # Connessioni ai joint, aggiornati con i nuovi nomi
    joints_to_connect_L = ["SQSLow_01_jnt", "SQSLow_02_jnt", "SQSLow_03_jnt", "SQSLow_04_jnt"]
    joints_to_connect_R = ["SQSUp_01_jnt", "SQSUp_02_jnt", "SQSUp_03_jnt", "SQSUp_04_jnt"]

    for joint_name in joints_to_connect_L:
        cmds.connectAttr(md_node_L + ".outputX", joint_name + ".scaleY")
        cmds.connectAttr(mdc_node_L + ".outputX", joint_name + ".scaleX")
        cmds.connectAttr(mdc_node_L + ".outputX", joint_name + ".scaleZ")

    for joint_name in joints_to_connect_R:
        cmds.connectAttr(md_node_R + ".outputX", joint_name + ".scaleY")
        cmds.connectAttr(mdc_node_R + ".outputX", joint_name + ".scaleX")
        cmds.connectAttr(mdc_node_R + ".outputX", joint_name + ".scaleZ")

    print("Configurazione completata per SQS occhi.")


# FASE 11: Creazione MasterSQS
def create_and_setup_control():
    # Crea il controllo e i gruppi
    ctrl = cmds.circle(name="MasterSqs_Ctrl", nr=(0, 1, 0))[0]
    grp_offset = cmds.group(name="MasterSqs_Ctrl_Grp_Offset", em=True)
    grp = cmds.group(name="MasterSqs_Ctrl_Grp", em=True)

    # Parent il controllo al gruppo offset e poi il gruppo offset al gruppo
    cmds.parent(ctrl, grp_offset)
    cmds.parent(grp_offset, grp)

    # Matcha la posizione del controllo con il locator
    locator = "LOC_SQSUp_04"
    if cmds.objExists(locator):
        position = cmds.xform(locator, q=True, ws=True, t=True)
        cmds.xform(grp, ws=True, t=position)
    else:
        print("Locator {} non trovato.".format(locator))

    # Sposta il gruppo 'MasterSqs_Ctrl' in alto di 10 unitÃ  sull'asse Y
    cmds.move(0, 10, 0, grp, relative=True, worldSpace=True)

    print("Controllo e gruppi creati e configurati.")

def posiziona_controlli():
    # Definisce i nomi degli oggetti
    driver_ctrl_grp = "MouthMaster_Driver_Ctrl_Grp"
    driver_locator = "LOC_Mouth_Master_Driver"
    master_ctrl_grp = "MouthMaster_Ctrl_Grp_static"
    master_locator = "LOC_Mouth_Master"

    # Ottiene la posizione del locator del driver
    driver_pos = cmds.xform(driver_locator, query=True, worldSpace=True, translation=True)

    # Imposta la posizione del gruppo di controllo del driver alla posizione del locator
    cmds.xform(driver_ctrl_grp, worldSpace=True, translation=driver_pos)

    # Ottiene la posizione del locator master
    master_pos = cmds.xform(master_locator, query=True, worldSpace=True, translation=True)

    # Imposta la posizione del gruppo di controllo master alla posizione del locator
    cmds.xform(master_ctrl_grp, worldSpace=True, translation=master_pos)

    print("Le posizioni sono state aggiornate correttamente.")


# FASE 12: nodi Multiply 
def setup_multiply_divide_nodes():
    ctrl_name = "MasterSqs_Ctrl"
    mid_grp_offset_name = "MidSQS_Ctrl_Grp_Offset"
    top_grp_offset_name = "TopSQS_Ctrl_Grp_Offset"

    # Verifica se esistono il controllo e i gruppi
    if not cmds.objExists(ctrl_name):
        print("Controllo {} non trovato.".format(ctrl_name))
        return

    if not cmds.objExists(mid_grp_offset_name):
        print("Gruppo {} non trovato.".format(mid_grp_offset_name))
        return

    if not cmds.objExists(top_grp_offset_name):
        print("Gruppo {} non trovato.".format(top_grp_offset_name))
        return

    # Crea il nodo MultiplyDivide per la traslazione, con input da Translate X collegato a tutti gli input1
    md_trans_node = cmds.createNode("multiplyDivide", name="MD_MidSQS_Trans_SLIDER")
    cmds.connectAttr(ctrl_name + ".translateX", md_trans_node + ".input1X")
    cmds.connectAttr(ctrl_name + ".translateX", md_trans_node + ".input1Y")
    cmds.connectAttr(ctrl_name + ".translateX", md_trans_node + ".input1Z")
    cmds.setAttr(md_trans_node + ".input2", 0.1, -1.5, 0, type="double3")
    cmds.connectAttr(md_trans_node + ".outputX", mid_grp_offset_name + ".translateX")
    cmds.connectAttr(md_trans_node + ".outputY", mid_grp_offset_name + ".rotateZ")

    # Crea il nodo MultiplyDivide per la rotazione, con input da Translate Z collegato a tutti gli input1
    md_rot_node = cmds.createNode("multiplyDivide", name="MD_MidSQS_Rot_SLIDER")
    cmds.connectAttr(ctrl_name + ".translateZ", md_rot_node + ".input1X")
    cmds.connectAttr(ctrl_name + ".translateZ", md_rot_node + ".input1Y")
    cmds.connectAttr(ctrl_name + ".translateZ", md_rot_node + ".input1Z")
    cmds.setAttr(md_rot_node + ".input2", 0.15, 1.15, 0, type="double3")
    cmds.connectAttr(md_rot_node + ".outputX", mid_grp_offset_name + ".translateZ")
    cmds.connectAttr(md_rot_node + ".outputY", mid_grp_offset_name + ".rotateX")

    # Crea il nodo MultiplyDivide per la traslazione Y, con input da Translate Y collegato a tutti gli input1
    md_transY_node = cmds.createNode("multiplyDivide", name="MD_SQS_TransY_SLIDER")
    cmds.connectAttr(ctrl_name + ".translateY", md_transY_node + ".input1X")
    cmds.connectAttr(ctrl_name + ".translateY", md_transY_node + ".input1Y")
    cmds.connectAttr(ctrl_name + ".translateY", md_transY_node + ".input1Z")
    cmds.setAttr(md_transY_node + ".input2", 0.2, 0.5, 1, type="double3")
    cmds.connectAttr(md_transY_node + ".outputX", mid_grp_offset_name + ".translateY")
    cmds.connectAttr(md_transY_node + ".outputY", top_grp_offset_name + ".translateY")
    
    # Crea un aim constraint da "MasterSqs_Ctrl" verso "TopSQS_Ctrl_Grp_Offset"
    cmds.aimConstraint(ctrl_name, top_grp_offset_name, aimVector=[0, 1, 0], upVector=[0, 1, 0], worldUpType="vector", worldUpVector=[0, 1, 0], mo=True)

    print("MasterSQS configurato.")

# FASE 12BIS: creo New Controls che vanno in giro in scena

def create_head_controls():
    """Crea controlli con prefisso 'Head_' e aggiunge gruppi padre con suffissi '_Grp', '_Grp_Matrix' e '_Grp_Position'."""
    base_ctrls = ["TopSQS_Ctrl", "MidSQS_Ctrl", "LowSQS_Ctrl", "MasterSqs_Ctrl"]
    created_controls = []

    for ctrl in base_ctrls:
        # Nomi per il nuovo controllo e i gruppi
        new_ctrl_name = "Head_" + ctrl
        new_position_group_name = new_ctrl_name + "_Grp_Position"
        new_matrix_group_name = new_ctrl_name + "_Grp_Matrix"
        new_group_name = new_ctrl_name + "_Grp"

        # Crea i gruppi
        group = cmds.group(em=True, name=new_group_name)
        matrix_group = cmds.group(em=True, name=new_matrix_group_name)
        position_group = cmds.group(em=True, name=new_position_group_name)

        # Crea il controllo (ad esempio, un cerchio NURBS)
        control = cmds.circle(name=new_ctrl_name)[0]

        # Organizza la gerarchia dei gruppi e del controllo
        cmds.parent(control, position_group)
        cmds.parent(position_group, matrix_group)
        cmds.parent(matrix_group, group)

        # Matcha la posizione del gruppo esterno con quella del controllo base
        pos = cmds.xform(ctrl, q=True, ws=True, t=True)
        cmds.xform(group, ws=True, t=pos)

        # Aggiungi il controllo e i gruppi alla lista dei controlli creati
        created_controls.append((control, position_group, matrix_group, group))

    # Matcha il pivot di "Head_TopSQS_Ctrl_Grp_Matrix" con la posizione di "TopSQS_Ctrl_Grp_Offset"
    offset_pos = cmds.xform("TopSQS_Ctrl_Grp_Offset", q=True, ws=True, rp=True)
    cmds.xform("Head_TopSQS_Ctrl_Grp_Matrix", ws=True, piv=offset_pos)
       
    # Ottieni il valore di traslazione Z da "LOC_SQS_Z"
    z_value = cmds.getAttr("LOC_SQS_Z.translateZ")

    # Imposta il valore di traslazione Z sui gruppi "_Grp_Position"
    for ctrl, pos_group, mat_group, group in created_controls:
        cmds.setAttr(pos_group + ".translateZ", z_value)

    return created_controls
        
def connect_controls():
    """ Connette i controlli 'Head_*SQS_Ctrl' con '*SQS_Ctrl' attraverso nodi Multiply/Divide. """
    head_controls = ["Head_TopSQS_Ctrl", "Head_MidSQS_Ctrl", "Head_LowSQS_Ctrl", "Head_MasterSqs_Ctrl"]
    base_controls = ["TopSQS_Ctrl", "MidSQS_Ctrl", "LowSQS_Ctrl", "MasterSqs_Ctrl"]

    for head_ctrl, base_ctrl in zip(head_controls, base_controls):
        # Crea e configura nodi Multiply/Divide per translate e rotate
        md_translate_node = cmds.createNode('multiplyDivide', n="MD_" + head_ctrl + "_translate")
        md_rotate_node = cmds.createNode('multiplyDivide', n="MD_" + head_ctrl + "_rotate")

        cmds.setAttr(md_translate_node + ".input2", 0.5, 0.5, 0.5)
        cmds.setAttr(md_rotate_node + ".input2", 0.5, 0.5, 0.5)

        # Connette le trasformazioni di translate e rotate ai nodi Multiply/Divide
        cmds.connectAttr(head_ctrl + ".translate", md_translate_node + ".input1", f=True)
        cmds.connectAttr(head_ctrl + ".rotate", md_rotate_node + ".input1", f=True)

        # Connette i nodi Multiply/Divide ai controlli di base
        cmds.connectAttr(md_translate_node + ".output", base_ctrl + ".translate", f=True)
        cmds.connectAttr(md_rotate_node + ".output", base_ctrl + ".rotate", f=True)

        # Se necessario, collega anche la scala direttamente
        cmds.connectAttr(head_ctrl + ".scale", base_ctrl + ".scale", f=True)

def match_position_to_LOC_SQS_Y():
    target_obj = "Head_MasterSqs_Ctrl_Grp_Position"
    source_obj = "LOC_SQS_Y"

    # Azzera le coordinate di traslazione di Head_MasterSqs_Ctrl_Grp_Position
    cmds.setAttr(target_obj + ".translateX", 0)
    cmds.setAttr(target_obj + ".translateY", 0)
    cmds.setAttr(target_obj + ".translateZ", 0)

    # Ottieni i valori di traslazione X, Y, Z da LOC_SQS_Y
    translate_x = cmds.getAttr(source_obj + ".translateX")
    translate_y = cmds.getAttr(source_obj + ".translateY")
    translate_z = cmds.getAttr(source_obj + ".translateZ")

    # Imposta i valori di traslazione X, Y, Z su Head_MasterSqs_Ctrl_Grp_Position
    cmds.setAttr(target_obj + ".translateX", translate_x)
    cmds.setAttr(target_obj + ".translateY", translate_y)
    cmds.setAttr(target_obj + ".translateZ", translate_z)
    
    
# FASE 13: replace shapes
def replace_and_remove(source_ctrl, target_ctrl):
    # Verifica se i due controlli esistono
    if not cmds.objExists(source_ctrl):
        cmds.warning("Il controllo sorgente '{}' non esiste.".format(source_ctrl))
        return
    
    if not cmds.objExists(target_ctrl):
        cmds.warning("Il controllo di destinazione '{}' non esiste.".format(target_ctrl))
        return

    # Ottieni e elimina le shape esistenti nel controllo di destinazione
    target_shapes = cmds.listRelatives(target_ctrl, shapes=True)
    if target_shapes:
        cmds.delete(target_shapes)

    # Ottieni la shape del controllo sorgente
    source_shapes = cmds.listRelatives(source_ctrl, shapes=True)
    if not source_shapes:
        cmds.warning("Il controllo sorgente '{}' non ha shape associate.".format(source_ctrl))
        return

    # Sposta ogni shape del controllo sorgente al controllo di destinazione
    for source_shape in source_shapes:
        cmds.parent(source_shape, target_ctrl, r=True, shape=True)

    # Trova il gruppo padre del controllo sorgente, se esiste
    source_parent = cmds.listRelatives(source_ctrl, parent=True)

    # Elimina il controllo sorgente
    cmds.delete(source_ctrl)

    # Elimina anche il gruppo padre del controllo sorgente, se non ha altri figli
    if source_parent and not cmds.listRelatives(source_parent[0], children=True):
        cmds.delete(source_parent)

    cmds.warning("Shape sostituita con successo da '{}' a '{}'.".format(source_ctrl, target_ctrl))

def process_multiple_pairs(control_pairs):
    # Esegui la funzione replace_and_remove per ogni coppia
    for source, target in control_pairs:
        replace_and_remove(source, target)

# Elenco delle coppie di controlli da sostituire
control_pairs = [
    ("TopSQS_Ctrl_REF", "Head_TopSQS_Ctrl"),
    ("MidSQS_Ctrl_REF", "Head_MidSQS_Ctrl"),
    ("LowSQS_Ctrl_REF", "Head_LowSQS_Ctrl"),
    ("HeadSqs_Ctrl_REF", "Head_MasterSqs_Ctrl")
    # Aggiungi altre coppie qui
]

# FASE 14: Connessione tra i gruppi offset del MasterSqs
def connect_groups():
    # Dizionario che mappa i gruppi sorgente ai gruppi destinazione
    group_mappings = {
        "TopSQS_Ctrl_Grp_Offset": "Head_TopSQS_Ctrl_Grp_Matrix",
        "MidSQS_Ctrl_Grp_Offset": "Head_MidSQS_Ctrl_Grp_Matrix"
    }

    for source, target in group_mappings.items():
        # Connetti le trasformazioni di translate, rotate e scale
        cmds.connectAttr(source + ".translate", target + ".translate", f=True)
        cmds.connectAttr(source + ".rotate", target + ".rotate", f=True)
        cmds.connectAttr(source + ".scale", target + ".scale", f=True)


def lock_and_hide_visibility(control):
    if cmds.objExists(control):
        # Blocca l'attributo 'visibility'
        cmds.setAttr(control + ".visibility", lock=True, keyable=False, channelBox=False)


def lock_and_hide_rotate(control):
    if cmds.objExists(control):
        # Blocca l'attributo 'scale'
        for attr in ('x', 'y', 'z'):
            cmds.setAttr("{}.r{}".format(control, attr), lock=True, keyable=False, channelBox=False)

            
def lock_and_hide_scale(control):
    if cmds.objExists(control):
        # Blocca l'attributo 'scale'
        for attr in ('x', 'y', 'z'):
            cmds.setAttr("{}.s{}".format(control, attr), lock=True, keyable=False, channelBox=False)



# FASE 15: Riorganizzazione e pulizia del file
def organize_and_clean():
    # Creazione di gruppi per gli elementi da organizzare
    utility_grp = cmds.group(["Low_SQS_cv", "Up_SQS_cv", "SQS_cv", "SQS_cvBaseWire", "Low_SQS_cv_ikh", "Up_SQS_cv_ikh"], name="SQS_Utilities_Grp")
    joints_grp = cmds.group(["SQSLow_01_jnt", "SQSUp_01_jnt", "TopSQS_Jnt", "MidSQS_Jnt", "LowSQS_Jnt", "ZeroSQS_Jnt"], name="SQS_Joints_Grp")
    controls_grp_static = cmds.group(["TopSQS_Ctrl_Grp", "MidSQS_Ctrl_Grp", "LowSQS_Ctrl_Grp", "MasterSqs_Ctrl_Grp"], name="SQS_Controls_Grp_Static")
    controls_grp = cmds.group(["Head_TopSQS_Ctrl_Grp", "Head_MidSQS_Ctrl_Grp", "Head_LowSQS_Ctrl_Grp", "Head_MasterSqs_Ctrl_Grp"], name="HeadSQS_Controls_Grp")
    rig_grp = cmds.group(["SQS_Utilities_Grp", "SQS_Joints_Grp", "SQS_Controls_Grp_Static"], name="SQS_RIG_Grp_Static")
    face_grp = cmds.group(["SQS_RIG_Grp_Static", "MouthMaster_Driver_Ctrl_Grp", "MouthMaster_Ctrl_Grp_static"], name="Face_RIG_Grp")

    # Nascondi i gruppi
    cmds.setAttr(utility_grp + ".visibility", 0)
    cmds.setAttr(joints_grp + ".visibility", 0)
    cmds.setAttr(controls_grp_static + ".visibility", 0)
    
    # Duplicare "MouthMaster_Ctrl_Grp_static"
    duplicated_grp = cmds.duplicate("MouthMaster_Ctrl_Grp_static", renameChildren=True)[0]

    # Rinominare il gruppo duplicato e lo imparento nello SQS
    all_descendants = cmds.listRelatives(duplicated_grp, allDescendents=True, type='transform') or []
    for obj in [duplicated_grp] + all_descendants:
        # Rimuovere il suffisso "_Static" se presente
        new_name = obj.replace("_static", "")
        new_name = new_name.replace("1", "")
        cmds.rename(obj, new_name)

    cmds.parent("MouthMaster_Ctrl_Grp", "Head_SQSLow_03_jnt")
    
    # Crea un Aim Constraint con Object Rotation Up
    cmds.select("MouthMaster_Ctrl_static", r=True)
    cmds.select("MouthMaster_Driver_Ctrl_Matrix_Grp", add=True)
    cmds.aimConstraint(
        mo=True, 
        weight=1, 
        aimVector=(1, 0, 0), 
        upVector=(0, 1, 0), 
        worldUpType="objectrotation",  # Imposta Object Rotation Up
        worldUpObject="Head_jnt",  # Specifica Head_jnt come World Up Object
        worldUpVector=(0, 1, 0)
    )


    # Elimina i gruppi dei locator e controlli REF
    for grp in ["LOCS_SQS_Grp", "SQS_Contorls_Grp_REF"]:
        if cmds.objExists(grp):
            cmds.delete(grp)
    
    # Trova ed elimina tutti i nodi di tweak
    tweak_nodes = cmds.ls(type='tweak')
    if tweak_nodes:
        cmds.delete(tweak_nodes)
        
    # Impostare a 0 la visibility di "MouthMaster_Driver_Ctrl_Grp" e "MouthMaster_Ctrl_Grp_static"
    for grp in ["MouthMaster_Driver_Ctrl_Grp", "MouthMaster_Ctrl_Grp_static"]:
        cmds.setAttr(grp + ".visibility", 0)
        
    attrs_to_connect = ['translateX', 'translateY', 'translateZ', 'rotateZ']
    for attr in attrs_to_connect:
        source_attr = "MouthMaster_Ctrl." + attr
        target_attr = "MouthMaster_Ctrl_static." + attr
        cmds.connectAttr(source_attr, target_attr, force=True)
    cmds.connectAttr("MouthMaster_Ctrl.Up_Down", "MouthMaster_Ctrl_static.Up_Down", force=True)
    cmds.connectAttr("MouthMaster_Ctrl_static.rotateZ", "MouthMaster_Driver_Ctrl.rotateZ", force=True)
    cmds.connectAttr("MouthMaster_Ctrl_static.Up_Down", "MouthMaster_Driver_Ctrl.translateY", force=True)
    cmds.connectAttr("MouthMaster_Ctrl.translateZ", "MouthMaster_Driver_Ctrl.translateZ", force=True)

    # Lista dei controlli per lock and hide
    controls_to_lock_hide = ["Head_TopSQS_Ctrl", "Head_MidSQS_Ctrl", "Head_LowSQS_Ctrl", "Head_MasterSqs_Ctrl"]

    # Applica lock and hide a ogni controllo
    for control in controls_to_lock_hide:
        lock_and_hide_visibility(control)
        lock_and_hide_scale(control)
        if control == "Head_MasterSqs_Ctrl":
            lock_and_hide_rotate(control)
        if control == "Head_MidSQS_Ctrl":
            cmds.setAttr("{}.ry".format(control), lock=True, keyable=False, channelBox=False)

    # Stampa un messaggio a conferma
    print("Gruppi creati e nascosti con successo!")
 
def cleanup_oldrig():

    cmds.joint(n = "Head_SQS_Driver_jnt")
    cmds.parent("Head_SQSUp_01_jnt", "Head_SQS_Driver_jnt")
    cmds.parent("Head_SQSLow_01_jnt", "Head_SQS_Driver_jnt")
    cmds.parent("Head_SQS_Driver_jnt", "Head_jnt")
    cmds.setAttr("Head_SQS_Driver_jnt.segmentScaleCompensate", 0)
    
    
    # "HeadSQS_Controls_Grp" diventa figlio di "JNT_Head"
    if cmds.objExists("HeadSQS_Controls_Grp") and cmds.objExists("JNT_Head"):
        cmds.parent("HeadSQS_Controls_Grp", "JNT_Head")
        print("Parented 'HeadSQS_Controls_Grp' to 'JNT_Head'.")


    if cmds.objExists("MouthMaster_Ctrl_Grp"):
        cmds.delete("MouthMaster_Ctrl_Grp")
        print("Deleted MouthMaster_Ctrl_Grp")

    # "MouthMaster_Ctrl_Grp_static" e "MouthMaster_Driver_Ctrl_Grp" diventano figli di "Head_SQSLow_03_jnt"
    if cmds.objExists("MouthMaster_Ctrl_Grp_static") and cmds.objExists("Head_SQSLow_03_jnt"):
        cmds.parent("MouthMaster_Ctrl_Grp_static", "Head_SQSLow_03_jnt")
        print("Parented 'MouthMaster_Ctrl_Grp_static' to 'Head_SQSLow_03_jnt'.")

    if cmds.objExists("MouthMaster_Driver_Ctrl_Grp") and cmds.objExists("Head_SQSLow_03_jnt"):
        cmds.parent("MouthMaster_Driver_Ctrl_Grp", "Head_SQSLow_03_jnt")
        print("Parented 'MouthMaster_Driver_Ctrl_Grp' to 'Head_SQSLow_03_jnt'.")

    # Imposta "segmentScaleCompensate" a 0 per "Head_SQSLow_01_jnt" e "Head_SQSUp_01_jnt"
    joints_to_disable_segment_scale = ["Head_SQSLow_01_jnt", "Head_SQSUp_01_jnt"]
    for joint in joints_to_disable_segment_scale:
        if cmds.objExists(joint):
            cmds.setAttr(f"{joint}.segmentScaleCompensate", 0)
            print(f"Set {joint}.segmentScaleCompensate to 0")
            

    
    # Rendere "HeadSQS_Controls_Grp", "SQS_Controls_Grp_Static", "Head_SQSLow_01_jnt", "Head_SQSUp_01_jnt",
    # "TopSQS_Jnt", "MidSQS_Jnt", "LowSQS_Jnt", "ZeroSQS_Jnt" figli di "Head_jnt"
    elements_to_parent = [
        "HeadSQS_Controls_Grp", 
    ]
    
    for element in elements_to_parent:
        if cmds.objExists(element) and cmds.objExists("Head_jnt"):
            cmds.parent(element, "Head_jnt")
            print(f"Parented '{element}' to 'Head_jnt'.")

    # Accendere "MouthMaster_Ctrl_Grp_static"
    if cmds.objExists("MouthMaster_Ctrl_Grp_static"):
        cmds.setAttr("MouthMaster_Ctrl_Grp_static.visibility", 1)
        print("Turned on visibility for 'MouthMaster_Ctrl_Grp_static'.")
    
    # Lista degli oggetti da rinominare
    objects_to_rename = [
        "MouthMaster_Ctrl_Grp_static",
        "MouthMaster_Ctrl_Matrix_Grp_static",
        "MouthMaster_Ctrl_static",
        "UpLip_Roll_Ctrl_Grp_static",
        "UpLip_Roll_Ctrl_static",
        "LowLip_Roll_Ctrl_Grp_static",
        "LowLip_Roll_Ctrl_static"
    ]
    
    # Itera su ogni oggetto nella lista
    for obj in objects_to_rename:
        if cmds.objExists(obj):
            # Rimuovi il suffisso "_static"
            new_name = obj.replace("_static", "")
            cmds.rename(obj, new_name)
            print(f"Renamed '{obj}' to '{new_name}'")
        else:
            print(f"Object '{obj}' does not exist.")

    cmds.spaceLocator(n = "Eyelid_LOC")
    cmds.matchTransform("Eyelid_LOC", "Head_SQSUp_04_jnt")
    cmds.parent("Eyelid_LOC", "Head_SQSUp_04_jnt")
    cmds.setAttr("Eyelid_LOC.visibility", 0)



#FASE 16: implementa rotazione sull'asse Y del sistema SQS
def create_rotate_y_on_sqs(control, original_joints, head_joints):
    # crea nodo multiply con valori (.25, .50, .75)
    base_md_name = control.split("_")[1]
    md_node_name = "MD_{}_rotY".format(base_md_name)
    md_node = cmds.createNode("multiplyDivide", name=md_node_name)
    
    for attr, value in zip(('X', 'Y', 'Z'), (0.25, 0.5, 0.75)):
        cmds.setAttr("{}.input2{}".format(md_node, attr), value)

    # creo nodi plusMinus
    pm_node_list = []
    for jnt in original_joints[1:]:
        base_pm_name = jnt.split("_jnt")[0]
        pm_node_name = "PM_{}_rotY".format(base_pm_name)
        pm_node = cmds.createNode("plusMinusAverage", name=pm_node_name)
        pm_node_list.append(pm_node)

    # connessioni
    for attr in ('X', 'Y', 'Z'):
        cmds.connectAttr("{}.rotateY".format(control), "{}.input1{}".format(md_node, attr), force=True)

    for attr, orig_jnt, head_jnt, pm_node in zip(('X', 'Y', 'Z'), original_joints[1:], head_joints[1:], pm_node_list):
        cmds.connectAttr("{}.rotate.rotateY".format(orig_jnt), "{}.input3D[0].input3Dy".format(pm_node), force=True)
        cmds.connectAttr("{}.output{}".format(md_node, attr), "{}.input3D[1].input3Dy".format(pm_node), force=True)
        cmds.connectAttr("{}.output3Dy".format(pm_node), "{}.rotateY".format(head_jnt), force=True)



def main():
    # Chiamate a funzioni definite al livello superiore
     # Definizione delle catene di joint
    original_low_joint_chain = create_low_joint_chain()
    original_up_joint_chain = create_up_joint_chain()
    create_head_joint()
    head_low_joint_chain = create_low_joint_chain("Head_")
    head_up_joint_chain = create_up_joint_chain("Head_")


    # Connessioni e altre operazioni
    connect_joint_chains(original_low_joint_chain, head_low_joint_chain)
    connect_joint_chains(original_up_joint_chain, head_up_joint_chain)
    set_draw_style_to_none(head_low_joint_chain)
    set_draw_style_to_none(head_up_joint_chain)
    create_low_linear_curves()
    create_up_linear_curves() 


    # Utilizza le catene di joint corrette per la creazione degli IK Spline Handles
    create_ik_spline_handles(original_low_joint_chain, original_up_joint_chain)
    remove_generated_curves()
    create_joints()
    create_controls()
    create_cubic_curve()
    bind_skin_to_curve()
    create_single_wire_deformer()
    setup_eye_sqs()
    create_and_setup_control()
    posiziona_controlli()
    head_controls = create_head_controls()
    connect_controls()
    match_position_to_LOC_SQS_Y()
    setup_multiply_divide_nodes()
    process_multiple_pairs(control_pairs)
    connect_groups()
    organize_and_clean()

    # Implementa rotazione sull'asse Y dello SQS per i controlli 'Head_TopSQS_Ctrl' e 'Head_LowSQS_Ctrl'
    create_rotate_y_on_sqs("Head_LowSQS_Ctrl", original_low_joint_chain, head_low_joint_chain)
    create_rotate_y_on_sqs("Head_TopSQS_Ctrl", original_up_joint_chain, head_up_joint_chain)

    # Crea sets
    controls_set = create_controls_set("HeadSquash", [["MouthMaster_Ctrl"], ["UpLip_Roll_Ctrl"], ["LowLip_Roll_Ctrl"]])
    rename_ctrl_shapes(controls_set)
    
    cleanup_oldrig()
    
    create_joints_set("HeadSquash", head_low_joint_chain)
    create_joints_set("HeadSquash", head_up_joint_chain)
    create_controls_set("HeadSquash", head_controls)


main()
