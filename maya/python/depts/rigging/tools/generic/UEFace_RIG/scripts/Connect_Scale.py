import maya.cmds as cmds

# Nome del nodo su cui operare
node = "Head_jnt"
master_scale_attr = f"{node}.masterScale"

# Funzione per scollegare e ricollegare le connessioni
def replace_scale_connections(node, master_scale_attr):
    # Lista di attributi di scala
    scale_attrs = ['scaleX', 'scaleY', 'scaleZ']
    
    for attr in scale_attrs:
        full_attr = f"{node}.{attr}"
        
        # Trova connessioni in uscita per l'attributo corrente
        connections = cmds.listConnections(full_attr, plugs=True, source=False, destination=True)
        
        if connections:
            for connection in connections:
                # Disconnetti l'attributo corrente
                cmds.disconnectAttr(full_attr, connection)
                # Connetti masterScale all'attributo che era collegato a scaleX, scaleY, scaleZ
                cmds.connectAttr(master_scale_attr, connection)

# Esegui la funzione
replace_scale_connections(node, master_scale_attr)

print(f"Le connessioni per {node} sono state sostituite con {master_scale_attr}.")