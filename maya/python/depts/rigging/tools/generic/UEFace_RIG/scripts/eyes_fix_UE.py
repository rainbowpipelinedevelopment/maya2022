# -*- coding: utf-8 -*-

import maya.cmds as cmds

# Funzione per creare un joint nella posizione di un locator
def create_joint_at_locator(locator_name):
    joint = cmds.joint()
    cmds.matchTransform(joint, locator_name, position=True, rotation=True)
    return joint

# Funzione per creare un joint e un figlio nella stessa posizione
def create_joint_with_child(locator_name):
    parent_joint = create_joint_at_locator(locator_name)
    cmds.select(clear=True)  # Deseleziona per evitare parentaggi automatici
    child_joint = create_joint_at_locator(locator_name)
    cmds.parent(child_joint, parent_joint)
    return parent_joint, child_joint

# Funzione per creare un joint nella posizione di un locator e metterlo come figlio di un altro joint
def create_joint_and_parent(locator_name, parent_joint):
    cmds.select(clear=True)  # Deseleziona per evitare parentaggi automatici
    joint = create_joint_at_locator(locator_name)
    cmds.parent(joint, parent_joint)
    return joint

# Funzione per disattivare il segment scale compensate su un joint e tutti i suoi figli
def disable_segment_scale_compensate(joint):
    cmds.setAttr(joint + ".segmentScaleCompensate", 0)
    children = cmds.listRelatives(joint, children=True, type="joint")
    if children:
        for child in children:
            disable_segment_scale_compensate(child)

# Creare e configurare i joint per il lato sinistro
cmds.select(clear=True)
left_eyemover_parent, left_eyemover_child = create_joint_with_child("LOC_L_Eyemover")
left_eye_joint = create_joint_and_parent("LOC_L_Eye", left_eyemover_child)

# Creare e configurare i joint per il lato destro
cmds.select(clear=True)
right_eyemover_parent, right_eyemover_child = create_joint_with_child("LOC_R_Eyemover")
right_eye_joint = create_joint_and_parent("LOC_R_Eye", right_eyemover_child)

# Rinomina i joint dopo averli posizionati correttamente
cmds.rename(left_eyemover_parent, "L_Eyemover_position")
cmds.rename(left_eyemover_child, "L_Eyemover_jnt_child")
cmds.rename(left_eye_joint, "L_Eye_driver")

cmds.rename(right_eyemover_parent, "R_Eyemover_position")
cmds.rename(right_eyemover_child, "R_Eyemover_jnt_child")
cmds.rename(right_eye_joint, "R_Eye_driver")

# Freeza le rotazioni di "R_Eyemover_jnt_child" e "R_Eyemover_position"
cmds.makeIdentity("R_Eyemover_jnt_child", apply=True, rotate=True)
cmds.makeIdentity("R_Eyemover_position", apply=True, rotate=True)

# Parentaggio dei gruppi e joint
cmds.parent("L_Eye_jnt_Grp", "L_Eye_driver")
cmds.parent("R_Eye_jnt_Grp", "R_Eye_driver")

cmds.parent("L_Iris_jnt", "L_Eye_driver")
cmds.parent("R_Iris_jnt", "R_Eye_driver")

cmds.parent("L_Pupil_jnt", "L_Iris_jnt")
cmds.parent("R_Pupil_jnt", "R_Iris_jnt")

cmds.parent("L_Spec_jnt", "L_Iris_jnt")
cmds.parent("R_Spec_jnt", "R_Iris_jnt")

# Parent constraint e scale constraint dei joint dai controlli
cmds.parentConstraint("L_Iris_Ctrl", "L_Iris_jnt", maintainOffset=True)
cmds.scaleConstraint("L_Iris_Ctrl", "L_Iris_jnt", maintainOffset=True)
cmds.parentConstraint("R_Iris_Ctrl", "R_Iris_jnt", maintainOffset=True)
cmds.scaleConstraint("R_Iris_Ctrl", "R_Iris_jnt", maintainOffset=True)

cmds.parentConstraint("L_Pupil_Ctrl", "L_Pupil_jnt", maintainOffset=True)
cmds.scaleConstraint("L_Pupil_Ctrl", "L_Pupil_jnt", maintainOffset=True)
cmds.parentConstraint("R_Pupil_Ctrl", "R_Pupil_jnt", maintainOffset=True)
cmds.scaleConstraint("R_Pupil_Ctrl", "R_Pupil_jnt", maintainOffset=True)

cmds.parentConstraint("L_Spec_Ctrl", "L_Spec_jnt", maintainOffset=True)
cmds.scaleConstraint("L_Spec_Ctrl", "L_Spec_jnt", maintainOffset=True)
cmds.parentConstraint("R_Spec_Ctrl", "R_Spec_jnt", maintainOffset=True)
cmds.scaleConstraint("R_Spec_Ctrl", "R_Spec_jnt", maintainOffset=True)

# Connessione diretta dei controlli su Eyemover_jnt_child
cmds.connectAttr("L_Eyeshaper_Ctrl.translate", "L_Eyemover_jnt_child.translate")
cmds.connectAttr("L_Eyeshaper_Ctrl.rotate", "L_Eyemover_jnt_child.rotate")
cmds.connectAttr("L_Eyeshaper_Ctrl.scale", "L_Eyemover_jnt_child.scale")

cmds.connectAttr("R_Eyeshaper_Ctrl.translate", "R_Eyemover_jnt_child.translate")
cmds.connectAttr("R_Eyeshaper_Ctrl.rotate", "R_Eyemover_jnt_child.rotate")
cmds.connectAttr("R_Eyeshaper_Ctrl.scale", "R_Eyemover_jnt_child.scale")

# Disattivare il segment scale compensate per i joint e i loro figli
disable_segment_scale_compensate("L_Eyemover_position")
disable_segment_scale_compensate("R_Eyemover_position")

# Parentare "L_Eyemover_position" e "R_Eyemover_position" a "Head_SQSUp_02_jnt"
cmds.parent("L_Eyemover_position", "Head_SQSUp_02_jnt")
cmds.parent("R_Eyemover_position", "Head_SQSUp_02_jnt")

# Stampa di conferma
print("Operazioni aggiuntive completate con successo.")
