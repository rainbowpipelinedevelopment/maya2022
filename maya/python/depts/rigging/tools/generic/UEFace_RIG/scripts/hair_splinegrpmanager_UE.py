import maya.cmds as cmds

def refresh_children_list(children_list_ui):
    """
    Refreshes the list of direct children displayed in the UI based on the selected groups,
    using full path names to ensure uniqueness.
    """
    cmds.textScrollList(children_list_ui, edit=True, removeAll=True)
    
    selected_groups = cmds.ls(selection=True, long=True)
    if not selected_groups:
        cmds.warning("No groups selected.")
        return
    
    for group in selected_groups:
        if cmds.objExists(group):
            children = cmds.listRelatives(group, children=True, fullPath=True) or []
            for child in children:
                cmds.textScrollList(children_list_ui, edit=True, append=child)

def rename_child_and_parent(children_list_ui, new_name_field, new_parent_name_field):
    """
    Renames a selected child object and its parent, considering namespaces and using full path names for precision.
    """
    selected_child_full_path = cmds.textScrollList(children_list_ui, query=True, selectItem=True)
    if not selected_child_full_path:
        cmds.warning("No child selected for renaming.")
        return
    
    old_child_name = selected_child_full_path[0]
    new_child_name = cmds.textField(new_name_field, query=True, text=True)
    new_parent_name = cmds.textField(new_parent_name_field, query=True, text=True)
    
    parent_path = '|'.join(old_child_name.split('|')[:-1])

    if new_child_name:
        try:
            cmds.rename(old_child_name, new_child_name)
        except RuntimeError as e:
            cmds.warning("Failed to rename child object: " + str(e))
            return
    
    if new_parent_name and cmds.objExists(parent_path):
        try:
            cmds.rename(parent_path, new_parent_name)
        except RuntimeError as e:
            cmds.warning("Failed to rename parent object: " + str(e))
            return
    
    refresh_children_list(children_list_ui)

def reparent_children(children_list_ui):
    """
    Reparents all selected children under the chosen child, using full path names to avoid ambiguity.
    """
    selected_child_full_path = cmds.textScrollList(children_list_ui, query=True, selectItem=True)
    if not selected_child_full_path:
        cmds.warning("No child selected for reparenting.")
        return
    
    selected_child_full_path = selected_child_full_path[0]
    all_children_full_paths = cmds.textScrollList(children_list_ui, query=True, allItems=True)
    
    for child_full_path in all_children_full_paths:
        if child_full_path != selected_child_full_path:
            try:
                cmds.parent(child_full_path, selected_child_full_path)
            except RuntimeError as e:
                cmds.warning(f"Failed to reparent {child_full_path}: {str(e)}")

    refresh_children_list(children_list_ui)

def create_ui():
    """
    Creates the UI for listing, renaming direct children and their parents of multiple selected groups,
    ensuring operations are done using full path names for clarity and to avoid naming conflicts.
    Named as "SplineGrps Manager". UI labels and functionalities have been updated as requested.
    Includes instructional text for users.
    """
    window_id = 'splineGrpsManagerUI'
    
    if cmds.window(window_id, exists=True):
        cmds.deleteUI(window_id)

    cmds.window(window_id, title="SplineGrps Manager", sizeable=True)
    main_layout = cmds.columnLayout(adjustableColumn=True)
    
    cmds.button(label="Refresh List", command=lambda x: refresh_children_list(children_list_ui))
    
    children_list_ui = cmds.textScrollList(allowMultiSelection=False, height=150)
    
    cmds.rowLayout(numberOfColumns=3)
    cmds.text(label="New Spline Group Name (ex: 'SplineGrp0'):")
    new_name_field = cmds.textField(placeholderText="SplineGrp0")
    cmds.setParent('..')
    
    cmds.rowLayout(numberOfColumns=3)
    cmds.text(label="New Description Name:")
    new_parent_name_field = cmds.textField(placeholderText="Description name")
    cmds.setParent('..')
    
    cmds.button(label="Rename Description and Spline Group", command=lambda x: rename_child_and_parent(children_list_ui, new_name_field, new_parent_name_field))
    
    cmds.button(label="Reparent all Splines to selected Spline Group", command=lambda x: reparent_children(children_list_ui))
    
    # Instructional Text
    instructions = (
        "Please subscribe to youtube.com/@AG_Groom for more amazing free tools and tutorials!\n\n"
        "Instructions:\n"
        "- After reimporting your groom caches, simply select all the groups containing the splines you want to 'regroup and rename' in the outliner. (Highly recommend pressing CTRL+H to hide all splines in the viewport to speed up Maya's UI!) (Do not expand group trees! Keep them collapsed!)\n"
        "- With all groups selected, press the 'Refresh List' button at the top. This will display all the spline groups contained within the groups in the list.\n"
        "- Select ANY ONE of the spline groups in the list and enter new names for the respective Spline Group and Description (Group Name) into their respective text input fields.\n"
        "- Press the 'Rename Description and Spline Group' button. New Description name should be reflected in the group name in the outliner and 'list' as well.\n"
        "- Now just reselect the same Spline Group you just renamed from the list and click the 'Reparent all Splines to selected Spline Group' button. This will parent ALL splines from the selected groups to the single group (Description) and Spline Group.\n"
        "- You can repeat this process for Spline Groups 1, 2, 3, etc.\n"
        "- Once finished, your groups should be prepared to run through the scripts provided by Epic to prepare your groom for UE5."
    )
    cmds.scrollField(editable=False, wordWrap=True, text=instructions, height=150)
    
    refresh_children_list(children_list_ui)
    
    cmds.showWindow(window_id)

create_ui()