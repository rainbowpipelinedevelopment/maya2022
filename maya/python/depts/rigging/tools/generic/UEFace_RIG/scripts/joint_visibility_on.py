import maya.cmds as cmds

def set_joint_draw_style_to_bone():
    # Ottieni tutti i joint presenti nella scena
    all_joints = cmds.ls(type='joint')
    
    # Itera attraverso ciascun joint
    for joint in all_joints:
        # Seleziona il joint
        cmds.select(joint, replace=True)
        
        # Imposta il Draw Style su "Bone"
        cmds.setAttr(f"{joint}.drawStyle", 1)  # 1 corrisponde a "Bone"

    # Deseleziona tutti gli oggetti per pulizia
    cmds.select(clear=True)

# Esegui la funzione
set_joint_draw_style_to_bone()