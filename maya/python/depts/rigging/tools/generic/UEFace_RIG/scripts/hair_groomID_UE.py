from maya import cmds

attr_name = 'groom_group_id'

# NOTE: change the following names to reflect your node's scene.
groups = ['EyelashesExport2|SplineGrp0']

for groom_group_id, group_name in enumerate(groups):

    # get curves under xgGroom
    curves = cmds.listRelatives(group_name, ad=True, type='nurbsCurve')

    # tag group with group id
    cmds.addAttr(group_name, longName=attr_name, attributeType='short', defaultValue=groom_group_id, keyable=True)

    # add attribute scope
    # forces Maya's alembic to export data as GeometryScope::kConstantScope
    cmds.addAttr(group_name, longName='{}_AbcGeomScope'.format(attr_name), dataType='string', keyable=True)
    cmds.setAttr('{}.{}_AbcGeomScope'.format(group_name, attr_name), 'con', type='string')