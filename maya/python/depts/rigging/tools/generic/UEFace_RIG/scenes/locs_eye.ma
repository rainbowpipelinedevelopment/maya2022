//Maya ASCII 2020 scene
//Name: locs_eye.ma
//Last modified: Mon, Jan 15, 2024 04:47:56 PM
//Codeset: 1252
requires maya "2020";
requires -nodeType "VRaySettingsNode" -dataType "VRaySunParams" -dataType "vrayFloatVectorData"
		 -dataType "vrayFloatVectorData" -dataType "vrayIntData" "vrayformaya" "5";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t pal;
fileInfo "application" "maya";
fileInfo "product" "Maya 2020";
fileInfo "version" "2020";
fileInfo "cutIdentifier" "202011110415-b1e20b88e2";
fileInfo "osv" "Microsoft Windows 10 Technical Preview  (Build 19045)\n";
fileInfo "UUID" "532E52A4-427D-176A-B527-00991E558AEC";
fileInfo "vrayBuild" "5.20.03 31979 09ccb64";
createNode transform -s -n "persp";
	rename -uid "7E37A33D-483E-9160-0743-21A86AA2A599";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 19.969226915263363 194.08805313051823 66.947484266406576 ;
	setAttr ".r" -type "double3" -27.338352729608864 16.60000000000176 8.2971923186686462e-16 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "470751CF-443C-28B6-BCBF-038364DC58F1";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 81.054778363666983;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 11.493145164370306 156.42662915906641 3.8579177627266548 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr -s 2 ".b";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "84B4E98D-453C-41F2-96B5-DC98680A4089";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "60228CCF-43CF-8A75-F5AD-0BA570EE0214";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "61F90A2D-4053-13BF-71A4-41AF10D452D4";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "A0D96949-4179-4780-3E27-75B14818C616";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "11761C3F-4015-2201-8A94-D3BA3F68A0E9";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "EF1C9D5B-4B68-3101-03E4-AC90B6BDA455";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "LOCS_FK_Eye_Grp";
	rename -uid "3F44318C-4FF2-40BD-3B05-C1A1A8E89D76";
createNode transform -n "LOC_R_Eye" -p "LOCS_FK_Eye_Grp";
	rename -uid "D5B2D9D6-4DBF-4770-D8E6-A98FF49F4056";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_R_EyeShape" -p "LOC_R_Eye";
	rename -uid "EA00F8CC-4657-E9DD-6081-1E8969538C47";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.5 0.5 0.5 ;
createNode transform -n "LOC_L_Eye" -p "LOCS_FK_Eye_Grp";
	rename -uid "3D17432D-4F31-75EF-6CDE-0DB378C321B6";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".t" -type "double3" 3.8775637149810791 155.56399536132813 2.5379118919372559 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_L_EyeShape" -p "LOC_L_Eye";
	rename -uid "5DA35DF4-4651-1D5C-E55E-DC80045682B3";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.5 0.5 0.5 ;
createNode transform -n "LOC_L_Eye_Iris" -p "LOCS_FK_Eye_Grp";
	rename -uid "8EE4DDD0-4A43-C8BE-7FDD-2F8555A0F8B3";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".t" -type "double3" 3.8775637149810791 155.56399536132813 4.1085713375465289 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_L_Eye_IrisShape" -p "LOC_L_Eye_Iris";
	rename -uid "07DC2D8D-4F96-8162-025F-46AF7E1EAF48";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.5 0.5 0.5 ;
createNode transform -n "LOC_L_Eye_Spec" -p "LOC_L_Eye_Iris";
	rename -uid "E7C1CDEE-4BB0-58C5-AB65-8BA86754631A";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".t" -type "double3" 0.94847085256695784 0.96067361334445422 0 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_L_Eye_SpecShape" -p "LOC_L_Eye_Spec";
	rename -uid "3AE3E827-4514-F163-E7B3-BEA27998C4C5";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.5 0.5 0.5 ;
createNode transform -n "LOC_L_Eye_Pupil" -p "LOCS_FK_Eye_Grp";
	rename -uid "38884E9C-4E3C-E5D0-34C9-4C82D7B2F813";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".t" -type "double3" 3.8775637149810791 155.56399536132813 3.4667771540528554 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_L_Eye_PupilShape" -p "LOC_L_Eye_Pupil";
	rename -uid "6E719632-4615-84FD-B676-5C8B05D9D113";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.5 0.5 0.5 ;
createNode transform -n "LOC_R_Eye_Iris" -p "LOCS_FK_Eye_Grp";
	rename -uid "6F3B0FC4-4A76-E139-DDD2-458668E44CD4";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_R_Eye_IrisShape" -p "LOC_R_Eye_Iris";
	rename -uid "00853D7E-4E8D-0180-8198-9D95763D87D4";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.5 0.5 0.5 ;
createNode transform -n "LOC_R_Eye_Spec" -p "LOC_R_Eye_Iris";
	rename -uid "AE9781E0-44A8-C8C7-7C24-B78A865E6C8E";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_R_Eye_SpecShape" -p "LOC_R_Eye_Spec";
	rename -uid "AA1FBC77-471B-E6ED-A5DF-7DBA597D6588";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.5 0.5 0.5 ;
createNode transform -n "LOC_R_Eye_Pupil" -p "LOCS_FK_Eye_Grp";
	rename -uid "0059D0BE-4A7E-A022-A064-90A4C96E4CFF";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_R_Eye_PupilShape" -p "LOC_R_Eye_Pupil";
	rename -uid "7F804833-4A24-B6B4-2485-CB8AE3810442";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.5 0.5 0.5 ;
createNode transform -n "LOC_L_Eyemover" -p "LOCS_FK_Eye_Grp";
	rename -uid "DF244C16-485D-C5B2-8593-D68894A9955F";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 21;
	setAttr ".t" -type "double3" 3.8775637149810791 153.66438751710902 3.4667770862579346 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_L_EyemoverShape" -p "LOC_L_Eyemover";
	rename -uid "787CD7BF-4E46-D336-D19A-B0B4653A47B0";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.5 0.5 0.5 ;
createNode transform -n "LOC_L_Eyeshaper" -p "LOC_L_Eyemover";
	rename -uid "B1D26A6C-4930-CBB4-3836-D2814541FC7C";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 12;
	setAttr ".t" -type "double3" 7.6054393109950134 -4.5825387076092738e-06 0 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr ".s" -type "double3" 1.0000042930672055 0.99999130384755419 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_L_EyeshaperShape" -p "LOC_L_Eyeshaper";
	rename -uid "4A2F8DCD-41DA-AA8C-E325-E0A8AA3CA4CA";
	setAttr -k off ".v";
createNode transform -n "LOC_R_Eyemover" -p "LOCS_FK_Eye_Grp";
	rename -uid "FB8D1AE3-4467-5CE8-27A6-B1B22571F359";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 21;
	setAttr ".r" -type "double3" 0 180 0 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_R_EyemoverShape" -p "LOC_R_Eyemover";
	rename -uid "69B48C64-4C00-0456-B13C-179D179D1120";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.5 0.5 0.5 ;
createNode transform -n "LOC_R_Eyeshaper" -p "LOC_R_Eyemover";
	rename -uid "1FE23AA0-4C71-2EA9-FA0E-0BBDF23E10CB";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 12;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr ".s" -type "double3" 1.0000042930672055 0.99999130384755419 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_R_EyeshaperShape" -p "LOC_R_Eyeshaper";
	rename -uid "D0983237-456A-40AE-431D-4481866456F3";
	setAttr -k off ".v";
createNode transform -n "Eyes_Contorls_Grp_REF";
	rename -uid "A095E79C-45FE-3A60-138A-EBBDAC936340";
	setAttr ".v" no;
createNode transform -n "R_eye_fk_Ctrl_Grp_REF" -p "Eyes_Contorls_Grp_REF";
	rename -uid "5016514D-4AB2-6C66-21F1-2C8CCB881F44";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".t" -type "double3" -3.0052408347413309 156.43899921739174 0.81292240317728237 ;
	setAttr ".r" -type "double3" 0 2.6751914462548618e-05 1.4920669999711128e-05 ;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999978 0.99999999999999989 ;
createNode transform -n "R_eye_fk_Ctrl_Grp_offset_REF" -p "R_eye_fk_Ctrl_Grp_REF";
	rename -uid "4D70B0EB-4286-F409-72DB-6A9CB46A52A3";
createNode transform -n "R_eye_fk_Ctrl_REF" -p "R_eye_fk_Ctrl_Grp_offset_REF";
	rename -uid "3FCBB71B-4C6E-F5F4-F6AF-BCA89A8924A4";
	addAttr -ci true -sn "EyeMover_VIS" -ln "EyeMover_VIS" -min 0 -max 1 -en "off:on" 
		-at "enum";
	addAttr -ci true -sn "Iris_Pupil_Spec_VIS" -ln "Iris_Pupil_Spec_VIS" -min 0 -max 
		1 -en "off:on" -at "enum";
	addAttr -ci true -sn "AutoEyelid" -ln "AutoEyelid" -dv 1 -min 0 -max 1 -at "double";
	addAttr -ci true -sn "Eyelashes_VIS" -ln "Eyelashes_VIS" -min 0 -max 1 -en "off:on" 
		-at "enum";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".EyeMover_VIS" 1;
	setAttr -k on ".Iris_Pupil_Spec_VIS" 1;
	setAttr -k on ".AutoEyelid";
	setAttr -k on ".Eyelashes_VIS" 1;
createNode nurbsCurve -n "R_eye_fk_Ctrl_REFShape" -p "R_eye_fk_Ctrl_REF";
	rename -uid "F44EF48D-4C17-756D-B445-A3B4A29559C4";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovc" 17;
	setAttr ".ovrgb" -type "float3" 0.47899997 1 0 ;
	setAttr ".cc" -type "nurbsCurve" 
		1 29 0 no 3
		30 0 4 12.944272 21.888544 25.888544 45.888544000000003 46.932133999999998
		 47.975695999999999 49.019289000000001 50.062885999999999 51.106444000000003 52.150039
		 53.193634000000003 54.237194000000002 55.280788999999999 56.324384000000002 57.367942999999997
		 58.411538 59.455132999999996 60.498693000000003 61.542287999999999 62.585881999999998
		 63.629441999999997 64.673036999999994 65.716632000000004 66.760191000000006 67.803787
		 68.847380000000001 69.890941999999995 70.934532000000004
		30
		-0.024494659006697006 -6.3925816926617887e-05 11.669305591446397
		-0.02449465900669523 0.48757305520053351 11.669305591446397
		-0.024494659006697006 -6.484576745341952e-05 13.763011226641611
		-0.024494659006697006 -0.48770090683438672 11.669305591446397
		-0.024494659006697006 -6.3925816926617887e-05 11.669305591446397
		-0.013065671176381008 -3.4589314943024307e-05 0.55840290084599087
		-0.013065671176381008 -0.14440741456162284 0.53889126365905737
		-0.013065671176381008 -0.27899929318343064 0.48314053797812484
		-0.013065671176381008 -0.39488642362680959 0.39485431643218172
		-0.013065671176381008 -0.48317334317362515 0.27896676718827834
		-0.013065671176381008 -0.5389230916533384 0.14437516776681955
		-0.013065671176381008 -0.55843947524620119 2.1707239801682559e-06
		-0.013065671176381008 -0.5389230916533384 -0.14437062232671447
		-0.013065671176381008 -0.48317334317362515 -0.27896250094852176
		-0.013065671176381008 -0.39488642362680959 -0.39484963139190166
		-0.013065671176381008 -0.27899929318343064 -0.48313655093871682
		-0.013065671176380075 -0.14440741456162284 -0.53888629941843302
		-0.013065671176380075 -3.4589314943024307e-05 -0.5584026830112957
		-0.013065671176380075 0.14433823593173661 -0.53888629941843302
		-0.013065671176380075 0.27893011455354461 -0.48313655093871682
		-0.013065671176380075 0.39481724499692433 -0.39484963139190166
		-0.013065671176380075 0.48310416454373994 -0.27896250094852176
		-0.013065671176380075 0.53885391302345265 -0.14437062232671447
		-0.013065671176380075 0.55837029661631632 2.1707239801682559e-06
		-0.013065671176380075 0.53885391302345265 0.14437516776681955
		-0.013065671176380075 0.48310416454373994 0.27896676718827834
		-0.013065671176381008 0.39481724499692433 0.39485431643218172
		-0.013065671176381008 0.27893011455354461 0.48314053797812484
		-0.013065671176381008 0.14433823593173661 0.53889126365905737
		-0.013065671176381008 -3.4589314943024307e-05 0.55840290084599087
		;
createNode transform -n "L_eye_fk_Ctrl_Grp_REF" -p "Eyes_Contorls_Grp_REF";
	rename -uid "D625AB31-426D-20EF-AB4E-7186AA77D219";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".t" -type "double3" 3.0052436016994983 156.43905718153269 0.81291968725073538 ;
	setAttr ".r" -type "double3" 0 2.6751914462548618e-05 1.4920669999711128e-05 ;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999967 0.99999999999999989 ;
createNode transform -n "L_eye_fk_Ctrl_Grp_offset_REF" -p "L_eye_fk_Ctrl_Grp_REF";
	rename -uid "93CBD698-4D8F-E0E9-DCA6-BA9E7E27F7FC";
createNode transform -n "L_eye_fk_Ctrl_REF" -p "L_eye_fk_Ctrl_Grp_offset_REF";
	rename -uid "D83F8379-40AB-E67A-6C23-71AD878A0782";
	addAttr -ci true -sn "EyeMover_VIS" -ln "EyeMover_VIS" -min 0 -max 1 -en "off:on" 
		-at "enum";
	addAttr -ci true -sn "Iris_Pupil_Spec_VIS" -ln "Iris_Pupil_Spec_VIS" -min 0 -max 
		1 -en "off:on" -at "enum";
	addAttr -ci true -sn "AutoEyelid" -ln "AutoEyelid" -dv 1 -min 0 -max 1 -at "double";
	addAttr -ci true -sn "Eyelashes_VIS" -ln "Eyelashes_VIS" -min 0 -max 1 -en "off:on" 
		-at "enum";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 1.7687511577023878e-05 0 2.279333108761976e-05 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".EyeMover_VIS" 1;
	setAttr -k on ".Iris_Pupil_Spec_VIS" 1;
	setAttr -k on ".AutoEyelid";
	setAttr -k on ".Eyelashes_VIS" 1;
createNode nurbsCurve -n "L_eye_fk_Ctrl_REFShape" -p "L_eye_fk_Ctrl_REF";
	rename -uid "FB5197D4-433E-E755-0B64-56971C62DDBD";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovc" 17;
	setAttr ".ovrgb" -type "float3" 0.47899997 1 0 ;
	setAttr ".cc" -type "nurbsCurve" 
		1 29 0 no 3
		30 0 4 12.944272 21.888544 25.888544 45.888544000000003 46.932133999999998
		 47.975695999999999 49.019289000000001 50.062885999999999 51.106444000000003 52.150039
		 53.193634000000003 54.237194000000002 55.280788999999999 56.324384000000002 57.367942999999997
		 58.411538 59.455132999999996 60.498693000000003 61.542287999999999 62.585881999999998
		 63.629441999999997 64.673036999999994 65.716632000000004 66.760191000000006 67.803787
		 68.847380000000001 69.890941999999995 70.934532000000004
		30
		-0.024494659006697006 -6.3925816926617887e-05 11.669305591446397
		-0.02449465900669523 0.48757305520053351 11.669305591446397
		-0.024494659006697006 -6.484576745341952e-05 13.763011226641611
		-0.024494659006697006 -0.48770090683438672 11.669305591446397
		-0.024494659006697006 -6.3925816926617887e-05 11.669305591446397
		-0.013065671176381008 -3.4589314943024307e-05 0.55840290084599087
		-0.013065671176381008 -0.14440741456162284 0.53889126365905737
		-0.013065671176381008 -0.27899929318343064 0.48314053797812484
		-0.013065671176381008 -0.39488642362680959 0.39485431643218172
		-0.013065671176381008 -0.48317334317362515 0.27896676718827834
		-0.013065671176381008 -0.5389230916533384 0.14437516776681955
		-0.013065671176381008 -0.55843947524620119 2.1707239801682559e-06
		-0.013065671176381008 -0.5389230916533384 -0.14437062232671447
		-0.013065671176381008 -0.48317334317362515 -0.27896250094852176
		-0.013065671176381008 -0.39488642362680959 -0.39484963139190166
		-0.013065671176381008 -0.27899929318343064 -0.48313655093871682
		-0.013065671176380075 -0.14440741456162284 -0.53888629941843302
		-0.013065671176380075 -3.4589314943024307e-05 -0.5584026830112957
		-0.013065671176380075 0.14433823593173661 -0.53888629941843302
		-0.013065671176380075 0.27893011455354461 -0.48313655093871682
		-0.013065671176380075 0.39481724499692433 -0.39484963139190166
		-0.013065671176380075 0.48310416454373994 -0.27896250094852176
		-0.013065671176380075 0.53885391302345265 -0.14437062232671447
		-0.013065671176380075 0.55837029661631632 2.1707239801682559e-06
		-0.013065671176380075 0.53885391302345265 0.14437516776681955
		-0.013065671176380075 0.48310416454373994 0.27896676718827834
		-0.013065671176381008 0.39481724499692433 0.39485431643218172
		-0.013065671176381008 0.27893011455354461 0.48314053797812484
		-0.013065671176381008 0.14433823593173661 0.53889126365905737
		-0.013065671176381008 -3.4589314943024307e-05 0.55840290084599087
		;
createNode transform -n "L_eye_Ctrl_Grp_REF" -p "Eyes_Contorls_Grp_REF";
	rename -uid "F5756A70-4C90-1338-7491-02B3F067ED89";
	setAttr ".t" -type "double3" 3.0052665682445965 156.43905718153587 49.999998687245842 ;
	setAttr ".r" -type "double3" 7.2317440787261664e-28 2.6751914462548625e-05 1.4920669999711128e-05 ;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "L_eye_Ctrl_REF" -p "L_eye_Ctrl_Grp_REF";
	rename -uid "F101E6B0-4061-B2E6-6317-40899269D638";
	addAttr -ci true -h true -sn "wireScale" -ln "wireScale" -min 0 -at "double";
	addAttr -ci true -h true -sn "wireType" -ln "wireType" -dt "string";
	addAttr -ci true -h true -sn "wireFacingAxis" -ln "wireFacingAxis" -at "short";
	addAttr -ci true -h true -sn "colorIndexKey" -ln "colorIndexKey" -dt "string";
	addAttr -ci true -h true -sn "colorIndexVal" -ln "colorIndexVal" -min 0 -max 255 
		-at "byte";
	addAttr -ci true -sn "eyeMoverCtrlVis" -ln "eyeMoverCtrlVis" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "Auto_Eyelid" -ln "Auto_Eyelid" -dv 1 -min 0 -max 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -5.6519887215065946e-14 -8.1619544226008665e-15 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".wireScale" 5.1973050970000001;
	setAttr ".wireType" -type "string" "leftEye_1";
	setAttr ".wireFacingAxis" 5;
	setAttr ".colorIndexKey" -type "string" "lfCtrlColor";
	setAttr ".colorIndexVal" 6;
	setAttr -l on ".eyeMoverCtrlVis" yes;
createNode nurbsCurve -n "L_eye_Ctrl_REFShape" -p "L_eye_Ctrl_REF";
	rename -uid "5A00D180-42D0-EEB8-4794-A5ABCAC480F8";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 1 0.85499263 0 ;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.9585865582746278 0.95855195146016359 3.438558766704782e-05
		8.3009383556385041e-17 1.3556115046060588 3.4385587659220592e-05
		-0.9585865582746278 0.95855195146016303 3.438558766704782e-05
		-1.3556461114205249 -3.4606814464760887e-05 3.4385587665009466e-05
		-0.9585865582746278 -0.9586211650890929 3.4385587666884743e-05
		-1.3579599814982322e-16 -1.355680718234991 3.4385587658894451e-05
		0.9585865582746278 -0.95862116508909256 3.4385587666884743e-05
		1.3556461114205249 -3.4606814465016033e-05 3.4385587665009466e-05
		0.9585865582746278 0.95855195146016359 3.438558766704782e-05
		8.3009383556385041e-17 1.3556115046060588 3.4385587659220592e-05
		-0.9585865582746278 0.95855195146016303 3.438558766704782e-05
		;
createNode transform -n "R_eye_Ctrl_Grp_REF" -p "Eyes_Contorls_Grp_REF";
	rename -uid "84B151B7-4BA2-CB77-579A-04818D27E933";
	setAttr ".t" -type "double3" -3.0052178681962753 156.43899921739484 50.000001312749738 ;
	setAttr ".r" -type "double3" 7.2317440787261664e-28 2.6751914462548625e-05 1.4920669999711128e-05 ;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "R_eye_Ctrl_REF" -p "R_eye_Ctrl_Grp_REF";
	rename -uid "99135B4B-440F-2941-7987-7499F4037336";
	addAttr -ci true -h true -sn "wireScale" -ln "wireScale" -min 0 -at "double";
	addAttr -ci true -h true -sn "wireType" -ln "wireType" -dt "string";
	addAttr -ci true -h true -sn "wireFacingAxis" -ln "wireFacingAxis" -at "short";
	addAttr -ci true -h true -sn "colorIndexKey" -ln "colorIndexKey" -dt "string";
	addAttr -ci true -h true -sn "colorIndexVal" -ln "colorIndexVal" -min 0 -max 255 
		-at "byte";
	addAttr -ci true -sn "eyeMoverCtrlVis" -ln "eyeMoverCtrlVis" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "Auto_Eyelid" -ln "Auto_Eyelid" -dv 1 -min 0 -max 1 -at "double";
	setAttr -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".wireScale" 5.1973050970000001;
	setAttr ".wireType" -type "string" "rightEye_1";
	setAttr ".wireFacingAxis" 5;
	setAttr ".colorIndexKey" -type "string" "rtCtrlColor";
	setAttr ".colorIndexVal" 13;
	setAttr -l on ".eyeMoverCtrlVis" yes;
createNode nurbsCurve -n "R_eye_Ctrl_REFShape" -p "R_eye_Ctrl_REF";
	rename -uid "1B099B11-4DC5-F424-6BAD-89BF73E5E45E";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 1 0.85499263 0 ;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.9585865582746278 0.95862116508905804 -3.438558763021546e-05
		8.3009383556385041e-17 1.3556807182349555 -3.4385587630147569e-05
		-0.9585865582746278 0.95862116508905781 -3.438558763035131e-05
		-1.3556461114205249 3.4606814430133346e-05 -3.4385587630241372e-05
		-0.9585865582746278 -0.95855195146019734 -3.4385587630317355e-05
		-1.3579599814982322e-16 -1.355611504606097 -3.438558763021546e-05
		0.9585865582746278 -0.95855195146019723 -3.4385587630317355e-05
		1.3556461114205249 3.4606814429878234e-05 -3.4385587630241379e-05
		0.9585865582746278 0.95862116508905804 -3.438558763021546e-05
		8.3009383556385041e-17 1.3556807182349555 -3.4385587630147569e-05
		-0.9585865582746278 0.95862116508905781 -3.438558763035131e-05
		;
createNode transform -n "EyeMaster_Ctrl_Grp_REF" -p "Eyes_Contorls_Grp_REF";
	rename -uid "FAA4DFF1-43FE-F378-164C-0296D0A37378";
	setAttr ".t" -type "double3" 2.4350024160241101e-05 156.4390281994653 49.999999999997833 ;
	setAttr ".r" -type "double3" 7.2317440787261664e-28 2.6751914462548618e-05 1.4920669999711128e-05 ;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "EyeMaster_Ctrl_REF" -p "EyeMaster_Ctrl_Grp_REF";
	rename -uid "AF761CB5-4417-E9D4-759D-4FAF2A924BFD";
	addAttr -ci true -h true -sn "wireScale" -ln "wireScale" -min 0 -at "double";
	addAttr -ci true -h true -sn "wireType" -ln "wireType" -dt "string";
	addAttr -ci true -h true -sn "wireFacingAxis" -ln "wireFacingAxis" -at "short";
	addAttr -ci true -h true -sn "colorIndexKey" -ln "colorIndexKey" -dt "string";
	addAttr -ci true -h true -sn "colorIndexVal" -ln "colorIndexVal" -min 0 -max 255 
		-at "byte";
	addAttr -ci true -sn "________SPACES___" -ln "________SPACES___" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "rootCtrl" -ln "rootCtrl" -dv 1 -min 0 -max 1 -at "double";
	addAttr -ci true -sn "worldSpaceCtrl" -ln "worldSpaceCtrl" -min 0 -max 1 -at "double";
	addAttr -ci true -sn "cogCtrl" -ln "cogCtrl" -min 0 -max 1 -at "double";
	addAttr -ci true -sn "headCtrl" -ln "headCtrl" -min 0 -max 1 -at "double";
	addAttr -ci true -sn "head_bCtrl" -ln "head_bCtrl" -min 0 -max 1 -at "double";
	addAttr -ci true -k true -sn "Follow" -ln "Follow" -min 0 -max 2 -en "World_Space_Grp:head_Ctrl:transform_M_root_jnt" 
		-at "enum";
	setAttr -k off ".v";
	setAttr ".t" -type "double3" 0 5.0604251674847512e-14 -3.2215701291262843e-14 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".wireScale" 18.710298349999999;
	setAttr ".wireType" -type "string" "eyeHolder_1";
	setAttr ".wireFacingAxis" 5;
	setAttr ".colorIndexKey" -type "string" "ctrCtrlColor";
	setAttr ".colorIndexVal" 17;
	setAttr -l on ".________SPACES___";
	setAttr -l on ".rootCtrl";
	setAttr -l on ".worldSpaceCtrl";
	setAttr -l on ".cogCtrl";
	setAttr -l on ".headCtrl";
	setAttr -l on ".head_bCtrl";
	setAttr -k on ".Follow" 1;
createNode nurbsCurve -n "EyeMaster_Ctrl_REFShape" -p "EyeMaster_Ctrl_REF";
	rename -uid "87F48604-4543-1239-8B40-079C2DAFBD87";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 1 0.85499263 0 ;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		5.5763389523964699 2.4875205915767755 -6.1973877639927839e-05
		1.4959841964601047e-16 2.4433722592836942 -6.1973877639805473e-05
		-5.5763389523964699 2.487520591576772 -6.1973877640172679e-05
		-6.2254032836623434 6.2372599452711636e-05 -6.1973877639974554e-05
		-5.5763389523964699 -2.4873409895260004 -6.1973877640111448e-05
		-2.4475918436194382e-16 -2.4432475140847987 -6.1973877639927839e-05
		5.5763389523964699 -2.4873409895259999 -6.1973877640111448e-05
		6.2254032836623434 6.237259945225184e-05 -6.1973877639974568e-05
		5.5763389523964699 2.4875205915767755 -6.1973877639927839e-05
		1.4959841964601047e-16 2.4433722592836942 -6.1973877639805473e-05
		-5.5763389523964699 2.487520591576772 -6.1973877640172679e-05
		;
createNode transform -n "R_Pupil_Ctrl_Grp_REF" -p "Eyes_Contorls_Grp_REF";
	rename -uid "EBACAF62-4FA7-27B8-4049-71B92896DCF3";
	setAttr ".t" -type "double3" -11.493000222164824 156.42668149886359 3.8583000221383639 ;
	setAttr ".r" -type "double3" 0 180.00002675191448 1.4920669977445177e-05 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
createNode transform -n "R_Pupil_Ctrl_REF" -p "R_Pupil_Ctrl_Grp_REF";
	rename -uid "4AB90292-4120-70DB-6E15-D1BE6E819402";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 1.1102230246251565e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0 3.0531133177191797e-15 -2.6645352591003757e-15 ;
	setAttr ".sp" -type "double3" 0 3.0531133177191805e-15 -2.6645352591003757e-15 ;
	setAttr ".spt" -type "double3" 0 -7.9040164917652159e-31 -8.7581154020301067e-46 ;
createNode nurbsCurve -n "R_Pupil_Ctrl_REFShape" -p "R_Pupil_Ctrl_REF";
	rename -uid "E9E57B83-4BD9-D570-B1FC-4EB3D72CE529";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovc" 18;
	setAttr ".ovrgb" -type "float3" 0.029999994 0.35600001 0.35600001 ;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.36286437409085287 0.33004808476854808 -0.0010405804241772833
		0.0090633937043523832 0.47659502086964722 -0.0010410746489655232
		-0.34473735701249636 0.3300475302897663 -0.0010415688737555394
		-0.49128619666755935 -0.023748854521144051 -0.0010417735883660572
		-0.34473680254091682 -0.37754500965942328 -0.0010415688737546512
		0.0090641778455801143 -0.52409194576052243 -0.0010410746489655232
		0.36286492856243241 -0.3775444551806415 -0.0010405804241763951
		0.50941376821747764 -0.023748070369730957 -0.0010403757095667654
		0.36286437409085287 0.33004808476854808 -0.0010405804241772833
		0.0090633937043523832 0.47659502086964722 -0.0010410746489655232
		-0.34473735701249636 0.3300475302897663 -0.0010415688737555394
		;
createNode transform -n "L_Pupil_Ctrl_Grp_REF" -p "Eyes_Contorls_Grp_REF";
	rename -uid "0E227E84-4553-6B86-E4D6-7AB19094E8A2";
	setAttr ".t" -type "double3" 11.50206400126925 156.40293303405781 3.8593411010192935 ;
	setAttr ".r" -type "double3" 0 5.3284618239490127e-05 2.9976470087792645e-05 ;
	setAttr ".s" -type "double3" 1.0000042930672055 0.99999130384755441 1.0000042930672057 ;
createNode transform -n "L_Pupil_Ctrl_REF" -p "L_Pupil_Ctrl_Grp_REF";
	rename -uid "9318B36D-4528-1ECA-EBFE-B191ED7D0E75";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 -1.1102230246251565e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr ".s" -type "double3" 1 0.99999999999999967 0.99999999999999967 ;
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -3.5527136788005009e-15 1.998401444325281e-15 -1.3322676295501867e-15 ;
	setAttr ".sp" -type "double3" -3.5527136788005009e-15 1.9984014443252818e-15 -1.3322676295501878e-15 ;
	setAttr ".spt" -type "double3" 0 -7.9810536895407019e-31 1.1832913578315175e-30 ;
createNode nurbsCurve -n "L_Pupil_Ctrl_REFShape" -p "L_Pupil_Ctrl_REF";
	rename -uid "BF556239-4483-8AC3-3F10-578B8BF25D70";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovc" 18;
	setAttr ".ovrgb" -type "float3" 0.029999994 0.35600001 0.35600001 ;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.35379934666776158 0.35379934666775947 -8.8817841970012523e-16
		3.5527136788005009e-15 0.50034783441628472 -1.7763568394002505e-15
		-0.35379934666775092 0.35379934666775947 -8.8817841970012523e-16
		-0.50034783441628505 2.1510571102112408e-15 -8.8817841970012523e-16
		-0.35379934666775092 -0.35379934666775548 -1.7763568394002505e-15
		3.5527136788005009e-15 -0.50034783441628072 -1.7763568394002505e-15
		0.35379934666776158 -0.35379934666775548 -1.7763568394002505e-15
		0.50034783441627795 2.0400348077487251e-15 -8.8817841970012523e-16
		0.35379934666776158 0.35379934666775947 -8.8817841970012523e-16
		3.5527136788005009e-15 0.50034783441628472 -1.7763568394002505e-15
		-0.35379934666775092 0.35379934666775947 -8.8817841970012523e-16
		;
createNode transform -n "L_Iris_Ctrl_Grp_REF" -p "Eyes_Contorls_Grp_REF";
	rename -uid "4ED3B5F5-43BC-4A4E-EEC2-EB92D37F2FB6";
	setAttr ".t" -type "double3" 19.344336413652414 156.40293713704145 3.8593338077684614 ;
	setAttr ".r" -type "double3" 0 5.3284618239490127e-05 2.9976470087792645e-05 ;
	setAttr ".s" -type "double3" 1.0000042930672055 0.99999130384755419 1.000004293067206 ;
createNode transform -n "L_Iris_Ctrl_REF" -p "L_Iris_Ctrl_Grp_REF";
	rename -uid "C9CAD977-477F-DA50-4845-27A00491E6E5";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -7.8422387451296558 1.3757154646015912e-14 -1.7262954981516248e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr ".s" -type "double3" 1 1 0.99999999999999967 ;
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 8.8817841970012523e-16 2.3314683517128276e-15 -1.2567056045111887e-15 ;
	setAttr ".sp" -type "double3" 8.8817841970012523e-16 2.3314683517128287e-15 -1.2567056045111899e-15 ;
	setAttr ".spt" -type "double3" 0 -1.1925358215645761e-30 1.1832913578315175e-30 ;
createNode nurbsCurve -n "L_Iris_Ctrl_REFShape" -p "L_Iris_Ctrl_REF";
	rename -uid "0C1E201B-47DA-3DBA-5D2B-45A076BF94A6";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovc" 18;
	setAttr ".ovrgb" -type "float3" 0.029999994 0.35600001 0.35600001 ;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.68828972001670585 0.68828972001670741 -4.4408920985006262e-16
		3.6205710020316097e-15 0.97338865688960585 -8.8817841970012523e-16
		-0.6882897200167023 0.68828972001670696 -1.9984014443252818e-15
		-0.97338865688960308 2.2065682614424986e-15 -9.4562740207537355e-16
		-0.6882897200167023 -0.68828972001670274 -1.8873791418627661e-15
		3.4417051091044687e-15 -0.97338865688960119 -2.2204460492503131e-15
		0.68828972001670585 -0.68828972001670274 -1.3322676295501878e-15
		0.97338865688960485 2.0400348077487251e-15 -2.9296515977206672e-16
		0.68828972001670585 0.68828972001670741 -4.4408920985006262e-16
		3.6205710020316097e-15 0.97338865688960585 -8.8817841970012523e-16
		-0.6882897200167023 0.68828972001670696 -1.9984014443252818e-15
		;
createNode transform -n "R_Iris_Ctrl_Grp_REF" -p "Eyes_Contorls_Grp_REF";
	rename -uid "F3885B32-4C30-4E8B-0CFD-6FAAF52A6B83";
	setAttr ".t" -type "double3" -11.493000222164824 156.42668149886362 3.8583000221383643 ;
	setAttr ".r" -type "double3" 0 180.00002675191448 1.4920669977445177e-05 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000002 ;
createNode transform -n "R_Iris_Ctrl_REF" -p "R_Iris_Ctrl_Grp_REF";
	rename -uid "9B56FF4C-4138-629E-B694-BF9B60C48298";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 1.1102230246251565e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999944 ;
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 3.9968028886505635e-15 3.5527136788005001e-15 4.4408920985006074e-16 ;
	setAttr ".sp" -type "double3" 3.9968028886505635e-15 3.5527136788005009e-15 4.4408920985006262e-16 ;
	setAttr ".spt" -type "double3" 0 -7.9040164917652159e-31 -1.8735446498999034e-30 ;
createNode nurbsCurve -n "R_Iris_Ctrl_REFShape" -p "R_Iris_Ctrl_REF";
	rename -uid "37B7B7EE-4D80-D3A4-9963-B7982F837080";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovc" 18;
	setAttr ".ovrgb" -type "float3" 0.029999994 0.35600001 0.35600001 ;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.69735592132367108 0.66453581144690865 -0.0010401131722280049
		0.0090630230314112051 0.94963172970775322 -0.0010410746489655427
		-0.67922942845601986 0.66453473275061037 -0.0010420361257024143
		-0.96432904993630753 -0.023749225198868625 -0.0010424343824082455
		-0.67922834977373325 -0.71203273633771147 -0.0010420361257025253
		0.0090645485185301307 -0.99712865459855604 -0.0010410746489642104
		0.69735700000595768 -0.71203165764141363 -0.0010401131722271167
		0.98245662148624358 -0.023747699691934628 -0.0010397149155233776
		0.69735592132367108 0.66453581144690865 -0.0010401131722280049
		0.0090630230314112051 0.94963172970775322 -0.0010410746489655427
		-0.67922942845601986 0.66453473275061037 -0.0010420361257024143
		;
createNode transform -n "L_Spec_Ctrl_Grp_REF" -p "Eyes_Contorls_Grp_REF";
	rename -uid "B0FCB7F6-455D-49E3-ACD1-DCB5084C18A5";
	setAttr ".t" -type "double3" 11.50206400126925 156.40293303405781 3.8593411010192935 ;
	setAttr ".r" -type "double3" 0 5.3284618239490127e-05 2.9976470087792645e-05 ;
	setAttr ".s" -type "double3" 1.0000042930672055 0.99999130384755441 1.0000042930672057 ;
createNode transform -n "L_Spec_Ctrl_REF" -p "L_Spec_Ctrl_Grp_REF";
	rename -uid "CA4F0CCF-4970-2AE1-B2C7-E6B88349446B";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 -1.1102230246251565e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr ".s" -type "double3" 1 0.99999999999999967 0.99999999999999967 ;
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -3.5527136788005009e-15 1.998401444325281e-15 -1.3322676295501867e-15 ;
	setAttr ".sp" -type "double3" -3.5527136788005009e-15 1.9984014443252818e-15 -1.3322676295501878e-15 ;
	setAttr ".spt" -type "double3" 0 -7.9810536895407019e-31 1.1832913578315175e-30 ;
createNode nurbsCurve -n "L_Spec_Ctrl_REFShape" -p "L_Spec_Ctrl_REF";
	rename -uid "1A64FE62-4348-44C2-161D-3D95C066080B";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovc" 18;
	setAttr ".ovrgb" -type "float3" 0.029999994 0.35600001 0.35600001 ;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.11784377806081757 0.11784377806079983 -1.1072912522878882e-15
		-1.186032076805599e-15 0.16665626917487586 -1.5572440068124893e-15
		-0.11784377806081753 0.11784377806079983 -1.1072912522878882e-15
		-0.16665626917489659 -1.823850714682047e-14 -1.1072912522878882e-15
		-0.11784377806081753 -0.11784377806083617 -1.5572440068124893e-15
		-1.186032076805599e-15 -0.16665626917491183 -1.5572440068124893e-15
		0.11784377806081757 -0.11784377806083617 -1.5572440068124893e-15
		0.16665626917489032 -1.8275486546851655e-14 -1.1072912522878882e-15
		0.11784377806081757 0.11784377806079983 -1.1072912522878882e-15
		-1.186032076805599e-15 0.16665626917487586 -1.5572440068124893e-15
		-0.11784377806081753 0.11784377806079983 -1.1072912522878882e-15
		;
createNode transform -n "R_Spec_Ctrl_Grp_REF" -p "Eyes_Contorls_Grp_REF";
	rename -uid "4E2EC460-40D5-8A98-64F9-D9A728EC2D94";
	setAttr ".t" -type "double3" -11.493000222164824 156.42668149886359 3.8583000221383639 ;
	setAttr ".r" -type "double3" 0 180.00002675191448 1.4920669977445177e-05 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
createNode transform -n "R_Spec_Ctrl_REF" -p "R_Spec_Ctrl_Grp_REF";
	rename -uid "17582EFA-4CAB-C9C7-CF46-5697D8D8BBFA";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 1.1102230246251565e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0 3.0531133177191797e-15 -2.6645352591003757e-15 ;
	setAttr ".sp" -type "double3" 0 3.0531133177191805e-15 -2.6645352591003757e-15 ;
	setAttr ".spt" -type "double3" 0 -7.9040164917652159e-31 -8.7581154020301067e-46 ;
createNode nurbsCurve -n "R_Spec_Ctrl_REFShape" -p "R_Spec_Ctrl_REF";
	rename -uid "1B0DB77A-495D-905B-DFC5-D8885FE49C6D";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovc" 18;
	setAttr ".ovrgb" -type "float3" 0.029999994 0.35600001 0.35600001 ;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.12690797740496257 0.094094383170990045 -0.0010409100321659898
		0.009063655183758219 0.1429063574610622 -0.0010410746489657422
		-0.10878059053888794 0.094094198484740307 -0.0010412392657663946
		-0.15759319886628376 -0.02374859303835742 -0.0010413074522776329
		-0.10878040585503712 -0.14159130806190556 -0.0010412392657659446
		0.0090639163661647999 -0.19040328235197737 -0.0010410746489657422
		0.12690816208881339 -0.14159112337565583 -0.0010409100321655399
		0.17572077041620288 -0.023748331852558292 -0.0010408418456547516
		0.12690797740496257 0.094094383170990045 -0.0010409100321659898
		0.009063655183758219 0.1429063574610622 -0.0010410746489657422
		-0.10878059053888794 0.094094198484740307 -0.0010412392657663946
		;
createNode transform -n "R_Eyeshaper_Ctrl_Grp" -p "Eyes_Contorls_Grp_REF";
	rename -uid "7B09AF8F-417F-17D3-C69F-4481CE923975";
	setAttr ".t" -type "double3" -11.493 153.50511832197239 3.858 ;
createNode transform -n "R_Eyeshaper_Ctrl" -p "R_Eyeshaper_Ctrl_Grp";
	rename -uid "B2A3D348-483E-CB9F-3EB8-B7A3AE5A965D";
	addAttr -ci true -h true -sn "wireScale" -ln "wireScale" -min 0 -at "double";
	addAttr -ci true -h true -sn "wireType" -ln "wireType" -dt "string";
	addAttr -ci true -h true -sn "wireFacingAxis" -ln "wireFacingAxis" -at "short";
	addAttr -ci true -h true -sn "colorIndexKey" -ln "colorIndexKey" -dt "string";
	addAttr -ci true -h true -sn "colorIndexVal" -ln "colorIndexVal" -min 0 -max 255 
		-at "byte";
	setAttr -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -0.0099995140959476831 1.9505209249981452e-06 0.00090312477673393254 ;
	setAttr ".rpt" -type "double3" 0.019999487843283113 2.6040816170080916e-09 -0.0018062542223307805 ;
	setAttr ".sp" -type "double3" -0.0099995140959476814 1.9505209249981448e-06 0.00090312477673393232 ;
	setAttr ".spt" -type "double3" -1.7347234759768275e-18 4.2351647451461897e-22 2.1684043449710093e-19 ;
	setAttr ".wireScale" 5.1973050970000001;
	setAttr ".wireType" -type "string" "circle";
	setAttr ".wireFacingAxis" 2;
	setAttr ".colorIndexKey" -type "string" "rtCtrlColor";
	setAttr ".colorIndexVal" 13;
createNode nurbsCurve -n "R_Eyeshaper_CtrlShape" -p "R_Eyeshaper_Ctrl";
	rename -uid "774424DC-4D1A-C711-B447-1C8FF4CBD20A";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovc" 17;
	setAttr ".ovrgb" -type "float3" 0.56690001 0.30000001 1 ;
	setAttr ".cc" -type "nurbsCurve" 
		3 17 0 no 3
		22 1 1 1 3 5 7 9 11 13 14 15 16 18 21 23 26 29 30 31 32 32 32
		20
		-1.2734670385371947 0.41985866011563311 -0.00089632231406227051
		-0.82305371887548018 0.13714120183141756 -0.00089934753177677376
		-0.17095859386106549 0.070419017208756443 -0.00090304415535682342
		0.52223051558611266 0.14871124932056914 -0.00090578581357541308
		1.1295514756869454 0.35875323568182577 -0.00090763739464469516
		1.592629460806432 0.63084047521882658 -0.00090857690611326116
		1.8806621800937451 0.95418959449801855 -0.00090899549450517423
		1.880661926663185 0.95418986099055292 -0.00090902885889065086
		1.8806623446392101 0.95418936887452421 -0.00090896525657035152
		1.7147243162295132 1.4181603033126815 -0.00091015567911624333
		1.2553045250403871 2.0337643603820266 -0.00090998648024094456
		0.21517521679609042 2.5105762369846127 -0.00090738121464759144
		-0.6858106609107536 2.4683797429002561 -0.00090412342142229328
		-1.4121697368303217 2.1400213798964742 -0.00089813187363069842
		-1.8480171589246648 1.4194016394643054 -0.00089306701215208651
		-1.9214410894387568 1.0964519692256369 -0.00089243474100021637
		-1.9214409788974511 1.096450788460724 -0.00089223193775846078
		-1.9214406287880541 1.0964497728675664 -0.00089244019677338295
		-1.587370733551468 0.65460546745958503 -0.00089419605038296157
		-1.2734660540254974 0.41985398248211137 -0.00089612364235163214
		;
createNode nurbsCurve -n "R_Eyeshaper_CtrlShape1" -p "R_Eyeshaper_Ctrl";
	rename -uid "2E6AF0A9-4E17-A54B-6F67-DEAB17D32633";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		3 17 0 no 3
		22 1 1 1 3 5 7 9 11 13 14 15 16 18 21 23 26 29 30 31 32 32 32
		20
		1.2534675506939119 -0.87063424414401769 0.00090993190826926806
		0.80305423103219742 -1.1533517024282331 0.00090690669055470963
		0.15095910601778292 -1.2200738870508945 0.00090321006697458017
		-0.54223000342939509 -1.1417816549390816 0.00090046840875590561
		-1.1495509635302277 -0.93173966857782498 0.00089861682768654916
		-1.6126289486497141 -0.65965242904082422 0.00089767731621792635
		-1.9006616679370272 -0.33630330976163236 0.00089725872782597804
		-1.9006614145064671 -0.33630304326909799 0.00089722536344050141
		-1.9006618324824922 -0.3363035353851267 0.00089728896576080075
		-1.7347238040727953 0.12766739905303051 0.00089609854321492932
		-1.2753040128836695 0.74327145612237533 0.00089626774209028426
		-0.2351747046393729 1.2200833327249616 0.00089887300768376477
		0.66581117306747095 1.1778868386406052 0.0009021308009091733
		1.3921702489870389 0.84952847563682321 0.00090812234870085717
		1.8280176710813818 0.12890873520465457 0.00091318721017952242
		1.9014416015954738 -0.19404093503401398 0.00091381948133140156
		1.9014414910541682 -0.19404211579892694 0.00091402228457315715
		1.9014411409447711 -0.19404313139208462 0.00091381402555823499
		1.567371245708185 -0.63588743680006576 0.00091205817194861538
		1.2534665661822146 -0.87063892177753943 0.00091013057997990643
		;
createNode nurbsCurve -n "R_Eyeshaper_CtrlShape2" -p "R_Eyeshaper_Ctrl";
	rename -uid "2F334403-464D-4A26-A48F-D6BC45B09EA1";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 0.56690001 0.30000001 1 ;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.71710578855958251 2.2449840402665115 -0.0018062542223313595
		-0.019999487843282794 2.5337349244389396 -0.001806254222331385
		0.67710681287301622 2.2449840402665115 -0.0018062542223311886
		0.96585769704544477 1.5478777395502119 -0.0018062542223312643
		0.67710681287301622 0.85077143883391293 -0.0018062542223312996
		-0.019999487843282635 0.56202055466148315 -0.0018062542223316071
		-0.71710578855958251 0.85077143883391293 -0.0018062542223315815
		-1.0058566727320106 1.5478777395502119 -0.0018062542223315058
		-0.71710578855958251 2.2449840402665115 -0.0018062542223313595
		-0.019999487843282794 2.5337349244389396 -0.001806254222331385
		0.67710681287301622 2.2449840402665115 -0.0018062542223311886
		;
createNode transform -n "L_Eyeshaper_Ctrl_Grp" -p "Eyes_Contorls_Grp_REF";
	rename -uid "124493E5-4208-7EE6-49A8-97A4F8C8D402";
	setAttr ".t" -type "double3" 11.493 153.50511832197239 3.858 ;
createNode transform -n "L_Eyeshaper_Ctrl" -p "L_Eyeshaper_Ctrl_Grp";
	rename -uid "AC060014-40D8-8F22-0EE9-62B9EF99269E";
	addAttr -ci true -h true -sn "wireScale" -ln "wireScale" -min 0 -at "double";
	addAttr -ci true -h true -sn "wireType" -ln "wireType" -dt "string";
	addAttr -ci true -h true -sn "wireFacingAxis" -ln "wireFacingAxis" -at "short";
	addAttr -ci true -h true -sn "colorIndexKey" -ln "colorIndexKey" -dt "string";
	addAttr -ci true -h true -sn "colorIndexVal" -ln "colorIndexVal" -min 0 -max 255 
		-at "byte";
	setAttr -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -0.0099995140959476831 1.9505209249981452e-06 0.00090312477673393254 ;
	setAttr ".rpt" -type "double3" 0.019999487843283113 2.6040816170080916e-09 -0.0018062542223307805 ;
	setAttr ".sp" -type "double3" -0.0099995140959476814 1.9505209249981448e-06 0.00090312477673393232 ;
	setAttr ".spt" -type "double3" -1.7347234759768275e-18 4.2351647451461897e-22 2.1684043449710093e-19 ;
	setAttr ".wireScale" 5.1973050970000001;
	setAttr ".wireType" -type "string" "circle";
	setAttr ".wireFacingAxis" 2;
	setAttr ".colorIndexKey" -type "string" "rtCtrlColor";
	setAttr ".colorIndexVal" 13;
createNode nurbsCurve -n "L_Eyeshaper_CtrlShape" -p "L_Eyeshaper_Ctrl";
	rename -uid "144439AF-44A4-4A50-FFDB-80AFC9044979";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovc" 17;
	setAttr ".ovrgb" -type "float3" 0.56690001 0.30000001 1 ;
	setAttr ".cc" -type "nurbsCurve" 
		3 17 0 no 3
		22 1 1 1 3 5 7 9 11 13 14 15 16 18 21 23 26 29 30 31 32 32 32
		20
		1.2534675506939119 0.41985865751155071 0.00090993190826926806
		0.80305423103219742 0.13714119922733525 0.00090690669055470963
		0.15095910601778292 0.070419014604674146 0.00090321006697458017
		-0.54223000342939509 0.14871124671648683 0.00090046840875590561
		-1.1495509635302277 0.35875323307774343 0.00089861682768654916
		-1.6126289486497141 0.63084047261474419 0.00089767731621792635
		-1.9006616679370272 0.95418959189393604 0.00089725872782597804
		-1.9006614145064671 0.95418985838647041 0.00089722536344050141
		-1.9006618324824922 0.95418936627044171 0.00089728896576080075
		-1.7347238040727953 1.418160300708599 0.00089609854321492932
		-1.2753040128836695 2.0337643577779438 0.00089626774209028426
		-0.2351747046393729 2.51057623438053 0.00089887300768376477
		0.66581117306747095 2.4683797402961734 0.0009021308009091733
		1.3921702489870389 2.1400213772923915 0.00090812234870085717
		1.8280176710813818 1.4194016368602229 0.00091318721017952242
		1.9014416015954738 1.0964519666215544 0.00091381948133140156
		1.9014414910541682 1.0964507858566415 0.00091402228457315715
		1.9014411409447711 1.0964497702634839 0.00091381402555823499
		1.567371245708185 0.65460546485550264 0.00091205817194861538
		1.2534665661822146 0.41985397987802897 0.00091013057997990643
		;
createNode nurbsCurve -n "L_Eyeshaper_CtrlShapeOrig" -p "L_Eyeshaper_Ctrl";
	rename -uid "99954874-41F2-D502-2385-1C9257842E6B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		3 17 0 no 3
		22 1 1 1 3 5 7 9 11 13 14 15 16 18 21 23 26 29 30 31 32 32 32
		20
		1.2534675506939119 -0.87063424414401769 0.00090993190826926806
		0.80305423103219742 -1.1533517024282331 0.00090690669055470963
		0.15095910601778292 -1.2200738870508945 0.00090321006697458017
		-0.54223000342939509 -1.1417816549390816 0.00090046840875590561
		-1.1495509635302277 -0.93173966857782498 0.00089861682768654916
		-1.6126289486497141 -0.65965242904082422 0.00089767731621792635
		-1.9006616679370272 -0.33630330976163236 0.00089725872782597804
		-1.9006614145064671 -0.33630304326909799 0.00089722536344050141
		-1.9006618324824922 -0.3363035353851267 0.00089728896576080075
		-1.7347238040727953 0.12766739905303051 0.00089609854321492932
		-1.2753040128836695 0.74327145612237533 0.00089626774209028426
		-0.2351747046393729 1.2200833327249616 0.00089887300768376477
		0.66581117306747095 1.1778868386406052 0.0009021308009091733
		1.3921702489870389 0.84952847563682321 0.00090812234870085717
		1.8280176710813818 0.12890873520465457 0.00091318721017952242
		1.9014416015954738 -0.19404093503401398 0.00091381948133140156
		1.9014414910541682 -0.19404211579892694 0.00091402228457315715
		1.9014411409447711 -0.19404313139208462 0.00091381402555823499
		1.567371245708185 -0.63588743680006576 0.00091205817194861538
		1.2534665661822146 -0.87063892177753943 0.00091013057997990643
		;
createNode nurbsCurve -n "nurbsCircleShape1" -p "L_Eyeshaper_Ctrl";
	rename -uid "95CA07CE-4BA8-1BBE-1911-50BEA42964BC";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 0.56690001 0.30000001 1 ;
	setAttr ".tw" yes;
	setAttr -s 11 ".cp[0:10]" -type "double3" -0.086505324174924625 2.2449840376624288 
		0.78361162489122471 1.8860657474221431e-16 2.5337349218348568 1.1081941875543877 
		0.086505324174925846 2.2449840376624288 0.78361162489122449 0.12233700266566105 1.5478777369461292 
		5.7448982375248316e-17 0.086505324174925846 0.85077143622983042 -0.78361162489122449 
		2.0835213690592979e-16 0.56202055205740087 -1.1081941875543886 -0.086505324174924625 
		0.85077143622983042 -0.78361162489122449 -0.12233700266566028 1.5478777369461292 
		-1.511240500779959e-16 0 0 0 0 0 0 0 0 0;
createNode multiplyDivide -n "MD_Eye_C";
	rename -uid "C30714E8-4804-146C-AFA8-58BFB6893B27";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "MD_Eye_iris";
	rename -uid "E9584FFC-4539-A0D1-8540-F89FBAA42DD5";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "MD_Eye_Pupil";
	rename -uid "C6AAAC8B-406A-7D0E-456B-40B30A89BA2F";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "33ABADA6-480A-1F4C-ACF9-6D95B3E718FE";
	setAttr -s 12 ".lnk";
	setAttr -s 4 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "F96A7F16-4337-79B2-48A7-EABBA85F71E9";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "77A41390-42C9-0387-5837-EFB650E80058";
createNode displayLayerManager -n "layerManager";
	rename -uid "C49320DF-4107-F014-2D95-3D81F8BDA910";
createNode displayLayer -n "defaultLayer";
	rename -uid "C00AC1F6-4652-C91E-2F4B-A9BD5C918923";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "B1F261A8-4BF8-C807-1A62-D1AF48C71CA1";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "906A74AF-4325-232E-CAE9-27A07BCA6525";
	setAttr ".g" yes;
createNode nodeGraphEditorInfo -n "modeling_b_bloomnaked_df_hig_vr069_hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "00518DD8-4F37-E408-819C-F3BEA77C3FB0";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -436.90474454372719 804.63627208953687 ;
	setAttr ".tgi[0].vh" -type "double2" 296.42855964955874 1205.6148494282513 ;
createNode nodeGraphEditorInfo -n "modeling_b_bloomnaked_df_hig_vr027_hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "B0B45DB8-4359-C778-8E21-26BDEAB3943A";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -436.90474454372719 804.63627208953687 ;
	setAttr ".tgi[0].vh" -type "double2" 296.42855964955874 1205.6148494282513 ;
createNode nodeGraphEditorInfo -n "modeling_b_bloomnaked_df_hig_vr028_hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "CE0308A5-4747-63D0-A8C0-7A922E1B7A2B";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -436.90474454372719 804.63627208953687 ;
	setAttr ".tgi[0].vh" -type "double2" 296.42855964955874 1205.6148494282513 ;
createNode nodeGraphEditorInfo -n "modeling_b_bloomnaked_df_hig_vr027_hyperShadePrimaryNodeEditorSavedTabsInfo1";
	rename -uid "8448EAC0-41FC-0DC9-B676-5C803A8DFF44";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -436.90474454372719 804.63627208953687 ;
	setAttr ".tgi[0].vh" -type "double2" 296.42855964955874 1205.6148494282513 ;
createNode nodeGraphEditorInfo -n "modeling_b_bloomnaked_df_hig_vr026_hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "DE1AD565-4CE9-6F36-2AE8-57A480E4F115";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -436.90474454372719 804.63627208953687 ;
	setAttr ".tgi[0].vh" -type "double2" 296.42855964955874 1205.6148494282513 ;
createNode nodeGraphEditorInfo -n "modeling_b_bloomnaked_df_hig_vr025_hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "FAA577FA-46FB-04C5-9D53-8BA2DC00C557";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -436.90474454372719 804.63627208953687 ;
	setAttr ".tgi[0].vh" -type "double2" 296.42855964955874 1205.6148494282513 ;
createNode nodeGraphEditorInfo -n "modeling_b_bloomnaked_df_hig_vr024_hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "E4E9488F-4534-D14D-2CC2-AE9C2C667369";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -436.90474454372719 804.63627208953687 ;
	setAttr ".tgi[0].vh" -type "double2" 296.42855964955874 1205.6148494282513 ;
createNode nodeGraphEditorInfo -n "modeling_b_bloomnaked_df_hig_vr023_hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "79C54CBE-4C84-CF73-FADD-289089D06C31";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -436.90474454372719 804.63627208953687 ;
	setAttr ".tgi[0].vh" -type "double2" 296.42855964955874 1205.6148494282513 ;
createNode nodeGraphEditorInfo -n "modeling_b_bloomnaked_df_hig_vr022_hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "E0B3BC5A-4381-7F82-D3C9-CDA92DF53DB8";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -436.90474454372719 804.63627208953687 ;
	setAttr ".tgi[0].vh" -type "double2" 296.42855964955874 1205.6148494282513 ;
createNode groupId -n "groupId6501";
	rename -uid "9CF5B5F1-47B7-572B-059E-31988DA8BCF6";
	setAttr ".ihi" 0;
createNode groupId -n "groupId6693";
	rename -uid "1852394F-4854-00DA-8560-C486D6BAB3B4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId6699";
	rename -uid "F8E9BF7C-47FE-2A92-D330-9DBF6E2E4D1A";
	setAttr ".ihi" 0;
createNode nodeGraphEditorInfo -n "modeling_b_bloomnaked_df_hig_vr061_hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "FA7BE437-4C86-0E08-57FC-02AD97BA23BE";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -436.90474454372719 804.63627208953687 ;
	setAttr ".tgi[0].vh" -type "double2" 296.42855964955874 1205.6148494282513 ;
createNode nodeGraphEditorInfo -n "modeling_b_bloomnaked_df_hig_vr061_modeling_b_bloomnaked_df_hig_vr027_hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "E2D2896D-41D2-0AD6-8AE3-6D85AF11CBFE";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -436.90474454372719 804.63627208953687 ;
	setAttr ".tgi[0].vh" -type "double2" 296.42855964955874 1205.6148494282513 ;
createNode nodeGraphEditorInfo -n "modeling_b_bloomnaked_df_hig_vr061_modeling_b_bloomnaked_df_hig_vr028_hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "76DF5D14-4279-0077-8528-EB973B303C35";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -436.90474454372719 804.63627208953687 ;
	setAttr ".tgi[0].vh" -type "double2" 296.42855964955874 1205.6148494282513 ;
createNode nodeGraphEditorInfo -n "modeling_b_bloomnaked_df_hig_vr061_modeling_b_bloomnaked_df_hig_vr027_hyperShadePrimaryNodeEditorSavedTabsInfo1";
	rename -uid "4328EA33-4853-D80D-A99D-2FB789F04AAF";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -436.90474454372719 804.63627208953687 ;
	setAttr ".tgi[0].vh" -type "double2" 296.42855964955874 1205.6148494282513 ;
createNode nodeGraphEditorInfo -n "modeling_b_bloomnaked_df_hig_vr061_modeling_b_bloomnaked_df_hig_vr026_hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "DC21FC47-4716-B17D-6BD1-548FC6E92FDD";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -436.90474454372719 804.63627208953687 ;
	setAttr ".tgi[0].vh" -type "double2" 296.42855964955874 1205.6148494282513 ;
createNode nodeGraphEditorInfo -n "modeling_b_bloomnaked_df_hig_vr061_modeling_b_bloomnaked_df_hig_vr025_hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "3A5B70A4-4A96-7662-6829-7B94EC63D857";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -436.90474454372719 804.63627208953687 ;
	setAttr ".tgi[0].vh" -type "double2" 296.42855964955874 1205.6148494282513 ;
createNode nodeGraphEditorInfo -n "modeling_b_bloomnaked_df_hig_vr061_modeling_b_bloomnaked_df_hig_vr024_hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "D413DCCA-405C-1478-8E07-8EA7F7BF1E40";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -436.90474454372719 804.63627208953687 ;
	setAttr ".tgi[0].vh" -type "double2" 296.42855964955874 1205.6148494282513 ;
createNode nodeGraphEditorInfo -n "modeling_b_bloomnaked_df_hig_vr061_modeling_b_bloomnaked_df_hig_vr023_hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "0B0C4577-40F6-A1BE-D599-229B8F87AC6C";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -436.90474454372719 804.63627208953687 ;
	setAttr ".tgi[0].vh" -type "double2" 296.42855964955874 1205.6148494282513 ;
createNode nodeGraphEditorInfo -n "modeling_b_bloomnaked_df_hig_vr061_modeling_b_bloomnaked_df_hig_vr022_hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "2BFA8715-448E-2500-9C61-2AB21502FB2A";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -436.90474454372719 804.63627208953687 ;
	setAttr ".tgi[0].vh" -type "double2" 296.42855964955874 1205.6148494282513 ;
createNode nodeGraphEditorInfo -n "modeling_b_bloomnaked_df_hig_vr061_hyperShadePrimaryNodeEditorSavedTabsInfo1";
	rename -uid "D9DFC61F-44D4-5020-691A-70BD277EC2DF";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -559.52378729033182 -357.02816405674997 ;
	setAttr ".tgi[0].vh" -type "double2" 136.90475646465572 353.45673562723721 ;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "6F4E38C2-4A0A-C483-1A41-FDA17F55B9C2";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -620.23807059204864 -255.95237078174756 ;
	setAttr ".tgi[0].vh" -type "double2" 28.571427436102034 405.95236482128331 ;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo1";
	rename -uid "7E1EB10F-4593-C65A-15B7-C2BA20DA5B4F";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -330.95236780151544 -323.80951094248991 ;
	setAttr ".tgi[0].vh" -type "double2" 317.85713022663526 338.09522466054096 ;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo2";
	rename -uid "3929EA77-4D49-FA26-2345-A38BCB498563";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -330.95236780151544 -323.80951094248991 ;
	setAttr ".tgi[0].vh" -type "double2" 317.85713022663526 338.09522466054096 ;
createNode multiplyDivide -n "MD_Eyemover";
	rename -uid "EA1C76EC-4953-7DF7-2E20-79A9CFEF647F";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "MD_Spec_compensate";
	rename -uid "4089F130-4AE0-D9E7-3C97-AC9CE8131B32";
createNode multiplyDivide -n "MD_Eyeshaper";
	rename -uid "60333061-49CB-57D7-3F4A-D9BE830B0C44";
createNode nodeGraphEditorInfo -n "MayaNodeEditorSavedTabsInfo";
	rename -uid "958B4A84-4988-77E8-1CB5-9CABFCDEE10E";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" 15.717822594425572 -873.75559062420757 ;
	setAttr ".tgi[0].vh" -type "double2" 957.73460289425554 -336.78081576997806 ;
	setAttr -s 3 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" 22.773115158081055;
	setAttr ".tgi[0].ni[0].y" -390;
	setAttr ".tgi[0].ni[0].nvs" 18306;
	setAttr ".tgi[0].ni[1].x" 494.20169067382813;
	setAttr ".tgi[0].ni[1].y" -402.43698120117188;
	setAttr ".tgi[0].ni[1].nvs" 18306;
	setAttr ".tgi[0].ni[2].x" 241.76823425292969;
	setAttr ".tgi[0].ni[2].y" -465.35223388671875;
	setAttr ".tgi[0].ni[2].nvs" 18306;
createNode VRaySettingsNode -s -n "vraySettings";
	rename -uid "F8850749-4662-6225-BD3C-23BD5F605246";
	setAttr ".gi" yes;
	setAttr ".rfc" yes;
	setAttr ".pe" 2;
	setAttr ".se" 3;
	setAttr ".cmph" 60;
	setAttr ".csdu" 0;
	setAttr ".cfile" -type "string" "";
	setAttr ".cfile2" -type "string" "";
	setAttr ".casf" -type "string" "";
	setAttr ".casf2" -type "string" "";
	setAttr ".st" 3;
	setAttr ".msr" 6;
	setAttr ".aaft" 3;
	setAttr ".aafs" 2;
	setAttr ".dma" 24;
	setAttr ".dmig" 1;
	setAttr ".dmag" 48;
	setAttr ".dam" 1;
	setAttr ".pt" 0.0099999997764825821;
	setAttr ".pmt" 0;
	setAttr ".sd" 1000;
	setAttr ".ss" 0.01;
	setAttr ".pfts" 20;
	setAttr ".ufg" yes;
	setAttr ".fnm" -type "string" "";
	setAttr ".lcfnm" -type "string" "";
	setAttr ".asf" -type "string" "";
	setAttr ".lcasf" -type "string" "";
	setAttr ".urtrshd" yes;
	setAttr ".rtrshd" 2;
	setAttr ".lightCacheType" 1;
	setAttr ".ifile" -type "string" "";
	setAttr ".ifile2" -type "string" "";
	setAttr ".iasf" -type "string" "";
	setAttr ".iasf2" -type "string" "";
	setAttr ".pmfile" -type "string" "";
	setAttr ".pmfile2" -type "string" "";
	setAttr ".pmasf" -type "string" "";
	setAttr ".pmasf2" -type "string" "";
	setAttr ".dmcstd" yes;
	setAttr ".dmculs" no;
	setAttr ".dmcsat" 0.004999999888241291;
	setAttr ".cmtp" 6;
	setAttr ".cmao" 2;
	setAttr ".cg" 2.2000000476837158;
	setAttr ".mtah" yes;
	setAttr ".rgbcs" -1;
	setAttr ".suv" 0;
	setAttr ".srflc" 1;
	setAttr ".srdml" 0;
	setAttr ".seu" yes;
	setAttr ".gormio" yes;
	setAttr ".gocle" yes;
	setAttr ".gopl" 2;
	setAttr ".gopv" yes;
	setAttr ".wi" 960;
	setAttr ".he" 540;
	setAttr ".aspr" 1.7777780294418335;
	setAttr ".productionGPUResizeTextures" 0;
	setAttr ".autolt" 0;
	setAttr ".jpegq" 100;
	setAttr ".vfbOn" yes;
	setAttr ".vfbSA" -type "Int32Array" 1026 0 4096 1 4084 0 1
		 4076 1700143739 1869181810 825893486 1632379436 1936876921 578501154 1936876886 577662825 573321530 1935764579 574235251
		 1953460082 1881287714 1701867378 1701409906 2067407475 1919252002 1852795251 741423650 1835101730 574235237 1696738338 1818386798
		 1949966949 744846706 1886938402 577007201 1818322490 573334899 1634760805 1650549870 975332716 1702195828 1931619453 1814913653
		 1919252833 1530536563 1818436219 577991521 1751327290 779317089 1886611812 1132028268 1701999215 1869182051 573317742 1886351984
		 1769239141 975336293 1702240891 1869181810 825893486 1634607660 975332717 1936278562 2036427888 1919894304 1952671090 577662825
		 1852121644 1701601889 1920219682 573334901 1634760805 975332462 1702195828 2019893804 1684955504 1701601889 1920219682 573334901
		 1718579824 577072233 573321530 1869641829 1701999987 774912546 1931619376 1600484961 1600284530 1835627120 1986622569 975336293
		 1936482662 1763847269 1717527395 577072233 740434490 1667459362 1869770847 1701603686 1952539743 1849303649 745303157 1667459362
		 1852142175 1953392996 578055781 573321274 1886088290 1852793716 1715085942 1702063201 1668227628 1717530473 577072233 740434490
		 1768124194 1868783471 1936879468 1701011824 741358114 1768124194 1768185711 1634496627 1986356345 577069929 573321274 1869177711
		 1701410399 1634890871 1868985198 975334770 1864510512 1601136995 1702257011 1835626089 577070945 1818322490 746415475 1651864354
		 2036427821 577991269 578509626 1935764579 574235251 1868654691 1701981811 1869819494 1701016181 1684828006 740455013 1869770786
		 1953654128 577987945 1981971258 1769173605 975335023 1847733297 577072481 1867719226 1701016181 1196564538 573317698 1650552421
		 975332716 1702195828 2019893804 1684955504 1634089506 744846188 1886938402 1633971809 577072226 1818322490 573334899 1852140642
		 1869438820 975332708 1864510512 1768120688 975337844 741355057 1869116194 1919967095 1701410405 1949966967 744846706 1668444962
		 1887007839 809116261 1931619453 1814913653 1919252833 1530536563 1818436219 577991521 1751327290 779317089 778462578 1751607660
		 2020175220 1881287714 1701867378 1701409906 2067407475 1919252002 1852795251 741423650 1835101730 574235237 1751607628 2020167028
		 1696738338 1818386798 1715085925 1702063201 2019893804 1684955504 1634089506 744846188 1886938402 1633971809 577072226 1970435130
		 573341029 761427315 1702453612 975336306 746413403 1818436219 577991521 1751327290 779317089 778462578 1886220131 1953067887
		 573317733 1886351984 1769239141 975336293 1702240891 1869181810 825893486 1634607660 975332717 1836008226 1769172848 740451700
		 1634624802 577072226 1818322490 573334899 1634760805 975332462 1936482662 1696738405 1851879544 1818386788 1949966949 744846706
		 1701601826 1834968174 577070191 573321274 1667330159 578385001 808333626 1752375852 1885304687 1769366898 975337317 1702195828
		 1931619453 1814913653 1919252833 1530536563 2103278941 1663204140 1936941420 1663187490 1936679272 778399790 1869505892 1919251305
		 1881287714 1701867378 1701409906 2067407475 1919252002 1852795251 741423650 1835101730 574235237 1869505860 1919251305 1853169722
		 1767994977 1818386796 573317733 1650552421 975332716 1936482662 1696738405 1851879544 1715085924 1702063201 2019893804 1684955504
		 1701601889 1920219682 573334901 1852140642 1869438820 975332708 1864510512 1768120688 975337844 741355057 1952669986 577074793
		 1818322490 573334899 1936028272 975336549 1931619378 1852142196 577270887 808333626 1634869804 1937074532 808532514 573321262
		 1665234792 1701602659 1702125938 1920219682 573334901 1869505892 1919251305 1685024095 825893477 1931619453 1814913653 1919252833
		 1530536563 2066513245 1634493218 975336307 1634231074 1882092399 1752378981 1701868129 1818373742 740455029 1869770786 1953654128
		 577987945 1981971258 1769173605 975335023 1847733297 577072481 1750278714 1701868129 1816276846 740455029 1634624802 577072226
		 1818322490 573334899 1634760805 975332462 1936482662 1696738405 1851879544 1818386788 1949966949 744846706 1701601826 1834968174
		 577070191 573321274 1667330159 578385001 808333626 1752375852 1701868129 1818386286 1667199605 1970302319 975332724 1936482662
		 1931619429 1886544232 1633644133 1853189997 825893492 573321262 1918986355 1601070448 1768186226 975336309 741682736 1970037282
		 1634885490 1937074532 774978082 808465203 875573296 892418354 1931619453 1814913653 1919252833 1530536563 2066513245 1634493218
		 975336307 1634231074 1882092399 1701588581 2019980142 1881287714 1701867378 1701409906 2067407475 1919252002 1852795251 741423650
		 1835101730 574235237 1936614732 1717978400 1937007461 1696738338 1818386798 1715085925 1702063201 2019893804 1684955504 1634089506
		 744846188 1886938402 1633971809 577072226 1970435130 1646406757 1684956524 1685024095 842670693 1886331436 1953063777 825893497
		 573321262 1918987367 1852792677 1634089506 744846188 1634494242 1935631730 577075817 774910778 1730292784 1701994860 1768257375
		 578054247 808333626 1818370604 1601007471 1734960503 975336552 808726064 808464432 959787056 1730292790 1701994860 1919448159
		 1869116261 975332460 741355057 1818846754 1601332596 1635020658 1852795252 774912546 1931619376 1920300129 1869182049 825893486
		 573321262 1685217640 1701994871 1667457375 1919249509 1684370529 1920219682 573334901 1684828003 1918990175 1715085933 1702063201
		 1852383788 1634887028 1986622563 1949966949 744846706 1986097954 1818713957 577073761 1970435130 1646406757 1600482145 1918987367
		 1702322021 1952999273 1634089506 744846188 1701995298 1600484449 1701209701 1601401955 1970496882 1667200108 1852727656 975334501
		 1936482662 1696738405 1818386798 1818386277 1936024673 1920219682 573334901 1701079411 909779571 1818370604 1936024673 1953460831
		 1869182049 825893486 741355061 1920234274 1600872805 1920298082 774912546 808464434 808464432 741882162 1702065442 1634887519
		 1735289204 1634089506 744846188 1634887458 1735289204 1852138591 2037672307 808794658 573321262 1952543335 1600613993 1735288172
		 975333492 808333361 1919361580 1852404833 1870290791 975334767 741355061 1634887458 1735289204 1869378399 975332720 741355056
		 1634887458 1735289204 1920234335 1952935525 825893480 573321262 1600484213 1818452847 1869181813 1715085934 1702063201 1668227628
		 1937075299 1601073001 1835891059 1769108581 1949966947 744846706 1667460898 1769174380 1885302383 1701016165 975336558 808333362
		 1668227628 1937075299 1601073001 1635020658 1852795252 774912546 1864510512 1970037603 1852795251 1668440415 808532514 741355056
		 1702065442 1919120223 1751348321 1634089506 744846188 1919120162 1836675935 1920230765 975332201 1702195828 1668489772 1634754418
		 1919251572 809116270 1668489772 1970102130 1734964332 1701994860 1970234207 975336558 1931619377 1683976803 1769172581 975337844
		 808333365 1668489772 1701601138 1752459118 808794658 573321262 1601332083 1886350451 1635147621 1851877746 975332707 741355056
		 1919120162 1684633439 1985964148 1634300513 577069934 808333370 1668489772 1702059890 975332453 1931619376 2053075555 577597295
		 808333882 1668489772 1869766514 1769234804 975335023 741355056 1919120162 1920234335 1952935525 825893480 573321262 1600484213
		 1953723748 1634089506 744846188 1937073186 1634754420 1919251572 809116270 1969496620 1683977331 1769172581 975337844 808333365
		 1969496620 1918858355 1969841249 1635147635 1851877746 975332707 741355056 1937073186 1768578932 1919251572 774912546 1679961136
		 1601467253 1836019578 775043618 1679961136 1601467253 1635020658 1852795252 774912546 1679961136 1601467253 1701999731 1752459118
		 774978082 1730292784 1701994860 1702065503 1935830879 1818452340 1835622245 577070945 1818322490 573334899 1918987367 1651466085
		 1667331187 1767859564 1701273965 1952542815 574235240 1864510498 1601467234 1734438249 1870290789 975334767 741355057 1935830818
		 1835622260 1600481121 1635020658 1852795252 774912546 1864510512 1601467234 1734438249 1953718117 1735288178 975333492 741355057
		 1702065442 1818846815 1601332596 1734438249 1715085925 1702063201 1818698284 1600483937 1734438249 1634754405 975333492 573317666
		 1701667171 1952407922 577073273 573321274 1919251571 1834970981 577070191 746401850 1651864354 2036427821 577991269 2103270202
		 2066513245 1634493218 975336307 1634231074 1865315183 1819436406 1932425569 1886216564 1881287714 1701867378 1701409906 2067407475
		 1919252002 1852795251 741423650 1835101730 574235237 1835103315 573317744 1650552421 975332716 1936482662 1696738405 1851879544
		 1715085924 1702063201 2019893804 1684955504 1701601889 1920219682 573334901 1835103347 1869111152 1601857906 1734962273 825893486
		 1953702444 1601203553 1953654134 1768710495 975335015 573321779 1835103347 1868783472 577924972 774986554 774974512 774974512
		 573332784 1835103347 1868980080 975336558 1702240891 1869181810 825893486 1869619756 1601465961 1702521203 808532514 1634083372
		 2037148013 741358114 2037674786 975332716 1998728240 1751607653 809116276 1634083372 975332707 1769095458 2099407969 1953702444
		 1601203553 1769108595 975333230 1702240891 1869181810 825893486 1634869804 1953718135 1735289202 572668450 1768301100 1600938350
		 1769108595 975333230 2105352738 1970479660 1634479458 1936876921 1566259746 746413437 1734693410 1198419817 975335013 1702240891
		 1869181810 825893486 1869423148 1600484213 1819045734 1700755311 1818386798 975332453 1936482662 1830956133 1702065519 1819240031
		 1601662828 1852403568 1869373300 1684368227 1634089506 744846188 1970236706 1717527923 1869376623 1869635447 1601465961 809116280
		 1869423148 1600484213 1819045734 1885304687 1953393007 975337823 573340976 1684956498 1767273061 975337317 1702240891 1869181810
		 825893486 1852121644 1701601889 1852142175 1601332580 1768383858 975335023 1936482662 1914842213 1701080677 1701994354 1852795239
		 573601887 774974778 1914842160 1701080677 1701994354 1852795239 573602143 774974778 1914842160 1701080677 1701994354 1852795239
		 573667423 774974778 1914842160 1701080677 1701994354 1852795239 573667679 774974778 1981951024 1601660265 577004914 1970435130
		 1981951077 1601660265 1701147239 1949966958 744846706 1701410338 1818386295 975332725 1702195828 1769349676 1834973029 577728111
		 1818322490 573334899 1869377379 1818451826 1601203553 1701080941 959855138 1868767788 1601335148 1835101283 1852792688 1920219682
		 573334901 1600484213 1702390128 1935761260 1952671088 577662815 1818322490 573334899 1702390128 1852399468 1818193766 1701536623
		 1715085924 1702063201 1768956460 1600939384 1868983913 1919902559 1952671090 1667196005 1919904879 1715085939 1702063201 1092758653
		 1869182051 975336302 1702240891 1869181810 825893486 1634738732 1231385461 1667191376 1801676136 975332453 1936482662 1948396645
		 1383363429 1918858085 1869177953 825571874 1702109740 1699902579 1751342963 1701536613 1715085924 1702063201 1701061164 1399289186
		 1768186216 1918855022 1869177953 909457954 1701061164 1399289186 1768186216 1667196782 1801676136 975332453 1936482662 1931619429
		 1701995892 1685015919 1634885477 577726820 741881658 1702130466 1299146098 1600480367 1667590243 577004907 1818322490 2105369971 ;
	setAttr ".vfbSyncM" yes;
	setAttr ".mSceneName" -type "string" "C:/Users/f.cecchini/Desktop/scenes/locs_eye.ma";
	setAttr ".rt_cpuRayBundleSize" 4;
	setAttr ".rt_gpuRayBundleSize" 128;
	setAttr ".rt_maxPaths" 10000;
	setAttr ".rt_engineType" 3;
	setAttr ".rt_gpuResizeTextures" 0;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "684E4727-4A04-B309-D668-83B2043B0976";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 0.6\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 456\n            -height 332\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 0.6\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 919\n            -height 708\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 0.6\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n"
		+ "            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n"
		+ "            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 456\n            -height 332\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n"
		+ "            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 0.6\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n"
		+ "            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 959\n            -height 708\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -autoExpandAnimatedShapes 1\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 0\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 0\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n"
		+ "            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 0\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -selectCommand \"print(\\\"\\\")\" \n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n"
		+ "            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n"
		+ "            -autoExpand 0\n            -autoExpandAnimatedShapes 1\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n"
		+ "            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n"
		+ "                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -autoExpandAnimatedShapes 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n"
		+ "                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -showPlayRangeShades \"on\" \n                -lockPlayRangeShades \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -valueLinesToggle 1\n                -outliner \"graphEditor1OutlineEd\" \n                -highlightAffectedCurves 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n"
		+ "            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -autoExpandAnimatedShapes 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n"
		+ "                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n"
		+ "                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n"
		+ "                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n"
		+ "                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n"
		+ "                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n"
		+ "                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n"
		+ "\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n{ string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n"
		+ "                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 0.6\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n"
		+ "                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n"
		+ "                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName; };\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 0.6\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 959\\n    -height 708\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 0.6\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 959\\n    -height 708\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "DE572359-479D-7FCD-4829-02BF4A056291";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode makeNurbCircle -n "makeNurbCircle1";
	rename -uid "FB66E6DB-425B-4C52-6F2E-DC908DAA0642";
	setAttr ".nr" -type "double3" 0 1 0 ;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -k on ".fzn";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av ".unw" 1;
	setAttr -av -k on ".etw";
	setAttr -av -k on ".tps";
	setAttr -av -k on ".tms";
select -ne :hardwareRenderingGlobals;
	setAttr -av -k on ".cch";
	setAttr -k on ".fzn";
	setAttr -av -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".rm";
	setAttr -k on ".lm";
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr -k on ".hom";
	setAttr -k on ".hodm";
	setAttr -k on ".xry";
	setAttr -k on ".jxr";
	setAttr -k on ".sslt";
	setAttr -k on ".cbr";
	setAttr -k on ".bbr";
	setAttr -av -k on ".mhl";
	setAttr -k on ".cons";
	setAttr -k on ".vac";
	setAttr -av -k on ".hwi";
	setAttr -k on ".csvd";
	setAttr -av -k on ".ta";
	setAttr -av -k on ".tq";
	setAttr -k on ".ts";
	setAttr -av -k on ".etmr";
	setAttr -av -k on ".tmr";
	setAttr -av -k on ".aoon";
	setAttr -av -k on ".aoam";
	setAttr -av -k on ".aora";
	setAttr -k on ".aofr";
	setAttr -av -k on ".aosm";
	setAttr -av -k on ".hff";
	setAttr -av -k on ".hfd";
	setAttr -av -k on ".hfs";
	setAttr -av -k on ".hfe";
	setAttr -av ".hfc";
	setAttr -av -k on ".hfcr";
	setAttr -av -k on ".hfcg";
	setAttr -av -k on ".hfcb";
	setAttr -av -k on ".hfa";
	setAttr -av -k on ".mbe";
	setAttr -k on ".mbt";
	setAttr -av -k on ".mbsof";
	setAttr -k on ".mbsc";
	setAttr -k on ".mbc";
	setAttr -k on ".mbfa";
	setAttr -k on ".mbftb";
	setAttr -k on ".mbftg";
	setAttr -k on ".mbftr";
	setAttr -k on ".mbfta";
	setAttr -k on ".mbfe";
	setAttr -k on ".mbme";
	setAttr -k on ".mbcsx";
	setAttr -k on ".mbcsy";
	setAttr -k on ".mbasx";
	setAttr -k on ".mbasy";
	setAttr -av -k on ".blen";
	setAttr -k on ".blth";
	setAttr -k on ".blfr";
	setAttr -k on ".blfa";
	setAttr -av -k on ".blat";
	setAttr -av -k on ".msaa" yes;
	setAttr -av -k on ".aasc";
	setAttr -k on ".aasq";
	setAttr -k on ".laa";
	setAttr ".fprt" yes;
	setAttr -k on ".rtfm";
select -ne :renderPartition;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -k on ".cch";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 6 ".u";
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -k on ".fzn";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".bbx";
	setAttr -k on ".vwm";
	setAttr -k on ".tpv";
	setAttr -k on ".uit";
	setAttr -k on ".mwc";
	setAttr -av -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -av -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k on ".macc";
	setAttr -av -k on ".macd";
	setAttr -av -k on ".macq";
	setAttr -av -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -av -k on ".clip";
	setAttr -av -k on ".edm";
	setAttr -av -k on ".edl";
	setAttr -av -k on ".ren" -type "string" "arnold";
	setAttr -av -k on ".esr";
	setAttr -av -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -av -cb on ".imfkey";
	setAttr -av -k on ".gama";
	setAttr -av -k on ".exrc";
	setAttr -av -k on ".expt";
	setAttr -av -k on ".an";
	setAttr -k on ".ar";
	setAttr -av -k on ".fs" 1;
	setAttr -av -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -av -cb on ".me";
	setAttr -cb on ".se";
	setAttr -av -k on ".be";
	setAttr -av -cb on ".ep";
	setAttr -av -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -av -k on ".pff";
	setAttr -av -k on ".peie";
	setAttr -av -k on ".ifp";
	setAttr -k on ".rv";
	setAttr -av -k on ".comp";
	setAttr -av -k on ".cth";
	setAttr -av -k on ".soll";
	setAttr -av -cb on ".sosl";
	setAttr -av -k on ".rd";
	setAttr -av -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -av -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -av -k on ".mm";
	setAttr -av -k on ".npu";
	setAttr -av -k on ".itf";
	setAttr -av -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -av -k on ".uf";
	setAttr -av -k on ".oi";
	setAttr -av -k on ".rut";
	setAttr -av -k on ".mot";
	setAttr -av -k on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -av -k on ".mbso";
	setAttr -av -k on ".mbsc";
	setAttr -av -k on ".afp";
	setAttr -av -k on ".pfb";
	setAttr -av -k on ".pram";
	setAttr -av -k on ".poam";
	setAttr -av -k on ".prlm";
	setAttr -av -k on ".polm";
	setAttr -av -cb on ".prm";
	setAttr -av -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -av -k on ".ubc";
	setAttr -av -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -av -k on ".udbx";
	setAttr -av -k on ".smc";
	setAttr -av -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -av -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -av -k on ".tlwd";
	setAttr -av -k on ".tlht";
	setAttr -av -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -av -k on ".ope";
	setAttr -av -k on ".oppf";
	setAttr -av -k on ".rcp";
	setAttr -av -k on ".icp";
	setAttr -av -k on ".ocp";
	setAttr -cb on ".hbl";
	setAttr ".dss" -type "string" "lambert1";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -av -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w";
	setAttr -av -k on ".h";
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar";
	setAttr -av -k on ".ldar";
	setAttr -av -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -av -k on ".isu";
	setAttr -av -k on ".pdu";
select -ne :hardwareRenderGlobals;
	setAttr -av -k on ".cch";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -av -k off -cb on ".fbfm";
	setAttr -av -k off -cb on ".ehql";
	setAttr -av -k off -cb on ".eams";
	setAttr -av -k off -cb on ".eeaa";
	setAttr -av -k off -cb on ".engm";
	setAttr -av -k off -cb on ".mes";
	setAttr -av -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -av -k off -cb on ".mbs";
	setAttr -av -k off -cb on ".trm";
	setAttr -av -k off -cb on ".tshc";
	setAttr -av -k off -cb on ".enpt";
	setAttr -av -k off -cb on ".clmt";
	setAttr -av -k off -cb on ".tcov";
	setAttr -av -k off -cb on ".lith";
	setAttr -av -k off -cb on ".sobc";
	setAttr -av -k off -cb on ".cuth";
	setAttr -av -k off -cb on ".hgcd";
	setAttr -av -k off -cb on ".hgci";
	setAttr -av -k off -cb on ".mgcs";
	setAttr -av -k off -cb on ".twa";
	setAttr -av -k off -cb on ".twz";
	setAttr -av -k on ".hwcc";
	setAttr -av -k on ".hwdp";
	setAttr -av -k on ".hwql";
	setAttr -av -k on ".hwfr";
	setAttr -av -k on ".soll";
	setAttr -av -k on ".sosl";
	setAttr -av -k on ".bswa";
	setAttr -av -k on ".shml";
	setAttr -av -k on ".hwel";
connectAttr "MD_Eye_C.o" "LOC_R_Eye.t";
connectAttr "MD_Eye_iris.o" "LOC_R_Eye_Iris.t";
connectAttr "MD_Spec_compensate.o" "LOC_R_Eye_Spec.t";
connectAttr "MD_Eye_Pupil.o" "LOC_R_Eye_Pupil.t";
connectAttr "MD_Eyemover.o" "LOC_R_Eyemover.t";
connectAttr "MD_Eyeshaper.o" "LOC_R_Eyeshaper.t";
connectAttr "makeNurbCircle1.oc" "nurbsCircleShape1.cr";
connectAttr "LOC_L_Eye.t" "MD_Eye_C.i1";
connectAttr "LOC_L_Eye_Iris.t" "MD_Eye_iris.i1";
connectAttr "LOC_L_Eye_Pupil.t" "MD_Eye_Pupil.i1";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":hyperGraphLayout.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":defaultObjectSet.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":defaultHardwareRenderGlobals.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":ikSystem.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":defaultRenderGlobals.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":defaultRenderGlobals.message" ":initialParticleSE.message";
relationship "link" ":lightLinker1" ":defaultLightSet.message" ":initialParticleSE.message";
relationship "link" ":lightLinker1" ":defaultLightSet.message" ":initialShadingGroup.message";
relationship "link" ":lightLinker1" ":defaultRenderGlobals.message" ":initialShadingGroup.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":initialShadingGroup.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":defaultHardwareRenderGlobals.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":ikSystem.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "LOC_L_Eyemover.t" "MD_Eyemover.i1";
connectAttr "LOC_L_Eye_Spec.t" "MD_Spec_compensate.i1";
connectAttr "LOC_L_Eyeshaper.t" "MD_Eyeshaper.i1";
connectAttr "LOC_L_Eyeshaper.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[0].dn";
connectAttr "LOC_R_Eyeshaper.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[1].dn";
connectAttr "MD_Eyeshaper.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[2].dn";
connectAttr "MD_Eye_C.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "MD_Eye_Pupil.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "MD_Eye_iris.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "MD_Eyemover.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "MD_Spec_compensate.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "MD_Eyeshaper.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of locs_eye.ma
