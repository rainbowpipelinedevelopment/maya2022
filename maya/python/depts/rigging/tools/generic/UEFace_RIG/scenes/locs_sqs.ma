//Maya ASCII 2022 scene
//Name: locs_sqs.ma
//Last modified: Fri, Aug 09, 2024 04:29:07 PM
//Codeset: 1252
requires maya "2022";
requires -nodeType "VRaySettingsNode" -dataType "VRaySunParams" -dataType "vrayFloatVectorData"
		 -dataType "vrayFloatVectorData" -dataType "vrayIntData" "vrayformaya" "6";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t pal;
fileInfo "vrayBuild" "6.20.03 32637 90384ab";
fileInfo "application" "maya";
fileInfo "product" "Maya 2022";
fileInfo "version" "2022";
fileInfo "cutIdentifier" "202303271415-baa69b5798";
fileInfo "osv" "Windows 10 Pro v2009 (Build: 19045)";
fileInfo "UUID" "242B39AA-4035-8F0B-FA9D-1A9166D32342";
createNode transform -s -n "persp";
	rename -uid "F5BD4533-41DB-8642-720A-E08CBD287196";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 73.490596946418407 160.40313690273047 15.983926518766474 ;
	setAttr ".r" -type "double3" -1.5383527295989874 86.199999999999889 1.499720601863533e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "81DA0288-41E6-926A-A2F6-7884C0F60545";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 79.38359280501686;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 4.5823692957913664e-06 159.23264251708986 6.8363548374117595 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr -s 2 ".b";
createNode transform -s -n "top";
	rename -uid "06F177E9-46A6-8001-FA19-92A8B7BD66A9";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "6FEFFB18-4DC1-D179-1B6A-B2ADD8F3A1F8";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "08E85021-4400-11D3-B209-74B452683C0F";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "7718E4CF-453B-97D1-304C-9384CAFC2D54";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "DB4444EB-4335-6523-A0FA-D2A5870D7EA4";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "537B9CD9-436F-4A0C-962C-3FBF23E76994";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "LOCS_SQS_Grp";
	rename -uid "13DF406E-4E6C-C71C-25DC-D2910047C0BC";
createNode transform -n "LOC_SQSLow_Grp" -p "LOCS_SQS_Grp";
	rename -uid "16E075C9-4C14-CA6A-2A15-5697E8299AFC";
createNode transform -n "LOC_SQSLow_01" -p "LOC_SQSLow_Grp";
	rename -uid "FEE1C1DC-431D-775D-7A34-86B95E29E969";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 152.88597106933594 -0.84799998998641968 ;
	setAttr -l on ".tx";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_SQSLow_Shape1" -p "LOC_SQSLow_01";
	rename -uid "51656003-44F7-3C68-9229-5AB972F2B1D0";
	setAttr -k off ".v";
	setAttr ".ove" yes;
createNode transform -n "LOC_SQSLow_02" -p "LOC_SQSLow_Grp";
	rename -uid "62A2C3C6-429A-CCFA-3A5E-7C9FA155E8F0";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 150.65253903712897 -0.84799998998641968 ;
	setAttr -l on ".tx";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_SQSLow_Shape2" -p "LOC_SQSLow_02";
	rename -uid "9A6571B7-459E-F131-01FF-C69D7A6DD80F";
	setAttr -k off ".v";
createNode transform -n "LOC_SQSLow_03" -p "LOC_SQSLow_Grp";
	rename -uid "067B58E2-4787-403A-2B03-22A94934AC75";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 148.50183895089458 -0.84799998998641968 ;
	setAttr -l on ".tx";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_SQSLow_Shape3" -p "LOC_SQSLow_03";
	rename -uid "A27F61D8-4247-0AC3-8D5A-9A95CF52BB68";
	setAttr -k off ".v";
createNode transform -n "LOC_SQSLow_04" -p "LOC_SQSLow_Grp";
	rename -uid "2E105267-4CBE-A160-7387-57A892813CC4";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 145.74528503417969 -0.84799998998641968 ;
	setAttr -l on ".tx";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_SQSLow_Shape4" -p "LOC_SQSLow_04";
	rename -uid "56F220DA-4C4F-D310-2799-93A88295A95F";
	setAttr -k off ".v";
createNode transform -n "LOC_SQSUp_Grp" -p "LOCS_SQS_Grp";
	rename -uid "900EAA32-495F-A572-83CE-34B052FF8C30";
createNode transform -n "LOC_SQSUp_01" -p "LOC_SQSUp_Grp";
	rename -uid "94BD013A-474E-77DE-2CD7-96911EB72ACD";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 153.71977233886719 -0.84799998998641968 ;
	setAttr -l on ".tx";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_SQSUp_Shape1" -p "LOC_SQSUp_01";
	rename -uid "9A0B1C4B-4CE4-37A6-0019-B88CBD1EB5F5";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
createNode transform -n "LOC_SQSUp_02" -p "LOC_SQSUp_Grp";
	rename -uid "86C4E27C-4BCB-757A-E4B0-CAA4EC65D5CD";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 156.71994018554688 -0.84799998998641968 ;
	setAttr -l on ".tx";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_SQSUp_Shape2" -p "LOC_SQSUp_02";
	rename -uid "4362F36E-41DA-0300-D7A2-7F9BCC08F78E";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
createNode transform -n "LOC_SQS_Z" -p "LOC_SQSUp_02";
	rename -uid "C0651BE2-45B6-CFF5-6805-E5BF9F6B3E9B";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 15.368709664809938 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_SQS_ZShape" -p "LOC_SQS_Z";
	rename -uid "CEDDC288-4A66-B5C1-5F96-928BE7BB8BBC";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
createNode transform -n "LOC_SQSUp_03" -p "LOC_SQSUp_Grp";
	rename -uid "EE7963C0-47BA-6D86-5911-7B93F71EA0AC";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 159.72012329101562 -0.84799998998641968 ;
	setAttr -l on ".tx";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_SQSUp_Shape3" -p "LOC_SQSUp_03";
	rename -uid "D2AF065F-49C1-F183-7D19-AD994716C61E";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
createNode transform -n "LOC_SQSUp_04" -p "LOC_SQSUp_Grp";
	rename -uid "FB9AAFA2-4ECC-093E-8AE9-CDA698975F3C";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 162.72000122070312 -0.84799998998641968 ;
	setAttr -l on ".tx";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_SQSUp_Shape4" -p "LOC_SQSUp_04";
	rename -uid "BAAA69F7-40E2-E769-5D3A-FDAA784DD765";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
createNode transform -n "LOC_SQS_Y" -p "LOC_SQSUp_04";
	rename -uid "93F2544A-4B54-0033-7345-8AA0D19FD857";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 9.9999987792969023 -1.0013580187262505e-08 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_SQS_YShape" -p "LOC_SQS_Y";
	rename -uid "E2602330-41FD-DED1-2B5E-0E94EB88951E";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
createNode transform -n "LOC_Mouth_Master_Driver" -p "LOCS_SQS_Grp";
	rename -uid "612C042E-442E-85F7-77DF-23A482FF9E96";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".t" -type "double3" 2.5982831832770199e-06 149.99981319904327 2.4999986867541111 ;
	setAttr -l on -k off ".tx";
	setAttr ".r" -type "double3" 7.2317440787261664e-28 2.6751914462548625e-05 1.4920669999711128e-05 ;
	setAttr ".s" -type "double3" 1.0000243186950679 0.99995148181915294 1.0000243186950684 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_Mouth_Master_DriverShape" -p "LOC_Mouth_Master_Driver";
	rename -uid "DB309CD8-4E00-C295-7407-7F8FB1FB4D3C";
	setAttr -k off ".v";
createNode transform -n "LOC_Mouth_Master" -p "LOC_Mouth_Master_Driver";
	rename -uid "EEC8DD0A-4AAF-3DD6-C4A2-48B0EE964A22";
	setAttr ".ove" yes;
	setAttr ".ovc" 21;
	setAttr ".t" -type "double3" -1.6940658945086007e-20 -3.861101814286729e-14 8.4986064175128782 ;
createNode locator -n "LOC_Mouth_MasterShape" -p "LOC_Mouth_Master";
	rename -uid "23C8D5DA-46B6-C75E-8D27-D6B2E4A01DCE";
	setAttr -k off ".v";
createNode transform -n "LOC_Head" -p "LOCS_SQS_Grp";
	rename -uid "0C7307C9-4284-320A-9CA0-B7B9B2B6A06A";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 145.62555153918785 -5.8511166720945678 ;
	setAttr -l on ".tx";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "LOC_HeadShape" -p "LOC_Head";
	rename -uid "E868F264-43E1-3709-0C2A-878DAD65DE51";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 4;
createNode transform -n "SQS_Contorls_Grp_REF";
	rename -uid "3617F6A2-44AF-EED7-54BD-D68D2541CB2F";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 5.9998184894951624e-08 1.3149833488781686e-12 7.9770016510688827e-12 ;
	setAttr ".r" -type "double3" 0 5.3284618232812864e-05 2.9846558208270709e-05 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000002 1 ;
createNode transform -n "MidSQS_Ctrl_Grp_REF" -p "SQS_Contorls_Grp_REF";
	rename -uid "5C397C03-4DDF-C211-7C9E-21B68C4382E7";
	setAttr ".t" -type "double3" 2.8610229473634649e-06 153.30238342285156 14.301439168702464 ;
createNode transform -n "MidSQS_Ctrl_Grp_Offset_REF" -p "MidSQS_Ctrl_Grp_REF";
	rename -uid "F8E428EF-4248-FB5C-798C-2FB75DA635FA";
	setAttr ".t" -type "double3" 0 5.4102039925699782e-15 5.9630437889878e-17 ;
	setAttr ".r" -type "double3" 4.95678069139941e-16 1.9301388494393572e-12 0 ;
	setAttr ".rp" -type "double3" 3.8040374075664116e-05 -0.00035291086526284543 -15.149553957218986 ;
	setAttr ".rpt" -type "double3" 0 -6.6609451244634132e-15 3.5354831536691102e-07 ;
	setAttr ".sp" -type "double3" 3.8040374075664116e-05 -0.00035291086526284543 -15.149553957218986 ;
createNode transform -n "MidSQS_Ctrl_REF" -p "MidSQS_Ctrl_Grp_Offset_REF";
	rename -uid "64AD6AEC-4029-17E4-2B6B-2A92FCDC2298";
	addAttr -ci true -sn "zooTranslateTrack" -ln "zooTranslateTrack" -at "double3" 
		-nc 3;
	addAttr -ci true -sn "zooTranslateTrack_x" -ln "zooTranslateTrack_x" -at "double" 
		-p "zooTranslateTrack";
	addAttr -ci true -sn "zooTranslateTrack_y" -ln "zooTranslateTrack_y" -at "double" 
		-p "zooTranslateTrack";
	addAttr -ci true -sn "zooTranslateTrack_z" -ln "zooTranslateTrack_z" -at "double" 
		-p "zooTranslateTrack";
	addAttr -ci true -sn "zooTranslateDefault" -ln "zooTranslateDefault" -at "double3" 
		-nc 3;
	addAttr -ci true -sn "zooTranslateDefault_x" -ln "zooTranslateDefault_x" -at "double" 
		-p "zooTranslateDefault";
	addAttr -ci true -sn "zooTranslateDefault_y" -ln "zooTranslateDefault_y" -at "double" 
		-p "zooTranslateDefault";
	addAttr -ci true -sn "zooTranslateDefault_z" -ln "zooTranslateDefault_z" -at "double" 
		-p "zooTranslateDefault";
	addAttr -ci true -sn "zooRotateTrack" -ln "zooRotateTrack" -at "double3" -nc 3;
	addAttr -ci true -sn "zooRotateTrack_x" -ln "zooRotateTrack_x" -at "double" -p "zooRotateTrack";
	addAttr -ci true -sn "zooRotateTrack_y" -ln "zooRotateTrack_y" -at "double" -p "zooRotateTrack";
	addAttr -ci true -sn "zooRotateTrack_z" -ln "zooRotateTrack_z" -at "double" -p "zooRotateTrack";
	addAttr -ci true -sn "zooRotateDefault" -ln "zooRotateDefault" -at "double3" -nc 
		3;
	addAttr -ci true -sn "zooRotateDefault_x" -ln "zooRotateDefault_x" -at "double" 
		-p "zooRotateDefault";
	addAttr -ci true -sn "zooRotateDefault_y" -ln "zooRotateDefault_y" -at "double" 
		-p "zooRotateDefault";
	addAttr -ci true -sn "zooRotateDefault_z" -ln "zooRotateDefault_z" -at "double" 
		-p "zooRotateDefault";
	addAttr -ci true -sn "zooScaleTrack" -ln "zooScaleTrack" -at "double3" -nc 3;
	addAttr -ci true -sn "zooScaleTrack_x" -ln "zooScaleTrack_x" -at "double" -p "zooScaleTrack";
	addAttr -ci true -sn "zooScaleTrack_y" -ln "zooScaleTrack_y" -at "double" -p "zooScaleTrack";
	addAttr -ci true -sn "zooScaleTrack_z" -ln "zooScaleTrack_z" -at "double" -p "zooScaleTrack";
	addAttr -ci true -sn "zooScaleDefault" -ln "zooScaleDefault" -at "double3" -nc 3;
	addAttr -ci true -sn "zooScaleDefault_x" -ln "zooScaleDefault_x" -at "double" -p "zooScaleDefault";
	addAttr -ci true -sn "zooScaleDefault_y" -ln "zooScaleDefault_y" -at "double" -p "zooScaleDefault";
	addAttr -ci true -sn "zooScaleDefault_z" -ln "zooScaleDefault_z" -at "double" -p "zooScaleDefault";
	addAttr -ci true -sn "zooColorTrack" -ln "zooColorTrack" -at "double3" -nc 3;
	addAttr -ci true -sn "zooColorTrack_r" -ln "zooColorTrack_r" -at "double" -p "zooColorTrack";
	addAttr -ci true -sn "zooColorTrack_g" -ln "zooColorTrack_g" -at "double" -p "zooColorTrack";
	addAttr -ci true -sn "zooColorTrack_b" -ln "zooColorTrack_b" -at "double" -p "zooColorTrack";
	addAttr -ci true -sn "zooColorDefault" -ln "zooColorDefault" -at "double3" -nc 3;
	addAttr -ci true -sn "zooColorDefault_r" -ln "zooColorDefault_r" -at "double" -p "zooColorDefault";
	addAttr -ci true -sn "zooColorDefault_g" -ln "zooColorDefault_g" -at "double" -p "zooColorDefault";
	addAttr -ci true -sn "zooColorDefault_b" -ln "zooColorDefault_b" -at "double" -p "zooColorDefault";
	addAttr -ci true -sn "zooShapeTrack" -ln "zooShapeTrack" -dt "string";
	addAttr -ci true -sn "zooShapeDefault" -ln "zooShapeDefault" -dt "string";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -4.0931303446513859e-14 2.8360276671838495e-15 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".zooScaleTrack" -type "double3" 2 2 2 ;
	setAttr ".zooScaleDefault" -type "double3" 2 2 2 ;
	setAttr ".zooColorTrack" -type "double3" 1 0.31854677812509186 0.91140750040635399 ;
	setAttr ".zooColorDefault" -type "double3" 0.15999996662139893 0.29999992251396179 
		0.875 ;
	setAttr ".zooShapeTrack" -type "string" "cube";
	setAttr ".zooShapeDefault" -type "string" "cube";
createNode nurbsCurve -n "MidSQS_Ctrl_REFShape" -p "MidSQS_Ctrl_REF";
	rename -uid "5E32D9F4-4AA7-2689-D792-E28D710A9F8D";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 1 0.40400004 0.033999979 ;
	setAttr ".cc" -type "nurbsCurve" 
		1 18 0 no 3
		19 0 0.10000000000000001 0.20000000000000001 0.29999999999999999 0.40000000000000002
		 0.5 0.59999999999999998 0.69999999999999996 0.80000000000000004 0.90000000000000002
		 1 1.1000000000000001 1.2 1.3 1.3999999999999999 1.5 1.6000000000000001 1.7 1.8
		19
		-0.00062550443552611781 0.0057595684912915904 -1.0313242355259606
		-0.00062550443542079331 0.75797231476930882 0.17105492078903825
		0.71223361237196559 -0.34571523177178748 0.17105492079290119
		0.71223649545550527 -0.3457152318217257 0.17105492079290069
		-0.00062550443552611781 0.0057595684912915904 -1.0313242355259606
		-0.67071254510233513 -0.3457150127990638 0.17105492078903875
		0.71223649540556788 -0.34571523182172598 0.17105492079290169
		0.71223361232202753 -0.34571523177178781 0.17105492079290219
		-0.0006255044354212927 0.75797231474047744 0.17105492078903925
		-0.00062550443542079331 0.75797231476930882 0.17105492078903825
		0.71223361237196559 -0.34571523177178748 0.17105492079290119
		0.71223361232202753 -0.34571523177178781 0.17105492079290219
		0.71223649540556788 -0.34571523182172598 0.17105492079290169
		0.71223649545550527 -0.3457152318217257 0.17105492079290069
		-0.00062550443552611781 0.0057595684912915904 -1.0313242355259606
		-0.67071254510233513 -0.3457150127990638 0.17105492078903875
		-0.0006255044354212927 0.75797231474047744 0.17105492078903925
		-0.00062550443542079331 0.75797231476930882 0.17105492078903825
		-0.00062550443552611781 0.0057595684912915904 -1.0313242355259606
		;
createNode transform -n "LowSQS_Ctrl_Grp_REF" -p "SQS_Contorls_Grp_REF";
	rename -uid "24D677AE-42D0-0433-D2A8-43B426EB1DC4";
	setAttr ".t" -type "double3" 2.8610229509783534e-06 145.74526977539062 12.187718955304476 ;
createNode transform -n "LowSQS_Ctrl_Grp_Offset_REF" -p "LowSQS_Ctrl_Grp_REF";
	rename -uid "4B6C87AF-4C30-072B-75F6-CABC726E5B92";
	setAttr ".r" -type "double3" 2.6806279861035226e-06 1.9297754532801719e-12 -1.1654904884638045e-12 ;
	setAttr ".rp" -type "double3" 3.8040374072596664e-05 1.5258782951832472e-05 -13.035833743821 ;
	setAttr ".sp" -type "double3" 3.8040374072596664e-05 1.5258782951832472e-05 -13.035833743821 ;
createNode transform -n "LowSQS_Ctrl_REF" -p "LowSQS_Ctrl_Grp_Offset_REF";
	rename -uid "85CA9510-4643-C47C-FDCB-47B474E0FEBC";
	addAttr -ci true -sn "zooTranslateTrack" -ln "zooTranslateTrack" -at "double3" 
		-nc 3;
	addAttr -ci true -sn "zooTranslateTrack_x" -ln "zooTranslateTrack_x" -at "double" 
		-p "zooTranslateTrack";
	addAttr -ci true -sn "zooTranslateTrack_y" -ln "zooTranslateTrack_y" -at "double" 
		-p "zooTranslateTrack";
	addAttr -ci true -sn "zooTranslateTrack_z" -ln "zooTranslateTrack_z" -at "double" 
		-p "zooTranslateTrack";
	addAttr -ci true -sn "zooTranslateDefault" -ln "zooTranslateDefault" -at "double3" 
		-nc 3;
	addAttr -ci true -sn "zooTranslateDefault_x" -ln "zooTranslateDefault_x" -at "double" 
		-p "zooTranslateDefault";
	addAttr -ci true -sn "zooTranslateDefault_y" -ln "zooTranslateDefault_y" -at "double" 
		-p "zooTranslateDefault";
	addAttr -ci true -sn "zooTranslateDefault_z" -ln "zooTranslateDefault_z" -at "double" 
		-p "zooTranslateDefault";
	addAttr -ci true -sn "zooRotateTrack" -ln "zooRotateTrack" -at "double3" -nc 3;
	addAttr -ci true -sn "zooRotateTrack_x" -ln "zooRotateTrack_x" -at "double" -p "zooRotateTrack";
	addAttr -ci true -sn "zooRotateTrack_y" -ln "zooRotateTrack_y" -at "double" -p "zooRotateTrack";
	addAttr -ci true -sn "zooRotateTrack_z" -ln "zooRotateTrack_z" -at "double" -p "zooRotateTrack";
	addAttr -ci true -sn "zooRotateDefault" -ln "zooRotateDefault" -at "double3" -nc 
		3;
	addAttr -ci true -sn "zooRotateDefault_x" -ln "zooRotateDefault_x" -at "double" 
		-p "zooRotateDefault";
	addAttr -ci true -sn "zooRotateDefault_y" -ln "zooRotateDefault_y" -at "double" 
		-p "zooRotateDefault";
	addAttr -ci true -sn "zooRotateDefault_z" -ln "zooRotateDefault_z" -at "double" 
		-p "zooRotateDefault";
	addAttr -ci true -sn "zooScaleTrack" -ln "zooScaleTrack" -at "double3" -nc 3;
	addAttr -ci true -sn "zooScaleTrack_x" -ln "zooScaleTrack_x" -at "double" -p "zooScaleTrack";
	addAttr -ci true -sn "zooScaleTrack_y" -ln "zooScaleTrack_y" -at "double" -p "zooScaleTrack";
	addAttr -ci true -sn "zooScaleTrack_z" -ln "zooScaleTrack_z" -at "double" -p "zooScaleTrack";
	addAttr -ci true -sn "zooScaleDefault" -ln "zooScaleDefault" -at "double3" -nc 3;
	addAttr -ci true -sn "zooScaleDefault_x" -ln "zooScaleDefault_x" -at "double" -p "zooScaleDefault";
	addAttr -ci true -sn "zooScaleDefault_y" -ln "zooScaleDefault_y" -at "double" -p "zooScaleDefault";
	addAttr -ci true -sn "zooScaleDefault_z" -ln "zooScaleDefault_z" -at "double" -p "zooScaleDefault";
	addAttr -ci true -sn "zooColorTrack" -ln "zooColorTrack" -at "double3" -nc 3;
	addAttr -ci true -sn "zooColorTrack_r" -ln "zooColorTrack_r" -at "double" -p "zooColorTrack";
	addAttr -ci true -sn "zooColorTrack_g" -ln "zooColorTrack_g" -at "double" -p "zooColorTrack";
	addAttr -ci true -sn "zooColorTrack_b" -ln "zooColorTrack_b" -at "double" -p "zooColorTrack";
	addAttr -ci true -sn "zooColorDefault" -ln "zooColorDefault" -at "double3" -nc 3;
	addAttr -ci true -sn "zooColorDefault_r" -ln "zooColorDefault_r" -at "double" -p "zooColorDefault";
	addAttr -ci true -sn "zooColorDefault_g" -ln "zooColorDefault_g" -at "double" -p "zooColorDefault";
	addAttr -ci true -sn "zooColorDefault_b" -ln "zooColorDefault_b" -at "double" -p "zooColorDefault";
	addAttr -ci true -sn "zooShapeTrack" -ln "zooShapeTrack" -dt "string";
	addAttr -ci true -sn "zooShapeDefault" -ln "zooShapeDefault" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".zooScaleTrack" -type "double3" 2 2 2 ;
	setAttr ".zooScaleDefault" -type "double3" 2 2 2 ;
	setAttr ".zooColorTrack" -type "double3" 1 0.31854677812509186 0.91140750040635399 ;
	setAttr ".zooColorDefault" -type "double3" 0.15999996662139893 0.29999992251396179 
		0.875 ;
	setAttr ".zooShapeTrack" -type "string" "cube";
	setAttr ".zooShapeDefault" -type "string" "cube";
createNode nurbsCurve -n "LowSQS_Ctrl_REF_MidSQS_Skin_jnt_grp1_MidSQS_Skin_jnt_ctrl_MidSQS_Skin_jnt_ctrlShape" 
		-p "LowSQS_Ctrl_REF";
	rename -uid "ABA5DB47-4E35-C734-87D3-779E0A89E32D";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 1 0.40400004 0.033999979 ;
	setAttr ".cc" -type "nurbsCurve" 
		1 18 0 no 3
		19 0 0.10000000000000001 0.20000000000000001 0.29999999999999999 0.40000000000000002
		 0.5 0.59999999999999998 0.69999999999999996 0.80000000000000004 0.90000000000000002
		 1 1.1000000000000001 1.2 1.3 1.3999999999999999 1.5 1.6000000000000001 1.7 1.8
		19
		-0.00062550443552611781 0.0057595684912915904 -1.0313242355259606
		-0.00062550443542079331 0.75797231476930882 0.17105492078903825
		0.71223361237196559 -0.34571523177178748 0.17105492079290119
		0.71223649545550527 -0.3457152318217257 0.17105492079290069
		-0.00062550443552611781 0.0057595684912915904 -1.0313242355259606
		-0.67071254510233513 -0.3457150127990638 0.17105492078903875
		0.71223649540556788 -0.34571523182172598 0.17105492079290169
		0.71223361232202753 -0.34571523177178781 0.17105492079290219
		-0.0006255044354212927 0.75797231474047744 0.17105492078903925
		-0.00062550443542079331 0.75797231476930882 0.17105492078903825
		0.71223361237196559 -0.34571523177178748 0.17105492079290119
		0.71223361232202753 -0.34571523177178781 0.17105492079290219
		0.71223649540556788 -0.34571523182172598 0.17105492079290169
		0.71223649545550527 -0.3457152318217257 0.17105492079290069
		-0.00062550443552611781 0.0057595684912915904 -1.0313242355259606
		-0.67071254510233513 -0.3457150127990638 0.17105492078903875
		-0.0006255044354212927 0.75797231474047744 0.17105492078903925
		-0.00062550443542079331 0.75797231476930882 0.17105492078903825
		-0.00062550443552611781 0.0057595684912915904 -1.0313242355259606
		;
createNode transform -n "TopSQS_Ctrl_Grp_REF" -p "SQS_Contorls_Grp_REF";
	rename -uid "1EDFAEE5-4FE7-ED82-B987-CC9DB93C7CEF";
	setAttr ".t" -type "double3" 2.8610229434401396e-06 162.71981811523438 12.187718955304476 ;
createNode transform -n "TopSQS_Ctrl_Grp_Offset_REF" -p "TopSQS_Ctrl_Grp_REF";
	rename -uid "A2B8C5B8-448E-82D0-DAE2-65B420F43B4F";
	setAttr ".t" -type "double3" 0 1.0820407985139956e-14 -5.2100742475727838e-16 ;
	setAttr ".r" -type "double3" 1.606180114139072e-13 1.1374549918722117e-21 -5.8359137556978142e-18 ;
	setAttr ".rp" -type "double3" 3.8040374080134885e-05 -16.974533081060855 -13.035833743821 ;
	setAttr ".sp" -type "double3" 3.8040374080134885e-05 -16.974533081060855 -13.035833743821 ;
createNode transform -n "TopSQS_Ctrl_REF" -p "TopSQS_Ctrl_Grp_Offset_REF";
	rename -uid "FE4B4BE9-4901-3D87-F925-6797678D9224";
	addAttr -ci true -sn "zooTranslateTrack" -ln "zooTranslateTrack" -at "double3" 
		-nc 3;
	addAttr -ci true -sn "zooTranslateTrack_x" -ln "zooTranslateTrack_x" -at "double" 
		-p "zooTranslateTrack";
	addAttr -ci true -sn "zooTranslateTrack_y" -ln "zooTranslateTrack_y" -at "double" 
		-p "zooTranslateTrack";
	addAttr -ci true -sn "zooTranslateTrack_z" -ln "zooTranslateTrack_z" -at "double" 
		-p "zooTranslateTrack";
	addAttr -ci true -sn "zooTranslateDefault" -ln "zooTranslateDefault" -at "double3" 
		-nc 3;
	addAttr -ci true -sn "zooTranslateDefault_x" -ln "zooTranslateDefault_x" -at "double" 
		-p "zooTranslateDefault";
	addAttr -ci true -sn "zooTranslateDefault_y" -ln "zooTranslateDefault_y" -at "double" 
		-p "zooTranslateDefault";
	addAttr -ci true -sn "zooTranslateDefault_z" -ln "zooTranslateDefault_z" -at "double" 
		-p "zooTranslateDefault";
	addAttr -ci true -sn "zooRotateTrack" -ln "zooRotateTrack" -at "double3" -nc 3;
	addAttr -ci true -sn "zooRotateTrack_x" -ln "zooRotateTrack_x" -at "double" -p "zooRotateTrack";
	addAttr -ci true -sn "zooRotateTrack_y" -ln "zooRotateTrack_y" -at "double" -p "zooRotateTrack";
	addAttr -ci true -sn "zooRotateTrack_z" -ln "zooRotateTrack_z" -at "double" -p "zooRotateTrack";
	addAttr -ci true -sn "zooRotateDefault" -ln "zooRotateDefault" -at "double3" -nc 
		3;
	addAttr -ci true -sn "zooRotateDefault_x" -ln "zooRotateDefault_x" -at "double" 
		-p "zooRotateDefault";
	addAttr -ci true -sn "zooRotateDefault_y" -ln "zooRotateDefault_y" -at "double" 
		-p "zooRotateDefault";
	addAttr -ci true -sn "zooRotateDefault_z" -ln "zooRotateDefault_z" -at "double" 
		-p "zooRotateDefault";
	addAttr -ci true -sn "zooScaleTrack" -ln "zooScaleTrack" -at "double3" -nc 3;
	addAttr -ci true -sn "zooScaleTrack_x" -ln "zooScaleTrack_x" -at "double" -p "zooScaleTrack";
	addAttr -ci true -sn "zooScaleTrack_y" -ln "zooScaleTrack_y" -at "double" -p "zooScaleTrack";
	addAttr -ci true -sn "zooScaleTrack_z" -ln "zooScaleTrack_z" -at "double" -p "zooScaleTrack";
	addAttr -ci true -sn "zooScaleDefault" -ln "zooScaleDefault" -at "double3" -nc 3;
	addAttr -ci true -sn "zooScaleDefault_x" -ln "zooScaleDefault_x" -at "double" -p "zooScaleDefault";
	addAttr -ci true -sn "zooScaleDefault_y" -ln "zooScaleDefault_y" -at "double" -p "zooScaleDefault";
	addAttr -ci true -sn "zooScaleDefault_z" -ln "zooScaleDefault_z" -at "double" -p "zooScaleDefault";
	addAttr -ci true -sn "zooColorTrack" -ln "zooColorTrack" -at "double3" -nc 3;
	addAttr -ci true -sn "zooColorTrack_r" -ln "zooColorTrack_r" -at "double" -p "zooColorTrack";
	addAttr -ci true -sn "zooColorTrack_g" -ln "zooColorTrack_g" -at "double" -p "zooColorTrack";
	addAttr -ci true -sn "zooColorTrack_b" -ln "zooColorTrack_b" -at "double" -p "zooColorTrack";
	addAttr -ci true -sn "zooColorDefault" -ln "zooColorDefault" -at "double3" -nc 3;
	addAttr -ci true -sn "zooColorDefault_r" -ln "zooColorDefault_r" -at "double" -p "zooColorDefault";
	addAttr -ci true -sn "zooColorDefault_g" -ln "zooColorDefault_g" -at "double" -p "zooColorDefault";
	addAttr -ci true -sn "zooColorDefault_b" -ln "zooColorDefault_b" -at "double" -p "zooColorDefault";
	addAttr -ci true -sn "zooShapeTrack" -ln "zooShapeTrack" -dt "string";
	addAttr -ci true -sn "zooShapeDefault" -ln "zooShapeDefault" -dt "string";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -2.8765454564084833e-20 0 7.293700639804996e-15 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".zooScaleTrack" -type "double3" 2 2 2 ;
	setAttr ".zooScaleDefault" -type "double3" 2 2 2 ;
	setAttr ".zooColorTrack" -type "double3" 1 0.31854677812509186 0.91140750040635399 ;
	setAttr ".zooColorDefault" -type "double3" 0.15999996662139893 0.29999992251396179 
		0.875 ;
	setAttr ".zooShapeTrack" -type "string" "cube";
	setAttr ".zooShapeDefault" -type "string" "cube";
createNode nurbsCurve -n "MidSQS_Skin_jnt_grp2_MidSQS_Skin_jnt_ctrl_MidSQS_Skin_jnt_ctrlShape" 
		-p "TopSQS_Ctrl_REF";
	rename -uid "4760FE22-4768-B56B-BC4F-859D882CEADF";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 1 0.40400004 0.033999979 ;
	setAttr ".cc" -type "nurbsCurve" 
		1 18 0 no 3
		19 0 0.10000000000000001 0.20000000000000001 0.29999999999999999 0.40000000000000002
		 0.5 0.59999999999999998 0.69999999999999996 0.80000000000000004 0.90000000000000002
		 1 1.1000000000000001 1.2 1.3 1.3999999999999999 1.5 1.6000000000000001 1.7 1.8
		19
		-0.00062550443552611781 0.0057595684912915904 -1.0313242355259606
		-0.00062550443542079331 0.75797231476930882 0.17105492078903825
		0.71223361237196559 -0.34571523177178748 0.17105492079290119
		0.71223649545550527 -0.3457152318217257 0.17105492079290069
		-0.00062550443552611781 0.0057595684912915904 -1.0313242355259606
		-0.67071254510233513 -0.3457150127990638 0.17105492078903875
		0.71223649540556788 -0.34571523182172598 0.17105492079290169
		0.71223361232202753 -0.34571523177178781 0.17105492079290219
		-0.0006255044354212927 0.75797231474047744 0.17105492078903925
		-0.00062550443542079331 0.75797231476930882 0.17105492078903825
		0.71223361237196559 -0.34571523177178748 0.17105492079290119
		0.71223361232202753 -0.34571523177178781 0.17105492079290219
		0.71223649540556788 -0.34571523182172598 0.17105492079290169
		0.71223649545550527 -0.3457152318217257 0.17105492079290069
		-0.00062550443552611781 0.0057595684912915904 -1.0313242355259606
		-0.67071254510233513 -0.3457150127990638 0.17105492078903875
		-0.0006255044354212927 0.75797231474047744 0.17105492078903925
		-0.00062550443542079331 0.75797231476930882 0.17105492078903825
		-0.00062550443552611781 0.0057595684912915904 -1.0313242355259606
		;
createNode transform -n "HeadSqs_move_Ctrl_Grp_REF" -p "SQS_Contorls_Grp_REF";
	rename -uid "E7233B59-4976-7FC6-B842-6CB59E8871C0";
	setAttr ".t" -type "double3" -8.2148793562915707e-06 179.43914298956463 -2.1930648078363939 ;
	setAttr ".r" -type "double3" -7.2317440787261664e-28 2.6714540826790191e-05 1.4924233448651719e-05 ;
	setAttr ".s" -type "double3" 0.99999999999999944 0.99999999999999944 0.99999999999999978 ;
createNode transform -n "HeadSqs_move_Ctrl_REF" -p "HeadSqs_move_Ctrl_Grp_REF";
	rename -uid "C44D937E-4740-7850-D5A8-0687A94790F7";
	setAttr -l on -k off ".v";
createNode transform -n "HeadSqs_Ctrl_Grp_REF" -p "HeadSqs_move_Ctrl_REF";
	rename -uid "0E99CEB1-42BC-0A2C-F9A2-849137CAEBF7";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
createNode transform -n "HeadSqs_Ctrl_REF" -p "HeadSqs_Ctrl_Grp_REF";
	rename -uid "83E1C137-432B-E7F6-F620-999D58174CFD";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 2.1640815546763439e-14 3.7269026162715588e-16 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "HeadSqs_Ctrl_REFShape" -p "HeadSqs_Ctrl_REF";
	rename -uid "7AF9DB96-4C9D-6E0B-9492-3B8794D0C2B5";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovc" 30;
	setAttr ".ovrgb" -type "float3" 1 0.40400004 0.033999979 ;
	setAttr ".cc" -type "nurbsCurve" 
		1 72 0 no 3
		73 0 2.3135629999999998 4.6271259999999996 6.9406889999999999 8.5778230000000004
		 10.214957999999999 11.849695000000001 13.484432 15.119168999999999 16.744737000000001
		 18.379473999999998 20.014211 21.648949000000002 23.283685999999999 24.920819999999999
		 26.557953999999999 28.871517000000001 31.185079999999999 33.498643000000001 35.812206000000003
		 38.125770000000003 40.439332999999998 42.076467000000001 43.713600999999997 45.348337999999998
		 46.983074999999999 48.617812000000001 50.252549999999999 51.878118000000001 53.512855000000002
		 55.147592000000003 56.782328999999997 58.419463 60.056598000000001 62.370161000000003
		 64.683723999999998 66.997287 69.310850000000002 71.624413000000004 73.937976000000006
		 75.575109999999995 77.212243999999998 78.846981999999997 80.481718999999998 82.116455999999999
		 83.742024000000001 85.376761000000002 87.011498000000003 88.646235000000004 90.280972000000006
		 91.918107000000006 93.555240999999995 95.868803999999997 98.182366999999999 100.49593
		 102.809493 105.12305600000001 107.43661899999999 109.07375399999999 110.710888 112.345625
		 113.980362 115.615099 117.240667 118.875404 120.510142 122.144879 123.779616 125.41674999999999
		 127.053884 129.367447 131.68100999999999 133.994574
		73
		7.2844652853453878e-17 -0.90984384721997524 -2.492696997203852
		0.46855437399122707 -0.71132202192430372 -2.4160581364849101
		0.93710874798245414 -0.52242409611988538 -2.2547280642240279
		1.4056634081777284 -0.34977187104919094 -2.0143621723622989
		0.93710874798245414 -0.34977187104919094 -2.0143621723622989
		0.46855437399122707 -0.34977187104919094 -2.0143621723622989
		0.46855437399122707 -0.19941718383889825 -1.703386021798162
		0.46855437399122707 -0.076630540677685821 -1.3327005522204849
		0.46855437399122707 0.014283906092199336 -0.91529942729955427
		0.46581397025459587 0.070139527723149506 -0.46855437399122674
		0.91529942729955438 0.014283906092199336 -0.46855437399122674
		1.3327005522204851 -0.076630540677685433 -0.46855437399122668
		1.7033860217981625 -0.19941718383889731 -0.46855437399122651
		2.0143621723622989 -0.34977187104918972 -0.46855437399122651
		2.0143621723622989 -0.34977187104918994 -0.93710874798245347
		2.0143621723622989 -0.34977187104919044 -1.4056634081777284
		2.2547280642240279 -0.52242409611988494 -0.93710874798245347
		2.4160581364849105 -0.71132202192430349 -0.46855437399122646
		2.4926969972038528 -0.9098438472199748 1.1213397862289161e-15
		2.4160581364849105 -0.71132202192430305 0.46855437399122785
		2.2547280642240279 -0.52242409611988327 0.93710874798245436
		2.0143621723622989 -0.34977187104918928 1.4056634081777284
		2.0143621723622989 -0.3497718710491895 0.93710874798245436
		2.0143621723622989 -0.3497718710491895 0.46855437399122762
		1.7033860217981625 -0.19941718383889731 0.46855437399122762
		1.3327005522204851 -0.076630540677684864 0.46855437399122729
		0.91529942729955438 0.014283906092200238 0.46855437399122729
		0.46581397025459587 0.070139527723150158 0.46855437399122729
		0.46855437399122707 0.014283906092200514 0.91529942729955438
		0.46855437399122707 -0.076630540677684628 1.3327005522204858
		0.46855437399122707 -0.19941718383889701 1.7033860217981631
		0.46855437399122707 -0.34977187104918894 2.0143621723622993
		0.93710874798245414 -0.34977187104918894 2.0143621723622993
		1.4056634081777284 -0.34977187104918894 2.0143621723622993
		0.93710874798245414 -0.52242409611988283 2.2547280642240284
		0.46855437399122707 -0.71132202192430249 2.416058136484911
		7.2844652853453878e-17 -0.90984384721997291 2.4926969972038528
		-0.46855437399122707 -0.71132202192430249 2.416058136484911
		-0.93710874798245414 -0.52242409611988283 2.2547280642240284
		-1.4056634081777284 -0.34977187104918894 2.0143621723622993
		-0.93710874798245414 -0.34977187104918894 2.0143621723622993
		-0.46855437399122707 -0.34977187104918894 2.0143621723622993
		-0.46855437399122707 -0.19941718383889701 1.7033860217981631
		-0.46855437399122707 -0.076630540677684628 1.3327005522204858
		-0.46855437399122707 0.014283906092200514 0.91529942729955438
		-0.46581397025459587 0.070139527723150158 0.46855437399122729
		-0.91529942729955438 0.014283906092200238 0.46855437399122729
		-1.3327005522204851 -0.076630540677684864 0.46855437399122729
		-1.7033860217981625 -0.19941718383889731 0.46855437399122762
		-2.0143621723622989 -0.3497718710491895 0.46855437399122762
		-2.0143621723622989 -0.3497718710491895 0.93710874798245436
		-2.0143621723622989 -0.34977187104918928 1.4056634081777284
		-2.2547280642240279 -0.52242409611988327 0.93710874798245436
		-2.4160581364849105 -0.71132202192430305 0.46855437399122785
		-2.4926969972038528 -0.9098438472199748 1.1213397862289161e-15
		-2.4160581364849105 -0.71132202192430349 -0.46855437399122646
		-2.2547280642240279 -0.52242409611988494 -0.93710874798245347
		-2.0143621723622989 -0.34977187104919044 -1.4056634081777284
		-2.0143621723622989 -0.34977187104918994 -0.93710874798245347
		-2.0143621723622989 -0.34977187104918972 -0.46855437399122651
		-1.7033860217981625 -0.19941718383889731 -0.46855437399122651
		-1.3327005522204851 -0.076630540677685433 -0.46855437399122668
		-0.91529942729955438 0.014283906092199336 -0.46855437399122674
		-0.46855437399122707 0.070139527723149506 -0.46581397025459531
		-0.46855437399122707 0.014283906092199336 -0.91529942729955427
		-0.46855437399122707 -0.076630540677685821 -1.3327005522204849
		-0.46855437399122707 -0.19941718383889825 -1.703386021798162
		-0.46855437399122707 -0.34977187104919094 -2.0143621723622989
		-0.93710874798245414 -0.34977187104919094 -2.0143621723622989
		-1.4056634081777284 -0.34977187104919094 -2.0143621723622989
		-0.93710874798245414 -0.52242409611988538 -2.2547280642240279
		-0.46855437399122707 -0.71132202192430372 -2.4160581364849101
		7.2844652853453878e-17 -0.90984384721997524 -2.492696997203852
		;
createNode transform -n "MouthMaster_Ctrl_Grp_static" -p "SQS_Contorls_Grp_REF";
	rename -uid "FA9AD2C6-4F6F-0FAE-659A-75930B115721";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".t" -type "double3" 7.831088104380123e-05 149.99999999997377 11.438073791822694 ;
	setAttr ".r" -type "double3" -7.2317440787261538e-28 -2.6532703770262447e-05 -1.4925888208559538e-05 ;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999967 0.99999999999999989 ;
createNode transform -n "MouthMaster_Ctrl_Matrix_Grp_static" -p "MouthMaster_Ctrl_Grp_static";
	rename -uid "D3102F72-437B-1326-AB65-768BC0EC7349";
	addAttr -ci true -sn "offsetAttr" -ln "offsetAttr" -at "matrix";
createNode transform -n "MouthMaster_Ctrl_static" -p "MouthMaster_Ctrl_Matrix_Grp_static";
	rename -uid "4CB53E0C-48E4-8CED-4FB8-45A94AB73572";
	addAttr -ci true -sn "zooTranslateTrack" -ln "zooTranslateTrack" -at "double3" 
		-nc 3;
	addAttr -ci true -sn "zooTranslateTrack_x" -ln "zooTranslateTrack_x" -at "double" 
		-p "zooTranslateTrack";
	addAttr -ci true -sn "zooTranslateTrack_y" -ln "zooTranslateTrack_y" -at "double" 
		-p "zooTranslateTrack";
	addAttr -ci true -sn "zooTranslateTrack_z" -ln "zooTranslateTrack_z" -at "double" 
		-p "zooTranslateTrack";
	addAttr -ci true -sn "zooTranslateDefault" -ln "zooTranslateDefault" -at "double3" 
		-nc 3;
	addAttr -ci true -sn "zooTranslateDefault_x" -ln "zooTranslateDefault_x" -at "double" 
		-p "zooTranslateDefault";
	addAttr -ci true -sn "zooTranslateDefault_y" -ln "zooTranslateDefault_y" -at "double" 
		-p "zooTranslateDefault";
	addAttr -ci true -sn "zooTranslateDefault_z" -ln "zooTranslateDefault_z" -at "double" 
		-p "zooTranslateDefault";
	addAttr -ci true -sn "zooRotateTrack" -ln "zooRotateTrack" -at "double3" -nc 3;
	addAttr -ci true -sn "zooRotateTrack_x" -ln "zooRotateTrack_x" -at "double" -p "zooRotateTrack";
	addAttr -ci true -sn "zooRotateTrack_y" -ln "zooRotateTrack_y" -at "double" -p "zooRotateTrack";
	addAttr -ci true -sn "zooRotateTrack_z" -ln "zooRotateTrack_z" -at "double" -p "zooRotateTrack";
	addAttr -ci true -sn "zooRotateDefault" -ln "zooRotateDefault" -at "double3" -nc 
		3;
	addAttr -ci true -sn "zooRotateDefault_x" -ln "zooRotateDefault_x" -at "double" 
		-p "zooRotateDefault";
	addAttr -ci true -sn "zooRotateDefault_y" -ln "zooRotateDefault_y" -at "double" 
		-p "zooRotateDefault";
	addAttr -ci true -sn "zooRotateDefault_z" -ln "zooRotateDefault_z" -at "double" 
		-p "zooRotateDefault";
	addAttr -ci true -sn "zooScaleTrack" -ln "zooScaleTrack" -at "double3" -nc 3;
	addAttr -ci true -sn "zooScaleTrack_x" -ln "zooScaleTrack_x" -at "double" -p "zooScaleTrack";
	addAttr -ci true -sn "zooScaleTrack_y" -ln "zooScaleTrack_y" -at "double" -p "zooScaleTrack";
	addAttr -ci true -sn "zooScaleTrack_z" -ln "zooScaleTrack_z" -at "double" -p "zooScaleTrack";
	addAttr -ci true -sn "zooScaleDefault" -ln "zooScaleDefault" -at "double3" -nc 3;
	addAttr -ci true -sn "zooScaleDefault_x" -ln "zooScaleDefault_x" -at "double" -p "zooScaleDefault";
	addAttr -ci true -sn "zooScaleDefault_y" -ln "zooScaleDefault_y" -at "double" -p "zooScaleDefault";
	addAttr -ci true -sn "zooScaleDefault_z" -ln "zooScaleDefault_z" -at "double" -p "zooScaleDefault";
	addAttr -ci true -sn "zooColorTrack" -ln "zooColorTrack" -at "double3" -nc 3;
	addAttr -ci true -sn "zooColorTrack_r" -ln "zooColorTrack_r" -at "double" -p "zooColorTrack";
	addAttr -ci true -sn "zooColorTrack_g" -ln "zooColorTrack_g" -at "double" -p "zooColorTrack";
	addAttr -ci true -sn "zooColorTrack_b" -ln "zooColorTrack_b" -at "double" -p "zooColorTrack";
	addAttr -ci true -sn "zooColorDefault" -ln "zooColorDefault" -at "double3" -nc 3;
	addAttr -ci true -sn "zooColorDefault_r" -ln "zooColorDefault_r" -at "double" -p "zooColorDefault";
	addAttr -ci true -sn "zooColorDefault_g" -ln "zooColorDefault_g" -at "double" -p "zooColorDefault";
	addAttr -ci true -sn "zooColorDefault_b" -ln "zooColorDefault_b" -at "double" -p "zooColorDefault";
	addAttr -ci true -sn "zooShapeTrack" -ln "zooShapeTrack" -dt "string";
	addAttr -ci true -sn "zooShapeDefault" -ln "zooShapeDefault" -dt "string";
	addAttr -ci true -sn "Up_Down" -ln "Up_Down" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".zooScaleTrack" -type "double3" 1.4142135623730949 1.4142135623730949 1.4142135623730949 ;
	setAttr ".zooScaleDefault" -type "double3" 1.4142135623730949 1.4142135623730949 
		1.4142135623730949 ;
	setAttr ".zooColorTrack" -type "double3" 1 0.89710648295685058 0.073238955878405454 ;
	setAttr ".zooColorDefault" -type "double3" 1 1 0 ;
	setAttr ".zooShapeTrack" -type "string" "pill";
	setAttr ".zooShapeDefault" -type "string" "circle";
	setAttr -k on ".Up_Down";
createNode nurbsCurve -n "MouthMaster_Ctrl_staticShape" -p "MouthMaster_Ctrl_static";
	rename -uid "7AD19762-4EDD-BF29-0384-E7BD1AF92E78";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 0.37999988 1 0.073000014 ;
	setAttr ".cc" -type "nurbsCurve" 
		3 16 2 no 3
		21 -0.125 -0.0625 0 0.0625 0.125 0.1875 0.25 0.3125 0.375 0.4375 0.5 0.5625
		 0.625 0.6875 0.75 0.8125 0.875 0.93750000000000011 1 1.0625 1.125
		19
		-0.30348721191717759 -0.47496774305345868 4.3579802269800197e-17
		0.00038674149372474979 -0.47496818113525024 4.7308386113121428e-17
		0.29158271755652931 -0.47496774305345835 4.3579802269800197e-17
		0.47294842959211764 -0.47496655272293403 3.3448709736867961e-17
		0.74776793697196309 -0.30844854676329803 1.8051776087995345e-17
		0.83912733002587692 -0.00054529437483902374 3.8752171320490385e-22
		0.74776793697196398 0.30738589330947413 -1.8052711588101535e-17
		0.47294842959211503 0.47382100507957503 -3.3447210812180937e-17
		0.2915827175565297 0.47382219592299468 -4.3582668673178418e-17
		0.000386741493724146 0.47382263287120202 -4.7301604404447993e-17
		-0.30348721191717776 0.47382219592299468 -4.35826686731784e-17
		-0.49114369391628721 0.47382100507957431 -3.3447210812180993e-17
		-0.74344951986643359 0.30083504065057531 -1.8052711588101458e-17
		-0.83900912255516413 -0.00054529437483943639 3.8752171317653155e-22
		-0.74344951986643393 -0.3018976941044027 1.8051776087995429e-17
		-0.49114369391628687 -0.4749665527229342 3.344870973686798e-17
		-0.30348721191717759 -0.47496774305345868 4.3579802269800197e-17
		0.00038674149372474979 -0.47496818113525024 4.7308386113121428e-17
		0.29158271755652931 -0.47496774305345835 4.3579802269800197e-17
		;
createNode transform -n "UpLip_Roll_Ctrl_Grp_static" -p "MouthMaster_Ctrl_static";
	rename -uid "058DB1D4-4EFB-1F46-A42D-2BA6ADA8ACC8";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".t" -type "double3" 0 0.5 0 ;
createNode transform -n "UpLip_Roll_Ctrl_static" -p "UpLip_Roll_Ctrl_Grp_static";
	rename -uid "93815003-432F-1609-8EAF-14AB3522DA47";
	addAttr -ci true -sn "zooTranslateTrack" -ln "zooTranslateTrack" -at "double3" 
		-nc 3;
	addAttr -ci true -sn "zooTranslateTrack_x" -ln "zooTranslateTrack_x" -at "double" 
		-p "zooTranslateTrack";
	addAttr -ci true -sn "zooTranslateTrack_y" -ln "zooTranslateTrack_y" -at "double" 
		-p "zooTranslateTrack";
	addAttr -ci true -sn "zooTranslateTrack_z" -ln "zooTranslateTrack_z" -at "double" 
		-p "zooTranslateTrack";
	addAttr -ci true -sn "zooTranslateDefault" -ln "zooTranslateDefault" -at "double3" 
		-nc 3;
	addAttr -ci true -sn "zooTranslateDefault_x" -ln "zooTranslateDefault_x" -at "double" 
		-p "zooTranslateDefault";
	addAttr -ci true -sn "zooTranslateDefault_y" -ln "zooTranslateDefault_y" -at "double" 
		-p "zooTranslateDefault";
	addAttr -ci true -sn "zooTranslateDefault_z" -ln "zooTranslateDefault_z" -at "double" 
		-p "zooTranslateDefault";
	addAttr -ci true -sn "zooRotateTrack" -ln "zooRotateTrack" -at "double3" -nc 3;
	addAttr -ci true -sn "zooRotateTrack_x" -ln "zooRotateTrack_x" -at "double" -p "zooRotateTrack";
	addAttr -ci true -sn "zooRotateTrack_y" -ln "zooRotateTrack_y" -at "double" -p "zooRotateTrack";
	addAttr -ci true -sn "zooRotateTrack_z" -ln "zooRotateTrack_z" -at "double" -p "zooRotateTrack";
	addAttr -ci true -sn "zooRotateDefault" -ln "zooRotateDefault" -at "double3" -nc 
		3;
	addAttr -ci true -sn "zooRotateDefault_x" -ln "zooRotateDefault_x" -at "double" 
		-p "zooRotateDefault";
	addAttr -ci true -sn "zooRotateDefault_y" -ln "zooRotateDefault_y" -at "double" 
		-p "zooRotateDefault";
	addAttr -ci true -sn "zooRotateDefault_z" -ln "zooRotateDefault_z" -at "double" 
		-p "zooRotateDefault";
	addAttr -ci true -sn "zooScaleTrack" -ln "zooScaleTrack" -at "double3" -nc 3;
	addAttr -ci true -sn "zooScaleTrack_x" -ln "zooScaleTrack_x" -at "double" -p "zooScaleTrack";
	addAttr -ci true -sn "zooScaleTrack_y" -ln "zooScaleTrack_y" -at "double" -p "zooScaleTrack";
	addAttr -ci true -sn "zooScaleTrack_z" -ln "zooScaleTrack_z" -at "double" -p "zooScaleTrack";
	addAttr -ci true -sn "zooScaleDefault" -ln "zooScaleDefault" -at "double3" -nc 3;
	addAttr -ci true -sn "zooScaleDefault_x" -ln "zooScaleDefault_x" -at "double" -p "zooScaleDefault";
	addAttr -ci true -sn "zooScaleDefault_y" -ln "zooScaleDefault_y" -at "double" -p "zooScaleDefault";
	addAttr -ci true -sn "zooScaleDefault_z" -ln "zooScaleDefault_z" -at "double" -p "zooScaleDefault";
	addAttr -ci true -sn "zooColorTrack" -ln "zooColorTrack" -at "double3" -nc 3;
	addAttr -ci true -sn "zooColorTrack_r" -ln "zooColorTrack_r" -at "double" -p "zooColorTrack";
	addAttr -ci true -sn "zooColorTrack_g" -ln "zooColorTrack_g" -at "double" -p "zooColorTrack";
	addAttr -ci true -sn "zooColorTrack_b" -ln "zooColorTrack_b" -at "double" -p "zooColorTrack";
	addAttr -ci true -sn "zooColorDefault" -ln "zooColorDefault" -at "double3" -nc 3;
	addAttr -ci true -sn "zooColorDefault_r" -ln "zooColorDefault_r" -at "double" -p "zooColorDefault";
	addAttr -ci true -sn "zooColorDefault_g" -ln "zooColorDefault_g" -at "double" -p "zooColorDefault";
	addAttr -ci true -sn "zooColorDefault_b" -ln "zooColorDefault_b" -at "double" -p "zooColorDefault";
	addAttr -ci true -sn "zooShapeTrack" -ln "zooShapeTrack" -dt "string";
	addAttr -ci true -sn "zooShapeDefault" -ln "zooShapeDefault" -dt "string";
	addAttr -ci true -sn "Up_Down" -ln "Up_Down" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".mntl" -type "double3" -1 -0.5 -1 ;
	setAttr ".mxtl" -type "double3" 1 0.25 1 ;
	setAttr ".mtye" yes;
	setAttr ".xtye" yes;
	setAttr ".zooScaleTrack" -type "double3" 1.4142135623730949 1.4142135623730949 1.4142135623730949 ;
	setAttr ".zooScaleDefault" -type "double3" 1.4142135623730949 1.4142135623730949 
		1.4142135623730949 ;
	setAttr ".zooColorTrack" -type "double3" 1 1 0 ;
	setAttr ".zooColorDefault" -type "double3" 1 1 0 ;
	setAttr ".zooShapeTrack" -type "string" "triangle_round";
	setAttr ".zooShapeDefault" -type "string" "circle";
	setAttr -l on ".Up_Down";
createNode nurbsCurve -n "UpLip_Roll_Ctrl_staticShape" -p "UpLip_Roll_Ctrl_static";
	rename -uid "4D858CB9-4D0F-2008-B8F7-658D3BC80240";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 0.37999988 1 0.073000014 ;
	setAttr ".cc" -type "nurbsCurve" 
		3 13 0 no 3
		18 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 13 13
		16
		0.5288252823025672 0.1213629630582527 -1.134097891947801e-06
		0.56472790986353094 0.087031167660332351 -1.1452146066624796e-06
		0.52023101322056098 0.061940406941516146 -1.1485759924039905e-06
		0.46816681480611527 0.054673920494696965 -1.1435538857283823e-06
		0.39481471498775228 0.054410903828767815 -1.136478350504667e-06
		-0.38246520960684716 0.05162379508302975 -1.0615020238403659e-06
		-0.45581730942520432 0.051360778417102335 -1.054426488616526e-06
		-0.50788150783965302 0.058253894451483199 -1.0494043819519786e-06
		-0.55465076169014771 0.083775559807991895 -1.0372394736921514e-06
		-0.52102049133670336 0.11830958325852084 -1.0328298944194483e-06
		-0.091273108684814183 0.24885553936404181 -1.0183370923543075e-06
		-0.043239860160341967 0.26662952352350822 -1.0214439215583661e-06
		0.0048682415828827695 0.27392222559918333 -1.0251727823886942e-06
		0.053443830822006599 0.26705115665231494 -1.0327864729659584e-06
		0.1020942732798906 0.24969880562165436 -1.0410221951690407e-06
		0.5288252823025672 0.1213629630582527 -1.134097891947801e-06
		;
createNode transform -n "LowLip_Roll_Ctrl_Grp_static" -p "MouthMaster_Ctrl_static";
	rename -uid "2ADE0CE5-4BEB-94DA-28A8-34A1BB5CF66B";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".t" -type "double3" 0 -0.5 0 ;
	setAttr ".r" -type "double3" 0 0 -180 ;
createNode transform -n "LowLip_Roll_Ctrl_static" -p "LowLip_Roll_Ctrl_Grp_static";
	rename -uid "31FE8C56-4654-5343-2370-E18282995248";
	addAttr -ci true -sn "zooTranslateTrack" -ln "zooTranslateTrack" -at "double3" 
		-nc 3;
	addAttr -ci true -sn "zooTranslateTrack_x" -ln "zooTranslateTrack_x" -at "double" 
		-p "zooTranslateTrack";
	addAttr -ci true -sn "zooTranslateTrack_y" -ln "zooTranslateTrack_y" -at "double" 
		-p "zooTranslateTrack";
	addAttr -ci true -sn "zooTranslateTrack_z" -ln "zooTranslateTrack_z" -at "double" 
		-p "zooTranslateTrack";
	addAttr -ci true -sn "zooTranslateDefault" -ln "zooTranslateDefault" -at "double3" 
		-nc 3;
	addAttr -ci true -sn "zooTranslateDefault_x" -ln "zooTranslateDefault_x" -at "double" 
		-p "zooTranslateDefault";
	addAttr -ci true -sn "zooTranslateDefault_y" -ln "zooTranslateDefault_y" -at "double" 
		-p "zooTranslateDefault";
	addAttr -ci true -sn "zooTranslateDefault_z" -ln "zooTranslateDefault_z" -at "double" 
		-p "zooTranslateDefault";
	addAttr -ci true -sn "zooRotateTrack" -ln "zooRotateTrack" -at "double3" -nc 3;
	addAttr -ci true -sn "zooRotateTrack_x" -ln "zooRotateTrack_x" -at "double" -p "zooRotateTrack";
	addAttr -ci true -sn "zooRotateTrack_y" -ln "zooRotateTrack_y" -at "double" -p "zooRotateTrack";
	addAttr -ci true -sn "zooRotateTrack_z" -ln "zooRotateTrack_z" -at "double" -p "zooRotateTrack";
	addAttr -ci true -sn "zooRotateDefault" -ln "zooRotateDefault" -at "double3" -nc 
		3;
	addAttr -ci true -sn "zooRotateDefault_x" -ln "zooRotateDefault_x" -at "double" 
		-p "zooRotateDefault";
	addAttr -ci true -sn "zooRotateDefault_y" -ln "zooRotateDefault_y" -at "double" 
		-p "zooRotateDefault";
	addAttr -ci true -sn "zooRotateDefault_z" -ln "zooRotateDefault_z" -at "double" 
		-p "zooRotateDefault";
	addAttr -ci true -sn "zooScaleTrack" -ln "zooScaleTrack" -at "double3" -nc 3;
	addAttr -ci true -sn "zooScaleTrack_x" -ln "zooScaleTrack_x" -at "double" -p "zooScaleTrack";
	addAttr -ci true -sn "zooScaleTrack_y" -ln "zooScaleTrack_y" -at "double" -p "zooScaleTrack";
	addAttr -ci true -sn "zooScaleTrack_z" -ln "zooScaleTrack_z" -at "double" -p "zooScaleTrack";
	addAttr -ci true -sn "zooScaleDefault" -ln "zooScaleDefault" -at "double3" -nc 3;
	addAttr -ci true -sn "zooScaleDefault_x" -ln "zooScaleDefault_x" -at "double" -p "zooScaleDefault";
	addAttr -ci true -sn "zooScaleDefault_y" -ln "zooScaleDefault_y" -at "double" -p "zooScaleDefault";
	addAttr -ci true -sn "zooScaleDefault_z" -ln "zooScaleDefault_z" -at "double" -p "zooScaleDefault";
	addAttr -ci true -sn "zooColorTrack" -ln "zooColorTrack" -at "double3" -nc 3;
	addAttr -ci true -sn "zooColorTrack_r" -ln "zooColorTrack_r" -at "double" -p "zooColorTrack";
	addAttr -ci true -sn "zooColorTrack_g" -ln "zooColorTrack_g" -at "double" -p "zooColorTrack";
	addAttr -ci true -sn "zooColorTrack_b" -ln "zooColorTrack_b" -at "double" -p "zooColorTrack";
	addAttr -ci true -sn "zooColorDefault" -ln "zooColorDefault" -at "double3" -nc 3;
	addAttr -ci true -sn "zooColorDefault_r" -ln "zooColorDefault_r" -at "double" -p "zooColorDefault";
	addAttr -ci true -sn "zooColorDefault_g" -ln "zooColorDefault_g" -at "double" -p "zooColorDefault";
	addAttr -ci true -sn "zooColorDefault_b" -ln "zooColorDefault_b" -at "double" -p "zooColorDefault";
	addAttr -ci true -sn "zooShapeTrack" -ln "zooShapeTrack" -dt "string";
	addAttr -ci true -sn "zooShapeDefault" -ln "zooShapeDefault" -dt "string";
	addAttr -ci true -sn "Up_Down" -ln "Up_Down" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".mntl" -type "double3" -1 -0.5 -1 ;
	setAttr ".mxtl" -type "double3" 1 0.25 1 ;
	setAttr ".mtye" yes;
	setAttr ".xtye" yes;
	setAttr ".zooScaleTrack" -type "double3" 1.4142135623730949 1.4142135623730949 1.4142135623730949 ;
	setAttr ".zooScaleDefault" -type "double3" 1.4142135623730949 1.4142135623730949 
		1.4142135623730949 ;
	setAttr ".zooColorTrack" -type "double3" 1 1 0 ;
	setAttr ".zooColorDefault" -type "double3" 1 1 0 ;
	setAttr ".zooShapeTrack" -type "string" "pill";
	setAttr ".zooShapeDefault" -type "string" "circle";
	setAttr -l on ".Up_Down";
createNode nurbsCurve -n "LowLip_Roll_Ctrl_staticShape" -p "LowLip_Roll_Ctrl_static";
	rename -uid "040E990F-45FF-EBEC-C176-F8A45D3C2D96";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 0.37999988 1 0.073000014 ;
	setAttr ".cc" -type "nurbsCurve" 
		3 13 0 no 3
		18 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 13 13
		16
		0.52882528230256698 0.12136296305825253 -1.134097891947801e-06
		0.56472790986353094 0.087031167660332323 -1.1452146066624796e-06
		0.52023101322056109 0.061940406941516299 -1.1485759924039905e-06
		0.46816681480611516 0.054673920494696784 -1.1435538857283823e-06
		0.39481471498775234 0.054410903828767745 -1.136478350504667e-06
		-0.38246520960684721 0.051623795083029736 -1.0615020238403659e-06
		-0.45581730942520426 0.05136077841710246 -1.054426488616526e-06
		-0.50788150783965269 0.058253894451483226 -1.0494043819519786e-06
		-0.55465076169014771 0.083775559807991923 -1.0372394736921514e-06
		-0.52102049133670292 0.11830958325852076 -1.0328298944194483e-06
		-0.091273108684814169 0.24885553936404162 -1.0183370923543075e-06
		-0.043239860160341953 0.26662952352350811 -1.0214439215583661e-06
		0.004868241582882793 0.27392222559918328 -1.0251727823886942e-06
		0.053443830822006655 0.26705115665231494 -1.0327864729659584e-06
		0.10209427327989053 0.24969880562165425 -1.0410221951690407e-06
		0.52882528230256698 0.12136296305825253 -1.134097891947801e-06
		;
createNode transform -n "MouthMaster_Driver_Ctrl_Grp" -p "SQS_Contorls_Grp_REF";
	rename -uid "990F6CC3-44C9-E126-E01B-44AAE8B4AED7";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".t" -type "double3" 7.8351363562072045e-05 149.99999999997695 2.5000000000659699 ;
	setAttr ".r" -type "double3" -7.2317440787261538e-28 -2.6532703770262447e-05 -1.4925888208559538e-05 ;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999967 0.99999999999999989 ;
createNode transform -n "MouthMaster_Driver_Ctrl_Matrix_Grp" -p "MouthMaster_Driver_Ctrl_Grp";
	rename -uid "44E57F06-4890-53DE-4E13-46A3C092A39E";
	setAttr ".t" -type "double3" 4.4300156517804986e-24 -0.00018680095672607422 -1.3132458889231202e-06 ;
	setAttr ".s" -type "double3" 1.0000243186950684 0.99995148181915283 1.0000243186950684 ;
createNode transform -n "MouthMaster_Driver_Ctrl" -p "MouthMaster_Driver_Ctrl_Matrix_Grp";
	rename -uid "E297598B-417C-D317-C8BE-D2B3922A0854";
	setAttr ".t" -type "double3" -8.6820877093565785e-21 -7.2118960109390784e-15 0 ;
createNode nurbsCurve -n "MouthMaster_Driver_CtrlShape" -p "MouthMaster_Driver_Ctrl";
	rename -uid "57345EBE-4188-7B92-D202-599DF5FB827B";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovrgbf" yes;
	setAttr ".ovrgb" -type "float3" 0.073238954 0.2019069 1 ;
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-1.1102230246251565e-16 1.4142135623730949 7.2808192543013419
		-1.1102230246251565e-16 1.4142135623730949 7.2807992543018472
		1.4142135623730949 1.1102230246251565e-16 7.2807992543018472
		1.4142135623730949 1.1102230246251565e-16 7.2808192543013419
		-1.1102230246251565e-16 1.4142135623730949 7.2808192543013419
		-1.4142135623730949 -1.1102230246251565e-16 7.2808192543013419
		-1.4142135623730949 -1.1102230246251565e-16 7.2807992543018472
		-1.1102230246251565e-16 1.4142135623730949 7.2807992543018472
		-1.1102230246251565e-16 1.4142135623730949 7.2808192543013419
		-1.4142135623730949 -1.1102230246251565e-16 7.2808192543013419
		1.1102230246251565e-16 -1.4142135623730949 7.2808192543013419
		1.4142135623730949 1.1102230246251565e-16 7.2808192543013419
		1.4142135623730949 1.1102230246251565e-16 7.2807992543018472
		1.1102230246251565e-16 -1.4142135623730949 7.2807992543018472
		1.1102230246251565e-16 -1.4142135623730949 7.2808192543013419
		1.1102230246251565e-16 -1.4142135623730949 7.2807992543018472
		-1.4142135623730949 -1.1102230246251565e-16 7.2807992543018472
		;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "111B5A09-4FFE-40E4-DD55-26BF68D7D715";
	setAttr -s 12 ".lnk";
	setAttr -s 4 ".slnk";
createNode VRaySettingsNode -s -n "vraySettings";
	rename -uid "2CC13D67-47E0-5F20-1BB8-14AA1CFC902A";
	setAttr ".sver" 1;
	setAttr ".gi" yes;
	setAttr ".rfc" yes;
	setAttr ".pe" 2;
	setAttr ".se" 3;
	setAttr ".cmph" 60;
	setAttr ".csdu" 0;
	setAttr ".csdp" 1;
	setAttr ".cfile" -type "string" "";
	setAttr ".cfile2" -type "string" "";
	setAttr ".casf" -type "string" "";
	setAttr ".casf2" -type "string" "";
	setAttr ".st" 3;
	setAttr ".msr" 6;
	setAttr ".aaft" 3;
	setAttr ".aafs" 2;
	setAttr ".dma" 24;
	setAttr ".dam" 1;
	setAttr ".dac" 1.5;
	setAttr ".pt" 0.0099999997764825821;
	setAttr ".pmt" 0;
	setAttr ".sd" 1000;
	setAttr ".ss" 0.01;
	setAttr ".pfts" 20;
	setAttr ".ufg" yes;
	setAttr ".fnm" -type "string" "";
	setAttr ".lcfnm" -type "string" "";
	setAttr ".asf" -type "string" "";
	setAttr ".lcasf" -type "string" "";
	setAttr ".urtrshd" yes;
	setAttr ".rtrshd" 2;
	setAttr ".lightCacheType" 1;
	setAttr ".ifile" -type "string" "";
	setAttr ".ifile2" -type "string" "";
	setAttr ".iasf" -type "string" "";
	setAttr ".iasf2" -type "string" "";
	setAttr ".pmfile" -type "string" "";
	setAttr ".pmfile2" -type "string" "";
	setAttr ".pmasf" -type "string" "";
	setAttr ".pmasf2" -type "string" "";
	setAttr ".dmcstd" yes;
	setAttr ".dmculs" no;
	setAttr ".dmcsat" 0.004999999888241291;
	setAttr ".cmtp" 6;
	setAttr ".cmao" 2;
	setAttr ".cg" 2.2000000476837158;
	setAttr ".mtah" yes;
	setAttr ".rgbcs" -1;
	setAttr ".suv" 0;
	setAttr ".srflc" 1;
	setAttr ".seu" yes;
	setAttr ".gomsb" yes;
	setAttr ".gormio" yes;
	setAttr ".gocle" yes;
	setAttr ".gopl" 2;
	setAttr ".gopv" yes;
	setAttr ".gopvgs" 1;
	setAttr ".wi" 960;
	setAttr ".he" 540;
	setAttr ".aspr" 1.7777780294418335;
	setAttr ".productionGPUResizeTextures" 0;
	setAttr ".autolt" 0;
	setAttr ".jpegq" 100;
	setAttr ".vfbOn" yes;
	setAttr ".vfbSA" -type "Int32Array" 1090 0 4352 1 4340 0 1
		 4332 1700143739 1869181810 825893486 1632379436 1936876921 578501154 1936876886 577662825 573321530 1935764579 574235251
		 1953460082 1881287714 1701867378 1701409906 2067407475 1919252002 1852795251 741423650 1835101730 574235237 1696738338 1818386798
		 1949966949 744846706 1886938402 577007201 1818322490 573334899 1634760805 1650549870 975332716 1702195828 1931619453 1814913653
		 1919252833 1530536563 1818436219 577991521 1751327290 779317089 1886611812 1132028268 1701999215 1869182051 573317742 1886351984
		 1769239141 975336293 1702240891 1869181810 825893486 1634607660 975332717 1936278562 2036427888 1919894304 1952671090 577662825
		 1852121644 1701601889 1920219682 573334901 1634760805 975332462 1702195828 2019893804 1684955504 1701601889 1920219682 573334901
		 1718579824 577072233 573321786 1869641829 1701999987 774912546 1931619376 1600484961 1600284530 1835627120 1986622569 975336293
		 1936482662 1864510565 1601136995 1701603686 1126316578 1917857594 1634887535 1766203501 796091756 1869903169 1802724708 2036419887
		 842019425 1701982002 1920298867 796091747 1330201423 1852793645 1936157030 2036419887 1701588321 2036556135 1852793647 778529126
		 1869177711 1864510498 1601136995 1869377379 1634759538 975332707 1864510512 1601136995 1886611812 1685676396 1667855973 809116261
		 1668227628 1985965929 1953981801 1936613746 1836216166 741358114 1768124194 1634951023 1852401014 1734438249 1715085925 1702063201
		 1667834412 1768316771 975332716 573317666 1600349033 1718579824 1600482409 1635017060 1970158114 573336684 1600349033 1684956530
		 1702129257 975336558 1646406704 1953524588 1986948963 1634089506 2103800684 1970479660 1634479458 1936876921 2069576226 1634493218
		 975336307 1634231074 1915646831 1932420709 1668445551 1819240037 577922404 1919951404 1919250543 1936025972 578501154 1936876918
		 577662825 573321530 1701667182 1394752034 1668445551 1377843813 740442695 1634624802 577072226 1970435130 1696738405 1851879544
		 1715085924 1702063201 2019893804 1684955504 1701601889 1634089506 744846188 1701601826 1834968174 577070191 573321274 1667330159
		 578385001 808333626 1752375852 1885304687 1769366898 975337317 1702195828 1920148012 2037669731 975332720 573340976 761427315
		 1702453612 975336306 1663204187 1936941420 1663187490 1936679272 1717924398 1734962222 1768780904 573317752 1886351984 1769239141
		 975336293 1702240891 1869181810 825893486 1634607660 975332717 1734954018 1766683752 573317752 1650552421 975332716 1936482662
		 1696738405 1851879544 1715085924 1702063201 2019893804 1684955504 1701601889 1920219682 746415477 1651864354 2036427821 577991269
		 2103270202 1663204140 1936941420 1663187490 1936679272 1717924398 1836016430 1769172848 740451700 1869770786 1953654128 577987945
		 1981971258 1769173605 975335023 1847733297 577072481 1866670650 1936683117 577074281 1852121644 1701601889 1634089506 744846188
		 1886938402 577007201 1818322490 573334899 1634760805 1650549870 975332716 1702195828 1818370604 1600417381 1701080941 741358114
		 1634758434 2037672291 774978082 1931619376 1601662824 1986359920 578250089 1970435130 573341029 761427315 1702453612 975336306
		 1568496987 578497661 1935764579 574235251 1868654691 1701850739 1852138542 1702062447 573317746 1886351984 1769239141 975336293
		 1702240891 1869181810 825893486 1634607660 975332717 1852130338 1702062447 1965046386 1635148142 1650551913 740451692 1634624802
		 577072226 1818322490 573334899 1634760805 975332462 1936482662 1696738405 1851879544 1818386788 1949966949 744846706 1701601826
		 1834968174 577070191 573321274 1667330159 578385001 808333626 1752375852 1885304687 1769366898 975337317 1702195828 1667310124
		 1702259060 1634089506 744846188 1701998626 578053491 573321786 1701999731 1752459118 774978082 1914842160 1969841249 825893491
		 741355056 1098344482 1818583907 1952543333 1949966949 744846706 1852138530 1702062447 1869438834 975332708 573340977 761427315
		 1702453612 975336306 746413403 1818436219 577991521 1751327290 779317089 1932420464 1886544232 1647210085 577926508 1919951404
		 1919250543 1936025972 578501154 1936876918 577662825 573321530 1701667182 1394752034 1886544232 1110404709 577926508 1852121644
		 1701601889 1634089506 744846188 1886938402 577007201 1818322490 573334899 1634760805 1650549870 975332716 1702195828 1818370604
		 1600417381 1701080941 741358114 1634758434 2037672291 774978082 1931619376 1601662824 1986359920 578250089 1970435130 1931619429
		 1886544232 1650421349 1601336684 1886220131 577074293 1818322490 573334899 1918986355 1601070448 1970236769 975336558 741355057
		 1634235170 1852141682 1684107871 577992041 892219450 1818370604 1918857845 1969841249 825893491 808661806 808464432 825831988
		 573340981 761427315 1702453612 975336306 746413403 1818436219 577991521 1751327290 779317089 1814979952 1718840933 573317752
		 1886351984 1769239141 975336293 1702240891 1869181810 825893486 1634607660 975332717 1852132386 1715806323 1952671078 573317747
		 1650552421 975332716 1936482662 1696738405 1851879544 1715085924 1702063201 2019893804 1684955504 1701601889 1920219682 573334901
		 1852140642 1869438820 975332708 1864510514 1768120688 975337844 741355057 1869116194 1919967095 1701410405 1949966967 744846706
		 1634494242 1868522866 1715085934 1702063201 1818698284 1600483937 1702521203 808663586 573321262 1918987367 1702322021 1952999273
		 774978082 1646406704 1836019564 1768257375 578054247 875442234 808464432 892350512 573322809 1918987367 1752457061 1752393074
		 577006703 808333626 1768301100 1919251564 1953460831 1869182049 809116270 573321262 1970561395 1769234802 975335023 741355057
		 1918986274 1918990180 1667325797 1701602659 1702125938 1949966948 744846706 1819239202 1635213156 975334770 1936482662 1763847269
		 1919251566 1769235297 975332726 1702195828 1634935340 1734305142 1701994860 1920219682 573334901 1701536098 1634494303 2002740594
		 1751607653 1715085940 1702063201 1919099436 1702125925 1717986655 1937007461 1936028255 1601465461 1851877475 577529198 1818322490
		 573334899 1650552421 1650419052 1701077356 1949966963 744846706 1684632354 975336293 1646406710 1701077356 1869766515 1769234804
		 975335023 808334641 1953702444 1801545074 1970037343 809116274 808464942 808464432 943272496 1937056300 1919377253 1852404833
		 1715085927 1702063201 1919361580 1852404833 1701076839 1953067886 893002361 741355056 1634887458 1735289204 1852140639 577270887
		 774910266 1730292784 1769234802 2053072750 577597295 808334650 1919361580 1852404833 1819500391 577073263 808333370 1919361580
		 1852404833 1953718119 1735288178 975333492 741355057 1702065442 1667460959 1769174380 975335023 1936482662 1864510565 1970037603
		 1852795251 1836675935 1920230765 975332201 1702195828 1668227628 1937075299 1601073001 1668441456 578055781 774910522 1864510512
		 1970037603 1852795251 1953460831 1869182049 809116270 573321262 1818452847 1869181813 1918984046 825893475 808333360 1937056300
		 1668505445 1668571506 1715085928 1702063201 1668489772 2037604210 1952804205 576940402 1970435130 1931619429 1885303395 1702130785
		 975335026 1931619376 1834971747 1769237621 1918987367 1868783461 578055797 573321530 1601332083 1936614756 578385001 774911290
		 1931619376 1818194531 1952935525 893002344 741355056 1919120162 1869378399 1985963376 1634300513 577069934 808333370 1668489772
		 1769430898 1600681060 1769103734 1701015137 774912546 1931619376 1935635043 577004901 573321274 1601332083 1836019578 775043618
		 1931619376 1918857827 1952543855 577662825 808333370 1668489772 1953718130 1735288178 975333492 741355057 1702065442 1937073247
		 1715085940 1702063201 1969496620 1885303923 1702130785 975335026 1679961136 1601467253 1936614756 578385001 774911290 1679961136
		 1601467253 1768186226 1985966965 1634300513 577069934 808333370 1969496620 1784640627 1702130793 809116274 573321262 1953723748
		 1869576799 842670701 573321262 1953723748 1953460831 1869182049 809116270 573321262 1953723748 1920234335 1952935525 825893480
		 573321262 1918987367 1937071973 1651466085 1667331187 1767859564 1701273965 1634089506 744846188 1634494242 1868522866 1635021666
		 1600482403 1734438249 1634754405 975333492 573317666 1953718895 1634560351 2053072231 577597295 808333626 1651450412 1767863411
		 1701273965 1953460831 1869182049 809116270 573321262 1953718895 1634560351 1935631719 1852142196 577270887 808333626 1937056300
		 1768316773 1919251564 1634560351 975332711 1936482662 1730292837 1701994860 1634560351 1885300071 577270881 740434490 1702065442
		 1919443807 1952542063 1633641321 1920099682 1869182049 1715085934 1702063201 1751327276 1634561906 1600350580 1919246945 1769234802
		 975335023 808529456 808464432 875638832 1663183929 1919249761 2037669729 975332720 1931619376 1701995892 1869438831 975332708
		 573340976 761427315 1702453612 975336306 1568496987 578497661 1935764579 574235251 1868654691 1986997875 1634497125 1953705593
		 577793377 1919951404 1919250543 1936025972 578501154 1936876918 577662825 573321530 1701667182 1394752034 1886216564 1696738338
		 1818386798 1715085925 1702063201 2019893804 1684955504 1634089506 744846188 1886938402 1633971809 577072226 1970435130 1931619429
		 1886216564 1919903839 1633647209 1852270956 741423650 1635021602 1985966189 1601466981 1734962273 859447918 1931619378 1886216564
		 1819239263 975336047 808333659 808333612 808333612 1931619421 1886216564 1852794463 2067407476 1919252002 1852795251 741423650
		 1768910882 1935635566 577075817 741355834 1835099682 578382953 573321274 1819898995 809116261 1702306348 1952999273 741358114
		 1667327522 574235237 1634300481 746398316 1635021602 1935634541 1852404340 2067407463 1919252002 1852795251 741423650 2002874914
		 1920234335 577203817 740434490 1852401186 1935633505 1852404340 574235239 746421538 1651864354 2036427821 577991269 2103270202
		 573341021 1768383826 1699180143 2067407470 1919252002 1852795251 741423650 1970236706 1717527923 1869376623 1852137335 1701601889
		 1715085924 1702063201 1869423148 1600484213 1819045734 1885304687 1953393007 1668246623 577004907 1818322490 573334899 1937076077
		 1868980069 2003790956 1768910943 2019521646 741358114 1970236706 1717527923 1869376623 1869635447 1601465961 809116281 1377971325
		 1701080677 1701402226 2067407479 1919252002 1852795251 741423650 1634624802 1600482402 1684956530 1918857829 1869178725 1715085934
		 1702063201 1701978668 1919247470 1734701663 1601073001 975319160 808333613 1701978668 1919247470 1734701663 1601073001 975319161
		 808333613 1701978668 1919247470 1734701663 1601073001 975319416 808333613 1701978668 1919247470 1734701663 1601073001 975319417
		 808333613 1769349676 1918859109 975332453 1702195828 1769349676 1734309733 1852138866 1920219682 573334901 2003134838 1970037343
		 1949966949 744846706 1701410338 1869438839 975335278 1936482662 1663183973 1919904879 1634493279 1834971245 577070191 741946938
		 1819239202 1667199599 1886216556 577662815 1970435130 1965173861 1885300083 1818589289 1886609759 1601463141 975335023 1936482662
		 1965173861 1885300083 1919905377 1600220513 2003134838 577662815 1818322490 573334899 1702390128 1852399468 1818193766 1701536623
		 1715085924 1702063201 1768956460 1600939384 1868983913 1919902559 1952671090 1667196005 1919904879 1715085939 1702063201 1953702444
		 1868919397 1685024095 809116261 1092758653 1869182051 975336302 1702240891 1869181810 825893486 1634738732 1231385461 1667191376
		 1801676136 975332453 1936482662 1948396645 1383363429 1918858085 1869177953 825571874 1702109740 1699902579 1751342963 1701536613
		 1715085924 1702063201 1701061164 1399289186 1768186216 1918855022 1869177953 909457954 1701061164 1399289186 1768186216 1667196782
		 1801676136 975332453 1936482662 1931619429 1701995892 1685015919 1634885477 577726820 741881658 1702130466 1299146098 1600480367
		 1667590243 577004907 1818322490 2105369971 ;
	setAttr ".vfbSyncM" yes;
	setAttr ".mSceneName" -type "string" "C:/rbw_pipe/f.cecchini/arakno/USER/maya2022/maya/python/depts/rigging/tools/generic/UEFace_RIG/scenes/locs_sqs.ma";
	setAttr ".rt_cpuRayBundleSize" 4;
	setAttr ".rt_gpuRayBundleSize" 128;
	setAttr ".rt_maxPaths" 10000;
	setAttr ".rt_engineType" 3;
	setAttr ".rt_gpuResizeTextures" 0;
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "22AEC365-4E01-CF17-E0AD-ADBAC47BF12D";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "C5B168EF-4161-86F1-66AB-B2A162BD0C7E";
createNode displayLayerManager -n "layerManager";
	rename -uid "6D30DCB7-4904-5DAF-5B3D-618A4AA28FB2";
createNode displayLayer -n "defaultLayer";
	rename -uid "4CEAF828-40E0-4FF7-3EF7-0294BBBB66E0";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "F3FC6148-4F96-B883-0618-6D9E2332631F";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "66D5E668-4C8E-6FDC-9405-4C947D3E370C";
	setAttr ".g" yes;
createNode groupId -n "groupId6501";
	rename -uid "88D31271-41C2-7198-CC14-AA98426CDA5E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId6693";
	rename -uid "1800755F-43CB-0D65-D602-99A71438315B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId6699";
	rename -uid "EF961485-4232-F6BF-8EBF-18814E22B608";
	setAttr ".ihi" 0;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "5BB95CEC-46FA-BEDB-FD25-858BD9974480";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -620.23807059204864 -255.95237078174756 ;
	setAttr ".tgi[0].vh" -type "double2" 28.571427436102034 405.95236482128331 ;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo1";
	rename -uid "D034FD6D-443D-A418-D921-A7908B174421";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -330.95236780151544 -323.80951094248991 ;
	setAttr ".tgi[0].vh" -type "double2" 317.85713022663526 338.09522466054096 ;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo2";
	rename -uid "40F58506-427B-519D-91FF-9AA159E8A3FD";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -330.95236780151544 -323.80951094248991 ;
	setAttr ".tgi[0].vh" -type "double2" 317.85713022663526 338.09522466054096 ;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo3";
	rename -uid "28A0AD5F-413C-71D6-87BA-9DA47207FE6D";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -330.95236780151544 -323.80951094248991 ;
	setAttr ".tgi[0].vh" -type "double2" 317.85713022663526 338.09522466054096 ;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "94CC1DDB-49D6-42F7-51D8-178165F7F011";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 0.6\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 584\n            -height 332\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 0.6\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 583\n            -height 332\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 0.6\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n"
		+ "            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n"
		+ "            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 584\n            -height 332\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n"
		+ "            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 0.6\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n"
		+ "            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 974\n            -height 708\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 0\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 0\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n"
		+ "            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -selectCommand \"print(\\\"\\\")\" \n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n"
		+ "            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n"
		+ "            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n"
		+ "            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n"
		+ "                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n"
		+ "                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showPlayRangeShades \"on\" \n"
		+ "                -lockPlayRangeShades \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -keyMinScale 1\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -valueLinesToggle 1\n                -highlightAffectedCurves 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n"
		+ "                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n"
		+ "                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n"
		+ "                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n"
		+ "                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit 1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n"
		+ "                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n"
		+ "                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit 1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n{ string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"|persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n"
		+ "                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 0.6\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n"
		+ "                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName; };\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 0.6\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 974\\n    -height 708\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 0.6\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 974\\n    -height 708\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "D1A4727B-4EEE-840D-EA3E-23A9C8F7C685";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr ".fs" 1;
	setAttr ".ef" 10;
	setAttr ".dss" -type "string" "lambert1";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :defaultColorMgtGlobals;
	setAttr ".cfe" yes;
	setAttr ".cfp" -type "string" "C:/Program Files/Autodesk/Maya2022/resources/OCIO-configs/Maya-legacy/config.ocio";
	setAttr ".vtn" -type "string" "sRGB gamma (legacy)";
	setAttr ".vn" -type "string" "sRGB gamma";
	setAttr ".dn" -type "string" "legacy";
	setAttr ".wsn" -type "string" "scene-linear Rec 709/sRGB";
	setAttr ".otn" -type "string" "sRGB gamma (legacy)";
	setAttr ".potn" -type "string" "sRGB gamma (legacy)";
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr ":defaultHardwareRenderGlobals.msg" ":perspShape.b" -na;
connectAttr ":ikSystem.msg" ":perspShape.b" -na;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":hyperGraphLayout.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":defaultObjectSet.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":defaultHardwareRenderGlobals.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":ikSystem.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":defaultRenderGlobals.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":defaultRenderGlobals.message" ":initialParticleSE.message";
relationship "link" ":lightLinker1" ":defaultLightSet.message" ":initialParticleSE.message";
relationship "link" ":lightLinker1" ":defaultLightSet.message" ":initialShadingGroup.message";
relationship "link" ":lightLinker1" ":defaultRenderGlobals.message" ":initialShadingGroup.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":initialShadingGroup.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":defaultHardwareRenderGlobals.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":ikSystem.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of locs_sqs.ma
