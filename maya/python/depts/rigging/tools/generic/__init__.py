import importlib
import os

iconpath = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'rigging')


def run_importGuidesUI(*args):
    import depts.rigging.tools.generic.importGuidesUI as importGuidesUI
    importlib.reload(importGuidesUI)

    win = importGuidesUI.ImportGuidesUI()
    win.show()


importGuidesUI = {
    "name": "Import Guides",
    "launch": run_importGuidesUI,
    "icon": os.path.join(iconpath, "importGuides.png"),
    "statustip": "Import Guides UI"
}


def run_faceRigUI(*args):
    import depts.rigging.tools.generic.Face_RIG.launcher as faceRigUI
    importlib.reload(faceRigUI)

    faceRigUI.main()


faceRigUI = {
    "name": "Face Rig",
    "launch": run_faceRigUI,
    "icon": os.path.join(iconpath, "Face_Icon.png"),
    "statustip": "Face Rig UI"
}



def run_UEfaceRigUI(*args):
    import depts.rigging.tools.generic.UEFace_RIG.launcher_UE as UEfaceRigUI
    importlib.reload(UEfaceRigUI)

    UEfaceRigUI.main()


UEfaceRigUI = {
    "name": "UE Face Rig",
    "launch": run_UEfaceRigUI,
    "icon": os.path.join(iconpath, "Face_IconUE.png"),
    "statustip": "UE Face Rig UI"
}


def run_clothesRigUI(*args):
    import depts.rigging.tools.generic.Clothes_Rig.clothesUI as clothesRigUI
    importlib.reload(clothesRigUI)
    
    clothesRigUI.main()


clothesRigUI = {
    "name": "Clothes Rig",
    "launch": run_clothesRigUI,
    "icon": os.path.join(iconpath, "cloth.png"),
    "statustip": "Clothes Rig UI"
}


def run_bodyRigUI(*args):
    import depts.rigging.tools.generic.Body_Rig.bodyUI as bodyRigUI
    importlib.reload(bodyRigUI)
    
    bodyRigUI.main()


bodyRigUI = {
    "name": "Body Rig",
    "launch": run_bodyRigUI,
    "icon": os.path.join(iconpath, "body.png"),
    "statustip": "Body Rig UI"
}

def importPropDefaultCmd(*args):
    import maya.cmds as cmds
    localpipe = os.getenv("LOCALPY")
    fileToImport = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "prop", "RBW_Prop_Default.ma")
    cmds.file(fileToImport, i=True, type="mayaAscii", namespace=":")
    
importNewPropDefaultTool = {
    "name": "Prop Default",
    "launch": importPropDefaultCmd,
    "statustip": "Prop Default",
    "icon": os.path.join(iconpath, "prop_def.png")
}

def importWheelsBaseRig(*args):
    import maya.cmds as cmd
    localpipe = os.getenv("LOCALPY")
    fileToImport = os.path.join(localpipe, "depts", "rigging", "tools", "generic", "vehicle", "default_vehicles.ma")
    cmd.file(fileToImport, i=True, type="mayaAscii", namespace=":")

importNewWheelsBaseRigTool = {
    "name": "Wheels Base Rig",
    "launch": importWheelsBaseRig,
    "statustip": "Vehicles Base Rig",
    "icon": os.path.join(iconpath, "wheels_base_rig.png")
}

def run_finalizeRigUI(*args):
    import depts.rigging.tools.generic.Finalize_Rig.finalizeUI as finalizeRigUI
    importlib.reload(finalizeRigUI)
    
    finalizeRigUI.main()
    

finalizeRigUI = {
    "name": "Finalize Rig",
    "launch": run_finalizeRigUI,
    "statustip": "Finalize Rig",
    "icon": os.path.join(iconpath, "finalize.png")
    
    
}


def run_CorrectiveUI(*args):
    import depts.rigging.tools.generic.CorrectiveJoints.CorrectiveUI as CorrectiveRigUI
    importlib.reload(CorrectiveRigUI)

    CorrectiveRigUI.main()

CorrectiveRigUI = {
    "name": "Corrective Joints Creator",
    "launch": run_CorrectiveUI,
    "statustip": "Corrective Joints Creator",
    "icon": os.path.join(iconpath, "Corrective_Joints.png")
}


def run_PblUI(*args):
    import depts.rigging.tools.generic.Pbl.pbl_launcher as PblUI
    importlib.reload(PblUI)

    PblUI.main()


PblUI = {
    "name": "Pebbles RIG",
    "launch": run_PblUI,
    "statustip": "Pebbles RIG",
    "icon": os.path.join(iconpath, "pbl_icon.png")

}

tools = [
    importGuidesUI,
    faceRigUI,
    UEfaceRigUI,
    clothesRigUI,
    bodyRigUI,
    importNewPropDefaultTool,
    importNewWheelsBaseRigTool,
    finalizeRigUI,
    CorrectiveRigUI,
    PblUI

]

PackageTool = {
    "name": "generic",
    "tools": tools,
    "icon_size": "32",
    "icon_only": False
}
