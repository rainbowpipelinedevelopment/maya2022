from imp import reload
import os
import warnings
import importlib

import maya.cmds as cmds
import maya.mel as mel
from maya import OpenMaya as om
from maya import OpenMayaUI as omui

import depts.rigging.tools.cnt_jnt.ImportExportShapes as impexp
importlib.reload(impexp)

localpipe = os.getenv("LOCALPY")


def _importNewShapesFixedCmd(*args):
    import maya.cmds as cmds
    localpipeBackDir = os.path.dirname(localpipe)
    fileToImport = os.path.join(localpipeBackDir, 'scenes', 'rigging', 'rbw_autorig', 'RBW_All_Controls_Shapes.ma')
    cmds.file(fileToImport, i=True, type="mayaAscii", namespace=":")        

def _newControlShapesFixedCmd(*args):
    import depts.rigging.tools.cnt_jnt.attachControlShapes as ncsf
    reload( ncsf )
    ncsf.newControlShapesFixed()

def run_import_quad_shapes(*args):
    import maya.cmds as cmds
    localpipeBackDir = os.path.dirname(localpipe)
    fileToImport = os.path.join(localpipeBackDir, 'scenes', 'rigging', 'rbw_autorig', 'RBW_quadruped_shapes.ma')
    cmds.file(fileToImport, i=True, type="mayaAscii", namespace=":")
    
def run_attach_control_shapes_quadruped(*args):
    import depts.rigging.tools.cnt_jnt.attach_control_shapes_quadruped as acsq
    reload(acsq)
    acsq.newControlShapesQuad()

def radioControl(a):
    radioCol = cmds.radioCollection(a, query=True, sl=True)
    value= cmds.radioButton(radioCol, query=True, label=True) 
    return value

def importShp(*args):
    if radioControl(charCheck)=="Biped":
        _importNewShapesFixedCmd()
    elif radioControl(charCheck)=="Quadruped":
        run_import_quad_shapes()

def attachShp(*args):
    if radioControl(charCheck)=="Biped":
        _newControlShapesFixedCmd()
    elif radioControl(charCheck)=="Quadruped":
        run_attach_control_shapes_quadruped()

def saveSel():
    S = cmds.ls(sl=True)
    return S

def freeAttr(a):
    axis = ['x', 'y', 'z']
    attrs = ['t', 'r', 's']
    cmds.setAttr(a+'.v', lock=0, k=True)       
    for ax in axis:
        for attr in attrs:
            cmds.setAttr(a+'.'+attr+ax, lock=0, k=True)
    return

def mirrorNurbs(*args):
    sel=saveSel()
    cnt=cmds.duplicate(sel[0])
    if (len(cnt)>1):
        cnt=cnt[0]
    cnt=cmds.rename(cnt, "NEW")
    freeAttr(cnt)
    child=cmds.listRelatives(cnt,c=True,pa=True)
    childShp=cmds.listRelatives(cnt,shapes=True,pa=True)
    for i in child:
        if i not in childShp:
            cmds.delete(i)
    grp=cmds.group(cnt)
    cmds.parent(grp, w=True)
    grp2=cmds.group(grp)
    cmds.xform(grp2, sp=(0,0,0), s=(-1,1,1), ws=1)
    cmds.parent(grp,w=True)
    cmds.delete(grp2)
    newpos=cmds.listRelatives(sel[1], parent=True)
    cmds.parent(grp, newpos[0])
    
    cmds.makeIdentity(grp, a=True, r=True, t=True, s=True)
    
    newshape = cmds.listRelatives(cnt, shapes=True,pa=True)
    oldshape = cmds.listRelatives(sel[1], shapes=True,pa=True)
    for a in oldshape:
        cmds.delete(a)
    for a in newshape:
        newname=str(oldshape[0])
        a=cmds.rename(a, newname)
        cmds.select(a, sel[1])
        mel.eval("parent -r -s;")
    cmds.delete(grp)
    return

def replaceSh(*args):  
    sel=saveSel()
    for i in range(len(sel)-1):
        cnt=cmds.duplicate(sel[0])
        oldcnt=sel[i+1]
        if len(cnt)>1:
            cnt=cnt[0]
            child=cmds.listRelatives(cnt,c=True,pa=True)
            childShp=cmds.listRelatives(cnt,shapes=True,pa=True)
            for i in child:
                if i not in childShp:
                    cmds.delete(i)
        cnt=cmds.rename(cnt, "NEW")
        newshape=cmds.listRelatives(cnt, shapes=True, pa=True)
        oldshape=cmds.listRelatives(oldcnt, shapes=True,pa=True)
        for a in oldshape:
            cmds.delete(a)
        for a in newshape:
            newname=str(oldshape[0])
            #print a
            a=cmds.rename(a, newname)
            #print a
            cmds.select(a, oldcnt)
            mel.eval("parent -r -s;")
        cmds.select(cl=True)
        cmds.delete(cnt)
    return

def createWindow():
    global charCheck
    if (cmds.window("shapes_ui", q=True,exists=True)):
        cmds.deleteUI("shapes_ui", window=True)
    W=cmds.window("shapes_ui",title="Shapes UI", ip=True, rtf=True)    
    
    C0=cmds.columnLayout( adjustableColumn=True, rowSpacing=10, p=W,cat=("both",10),w=200,h=150)
    cmds.text("Select char type:",al="center")
    R1=cmds.rowLayout(numberOfColumns=2,p=C0, columnAttach=[(1,'left',100),(2,'right',100)])
    C1=cmds.columnLayout( adjustableColumn=True, rowSpacing=10, p=R1) 
    charCheck= cmds.radioCollection(p=C1) 
    biped= cmds.radioButton( label='Biped',p=C1)    
    C2=cmds.columnLayout( adjustableColumn=True, rowSpacing=10, p=R1)
    quad= cmds.radioButton( label='Quadruped',p=C2) 
    charCheck = cmds.radioCollection(charCheck, edit=True, select=biped)
    
    R2=cmds.rowLayout(numberOfColumns=1,p=W)
    C3=cmds.columnLayout( adjustableColumn=True, rowSpacing=10, p=R2)
    cmds.button(l="Import Shapes", c=importShp,w=150,h=30, bgc=[0.3, 0.3, 0.3],p=C3)
    cmds.button(l="Attach Shapes", c=attachShp, w=150,h=30, bgc=[0.3, 0.3, 0.3],p=C3)
    
    R3=cmds.rowLayout(numberOfColumns=2,p=C3)
    cmds.button(l="Replace Shape", c=replaceSh,w=150,h=30, bgc=[0.3, 0.3, 0.3],p=R3)
    cmds.button(l="Mirror Shape", c=mirrorNurbs, w=150,h=30, bgc=[0.3, 0.3, 0.3],p=R3)
    
    R4=cmds.rowLayout(numberOfColumns=2,p=C3)
    cmds.button(l="Save Shapes", c=impexp.saveNurbsShapeCommand,w=150,h=30, bgc=[0.3, 0.3, 0.3],p=R4)
    cmds.button(l="Load Shapes", c=impexp.loadNurbsShapeCommand, w=150,h=30, bgc=[0.3, 0.3, 0.3],p=R4)
    
    cmds.showWindow( W )
    return
