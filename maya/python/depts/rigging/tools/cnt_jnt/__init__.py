import os
import importlib

iconpath = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'rigging')


def run_shp(*args):
    import depts.rigging.tools.cnt_jnt.shapes_ui as shp
    importlib.reload(shp)
    shp.createWindow()

shpTool = {
    "name": "SHAPES Module",
    "launch": run_shp,
    "icon": os.path.join(iconpath, "imp_exp_shapes.png"),
    "statustip": "SHAPES Module"
}


def run_cfkc(*args):
    import maya.mel as mel
    mel.eval("source createFKControl.mel; createFKControls;")

cfkcTool= {
    "name": "Create FK Chain",
    "launch": run_cfkc,
    "icon": os.path.join(iconpath, "createFK.png"),
    "statustip": "Create FK Chain"
}


def run_fkcn(*args):
    import depts.rigging.tools.cnt_jnt.fk_controls_number as fkcn
    importlib.reload(fkcn)
    fkcn.create_FK_UI()

fkcnTool = {
    "name": 'FK Multi Controls',
    "launch": run_fkcn,
    "icon": os.path.join(iconpath, "fkCntrlNumber.png"),
    "statustip": "FK Multi Controls"
}


def run_bnr(*args):
    import depts.rigging.tools.cnt_jnt.bendy_UI as bnr
    importlib.reload(bnr)
    bnr.bendy_UI()

bnrTool = {
    "name": 'Bendy Rig',
    "launch": run_bnr,
    "icon": os.path.join(iconpath, "bendyRig.png"),
    "statustip": "Bendy Rig",
}


def run_aor(*args):
    import depts.rigging.tools.cnt_jnt.ao_ribbon as aor
    importlib.reload(aor)
    aor.UI()

aorTool = {
    "name": "Ao Ribbon",
    "launch": run_aor,
    "icon": os.path.join(iconpath, "aoRibbon.png"),
    "statustip": "Ao Ribbon",
}


def run_ikRibbonUI(*args):
    import depts.rigging.tools.cnt_jnt.ik_ribbon as ikRibbonUI
    importlib.reload(ikRibbonUI)
    
    ikRibbonUI.createWindow()
    

ikRibbonUI = {
    "name": "IK Ribbon",
    "launch": run_ikRibbonUI,
    "statustip": "IK Ribbon",
    "icon": os.path.join(iconpath, "IK_Ribbon.png")
    
    
}


tools = [
    shpTool,
    cfkcTool,
    fkcnTool,
    bnrTool,
    aorTool,
    ikRibbonUI
]


PackageTool = {
    "name": "cnt_jnt",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
