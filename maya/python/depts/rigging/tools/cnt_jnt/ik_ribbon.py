import maya.cmds as cmds

def saveSel():
    S = cmds.ls(sl=True)
    return S

def getIntValue(a):
    n = cmds.intField(a, q=True, v=True)
    return n

def getTextValue(a):
    n = cmds.textField(a, q=True, tx=True)
    return n

def getParent(obj):
    P = cmds.listRelatives(obj, parent=True)
    return P

def getPosition(a):
    p = cmds.xform(a, q=1, t=1, ws=1)
    return p

def orNormal(objs, msh):
    for obj in objs:
        NC = cmds.normalConstraint(msh, obj, aim=(1,0,0), u=(0,1,0))
        cmds.delete(NC)
    return

def constraintList(obj1, obj2):
    for i in range(len(obj1)):
        cmds.parentConstraint(obj1[i], obj2[i])
        cmds.scaleConstraint(obj1[i], obj2[i])
    return

def curveonLocs():
    sel = saveSel()
    k = list(range(len(sel)))
    p = [cmds.xform(sel[i], q=1, t=1, ws=1) for i in range(len(sel))]
    c = cmds.curve(degree=1, knot=k, ep=p)
    return c

def createSurf(jnt1, jnt2, div):
    j1 = cmds.duplicate(jnt1)[0]
    j2 = cmds.duplicate(jnt1)[0]
    z1 = cmds.getAttr(j1+".tz")
    cmds.move(0.5, j1, z=True, r=True, objectSpace=True, worldSpaceDistance=True)
    cmds.move(-0.5, j2, z=True, r=True, objectSpace=True, worldSpaceDistance=True)
    cmds.select(cl=True)
    cmds.select(j1, j2)
    c1 = curveonLocs()
    cmds.select(cl=True)
    cmds.select(cmds.pickWalk(j1, d="down"), cmds.pickWalk(j2, d="down"))
    c2 = curveonLocs()
    surf = cmds.loft(c1, c2, u=True, d=3, ss=div)[0]
    surf = cmds.rename(surf, "NBS_" + NAME + "_A")
    cmds.delete(j1, j2, c1, c2)
    cmds.delete(surf, constructionHistory=True)
    return surf

def createControl(jnt, name, colour, r, PC, SC):
    pos = getPosition(jnt)
    points = [(-r, r, -r), (-r, r, r), (r, r, r), (r, r, -r), (-r, r, -r),
              (-r, -r, -r), (-r, -r, r), (r, -r, r), (r, r, r), (-r, r, r),
              (-r, -r, r), (-r, -r, -r), (r, -r, -r), (r, r, -r), (r, r, r),
              (r, -r, r), (r, -r, -r)]
    c = cmds.curve(degree=1, knot=list(range(17)), point=points)
    cmds.delete(c, constructionHistory=True)
    cmds.setAttr(c+".v", lock=True, keyable=False, channelBox=False)
    cnt = cmds.rename(c, name)
    G_offset = cmds.group(cnt)
    G_offset_name = name.replace("_Ctrl", "_Grp")
    G_offset = cmds.rename(G_offset, G_offset_name)
    cmds.xform(G_offset, t=(pos[0], pos[1], pos[2]))
    ori = cmds.orientConstraint(jnt, G_offset, mo=False)
    cmds.delete(ori)
    shape = cmds.pickWalk(cnt, d="down")[0]
    cmds.setAttr(shape+".overrideEnabled", 1)
    cmds.setAttr(shape+".overrideColor", colour)
    cmds.select(cl=True)
    if PC == 1:
        cmds.parentConstraint(cnt, jnt)
    if SC == 1:
        cmds.scaleConstraint(cnt, jnt)
    return cnt

def createIK(jnt1, jnt2, mstr):
    jnt1 = cmds.rename(jnt1, NAME+"_01_ik_jnt")
    jnt2 = cmds.rename(jnt2, NAME+"_02_ik_jnt")
    cmds.joint(jnt1, e=True, oj='xyz', secondaryAxisOrient='yup', ch=True)
    cmds.joint(jnt2, e=True, oj='none')
    ik = cmds.ikHandle(n="IKH_"+NAME, sj=jnt1, ee=jnt2, sol="ikSCsolver")
    ikCnt = createControl(jnt2, NAME+"_ik_Ctrl", 18, 1, 0, 0)
    cmds.parent(getParent(ikCnt)[0], mstr)
    cmds.setAttr(ikCnt+".ry", lock=True, keyable=False, channelBox=False)
    cmds.setAttr(ikCnt+".rz", lock=True, keyable=False, channelBox=False)
    cmds.setAttr(ikCnt+".sx", lock=True, keyable=False, channelBox=False)
    cmds.setAttr(ikCnt+".sy", lock=True, keyable=False, channelBox=False)
    cmds.setAttr(ikCnt+".sz", lock=True, keyable=False, channelBox=False)
    cmds.parent(ik[0], ikCnt)
    cmds.hide(ik[0])
    cmds.hide(jnt1)
    cmds.select(jnt1, "NBS_"+NAME+"_A")
    cmds.skinCluster(tsb=True, n="SK_"+NAME+"_A")
    return ik, ikCnt, jnt1, jnt2

def stretch(chain, cnt, master):
    pos1 = getPosition(chain[0])
    pos2 = getPosition(chain[1])

    DIS = cmds.rename(cmds.pickWalk(cmds.distanceDimension(sp=(100,100,100), ep=(1,1,1)), d='up'), "DIS_stretch_" + NAME)
    DIS2 = cmds.rename(cmds.pickWalk(cmds.distanceDimension(sp=(100,100,100), ep=(2,2,2)), d='up'), "DIS_stretch_" + NAME + "_scalefix")
    cmds.parent(DIS, DIS2, NAME+"_utilities_Grp")
    
    locs = cmds.listConnections(cmds.pickWalk(DIS, d="down")) + cmds.listConnections(cmds.pickWalk(DIS2, d="down"))
    cmds.xform(locs[0], t=(pos1[0], pos1[1], pos1[2]))
    cmds.xform(locs[1], t=(pos2[0], pos2[1], pos2[2]))
    cmds.xform(locs[3], t=(pos2[0], pos2[1], pos2[2]))
    cmds.parent(locs[0], master)
    cmds.parent(locs[1], cnt)
    cmds.parent(locs[3], master)
    LOC1 = cmds.rename(locs[0], "LOC_" + NAME + "stretch_01")
    LOC2 = cmds.rename(locs[1], "LOC_" + NAME + "stretch_02")
    LOC3 = cmds.rename(locs[3], "LOC_" + NAME + "_scalefix")
    cmds.hide(LOC1, LOC2, LOC3)
    
    MLP = cmds.shadingNode('multiplyDivide', asUtility=1, n="MLP_stretch_" + NAME)
    cmds.setAttr(MLP + ".operation", 2)
    cmds.connectAttr(DIS + ".distance", MLP + ".input1X")
    cmds.connectAttr(DIS2 + ".distance", MLP + ".input2X")
    
    CND = cmds.shadingNode('condition', asUtility=1, n="CND_stretch_" + NAME)
    cmds.setAttr(CND + ".operation", 1)
    cmds.setAttr(CND + ".secondTerm", 1)
    cmds.connectAttr(MLP + ".outputX", CND + ".firstTerm")
    cmds.connectAttr(MLP + ".outputX", CND + ".colorIfTrueR")
    
    BLEND1 = cmds.shadingNode('blendColors', asUtility=1, n="BLD_stretch_01_" + NAME)
    cmds.addAttr(cnt, longName='stretch', attributeType='float', min=0, max=1, dv=0)
    cmds.setAttr(cnt + ".stretch", e=1, keyable=True)
    cmds.setAttr(cnt + ".stretch", 1)
    cmds.connectAttr(cnt + ".stretch", BLEND1 + ".blender")
    cmds.connectAttr(CND + ".outColorR", BLEND1 + ".color1R")
    cmds.setAttr(BLEND1 + ".color2R", 1)
    cmds.connectAttr(BLEND1 + ".outputR", chain[0] + ".sx")
    return
    
def parentToSurface(objs, msh, cnt):
    import sys
    from imp import reload
    
    # Generic import for the pipeline structure
    import depts.rigging.tools.utilities.parent_to_surface as par
    reload(par)
    
    follicles = []
    num = 1
    for i in objs:
        cmds.select(i, msh)
        par.parentToSurface()
        f = getParent(i)[0]
        f = cmds.rename(f, str(msh).replace("NBS", "FLC") + "_" + str(num).zfill(2))
        follicles.append(f)
        cmds.delete(i)
        cmds.scaleConstraint(cnt, f, mo=True)
        num += 1  # Increment the number for unique follicle names

    g = cmds.group(follicles, n=str(msh).replace("NBS_", "") + "_follicles_Grp")
    cmds.parent(g, NAME + "_utilities_Grp")
    cmds.parent(msh, NAME + "_utilities_Grp")
    return follicles


def intermidiateLocs(a, b, numlocs):
    locs = []
    pos1 = cmds.xform(a, q=1, t=1, ws=1)
    pos2 = cmds.xform(b, q=1, t=1, ws=1)
    dX = pos2[0] - pos1[0]
    dY = pos2[1] - pos1[1]
    dZ = pos2[2] - pos1[2]
    incY = dY / (numlocs - 1)
    incX = dX / (numlocs - 1)
    incZ = dZ / (numlocs - 1)
    posX = pos1[0]
    posY = pos1[1]
    posZ = pos1[2]
    for _ in range(numlocs):
        l = cmds.createNode('locator')
        l = cmds.pickWalk(d="up")
        cmds.setAttr(l[0] + ".ty", posY)
        cmds.setAttr(l[0] + ".tx", posX)
        cmds.setAttr(l[0] + ".tz", posZ)
        posX += incX
        posY += incY
        posZ += incZ
        locs.append(l[0])
    return locs

def createJnts(pos, surf):
    joints = []
    for i in pos:
        name = str(i).replace("FLC_", "").replace("_A_", "_ctrl_").replace("_B_", "_sk_") + "_jnt"
        jnt = cmds.createNode('joint', n=name)
        pC = cmds.pointConstraint(i, jnt, mo=False)
        oC = cmds.orientConstraint(i, jnt, mo=False)
        cmds.delete(pC, oC)
        cmds.makeIdentity(jnt, a=True, r=True)
        joints.append(jnt)
    if surf == "NBS_" + NAME + "_A":
        g = cmds.group(joints, n=NAME + "_control_joints_Grp")
    else:
        g = cmds.group(joints, n=NAME + "_sk_joints_Grp")
    cmds.parent(g, NAME + "_joints_Grp")
    cmds.hide(g)
    return joints

def createControlsOnJoints(jnts):
    controls = []
    for i in jnts:
        n = str(i).replace("_ctrl", "").replace("jnt", "Ctrl")
        c = createControl(i, n, 20, .3, 1, 1)
        controls.append(c)
    groups = []
    for i in controls:
        groups.append(getParent(i)[0])
    g = cmds.group(groups, n=NAME + "_controls_sec_Grp")
    cmds.parent(g, NAME + "_controls_Grp")
    return controls, groups

def organizeOutliner(j):
    gJnt = cmds.group(j[0], n=NAME + "_joints_Grp")
    gCnt = cmds.group(n=NAME + "_controls_Grp")
    gUtil = cmds.group(n=NAME + "_utilities_Grp")
    gRig = cmds.group(gJnt, gCnt, gUtil, n="RIG_" + NAME + "_Grp")
    cmds.hide(gJnt, gUtil)
    return

def createMaster(jnt):
    n = NAME + "_master_Ctrl"
    cnt = createControl(jnt, n, 17, 2, 1, 0)
    cmds.scaleConstraint(cnt, getParent(jnt)[0])
    cmds.parent(getParent(cnt)[0], NAME + "_controls_Grp")
    cmds.addAttr(cnt, longName='master_scale', attributeType='float', min=0, dv=1)
    cmds.setAttr(cnt + ".master_scale", e=1, keyable=True)
    cmds.connectAttr(cnt + ".master_scale", cnt + ".sx")
    cmds.connectAttr(cnt + ".master_scale", cnt + ".sy")
    cmds.connectAttr(cnt + ".master_scale", cnt + ".sz")
    cmds.setAttr(cnt + ".sx", lock=True, keyable=False, channelBox=False)
    cmds.setAttr(cnt + ".sy", lock=True, keyable=False, channelBox=False)
    cmds.setAttr(cnt + ".sz", lock=True, keyable=False, channelBox=False)
    return cnt

def secSurf(s1, jnts):
    s2 = cmds.duplicate(s1, n="NBS_" + NAME + "_B")
    cmds.select(jnts, s2)
    cmds.skinCluster(tsb=True, n="SK_" + NAME + "_B")
    cmds.parent(s2, w=True)
    return s2

def operate(NAME, NUM, jNUM):
    joints = saveSel()
    organizeOutliner(joints)
    master = createMaster(joints[0])
    srf = createSurf(joints[0], joints[1], NUM-1)
    IK = createIK(joints[0], joints[1], master)
    
    ikh = IK[0][0]
    ikCtrl = IK[1]
    joints = [IK[2], IK[3]]
    
    stretch(joints, ikCtrl, master)
    locList = intermidiateLocs(joints[0], joints[1], NUM)
    orNormal(locList, srf)
    fol = parentToSurface(locList, srf, master)
    jntList = createJnts(fol, srf)
    cntList = createControlsOnJoints(jntList)
    constraintList(fol, cntList[1])
    
    srf2 = secSurf(srf, jntList)[0]
    locList2 = intermidiateLocs(joints[0], joints[1], jNUM)
    orNormal(locList2, srf2)
    fol2 = parentToSurface(locList2, srf2, master)
    jntList2 = createJnts(fol2, srf2)
    constraintList(fol2, jntList2)
    cmds.sets(jntList2, n='INF_' + NAME)
    return

def commandsbutton(*args):
    global NAME
    NAME = getTextValue(name)
    NUM = getIntValue(num)
    jNUM = getIntValue(jnum)
    operate(NAME, NUM, jNUM)
    return

def createWindow():
    global name, num, jnum
    if cmds.window("ikribbon", q=True, exists=True):
        cmds.deleteUI("ikribbon", window=True)
    W = cmds.window("ikribbon", title="ikRibbon Tool", s=True, ip=True, rtf=True)
    
    C0 = cmds.columnLayout(p=W, adj=True, rs=10, h=30)
    cmds.text("Select 2 joints and set parameters:", p=C0)
    RC1 = cmds.rowColumnLayout(numberOfColumns=2, p=W, columnAttach=[(1, 'right', 0), (2, 'both', 5), (3, 'both', 5)], columnWidth=[(1, 60), (2, 270)])
    cmds.text(label='Name: ', ann="Input ik ribbon name")
    name = cmds.textField(w=250, tx="ikRibbon")
    cmds.text(label='Controls: ', ann="Input number of ribbon controls")
    num = cmds.intField(w=30, v=5, min=1)
    cmds.text(label='Joints: ', ann="Input number of ribbon joints")
    jnum = cmds.intField(w=30, v=10, min=1)
    
    RC2 = cmds.rowLayout(numberOfColumns=1, p=W, w=330, columnAttach=[(1, 'both', 20)])
    cmds.button(l="Create IK Ribbon", c=commandsbutton, w=300, h=30, bgc=[0.3, 0.3, 0.3], p=RC2)
    cmds.showWindow(W)
    return


