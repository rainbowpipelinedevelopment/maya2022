#//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#NEW SHAPE FOR EACH CONTROLS
#Importare File contenente tutte le nuove surface shapes
#//////////////////////////////////////////////////////////////////////////////////////////////////////////////
from imp import reload
import maya.cmds as cmds


def newControlShapesQuad():
    cmds.undoInfo(openChunk=True)
    try:
        cmds.select('newShapes_Grp')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "newShapes_Grp <--> You should load the file with all the new shapes. ---" + "\n"); 
        cmds.select( cl=True )

    # //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    # cog, hip, spine, clavicles, neck, head, eyes
    try:
        cmds.parent('cog_CtrlShape', 'RIG_cog_ctrl', r=True, s=True)
        cmds.delete('cog_Ctrl')
        cmds.rename('RIG_cog_ctrlShape', 'RIG_cog_ctrlShapeOLD')
        cmds.rename('cog_CtrlShape', 'RIG_cog_ctrlShape')
        cmds.delete('RIG_cog_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_cog_ctrl" + "\n"); 
        cmds.select( cl=True )
        
    try:
        cmds.parent('hip_CtrlShape', 'RIG_hip_ctrl', r=True, s=True)
        cmds.delete('hip_Ctrl')
        cmds.rename('RIG_hip_ctrlShape', 'RIG_hip_ctrlShapeOLD')
        cmds.rename('hip_CtrlShape', 'RIG_hip_ctrlShape')
        cmds.delete('RIG_hip_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_hip_ctrl" + "\n"); 
        cmds.select( cl=True )
        
    try:
        cmds.parent('spineLow_CtrlShape', 'RIG_spineLow_ctrl', r=True, s=True)
        cmds.delete('spineLow_Ctrl')
        cmds.rename('RIG_spineLow_ctrlShape', 'RIG_spineLow_ctrlShapeOLD')
        cmds.rename('spineLow_CtrlShape', 'RIG_spineLow_ctrlShape')
        cmds.delete('RIG_spineLow_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_spineLow_ctrl" + "\n"); 
        cmds.select( cl=True )

    try:
        cmds.parent('spineMid_CtrlShape', 'RIG_spineMid_ctrl', r=True, s=True)
        cmds.delete('spineMid_Ctrl')
        cmds.rename('RIG_spineMid_ctrlShape', 'RIG_spineMid_ctrlShapeOLD')
        cmds.rename('spineMid_CtrlShape', 'RIG_spineMid_ctrlShape')
        cmds.delete('RIG_spineMid_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_spineMid_1_ctrl" + "\n"); 
        cmds.select( cl=True )

    try:
        cmds.parent('L_clavicleTrans_CtrlShape', 'RIG_lf_clavicleTrans_ctrl', r=True, s=True)
        cmds.delete('L_clavicleTrans_Ctrl')
        cmds.rename('RIG_lf_clavicleTrans_ctrlShape', 'RIG_lf_clavicleTrans_ctrlShapeOLD')
        cmds.rename('L_clavicleTrans_CtrlShape', 'RIG_lf_clavicleTrans_ctrlShape')
        cmds.delete('RIG_lf_clavicleTrans_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_clavicleTrans_ctrl" + "\n"); 
        cmds.select( cl=True )
        
    try:
        cmds.parent('R_clavicleTrans_CtrlShape', 'RIG_rt_clavicleTrans_ctrl', r=True, s=True)
        cmds.delete('R_clavicleTrans_Ctrl')
        cmds.rename('RIG_rt_clavicleTrans_ctrlShape', 'RIG_rt_clavicleTrans_ctrlShapeOLD')
        cmds.rename('R_clavicleTrans_CtrlShape', 'RIG_rt_clavicleTrans_ctrlShape')
        cmds.delete('RIG_rt_clavicleTrans_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_clavicleTrans_ctrl" + "\n"); 
        cmds.select( cl=True )

    try:
        cmds.parent('spineHigh_CtrlShape', 'RIG_spineHigh_ctrl', r=True, s=True)
        cmds.delete('spineHigh_Ctrl')
        cmds.rename('RIG_spineHigh_ctrlShape', 'RIG_spineHigh_ctrlShapeOLD')
        cmds.rename('spineHigh_CtrlShape', 'RIG_spineHigh_ctrlShape')
        cmds.delete('RIG_spineHigh_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_spineHigh_ctrl" + "\n"); 
        cmds.select( cl=True )
        
    try:
        cmds.parent('neck_CtrlShape', 'RIG_neck_ctrl', r=True, s=True)
        cmds.delete('neck_Ctrl')
        cmds.rename('RIG_neck_ctrlShape', 'RIG_neck_ctrlShapeOLD')
        cmds.rename('neck_CtrlShape', 'RIG_neck_ctrlShape')
        cmds.delete('RIG_neck_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_neck_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('head_CtrlShape', 'RIG_head_ctrl', r=True, s=True)
        cmds.delete('head_Ctrl')
        cmds.rename('RIG_head_ctrlShape', 'RIG_head_ctrlShapeOLD')
        cmds.rename('head_CtrlShape', 'RIG_head_ctrlShape')
        cmds.delete('RIG_head_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_head_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('eyeMaster_CtrlShape', 'RIG_eyeMaster_ctrl', r=True, s=True)
        cmds.delete('eyeMaster_Ctrl')
        cmds.rename('RIG_eyeMaster_ctrlShape', 'RIG_eyeMaster_ctrlShapeOLD')
        cmds.rename('eyeMaster_CtrlShape', 'RIG_eyeMaster_ctrlShape')
        cmds.delete('RIG_eyeMaster_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_eyeMaster_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('L_eye_CtrlShape', 'RIG_lf_eye_ctrl', r=True, s=True)
        cmds.delete('L_eye_Ctrl')
        cmds.rename('RIG_lf_eye_ctrlShape', 'RIG_lf_eye_ctrlShapeOLD')
        cmds.rename('L_eye_CtrlShape', 'RIG_lf_eye_ctrlShape')
        cmds.delete('RIG_lf_eye_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_eye_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('R_eye_CtrlShape', 'RIG_rt_eye_ctrl', r=True, s=True)
        cmds.delete('R_eye_Ctrl')
        cmds.rename('RIG_rt_eye_ctrlShape', 'RIG_rt_eye_ctrlShapeOLD')
        cmds.rename('R_eye_CtrlShape', 'RIG_rt_eye_ctrlShape')
        cmds.delete('RIG_rt_eye_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_eye_ctrl" + "\n");
        cmds.select( cl=True )
        
    # //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    # leg ik
    try:
        cmds.parent('L_Rear_legOffset_CtrlShape', 'Rear_lt_legOffset_ctrl', r=True, s=True)
        cmds.delete('L_Rear_legOffset_Ctrl')
        cmds.rename('Rear_lt_legOffset_ctrlShape', 'Rear_lt_legOffset_ctrlShapeOLD')
        cmds.rename('L_Rear_legOffset_CtrlShape', 'Rear_lt_legOffset_ctrlShape')
        cmds.delete('Rear_lt_legOffset_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Rear_lt_legOffset_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('R_Rear_legOffset_CtrlShape', 'Rear_rt_legOffset_ctrl', r=True, s=True)
        cmds.delete('R_Rear_legOffset_Ctrl')
        cmds.rename('Rear_rt_legOffset_ctrlShape', 'Rear_rt_legOffset_ctrlShapeOLD')
        cmds.rename('R_Rear_legOffset_CtrlShape', 'Rear_rt_legOffset_ctrlShape')
        cmds.delete('Rear_rt_legOffset_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Rear_rt_legOffset_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('L_Front_legOffset_CtrlShape', 'Front_lt_legOffset_ctrl', r=True, s=True)
        cmds.delete('L_Front_legOffset_Ctrl')
        cmds.rename('Front_lt_legOffset_ctrlShape', 'Front_lt_legOffset_ctrlShapeOLD')
        cmds.rename('L_Front_legOffset_CtrlShape', 'Front_lt_legOffset_ctrlShape')
        cmds.delete('Front_lt_legOffset_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Front_lt_legOffset_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('R_Front_legOffset_CtrlShape', 'Front_rt_legOffset_ctrl', r=True, s=True)
        cmds.delete('R_Front_legOffset_Ctrl')
        cmds.rename('Front_rt_legOffset_ctrlShape', 'Front_rt_legOffset_ctrlShapeOLD')
        cmds.rename('R_Front_legOffset_CtrlShape', 'Front_rt_legOffset_ctrlShape')
        cmds.delete('Front_rt_legOffset_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Front_rt_legOffset_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('L_Rear_foot_CtrlShape', 'Rear_lt_foot_ctrl', r=True, s=True)
        cmds.delete('L_Rear_foot_Ctrl')
        cmds.rename('Rear_lt_foot_ctrlShape', 'Rear_lt_foot_ctrlShapeOLD')
        cmds.rename('L_Rear_foot_CtrlShape', 'Rear_lt_foot_ctrlShape')
        cmds.delete('Rear_lt_foot_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Front_rt_legOffset_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('R_Rear_foot_CtrlShape', 'Rear_rt_foot_ctrl', r=True, s=True)
        cmds.delete('R_Rear_foot_Ctrl')
        cmds.rename('Rear_rt_foot_ctrlShape', 'Rear_rt_foot_ctrlShapeOLD')
        cmds.rename('R_Rear_foot_CtrlShape', 'Rear_rt_foot_ctrlShape')
        cmds.delete('Rear_rt_foot_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Front_rt_legOffset_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('L_Front_foot_CtrlShape', 'Front_lt_foot_ctrl', r=True, s=True)
        cmds.delete('L_Front_foot_Ctrl')
        cmds.rename('Front_lt_foot_ctrlShape', 'Front_lt_foot_ctrlShapeOLD')
        cmds.rename('L_Front_foot_CtrlShape', 'Front_lt_foot_ctrlShape')
        cmds.delete('Front_lt_foot_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Front_rt_legOffset_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('R_Front_foot_CtrlShape', 'Front_rt_foot_ctrl', r=True, s=True)
        cmds.delete('R_Front_foot_Ctrl')
        cmds.rename('Front_rt_foot_ctrlShape', 'Front_rt_foot_ctrlShapeOLD')
        cmds.rename('R_Front_foot_CtrlShape', 'Front_rt_foot_ctrlShape')
        cmds.delete('Front_rt_foot_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Front_rt_legOffset_ctrl" + "\n");
        cmds.select( cl=True )
        
    # //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    # leg fk
    try:
        cmds.parent('L_Rear_fk_hip1_CtrlShape', 'Rear_lt_fk_Rear_lt_hip1_jnt_ctrl', r=True, s=True)
        cmds.delete('L_Rear_fk_hip1_Ctrl')
        cmds.rename('Rear_lt_fk_Rear_lt_hip1_jnt_ctrlShape', 'Rear_lt_fk_Rear_lt_hip1_jnt_ctrlShapeOLD')
        cmds.rename('L_Rear_fk_hip1_CtrlShape', 'Rear_lt_fk_Rear_lt_hip1_jnt_ctrlShape')
        cmds.delete('Rear_lt_fk_Rear_lt_hip1_jnt_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Rear_lt_fk_Rear_lt_hip1_jnt_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('L_Rear_fk_hip2_CtrlShape', 'Rear_lt_fk_Rear_lt_hip2_jnt_ctrl', r=True, s=True)
        cmds.delete('L_Rear_fk_hip2_Ctrl')
        cmds.rename('Rear_lt_fk_Rear_lt_hip2_jnt_ctrlShape', 'Rear_lt_fk_Rear_lt_hip2_jnt_ctrlShapeOLD')
        cmds.rename('L_Rear_fk_hip2_CtrlShape', 'Rear_lt_fk_Rear_lt_hip2_jnt_ctrlShape')
        cmds.delete('Rear_lt_fk_Rear_lt_hip2_jnt_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Rear_lt_fk_Rear_lt_hip2_jnt_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('L_Rear_fk_knee1_CtrlShape', 'Rear_lt_fk_Rear_lt_knee1_jnt_ctrl', r=True, s=True)
        cmds.delete('L_Rear_fk_knee1_Ctrl')
        cmds.rename('Rear_lt_fk_Rear_lt_knee1_jnt_ctrlShape', 'Rear_lt_fk_Rear_lt_knee1_jnt_ctrlShapeOLD')
        cmds.rename('L_Rear_fk_knee1_CtrlShape', 'Rear_lt_fk_Rear_lt_knee1_jnt_ctrlShape')
        cmds.delete('Rear_lt_fk_Rear_lt_knee1_jnt_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Rear_lt_fk_Rear_lt_knee1_jnt_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('L_Rear_fk_ankle_CtrlShape', 'Rear_lt_fk_Rear_lt_ankle_jnt_ctrl', r=True, s=True)
        cmds.delete('L_Rear_fk_ankle_Ctrl')
        cmds.rename('Rear_lt_fk_Rear_lt_ankle_jnt_ctrlShape', 'Rear_lt_fk_Rear_lt_ankle_jnt_ctrlShapeOLD')
        cmds.rename('L_Rear_fk_ankle_CtrlShape', 'Rear_lt_fk_Rear_lt_ankle_jnt_ctrlShape')
        cmds.delete('Rear_lt_fk_Rear_lt_ankle_jnt_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Rear_lt_fk_Rear_lt_ankle_jnt_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('L_Rear_fk_ball_CtrlShape', 'Rear_lt_fk_Rear_lt_ball_jnt_ctrl', r=True, s=True)
        cmds.delete('L_Rear_fk_ball_Ctrl')
        cmds.rename('Rear_lt_fk_Rear_lt_ball_jnt_ctrlShape', 'Rear_lt_fk_Rear_lt_ball_jnt_ctrlShapeOLD')
        cmds.rename('L_Rear_fk_ball_CtrlShape', 'Rear_lt_fk_Rear_lt_ball_jnt_ctrlShape')
        cmds.delete('Rear_lt_fk_Rear_lt_ball_jnt_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Rear_lt_fk_Rear_lt_ball_jnt_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('R_Rear_fk_hip1_CtrlShape', 'Rear_rt_fk_Rear_rt_hip1_jnt_ctrl', r=True, s=True)
        cmds.delete('R_Rear_fk_hip1_Ctrl')
        cmds.rename('Rear_rt_fk_Rear_rt_hip1_jnt_ctrlShape', 'Rear_rt_fk_Rear_rt_hip1_jnt_ctrlShapeOLD')
        cmds.rename('R_Rear_fk_hip1_CtrlShape', 'Rear_rt_fk_Rear_rt_hip1_jnt_ctrlShape')
        cmds.delete('Rear_rt_fk_Rear_rt_hip1_jnt_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Rear_rt_fk_Rear_lt_hip1_jnt_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('R_Rear_fk_hip2_CtrlShape', 'Rear_rt_fk_Rear_rt_hip2_jnt_ctrl', r=True, s=True)
        cmds.delete('R_Rear_fk_hip2_Ctrl')
        cmds.rename('Rear_rt_fk_Rear_rt_hip2_jnt_ctrlShape', 'Rear_rt_fk_Rear_rt_hip2_jnt_ctrlShapeOLD')
        cmds.rename('R_Rear_fk_hip2_CtrlShape', 'Rear_rt_fk_Rear_rt_hip2_jnt_ctrlShape')
        cmds.delete('Rear_rt_fk_Rear_rt_hip2_jnt_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Rear_rt_fk_Rear_rt_hip2_jnt_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('R_Rear_fk_knee1_CtrlShape', 'Rear_rt_fk_Rear_rt_knee1_jnt_ctrl', r=True, s=True)
        cmds.delete('R_Rear_fk_knee1_Ctrl')
        cmds.rename('Rear_rt_fk_Rear_rt_knee1_jnt_ctrlShape', 'Rear_rt_fk_Rear_rt_knee1_jnt_ctrlShapeOLD')
        cmds.rename('R_Rear_fk_knee1_CtrlShape', 'Rear_rt_fk_Rear_rt_knee1_jnt_ctrlShape')
        cmds.delete('Rear_rt_fk_Rear_rt_knee1_jnt_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Rear_rt_fk_Rear_rt_knee1_jnt_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('R_Rear_fk_ankle_CtrlShape', 'Rear_rt_fk_Rear_rt_ankle_jnt_ctrl', r=True, s=True)
        cmds.delete('R_Rear_fk_ankle_Ctrl')
        cmds.rename('Rear_rt_fk_Rear_rt_ankle_jnt_ctrlShape', 'Rear_rt_fk_Rear_rt_ankle_jnt_ctrlShapeOLD')
        cmds.rename('R_Rear_fk_ankle_CtrlShape', 'Rear_rt_fk_Rear_rt_ankle_jnt_ctrlShape')
        cmds.delete('Rear_rt_fk_Rear_rt_ankle_jnt_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Rear_rt_fk_Rear_rt_ankle_jnt_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('R_Rear_fk_ball_CtrlShape', 'Rear_rt_fk_Rear_rt_ball_jnt_ctrl', r=True, s=True)
        cmds.delete('R_Rear_fk_ball_Ctrl')
        cmds.rename('Rear_rt_fk_Rear_rt_ball_jnt_ctrlShape', 'Rear_rt_fk_Rear_rt_ball_jnt_ctrlShapeOLD')
        cmds.rename('R_Rear_fk_ball_CtrlShape', 'Rear_rt_fk_Rear_rt_ball_jnt_ctrlShape')
        cmds.delete('Rear_rt_fk_Rear_rt_ball_jnt_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Rear_rt_fk_Rear_rt_ball_jnt_ctrl" + "\n");
        cmds.select( cl=True )
        
    # //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    try:
        cmds.parent('L_Front_fk_hip1_CtrlShape', 'Front_lt_fk_Front_lt_hip1_jnt_ctrl', r=True, s=True)
        cmds.delete('L_Front_fk_hip1_Ctrl')
        cmds.rename('Front_lt_fk_Front_lt_hip1_jnt_ctrlShape', 'Front_lt_fk_Front_lt_hip1_jnt_ctrlShapeOLD')
        cmds.rename('L_Front_fk_hip1_CtrlShape', 'Front_lt_fk_Front_lt_hip1_jnt_ctrlShape')
        cmds.delete('Front_lt_fk_Front_lt_hip1_jnt_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Front_lt_fk_Front_lt_hip1_jnt_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('L_Front_fk_hip2_CtrlShape', 'Front_lt_fk_Front_lt_hip2_jnt_ctrl', r=True, s=True)
        cmds.delete('L_Front_fk_hip2_Ctrl')
        cmds.rename('Front_lt_fk_Front_lt_hip2_jnt_ctrlShape', 'Front_lt_fk_Front_lt_hip2_jnt_ctrlShapeOLD')
        cmds.rename('L_Front_fk_hip2_CtrlShape', 'Front_lt_fk_Front_lt_hip2_jnt_ctrlShape')
        cmds.delete('Front_lt_fk_Front_lt_hip2_jnt_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Front_lt_fk_Front_lt_hip2_jnt_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('L_Front_fk_knee1_CtrlShape', 'Front_lt_fk_Front_lt_knee1_jnt_ctrl', r=True, s=True)
        cmds.delete('L_Front_fk_knee1_Ctrl')
        cmds.rename('Front_lt_fk_Front_lt_knee1_jnt_ctrlShape', 'Front_lt_fk_Front_lt_knee1_jnt_ctrlShapeOLD')
        cmds.rename('L_Front_fk_knee1_CtrlShape', 'Front_lt_fk_Front_lt_knee1_jnt_ctrlShape')
        cmds.delete('Front_lt_fk_Front_lt_knee1_jnt_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Front_lt_fk_Front_lt_knee1_jnt_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('L_Front_fk_ankle_CtrlShape', 'Front_lt_fk_Front_lt_ankle_jnt_ctrl', r=True, s=True)
        cmds.delete('L_Front_fk_ankle_Ctrl')
        cmds.rename('Front_lt_fk_Front_lt_ankle_jnt_ctrlShape', 'Front_lt_fk_Front_lt_ankle_jnt_ctrlShapeOLD')
        cmds.rename('L_Front_fk_ankle_CtrlShape', 'Front_lt_fk_Front_lt_ankle_jnt_ctrlShape')
        cmds.delete('Front_lt_fk_Front_lt_ankle_jnt_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Front_lt_fk_Front_lt_ankle_jnt_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('L_Front_fk_ball_CtrlShape', 'Front_lt_fk_Front_lt_ball_jnt_ctrl', r=True, s=True)
        cmds.delete('L_Front_fk_ball_Ctrl')
        cmds.rename('Front_lt_fk_Front_lt_ball_jnt_ctrlShape', 'Front_lt_fk_Front_lt_ball_jnt_ctrlShapeOLD')
        cmds.rename('L_Front_fk_ball_CtrlShape', 'Front_lt_fk_Front_lt_ball_jnt_ctrlShape')
        cmds.delete('Front_lt_fk_Front_lt_ball_jnt_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Front_lt_fk_Front_lt_ball_jnt_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('R_Front_fk_hip1_CtrlShape', 'Front_rt_fk_Front_rt_hip1_jnt_ctrl', r=True, s=True)
        cmds.delete('R_Front_fk_hip1_Ctrl')
        cmds.rename('Front_rt_fk_Front_rt_hip1_jnt_ctrlShape', 'Front_rt_fk_Front_rt_hip1_jnt_ctrlShapeOLD')
        cmds.rename('R_Front_fk_hip1_CtrlShape', 'Front_rt_fk_Front_rt_hip1_jnt_ctrlShape')
        cmds.delete('Front_rt_fk_Front_rt_hip1_jnt_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Front_rt_fk_Front_lt_hip1_jnt_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('R_Front_fk_hip2_CtrlShape', 'Front_rt_fk_Front_rt_hip2_jnt_ctrl', r=True, s=True)
        cmds.delete('R_Front_fk_hip2_Ctrl')
        cmds.rename('Front_rt_fk_Front_rt_hip2_jnt_ctrlShape', 'Front_rt_fk_Front_rt_hip2_jnt_ctrlShapeOLD')
        cmds.rename('R_Front_fk_hip2_CtrlShape', 'Front_rt_fk_Front_rt_hip2_jnt_ctrlShape')
        cmds.delete('Front_rt_fk_Front_rt_hip2_jnt_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Front_rt_fk_Front_rt_hip2_jnt_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('R_Front_fk_knee1_CtrlShape', 'Front_rt_fk_Front_rt_knee1_jnt_ctrl', r=True, s=True)
        cmds.delete('R_Front_fk_knee1_Ctrl')
        cmds.rename('Front_rt_fk_Front_rt_knee1_jnt_ctrlShape', 'Front_rt_fk_Front_rt_knee1_jnt_ctrlShapeOLD')
        cmds.rename('R_Front_fk_knee1_CtrlShape', 'Front_rt_fk_Front_rt_knee1_jnt_ctrlShape')
        cmds.delete('Front_rt_fk_Front_rt_knee1_jnt_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Front_rt_fk_Front_rt_knee1_jnt_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('R_Front_fk_ankle_CtrlShape', 'Front_rt_fk_Front_rt_ankle_jnt_ctrl', r=True, s=True)
        cmds.delete('R_Front_fk_ankle_Ctrl')
        cmds.rename('Front_rt_fk_Front_rt_ankle_jnt_ctrlShape', 'Front_rt_fk_Front_rt_ankle_jnt_ctrlShapeOLD')
        cmds.rename('R_Front_fk_ankle_CtrlShape', 'Front_rt_fk_Front_rt_ankle_jnt_ctrlShape')
        cmds.delete('Front_rt_fk_Front_rt_ankle_jnt_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Front_rt_fk_Front_rt_ankle_jnt_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('R_Front_fk_ball_CtrlShape', 'Front_rt_fk_Front_rt_ball_jnt_ctrl', r=True, s=True)
        cmds.delete('R_Front_fk_ball_Ctrl')
        cmds.rename('Front_rt_fk_Front_rt_ball_jnt_ctrlShape', 'Front_rt_fk_Front_rt_ball_jnt_ctrlShapeOLD')
        cmds.rename('R_Front_fk_ball_CtrlShape', 'Front_rt_fk_Front_rt_ball_jnt_ctrlShape')
        cmds.delete('Front_rt_fk_Front_rt_ball_jnt_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "Front_rt_fk_Front_rt_ball_jnt_ctrl" + "\n");
        cmds.select( cl=True )

    # ////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
    # shapes che rimangono scollegate
    try:
        cmds.setAttr('newShapes_Grp.v', 0)
        array = cmds.listRelatives('newShapes_Grp', ad=True, typ='transform' )
        print (array)
        print ("Il numero di Controlli non sostituiti e':")
        print (len(array))
    except:
        print("--- skipped " + "newShapes_Grp" + "\n")
        
    print("--- END SCRIPT ---" + "\n")
    cmds.undoInfo(closeChunk=True) 
# //////////////////////////////////////////////////////////////////////////////////////////////////////////////
# END
# //////////////////////////////////////////////////////////////////////////////////////////////////////////////