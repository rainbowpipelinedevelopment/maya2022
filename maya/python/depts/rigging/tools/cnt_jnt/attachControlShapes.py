#//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#NEW SHAPE FOR EACH CONTROLS
#Importare File contenente tutte le nuove surface shapes
#//////////////////////////////////////////////////////////////////////////////////////////////////////////////
from imp import reload
import maya.cmds as cmds


def newControlShapesFixed():
    cmds.undoInfo(openChunk=True)
    try:
        cmds.select('newShapes_Grp')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "newShapes_Grp <--> You should load the file with all the new shapes. ---" + "\n"); 
        cmds.select( cl=True )

    # LEGS CONTROLS
    # leg IK lf
    try:
        cmds.parent('L_heel_ik_CtrlShape', 'RIG_lf_heel_ik_ctrl', r=True, s=True)
        cmds.delete('L_heel_ik_Ctrl')
        cmds.rename('RIG_lf_heel_ik_ctrlShape', 'RIG_lf_heel_ik_ctrlShapeOLD')
        cmds.rename('L_heel_ik_CtrlShape', 'RIG_lf_heel_ik_ctrlShape')
        cmds.delete('RIG_lf_heel_ik_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_heel_ik_ctrl" + "\n"); 
        cmds.select( cl=True )
    try:
        cmds.parent('L_knee_pv_CtrlShape', 'RIG_lf_knee_pv_ctrl', r=True, s=True)
        cmds.parent('L_knee_pv_CtrlShape1', 'RIG_lf_knee_pv_ctrl', r=True, s=True)
        cmds.parent('L_knee_pv_CtrlShape2', 'RIG_lf_knee_pv_ctrl', r=True, s=True)
        cmds.delete('L_knee_pv_Ctrl')
        cmds.rename('RIG_lf_knee_pv_ctrlShape', 'RIG_lf_knee_pv_ctrlShapeOLD')
        cmds.rename('L_knee_pv_CtrlShape', 'RIG_lf_knee_pv_ctrlShape')
        cmds.rename('L_knee_pv_CtrlShape1', 'RIG_lf_knee_pv_ctrlShape1')
        cmds.rename('L_knee_pv_CtrlShape2', 'RIG_lf_knee_pv_ctrlShape2')
        cmds.delete('RIG_lf_knee_pv_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_heel_ik_ctrl" + "\n"); 
        cmds.select( cl=True )

    try:
        cmds.parent('L_hip_CtrlShape', 'RIG_lf_hip_ctrl', r=True, s=True)
        cmds.delete('L_hip_Ctrl')
        cmds.rename('RIG_lf_hip_ctrlShape', 'RIG_lf_hip_ctrlShapeOLD')
        cmds.rename('L_hip_CtrlShape', 'RIG_lf_hip_ctrlShape')
        cmds.delete('RIG_lf_hip_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_hip_ctrl" + "\n"); 
        cmds.select( cl=True )
    # //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    # leg IK rt
    try:
        cmds.parent('R_heel_ik_CtrlShape', 'RIG_rt_heel_ik_ctrl', r=True, s=True)
        cmds.delete('R_heel_ik_Ctrl')
        cmds.rename('RIG_rt_heel_ik_ctrlShape', 'RIG_rt_heel_ik_ctrlShapeOLD')
        cmds.rename('R_heel_ik_CtrlShape', 'RIG_rt_heel_ik_ctrlShape')
        cmds.delete('RIG_rt_heel_ik_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_heel_ik_ctrl" + "\n"); 
        cmds.select( cl=True )

    try:  
        cmds.parent('R_knee_pv_CtrlShape', 'RIG_rt_knee_pv_ctrl', r=True, s=True)
        cmds.parent('R_knee_pv_CtrlShape1', 'RIG_rt_knee_pv_ctrl', r=True, s=True)
        cmds.parent('R_knee_pv_CtrlShape2', 'RIG_rt_knee_pv_ctrl', r=True, s=True)
        cmds.parent('R_knee_pv_CtrlShape3', 'RIG_rt_knee_pv_ctrl', r=True, s=True)
        cmds.delete('R_knee_pv_Ctrl')
        cmds.rename('RIG_rt_knee_pv_ctrlShape', 'RIG_rt_knee_pv_ctrlShapeOLD')
        cmds.rename('R_knee_pv_CtrlShape', 'RIG_rt_knee_pv_ctrlShape')
        cmds.rename('R_knee_pv_CtrlShape1', 'RIG_rt_knee_pv_ctrlShape1')
        cmds.rename('R_knee_pv_CtrlShape2', 'RIG_rt_knee_pv_ctrlShape2')
        cmds.rename('R_knee_pv_CtrlShape3', 'RIG_rt_knee_pv_ctrlShape3')
        cmds.delete('RIG_rt_knee_pv_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_knee_pv_ctrl" + "\n"); 
        cmds.select( cl=True )

    try: 
        cmds.parent('R_hip_CtrlShape', 'RIG_rt_hip_ctrl', r=True, s=True)
        cmds.delete('R_hip_Ctrl')
        cmds.rename('RIG_rt_hip_ctrlShape', 'RIG_rt_hip_ctrlShapeOLD')
        cmds.rename('R_hip_CtrlShape', 'RIG_rt_hip_ctrlShape')
        cmds.delete('RIG_rt_hip_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_hip_ctrl" + "\n"); 
        cmds.select( cl=True )

    # //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    # leg FK lf
    try: 
        cmds.setAttr("RIG_lf_upLeg_sec_fk_ctrlShape.overrideEnabled", 1);
        cmds.setAttr("RIG_lf_upLeg_sec_fk_ctrlShape.overrideColor", 6);
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_upLeg_sec_fk_ctrl" + "\n"); 
        cmds.select( cl=True )

    try: 
        cmds.setAttr("RIG_lf_knee_sec_fk_ctrlShape.overrideEnabled", 1);
        cmds.setAttr("RIG_lf_knee_sec_fk_ctrlShape.overrideColor", 6);
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_knee_sec_fk_ctrl" + "\n"); 
        cmds.select( cl=True )

    try: 
        cmds.setAttr("RIG_lf_ankle_sec_fk_ctrlShape.overrideEnabled", 1);
        cmds.setAttr("RIG_lf_ankle_sec_fk_ctrlShape.overrideColor", 6);
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_ankle_sec_fk_ctrl" + "\n"); 
        cmds.select( cl=True )

    try:   
        cmds.parent('L_upLeg_fk_CtrlShape', 'RIG_lf_upLeg_fk_ctrl', r=True, s=True)
        cmds.delete('L_upLeg_fk_Ctrl')
        cmds.rename('RIG_lf_upLeg_fk_ctrlShape', 'RIG_lf_upLeg_fk_ctrlShapeOLD')
        cmds.rename('L_upLeg_fk_CtrlShape', 'RIG_lf_upLeg_fk_ctrlShape')
        cmds.delete('RIG_lf_upLeg_fk_ctrlShapeOLD')
        cmds.setAttr("RIG_lf_upLeg_fk_ctrl.secondaryFkCtrlVis", 0)
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_upLeg_fk_ctrl" + "\n"); 
        cmds.select( cl=True )

    try: 
        cmds.parent('L_knee_fk_CtrlShape', 'RIG_lf_knee_fk_ctrl', r=True, s=True)
        cmds.delete('L_knee_fk_Ctrl')
        cmds.rename('RIG_lf_knee_fk_ctrlShape', 'RIG_lf_knee_fk_ctrlShapeOLD')
        cmds.rename('L_knee_fk_CtrlShape', 'RIG_lf_knee_fk_ctrlShape')
        cmds.delete('RIG_lf_knee_fk_ctrlShapeOLD')
        cmds.setAttr("RIG_lf_knee_fk_ctrl.secondaryFkCtrlVis", 0)
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_knee_fk_ctrl" + "\n"); 
        cmds.select( cl=True )

    try: 
        cmds.parent('L_ankle_fk_CtrlShape', 'RIG_lf_ankle_fk_ctrl', r=True, s=True)
        cmds.delete('L_ankle_fk_Ctrl')
        cmds.rename('RIG_lf_ankle_fk_ctrlShape', 'RIG_lf_ankle_fk_ctrlShapeOLD')
        cmds.rename('L_ankle_fk_CtrlShape', 'RIG_lf_ankle_fk_ctrlShape')
        cmds.delete('RIG_lf_ankle_fk_ctrlShapeOLD')
        cmds.setAttr("RIG_lf_ankle_fk_ctrl.secondaryFkCtrlVis", 0)
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_ankle_fk_ctrl" + "\n"); 
        cmds.select( cl=True )

    try: 
        cmds.parent('L_ball_fk_CtrlShape', 'RIG_lf_ball_fk_ctrl', r=True, s=True)
        cmds.delete('L_ball_fk_Ctrl')
        cmds.rename('RIG_lf_ball_fk_ctrlShape', 'RIG_lf_ball_fk_ctrlShapeOLD')
        cmds.rename('L_ball_fk_CtrlShape', 'RIG_lf_ball_fk_ctrlShape')
        cmds.delete('RIG_lf_ball_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_ball_fk_ctrl" + "\n"); 
        cmds.select( cl=True )

    # //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    # leg FK rt
    try:
        cmds.setAttr("RIG_rt_upLeg_sec_fk_ctrlShape.overrideEnabled", 1);
        cmds.setAttr("RIG_rt_upLeg_sec_fk_ctrlShape.overrideColor", 6);
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_upLeg_sec_fk_ctrl" + "\n"); 
        cmds.select( cl=True )

    try:
        cmds.setAttr("RIG_rt_knee_sec_fk_ctrlShape.overrideEnabled", 1);
        cmds.setAttr("RIG_rt_knee_sec_fk_ctrlShape.overrideColor", 6);
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_knee_sec_fk_ctrl" + "\n"); 
        cmds.select( cl=True )

    try:
        cmds.setAttr("RIG_rt_ankle_sec_fk_ctrlShape.overrideEnabled", 1);
        cmds.setAttr("RIG_rt_ankle_sec_fk_ctrlShape.overrideColor", 6);
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_ankle_sec_fk_ctrl" + "\n"); 
        cmds.select( cl=True )

    try:
        cmds.parent('R_upLeg_fk_CtrlShape', 'RIG_rt_upLeg_fk_ctrl', r=True, s=True)
        cmds.delete('R_upLeg_fk_Ctrl')
        cmds.rename('RIG_rt_upLeg_fk_ctrlShape', 'RIG_rt_upLeg_fk_ctrlShapeOLD')
        cmds.rename('R_upLeg_fk_CtrlShape', 'RIG_rt_upLeg_fk_ctrlShape')
        cmds.delete('RIG_rt_upLeg_fk_ctrlShapeOLD')
        cmds.setAttr("RIG_rt_upLeg_fk_ctrl.secondaryFkCtrlVis", 0)
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_upLeg_fk_ctrl" + "\n"); 
        cmds.select( cl=True )

    try:
        cmds.parent('R_knee_fk_CtrlShape', 'RIG_rt_knee_fk_ctrl', r=True, s=True)
        cmds.delete('R_knee_fk_Ctrl')
        cmds.rename('RIG_rt_knee_fk_ctrlShape', 'RIG_rt_knee_fk_ctrlShapeOLD')
        cmds.rename('R_knee_fk_CtrlShape', 'RIG_rt_knee_fk_ctrlShape')
        cmds.delete('RIG_rt_knee_fk_ctrlShapeOLD')
        cmds.setAttr("RIG_rt_knee_fk_ctrl.secondaryFkCtrlVis", 0)
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_knee_fk_ctrl" + "\n"); 
        cmds.select( cl=True )

    try:
        cmds.parent('R_ankle_fk_CtrlShape', 'RIG_rt_ankle_fk_ctrl', r=True, s=True)
        cmds.delete('R_ankle_fk_Ctrl')
        cmds.rename('RIG_rt_ankle_fk_ctrlShape', 'RIG_rt_ankle_fk_ctrlShapeOLD')
        cmds.rename('R_ankle_fk_CtrlShape', 'RIG_rt_ankle_fk_ctrlShape')
        cmds.delete('RIG_rt_ankle_fk_ctrlShapeOLD')
        cmds.setAttr("RIG_rt_ankle_fk_ctrl.secondaryFkCtrlVis", 0)
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_ankle_fk_ctrl" + "\n"); 
        cmds.select( cl=True )

    try:
        cmds.parent('R_ball_fk_CtrlShape', 'RIG_rt_ball_fk_ctrl', r=True, s=True)
        cmds.delete('R_ball_fk_Ctrl')
        cmds.rename('RIG_rt_ball_fk_ctrlShape', 'RIG_rt_ball_fk_ctrlShapeOLD')
        cmds.rename('R_ball_fk_CtrlShape', 'RIG_rt_ball_fk_ctrlShape')
        cmds.delete('RIG_rt_ball_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_ball_fk_ctrl" + "\n"); 
        cmds.select( cl=True )

    # //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    # cog, hip, spine, clavicles, neck and head
    try:
        cmds.parent('cog_CtrlShape', 'RIG_cog_ctrl', r=True, s=True)
        cmds.delete('cog_Ctrl')
        cmds.rename('RIG_cog_ctrlShape', 'RIG_cog_ctrlShapeOLD')
        cmds.rename('cog_CtrlShape', 'RIG_cog_ctrlShape')
        cmds.delete('RIG_cog_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_cog_ctrl" + "\n"); 
        cmds.select( cl=True )
        
    try:
        cmds.parent('hip_CtrlShape', 'RIG_hip_ctrl', r=True, s=True)
        cmds.delete('hip_Ctrl')
        cmds.rename('RIG_hip_ctrlShape', 'RIG_hip_ctrlShapeOLD')
        cmds.rename('hip_CtrlShape', 'RIG_hip_ctrlShape')
        cmds.delete('RIG_hip_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_hip_ctrl" + "\n"); 
        cmds.select( cl=True )
        
    try:
        cmds.parent('spineLow_CtrlShape', 'RIG_spineLow_ctrl', r=True, s=True)
        cmds.delete('spineLow_Ctrl')
        cmds.rename('RIG_spineLow_ctrlShape', 'RIG_spineLow_ctrlShapeOLD')
        cmds.rename('spineLow_CtrlShape', 'RIG_spineLow_ctrlShape')
        cmds.delete('RIG_spineLow_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_spineLow_ctrl" + "\n"); 
        cmds.select( cl=True )

    try:
        cmds.parent('spineMid_CtrlShape', 'RIG_spineMid_ctrl', r=True, s=True)
        cmds.delete('spineMid_Ctrl')
        cmds.rename('RIG_spineMid_ctrlShape', 'RIG_spineMid_ctrlShapeOLD')
        cmds.rename('spineMid_CtrlShape', 'RIG_spineMid_ctrlShape')
        cmds.delete('RIG_spineMid_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_spineMid_ctrl" + "\n"); 
        cmds.select( cl=True )

    try:
        cmds.parent('L_clavicleTrans_CtrlShape', 'RIG_lf_clavicleTrans_ctrl', r=True, s=True)
        cmds.delete('L_clavicleTrans_Ctrl')
        cmds.rename('RIG_lf_clavicleTrans_ctrlShape', 'RIG_lf_clavicleTrans_ctrlShapeOLD')
        cmds.rename('L_clavicleTrans_CtrlShape', 'RIG_lf_clavicleTrans_ctrlShape')
        cmds.delete('RIG_lf_clavicleTrans_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_clavicleTrans_ctrl" + "\n"); 
        cmds.select( cl=True )
        
    try:
        cmds.parent('R_clavicleTrans_CtrlShape', 'RIG_rt_clavicleTrans_ctrl', r=True, s=True)
        cmds.delete('R_clavicleTrans_Ctrl')
        cmds.rename('RIG_rt_clavicleTrans_ctrlShape', 'RIG_rt_clavicleTrans_ctrlShapeOLD')
        cmds.rename('R_clavicleTrans_CtrlShape', 'RIG_rt_clavicleTrans_ctrlShape')
        cmds.delete('RIG_rt_clavicleTrans_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_clavicleTrans_ctrl" + "\n"); 
        cmds.select( cl=True )

    try:
        cmds.parent('spineHigh_CtrlShape', 'RIG_spineHigh_ctrl', r=True, s=True)
        cmds.delete('spineHigh_Ctrl')
        cmds.rename('RIG_spineHigh_ctrlShape', 'RIG_spineHigh_ctrlShapeOLD')
        cmds.rename('spineHigh_CtrlShape', 'RIG_spineHigh_ctrlShape')
        cmds.delete('RIG_spineHigh_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_spineHigh_ctrl" + "\n"); 
        cmds.select( cl=True )
        
    try:
        cmds.parent('neck_CtrlShape', 'RIG_neck_ctrl', r=True, s=True)
        cmds.delete('neck_Ctrl')
        cmds.rename('RIG_neck_ctrlShape', 'RIG_neck_ctrlShapeOLD')
        cmds.rename('neck_CtrlShape', 'RIG_neck_ctrlShape')
        cmds.delete('RIG_neck_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_neck_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('head_CtrlShape', 'RIG_head_ctrl', r=True, s=True)
        cmds.delete('head_Ctrl')
        cmds.rename('RIG_head_ctrlShape', 'RIG_head_ctrlShapeOLD')
        cmds.rename('head_CtrlShape', 'RIG_head_ctrlShape')
        cmds.delete('RIG_head_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_head_ctrl" + "\n");
        cmds.select( cl=True )

    # //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    # hands
    try:
        cmds.parent('L_hand_CtrlShape', 'RIG_lf_hand_ctrl', r=True, s=True)
        cmds.delete('L_hand_Ctrl')
        cmds.rename('RIG_lf_hand_ctrlShape', 'RIG_lf_hand_ctrlShapeOLD')
        cmds.rename('L_hand_CtrlShape', 'L_hand_CtrlShape')
        cmds.delete('RIG_lf_hand_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_hand_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('R_hand_CtrlShape', 'RIG_rt_hand_ctrl', r=True, s=True)
        cmds.delete('R_hand_Ctrl')
        cmds.rename('RIG_rt_hand_ctrlShape', 'RIG_rt_hand_ctrlShapeOLD')
        cmds.rename('R_hand_CtrlShape', 'R_hand_CtrlShape')
        cmds.delete('RIG_rt_hand_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_hand_ctrl" + "\n");
        cmds.select( cl=True )
        
    # //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    # arm IK lf
    try:
        cmds.parent('L_arm_ik_CtrlShape', 'RIG_lf_arm_ik_ctrl', r=True, s=True)
        cmds.delete('L_arm_ik_Ctrl')
        cmds.rename('RIG_lf_arm_ik_ctrlShape', 'RIG_lf_arm_ik_ctrlShapeOLD')
        cmds.rename('L_arm_ik_CtrlShape', 'RIG_lf_arm_ik_ctrlShape')
        cmds.delete('RIG_lf_arm_ik_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_arm_ik_ctrl" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('L_elbow_pv_CtrlShape', 'RIG_lf_elbow_pv_ctrl', r=True, s=True)
        cmds.parent('L_elbow_pv_CtrlShape1', 'RIG_lf_elbow_pv_ctrl', r=True, s=True)
        cmds.parent('L_elbow_pv_CtrlShape2', 'RIG_lf_elbow_pv_ctrl', r=True, s=True)
        cmds.parent('L_elbow_pv_CtrlShape3', 'RIG_lf_elbow_pv_ctrl', r=True, s=True)
        cmds.delete('L_elbow_pv_Ctrl')
        cmds.rename('RIG_lf_elbow_pv_ctrlShape', 'RIG_lf_elbow_pv_ctrlShapeOLD')
        cmds.rename('L_elbow_pv_CtrlShape', 'RIG_lf_knee_pv_ctrlShape')
        cmds.rename('L_elbow_pv_CtrlShape1', 'RIG_lf_knee_pv_ctrlShape1')
        cmds.rename('L_elbow_pv_CtrlShape2', 'RIG_lf_knee_pv_ctrlShape2')
        cmds.rename('L_elbow_pv_CtrlShape3', 'RIG_lf_knee_pv_ctrlShape3')
        cmds.delete('RIG_lf_elbow_pv_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_elbow_pv_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('L_shoulder_CtrlShape', 'RIG_lf_shoulder_ctrl', r=True, s=True)
        cmds.delete('L_shoulder_Ctrl')
        cmds.rename('RIG_lf_shoulder_ctrlShape', 'RIG_lf_shoulder_ctrlShapeOLD')
        cmds.rename('L_shoulder_CtrlShape', 'RIG_lf_shoulder_ctrlShape')
        cmds.delete('RIG_lf_shoulder_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_shoulder_ctrl" + "\n");
        cmds.select( cl=True )

    # //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    # arm IK rt
    try:
        cmds.parent('R_arm_ik_CtrlShape', 'RIG_rt_arm_ik_ctrl', r=True, s=True)
        cmds.delete('R_arm_ik_Ctrl')
        cmds.rename('RIG_rt_arm_ik_ctrlShape', 'RIG_rt_arm_ik_ctrlShapeOLD')
        cmds.rename('R_arm_ik_CtrlShape', 'RIG_rt_arm_ik_ctrlShape')
        cmds.delete('RIG_rt_arm_ik_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_arm_ik_ctrl" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('R_elbow_pv_CtrlShape', 'RIG_rt_elbow_pv_ctrl', r=True, s=True)
        cmds.parent('R_elbow_pv_CtrlShape1', 'RIG_rt_elbow_pv_ctrl', r=True, s=True)
        cmds.parent('R_elbow_pv_CtrlShape2', 'RIG_rt_elbow_pv_ctrl', r=True, s=True)
        cmds.parent('R_elbow_pv_CtrlShape3', 'RIG_rt_elbow_pv_ctrl', r=True, s=True)
        cmds.delete('R_elbow_pv_Ctrl')
        cmds.rename('RIG_rt_elbow_pv_ctrlShape', 'RIG_rt_elbow_pv_ctrlShapeOLD')
        cmds.rename('R_elbow_pv_CtrlShape', 'RIG_rt_knee_pv_ctrlShape')
        cmds.rename('R_elbow_pv_CtrlShape1', 'RIG_rt_knee_pv_ctrlShape1')
        cmds.rename('R_elbow_pv_CtrlShape2', 'RIG_rt_knee_pv_ctrlShape2')
        cmds.rename('R_elbow_pv_CtrlShape3', 'RIG_rt_knee_pv_ctrlShape3')
        cmds.delete('RIG_rt_elbow_pv_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_elbow_pv_ctrl" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('R_shoulder_CtrlShape', 'RIG_rt_shoulder_ctrl', r=True, s=True)
        cmds.delete('R_shoulder_Ctrl')
        cmds.rename('RIG_rt_shoulder_ctrlShape', 'RIG_rt_shoulder_ctrlShapeOLD')
        cmds.rename('R_shoulder_CtrlShape', 'RIG_rt_shoulder_ctrlShape')
        cmds.delete('RIG_rt_shoulder_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_shoulder_ctrl" + "\n");
        cmds.select( cl=True )

    # //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    # arm FK lf
    try:
        cmds.setAttr("RIG_lf_upArm_sec_fk_ctrlShape.overrideEnabled", 1);
        cmds.setAttr("RIG_lf_upArm_sec_fk_ctrlShape.overrideColor", 6);
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_upArm_sec_fk_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.setAttr("RIG_lf_elbow_sec_fk_ctrlShape.overrideEnabled", 1);
        cmds.setAttr("RIG_lf_elbow_sec_fk_ctrlShape.overrideColor", 6);
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_elbow_sec_fk_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.setAttr("RIG_lf_wrist_sec_fk_ctrlShape.overrideEnabled", 1);
        cmds.setAttr("RIG_lf_wrist_sec_fk_ctrlShape.overrideColor", 6);
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_wrist_sec_fk_ctrl" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('L_upArm_fk_CtrlShape', 'RIG_lf_upArm_fk_ctrl', r=True, s=True)
        cmds.delete('L_upArm_fk_Ctrl')
        cmds.rename('RIG_lf_upArm_fk_ctrlShape', 'RIG_lf_upArm_fk_ctrlShapeOLD')
        cmds.rename('L_upArm_fk_CtrlShape', 'RIG_lf_upLeg_fk_ctrlShape')
        cmds.delete('RIG_lf_upArm_fk_ctrlShapeOLD')
        cmds.setAttr("RIG_lf_upArm_fk_ctrl.secondaryFkCtrlVis", 0)
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_upArm_fk_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('L_elbow_fk_CtrlShape', 'RIG_lf_elbow_fk_ctrl', r=True, s=True)
        cmds.delete('L_elbow_fk_Ctrl')
        cmds.rename('RIG_lf_elbow_fk_ctrlShape', 'RIG_lf_elbow_fk_ctrlShapeOLD')
        cmds.rename('L_elbow_fk_CtrlShape', 'RIG_lf_upLeg_fk_ctrlShape')
        cmds.delete('RIG_lf_elbow_fk_ctrlShapeOLD')
        cmds.setAttr("RIG_lf_elbow_fk_ctrl.secondaryFkCtrlVis", 0)
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_elbow_fk_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('L_wrist_fk_CtrlShape', 'RIG_lf_wrist_fk_ctrl', r=True, s=True)
        cmds.delete('L_wrist_fk_Ctrl')
        cmds.rename('RIG_lf_wrist_fk_ctrlShape', 'RIG_lf_wrist_fk_ctrlShapeOLD')
        cmds.rename('L_wrist_fk_CtrlShape', 'RIG_lf_upLeg_fk_ctrlShape')
        cmds.delete('RIG_lf_wrist_fk_ctrlShapeOLD')
        cmds.setAttr("RIG_lf_wrist_fk_ctrl.secondaryFkCtrlVis", 0)
    except:
        print("--- skipped " + "RIG_lf_wrist_fk_ctrl" + "\n");
        cmds.select( cl=True )

    # //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    # arm FK rt
    try:
        cmds.setAttr("RIG_rt_upArm_sec_fk_ctrlShape.overrideEnabled", 1);
        cmds.setAttr("RIG_rt_upArm_sec_fk_ctrlShape.overrideColor", 6);
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_upArm_sec_fk_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.setAttr("RIG_rt_elbow_sec_fk_ctrlShape.overrideEnabled", 1);
        cmds.setAttr("RIG_rt_elbow_sec_fk_ctrlShape.overrideColor", 6);
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_elbow_sec_fk_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.setAttr("RIG_rt_wrist_sec_fk_ctrlShape.overrideEnabled", 1);
        cmds.setAttr("RIG_rt_wrist_sec_fk_ctrlShape.overrideColor", 6);
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_wrist_sec_fk_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('R_upArm_fk_CtrlShape', 'RIG_rt_upArm_fk_ctrl', r=True, s=True)
        cmds.delete('R_upArm_fk_Ctrl')
        cmds.rename('RIG_rt_upArm_fk_ctrlShape', 'RIG_rt_upArm_fk_ctrlShapeOLD')
        cmds.rename('R_upArm_fk_CtrlShape', 'RIG_rt_upLeg_fk_ctrlShape')
        cmds.delete('RIG_rt_upArm_fk_ctrlShapeOLD')
        cmds.setAttr("RIG_rt_upArm_fk_ctrl.secondaryFkCtrlVis", 0)
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_upArm_fk_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('R_elbow_fk_CtrlShape', 'RIG_rt_elbow_fk_ctrl', r=True, s=True)
        cmds.delete('R_elbow_fk_Ctrl')
        cmds.rename('RIG_rt_elbow_fk_ctrlShape', 'RIG_rt_elbow_fk_ctrlShapeOLD')
        cmds.rename('R_elbow_fk_CtrlShape', 'RIG_rt_upLeg_fk_ctrlShape')
        cmds.delete('RIG_rt_elbow_fk_ctrlShapeOLD')
        cmds.setAttr("RIG_rt_elbow_fk_ctrl.secondaryFkCtrlVis", 0)
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_elbow_fk_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('R_wrist_fk_CtrlShape', 'RIG_rt_wrist_fk_ctrl', r=True, s=True)
        cmds.delete('R_wrist_fk_Ctrl')
        cmds.rename('RIG_rt_wrist_fk_ctrlShape', 'RIG_rt_wrist_fk_ctrlShapeOLD')
        cmds.rename('R_wrist_fk_CtrlShape', 'RIG_rt_upLeg_fk_ctrlShape')
        cmds.delete('RIG_rt_wrist_fk_ctrlShapeOLD')
        cmds.setAttr("RIG_rt_wrist_fk_ctrl.secondaryFkCtrlVis", 0)
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_wrist_fk_ctrl" + "\n");
        cmds.select( cl=True )

    # ////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
    # Ears
    try:
        cmds.parent('L_ear_root_CtrlShape', 'CNT_ear_root_L', r=True, s=True)
        cmds.delete('L_ear_root_Ctrl')
        cmds.rename('CNT_ear_root_LShape', 'CNT_ear_root_LShapeOLD')
        cmds.rename('L_ear_root_CtrlShape', 'CNT_ear_root_LShape')
        cmds.delete('CNT_ear_root_LShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_ear_root_L" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('L_ear_01_CtrlShape', 'CNT_ear_01_L', r=True, s=True)
        cmds.delete('L_ear_01_Ctrl')
        cmds.rename('CNT_ear_01_LShape', 'CNT_ear_01_LShapeOLD')
        cmds.rename('L_ear_01_CtrlShape', 'CNT_ear_01_LShape')
        cmds.delete('CNT_ear_01_LShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_ear_01_L" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('L_ear_02_CtrlShape', 'CNT_ear_02_L', r=True, s=True)
        cmds.delete('L_ear_02_Ctrl')
        cmds.rename('CNT_ear_02_LShape', 'CNT_ear_02_LShapeOLD')
        cmds.rename('L_ear_02_CtrlShape', 'CNT_ear_02_LShape')
        cmds.delete('CNT_ear_02_LShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_ear_02_L" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('L_ear_03_CtrlShape', 'CNT_ear_03_L', r=True, s=True)
        cmds.delete('L_ear_03_Ctrl')
        cmds.rename('CNT_ear_03_LShape', 'CNT_ear_03_LShapeOLD')
        cmds.rename('L_ear_03_CtrlShape', 'CNT_ear_03_LShape')
        cmds.delete('CNT_ear_03_LShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_ear_03_L" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('R_ear_root_CtrlShape', 'CNT_ear_root_R', r=True, s=True)
        cmds.delete('R_ear_root_Ctrl')
        cmds.rename('CNT_ear_root_RShape', 'CNT_ear_root_RShapeOLD')
        cmds.rename('R_ear_root_CtrlShape', 'CNT_ear_root_RShape')
        cmds.delete('CNT_ear_root_RShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_ear_root_R" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('R_ear_01_CtrlShape', 'CNT_ear_01_R', r=True, s=True)
        cmds.delete('R_ear_01_Ctrl')
        cmds.rename('CNT_ear_01_RShape', 'CNT_ear_01_RShapeOLD')
        cmds.rename('R_ear_01_CtrlShape', 'CNT_ear_01_RShape')
        cmds.delete('CNT_ear_01_RShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_ear_01_R" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('R_ear_02_CtrlShape', 'CNT_ear_02_R', r=True, s=True)
        cmds.delete('R_ear_02_Ctrl')
        cmds.rename('CNT_ear_02_RShape', 'CNT_ear_02_RShapeOLD')
        cmds.rename('R_ear_02_CtrlShape', 'CNT_ear_02_RShape')
        cmds.delete('CNT_ear_02_RShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_ear_02_R" + "\n");
        cmds.select( cl=True )
            
    try:
        cmds.parent('R_ear_03_CtrlShape', 'CNT_ear_03_R', r=True, s=True)
        cmds.delete('R_ear_03_Ctrl')
        cmds.rename('CNT_ear_03_RShape', 'CNT_ear_03_RShapeOLD')
        cmds.rename('R_ear_03_CtrlShape', 'CNT_ear_03_RShape')
        cmds.delete('CNT_ear_03_RShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_ear_03_R" + "\n");
        cmds.select( cl=True )

    # ////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
    # Fingers 

    # Fingers_Thumb_L
    try:
        cmds.parent('L_thumbBase_fk_CtrlShape', 'RIG_lf_thumbBase_fk_ctrl', r=True, s=True)
        cmds.delete('L_thumbBase_fk_Ctrl')
        cmds.rename('RIG_lf_thumbBase_fk_ctrlShape', 'RIG_lf_thumbBase_fk_ctrlShapeOLD')
        cmds.rename('L_thumbBase_fk_CtrlShape', 'RIG_lf_thumbBase_fk_ctrlShape')
        cmds.delete('RIG_lf_thumbBase_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_thumbBase_fk_ctrl" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('L_thumbMid_fk_CtrlShape', 'RIG_lf_thumbMid_fk_ctrl', r=True, s=True)
        cmds.delete('L_thumbMid_fk_Ctrl')
        cmds.rename('RIG_lf_thumbMid_fk_ctrlShape', 'RIG_lf_thumbMid_fk_ctrlShapeOLD')
        cmds.rename('L_thumbMid_fk_CtrlShape', 'RIG_lf_thumbMid_fk_ctrlShape')
        cmds.delete('RIG_lf_thumbMid_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_thumbMid_fk_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('L_thumbTip_fk_CtrlShape', 'RIG_lf_thumbTip_fk_ctrl', r=True, s=True)
        cmds.delete('L_thumbTip_fk_Ctrl')
        cmds.rename('RIG_lf_thumbTip_fk_ctrlShape', 'RIG_lf_thumbTip_fk_ctrlShapeOLD')
        cmds.rename('L_thumbTip_fk_CtrlShape', 'RIG_lf_thumbTip_fk_ctrlShape')
        cmds.delete('RIG_lf_thumbTip_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_thumbTip_fk_ctrl" + "\n");
        cmds.select( cl=True )

    # Fingers_Thumb_R
    try:
        cmds.parent('R_thumbBase_fk_CtrlShape', 'RIG_rt_thumbBase_fk_ctrl', r=True, s=True)
        cmds.delete('R_thumbBase_fk_Ctrl')
        cmds.rename('RIG_rt_thumbBase_fk_ctrlShape', 'RIG_rt_thumbBase_fk_ctrlShapeOLD')
        cmds.rename('R_thumbBase_fk_CtrlShape', 'RIG_rt_thumbBase_fk_ctrlShape')
        cmds.delete('RIG_rt_thumbBase_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_thumbBase_fk_ctrl" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('R_thumbMid_fk_CtrlShape', 'RIG_rt_thumbMid_fk_ctrl', r=True, s=True)
        cmds.delete('R_thumbMid_fk_Ctrl')
        cmds.rename('RIG_rt_thumbMid_fk_ctrlShape', 'RIG_rt_thumbMid_fk_ctrlShapeOLD')
        cmds.rename('R_thumbMid_fk_CtrlShape', 'RIG_rt_thumbMid_fk_ctrlShape')
        cmds.delete('RIG_rt_thumbMid_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_thumbMid_fk_ctrl" + "\n");
        cmds.select( cl=True )
        
    try:
        cmds.parent('R_thumbTip_fk_CtrlShape', 'RIG_rt_thumbTip_fk_ctrl', r=True, s=True)
        cmds.delete('R_thumbTip_fk_Ctrl')
        cmds.rename('RIG_rt_thumbTip_fk_ctrlShape', 'RIG_rt_thumbTip_fk_ctrlShapeOLD')
        cmds.rename('R_thumbTip_fk_CtrlShape', 'RIG_rt_thumbTip_fk_ctrlShape')
        cmds.delete('RIG_rt_thumbTip_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_thumbTip_fk_ctrl" + "\n");
        cmds.select( cl=True )

    # Fingers_Index_L
    try:
        cmds.parent('L_indexTop_fk_CtrlShape', 'RIG_lf_indexTop_fk_ctrl', r=True, s=True)
        cmds.delete('L_indexTop_fk_Ctrl')
        cmds.rename('RIG_lf_indexTop_fk_ctrlShape', 'RIG_lf_indexTop_fk_ctrlShapeOLD')
        cmds.rename('L_indexTop_fk_CtrlShape', 'RIG_lf_indexTop_fk_ctrlShape')
        cmds.delete('RIG_lf_indexTop_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_indexTop_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    try:
        cmds.parent('L_indexBase_fk_CtrlShape', 'RIG_lf_indexBase_fk_ctrl', r=True, s=True)
        cmds.delete('L_indexBase_fk_Ctrl')
        cmds.rename('RIG_lf_indexBase_fk_ctrlShape', 'RIG_lf_indexBase_fk_ctrlShapeOLD')
        cmds.rename('L_indexBase_fk_CtrlShape', 'RIG_lf_indexBase_fk_ctrlShape')
        cmds.delete('RIG_lf_indexBase_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_indexBase_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    try:
        cmds.parent('L_indexMid_fk_CtrlShape', 'RIG_lf_indexMid_fk_ctrl', r=True, s=True)
        cmds.delete('L_indexMid_fk_Ctrl')
        cmds.rename('RIG_lf_indexMid_fk_ctrlShape', 'RIG_lf_indexMid_fk_ctrlShapeOLD')
        cmds.rename('L_indexMid_fk_CtrlShape', 'RIG_lf_indexMid_fk_ctrlShape')
        cmds.delete('RIG_lf_indexMid_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_indexMid_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    try:
        cmds.parent('L_indexTip_fk_CtrlShape', 'RIG_lf_indexTip_fk_ctrl', r=True, s=True)
        cmds.delete('L_indexTip_fk_Ctrl')
        cmds.rename('RIG_lf_indexTip_fk_ctrlShape', 'RIG_lf_indexTip_fk_ctrlShapeOLD')
        cmds.rename('L_indexTip_fk_CtrlShape', 'RIG_lf_indexTip_fk_ctrlShape')
        cmds.delete('RIG_lf_indexTip_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_indexTip_fk_ctrl" + "\n");
        cmds.select( cl=True ) 
        
    # Fingers_Index_R
    try:
        cmds.parent('R_indexTop_fk_CtrlShape', 'RIG_rt_indexTop_fk_ctrl', r=True, s=True)
        cmds.delete('R_indexTop_fk_Ctrl')
        cmds.rename('RIG_rt_indexTop_fk_ctrlShape', 'RIG_rt_indexTop_fk_ctrlShapeOLD')
        cmds.rename('R_indexTop_fk_CtrlShape', 'RIG_rt_indexTop_fk_ctrlShape')
        cmds.delete('RIG_rt_indexTop_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_indexTop_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    try:
        cmds.parent('R_indexBase_fk_CtrlShape', 'RIG_rt_indexBase_fk_ctrl', r=True, s=True)
        cmds.delete('R_indexBase_fk_Ctrl')
        cmds.rename('RIG_rt_indexBase_fk_ctrlShape', 'RIG_rt_indexBase_fk_ctrlShapeOLD')
        cmds.rename('R_indexBase_fk_CtrlShape', 'RIG_rt_indexBase_fk_ctrlShape')
        cmds.delete('RIG_rt_indexBase_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_indexBase_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    try:
        cmds.parent('R_indexMid_fk_CtrlShape', 'RIG_rt_indexMid_fk_ctrl', r=True, s=True)
        cmds.delete('R_indexMid_fk_Ctrl')
        cmds.rename('RIG_rt_indexMid_fk_ctrlShape', 'RIG_rt_indexMid_fk_ctrlShapeOLD')
        cmds.rename('R_indexMid_fk_CtrlShape', 'RIG_rt_indexMid_fk_ctrlShape')
        cmds.delete('RIG_rt_indexMid_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_indexMid_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    try:
        cmds.parent('R_indexTip_fk_CtrlShape', 'RIG_rt_indexTip_fk_ctrl', r=True, s=True)
        cmds.delete('R_indexTip_fk_Ctrl')
        cmds.rename('RIG_rt_indexTip_fk_ctrlShape', 'RIG_rt_indexTip_fk_ctrlShapeOLD')
        cmds.rename('R_indexTip_fk_CtrlShape', 'RIG_rt_indexTip_fk_ctrlShape')
        cmds.delete('RIG_rt_indexTip_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_indexTip_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    # Fingers_Middle_L
    try:
        cmds.parent('L_middleTop_fk_CtrlShape', 'RIG_lf_middleTop_fk_ctrl', r=True, s=True)
        cmds.delete('L_middleTop_fk_Ctrl')
        cmds.rename('RIG_lf_middleTop_fk_ctrlShape', 'RIG_lf_middleTop_fk_ctrlShapeOLD')
        cmds.rename('L_middleTop_fk_CtrlShape', 'RIG_lf_middleTop_fk_ctrlShape')
        cmds.delete('RIG_lf_middleTop_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_middleTop_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    try:
        cmds.parent('L_middleBase_fk_CtrlShape', 'RIG_lf_middleBase_fk_ctrl', r=True, s=True)
        cmds.delete('L_middleBase_fk_Ctrl')
        cmds.rename('RIG_lf_middleBase_fk_ctrlShape', 'RIG_lf_middleBase_fk_ctrlShapeOLD')
        cmds.rename('L_middleBase_fk_CtrlShape', 'RIG_lf_middleBase_fk_ctrlShape')
        cmds.delete('RIG_lf_middleBase_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_middleBase_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    try:
        cmds.parent('L_middleMid_fk_CtrlShape', 'RIG_lf_middleMid_fk_ctrl', r=True, s=True)
        cmds.delete('L_middleMid_fk_Ctrl')
        cmds.rename('RIG_lf_middleMid_fk_ctrlShape', 'RIG_lf_middleMid_fk_ctrlShapeOLD')
        cmds.rename('L_middleMid_fk_CtrlShape', 'RIG_lf_middleMid_fk_ctrlShape')
        cmds.delete('RIG_lf_middleMid_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_middleMid_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    try:
        cmds.parent('L_middleTip_fk_CtrlShape', 'RIG_lf_middleTip_fk_ctrl', r=True, s=True)
        cmds.delete('L_middleTip_fk_Ctrl')
        cmds.rename('RIG_lf_middleTip_fk_ctrlShape', 'RIG_lf_middleTip_fk_ctrlShapeOLD')
        cmds.rename('L_middleTip_fk_CtrlShape', 'RIG_lf_middleTip_fk_ctrlShape')
        cmds.delete('RIG_lf_middleTip_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_middleTip_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    # Fingers_Middle_R
    try:
        cmds.parent('R_middleTop_fk_CtrlShape', 'RIG_rt_middleTop_fk_ctrl', r=True, s=True)
        cmds.delete('R_middleTop_fk_Ctrl')
        cmds.rename('RIG_rt_middleTop_fk_ctrlShape', 'RIG_rt_middleTop_fk_ctrlShapeOLD')
        cmds.rename('R_middleTop_fk_CtrlShape', 'RIG_rt_middleTop_fk_ctrlShape')
        cmds.delete('RIG_rt_middleTop_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_middleTop_fk_ctrl" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('R_middleBase_fk_CtrlShape', 'RIG_rt_middleBase_fk_ctrl', r=True, s=True)
        cmds.delete('R_middleBase_fk_Ctrl')
        cmds.rename('RIG_rt_middleBase_fk_ctrlShape', 'RIG_rt_middleBase_fk_ctrlShapeOLD')
        cmds.rename('R_middleBase_fk_CtrlShape', 'RIG_rt_middleBase_fk_ctrlShape')
        cmds.delete('RIG_rt_middleBase_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_middleBase_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    try:
        cmds.parent('R_middleMid_fk_CtrlShape', 'RIG_rt_middleMid_fk_ctrl', r=True, s=True)
        cmds.delete('R_middleMid_fk_Ctrl')
        cmds.rename('RIG_rt_middleMid_fk_ctrlShape', 'RIG_rt_middleMid_fk_ctrlShapeOLD')
        cmds.rename('R_middleMid_fk_CtrlShape', 'RIG_rt_middleMid_fk_ctrlShape')
        cmds.delete('RIG_rt_middleMid_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_middleMid_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    try:
        cmds.parent('R_middleTip_fk_CtrlShape', 'RIG_rt_middleTip_fk_ctrl', r=True, s=True)
        cmds.delete('R_middleTip_fk_Ctrl')
        cmds.rename('RIG_rt_middleTip_fk_ctrlShape', 'RIG_rt_middleTip_fk_ctrlShapeOLD')
        cmds.rename('R_middleTip_fk_CtrlShape', 'RIG_rt_middleTip_fk_ctrlShape')
        cmds.delete('RIG_rt_middleTip_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_middleTip_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    # Fingers_Ring_L 
    try:
        cmds.parent('L_ringTop_fk_CtrlShape', 'RIG_lf_ringTop_fk_ctrl', r=True, s=True)
        cmds.delete('L_ringTop_fk_Ctrl')
        cmds.rename('RIG_lf_ringTop_fk_ctrlShape', 'RIG_lf_ringTop_fk_ctrlShapeOLD')
        cmds.rename('L_ringTop_fk_CtrlShape', 'RIG_lf_ringTop_fk_ctrlShape')
        cmds.delete('RIG_lf_ringTop_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_ringTop_fk_ctrl" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('L_ringBase_fk_CtrlShape', 'RIG_lf_ringBase_fk_ctrl', r=True, s=True)
        cmds.delete('L_ringBase_fk_Ctrl')
        cmds.rename('RIG_lf_ringBase_fk_ctrlShape', 'RIG_lf_ringBase_fk_ctrlShapeOLD')
        cmds.rename('L_ringBase_fk_CtrlShape', 'RIG_lf_ringBase_fk_ctrlShape')
        cmds.delete('RIG_lf_ringBase_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_ringBase_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    try:
        cmds.parent('L_ringMid_fk_CtrlShape', 'RIG_lf_ringMid_fk_ctrl', r=True, s=True)
        cmds.delete('L_ringMid_fk_Ctrl')
        cmds.rename('RIG_lf_ringMid_fk_ctrlShape', 'RIG_lf_ringMid_fk_ctrlShapeOLD')
        cmds.rename('L_ringMid_fk_CtrlShape', 'RIG_lf_ringMid_fk_ctrlShape')
        cmds.delete('RIG_lf_ringMid_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_ringMid_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    try:
        cmds.parent('L_ringTip_fk_CtrlShape', 'RIG_lf_ringTip_fk_ctrl', r=True, s=True)
        cmds.delete('L_ringTip_fk_Ctrl')
        cmds.rename('RIG_lf_ringTip_fk_ctrlShape', 'RIG_lf_ringTip_fk_ctrlShapeOLD')
        cmds.rename('L_ringTip_fk_CtrlShape', 'RIG_lf_ringTip_fk_ctrlShape')
        cmds.delete('RIG_lf_ringTip_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_ringTip_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    # Fingers_Ring_R 
    try:
        cmds.parent('R_ringTop_fk_CtrlShape', 'RIG_rt_ringTop_fk_ctrl', r=True, s=True)
        cmds.delete('R_ringTop_fk_Ctrl')
        cmds.rename('RIG_rt_ringTop_fk_ctrlShape', 'RIG_rt_ringTop_fk_ctrlShapeOLD')
        cmds.rename('R_ringTop_fk_CtrlShape', 'RIG_rt_ringTop_fk_ctrlShape')
        cmds.delete('RIG_rt_ringTop_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_ringTop_fk_ctrl" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('R_ringBase_fk_CtrlShape', 'RIG_rt_ringBase_fk_ctrl', r=True, s=True)
        cmds.delete('R_ringBase_fk_Ctrl')
        cmds.rename('RIG_rt_ringBase_fk_ctrlShape', 'RIG_rt_ringBase_fk_ctrlShapeOLD')
        cmds.rename('R_ringBase_fk_CtrlShape', 'RIG_rt_ringBase_fk_ctrlShape')
        cmds.delete('RIG_rt_ringBase_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_ringBase_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    try:
        cmds.parent('R_ringMid_fk_CtrlShape', 'RIG_rt_ringMid_fk_ctrl', r=True, s=True)
        cmds.delete('R_ringMid_fk_Ctrl')
        cmds.rename('RIG_rt_ringMid_fk_ctrlShape', 'RIG_rt_ringMid_fk_ctrlShapeOLD')
        cmds.rename('R_ringMid_fk_CtrlShape', 'RIG_rt_ringMid_fk_ctrlShape')
        cmds.delete('RIG_rt_ringMid_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_ringMid_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    try:
        cmds.parent('R_ringTip_fk_CtrlShape', 'RIG_rt_ringTip_fk_ctrl', r=True, s=True)
        cmds.delete('R_ringTip_fk_Ctrl')
        cmds.rename('RIG_rt_ringTip_fk_ctrlShape', 'RIG_rt_ringTip_fk_ctrlShapeOLD')
        cmds.rename('R_ringTip_fk_CtrlShape', 'RIG_rt_ringTip_fk_ctrlShape')
        cmds.delete('RIG_rt_ringTip_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_ringTip_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    # Fingers_Pinky_L   
    try:
        cmds.parent('L_pinkyTop_fk_CtrlShape', 'RIG_lf_pinkyTop_fk_ctrl', r=True, s=True)
        cmds.delete('L_pinkyTop_fk_Ctrl')
        cmds.rename('RIG_lf_pinkyTop_fk_ctrlShape', 'RIG_lf_pinkyTop_fk_ctrlShapeOLD')
        cmds.rename('L_pinkyTop_fk_CtrlShape', 'RIG_lf_pinkyTop_fk_ctrlShape')
        cmds.delete('RIG_lf_pinkyTop_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_pinkyTop_fk_ctrl" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('L_pinkyBase_fk_CtrlShape', 'RIG_lf_pinkyBase_fk_ctrl', r=True, s=True)
        cmds.delete('L_pinkyBase_fk_Ctrl')
        cmds.rename('RIG_lf_pinkyBase_fk_ctrlShape', 'RIG_lf_pinkyBase_fk_ctrlShapeOLD')
        cmds.rename('L_pinkyBase_fk_CtrlShape', 'RIG_lf_pinkyBase_fk_ctrlShape')
        cmds.delete('RIG_lf_pinkyBase_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_pinkyBase_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    try:
        cmds.parent('L_pinkyMid_fk_CtrlShape', 'RIG_lf_pinkyMid_fk_ctrl', r=True, s=True)
        cmds.delete('L_pinkyMid_fk_Ctrl')
        cmds.rename('RIG_lf_pinkyMid_fk_ctrlShape', 'RIG_lf_pinkyMid_fk_ctrlShapeOLD')
        cmds.rename('L_pinkyMid_fk_CtrlShape', 'RIG_lf_pinkyMid_fk_ctrlShape')
        cmds.delete('RIG_lf_pinkyMid_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_pinkyMid_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    try:
        cmds.parent('L_pinkyTip_fk_CtrlShape', 'RIG_lf_pinkyTip_fk_ctrl', r=True, s=True)
        cmds.delete('L_pinkyTip_fk_Ctrl')
        cmds.rename('RIG_lf_pinkyTip_fk_ctrlShape', 'RIG_lf_pinkyTip_fk_ctrlShapeOLD')
        cmds.rename('L_pinkyTip_fk_CtrlShape', 'RIG_lf_pinkyTip_fk_ctrlShape')
        cmds.delete('RIG_lf_pinkyTip_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_pinkyTip_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    # Fingers_Pinky_R  
    try:
        cmds.parent('R_pinkyTop_fk_CtrlShape', 'RIG_rt_pinkyTop_fk_ctrl', r=True, s=True)
        cmds.delete('R_pinkyTop_fk_Ctrl')
        cmds.rename('RIG_rt_pinkyTop_fk_ctrlShape', 'RIG_rt_pinkyTop_fk_ctrlShapeOLD')
        cmds.rename('R_pinkyTop_fk_CtrlShape', 'RIG_rt_pinkyTop_fk_ctrlShape')
        cmds.delete('RIG_rt_pinkyTop_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_pinkyTop_fk_ctrl" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('R_pinkyBase_fk_CtrlShape', 'RIG_rt_pinkyBase_fk_ctrl', r=True, s=True)
        cmds.delete('R_pinkyBase_fk_Ctrl')
        cmds.rename('RIG_rt_pinkyBase_fk_ctrlShape', 'RIG_rt_pinkyBase_fk_ctrlShapeOLD')
        cmds.rename('R_pinkyBase_fk_CtrlShape', 'RIG_rt_pinkyBase_fk_ctrlShape')
        cmds.delete('RIG_rt_pinkyBase_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_pinkyBase_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    try:
        cmds.parent('R_pinkyMid_fk_CtrlShape', 'RIG_rt_pinkyMid_fk_ctrl', r=True, s=True)
        cmds.delete('R_pinkyMid_fk_Ctrl')
        cmds.rename('RIG_rt_pinkyMid_fk_ctrlShape', 'RIG_rt_pinkyMid_fk_ctrlShapeOLD')
        cmds.rename('R_pinkyMid_fk_CtrlShape', 'RIG_rt_pinkyMid_fk_ctrlShape')
        cmds.delete('RIG_rt_pinkyMid_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_pinkyMid_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    try:
        cmds.parent('R_pinkyTip_fk_CtrlShape', 'RIG_rt_pinkyTip_fk_ctrl', r=True, s=True)
        cmds.delete('R_pinkyTip_fk_Ctrl')
        cmds.rename('RIG_rt_pinkyTip_fk_ctrlShape', 'RIG_rt_pinkyTip_fk_ctrlShapeOLD')
        cmds.rename('R_pinkyTip_fk_CtrlShape', 'RIG_rt_pinkyTip_fk_ctrlShape')
        cmds.delete('RIG_rt_pinkyTip_fk_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_pinkyTip_fk_ctrl" + "\n");
        cmds.select( cl=True ) 

    # fingers foot
    try:
        cmds.parent('L_inner_toe_CtrlShape', 'CNT_inner_toe_L', r=True, s=True)
        cmds.delete('L_inner_toe_Ctrl')
        cmds.rename('CNT_inner_toe_LShape', 'CNT_inner_toe_LShapeOLD')
        cmds.rename('L_inner_toe_CtrlShape', 'CNT_inner_toe_LShape')
        cmds.delete('CNT_inner_toe_LShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_inner_toe_L" + "\n");
        cmds.select( cl=True ) 

    try:
        cmds.parent('L_outer_toe_CtrlShape', 'CNT_outer_toe_L', r=True, s=True)
        cmds.delete('L_outer_toe_Ctrl')
        cmds.rename('CNT_outer_toe_LShape', 'CNT_outer_toe_LShapeOLD')
        cmds.rename('L_outer_toe_CtrlShape', 'CNT_outer_toe_LShape')
        cmds.delete('CNT_outer_toe_LShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_outer_toe_L" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('L_center_toe_CtrlShape', 'CNT_center_toe_L', r=True, s=True)
        cmds.delete('L_center_toe_Ctrl')
        cmds.rename('CNT_center_toe_LShape', 'CNT_center_toe_LShapeOLD')
        cmds.rename('L_center_toe_CtrlShape', 'CNT_center_toe_LShape')
        cmds.delete('CNT_center_toe_LShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_center_toe_L" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('L_outer_toe_CtrlShape', 'CNT_outer_toe_L', r=True, s=True)
        cmds.delete('L_outer_toe_Ctrl')
        cmds.rename('CNT_outer_toe_LShape', 'CNT_outer_toe_LShapeOLD')
        cmds.rename('L_outer_toe_CtrlShape', 'CNT_outer_toe_LShape')
        cmds.delete('CNT_outer_toe_LShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_outer_toe_L" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('R_inner_toe_CtrlShape', 'CNT_inner_toe_R', r=True, s=True)
        cmds.delete('R_inner_toe_Ctrl')
        cmds.rename('CNT_inner_toe_RShape', 'CNT_inner_toe_RShapeOLD')
        cmds.rename('R_inner_toe_CtrlShape', 'CNT_inner_toe_RShape')
        cmds.delete('CNT_inner_toe_RShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_inner_toe_R" + "\n");
        cmds.select( cl=True ) 

    try:
        cmds.parent('R_outer_toe_CtrlShape', 'CNT_outer_toe_R', r=True, s=True)
        cmds.delete('R_outer_toe_Ctrl')
        cmds.rename('CNT_outer_toe_RShape', 'CNT_outer_toe_RShapeOLD')
        cmds.rename('R_outer_toe_CtrlShape', 'CNT_outer_toe_RShape')
        cmds.delete('CNT_outer_toe_RShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_outer_toe_R" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('R_outer_toe_CtrlShape', 'CNT_outer_toe_R', r=True, s=True)
        cmds.delete('R_outer_toe_Ctrl')
        cmds.rename('CNT_outer_toe_RShape', 'CNT_outer_toe_RShapeOLD')
        cmds.rename('R_outer_toe_CtrlShape', 'CNT_outer_toe_RShape')
        cmds.delete('CNT_outer_toe_RShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_outer_toe_R" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('R_center_toe_CtrlShape', 'CNT_center_toe_R', r=True, s=True)
        cmds.delete('R_center_toe_Ctrl')
        cmds.rename('CNT_center_toe_RShape', 'CNT_center_toe_RShapeOLD')
        cmds.rename('R_center_toe_CtrlShape', 'CNT_center_toe_RShape')
        cmds.delete('CNT_center_toe_RShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_center_toe_R" + "\n");
        cmds.select( cl=True )    
    # ////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
    # Jaw  
    try:
        cmds.parent('jaw_CtrlShape', 'CNT_jaw', r=True, s=True)
        cmds.delete('jaw_Ctrl')
        cmds.rename('CNT_jawShape', 'CNT_jawShapeOLD')
        cmds.rename('jaw_CtrlShape', 'CNT_jawShape')
        cmds.delete('CNT_jawShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_wrist_fk_ctrl" + "\n");
        cmds.select( cl=True )

    # ////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
    # Eyes
    try:
        cmds.parent('eyeMaster_CtrlShape', 'RIG_eyeMaster_ctrl', r=True, s=True)
        cmds.delete('eyeMaster_Ctrl')
        cmds.rename('RIG_eyeMaster_ctrlShape', 'RIG_eyeMaster_ctrlShapeOLD')
        cmds.rename('eyeMaster_CtrlShape', 'RIG_eyeMaster_ctrlShape')
        cmds.delete('RIG_eyeMaster_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_eyeMaster_ctrl" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('L_eye_CtrlShape', 'RIG_lf_eye_ctrl', r=True, s=True)
        cmds.delete('L_eye_Ctrl')
        cmds.rename('RIG_lf_eye_ctrlShape', 'RIG_lf_eye_ctrlShapeOLD')
        cmds.rename('L_eye_CtrlShape', 'RIG_lf_eye_ctrlShape')
        cmds.delete('RIG_lf_eye_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_lf_eye_ctrl" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('R_eye_CtrlShape', 'RIG_rt_eye_ctrl', r=True, s=True)
        cmds.delete('R_eye_Ctrl')
        cmds.rename('RIG_rt_eye_ctrlShape', 'RIG_rt_eye_ctrlShapeOLD')
        cmds.rename('R_eye_CtrlShape', 'RIG_rt_eye_ctrlShape')
        cmds.delete('RIG_rt_eye_ctrlShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "RIG_rt_eye_ctrl" + "\n");
        cmds.select( cl=True )    

    # ////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
    # Tail
    try:
        cmds.parent('tail_root_CtrlShape', 'CNT_tail_root', r=True, s=True)
        cmds.delete('tail_root_Ctrl')
        cmds.rename('CNT_tail_rootShape', 'CNT_tail_rootShapeOLD')
        cmds.rename('tail_root_CtrlShape', 'CNT_tail_rootShape')
        cmds.delete('CNT_tail_rootShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_tail_root" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('tail_FK_01_CtrlShape', 'CNT_tail_FK_01', r=True, s=True)
        cmds.delete('tail_FK_01_Ctrl')
        cmds.rename('CNT_tail_FK_01Shape', 'CNT_tail_FK_01ShapeOLD')
        cmds.rename('tail_FK_01_CtrlShape', 'CNT_tail_FK_01Shape')
        cmds.delete('CNT_tail_FK_01ShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_tail_FK_01" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('tail_IK_01_CtrlShape', 'CNT_tail_IK_01', r=True, s=True)
        cmds.delete('tail_IK_01_Ctrl')
        cmds.rename('CNT_tail_IK_01Shape', 'CNT_tail_IK_01ShapeOLD')
        cmds.rename('tail_IK_01_CtrlShape', 'CNT_tail_IK_01Shape')
        cmds.delete('CNT_tail_IK_01ShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_tail_IK_01" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('tail_FK_02_CtrlShape', 'CNT_tail_FK_02', r=True, s=True)
        cmds.delete('tail_FK_02_Ctrl')
        cmds.rename('CNT_tail_FK_02Shape', 'CNT_tail_FK_02ShapeOLD')
        cmds.rename('tail_FK_02_CtrlShape', 'CNT_tail_FK_02Shape')
        cmds.delete('CNT_tail_FK_02ShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_tail_FK_02" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('tail_IK_02_CtrlShape', 'CNT_tail_IK_02', r=True, s=True)
        cmds.delete('tail_IK_02_Ctrl')
        cmds.rename('CNT_tail_IK_02Shape', 'CNT_tail_IK_02ShapeOLD')
        cmds.rename('tail_IK_02_CtrlShape', 'CNT_tail_IK_02Shape')
        cmds.delete('CNT_tail_IK_02ShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_tail_IK_02" + "\n");
        cmds.select( cl=True )    

    try:
        cmds.parent('tail_FK_03_CtrlShape', 'CNT_tail_FK_03', r=True, s=True)
        cmds.delete('tail_FK_03_Ctrl')
        cmds.rename('CNT_tail_FK_03Shape', 'CNT_tail_FK_03ShapeOLD')
        cmds.rename('tail_FK_03_CtrlShape', 'CNT_tail_FK_03Shape')
        cmds.delete('CNT_tail_FK_03ShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_tail_FK_03" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('tail_IK_03_CtrlShape', 'CNT_tail_IK_03', r=True, s=True)
        cmds.delete('tail_IK_03_Ctrl')
        cmds.rename('CNT_tail_IK_03Shape', 'CNT_tail_IK_03ShapeOLD')
        cmds.rename('tail_IK_03_CtrlShape', 'CNT_tail_IK_03Shape')
        cmds.delete('CNT_tail_IK_03ShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_tail_IK_03" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('tail_FK_04_CtrlShape', 'CNT_tail_FK_04', r=True, s=True)
        cmds.delete('tail_FK_04_Ctrl')
        cmds.rename('CNT_tail_FK_04Shape', 'CNT_tail_FK_04ShapeOLD')
        cmds.rename('tail_FK_04_CtrlShape', 'CNT_tail_FK_04Shape')
        cmds.delete('CNT_tail_FK_04ShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_tail_FK_04" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('tail_IK_04_CtrlShape', 'CNT_tail_IK_04', r=True, s=True)
        cmds.delete('tail_IK_04_Ctrl')
        cmds.rename('CNT_tail_IK_04Shape', 'CNT_tail_IK_04ShapeOLD')
        cmds.rename('tail_IK_04_CtrlShape', 'CNT_tail_IK_04Shape')
        cmds.delete('CNT_tail_IK_04ShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_tail_IK_04" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('tail_FK_05_CtrlShape', 'CNT_tail_FK_05', r=True, s=True)
        cmds.delete('tail_FK_05_Ctrl')
        cmds.rename('CNT_tail_FK_05Shape', 'CNT_tail_FK_05ShapeOLD')
        cmds.rename('tail_FK_05_CtrlShape', 'CNT_tail_FK_05Shape')
        cmds.delete('CNT_tail_FK_05ShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_tail_FK_05" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('tail_IK_05_CtrlShape', 'CNT_tail_IK_05', r=True, s=True)
        cmds.delete('tail_IK_05_Ctrl')
        cmds.rename('CNT_tail_IK_05Shape', 'CNT_tail_IK_05ShapeOLD')
        cmds.rename('tail_IK_05_CtrlShape', 'CNT_tail_IK_05Shape')
        cmds.delete('CNT_tail_IK_05ShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_tail_IK_05" + "\n");
        cmds.select( cl=True )

    try:
        cmds.parent('tail_FK_06_CtrlShape', 'CNT_tail_FK_06', r=True, s=True)
        cmds.delete('tail_FK_06_Ctrl')
        cmds.rename('CNT_tail_FK_06Shape', 'CNT_tail_FK_06ShapeOLD')
        cmds.rename('tail_FK_06_CtrlShape', 'CNT_tail_FK_06Shape')
        cmds.delete('CNT_tail_FK_06ShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_tail_FK_06" + "\n")
        cmds.select( cl=True )

    try:
        cmds.parent('tail_IK_06_CtrlShape', 'CNT_tail_IK_06', r=True, s=True)
        cmds.delete('tail_IK_06_Ctrl')
        cmds.rename('CNT_tail_IK_06Shape', 'CNT_tail_IK_06ShapeOLD')
        cmds.rename('tail_IK_06_CtrlShape', 'CNT_tail_IK_06Shape')
        cmds.delete('CNT_tail_IK_06ShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_tail_IK_06" + "\n")
        cmds.select( cl=True )

    try:
        cmds.parent('tail_IK_07_CtrlShape', 'CNT_tail_IK_07', r=True, s=True)
        cmds.delete('tail_IK_07_Ctrl')
        cmds.rename('CNT_tail_IK_07Shape', 'CNT_tail_IK_07ShapeOLD')
        cmds.rename('tail_IK_07_CtrlShape', 'CNT_tail_IK_07Shape')
        cmds.delete('CNT_tail_IK_07ShapeOLD')
        cmds.select( cl=True )
    except:
        print("--- skipped " + "CNT_tail_IK_07" + "\n")
        cmds.select( cl=True )

    # ////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
    # shapes che rimangono scollegate
    try:
        cmds.setAttr('newShapes_Grp.v', 0)
        array = cmds.listRelatives('newShapes_Grp', ad=True, typ='transform' )
        print (array)
        print ("Il numero di Controlli non sostituiti e':")
        print (len(array))
    except:
        print("--- skipped " + "newShapes_Grp" + "\n")
        
    print("--- END SCRIPT ---" + "\n")
    cmds.undoInfo(closeChunk=True) 
# //////////////////////////////////////////////////////////////////////////////////////////////////////////////
# END
# //////////////////////////////////////////////////////////////////////////////////////////////////////////////