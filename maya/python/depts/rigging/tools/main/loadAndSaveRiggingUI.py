import importlib

import maya.cmds as cmd

import common.loadAndSaveAssetUI
import api.log
import api.widgets.rbw_UI as rbwUI

importlib.reload(common.loadAndSaveAssetUI)
importlib.reload(api.log)
# importlib.reload(rbwUI)

################################################################################
# @brief      This class describes a load and save rigging ui.
#
class LoadAndSaveRiggingUI(common.loadAndSaveAssetUI.AssetUI):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(LoadAndSaveRiggingUI, self).__init__('Rigging')

    ############################################################################
    # @brief      check the guides save node.
    #
    def checkGuidesNode(self):
        api.log.logger().debug('Check guides node')
        dynamicGuidesSaveNode = cmd.ls('*dynamicGuides_saveNode*')
        innerGuidesSaveNode = cmd.ls('*innerGuides_saveNode*')
        clusterGuidesSaveNode = cmd.ls('*clusterGuides_saveNode*')

        saveNodes = dynamicGuidesSaveNode + innerGuidesSaveNode + clusterGuidesSaveNode

        if len(saveNodes) == 0:
            return True
        else:
            if dynamicGuidesSaveNode:
                if len(dynamicGuidesSaveNode) > 1:
                    rbwUI.RBWError(title='ATTENTION', text='More than 1 dynamicGuide_saveNode in scene.\nPlease clean the scene and try again.')
                    return False
                else:
                    if int(cmd.getAttr('{}.asset_id'.format(dynamicGuidesSaveNode[0]))) != int(self.currentAsset.variantId):
                        rbwUI.RBWError(title='ATTENTION', text='The imported dynamic guides came from an other asset.\nPlease import the right ones and try again.')
                        return False

            if innerGuidesSaveNode:
                if len(innerGuidesSaveNode) > 1:
                    rbwUI.RBWError(title='ATTENTION', text='More than 1 innerGuide_saveNode in scene.\nPlease clean the scene and try again.')
                    return False
                else:
                    if int(cmd.getAttr('{}.asset_id'.format(innerGuidesSaveNode[0]))) != int(self.currentAsset.variantId):
                        rbwUI.RBWError(title='ATTENTION', text='The imported inner guides came from an other asset.\nPlease import the right ones and try again.')
                        return False

            if clusterGuidesSaveNode:
                if len(clusterGuidesSaveNode) > 1:
                    rbwUI.RBWError(title='ATTENTION', text='More than 1 clusterGuide_saveNode in scene.\nPlease clean the scene and try again.')
                    return False
                else:
                    if int(cmd.getAttr('{}.asset_id'.format(clusterGuidesSaveNode[0]))) != int(self.currentAsset.variantId):
                        rbwUI.RBWError(title='ATTENTION', text='The imported cluster guides came from an other asset.\nPlease import the right ones and try again.')
                        return False

        return True

    ############################################################################
    # @brief      check function
    #
    def checkToSave(self):
        super(LoadAndSaveRiggingUI, self).checkToSave()

        if self.currentDeptType != 'crowd':

            if not self.checkTextureExt():
                return

            if not self.checkTexturesPath():
                return

        if not self.checkGuidesNode():
            return

        if self.currentAsset.group not in ['proxy', 'viewtool']:
            if not self.checkMSHSet():
                return

        return True
