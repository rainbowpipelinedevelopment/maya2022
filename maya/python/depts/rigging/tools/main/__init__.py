import importlib
import os

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'arakno')


def run_loadAndSave(*args):
    import depts.rigging.tools.main.loadAndSaveRiggingUI as loadAndSaveRiggingUI
    importlib.reload(loadAndSaveRiggingUI)

    win = loadAndSaveRiggingUI.LoadAndSaveRiggingUI()
    win.show()


def run_loadAndSaveModeling(*args):
    import depts.modeling.tools.main.loadAndSaveModelingUI as loadAndSaveMod
    importlib.reload(loadAndSaveMod)

    win = loadAndSaveMod.LoadAndSaveModelingUI()
    win.show()


def run_loadAndSaveSurfacing(*args):
    import depts.surfacing.tools.main.loadAndSaveSurfacingUI as loadAndSaveSurf
    importlib.reload(loadAndSaveSurf)

    win = loadAndSaveSurf.LoadAndSaveSurfacingUI()
    win.show()


loadAndSaveTool = {
    "name": "L/S Rigging",
    "launch": run_loadAndSave,
    "icon": os.path.join(iconDir, "loadAndSaveRigging.png"),
    "statustip": "Load & Save Rigging"
}


loadAndSaveModelingTool = {
    "name": "L/S Modeling",
    "launch": run_loadAndSaveModeling,
    "icon": os.path.join(iconDir, "loadAndSaveModeling.png"),
    "statustip": "Load & Save Modeling"
}


loadAndSaveSurfacingTool = {
    "name": "L/S Surfacing",
    "launch": run_loadAndSaveSurfacing,
    "icon": os.path.join(iconDir, "loadAndSaveSurfacing.png"),
    "statustip": "Load & Save Surfacing"
}


tools = [
    loadAndSaveTool,
    loadAndSaveModelingTool,
    loadAndSaveSurfacingTool
]

PackageTool = {
    "name": "main",
    "tools": tools
}
