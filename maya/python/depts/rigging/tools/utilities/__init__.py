import os
import importlib

iconpath = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'rigging')


def run_tof(*args):
    import depts.rigging.tools.utilities.TransformOffset as tof
    importlib.reload(tof)
    tof.createTransformOffsetGroup()

tofTool = {
    "name": "Transform Offset Group",
    "launch": run_tof
}


def run_jov(*args):
    import depts.rigging.tools.utilities.JointOnVertex as jov
    importlib.reload(jov)
    jov.jointsOrientedOnVertexSelected()

jovTool = {
    "name": "Joint On Vertex",
    "launch": run_jov,
}


def run_pts(*args):
    import depts.rigging.tools.utilities.parent_to_surface as pts
    importlib.reload(pts)
    pts.parentToSurface()

ptsTool = {
    "name": "Parent To Surface",
    "launch": run_pts,
    "icon": os.path.join(iconpath, "parSurf.png"),
    "statustip": "Parent To Surface",
}


def run_rla(*args):
    import depts.rigging.tools.utilities.resetLocalAxis as rla
    importlib.reload(rla)
    rla.resetLocalAxisCmd()

rlaTool = {
    "name": "Reset Local Axis",
    "launch": run_rla,
    "icon": os.path.join(iconpath, "resetLocalAxis.png"),
    "statustip": "Reset Local Axis",
}


def run_rdo(*args):
    import depts.rigging.tools.utilities.resetDisplayOverrides as rdo
    importlib.reload(rdo)
    rdo.resetDisplayOverridesCmd()

rdoTool = {
    "name": "Reset Display Override",
    "launch": run_rdo,
    "icon": os.path.join(iconpath, "resetDisplayOverrides.png"),
    "statustip": "Reset Display Override"
}


def run_mlp(*args):
    import depts.rigging.tools.utilities.makeLocsPos as mlp
    importlib.reload(mlp)
    mlp.makeLocsPos()

mlpTool = {
    "name": "Make Locator Position",
    "launch": run_mlp,
    "icon": os.path.join(iconpath, "makeLocsPos.png"),
    "statustip": "Make Locator Position",
}


def run_dka(*args):
    import maya.mel as mel
    mel.eval("dkAnim();")

dkaTool = {
    "name": "dkAnimTool",
    "launch": run_dka,
    "icon": os.path.join(iconpath, "dkAnim.png"),
    "statustip": "DK Anim tool"
}


def run_ccr(*args):
    import depts.rigging.tools.utilities.CAC_import as ccr
    importlib.reload(ccr)
    ccr.CacImport()

ccrTool = {
    "name": "CAC Import",
    "launch": run_ccr,
    "icon": os.path.join(iconpath, "CAC_import.png"),
    "statustip": "CAC Import"
}



tools = [
    tofTool,
    jovTool,
    ptsTool,
    rlaTool,
    rdoTool,
    mlpTool,
    dkaTool,
    ccrTool,
]


PackageTool = {
    "name": "utilities",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
