#Crea un gruppo offset per azzerare i controlli
#USAGE: selezionare i controlli ed eseguire

import maya.cmds as cmds

def createTransformOffsetGroup():
	controls = cmds.ls(sl=True)
	for control in controls:
		parent_group = cmds.listRelatives(control,p=True)
		cmds.select(cl=True);
		offset_group = cmds.group( em = True, n="GRP_"+control+"_transform_offset")
		cmds.parent(offset_group,control)
		cmds.setAttr(offset_group+".tx",0)
		cmds.setAttr(offset_group+".ty",0)
		cmds.setAttr(offset_group+".tz",0)
		cmds.setAttr(offset_group+".rx",0)
		cmds.setAttr(offset_group+".ry",0)
		cmds.setAttr(offset_group+".rz",0)
		cmds.parent(offset_group, parent_group)
		cmds.parent(control,offset_group)
