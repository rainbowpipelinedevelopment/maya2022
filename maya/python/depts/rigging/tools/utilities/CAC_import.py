import importlib
import os

import maya.cmds as cmd

import api.log
import api.savedAsset
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.log)
importlib.reload(api.savedAsset)
# importlib.reload(rbwUI)


def CacImport():
    saveNode = 'rigging_saveNode'
    if cmd.objExists(saveNode):
        riggingSavedAsset = api.savedAsset.SavedAsset(saveNode=saveNode)

        groomingSavedAsset = api.savedAsset.SavedAsset(params={'asset': riggingSavedAsset.asset, 'dept': 'grooming', 'deptType': 'hrs', 'approvation': 'def'})
        latest = groomingSavedAsset.getLatest()

        if latest:
            groomingPath = os.path.join(os.getenv('MC_FOLDER'), latest[2]).replace('\\', '/')
            cacReaderPath = groomingPath.replace('.mb', '_CacReader.ma')

            if os.path.exists(cacReaderPath):
                cmd.file(cacReaderPath, i=True)
                rbwUI.RBWConfirm(title='Import Complete', text='Cac_Reader file import successfully.', defCancel=None)
            else:
                rbwUI.RBWError(text='No Cac_Reader file for the latest grooming save.')
        else:
            rbwUI.RBWError(text='No grooming save for the current asset.')
    else:
        rbwUI.RBWError(text='No rigging saveNode in scene.\nAre you using an official rigging asset?')
