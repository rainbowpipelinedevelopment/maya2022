__author__ = 'm.boldoni'

import maya.cmds as cmd

def resetDisplayOverridesCmd():
    """
    spegne tutti gli overrides reference attivi in scena

    import depts.rigging.tools.general.resetDisplayOverrides as rdo
    reload(rdo)
    rdo.resetDisplayOverridesCmd()

    @return
    """
    cmd.select(allDagObjects=1)
    cmd.select(hierarchy=1)
    sel = cmd.ls()
    for s in sel:
        try:
            if cmd.getAttr(s+".overrideEnabled"):
                cmd.setAttr(s+".overrideDisplayType", 0)
                print("overrideDisplayType to Normal on: ", s)
        except:
            pass
    cmd.select(cl=1)
    print('All the Overrides has been turned to normal. (no reference)')
