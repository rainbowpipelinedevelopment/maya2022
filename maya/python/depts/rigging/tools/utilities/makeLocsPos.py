'''
It create a locator for every object you have selected.
@author m.boldoni

'''
import maya.cmds as cmd
import maya.mel as mel

def makeLocsPos():
    jntChain = cmd.ls(sl=1)
    if len(jntChain) == 0 :
        print("----------------------------------------------------------------")
        print("WARNING: You must select at least one object to run this script!")
        print("----------------------------------------------------------------")
        cmd.confirmDialog(message = "You must select at least one object to run this script!", button=['OK'], title='Warning', icn = 'warning' )
    else:
        for i in range(0, len(jntChain), 1):
            jntCoordinate = cmd.xform (jntChain[i], q=True, ws=1, rp=1)
            locName = jntChain[i].replace("JNT", "LOC")
            cmd.spaceLocator( p=(jntCoordinate[0], jntCoordinate[1], jntCoordinate[2]), n= locName )
            mel.eval("CenterPivot;")
            cmd.select(cl=True)
        print("---------------------------")
        print("INFO: All Locs are created!")
        print("---------------------------")
        cmd.confirmDialog(message = "All Locs are created!", button=['OK'], title='Info', icn = 'information')
