"""
@author: Dino Mascitelli

THIS SCRIPT IS A PYTHON CONVERTED VERSION OF "parentToSurface.mel"
IT CONNECTS SELECTED ITEMS TO A SELECTED SURFACE OR MESH BY FOLLICLE NODES 
"""

import maya.cmds as cmd
import maya.mel as mel



def __convertToCmFactor():
	unit=cmd.currentUnit(q=True, linear=True) 
	if unit == "mm":
		return(0.1)
	elif unit == "cm":
		return(1)
	elif unit == "m":
		return(100)
	elif unit == "in":
		return(2.54)
	elif unit == "ft":
		return(30.48)
	elif unit == "yd":
		return(91.44)
	else:
		return(1.0)

def __attachObjectToSurface(obj, surface, u, v):
	follicle=cmd.createNode ("follicle")
	tforms=mel.eval("listTransforms %s" %(follicle))
	follicleDag=tforms[0]
	cmd.connectAttr (surface + ".worldMatrix[0]", follicle + ".inputWorldMatrix")
	nType = cmd.nodeType (surface)
	if nType == "nurbsSurface":	
		cmd.connectAttr (surface + ".local", follicle + ".inputSurface")
	else:
		cmd.connectAttr (surface + ".outMesh", follicle + ".inputMesh")

	cmd.connectAttr (follicle + ".outTranslate", follicleDag + ".translate")
	cmd.connectAttr (follicle + ".outRotate", follicleDag + ".rotate")
	cmd.setAttr (follicleDag + ".translate", lock=True)
	cmd.setAttr (follicleDag + ".rotate", lock=True)
	cmd.setAttr (follicle + ".parameterU", u);
	cmd.setAttr (follicle + ".parameterV", v);
	cmd.parent (obj, follicleDag)
	
def parentToSurface():
	'''
	Select first items you want to attach to a surface or a mesh
	and then select the driver surface or mesh.
	'''
	shapes=[]
	sl = cmd.ls (sl=True)
	numSel = len(sl)
	if numSel < 2:
		print("ParentToSurface: select object(s) to parent followed by a mesh or nurbsSurface to attach to.")
		return
	surface = sl[numSel-1]
	if cmd.nodeType (surface) == "transform":
		shapes = cmd.ls (surface, dag=True, s=True, ni=True, v=True)
	if len(shapes)>0:
		surface = shapes[0]
	nType = cmd.nodeType (surface)
	if nType != "mesh" and nType != "nurbsSurface":
		print("ParentToSurface: Last selected item must be a mesh or nurbsSurface.")
		return

	clPos = ""
	convertFac = 1.0

	if nType == "nurbsSurface":
		clPos = cmd.createNode ("closestPointOnSurface")
		cmd.connectAttr (surface + ".worldSpace[0]", clPos + ".inputSurface")
		minU = cmd.getAttr (surface+".mnu")
		maxU = cmd.getAttr (surface+".mxu")
		sizeU = maxU - minU
		minV = cmd.getAttr (surface+".mnv")
		maxV = cmd.getAttr (surface+".mxv");
		sizeV = maxV - minV
	else:
		pomLoaded = cmd.pluginInfo ("nearestPointOnMesh", query=True, l=True)
		if pomLoaded == 0:
			cmd.loadPlugin ("nearestPointOnMesh")
			pomLoaded = cmd.pluginInfo ("nearestPointOnMesh", query=True, l=True)
			if pomLoaded == 0:
				print("ParentToSurface: Can't load nearestPointOnMesh plugin.")
				return
		convertFac = __convertToCmFactor()
		clPos = cmd.createNode ("nearestPointOnMesh")
		cmd.connectAttr (surface + ".worldMesh", clPos + ".inMesh")
	for i in range (0, numSel-1, 1):
		obj = sl[i]
		if cmd.nodeType(obj) != "transform":
			print("ParentToSurface: select the transform of the node(s) to constrain\n")
			pass
		bbox = cmd.xform (obj, q=True, ws=True, bb=True)
		pos=[]
		pos.append ((bbox[0] + bbox[3])*0.5)
		pos.append ((bbox[1] + bbox[4])*0.5)
		pos.append ((bbox[2] + bbox[5])*0.5)
		
		cmd.setAttr (clPos + ".inPosition", pos[0]*convertFac, pos[1]*convertFac, pos[2]*convertFac, type="double3")
		closestU = cmd.getAttr (clPos + ".parameterU")
		closestV = cmd.getAttr (clPos + ".parameterV")
		if nType == "nurbsSurface":
			closestU = (closestU + minU)/sizeU
			closestV = (closestV + minV)/sizeV
		__attachObjectToSurface (obj, surface, closestU, closestV)

	if  clPos != "":
		cmd.delete (clPos)
