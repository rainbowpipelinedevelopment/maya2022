__author__ = 'm.boldoni'

import maya.cmds as cmd

def resetLocalAxisCmd():
    """
    elimina tutte le local axis display attive in scena

    import depts.rigging.tools.general.resetLocalAxis as rla
    reload(rla)
    rla.resetLocalAxisCmd()

    @return
    """
    cmd.select(allDagObjects=1)
    cmd.select(hierarchy=1)
    sel=cmd.ls()
    for s in sel:
        try:
            inf=cmd.getAttr(s+".displayLocalAxis")
            if inf == 1:
                print("Active Local Axis On:", s)
            cmd.setAttr(s+".displayLocalAxis", 0)

        except:
            pass
    cmd.select(cl=1)
    print('All the local rotation axis has been turned off.')
