#Crea joint orientati secondo le normali su un insieme di vertici (asse x positivo lungo la normale)
#USAGE: Select the vertexes where you want to position your joints and run this script

from maya import cmds , OpenMaya


def jointsOrientedOnVertexSelected():
	vtx = cmds.ls(sl = 1, fl = 1)

	for v in vtx :
	    pos = cmds.xform(v, q = 1, ws = 1, t = 1)
	    jnt = cmds.joint(p = pos)
	    constr = cmds.normalConstraint(v, jnt, aimVector = (1,0,0),  worldUpType= 0)
	    cmds.delete(constr)
	    cmds.makeIdentity(jnt, apply=True, t=1, r=1, s=1, n=0)
	    cmds.parent(w = True)
	    
jointsOrientedOnVertexSelected()
