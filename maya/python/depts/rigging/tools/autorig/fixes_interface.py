import maya.cmds as cmd
import importlib
from maya import OpenMayaUI as omui

def _rbwAutorigFixedCmd(*args):
    import depts.rigging.tools.autorig.modules.rbw_autorig_fixed as rbwfix
    importlib.reload( rbwfix )
    rbwfix.rbwAutorigFixed()

def _rbwFixedSpineCmd(*args):
    import depts.rigging.tools.autorig.modules.fix_spine_autorig as rbwfixspine
    importlib.reload( rbwfixspine )
    rbwfixspine.rbwFixedSpine()

def _rbwFixedSpineQuadCmd(*args):
    import depts.rigging.tools.autorig.modules.fix_spine_autorig_quadruped as rbwfixspinequad
    importlib.reload( rbwfixspinequad )
    rbwfixspinequad.rbwFixedSpineQuad()
    
def _rbwFixedSpineArmsCmd(*args):
    import depts.rigging.tools.autorig.modules.fix_spine_arms_autorig as rbwfixspinearms
    importlib.reload( rbwfixspinearms )
    rbwfixspinearms.rbwFixedSpineArms()

def _rbwFixedArmsCmd(*args):
    import depts.rigging.tools.autorig.modules.fix_arms_autorig as rbwfixarms
    importlib.reload( rbwfixarms )
    rbwfixarms.rbwFixedArms()

def _rbwFixedLegsCmd(*args):
    import depts.rigging.tools.autorig.modules.fix_legs_autorig as rbwfixlegs
    importlib.reload( rbwfixlegs )
    rbwfixlegs.rbwFixedLegs()

def _attachQuadLegsCmd(*args):
    import depts.rigging.tools.autorig.modules.attach_quadrupeds_legs_rig_rbw as attquadlegs
    importlib.reload( attquadlegs )
    attquadlegs.main()
    
def _attachRearQuadLegsCmd(*args):
    import depts.rigging.tools.autorig.modules.attach_rear_quadrupeds_legs_rig as attrearquadlegs
    importlib.reload( attrearquadlegs )
    attrearquadlegs.main()

def _bendyFixBipedCmd(*args):
    import depts.rigging.tools.autorig.modules.bendy_fix_biped as bfb
    importlib.reload(bfb)
    bfb.main()

def _bendyFixQuadCmd(*args):
    import depts.rigging.tools.autorig.modules.bendy_fix_quad as bfq
    importlib.reload(bfq)
    bfq.main()

def fixesWindow():
    if (cmd.window("fixes_ui", q=True,exists=True)):
        cmd.deleteUI("fixes_ui", window=True)
    W=cmd.window("fixes_ui",title="BODY FIXES", s=True, ip=True, rtf=True)
    RC1=cmd.rowColumnLayout( numberOfColumns=3, p=W, columnAttach=[(1,'both',5),(2,'both',5),(3,'both',5)])
    
    C1=cmd.columnLayout(p=RC1,adj=True)
    cmd.text("BIPED:", al="center",w=230,h=30)
    cmd.button(l="All Body Fix", c=_rbwAutorigFixedCmd, w=150,h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="Spine Fix", c=_rbwFixedSpineCmd, w=150,h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="Arms Fix", c=_rbwFixedArmsCmd, w=150,h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="Legs Fix", c=_rbwFixedLegsCmd, w=150,h=30, bgc=[0.3, 0.3, 0.3])
    
    C2=cmd.columnLayout(p=RC1,adj=True)
    cmd.text("QUADRUPED:", al="center",w=230,h=30)
    cmd.button(l="Spine Quad Fix", c=_rbwFixedSpineQuadCmd, w=150,h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="Attach Quad Legs", c=_attachQuadLegsCmd, w=150,h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="Bendy Fix Quad", c=_bendyFixQuadCmd, w=150,h=30, bgc=[0.3, 0.3, 0.3])
    
    C3=cmd.columnLayout(p=RC1,adj=True)
    cmd.text("BIPED - QUAD LEGS:", al="center",w=230,h=30)
    cmd.button(l="Spine Arm Fix", c=_rbwFixedSpineArmsCmd, w=150,h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="Attach Rear Legs", c=_attachRearQuadLegsCmd, w=150,h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="Bendy Fix", c=_bendyFixBipedCmd, w=150,h=30, bgc=[0.3, 0.3, 0.3])
    
    cmd.showWindow( W )
    return