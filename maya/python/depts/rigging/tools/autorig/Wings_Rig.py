import maya.cmds as cmds
import pymel.core as pmc
import math


def SelectionNurbs():
    sel = cmds.ls(sl=True)
    if len(sel) > 0:
        cmds.textField('input_Nurbs', edit = True, text = sel[0])
        cmds.select(cl=True)
    else: 
        cmds.textField('input_Nurbs', edit = True)
        cmds.warning('Nothing is selected.')

def SelectionMesh():
    sel = cmds.ls(sl=True)
    if len(sel) > 0:
        cmds.textField('input_Mesh', edit = True, text = sel[0])
        cmds.select(cl=True)
    else: 
        cmds.textField('input_Mesh', edit = True)
        cmds.warning('Nothing is selected.')

#FK CHAIN

def FeatherSetup():
    
    global IdName

    #VARIABLES

    Label = cmds.optionMenu('Number_Of_Feather', q=True, v=True)
    
    #Check the value of the checkbox for the side name
    input_Left = cmds.checkBox('Left_Side', q=True, v=True)
    input_Right = cmds.checkBox('Right_Side', q=True, v=True)

    if input_Left == 1:
        IdName = 'L_'
    if input_Right == 1:
        IdName = 'R_'
    if input_Left == 0 and input_Right == 0 or input_Left == 1 and input_Right == 1:
        cmds.warning('!Hai selezionato entrambi i side o nessuno dei due!')
        return False

    #Check selection exists
    planeUM = cmds.textField('input_Mesh', q=True, text=True)
    if planeUM == '':
        cmds.warning('!Manca la mesh del pianetto!')
        return False

    NurbsUM = cmds.textField('input_Nurbs', q=True, text=True)
    if NurbsUM == '':
        cmds.warning('!Manca la Nurbs per lo spread!')
        return False
    
    
    #Create FK joint chain on selected locator
    loc = cmds.ls(sl=True)
    lenLoc = len(loc)
    if lenLoc == 0:
        cmds.warning('!Seleziona dei locator, thanks!')
        return False
    
    #Feather or fingers
    if lenLoc == 3:
        Feather = 'Feather_fingers_'
    else:
        Feather =  'Feather_'
    ctrl_list = []
    jnt_list = []
    posLoc = [cmds.pointPosition(i) for i in loc]

    #Create Master Group
    if cmds.objExists('GRP_{}WIng_RIG'.format(IdName)):
        print ('')
    else:
        GrpWing = cmds.group(w=True, em=True, n='GRP_{}WIng_RIG'.format(IdName))
    
    cmds.select(cl=True)
    
    i=-1
    for index in range(0, lenLoc):
        jnt = cmds.joint(p=posLoc[index], n='{}{}{}_0{}_jnt'.format(IdName, Feather, Label, str(i)))
        i+=1
        jnt_list.append(jnt)
        cmds.select(cl=True)
        if index == 0:
            continue
        if input_Left == 1:
            aim_const = cmds.aimConstraint(jnt,jnt_list[index-1],aim=[1,0,0],mo=False,u=[0,0,1],wut='objectrotation',wu=[0,0,1],wuo=loc[index-1])
        if input_Right == 1:
            aim_const = cmds.aimConstraint(jnt,jnt_list[index-1],aim=[-1,0,0],mo=False,u=[0,0,-1],wut='objectrotation',wu=[0,0,1],wuo=loc[index-1]) 
        cmds.delete(aim_const)
        cmds.makeIdentity(jnt, a=True, r=True)
        cmds.setAttr('{}.segmentScaleCompensate'.format(str(jnt_list[index])), False)
        cmds.parent(jnt, jnt_list[index-1])
        cmds.select(cl=True)
    
    #Group Fk Joint
    
    if cmds.objExists('{}feathers_joints_Grp'.format(IdName)):
        cmds.parent(jnt_list[0], '{}feathers_joints_Grp'.format(IdName))
    else:
        GRPJnt = cmds.group(em=True, n='{}feathers_joints_Grp'.format(IdName), p='GRP_{}WIng_RIG'.format(IdName))
        cmds.parent(jnt_list[0], GRPJnt)
    
    #create FK controls
    for index in range(0, lenLoc-1):
        if index == 0:
            ctrl = cmds.circle(ch=False, nr=[1, 0, 0], n='{}{}{}_Root_Ctrl'.format(IdName, Feather, Label))
            cmds.setAttr(str(ctrl[0])+'.visibility',l=True,k=False,cb=False)
            cmds.setAttr(str(ctrl[0]) + '.overrideEnabled', 1)
            cmds.setAttr(str(ctrl[0]) + '.overrideColor', 6)
            ctrl_list.append(ctrl[0])          
            grp_parent_01 = cmds.group(ctrl[0],w=True,n='{}_Grp_animation_off2'.format(str(ctrl[0])))
            grp_parent_02 = cmds.group(grp_parent_01,w=True,n='{}_Grp_animation_off'.format(str(ctrl[0])))
            cmds.setAttr('{}.rotateOrder'.format(grp_parent_02), 5)
            grp_parent_03 = cmds.group(grp_parent_02,w=True,n='{}_Grp_close_wing'.format(str(ctrl[0])))
            grp_parent_04 = cmds.group(grp_parent_03,w=True,n='{}_Grp_offset'.format(str(ctrl[0])))
            grp_parent_05 = cmds.group(grp_parent_04,w=True,n='{}_Grp_spread'.format(str(ctrl[0])))
            grp_parent_06 = cmds.group(grp_parent_05,w=True,n='{}_Grp'.format(str(ctrl[0])))
            cmds.matchTransform(grp_parent_06,jnt_list[index])
        elif index == 1:
            ctrlBase = cmds.circle(ch=False, nr=[1, 0, 0], n='{}{}{}_Base_Ctrl'.format(IdName, Feather, Label))
            cmds.setAttr('{}.visibility'.format(str(ctrlBase[0])),l=True,k=False,cb=False)
            cmds.setAttr('{}.overrideEnabled'.format(str(ctrlBase[0])), 1)
            cmds.setAttr('{}.overrideColor'.format(str(ctrlBase[0])), 6)
            ctrl_list.append(ctrlBase[0])          
            grpBase_parent_01 = cmds.group(ctrlBase[0],w=True,n='{}_Grp_animation_off2'.format(str(ctrlBase[0])))
            grpBase_parent_02 = cmds.group(grpBase_parent_01,w=True,n='{}_Grp_animation_off'.format(str(ctrlBase[0])))
            cmds.setAttr('{}.rotateOrder'.format(grpBase_parent_02), 5)
            grpBase_parent_03 = cmds.group(grpBase_parent_02,w=True,n='{}_Grp_close_wing'.format(str(ctrlBase[0])))
            grpBase_parent_04 = cmds.group(grpBase_parent_03,w=True,n='{}_Grp_offset'.format(str(ctrlBase[0])))
            grpBase_parent_05 = cmds.group(grpBase_parent_04,w=True,n='{}_Grp'.format(str(ctrlBase[0])))
            cmds.matchTransform(grpBase_parent_05,jnt_list[index])
            cmds.parent(grpBase_parent_05,ctrl_list[index-1])
        else:
            copy_ctrl = cmds.duplicate(ctrl_list[index-1], n='{}{}{}_0{}_Ctrl'.format(IdName, Feather, Label, str(index-1)))                    
            ctrl_list.append(copy_ctrl[0])
            copy_parent_01 = cmds.group(copy_ctrl[0],w=False,n='{}_Grp_animation_off2'.format(str(copy_ctrl[0])))
            copy_parent_02 = cmds.group(copy_parent_01,w=False,n='{}_Grp_animation_off'.format(str(copy_ctrl[0])))
            cmds.setAttr('{}.rotateOrder'.format(copy_parent_02), 5)
            copy_parent_03 = cmds.group(copy_parent_02,w=False,n='{}_Grp_close_wing'.format(str(copy_ctrl[0])))
            copy_parent_04 = cmds.group(copy_parent_03,w=False,n='{}_Grp_offset'.format(str(copy_ctrl[0])))
            copy_parent_05 = cmds.group(copy_parent_04,w=False,n='{}_Grp'.format(str(copy_ctrl[0])))
            cmds.matchTransform(copy_parent_05,jnt_list[index])
            cmds.parent(copy_parent_05,ctrl_list[index-1])
        cmds.parentConstraint(ctrl_list[index],jnt_list[index],mo=True)
        cmds.scaleConstraint(ctrl_list[index],jnt_list[index],mo=True)
    
    #Group Fk controls
    if cmds.objExists('{}feathers_controls_Grp'.format(IdName)):
        cmds.parent(grp_parent_06, '{}feathers_controls_Grp'.format(IdName))
    else:
        GRPCtrl = cmds.group(em=True, n='{}feathers_controls_Grp'.format(IdName), p='GRP_{}WIng_RIG'.format(IdName))
        cmds.parent(grp_parent_06, GRPCtrl)
    
    #rename first 2 joint
    cmds.rename(jnt_list[0], '{}{}{}_Root_jnt'.format(IdName, Feather, Label))
    cmds.rename(jnt_list[1], '{}{}{}_Base_jnt'.format(IdName, Feather, Label))
    cmds.select(cl=True)
    
    #IKjoints
    IK_loc = cmds.ls(loc[0], loc[-1])
    jntIK_list = []
    IK_locPos = [cmds.pointPosition(i) for i in IK_loc]
        
    for index in range(0, 2):
        jntIK = cmds.joint(p=IK_locPos[index], n='{}{}IK_{}_jnt'.format(IdName, Feather, Label))
        jntIK_list.append(jntIK)
        if index == 1:
            cmds.rename(jntIK, '{}{}IK_{}_end_jnt'.format(IdName, Feather, Label))
            continue
        if input_Left == 1:
            aim_constIK = cmds.aimConstraint(IK_loc[index-1],jntIK,aim=[1,0,0],mo=False,u=[0,0,1],wut='objectrotation',wu=[0,0,1],wuo=IK_loc[index-1])
        if input_Right == 1:
            aim_constIK = cmds.aimConstraint(IK_loc[index-1],jntIK,aim=[-1,0,0],mo=False,u=[0,0,-1],wut='objectrotation',wu=[0,0,1],wuo=IK_loc[index-1])
        cmds.delete(aim_constIK)
        cmds.makeIdentity(jntIK_list[0], a=True, r=True)
    cmds.pickWalk(d='up')
    Grp_ikJnt = cmds.group(n='{}{}IK_{}_Grp'.format(IdName, Feather, Label), p='GRP_{}WIng_RIG'.format(IdName))
    cmds.select(cl=True)
    IKH = cmds.ikHandle(sj='{}{}IK_{}_jnt'.format(IdName, Feather, Label), ee='{}{}IK_{}_end_jnt'.format(IdName, Feather, Label), solver='ikSCsolver', n='{}{}{}_IKH'.format(IdName, Feather, Label))
    cmds.parentConstraint('{}{}IK_{}_jnt'.format(IdName, Feather, Label), '{}{}{}_Root_Ctrl_Grp_spread'.format(IdName, Feather, Label), mo=True, st=['x', 'y', 'z'], sr=['x'] )
    
    #Group ik joints
    if cmds.objExists('{}feathers_ik_joints_Grp'.format(IdName)):
        cmds.parent(Grp_ikJnt, '{}feathers_ik_joints_Grp'.format(IdName))
    else:
        GRPJntIK = cmds.group(em=True, n='{}feathers_ik_joints_Grp'.format(IdName), p='GRP_{}WIng_RIG'.format(IdName))
        cmds.parent(Grp_ikJnt, GRPJntIK)
    
    
    #CreateFollicleMesh
    cmds.select(loc)
    cmds.pickWalk(d='down')
    shapeLoc = cmds.ls(sl=True)
    cmds.select(cl=True)
            
    flcShape = cmds.createNode('follicle')
    cmds.pickWalk(d='up')
    cmds.rename('{}{}{}_Root_flc'.format(IdName, Feather, Label))
    flc = cmds.ls(sl=True)
    cmds.connectAttr('{}Shape.outMesh'.format(planeUM), '{}{}{}_Root_flcShape.inputMesh'.format(IdName, Feather, Label))
    cmds.connectAttr('{}Shape.worldMatrix[0]'.format(planeUM), '{}{}{}_Root_flc.inputWorldMatrix'.format(IdName, Feather, Label))
    cmds.connectAttr('{}{}{}_Root_flcShape.outRotate'.format(IdName, Feather, Label), '{}{}{}_Root_flc.rotate'.format(IdName, Feather, Label))
    cmds.connectAttr('{}{}{}_Root_flcShape.outTranslate'.format(IdName, Feather, Label),'{}{}{}_Root_flc.translate'.format(IdName, Feather, Label))
    
    cmds.scaleConstraint('Transform_Null', flc, mo=True)

    #create ClosestPointOnMesh for UV parameter
    CPOM = cmds.shadingNode('closestPointOnMesh', asUtility=True)
    cmds.connectAttr('{}Shape.worldMesh[0]'.format(planeUM), '{}.inMesh'.format(CPOM))
    cmds.connectAttr('{}.worldPosition[0]'.format(shapeLoc[0]), '{}.inPosition'.format(CPOM))
    cmds.connectAttr('{}.parameterU'.format(CPOM),'{}{}{}_Root_flcShape.parameterU'.format(IdName, Feather, Label))
    cmds.connectAttr('{}.parameterV'.format(CPOM),'{}{}{}_Root_flcShape.parameterV'.format(IdName, Feather, Label))
    cmds.select(cl=True)
    cmds.parentConstraint(flc, Grp_ikJnt, mo=True)
    cmds.scaleConstraint(flc, Grp_ikJnt, mo=True)
    cmds.delete(CPOM)
    if Feather == 'feather_fingers_':
        print (' ')
    else:
        cmds.parentConstraint(flc, '{}{}{}_Root_Ctrl_Grp'.format(IdName, Feather, Label), mo=True, sr=['x', 'y', 'z'])
        cmds.scaleConstraint(flc, '{}{}{}_Root_Ctrl_Grp'.format(IdName, Feather, Label), mo=True)
    
    cmds.select(cl=True)

    #Group follicle Mesh
    if cmds.objExists('{}feathers_follicles_Grp'.format(IdName)):
        cmds.parent(flc, '{}feathers_follicles_Grp'.format(IdName))
    else:
        GRPflc = cmds.group(em=True, n='{}feathers_follicles_Grp'.format(IdName), p='GRP_{}WIng_RIG'.format(IdName))
        cmds.parent(flc, GRPflc)

    
    #CreateFollicleNurbs
    flcShapeIK = cmds.shadingNode('follicle', asUtility=True)
    cmds.pickWalk(d='up')
    cmds.rename('{}{}{}_ik_flc'.format(IdName, Feather, Label))
    flcIK = cmds.ls(sl=True)
    cmds.connectAttr('{}Shape.local'.format(NurbsUM), '{}{}{}_ik_flcShape.inputSurface'.format(IdName, Feather, Label))
    cmds.connectAttr('{}Shape.worldMatrix[0]'.format(NurbsUM), '{}{}{}_ik_flc.inputWorldMatrix'.format(IdName, Feather, Label))
    cmds.connectAttr('{}{}{}_ik_flcShape.outRotate'.format(IdName, Feather, Label), '{}{}{}_ik_flc.rotate'.format(IdName, Feather, Label))
    cmds.connectAttr('{}{}{}_ik_flcShape.outTranslate'.format(IdName, Feather, Label), '{}{}{}_ik_flc.translate'.format(IdName, Feather, Label))

    cmds.scaleConstraint('Transform_Null', flcIK, mo=True)

    CPOS = cmds.shadingNode('closestPointOnSurface',  asUtility=True)
    cmds.connectAttr('{}Shape.worldSpace[0]'.format(NurbsUM), '{}.inputSurface'.format(CPOS))
    cmds.connectAttr('{}.worldPosition[0]'.format(shapeLoc[-1]), '{}.inPosition'.format(CPOS))
    cmds.connectAttr('{}.parameterU'.format(CPOS),'{}{}{}_ik_flcShape.parameterU'.format(IdName, Feather, Label))
    cmds.connectAttr('{}.parameterV'.format(CPOS),'{}{}{}_ik_flcShape.parameterV'.format(IdName, Feather, Label))
    cmds.select(cl=True)
    cmds.parent('{}{}{}_IKH'.format(IdName, Feather, Label), flcIK)
    cmds.delete(CPOS)
    
    #Group follicle Nurbs
    if cmds.objExists('{}feathers_ik_follicles_Grp'.format(IdName)):
        cmds.parent(flcIK, '{}feathers_ik_follicles_Grp'.format(IdName))
    else:
        GRPflcIK = cmds.group(em=True, n='{}feathers_ik_follicles_Grp'.format(IdName), p='GRP_{}WIng_RIG'.format(IdName))
        cmds.parent(flcIK, GRPflcIK)
    
    
    # Parent Rotate weightJnt/RootGrp
    weightSplineGrp = cmds.select('*armWeightJnt_grp')
    weightSpline_hi = cmds.listRelatives(allDescendents=True,type='joint')
    wrist_jnt = cmds.ls('*wristWeight_jnt')
    weightSpline_hi_list = [x for x in weightSpline_hi if x not in wrist_jnt]

    cmds.select(cl=True)
    
    RootGrpSel = cmds.ls('{}{}{}*_Root_Ctrl_Grp'.format(IdName, Feather, Label))
    
    # Loop through Root group
    for obj in RootGrpSel:
        if str(obj)[10:17] == 'fingers':
            parent_hi = cmds.listRelatives(obj,children=True,type='constraint')
            cmds.delete(parent_hi)
            cmds.parentConstraint('{}hand_jnt'.format(IdName), obj, mo=True)
            cmds.scaleConstraint('{}hand_jnt'.format(IdName), obj, mo=True)
            continue
        closest_jnt = None # Name of the closest joint
        closest_dist = 0 # Distance of the closest joint
    
        obj_pos = cmds.xform(obj, q=True, ws=True, t=True)
    
        # Loop through ArmWeight joints to find the nearest joint
        for target in weightSpline_hi:
            target_pos = cmds.xform(target, q=True, ws=True, t=True)
    
            # Calculate the distance between the 2 objects
            dist = math.sqrt((target_pos[0]-obj_pos[0])**2 + 
                             (target_pos[1]-obj_pos[1])**2 + 
                             (target_pos[2]-obj_pos[2])**2)
    
            # If this is the first object we loop through, automatically say it's the closest
            # Otherwise, check to see if this distance beats the closest distance
            if closest_jnt is None or dist < closest_dist:
                closest_jnt = target
                closest_dist = dist
    
        cmds.parentConstraint(closest_jnt, obj, st=['x', 'y', 'z'], mo=True)

def createSpreadSystem():
    
    
    Label = cmds.optionMenu('Number_Of_Feather', q=True, v=True)

    input_Left = cmds.checkBox('Left_Side', q=True, v=True)
    input_Right = cmds.checkBox('Right_Side', q=True, v=True)

    #Side Name Variable
    if input_Left == 1:
        IdName = 'L_'
    if input_Right == 1:
        IdName = 'R_'
    if input_Left == 0 and input_Right == 0 or input_Left == 1 and input_Right == 1:
        cmds.warning('!Hai selezionato entrambi i side o nessuno dei due!')
        return False
    
    #Curve for Spread
    JNTSel = cmds.select(IdName+'*_Root_jnt')
    jointsRoot = cmds.ls(sl=True)
    locatorOrder = []
    cmds.select(cl=True)
        
    for item in jointsRoot:
        JNT = cmds.listRelatives(item, ad=True, type='joint')
        lastJNT = cmds.ls(JNT[0])
        posJNT = cmds.xform(lastJNT, q=True, ws=True, t=True)
        loc = cmds.spaceLocator(p=posJNT)
        locatorOrder.append(loc[0])
        
        
    positionOrder = [cmds.pointPosition(i) for i in locatorOrder]
     
    #Create curve for spread distance    
    CRV_spread = cmds.curve(p=positionOrder)
    
    cmds.select(cl=True) 

    #gather info

    jointAmount = cmds.intField('jntAmountIF', q = True, v = True)

    #Create spread joints
    jntSpread_list = []
    CtrlSpread_list = []
    GrpSpread_list = []

    if IdName == 'R_':
        for index in range(jointAmount):
            jntSpread = cmds.joint(o=[180, 0, 0], n='{}feathersSpread_0{}_jnt'.format(IdName, str(index+1)))
            cmds.select(cl=True)
            jntSpread_list.append(jntSpread)
    else:
        for index in range(jointAmount):
            jntSpread = cmds.joint(n='{}feathersSpread_0{}_jnt'.format(IdName, str(index+1)))
            cmds.select(cl=True)
            jntSpread_list.append(jntSpread)


    MLP = cmds.shadingNode('multiplyDivide', asUtility=True, n='MLP_Follow_{}feathersSpread_Ctrl'.format(IdName))
    cmds.setAttr("MLP_Follow_{}feathersSpread_Ctrl.input2X".format(IdName), 0.1)
    cmds.connectAttr('spineHigh_Ctrl.{}Close_Wing'.format(IdName), 'MLP_Follow_{}feathersSpread_Ctrl.input1X'.format(IdName))


    #Create spread Joints
    for index in range(jointAmount):

        CtrlSpread = cmds.circle(ch=False, n=str('{}feathersSpread_0{}_Ctrl'.format(IdName, str(index+1))))
        cmds.setAttr('{}.visibility'.format(str(CtrlSpread[0])),l=True,k=False,cb=False)
        cmds.setAttr('{}.overrideEnabled'.format(str(CtrlSpread[0])), 1)
        cmds.setAttr('{}.overrideColor'.format(str(CtrlSpread[0])), 18)
        circle_y = cmds.duplicate(CtrlSpread[0])
        circle_x = cmds.duplicate(CtrlSpread[0])
        circle_yShape = cmds.listRelatives(circle_y, shapes=True)
        circle_xShape = cmds.listRelatives(circle_x, shapes=True)
        cmds.rotate(90,0,0, str(circle_y[0])+'.cv[0:7]', r=True,ocp=True,os=True,forceOrderXYZ=True)
        cmds.rotate(0,90,0, str(circle_x[0])+'.cv[0:7]',r=True,ocp=True,os=True,forceOrderXYZ=True)
        cmds.parent(circle_yShape,CtrlSpread[0],r=True,s=True)
        cmds.parent(circle_xShape,CtrlSpread[0],r=True,s=True)    
        cmds.delete(circle_y[0],circle_x[0])
        grp_spread_01 = cmds.group(CtrlSpread[0],w=True,n='{}_Grp_animation_off2'.format(str(CtrlSpread[0])))
        grp_spread_02 = cmds.group(grp_spread_01,w=True,n='{}_Grp_animation_off'.format(str(CtrlSpread[0])))
        cmds.setAttr('{}.rotateOrder'.format(grp_spread_02), 5)
        grp_spread_03 = cmds.group(grp_spread_02,w=True,n='{}_Grp_close_wing'.format(str(CtrlSpread[0])))
        grp_spread_04 = cmds.group(grp_spread_03,w=True,n='{}_Grp_offset'.format(str(CtrlSpread[0])))
        grp_spread_05 = cmds.group(grp_spread_04,w=True,n='{}_Grp'.format(str(CtrlSpread[0])))
        GrpSpread_list.append(grp_spread_05)
        CtrlSpread_list.append(CtrlSpread)

        if index < (jointAmount-1):
            #Follow System
            follow = cmds.addAttr(CtrlSpread, ln='follow', at='float', min=0, max=1, k=True)
            BND = cmds.shadingNode('blendColors', asUtility=True, n='BND_Follow_{}feathersSpread_0{}_Ctrl'.format(IdName, str(index+1)))
            cmds.connectAttr('{}.follow'.format(CtrlSpread[0]), '{}.color2R'.format(BND))
            cmds.connectAttr('{}.follow'.format(CtrlSpread[0]), '{}.color2G'.format(BND))
            cmds.connectAttr('{}.follow'.format(CtrlSpread[0]), '{}.color2B'.format(BND))
            cmds.connectAttr('{}.outputX'.format(MLP), '{}.blender'.format(BND, IdName, str(index+1)))
            REV = cmds.shadingNode('reverse', asUtility=True, n='REV_Follow_{}feathersSpread_0{}_Ctrl'.format(IdName, str(index+1)))
            cmds.connectAttr('{}.outputR'.format(BND), '{}.inputX'.format(REV))
        else:
            continue


    #Equidistant joints along curve
    selection = cmds.select(CRV_spread, jntSpread_list)
    alongCurve = cmds.PositionAlongCurve()

    #group spread joints
    GrpSpreadJnt = cmds.group(n='{}feathers_spread_joints_Grp'.format(IdName), p='GRP_{}WIng_RIG'.format(IdName))

    for index in range(jointAmount): 
        cmds.matchTransform(GrpSpread_list[index], jntSpread_list[index])
        cmds.parentConstraint(CtrlSpread_list[index], jntSpread_list[index], mo=True)
        cmds.scaleConstraint(CtrlSpread_list[index], jntSpread_list[index], mo=True)

    GrpSpreadCtrl = cmds.group(GrpSpread_list, n='{}feathers_spread_controls_Grp'.format(IdName), p='GRP_{}WIng_RIG'.format(IdName))
    
    cmds.delete(CRV_spread)
    cmds.delete(locatorOrder)


def CreateFlapCtrl():

    #WingsMaster
    for index in range(2):
        if index == 0:
            sideFlap = 'L_'
        if index == 1:
            sideFlap = 'R_'
        WingsMaster = cmds.sphere(ax=(0,0,1), ch=False, n='{}Wing_master_Ctrl'.format(sideFlap))
        yellow = cmds.sets(e=True, fe='Control_Yellow_SG')
        cmds.setAttr('.overrideEnabled', 1)
        cmds.setAttr('.overrideColor', 17)
        cmds.setAttr('.visibility', l=True, k=False, cb=False)
        cmds.setAttr('.ty', l=True, k=False, cb=False)
        cmds.setAttr('.rx', l=True, k=False, cb=False)
        cmds.setAttr('.ry', l=True, k=False, cb=False)
        cmds.setAttr('.rz', l=True, k=False, cb=False)
        cmds.setAttr('.sx', l=True, k=False, cb=False)
        cmds.setAttr('.sy', l=True, k=False, cb=False)
        cmds.setAttr('.sz', l=True, k=False, cb=False)
        a = cmds.addAttr(ln='spread_controls_vis', at='enum', en=('off:on'), dv=1)
        cmds.setAttr('{}Wing_master_Ctrl.spread_controls_vis'.format(sideFlap), k=False, cb=True)
        b = cmds.addAttr(ln='flap_controls_vis', at='enum', en=('off:on'), dv=1)
        cmds.setAttr('{}Wing_master_Ctrl.flap_controls_vis'.format(sideFlap), k=False, cb=True)
        c = cmds.addAttr(ln='feathers_controls_vis', at='enum', en=('off:on'), dv=1)
        cmds.setAttr('{}Wing_master_Ctrl.feathers_controls_vis'.format(sideFlap), k=False, cb=True, )
                
        Grp01Master = cmds.group(WingsMaster[0], n='{}Wing_master_Ctrl_Grp_animation_0ff2'.format(sideFlap))
        Grp02Master = cmds.group(Grp01Master, n='{}Wing_master_Ctrl_Grp_animation_off'.format(sideFlap))
        cmds.setAttr('{}.rotateOrder'.format(Grp02Master), 5)
        Grp03Master = cmds.group(Grp02Master, n='{}Wing_master_Ctrl_Grp_close_wing'.format(sideFlap))
        Grp04Master = cmds.group(Grp03Master, n='{}Wing_master_Ctrl_Grp'.format(sideFlap))
        if index == 0:
            cmds.rotate('90deg', 0, 0, Grp04Master)
            cmds.move(25, 2, 0, Grp04Master)
        if index == 1:
            cmds.rotate('-90deg', 0, 0, Grp04Master)
            cmds.move(-25, 2, 0, Grp04Master)


    half = cmds.intSliderGrp('input_controls', q=True, v=True)
    
    FlapCtrl = half*2

    i=1
    for index in range (0, half):
        #L_FlapControls
        L_Flap = cmds.sphere(ax=(0,1,0), ch=False, n='L_flap_0{}_Ctrl'.format(str(i)))
        cmds.setAttr('.overrideEnabled', 1)
        cmds.setAttr('.overrideColor', 6)
        cmds.setAttr('.visibility', l=True, k=False, cb=False)
        L_blue = cmds.sets(e=True, fe='Control_Blue_SG')
         
        if index == (half-1):
            cmds.setAttr('.tx', l=True, k=False, cb=False)
            cmds.setAttr('.rx', l=True, k=False, cb=False)
            cmds.setAttr('.ry', l=True, k=False, cb=False)
            cmds.setAttr('.rz', l=True, k=False, cb=False)
            cmds.setAttr('.sx', l=True, k=False, cb=False)
            cmds.setAttr('.sy', l=True, k=False, cb=False)
            cmds.setAttr('.sz', l=True, k=False, cb=False)
        else:
            cmds.setAttr('.ty', l=True, k=False, cb=False)
            cmds.setAttr('.rx', l=True, k=False, cb=False)
            cmds.setAttr('.ry', l=True, k=False, cb=False)
            cmds.setAttr('.rz', l=True, k=False, cb=False)
            cmds.setAttr('.sx', l=True, k=False, cb=False)
            cmds.setAttr('.sy', l=True, k=False, cb=False)
            cmds.setAttr('.sz', l=True, k=False, cb=False) 
    
        #Group
        L_Grp01 = cmds.group(L_Flap[0], n='L_flap_0{}_Ctrl_Grp_animation_off2'.format(str(i)))
        L_Grp02 = cmds.group(L_Grp01, n='L_flap_0{}_Ctrl_Grp_animation_off'.format(str(i)))
        cmds.setAttr('{}.rotateOrder'.format(L_Grp02), 5)
        L_Grp03 = cmds.group(L_Grp02, n='L_flap_0{}_Ctrl_Grp_close_wing'.format(str(i)))
        L_Grp04 = cmds.group(L_Grp03, n='L_flap_0{}_Ctrl_Grp'.format(str(i)))
        cmds.rotate('90deg', 0, 0, L_Grp04)
        cmds.connectAttr('L_Wing_master_Ctrl.flap_controls_vis', '{}.visibility'.format(L_Grp04))

        if cmds.objExists('L_WIng_Controls_Grp'):
            cmds.parent(L_Grp04, 'L_WIng_Controls_Grp')
        else:
            L_GrpFlaps = cmds.group(w=True, em=True, n='L_WIng_Controls_Grp')
            cmds.parent(L_Grp04, L_GrpFlaps)


        L_PM = cmds.shadingNode('plusMinusAverage', asUtility=True, n='PLS_L_flap_0'+str(i))
        if index == (half-1):
            cmds.connectAttr('L_flap_0{}_Ctrl.ty'.format(str(i)), '{}.input3D[0].input3Dy'.format(L_PM))
            cmds.connectAttr('L_flap_0{}_Ctrl.tz'.format(str(i)), '{}.input3D[0].input3Dz'.format(L_PM))
            cmds.connectAttr('L_Wing_master_Ctrl.tx', '{}.input3D[1].input3Dy'.format(L_PM))
            cmds.connectAttr('L_Wing_master_Ctrl.tz', '{}.input3D[1].input3Dz'.format(L_PM))
        else:
            cmds.connectAttr('L_flap_0{}_Ctrl.translate'.format(str(i)), '{}.input3D[0]'.format(L_PM))
            cmds.connectAttr('L_Wing_master_Ctrl.translate', '{}.input3D[1]'.format(L_PM))
        
        L_MLP = cmds.shadingNode('multiplyDivide', asUtility=True, n='MLP_L_flap_0{}'.format(str(i)))
        cmds.connectAttr('{}.output3D'.format(L_PM), '{}.input1'.format(L_MLP))
        if index == (half-1):
            cmds.setAttr('{}.input2Y'.format(L_MLP), 3)
            cmds.setAttr('{}.input2Z'.format(L_MLP), -3)
        else:
            cmds.setAttr('{}.input2X'.format(L_MLP), 3)
            cmds.setAttr('{}.input2Z'.format(L_MLP), -3)

        i+=1


    i=1
    for index in range(half, FlapCtrl):
        #R_FlapControls
        R_Flap = cmds.sphere(ax=(0,1,0), ch=False, n='R_flap_0{}_Ctrl'.format(str(i)))
        cmds.setAttr('.overrideEnabled', 1)
        cmds.setAttr('.overrideColor', 6)
        cmds.setAttr('.visibility', l=True, k=False, cb=False)
        R_blue = cmds.sets(e=True, fe='Control_Blue_SG')
         
        if index == (FlapCtrl-1):
            cmds.setAttr('.tx', l=True, k=False, cb=False)
            cmds.setAttr('.rx', l=True, k=False, cb=False)
            cmds.setAttr('.ry', l=True, k=False, cb=False)
            cmds.setAttr('.rz', l=True, k=False, cb=False)
            cmds.setAttr('.sx', l=True, k=False, cb=False)
            cmds.setAttr('.sy', l=True, k=False, cb=False)
            cmds.setAttr('.sz', l=True, k=False, cb=False)
        else:
            cmds.setAttr('.ty', l=True, k=False, cb=False)
            cmds.setAttr('.rx', l=True, k=False, cb=False)
            cmds.setAttr('.ry', l=True, k=False, cb=False)
            cmds.setAttr('.rz', l=True, k=False, cb=False)
            cmds.setAttr('.sx', l=True, k=False, cb=False)
            cmds.setAttr('.sy', l=True, k=False, cb=False)
            cmds.setAttr('.sz', l=True, k=False, cb=False) 
    
        #Group
        R_Grp01 = cmds.group(R_Flap[0], n='R_flap_0{}_Ctrl_Grp_animation_off2'.format(str(i)))
        R_Grp02 = cmds.group(R_Grp01, n='R_flap_0{}_Ctrl_Grp_animation_off'.format(str(i)))
        cmds.setAttr('{}.rotateOrder'.format(R_Grp02), 5)
        R_Grp03 = cmds.group(R_Grp02, n='R_flap_0{}_Ctrl_Grp_close_wing'.format(str(i)))
        R_Grp04 = cmds.group(R_Grp03, n='R_flap_0{}_Ctrl_Grp'.format(str(i)))
        cmds.rotate('-90deg', 0, 0, R_Grp04)
        cmds.connectAttr('R_Wing_master_Ctrl.flap_controls_vis', '{}.visibility'.format(R_Grp04))

        
        if cmds.objExists('R_WIng_Controls_Grp'):
            cmds.parent(R_Grp04, 'R_WIng_Controls_Grp')
        else:
            R_GrpFlaps = cmds.group(w=True, em=True, n='R_WIng_Controls_Grp')
            cmds.parent(R_Grp04, R_GrpFlaps)

        R_PM = cmds.shadingNode('plusMinusAverage', asUtility=True, n='PLS_R_flap_0'+str(i))
        if index == (FlapCtrl-1):
            cmds.connectAttr('R_flap_0{}_Ctrl.ty'.format(str(i)), '{}.input3D[0].input3Dy'.format(R_PM))
            cmds.connectAttr('R_flap_0{}_Ctrl.tz'.format(str(i)), '{}.input3D[0].input3Dz'.format(R_PM))
            cmds.connectAttr('R_Wing_master_Ctrl.tx', '{}.input3D[1].input3Dy'.format(R_PM))
            cmds.connectAttr('R_Wing_master_Ctrl.tz', '{}.input3D[1].input3Dz'.format(R_PM))
        else:
            cmds.connectAttr('R_flap_0{}_Ctrl.translate'.format(str(i)), '{}.input3D[0]'.format(R_PM))
            cmds.connectAttr('R_Wing_master_Ctrl.translate', '{}.input3D[1]'.format(R_PM))
        
        R_MLP = cmds.shadingNode('multiplyDivide', asUtility=True, n='MLP_R_flap_0{}'.format(str(i)))
        cmds.connectAttr('{}.output3D'.format(R_PM), '{}.input1'.format(R_MLP))
        if index == (FlapCtrl-1):
            cmds.setAttr('{}.input2Y'.format(R_MLP), -3)
            cmds.setAttr('{}.input2Z'.format(R_MLP), 3)
        else:
            cmds.setAttr('{}.input2X'.format(R_MLP), -3)
            cmds.setAttr('{}.input2Z'.format(R_MLP), 3)
        
        i+=1

    # VIS connection with Wings master
    cmds.connectAttr('R_Wing_master_Ctrl.feathers_controls_vis', 'R_feathers_controls_Grp.visibility')
    cmds.connectAttr('R_Wing_master_Ctrl.spread_controls_vis', 'R_feathers_spread_controls_Grp.visibility')
    cmds.parent('R_Wing_master_Ctrl_Grp', R_GrpFlaps)
    cmds.connectAttr('L_Wing_master_Ctrl.feathers_controls_vis', 'L_feathers_controls_Grp.visibility')
    cmds.connectAttr('L_Wing_master_Ctrl.spread_controls_vis', 'L_feathers_spread_controls_Grp.visibility')       
    cmds.parent('L_Wing_master_Ctrl_Grp', L_GrpFlaps)


def flapConnections():
    
    sel = cmds.ls(sl=True)
    MLP = sel[0]
    grp = sel[1:]
    
    for item in grp:
        hierarchy = cmds.listRelatives(item,allDescendents=True,type='transform')
        for token in hierarchy:
            if not 'IK' in str(token):
                if str(token)[-6:] == 'offset':
                    cmds.connectAttr(MLP+'.outputX',token+'.rz',force=True)
                    cmds.connectAttr(MLP+'.outputZ',token+'.ry',force=True)
                    if str(token)[10:17] == 'fingers':
                        cmds.connectAttr(MLP+'.outputY',token+'.rz',force=True)
                        cmds.connectAttr(MLP+'.outputZ',token+'.ry',force=True)
                    

def fixGroupHiAB():
    
    sel = cmds.ls(sl=True)
    
    for obj in sel:
        #check rotate order for close wings group
        rotOrdCtrl = cmds.getAttr('{}.rotateOrder'.format(obj))

        ctrlGrp_01 = cmds.group(obj,name='{}_Grp_animation_off2'.format(str(obj)))
        cmds.xform(ctrlGrp_01, piv=[0,0,0], os=True)
        ctrlGrp_02 = cmds.group(ctrlGrp_01,name='{}_Grp_animation_off'.format(str(obj)))
        cmds.xform(ctrlGrp_02, piv=[0,0,0], os=True)
        cmds.setAttr('{}.rotateOrder'.format(ctrlGrp_02), 5)
        ctrlGrp_03 = cmds.group(ctrlGrp_02,name='{}_Grp_close_wing'.format(str(obj)))
        cmds.setAttr('{}.rotateOrder'.format(ctrlGrp_03), rotOrdCtrl)
        cmds.xform(ctrlGrp_03, piv=[0,0,0], os=True)


def fixGroupHiCurvy():
    
    sel = cmds.ls(sl=True)
    hi = cmds.listRelatives(sel, ad=True, type='transform')
    hi_cnt = [obj for obj in hi if obj[0:3] == 'CNT']

    rename_list = []
    cmds.select(hi_cnt)

    for i in hi:
        if i[10:15] == '_CNT_':
            cmds.rename(i, i.replace('_CNT_', '_'))
            
        else:
            ren = cmds.rename(i, str(i.replace('CNT_', '')+'_Ctrl'))
            rename_list.append(ren)
    
    for item in rename_list:
        #check rotate order for close wings group
        rotOrdCtrl = cmds.getAttr('{}.rotateOrder'.format(item))

        ctrlGrp_01 = cmds.group(item,name='{}_Grp_animation_off2'.format(str(item.replace('CNT_', ''))))
        cmds.xform(ctrlGrp_01, piv=[0,0,0], os=True)
        ctrlGrp_02 = cmds.group(ctrlGrp_01,name='{}_Grp_animation_off'.format(str(item.replace('CNT_', ''))))
        cmds.xform(ctrlGrp_02, piv=[0,0,0], os=True)
        cmds.setAttr('{}.rotateOrder'.format(ctrlGrp_02), 5)
        ctrlGrp_03 = cmds.group(ctrlGrp_02,name='{}_Grp_close_wing'.format(str(item.replace('CNT_', ''))))
        cmds.setAttr('{}.rotateOrder'.format(ctrlGrp_03), rotOrdCtrl)
        cmds.xform(ctrlGrp_03, piv=[0,0,0], os=True)
        ctrlGrp_04 = cmds.group(ctrlGrp_03,name='{}_Grp_offset'.format(str(item.replace('CNT_', ''))))
        cmds.xform(ctrlGrp_04, piv=[0,0,0], os=True)


    #UI
def CreateUI():
    if cmds.window('WingsRIg', q=True, ex=True):
        cmds.deleteUI('WingsRIg') 
    WingRig = cmds.window('WingsRIg', title='Wings RIG', h=50)

    form = cmds.formLayout()
    tabs = cmds.tabLayout(innerMarginWidth=5, innerMarginHeight=5)
    cmds.formLayout( form, edit=True, attachForm=((tabs, 'top', 0), (tabs, 'left', 0), (tabs, 'bottom', 0), (tabs, 'right', 0)) )

    #TAB 01
    shelf1=cmds.rowColumnLayout(nc=1, cw=[1, 230], p=tabs)
    cmds.text('Insert UM wings', h=20, bgc=[0.2, 0.2, 0.2], fn='fixedWidthFont')

    cmds.rowColumnLayout(nc=2, cw=[(1, 60), (2, 170)])
    cmds.button( label='Plane >>', h=25, c='SelectionMesh()' )
    cmds.textField( 'input_Mesh')
    cmds.setParent('..')

    cmds.rowColumnLayout(nc=2, cw=[(1, 60), (2, 170)])
    cmds.button( label='Nurbs >>', h=25, c='SelectionNurbs()' )
    cmds.textField( 'input_Nurbs')
    cmds.setParent('..')

    cmds.separator(st='none', h=10)

    cmds.rowColumnLayout()
    cmds.text('Side name', al='center', bgc=[0.2, 0.2, 0.2], fn='fixedWidthFont')
    cmds.rowColumnLayout(nc=4, co=([1, 'left', 60], [2, 'right', 60]))
    cmds.separator(h=50)
    cmds.checkBox('Left_Side', l='L', v=False)
    cmds.checkBox('Right_Side', l='R', v=False)
    cmds.setParent('..')


    cmds.rowColumnLayout(nc=1, cw=[1, 230])
    cmds.optionMenu('Number_Of_Feather', l='Feather Label')
    cmds.menuItem(l='A')
    cmds.menuItem(l='B')
    cmds.menuItem(l='C')
    cmds.menuItem(l='D')
    cmds.menuItem(l='E')
    cmds.menuItem(l='F')
    cmds.menuItem(l='G')
    cmds.menuItem(l='H')
    cmds.menuItem(l='I')
    cmds.menuItem(l='L')
    cmds.menuItem(l='M')
    cmds.menuItem(l='N')
    cmds.menuItem(l='O')
    cmds.menuItem(l='P')
    cmds.menuItem(l='Q')
    cmds.menuItem(l='R')
    cmds.menuItem(l='S')
    cmds.menuItem(l='T')
    cmds.menuItem(l='U')
    cmds.menuItem(l='V')
    cmds.menuItem(l='Z')

    cmds.separator(st='none', h=10)

    cmds.rowColumnLayout()
    cmds.button(l='Create Feather Setup', w=230, h=25, c='FeatherSetup()')
    cmds.separator(st='none', h=20)
    cmds.text (l = "Spread Joint Amount:", bgc=[0.2, 0.2, 0.2], fn='fixedWidthFont')
    jntAmountIF = cmds.intField('jntAmountIF', v = 4, min = 2)
    cmds.button(l = "Create Spread", w = 230, c = "createSpreadSystem()")

    #TAB 02
    shelf2 = cmds.rowColumnLayout(nr=14, ro=([1, 'top', 5], [2, 'top', 5], [3, 'top', 10]), p=tabs)
    cmds.text('Number Of Flap Controls', bgc=[0.3, 0.3, 0.3], fn='plainLabelFont', w=230)
    Number_Controls = cmds.intSliderGrp('input_controls', field=True, min=1, max=10, v=3, w=230)
    cmds.separator(st='none', h=10)
    cmds.button(l='Create Flap', h=30, c='CreateFlapCtrl()')
    cmds.separator(st='none', h=5)
    cmds.text('select the multiplyDivide flap node and \nthe root group of all controls \nto be connected to it', al='center', bgc=[0.3, 0.3, 0.3], fn='plainLabelFont')
    cmds.separator(st='none', h=5)
    cmds.button(l='Flap Connections', h=30, c='flapConnections()')
    cmds.separator(st='none', h=10)
    cmds.button(l='Fix AB Ctrl Hierarchy', w=230, h=30, c='fixGroupHiAB()')
    cmds.separator(st='none', h=5)
    cmds.button(l='Fix Curvy Ctrl Hierarchy', w=230, h=30, c='fixGroupHiCurvy()')


    cmds.tabLayout( tabs, edit=True, tabLabel=((shelf1, 'Base Setup'), (shelf2, 'Flap and Fix')) )

    cmds.showWindow(WingRig)

CreateUI()