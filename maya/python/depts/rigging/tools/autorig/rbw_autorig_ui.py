import os
import importlib

import maya.cmds as cmd
from maya import OpenMayaUI as omui

localpipe = os.getenv("LOCALPY")

# Special cases for different Maya versions
try:
    from shiboken2 import wrapInstance
except ImportError:
    from shiboken import wrapInstance

try:
    from PySide2.QtGui import QIcon
    from PySide2.QtWidgets import QWidget
except ImportError:
    from PySide.QtGui import QIcon, QWidget

#Body Rig
def run_abautorigCmd(*args):
    import maya.mel as mel
    mel.eval("source abAutoRig; abAutoRig;")

def run_fingerInterfaceCmd(*args):
    import depts.rigging.tools.autorig.fingers_interface as fgi
    importlib.reload(fgi)
    fgi.fingersWindow()

def run_createCupBiped_v01(*args):
    import depts.rigging.tools.autorig.modules.createCupBiped_v01 as ccb
    importlib.reload(ccb)
    ccb.createCupBiped(item)

def run_creaturerigCmd(*args):
    import maya.mel as mel
    mel.eval("source cr_moduleTemplateBuilder; cr_moduleTemplateBuilder;")
    
def run_AutoTailRIG(*args):
    import depts.rigging.tools.autorig.modules.AutoTailRIG as atr
    importlib.reload(atr)
    atr.createWin()

def importUplegs(*args):
    import maya.cmds as cmds
    localpipeBackDir = os.path.dirname(localpipe)
    fileToImport = os.path.join(localpipeBackDir, 'scenes', 'rigging', 'rbw_autorig', 'RBW_uplegs_rig.ma')
    cmds.file(fileToImport, i=True, type="mayaAscii", namespace=":")

def spaceOnQuadLegsCmd(*args):
    import depts.rigging.tools.autorig.modules.spaceSwitchOnQuadLegs as ssoql
    importlib.reload(ssoql)

def run_fixesInterfaceCmd(*args):
    import depts.rigging.tools.autorig.fixes_interface as fix
    importlib.reload(fix)
    fix.fixesWindow()

def run_autoClavsAndBreathCmd(*args):
    import depts.rigging.tools.autorig.modules.AUTOClavicle_AUTObreath_Vr04 as acab
    importlib.reload(acab)
    acab.AutoClavicleAutoBreath()
 
def importSetsAndLayers(*args):
    import maya.cmds as cmds
    localpipeBackDir = os.path.dirname(localpipe)
    fileToImport = os.path.join(localpipeBackDir, 'scenes', 'rigging', 'rbw_autorig', 'RBW_sets_and_layers.ma')
    cmds.file(fileToImport, i=True, type="mayaAscii", namespace=":")

#Utilities Rig

def run_fk_controls_number(*args):
    import depts.rigging.tools.autorig.modules.fk_controls_number_EARSrigUI as fkcn
    importlib.reload(fkcn)
    fkcn.create_FK_UI()

def run_fk_follow_trans_rot(*args): 
    import depts.rigging.tools.autorig.modules.fk_follow_trans_rot as fkftr
    importlib.reload(fkftr)
    fkftr.create_FK_UI()

def importToesClawsRig(*args):
    import maya.cmds as cmds
    localpipeBackDir = os.path.dirname(localpipe)
    fileToImport = os.path.join(localpipeBackDir, 'scenes', 'rigging', 'rbw_autorig', 'RBW_toe_and_claws_quadruped.mb')
    cmds.file(fileToImport, i=True, type="mayaBinary", namespace=":")

def importContactHelperFoot(*args):
    import maya.cmds as cmds
    localpipeBackDir = os.path.dirname(localpipe)
    fileToImport = os.path.join(localpipeBackDir, 'scenes', 'rigging','rbw_autorig', 'RBW_contact_helper.ma')
    cmds.file(fileToImport, i=True, type="mayaAscii", namespace=":")

def run_propNullInterfaceCmd(*args):
    import depts.rigging.tools.autorig.propNULL_ui as prn
    importlib.reload(prn)
    prn.createWindow()

def run_proxyInterfaceCmd(*args):
    import depts.rigging.tools.autorig.modules.proxyMeshesBuilderUI as prx
    importlib.reload(prx)
    prx.proxyMeshesBuilder_UI()

#Face Rig
def importLevelRoot(*args):
    import maya.cmds as cmds
    localpipeBackDir = os.path.dirname(localpipe)
    fileToImport = os.path.join(localpipeBackDir, 'scenes', 'rigging','rbw_autorig', 'RBW_face_root_level.ma')
    cmds.file(fileToImport, i=True, type="mayaAscii", namespace=":")

def eyesRigCmd(*args):
    import depts.rigging.tools.autorig.modules.RBW_Eyes_Rig as eye
    importlib.reload(eye)
    eye.createWindow()

def importLevelMaster(*args):
    import maya.cmds as cmds
    localpipeBackDir = os.path.dirname(localpipe)
    fileToImport = os.path.join(localpipeBackDir, 'scenes', 'rigging','rbw_autorig', 'RBW_face_master_level.ma')
    cmds.file(fileToImport, i=True, type="mayaAscii", namespace=":")

def importLevelRM(*args):
    import maya.cmds as cmds
    localpipeBackDir = os.path.dirname(localpipe)
    fileToImport = os.path.join(localpipeBackDir, 'scenes', 'rigging','rbw_autorig', 'RBW_face_tweaks_level.ma')
    cmds.file(fileToImport, i=True, type="mayaAscii", namespace=":")

def run_fk_follow_trans_rot_whisker(*args): 
    import depts.rigging.tools.autorig.modules.fk_follow_trans_rot_whisker as fkftrw
    importlib.reload(fkftrw)
    fkftrw.create_FK_UI()

def importNewTeethTongueCmd(*args):
    import maya.cmds as cmds
    localpipeBackDir = os.path.dirname(localpipe)
    fileToImport = os.path.join(localpipeBackDir, 'scenes', 'rigging','rbw_autorig', 'RBW_teeth_and_tongue.ma')
    cmds.file(fileToImport, i=True, type="mayaAscii", namespace=":")

def importFaceAbndHairsCmd(*args):
    import maya.cmds as cmds
    localpipeBackDir = os.path.dirname(localpipe)
    fileToImport = os.path.join(localpipeBackDir, 'scenes', 'rigging','rbw_autorig', 'RBW_face_and_hairs.ma')
    cmds.file(fileToImport, i=True, type="mayaAscii", namespace=":")

def bsExpressionCmd(*args):
    import maya.cmds as cmds
    import webbrowser
    localpipeBackDir = os.path.dirname(localpipe)
    fileToOpen=os.path.join(localpipeBackDir, 'scenes', 'rigging','rbw_autorig', 'RBW_expression_BS.txt')
    webbrowser.open(fileToOpen)

def attachBSCmd(*args):
    import depts.rigging.tools.autorig.modules.attachBS as bs
    importlib.reload(bs)
    bs.createWindow()

#Finalize Rig

def run_renameTool(*args):
    import depts.rigging.tools.autorig.modules.rename as rnm
    importlib.reload(rnm)
    rnm.createWindow()
    
def checkNamingToolCmd(*args):
    import depts.rigging.tools.autorig.modules.check_naming as cknm
    importlib.reload(cknm)
    cknm.createWindow()
    
def run_shadersGeoVisCmd(*args):    
    import depts.rigging.tools.autorig.modules.shadersGeoVis as sgv
    importlib.reload(sgv)
    sgv.shaderGeoVisExc()

def run_renameDeformers(*args):
    import depts.rigging.tools.autorig.modules.renameDeformersUI as rdef
    importlib.reload(rdef)
    rdef.renameDeformers_UI()

#Props Rig
'''
'''
#Extra
'''
'''
def linkWiki(*args):
    import webbrowser
    pathToHelp="https://sites.google.com/rbw-cgi.it/rbw-rigging-team/home"
    webbrowser.open(pathToHelp)    

def main():
    '''
    auto rig graphic user interface
    '''
    if cmd.window("rbwAutorigUI", q=True, ex=True):
         cmd.deleteUI("rbwAutorigUI")
    ww=cmd.window("rbwAutorigUI",t="Rainbow CGI - Autorig 2.0")
    
    # Get a pointer and convert it to Qt Widget object
    qw = omui.MQtUtil.findWindow(ww)
    widget = wrapInstance(int(qw), QWidget)

    # Create a QIcon object
    iconpath = os.path.join(localpipe, "depts", "rigging", "tools", "icons", "autorig", "RainbowCGI_icona.ico")
    imagepath = os.path.join(localpipe, "depts", "rigging", "tools", "icons", "autorig", "rbwAuto.png")
    icon = QIcon(iconpath)

    # Assign the icon
    widget.setWindowIcon(icon)
    
    col = cmd.columnLayout(adjustableColumn=1)

    col1=cmd.columnLayout(adj=1, p=col, bgc=[0.2, 0.2, 0.2])
    cmd.rowLayout(numberOfColumns=2,p=col1, adj=2, cat=[(1,"left",5),(2,"left",0)])
    cmd.picture(i=imagepath)
    cmd.text("RBW Autorig UI 2.0", h=20)

    #Body Rig
    body = cmd.frameLayout(label="Body Rig", collapsable=1, collapse=1, p=col, bgc=[0.0, 0.5, 0.9])
    cmd.columnLayout(adj=1, bgc=[0.2, 0.2, 0.2] )
    cmd.button(l="| AB Autorig |", c= run_abautorigCmd, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| Fingers Module |", c= run_fingerInterfaceCmd, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| Body Fixes |", c= run_fixesInterfaceCmd, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| Create Cup Biped |", c= run_createCupBiped_v01, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| Creature Module |", c= run_creaturerigCmd, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| Import Quad Uplegs |", c= importUplegs, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| Space Switch for Quad |", c= spaceOnQuadLegsCmd, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| Tail Module |", c= run_AutoTailRIG, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| Auto Clavs and Breath |", c= run_autoClavsAndBreathCmd, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| SETS and Layers |", c=importSetsAndLayers, h=30, bgc=[0.3, 0.3, 0.3])

    #Utilities Rig
    cmd.frameLayout(label="Utilities", collapsable=1, collapse=1, p=col, bgc=[0.0, 0.6, 0.0] )
    cmd.columnLayout(adj=1, bgc=[0.2, 0.2, 0.2] )
    cmd.button(l="| EARS Module |", c=run_fk_controls_number, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| TAIL Module |", c=run_fk_follow_trans_rot, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| Import TOES && CLAWS |", c=importToesClawsRig, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| Import Contact Helper |", c=importContactHelperFoot, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| PROXY Module |", c=run_proxyInterfaceCmd, h=30, bgc=[0.3, 0.3, 0.3])

    #Face Rig
    cmd.frameLayout(label="Face Rig", collapsable=1, collapse=1, p=col, bgc=[0.9, 0.6, 0.0])
    cmd.columnLayout(adj=1, bgc=[0.2, 0.2, 0.2])
    cmd.button(l="| Root Level |", c=importLevelRoot, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| Eyes Module |", c=eyesRigCmd, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| Master Level |", c=importLevelMaster, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| Tweak Level |", c=importLevelRM, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| Whiskers Module |", c=run_fk_follow_trans_rot_whisker, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| Teeth and Tongue |", c=importNewTeethTongueCmd, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| Import Face and Hairs Panel |", c=importFaceAbndHairsCmd, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| Attach BS |", c=attachBSCmd, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| BS Expression |", c=bsExpressionCmd, h=30, bgc=[0.3, 0.3, 0.3])

    #Finalize Rig
    cmd.frameLayout(label="Finalize", collapsable=1, collapse=1, p=col, bgc=[0.9, 0.0, 0.0]  )
    cmd.columnLayout(adj=1, bgc=[0.2, 0.2, 0.2])
    cmd.button(l="| Rename Body |", c=run_renameTool, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| Rename Deformers |", c=run_renameDeformers, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| Check Naming |", c=checkNamingToolCmd, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| NULL PROPS Module |", c=run_propNullInterfaceCmd, h=30, bgc=[0.3, 0.3, 0.3])
    cmd.button(l="| Shaders GEO Vis |", c=run_shadersGeoVisCmd, h=30, bgc=[0.3, 0.3, 0.3])

    cmd.columnLayout(adj=1, p=col, bgc=[0.2, 0.2, 0.2])
    cmd.button(l="| Wiki |", c=linkWiki, h=30)

    cmd.showWindow(ww)
