import maya.cmds as cmds
import importlib
import os
from maya import OpenMayaUI as omui
localpipe = os.getenv("LOCALPY")

def importPropNullBipedRig(t):
    import maya.cmds as cmds
    localpipeBackDir = os.path.dirname(localpipe)
    if (t=="Present"):
        fileToImport = os.path.join(localpipeBackDir, 'scenes', 'rigging','rbw_autorig', 'RBW_Prop_Nulls_biped.ma')
    else:
        fileToImport = os.path.join(localpipeBackDir, 'scenes', 'rigging','rbw_autorig', 'RBW_Prop_Nulls_biped_notail.ma')
    cmds.file(fileToImport, i=True, type="mayaAscii", namespace=":")

def run_prop_null_biped(t):   
    import depts.rigging.tools.autorig.modules.prop_null_biped as prnul
    importlib.reload(prnul)
    prnul.prop_null(t)
    
def importPropNullQuadRig(t):
    import maya.cmds as cmds
    localpipeBackDir = os.path.dirname(localpipe)
    if (t=="Present"):
        fileToImport = os.path.join(localpipeBackDir, 'scenes', 'rigging','rbw_autorig', 'RBW_Prop_Nulls_quad.mb')
    else:
        fileToImport = os.path.join(localpipeBackDir, 'scenes', 'rigging','rbw_autorig', 'RBW_Prop_Nulls_quad_notail.mb')
    cmds.file(fileToImport, i=True, type="mayaBinary", namespace=":")

def run_prop_null_quad(t):   
    import depts.rigging.tools.autorig.modules.prop_null_quad as prnulq
    importlib.reload(prnulq)
    prnulq.prop_null(t)

def radioControl(a):
    radioCol = cmds.radioCollection(a, query=True, sl=True)
    value= cmds.radioButton(radioCol, query=True, label=True) 
    return value

def importPrNull(*args):
    tail=radioControl(tailCheck)
    if radioControl(charCheck)=="Biped":
        importPropNullBipedRig(tail)
    elif radioControl(charCheck)=="Quadruped":
        importPropNullQuadRig(tail)

def attachPrNull(*args):
    tail=radioControl(tailCheck)
    if radioControl(charCheck)=="Biped":
        run_prop_null_biped(tail)
    elif radioControl(charCheck)=="Quadruped":
        run_prop_null_quad(tail)

def createWindow():
    global charCheck, tailCheck
    if (cmds.window("prNull_ui", q=True,exists=True)):
        cmds.deleteUI("prNull_ui", window=True)
    W=cmds.window("prNull_ui",title="Prop NULL UI", ip=True)    
    
    C0=cmds.columnLayout( adjustableColumn=True, rowSpacing=10, p=W,cat=("both",10),w=200)
    cmds.text("Select char type:",al="center")
    R1=cmds.rowLayout(numberOfColumns=2,p=C0)
    C1=cmds.columnLayout( rowSpacing=10, p=R1) 
    charCheck= cmds.radioCollection(p=C1) 
    biped= cmds.radioButton( label='Biped',p=C1)    
    C2=cmds.columnLayout( rowSpacing=10, p=R1)
    quad= cmds.radioButton( label='Quadruped',p=C2) 
    charCheck = cmds.radioCollection(charCheck, edit=True, select=biped)
    
    Ctail=cmds.columnLayout( adjustableColumn=True, rowSpacing=10, p=W,w=150)
    cmds.text("Tail:",al="center")
    Rtail=cmds.rowLayout(numberOfColumns=2,p=Ctail)
    Ctail1=cmds.columnLayout( rowSpacing=10, p=Rtail) 
    tailCheck= cmds.radioCollection(p=Ctail1) 
    tail= cmds.radioButton( label='Present',p=Ctail1)    
    Ctail2=cmds.columnLayout( rowSpacing=10, p=Rtail)
    notail= cmds.radioButton( label='Not Present',p=Ctail2) 
    tailCheck = cmds.radioCollection(tailCheck, edit=True, select=tail)
    
    R2=cmds.rowLayout(numberOfColumns=1,p=W)
    C3=cmds.columnLayout( adjustableColumn=True, rowSpacing=10, p=R2)
    cmds.button(l="Import Prop NULL", c=importPrNull,w=150,h=30, bgc=[0.3, 0.3, 0.3],p=C3)
    cmds.button(l="Attach Prop NULL", c=attachPrNull, w=150,h=30, bgc=[0.3, 0.3, 0.3],p=C3)
    
    cmds.showWindow( W )
    return