import maya.cmds as cmds

def align(a,b):
    parCon = cmds.parentConstraint(a, b, mo=False)
    cmds.delete(parCon)


def eyeStart(*args):
    #fixo la gerarchia di base di AB
    cmds.delete("RIG_lf_eye_jnt_aimConstraint1", "RIG_lf_eyeMover_jnt_parentConstraint1", "RIG_rt_eye_jnt_aimConstraint1", "RIG_rt_eyeMover_jnt_parentConstraint1")
    cmds.parent("L_eye_jnt", "R_eye_jnt", "head_a_jnt")
    align("L_eye_jnt","L_eyeMover_jnt")
    align("R_eye_jnt","R_eyeMover_jnt")
    cmds.setAttr("R_eyeMover_jnt.jointOrientY", 180)
    align("L_eyeMover_jnt", "RIG_RIG_lf_eyeMover_ctrl_frz_grp")
    align("R_eyeMover_jnt", "RIG_RIG_rt_eyeMover_ctrl_frz_grp")
    align("L_eye_jnt", "L_eye_fk_Ctrl_Grp")
    align("R_eye_jnt", "R_eye_fk_Ctrl_Grp")
    cmds.parentConstraint("L_eye_fk_Ctrl", "L_eye_jnt", mo=True)
    cmds.parentConstraint("R_eye_fk_Ctrl", "R_eye_jnt", mo=True)
    cmds.scaleConstraint("L_eye_fk_Ctrl", "L_eye_jnt", mo=True)
    cmds.scaleConstraint("R_eye_fk_Ctrl", "R_eye_jnt", mo=True)
    eyemoversJoint = ["L_eyeMover_jnt", "R_eyeMover_jnt"]
    
    #creo dei locator per visualizzare dove andra' il pivot degli eyemovers
    global pivotLocators
    pivotLocators = []
    
    
    for eyemover in eyemoversJoint:
        a = cmds.spaceLocator()
        shapeA = cmds.pickWalk(direction="down")[0]
        cmds.setAttr(shapeA + ".overrideEnabled", 1)
        cmds.setAttr(shapeA + ".overrideColor", 17)
        align(eyemover, a)
        cmds.makeIdentity(a, apply=True, t=True, r=False, s=False, n=False, pn=True)
        b = cmds.rename(a, eyemover[:-4] + "_loc")
        pivotLocators.append(b)
    
    mdnTrans = cmds.shadingNode("multiplyDivide", asUtility=True, name="MDN_Trans_eyemoverLoc_L_R")
    cmds.connectAttr(pivotLocators[0] + ".t", mdnTrans + ".input1")
    cmds.setAttr(mdnTrans + ".input2X", -1)
    cmds.connectAttr(mdnTrans + ".output", pivotLocators[1] + ".t")
    
    for locator in pivotLocators:
        cmds.setAttr(locator + ".r", lock=True, channelBox=False, keyable=False)
        cmds.setAttr(locator + ".s", lock=True, channelBox=False, keyable=False)
        cmds.setAttr(locator + ".v", lock=True, channelBox=False, keyable=False)
        cmds.select(clear=True)


def finalize_rig(*args):
    #memorizzo le mesh degli occhi
    eyesMeshes = cmds.ls(selection=True)
    cmds.select(clear=True)
    
    #memorizzo i valori dei locators pivot
    eyemoverLocPosLeft = cmds.xform(pivotLocators[0], ws=True, q=True, rp=True)
    eyemoverLocPosRight = cmds.xform(pivotLocators[1], ws=True, q=True, rp=True)
    
    #unlock attributi eyemovers
    cmds.setAttr("L_eyeMover_Ctrl.sx", keyable=True, lock=False)
    cmds.setAttr("L_eyeMover_Ctrl.sy", keyable=True, lock=False)
    cmds.setAttr("L_eyeMover_Ctrl.sz", keyable=True, lock=False)
    cmds.setAttr("R_eyeMover_Ctrl.sx", keyable=True, lock=False)
    cmds.setAttr("R_eyeMover_Ctrl.sy", keyable=True, lock=False)
    cmds.setAttr("R_eyeMover_Ctrl.sz", keyable=True, lock=False)
    
    #sposto i pivot degli eyemover e continuo la costruzione del rig
    cmds.move(eyemoverLocPosLeft[0], eyemoverLocPosLeft[1], eyemoverLocPosLeft[2], ("L_eyeMover_Ctrl.scalePivot", "L_eyeMover_Ctrl.rotatePivot"), rpr=True, spr=True)
    cmds.move(eyemoverLocPosRight[0], eyemoverLocPosRight[1], eyemoverLocPosRight[2], ("R_eyeMover_Ctrl.scalePivot", "R_eyeMover_Ctrl.rotatePivot"), rpr=True, spr=True)
    cmds.parentConstraint("L_eyeMover_Ctrl","L_eyeMover_jnt", mo=True)
    cmds.parentConstraint("R_eyeMover_Ctrl","R_eyeMover_jnt", mo=True)
    cmds.scaleConstraint("L_eyeMover_Ctrl","L_eyeMover_jnt", mo=True)
    cmds.scaleConstraint("R_eyeMover_Ctrl","R_eyeMover_jnt", mo=True)
    cmds.pointConstraint("L_eyeMover_Ctrl","L_eye_fk_Ctrl_Grp_offset", mo=True)
    cmds.pointConstraint("R_eyeMover_Ctrl","R_eye_fk_Ctrl_Grp_offset", mo=True)
    cmds.aimConstraint("L_eye_Ctrl", "L_eye_fk_Ctrl_Grp_offset", mo=True, weight=1, aimVector=(0 ,0, 1), upVector=(0, 1, 0), worldUpType= "objectrotation", worldUpVector=(0, 1, 0), worldUpObject="head_Ctrl")
    cmds.aimConstraint("R_eye_Ctrl", "R_eye_fk_Ctrl_Grp_offset", mo=True, weight=1, aimVector=(0 ,0, 1), upVector=(0, 1, 0), worldUpType= "objectrotation", worldUpVector=(0, 1, 0), worldUpObject="head_Ctrl")
    cmds.parent("L_eye_fk_Ctrl_Grp", "R_eye_fk_Ctrl_Grp", "head_Ctrl")
    cmds.select(clear=True)
    
    #divido le mesh tra left e right
    leftMesh = []
    rightMesh = []
    
    for mesh in eyesMeshes:
        letter = str(mesh)
        if "L" in letter:
            leftMesh.append(mesh)
        if "R" in letter:
            rightMesh.append(mesh)
    
    #creo il rig dei lattici
    sD = cmds.intSliderGrp("sDivisions", q=True, value=True )
    tD = cmds.intSliderGrp("tDivisions", q=True, value=True )
    uD = cmds.intSliderGrp("uDivisions", q=True, value=True )
    leftLattice = cmds.lattice(leftMesh, dv=(sD, tD, uD), name="LTC_L_Eye", objectCentered=True, outsideLattice=1, ldv=(2, 2, 2))
    rightLattice = cmds.lattice(rightMesh, dv=(sD, tD, uD), name="LTC_R_Eye", objectCentered=True, outsideLattice=1, ldv=(2, 2, 2))
    leftCluster = cmds.cluster(leftLattice[1], name= "CLS_L_Eye", relative=True)
    rightCluster = cmds.cluster(rightLattice[1], name= "CLS_R_Eye", relative=True)
    cmds.move(eyemoverLocPosLeft[0], eyemoverLocPosLeft[1], eyemoverLocPosLeft[2], (leftCluster[1] + ".scalePivot", leftCluster[1] + ".rotatePivot"), rpr=True, spr=True)
    cmds.move(eyemoverLocPosRight[0], eyemoverLocPosRight[1], eyemoverLocPosRight[2], (rightCluster[1] + ".scalePivot", rightCluster[1] + ".rotatePivot"), rpr=True, spr=True)
    cmds.connectAttr("L_eyeMover_Ctrl.s", leftCluster[1] + ".s")
    cmds.connectAttr("R_eyeMover_Ctrl.s", rightCluster[1] + ".s")
    
    #raggruppo tutto e metto in ordine
    leftDefGrp = cmds.group(leftLattice[1], leftLattice[2], leftCluster[1], name="L_Deformer_Eye_Grp")
    rightDefGrp = cmds.group(rightLattice[1], rightLattice[2], rightCluster[1], name="R_Deformer_Eye_Grp")
    cmds.parentConstraint("head_Ctrl", leftDefGrp, mo=True)
    cmds.scaleConstraint("head_Ctrl", leftDefGrp, mo=True)
    cmds.parentConstraint("head_Ctrl", rightDefGrp, mo=True)
    cmds.scaleConstraint("head_Ctrl", rightDefGrp, mo=True)
    cmds.group(leftDefGrp, rightDefGrp, name= "Eyes_Deformer_RIG_Grp")
    cmds.parent("Eyes_Deformer_RIG_Grp", "RIG_controls_grp")
    cmds.setAttr("Eyes_Deformer_RIG_Grp.v", 0)
    cmds.delete(pivotLocators)
    cmds.select(clear=True)

def createWindow(*args):
    if cmds.window("Eye_Rig_UI", ex= True):
        cmds.deleteUI("Eye_Rig_UI", window=True)
    
    cmds.window("Eye_Rig_UI", title="Eyes Rig UI", sizeable=True, visible=True)
    r1 = cmds.rowLayout(numberOfColumns=2, columnAttach=[(1,"left", 10),(2,"right", 10)], cl2=("center", "center"), cw2=(150,350))
    c1 = cmds.columnLayout(parent=r1)
    cmds.text("Click dopo aver importato \n il Face Root Level \n", p=c1)
    cmds.button(label="Start Rig", command=eyeStart, width=100, p=c1)
    c2 = cmds.columnLayout(parent=r1, adj=True)
    cmds.text(label="Posiziona i locator eyemovers \n e scegli il numerro di suddivisione dei lattici \n", align="center", p=c2)
    cmds.intSliderGrp("sDivisions", field=True, label='S Divisions', minValue=1, maxValue=20, fieldMinValue=1, fieldMaxValue=20, value=6, p=c2 )
    cmds.intSliderGrp("tDivisions", field=True, label='T Divisions', minValue=1, maxValue=20, fieldMinValue=1, fieldMaxValue=20, value=6, p=c2 )
    cmds.intSliderGrp("uDivisions", field=True, label='U Divisions', minValue=1, maxValue=20, fieldMinValue=1, fieldMaxValue=20, value=6, p=c2 )
    cmds.text(label="\n Seleziona le mesh degli occhi \n e clicca il seguente bottone \n", align="center", p=c2)
    cmds.button(label="Finalize Eyes Rig", command=finalize_rig, p=c2, align="left", w=150)
    cmds.showWindow("Eye_Rig_UI")