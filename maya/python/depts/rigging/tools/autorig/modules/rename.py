import maya.cmds as cmds
import sys

def renameJnts(biChk,faChk,quChk):
    print("--------------------------------------------------------------------------------------------------------"+"\n"+"\n")

    jnts=cmds.ls(type="joint")
    jntCount=0
    print("RENAME JOINTS:")
    for i in jnts:
        if ("_ctrl" in str(i)) or ("_Ctrl" in str(i)):
            continue
        n=str(i).replace("RIG_","").replace("Jnt","jnt")
        if ("lf_" in i) or ("lt_" in i):
            n="L_"+n.replace("lf_","").replace("lt_","")
        if "rt_" in i:
            n="R_"+n.replace("rt_","")
        if ("Rear_" in n) or ("Front_" in n):
            n=n.replace("segment","bendy").replace("Rear_","rear_").replace("Front_","front_").replace("_jnt_","_").replace("joint","jnt")
            if faChk:
                n=n.replace("rear_","")
        if "_ik" in n:
            n=n.replace("_ik","").replace("_jnt","_ik_jnt")
        if "_fk" in n:
            n=n.replace("_fk","").replace("_jnt","_fk_jnt")
        if str(i)!=n:
            jntCount=jntCount+1
            print(i+"  ->  "+n)
            cmds.rename(i,n)
    print("\n"+"Rinominati "+str(jntCount)+" joints."+"\n"+"\n")
    return

def setsCheck():
    chkA=False
    chkB=False
    if cmds.sets("BODY_CONTROLS", q=True)==None:
        print("Set BODY_CONTROLS vuoto")
        chkA=True
    if cmds.sets("BENDY_CONTROLS", q=True)==None:
        print("Set BENDY_CONTROLS vuoto")
        chkB=True
    if chkA and chkB:
        print("Riempi i set e rilancia lo script")
        sys.exit()
    return chkA,chkB
    
def bendyCheck(cntList):
    bendy=["Rear_rt_hip1_jnt_bendy_ctrl","Rear_lt_knee1_jnt_ctrl","Rear_lt_knee1_jnt_bendy_ctrl","Rear_lt_hip2_jnt_ctrl","Rear_lt_hip2_jnt_bendy_ctrl","Rear_lt_hip1_jnt_bendy_ctrl","Rear_rt_hip2_jnt_ctrl","Rear_rt_hip2_jnt_bendy_ctrl","Rear_rt_knee1_jnt_ctrl","Rear_rt_knee1_jnt_bendy_ctrl"]
    bndCheck=False   
    for i in cntList:
        for b in bendy:
            if str(i)==str(b):
                bndCheck=True
    if bndCheck:
        print("Errore: Non e' stato lanciato il Bendy Fix")
        sys.exit()
    return

def renameCnts(biChk,faChk,quChk):
    sts=setsCheck()
    cnts=[]
    ik=["legOffset","flex", "foot"]
    if sts[0]==False:
        cnts=cnts+cmds.sets("BODY_CONTROLS", q=True)
    if sts[1]==False:
        cnts=cnts+cmds.sets("BENDY_CONTROLS", q=True)    
    if quChk or faChk:
        bendyCheck(cnts)
    print("--------------------------------------------------------------------------------------------------------"+"\n"+"\n")
    print("RENAME CONTROLS:"+"\n")
    cntCount=0   
    for i in cnts:
        n=str(i).replace("RIG_","").replace("_ctrl","_Ctrl").replace("_jnt","").replace("Constrain","constraint")
        if ("lf_" in i) or ("lt_" in i):
            n="L_"+n.replace("lf_","").replace("lt_","")
        if "rt_" in i:
            n="R_"+n.replace("rt_","")
        #rename bendy
        if "RubberHose" in n:
            n=n.replace("RubberHose","_bendy")
        #rename arms
        if "arm_ik" in n:
            n=n.replace("_ik","").replace("Ctrl","ik_Ctrl")        
        if "shoulder_Ctrl" in n:
            n=n.replace("_Ctrl","_ik_Ctrl")
        #rename legs
        if "Upleg" in n:
            n=n.replace("Upleg","upleg")
        if "heel_ik" in n:
            n=n.replace("_ik","").replace("Ctrl","ik_Ctrl")
        if ("Rear_" in n) or ("Front_" in n):
            n=n.replace("Rear_","rear_").replace("Front_","front_")
            for a in ik:
                if a in n:
                    n=n.replace("Ctrl","ik_Ctrl").replace("legOffset","legoffset")
            if "knee_Ctrl" in n:
                n=n.replace("Ctrl","pv_Ctrl")
            if "fk" in n:
                n=n.replace("_fk","").replace("knee1","knee").replace("Ctrl","fk_Ctrl").replace("front_front","front").replace("rear_rear","rear")
        if faChk:
            n=n.replace("rear_","")
        if str(i)!=n:
            cntCount=cntCount+1
            print(i + "  ->  " + n)
            cmds.rename(i,n)
    print("\n"+"Rinominati "+str(cntCount)+" controlli.") 
    return

def operateBiped(*args):
    renameJnts(1,0,0)
    renameCnts(1,0,0)
    return

def operateQuad(*args):
    renameJnts(0,0,1)
    renameCnts(0,0,1)
    return

def operateFauno(*args):
    faChk=True
    renameJnts(0,1,0)
    renameCnts(0,1,0)
    return

def createWindow():
    global biChk,quChk,faChk
    if (cmds.window("Rename_Char_UI", q=True,exists=True)):
        cmds.deleteUI("Rename_Char_UI", window=True)
    W=cmds.window("Rename_Char_UI", t="Rename Char UI", s=True, ip=True, rtf=True)
    print(W)
    C0= cmds.columnLayout( adjustableColumn=True, rowSpacing=10, p=W)
    R0= cmds.rowLayout( numberOfColumns=3, columnAttach=[(1, 'left', 10),(2, 'both', 10),(3, 'right', 10)], p=C0)
    cmds.button( label="Biped", command=operateBiped,w=130)
    cmds.button( label="Biped - Quad Legs", command=operateFauno,w=130)
    cmds.button( label="Quadruped", command=operateQuad,w=130)
    R0= cmds.rowLayout( numberOfColumns=1, columnAttach=[(1, 'both', 10)], p=C0)
    cmds.text("Guardare lo script editor per il check dell'output dello script.", al="center")
    cmds.showWindow( W )
    return