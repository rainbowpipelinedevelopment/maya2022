"""
Descrizione: 
    Questo script crea una diretta di controlli sui joints selezionati, 
    potendo scegliere il numero di controlli.
    [EARS MODULE]

Autore: 
    Salvatore Iamonte

Versione: 
    1.0.0

Uso:
    import FK_Controls_Number
    reload(FK_Controls_Number)
    _controls_number.create_FK_UI()

"""

from maya import cmds

# utility funtions

def align(x, y):
    
    T = cmds.xform(x, ws=True, q=True, rp=True)
    R = cmds.xform(x, ws=True, q=True, ro=True)
    cmds.xform(y, ws=True, t=T)
    cmds.xform(y, ws=True, ro=R)

def create_control(name):

    name = name.replace("_Jnt", "")
    name = name.replace("JNT_", "")
    name = name.replace("_jnt", "")
    name = name.replace("jnt_", "")
    name = name.replace("Jnt_", "")
    name = name.replace("_JNT", "")
    ctrl = cmds.circle(ch=False, nr=(1,0,0), n=name + "_Ctrl") [0]
    ctrl_grp = cmds.group(ctrl, n=name + "_Ctrl_Grp")
    shape = cmds.pickWalk(ctrl, d="down")[0]
    cmds.setAttr(ctrl + ".v", k=False)
    cmds.setAttr(shape + ".overrideEnabled", 1)
    cmds.setAttr(shape + ".overrideColor", 6)
    return ctrl_grp, ctrl

#####################################################################################################

def create_FK(*args):

    controls_number = cmds.intSliderGrp("num_ctrls", q=True, v=True)
    joints = cmds.ls(sl=True, typ="joint")
    len_joints = len(joints)

    if controls_number <= len_joints:

        for joint in joints:
            cmds.setAttr(joint+".segmentScaleCompensate", 0)

        # creo i controlli

        controls = []
        controls_group = []
        for index in range(controls_number):
            ctrl = create_control(joints[index])
            controls.append(ctrl[1])
            controls_group.append(ctrl[0])

        # posiziono i controlli calcolando la media distanza tra i joint e il numero di controlli, e faccio i constrain

        average = len_joints/controls_number
        remainder = len_joints%controls_number
        index_joints = 0
        index_controls = 0
        for control in controls:

            # verifico se il contatore dei joints supera il numero dei joints totali.
            # se e' cosi', cancello i controlli eccessivi

            if index_joints >= len_joints:
                cmds.warning("Consider to change the number of controls or joints. A FK with less controls was created.")
                cmds.delete(controls_group[index_controls])
                index_controls += 1

            else:
                align(joints[index_joints], controls_group[index_controls])
                cmds.parentConstraint(control, joints[index_joints], mo=True)
                cmds.scaleConstraint(control, joints[index_joints], mo=True)

                if index_joints >= 1:
                    cmds.parentConstraint(joints[index_joints-1], controls_group[index_controls], mo=True)
                    cmds.scaleConstraint(joints[index_joints-1], controls_group[index_controls], mo=True)

                index_joints += average
                index_controls += 1

                if remainder >= 2:
                    index_joints += 1  

        # connetto le rotate del joint precedente a quello successivo, verificando prima che gli attributi siano liberi.

        for n in range(1,len_joints):
            if not cmds.connectionInfo(joints[n] + ".rotateX", isDestination=True):
                cmds.connectAttr(joints[n-1] + ".rotateX", joints[n] + ".rotateX")
            if not cmds.connectionInfo(joints[n] + ".rotateY", isDestination=True):
                cmds.connectAttr(joints[n-1] + ".rotateY", joints[n] + ".rotateY")                
            if not cmds.connectionInfo(joints[n] + ".rotateZ", isDestination=True):
                cmds.connectAttr(joints[n-1] + ".rotateZ", joints[n] + ".rotateZ")                
        cmds.select(cl=True)

    else:

        cmds.warning("Controls are greater than Joints")

####################################################################################################

def create_FK_UI():
    
    name = "Create_FK"
    if cmds.window(name, ex=True, wh=(200, 50)):
        cmds.deleteUI(name)

    window = cmds.window(name, t="EARS Rig UI")
    cmds.columnLayout(adj=True, w=200, h=50)
    cmds.intSliderGrp("num_ctrls", ad3=True, cl3=("center","center","center"), cw3=(100,30,100), l="Controls", f=True, min=1, max=20, v=3)
    cmds.button(l="Create EAR Chain", c=create_FK)

    cmds.showWindow(name)

