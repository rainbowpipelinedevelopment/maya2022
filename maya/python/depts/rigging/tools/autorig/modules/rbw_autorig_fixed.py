#//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#RBW AUTORIG DEF
#//////////////////////////////////////////////////////////////////////////////////////////////////////////////

#//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#PLACER SCRIPT AB FIXES
#//////////////////////////////////////////////////////////////////////////////////////////////////////////////
import maya.cmds as cmds
import maya.mel as mel

def rbwAutorigFixed():

    #create GRP meshes RM
    cmds.group(em=True, name='GRP_Meshes_grp')
    cmds.group(em=True, name='GRP_Meshes_RM')
    cmds.group(em=True, name='GRP_Meshes_RND')
    cmds.parent('GRP_Meshes_RM', 'GRP_Meshes_grp')
    cmds.parent('GRP_Meshes_RND', 'GRP_Meshes_grp')

    #collect placer controls
    root_control = "RIG_root_ctrl";
    master_control = "RIG_master_ctrl";
    submaster_control = "RIG_subMaster_ctrl";

    #reset root group and save notes 
    cmds.select(root_control)
    cmds.pickWalk(direction = "up");
    root_control_frz_group = cmds.ls(sl=True)[0];
    print (root_control_frz_group);
    root_control_offset_z = cmds.getAttr(root_control_frz_group + ".tz");
    cmds.select(root_control);
    root_control_offset_group = cmds.group(n=root_control + "_offset_group");
    cmds.setAttr(root_control_offset_group + ".tz", -root_control_offset_z);

    #duplicate root_control
    new_root_control = cmds.duplicate(root_control);
    root_control_hidden = cmds.rename(root_control, root_control + "_HIDDEN");
    new_root_control = cmds.rename(new_root_control,root_control);
    cmds.parent(root_control_hidden, new_root_control);

    #adding attribute to "RIG_Transform_ctrl"
    transformCtrl = root_control
    cmds.select(transformCtrl)
    cmds.addAttr(ln="L_Arm_01_IKFK", at="float", k=True, hxv=True, hnv=True, max=1, min=0, dv=1)
    cmds.addAttr(ln="R_Arm_01_IKFK", at="float", k=True, hxv=True, hnv=True, max=1, min=0, dv=1)
    cmds.addAttr(ln="L_Leg_01_IKFK", at="float", k=True, hxv=True, hnv=True, max=1, min=0, dv=1)
    cmds.addAttr(ln="R_Leg_01_IKFK", at="float", k=True, hxv=True, hnv=True, max=1, min=0, dv=1)
    #cmds.addAttr(ln="Tail_01_IKFK", at="float", k=True, hxv=True, hnv=True, max=1, min=0, dv=1)
    cmds.addAttr(ln="Geo_Vis", at="enum", en="off:on", dv=1)
    cmds.setAttr(transformCtrl + ".Geo_Vis", k=False, cb=True)
    cmds.addAttr(ln="Head_Geo_Vis", at="enum", en="off:on", dv=1)
    cmds.setAttr(transformCtrl + ".Head_Geo_Vis", k=False, cb=True)
    cmds.addAttr(ln="Mouth_Geo_Vis", at="enum", en="off:on", dv=1)
    cmds.setAttr(transformCtrl + ".Mouth_Geo_Vis", k=False, cb=True)
    cmds.addAttr(ln="Arms_Geo_Vis", at="enum", en="off:on", dv=1)
    cmds.setAttr(transformCtrl + ".Arms_Geo_Vis", k=False, cb=True)
    cmds.addAttr(ln="Legs_Geo_Vis", at="enum", en="off:on", dv=1)
    cmds.setAttr(transformCtrl + ".Legs_Geo_Vis", k=False, cb=True)
    cmds.addAttr(ln="Claws_Geo_Vis", at="enum", en="off:on", dv=1)
    cmds.setAttr(transformCtrl + ".Claws_Geo_Vis", k=True, cb=False)
    cmds.addAttr(ln="Clothes_Geo_Vis", at="enum", en="off:on", dv=1)
    cmds.setAttr(transformCtrl + ".Clothes_Geo_Vis", k=False, cb=True)
    cmds.addAttr(ln="Geo_Res", at="enum", en="HiRes:Proxy")
    cmds.setAttr(transformCtrl + ".Geo_Res", k=False, cb=True)
    cmds.addAttr(ln="Body_Cage_Fur", at="enum", en="off:on", dv=0)
    cmds.setAttr(transformCtrl + ".Body_Cage_Fur", k=False, cb=True)
    #cmds.addAttr(ln="Transparency_Body_Cage_Fur", at="float", k=True, hxv=True, hnv=True, max=10, min=0, dv=10)
    cmds.addAttr(ln="Contact_Helper_Foot", at="enum", en="off:on", dv=1)
    cmds.setAttr(transformCtrl + ".Contact_Helper_Foot", k=False, cb=True)
    cmds.addAttr(ln="Main_Ctrl_Vis", at="enum", en="off:on", dv=1)
    cmds.setAttr(transformCtrl + ".Main_Ctrl_Vis", k=False, cb=True)
    cmds.addAttr(ln="Root_Ctrl_Vis", at="enum", en="off:on", dv=1)
    cmds.setAttr(transformCtrl + ".Root_Ctrl_Vis", k=False, cb=True)

    #connecting new attributes to old and hide it
    cmds.connectAttr(transformCtrl+".L_Arm_01_IKFK", "RIG_lf_hand_ctrl.ikFkBlend", f=True)
    cmds.setAttr("RIG_lf_hand_ctrl.ikFkBlend", k=False)
    cmds.connectAttr(transformCtrl+".R_Arm_01_IKFK", "RIG_rt_hand_ctrl.ikFkBlend", f=True)
    cmds.setAttr("RIG_rt_hand_ctrl.ikFkBlend", k=False)
    cmds.connectAttr(transformCtrl+".L_Leg_01_IKFK", "RIG_lf_foot_ctrl.ikFkBlend", f=True)
    groupLfFoot = cmds.group("RIG_lf_foot_ctrl", n="GRP_RIG_lf_foot_ctrl")
    cmds.hide(groupLfFoot)
    cmds.connectAttr(transformCtrl+".R_Leg_01_IKFK", "RIG_rt_foot_ctrl.ikFkBlend", f=True)
    groupRfFoot = cmds.group("RIG_rt_foot_ctrl", n="GRP_RIG_rt_foot_ctrl")
    cmds.hide(groupRfFoot)

    cmds.connectAttr(transformCtrl+".Geo_Vis", "GRP_Meshes_grp.visibility", f=True)

    #add Pivot Vis Attr
    cmds.addAttr(root_control, ln= "Pivot_Vis", at="enum", en = "off:on");
    cmds.setAttr(root_control + ".Pivot_Vis", e=True, keyable=False, cb=True);
    cmds.setAttr(root_control + ".Pivot_Vis", 1);

    cmds.hide(root_control_hidden + "Shape");

    pivot_control = cmds.curve( d = 1, p=[(2, 0, 5), (2, 0, 2), (5, 0, 2), (5, 0, -2), (2, 0, -2), (2, 0, -5), (-2, 0, -5), (-2, 0, -2), (-5, 0, -2), (-5, 0, 2), (-2, 0, 2), (-2, 0, 5), (2, 0, 5)] );
    cmds.setAttr(pivot_control + ".ry", 45);
    cmds.setAttr(pivot_control + ".scaleX", 2);
    cmds.setAttr(pivot_control + ".scaleY", 2);
    cmds.setAttr(pivot_control + ".scaleZ", 2);
    cmds.makeIdentity(pivot_control, apply=True);
    pivot_control = cmds.rename(pivot_control,"RIG_TransformPivot_Ctrl");
    cmds.select(pivot_control);
    pivot_control_group = cmds.group(n="Transform_Pivot_Ctrl_Grp");
    cmds.connectAttr(root_control + ".Pivot_Vis", pivot_control_group + ".v");
    cmds.setAttr("RIG_TransformPivot_Ctrl.v", lock=True, keyable=False, channelBox=False)

    #parent and connect 
    cmds.parent(pivot_control_group, root_control_hidden);
    cmds.connectAttr(pivot_control + ".translate", root_control + ".rotatePivot");
    cmds.connectAttr(pivot_control + ".translate", root_control + ".scalePivot");

    #change ctrl colors
    cmds.setAttr(root_control + "Shape.overrideEnabled", 1);
    cmds.setAttr(root_control + "Shape.overrideColor", 17);
    cmds.setAttr(master_control + "Shape.overrideEnabled", 1);
    cmds.setAttr(master_control + "Shape.overrideColor", 20);
    cmds.setAttr(submaster_control + "Shape.overrideEnabled", 1);
    cmds.setAttr(submaster_control + "Shape.overrideColor", 18);
    cmds.setAttr(pivot_control + ".overrideEnabled",1);
    cmds.setAttr(pivot_control + ".overrideColor",20);

    #rename placers
    cmds.rename(master_control, "Root_Ctrl");
    cmds.rename(submaster_control, "Main_Ctrl");
    cmds.rename(root_control, "Transform_Ctrl");
    cmds.select(cl=True)

    #connection between Transform and HIDDEN
    cmds.connectAttr ('Transform_Ctrl.masterScale', 'RIG_root_ctrl_HIDDEN.masterScale')
    cmds.connectAttr ('Transform_Ctrl.skeletonVis', 'RIG_root_ctrl_HIDDEN.skeletonVis')
    cmds.connectAttr ('Transform_Ctrl.masterCtrlVis', 'RIG_root_ctrl_HIDDEN.masterCtrlVis')
    cmds.connectAttr ('Transform_Ctrl.subMasterCtrlVis', 'RIG_root_ctrl_HIDDEN.subMasterCtrlVis')
    cmds.connectAttr ('Transform_Ctrl.rigSettingsCtrlVis', 'RIG_root_ctrl_HIDDEN.rigSettingsCtrlVis')
    cmds.connectAttr ('Transform_Ctrl.masterSpineCtrlVis', 'RIG_root_ctrl_HIDDEN.masterSpineCtrlVis')
    cmds.connectAttr ('Transform_Ctrl.worldSpaceCtrlVis', 'RIG_root_ctrl_HIDDEN.worldSpaceCtrlVis')
    cmds.setAttr ('Transform_Ctrl.v', k=False, cb=False)
    cmds.setAttr ('Transform_Ctrl.skeletonVis', k=False, cb=False)
    cmds.connectAttr ('Transform_Ctrl.Main_Ctrl_Vis', 'Transform_Ctrl.subMasterCtrlVis')
    cmds.setAttr ('Transform_Ctrl.subMasterCtrlVis', k=False, cb=False)
    cmds.setAttr ('Transform_Ctrl.Main_Ctrl_Vis', 1)
    cmds.connectAttr ('Transform_Ctrl.Root_Ctrl_Vis', 'Transform_Ctrl.masterCtrlVis')
    cmds.setAttr ('Transform_Ctrl.masterCtrlVis', k=False, cb=False)
    cmds.setAttr ('Transform_Ctrl.Root_Ctrl_Vis', 1)
    cmds.setAttr ('Transform_Ctrl.rigSettingsCtrlVis', k=False, cb=False)
    cmds.setAttr ('Transform_Ctrl.rigSettingsCtrlVis', 0)
    cmds.setAttr ('Transform_Ctrl.masterSpineCtrlVis', k=False, cb=False)
    cmds.setAttr ('Transform_Ctrl.masterSpineCtrlVis', 0)
    cmds.setAttr ('Transform_Ctrl.worldSpaceCtrlVis', k=False, cb=False)
    cmds.setAttr ('Transform_Ctrl.worldSpaceCtrlVis', 0)
    cmds.disconnectAttr ('RIG_root_ctrl_HIDDEN.subMasterCtrlVis', 'Main_Ctrl.v')
    cmds.connectAttr ('RIG_root_ctrl_HIDDEN.subMasterCtrlVis', 'Main_CtrlShape.v')
    cmds.disconnectAttr ('RIG_root_ctrl_HIDDEN.masterCtrlVis', 'Root_Ctrl.v')
    cmds.connectAttr ('RIG_root_ctrl_HIDDEN.masterCtrlVis', 'Root_CtrlShape.v')

    #connection for MS
    cmds.group ('RIG_root_ctrl_HIDDEN', n='group1')
    cmds.parent ('group1', 'RIG_RIG_root_ctrl_frz_grp')
    cmds.delete ('RIG_RIG_root_ctrl_frz_grp_parentConstraint1')
    cmds.delete ('RIG_RIG_subMaster_ctrl_frz_grp_parentConstraint1')
    cmds.parent ('RIG_RIG_subMaster_ctrl_frz_grp', 'Root_Ctrl')
    cmds.parent ('RIG_root_ctrl_offset_group', 'Main_Ctrl')
    cmds.setAttr ('group1.translateZ', 0)
    cmds.parent ('RIG_root_ctrl_HIDDEN', 'RIG_RIG_root_ctrl_frz_grp')
    cmds.delete ('group1')
    cmds.group(em=True, name='Transform_Null')
    cmds.parent ('Transform_Null', 'Main_Ctrl')
    cmds.rename ('RIG_root_ctrl_offset_group', 'Transform_Ctrl_Grp')
    cmds.rename ('RIG_RIG_subMaster_ctrl_frz_grp', 'Main_Ctrl_Grp')
    cmds.rename ('RIG_RIG_master_ctrl_frz_grp', 'Root_Ctrl_Grp')
    cmds.parent("Transform_Pivot_Ctrl_Grp", "Main_Ctrl")
    cmds.setAttr("Transform_Pivot_Ctrl_Grp.tz", 0)
    cmds.parentConstraint ('Transform_Null', 'RIG_RIG_root_ctrl_frz_grp', mo=True)
    cmds.setAttr ('Transform_Ctrl.Main_Ctrl_Vis', 0)
    cmds.setAttr ('Transform_Ctrl.Root_Ctrl_Vis', 0)

    cmds.setAttr("Transform_Ctrl.sx", k=True, l=False)
    cmds.setAttr("Transform_Ctrl.sy", k=True, l=False)
    cmds.setAttr("Transform_Ctrl.sz", k=True, l=False)
    cmds.connectAttr ('Transform_Ctrl.masterScale', 'Transform_Ctrl.sx')
    cmds.connectAttr ('Transform_Ctrl.masterScale', 'Transform_Ctrl.sy')
    cmds.connectAttr ('Transform_Ctrl.masterScale', 'Transform_Ctrl.sz')
    cmds.setAttr("Transform_Ctrl.sx", k=False, l=True)
    cmds.setAttr("Transform_Ctrl.sy", k=False, l=True)
    cmds.setAttr("Transform_Ctrl.sz", k=False, l=True)
    cmds.disconnectAttr ('RIG_head_a_jnt.scale', 'RIG_head_rig_grp.scale')

    cmds.parent("Transform_Ctrl_Grp", "RIG_rig_grp")
    cmds.parent("Root_Ctrl_Grp", "Transform_Ctrl")

    cmds.select ( cl=True )
    #mettere se necessario il valore inverso di Tz sul 'Transform Null' che leggi dal 'RIG_RIG_root_ctrl_frz_grp'

    #remove unuseful things
    cmds.delete ('RIG_RIG_head_b_ctrl_frz_grp')
    cmds.delete ('RIG_RIG_head_c_ctrl_frz_grp')
    cmds.delete ('RIG_jawCtrl_grp')
    cmds.parent ('RIG_head_d_jnt', 'RIG_head_a_jnt')
    cmds.parent ('RIG_lf_eyeMover_jnt', 'RIG_head_a_jnt')
    cmds.parent ('RIG_rt_eyeMover_jnt', 'RIG_head_a_jnt')
    cmds.delete ('RIG_head_b_jnt')
    cmds.delete ('RIG_jaw_a_jnt')
    cmds.setAttr ("RIG_eyeMaster_ctrl.head_bCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_rt_eye_ctrl.eyeMoverCtrlVis", 1)
    cmds.setAttr ("RIG_lf_eye_ctrl.eyeMoverCtrlVis", 1)
    cmds.parent ('RIG_RIG_lf_eyeMover_ctrl_frz_grp', 'RIG_head_ctrl')
    cmds.setAttr("RIG_lf_eyeMover_ctrlShape.overrideColor", 17)
    cmds.parent ('RIG_RIG_rt_eyeMover_ctrl_frz_grp', 'RIG_head_ctrl')
    cmds.setAttr("RIG_rt_eyeMover_ctrlShape.overrideColor", 17)
    cmds.setAttr ("RIG_eyeMaster_ctrl.rootCtrl", 1)
    cmds.select ( cl=True )

    #fix IK handL attrs
    cmds.setAttr ("RIG_lf_hand_ctrl.rubberHoseCtrlVis", 1)
    cmds.setAttr ("RIG_lf_hand_ctrl.autoSquashStretch", 0)
    cmds.select ( "RIG_lf_arm_ik_ctrl" )
    cmds.addAttr( shortName='asl', longName='armStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='esl', longName='elbowStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='wsl', longName='wristStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='esls', longName='elbowSoftness', at="float", h=False, k=True)
    cmds.connectAttr ('RIG_lf_arm_ik_ctrl.armStretch', 'RIG_lf_hand_ctrl.armStretch')
    cmds.connectAttr ('RIG_lf_arm_ik_ctrl.elbowStretch', 'RIG_lf_hand_ctrl.elbowStretch')
    cmds.connectAttr ('RIG_lf_arm_ik_ctrl.wristStretch', 'RIG_lf_hand_ctrl.wristStretch')
    cmds.connectAttr ('RIG_lf_arm_ik_ctrl.elbowSoftness', 'RIG_lf_hand_ctrl.elbowSoftness')
    cmds.setAttr ("RIG_lf_hand_ctrl.armStretch", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_lf_hand_ctrl.elbowStretch", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_lf_hand_ctrl.wristStretch", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_lf_hand_ctrl.elbowSoftness", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_lf_hand_ctrl.tx", lock=False, keyable=True)
    cmds.setAttr ("RIG_lf_hand_ctrl.ty", lock=False, keyable=True)
    cmds.setAttr ("RIG_lf_hand_ctrl.tz", lock=False, keyable=True)
    cmds.setAttr ("RIG_lf_hand_ctrl.sx", lock=False, keyable=True)
    cmds.setAttr ("RIG_lf_hand_ctrl.sy", lock=False, keyable=True)
    cmds.setAttr ("RIG_lf_hand_ctrl.sz", lock=False, keyable=True)
    cmds.setAttr ("RIG_lf_arm_ik_ctrl.armTwist", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_lf_arm_ik_ctrl.pvControl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_lf_arm_ik_ctrl.maxStretch", 10)
    #mel.eval('dynRenameMaxCheckChanged false "RIG_lf_arm_ik_ctrl.maxStretch";')
    cmds.select ( cl=True )

    #fix IK handR attrs
    cmds.setAttr ("RIG_rt_hand_ctrl.rubberHoseCtrlVis", 1)
    cmds.setAttr ("RIG_rt_hand_ctrl.autoSquashStretch", 0)
    cmds.select ( "RIG_rt_arm_ik_ctrl" )
    cmds.addAttr( shortName='asr', longName='armStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='esr', longName='elbowStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='wsr', longName='wristStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='esrs', longName='elbowSoftness', at="float", h=False, k=True)
    cmds.connectAttr ('RIG_rt_arm_ik_ctrl.armStretch', 'RIG_rt_hand_ctrl.armStretch')
    cmds.connectAttr ('RIG_rt_arm_ik_ctrl.elbowStretch', 'RIG_rt_hand_ctrl.elbowStretch')
    cmds.connectAttr ('RIG_rt_arm_ik_ctrl.wristStretch', 'RIG_rt_hand_ctrl.wristStretch')
    cmds.connectAttr ('RIG_rt_arm_ik_ctrl.elbowSoftness', 'RIG_rt_hand_ctrl.elbowSoftness')
    cmds.setAttr ("RIG_rt_hand_ctrl.armStretch", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_rt_hand_ctrl.elbowStretch", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_rt_hand_ctrl.wristStretch", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_rt_hand_ctrl.elbowSoftness", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_rt_hand_ctrl.tx", lock=False, keyable=True)
    cmds.setAttr ("RIG_rt_hand_ctrl.ty", lock=False, keyable=True)
    cmds.setAttr ("RIG_rt_hand_ctrl.tz", lock=False, keyable=True)
    cmds.setAttr ("RIG_rt_hand_ctrl.sx", lock=False, keyable=True)
    cmds.setAttr ("RIG_rt_hand_ctrl.sy", lock=False, keyable=True)
    cmds.setAttr ("RIG_rt_hand_ctrl.sz", lock=False, keyable=True)
    cmds.setAttr ("RIG_rt_arm_ik_ctrl.armTwist", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_rt_arm_ik_ctrl.pvControl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_rt_arm_ik_ctrl.maxStretch", 10)
    #mel.eval('dynRenameMaxCheckChanged false "RIG_rt_arm_ik_ctrl.maxStretch";')
    cmds.select ( cl=True )

    #fix IK ankleL attrs
    cmds.setAttr ("RIG_lf_foot_ctrl.rubberHoseCtrlVis", 1)
    cmds.setAttr ("RIG_lf_foot_ctrl.autoSquashStretch", 0)
    cmds.select ( "RIG_lf_heel_ik_ctrl" )
    cmds.addAttr( shortName='lsl', longName='legStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='ksl', longName='kneeStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='asl', longName='ankleStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='ksls', longName='kneeSoftness', at="float", h=False, k=True)
    cmds.connectAttr ('RIG_lf_heel_ik_ctrl.legStretch', 'RIG_lf_foot_ctrl.legStretch')
    cmds.connectAttr ('RIG_lf_heel_ik_ctrl.kneeStretch', 'RIG_lf_foot_ctrl.kneeStretch')
    cmds.connectAttr ('RIG_lf_heel_ik_ctrl.ankleStretch', 'RIG_lf_foot_ctrl.ankleStretch')
    cmds.connectAttr ('RIG_lf_heel_ik_ctrl.kneeSoftness', 'RIG_lf_foot_ctrl.kneeSoftness')
    cmds.setAttr ("GRP_RIG_lf_foot_ctrl.v", lock=True)
    cmds.setAttr ("RIG_lf_heel_ik_ctrl.pvControl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_lf_heel_ik_ctrl.maxStretch", 10)
    #mel.eval('dynRenameMaxCheckChanged false "RIG_lf_heel_ik_ctrl.maxStretch";')
    cmds.select ( cl=True )

    #fix IK ankleR attrs
    cmds.setAttr ("RIG_rt_foot_ctrl.rubberHoseCtrlVis", 1)
    cmds.setAttr ("RIG_rt_foot_ctrl.autoSquashStretch", 0)
    cmds.select ( "RIG_rt_heel_ik_ctrl" )
    cmds.addAttr( shortName='lsr', longName='legStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='ksr', longName='kneeStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='asr', longName='ankleStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='ksrs', longName='kneeSoftness', at="float", h=False, k=True)
    cmds.connectAttr ('RIG_rt_heel_ik_ctrl.legStretch', 'RIG_rt_foot_ctrl.legStretch')
    cmds.connectAttr ('RIG_rt_heel_ik_ctrl.kneeStretch', 'RIG_rt_foot_ctrl.kneeStretch')
    cmds.connectAttr ('RIG_rt_heel_ik_ctrl.ankleStretch', 'RIG_rt_foot_ctrl.ankleStretch')
    cmds.connectAttr ('RIG_rt_heel_ik_ctrl.kneeSoftness', 'RIG_rt_foot_ctrl.kneeSoftness')
    cmds.setAttr ("GRP_RIG_rt_foot_ctrl.v", lock=True)
    cmds.setAttr ("RIG_rt_heel_ik_ctrl.pvControl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_rt_heel_ik_ctrl.maxStretch", 10)
    #mel.eval('dynRenameMaxCheckChanged false "RIG_rt_heel_ik_ctrl.maxStretch";')
    cmds.select ( cl=True )

    #clavicleL fixed
    cmds.setAttr ( "RIG_lf_clavicle_ctrl.clavicleTransCtrlVis", 1)
    cmds.setAttr ("RIG_lf_clavicleTrans_ctrl.rx", lock=False, keyable=True )
    cmds.setAttr ("RIG_lf_clavicleTrans_ctrl.ry", lock=False, keyable=True )
    cmds.setAttr ("RIG_lf_clavicleTrans_ctrl.rz", lock=False, keyable=True )
    cmds.delete ('RIG_RIG_lf_clavicle_ctrl_frz_grp1_pointConstraint1')
    cmds.parentConstraint ('RIG_lf_clavicleTrans_ctrl', 'RIG_RIG_lf_clavicle_ctrl_frz_grp1', mo=True)
    cmds.setAttr ("RIG_RIG_lf_clavicle_ctrl_frz_grp1.v", 0)
    cmds.setAttr ("RIG_RIG_lf_clavicle_ctrl_frz_grp1.v", lock=True)
    cmds.select ( cl=True )

    #clavicleR fixed
    cmds.setAttr ( "RIG_rt_clavicle_ctrl.clavicleTransCtrlVis", 1)
    cmds.setAttr ("RIG_rt_clavicleTrans_ctrl.rx", lock=False, keyable=True )
    cmds.setAttr ("RIG_rt_clavicleTrans_ctrl.ry", lock=False, keyable=True )
    cmds.setAttr ("RIG_rt_clavicleTrans_ctrl.rz", lock=False, keyable=True )
    cmds.delete ('RIG_RIG_rt_clavicle_ctrl_frz_grp1_pointConstraint1')
    cmds.setAttr("RIG_RIG_rt_clavicleTrans_ctrl_frz_grp.rx", 0)
    cmds.parentConstraint ('RIG_rt_clavicleTrans_ctrl', 'RIG_RIG_rt_clavicle_ctrl_frz_grp1', mo=True)
    cmds.setAttr ("RIG_RIG_rt_clavicle_ctrl_frz_grp1.v", 0)
    cmds.setAttr ("RIG_RIG_rt_clavicle_ctrl_frz_grp1.v", lock=True)
    cmds.select ( cl=True )

    #add scale attribute to hip and mid spine controls
    cmds.setAttr("RIG_hip_ctrl.sx",k=True,lock=False);
    cmds.setAttr("RIG_hip_ctrl.sy",k=True,lock=False);
    cmds.setAttr("RIG_hip_ctrl.sz",k=True,lock=False);

    cmds.setAttr("RIG_spineMid_1_ctrl.sx",k=True,lock=False);
    cmds.setAttr("RIG_spineMid_1_ctrl.sy",k=True,lock=False);
    cmds.setAttr("RIG_spineMid_1_ctrl.sz",k=True,lock=False);

    cmds.scaleConstraint("RIG_hip_ctrl", "RIG_hip_jnt", mo=True, w=1.0);
    cmds.setAttr("RIG_hip_jnt.segmentScaleCompensate",0);
    cmds.parentConstraint("RIG_spineMid_1_ctrl", "weightSpine_grp", mo=True, w=1.0);
    cmds.scaleConstraint("RIG_spineMid_1_ctrl", "weightSpine_grp", mo=True, w=1.0);

    #fix spine follicles
    try:
        cmds.select("RIG_root_ctrl_HIDDEN");
    except:
        cmds.warning("RIG_root_ctrl_HIDDEN not found, using RIG_root_ctrl instead");
        cmds.select("RIG_root_ctrl");
        
    root_control = cmds.ls(sl=True)[0];
    spine_follicles = cmds.listRelatives("RIG_spineRhFollicles_grp",c=True);
    for follicle in spine_follicles:
        cmds.scaleConstraint(root_control, follicle, mo=True, w=1.0)

    #name follicles
    cmds.rename ("follicle1", "FLC_spine_01")
    cmds.rename ("follicle2", "FLC_spine_02")
    cmds.rename ("follicle3", "FLC_spine_03")
    cmds.rename ("follicle4", "FLC_spine_04")
    cmds.rename ("follicle5", "FLC_spine_05")
    cmds.rename ("follicle6", "FLC_spine_06")

    cmds.select ( cl=True )

    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #ALIGN FOLLOW SWITCH
    #Sostituisce l'attributo align con un attributo follow con funzionamento inverso
    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////

    cmds.select ( "RIG_lf_wrist_fk_ctrl", "RIG_head_ctrl", "RIG_lf_upArm_fk_ctrl", "RIG_rt_upArm_fk_ctrl", "RIG_neck_ctrl", "RIG_rt_wrist_fk_ctrl", "RIG_lf_upLeg_fk_ctrl", "RIG_rt_upLeg_fk_ctrl" )

    controls = cmds.ls(sl=True);
    for control in controls:
        cmds.addAttr(control, ln= "follow", at="float", dv = 1, min = 0, max = 1);
        cmds.setAttr(control + ".follow", e=True, keyable=True);
        reverse_node = cmds.shadingNode("reverse", asUtility = True, n="REV_align_" + control);
        cmds.connectAttr(control + ".follow", reverse_node + ".input.inputX");
        cmds.connectAttr(reverse_node + ".output.outputX", control + ".align");
        cmds.setAttr(control + ".align", e=True, keyable = False, lock=True, channelBox = False);
        cmds.select ( cl=True )
            
    cmds.select ( cl=True )

    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #SPACE SWITCH FIXED
    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////

    # Lanciare tutto lo script 
    # Creo gli attributi Follow sui controlli
    heel = ["RIG_lf_heel_ik_ctrl", "RIG_rt_heel_ik_ctrl"]
    cmds.select(heel)
    cmds.addAttr(ln="Follow", at="enum", en="World:Transform:Cog", dv=1)
    cmds.setAttr("RIG_lf_heel_ik_ctrl.Follow", k=True)
    cmds.setAttr("RIG_rt_heel_ik_ctrl.Follow", k=True)
    cmds.select(cl=True)

    knee = ["RIG_lf_knee_pv_ctrl", "RIG_rt_knee_pv_ctrl"]
    cmds.select(knee)
    cmds.addAttr(ln="Follow", at="enum", en="World:Transform:Cog:Hip:IKCtrl", dv=1)
    cmds.setAttr("RIG_lf_knee_pv_ctrl.Follow", k=True)
    cmds.setAttr("RIG_rt_knee_pv_ctrl.Follow", k=True)
    cmds.select(cl=True)

    arm = ["RIG_lf_arm_ik_ctrl", "RIG_rt_arm_ik_ctrl"]
    cmds.select(arm)
    cmds.addAttr(ln="Follow", at="enum", en="World:Transform:Cog:LowSpine:HighSpine:MasterSpine:Head", dv=1)
    cmds.setAttr("RIG_lf_arm_ik_ctrl.Follow", k=True)
    cmds.setAttr("RIG_rt_arm_ik_ctrl.Follow", k=True)
    cmds.select(cl=True)

    elbow = ["RIG_lf_elbow_pv_ctrl", "RIG_rt_elbow_pv_ctrl"]
    cmds.select(elbow)
    cmds.addAttr(ln="Follow", at="enum", en="World:Transform:Cog:LowSpine:HighSpine:MasterSpine:IKCtrl", dv=1)
    cmds.setAttr("RIG_lf_elbow_pv_ctrl.Follow", k=True)
    cmds.setAttr("RIG_rt_elbow_pv_ctrl.Follow", k=True)
    cmds.select(cl=True)

    eye = ["RIG_eyeMaster_ctrl"]
    cmds.select(eye)
    cmds.addAttr(ln="Follow", at="enum", en="World:Transform:Cog:Head", dv=1)
    cmds.setAttr("RIG_eyeMaster_ctrl.Follow", k=True)
    cmds.select(cl=True)

    # Creo e collego i condition per le gambe
    heel_L_World = cmds.createNode("condition", n="CND_lf_heel_ik_Follow_World")
    heel_L_Transform = cmds.createNode("condition", n="CND_lf_heel_ik_Follow_Transform")
    heel_L_Cog = cmds.createNode("condition", n="CND_lf_heel_ik_Follow_Cog")
    heel_R_World = cmds.createNode("condition", n="CND_rt_heel_ik_Follow_World")
    heel_R_Transform = cmds.createNode("condition", n="CND_rt_heel_ik_Follow_Transform")
    heel_R_Cog = cmds.createNode("condition", n="CND_rt_heel_ik_Follow_Cog")

    heelCND = [heel_L_World, heel_L_Transform, heel_L_Cog, heel_R_World, heel_R_Transform, heel_R_Cog]

    for heels in heelCND:
        cmds.setAttr(heels + ".colorIfTrueR", 1)
        cmds.setAttr(heels + ".colorIfTrueG", 1)
        cmds.setAttr(heels + ".colorIfTrueB", 1)
        cmds.setAttr(heels + ".colorIfFalseR", 0)
        cmds.setAttr(heels + ".colorIfFalseG", 0)
        cmds.setAttr(heels + ".colorIfFalseB", 0)
        
    cmds.setAttr(heel_L_Transform + ".secondTerm", 1)
    cmds.setAttr(heel_L_Cog + ".secondTerm", 2)
    cmds.setAttr(heel_R_Transform + ".secondTerm", 1)
    cmds.setAttr(heel_R_Cog + ".secondTerm", 2)

    cmds.connectAttr("RIG_lf_heel_ik_ctrl.Follow", heel_L_World + ".firstTerm")
    cmds.connectAttr("RIG_lf_heel_ik_ctrl.Follow", heel_L_Transform + ".firstTerm")
    cmds.connectAttr("RIG_lf_heel_ik_ctrl.Follow", heel_L_Cog + ".firstTerm")
    cmds.connectAttr("RIG_rt_heel_ik_ctrl.Follow", heel_R_World + ".firstTerm")
    cmds.connectAttr("RIG_rt_heel_ik_ctrl.Follow", heel_R_Transform + ".firstTerm")
    cmds.connectAttr("RIG_rt_heel_ik_ctrl.Follow", heel_R_Cog + ".firstTerm")

    cmds.connectAttr(heel_L_World + ".outColorR", "RIG_lf_heel_ik_ctrl.worldSpaceCtrl")
    cmds.connectAttr(heel_L_Transform + ".outColorR", "RIG_lf_heel_ik_ctrl.rootCtrl")
    cmds.connectAttr(heel_L_Cog + ".outColorR", "RIG_lf_heel_ik_ctrl.cogCtrl")
    cmds.connectAttr(heel_R_World + ".outColorR", "RIG_rt_heel_ik_ctrl.worldSpaceCtrl")
    cmds.connectAttr(heel_R_Transform + ".outColorR", "RIG_rt_heel_ik_ctrl.rootCtrl")
    cmds.connectAttr(heel_R_Cog + ".outColorR", "RIG_rt_heel_ik_ctrl.cogCtrl")

    # Creo e collego i condition per i pole vector delle gambe
    knee_L_World = cmds.createNode("condition", n="CND_lf_knee_ik_Follow_World")
    knee_L_Transform = cmds.createNode("condition", n="CND_lf_knee_ik_Follow_Transform")
    knee_L_Cog = cmds.createNode("condition", n="CND_lf_knee_ik_Follow_Cog")
    knee_L_Hip = cmds.createNode("condition", n="CND_lf_knee_ik_Follow_Hip")
    knee_L_IKCtrl = cmds.createNode("condition", n="CND_lf_knee_ik_Follow_IKCtrl")
    knee_R_World = cmds.createNode("condition", n="CND_rt_knee_ik_Follow_World")
    knee_R_Transform = cmds.createNode("condition", n="CND_rt_knee_ik_Follow_Transform")
    knee_R_Cog = cmds.createNode("condition", n="CND_rt_knee_ik_Follow_Cog")
    knee_R_Hip = cmds.createNode("condition", n="CND_rt_knee_ik_Follow_Hip")
    knee_R_IKCtrl = cmds.createNode("condition", n="CND_rt_knee_ik_Follow_IKCtrl")

    kneeCND = [knee_L_World, knee_L_Transform, knee_L_Cog, knee_L_Hip, knee_L_IKCtrl, knee_R_World, knee_R_Transform, knee_R_Cog, knee_R_Hip, knee_R_IKCtrl]

    for knees in kneeCND:
        cmds.setAttr(knees + ".colorIfTrueR", 1)
        cmds.setAttr(knees + ".colorIfTrueG", 1)
        cmds.setAttr(knees + ".colorIfTrueB", 1)
        cmds.setAttr(knees + ".colorIfFalseR", 0)
        cmds.setAttr(knees + ".colorIfFalseG", 0)
        cmds.setAttr(knees + ".colorIfFalseB", 0)
        
    cmds.setAttr(knee_L_Transform + ".secondTerm", 1)
    cmds.setAttr(knee_L_Cog + ".secondTerm", 2)
    cmds.setAttr(knee_L_Hip + ".secondTerm", 3)
    cmds.setAttr(knee_L_IKCtrl + ".secondTerm", 4)
    cmds.setAttr(knee_R_Transform + ".secondTerm", 1)
    cmds.setAttr(knee_R_Cog + ".secondTerm", 2)
    cmds.setAttr(knee_R_Hip + ".secondTerm", 3)
    cmds.setAttr(knee_R_IKCtrl + ".secondTerm", 4)

    cmds.connectAttr("RIG_lf_knee_pv_ctrl.Follow", knee_L_World + ".firstTerm")
    cmds.connectAttr("RIG_lf_knee_pv_ctrl.Follow", knee_L_Transform + ".firstTerm")
    cmds.connectAttr("RIG_lf_knee_pv_ctrl.Follow", knee_L_Cog + ".firstTerm")
    cmds.connectAttr("RIG_lf_knee_pv_ctrl.Follow", knee_L_Hip + ".firstTerm")
    cmds.connectAttr("RIG_lf_knee_pv_ctrl.Follow", knee_L_IKCtrl + ".firstTerm")
    cmds.connectAttr("RIG_rt_knee_pv_ctrl.Follow", knee_R_World + ".firstTerm")
    cmds.connectAttr("RIG_rt_knee_pv_ctrl.Follow", knee_R_Transform + ".firstTerm")
    cmds.connectAttr("RIG_rt_knee_pv_ctrl.Follow", knee_R_Cog + ".firstTerm")
    cmds.connectAttr("RIG_rt_knee_pv_ctrl.Follow", knee_R_Hip + ".firstTerm")
    cmds.connectAttr("RIG_rt_knee_pv_ctrl.Follow", knee_R_IKCtrl + ".firstTerm")

    cmds.connectAttr(knee_L_World + ".outColorR", "RIG_lf_knee_pv_ctrl.worldSpaceCtrl")
    cmds.connectAttr(knee_L_Transform + ".outColorR", "RIG_lf_knee_pv_ctrl.rootCtrl")
    cmds.connectAttr(knee_L_Cog + ".outColorR", "RIG_lf_knee_pv_ctrl.cogCtrl")
    cmds.connectAttr(knee_L_Hip + ".outColorR", "RIG_lf_knee_pv_ctrl.hipCtrl")
    cmds.connectAttr(knee_L_IKCtrl + ".outColorR", "RIG_lf_knee_pv_ctrl.lfLegIkCtrl")
    cmds.connectAttr(knee_R_World + ".outColorR", "RIG_rt_knee_pv_ctrl.worldSpaceCtrl")
    cmds.connectAttr(knee_R_Transform + ".outColorR", "RIG_rt_knee_pv_ctrl.rootCtrl")
    cmds.connectAttr(knee_R_Cog + ".outColorR", "RIG_rt_knee_pv_ctrl.cogCtrl")
    cmds.connectAttr(knee_R_Hip + ".outColorR", "RIG_rt_knee_pv_ctrl.hipCtrl")
    cmds.connectAttr(knee_R_IKCtrl + ".outColorR", "RIG_rt_knee_pv_ctrl.rtLegIkCtrl")

    # Creo e collego i condition per le braccia
    arm_L_World = cmds.createNode("condition", n="CND_lf_arm_ik_Follow_World")
    arm_L_Transform = cmds.createNode("condition", n="CND_lf_arm_ik_Follow_Transform")
    arm_L_Cog = cmds.createNode("condition", n="CND_lf_arm_ik_Follow_Cog")
    arm_L_LowSpine = cmds.createNode("condition", n="CND_lf_arm_ik_Follow_LowSpine")
    arm_L_HighSpine = cmds.createNode("condition", n="CND_lf_arm_ik_Follow_HighSpine")
    arm_L_MasterSpine = cmds.createNode("condition", n="CND_lf_arm_ik_Follow_MasterSpine")
    arm_L_Head = cmds.createNode("condition", n="CND_lf_arm_ik_Follow_Head")
    arm_R_World = cmds.createNode("condition", n="CND_rt_arm_ik_Follow_World")
    arm_R_Transform = cmds.createNode("condition", n="CND_rt_arm_ik_Follow_Transform")
    arm_R_Cog = cmds.createNode("condition", n="CND_rt_arm_ik_Follow_Cog")
    arm_R_LowSpine = cmds.createNode("condition", n="CND_rt_arm_ik_Follow_LowSpine")
    arm_R_HighSpine = cmds.createNode("condition", n="CND_rt_arm_ik_Follow_HighSpine")
    arm_R_MasterSpine = cmds.createNode("condition", n="CND_rt_arm_ik_Follow_MasterSpine")
    arm_R_Head = cmds.createNode("condition", n="CND_rt_arm_ik_Follow_Head")

    armCND = [arm_L_World, arm_L_Transform, arm_L_Cog, arm_L_LowSpine, arm_L_HighSpine, arm_L_MasterSpine, arm_L_Head, arm_R_World, arm_R_Transform, arm_R_Cog, arm_R_LowSpine, arm_R_HighSpine, arm_R_MasterSpine, arm_R_Head]

    for arms in armCND:
        cmds.setAttr(arms + ".colorIfTrueR", 1)
        cmds.setAttr(arms + ".colorIfTrueG", 1)
        cmds.setAttr(arms + ".colorIfTrueB", 1)
        cmds.setAttr(arms + ".colorIfFalseR", 0)
        cmds.setAttr(arms + ".colorIfFalseG", 0)
        cmds.setAttr(arms + ".colorIfFalseB", 0)
        
    cmds.setAttr(arm_L_Transform + ".secondTerm", 1)
    cmds.setAttr(arm_L_Cog + ".secondTerm", 2)
    cmds.setAttr(arm_L_LowSpine + ".secondTerm", 3)
    cmds.setAttr(arm_L_HighSpine + ".secondTerm", 4)
    cmds.setAttr(arm_L_MasterSpine + ".secondTerm", 5)
    cmds.setAttr(arm_L_Head + ".secondTerm", 6)
    cmds.setAttr(arm_R_Transform + ".secondTerm", 1)
    cmds.setAttr(arm_R_Cog + ".secondTerm", 2)
    cmds.setAttr(arm_R_LowSpine + ".secondTerm", 3)
    cmds.setAttr(arm_R_HighSpine + ".secondTerm", 4)
    cmds.setAttr(arm_R_MasterSpine + ".secondTerm", 5)
    cmds.setAttr(arm_R_Head + ".secondTerm", 6)

    cmds.connectAttr("RIG_lf_arm_ik_ctrl.Follow", arm_L_World + ".firstTerm")
    cmds.connectAttr("RIG_lf_arm_ik_ctrl.Follow", arm_L_Transform + ".firstTerm")
    cmds.connectAttr("RIG_lf_arm_ik_ctrl.Follow", arm_L_Cog + ".firstTerm")
    cmds.connectAttr("RIG_lf_arm_ik_ctrl.Follow", arm_L_LowSpine + ".firstTerm")
    cmds.connectAttr("RIG_lf_arm_ik_ctrl.Follow", arm_L_HighSpine + ".firstTerm")
    cmds.connectAttr("RIG_lf_arm_ik_ctrl.Follow", arm_L_MasterSpine + ".firstTerm")
    cmds.connectAttr("RIG_lf_arm_ik_ctrl.Follow", arm_L_Head + ".firstTerm")
    cmds.connectAttr("RIG_rt_arm_ik_ctrl.Follow", arm_R_World + ".firstTerm")
    cmds.connectAttr("RIG_rt_arm_ik_ctrl.Follow", arm_R_Transform + ".firstTerm")
    cmds.connectAttr("RIG_rt_arm_ik_ctrl.Follow", arm_R_Cog + ".firstTerm")
    cmds.connectAttr("RIG_rt_arm_ik_ctrl.Follow", arm_R_LowSpine + ".firstTerm")
    cmds.connectAttr("RIG_rt_arm_ik_ctrl.Follow", arm_R_HighSpine + ".firstTerm")
    cmds.connectAttr("RIG_rt_arm_ik_ctrl.Follow", arm_R_MasterSpine + ".firstTerm")
    cmds.connectAttr("RIG_rt_arm_ik_ctrl.Follow", arm_R_Head + ".firstTerm")

    cmds.connectAttr(arm_L_World + ".outColorR", "RIG_lf_arm_ik_ctrl.worldSpaceCtrl")
    cmds.connectAttr(arm_L_Transform + ".outColorR", "RIG_lf_arm_ik_ctrl.rootCtrl")
    cmds.connectAttr(arm_L_Cog + ".outColorR", "RIG_lf_arm_ik_ctrl.cogCtrl")
    cmds.connectAttr(arm_L_LowSpine + ".outColorR", "RIG_lf_arm_ik_ctrl.lowSpineCtrl")
    cmds.connectAttr(arm_L_HighSpine + ".outColorR", "RIG_lf_arm_ik_ctrl.hiSpineCtrl")
    cmds.connectAttr(arm_L_MasterSpine + ".outColorR", "RIG_lf_arm_ik_ctrl.masterSpineCtrl")
    cmds.connectAttr(arm_L_Head + ".outColorR", "RIG_lf_arm_ik_ctrl.headCtrl")
    cmds.connectAttr(arm_R_World + ".outColorR", "RIG_rt_arm_ik_ctrl.worldSpaceCtrl")
    cmds.connectAttr(arm_R_Transform + ".outColorR", "RIG_rt_arm_ik_ctrl.rootCtrl")
    cmds.connectAttr(arm_R_Cog + ".outColorR", "RIG_rt_arm_ik_ctrl.cogCtrl")
    cmds.connectAttr(arm_R_LowSpine + ".outColorR", "RIG_rt_arm_ik_ctrl.lowSpineCtrl")
    cmds.connectAttr(arm_R_HighSpine + ".outColorR", "RIG_rt_arm_ik_ctrl.hiSpineCtrl")
    cmds.connectAttr(arm_R_MasterSpine + ".outColorR", "RIG_rt_arm_ik_ctrl.masterSpineCtrl")
    cmds.connectAttr(arm_R_Head + ".outColorR", "RIG_rt_arm_ik_ctrl.headCtrl")

    # Creo e collego i condition per i pole vector delle braccia
    elbow_L_World = cmds.createNode("condition", n="CND_lf_elbow_ik_Follow_World")
    elbow_L_Transform = cmds.createNode("condition", n="CND_lf_elbow_ik_Follow_Transform")
    elbow_L_Cog = cmds.createNode("condition", n="CND_lf_elbow_ik_Follow_Cog")
    elbow_L_LowSpine = cmds.createNode("condition", n="CND_lf_elbow_ik_Follow_LowSpine")
    elbow_L_HighSpine = cmds.createNode("condition", n="CND_lf_elbow_ik_Follow_HighSpine")
    elbow_L_MasterSpine = cmds.createNode("condition", n="CND_lf_elbow_ik_Follow_MasterSpine")
    elbow_L_IKCtrl = cmds.createNode("condition", n="CND_lf_elbow_ik_Follow_IKCtrl")
    elbow_R_World = cmds.createNode("condition", n="CND_rt_elbow_ik_Follow_World")
    elbow_R_Transform = cmds.createNode("condition", n="CND_rt_elbow_ik_Follow_Transform")
    elbow_R_Cog = cmds.createNode("condition", n="CND_rt_elbow_ik_Follow_Cog")
    elbow_R_LowSpine = cmds.createNode("condition", n="CND_rt_elbow_ik_Follow_LowSpine")
    elbow_R_HighSpine = cmds.createNode("condition", n="CND_rt_elbow_ik_Follow_HighSpine")
    elbow_R_MasterSpine = cmds.createNode("condition", n="CND_rt_elbowik_Follow_MasterSpine")
    elbow_R_IKCtrl = cmds.createNode("condition", n="CND_rt_elbow_ik_Follow_IKCtrl")

    elbowCND = [elbow_L_World, elbow_L_Transform, elbow_L_Cog, elbow_L_LowSpine, elbow_L_HighSpine, elbow_L_MasterSpine, elbow_L_IKCtrl, elbow_R_World, elbow_R_Transform, elbow_R_Cog, elbow_R_LowSpine, elbow_R_HighSpine, elbow_R_MasterSpine, elbow_R_IKCtrl]

    for elbows in elbowCND:
        cmds.setAttr(elbows + ".colorIfTrueR", 1)
        cmds.setAttr(elbows + ".colorIfTrueG", 1)
        cmds.setAttr(elbows + ".colorIfTrueB", 1)
        cmds.setAttr(elbows + ".colorIfFalseR", 0)
        cmds.setAttr(elbows + ".colorIfFalseG", 0)
        cmds.setAttr(elbows + ".colorIfFalseB", 0)

    cmds.setAttr(elbow_L_Transform + ".secondTerm", 1)
    cmds.setAttr(elbow_L_Cog + ".secondTerm", 2)
    cmds.setAttr(elbow_L_LowSpine + ".secondTerm", 3)
    cmds.setAttr(elbow_L_HighSpine + ".secondTerm", 4)
    cmds.setAttr(elbow_L_MasterSpine + ".secondTerm", 5)
    cmds.setAttr(elbow_L_IKCtrl + ".secondTerm", 6)
    cmds.setAttr(elbow_R_Transform + ".secondTerm", 1)
    cmds.setAttr(elbow_R_Cog + ".secondTerm", 2)
    cmds.setAttr(elbow_R_LowSpine + ".secondTerm", 3)
    cmds.setAttr(elbow_R_HighSpine + ".secondTerm", 4)
    cmds.setAttr(elbow_R_MasterSpine + ".secondTerm", 5)
    cmds.setAttr(elbow_R_IKCtrl + ".secondTerm", 6)

    cmds.connectAttr("RIG_lf_elbow_pv_ctrl.Follow", elbow_L_World + ".firstTerm")
    cmds.connectAttr("RIG_lf_elbow_pv_ctrl.Follow", elbow_L_Transform + ".firstTerm")
    cmds.connectAttr("RIG_lf_elbow_pv_ctrl.Follow", elbow_L_Cog + ".firstTerm")
    cmds.connectAttr("RIG_lf_elbow_pv_ctrl.Follow", elbow_L_LowSpine + ".firstTerm")
    cmds.connectAttr("RIG_lf_elbow_pv_ctrl.Follow", elbow_L_HighSpine + ".firstTerm")
    cmds.connectAttr("RIG_lf_elbow_pv_ctrl.Follow", elbow_L_MasterSpine + ".firstTerm")
    cmds.connectAttr("RIG_lf_elbow_pv_ctrl.Follow", elbow_L_IKCtrl + ".firstTerm")
    cmds.connectAttr("RIG_rt_elbow_pv_ctrl.Follow", elbow_R_World + ".firstTerm")
    cmds.connectAttr("RIG_rt_elbow_pv_ctrl.Follow", elbow_R_Transform + ".firstTerm")
    cmds.connectAttr("RIG_rt_elbow_pv_ctrl.Follow", elbow_R_Cog + ".firstTerm")
    cmds.connectAttr("RIG_rt_elbow_pv_ctrl.Follow", elbow_R_LowSpine + ".firstTerm")
    cmds.connectAttr("RIG_rt_elbow_pv_ctrl.Follow", elbow_R_HighSpine + ".firstTerm")
    cmds.connectAttr("RIG_rt_elbow_pv_ctrl.Follow", elbow_R_MasterSpine + ".firstTerm")
    cmds.connectAttr("RIG_rt_elbow_pv_ctrl.Follow", elbow_R_IKCtrl + ".firstTerm")

    cmds.connectAttr(elbow_L_World + ".outColorR", "RIG_lf_elbow_pv_ctrl.worldSpaceCtrl")
    cmds.connectAttr(elbow_L_Transform + ".outColorR", "RIG_lf_elbow_pv_ctrl.rootCtrl")
    cmds.connectAttr(elbow_L_Cog + ".outColorR", "RIG_lf_elbow_pv_ctrl.cogCtrl")
    cmds.connectAttr(elbow_L_LowSpine + ".outColorR", "RIG_lf_elbow_pv_ctrl.lowSpineCtrl")
    cmds.connectAttr(elbow_L_HighSpine + ".outColorR", "RIG_lf_elbow_pv_ctrl.hiSpineCtrl")
    cmds.connectAttr(elbow_L_MasterSpine + ".outColorR", "RIG_lf_elbow_pv_ctrl.masterSpineCtrl")
    cmds.connectAttr(elbow_L_IKCtrl + ".outColorR", "RIG_lf_elbow_pv_ctrl.lfArmIkCtrl")
    cmds.connectAttr(elbow_R_World + ".outColorR", "RIG_rt_elbow_pv_ctrl.worldSpaceCtrl")
    cmds.connectAttr(elbow_R_Transform + ".outColorR", "RIG_rt_elbow_pv_ctrl.rootCtrl")
    cmds.connectAttr(elbow_R_Cog + ".outColorR", "RIG_rt_elbow_pv_ctrl.cogCtrl")
    cmds.connectAttr(elbow_R_LowSpine + ".outColorR", "RIG_rt_elbow_pv_ctrl.lowSpineCtrl")
    cmds.connectAttr(elbow_R_HighSpine + ".outColorR", "RIG_rt_elbow_pv_ctrl.hiSpineCtrl")
    cmds.connectAttr(elbow_R_MasterSpine + ".outColorR", "RIG_rt_elbow_pv_ctrl.masterSpineCtrl")
    cmds.connectAttr(elbow_R_IKCtrl + ".outColorR", "RIG_rt_elbow_pv_ctrl.rtArmIkCtrl")

    # Creo e collego i condition per gli occhi
    eye_L_World = cmds.createNode("condition", n="CND_eyeMaster_Follow_World")
    eye_L_Transform = cmds.createNode("condition", n="CND_eyeMaster_Follow_Transform")
    eye_L_Cog = cmds.createNode("condition", n="CND_eyeMaster_Follow_Cog")
    eye_L_Head = cmds.createNode("condition", n="CND_eyeMaster_Follow_Head")

    eyeCND = [eye_L_World, eye_L_Transform, eye_L_Cog, eye_L_Head]

    for eyes in eyeCND:
        cmds.setAttr(eyes + ".colorIfTrueR", 1)
        cmds.setAttr(eyes + ".colorIfTrueG", 1)
        cmds.setAttr(eyes + ".colorIfTrueB", 1)
        cmds.setAttr(eyes + ".colorIfFalseR", 0)
        cmds.setAttr(eyes + ".colorIfFalseG", 0)
        cmds.setAttr(eyes + ".colorIfFalseB", 0)
        cmds.connectAttr("RIG_eyeMaster_ctrl.Follow", eyes + ".firstTerm")

    cmds.setAttr(eye_L_Transform + ".secondTerm", 1)
    cmds.setAttr(eye_L_Cog + ".secondTerm", 2)
    cmds.setAttr(eye_L_Head + ".secondTerm", 3)

    cmds.connectAttr(eye_L_World + ".outColorR", "RIG_eyeMaster_ctrl.worldSpaceCtrl")
    cmds.connectAttr(eye_L_Transform + ".outColorR", "RIG_eyeMaster_ctrl.rootCtrl")
    cmds.connectAttr(eye_L_Cog + ".outColorR", "RIG_eyeMaster_ctrl.cogCtrl")
    cmds.connectAttr(eye_L_Head + ".outColorR", "RIG_eyeMaster_ctrl.headCtrl")

    cmds.setAttr("RIG_eyeMaster_ctrl.Follow", 3)

    #lockandhide connected attrs
    cmds.setAttr ("RIG_eyeMaster_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_eyeMaster_ctrl.rootCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_eyeMaster_ctrl.worldSpaceCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_eyeMaster_ctrl.cogCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_eyeMaster_ctrl.headCtrl", lock=True, keyable=False, channelBox=False)

    cmds.setAttr ("RIG_lf_elbow_pv_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_lf_elbow_pv_ctrl.rootCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_lf_elbow_pv_ctrl.worldSpaceCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_lf_elbow_pv_ctrl.cogCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_lf_elbow_pv_ctrl.lowSpineCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_lf_elbow_pv_ctrl.hiSpineCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_lf_elbow_pv_ctrl.masterSpineCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_lf_elbow_pv_ctrl.lfArmIkCtrl", lock=True, keyable=False, channelBox=False)

    cmds.setAttr ("RIG_rt_elbow_pv_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_rt_elbow_pv_ctrl.rootCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_rt_elbow_pv_ctrl.worldSpaceCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_rt_elbow_pv_ctrl.cogCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_rt_elbow_pv_ctrl.lowSpineCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_rt_elbow_pv_ctrl.hiSpineCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_rt_elbow_pv_ctrl.masterSpineCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_rt_elbow_pv_ctrl.rtArmIkCtrl", lock=True, keyable=False, channelBox=False)

    cmds.setAttr ("RIG_lf_knee_pv_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_lf_knee_pv_ctrl.rootCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_lf_knee_pv_ctrl.worldSpaceCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_lf_knee_pv_ctrl.cogCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_lf_knee_pv_ctrl.hipCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_lf_knee_pv_ctrl.lfLegIkCtrl", lock=True, keyable=False, channelBox=False)

    cmds.setAttr ("RIG_rt_knee_pv_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_rt_knee_pv_ctrl.rootCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_rt_knee_pv_ctrl.worldSpaceCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_rt_knee_pv_ctrl.cogCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_rt_knee_pv_ctrl.hipCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_rt_knee_pv_ctrl.rtLegIkCtrl", lock=True, keyable=False, channelBox=False)

    cmds.select ( cl=True )

    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #FIX_Translate_FK
    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////

    #delete all constraints on groups of each clavicle
    cmds.delete("RIG_lf_armFK_Grp_parentConstraint1")
    cmds.delete("RIG_lf_armIK_Grp_parentConstraint1")
    cmds.delete("RIG_lf_upArm_upJntFkCtrlAlign_grp_parentConstraint1")
    cmds.delete("RIG_RIG_lf_shoulder_ctrl_frz_grp_parentConstraint1")

    cmds.delete("RIG_rt_armFK_Grp_parentConstraint1")
    cmds.delete("RIG_rt_armIK_Grp_parentConstraint1")
    cmds.delete("RIG_rt_upArm_upJntFkCtrlAlign_grp_parentConstraint1")
    cmds.delete("RIG_RIG_rt_shoulder_ctrl_frz_grp_parentConstraint1")

    #do a "breakConnection" on the Cavicle joint's "RotateX"
    cmds.delete("RIG_lf_clavicle_jnt_parentConstraint1")
    cmds.parentConstraint("RIG_lf_clavicleTrans_ctrl", "RIG_lf_clavicle_jnt", mo=True, sr=["x"])

    cmds.delete("RIG_rt_clavicle_jnt_parentConstraint1")
    cmds.parentConstraint("RIG_rt_clavicleTrans_ctrl", "RIG_rt_clavicle_jnt", mo=True, sr=["x"])

    #delete all the constraints attached on groups and joints of legs and arms (joints FK e CTRL)
    cmds.delete ('RIG_RIG_lf_wrist_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_lf_wrist_fk_ctrl_frz_grp_pointConstraint1')
    cmds.delete ('RIG_RIG_lf_elbow_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_lf_elbow_fk_ctrl_frz_grp_pointConstraint1')
    cmds.delete ('RIG_RIG_lf_upArm_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_lf_upArm_fk_ctrl_frz_grp_pointConstraint1')

    cmds.delete ('RIG_RIG_rt_wrist_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_rt_wrist_fk_ctrl_frz_grp_pointConstraint1')
    cmds.delete ('RIG_RIG_rt_elbow_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_rt_elbow_fk_ctrl_frz_grp_pointConstraint1')
    cmds.delete ('RIG_RIG_rt_upArm_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_rt_upArm_fk_ctrl_frz_grp_pointConstraint1')

    cmds.delete ('RIG_RIG_lf_upLeg_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_lf_upLeg_fk_ctrl_frz_grp_pointConstraint1')
    cmds.delete ('RIG_RIG_lf_knee_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_lf_knee_fk_ctrl_frz_grp_pointConstraint1')
    cmds.delete ('RIG_RIG_lf_ankle_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_lf_ankle_fk_ctrl_frz_grp_pointConstraint1')

    cmds.delete ('RIG_RIG_rt_upLeg_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_rt_upLeg_fk_ctrl_frz_grp_pointConstraint1')
    cmds.delete ('RIG_RIG_rt_knee_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_rt_knee_fk_ctrl_frz_grp_pointConstraint1')
    cmds.delete ('RIG_RIG_rt_ankle_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_rt_ankle_fk_ctrl_frz_grp_pointConstraint1')

    cmds.delete ('RIG_lf_wrist_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_lf_elbow_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_lf_upArm_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_lf_upArm_fk_jnt_pointConstraint1')
    cmds.delete ('RIG_lf_upArm_jnt_pointConstraint1')

    cmds.delete ('RIG_rt_wrist_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_rt_elbow_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_rt_upArm_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_rt_upArm_fk_jnt_pointConstraint1')
    cmds.delete ('RIG_rt_upArm_jnt_pointConstraint1')

    cmds.delete ('RIG_lf_ankle_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_lf_knee_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_lf_upLeg_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_lf_upLeg_fk_jnt_pointConstraint1')
    cmds.delete ('RIG_lf_upLeg_jnt_pointConstraint1')

    cmds.delete ('RIG_rt_ankle_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_rt_knee_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_rt_upLeg_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_rt_upLeg_fk_jnt_pointConstraint1')
    cmds.delete ('RIG_rt_upLeg_jnt_pointConstraint1')

    #do a "breakConnection" on the FK joint's "TranslateX"
    cmds.disconnectAttr("RIG_lf_armTrans_blend.outputR", "RIG_lf_elbow_jnt.tx")
    cmds.disconnectAttr("RIG_lf_armTrans_blend.outputG", "RIG_lf_wrist_jnt.tx")
    cmds.disconnectAttr("RIG_rt_armTrans_blend.outputR", "RIG_rt_elbow_jnt.tx")
    cmds.disconnectAttr("RIG_rt_armTrans_blend.outputG", "RIG_rt_wrist_jnt.tx")

    cmds.disconnectAttr("RIG_lf_legTrans_blend.outputR", "RIG_lf_knee_jnt.tx")
    cmds.disconnectAttr("RIG_lf_legTrans_blend.outputG", "RIG_lf_ankle_jnt.tx")
    cmds.disconnectAttr("RIG_rt_legTrans_blend.outputR", "RIG_rt_knee_jnt.tx")
    cmds.disconnectAttr("RIG_rt_legTrans_blend.outputG", "RIG_rt_ankle_jnt.tx")

    #attiva t e r su tutti controlli fk
    cmds.setAttr ("RIG_lf_wrist_fk_ctrl.secondaryFkCtrlVis", 1)
    cmds.setAttr ("RIG_lf_upArm_fk_ctrl.secondaryFkCtrlVis", 1)
    cmds.setAttr ("RIG_rt_wrist_fk_ctrl.secondaryFkCtrlVis", 1)
    cmds.setAttr ("RIG_rt_upArm_fk_ctrl.secondaryFkCtrlVis", 1)
    cmds.setAttr ("RIG_lf_upLeg_fk_ctrl.secondaryFkCtrlVis", 1)
    cmds.setAttr ("RIG_lf_ankle_fk_ctrl.secondaryFkCtrlVis", 1)
    cmds.setAttr ("RIG_rt_upLeg_fk_ctrl.secondaryFkCtrlVis", 1)
    cmds.setAttr ("RIG_rt_ankle_fk_ctrl.secondaryFkCtrlVis", 1)

    cmds.setAttr("RIG_lf_elbow_fk_ctrl.rx",k=True,lock=False);
    cmds.setAttr("RIG_rt_elbow_fk_ctrl.rx",k=True,lock=False);
    cmds.setAttr("RIG_lf_knee_fk_ctrl.rx",k=True,lock=False);
    cmds.setAttr("RIG_rt_knee_fk_ctrl.rx",k=True,lock=False);

    cmds.setAttr("RIG_lf_upArm_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_lf_upArm_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_lf_upArm_fk_ctrl.tz",k=True,lock=False);
    cmds.setAttr("RIG_rt_upArm_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_rt_upArm_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_rt_upArm_fk_ctrl.tz",k=True,lock=False);

    cmds.setAttr("RIG_lf_upArm_sec_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_lf_upArm_sec_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_lf_upArm_sec_fk_ctrl.tz",k=True,lock=False);
    cmds.setAttr("RIG_rt_upArm_sec_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_rt_upArm_sec_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_rt_upArm_sec_fk_ctrl.tz",k=True,lock=False);

    cmds.setAttr("RIG_lf_elbow_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_lf_elbow_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_lf_elbow_fk_ctrl.tz",k=True,lock=False);
    cmds.setAttr("RIG_rt_elbow_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_rt_elbow_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_rt_elbow_fk_ctrl.tz",k=True,lock=False);

    cmds.setAttr("RIG_lf_wrist_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_lf_wrist_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_lf_wrist_fk_ctrl.tz",k=True,lock=False);
    cmds.setAttr("RIG_rt_wrist_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_rt_wrist_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_rt_wrist_fk_ctrl.tz",k=True,lock=False);

    cmds.setAttr("RIG_lf_wrist_sec_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_lf_wrist_sec_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_lf_wrist_sec_fk_ctrl.tz",k=True,lock=False);
    cmds.setAttr("RIG_rt_wrist_sec_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_rt_wrist_sec_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_rt_wrist_sec_fk_ctrl.tz",k=True,lock=False);

    cmds.setAttr("RIG_lf_upLeg_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_lf_upLeg_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_lf_upLeg_fk_ctrl.tz",k=True,lock=False);
    cmds.setAttr("RIG_rt_upLeg_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_rt_upLeg_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_rt_upLeg_fk_ctrl.tz",k=True,lock=False);

    cmds.setAttr("RIG_lf_upLeg_sec_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_lf_upLeg_sec_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_lf_upLeg_sec_fk_ctrl.tz",k=True,lock=False);
    cmds.setAttr("RIG_rt_upLeg_sec_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_rt_upLeg_sec_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_rt_upLeg_sec_fk_ctrl.tz",k=True,lock=False);

    cmds.setAttr("RIG_lf_knee_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_lf_knee_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_lf_knee_fk_ctrl.tz",k=True,lock=False);
    cmds.setAttr("RIG_rt_knee_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_rt_knee_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_rt_knee_fk_ctrl.tz",k=True,lock=False);

    cmds.setAttr("RIG_lf_ankle_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_lf_ankle_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_lf_ankle_fk_ctrl.tz",k=True,lock=False);
    cmds.setAttr("RIG_rt_ankle_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_rt_ankle_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_rt_ankle_fk_ctrl.tz",k=True,lock=False);

    cmds.setAttr("RIG_lf_ankle_sec_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_lf_ankle_sec_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_lf_ankle_sec_fk_ctrl.tz",k=True,lock=False);
    cmds.setAttr("RIG_rt_ankle_sec_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_rt_ankle_sec_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_rt_ankle_sec_fk_ctrl.tz",k=True,lock=False);

    #crea sec elbow e knee
    cmds.duplicate ("RIG_RIG_lf_elbow_fk_ctrl_frz_grp1")
    cmds.rename ("RIG_RIG_lf_elbow_fk_ctrl_frz_grp2", "RIG_lf_elbow_sec_fk_ctrl_grp")
    cmds.rename ("RIG_lf_elbow_sec_fk_ctrl_grp|RIG_lf_elbow_fk_ctrl", "RIG_lf_elbow_sec_fk_ctrl")
    cmds.parent ("RIG_lf_elbow_sec_fk_ctrl_grp", "RIG_lf_elbow_fk_ctrl")
    cmds.setAttr("RIG_lf_elbow_sec_fk_ctrl.v",1);
    cmds.select ( "RIG_lf_elbow_fk_ctrl" )
    cmds.addAttr (shortName='secfkel', longName='secondaryFkCtrlVis', at="bool", h=False, k=True)
    cmds.connectAttr ("RIG_lf_elbow_fk_ctrl.secondaryFkCtrlVis", "RIG_lf_elbow_sec_fk_ctrl_grp.v")

    cmds.duplicate ("RIG_RIG_rt_elbow_fk_ctrl_frz_grp1")
    cmds.rename ("RIG_RIG_rt_elbow_fk_ctrl_frz_grp2", "RIG_rt_elbow_sec_fk_ctrl_grp")
    cmds.rename ("RIG_rt_elbow_sec_fk_ctrl_grp|RIG_rt_elbow_fk_ctrl", "RIG_rt_elbow_sec_fk_ctrl")
    cmds.parent ("RIG_rt_elbow_sec_fk_ctrl_grp", "RIG_rt_elbow_fk_ctrl")
    cmds.setAttr("RIG_rt_elbow_sec_fk_ctrl.v",1);
    cmds.select ( "RIG_rt_elbow_fk_ctrl" )
    cmds.addAttr (shortName='secfker', longName='secondaryFkCtrlVis', at="bool", h=False, k=True)
    cmds.connectAttr ("RIG_rt_elbow_fk_ctrl.secondaryFkCtrlVis", "RIG_rt_elbow_sec_fk_ctrl_grp.v")

    cmds.duplicate ("RIG_RIG_lf_knee_fk_ctrl_frz_grp1")
    cmds.rename ("RIG_RIG_lf_knee_fk_ctrl_frz_grp2", "RIG_RIG_lf_knee_sec_fk_ctrl_frz_grp")
    cmds.rename ("RIG_RIG_lf_knee_sec_fk_ctrl_frz_grp|RIG_lf_knee_fk_ctrl", "RIG_lf_knee_sec_fk_ctrl")
    cmds.parent ("RIG_RIG_lf_knee_sec_fk_ctrl_frz_grp", "RIG_lf_knee_fk_ctrl")
    cmds.setAttr("RIG_lf_knee_sec_fk_ctrl.v",1);
    cmds.select ( "RIG_lf_knee_fk_ctrl" )
    cmds.addAttr (shortName='secfkkl', longName='secondaryFkCtrlVis', at="bool", h=False, k=True)
    cmds.connectAttr ("RIG_lf_knee_fk_ctrl.secondaryFkCtrlVis", "RIG_RIG_lf_knee_sec_fk_ctrl_frz_grp.v")

    cmds.duplicate ("RIG_RIG_rt_knee_fk_ctrl_frz_grp1")
    cmds.rename ("RIG_RIG_rt_knee_fk_ctrl_frz_grp2", "RIG_RIG_rt_knee_sec_fk_ctrl_frz_grp")
    cmds.rename ("RIG_RIG_rt_knee_sec_fk_ctrl_frz_grp|RIG_rt_knee_fk_ctrl", "RIG_rt_knee_sec_fk_ctrl")
    cmds.parent ("RIG_RIG_rt_knee_sec_fk_ctrl_frz_grp", "RIG_rt_knee_fk_ctrl")
    cmds.setAttr("RIG_rt_knee_sec_fk_ctrl.v",1);
    cmds.select ( "RIG_rt_knee_fk_ctrl" )
    cmds.addAttr (shortName='secfkkr', longName='secondaryFkCtrlVis', at="bool", h=False, k=True)
    cmds.connectAttr ("RIG_rt_knee_fk_ctrl.secondaryFkCtrlVis", "RIG_RIG_rt_knee_sec_fk_ctrl_frz_grp.v")

    cmds.setAttr ("RIG_lf_elbow_fk_ctrl.secondaryFkCtrlVis", 0)
    cmds.setAttr ("RIG_rt_elbow_fk_ctrl.secondaryFkCtrlVis", 0)
    cmds.setAttr ("RIG_lf_knee_fk_ctrl.secondaryFkCtrlVis", 0)
    cmds.setAttr ("RIG_rt_knee_fk_ctrl.secondaryFkCtrlVis", 0)
    cmds.setAttr ("RIG_lf_upArm_fk_ctrl.secondaryFkCtrlVis", 0)
    cmds.setAttr ("RIG_rt_upArm_fk_ctrl.secondaryFkCtrlVis", 0)
    cmds.setAttr ("RIG_lf_wrist_fk_ctrl.secondaryFkCtrlVis", 0)
    cmds.setAttr ("RIG_rt_wrist_fk_ctrl.secondaryFkCtrlVis", 0)
    ################## - Fix rotateX Left cavicle - ##################
    #cmds.disconnectAttr("RIG_lf_clavicle_jnt.rotateX")
    cmds.parentConstraint("RIG_lf_clavicle_ctrl", "RIG_RIG_lf_shoulder_ctrl_frz_grp", mo=True)
    cmds.parentConstraint("RIG_lf_clavicle_ctrl", "RIG_lf_armFK_Grp", mo=True)
    cmds.parentConstraint("RIG_lf_clavicle_ctrl", "RIG_lf_armIK_Grp", mo=True)
    cmds.parentConstraint("RIG_lf_clavicle_ctrl", "RIG_lf_upArm_upJntFkCtrlAlign_grp", mo=True)

    ################## - Fix rotateX Right cavicle - ##################
    #cmds.disconnectAttr("RIG_rt_clavicle_jnt.rotateX")
    cmds.parentConstraint("RIG_rt_clavicle_ctrl", "RIG_RIG_rt_shoulder_ctrl_frz_grp", mo=True)
    cmds.parentConstraint("RIG_rt_clavicle_ctrl", "RIG_rt_armFK_Grp", mo=True)
    cmds.parentConstraint("RIG_rt_clavicle_ctrl", "RIG_rt_armIK_Grp", mo=True)
    cmds.parentConstraint("RIG_rt_clavicle_ctrl", "RIG_rt_upArm_upJntFkCtrlAlign_grp", mo=True)

    ################## - Fix Translate Left Arm - ##################
    cmds.parentConstraint("RIG_lf_upArm_sec_fk_ctrl", "RIG_lf_upArm_fk_jnt",mo=True)
    cmds.parentConstraint("RIG_lf_elbow_sec_fk_ctrl", "RIG_lf_elbow_fk_jnt",mo=True)
    cmds.parentConstraint("RIG_lf_wrist_sec_fk_ctrl", "RIG_lf_wrist_fk_jnt",mo=True)

    # - Left upArm
         # - PAC just translate
    cmds.parentConstraint("RIG_lf_clavicle_ctrl", "RIG_RIG_lf_upArm_fk_ctrl_frz_grp", sr=["x", "y", "z"], mo=True)
         # - PAC just rotate
    constFollowLfUpArm = cmds.parentConstraint("RIG_lf_clavicle_ctrl", "RIG_root_ctrl_HIDDEN", "RIG_RIG_lf_upArm_fk_ctrl_frz_grp1", st=["x", "y", "z"], mo=True)
    print (constFollowLfUpArm)
    stringConstFollowLfUpArm = str(constFollowLfUpArm)
    lenghtStringLfUpArmConst = len(stringConstFollowLfUpArm)
    print (lenghtStringLfUpArmConst)
    ['RIG_RIG_lf_upArm_fk_ctrl_frz_grp1_parentConstraint1']
    nameConstLfUpArm = stringConstFollowLfUpArm[2:lenghtStringLfUpArmConst-2]
    print (nameConstLfUpArm)
         # - Follow lf upArm
    cmds.createNode("reverse", n="REV_Follow_lf_upArm")
    cmds.connectAttr("RIG_lf_upArm_fk_ctrl.follow", "REV_Follow_lf_upArm.inputX", f=True)
    cmds.connectAttr("RIG_lf_upArm_fk_ctrl.follow", nameConstLfUpArm+".RIG_lf_clavicle_ctrlW0", f=True)
    cmds.connectAttr("REV_Follow_lf_upArm.outputX", nameConstLfUpArm+".RIG_root_ctrl_HIDDENW1", f=True)
    #cmds.setAttr( "RIG_lf_upArm_fk_ctrl.localAlign", lock=True, keyable=False, channelBox=False)

    # - Left Elbow
    cmds.parentConstraint("RIG_lf_upArm_sec_fk_ctrl", "RIG_RIG_lf_elbow_fk_ctrl_frz_grp", mo=True)

    # - Left Wrist
         # - PAC just translate
    cmds.parentConstraint("RIG_lf_elbow_sec_fk_ctrl", "RIG_RIG_lf_wrist_fk_ctrl_frz_grp", sr=["x", "y", "z"], mo=True)
         # - PAC just rotate
    constFollowLfWrist = cmds.parentConstraint("RIG_lf_elbow_sec_fk_ctrl", "RIG_root_ctrl_HIDDEN", "RIG_RIG_lf_wrist_fk_ctrl_frz_grp1", st=["x", "y", "z"], mo=True)
    print (constFollowLfWrist)
    stringConstFollowLfWrist = str(constFollowLfWrist)
    lenghtStringLfWristConst = len(stringConstFollowLfWrist)
    nameConstLfWrist = stringConstFollowLfWrist[2:lenghtStringLfWristConst-2]
    print (nameConstLfWrist)
         # - Follow lf Wrist
    cmds.createNode("reverse", n="REV_Follow_lf_wrist")
    cmds.connectAttr("RIG_lf_wrist_fk_ctrl.follow", "REV_Follow_lf_wrist.inputX")
    cmds.connectAttr("RIG_lf_wrist_fk_ctrl.follow", nameConstLfWrist+".RIG_lf_elbow_sec_fk_ctrlW0")
    cmds.connectAttr("REV_Follow_lf_wrist.outputX", nameConstLfWrist+".RIG_root_ctrl_HIDDENW1")
    cmds.setAttr( "RIG_lf_wrist_fk_ctrl.localAlign", lock=True, keyable=False, channelBox=False)

    # - Connection to DEF joints chain
    bndLfUpArm = cmds.createNode("blendColors", n="BND_translate_lf_upArm")
    bndLfElbow = cmds.createNode("blendColors", n="BND_translate_lf_elbow")
    bndLfWrist = cmds.createNode("blendColors", n="BND_translate_lf_wrist")

    cmds.connectAttr("RIG_lf_hand_ctrl.ikFkBlend", bndLfUpArm+".blender", f=True)
    cmds.connectAttr("RIG_lf_hand_ctrl.ikFkBlend", bndLfElbow+".blender", f=True)
    cmds.connectAttr("RIG_lf_hand_ctrl.ikFkBlend", bndLfWrist+".blender", f=True)

    cmds.connectAttr("RIG_lf_upArm_ik_jnt.translate", bndLfUpArm+".color1", f=True)
    cmds.connectAttr("RIG_lf_upArm_fk_jnt.translate", bndLfUpArm+".color2", f=True)
    cmds.connectAttr(bndLfUpArm+".output", "RIG_lf_upArm_jnt.translate", f=True)

    cmds.connectAttr("RIG_lf_elbow_ik_jnt.translate", bndLfElbow+".color1", f=True)
    cmds.connectAttr("RIG_lf_elbow_fk_jnt.translate", bndLfElbow+".color2", f=True)
    cmds.connectAttr(bndLfElbow+".output", "RIG_lf_elbow_jnt.translate", f=True)

    cmds.connectAttr("RIG_lf_wrist_ik_jnt.translate", bndLfWrist+".color1", f=True)
    cmds.connectAttr("RIG_lf_wrist_fk_jnt.translate", bndLfWrist+".color2", f=True)
    cmds.connectAttr(bndLfWrist+".output", "RIG_lf_wrist_jnt.translate", f=True)


    ################## - Fix Translate Right Arm - ##################
    cmds.parentConstraint("RIG_rt_upArm_sec_fk_ctrl", "RIG_rt_upArm_fk_jnt",mo=True)
    cmds.parentConstraint("RIG_rt_elbow_sec_fk_ctrl", "RIG_rt_elbow_fk_jnt",mo=True)
    cmds.parentConstraint("RIG_rt_wrist_sec_fk_ctrl", "RIG_rt_wrist_fk_jnt",mo=True)

    # - Right upArm
         # - PAC just translate
    cmds.parentConstraint("RIG_rt_clavicle_ctrl", "RIG_RIG_rt_upArm_fk_ctrl_frz_grp", sr=["x", "y", "z"], mo=True)
         # - PAC just rotate
    constFollowRtUpArm = cmds.parentConstraint("RIG_rt_clavicle_ctrl", "RIG_root_ctrl_HIDDEN", "RIG_RIG_rt_upArm_fk_ctrl_frz_grp1", st=["x", "y", "z"], mo=True)
    print (constFollowRtUpArm)
    stringConstFollowRtUpArm = str(constFollowRtUpArm)
    lenghtStringRtUpArmConst = len(stringConstFollowRtUpArm)
    nameConstRtUpArm = stringConstFollowRtUpArm[2:lenghtStringRtUpArmConst-2]
    print (nameConstRtUpArm)
         # - Follow rt upArm
    cmds.createNode("reverse", n="REV_Follow_rt_upArm")
    cmds.connectAttr("RIG_rt_upArm_fk_ctrl.follow", "REV_Follow_rt_upArm.inputX", f=True)
    cmds.connectAttr("RIG_rt_upArm_fk_ctrl.follow", nameConstRtUpArm+".RIG_rt_clavicle_ctrlW0", f=True)
    cmds.connectAttr("REV_Follow_rt_upArm.outputX", nameConstRtUpArm+".RIG_root_ctrl_HIDDENW1", f=True)
    #cmds.setAttr( "RIG_rt_upArm_fk_ctrl.localAlign", lock=True, keyable=False, channelBox=False)

    # - Right Elbow
    cmds.parentConstraint("RIG_rt_upArm_sec_fk_ctrl", "RIG_RIG_rt_elbow_fk_ctrl_frz_grp", mo=True)

    # - Right Wrist
         # - PAC just translate
    cmds.parentConstraint("RIG_rt_elbow_sec_fk_ctrl", "RIG_RIG_rt_wrist_fk_ctrl_frz_grp", sr=["x", "y", "z"], mo=True)
         # - PAC just rotate
    constFollowRtWrist = cmds.parentConstraint("RIG_rt_elbow_sec_fk_ctrl", "RIG_root_ctrl_HIDDEN", "RIG_RIG_rt_wrist_fk_ctrl_frz_grp1", st=["x", "y", "z"], mo=True)
    print (constFollowRtWrist)
    stringConstFollowRtWrist = str(constFollowRtWrist)
    lenghtStringRtWristConst = len(stringConstFollowRtWrist)
    nameConstRtWrist = stringConstFollowRtWrist[2:lenghtStringRtWristConst-2]
    print (nameConstRtWrist)
         # - Follow rt Wrist
    cmds.createNode("reverse", n="REV_Follow_rt_wrist")
    cmds.connectAttr("RIG_rt_wrist_fk_ctrl.follow", "REV_Follow_rt_wrist.inputX", f=True)
    cmds.connectAttr("RIG_rt_wrist_fk_ctrl.follow", nameConstRtWrist+".RIG_rt_elbow_sec_fk_ctrlW0", f=True)
    cmds.connectAttr("REV_Follow_rt_wrist.outputX", nameConstRtWrist+".RIG_root_ctrl_HIDDENW1", f=True)
    cmds.setAttr( "RIG_rt_wrist_fk_ctrl.localAlign", lock=True, keyable=False, channelBox=False)

    # - Connection to DEF joints chain
    bndRtUpArm = cmds.createNode("blendColors", n="BND_translate_rt_upArm")
    bndRtElbow = cmds.createNode("blendColors", n="BND_translate_rt_elbow")
    bndRtWrist = cmds.createNode("blendColors", n="BND_translate_rt_wrist")

    cmds.connectAttr("RIG_rt_hand_ctrl.ikFkBlend", bndRtUpArm+".blender", f=True)
    cmds.connectAttr("RIG_rt_hand_ctrl.ikFkBlend", bndRtElbow+".blender", f=True)
    cmds.connectAttr("RIG_rt_hand_ctrl.ikFkBlend", bndRtWrist+".blender", f=True)

    cmds.connectAttr("RIG_rt_upArm_ik_jnt.translate", bndRtUpArm+".color1", f=True)
    cmds.connectAttr("RIG_rt_upArm_fk_jnt.translate", bndRtUpArm+".color2", f=True)
    cmds.connectAttr(bndRtUpArm+".output", "RIG_rt_upArm_jnt.translate", f=True)

    cmds.connectAttr("RIG_rt_elbow_ik_jnt.translate", bndRtElbow+".color1", f=True)
    cmds.connectAttr("RIG_rt_elbow_fk_jnt.translate", bndRtElbow+".color2", f=True)
    cmds.connectAttr(bndRtElbow+".output", "RIG_rt_elbow_jnt.translate", f=True)

    cmds.connectAttr("RIG_rt_wrist_ik_jnt.translate", bndRtWrist+".color1", f=True)
    cmds.connectAttr("RIG_rt_wrist_fk_jnt.translate", bndRtWrist+".color2", f=True)
    cmds.connectAttr(bndRtWrist+".output", "RIG_rt_wrist_jnt.translate", f=True)


    ################## - Fix Translate Left Leg - ##################
    cmds.parentConstraint("RIG_lf_upLeg_sec_fk_ctrl", "RIG_lf_upLeg_fk_jnt",mo=True)
    cmds.parentConstraint("RIG_lf_knee_sec_fk_ctrl", "RIG_lf_knee_fk_jnt",mo=True)
    cmds.parentConstraint("RIG_lf_ankle_sec_fk_ctrl", "RIG_lf_ankle_fk_jnt",mo=True)

    # - Left upLeg
         # - PAC just translate
    cmds.parentConstraint("RIG_hip_ctrl", "RIG_RIG_lf_upLeg_fk_ctrl_frz_grp", sr=["x", "y", "z"], mo=True)
         # - PAC just rotate
    constFollowLfUpLeg = cmds.parentConstraint("RIG_hip_ctrl", "RIG_root_ctrl_HIDDEN", "RIG_RIG_lf_upLeg_fk_ctrl_frz_grp1", st=["x", "y", "z"], mo=True)
    print (constFollowLfUpLeg)
    stringConstFollowLfUpLeg = str(constFollowLfUpLeg)
    lenghtStringLfUpLegConst = len(stringConstFollowLfUpLeg)
    nameConstLfUpLeg = stringConstFollowLfUpLeg[2:lenghtStringLfUpLegConst-2]
    print (nameConstLfUpLeg)
         # - Follow lf upLeg
    cmds.createNode("reverse", n="REV_Follow_lf_upLeg")
    cmds.connectAttr("RIG_lf_upLeg_fk_ctrl.follow", "REV_Follow_lf_upLeg.inputX", f=True)
    cmds.connectAttr("RIG_lf_upLeg_fk_ctrl.follow", nameConstLfUpLeg+".RIG_hip_ctrlW0", f=True)
    cmds.connectAttr("REV_Follow_lf_upLeg.outputX", nameConstLfUpLeg+".RIG_root_ctrl_HIDDENW1", f=True)
    #cmds.setAttr( "RIG_lf_upLeg_fk_ctrl.localAlign", lock=True, keyable=False, channelBox=False)

    # - Left Knee
    cmds.parentConstraint("RIG_lf_upLeg_sec_fk_ctrl", "RIG_RIG_lf_knee_fk_ctrl_frz_grp", mo=True)

    # - Left Ankle
    cmds.parentConstraint("RIG_lf_knee_sec_fk_ctrl", "RIG_RIG_lf_ankle_fk_ctrl_frz_grp", mo=True)

    # - Connection to DEF joints chain
    bndLfUpLeg = cmds.createNode("blendColors", n="BND_translate_lf_upLeg")
    bndLfKnee = cmds.createNode("blendColors", n="BND_translate_lf_Knee")
    bndLfAnkle = cmds.createNode("blendColors", n="BND_translate_lf_Ankle")

    cmds.connectAttr("RIG_lf_foot_ctrl.ikFkBlend", bndLfUpLeg+".blender", f=True)
    cmds.connectAttr("RIG_lf_foot_ctrl.ikFkBlend", bndLfKnee+".blender", f=True)
    cmds.connectAttr("RIG_lf_foot_ctrl.ikFkBlend", bndLfAnkle+".blender", f=True)

    cmds.connectAttr("RIG_lf_upLeg_ik_jnt.translate", bndLfUpLeg+".color1", f=True)
    cmds.connectAttr("RIG_lf_upLeg_fk_jnt.translate", bndLfUpLeg+".color2", f=True)
    cmds.connectAttr(bndLfUpLeg+".output", "RIG_lf_upLeg_jnt.translate", f=True)

    cmds.connectAttr("RIG_lf_knee_ik_jnt.translate", bndLfKnee+".color1", f=True)
    cmds.connectAttr("RIG_lf_knee_fk_jnt.translate", bndLfKnee+".color2", f=True)
    cmds.connectAttr(bndLfKnee+".output", "RIG_lf_knee_jnt.translate", f=True)

    cmds.connectAttr("RIG_lf_ankle_ik_jnt.translate", bndLfAnkle+".color1", f=True)
    cmds.connectAttr("RIG_lf_ankle_fk_jnt.translate", bndLfAnkle+".color2", f=True)
    cmds.connectAttr(bndLfAnkle+".output", "RIG_lf_ankle_jnt.translate", f=True)

    ################## - Fix Translate Right Leg - ##################
    cmds.parentConstraint("RIG_rt_upLeg_sec_fk_ctrl", "RIG_rt_upLeg_fk_jnt",mo=True)
    cmds.parentConstraint("RIG_rt_knee_sec_fk_ctrl", "RIG_rt_knee_fk_jnt",mo=True)
    cmds.parentConstraint("RIG_rt_ankle_sec_fk_ctrl", "RIG_rt_ankle_fk_jnt",mo=True)

    # - Right upLeg
         # - PAC just translate
    cmds.parentConstraint("RIG_hip_ctrl", "RIG_RIG_rt_upLeg_fk_ctrl_frz_grp", sr=["x", "y", "z"], mo=True)
         # - PAC just rotate
    constFollowRtUpLeg = cmds.parentConstraint("RIG_hip_ctrl", "RIG_root_ctrl_HIDDEN", "RIG_RIG_rt_upLeg_fk_ctrl_frz_grp1", st=["x", "y", "z"], mo=True)
    print (constFollowRtUpLeg)
    stringConstFollowRtUpLeg = str(constFollowRtUpLeg)
    lenghtStringRtUpLegConst = len(stringConstFollowRtUpLeg)
    nameConstRtUpLeg = stringConstFollowRtUpLeg[2:lenghtStringRtUpLegConst-2]
    print (nameConstRtUpLeg)
         # - Follow rt upLeg
    cmds.createNode("reverse", n="REV_Follow_rt_upLeg")
    cmds.connectAttr("RIG_rt_upLeg_fk_ctrl.follow", "REV_Follow_rt_upLeg.inputX", f=True)
    cmds.connectAttr("RIG_rt_upLeg_fk_ctrl.follow", nameConstRtUpLeg+".RIG_hip_ctrlW0", f=True)
    cmds.connectAttr("REV_Follow_rt_upLeg.outputX", nameConstRtUpLeg+".RIG_root_ctrl_HIDDENW1", f=True)
    #cmds.setAttr( "RIG_rt_upLeg_fk_ctrl.localAlign", lock=True, keyable=False, channelBox=False)

    # - Right Knee
    cmds.parentConstraint("RIG_rt_upLeg_sec_fk_ctrl", "RIG_RIG_rt_knee_fk_ctrl_frz_grp", mo=True)

    # - Right Ankle
    cmds.parentConstraint("RIG_rt_knee_sec_fk_ctrl", "RIG_RIG_rt_ankle_fk_ctrl_frz_grp", mo=True)

    # - Connection to DEF joints chain
    bndRtUpLeg = cmds.createNode("blendColors", n="BND_translate_rt_upLeg")
    bndRtKnee = cmds.createNode("blendColors", n="BND_translate_rt_Knee")
    bndRtAnkle = cmds.createNode("blendColors", n="BND_translate_rt_Ankle")

    cmds.connectAttr("RIG_rt_foot_ctrl.ikFkBlend", bndRtUpLeg+".blender", f=True)
    cmds.connectAttr("RIG_rt_foot_ctrl.ikFkBlend", bndRtKnee+".blender", f=True)
    cmds.connectAttr("RIG_rt_foot_ctrl.ikFkBlend", bndRtAnkle+".blender", f=True)

    cmds.connectAttr("RIG_rt_upLeg_ik_jnt.translate", bndRtUpLeg+".color1", f=True)
    cmds.connectAttr("RIG_rt_upLeg_fk_jnt.translate", bndRtUpLeg+".color2", f=True)
    cmds.connectAttr(bndRtUpLeg+".output", "RIG_rt_upLeg_jnt.translate", f=True)

    cmds.connectAttr("RIG_rt_knee_ik_jnt.translate", bndRtKnee+".color1", f=True)
    cmds.connectAttr("RIG_rt_knee_fk_jnt.translate", bndRtKnee+".color2", f=True)
    cmds.connectAttr(bndRtKnee+".output", "RIG_rt_knee_jnt.translate", f=True)

    cmds.connectAttr("RIG_rt_ankle_ik_jnt.translate", bndRtAnkle+".color1", f=True)
    cmds.connectAttr("RIG_rt_ankle_fk_jnt.translate", bndRtAnkle+".color2", f=True)
    cmds.connectAttr(bndRtAnkle+".output", "RIG_rt_ankle_jnt.translate", f=True)

    cmds.select ( cl=True )

    #delete 4 local align
    cmds.deleteAttr("RIG_lf_upArm_fk_ctrl.localAlign")
    cmds.deleteAttr("RIG_rt_upArm_fk_ctrl.localAlign")
    cmds.deleteAttr("RIG_lf_upLeg_fk_ctrl.localAlign")
    cmds.deleteAttr("RIG_rt_upLeg_fk_ctrl.localAlign")

    #rotateOrder added
    cmds.setAttr ("RIG_head_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_neck_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_spineHigh_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_spineMid_1_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_spineLow_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_cog_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_hip_ctrl.rotateOrder", k=False, cb=True )

    cmds.setAttr ("RIG_lf_clavicleTrans_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_shoulder_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_arm_ik_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_hand_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_elbow_pv_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_upArm_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_elbow_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_wrist_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_upArm_sec_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_elbow_sec_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_wrist_sec_fk_ctrl.rotateOrder", k=False, cb=True )

    cmds.setAttr ("RIG_rt_clavicleTrans_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_shoulder_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_arm_ik_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_hand_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_elbow_pv_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_upArm_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_elbow_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_wrist_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_upArm_sec_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_elbow_sec_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_wrist_sec_fk_ctrl.rotateOrder", k=False, cb=True )

    cmds.setAttr ("RIG_lf_hip_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_heel_ik_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_knee_pv_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_upLeg_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_knee_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_ankle_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_ball_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_upLeg_sec_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_knee_sec_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_ankle_sec_fk_ctrl.rotateOrder", k=False, cb=True )

    cmds.setAttr ("RIG_rt_hip_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_heel_ik_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_knee_pv_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_upLeg_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_knee_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_ankle_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_ball_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_upLeg_sec_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_knee_sec_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_ankle_sec_fk_ctrl.rotateOrder", k=False, cb=True )

    cmds.select ( cl=True )

    #autoTwist added
    cmds.select("RIG_lf_wrist_fk_ctrl")
    cmds.addAttr( shortName='atwl', longName='AutoTwist', at="float", h=False, k=True, minValue=0, maxValue=1)
    cmds.setAttr ("RIG_lf_wrist_fk_ctrl.AutoTwist", 1 )

    cmds.select("RIG_rt_wrist_fk_ctrl")
    cmds.addAttr( shortName='atwr', longName='AutoTwist', at="float", h=False, k=True, minValue=0, maxValue=1)
    cmds.setAttr ("RIG_rt_wrist_fk_ctrl.AutoTwist", 1 )

    cmds.select("RIG_lf_ankle_fk_ctrl")
    cmds.addAttr( shortName='atal', longName='AutoTwist', at="float", h=False, k=True, minValue=0, maxValue=1)
    cmds.setAttr ("RIG_lf_ankle_fk_ctrl.AutoTwist", 1 )

    cmds.select("RIG_rt_ankle_fk_ctrl")
    cmds.addAttr( shortName='atar', longName='AutoTwist', at="float", h=False, k=True, minValue=0, maxValue=1)
    cmds.setAttr ("RIG_rt_ankle_fk_ctrl.AutoTwist", 1 )

    cmds.shadingNode('blendColors', asShader=True, n="BLD_autoTwistWrist_L")
    cmds.shadingNode('blendColors', asShader=True, n="BLD_autoTwistWrist_R")
    cmds.shadingNode('blendColors', asShader=True, n="BLD_autoTwistAnkle_L")
    cmds.shadingNode('blendColors', asShader=True, n="BLD_autoTwistAnkle_R")

    cmds.connectAttr("RIG_lf_wrist_fk_ctrl.AutoTwist", "BLD_autoTwistWrist_L.blender")
    cmds.connectAttr("RIG_lf_wrist_fk_ctrl.rx", "BLD_autoTwistWrist_L.color1R")
    cmds.connectAttr("BLD_autoTwistWrist_L.outputR", "RIG_lf_elbow_sec_fk_ctrl_grp.rx")

    cmds.connectAttr("RIG_rt_wrist_fk_ctrl.AutoTwist", "BLD_autoTwistWrist_R.blender")
    cmds.connectAttr("RIG_rt_wrist_fk_ctrl.rx", "BLD_autoTwistWrist_R.color1R")
    cmds.connectAttr("BLD_autoTwistWrist_R.outputR", "RIG_rt_elbow_sec_fk_ctrl_grp.rx")

    cmds.connectAttr("RIG_lf_ankle_fk_ctrl.AutoTwist", "BLD_autoTwistAnkle_L.blender")
    cmds.connectAttr("RIG_lf_ankle_fk_ctrl.ry", "BLD_autoTwistAnkle_L.color1R")
    cmds.connectAttr("BLD_autoTwistAnkle_L.outputR", "RIG_RIG_lf_knee_sec_fk_ctrl_frz_grp.rx")

    cmds.connectAttr("RIG_rt_ankle_fk_ctrl.AutoTwist", "BLD_autoTwistAnkle_R.blender")
    cmds.connectAttr("RIG_rt_ankle_fk_ctrl.ry", "BLD_autoTwistAnkle_R.color1R")
    cmds.connectAttr("BLD_autoTwistAnkle_R.outputR", "RIG_RIG_rt_knee_sec_fk_ctrl_frz_grp.rx")

    cmds.select ( cl=True )

    #/////////////////////FINGERS TRANSLATE ROTATE AUTOMATIC FIX///////////////////////////////////////////////////
    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #FOREARM FIX SCRIPT
    #Lanciare lo script senza selezionare nulla
    #Dopo aver lanciato lo script andare sul controllo "RIG_rigSettings_ctrl" e settare gli attributi
    #'Right Forearm 1...4' e 'Left Forearm 1...4' in modo tale da avere una buona deformazione durante la 'roateX' del wrist/hand
    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////

    pls_lf = cmds.createNode("plusMinusAverage", n="PLS_lf_wrist_rotation")

    cmds.connectAttr("RIG_lf_wrist_jnt.rotateX", pls_lf+".input3D[0].input3Dx", force=True)
    cmds.connectAttr("RIG_lf_hand_jnt.rotateX", pls_lf+".input3D[1].input3Dx", force=True)

    cmds.connectAttr(pls_lf+".output3Dx", "RIG_lf_forearmManualRotCtrl_blend.color1.color1R", force=True)

    pls_rt = cmds.createNode("plusMinusAverage", n="PLS_rt_wrist_rotation")

    cmds.connectAttr("RIG_rt_wrist_jnt.rotateX", pls_rt+".input3D[0].input3Dx", force=True)
    cmds.connectAttr("RIG_rt_hand_jnt.rotateX", pls_rt+".input3D[1].input3Dx", force=True)

    cmds.connectAttr(pls_rt+".output3Dx", "RIG_lf_forearmManualRotCtrl_blend.color1.color1R", force=True)

    cmds.setAttr("RIG_lf_hand_ctrl.forearmTwistCtrl", 0)
    cmds.setAttr("RIG_lf_hand_ctrl.forearmTwistCtrl", k=False, l=True, cb=False)

    cmds.setAttr("RIG_rt_hand_ctrl.forearmTwistCtrl", 0)
    cmds.setAttr("RIG_rt_hand_ctrl.forearmTwistCtrl", k=False, l=True, cb=False)

    cmds.select(cl=True)

    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #BALL TWIST FIXED
    #script per fixare il funzionamento del ball twist fatto da ab
    #Usage: lanciare tutto lo script per eseguire il fix
    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////

    #prendo le posizioni del ball
    posBallL = cmds.xform('RIG_lf_ball_jnt', q=True, t=True, ws=True)
    posBallR = cmds.xform('RIG_rt_ball_jnt', q=True, t=True, ws=True)

    #creo i locator con i gruppi
    LocBallRevL = cmds.spaceLocator(n='LOC_lf_ball_rev_rig_jnt')
    GrpLocBallRevL = cmds.group(LocBallRevL, n='GRP_LOC_lf_ball_rev_rig_jnt')

    LocBallRevR = cmds.spaceLocator(n='LOC_rt_ball_rev_rig_jnt')
    GrpLocBallRevR = cmds.group(LocBallRevR, n='GRP_LOC_rt_ball_rev_rig_jnt')

    LocBallIkL = cmds.spaceLocator(n='LOC_lf_ball_ik_jnt')
    GrpLocBallIkL = cmds.group(LocBallIkL, n='GRP_LOC_lf_ball_ik_jnt')

    LocBallIkR = cmds.spaceLocator(n='LOC_rt_ball_ik_jnt')
    GrpLocBallIkR = cmds.group(LocBallIkR, n='GRP_LOC_rt_ball_ik_jnt')

    #posiziono i gruppi nei rispettivi ball
    cmds.setAttr(GrpLocBallRevL + '.tx', posBallL[0])
    cmds.setAttr(GrpLocBallRevL + '.ty', posBallL[1])
    cmds.setAttr(GrpLocBallRevL + '.tz', posBallL[2])

    cmds.setAttr(GrpLocBallRevR + '.tx', posBallR[0])
    cmds.setAttr(GrpLocBallRevR + '.ty', posBallR[1])
    cmds.setAttr(GrpLocBallRevR + '.tz', posBallR[2])

    cmds.setAttr(GrpLocBallIkL + '.tx', posBallL[0])
    cmds.setAttr(GrpLocBallIkL + '.ty', posBallL[1])
    cmds.setAttr(GrpLocBallIkL + '.tz', posBallL[2])

    cmds.setAttr(GrpLocBallIkR + '.tx', posBallR[0])
    cmds.setAttr(GrpLocBallIkR + '.ty', posBallR[1])
    cmds.setAttr(GrpLocBallIkR + '.tz', posBallR[2])

    #ruoto i gruppi dei locator
    cmds.setAttr(GrpLocBallRevL + '.rx', 180)
    cmds.setAttr(GrpLocBallRevL + '.ry', -90)
    cmds.setAttr(GrpLocBallIkL + '.rx', 180)
    cmds.setAttr(GrpLocBallIkL + '.ry', -90)
    cmds.setAttr(GrpLocBallRevR + '.ry', 90)
    cmds.setAttr(GrpLocBallIkR + '.ry', 90)

    #parento i locator sotto i rispettivi joint
    cmds.parent(GrpLocBallRevL, 'RIG_lf_toe_rev_rig_jnt')
    cmds.parent(GrpLocBallRevR, 'RIG_rt_toe_rev_rig_jnt')
    cmds.parent(GrpLocBallIkL, 'RIG_lf_ballToePar_ik_jnt')
    cmds.parent(GrpLocBallIkR, 'RIG_rt_ballToePar_ik_jnt')

    #rompo i collegamenti dei joint
    source_attribute = cmds.listConnections('RIG_lf_ball_rev_rig_jnt.rotateX',s=True,p=True)[0];
    cmds.disconnectAttr (source_attribute, 'RIG_lf_ball_rev_rig_jnt.rotateX')
    cmds.connectAttr(source_attribute, 'LOC_lf_ball_rev_rig_jnt.rx')

    source_attribute = cmds.listConnections('RIG_lf_ball_rev_rig_jnt.rotateY',s=True,p=True)[0];
    cmds.disconnectAttr (source_attribute, 'RIG_lf_ball_rev_rig_jnt.rotateY')
    cmds.connectAttr(source_attribute, 'LOC_lf_ball_rev_rig_jnt.ry')

    source_attribute = cmds.listConnections('RIG_lf_ball_rev_rig_jnt.rotateZ',s=True,p=True)[0];
    cmds.disconnectAttr (source_attribute, 'RIG_lf_ball_rev_rig_jnt.rotateZ')
    cmds.connectAttr(source_attribute, 'LOC_lf_ball_rev_rig_jnt.rz')

    source_attribute = cmds.listConnections('RIG_lf_ball_ik_jnt.rotateY',s=True,p=True)[0];
    cmds.disconnectAttr (source_attribute, 'RIG_lf_ball_ik_jnt.rotateY')
    cmds.connectAttr(source_attribute, 'LOC_lf_ball_ik_jnt.ry')

    source_attribute = cmds.listConnections('RIG_lf_ball_ik_jnt.rotateZ',s=True,p=True)[0];
    cmds.disconnectAttr (source_attribute, 'RIG_lf_ball_ik_jnt.rotateZ')
    cmds.connectAttr(source_attribute, 'LOC_lf_ball_ik_jnt.rz')

    source_attribute = cmds.listConnections('RIG_rt_ball_rev_rig_jnt.rotateX',s=True,p=True)[0];
    cmds.disconnectAttr (source_attribute, 'RIG_rt_ball_rev_rig_jnt.rotateX')
    cmds.connectAttr(source_attribute, 'LOC_rt_ball_rev_rig_jnt.rx')

    source_attribute = cmds.listConnections('RIG_rt_ball_rev_rig_jnt.rotateY',s=True,p=True)[0];
    cmds.disconnectAttr (source_attribute, 'RIG_rt_ball_rev_rig_jnt.rotateY')
    cmds.connectAttr(source_attribute, 'LOC_rt_ball_rev_rig_jnt.ry')

    source_attribute = cmds.listConnections('RIG_rt_ball_rev_rig_jnt.rotateZ',s=True,p=True)[0];
    cmds.disconnectAttr (source_attribute, 'RIG_rt_ball_rev_rig_jnt.rotateZ')
    cmds.connectAttr(source_attribute, 'LOC_rt_ball_rev_rig_jnt.rz')

    source_attribute = cmds.listConnections('RIG_rt_ball_ik_jnt.rotateY',s=True,p=True)[0];
    cmds.disconnectAttr (source_attribute, 'RIG_rt_ball_ik_jnt.rotateY')
    cmds.connectAttr(source_attribute, 'LOC_rt_ball_ik_jnt.ry')

    source_attribute = cmds.listConnections('RIG_rt_ball_ik_jnt.rotateZ',s=True,p=True)[0];
    cmds.disconnectAttr (source_attribute, 'RIG_rt_ball_ik_jnt.rotateZ')
    cmds.connectAttr(source_attribute, 'LOC_rt_ball_ik_jnt.rz')


    #faccio un orient constraint dei locator sui joint
    cmds.orientConstraint('LOC_lf_ball_ik_jnt', 'RIG_lf_ball_ik_jnt', mo=True, w=1)
    cmds.orientConstraint('LOC_lf_ball_rev_rig_jnt', 'RIG_lf_ball_rev_rig_jnt', mo=True, w=1)
    cmds.orientConstraint('LOC_rt_ball_ik_jnt', 'RIG_rt_ball_ik_jnt', mo=True, w=1)
    cmds.orientConstraint('LOC_rt_ball_rev_rig_jnt', 'RIG_rt_ball_rev_rig_jnt', mo=True, w=1)

    cmds.select(cl=True)

    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #ARMS LEGS TOONY SCALE FIXED
    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    # HOW TO USE THE SCRIPT: 
    #    1) pick all Toony Controls and Show/Unlock the Scale Attributes
    #    2) RUN the script

    ############################################
    #               Arm Toony Fix              #
    ############################################

    upArmTcntLF = "RIG_lf_upArmRubberHose_ctrl"
    elbowTcntLF = "RIG_lf_elbowRubberHose_ctrl"
    forearmTcntLF = "RIG_lf_forearmRubberHose_ctrl"

    upArmTcntRT = "RIG_rt_upArmRubberHose_ctrl"
    elbowTcntRT = "RIG_rt_elbowRubberHose_ctrl"
    forearmTcntRT = "RIG_rt_forearmRubberHose_ctrl"

    #Slocca i canali dello scale a tutti rubberHose controls
    cmds.setAttr("RIG_lf_upArmRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_lf_upArmRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_lf_upArmRubberHose_ctrl.sz", k=True, l=False)
    cmds.setAttr("RIG_lf_elbowRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_lf_elbowRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_lf_elbowRubberHose_ctrl.sz", k=True, l=False)
    cmds.setAttr("RIG_lf_forearmRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_lf_forearmRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_lf_forearmRubberHose_ctrl.sz", k=True, l=False)

    cmds.setAttr("RIG_rt_upArmRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_rt_upArmRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_rt_upArmRubberHose_ctrl.sz", k=True, l=False)
    cmds.setAttr("RIG_rt_elbowRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_rt_elbowRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_rt_elbowRubberHose_ctrl.sz", k=True, l=False)
    cmds.setAttr("RIG_rt_forearmRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_rt_forearmRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_rt_forearmRubberHose_ctrl.sz", k=True, l=False)

    ############################################
    #               Arm Toony Left             #
    ############################################

    # mlp and str first armToony ---> RIG_lf_upArmRubberHose_ctrl
    mlpUpArmLF = cmds.createNode("multiplyDivide", n="MLP_RIG_lf_upArmRubberHose_ctrl")
    strUpArmLF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_upArmWeight_jnt")
    strUpArmLFSplit1LF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_upArmWeightSplit_1_jnt")
    strUpArmLFSpilt2LF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_upArmWeightSplit_2_jnt")

    cmds.setAttr(strUpArmLF+".minX", -5)
    cmds.setAttr(strUpArmLF+".minY", -5)
    cmds.setAttr(strUpArmLF+".minZ", -5)
    cmds.setAttr(strUpArmLF+".maxX", 5)
    cmds.setAttr(strUpArmLF+".maxY", 5)
    cmds.setAttr(strUpArmLF+".maxZ", 5)
    cmds.setAttr(strUpArmLF+".oldMinX", -12.5)
    cmds.setAttr(strUpArmLF+".oldMinY", -12.5)
    cmds.setAttr(strUpArmLF+".oldMinZ", -12.5)
    cmds.setAttr(strUpArmLF+".oldMaxX", 10)
    cmds.setAttr(strUpArmLF+".oldMaxY", 10)
    cmds.setAttr(strUpArmLF+".oldMaxZ", 10)

    cmds.setAttr(strUpArmLFSplit1LF+".minX", -7)
    cmds.setAttr(strUpArmLFSplit1LF+".minY", -7)
    cmds.setAttr(strUpArmLFSplit1LF+".minZ", -7)
    cmds.setAttr(strUpArmLFSplit1LF+".maxX", 7)
    cmds.setAttr(strUpArmLFSplit1LF+".maxY", 7)
    cmds.setAttr(strUpArmLFSplit1LF+".maxZ", 7)
    cmds.setAttr(strUpArmLFSplit1LF+".oldMinX", -11)
    cmds.setAttr(strUpArmLFSplit1LF+".oldMinY", -11)
    cmds.setAttr(strUpArmLFSplit1LF+".oldMinZ", -11)
    cmds.setAttr(strUpArmLFSplit1LF+".oldMaxX", 10)
    cmds.setAttr(strUpArmLFSplit1LF+".oldMaxY", 10)
    cmds.setAttr(strUpArmLFSplit1LF+".oldMaxZ", 10)

    cmds.setAttr(strUpArmLFSpilt2LF+".minX", -9)
    cmds.setAttr(strUpArmLFSpilt2LF+".minY", -9)
    cmds.setAttr(strUpArmLFSpilt2LF+".minZ", -9)
    cmds.setAttr(strUpArmLFSpilt2LF+".maxX", 9)
    cmds.setAttr(strUpArmLFSpilt2LF+".maxY", 9)
    cmds.setAttr(strUpArmLFSpilt2LF+".maxZ", 9)
    cmds.setAttr(strUpArmLFSpilt2LF+".oldMinX", -10.25)
    cmds.setAttr(strUpArmLFSpilt2LF+".oldMinY", -10.25)
    cmds.setAttr(strUpArmLFSpilt2LF+".oldMinZ", -10.25)
    cmds.setAttr(strUpArmLFSpilt2LF+".oldMaxX", 10)
    cmds.setAttr(strUpArmLFSpilt2LF+".oldMaxY", 10)
    cmds.setAttr(strUpArmLFSpilt2LF+".oldMaxZ", 10)

    # mlp second armToony ---> RIG_lf_elbowRubberHose_ctrl
    mlpElbowLF = cmds.createNode("multiplyDivide", n="MLP_RIG_lf_elbowRubberHose_ctrl")

    # mlp and str first armToony ---> RIG_lf_forearmRubberHose_ctrl
    mlpForearmLF = cmds.createNode("multiplyDivide", n="MLP_RIG_lf_forearmRubberHose_ctrl")
    strForearmSplit3LF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_elbowWeightSplit_3_jnt")
    strForearmSplit4LF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_elbowWeightSplit_4_jnt")
    strWristLF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_elbowWeightSplit_4_jnt")

    cmds.setAttr(strWristLF+".minX", -5)
    cmds.setAttr(strWristLF+".minY", -5)
    cmds.setAttr(strWristLF+".minZ", -5)
    cmds.setAttr(strWristLF+".maxX", 5)
    cmds.setAttr(strWristLF+".maxY", 5)
    cmds.setAttr(strWristLF+".maxZ", 5)
    cmds.setAttr(strWristLF+".oldMinX", -12.5)
    cmds.setAttr(strWristLF+".oldMinY", -12.5)
    cmds.setAttr(strWristLF+".oldMinZ", -12.5)
    cmds.setAttr(strWristLF+".oldMaxX", 10)
    cmds.setAttr(strWristLF+".oldMaxY", 10)
    cmds.setAttr(strWristLF+".oldMaxZ", 10)

    cmds.setAttr(strForearmSplit4LF+".minX", -7)
    cmds.setAttr(strForearmSplit4LF+".minY", -7)
    cmds.setAttr(strForearmSplit4LF+".minZ", -7)
    cmds.setAttr(strForearmSplit4LF+".maxX", 7)
    cmds.setAttr(strForearmSplit4LF+".maxY", 7)
    cmds.setAttr(strForearmSplit4LF+".maxZ", 7)
    cmds.setAttr(strForearmSplit4LF+".oldMinX", -11)
    cmds.setAttr(strForearmSplit4LF+".oldMinY", -11)
    cmds.setAttr(strForearmSplit4LF+".oldMinZ", -11)
    cmds.setAttr(strForearmSplit4LF+".oldMaxX", 10)
    cmds.setAttr(strForearmSplit4LF+".oldMaxY", 10)
    cmds.setAttr(strForearmSplit4LF+".oldMaxZ", 10)

    cmds.setAttr(strForearmSplit3LF+".minX", -9)
    cmds.setAttr(strForearmSplit3LF+".minY", -9)
    cmds.setAttr(strForearmSplit3LF+".minZ", -9)
    cmds.setAttr(strForearmSplit3LF+".maxX", 9)
    cmds.setAttr(strForearmSplit3LF+".maxY", 9)
    cmds.setAttr(strForearmSplit3LF+".maxZ", 9)
    cmds.setAttr(strForearmSplit3LF+".oldMinX", -10.25)
    cmds.setAttr(strForearmSplit3LF+".oldMinY", -10.25)
    cmds.setAttr(strForearmSplit3LF+".oldMinZ", -10.25)
    cmds.setAttr(strForearmSplit3LF+".oldMaxX", 10)
    cmds.setAttr(strForearmSplit3LF+".oldMaxY", 10)
    cmds.setAttr(strForearmSplit3LF+".oldMaxZ", 10)

    # connection controls to multiplies
    cmds.connectAttr(upArmTcntLF+".scaleX", mlpUpArmLF+".input1X", force=True)
    cmds.connectAttr(upArmTcntLF+".scaleY", mlpUpArmLF+".input1Y", force=True)
    cmds.connectAttr(upArmTcntLF+".scaleZ", mlpUpArmLF+".input1Z", force=True)

    cmds.connectAttr(elbowTcntLF+".scaleX", mlpElbowLF+".input1X", force=True)
    cmds.connectAttr(elbowTcntLF+".scaleY", mlpElbowLF+".input1Y", force=True)
    cmds.connectAttr(elbowTcntLF+".scaleZ", mlpElbowLF+".input1Z", force=True)

    cmds.connectAttr(forearmTcntLF+".scaleX", mlpForearmLF+".input1X", force=True)
    cmds.connectAttr(forearmTcntLF+".scaleY", mlpForearmLF+".input1Y", force=True)
    cmds.connectAttr(forearmTcntLF+".scaleZ", mlpForearmLF+".input1Z", force=True)

    # connection mlps to strs
    cmds.connectAttr(mlpUpArmLF+".outputX", strUpArmLF+".valueX", force=True)
    cmds.connectAttr(mlpUpArmLF+".outputY", strUpArmLF+".valueY", force=True)
    cmds.connectAttr(mlpUpArmLF+".outputZ", strUpArmLF+".valueZ", force=True)

    cmds.connectAttr(mlpUpArmLF+".outputX", strUpArmLFSplit1LF+".valueX", force=True)
    cmds.connectAttr(mlpUpArmLF+".outputY", strUpArmLFSplit1LF+".valueY", force=True)
    cmds.connectAttr(mlpUpArmLF+".outputZ", strUpArmLFSplit1LF+".valueZ", force=True)

    cmds.connectAttr(mlpUpArmLF+".outputX", strUpArmLFSpilt2LF+".valueX", force=True)
    cmds.connectAttr(mlpUpArmLF+".outputY", strUpArmLFSpilt2LF+".valueY", force=True)
    cmds.connectAttr(mlpUpArmLF+".outputZ", strUpArmLFSpilt2LF+".valueZ", force=True)

    cmds.connectAttr(mlpForearmLF+".outputX", strForearmSplit3LF+".valueX", force=True)
    cmds.connectAttr(mlpForearmLF+".outputY", strForearmSplit3LF+".valueY", force=True)
    cmds.connectAttr(mlpForearmLF+".outputZ", strForearmSplit3LF+".valueZ", force=True)

    cmds.connectAttr(mlpForearmLF+".outputX", strForearmSplit4LF+".valueX", force=True)
    cmds.connectAttr(mlpForearmLF+".outputY", strForearmSplit4LF+".valueY", force=True)
    cmds.connectAttr(mlpForearmLF+".outputZ", strForearmSplit4LF+".valueZ", force=True)

    cmds.connectAttr(mlpForearmLF+".outputX", strWristLF+".valueX", force=True)
    cmds.connectAttr(mlpForearmLF+".outputY", strWristLF+".valueY", force=True)
    cmds.connectAttr(mlpForearmLF+".outputZ", strWristLF+".valueZ", force=True)

    # connection scale directly where doesn't need the scale averaged
    cmds.connectAttr(mlpUpArmLF+".outputX", "RIG_lf_upArmWeightSplit_3_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpUpArmLF+".outputY", "RIG_lf_upArmWeightSplit_3_jnt.scaleX", force=True)
    cmds.connectAttr(mlpUpArmLF+".outputZ", "RIG_lf_upArmWeightSplit_3_jnt.scaleY", force=True)

    cmds.connectAttr(mlpElbowLF+".outputX", "RIG_lf_upArmWeightSplit_4_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpElbowLF+".outputY", "RIG_lf_upArmWeightSplit_4_jnt.scaleX", force=True)
    cmds.connectAttr(mlpElbowLF+".outputZ", "RIG_lf_upArmWeightSplit_4_jnt.scaleY", force=True)

    cmds.connectAttr(mlpElbowLF+".outputX", "RIG_lf_elbowWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpElbowLF+".outputY", "RIG_lf_elbowWeight_jnt.scaleX", force=True)
    cmds.connectAttr(mlpElbowLF+".outputZ", "RIG_lf_elbowWeight_jnt.scaleY", force=True)

    cmds.connectAttr(mlpElbowLF+".outputX", "RIG_lf_elbowWeightSplit_1_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpElbowLF+".outputY", "RIG_lf_elbowWeightSplit_1_jnt.scaleX", force=True)
    cmds.connectAttr(mlpElbowLF+".outputZ", "RIG_lf_elbowWeightSplit_1_jnt.scaleY", force=True)

    cmds.connectAttr(mlpForearmLF+".outputX", "RIG_lf_elbowWeightSplit_2_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpForearmLF+".outputY", "RIG_lf_elbowWeightSplit_2_jnt.scaleX", force=True)
    cmds.connectAttr(mlpForearmLF+".outputZ", "RIG_lf_elbowWeightSplit_2_jnt.scaleY", force=True)

    # connection
    cmds.connectAttr(strUpArmLF+".outValueX", "RIG_lf_upArmWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpArmLF+".outValueY", "RIG_lf_upArmWeight_jnt.scaleX", force=True)
    cmds.connectAttr(strUpArmLF+".outValueZ", "RIG_lf_upArmWeight_jnt.scaleY", force=True)

    cmds.connectAttr(strUpArmLFSplit1LF+".outValueX", "RIG_lf_upArmWeightSplit_1_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpArmLFSplit1LF+".outValueY", "RIG_lf_upArmWeightSplit_1_jnt.scaleX", force=True)
    cmds.connectAttr(strUpArmLFSplit1LF+".outValueZ", "RIG_lf_upArmWeightSplit_1_jnt.scaleY", force=True)

    cmds.connectAttr(strUpArmLFSpilt2LF+".outValueX", "RIG_lf_upArmWeightSplit_2_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpArmLFSpilt2LF+".outValueY", "RIG_lf_upArmWeightSplit_2_jnt.scaleX", force=True)
    cmds.connectAttr(strUpArmLFSpilt2LF+".outValueZ", "RIG_lf_upArmWeightSplit_2_jnt.scaleY", force=True)


    cmds.connectAttr(strForearmSplit3LF+".outValueX", "RIG_lf_elbowWeightSplit_3_jnt.scaleZ", force=True)
    cmds.connectAttr(strForearmSplit3LF+".outValueY", "RIG_lf_elbowWeightSplit_3_jnt.scaleX", force=True)
    cmds.connectAttr(strForearmSplit3LF+".outValueZ", "RIG_lf_elbowWeightSplit_3_jnt.scaleY", force=True)

    cmds.connectAttr(strForearmSplit4LF+".outValueX", "RIG_lf_elbowWeightSplit_4_jnt.scaleZ", force=True)
    cmds.connectAttr(strForearmSplit4LF+".outValueY", "RIG_lf_elbowWeightSplit_4_jnt.scaleX", force=True)
    cmds.connectAttr(strForearmSplit4LF+".outValueZ", "RIG_lf_elbowWeightSplit_4_jnt.scaleY", force=True)

    cmds.connectAttr(strWristLF+".outValueX", "RIG_lf_wristWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(strWristLF+".outValueY", "RIG_lf_wristWeight_jnt.scaleX", force=True)
    cmds.connectAttr(strWristLF+".outValueZ", "RIG_lf_wristWeight_jnt.scaleY", force=True)


    ############################################
    #               Arm Toony Right            #
    ############################################

    # mlp and str first armToony ---> RIG_rt_upArmRubberHose_ctrl
    mlpUpArmRT = cmds.createNode("multiplyDivide", n="MLP_RIG_rt_upArmRubberHose_ctrl")
    strUpArmRT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_upArmWeight_jnt")
    strUpArmRTSplit1RT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_upArmWeightSplit_1_jnt")
    strUpArmRTSpilt2RT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_upArmWeightSplit_2_jnt")

    cmds.setAttr(strUpArmRT+".minX", -5)
    cmds.setAttr(strUpArmRT+".minY", -5)
    cmds.setAttr(strUpArmRT+".minZ", -5)
    cmds.setAttr(strUpArmRT+".maxX", 5)
    cmds.setAttr(strUpArmRT+".maxY", 5)
    cmds.setAttr(strUpArmRT+".maxZ", 5)
    cmds.setAttr(strUpArmRT+".oldMinX", -12.5)
    cmds.setAttr(strUpArmRT+".oldMinY", -12.5)
    cmds.setAttr(strUpArmRT+".oldMinZ", -12.5)
    cmds.setAttr(strUpArmRT+".oldMaxX", 10)
    cmds.setAttr(strUpArmRT+".oldMaxY", 10)
    cmds.setAttr(strUpArmRT+".oldMaxZ", 10)

    cmds.setAttr(strUpArmRTSplit1RT+".minX", -7)
    cmds.setAttr(strUpArmRTSplit1RT+".minY", -7)
    cmds.setAttr(strUpArmRTSplit1RT+".minZ", -7)
    cmds.setAttr(strUpArmRTSplit1RT+".maxX", 7)
    cmds.setAttr(strUpArmRTSplit1RT+".maxY", 7)
    cmds.setAttr(strUpArmRTSplit1RT+".maxZ", 7)
    cmds.setAttr(strUpArmRTSplit1RT+".oldMinX", -11)
    cmds.setAttr(strUpArmRTSplit1RT+".oldMinY", -11)
    cmds.setAttr(strUpArmRTSplit1RT+".oldMinZ", -11)
    cmds.setAttr(strUpArmRTSplit1RT+".oldMaxX", 10)
    cmds.setAttr(strUpArmRTSplit1RT+".oldMaxY", 10)
    cmds.setAttr(strUpArmRTSplit1RT+".oldMaxZ", 10)

    cmds.setAttr(strUpArmRTSpilt2RT+".minX", -9)
    cmds.setAttr(strUpArmRTSpilt2RT+".minY", -9)
    cmds.setAttr(strUpArmRTSpilt2RT+".minZ", -9)
    cmds.setAttr(strUpArmRTSpilt2RT+".maxX", 9)
    cmds.setAttr(strUpArmRTSpilt2RT+".maxY", 9)
    cmds.setAttr(strUpArmRTSpilt2RT+".maxZ", 9)
    cmds.setAttr(strUpArmRTSpilt2RT+".oldMinX", -10.25)
    cmds.setAttr(strUpArmRTSpilt2RT+".oldMinY", -10.25)
    cmds.setAttr(strUpArmRTSpilt2RT+".oldMinZ", -10.25)
    cmds.setAttr(strUpArmRTSpilt2RT+".oldMaxX", 10)
    cmds.setAttr(strUpArmRTSpilt2RT+".oldMaxY", 10)
    cmds.setAttr(strUpArmRTSpilt2RT+".oldMaxZ", 10)

    # mlp second armToony ---> RIG_rt_elbowRubberHose_ctrl
    mlpElbowRT = cmds.createNode("multiplyDivide", n="MLP_RIG_rt_elbowRubberHose_ctrl")

    # mlp and str first armToony ---> RIG_rt_forearmRubberHose_ctrl
    mlpForearmRT = cmds.createNode("multiplyDivide", n="MLP_RIG_rt_forearmRubberHose_ctrl")
    strForearmSplit3RT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_elbowWeightSplit_3_jnt")
    strForearmSplit4RT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_elbowWeightSplit_4_jnt")
    strWristRT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_elbowWeightSplit_4_jnt")

    cmds.setAttr(strWristRT+".minX", -5)
    cmds.setAttr(strWristRT+".minY", -5)
    cmds.setAttr(strWristRT+".minZ", -5)
    cmds.setAttr(strWristRT+".maxX", 5)
    cmds.setAttr(strWristRT+".maxY", 5)
    cmds.setAttr(strWristRT+".maxZ", 5)
    cmds.setAttr(strWristRT+".oldMinX", -12.5)
    cmds.setAttr(strWristRT+".oldMinY", -12.5)
    cmds.setAttr(strWristRT+".oldMinZ", -12.5)
    cmds.setAttr(strWristRT+".oldMaxX", 10)
    cmds.setAttr(strWristRT+".oldMaxY", 10)
    cmds.setAttr(strWristRT+".oldMaxZ", 10)

    cmds.setAttr(strForearmSplit4RT+".minX", -7)
    cmds.setAttr(strForearmSplit4RT+".minY", -7)
    cmds.setAttr(strForearmSplit4RT+".minZ", -7)
    cmds.setAttr(strForearmSplit4RT+".maxX", 7)
    cmds.setAttr(strForearmSplit4RT+".maxY", 7)
    cmds.setAttr(strForearmSplit4RT+".maxZ", 7)
    cmds.setAttr(strForearmSplit4RT+".oldMinX", -11)
    cmds.setAttr(strForearmSplit4RT+".oldMinY", -11)
    cmds.setAttr(strForearmSplit4RT+".oldMinZ", -11)
    cmds.setAttr(strForearmSplit4RT+".oldMaxX", 10)
    cmds.setAttr(strForearmSplit4RT+".oldMaxY", 10)
    cmds.setAttr(strForearmSplit4RT+".oldMaxZ", 10)

    cmds.setAttr(strForearmSplit3RT+".minX", -9)
    cmds.setAttr(strForearmSplit3RT+".minY", -9)
    cmds.setAttr(strForearmSplit3RT+".minZ", -9)
    cmds.setAttr(strForearmSplit3RT+".maxX", 9)
    cmds.setAttr(strForearmSplit3RT+".maxY", 9)
    cmds.setAttr(strForearmSplit3RT+".maxZ", 9)
    cmds.setAttr(strForearmSplit3RT+".oldMinX", -10.25)
    cmds.setAttr(strForearmSplit3RT+".oldMinY", -10.25)
    cmds.setAttr(strForearmSplit3RT+".oldMinZ", -10.25)
    cmds.setAttr(strForearmSplit3RT+".oldMaxX", 10)
    cmds.setAttr(strForearmSplit3RT+".oldMaxY", 10)
    cmds.setAttr(strForearmSplit3RT+".oldMaxZ", 10)

    # connection controls to multiplies
    cmds.connectAttr(upArmTcntRT+".scaleX", mlpUpArmRT+".input1X", force=True)
    cmds.connectAttr(upArmTcntRT+".scaleY", mlpUpArmRT+".input1Y", force=True)
    cmds.connectAttr(upArmTcntRT+".scaleZ", mlpUpArmRT+".input1Z", force=True)

    cmds.connectAttr(elbowTcntRT+".scaleX", mlpElbowRT+".input1X", force=True)
    cmds.connectAttr(elbowTcntRT+".scaleY", mlpElbowRT+".input1Y", force=True)
    cmds.connectAttr(elbowTcntRT+".scaleZ", mlpElbowRT+".input1Z", force=True)

    cmds.connectAttr(forearmTcntRT+".scaleX", mlpForearmRT+".input1X", force=True)
    cmds.connectAttr(forearmTcntRT+".scaleY", mlpForearmRT+".input1Y", force=True)
    cmds.connectAttr(forearmTcntRT+".scaleZ", mlpForearmRT+".input1Z", force=True)

    # connection mlps to strs
    cmds.connectAttr(mlpUpArmRT+".outputX", strUpArmRT+".valueX", force=True)
    cmds.connectAttr(mlpUpArmRT+".outputY", strUpArmRT+".valueY", force=True)
    cmds.connectAttr(mlpUpArmRT+".outputZ", strUpArmRT+".valueZ", force=True)

    cmds.connectAttr(mlpUpArmRT+".outputX", strUpArmRTSplit1RT+".valueX", force=True)
    cmds.connectAttr(mlpUpArmRT+".outputY", strUpArmRTSplit1RT+".valueY", force=True)
    cmds.connectAttr(mlpUpArmRT+".outputZ", strUpArmRTSplit1RT+".valueZ", force=True)

    cmds.connectAttr(mlpUpArmRT+".outputX", strUpArmRTSpilt2RT+".valueX", force=True)
    cmds.connectAttr(mlpUpArmRT+".outputY", strUpArmRTSpilt2RT+".valueY", force=True)
    cmds.connectAttr(mlpUpArmRT+".outputZ", strUpArmRTSpilt2RT+".valueZ", force=True)

    cmds.connectAttr(mlpForearmRT+".outputX", strForearmSplit3RT+".valueX", force=True)
    cmds.connectAttr(mlpForearmRT+".outputY", strForearmSplit3RT+".valueY", force=True)
    cmds.connectAttr(mlpForearmRT+".outputZ", strForearmSplit3RT+".valueZ", force=True)

    cmds.connectAttr(mlpForearmRT+".outputX", strForearmSplit4RT+".valueX", force=True)
    cmds.connectAttr(mlpForearmRT+".outputY", strForearmSplit4RT+".valueY", force=True)
    cmds.connectAttr(mlpForearmRT+".outputZ", strForearmSplit4RT+".valueZ", force=True)

    cmds.connectAttr(mlpForearmRT+".outputX", strWristRT+".valueX", force=True)
    cmds.connectAttr(mlpForearmRT+".outputY", strWristRT+".valueY", force=True)
    cmds.connectAttr(mlpForearmRT+".outputZ", strWristRT+".valueZ", force=True)

    # connection scale directly where doesn't need the scale averaged
    cmds.connectAttr(mlpUpArmRT+".outputX", "RIG_rt_upArmWeightSplit_3_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpUpArmRT+".outputY", "RIG_rt_upArmWeightSplit_3_jnt.scaleX", force=True)
    cmds.connectAttr(mlpUpArmRT+".outputZ", "RIG_rt_upArmWeightSplit_3_jnt.scaleY", force=True)

    cmds.connectAttr(mlpElbowRT+".outputX", "RIG_rt_upArmWeightSplit_4_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpElbowRT+".outputY", "RIG_rt_upArmWeightSplit_4_jnt.scaleX", force=True)
    cmds.connectAttr(mlpElbowRT+".outputZ", "RIG_rt_upArmWeightSplit_4_jnt.scaleY", force=True)

    cmds.connectAttr(mlpElbowRT+".outputX", "RIG_rt_elbowWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpElbowRT+".outputY", "RIG_rt_elbowWeight_jnt.scaleX", force=True)
    cmds.connectAttr(mlpElbowRT+".outputZ", "RIG_rt_elbowWeight_jnt.scaleY", force=True)

    cmds.connectAttr(mlpElbowRT+".outputX", "RIG_rt_elbowWeightSplit_1_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpElbowRT+".outputY", "RIG_rt_elbowWeightSplit_1_jnt.scaleX", force=True)
    cmds.connectAttr(mlpElbowRT+".outputZ", "RIG_rt_elbowWeightSplit_1_jnt.scaleY", force=True)


    cmds.connectAttr(mlpForearmRT+".outputX", "RIG_rt_elbowWeightSplit_2_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpForearmRT+".outputY", "RIG_rt_elbowWeightSplit_2_jnt.scaleX", force=True)
    cmds.connectAttr(mlpForearmRT+".outputZ", "RIG_rt_elbowWeightSplit_2_jnt.scaleY", force=True)

    # connection strs output to jnts scale
    cmds.connectAttr(strUpArmRT+".outValueX", "RIG_rt_upArmWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpArmRT+".outValueY", "RIG_rt_upArmWeight_jnt.scaleX", force=True)
    cmds.connectAttr(strUpArmRT+".outValueZ", "RIG_rt_upArmWeight_jnt.scaleY", force=True)

    cmds.connectAttr(strUpArmRTSplit1RT+".outValueX", "RIG_rt_upArmWeightSplit_1_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpArmRTSplit1RT+".outValueY", "RIG_rt_upArmWeightSplit_1_jnt.scaleX", force=True)
    cmds.connectAttr(strUpArmRTSplit1RT+".outValueZ", "RIG_rt_upArmWeightSplit_1_jnt.scaleY", force=True)

    cmds.connectAttr(strUpArmRTSpilt2RT+".outValueX", "RIG_rt_upArmWeightSplit_2_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpArmRTSpilt2RT+".outValueY", "RIG_rt_upArmWeightSplit_2_jnt.scaleX", force=True)
    cmds.connectAttr(strUpArmRTSpilt2RT+".outValueZ", "RIG_rt_upArmWeightSplit_2_jnt.scaleY", force=True)

    cmds.connectAttr(strForearmSplit3RT+".outValueX", "RIG_rt_elbowWeightSplit_3_jnt.scaleZ", force=True)
    cmds.connectAttr(strForearmSplit3RT+".outValueY", "RIG_rt_elbowWeightSplit_3_jnt.scaleX", force=True)
    cmds.connectAttr(strForearmSplit3RT+".outValueZ", "RIG_rt_elbowWeightSplit_3_jnt.scaleY", force=True)

    cmds.connectAttr(strForearmSplit4RT+".outValueX", "RIG_rt_elbowWeightSplit_4_jnt.scaleZ", force=True)
    cmds.connectAttr(strForearmSplit4RT+".outValueY", "RIG_rt_elbowWeightSplit_4_jnt.scaleX", force=True)
    cmds.connectAttr(strForearmSplit4RT+".outValueZ", "RIG_rt_elbowWeightSplit_4_jnt.scaleY", force=True)

    cmds.connectAttr(strWristRT+".outValueX", "RIG_rt_wristWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(strWristRT+".outValueY", "RIG_rt_wristWeight_jnt.scaleX", force=True)
    cmds.connectAttr(strWristRT+".outValueZ", "RIG_rt_wristWeight_jnt.scaleY", force=True)

    ############################################
    #               Leg Toony Fix              #
    ############################################

    upLegTcntLF = "RIG_lf_upLegRubberHose_ctrl"
    kneeTcntLF = "RIG_lf_kneeRubberHose_ctrl"
    tibiaTcntLF = "RIG_lf_lowLegRubberHose_ctrl"

    upLegTcntRT = "RIG_rt_upLegRubberHose_ctrl"
    kneeTcntRT = "RIG_rt_kneeRubberHose_ctrl"
    tibiaTcntRT = "RIG_rt_lowLegRubberHose_ctrl"

    #Slocca i canali dello scale a tutti rubberHose controls
    cmds.setAttr("RIG_lf_upLegRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_lf_upLegRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_lf_upLegRubberHose_ctrl.sz", k=True, l=False)
    cmds.setAttr("RIG_lf_kneeRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_lf_kneeRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_lf_kneeRubberHose_ctrl.sz", k=True, l=False)
    cmds.setAttr("RIG_lf_lowLegRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_lf_lowLegRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_lf_lowLegRubberHose_ctrl.sz", k=True, l=False)

    cmds.setAttr("RIG_rt_upLegRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_rt_upLegRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_rt_upLegRubberHose_ctrl.sz", k=True, l=False)
    cmds.setAttr("RIG_rt_kneeRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_rt_kneeRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_rt_kneeRubberHose_ctrl.sz", k=True, l=False)
    cmds.setAttr("RIG_rt_lowLegRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_rt_lowLegRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_rt_lowLegRubberHose_ctrl.sz", k=True, l=False)

    ############################################
    #               Leg Toony Left             #
    ############################################

    # mlp and str first armToony ---> RIG_lf_upLegRubberHose_ctrl
    mlpUpLegLF = cmds.createNode("multiplyDivide", n="MLP_RIG_lf_upLegRubberHose_ctrl")
    strUpLegLF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_upLegWeight_jnt")
    strUpLegLFSplit1LF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_upLegWeightSplit_1_jnt")
    strUpLegLFSpilt2LF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_upLegWeightSplit_2_jnt")

    cmds.setAttr(strUpLegLF+".minX", -5)
    cmds.setAttr(strUpLegLF+".minY", -5)
    cmds.setAttr(strUpLegLF+".minZ", -5)
    cmds.setAttr(strUpLegLF+".maxX", 5)
    cmds.setAttr(strUpLegLF+".maxY", 5)
    cmds.setAttr(strUpLegLF+".maxZ", 5)
    cmds.setAttr(strUpLegLF+".oldMinX", -12.5)
    cmds.setAttr(strUpLegLF+".oldMinY", -12.5)
    cmds.setAttr(strUpLegLF+".oldMinZ", -12.5)
    cmds.setAttr(strUpLegLF+".oldMaxX", 10)
    cmds.setAttr(strUpLegLF+".oldMaxY", 10)
    cmds.setAttr(strUpLegLF+".oldMaxZ", 10)

    cmds.setAttr(strUpLegLFSplit1LF+".minX", -7)
    cmds.setAttr(strUpLegLFSplit1LF+".minY", -7)
    cmds.setAttr(strUpLegLFSplit1LF+".minZ", -7)
    cmds.setAttr(strUpLegLFSplit1LF+".maxX", 7)
    cmds.setAttr(strUpLegLFSplit1LF+".maxY", 7)
    cmds.setAttr(strUpLegLFSplit1LF+".maxZ", 7)
    cmds.setAttr(strUpLegLFSplit1LF+".oldMinX", -11)
    cmds.setAttr(strUpLegLFSplit1LF+".oldMinY", -11)
    cmds.setAttr(strUpLegLFSplit1LF+".oldMinZ", -11)
    cmds.setAttr(strUpLegLFSplit1LF+".oldMaxX", 10)
    cmds.setAttr(strUpLegLFSplit1LF+".oldMaxY", 10)
    cmds.setAttr(strUpLegLFSplit1LF+".oldMaxZ", 10)

    cmds.setAttr(strUpLegLFSpilt2LF+".minX", -9)
    cmds.setAttr(strUpLegLFSpilt2LF+".minY", -9)
    cmds.setAttr(strUpLegLFSpilt2LF+".minZ", -9)
    cmds.setAttr(strUpLegLFSpilt2LF+".maxX", 9)
    cmds.setAttr(strUpLegLFSpilt2LF+".maxY", 9)
    cmds.setAttr(strUpLegLFSpilt2LF+".maxZ", 9)
    cmds.setAttr(strUpLegLFSpilt2LF+".oldMinX", -10.25)
    cmds.setAttr(strUpLegLFSpilt2LF+".oldMinY", -10.25)
    cmds.setAttr(strUpLegLFSpilt2LF+".oldMinZ", -10.25)
    cmds.setAttr(strUpLegLFSpilt2LF+".oldMaxX", 10)
    cmds.setAttr(strUpLegLFSpilt2LF+".oldMaxY", 10)
    cmds.setAttr(strUpLegLFSpilt2LF+".oldMaxZ", 10)

    # mlp second armToony ---> RIG_lf_kneeRubberHose_ctrl
    mlpKneeLF = cmds.createNode("multiplyDivide", n="MLP_RIG_lf_kneeRubberHose_ctrl")

    # mlp and str first armToony ---> RIG_lf_lowLegRubberHose_ctrl
    mlpTibiaLF = cmds.createNode("multiplyDivide", n="MLP_RIG_lf_lowLegRubberHose_ctrl")
    strTibiaSplit3LF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_lowLegWeightSplit_3_jnt")
    strTibiaSplit4LF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_lowLegWeightSplit_4_jnt")
    strAnkleLF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_ankleWeight_jnt")

    cmds.setAttr(strAnkleLF+".minX", -5)
    cmds.setAttr(strAnkleLF+".minY", -5)
    cmds.setAttr(strAnkleLF+".minZ", -5)
    cmds.setAttr(strAnkleLF+".maxX", 5)
    cmds.setAttr(strAnkleLF+".maxY", 5)
    cmds.setAttr(strAnkleLF+".maxZ", 5)
    cmds.setAttr(strAnkleLF+".oldMinX", -12.5)
    cmds.setAttr(strAnkleLF+".oldMinY", -12.5)
    cmds.setAttr(strAnkleLF+".oldMinZ", -12.5)
    cmds.setAttr(strAnkleLF+".oldMaxX", 10)
    cmds.setAttr(strAnkleLF+".oldMaxY", 10)
    cmds.setAttr(strAnkleLF+".oldMaxZ", 10)

    cmds.setAttr(strTibiaSplit4LF+".minX", -7)
    cmds.setAttr(strTibiaSplit4LF+".minY", -7)
    cmds.setAttr(strTibiaSplit4LF+".minZ", -7)
    cmds.setAttr(strTibiaSplit4LF+".maxX", 7)
    cmds.setAttr(strTibiaSplit4LF+".maxY", 7)
    cmds.setAttr(strTibiaSplit4LF+".maxZ", 7)
    cmds.setAttr(strTibiaSplit4LF+".oldMinX", -11)
    cmds.setAttr(strTibiaSplit4LF+".oldMinY", -11)
    cmds.setAttr(strTibiaSplit4LF+".oldMinZ", -11)
    cmds.setAttr(strTibiaSplit4LF+".oldMaxX", 10)
    cmds.setAttr(strTibiaSplit4LF+".oldMaxY", 10)
    cmds.setAttr(strTibiaSplit4LF+".oldMaxZ", 10)

    cmds.setAttr(strTibiaSplit3LF+".minX", -9)
    cmds.setAttr(strTibiaSplit3LF+".minY", -9)
    cmds.setAttr(strTibiaSplit3LF+".minZ", -9)
    cmds.setAttr(strTibiaSplit3LF+".maxX", 9)
    cmds.setAttr(strTibiaSplit3LF+".maxY", 9)
    cmds.setAttr(strTibiaSplit3LF+".maxZ", 9)
    cmds.setAttr(strTibiaSplit3LF+".oldMinX", -10.25)
    cmds.setAttr(strTibiaSplit3LF+".oldMinY", -10.25)
    cmds.setAttr(strTibiaSplit3LF+".oldMinZ", -10.25)
    cmds.setAttr(strTibiaSplit3LF+".oldMaxX", 10)
    cmds.setAttr(strTibiaSplit3LF+".oldMaxY", 10)
    cmds.setAttr(strTibiaSplit3LF+".oldMaxZ", 10)

    # connection controls to multiplies
    cmds.connectAttr(upLegTcntLF+".scaleX", mlpUpLegLF+".input1X", force=True)
    cmds.connectAttr(upLegTcntLF+".scaleY", mlpUpLegLF+".input1Y", force=True)
    cmds.connectAttr(upLegTcntLF+".scaleZ", mlpUpLegLF+".input1Z", force=True)

    cmds.connectAttr(kneeTcntLF+".scaleX", mlpKneeLF+".input1X", force=True)
    cmds.connectAttr(kneeTcntLF+".scaleY", mlpKneeLF+".input1Y", force=True)
    cmds.connectAttr(kneeTcntLF+".scaleZ", mlpKneeLF+".input1Z", force=True)

    cmds.connectAttr(tibiaTcntLF+".scaleX", mlpTibiaLF+".input1X", force=True)
    cmds.connectAttr(tibiaTcntLF+".scaleY", mlpTibiaLF+".input1Y", force=True)
    cmds.connectAttr(tibiaTcntLF+".scaleZ", mlpTibiaLF+".input1Z", force=True)

    # connection mlps to strs
    cmds.connectAttr(mlpUpLegLF+".outputX", strUpLegLF+".valueX", force=True)
    cmds.connectAttr(mlpUpLegLF+".outputY", strUpLegLF+".valueY", force=True)
    cmds.connectAttr(mlpUpLegLF+".outputZ", strUpLegLF+".valueZ", force=True)

    cmds.connectAttr(mlpUpLegLF+".outputX", strUpLegLFSplit1LF+".valueX", force=True)
    cmds.connectAttr(mlpUpLegLF+".outputY", strUpLegLFSplit1LF+".valueY", force=True)
    cmds.connectAttr(mlpUpLegLF+".outputZ", strUpLegLFSplit1LF+".valueZ", force=True)

    cmds.connectAttr(mlpUpLegLF+".outputX", strUpLegLFSpilt2LF+".valueX", force=True)
    cmds.connectAttr(mlpUpLegLF+".outputY", strUpLegLFSpilt2LF+".valueY", force=True)
    cmds.connectAttr(mlpUpLegLF+".outputZ", strUpLegLFSpilt2LF+".valueZ", force=True)

    cmds.connectAttr(mlpTibiaLF+".outputX", strTibiaSplit3LF+".valueX", force=True)
    cmds.connectAttr(mlpTibiaLF+".outputY", strTibiaSplit3LF+".valueY", force=True)
    cmds.connectAttr(mlpTibiaLF+".outputZ", strTibiaSplit3LF+".valueZ", force=True)

    cmds.connectAttr(mlpTibiaLF+".outputX", strTibiaSplit4LF+".valueX", force=True)
    cmds.connectAttr(mlpTibiaLF+".outputY", strTibiaSplit4LF+".valueY", force=True)
    cmds.connectAttr(mlpTibiaLF+".outputZ", strTibiaSplit4LF+".valueZ", force=True)

    cmds.connectAttr(mlpTibiaLF+".outputX", strAnkleLF+".valueX", force=True)
    cmds.connectAttr(mlpTibiaLF+".outputY", strAnkleLF+".valueY", force=True)
    cmds.connectAttr(mlpTibiaLF+".outputZ", strAnkleLF+".valueZ", force=True)

    # connection scale directly where doesn't need the scale averaged
    cmds.connectAttr(mlpUpLegLF+".outputX", "RIG_lf_upLegWeightSplit_3_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpUpLegLF+".outputY", "RIG_lf_upLegWeightSplit_3_jnt.scaleX", force=True)
    cmds.connectAttr(mlpUpLegLF+".outputZ", "RIG_lf_upLegWeightSplit_3_jnt.scaleY", force=True)

    cmds.connectAttr(mlpKneeLF+".outputX", "RIG_lf_upLegWeightSplit_4_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpKneeLF+".outputY", "RIG_lf_upLegWeightSplit_4_jnt.scaleX", force=True)
    cmds.connectAttr(mlpKneeLF+".outputZ", "RIG_lf_upLegWeightSplit_4_jnt.scaleY", force=True)

    cmds.connectAttr(mlpKneeLF+".outputX", "RIG_lf_kneeWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpKneeLF+".outputY", "RIG_lf_kneeWeight_jnt.scaleX", force=True)
    cmds.connectAttr(mlpKneeLF+".outputZ", "RIG_lf_kneeWeight_jnt.scaleY", force=True)

    cmds.connectAttr(mlpKneeLF+".outputX", "RIG_lf_kneeWeightSplit_1_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpKneeLF+".outputY", "RIG_lf_kneeWeightSplit_1_jnt.scaleX", force=True)
    cmds.connectAttr(mlpKneeLF+".outputZ", "RIG_lf_kneeWeightSplit_1_jnt.scaleY", force=True)

    cmds.connectAttr(mlpTibiaLF+".outputX", "RIG_lf_kneeWeightSplit_2_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpTibiaLF+".outputY", "RIG_lf_kneeWeightSplit_2_jnt.scaleX", force=True)
    cmds.connectAttr(mlpTibiaLF+".outputZ", "RIG_lf_kneeWeightSplit_2_jnt.scaleY", force=True)

    # connection
    cmds.connectAttr(strUpLegLF+".outValueX", "RIG_lf_upLegWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpLegLF+".outValueY", "RIG_lf_upLegWeight_jnt.scaleX", force=True)
    cmds.connectAttr(strUpLegLF+".outValueZ", "RIG_lf_upLegWeight_jnt.scaleY", force=True)

    cmds.connectAttr(strUpLegLFSplit1LF+".outValueX", "RIG_lf_upLegWeightSplit_1_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpLegLFSplit1LF+".outValueY", "RIG_lf_upLegWeightSplit_1_jnt.scaleX", force=True)
    cmds.connectAttr(strUpLegLFSplit1LF+".outValueZ", "RIG_lf_upLegWeightSplit_1_jnt.scaleY", force=True)

    cmds.connectAttr(strUpLegLFSpilt2LF+".outValueX", "RIG_lf_upLegWeightSplit_2_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpLegLFSpilt2LF+".outValueY", "RIG_lf_upLegWeightSplit_2_jnt.scaleX", force=True)
    cmds.connectAttr(strUpLegLFSpilt2LF+".outValueZ", "RIG_lf_upLegWeightSplit_2_jnt.scaleY", force=True)

    cmds.connectAttr(strTibiaSplit3LF+".outValueX", "RIG_lf_kneeWeightSplit_3_jnt.scaleZ", force=True)
    cmds.connectAttr(strTibiaSplit3LF+".outValueY", "RIG_lf_kneeWeightSplit_3_jnt.scaleX", force=True)
    cmds.connectAttr(strTibiaSplit3LF+".outValueZ", "RIG_lf_kneeWeightSplit_3_jnt.scaleY", force=True)

    cmds.connectAttr(strTibiaSplit4LF+".outValueX", "RIG_lf_kneeWeightSplit_4_jnt.scaleZ", force=True)
    cmds.connectAttr(strTibiaSplit4LF+".outValueY", "RIG_lf_kneeWeightSplit_4_jnt.scaleX", force=True)
    cmds.connectAttr(strTibiaSplit4LF+".outValueZ", "RIG_lf_kneeWeightSplit_4_jnt.scaleY", force=True)

    cmds.connectAttr(strAnkleLF+".outValueX", "RIG_lf_ankleWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(strAnkleLF+".outValueY", "RIG_lf_ankleWeight_jnt.scaleX", force=True)
    cmds.connectAttr(strAnkleLF+".outValueZ", "RIG_lf_ankleWeight_jnt.scaleY", force=True)


    ############################################
    #               Leg Toony Right            #
    ############################################

    # mlp and str first armToony ---> RIG_rt_upLegRubberHose_ctrl
    mlpUpLegRT = cmds.createNode("multiplyDivide", n="MLP_RIG_rt_upLegRubberHose_ctrl")
    strUpLegRT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_upArmWeight_jnt")
    strUpLegRTSplit1RT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_upArmWeightSplit_1_jnt")
    strUpLegRTSpilt2RT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_upArmWeightSplit_2_jnt")

    cmds.setAttr(strUpLegRT+".minX", -5)
    cmds.setAttr(strUpLegRT+".minY", -5)
    cmds.setAttr(strUpLegRT+".minZ", -5)
    cmds.setAttr(strUpLegRT+".maxX", 5)
    cmds.setAttr(strUpLegRT+".maxY", 5)
    cmds.setAttr(strUpLegRT+".maxZ", 5)
    cmds.setAttr(strUpLegRT+".oldMinX", -12.5)
    cmds.setAttr(strUpLegRT+".oldMinY", -12.5)
    cmds.setAttr(strUpLegRT+".oldMinZ", -12.5)
    cmds.setAttr(strUpLegRT+".oldMaxX", 10)
    cmds.setAttr(strUpLegRT+".oldMaxY", 10)
    cmds.setAttr(strUpLegRT+".oldMaxZ", 10)

    cmds.setAttr(strUpLegRTSplit1RT+".minX", -7)
    cmds.setAttr(strUpLegRTSplit1RT+".minY", -7)
    cmds.setAttr(strUpLegRTSplit1RT+".minZ", -7)
    cmds.setAttr(strUpLegRTSplit1RT+".maxX", 7)
    cmds.setAttr(strUpLegRTSplit1RT+".maxY", 7)
    cmds.setAttr(strUpLegRTSplit1RT+".maxZ", 7)
    cmds.setAttr(strUpLegRTSplit1RT+".oldMinX", -11)
    cmds.setAttr(strUpLegRTSplit1RT+".oldMinY", -11)
    cmds.setAttr(strUpLegRTSplit1RT+".oldMinZ", -11)
    cmds.setAttr(strUpLegRTSplit1RT+".oldMaxX", 10)
    cmds.setAttr(strUpLegRTSplit1RT+".oldMaxY", 10)
    cmds.setAttr(strUpLegRTSplit1RT+".oldMaxZ", 10)

    cmds.setAttr(strUpLegRTSpilt2RT+".minX", -9)
    cmds.setAttr(strUpLegRTSpilt2RT+".minY", -9)
    cmds.setAttr(strUpLegRTSpilt2RT+".minZ", -9)
    cmds.setAttr(strUpLegRTSpilt2RT+".maxX", 9)
    cmds.setAttr(strUpLegRTSpilt2RT+".maxY", 9)
    cmds.setAttr(strUpLegRTSpilt2RT+".maxZ", 9)
    cmds.setAttr(strUpLegRTSpilt2RT+".oldMinX", -10.25)
    cmds.setAttr(strUpLegRTSpilt2RT+".oldMinY", -10.25)
    cmds.setAttr(strUpLegRTSpilt2RT+".oldMinZ", -10.25)
    cmds.setAttr(strUpLegRTSpilt2RT+".oldMaxX", 10)
    cmds.setAttr(strUpLegRTSpilt2RT+".oldMaxY", 10)
    cmds.setAttr(strUpLegRTSpilt2RT+".oldMaxZ", 10)

    # mlp second armToony ---> RIG_rt_kneeRubberHose_ctrl
    mlpKneeRT = cmds.createNode("multiplyDivide", n="MLP_RIG_rt_kneeRubberHose_ctrl")

    # mlp and str first armToony ---> RIG_rt_lowLegRubberHose_ctrl
    mlpTibiaRT = cmds.createNode("multiplyDivide", n="MLP_RIG_rt_lowLegRubberHose_ctrl")
    strTibiaSplit3RT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_elbowWeightSplit_3_jnt")
    strTibiaSplit4RT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_elbowWeightSplit_4_jnt")
    strAnkleRT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_elbowWeightSplit_4_jnt")

    cmds.setAttr(strAnkleRT+".minX", -5)
    cmds.setAttr(strAnkleRT+".minY", -5)
    cmds.setAttr(strAnkleRT+".minZ", -5)
    cmds.setAttr(strAnkleRT+".maxX", 5)
    cmds.setAttr(strAnkleRT+".maxY", 5)
    cmds.setAttr(strAnkleRT+".maxZ", 5)
    cmds.setAttr(strAnkleRT+".oldMinX", -12.5)
    cmds.setAttr(strAnkleRT+".oldMinY", -12.5)
    cmds.setAttr(strAnkleRT+".oldMinZ", -12.5)
    cmds.setAttr(strAnkleRT+".oldMaxX", 10)
    cmds.setAttr(strAnkleRT+".oldMaxY", 10)
    cmds.setAttr(strAnkleRT+".oldMaxZ", 10)

    cmds.setAttr(strTibiaSplit4RT+".minX", -7)
    cmds.setAttr(strTibiaSplit4RT+".minY", -7)
    cmds.setAttr(strTibiaSplit4RT+".minZ", -7)
    cmds.setAttr(strTibiaSplit4RT+".maxX", 7)
    cmds.setAttr(strTibiaSplit4RT+".maxY", 7)
    cmds.setAttr(strTibiaSplit4RT+".maxZ", 7)
    cmds.setAttr(strTibiaSplit4RT+".oldMinX", -11)
    cmds.setAttr(strTibiaSplit4RT+".oldMinY", -11)
    cmds.setAttr(strTibiaSplit4RT+".oldMinZ", -11)
    cmds.setAttr(strTibiaSplit4RT+".oldMaxX", 10)
    cmds.setAttr(strTibiaSplit4RT+".oldMaxY", 10)
    cmds.setAttr(strTibiaSplit4RT+".oldMaxZ", 10)

    cmds.setAttr(strTibiaSplit3RT+".minX", -9)
    cmds.setAttr(strTibiaSplit3RT+".minY", -9)
    cmds.setAttr(strTibiaSplit3RT+".minZ", -9)
    cmds.setAttr(strTibiaSplit3RT+".maxX", 9)
    cmds.setAttr(strTibiaSplit3RT+".maxY", 9)
    cmds.setAttr(strTibiaSplit3RT+".maxZ", 9)
    cmds.setAttr(strTibiaSplit3RT+".oldMinX", -10.25)
    cmds.setAttr(strTibiaSplit3RT+".oldMinY", -10.25)
    cmds.setAttr(strTibiaSplit3RT+".oldMinZ", -10.25)
    cmds.setAttr(strTibiaSplit3RT+".oldMaxX", 10)
    cmds.setAttr(strTibiaSplit3RT+".oldMaxY", 10)
    cmds.setAttr(strTibiaSplit3RT+".oldMaxZ", 10)

    # connection controls to multiplies
    cmds.connectAttr(upLegTcntRT+".scaleX", mlpUpLegRT+".input1X", force=True)
    cmds.connectAttr(upLegTcntRT+".scaleY", mlpUpLegRT+".input1Y", force=True)
    cmds.connectAttr(upLegTcntRT+".scaleZ", mlpUpLegRT+".input1Z", force=True)

    cmds.connectAttr(kneeTcntRT+".scaleX", mlpKneeRT+".input1X", force=True)
    cmds.connectAttr(kneeTcntRT+".scaleY", mlpKneeRT+".input1Y", force=True)
    cmds.connectAttr(kneeTcntRT+".scaleZ", mlpKneeRT+".input1Z", force=True)

    cmds.connectAttr(tibiaTcntRT+".scaleX", mlpTibiaRT+".input1X", force=True)
    cmds.connectAttr(tibiaTcntRT+".scaleY", mlpTibiaRT+".input1Y", force=True)
    cmds.connectAttr(tibiaTcntRT+".scaleZ", mlpTibiaRT+".input1Z", force=True)

    # connection mlps to strs
    cmds.connectAttr(mlpUpLegRT+".outputX", strUpLegRT+".valueX", force=True)
    cmds.connectAttr(mlpUpLegRT+".outputY", strUpLegRT+".valueY", force=True)
    cmds.connectAttr(mlpUpLegRT+".outputZ", strUpLegRT+".valueZ", force=True)

    cmds.connectAttr(mlpUpLegRT+".outputX", strUpLegRTSplit1RT+".valueX", force=True)
    cmds.connectAttr(mlpUpLegRT+".outputY", strUpLegRTSplit1RT+".valueY", force=True)
    cmds.connectAttr(mlpUpLegRT+".outputZ", strUpLegRTSplit1RT+".valueZ", force=True)

    cmds.connectAttr(mlpUpLegRT+".outputX", strUpLegRTSpilt2RT+".valueX", force=True)
    cmds.connectAttr(mlpUpLegRT+".outputY", strUpLegRTSpilt2RT+".valueY", force=True)
    cmds.connectAttr(mlpUpLegRT+".outputZ", strUpLegRTSpilt2RT+".valueZ", force=True)

    cmds.connectAttr(mlpTibiaRT+".outputX", strTibiaSplit3RT+".valueX", force=True)
    cmds.connectAttr(mlpTibiaRT+".outputY", strTibiaSplit3RT+".valueY", force=True)
    cmds.connectAttr(mlpTibiaRT+".outputZ", strTibiaSplit3RT+".valueZ", force=True)

    cmds.connectAttr(mlpTibiaRT+".outputX", strTibiaSplit4RT+".valueX", force=True)
    cmds.connectAttr(mlpTibiaRT+".outputY", strTibiaSplit4RT+".valueY", force=True)
    cmds.connectAttr(mlpTibiaRT+".outputZ", strTibiaSplit4RT+".valueZ", force=True)

    cmds.connectAttr(mlpTibiaRT+".outputX", strAnkleRT+".valueX", force=True)
    cmds.connectAttr(mlpTibiaRT+".outputY", strAnkleRT+".valueY", force=True)
    cmds.connectAttr(mlpTibiaRT+".outputZ", strAnkleRT+".valueZ", force=True)

    # connection scale directly where doesn't need the scale averaged
    cmds.connectAttr(mlpUpLegRT+".outputX", "RIG_rt_upLegWeightSplit_3_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpUpLegRT+".outputY", "RIG_rt_upLegWeightSplit_3_jnt.scaleX", force=True)
    cmds.connectAttr(mlpUpLegRT+".outputZ", "RIG_rt_upLegWeightSplit_3_jnt.scaleY", force=True)

    cmds.connectAttr(mlpKneeRT+".outputX", "RIG_rt_upLegWeightSplit_4_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpKneeRT+".outputY", "RIG_rt_upLegWeightSplit_4_jnt.scaleX", force=True)
    cmds.connectAttr(mlpKneeRT+".outputZ", "RIG_rt_upLegWeightSplit_4_jnt.scaleY", force=True)

    cmds.connectAttr(mlpKneeRT+".outputX", "RIG_rt_kneeWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpKneeRT+".outputY", "RIG_rt_kneeWeight_jnt.scaleX", force=True)
    cmds.connectAttr(mlpKneeRT+".outputZ", "RIG_rt_kneeWeight_jnt.scaleY", force=True)

    cmds.connectAttr(mlpKneeRT+".outputX", "RIG_rt_kneeWeightSplit_1_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpKneeRT+".outputY", "RIG_rt_kneeWeightSplit_1_jnt.scaleX", force=True)
    cmds.connectAttr(mlpKneeRT+".outputZ", "RIG_rt_kneeWeightSplit_1_jnt.scaleY", force=True)

    cmds.connectAttr(mlpTibiaRT+".outputX", "RIG_rt_kneeWeightSplit_2_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpTibiaRT+".outputY", "RIG_rt_kneeWeightSplit_2_jnt.scaleX", force=True)
    cmds.connectAttr(mlpTibiaRT+".outputZ", "RIG_rt_kneeWeightSplit_2_jnt.scaleY", force=True)

    # connection strs output to jnts scale
    cmds.connectAttr(strUpLegRT+".outValueX", "RIG_rt_upLegWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpLegRT+".outValueY", "RIG_rt_upLegWeight_jnt.scaleX", force=True)
    cmds.connectAttr(strUpLegRT+".outValueZ", "RIG_rt_upLegWeight_jnt.scaleY", force=True)

    cmds.connectAttr(strUpLegRTSplit1RT+".outValueX", "RIG_rt_upLegWeightSplit_1_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpLegRTSplit1RT+".outValueY", "RIG_rt_upLegWeightSplit_1_jnt.scaleX", force=True)
    cmds.connectAttr(strUpLegRTSplit1RT+".outValueZ", "RIG_rt_upLegWeightSplit_1_jnt.scaleY", force=True)

    cmds.connectAttr(strUpLegRTSpilt2RT+".outValueX", "RIG_rt_upLegWeightSplit_2_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpLegRTSpilt2RT+".outValueY", "RIG_rt_upLegWeightSplit_2_jnt.scaleX", force=True)
    cmds.connectAttr(strUpLegRTSpilt2RT+".outValueZ", "RIG_rt_upLegWeightSplit_2_jnt.scaleY", force=True)

    cmds.connectAttr(strTibiaSplit3RT+".outValueX", "RIG_rt_kneeWeightSplit_3_jnt.scaleZ", force=True)
    cmds.connectAttr(strTibiaSplit3RT+".outValueY", "RIG_rt_kneeWeightSplit_3_jnt.scaleX", force=True)
    cmds.connectAttr(strTibiaSplit3RT+".outValueZ", "RIG_rt_kneeWeightSplit_3_jnt.scaleY", force=True)

    cmds.connectAttr(strTibiaSplit4RT+".outValueX", "RIG_rt_kneeWeightSplit_4_jnt.scaleZ", force=True)
    cmds.connectAttr(strTibiaSplit4RT+".outValueY", "RIG_rt_kneeWeightSplit_4_jnt.scaleX", force=True)
    cmds.connectAttr(strTibiaSplit4RT+".outValueZ", "RIG_rt_kneeWeightSplit_4_jnt.scaleY", force=True)

    cmds.connectAttr(strAnkleRT+".outValueX", "RIG_rt_ankleWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(strAnkleRT+".outValueY", "RIG_rt_ankleWeight_jnt.scaleX", force=True)
    cmds.connectAttr(strAnkleRT+".outValueZ", "RIG_rt_ankleWeight_jnt.scaleY", force=True)

    cmds.select ( cl=True )

    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #CHIUSURA
    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    cmds.setAttr("RIG_head_ctrl.useRootSpace", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_neck_ctrl.useRootSpace", lock=True, keyable=False, channelBox=False )

    cmds.setAttr("RIG_lf_hand_ctrl.ikVis", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.fkVis", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.shoulderCtrlVis", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.upLimbAutoTwist", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.upLimbTwist", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.upLimbTwistScalar", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.midJntUpLimbTwistScalar", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.useAimTwist", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.rubberHoseCtrlVis", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.autoSquashStretch", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.squashStretch", lock=True, keyable=False, channelBox=False )

    cmds.setAttr("RIG_rt_hand_ctrl.ikVis", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.fkVis", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.shoulderCtrlVis", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.upLimbAutoTwist", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.upLimbTwist", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.upLimbTwistScalar", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.midJntUpLimbTwistScalar", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.useAimTwist", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.rubberHoseCtrlVis", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.autoSquashStretch", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.squashStretch", lock=True, keyable=False, channelBox=False )

    cmds.setAttr("RIG_lf_arm_ik_ctrl.stretchyArm", 1)
    cmds.setAttr("RIG_lf_arm_ik_ctrl.rootCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_arm_ik_ctrl.worldSpaceCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_arm_ik_ctrl.cogCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_arm_ik_ctrl.lowSpineCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_arm_ik_ctrl.hiSpineCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_arm_ik_ctrl.masterSpineCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_arm_ik_ctrl.headCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_arm_ik_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_arm_ik_ctrl.stretchValue", lock=True, keyable=False, channelBox=False )

    cmds.setAttr("RIG_rt_arm_ik_ctrl.stretchyArm", 1)
    cmds.setAttr("RIG_rt_arm_ik_ctrl.rootCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_arm_ik_ctrl.worldSpaceCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_arm_ik_ctrl.cogCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_arm_ik_ctrl.lowSpineCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_arm_ik_ctrl.hiSpineCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_arm_ik_ctrl.masterSpineCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_arm_ik_ctrl.headCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_arm_ik_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_arm_ik_ctrl.stretchValue", lock=True, keyable=False, channelBox=False )
    
    cmds.setAttr ("RIG_spineHigh_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineHigh_ctrl.rootCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineHigh_ctrl.worldSpaceCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineHigh_ctrl.cogCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineHigh_ctrl.midSpineCtrl_1", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineMid_1_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineMid_1_ctrl.rootCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineMid_1_ctrl.worldSpaceCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineMid_1_ctrl.cogCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineMid_1_ctrl.lowSpineCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineLow_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineLow_ctrl.rootCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineLow_ctrl.worldSpaceCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineLow_ctrl.cogCtrl", lock=True, keyable=False, channelBox=False )

    cmds.setAttr ("RIG_lf_eye_ctrl.eyeMoverCtrlVis", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_rt_eye_ctrl.eyeMoverCtrlVis", lock=True, keyable=False, channelBox=False )

    cmds.setAttr ("RIG_hip_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_hip_ctrl.rootCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_hip_ctrl.worldSpaceCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_hip_ctrl.cogCtrl", lock=True, keyable=False, channelBox=False )

    cmds.setAttr ("RIG_cog_ctrl.autoSquashStretch", 0)
    cmds.setAttr ("RIG_cog_ctrl.spineShaper", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_cog_ctrl.stretchValue", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_cog_ctrl.autoSquashStretch", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_cog_ctrl.squashStretch", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_cog_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_cog_ctrl.rootCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_cog_ctrl.worldSpaceCtrl", lock=True, keyable=False, channelBox=False )

    cmds.setAttr ("RIG_lf_heel_ik_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_lf_heel_ik_ctrl.rootCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_lf_heel_ik_ctrl.worldSpaceCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_lf_heel_ik_ctrl.cogCtrl", lock=True, keyable=False, channelBox=False )

    cmds.setAttr ("RIG_rt_heel_ik_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_rt_heel_ik_ctrl.rootCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_rt_heel_ik_ctrl.worldSpaceCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_rt_heel_ik_ctrl.cogCtrl", lock=True, keyable=False, channelBox=False )

    cmds.select ('RIG_Skeleton_grp', 'RIG_rig_grp', 'GRP_Meshes_grp')
    cmds.group(n='CHAR')
    cmds.select ( cl=True )

    cmds.setAttr("RIG_lf_upArmRubberHose_ctrlShape.overrideEnabled", 1);
    cmds.setAttr("RIG_lf_upArmRubberHose_ctrlShape.overrideColor", 18);
    cmds.setAttr("RIG_lf_elbowRubberHose_ctrlShape.overrideEnabled", 1);
    cmds.setAttr("RIG_lf_elbowRubberHose_ctrlShape.overrideColor", 18);
    cmds.setAttr("RIG_lf_forearmRubberHose_ctrlShape.overrideEnabled", 1);
    cmds.setAttr("RIG_lf_forearmRubberHose_ctrlShape.overrideColor", 18);

    cmds.setAttr("RIG_rt_upArmRubberHose_ctrlShape.overrideEnabled", 1);
    cmds.setAttr("RIG_rt_upArmRubberHose_ctrlShape.overrideColor", 18);
    cmds.setAttr("RIG_rt_elbowRubberHose_ctrlShape.overrideEnabled", 1);
    cmds.setAttr("RIG_rt_elbowRubberHose_ctrlShape.overrideColor", 18);
    cmds.setAttr("RIG_rt_forearmRubberHose_ctrlShape.overrideEnabled", 1);
    cmds.setAttr("RIG_rt_forearmRubberHose_ctrlShape.overrideColor", 18);

    cmds.setAttr("RIG_lf_upLegRubberHose_ctrlShape.overrideEnabled", 1);
    cmds.setAttr("RIG_lf_upLegRubberHose_ctrlShape.overrideColor", 18);
    cmds.setAttr("RIG_lf_kneeRubberHose_ctrlShape.overrideEnabled", 1);
    cmds.setAttr("RIG_lf_kneeRubberHose_ctrlShape.overrideColor", 18);
    cmds.setAttr("RIG_lf_lowLegRubberHose_ctrlShape.overrideEnabled", 1);
    cmds.setAttr("RIG_lf_lowLegRubberHose_ctrlShape.overrideColor", 18);

    cmds.setAttr("RIG_rt_upLegRubberHose_ctrlShape.overrideEnabled", 1);
    cmds.setAttr("RIG_rt_upLegRubberHose_ctrlShape.overrideColor", 18);
    cmds.setAttr("RIG_rt_kneeRubberHose_ctrlShape.overrideEnabled", 1);
    cmds.setAttr("RIG_rt_kneeRubberHose_ctrlShape.overrideColor", 18);
    cmds.setAttr("RIG_rt_lowLegRubberHose_ctrlShape.overrideEnabled", 1);
    cmds.setAttr("RIG_rt_lowLegRubberHose_ctrlShape.overrideColor", 18);

    cmds.select("RIG_lf_hand_jnt")
    lfHandJnt = cmds.listRelatives( "RIG_lf_hand_jnt", ad=True, typ='joint' )
    print (lfHandJnt)
    for i in lfHandJnt:
        cmds.setAttr( i + ".segmentScaleCompensate", 0 )
        print (i)

    cmds.select("RIG_rt_hand_jnt")
    rtHandJnt = cmds.listRelatives( "RIG_rt_hand_jnt", ad=True, typ='joint' )
    print (rtHandJnt)
    for i in rtHandJnt:
        cmds.setAttr( i + ".segmentScaleCompensate", 0 )
        print (i)
        
    try:
        cmds.scaleConstraint( "RIG_lf_hand_ctrl", "RIG_lf_hand_jnt", mo=True )
        cmds.pointConstraint( "RIG_lf_hand_ctrl", "RIG_lf_hand_jnt", mo=True )
         
    except:
        pass

    try:
        cmds.scaleConstraint( "RIG_rt_hand_ctrl", "RIG_rt_hand_jnt", mo=True )
        cmds.pointConstraint( "RIG_rt_hand_ctrl", "RIG_rt_hand_jnt", mo=True )   
    except:
        pass

    try:
        cmds.delete('abRTDeleteMeWhenDone')
    except:
        print("--- skipped " + "abRTDeleteMeWhenDone" + "\n");

    try:
        cmds.rename('RIG_rig_grp', 'RIG_controls_grp')
    except:
        print("--- skipped " + "RIG_rig_grp" + "\n");
    
    cmds.select(cl=True)
        
    print("--- END SCRIPT ---" + "\n");


    ##################################################################################################################

    # ADD IK CONSTRAIN CTRLS

    ####################################################################################################################

    ctrl = cmds.circle(ch=False, nr=(1,0,0), r=2, n="L_arm_ik_Constrain_Ctrl")[0]
    ctrl_grp = cmds.group(ctrl, n="L_arm_ik_Constrain_Ctrl_Grp")
    cmds.setAttr(ctrl + ".v", k=False)
    shape = cmds.pickWalk(ctrl, d="down")[0]
    cmds.setAttr(shape + ".overrideEnabled", 1)
    cmds.setAttr(shape + ".overrideColor", 4)

    ctrl = cmds.circle(ch=False, nr=(1,0,0), r=2, n="R_arm_ik_Constrain_Ctrl")[0]
    ctrl_grp = cmds.group(ctrl, n="R_arm_ik_Constrain_Ctrl_Grp")
    cmds.setAttr(ctrl + ".v", k=False)
    shape = cmds.pickWalk(ctrl, d="down")[0]
    cmds.setAttr(shape + ".overrideEnabled", 1)
    cmds.setAttr(shape + ".overrideColor", 4)

    ctrl = cmds.circle(ch=False, nr=(0,1,0), r=2, n="L_heel_ik_Constrain_Ctrl")[0]
    ctrl_grp = cmds.group(ctrl, n="L_heel_ik_Constrain_Ctrl_Grp")
    cmds.setAttr(ctrl + ".v", k=False)
    shape = cmds.pickWalk(ctrl, d="down")[0]
    cmds.setAttr(shape + ".overrideEnabled", 1)
    cmds.setAttr(shape + ".overrideColor", 4)

    ctrl = cmds.circle(ch=False, nr=(0,1,0), r=2, n="R_heel_ik_Constrain_Ctrl")[0]
    ctrl_grp = cmds.group(ctrl, n="R_heel_ik_Constrain_Ctrl_Grp")
    cmds.setAttr(ctrl + ".v", k=False)
    shape = cmds.pickWalk(ctrl, d="down")[0]
    cmds.setAttr(shape + ".overrideEnabled", 1)
    cmds.setAttr(shape + ".overrideColor", 4)

    pos = cmds.xform("RIG_lf_arm_ik_ctrl", ws=True, q=True, t=True)
    cmds.xform("L_arm_ik_Constrain_Ctrl_Grp", ws=True, t=pos)

    pos = cmds.xform("RIG_rt_arm_ik_ctrl", ws=True, q=True, t=True)
    cmds.xform("R_arm_ik_Constrain_Ctrl_Grp", ws=True, t=pos)

    pos = cmds.xform("RIG_lf_heel_ik_ctrl", ws=True, q=True, t=True)
    cmds.xform("L_heel_ik_Constrain_Ctrl_Grp", ws=True, t=pos)

    pos = cmds.xform("RIG_rt_heel_ik_ctrl", ws=True, q=True, t=True)
    cmds.xform("R_heel_ik_Constrain_Ctrl_Grp", ws=True, t=pos)

    cmds.parent("L_arm_ik_Constrain_Ctrl_Grp", "RIG_lf_armIkCtrl_space_grp")
    cmds.parent("R_arm_ik_Constrain_Ctrl_Grp", "RIG_rt_armIkCtrl_space_grp")
    cmds.parent("L_heel_ik_Constrain_Ctrl_Grp", "RIG_lf_legIkCtrl_space_grp")
    cmds.parent("R_heel_ik_Constrain_Ctrl_Grp", "RIG_rt_legIkCtrl_space_grp")

    cmds.parent("RIG_RIG_lf_arm_ik_ctrl_frz_grp", "L_arm_ik_Constrain_Ctrl")
    cmds.parent("RIG_RIG_rt_arm_ik_ctrl_frz_grp", "R_arm_ik_Constrain_Ctrl")
    cmds.parent("RIG_RIG_lf_heel_ik_ctrl_frz_grp", "L_heel_ik_Constrain_Ctrl")
    cmds.parent("RIG_RIG_rt_heel_ik_ctrl_frz_grp", "R_heel_ik_Constrain_Ctrl")

    cmds.select(cl=True)
    
    ########################################################################################################################
    #fix dei controlli uparm fk e fix di visibility dei controlli ik di braccia e gambe
    ########################################################################################################################
    
    cmds.delete("RIG_RIG_lf_upArm_fk_ctrl_frz_grp1_parentConstraint1", "RIG_RIG_rt_upArm_fk_ctrl_frz_grp1_parentConstraint1")
    cmds.deleteAttr("RIG_rt_upArm_fk_ctrl", "RIG_lf_upArm_fk_ctrl", at = "follow")

    sel = ["RIG_lf_clavicleTrans_ctrl", "RIG_cog_ctrl", "Transform_Null", "RIG_lf_upArm_fk_ctrl"]
    last = sel[-1]
    lenght = len(sel)
    attribute_name = "follow"

    #creo l attributo enum all ultimo controllo selezionato, con i nomi switch dei controlli precedenti
    enum_attributes = ""
    i = 0
    while i <= lenght-2:
        enum_attributes += sel[i]+":"
        i += 1
    cmds.addAttr(last, ln=attribute_name, at="enum", en=enum_attributes, k=True)

    #tutti i controlli tranne l'ultimo, fanno il parent constrain sul gruppo padre dell ultimo controllo selezionato
    last_group = cmds.pickWalk(last, d="up")
    i = 0
    while i <= lenght-2:
        par_constr = cmds.parentConstraint(sel[i], last_group, mo=True)[0]

        #creo e setto un condition per ogni attributo
        cnd = cmds.createNode("condition", n="CND_"+attribute_name+"_"+last+"_Weigth_"+sel[i])
        cmds.setAttr(cnd+".secondTerm", i)
        cmds.setAttr(cnd+".colorIfTrueR", 1)
        cmds.setAttr(cnd+".colorIfFalseR", 0)

        #connetto gli attributi ai condition e i condition al constrain
        cmds.connectAttr(last+"."+attribute_name, cnd+".firstTerm")
        cmds.connectAttr(cnd+".outColorR", par_constr+"."+sel[i]+"W"+str(i))

        i += 1
    
    cmds.select(cl=True)

    sel = ["RIG_rt_clavicleTrans_ctrl", "RIG_cog_ctrl", "Transform_Null", "RIG_rt_upArm_fk_ctrl"]
    last = sel[-1]
    lenght = len(sel)
    attribute_name = "follow"

    #creo l attributo enum all ultimo controllo selezionato, con i nomi switch dei controlli precedenti
    enum_attributes = ""
    i = 0
    while i <= lenght-2:
        enum_attributes += sel[i]+":"
        i += 1
    cmds.addAttr(last, ln=attribute_name, at="enum", en=enum_attributes, k=True)

    #tutti i controlli tranne l'ultimo, fanno il parent constrain sul gruppo padre dell ultimo controllo selezionato
    last_group = cmds.pickWalk(last, d="up")
    i = 0
    while i <= lenght-2:
        par_constr = cmds.parentConstraint(sel[i], last_group, mo=True)[0]

        #creo e setto un condition per ogni attributo
        cnd = cmds.createNode("condition", n="CND_"+attribute_name+"_"+last+"_Weigth_"+sel[i])
        cmds.setAttr(cnd+".secondTerm", i)
        cmds.setAttr(cnd+".colorIfTrueR", 1)
        cmds.setAttr(cnd+".colorIfFalseR", 0)

        #connetto gli attributi ai condition e i condition al constrain
        cmds.connectAttr(last+"."+attribute_name, cnd+".firstTerm")
        cmds.connectAttr(cnd+".outColorR", par_constr+"."+sel[i]+"W"+str(i))

        i += 1
    
    cmds.select(cl=True)

    cmds.addAttr("RIG_lf_upArm_fk_ctrl.follow", e=True, en="Clavicle:COG:Transform")
    cmds.addAttr("RIG_rt_upArm_fk_ctrl.follow", e=True, en="Clavicle:COG:Transform")
    cmds.disconnectAttr("RIG_RIG_lf_upArm_fk_ctrl_frz_grp1_parentConstraint1.constraintTranslateX", "RIG_RIG_lf_upArm_fk_ctrl_frz_grp1.tx")
    cmds.disconnectAttr("RIG_RIG_lf_upArm_fk_ctrl_frz_grp1_parentConstraint1.constraintTranslateY", "RIG_RIG_lf_upArm_fk_ctrl_frz_grp1.ty")
    cmds.disconnectAttr("RIG_RIG_lf_upArm_fk_ctrl_frz_grp1_parentConstraint1.constraintTranslateZ", "RIG_RIG_lf_upArm_fk_ctrl_frz_grp1.tz")
    cmds.disconnectAttr("RIG_RIG_rt_upArm_fk_ctrl_frz_grp1_parentConstraint1.constraintTranslateX", "RIG_RIG_rt_upArm_fk_ctrl_frz_grp1.tx")
    cmds.disconnectAttr("RIG_RIG_rt_upArm_fk_ctrl_frz_grp1_parentConstraint1.constraintTranslateY", "RIG_RIG_rt_upArm_fk_ctrl_frz_grp1.ty")
    cmds.disconnectAttr("RIG_RIG_rt_upArm_fk_ctrl_frz_grp1_parentConstraint1.constraintTranslateZ", "RIG_RIG_rt_upArm_fk_ctrl_frz_grp1.tz")
    
    ############################################################################################################################
    
    cmds.rename("RIG_spineMid_1_ctrl", "RIG_spineMid_ctrl")
    
    cmds.connectAttr("Transform_Ctrl.L_Arm_01_IKFK", "L_arm_ik_Constrain_Ctrl_Grp.v")
    cmds.connectAttr("Transform_Ctrl.R_Arm_01_IKFK", "R_arm_ik_Constrain_Ctrl_Grp.v")
    cmds.connectAttr("Transform_Ctrl.L_Leg_01_IKFK", "L_heel_ik_Constrain_Ctrl_Grp.v")
    cmds.connectAttr("Transform_Ctrl.R_Leg_01_IKFK", "R_heel_ik_Constrain_Ctrl_Grp.v")
    cmds.connectAttr("Transform_Ctrl.L_Arm_01_IKFK", "RIG_RIG_lf_shoulder_ctrl_frz_grp.v")
    cmds.connectAttr("Transform_Ctrl.R_Arm_01_IKFK", "RIG_RIG_rt_shoulder_ctrl_frz_grp.v")
    cmds.connectAttr("Transform_Ctrl.L_Leg_01_IKFK", "RIG_RIG_lf_hip_ctrl_frz_grp.v")
    cmds.connectAttr("Transform_Ctrl.R_Leg_01_IKFK", "RIG_RIG_rt_hip_ctrl_frz_grp.v")
    
    cmds.disconnectAttr("RIG_lf_arm_ik_ctrl.pvControl", "RIG_lf_elbow_line_loc.v")
    cmds.setAttr("RIG_lf_elbow_line_loc.v", 0)
    cmds.disconnectAttr("RIG_rt_arm_ik_ctrl.pvControl", "RIG_rt_elbow_line_loc.v")
    cmds.setAttr("RIG_rt_elbow_line_loc.v", 0)
    cmds.disconnectAttr("RIG_lf_heel_ik_ctrl.pvControl", "RIG_lf_knee_line_loc.v")
    cmds.setAttr("RIG_lf_knee_line_loc.v", 0)
    cmds.disconnectAttr("RIG_rt_heel_ik_ctrl.pvControl", "RIG_rt_knee_line_loc.v")
    cmds.setAttr("RIG_rt_knee_line_loc.v", 0)
    
    #######################################
    #####FIX SEGMENT SCALE COMPENSATE######
    #######################################
    cmds.setAttr("RIG_lf_hand_jnt.segmentScaleCompensate", 0)
    cmds.setAttr("RIG_rt_hand_jnt.segmentScaleCompensate", 0)
    cmds.setAttr("RIG_lf_eye_jnt.segmentScaleCompensate", 0)
    cmds.setAttr("RIG_rt_eye_jnt.segmentScaleCompensate", 0)
    cmds.setAttr("RIG_lf_eyeMover_jnt.segmentScaleCompensate", 0)
    cmds.setAttr("RIG_rt_eyeMover_jnt.segmentScaleCompensate", 0)
    
    #######################################
    #####ADD SPINE FOLLOW ATTR######
    #######################################
    
    cmds.addAttr("RIG_spineMid_ctrl", ln="follow_translate", at="float", k=True, dv=1, min=0, max=1)
    cmds.addAttr("RIG_spineMid_ctrl", ln="follow_rotate", at="float", k=True, dv=1, min=0, max=1)
    cmds.addAttr("RIG_spineHigh_ctrl", ln="follow_translate", at="float", k=True, dv=1, min=0, max=1)
    cmds.addAttr("RIG_spineHigh_ctrl", ln="follow_rotate", at="float", k=True, dv=1, min=0, max=1)
    rev_mid = cmds.createNode("reverse", n="REV_Follow_spineMid")
    rev_high = cmds.createNode("reverse", n="REV_Follow_spineHigh")
    constr_trans_mid = cmds.parentConstraint("RIG_cog_ctrl","RIG_spineLow_ctrl", "RIG_RIG_spineMid_1_ctrl_frz_grp", mo=True, sr=("x","y","z"))[0]
    constr_rot_mid = cmds.parentConstraint("RIG_cog_ctrl","RIG_spineLow_ctrl", "RIG_RIG_spineMid_1_ctrl_frz_grp", mo=True, st=("x","y","z"))[0]
    constr_trans_high = cmds.parentConstraint("RIG_cog_ctrl","RIG_spineMid_ctrl", "RIG_RIG_spineHigh_ctrl_frz_grp1", mo=True, sr=("x","y","z"))[0]
    constr_rot_high = cmds.parentConstraint("RIG_cog_ctrl","RIG_spineMid_ctrl", "RIG_RIG_spineHigh_ctrl_frz_grp1", mo=True, st=("x","y","z"))[0]

    cmds.connectAttr("RIG_spineMid_ctrl.follow_translate", "REV_Follow_spineMid.inputX")
    cmds.connectAttr("RIG_spineMid_ctrl.follow_rotate", "REV_Follow_spineMid.inputY")
    cmds.connectAttr("REV_Follow_spineMid.outputX", constr_trans_mid + ".RIG_cog_ctrlW0")
    cmds.connectAttr("RIG_spineMid_ctrl.follow_translate", constr_trans_mid + ".RIG_spineLow_ctrlW1")
    cmds.connectAttr("REV_Follow_spineMid.outputY", constr_rot_mid + ".RIG_cog_ctrlW0")
    cmds.connectAttr("RIG_spineMid_ctrl.follow_rotate", constr_rot_mid + ".RIG_spineLow_ctrlW1")

    cmds.connectAttr("RIG_spineHigh_ctrl.follow_translate", "REV_Follow_spineHigh.inputX")
    cmds.connectAttr("RIG_spineHigh_ctrl.follow_rotate", "REV_Follow_spineHigh.inputY")
    cmds.connectAttr("REV_Follow_spineHigh.outputX", constr_trans_high + ".RIG_cog_ctrlW0")
    cmds.connectAttr("RIG_spineHigh_ctrl.follow_translate", constr_trans_high + ".RIG_spineMid_ctrlW1")
    cmds.connectAttr("REV_Follow_spineHigh.outputY", constr_rot_high + ".RIG_cog_ctrlW0")
    cmds.connectAttr("RIG_spineHigh_ctrl.follow_rotate", constr_rot_high + ".RIG_spineMid_ctrlW1")

    cmds.setAttr(constr_trans_mid + ".interpType", 0)
    cmds.setAttr(constr_rot_mid + ".interpType", 0)
    cmds.setAttr(constr_trans_high + ".interpType", 0)
    cmds.setAttr(constr_rot_high + ".interpType", 0)
    cmds.select(cl=True)
    
#//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#END
#//////////////////////////////////////////////////////////////////////////////////////////////////////////////