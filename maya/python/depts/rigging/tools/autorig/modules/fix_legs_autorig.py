#//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#RBW AUTORIG DEF
#//////////////////////////////////////////////////////////////////////////////////////////////////////////////

#//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#PLACER SCRIPT AB FIXES
#//////////////////////////////////////////////////////////////////////////////////////////////////////////////
import maya.cmds as cmds
import maya.mel as mel

def rbwFixedLegs():

    #fix IK ankleL attrs
    cmds.setAttr ("RIG_lf_foot_ctrl.rubberHoseCtrlVis", 1)
    cmds.setAttr ("RIG_lf_foot_ctrl.autoSquashStretch", 0)
    cmds.select ( "RIG_lf_heel_ik_ctrl" )
    cmds.addAttr( shortName='lsl', longName='legStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='ksl', longName='kneeStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='asl', longName='ankleStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='ksls', longName='kneeSoftness', at="float", h=False, k=True)
    cmds.connectAttr ('RIG_lf_heel_ik_ctrl.legStretch', 'RIG_lf_foot_ctrl.legStretch')
    cmds.connectAttr ('RIG_lf_heel_ik_ctrl.kneeStretch', 'RIG_lf_foot_ctrl.kneeStretch')
    cmds.connectAttr ('RIG_lf_heel_ik_ctrl.ankleStretch', 'RIG_lf_foot_ctrl.ankleStretch')
    cmds.connectAttr ('RIG_lf_heel_ik_ctrl.kneeSoftness', 'RIG_lf_foot_ctrl.kneeSoftness')
    cmds.setAttr ("RIG_lf_heel_ik_ctrl.pvControl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_lf_heel_ik_ctrl.stretchValue", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_lf_heel_ik_ctrl.maxStretch", 10)
    cmds.select ( cl=True )

    #fix IK ankleR attrs
    cmds.setAttr ("RIG_rt_foot_ctrl.rubberHoseCtrlVis", 1)
    cmds.setAttr ("RIG_rt_foot_ctrl.autoSquashStretch", 0)
    cmds.select ( "RIG_rt_heel_ik_ctrl" )
    cmds.addAttr( shortName='lsr', longName='legStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='ksr', longName='kneeStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='asr', longName='ankleStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='ksrs', longName='kneeSoftness', at="float", h=False, k=True)
    cmds.connectAttr ('RIG_rt_heel_ik_ctrl.legStretch', 'RIG_rt_foot_ctrl.legStretch')
    cmds.connectAttr ('RIG_rt_heel_ik_ctrl.kneeStretch', 'RIG_rt_foot_ctrl.kneeStretch')
    cmds.connectAttr ('RIG_rt_heel_ik_ctrl.ankleStretch', 'RIG_rt_foot_ctrl.ankleStretch')
    cmds.connectAttr ('RIG_rt_heel_ik_ctrl.kneeSoftness', 'RIG_rt_foot_ctrl.kneeSoftness')
    cmds.setAttr ("RIG_rt_heel_ik_ctrl.pvControl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_rt_heel_ik_ctrl.stretchValue", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_rt_heel_ik_ctrl.maxStretch", 10)
    cmds.select ( cl=True )
      
    #delete all the constraints attached on groups and joints of legs and arms (joints FK e CTRL)

    cmds.delete ('RIG_RIG_lf_upLeg_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_lf_upLeg_fk_ctrl_frz_grp_pointConstraint1')
    cmds.delete ('RIG_RIG_lf_knee_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_lf_knee_fk_ctrl_frz_grp_pointConstraint1')
    cmds.delete ('RIG_RIG_lf_ankle_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_lf_ankle_fk_ctrl_frz_grp_pointConstraint1')

    cmds.delete ('RIG_RIG_rt_upLeg_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_rt_upLeg_fk_ctrl_frz_grp_pointConstraint1')
    cmds.delete ('RIG_RIG_rt_knee_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_rt_knee_fk_ctrl_frz_grp_pointConstraint1')
    cmds.delete ('RIG_RIG_rt_ankle_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_rt_ankle_fk_ctrl_frz_grp_pointConstraint1')

    cmds.delete ('RIG_lf_ankle_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_lf_knee_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_lf_upLeg_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_lf_upLeg_fk_jnt_pointConstraint1')
    cmds.delete ('RIG_lf_upLeg_jnt_pointConstraint1')

    cmds.delete ('RIG_rt_ankle_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_rt_knee_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_rt_upLeg_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_rt_upLeg_fk_jnt_pointConstraint1')
    cmds.delete ('RIG_rt_upLeg_jnt_pointConstraint1')

    #do a "breakConnection" on the FK joint's "TranslateX"

    cmds.disconnectAttr("RIG_lf_legTrans_blend.outputR", "RIG_lf_knee_jnt.tx")
    cmds.disconnectAttr("RIG_lf_legTrans_blend.outputG", "RIG_lf_ankle_jnt.tx")
    cmds.disconnectAttr("RIG_rt_legTrans_blend.outputR", "RIG_rt_knee_jnt.tx")
    cmds.disconnectAttr("RIG_rt_legTrans_blend.outputG", "RIG_rt_ankle_jnt.tx")

    #attiva t e r su tutti controlli fk
    cmds.setAttr ("RIG_lf_upLeg_fk_ctrl.secondaryFkCtrlVis", 1)
    cmds.setAttr ("RIG_lf_ankle_fk_ctrl.secondaryFkCtrlVis", 1)
    cmds.setAttr ("RIG_rt_upLeg_fk_ctrl.secondaryFkCtrlVis", 1)
    cmds.setAttr ("RIG_rt_ankle_fk_ctrl.secondaryFkCtrlVis", 1)

    cmds.setAttr("RIG_lf_knee_fk_ctrl.rx",k=True,lock=False);
    cmds.setAttr("RIG_rt_knee_fk_ctrl.rx",k=True,lock=False);

    cmds.setAttr("RIG_lf_upLeg_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_lf_upLeg_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_lf_upLeg_fk_ctrl.tz",k=True,lock=False);
    cmds.setAttr("RIG_rt_upLeg_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_rt_upLeg_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_rt_upLeg_fk_ctrl.tz",k=True,lock=False);

    cmds.setAttr("RIG_lf_upLeg_sec_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_lf_upLeg_sec_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_lf_upLeg_sec_fk_ctrl.tz",k=True,lock=False);
    cmds.setAttr("RIG_rt_upLeg_sec_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_rt_upLeg_sec_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_rt_upLeg_sec_fk_ctrl.tz",k=True,lock=False);

    cmds.setAttr("RIG_lf_knee_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_lf_knee_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_lf_knee_fk_ctrl.tz",k=True,lock=False);
    cmds.setAttr("RIG_rt_knee_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_rt_knee_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_rt_knee_fk_ctrl.tz",k=True,lock=False);

    cmds.setAttr("RIG_lf_ankle_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_lf_ankle_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_lf_ankle_fk_ctrl.tz",k=True,lock=False);
    cmds.setAttr("RIG_rt_ankle_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_rt_ankle_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_rt_ankle_fk_ctrl.tz",k=True,lock=False);

    cmds.setAttr("RIG_lf_ankle_sec_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_lf_ankle_sec_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_lf_ankle_sec_fk_ctrl.tz",k=True,lock=False);
    cmds.setAttr("RIG_rt_ankle_sec_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_rt_ankle_sec_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_rt_ankle_sec_fk_ctrl.tz",k=True,lock=False);

    #crea sec elbow e knee

    cmds.duplicate ("RIG_RIG_lf_knee_fk_ctrl_frz_grp1")
    cmds.rename ("RIG_RIG_lf_knee_fk_ctrl_frz_grp2", "RIG_RIG_lf_knee_sec_fk_ctrl_frz_grp")
    cmds.rename ("RIG_RIG_lf_knee_sec_fk_ctrl_frz_grp|RIG_lf_knee_fk_ctrl", "RIG_lf_knee_sec_fk_ctrl")
    cmds.parent ("RIG_RIG_lf_knee_sec_fk_ctrl_frz_grp", "RIG_lf_knee_fk_ctrl")
    cmds.setAttr("RIG_lf_knee_sec_fk_ctrl.v",1);
    cmds.select ( "RIG_lf_knee_fk_ctrl" )
    cmds.addAttr (shortName='secfkkl', longName='secondaryFkCtrlVis', at="bool", h=False, k=True)
    cmds.connectAttr ("RIG_lf_knee_fk_ctrl.secondaryFkCtrlVis", "RIG_RIG_lf_knee_sec_fk_ctrl_frz_grp.v")

    cmds.duplicate ("RIG_RIG_rt_knee_fk_ctrl_frz_grp1")
    cmds.rename ("RIG_RIG_rt_knee_fk_ctrl_frz_grp2", "RIG_RIG_rt_knee_sec_fk_ctrl_frz_grp")
    cmds.rename ("RIG_RIG_rt_knee_sec_fk_ctrl_frz_grp|RIG_rt_knee_fk_ctrl", "RIG_rt_knee_sec_fk_ctrl")
    cmds.parent ("RIG_RIG_rt_knee_sec_fk_ctrl_frz_grp", "RIG_rt_knee_fk_ctrl")
    cmds.setAttr("RIG_rt_knee_sec_fk_ctrl.v",1);
    cmds.select ( "RIG_rt_knee_fk_ctrl" )
    cmds.addAttr (shortName='secfkkr', longName='secondaryFkCtrlVis', at="bool", h=False, k=True)
    cmds.connectAttr ("RIG_rt_knee_fk_ctrl.secondaryFkCtrlVis", "RIG_RIG_rt_knee_sec_fk_ctrl_frz_grp.v")

    cmds.setAttr ("RIG_lf_knee_fk_ctrl.secondaryFkCtrlVis", 0)
    cmds.setAttr ("RIG_rt_knee_fk_ctrl.secondaryFkCtrlVis", 0)

    ################## - Fix Translate Left Leg - ##################
    cmds.parentConstraint("RIG_lf_upLeg_sec_fk_ctrl", "RIG_lf_upLeg_fk_jnt",mo=True)
    cmds.parentConstraint("RIG_lf_knee_sec_fk_ctrl", "RIG_lf_knee_fk_jnt",mo=True)
    cmds.parentConstraint("RIG_lf_ankle_sec_fk_ctrl", "RIG_lf_ankle_fk_jnt",mo=True)

    # - Left Knee
    cmds.parentConstraint("RIG_lf_upLeg_sec_fk_ctrl", "RIG_RIG_lf_knee_fk_ctrl_frz_grp", mo=True)

    # - Left Ankle
    cmds.parentConstraint("RIG_lf_knee_sec_fk_ctrl", "RIG_RIG_lf_ankle_fk_ctrl_frz_grp", mo=True)

    # - Connection to DEF joints chain
    bndLfUpLeg = cmds.createNode("blendColors", n="BND_translate_lf_upLeg")
    bndLfKnee = cmds.createNode("blendColors", n="BND_translate_lf_Knee")
    bndLfAnkle = cmds.createNode("blendColors", n="BND_translate_lf_Ankle")

    cmds.connectAttr("RIG_lf_foot_ctrl.ikFkBlend", bndLfUpLeg+".blender", f=True)
    cmds.connectAttr("RIG_lf_foot_ctrl.ikFkBlend", bndLfKnee+".blender", f=True)
    cmds.connectAttr("RIG_lf_foot_ctrl.ikFkBlend", bndLfAnkle+".blender", f=True)

    cmds.connectAttr("RIG_lf_upLeg_ik_jnt.translate", bndLfUpLeg+".color1", f=True)
    cmds.connectAttr("RIG_lf_upLeg_fk_jnt.translate", bndLfUpLeg+".color2", f=True)
    cmds.connectAttr(bndLfUpLeg+".output", "RIG_lf_upLeg_jnt.translate", f=True)

    cmds.connectAttr("RIG_lf_knee_ik_jnt.translate", bndLfKnee+".color1", f=True)
    cmds.connectAttr("RIG_lf_knee_fk_jnt.translate", bndLfKnee+".color2", f=True)
    cmds.connectAttr(bndLfKnee+".output", "RIG_lf_knee_jnt.translate", f=True)

    cmds.connectAttr("RIG_lf_ankle_ik_jnt.translate", bndLfAnkle+".color1", f=True)
    cmds.connectAttr("RIG_lf_ankle_fk_jnt.translate", bndLfAnkle+".color2", f=True)
    cmds.connectAttr(bndLfAnkle+".output", "RIG_lf_ankle_jnt.translate", f=True)

    ################## - Fix Translate Right Leg - ##################
    cmds.parentConstraint("RIG_rt_upLeg_sec_fk_ctrl", "RIG_rt_upLeg_fk_jnt",mo=True)
    cmds.parentConstraint("RIG_rt_knee_sec_fk_ctrl", "RIG_rt_knee_fk_jnt",mo=True)
    cmds.parentConstraint("RIG_rt_ankle_sec_fk_ctrl", "RIG_rt_ankle_fk_jnt",mo=True)

    # - Right Knee
    cmds.parentConstraint("RIG_rt_upLeg_sec_fk_ctrl", "RIG_RIG_rt_knee_fk_ctrl_frz_grp", mo=True)

    # - Right Ankle
    cmds.parentConstraint("RIG_rt_knee_sec_fk_ctrl", "RIG_RIG_rt_ankle_fk_ctrl_frz_grp", mo=True)

    # - Connection to DEF joints chain
    bndRtUpLeg = cmds.createNode("blendColors", n="BND_translate_rt_upLeg")
    bndRtKnee = cmds.createNode("blendColors", n="BND_translate_rt_Knee")
    bndRtAnkle = cmds.createNode("blendColors", n="BND_translate_rt_Ankle")

    cmds.connectAttr("RIG_rt_foot_ctrl.ikFkBlend", bndRtUpLeg+".blender", f=True)
    cmds.connectAttr("RIG_rt_foot_ctrl.ikFkBlend", bndRtKnee+".blender", f=True)
    cmds.connectAttr("RIG_rt_foot_ctrl.ikFkBlend", bndRtAnkle+".blender", f=True)

    cmds.connectAttr("RIG_rt_upLeg_ik_jnt.translate", bndRtUpLeg+".color1", f=True)
    cmds.connectAttr("RIG_rt_upLeg_fk_jnt.translate", bndRtUpLeg+".color2", f=True)
    cmds.connectAttr(bndRtUpLeg+".output", "RIG_rt_upLeg_jnt.translate", f=True)

    cmds.connectAttr("RIG_rt_knee_ik_jnt.translate", bndRtKnee+".color1", f=True)
    cmds.connectAttr("RIG_rt_knee_fk_jnt.translate", bndRtKnee+".color2", f=True)
    cmds.connectAttr(bndRtKnee+".output", "RIG_rt_knee_jnt.translate", f=True)

    cmds.connectAttr("RIG_rt_ankle_ik_jnt.translate", bndRtAnkle+".color1", f=True)
    cmds.connectAttr("RIG_rt_ankle_fk_jnt.translate", bndRtAnkle+".color2", f=True)
    cmds.connectAttr(bndRtAnkle+".output", "RIG_rt_ankle_jnt.translate", f=True)

    cmds.select ( cl=True )

    #rotateOrder added

    cmds.setAttr ("RIG_lf_hip_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_heel_ik_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_knee_pv_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_upLeg_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_knee_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_ankle_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_ball_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_upLeg_sec_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_knee_sec_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_ankle_sec_fk_ctrl.rotateOrder", k=False, cb=True )

    cmds.setAttr ("RIG_rt_hip_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_heel_ik_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_knee_pv_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_upLeg_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_knee_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_ankle_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_ball_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_upLeg_sec_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_knee_sec_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_ankle_sec_fk_ctrl.rotateOrder", k=False, cb=True )

    cmds.select ( cl=True )

    #autoTwist added
    cmds.select("RIG_lf_ankle_fk_ctrl")
    cmds.addAttr( shortName='atal', longName='AutoTwist', at="float", h=False, k=True, minValue=0, maxValue=1)
    cmds.setAttr ("RIG_lf_ankle_fk_ctrl.AutoTwist", 1 )

    cmds.select("RIG_rt_ankle_fk_ctrl")
    cmds.addAttr( shortName='atar', longName='AutoTwist', at="float", h=False, k=True, minValue=0, maxValue=1)
    cmds.setAttr ("RIG_rt_ankle_fk_ctrl.AutoTwist", 1 )

    cmds.shadingNode('blendColors', asShader=True, n="BLD_autoTwistAnkle_L")
    cmds.shadingNode('blendColors', asShader=True, n="BLD_autoTwistAnkle_R")

    cmds.connectAttr("RIG_lf_ankle_fk_ctrl.AutoTwist", "BLD_autoTwistAnkle_L.blender")
    cmds.connectAttr("RIG_lf_ankle_fk_ctrl.ry", "BLD_autoTwistAnkle_L.color1R")
    cmds.connectAttr("BLD_autoTwistAnkle_L.outputR", "RIG_RIG_lf_knee_sec_fk_ctrl_frz_grp.rx")

    cmds.connectAttr("RIG_rt_ankle_fk_ctrl.AutoTwist", "BLD_autoTwistAnkle_R.blender")
    cmds.connectAttr("RIG_rt_ankle_fk_ctrl.ry", "BLD_autoTwistAnkle_R.color1R")
    cmds.connectAttr("BLD_autoTwistAnkle_R.outputR", "RIG_RIG_rt_knee_sec_fk_ctrl_frz_grp.rx")

    cmds.select ( cl=True )

    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #BALL TWIST FIXED
    #script per fixare il funzionamento del ball twist fatto da ab
    #Usage: lanciare tutto lo script per eseguire il fix
    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////

    #prendo le posizioni del ball
    posBallL = cmds.xform('RIG_lf_ball_jnt', q=True, t=True, ws=True)
    posBallR = cmds.xform('RIG_rt_ball_jnt', q=True, t=True, ws=True)

    #creo i locator con i gruppi
    LocBallRevL = cmds.spaceLocator(n='LOC_lf_ball_rev_rig_jnt')
    GrpLocBallRevL = cmds.group(LocBallRevL, n='GRP_LOC_lf_ball_rev_rig_jnt')

    LocBallRevR = cmds.spaceLocator(n='LOC_rt_ball_rev_rig_jnt')
    GrpLocBallRevR = cmds.group(LocBallRevR, n='GRP_LOC_rt_ball_rev_rig_jnt')

    LocBallIkL = cmds.spaceLocator(n='LOC_lf_ball_ik_jnt')
    GrpLocBallIkL = cmds.group(LocBallIkL, n='GRP_LOC_lf_ball_ik_jnt')

    LocBallIkR = cmds.spaceLocator(n='LOC_rt_ball_ik_jnt')
    GrpLocBallIkR = cmds.group(LocBallIkR, n='GRP_LOC_rt_ball_ik_jnt')

    #posiziono i gruppi nei rispettivi ball
    cmds.setAttr(GrpLocBallRevL + '.tx', posBallL[0])
    cmds.setAttr(GrpLocBallRevL + '.ty', posBallL[1])
    cmds.setAttr(GrpLocBallRevL + '.tz', posBallL[2])

    cmds.setAttr(GrpLocBallRevR + '.tx', posBallR[0])
    cmds.setAttr(GrpLocBallRevR + '.ty', posBallR[1])
    cmds.setAttr(GrpLocBallRevR + '.tz', posBallR[2])

    cmds.setAttr(GrpLocBallIkL + '.tx', posBallL[0])
    cmds.setAttr(GrpLocBallIkL + '.ty', posBallL[1])
    cmds.setAttr(GrpLocBallIkL + '.tz', posBallL[2])

    cmds.setAttr(GrpLocBallIkR + '.tx', posBallR[0])
    cmds.setAttr(GrpLocBallIkR + '.ty', posBallR[1])
    cmds.setAttr(GrpLocBallIkR + '.tz', posBallR[2])

    #ruoto i gruppi dei locator
    cmds.setAttr(GrpLocBallRevL + '.rx', 180)
    cmds.setAttr(GrpLocBallRevL + '.ry', -90)
    cmds.setAttr(GrpLocBallIkL + '.rx', 180)
    cmds.setAttr(GrpLocBallIkL + '.ry', -90)
    cmds.setAttr(GrpLocBallRevR + '.ry', 90)
    cmds.setAttr(GrpLocBallIkR + '.ry', 90)

    #parento i locator sotto i rispettivi joint
    cmds.parent(GrpLocBallRevL, 'RIG_lf_toe_rev_rig_jnt')
    cmds.parent(GrpLocBallRevR, 'RIG_rt_toe_rev_rig_jnt')
    cmds.parent(GrpLocBallIkL, 'RIG_lf_ballToePar_ik_jnt')
    cmds.parent(GrpLocBallIkR, 'RIG_rt_ballToePar_ik_jnt')

    #rompo i collegamenti dei joint
    source_attribute = cmds.listConnections('RIG_lf_ball_rev_rig_jnt.rotateX',s=True,p=True)[0];
    cmds.disconnectAttr (source_attribute, 'RIG_lf_ball_rev_rig_jnt.rotateX')
    cmds.connectAttr(source_attribute, 'LOC_lf_ball_rev_rig_jnt.rx')

    source_attribute = cmds.listConnections('RIG_lf_ball_rev_rig_jnt.rotateY',s=True,p=True)[0];
    cmds.disconnectAttr (source_attribute, 'RIG_lf_ball_rev_rig_jnt.rotateY')
    cmds.connectAttr(source_attribute, 'LOC_lf_ball_rev_rig_jnt.ry')

    source_attribute = cmds.listConnections('RIG_lf_ball_rev_rig_jnt.rotateZ',s=True,p=True)[0];
    cmds.disconnectAttr (source_attribute, 'RIG_lf_ball_rev_rig_jnt.rotateZ')
    cmds.connectAttr(source_attribute, 'LOC_lf_ball_rev_rig_jnt.rz')

    source_attribute = cmds.listConnections('RIG_lf_ball_ik_jnt.rotateY',s=True,p=True)[0];
    cmds.disconnectAttr (source_attribute, 'RIG_lf_ball_ik_jnt.rotateY')
    cmds.connectAttr(source_attribute, 'LOC_lf_ball_ik_jnt.ry')

    source_attribute = cmds.listConnections('RIG_lf_ball_ik_jnt.rotateZ',s=True,p=True)[0];
    cmds.disconnectAttr (source_attribute, 'RIG_lf_ball_ik_jnt.rotateZ')
    cmds.connectAttr(source_attribute, 'LOC_lf_ball_ik_jnt.rz')

    source_attribute = cmds.listConnections('RIG_rt_ball_rev_rig_jnt.rotateX',s=True,p=True)[0];
    cmds.disconnectAttr (source_attribute, 'RIG_rt_ball_rev_rig_jnt.rotateX')
    cmds.connectAttr(source_attribute, 'LOC_rt_ball_rev_rig_jnt.rx')

    source_attribute = cmds.listConnections('RIG_rt_ball_rev_rig_jnt.rotateY',s=True,p=True)[0];
    cmds.disconnectAttr (source_attribute, 'RIG_rt_ball_rev_rig_jnt.rotateY')
    cmds.connectAttr(source_attribute, 'LOC_rt_ball_rev_rig_jnt.ry')

    source_attribute = cmds.listConnections('RIG_rt_ball_rev_rig_jnt.rotateZ',s=True,p=True)[0];
    cmds.disconnectAttr (source_attribute, 'RIG_rt_ball_rev_rig_jnt.rotateZ')
    cmds.connectAttr(source_attribute, 'LOC_rt_ball_rev_rig_jnt.rz')

    source_attribute = cmds.listConnections('RIG_rt_ball_ik_jnt.rotateY',s=True,p=True)[0];
    cmds.disconnectAttr (source_attribute, 'RIG_rt_ball_ik_jnt.rotateY')
    cmds.connectAttr(source_attribute, 'LOC_rt_ball_ik_jnt.ry')

    source_attribute = cmds.listConnections('RIG_rt_ball_ik_jnt.rotateZ',s=True,p=True)[0];
    cmds.disconnectAttr (source_attribute, 'RIG_rt_ball_ik_jnt.rotateZ')
    cmds.connectAttr(source_attribute, 'LOC_rt_ball_ik_jnt.rz')


    #faccio un orient constraint dei locator sui joint
    cmds.orientConstraint('LOC_lf_ball_ik_jnt', 'RIG_lf_ball_ik_jnt', mo=True, w=1)
    cmds.orientConstraint('LOC_lf_ball_rev_rig_jnt', 'RIG_lf_ball_rev_rig_jnt', mo=True, w=1)
    cmds.orientConstraint('LOC_rt_ball_ik_jnt', 'RIG_rt_ball_ik_jnt', mo=True, w=1)
    cmds.orientConstraint('LOC_rt_ball_rev_rig_jnt', 'RIG_rt_ball_rev_rig_jnt', mo=True, w=1)

    cmds.select(cl=True)

    ############################################
    #               Leg Toony Fix              #
    ############################################

    upLegTcntLF = "RIG_lf_upLegRubberHose_ctrl"
    kneeTcntLF = "RIG_lf_kneeRubberHose_ctrl"
    tibiaTcntLF = "RIG_lf_lowLegRubberHose_ctrl"

    upLegTcntRT = "RIG_rt_upLegRubberHose_ctrl"
    kneeTcntRT = "RIG_rt_kneeRubberHose_ctrl"
    tibiaTcntRT = "RIG_rt_lowLegRubberHose_ctrl"

    #Slocca i canali dello scale a tutti rubberHose controls
    cmds.setAttr("RIG_lf_upLegRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_lf_upLegRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_lf_upLegRubberHose_ctrl.sz", k=True, l=False)
    cmds.setAttr("RIG_lf_kneeRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_lf_kneeRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_lf_kneeRubberHose_ctrl.sz", k=True, l=False)
    cmds.setAttr("RIG_lf_lowLegRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_lf_lowLegRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_lf_lowLegRubberHose_ctrl.sz", k=True, l=False)

    cmds.setAttr("RIG_rt_upLegRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_rt_upLegRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_rt_upLegRubberHose_ctrl.sz", k=True, l=False)
    cmds.setAttr("RIG_rt_kneeRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_rt_kneeRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_rt_kneeRubberHose_ctrl.sz", k=True, l=False)
    cmds.setAttr("RIG_rt_lowLegRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_rt_lowLegRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_rt_lowLegRubberHose_ctrl.sz", k=True, l=False)

    ############################################
    #               Leg Toony Left             #
    ############################################

    # mlp and str first armToony ---> RIG_lf_upLegRubberHose_ctrl
    mlpUpLegLF = cmds.createNode("multiplyDivide", n="MLP_RIG_lf_upLegRubberHose_ctrl")
    strUpLegLF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_upLegWeight_jnt")
    strUpLegLFSplit1LF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_upLegWeightSplit_1_jnt")
    strUpLegLFSpilt2LF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_upLegWeightSplit_2_jnt")

    cmds.setAttr(strUpLegLF+".minX", -5)
    cmds.setAttr(strUpLegLF+".minY", -5)
    cmds.setAttr(strUpLegLF+".minZ", -5)
    cmds.setAttr(strUpLegLF+".maxX", 5)
    cmds.setAttr(strUpLegLF+".maxY", 5)
    cmds.setAttr(strUpLegLF+".maxZ", 5)
    cmds.setAttr(strUpLegLF+".oldMinX", -12.5)
    cmds.setAttr(strUpLegLF+".oldMinY", -12.5)
    cmds.setAttr(strUpLegLF+".oldMinZ", -12.5)
    cmds.setAttr(strUpLegLF+".oldMaxX", 10)
    cmds.setAttr(strUpLegLF+".oldMaxY", 10)
    cmds.setAttr(strUpLegLF+".oldMaxZ", 10)

    cmds.setAttr(strUpLegLFSplit1LF+".minX", -7)
    cmds.setAttr(strUpLegLFSplit1LF+".minY", -7)
    cmds.setAttr(strUpLegLFSplit1LF+".minZ", -7)
    cmds.setAttr(strUpLegLFSplit1LF+".maxX", 7)
    cmds.setAttr(strUpLegLFSplit1LF+".maxY", 7)
    cmds.setAttr(strUpLegLFSplit1LF+".maxZ", 7)
    cmds.setAttr(strUpLegLFSplit1LF+".oldMinX", -11)
    cmds.setAttr(strUpLegLFSplit1LF+".oldMinY", -11)
    cmds.setAttr(strUpLegLFSplit1LF+".oldMinZ", -11)
    cmds.setAttr(strUpLegLFSplit1LF+".oldMaxX", 10)
    cmds.setAttr(strUpLegLFSplit1LF+".oldMaxY", 10)
    cmds.setAttr(strUpLegLFSplit1LF+".oldMaxZ", 10)

    cmds.setAttr(strUpLegLFSpilt2LF+".minX", -9)
    cmds.setAttr(strUpLegLFSpilt2LF+".minY", -9)
    cmds.setAttr(strUpLegLFSpilt2LF+".minZ", -9)
    cmds.setAttr(strUpLegLFSpilt2LF+".maxX", 9)
    cmds.setAttr(strUpLegLFSpilt2LF+".maxY", 9)
    cmds.setAttr(strUpLegLFSpilt2LF+".maxZ", 9)
    cmds.setAttr(strUpLegLFSpilt2LF+".oldMinX", -10.25)
    cmds.setAttr(strUpLegLFSpilt2LF+".oldMinY", -10.25)
    cmds.setAttr(strUpLegLFSpilt2LF+".oldMinZ", -10.25)
    cmds.setAttr(strUpLegLFSpilt2LF+".oldMaxX", 10)
    cmds.setAttr(strUpLegLFSpilt2LF+".oldMaxY", 10)
    cmds.setAttr(strUpLegLFSpilt2LF+".oldMaxZ", 10)

    # mlp second armToony ---> RIG_lf_kneeRubberHose_ctrl
    mlpKneeLF = cmds.createNode("multiplyDivide", n="MLP_RIG_lf_kneeRubberHose_ctrl")

    # mlp and str first armToony ---> RIG_lf_lowLegRubberHose_ctrl
    mlpTibiaLF = cmds.createNode("multiplyDivide", n="MLP_RIG_lf_lowLegRubberHose_ctrl")
    strTibiaSplit3LF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_lowLegWeightSplit_3_jnt")
    strTibiaSplit4LF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_lowLegWeightSplit_4_jnt")
    strAnkleLF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_ankleWeight_jnt")

    cmds.setAttr(strAnkleLF+".minX", -5)
    cmds.setAttr(strAnkleLF+".minY", -5)
    cmds.setAttr(strAnkleLF+".minZ", -5)
    cmds.setAttr(strAnkleLF+".maxX", 5)
    cmds.setAttr(strAnkleLF+".maxY", 5)
    cmds.setAttr(strAnkleLF+".maxZ", 5)
    cmds.setAttr(strAnkleLF+".oldMinX", -12.5)
    cmds.setAttr(strAnkleLF+".oldMinY", -12.5)
    cmds.setAttr(strAnkleLF+".oldMinZ", -12.5)
    cmds.setAttr(strAnkleLF+".oldMaxX", 10)
    cmds.setAttr(strAnkleLF+".oldMaxY", 10)
    cmds.setAttr(strAnkleLF+".oldMaxZ", 10)

    cmds.setAttr(strTibiaSplit4LF+".minX", -7)
    cmds.setAttr(strTibiaSplit4LF+".minY", -7)
    cmds.setAttr(strTibiaSplit4LF+".minZ", -7)
    cmds.setAttr(strTibiaSplit4LF+".maxX", 7)
    cmds.setAttr(strTibiaSplit4LF+".maxY", 7)
    cmds.setAttr(strTibiaSplit4LF+".maxZ", 7)
    cmds.setAttr(strTibiaSplit4LF+".oldMinX", -11)
    cmds.setAttr(strTibiaSplit4LF+".oldMinY", -11)
    cmds.setAttr(strTibiaSplit4LF+".oldMinZ", -11)
    cmds.setAttr(strTibiaSplit4LF+".oldMaxX", 10)
    cmds.setAttr(strTibiaSplit4LF+".oldMaxY", 10)
    cmds.setAttr(strTibiaSplit4LF+".oldMaxZ", 10)

    cmds.setAttr(strTibiaSplit3LF+".minX", -9)
    cmds.setAttr(strTibiaSplit3LF+".minY", -9)
    cmds.setAttr(strTibiaSplit3LF+".minZ", -9)
    cmds.setAttr(strTibiaSplit3LF+".maxX", 9)
    cmds.setAttr(strTibiaSplit3LF+".maxY", 9)
    cmds.setAttr(strTibiaSplit3LF+".maxZ", 9)
    cmds.setAttr(strTibiaSplit3LF+".oldMinX", -10.25)
    cmds.setAttr(strTibiaSplit3LF+".oldMinY", -10.25)
    cmds.setAttr(strTibiaSplit3LF+".oldMinZ", -10.25)
    cmds.setAttr(strTibiaSplit3LF+".oldMaxX", 10)
    cmds.setAttr(strTibiaSplit3LF+".oldMaxY", 10)
    cmds.setAttr(strTibiaSplit3LF+".oldMaxZ", 10)

    # connection controls to multiplies
    cmds.connectAttr(upLegTcntLF+".scaleX", mlpUpLegLF+".input1X", force=True)
    cmds.connectAttr(upLegTcntLF+".scaleY", mlpUpLegLF+".input1Y", force=True)
    cmds.connectAttr(upLegTcntLF+".scaleZ", mlpUpLegLF+".input1Z", force=True)

    cmds.connectAttr(kneeTcntLF+".scaleX", mlpKneeLF+".input1X", force=True)
    cmds.connectAttr(kneeTcntLF+".scaleY", mlpKneeLF+".input1Y", force=True)
    cmds.connectAttr(kneeTcntLF+".scaleZ", mlpKneeLF+".input1Z", force=True)

    cmds.connectAttr(tibiaTcntLF+".scaleX", mlpTibiaLF+".input1X", force=True)
    cmds.connectAttr(tibiaTcntLF+".scaleY", mlpTibiaLF+".input1Y", force=True)
    cmds.connectAttr(tibiaTcntLF+".scaleZ", mlpTibiaLF+".input1Z", force=True)

    # connection mlps to strs
    cmds.connectAttr(mlpUpLegLF+".outputX", strUpLegLF+".valueX", force=True)
    cmds.connectAttr(mlpUpLegLF+".outputY", strUpLegLF+".valueY", force=True)
    cmds.connectAttr(mlpUpLegLF+".outputZ", strUpLegLF+".valueZ", force=True)

    cmds.connectAttr(mlpUpLegLF+".outputX", strUpLegLFSplit1LF+".valueX", force=True)
    cmds.connectAttr(mlpUpLegLF+".outputY", strUpLegLFSplit1LF+".valueY", force=True)
    cmds.connectAttr(mlpUpLegLF+".outputZ", strUpLegLFSplit1LF+".valueZ", force=True)

    cmds.connectAttr(mlpUpLegLF+".outputX", strUpLegLFSpilt2LF+".valueX", force=True)
    cmds.connectAttr(mlpUpLegLF+".outputY", strUpLegLFSpilt2LF+".valueY", force=True)
    cmds.connectAttr(mlpUpLegLF+".outputZ", strUpLegLFSpilt2LF+".valueZ", force=True)

    cmds.connectAttr(mlpTibiaLF+".outputX", strTibiaSplit3LF+".valueX", force=True)
    cmds.connectAttr(mlpTibiaLF+".outputY", strTibiaSplit3LF+".valueY", force=True)
    cmds.connectAttr(mlpTibiaLF+".outputZ", strTibiaSplit3LF+".valueZ", force=True)

    cmds.connectAttr(mlpTibiaLF+".outputX", strTibiaSplit4LF+".valueX", force=True)
    cmds.connectAttr(mlpTibiaLF+".outputY", strTibiaSplit4LF+".valueY", force=True)
    cmds.connectAttr(mlpTibiaLF+".outputZ", strTibiaSplit4LF+".valueZ", force=True)

    cmds.connectAttr(mlpTibiaLF+".outputX", strAnkleLF+".valueX", force=True)
    cmds.connectAttr(mlpTibiaLF+".outputY", strAnkleLF+".valueY", force=True)
    cmds.connectAttr(mlpTibiaLF+".outputZ", strAnkleLF+".valueZ", force=True)

    # connection scale directly where doesn't need the scale averaged
    cmds.connectAttr(mlpUpLegLF+".outputX", "RIG_lf_upLegWeightSplit_3_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpUpLegLF+".outputY", "RIG_lf_upLegWeightSplit_3_jnt.scaleX", force=True)
    cmds.connectAttr(mlpUpLegLF+".outputZ", "RIG_lf_upLegWeightSplit_3_jnt.scaleY", force=True)

    cmds.connectAttr(mlpKneeLF+".outputX", "RIG_lf_upLegWeightSplit_4_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpKneeLF+".outputY", "RIG_lf_upLegWeightSplit_4_jnt.scaleX", force=True)
    cmds.connectAttr(mlpKneeLF+".outputZ", "RIG_lf_upLegWeightSplit_4_jnt.scaleY", force=True)

    cmds.connectAttr(mlpKneeLF+".outputX", "RIG_lf_kneeWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpKneeLF+".outputY", "RIG_lf_kneeWeight_jnt.scaleX", force=True)
    cmds.connectAttr(mlpKneeLF+".outputZ", "RIG_lf_kneeWeight_jnt.scaleY", force=True)

    cmds.connectAttr(mlpKneeLF+".outputX", "RIG_lf_kneeWeightSplit_1_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpKneeLF+".outputY", "RIG_lf_kneeWeightSplit_1_jnt.scaleX", force=True)
    cmds.connectAttr(mlpKneeLF+".outputZ", "RIG_lf_kneeWeightSplit_1_jnt.scaleY", force=True)

    cmds.connectAttr(mlpTibiaLF+".outputX", "RIG_lf_kneeWeightSplit_2_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpTibiaLF+".outputY", "RIG_lf_kneeWeightSplit_2_jnt.scaleX", force=True)
    cmds.connectAttr(mlpTibiaLF+".outputZ", "RIG_lf_kneeWeightSplit_2_jnt.scaleY", force=True)

    # connection
    cmds.connectAttr(strUpLegLF+".outValueX", "RIG_lf_upLegWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpLegLF+".outValueY", "RIG_lf_upLegWeight_jnt.scaleX", force=True)
    cmds.connectAttr(strUpLegLF+".outValueZ", "RIG_lf_upLegWeight_jnt.scaleY", force=True)

    cmds.connectAttr(strUpLegLFSplit1LF+".outValueX", "RIG_lf_upLegWeightSplit_1_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpLegLFSplit1LF+".outValueY", "RIG_lf_upLegWeightSplit_1_jnt.scaleX", force=True)
    cmds.connectAttr(strUpLegLFSplit1LF+".outValueZ", "RIG_lf_upLegWeightSplit_1_jnt.scaleY", force=True)

    cmds.connectAttr(strUpLegLFSpilt2LF+".outValueX", "RIG_lf_upLegWeightSplit_2_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpLegLFSpilt2LF+".outValueY", "RIG_lf_upLegWeightSplit_2_jnt.scaleX", force=True)
    cmds.connectAttr(strUpLegLFSpilt2LF+".outValueZ", "RIG_lf_upLegWeightSplit_2_jnt.scaleY", force=True)

    cmds.connectAttr(strTibiaSplit3LF+".outValueX", "RIG_lf_kneeWeightSplit_3_jnt.scaleZ", force=True)
    cmds.connectAttr(strTibiaSplit3LF+".outValueY", "RIG_lf_kneeWeightSplit_3_jnt.scaleX", force=True)
    cmds.connectAttr(strTibiaSplit3LF+".outValueZ", "RIG_lf_kneeWeightSplit_3_jnt.scaleY", force=True)

    cmds.connectAttr(strTibiaSplit4LF+".outValueX", "RIG_lf_kneeWeightSplit_4_jnt.scaleZ", force=True)
    cmds.connectAttr(strTibiaSplit4LF+".outValueY", "RIG_lf_kneeWeightSplit_4_jnt.scaleX", force=True)
    cmds.connectAttr(strTibiaSplit4LF+".outValueZ", "RIG_lf_kneeWeightSplit_4_jnt.scaleY", force=True)

    cmds.connectAttr(strAnkleLF+".outValueX", "RIG_lf_ankleWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(strAnkleLF+".outValueY", "RIG_lf_ankleWeight_jnt.scaleX", force=True)
    cmds.connectAttr(strAnkleLF+".outValueZ", "RIG_lf_ankleWeight_jnt.scaleY", force=True)


    ############################################
    #               Leg Toony Right            #
    ############################################

    # mlp and str first armToony ---> RIG_rt_upLegRubberHose_ctrl
    mlpUpLegRT = cmds.createNode("multiplyDivide", n="MLP_RIG_rt_upLegRubberHose_ctrl")
    strUpLegRT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_upArmWeight_jnt")
    strUpLegRTSplit1RT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_upArmWeightSplit_1_jnt")
    strUpLegRTSpilt2RT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_upArmWeightSplit_2_jnt")

    cmds.setAttr(strUpLegRT+".minX", -5)
    cmds.setAttr(strUpLegRT+".minY", -5)
    cmds.setAttr(strUpLegRT+".minZ", -5)
    cmds.setAttr(strUpLegRT+".maxX", 5)
    cmds.setAttr(strUpLegRT+".maxY", 5)
    cmds.setAttr(strUpLegRT+".maxZ", 5)
    cmds.setAttr(strUpLegRT+".oldMinX", -12.5)
    cmds.setAttr(strUpLegRT+".oldMinY", -12.5)
    cmds.setAttr(strUpLegRT+".oldMinZ", -12.5)
    cmds.setAttr(strUpLegRT+".oldMaxX", 10)
    cmds.setAttr(strUpLegRT+".oldMaxY", 10)
    cmds.setAttr(strUpLegRT+".oldMaxZ", 10)

    cmds.setAttr(strUpLegRTSplit1RT+".minX", -7)
    cmds.setAttr(strUpLegRTSplit1RT+".minY", -7)
    cmds.setAttr(strUpLegRTSplit1RT+".minZ", -7)
    cmds.setAttr(strUpLegRTSplit1RT+".maxX", 7)
    cmds.setAttr(strUpLegRTSplit1RT+".maxY", 7)
    cmds.setAttr(strUpLegRTSplit1RT+".maxZ", 7)
    cmds.setAttr(strUpLegRTSplit1RT+".oldMinX", -11)
    cmds.setAttr(strUpLegRTSplit1RT+".oldMinY", -11)
    cmds.setAttr(strUpLegRTSplit1RT+".oldMinZ", -11)
    cmds.setAttr(strUpLegRTSplit1RT+".oldMaxX", 10)
    cmds.setAttr(strUpLegRTSplit1RT+".oldMaxY", 10)
    cmds.setAttr(strUpLegRTSplit1RT+".oldMaxZ", 10)

    cmds.setAttr(strUpLegRTSpilt2RT+".minX", -9)
    cmds.setAttr(strUpLegRTSpilt2RT+".minY", -9)
    cmds.setAttr(strUpLegRTSpilt2RT+".minZ", -9)
    cmds.setAttr(strUpLegRTSpilt2RT+".maxX", 9)
    cmds.setAttr(strUpLegRTSpilt2RT+".maxY", 9)
    cmds.setAttr(strUpLegRTSpilt2RT+".maxZ", 9)
    cmds.setAttr(strUpLegRTSpilt2RT+".oldMinX", -10.25)
    cmds.setAttr(strUpLegRTSpilt2RT+".oldMinY", -10.25)
    cmds.setAttr(strUpLegRTSpilt2RT+".oldMinZ", -10.25)
    cmds.setAttr(strUpLegRTSpilt2RT+".oldMaxX", 10)
    cmds.setAttr(strUpLegRTSpilt2RT+".oldMaxY", 10)
    cmds.setAttr(strUpLegRTSpilt2RT+".oldMaxZ", 10)

    # mlp second armToony ---> RIG_rt_kneeRubberHose_ctrl
    mlpKneeRT = cmds.createNode("multiplyDivide", n="MLP_RIG_rt_kneeRubberHose_ctrl")

    # mlp and str first armToony ---> RIG_rt_lowLegRubberHose_ctrl
    mlpTibiaRT = cmds.createNode("multiplyDivide", n="MLP_RIG_rt_lowLegRubberHose_ctrl")
    strTibiaSplit3RT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_elbowWeightSplit_3_jnt")
    strTibiaSplit4RT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_elbowWeightSplit_4_jnt")
    strAnkleRT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_elbowWeightSplit_4_jnt")

    cmds.setAttr(strAnkleRT+".minX", -5)
    cmds.setAttr(strAnkleRT+".minY", -5)
    cmds.setAttr(strAnkleRT+".minZ", -5)
    cmds.setAttr(strAnkleRT+".maxX", 5)
    cmds.setAttr(strAnkleRT+".maxY", 5)
    cmds.setAttr(strAnkleRT+".maxZ", 5)
    cmds.setAttr(strAnkleRT+".oldMinX", -12.5)
    cmds.setAttr(strAnkleRT+".oldMinY", -12.5)
    cmds.setAttr(strAnkleRT+".oldMinZ", -12.5)
    cmds.setAttr(strAnkleRT+".oldMaxX", 10)
    cmds.setAttr(strAnkleRT+".oldMaxY", 10)
    cmds.setAttr(strAnkleRT+".oldMaxZ", 10)

    cmds.setAttr(strTibiaSplit4RT+".minX", -7)
    cmds.setAttr(strTibiaSplit4RT+".minY", -7)
    cmds.setAttr(strTibiaSplit4RT+".minZ", -7)
    cmds.setAttr(strTibiaSplit4RT+".maxX", 7)
    cmds.setAttr(strTibiaSplit4RT+".maxY", 7)
    cmds.setAttr(strTibiaSplit4RT+".maxZ", 7)
    cmds.setAttr(strTibiaSplit4RT+".oldMinX", -11)
    cmds.setAttr(strTibiaSplit4RT+".oldMinY", -11)
    cmds.setAttr(strTibiaSplit4RT+".oldMinZ", -11)
    cmds.setAttr(strTibiaSplit4RT+".oldMaxX", 10)
    cmds.setAttr(strTibiaSplit4RT+".oldMaxY", 10)
    cmds.setAttr(strTibiaSplit4RT+".oldMaxZ", 10)

    cmds.setAttr(strTibiaSplit3RT+".minX", -9)
    cmds.setAttr(strTibiaSplit3RT+".minY", -9)
    cmds.setAttr(strTibiaSplit3RT+".minZ", -9)
    cmds.setAttr(strTibiaSplit3RT+".maxX", 9)
    cmds.setAttr(strTibiaSplit3RT+".maxY", 9)
    cmds.setAttr(strTibiaSplit3RT+".maxZ", 9)
    cmds.setAttr(strTibiaSplit3RT+".oldMinX", -10.25)
    cmds.setAttr(strTibiaSplit3RT+".oldMinY", -10.25)
    cmds.setAttr(strTibiaSplit3RT+".oldMinZ", -10.25)
    cmds.setAttr(strTibiaSplit3RT+".oldMaxX", 10)
    cmds.setAttr(strTibiaSplit3RT+".oldMaxY", 10)
    cmds.setAttr(strTibiaSplit3RT+".oldMaxZ", 10)

    # connection controls to multiplies
    cmds.connectAttr(upLegTcntRT+".scaleX", mlpUpLegRT+".input1X", force=True)
    cmds.connectAttr(upLegTcntRT+".scaleY", mlpUpLegRT+".input1Y", force=True)
    cmds.connectAttr(upLegTcntRT+".scaleZ", mlpUpLegRT+".input1Z", force=True)

    cmds.connectAttr(kneeTcntRT+".scaleX", mlpKneeRT+".input1X", force=True)
    cmds.connectAttr(kneeTcntRT+".scaleY", mlpKneeRT+".input1Y", force=True)
    cmds.connectAttr(kneeTcntRT+".scaleZ", mlpKneeRT+".input1Z", force=True)

    cmds.connectAttr(tibiaTcntRT+".scaleX", mlpTibiaRT+".input1X", force=True)
    cmds.connectAttr(tibiaTcntRT+".scaleY", mlpTibiaRT+".input1Y", force=True)
    cmds.connectAttr(tibiaTcntRT+".scaleZ", mlpTibiaRT+".input1Z", force=True)

    # connection mlps to strs
    cmds.connectAttr(mlpUpLegRT+".outputX", strUpLegRT+".valueX", force=True)
    cmds.connectAttr(mlpUpLegRT+".outputY", strUpLegRT+".valueY", force=True)
    cmds.connectAttr(mlpUpLegRT+".outputZ", strUpLegRT+".valueZ", force=True)

    cmds.connectAttr(mlpUpLegRT+".outputX", strUpLegRTSplit1RT+".valueX", force=True)
    cmds.connectAttr(mlpUpLegRT+".outputY", strUpLegRTSplit1RT+".valueY", force=True)
    cmds.connectAttr(mlpUpLegRT+".outputZ", strUpLegRTSplit1RT+".valueZ", force=True)

    cmds.connectAttr(mlpUpLegRT+".outputX", strUpLegRTSpilt2RT+".valueX", force=True)
    cmds.connectAttr(mlpUpLegRT+".outputY", strUpLegRTSpilt2RT+".valueY", force=True)
    cmds.connectAttr(mlpUpLegRT+".outputZ", strUpLegRTSpilt2RT+".valueZ", force=True)

    cmds.connectAttr(mlpTibiaRT+".outputX", strTibiaSplit3RT+".valueX", force=True)
    cmds.connectAttr(mlpTibiaRT+".outputY", strTibiaSplit3RT+".valueY", force=True)
    cmds.connectAttr(mlpTibiaRT+".outputZ", strTibiaSplit3RT+".valueZ", force=True)

    cmds.connectAttr(mlpTibiaRT+".outputX", strTibiaSplit4RT+".valueX", force=True)
    cmds.connectAttr(mlpTibiaRT+".outputY", strTibiaSplit4RT+".valueY", force=True)
    cmds.connectAttr(mlpTibiaRT+".outputZ", strTibiaSplit4RT+".valueZ", force=True)

    cmds.connectAttr(mlpTibiaRT+".outputX", strAnkleRT+".valueX", force=True)
    cmds.connectAttr(mlpTibiaRT+".outputY", strAnkleRT+".valueY", force=True)
    cmds.connectAttr(mlpTibiaRT+".outputZ", strAnkleRT+".valueZ", force=True)

    # connection scale directly where doesn't need the scale averaged
    cmds.connectAttr(mlpUpLegRT+".outputX", "RIG_rt_upLegWeightSplit_3_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpUpLegRT+".outputY", "RIG_rt_upLegWeightSplit_3_jnt.scaleX", force=True)
    cmds.connectAttr(mlpUpLegRT+".outputZ", "RIG_rt_upLegWeightSplit_3_jnt.scaleY", force=True)

    cmds.connectAttr(mlpKneeRT+".outputX", "RIG_rt_upLegWeightSplit_4_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpKneeRT+".outputY", "RIG_rt_upLegWeightSplit_4_jnt.scaleX", force=True)
    cmds.connectAttr(mlpKneeRT+".outputZ", "RIG_rt_upLegWeightSplit_4_jnt.scaleY", force=True)

    cmds.connectAttr(mlpKneeRT+".outputX", "RIG_rt_kneeWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpKneeRT+".outputY", "RIG_rt_kneeWeight_jnt.scaleX", force=True)
    cmds.connectAttr(mlpKneeRT+".outputZ", "RIG_rt_kneeWeight_jnt.scaleY", force=True)

    cmds.connectAttr(mlpKneeRT+".outputX", "RIG_rt_kneeWeightSplit_1_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpKneeRT+".outputY", "RIG_rt_kneeWeightSplit_1_jnt.scaleX", force=True)
    cmds.connectAttr(mlpKneeRT+".outputZ", "RIG_rt_kneeWeightSplit_1_jnt.scaleY", force=True)

    cmds.connectAttr(mlpTibiaRT+".outputX", "RIG_rt_kneeWeightSplit_2_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpTibiaRT+".outputY", "RIG_rt_kneeWeightSplit_2_jnt.scaleX", force=True)
    cmds.connectAttr(mlpTibiaRT+".outputZ", "RIG_rt_kneeWeightSplit_2_jnt.scaleY", force=True)

    # connection strs output to jnts scale
    cmds.connectAttr(strUpLegRT+".outValueX", "RIG_rt_upLegWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpLegRT+".outValueY", "RIG_rt_upLegWeight_jnt.scaleX", force=True)
    cmds.connectAttr(strUpLegRT+".outValueZ", "RIG_rt_upLegWeight_jnt.scaleY", force=True)

    cmds.connectAttr(strUpLegRTSplit1RT+".outValueX", "RIG_rt_upLegWeightSplit_1_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpLegRTSplit1RT+".outValueY", "RIG_rt_upLegWeightSplit_1_jnt.scaleX", force=True)
    cmds.connectAttr(strUpLegRTSplit1RT+".outValueZ", "RIG_rt_upLegWeightSplit_1_jnt.scaleY", force=True)

    cmds.connectAttr(strUpLegRTSpilt2RT+".outValueX", "RIG_rt_upLegWeightSplit_2_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpLegRTSpilt2RT+".outValueY", "RIG_rt_upLegWeightSplit_2_jnt.scaleX", force=True)
    cmds.connectAttr(strUpLegRTSpilt2RT+".outValueZ", "RIG_rt_upLegWeightSplit_2_jnt.scaleY", force=True)

    cmds.connectAttr(strTibiaSplit3RT+".outValueX", "RIG_rt_kneeWeightSplit_3_jnt.scaleZ", force=True)
    cmds.connectAttr(strTibiaSplit3RT+".outValueY", "RIG_rt_kneeWeightSplit_3_jnt.scaleX", force=True)
    cmds.connectAttr(strTibiaSplit3RT+".outValueZ", "RIG_rt_kneeWeightSplit_3_jnt.scaleY", force=True)

    cmds.connectAttr(strTibiaSplit4RT+".outValueX", "RIG_rt_kneeWeightSplit_4_jnt.scaleZ", force=True)
    cmds.connectAttr(strTibiaSplit4RT+".outValueY", "RIG_rt_kneeWeightSplit_4_jnt.scaleX", force=True)
    cmds.connectAttr(strTibiaSplit4RT+".outValueZ", "RIG_rt_kneeWeightSplit_4_jnt.scaleY", force=True)

    cmds.connectAttr(strAnkleRT+".outValueX", "RIG_rt_ankleWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(strAnkleRT+".outValueY", "RIG_rt_ankleWeight_jnt.scaleX", force=True)
    cmds.connectAttr(strAnkleRT+".outValueZ", "RIG_rt_ankleWeight_jnt.scaleY", force=True)

    cmds.select ( cl=True )

    #ultimate fixes
    cmds.delete ('RIG_lf_legPvCtrlGrp_lfLegIkCtrlSpcParCon')
    cmds.delete ('RIG_rt_legPvCtrlGrp_rtLegIkCtrlSpcParCon')
    cmds.setAttr ("RIG_lf_upLeg_fk_ctrl.align", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_rt_upLeg_fk_ctrl.align", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_lf_knee_pv_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_lf_knee_pv_ctrl.lfLegIkCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_rt_knee_pv_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_rt_knee_pv_ctrl.rtLegIkCtrl", lock=True, keyable=False, channelBox=False )

    cmds.group("RIG_lf_upLeg_jnt","RIG_rt_upLeg_jnt", n="GRP_legs_Joints")
    cmds.parent("GRP_legs_Joints","RIG_Skeleton_grp")
    cmds.delete("RIG_spine_low_jnt")
    cmds.delete("weightSpine_grp")
    cmds.delete("lf_armWeightJnt_grp")
    cmds.delete("rt_armWeightJnt_grp")

    try:
        cmds.delete("abRTDeleteMeWhenDone")
    except:
        pass
        
    cmds.select ( cl=True )