import maya.cmds as mc
import maya.api.OpenMaya as om2

""" Inserisce degli spaceSwitch sul controllo IK delle zampe o gambe quadrupedi dei char """

def bipSpaceSwitch(toCreateAttr, toConstrain, *args):

	listOfSpace = args
	niceNames = toCreateAttr.replace('_constraint_ik_Ctrl',''), 'world', 'transform', 'cog'

	mc.addAttr(toCreateAttr, longName = "Follow", k = True, at = 'enum', enumName = 'world=0:transform=1:cog=2')
	parentConstraint = mc.parentConstraint(listOfSpace, toConstrain, mo = True)[0]
	weightList = mc.parentConstraint(parentConstraint, q= True, wal = True)

	for num, item in enumerate(listOfSpace):
		condition = mc.createNode('condition', name = 'CON_{}_spaceSwitch_{}'.format(niceNames[0], niceNames[num+1]))
		mc.setAttr("{}.firstTerm".format(condition), num)
		mc.setAttr("{}.colorIfTrueR".format(condition), 1)
		mc.setAttr("{}.colorIfFalseR".format(condition), 0)

		mc.connectAttr('{}.Follow'.format(toCreateAttr), "{}.secondTerm".format(condition))
		mc.connectAttr('{}.outColorR'.format(condition), '{}.{}'.format(parentConstraint, weightList[num]))

	mc.setAttr("{}.Follow".format(toCreateAttr), 1)

def quadSpaceSwitch(toCreateAttr, toConstrain, side, *args):

	listOfSpace = args

	if 'rear' in toCreateAttr:

			niceNames = toCreateAttr.replace('_constraint_ik_Ctrl',''), 'world', 'transform', 'cog', 'hip', 'upleg'
			mc.addAttr(toCreateAttr, longName = "Follow", k = True, at = 'enum', enumName = 'world=0:transform=1:cog=2:hip=3:upleg=4')

	elif 'front' in toCreateAttr:

			niceNames = toCreateAttr.replace('_constraint_ik_Ctrl',''), 'world', 'transform', 'cog', 'head', 'upleg'
			mc.addAttr(toCreateAttr, longName = "Follow", k = True, at = 'enum', enumName = 'world=0:transform=1:cog=2:head=3:upleg=4')


	parentConstraint = mc.parentConstraint(listOfSpace, toConstrain, mo = True)[0]
	weightList = mc.parentConstraint(parentConstraint, q= True, wal = True)

	for num, item in enumerate(listOfSpace):
		condition = mc.createNode('condition', name = 'CON_{}_{}_spaceSwitch_{}'.format(niceNames[0], side, niceNames[num+1]))
		mc.setAttr("{}.firstTerm".format(condition), num)
		mc.setAttr("{}.colorIfTrueR".format(condition), 1)
		mc.setAttr("{}.colorIfFalseR".format(condition), 0)

		mc.connectAttr('{}.Follow'.format(toCreateAttr), "{}.secondTerm".format(condition))
		mc.connectAttr('{}.outColorR'.format(condition), '{}.{}'.format(parentConstraint, weightList[num]))

	mc.setAttr("{}.Follow".format(toCreateAttr), 1)

def unlockAttr(toCheck):

	lockedAttr = mc.listAttr(toCheck, locked = True)

	if not lockedAttr == None :

		for attr in lockedAttr:

			mc.setAttr("{}.{}".format(toCheck[0], attr), lock = False) 

	else:

		pass

def createSpaceSwitchOnQuad():

	mc.undoInfo(openChunk = True)

	if mc.objExists("L_foot_constraint_ik_Ctrl") and mc.objExists("R_foot_constraint_ik_Ctrl"):

		constraintCtrl = "L_foot_constraint_ik_Ctrl", "R_foot_constraint_ik_Ctrl"
		footCtrl = constraintCtrl[0].replace("constraint_", ""), constraintCtrl[1].replace("constraint_", "")


		for num, item in enumerate(constraintCtrl):

			constraintCtrlGrp = mc.listRelatives(item, parent = True)

			unlockAttr(constraintCtrlGrp)
			
			hierarchyOrder = mc.listRelatives(constraintCtrlGrp, parent = True)[0]

			ctrlGrpSW = mc.group(empty = True, name = '{}_spaceSwitch'.format(constraintCtrlGrp[0]))
			mc.matchTransform(ctrlGrpSW, constraintCtrlGrp)
			mc.parent(constraintCtrlGrp, ctrlGrpSW)
			mc.parent(ctrlGrpSW, hierarchyOrder)

			bipSpaceSwitch(footCtrl[num], ctrlGrpSW, "RIG_worldSpaceCtrl_space_switch_grp", "Transform_Null", "RIG_cogCtrl_space_switch_grp")

	else:
		om2.MGlobal.displayInfo('Non esistono controlli che si chiamano L/R_foot_constraint_ik_Ctrl')

	if mc.objExists("L_front_foot_constraint_ik_Ctrl") and mc.objExists("R_front_foot_constraint_ik_Ctrl"):

		constraintCtrl = "L_front_foot_constraint_ik_Ctrl", "R_front_foot_constraint_ik_Ctrl"
		footCtrl = constraintCtrl[0].replace("constraint_",""), constraintCtrl[1].replace("constraint_","")

		for num, item in enumerate(constraintCtrl):

			constraintCtrlGrp = mc.listRelatives(item, parent = True)

			unlockAttr(constraintCtrlGrp)

			hierarchyOrder = mc.listRelatives(constraintCtrlGrp, parent = True)[0]

			ctrlGrpSW = mc.group(empty = True, name = '{}_spaceSwitch'.format(constraintCtrlGrp[0]))
			mc.matchTransform(ctrlGrpSW, constraintCtrlGrp)
			mc.parent(constraintCtrlGrp, ctrlGrpSW)
			mc.parent(ctrlGrpSW, hierarchyOrder)

			if num == 0:

				quadSpaceSwitch(footCtrl[0], ctrlGrpSW, "L", "RIG_worldSpaceCtrl_space_switch_grp", "Transform_Null", "RIG_cogCtrl_space_switch_grp", "head_Ctrl", "L_front_upleg_Ctrl")

			if num == 1:

				quadSpaceSwitch(footCtrl[1], ctrlGrpSW, "R", "RIG_worldSpaceCtrl_space_switch_grp", "Transform_Null", "RIG_cogCtrl_space_switch_grp", "head_Ctrl", "R_front_upleg_Ctrl")

	else:
		om2.MGlobal.displayInfo('Non esistono controlli che si chiamano L/R_front_foot_constraint_ik_Ctrl')


	if mc.objExists("L_rear_foot_constraint_ik_Ctrl") and mc.objExists("L_rear_foot_constraint_ik_Ctrl"):

		constraintCtrl = "L_rear_foot_constraint_ik_Ctrl", "R_rear_foot_constraint_ik_Ctrl"
		footCtrl = constraintCtrl[0].replace("constraint_",""), constraintCtrl[1].replace("constraint_","")

		for num, item in enumerate(constraintCtrl):

			constraintCtrlGrp = mc.listRelatives(item, parent = True)

			unlockAttr(constraintCtrlGrp)

			hierarchyOrder = mc.listRelatives(constraintCtrlGrp, parent = True)[0]

			ctrlGrpSW = mc.group(empty = True, name = '{}_spaceSwitch'.format(constraintCtrlGrp[0]))
			mc.matchTransform(ctrlGrpSW, constraintCtrlGrp)
			mc.parent(constraintCtrlGrp, ctrlGrpSW)
			mc.parent(ctrlGrpSW, hierarchyOrder)

			if num == 0:

				quadSpaceSwitch(footCtrl[0], ctrlGrpSW, "L", "RIG_worldSpaceCtrl_space_switch_grp", "Transform_Null", "RIG_cogCtrl_space_switch_grp", "hip_Ctrl", "L_rear_upleg_Ctrl")

			if num == 1:

				quadSpaceSwitch(footCtrl[1], ctrlGrpSW, "R", "RIG_worldSpaceCtrl_space_switch_grp", "Transform_Null", "RIG_cogCtrl_space_switch_grp", "hip_Ctrl", "R_rear_upleg_Ctrl")

	else:

		om2.MGlobal.displayInfo('Non esistono controlli che si chiamano L/R_rear_foot_constraint_ik_Ctrl')

	mc.undoInfo(closeChunk = True)

createSpaceSwitchOnQuad()