import maya.cmds as mc

""" 
	Sistema il masterCup del bipede, vanno sistemati i valori del multiplyDivide che viene creato sopra il controllo TOP di ogni dito 
	Serve aver fatto il finger module
	Selezionare i controlli "top", anche tutti insieme tra L e R e lanciare lo script
"""

def createCupBiped(finger):

	name = finger.split("_")[1]
	side = finger.split("_")[0]

	section = (name, name.replace("Top", "Base"), name.replace("Top", "Mid"), name.replace("Top", "Tip"))

	if mc.objExists("MLP_RIG_{}_{}_fk_grp_offset_Cup".format(side.replace("L", "lf").replace("R", "rt"), name)):

		mdlCup = "MLP_RIG_{}_{}_fk_grp_offset_Cup".format(side.replace("L", "lf").replace("R", "rt"), name)
		mc.select(mdlCup)

		for item in section[1::]:

			cupGrp = mc.group(empty = True, name = "RIG_{}_{}_fk_grp_cup".format(side, item))
			mc.matchTransform(cupGrp, "{}_{}_fk_Ctrl".format(side, item))
			mc.parent(cupGrp, "RIG_{}_{}_fk_grp_offset".format(side.replace("L", "lf").replace("R", "rt"), item))

			mc.parent("{}_{}_fk_Ctrl".format(side, item), cupGrp)
			mc.connectAttr("{}.outputY".format(mdlCup), "{}.rotateY".format(cupGrp))

	else:

		mdlCup = mc.createNode("multiplyDivide", name = "MLP_RIG_{}_{}_fk_grp_offset_Cup".format(side.replace("L", "lf").replace("R", "rt"), name))
		mc.connectAttr("{}_hand_Ctrl.master_cup".format(side), "{}.input1X".format(mdlCup))


		for item in section:

			cupGrp = mc.group(empty = True, name = "RIG_{}_{}_fk_grp_cup".format(side, item))
			mc.matchTransform(cupGrp, "{}_{}_fk_Ctrl".format(side, item))
			mc.parent(cupGrp, "RIG_{}_{}_fk_grp_offset".format(side.replace("L", "lf").replace("R", "rt"), item))

			mc.parent("{}_{}_fk_Ctrl".format(side, item), cupGrp)
			mc.connectAttr("{}.outputX".format(mdlCup), "{}.rotateY".format(cupGrp))

sel = mc.ls(sl = True)

for num, item in enumerate(sel):

	createCupBiped(item)