import maya.cmds as cmds

'''
Script che serve per l'autorig delle code per le sirene, prendendo come riferimento il rig di "MerlindaMermaid". Lanciando questo script, si aprira una Tab
con 4 pulsanti, il primo "Create Locators" crea i Locator da posizionare per la posizione dei 4 joint principali. Il secondo Button ,"Create Joints", creera i jnt, in modo tale da controllare 
l'orientamento e la struttura prima di premere il terzo pulsante "Auto Tail Rig" , che costruisce il rig effettivo della coda della sirenae crea due set, uno contenente i
joint che devono essere selezionati per lo script dei Bendy "Bendy_Jnt_Selection" , ed un altro set con i Controlli principali "Tail_Controls".
Il Quarto ed ultimo bottone, "Connect Tail to Body", serve per agganciare il rig della coda con il Body Rig della sirena.Prima di premere questo pulsante bisogna importare il rig del body del seguente char,
cancellare i controlli e i jnt delle gambe, e skinnare il nuovo body.
Alla fine del procedimento, servira sostituire le shape provvisorie che crea lo script, con le shape effettive della coda presenti in "MerlindaMermaid".

'''

def CreateLOC():
    
    if cmds.objExists("Up"):
       cmds.delete("Up")
    if cmds.objExists("Mid"):
       cmds.delete("Mid")
    if cmds.objExists("Low"):
       cmds.delete("Low")
    if cmds.objExists("End"):
       cmds.delete("End")
    
    LocUp = cmds.spaceLocator(n = "Up")
    cmds.select (LocUp)
    cmds.move ( 92.198, y=True )
    cmds.move ( -1.697, z=True )
    LocMid = cmds.spaceLocator(n = "Mid")
    cmds.select (LocMid)
    cmds.move ( 51.379, y=True )
    cmds.move ( -0.47, z=True)
    LocLow = cmds.spaceLocator(n = "Low")
    cmds.select (LocLow)
    cmds.move ( -0.003, y=True )
    cmds.move ( -2.177, z=True )
    LocEnd = cmds.spaceLocator(n = "End")
    cmds.move ( -34.695, y=True )
    cmds.move ( -2.663, z=True )

def CreateJoints():
    
    
    if cmds.objExists("up_tail_jnt"):
       cmds.delete("up_tail_jnt")
    if cmds.objExists("mid_tail_jnt"):
       cmds.delete("mid_tail_jnt")
    if cmds.objExists("low_tail_jnt"):
       cmds.delete("low_tail_jnt")
    if cmds.objExists("end_tail_jnt"):
       cmds.delete("end_tail_jnt")
       
    cmds.select("Mid")
    cmds.joint(n = "up_tail_jnt")
    cmds.parent("up_tail_jnt", world = True)
    cmds.matchTransform ("up_tail_jnt", "Up")
    cmds.joint(n = "mid_tail_jnt")
    cmds.parent("mid_tail_jnt", world = True)
    cmds.matchTransform ("mid_tail_jnt", "Mid")
    cmds.joint(n = "low_tail_jnt")
    cmds.parent("low_tail_jnt", world = True)
    cmds.matchTransform ("low_tail_jnt", "Low")
    cmds.joint(n = "end_tail_jnt")
    cmds.parent("end_tail_jnt", world = True)
    cmds.matchTransform ("end_tail_jnt", "End")
    
    cmds.parent("end_tail_jnt","low_tail_jnt")
    cmds.parent("low_tail_jnt","mid_tail_jnt")
    cmds.parent("mid_tail_jnt","up_tail_jnt")
    cmds.select("up_tail_jnt")
    cmds.joint (e=True, zso=True, oj="xyz",ch=True, sao="zdown")
    cmds.setAttr("end_tail_jnt.rx", -90)
    cmds.setAttr("end_tail_jnt.rz", -90)
    cmds.select("end_tail_jnt")
    cmds.makeIdentity(apply=True, t=0, r=1, s=0, n=0)
    cmds.select(clear=True)
    
    
def AutoTailRig():
    
    cmds.undoInfo(openChunk = True)
      
    cmds.duplicate("up_tail_jnt",rr=True, rc = True)
    Suffix = ("_IK_jnt")
    cmds.select("up_tail_jnt1")
    cmds.rename("up_tail" + Suffix)
    cmds.select("mid_tail_jnt1")
    cmds.rename("mid_tail" + Suffix)
    cmds.select("low_tail_jnt1")
    cmds.rename("low_tail" + Suffix)
    cmds.select("end_tail_jnt1")
    cmds.rename("end_tail" + Suffix)
    
    cmds.duplicate("up_tail_jnt",rr=True, rc = True)
    Suffix2 = ("_FK_jnt")
    cmds.select("up_tail_jnt1")
    cmds.rename("up_tail" + Suffix2)
    cmds.select("mid_tail_jnt1")
    cmds.rename("mid_tail" + Suffix2)
    cmds.select("low_tail_jnt1")
    cmds.rename("low_tail" + Suffix2)
    cmds.select("end_tail_jnt1")
    cmds.rename("end_tail" + Suffix2)
    
    cmds.duplicate("low_tail_jnt",rr=True, rc = True)
    cmds.parent("low_tail_jnt1", world = True)
    cmds.rename("low_tail_jnt1", "low_tail_foot_jnt")
    cmds.parent("end_tail_jnt1", world = True)
    cmds.rename("end_tail_jnt1", "end_tail_foot_jnt")
    cmds.parent("end_tail_foot_jnt", "low_tail_foot_jnt")
    
    cmds.duplicate("low_tail_foot_jnt",rr=True, rc = True)
    cmds.select("low_tail_foot_jnt1")
    cmds.rename("low_tail_foot" + Suffix)
    cmds.select("end_tail_foot_jnt1")
    cmds.rename("end_tail_foot" + Suffix)
    
    cmds.duplicate("low_tail_foot_jnt",rr=True, rc = True)
    cmds.select("low_tail_foot_jnt1")
    cmds.rename("low_tail_foot" + Suffix2)
    cmds.select("end_tail_foot_jnt1")
    cmds.rename("end_tail_foot" + Suffix2)
    cmds.setAttr("up_tail_IK_jnt.visibility",0)
    cmds.setAttr("up_tail_FK_jnt.visibility",0)
    cmds.setAttr("low_tail_foot_IK_jnt.visibility",0)
    cmds.setAttr("low_tail_foot_FK_jnt.visibility",0)
    
  
   
    
    cmds.parentConstraint("up_tail_IK_jnt", "up_tail_FK_jnt", "up_tail_jnt", mo=True)
    cmds.scaleConstraint("up_tail_IK_jnt", "up_tail_FK_jnt", "up_tail_jnt", mo=True)
    cmds.parentConstraint("mid_tail_IK_jnt", "mid_tail_FK_jnt", "mid_tail_jnt", mo=True)
    cmds.scaleConstraint("mid_tail_IK_jnt", "mid_tail_FK_jnt", "mid_tail_jnt", mo=True)
    cmds.parentConstraint("low_tail_IK_jnt", "low_tail_FK_jnt", "low_tail_jnt", mo=True)
    cmds.scaleConstraint("low_tail_IK_jnt", "low_tail_FK_jnt", "low_tail_jnt", mo=True)
    cmds.parentConstraint("end_tail_IK_jnt", "end_tail_FK_jnt", "end_tail_jnt", mo=True)
    cmds.scaleConstraint("end_tail_IK_jnt", "end_tail_FK_jnt", "end_tail_jnt", mo=True)
    cmds.parentConstraint("low_tail_foot_IK_jnt", "low_tail_foot_FK_jnt", "low_tail_foot_jnt", mo=True)
    cmds.scaleConstraint("low_tail_foot_IK_jnt", "low_tail_foot_FK_jnt", "low_tail_foot_jnt", mo=True)
    cmds.parentConstraint("end_tail_foot_IK_jnt", "end_tail_foot_FK_jnt", "end_tail_foot_jnt", mo=True)
    cmds.scaleConstraint("end_tail_foot_IK_jnt", "end_tail_foot_FK_jnt", "end_tail_foot_jnt", mo=True)
    
    cmds.circle(n = "tail_Ctrl", r = 7)
    cmds.setAttr ("tail_Ctrl"  +'.rotate', lock = True  )
    cmds.setAttr ("tail_Ctrl.rotateX", k = False  )
    cmds.setAttr ("tail_Ctrl.rotateY", k = False  )
    cmds.setAttr ("tail_Ctrl.rotateZ", k = False  )
    cmds.setAttr ("tail_Ctrl"  +'.translate', lock = True  )
    cmds.setAttr ("tail_Ctrl.translateX", k = False  )  
    cmds.setAttr ("tail_Ctrl.translateY", k = False  )
    cmds.setAttr ("tail_Ctrl.translateZ", k = False  )
    cmds.setAttr ("tail_Ctrl"  +'.scale', lock = True)
    cmds.setAttr ("tail_Ctrl.scaleX", k = False  )
    cmds.setAttr ("tail_Ctrl.scaleY", k = False  )
    cmds.setAttr ("tail_Ctrl.scaleZ", k = False  )
    cmds.setAttr ("tail_Ctrl.visibility", k = False, lock = True)
    cmds.DeleteHistory("tail_Ctrl")
    cmds.addAttr ( "tail_Ctrl", shortName='Switch_IKFK', longName='SwitchIKFK', defaultValue=0, minValue=0, maxValue=1, k= True)
    cmds.addAttr ( "tail_Ctrl", ln= "Bendy_Controls", at="enum", en= "off:on", k=True)
    cmds.setAttr("tail_Ctrl.Switch_IKFK",1)
    cmds.setAttr ("tail_Ctrl.Bendy_Controls",cb=True)
    cmds.addAttr ( "tail_Ctrl", ln= "Bendy_Tweak_Controls", at="enum", en= "off:on", k=False)
    cmds.setAttr ("tail_Ctrl.Bendy_Tweak_Controls",cb=True)
    cmds.addAttr ( "tail_Ctrl", ln= "Constraint_Vis", at="enum", en= "off:on", k=False)
    cmds.setAttr ("tail_Ctrl.Constraint_Vis",cb=True)
    cmds.group("tail_Ctrl", n ="tail_Ctrl_offset" )
    
    cmds.circle(n = "tail_IK_Ctrl", r = 12)
    cmds.select("tail_IK_Ctrl")
    cmds.rotate(90 , x=True)
    cmds.makeIdentity("tail_IK_Ctrl", apply = True)
    cmds.DeleteHistory("tail_IK_Ctrl")
    cmds.addAttr ( "tail_IK_Ctrl", shortName='Stretch', longName='Stretch', defaultValue=0, minValue=0, maxValue=1, k= True)
    cmds.addAttr( "tail_IK_Ctrl", ln= "Follow", at="enum", en= "World:Transform:Cog:Hip", k=True)
    cmds.setAttr ("tail_IK_Ctrl.visibility", k = False, lock = True)
    cmds.group("tail_IK_Ctrl", n = "tail_IK_Ctrl_offset")
    cmds.circle(n = "tail_IK_constraint_Ctrl", r = 15)
    cmds.select("tail_IK_constraint_Ctrl")
    cmds.rotate(90 , x=True)
    cmds.makeIdentity("tail_IK_constraint_Ctrl", apply = True)
    cmds.DeleteHistory("tail_IK_constraint_Ctrl")
    cmds.setAttr ("tail_IK_constraint_Ctrl.visibility", k = False, lock = True)
    cmds.group("tail_IK_constraint_Ctrl", n = "tail_IK_constraint_Ctrl_offset")
    cmds.parent("tail_IK_Ctrl_offset", "tail_IK_constraint_Ctrl")
    len = cmds.select("tail_IK_constraint_Ctrl_offset", "low_tail_jnt")
    cmds.MatchTranslation(len)
    cmds.circle(n = "up_tail_IK_Ctrl", r = 20)
    cmds.select("up_tail_IK_Ctrl")
    cmds.rotate(90 , y=True)
    cmds.makeIdentity("up_tail_IK_Ctrl", apply = True)
    cmds.DeleteHistory("up_tail_IK_Ctrl")
    cmds.setAttr ("up_tail_IK_Ctrl"  +'.scale', lock = True)
    cmds.setAttr ("up_tail_IK_Ctrl.scaleX", k = False  )
    cmds.setAttr ("up_tail_IK_Ctrl.scaleY", k = False  )
    cmds.setAttr ("up_tail_IK_Ctrl.scaleZ", k = False  )
    cmds.setAttr ("up_tail_IK_Ctrl.visibility", k = False, lock = True)
    cmds.group("up_tail_IK_Ctrl", n = "up_tail_IK_Ctrl_offset")
    cmds.matchTransform( "up_tail_IK_Ctrl_offset", "up_tail_jnt")
    cmds.circle(n = "tail_IK_poleVector", r = 6)
    cmds.DeleteHistory("tail_IK_poleVector")
    cmds.setAttr ("tail_IK_poleVector"  +'.scale', lock = True)
    cmds.setAttr ("tail_IK_poleVector.scaleX", k = False  )
    cmds.setAttr ("tail_IK_poleVector.scaleY", k = False  )
    cmds.setAttr ("tail_IK_poleVector.scaleZ", k = False  )
    cmds.setAttr ("tail_IK_poleVector"  +'.rotate', lock = True)
    cmds.setAttr ("tail_IK_poleVector.rotateX", k = False  )
    cmds.setAttr ("tail_IK_poleVector.rotateY", k = False  )
    cmds.setAttr ("tail_IK_poleVector.rotateZ", k = False  )
    cmds.setAttr ("tail_IK_poleVector.visibility", k = False, lock = True)
    
    cmds.group("tail_IK_poleVector", n = "tail_IK_poleVector_offset")
    done = cmds.select("tail_IK_poleVector_offset", "mid_tail_FK_jnt")
    cmds.MatchTranslation(done)
    cmds.select("tail_IK_poleVector_offset")
    cmds.move(40, z=True)
    
    cmds.circle(n = "up_tail_FK_Ctrl", r = 20)
    cmds.select("up_tail_FK_Ctrl")
    cmds.rotate(90 , y=True)
    cmds.makeIdentity("up_tail_FK_Ctrl", apply = True)
    cmds.group("up_tail_FK_Ctrl", n = "up_tail_FK_Ctrl_offset")
    cmds.DeleteHistory("up_tail_FK_Ctrl")
    cmds.setAttr ("up_tail_FK_Ctrl.visibility", k = False, lock = True)
    cmds.circle(n = "mid_tail_FK_Ctrl", r = 15)
    cmds.select("mid_tail_FK_Ctrl")
    cmds.rotate(90 , y=True)
    cmds.makeIdentity("mid_tail_FK_Ctrl", apply = True)
    cmds.group("mid_tail_FK_Ctrl", n = "mid_tail_FK_Ctrl_offset")
    cmds.DeleteHistory("mid_tail_FK_Ctrl")
    cmds.setAttr ("mid_tail_FK_Ctrl.visibility", k = False, lock = True)
    cmds.circle(n = "low_tail_FK_Ctrl", r = 10)
    cmds.select("low_tail_FK_Ctrl")
    cmds.rotate(90 , y=True)
    cmds.makeIdentity("low_tail_FK_Ctrl", apply = True)
    cmds.group("low_tail_FK_Ctrl", n = "low_tail_FK_Ctrl_offset")
    cmds.DeleteHistory("low_tail_FK_Ctrl")
    cmds.setAttr ("low_tail_FK_Ctrl.visibility", k = False, lock = True)
    cmds.matchTransform("up_tail_FK_Ctrl_offset", "up_tail_jnt")
    cmds.matchTransform("mid_tail_FK_Ctrl_offset", "mid_tail_jnt")
    cmds.matchTransform("low_tail_FK_Ctrl_offset", "low_tail_jnt")
    
    Blue = ( "up_tail_FK_Ctrl", "mid_tail_FK_Ctrl", "low_tail_FK_Ctrl",  "tail_Ctrl")
    shapeBlue = cmds.listRelatives(Blue, children = True, shapes = True)

    
    for shape in shapeBlue:
        cmds.setAttr(str(shape) + ".overrideEnabled", 1)
        cmds.setAttr(str(shape) + ".overrideColor", 6);

    Red =  ("tail_IK_Ctrl", "up_tail_IK_Ctrl", "tail_IK_poleVector")
    shapeRed = cmds.listRelatives(Red, children = True, shapes = True)
    
    for shape in shapeRed:
        cmds.setAttr(str(shape) + ".overrideEnabled", 1)
        cmds.setAttr(str(shape) + ".overrideColor", 13);
    
    cmds.setAttr("tail_IK_constraint_CtrlShape.overrideEnabled", 1)
    cmds.setAttr("tail_IK_constraint_CtrlShape.overrideColor", 4)
    
   
 
    cmds.createNode("reverse" , n='Rev_tail_foot_jnt_IKFK')
    cmds.connectAttr("tail_Ctrl.Switch_IKFK", "Rev_tail_foot_jnt_IKFK.inputX")
    cmds.connectAttr("Rev_tail_foot_jnt_IKFK.outputX", "up_tail_jnt_parentConstraint1.up_tail_FK_jntW1")
    cmds.connectAttr("Rev_tail_foot_jnt_IKFK.outputX", "up_tail_jnt_scaleConstraint1.up_tail_FK_jntW1")
    cmds.connectAttr("Rev_tail_foot_jnt_IKFK.outputX", "mid_tail_jnt_parentConstraint1.mid_tail_FK_jntW1")
    cmds.connectAttr("Rev_tail_foot_jnt_IKFK.outputX", "mid_tail_jnt_scaleConstraint1.mid_tail_FK_jntW1")
    cmds.connectAttr("Rev_tail_foot_jnt_IKFK.outputX", "low_tail_jnt_parentConstraint1.low_tail_FK_jntW1")
    cmds.connectAttr("Rev_tail_foot_jnt_IKFK.outputX", "low_tail_jnt_scaleConstraint1.low_tail_FK_jntW1")
    cmds.connectAttr("Rev_tail_foot_jnt_IKFK.outputX", "end_tail_jnt_parentConstraint1.end_tail_FK_jntW1")
    cmds.connectAttr("Rev_tail_foot_jnt_IKFK.outputX", "end_tail_jnt_scaleConstraint1.end_tail_FK_jntW1")
    
    cmds.connectAttr("tail_Ctrl.Switch_IKFK", "up_tail_jnt_parentConstraint1.up_tail_IK_jntW0")
    cmds.connectAttr("tail_Ctrl.Switch_IKFK", "up_tail_jnt_scaleConstraint1.up_tail_IK_jntW0")
    cmds.connectAttr("tail_Ctrl.Switch_IKFK", "mid_tail_jnt_parentConstraint1.mid_tail_IK_jntW0")
    cmds.connectAttr("tail_Ctrl.Switch_IKFK", "mid_tail_jnt_scaleConstraint1.mid_tail_IK_jntW0")
    cmds.connectAttr("tail_Ctrl.Switch_IKFK", "low_tail_jnt_parentConstraint1.low_tail_IK_jntW0")
    cmds.connectAttr("tail_Ctrl.Switch_IKFK", "low_tail_jnt_scaleConstraint1.low_tail_IK_jntW0")
    cmds.connectAttr("tail_Ctrl.Switch_IKFK", "end_tail_jnt_parentConstraint1.end_tail_IK_jntW0")
    cmds.connectAttr("tail_Ctrl.Switch_IKFK", "end_tail_jnt_scaleConstraint1.end_tail_IK_jntW0")
    
    cmds.connectAttr("Rev_tail_foot_jnt_IKFK.outputX", "low_tail_foot_jnt_parentConstraint1.low_tail_foot_FK_jntW1")
    cmds.connectAttr("Rev_tail_foot_jnt_IKFK.outputX", "low_tail_foot_jnt_scaleConstraint1.low_tail_foot_FK_jntW1")
    cmds.connectAttr("Rev_tail_foot_jnt_IKFK.outputX", "end_tail_foot_jnt_parentConstraint1.end_tail_foot_FK_jntW1")
    cmds.connectAttr("Rev_tail_foot_jnt_IKFK.outputX", "end_tail_foot_jnt_scaleConstraint1.end_tail_foot_FK_jntW1")
    
    cmds.connectAttr("tail_Ctrl.Switch_IKFK", "low_tail_foot_jnt_parentConstraint1.low_tail_foot_IK_jntW0")
    cmds.connectAttr("tail_Ctrl.Switch_IKFK", "low_tail_foot_jnt_scaleConstraint1.low_tail_foot_IK_jntW0")
    cmds.connectAttr("tail_Ctrl.Switch_IKFK", "end_tail_foot_jnt_parentConstraint1.end_tail_foot_IK_jntW0")
    cmds.connectAttr("tail_Ctrl.Switch_IKFK", "end_tail_foot_jnt_scaleConstraint1.end_tail_foot_IK_jntW0")
    
    
    cmds.matchTransform ("tail_Ctrl_offset","end_tail_foot_jnt" )
    cmds.select("tail_Ctrl_offset")
    cmds.move(-80, y=True)
    
    
    
    
    IK = cmds.select("up_tail_IK_jnt", "low_tail_IK_jnt")
    cmds.ikHandle(IK, n = "IKHandle_Tail")
    cmds.setAttr("IKHandle_Tail.visibility",0)
    cmds.parent("IKHandle_Tail","tail_IK_Ctrl")
    cmds.poleVectorConstraint("tail_IK_poleVector", "IKHandle_Tail")
    cmds.parentConstraint("tail_IK_Ctrl", "low_tail_foot_IK_jnt",mo=True)
    cmds.scaleConstraint("tail_IK_Ctrl", "low_tail_foot_IK_jnt",mo=True)
    
    cmds.delete("end_tail_jnt","end_tail_IK_jnt","end_tail_FK_jnt")
    
    cmds.parentConstraint("up_tail_IK_Ctrl", "up_tail_IK_jnt",mo=True)
     
    cmds.distanceDimension( sp=(-5, 0, 0), ep=(5, 0, 0) )
    cmds.rename("distanceDimension1", "DIS_Tail_IK_Stretch")
    cmds.rename("locator1", "LOC_01_Tail_IK_Stretch")
    cmds.rename("locator2", "LOC_02_Tail_IK_Stretch")
    cmds.setAttr("LOC_01_Tail_IK_Stretch.visibility",0)
    cmds.setAttr("LOC_02_Tail_IK_Stretch.visibility",0)
    cmds.matchTransform("LOC_01_Tail_IK_Stretch", "up_tail_jnt")
    cmds.matchTransform("LOC_02_Tail_IK_Stretch", "low_tail_jnt")
    cmds.parent("LOC_01_Tail_IK_Stretch", "up_tail_IK_Ctrl")
    cmds.parent("LOC_02_Tail_IK_Stretch", "tail_IK_Ctrl")
    cmds.distanceDimension( sp=(-5, 0, 0), ep=(5, 0, 0) )  
    cmds.rename("distanceDimension1", "DIS_Tail_IK_Stretch_ScaleFix")
    cmds.rename("locator1", "LOC_01_Tail_IK_Stretch_ScaleFix")
    cmds.rename("locator2", "LOC_02_Tail_IK_Stretch_ScaleFix")
    cmds.setAttr("LOC_01_Tail_IK_Stretch_ScaleFix.visibility",0)
    cmds.setAttr("LOC_02_Tail_IK_Stretch_ScaleFix.visibility",0)
    cmds.matchTransform("LOC_01_Tail_IK_Stretch_ScaleFix", "up_tail_jnt")
    cmds.matchTransform("LOC_02_Tail_IK_Stretch_ScaleFix", "low_tail_jnt")
    
    cmds.createNode("multiplyDivide", n = "MLD_tail_IK_Stretch")
    cmds.setAttr("MLD_tail_IK_Stretch.operation", 2)
    cmds.createNode("condition", n = "CND_tail_IK_Stretch")
    cmds.setAttr("CND_tail_IK_Stretch.secondTerm", 1)
    cmds.setAttr("CND_tail_IK_Stretch.operation", 2)
    cmds.createNode("blendColors", n = "BLD_tail_IK_Stretch")
    cmds.setAttr("BLD_tail_IK_Stretch.color2R",1)
    cmds.connectAttr("DIS_Tail_IK_Stretch_ScaleFix.distance", "MLD_tail_IK_Stretch.input2X")
    cmds.connectAttr("DIS_Tail_IK_Stretch.distance", "MLD_tail_IK_Stretch.input1X")
    cmds.connectAttr("MLD_tail_IK_Stretch.outputX", "CND_tail_IK_Stretch.colorIfTrueR")
    cmds.connectAttr("MLD_tail_IK_Stretch.outputX", "CND_tail_IK_Stretch.firstTerm")
    cmds.connectAttr("CND_tail_IK_Stretch.outColorR", "BLD_tail_IK_Stretch.color1R")
    cmds.connectAttr("tail_IK_Ctrl.Stretch", "BLD_tail_IK_Stretch.blender")
    cmds.connectAttr("BLD_tail_IK_Stretch.outputR", "up_tail_IK_jnt.scaleX")
    cmds.connectAttr("BLD_tail_IK_Stretch.outputR", "mid_tail_IK_jnt.scaleX")
    cmds.setAttr("tail_IK_Ctrl.Stretch",1)
    
    
    cmds.parentConstraint("up_tail_FK_Ctrl", "up_tail_FK_jnt", mo=True)
    cmds.scaleConstraint("up_tail_FK_Ctrl", "up_tail_FK_jnt", mo=True)
    cmds.parentConstraint("mid_tail_FK_Ctrl", "mid_tail_FK_jnt", mo=True)
    cmds.scaleConstraint("mid_tail_FK_Ctrl", "mid_tail_FK_jnt", mo=True)
    cmds.parentConstraint("low_tail_FK_Ctrl", "low_tail_FK_jnt", mo=True)
    cmds.scaleConstraint("low_tail_FK_Ctrl", "low_tail_FK_jnt", mo=True)
    cmds.parentConstraint("low_tail_FK_jnt", "low_tail_foot_FK_jnt", mo=True)
    cmds.scaleConstraint("low_tail_FK_jnt", "low_tail_foot_FK_jnt", mo=True)
    
    cmds.parent("low_tail_FK_Ctrl_offset","mid_tail_FK_Ctrl")
    cmds.parent("mid_tail_FK_Ctrl_offset","up_tail_FK_Ctrl")
    
    cmds.group("tail_IK_constraint_Ctrl_offset","up_tail_IK_Ctrl_offset","tail_IK_poleVector_offset", n = "GRP_Tail_IK_Controls")
    cmds.group("up_tail_FK_Ctrl_offset", n = "GRP_Tail_FK_Controls")
    cmds.connectAttr("tail_Ctrl.SwitchIKFK", "GRP_Tail_IK_Controls.visibility")
    cmds.connectAttr("Rev_tail_foot_jnt_IKFK.outputX", "GRP_Tail_FK_Controls.visibility")
    cmds.group("up_tail_IK_jnt","low_tail_foot_IK_jnt", n = "GRP_Tail_IK_joints")
    cmds.group("up_tail_FK_jnt","low_tail_foot_FK_jnt", n = "GRP_Tail_FK_joints")
    cmds.group("GRP_Tail_IK_joints", "GRP_Tail_FK_joints", "up_tail_jnt", "low_tail_foot_jnt", n = "GRP_Tail_Joints")
    cmds.group("GRP_Tail_FK_Controls","GRP_Tail_IK_Controls", "tail_Ctrl_offset", n= "GRP_Tail_Controls")
    cmds.group("DIS_Tail_IK_Stretch", "LOC_01_Tail_IK_Stretch_ScaleFix", "LOC_02_Tail_IK_Stretch_ScaleFix", "DIS_Tail_IK_Stretch_ScaleFix", n = "GRP_Tail_utilities")
    cmds.setAttr("GRP_Tail_Joints.visibility",1)
    cmds.setAttr("GRP_Tail_utilities.visibility",0)
    cmds.group("GRP_Tail_Joints", "GRP_Tail_utilities", "GRP_Tail_Controls", n = "RIG_Tail_Grp")
    cmds.delete("Up", "Mid", "Low", "End")
    cmds.parentConstraint("end_tail_foot_jnt", "tail_Ctrl_offset", mo = True)
    cmds.scaleConstraint("end_tail_foot_jnt", "tail_Ctrl_offset", mo = True)
    
    cmds.sets("up_tail_jnt","mid_tail_jnt","low_tail_jnt", n = "Bendy_Jnt_Selection")
    cmds.sets("up_tail_FK_Ctrl","mid_tail_FK_Ctrl","low_tail_FK_Ctrl","tail_IK_constraint_Ctrl", "tail_IK_Ctrl","up_tail_IK_Ctrl", "tail_IK_poleVector", "tail_Ctrl", n = "Tail_Controls")


def AutoTailRigEnd():
    
    cmds.undoInfo(openChunk = True)
    
    cmds.addAttr( "tail_IK_poleVector", ln= "Follow", at="enum", en= "World:Transform:Cog:Hip:IKCtrl", k=True)
    cmds.select(clear=True)
    cmds.circle(n = "World_Space_tail_C")
    cmds.select("World_Space_tail_C")
    cmds.group(n = "World_Space_tail")
    cmds.delete("World_Space_tail_C")
    cmds.select(clear=True)
    cmds.parent("World_Space_tail", "GRP_Tail_utilities")
    cmds.parentConstraint("hip_Ctrl", "up_tail_IK_Ctrl_offset",mo=True)
    cmds.scaleConstraint("hip_Ctrl", "up_tail_IK_Ctrl_offset",mo=True)
    cmds.parentConstraint("hip_Ctrl", "up_tail_FK_Ctrl_offset",mo=True)
    cmds.scaleConstraint("hip_Ctrl", "up_tail_FK_Ctrl_offset",mo=True)
    cmds.parentConstraint("World_Space_tail", "Transform_Null", "cog_Ctrl", "hip_Ctrl", "tail_IK_constraint_Ctrl_offset", mo=True)
    cmds.scaleConstraint("hip_Ctrl", "tail_IK_constraint_Ctrl_offset",mo=True)
    cmds.parentConstraint("World_Space_tail", "Transform_Null", "cog_Ctrl", "hip_Ctrl", "tail_IK_Ctrl", "tail_IK_poleVector_offset", mo=True)
    cmds.scaleConstraint("hip_Ctrl", "tail_IK_poleVector_offset",mo=True)
    
    

    cmds.createNode("condition", n= "CND_Follow_IKCtrlPoleVector_World")
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_World.colorIfTrueR", 1)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_World.colorIfTrueG", 1)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_World.colorIfTrueB", 1)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_World.colorIfFalseR", 0)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_World.colorIfFalseG", 0)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_World.colorIfFalseB", 0)
    
    cmds.createNode("condition", n= "CND_Follow_IKCtrlPoleVector_Transform")
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_Transform.colorIfTrueR", 1)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_Transform.colorIfTrueG", 1)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_Transform.colorIfTrueB", 1)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_Transform.colorIfFalseR", 0)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_Transform.colorIfFalseG", 0)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_Transform.colorIfFalseB", 0)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_Transform.secondTerm", 1)
    
    cmds.createNode("condition", n= "CND_Follow_IKCtrlPoleVector_Cog")
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_Cog.colorIfTrueR", 1)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_Cog.colorIfTrueG", 1)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_Cog.colorIfTrueB", 1)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_Cog.colorIfFalseR", 0)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_Cog.colorIfFalseG", 0)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_Cog.colorIfFalseB", 0)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_Cog.secondTerm", 2)
    
    cmds.createNode("condition", n= "CND_Follow_IKCtrlPoleVector_Hip")
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_Hip.colorIfTrueR", 1)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_Hip.colorIfTrueG", 1)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_Hip.colorIfTrueB", 1)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_Hip.colorIfFalseR", 0)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_Hip.colorIfFalseG", 0)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_Hip.colorIfFalseB", 0)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_Hip.secondTerm", 3)
    
    cmds.createNode("condition", n= "CND_Follow_IKCtrlPoleVector_IKCtrl")
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_IKCtrl.colorIfTrueR", 1)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_IKCtrl.colorIfTrueG", 1)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_IKCtrl.colorIfTrueB", 1)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_IKCtrl.colorIfFalseR", 0)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_IKCtrl.colorIfFalseG", 0)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_IKCtrl.colorIfFalseB", 0)
    cmds.setAttr("CND_Follow_IKCtrlPoleVector_IKCtrl.secondTerm", 4)
    
    
    cmds.createNode("condition", n= "CND_Follow_IKCtrl_World")
    cmds.setAttr("CND_Follow_IKCtrl_World.colorIfTrueR", 1)
    cmds.setAttr("CND_Follow_IKCtrl_World.colorIfTrueG", 1)
    cmds.setAttr("CND_Follow_IKCtrl_World.colorIfTrueB", 1)
    cmds.setAttr("CND_Follow_IKCtrl_World.colorIfFalseR", 0)
    cmds.setAttr("CND_Follow_IKCtrl_World.colorIfFalseG", 0)
    cmds.setAttr("CND_Follow_IKCtrl_World.colorIfFalseB", 0)
    
    cmds.createNode("condition", n= "CND_Follow_IKCtrl_Transform")
    cmds.setAttr("CND_Follow_IKCtrl_Transform.colorIfTrueR", 1)
    cmds.setAttr("CND_Follow_IKCtrl_Transform.colorIfTrueG", 1)
    cmds.setAttr("CND_Follow_IKCtrl_Transform.colorIfTrueB", 1)
    cmds.setAttr("CND_Follow_IKCtrl_Transform.colorIfFalseR", 0)
    cmds.setAttr("CND_Follow_IKCtrl_Transform.colorIfFalseG", 0)
    cmds.setAttr("CND_Follow_IKCtrl_Transform.colorIfFalseB", 0)
    cmds.setAttr("CND_Follow_IKCtrl_Transform.secondTerm", 1)
    
    cmds.createNode("condition", n= "CND_Follow_IKCtrl_Cog")
    cmds.setAttr("CND_Follow_IKCtrl_Cog.colorIfTrueR", 1)
    cmds.setAttr("CND_Follow_IKCtrl_Cog.colorIfTrueG", 1)
    cmds.setAttr("CND_Follow_IKCtrl_Cog.colorIfTrueB", 1)
    cmds.setAttr("CND_Follow_IKCtrl_Cog.colorIfFalseR", 0)
    cmds.setAttr("CND_Follow_IKCtrl_Cog.colorIfFalseG", 0)
    cmds.setAttr("CND_Follow_IKCtrl_Cog.colorIfFalseB", 0)
    cmds.setAttr("CND_Follow_IKCtrl_Cog.secondTerm", 2)
    
    cmds.createNode("condition", n= "CND_Follow_IKCtrl_Hip")
    cmds.setAttr("CND_Follow_IKCtrl_Hip.colorIfTrueR", 1)
    cmds.setAttr("CND_Follow_IKCtrl_Hip.colorIfTrueG", 1)
    cmds.setAttr("CND_Follow_IKCtrl_Hip.colorIfTrueB", 1)
    cmds.setAttr("CND_Follow_IKCtrl_Hip.colorIfFalseR", 0)
    cmds.setAttr("CND_Follow_IKCtrl_Hip.colorIfFalseG", 0)
    cmds.setAttr("CND_Follow_IKCtrl_Hip.colorIfFalseB", 0)
    cmds.setAttr("CND_Follow_IKCtrl_Hip.secondTerm", 3)
    
    cmds.connectAttr( "tail_IK_poleVector.Follow", "CND_Follow_IKCtrlPoleVector_World.firstTerm")
    cmds.connectAttr( "tail_IK_poleVector.Follow", "CND_Follow_IKCtrlPoleVector_Transform.firstTerm")
    cmds.connectAttr( "tail_IK_poleVector.Follow", "CND_Follow_IKCtrlPoleVector_Cog.firstTerm")
    cmds.connectAttr( "tail_IK_poleVector.Follow", "CND_Follow_IKCtrlPoleVector_Hip.firstTerm")
    cmds.connectAttr( "tail_IK_poleVector.Follow", "CND_Follow_IKCtrlPoleVector_IKCtrl.firstTerm")
    
    cmds.connectAttr( "CND_Follow_IKCtrlPoleVector_World.outColorR", "tail_IK_poleVector_offset_parentConstraint1.World_Space_tailW0")
    cmds.connectAttr( "CND_Follow_IKCtrlPoleVector_Transform.outColorR", "tail_IK_poleVector_offset_parentConstraint1.Transform_NullW1")
    cmds.connectAttr( "CND_Follow_IKCtrlPoleVector_Cog.outColorR", "tail_IK_poleVector_offset_parentConstraint1.cog_CtrlW2")
    cmds.connectAttr( "CND_Follow_IKCtrlPoleVector_Hip.outColorR", "tail_IK_poleVector_offset_parentConstraint1.hip_CtrlW3")
    cmds.connectAttr( "CND_Follow_IKCtrlPoleVector_IKCtrl.outColorR", "tail_IK_poleVector_offset_parentConstraint1.tail_IK_CtrlW4")
    cmds.setAttr("tail_IK_poleVector.Follow", 4)
    
    
    cmds.connectAttr( "tail_IK_Ctrl.Follow", "CND_Follow_IKCtrl_World.firstTerm")
    cmds.connectAttr( "tail_IK_Ctrl.Follow", "CND_Follow_IKCtrl_Transform.firstTerm")
    cmds.connectAttr( "tail_IK_Ctrl.Follow", "CND_Follow_IKCtrl_Cog.firstTerm")
    cmds.connectAttr( "tail_IK_Ctrl.Follow", "CND_Follow_IKCtrl_Hip.firstTerm")
    
    cmds.connectAttr( "CND_Follow_IKCtrl_World.outColorR", "tail_IK_constraint_Ctrl_offset_parentConstraint1.World_Space_tailW0")
    cmds.connectAttr( "CND_Follow_IKCtrl_Transform.outColorR", "tail_IK_constraint_Ctrl_offset_parentConstraint1.Transform_NullW1")
    cmds.connectAttr( "CND_Follow_IKCtrl_Cog.outColorR", "tail_IK_constraint_Ctrl_offset_parentConstraint1.cog_CtrlW2")
    cmds.connectAttr( "CND_Follow_IKCtrl_Hip.outColorR", "tail_IK_constraint_Ctrl_offset_parentConstraint1.hip_CtrlW3")
    cmds.setAttr("tail_IK_Ctrl.Follow", 1)
    
    cmds.setAttr("GRP_Tail_Joints.visibility", 0)
    
    cmds.setAttr("low_tail_foot_jnt.segmentScaleCompensate",0)
    cmds.setAttr("end_tail_foot_jnt.segmentScaleCompensate",0)
    cmds.parentConstraint("Transform_Null", "LOC_01_Tail_IK_Stretch_ScaleFix", mo = True)
    cmds.parentConstraint("Transform_Null", "LOC_02_Tail_IK_Stretch_ScaleFix", mo = True)
    cmds.scaleConstraint("Transform_Null", "LOC_01_Tail_IK_Stretch_ScaleFix", mo = True)
    cmds.scaleConstraint("Transform_Null", "LOC_02_Tail_IK_Stretch_ScaleFix", mo = True)
    cmds.scaleConstraint("Transform_Null", "World_Space_tail", mo = True)
    cmds.parentConstraint("Transform_Null", "GRP_Tail_IK_joints", mo = True)
    cmds.scaleConstraint("Transform_Null", "GRP_Tail_IK_joints", mo = True)
    cmds.setAttr("up_tail_FK_jnt.segmentScaleCompensate",0)
    cmds.setAttr("mid_tail_FK_jnt.segmentScaleCompensate",0)
    cmds.setAttr("low_tail_FK_jnt.segmentScaleCompensate",0)
    
    cmds.undoInfo(closeChunk = True)

def createWin():
    if cmds.window("Auto" , query=True, exists=True):
       cmds.deleteUI("Auto" , window = True)
       
    cmds.window("Auto", h = 800, w = 500)
    cmds.columnLayout(columnAttach=('both', 2), rowSpacing=5, columnWidth=400 )
    cmds.rowColumnLayout(nc = 2)
    cmds.button("Create Locators", c = "CreateLOC()", w = 200, h = 100 )
    cmds.button("Create Joints", c = "CreateJoints()", w = 200, h = 100 )
    cmds.showWindow("Auto")
    cmds.setParent('..')
    
    
    cmds.separator(style='in',height=10) 
    cmds.button("Auto Tail Rig", c='AutoTailRig()',w = 200, h = 100)
    cmds.separator(style='in',height=10) 
    cmds.button("Connect Tail to Body", c='AutoTailRigEnd()',w = 200, h = 100)
    
       
       
       
createWin()