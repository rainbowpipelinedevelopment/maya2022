import maya.cmds as cmds

def listBS(type):
    bs=[]
    ib=[]
    a=cmds.filterExpand(cmds.listRelatives(str(type), ad=True, pa=True),sm=12, ex=True)
    try:
        for i in a:
            for n in range(1,9):
                if ("_0"+str(n) in i):
                    if i not in ib:
                        ib.append(i)
            if (i not in ib) and (i not in bs):
                bs.append(i)
    except:
        pass
    return bs, ib

def createBS(msh,bList,name):
    b=cmds.blendShape(msh,n=name,o="local")
    print("\nBS node " + name + " has been created")
    if len(bList)>0:
        print("\nTarget found for mesh "+str(msh)+":\n")
        n=0
        for i in bList:
            check=True
            try:
                cmds.blendShape( b, edit=True, t=(msh, n, i, 1.0) )
            except:
                check=False
            if (check==True):
                print(i)
                n=n+1
        allTargets=cmds.blendShape(b, q=True, t=True)
        for target in allTargets:
            cmds.hide(target)
    else:
        print("\nNo target found for mesh "+str(msh)+".\n")
    return b

def getTarget(ib,bsList):
    n=ib.strip("_0123456789")
    for i in range(len(bsList)):
        if bsList[i] == n:
            return i

def getWeight(ib,bs):
    all=getIBidx(ib,bs)
    num=all.index(ib)+1
    div=round(1.0/(len(all)+1),2)
    w=div*num
    return w

def getIBidx(ib,bs):
    all=[]
    n=ib.strip("_0123456789")
    for i in bs:
        if n in i:
            all.append(i)
    if len(all)>1:
        order=False
        while not order:
            order=True
            for i in range(len(all)-1):
                item1=all[i]
                item2=all[i+1]
                num1=float(item1[-1])
                num2=float(item2[-1])
                if num1>num2:
                    idx=all.index(item1)
                    c=all[idx+1]
                    all[idx+1]=all[idx]
                    all[idx]=c
                    order=False
    return all

def renameIB(ib):
    for i in ib:
        if i[-1] not in ["1","2","3","4","5","6","7","8","9"]:
            parts=i.split("_")
            newname=i.replace("_"+parts[-2],"")+"_"+parts[-2]
            idx = ib.index(i)
            i=cmds.rename(i,newname)
            ib[idx]=newname
    return ib
    
def createIB(node,bs,ib,base):
    for i in ib:
        t=getTarget(i,bs)
        w=getWeight(i,ib)
        print("Adding ib "+i+" to target index " +str(t)+ " at weight "+str(w))
        cmds.blendShape( node, edit=True, ib=True, t=(base, t, i, w))
        cmds.hide(i)

def operate(*args):
    meshes=queryList(mshs)
    groups=queryList(grps)
    len_msh=len(meshes)
    len_grp=len(groups)
    type=["body","eyelashes","upperTeeth","lowerTeeth","eyebrows","tongue","iris","bulb","specular"]
    if (len_msh!=len_grp):
        print("Please select the same number of meshes and groups.")
    else:
        print("ok")
        for i in range(len_msh-1):
            print("\n----------------------------------------------------------------")
            msh=meshes[i]
            grp=groups[i]
            print("\nCREATING BS FOR MESH " + msh)
            bsList=listBS(grp)[0]

            flag=False
            for t in type:
                if t in str(msh):
                    name="BS_"+t.capitalize()+"BS"
                    flag=True
            if flag==False:
                name=str(msh).replace("MSH","BS")+"BS"
            bsNode=createBS(msh,bsList,name)
            
            ibList=renameIB(listBS(grp)[1])
            if len(ibList)>0:
                print("\nIn Betweens found for mesh "+str(msh)+":\n")
                createIB(bsNode,bsList,ibList,msh)
    return

def addToMshList(*args):
    sel=cmds.ls(sl=True,tr=True)  
    sel=cmds.filterExpand(sel, sm=12, ex=True)
    cmds.scrollField(mshs, e=True, cl=True)
    for i in sel:
        cmds.scrollField(mshs, e=True, it=str(i)+"\n")
    return

def addToGrpList(*args):
    sel=cmds.ls(sl=True,tr=True)
    cmds.scrollField(grps, e=True, cl=True)
    for i in sel:
        cmds.scrollField(grps, e=True, it=str(i)+"\n")
    return

def queryList(a):
    t=cmds.scrollField(a, q=True, text=True)
    list=t.split("\n")
    return list

def createWindow():
    global mshs, grps
    if (cmds.window("bsAttach", q=True,exists=True)):
        cmds.deleteUI("bsAttach", window=True)
    W=cmds.window("bsAttach",title="BS Attach UI", ip=True, rtf=True)
    
    C=cmds.columnLayout(adjustableColumn=True, rowSpacing=10, p=W)
    
    R=cmds.rowLayout(numberOfColumns=2,p=C)
    C0=cmds.columnLayout(adjustableColumn=True, rowSpacing=10, p=R)
    cmds.text("Select all base bs meshes:",p=C0)
    mshs=cmds.scrollField( editable=False, wordWrap=True, text='Select the meshes and press the button below', h=150,p=C0 )
    cmds.button(label="Add Meshes", command=addToMshList, p=C0, align="left", w=150)
    
    C1=cmds.columnLayout(adjustableColumn=True, rowSpacing=10, p=R)
    cmds.text("Select all bs groups (same order as meshes):",p=C1)
    grps=cmds.scrollField( editable=False, wordWrap=True, text='Select the groups and press the button below', h=150, p=C1 )
    cmds.button(label="Add Groups", command=addToGrpList, p=C1, align="left", w=150)
    
    R1=cmds.rowLayout(numberOfColumns=1,p=C)
    C2=cmds.columnLayout(adjustableColumn=True, rowSpacing=10, p=R1)
    cmds.button(label="Attach BS", command=operate, p=C2, align="left", w=515)
    
    cmds.showWindow( W )
    return