"""
Descrizione: 
    Questo script crea una diretta di controlli sui joints selezionati, 
    con due attributi follow di translate e rotate ad ogni controllo.

Autore: 
    Salvatore Iamonte

Versione: 
    1.0.0

Uso:
    import FK_Follow_Trans_Rot
    reload(FK_Follow_Trans_Rot)
    FK_Follow_Trans_Rot.create_FK_UI()

"""

from maya import cmds

#utility funtions

def align(x, y):
    
    T = cmds.xform(x, ws=True, q=True, rp=True)
    R = cmds.xform(x, ws=True, q=True, ro=True)
    cmds.xform(y, ws=True, t=T)
    cmds.xform(y, ws=True, ro=R)

def create_control(name):

    name = name.replace("_Jnt", "")
    name = name.replace("JNT_", "")
    name = name.replace("_jnt", "")
    name = name.replace("jnt_", "")
    name = name.replace("Jnt_", "")
    name = name.replace("_JNT", "")
    ctrl = cmds.circle(ch=False, nr=(1,0,0), n=name + "_Ctrl") [0]
    ctrl_grp = cmds.group(ctrl, n=name + "_Ctrl_Grp")
    shape = cmds.pickWalk(ctrl, d="down")[0]
    cmds.setAttr(ctrl + ".v", k=False)
    cmds.setAttr(shape + ".overrideEnabled", 1)
    cmds.setAttr(shape + ".overrideColor", 6)
    cmds.addAttr(ctrl, ln="Follow_Translate", k=True, min=0, max=1, dv=1)
    cmds.addAttr(ctrl, ln="Follow_Rotate", k=True, min=0, max=1, dv=1)
    return ctrl_grp, ctrl

#####################################################################################################

def create_FK(*args):

    master = cmds.textField("Master_Ctrl", q=True, tx=True)
    attach = cmds.textField("Attach_Ctrl", q=True, tx=True)

    if cmds.ls(sl=True, typ="joint") and master:

        joints = cmds.ls(sl=True, typ="joint")

        #creo i controlli e faccio i constrain ad ogni joint

        i = 0
        while i < len(joints):

            cmds.setAttr(joints[i]+".segmentScaleCompensate", 0)
            ctrl = create_control(joints[i])
            align(joints[i], ctrl)
            cmds.parentConstraint(ctrl[1], joints[i], mo=True)
            cmds.scaleConstraint(ctrl[1], joints[i], mo=True)

            #verifico che si agisca sulla creazione del primo controllo o dei successivi 

            if i > 0:

                #faccio i constrain ai controlli per far funzionare la diretta

                par_constr_trans = cmds.parentConstraint(master, joints[i-1], ctrl[0], sr=["x","y","z"], mo=True)
                par_constr_rot = cmds.parentConstraint(master, joints[i-1], ctrl[0], st=["x","y","z"], mo=True)
                cmds.scaleConstraint(joints[i-1], ctrl[0], mo=True)
                cmds.setAttr(par_constr_trans[0] + ".interpType", 0)
                cmds.setAttr(par_constr_rot[0] + ".interpType", 0)

                #connetto gli attributi dei follow ai constrain

                rev = cmds.createNode("reverse", n="REV_Follow_"+str(ctrl[1]), ss=True)
                cmds.connectAttr(ctrl[1]+".Follow_Translate", rev+".inputX")
                cmds.connectAttr(ctrl[1]+".Follow_Rotate", rev+".inputY")
                cmds.connectAttr(rev+".outputX", par_constr_trans[0]+"."+master+"W0")
                cmds.connectAttr(rev+".outputY", par_constr_rot[0]+"."+master+"W0")
                cmds.connectAttr(ctrl[1]+".Follow_Translate", par_constr_trans[0]+"."+joints[i-1]+"W1")
                cmds.connectAttr(ctrl[1]+".Follow_Rotate", par_constr_rot[0]+"."+joints[i-1]+"W1")

            else:

                #faccio i constrain ai controlli per far funzionare la diretta

                par_constr_trans = cmds.parentConstraint(master, attach, ctrl[0], sr=["x","y","z"], mo=True)
                par_constr_rot = cmds.parentConstraint(master, attach, ctrl[0], st=["x","y","z"], mo=True)
                cmds.scaleConstraint(attach, ctrl[0], mo=True)
                cmds.setAttr(par_constr_trans[0] + ".interpType", 0)
                cmds.setAttr(par_constr_rot[0] + ".interpType", 0)

                #connetto gli attributi dei follow ai constrain

                rev = cmds.createNode("reverse", n="REV_Follow_"+str(ctrl[1]), ss=True)
                cmds.connectAttr(ctrl[1]+".Follow_Translate", rev+".inputX")
                cmds.connectAttr(ctrl[1]+".Follow_Rotate", rev+".inputY")
                cmds.connectAttr(rev+".outputX", par_constr_trans[0]+"."+master+"W0")
                cmds.connectAttr(rev+".outputY", par_constr_rot[0]+"."+master+"W0")
                cmds.connectAttr(ctrl[1]+".Follow_Translate", par_constr_trans[0]+"."+attach+"W1")
                cmds.connectAttr(ctrl[1]+".Follow_Rotate", par_constr_rot[0]+"."+attach+"W1")


            i += 1
            
            cmds.select(cl=True)

    else:

        cmds.warning("Select joints or fill the field")

#####################################################################################################

def create_FK_UI():
    
    name = "Create_FK_Follow_Trans_Rot"
    if cmds.window(name, ex=True, wh=(300, 200)):
        cmds.deleteUI(name)

    window = cmds.window(name, t="WHISKERS Rig UI")
    cmds.columnLayout(adj=True, w=300, h=200)
    cmds.text(l="Select Master Control", h=20)
    test = cmds.textField("Master_Ctrl")
    cmds.button(l="^^^", c=select_master)
    cmds.separator(h=10)
    cmds.text(l="Select Attach Control", h=20)
    test = cmds.textField("Attach_Ctrl")
    cmds.button(l="^^^", c=select_attach)
    cmds.separator(h=10)
    cmds.text(l="Select Joints", h=20)
    cmds.button(l="Create Whisker Chain", c=create_FK)

    cmds.showWindow(name)

def select_master(*args):

    sel = cmds.ls(sl=True)

    if len(sel) == 1:
        cmds.textField("Master_Ctrl", e=True, tx=sel[0])
    else:
        cmds.warning("Select one control")

def select_attach(*args):

    sel = cmds.ls(sl=True)

    if len(sel) == 1:
        cmds.textField("Attach_Ctrl", e=True, tx=sel[0])
    else:
        cmds.warning("Select one control")