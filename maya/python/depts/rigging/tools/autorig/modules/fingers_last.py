#//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#FINGERS_LAST
#seleziona tutti i gruppi base
#//////////////////////////////////////////////////////////////////////////////////////////////////////////////

import maya.cmds as cmds
import maya.mel as mel
from maya import cmds , OpenMaya

def fingersLast():
    grpFingers = cmds.ls(sl=1)
    
    if len(grpFingers) == 0 :
        print ("----------------------------------------------------------------------")
        print ("WARNING: You must select at least one control hand to run this script!")
        print ("----------------------------------------------------------------------")
        cmds.confirmDialog(message = "You must select at least one control hand to run this script!", button=['OK'], title='Warning', icn = 'warning', bgc = (1, 0.7, 0) )
    
    else:
        #print grpFingers[0]
        checkSide = grpFingers[0].find( "lf")
        #print checkSide
        
        if checkSide > 0 :
            cmds.group( grpFingers, n="RIG_lf_hand_fingers_grp"  )
            cmds.xform( "RIG_lf_hand_fingers_grp", rp=(0, 0, 0), sp=(0, 0, 0) )
            cmds.select( "RIG_lf_hand_ctrl" )
            cmds.addAttr( ln="master_fist", nn="Master Fist", at="float", dv=0, max=100, min=-100, k=True )
            cmds.addAttr( ln="master_spread", nn="Master Spread", at="float", dv=0, max=100, min=-100, k=True )
            cmds.addAttr( ln="master_cup", nn="Master Cup", at="float", dv=0, max=100, min=-100, k=True )
            
            for i in grpFingers:
                listGrp = cmds.listRelatives( i, ad=True, typ='transform' )
                #print listGrp
                                
                #print listGrp[0]
                checkFingerName = listGrp[0].find( "thumb")
                #print checkFingerName
                
                if checkFingerName > 0 :
                    cmds.shadingNode( "multiplyDivide", au=True, n= "MLP_" + listGrp[10] + "_Spread" )
                    cmds.shadingNode( "multiplyDivide", au=True, n= "MLP_" + listGrp[10] + "_Fist" )
                                        
                    cmds.connectAttr( "RIG_lf_hand_ctrl.master_spread", "MLP_" + listGrp[10] + "_Spread.input1X" )
                    cmds.connectAttr( "MLP_" + listGrp[10] + "_Spread.outputX", listGrp[10] + ".rotateY" )
                    
                    cmds.connectAttr( "RIG_lf_hand_ctrl.master_fist", "MLP_" + listGrp[10] + "_Fist.input1Z" )
                    cmds.connectAttr( "MLP_" + listGrp[10] + "_Fist.outputZ", listGrp[10] + ".rotateZ" )
                    cmds.connectAttr( "MLP_" + listGrp[10] + "_Fist.outputZ", listGrp[6] + ".rotateZ" )
                    cmds.connectAttr( "MLP_" + listGrp[10] + "_Fist.outputZ", listGrp[2] + ".rotateZ" )
                    cmds.setAttr( "MLP_" + listGrp[10] + "_Fist.input2Z", 1.5 )
                    
                else:
                    cmds.shadingNode( "multiplyDivide", au=True, n= "MLP_" + listGrp[10] + "_Spread" )
                    cmds.shadingNode( "multiplyDivide", au=True, n= "MLP_" + listGrp[10] + "_Fist" )
                    
                    cmds.connectAttr( "RIG_lf_hand_ctrl.master_spread", "MLP_" + listGrp[10] + "_Spread.input1X" )
                    cmds.connectAttr( "MLP_" + listGrp[10] + "_Spread.outputX", listGrp[10] + ".rotateZ" )
                    
                    cmds.connectAttr( "RIG_lf_hand_ctrl.master_fist", "MLP_" + listGrp[10] + "_Fist.input1Y" )
                    cmds.connectAttr( "MLP_" + listGrp[10] + "_Fist.outputY", listGrp[10] + ".rotateY" )
                    cmds.connectAttr( "MLP_" + listGrp[10] + "_Fist.outputY", listGrp[6] + ".rotateY" )
                    cmds.connectAttr( "MLP_" + listGrp[10] + "_Fist.outputY", listGrp[2] + ".rotateY" )
                    cmds.setAttr( "MLP_" + listGrp[10] + "_Fist.input2Z", 1.5 )
                    
                    #print listGrp[0]
                    checkRing = listGrp[0].find( "ring")
                    #print checkRing
                
                    if checkRing > 0 :
                        #print( "Esiste Ring" )
                        cmds.shadingNode( "multiplyDivide", au=True, n= "MLP_" + listGrp[13] + "_Cup" )
                        cmds.connectAttr( "RIG_lf_hand_ctrl.master_cup", "MLP_" + listGrp[13] + "_Cup.input1Y" )
                        cmds.connectAttr( "MLP_" + listGrp[13] + "_Cup.outputY", listGrp[13] + ".rotateY" )
                        
                    else:
                        #print( "Ring Finger lf doesn't exist!" )
                        cmds.select(cl=True)
                        
                    #print listGrp[0]
                    checkPinky = listGrp[0].find( "pinky")
                    #print checkPinky
                    
                    if checkPinky > 0 :
                        #print( "Esiste Pinky" )
                        cmds.shadingNode( "multiplyDivide", au=True, n= "MLP_" + listGrp[13] + "_Cup" )
                        cmds.connectAttr( "RIG_lf_hand_ctrl.master_cup", "MLP_" + listGrp[13] + "_Cup.input1Y" )
                        cmds.connectAttr( "MLP_" + listGrp[13] + "_Cup.outputY", listGrp[13] + ".rotateY" )
                    
                    else:
                        #print( "Pinky Finger lf doesn't exist!" )
                        cmds.select(cl=True)

        else:
            cmds.group( grpFingers, n="RIG_rt_hand_fingers_grp"  )
            cmds.xform( "RIG_rt_hand_fingers_grp", rp=(0, 0, 0), sp=(0, 0, 0) )
            cmds.select( "RIG_rt_hand_ctrl" )
            cmds.addAttr( ln="master_fist", nn="Master Fist", at="float", dv=0, max=100, min=-100, k=True )
            cmds.addAttr( ln="master_spread", nn="Master Spread", at="float", dv=0, max=100, min=-100, k=True )
            cmds.addAttr( ln="master_cup", nn="Master Cup", at="float", dv=0, max=100, min=-100, k=True )
            
            for i in grpFingers:
                listGrp = cmds.listRelatives( i, ad=True, typ='transform' )
                #print listGrp
                                
                #print listGrp[0]
                checkFingerName = listGrp[0].find( "thumb")
                #print checkFingerName
                
                if checkFingerName > 0 :
                    cmds.shadingNode( "multiplyDivide", au=True, n= "MLP_" + listGrp[10] + "_Spread" )
                    cmds.shadingNode( "multiplyDivide", au=True, n= "MLP_" + listGrp[10] + "_Fist" )
                                        
                    cmds.connectAttr( "RIG_rt_hand_ctrl.master_spread", "MLP_" + listGrp[10] + "_Spread.input1X" )
                    cmds.connectAttr( "MLP_" + listGrp[10] + "_Spread.outputX", listGrp[10] + ".rotateY" )
                    
                    cmds.connectAttr( "RIG_rt_hand_ctrl.master_fist", "MLP_" + listGrp[10] + "_Fist.input1Z" )
                    cmds.connectAttr( "MLP_" + listGrp[10] + "_Fist.outputZ", listGrp[10] + ".rotateZ" )
                    cmds.connectAttr( "MLP_" + listGrp[10] + "_Fist.outputZ", listGrp[6] + ".rotateZ" )
                    cmds.connectAttr( "MLP_" + listGrp[10] + "_Fist.outputZ", listGrp[2] + ".rotateZ" )
                    cmds.setAttr( "MLP_" + listGrp[10] + "_Fist.input2Z", 1.5 )
                    
                else:
                    cmds.shadingNode( "multiplyDivide", au=True, n= "MLP_" + listGrp[10] + "_Spread" )
                    cmds.shadingNode( "multiplyDivide", au=True, n= "MLP_" + listGrp[10] + "_Fist" )
                    
                    cmds.connectAttr( "RIG_rt_hand_ctrl.master_spread", "MLP_" + listGrp[10] + "_Spread.input1X" )
                    cmds.connectAttr( "MLP_" + listGrp[10] + "_Spread.outputX", listGrp[10] + ".rotateZ" )
                    
                    cmds.connectAttr( "RIG_rt_hand_ctrl.master_fist", "MLP_" + listGrp[10] + "_Fist.input1Y" )
                    cmds.connectAttr( "MLP_" + listGrp[10] + "_Fist.outputY", listGrp[10] + ".rotateY" )
                    cmds.connectAttr( "MLP_" + listGrp[10] + "_Fist.outputY", listGrp[6] + ".rotateY" )
                    cmds.connectAttr( "MLP_" + listGrp[10] + "_Fist.outputY", listGrp[2] + ".rotateY" )
                    cmds.setAttr( "MLP_" + listGrp[10] + "_Fist.input2Z", 1.5 )
                    
                    #print listGrp[0]
                    checkRing = listGrp[0].find( "ring")
                    #print checkRing
                
                    if checkRing > 0 :
                        #print( "Esiste Ring" )
                        cmds.shadingNode( "multiplyDivide", au=True, n= "MLP_" + listGrp[13] + "_Cup" )
                        cmds.connectAttr( "RIG_rt_hand_ctrl.master_cup", "MLP_" + listGrp[13] + "_Cup.input1Y" )
                        cmds.connectAttr( "MLP_" + listGrp[13] + "_Cup.outputY", listGrp[13] + ".rotateY" )
                        
                    else:
                        #print( "Ring Finger lf doesn't exist!" )
                        cmds.select(cl=True)
                        
                    #print listGrp[0]
                    checkPinky = listGrp[0].find( "pinky")
                    #print checkPinky
                    
                    if checkPinky > 0 :
                        #print( "Esiste Pinky" )
                        cmds.shadingNode( "multiplyDivide", au=True, n= "MLP_" + listGrp[13] + "_Cup" )
                        cmds.connectAttr( "RIG_rt_hand_ctrl.master_cup", "MLP_" + listGrp[13] + "_Cup.input1Y" )
                        cmds.connectAttr( "MLP_" + listGrp[13] + "_Cup.outputY", listGrp[13] + ".rotateY" )
                    
                    else:
                        #print( "Pinky Finger lf doesn't exist!" )
                        cmds.select(cl=True)

        try:
            cmds.setAttr( "MLP_RIG_lf_indexBase_fk_grp_anchor_Spread.input2X", -1 )
            cmds.setAttr( "MLP_RIG_rt_indexBase_fk_grp_anchor_Spread.input2X", -1 )
        except:
            pass
          
        try:
            cmds.setAttr( "MLP_RIG_lf_middleBase_fk_grp_anchor_Spread.input2X", -.5 )
            cmds.setAttr( "MLP_RIG_rt_middleBase_fk_grp_anchor_Spread.input2X", -.5 )
        except:
            pass
            
        try:
            cmds.setAttr( "MLP_RIG_lf_ringBase_fk_grp_anchor_Spread.input2X", .25 )
            cmds.setAttr( "MLP_RIG_lf_ringTop_fk_grp_offset_Cup.input2Y", .5 )
        except:
            pass
            
        try:
            cmds.setAttr( "MLP_RIG_rt_ringBase_fk_grp_anchor_Spread.input2X", .25 )
            cmds.setAttr( "MLP_RIG_rt_ringTop_fk_grp_offset_Cup.input2Y", .5 )            
        except:
            pass
        
        try:
            cmds.parentConstraint( "RIG_lf_hand_jnt", "RIG_lf_hand_fingers_grp", mo=True )
            cmds.scaleConstraint( "RIG_lf_hand_jnt", "RIG_lf_hand_fingers_grp", mo=True )
        except:
            pass
        
        try:
            cmds.parentConstraint( "RIG_rt_hand_jnt", "RIG_rt_hand_fingers_grp", mo=True )
            cmds.scaleConstraint( "RIG_rt_hand_jnt", "RIG_rt_hand_fingers_grp", mo=True )
        except:
            pass
        
        try:            
            cmds.parent( "RIG_lf_hand_fingers_grp", "RIG_rig_grp" )
        except:
            pass
            
        try:            
            cmds.parent( "RIG_rt_hand_fingers_grp", "RIG_rig_grp" )
        except:
            pass
            
        cmds.select(cl=True)

        print ("---------------------------")
        print ("INFO: Fingers are Finished!")
        print ("---------------------------")
        #cmd.confirmDialog(message = "Fingers are Finalized!", button=['OK'], title='Info', icn = 'information', bgc = (1, 0.7, 0))
    