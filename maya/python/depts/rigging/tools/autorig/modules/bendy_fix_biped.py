import maya.cmds as cmds

def freeScale(obj):
    axis = ['x', 'y', 'z']
    attrs = ['s']
    for ax in axis:
        for attr in attrs:
            cmds.setAttr(obj+'.'+attr+ax, lock=0, k=True)
    return

def main():
    obj=["main_ctrl_const_grp","curve_segment_dnt_grp","Rear_lt_hip1_jnt_segment_1_ikh_grp","Rear_lt_hip1_jnt_ctrl_orientPad","Rear_lt_hip2_jnt_segment_1_ikh_grp","Rear_lt_hip2_jnt_segment_1_curve_baseClusterHandle_grp","Rear_lt_hip2_jnt_segment_1_joint","Rear_lt_hip1_jnt_segment_5_curve_tipClusterHandle_grp","Rear_lt_hip1_jnt_segment_5_ikh","Rear_lt_knee1_jnt_segment_1_ikh_grp","Rear_lt_knee1_jnt_segment_5_ikh_grp","Rear_lt_knee1_jnt_segment_1_curve_baseClusterHandle_grp","Rear_lt_knee1_jnt_segment_1_joint","Rear_lt_hip2_jnt_segment_5_curve_tipClusterHandle_grp","Rear_lt_hip2_jnt_segment_5_ikh","Rear_lt_ankle_jnt_ctrl_orientPad"]
    for i in obj:
        cmds.hide(i)
        if "_lt_" in i:
            cmds.hide(str(i).replace("_lt_","_rt_"))
        if "_ikh_" in i:
            cmds.hide(str(i).replace("_1_","_2_"))
            cmds.hide(str(i).replace("_1_","_3_"))
            cmds.hide(str(i).replace("_1_","_4_"))
            cmds.hide(str(i).replace("_lt_","_rt_").replace("_1_","_2_"))
            cmds.hide(str(i).replace("_lt_","_rt_").replace("_1_","_3_"))
            cmds.hide(str(i).replace("_lt_","_rt_").replace("_1_","_4_"))
    cnts=["Rear_rt_hip1_jnt_bendy_ctrl","Rear_lt_knee1_jnt_ctrl","Rear_lt_knee1_jnt_bendy_ctrl","Rear_lt_hip2_jnt_ctrl","Rear_lt_hip2_jnt_bendy_ctrl","Rear_lt_hip1_jnt_bendy_ctrl","Rear_rt_hip2_jnt_ctrl","Rear_rt_hip2_jnt_bendy_ctrl","Rear_rt_knee1_jnt_ctrl","Rear_rt_knee1_jnt_bendy_ctrl"]
    for i in cnts:
        cmds.setAttr(i+".sx", lock=True, keyable=False, channelBox=False)
        cmds.setAttr(i+".v", lock=True, keyable=False, channelBox=False)
        shp=cmds.pickWalk(i,d="down")[0]
        cmds.setAttr(str(shp)+".overrideEnabled",1)
        cmds.setAttr(str(shp)+".overrideColor",18)
        if "_lt" in str(i):
            n="L_"+str(i).replace("_lt","")
        if "_rt" in str(i):
            n="R_"+str(i).replace("_rt","")
        if "_bendy" not in n:
            n=n.replace("_ctrl","_A_bendy_ctrl")
        else:
            cmds.setAttr(i+".twist", lock=True, keyable=False, channelBox=False)
            if "hip1" not in n:
                n=n.replace("_bendy","_B_bendy")
        n=n.replace("_jnt","").replace("_ctrl","_Ctrl").replace("knee1","knee").replace("_Rear","")
        cmds.rename(i,n)
    
    scCnts = ["L_hip1_bendy_Ctrl","L_hip2_B_bendy_Ctrl","L_knee_B_bendy_Ctrl"]
    for l in scCnts:
        r=l.replace("L_","R_")
        freeScale(l)
        freeScale(r)
        jnt=l.replace("_Ctrl","").replace("L_","lt_").replace("_B_","_").replace("knee","knee1").replace("bendy","jnt_segment")
        l_joints=["Rear_"+jnt+"_1_joint","Rear_"+jnt+"_2_joint","Rear_"+jnt+"_3_joint","Rear_"+jnt+"_4_joint","Rear_"+jnt+"_5_joint","Rear_"+jnt+"_6_joint"]
        for i in l_joints:
            cmds.connectAttr(l+".sy",i+".sy")
            cmds.connectAttr(l+".sz",i+".sz")
            r_joint = i.replace("_lt_","_rt_")
            cmds.connectAttr(r+".sy",r_joint+".sy")
            cmds.connectAttr(r+".sz",r_joint+".sz")