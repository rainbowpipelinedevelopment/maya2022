#//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#FINGERS_AUTORIG
#seleziona tutti i joint di un unico dito senza selezionare il tip
#//////////////////////////////////////////////////////////////////////////////////////////////////////////////

import maya.cmds as cmds
import maya.mel as mel
from maya import cmds , OpenMaya

def fingersAutorig():
    
    try:
        cmds.rename("RIG_lf_thumb_a_jnt", "RIG_lf_thumbBase_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_lf_thumb_b_jnt", "RIG_lf_thumbMid_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_lf_thumb_c_jnt", "RIG_lf_thumbTip_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_lf_thumb_d_jnt", "RIG_lf_thumbEnd_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_thumb_a_jnt", "RIG_rt_thumbBase_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_thumb_b_jnt", "RIG_rt_thumbMid_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_thumb_c_jnt", "RIG_rt_thumbTip_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_thumb_d_jnt", "RIG_rt_thumbEnd_fk_jnt")
    except:
        pass 

    try:
        cmds.rename("RIG_lf_index_a_jnt1", "RIG_lf_indexTop_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_lf_index_a_jnt", "RIG_lf_indexBase_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_lf_index_b_jnt", "RIG_lf_indexMid_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_lf_index_c_jnt", "RIG_lf_indexTip_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_lf_index_d_jnt", "RIG_lf_indexEnd_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_index_a_jnt1", "RIG_rt_indexTop_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_index_a_jnt", "RIG_rt_indexBase_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_index_b_jnt", "RIG_rt_indexMid_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_index_c_jnt", "RIG_rt_indexTip_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_index_d_jnt", "RIG_rt_indexEnd_fk_jnt")
    except:
        pass

    try:
        cmds.rename("RIG_lf_middle_a_jnt1", "RIG_lf_middleTop_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_lf_middle_a_jnt", "RIG_lf_middleBase_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_lf_middle_b_jnt", "RIG_lf_middleMid_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_lf_middle_c_jnt", "RIG_lf_middleTip_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_lf_middle_d_jnt", "RIG_lf_middleEnd_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_middle_a_jnt1", "RIG_rt_middleTop_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_middle_a_jnt", "RIG_rt_middleBase_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_middle_b_jnt", "RIG_rt_middleMid_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_middle_c_jnt", "RIG_rt_middleTip_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_middle_d_jnt", "RIG_rt_middleEnd_fk_jnt")
    except:
        pass

    try:
        cmds.rename("RIG_lf_ringCup_jnt", "RIG_lf_ringTop_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_lf_ring_a_jnt", "RIG_lf_ringBase_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_lf_ring_b_jnt", "RIG_lf_ringMid_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_lf_ring_c_jnt", "RIG_lf_ringTip_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_lf_ring_d_jnt", "RIG_lf_ringEnd_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_ringCup_jnt", "RIG_rt_ringTop_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_ring_a_jnt", "RIG_rt_ringBase_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_ring_b_jnt", "RIG_rt_ringMid_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_ring_c_jnt", "RIG_rt_ringTip_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_ring_d_jnt", "RIG_rt_ringEnd_fk_jnt")
    except:
        pass

    try:
        cmds.rename("RIG_lf_pinkyCup_jnt", "RIG_lf_pinkyTop_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_lf_pinky_a_jnt", "RIG_lf_pinkyBase_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_lf_pinky_b_jnt", "RIG_lf_pinkyMid_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_lf_pinky_c_jnt", "RIG_lf_pinkyTip_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_lf_pinky_d_jnt", "RIG_lf_pinkyEnd_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_pinkyCup_jnt", "RIG_rt_pinkyTop_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_pinky_a_jnt", "RIG_rt_pinkyBase_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_pinky_b_jnt", "RIG_rt_pinkyMid_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_pinky_c_jnt", "RIG_rt_pinkyTip_fk_jnt")
    except:
        pass
    try:
        cmds.rename("RIG_rt_pinky_d_jnt", "RIG_rt_pinkyEnd_fk_jnt")
    except:
        pass    

    jntChain = cmds.ls(sl=1)
    #print jntChain
    if len(jntChain) == 0 :
        print ("----------------------------------------------------------------------")
        print ("WARNING: You must select at least one finger joint to run this script!")
        print ("----------------------------------------------------------------------")
        cmds.confirmDialog(message = "You must select at least one finger joint to run this script!", button=['OK'], title='Warning', icn = 'warning', bgc = (1, 0.7, 0) )
    else:
        
        my_grp = []
        my_cnt = []
        
        for i in range(len(jntChain)):
            #jntCoordinate = cmds.xform (jntChain[i], q=True, ws=1, rp=1)
            locName = jntChain[i].replace("jnt", "ctrl")
            cntNew = cmds.circle( n= locName, ch=False )
            my_cnt.append( locName )
            cmds.setAttr( locName + ".visibility", l=True, cb=False, k=False )

            grpName = jntChain[i].replace("jnt", "grp")
            grpOff = cmds.group( n = grpName + "_offset" )
            grpAnchor = cmds.group( n = grpName + "_anchor" ) 
            grpObj = cmds.group( n = grpName )
            
            cmds.parent( grpObj, jntChain[i] )
            cmds.setAttr( grpName + ".translateX", 0 )
            cmds.setAttr( grpName + ".translateY", 0 ) 
            cmds.setAttr( grpName + ".translateZ", 0 ) 
            cmds.setAttr( grpName + ".rotateX", 0 )
            cmds.setAttr( grpName + ".rotateY", 0 ) 
            cmds.setAttr( grpName + ".rotateZ", 0 )
            cmds.parent( grpObj, w=True )
            my_grp.append( grpName )
        
        print (my_cnt)
        print (my_grp)
        
        try:
            cmds.parent( my_grp[3], my_cnt[2] )
        except:
            pass
            
        cmds.parent( my_grp[2], my_cnt[1] )
        cmds.parent( my_grp[1], my_cnt[0] )
        
        cmds.parentConstraint( my_cnt[0], jntChain[0], mo=True )
        cmds.parentConstraint( my_cnt[1], jntChain[1], mo=True )
        cmds.parentConstraint( my_cnt[2], jntChain[2], mo=True )
        
        try:
            cmds.parentConstraint( my_cnt[3], jntChain[3], mo=True )
        except:
            pass
            
        cmds.scaleConstraint( my_cnt[0], jntChain[0], mo=True )
        cmds.scaleConstraint( my_cnt[1], jntChain[1], mo=True )
        cmds.scaleConstraint( my_cnt[2], jntChain[2], mo=True )
        
        try:
            cmds.scaleConstraint( my_cnt[3], jntChain[3], mo=True )
        except:
            pass
        cmds.select(cl=True)
        print ("-----------------------------------------")
        print ("INFO: Fingers are Ready for Finalization!")
        print ("-----------------------------------------")
        #cmds.confirmDialog(message = "Fingers are Ready for Finalization!", button=['OK'], title='Info', icn = 'information', bgc = (1, 0.7, 0))
