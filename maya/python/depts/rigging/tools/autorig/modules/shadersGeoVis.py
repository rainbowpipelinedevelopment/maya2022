from maya import cmds

def shaderGeoVisExc():

    names = ["Head", "Arms", "Legs"]

    for name in names:

        shader = cmds.shadingNode("lambert", asShader=True, n="SDM_"+name)
        
        cmds.setAttr("Transform_Ctrl."+name+"_Geo_Vis", k=True, cb=False)

        cmds.setAttr("Transform_Ctrl."+name+"_Geo_Vis", 1)

        cmds.setAttr(shader+".transparencyR", 0)
        cmds.setAttr(shader+".transparencyG", 0)
        cmds.setAttr(shader+".transparencyB", 0)

        cmds.setDrivenKeyframe(shader+".transparencyR", cd="Transform_Ctrl."+name+"_Geo_Vis")
        cmds.setDrivenKeyframe(shader+".transparencyG", cd="Transform_Ctrl."+name+"_Geo_Vis")
        cmds.setDrivenKeyframe(shader+".transparencyB", cd="Transform_Ctrl."+name+"_Geo_Vis")

        cmds.setAttr("Transform_Ctrl."+name+"_Geo_Vis", 0)

        cmds.setAttr(shader+".transparencyR", 1)
        cmds.setAttr(shader+".transparencyG", 1)
        cmds.setAttr(shader+".transparencyB", 1)

        cmds.setDrivenKeyframe(shader+".transparencyR", cd="Transform_Ctrl."+name+"_Geo_Vis")
        cmds.setDrivenKeyframe(shader+".transparencyG", cd="Transform_Ctrl."+name+"_Geo_Vis")
        cmds.setDrivenKeyframe(shader+".transparencyB", cd="Transform_Ctrl."+name+"_Geo_Vis")
        
        cmds.setAttr("Transform_Ctrl."+name+"_Geo_Vis", k=False, cb=True)
            
    cmds.select(cl=True)