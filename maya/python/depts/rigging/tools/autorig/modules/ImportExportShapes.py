import maya.cmds as cmds
import json

def getNurbsCurveParameters(curveShape): 
    nurbsCurveParameters = {}
    nurbsCurve_degree = cmds.getAttr(curveShape+'.degree')
    formDictionary = {}
    formDictionary.setdefault(0,'open')
    formDictionary.setdefault(1,'closed')    
    formDictionary.setdefault(2,'periodic')     
    form_index = cmds.getAttr(curveShape+'.form')
    nurbsCurve_form = formDictionary[form_index]
    CI_node = cmds.createNode('curveInfo')
    cmds.connectAttr(curveShape+'.worldSpace[0]',CI_node+'.inputCurve',force=True)
    knotsList = cmds.getAttr(CI_node+'.knots')
    nurbsCurve_knots = list(knotsList[0])
    nurbsCurve_worldPoints = cmds.getAttr(CI_node+'.controlPoints[*]')
    cmds.delete(CI_node)
    nurbsCurve_color = cmds.getAttr(curveShape+'.drawOverride.overrideColor')
    nurbsCurveParameters.setdefault('degree',nurbsCurve_degree)
    nurbsCurveParameters.setdefault('form',nurbsCurve_form)
    nurbsCurveParameters.setdefault('points',nurbsCurve_worldPoints)      
    nurbsCurveParameters.setdefault('knots',nurbsCurve_knots)
    nurbsCurveParameters.setdefault('color',nurbsCurve_color)
    nurbsCurveParameters.setdefault('type','nurbsCurve')      
    return nurbsCurveParameters

def getNurbsSurfaceParameters(surfaceShape):
    nurbsSurfaceParameters = {}
    nurbsSurface_degreeU = cmds.getAttr(surfaceShape+'.degreeUV.degreeU')
    nurbsSurface_degreeV = cmds.getAttr(surfaceShape+'.degreeUV.degreeV')
    formDictionary = {}
    formDictionary.setdefault(0,'open')
    formDictionary.setdefault(1,'closed')    
    formDictionary.setdefault(2,'periodic')    
    formU_index = cmds.getAttr(surfaceShape+'.formU')        
    formV_index = cmds.getAttr(surfaceShape+'.formV')  
    nurbsSurface_formU = formDictionary[formU_index]
    nurbsSurface_formV = formDictionary[formV_index]     
    SI_node = cmds.createNode('surfaceInfo')
    cmds.connectAttr(surfaceShape+'.worldSpace[0]',SI_node+'.inputSurface',force=True)
    knotsU_list = cmds.getAttr(SI_node+'.knotsU')
    knotsV_list = cmds.getAttr(SI_node+'.knotsV')    
    nurbsSurface_knotsU = list(knotsU_list[0])
    nurbsSurface_knotsV = list(knotsV_list[0])    
    nurbsSurface_worldPoints = cmds.getAttr(SI_node+'.controlPoints[*]')
    cmds.delete(SI_node)
    nurbsSurface_color = cmds.getAttr(surfaceShape+'.drawOverride.overrideColor')
    nurbsSurfaceParameters.setdefault('degreeU',nurbsSurface_degreeU)
    nurbsSurfaceParameters.setdefault('degreeV',nurbsSurface_degreeV)    
    nurbsSurfaceParameters.setdefault('formU',nurbsSurface_formU)    
    nurbsSurfaceParameters.setdefault('formV',nurbsSurface_formV)        
    nurbsSurfaceParameters.setdefault('points',nurbsSurface_worldPoints)      
    nurbsSurfaceParameters.setdefault('knotsU',nurbsSurface_knotsU)      
    nurbsSurfaceParameters.setdefault('knotsV',nurbsSurface_knotsV)
    nurbsSurfaceParameters.setdefault('color',nurbsSurface_color)
    nurbsSurfaceParameters.setdefault('type','nurbsSurface')              
    return nurbsSurfaceParameters

def changeCoordinateSystem(pointList,spaceMatrix):
    transformedPointList = []
    for item in pointList:
        point_x = item[0]*spaceMatrix[0] + item[1]*spaceMatrix[4] + item[2]*spaceMatrix[8] + spaceMatrix[12]
        point_y = item[0]*spaceMatrix[1] + item[1]*spaceMatrix[5] + item[2]*spaceMatrix[9] + spaceMatrix[13]        
        point_z = item[0]*spaceMatrix[2] + item[1]*spaceMatrix[6] + item[2]*spaceMatrix[10] + spaceMatrix[14]    
        transformedPointList.append((point_x,point_y,point_z))
    return transformedPointList
    
def saveNurbsShapeCommand(*args):
    selection = cmds.ls(selection=True)    
    targetShapes = []
    validShapes = ['nurbsCurve','nurbsSurface']
    shapeParameters = {}
    for item in selection:            
        itemShapeList = cmds.listRelatives(item,shapes=True,path=True,noIntermediate=True)
        if not itemShapeList:
            if cmds.objectType(item) in validShapes:                
                targetShapes.append(item)
            else:
                cmds.warning('Skipping "'+str(item)+'": not a valid NURBS curve/surface')
        for token in itemShapeList:
            if cmds.objectType(token) in validShapes:
                targetShapes.append(token)
            else:
                cmds.warning('Skipping "'+str(token)+'": not a valid NURBS curve/surface')                
    if not targetShapes:
        cmds.warning('Select at least one valid NURBS curve/surface')
        return
    for item in targetShapes:
        targetName = cmds.listRelatives(item,parent=True,noIntermediate=True)           
        if cmds.objectType(item) == 'nurbsCurve':
            item_parameters = getNurbsCurveParameters(item)
            shapeParameters.setdefault(targetName[0],[])
            shapeParameters[targetName[0]].append(item_parameters)
        if cmds.objectType(item) == 'nurbsSurface':
            item_parameters = getNurbsSurfaceParameters(item)
            shapeParameters.setdefault(targetName[0],[])
            shapeParameters[targetName[0]].append(item_parameters)            
    saveFile = cmds.fileDialog2(caption='Save As',fileFilter='*.json',selectFileFilter='*.json',fileMode=0)
    with open(saveFile[0],'w') as storage:
        json.dump(shapeParameters,storage,indent=4,sort_keys=True)
        
def loadNurbsShapeCommand(*args):
    selection = cmds.ls(selection=True)    
    target = []
    validShapes = ['nurbsCurve','nurbsSurface']
    shapeParameters = {}
    for item in selection:            
        itemShapeList = cmds.listRelatives(item,shapes=True,path=True,noIntermediate=True)
        if not itemShapeList:
            itemType = cmds.objectType(item)
            if itemType in validShapes:
                itemParentList = cmds.listRelatives(item,parent=True,noIntermediate=True)
                new_entry = (itemParentList[0])                
                if not new_entry in target:
                    target.append(new_entry)
            else:
                cmds.warning('Skipping "'+str(item)+'": not a valid NURBS curve/surface')
        else:
            for token in itemShapeList:
                tokenType = cmds.objectType(token)
                if tokenType in validShapes:
                    tokenParentList = cmds.listRelatives(token,parent=True,noIntermediate=True)
                    new_entry = (tokenParentList[0])
                    if not new_entry in target:
                        target.append(new_entry)
                else:
                    cmds.warning('Skipping "'+str(token)+'": not a valid NURBS curve/surface')                
    if not target:
        cmds.warning('Select at least one valid NURBS curve/surface')
        return
    loadFile = cmds.fileDialog2(caption='Load',fileFilter='*.json',selectFileFilter='*.json',okCaption='Load',fileMode=1)
    with open(loadFile[0],'r') as storage:
        shapeParameters = json.load(storage)
    storedNurbs = shapeParameters.keys()
    for item in target:
        if item in storedNurbs:
            try:
                storedMatch = shapeParameters[item]
            except:
                cmds.warning('Skipping '+str(item)+': match not found') 
                continue
            if storedMatch[0]['type'] == 'nurbsCurve':
                oldShapes = cmds.listRelatives(item,shapes=True,path=True,noIntermediate=True)                    
                cmds.delete(oldShapes) 
                for token in storedMatch:
                    curveDegree = token['degree']
                    if token['form'] == 'periodic':
                        curvePeriodic = True
                    else:
                        curvePeriodic = False
                    curveKnots = token['knots']
                    curveColor = token['color']
                    storedPoints = token['points']
                    spaceMatrix = cmds.getAttr(item+'.parentInverseMatrix[0]')                    
                    curvePoints = changeCoordinateSystem(storedPoints,spaceMatrix)
                    newCurve = cmds.curve(degree=curveDegree,periodic=curvePeriodic,knot=curveKnots,point=curvePoints)
                    newCurveShape = cmds.listRelatives(newCurve,shapes=True,path=True,noIntermediate=True)
                    cmds.setAttr(newCurveShape[0]+'.drawOverride.overrideEnabled',True)
                    cmds.setAttr(newCurveShape[0]+'.drawOverride.overrideColor',curveColor)                                       
                    cmds.parent(newCurveShape[0],item,relative=True,shape=True)
                    cmds.delete(newCurve)
                    cmds.rename(newCurveShape[0],oldShapes[0])
                    cmds.select(clear=True)                                          
            if storedMatch[0]['type'] == 'nurbsSurface':                   
                oldShapes = cmds.listRelatives(item,shapes=True,path=True,noIntermediate=True)
                surfaceShader = cmds.listConnections(oldShapes[0],type='shadingEngine')                  
                cmds.delete(oldShapes)                 
                for token in storedMatch:
                    surfaceDegreeU = token['degreeU']
                    surfaceDegreeV = token['degreeV']                    
                    surfaceFormU = token['formU']
                    surfaceFormV = token['formV']
                    surfaceKnotsU = token['knotsU']                    
                    surfaceKnotsV = token['knotsV']                    
                    surfaceColor = token['color'] 
                    storedPoints = token['points']                     
                    spaceMatrix = cmds.getAttr(item+'.parentInverseMatrix[0]')
                    surfacePoints = changeCoordinateSystem(storedPoints,spaceMatrix)
                    newSurface = cmds.surface(degreeU=surfaceDegreeU,degreeV=surfaceDegreeV,formU=surfaceFormU,formV=surfaceFormV,knotU=surfaceKnotsU,knotV=surfaceKnotsV,point=surfacePoints)                                      
                    newSurfaceParent = cmds.listRelatives(newSurface,parent=True,path=True,noIntermediate=True)
                    cmds.setAttr(newSurface+'.drawOverride.overrideEnabled',True)
                    cmds.setAttr(newSurface+'.drawOverride.overrideColor',surfaceColor)                                       
                    cmds.parent(newSurface,item,relative=True,shape=True)
                    cmds.delete(newSurfaceParent)                    
                    if not surfaceShader:
                        cmds.sets(newSurface,edit=True,forceElement='initialShadingGroup')
                    else:                    
                        cmds.sets(newSurface,edit=True,forceElement=surfaceShader[0])
                    cmds.rename(newSurface,oldShapes[0])                 
                    cmds.select(clear=True)    
                    
def createWindow():
    if (cmds.window("shp", q=True,exists=True)):
		cmds.deleteUI("shp", window=True)
    W=cmds.window("shp",title="Save Shapes Tool", ip=True, rtf=True)
    C0=cmds.columnLayout(adjustableColumn=True, rowSpacing=10, p=W)
    cmds.text("Select controls with shapes to save or to load:",p=C0)
    R0= cmds.rowLayout( numberOfColumns=2, columnAttach=[(1, 'both', 5),(2, 'right', 5)], p=C0)
    cmds.button(label="Save Shapes", command=saveNurbsShapeCommand, p=R0, align="left", w=150)
    cmds.button(label="Load Shapes", command=loadNurbsShapeCommand, p=R0, align="left", w=150)
    
    cmds.showWindow( W )
    return