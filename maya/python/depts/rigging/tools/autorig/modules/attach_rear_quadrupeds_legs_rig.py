from maya import cmds

def main():
    #faccio l'unlock agli attributi dei gruppi a cui devo fare i constrain
    unlock_attr_list = ["frzRear_lt_legOffset_grp", "frzRear_rt_legOffset_grp", "frzFk_Rear_lt_hip1_jnt_grp", "frzFk_Rear_rt_hip1_jnt_grp"]

    for ctrl in unlock_attr_list:
        cmds.setAttr(ctrl+".tx", l=False)
        cmds.setAttr(ctrl+".ty", l=False)
        cmds.setAttr(ctrl+".tz", l=False)
        cmds.setAttr(ctrl+".rx", l=False)
        cmds.setAttr(ctrl+".ry", l=False)
        cmds.setAttr(ctrl+".rz", l=False)
        cmds.setAttr(ctrl+".sx", l=False)
        cmds.setAttr(ctrl+".sy", l=False)
        cmds.setAttr(ctrl+".sz", l=False)
        cmds.setAttr(ctrl+".v", l=False)
        
    #faccio il lock dell'attributo radius
    lock_attr_list = ["Rear_rt_fk_Rear_rt_hip1_jnt_ctrl", "Rear_rt_fk_Rear_rt_ball_jnt_ctrl", "Rear_rt_legOffset_ctrl", "Rear_rt_fk_Rear_rt_ankle_jnt_ctrl",
    "Rear_rt_fk_Rear_rt_hip2_jnt_ctrl", "Rear_rt_fk_Rear_rt_knee1_jnt_ctrl",
    "Rear_lt_legOffset_ctrl", "Rear_lt_fk_Rear_lt_hip2_jnt_ctrl", "Rear_lt_fk_Rear_lt_hip1_jnt_ctrl", "Rear_lt_fk_Rear_lt_knee1_jnt_ctrl",
    "Rear_rt_flex_ctrl", "Rear_lt_fk_Rear_lt_ankle_jnt_ctrl", "Rear_lt_fk_Rear_lt_ball_jnt_ctrl", "Rear_rt_foot_ctrl", "Rear_lt_flex_ctrl", "Rear_lt_foot_ctrl"]

    for ctrl in lock_attr_list:
        cmds.setAttr(ctrl+".radius", 0)
        cmds.setAttr(ctrl+".radius", l=True, k=False, cb=False)
        cmds.setAttr(ctrl+".drawStyle", 2)
        
    #cancello i constrain sbagliati
    cmds.delete("frzFk_Rear_lt_hip1_jnt_grp_parentConstraint1", "frzFk_Rear_rt_hip1_jnt_grp_parentConstraint1",
    "non_scale_grp", "Rear_charRigInfo")

    #faccio i constrain
    cmds.parentConstraint("Transform_Null", "Rear_worldA_ctrl", mo=True)
    cmds.scaleConstraint("Transform_Null", "Rear_worldA_ctrl", mo=True)

    cmds.parentConstraint("RIG_hip_jnt", "frzRear_lt_legOffset_grp", mo=True)
    cmds.parentConstraint("RIG_hip_jnt", "frzRear_rt_legOffset_grp", mo=True)
    cmds.scaleConstraint("Transform_Null", "frzRear_lt_legOffset_grp", mo=True)
    cmds.scaleConstraint("Transform_Null", "frzRear_rt_legOffset_grp", mo=True)

    cmds.parentConstraint("RIG_hip_jnt", "frzFk_Rear_lt_hip1_jnt_grp", mo=True)
    cmds.parentConstraint("RIG_hip_jnt", "frzFk_Rear_rt_hip1_jnt_grp", mo=True)
    
    #connetto gli switch
    cmds.connectAttr("Transform_Ctrl.L_Leg_01_IKFK", "Rear_lt_legSwitches_ctrl.FK_IK")
    cmds.connectAttr("Transform_Ctrl.R_Leg_01_IKFK", "Rear_rt_legSwitches_ctrl.FK_IK")
    
    #nascondo la visibility delle shape che non si devono vedere
    cmds.setAttr("Rear_worldA_ctrlShape.v", 0)
    cmds.setAttr("Rear_worldB_ctrlShape.v", 0)

    cmds.setAttr("Rear_lt_legSwitches_ctrlShape.v", 0)
    cmds.setAttr("Rear_rt_legSwitches_ctrlShape.v", 0)

    #coloro i controlli
    red_ctrls = ["Rear_lt_legOffset_ctrlShape", "Rear_rt_legOffset_ctrlShape", "Rear_lt_foot_ctrlShape", "Rear_rt_foot_ctrlShape", "Rear_lt_knee_ctrlShape", "Rear_rt_knee_ctrlShape"]

    for ctrl in red_ctrls:
        cmds.setAttr(ctrl+".overrideEnabled", 1)
        cmds.setAttr(ctrl+".overrideColor", 13)

    blue_ctrls = ["Rear_lt_flex_ctrlShape", "Rear_rt_flex_ctrlShape",
    "Rear_lt_fk_Rear_lt_hip1_jnt_ctrlShape", "Rear_lt_fk_Rear_lt_hip2_jnt_ctrlShape", "Rear_lt_fk_Rear_lt_knee1_jnt_ctrlShape", "Rear_lt_fk_Rear_lt_ankle_jnt_ctrlShape", "Rear_lt_fk_Rear_lt_ball_jnt_ctrlShape",
    "Rear_rt_fk_Rear_rt_hip1_jnt_ctrlShape", "Rear_rt_fk_Rear_rt_hip2_jnt_ctrlShape", "Rear_rt_fk_Rear_rt_knee1_jnt_ctrlShape", "Rear_rt_fk_Rear_rt_ankle_jnt_ctrlShape", "Rear_rt_fk_Rear_rt_ball_jnt_ctrlShape"]

    for ctrl in blue_ctrls:
        cmds.setAttr(ctrl+".overrideEnabled", 1)
        cmds.setAttr(ctrl+".overrideColor", 6)

    #rinomino e setto gli attributi di stretch
    cmds.renameAttr("Rear_lt_foot_ctrl.autoExtend", "Stretch")
    cmds.renameAttr("Rear_rt_foot_ctrl.autoExtend", "Stretch")

    cmds.renameAttr("Rear_lt_foot_ctrl.extendClamp", "Max_Stretch")
    cmds.renameAttr("Rear_rt_foot_ctrl.extendClamp", "Max_Stretch")

    cmds.setAttr("Rear_lt_foot_ctrl.Max_Stretch", 10)
    cmds.setAttr("Rear_rt_foot_ctrl.Max_Stretch", 10)
    
    #sblocco le translate ai controlli fk
    
    #cancello i constraint che non servono
    cmds.delete("Rear_lt_hip1_jnt_parentConstraint2", "Rear_rt_hip1_jnt_parentConstraint2")
      
    #sblocco le translate ai controlli fk e faccio i constraint solo translate ai joint def
    prefix = ["Rear_lt_", "Rear_rt_"]
    joint_name = ["hip1_jnt", "hip2_jnt", "knee1_jnt", "ankle_jnt", "ball_jnt"]
    
    for jnt in joint_name:
        cmds.setAttr(prefix[0] + "fk_" + prefix[0] + jnt + "_ctrl.tx", l=False, k=True)
        cmds.setAttr(prefix[0] + "fk_" + prefix[0] + jnt + "_ctrl.ty", l=False, k=True)
        cmds.setAttr(prefix[0] + "fk_" + prefix[0] + jnt + "_ctrl.tz", l=False, k=True)
        constr = cmds.parentConstraint("fk_" + prefix[0] + jnt, "ik_" + prefix[0] + jnt, prefix[0] + jnt, sr=["x", "y", "z"], mo=True)
        cmds.connectAttr(prefix[0] + "legSwitches_ctrl.FK_IK", constr[0] + ".ik_" + prefix[0] + jnt + "W1")
        cmds.connectAttr(prefix[0] + "legSwitches_ctrl_IK_FK_rev.outputX", constr[0] + ".fk_" + prefix[0] + jnt + "W0")
        
    for jnt in joint_name:
        cmds.setAttr(prefix[1] + "fk_" + prefix[1] + jnt + "_ctrl.tx", l=False, k=True)
        cmds.setAttr(prefix[1] + "fk_" + prefix[1] + jnt + "_ctrl.ty", l=False, k=True)
        cmds.setAttr(prefix[1] + "fk_" + prefix[1] + jnt + "_ctrl.tz", l=False, k=True)
        constr = cmds.parentConstraint("fk_" + prefix[1] + jnt, "ik_" + prefix[1] + jnt, prefix[1] + jnt, sr=["x", "y", "z"], mo=True)
        cmds.connectAttr(prefix[1] + "legSwitches_ctrl.FK_IK", constr[0] + ".ik_" + prefix[1] + jnt + "W1")
        cmds.connectAttr(prefix[1] + "legSwitches_ctrl_IK_FK_rev.outputX", constr[0] + ".fk_" + prefix[1] + jnt + "W0")

    #parento tutti i joint nel gruppo in cui devono stare
    cmds.parent("fk_Rear_lt_hip1_jnt", "ik_Rear_lt_hip1_jnt", "Rear_lt_legFlex1_jnt", "fk_Rear_rt_hip1_jnt", "ik_Rear_rt_hip1_jnt", "Rear_rt_legFlex1_jnt", "Rear_skeletons_grp")
    cmds.parent("Rear_rig", "RIG_controls_grp")
    cmds.select(cl=True)
    
    #connetto la visibility dei leg offset allo switch IK/FK
    
    cmds.connectAttr("Transform_Ctrl.L_Leg_01_IKFK", "frzRear_lt_legOffset_grp.v")
    cmds.connectAttr("Transform_Ctrl.R_Leg_01_IKFK", "frzRear_rt_legOffset_grp.v")
    cmds.select(cl=True)
    
    #######################################################################################################################
    #faccio gli space switch ai pole vector
    #######################################################################################################################
    
    #?sblocco gli attributi dei gruppi padre dei pole vector
    groups = ["frzRear_lt_knee_grp", "frzRear_rt_knee_grp"]
    attrs = [".tx",".ty",".tz", ".rx", ".ry", ".rz", ".sx", ".sy", ".sz", ".v"]

    for grp in groups:
        i = 0
        while i <= 9:
            cmds.setAttr(grp + attrs[i] , e=True, l=False)
            i += 1
            
    controls = [["RIG_worldSpaceCtrl_space_switch_grp", "Transform_Null", "RIG_cog_ctrl", "Rear_lt_foot_ctrl", "Rear_lt_knee_ctrl"],
    ["RIG_worldSpaceCtrl_space_switch_grp", "Transform_Null", "RIG_cog_ctrl", "Rear_rt_foot_ctrl", "Rear_rt_knee_ctrl"]]
    
    for ctrls in controls:
        sel = ctrls
        last = sel[-1]
        lenght = len(sel)
        attribute_name = "follow"
    
        #creo l attributo enum all ultimo controllo selezionato, con i nomi switch dei controlli precedenti
        enum_attributes = ""
        i = 0
        while i <= lenght-2:
            enum_attributes += sel[i]+":"
            i += 1
        cmds.addAttr(last, ln=attribute_name, at="enum", en=enum_attributes, k=True)
    
        #tutti i controlli tranne l'ultimo, fanno il parent constrain sul gruppo padre dell ultimo controllo selezionato
        last_group = cmds.pickWalk(last, d="up")
        i = 0
        while i <= lenght-2:
            par_constr = cmds.parentConstraint(sel[i], last_group, mo=True)[0]
    
            #creo e setto un condition per ogni attributo
            cnd = cmds.createNode("condition", n="CND_"+attribute_name+"_"+last+"_Weigth_"+sel[i])
            cmds.setAttr(cnd+".secondTerm", i)
            cmds.setAttr(cnd+".colorIfTrueR", 1)
            cmds.setAttr(cnd+".colorIfFalseR", 0)
    
            #connetto gli attributi ai condition e i condition al constrain
            cmds.connectAttr(last+"."+attribute_name, cnd+".firstTerm")
            cmds.connectAttr(cnd+".outColorR", par_constr+"."+sel[i]+"W"+str(i))
    
            i += 1
            
            cmds.addAttr(last + ".follow", e=True, en="World:Transform:Cog:Foot")
            cmds.setAttr(last + ".follow", 1)
        
        cmds.select(cl=True)
    
    ########################################################################################################################