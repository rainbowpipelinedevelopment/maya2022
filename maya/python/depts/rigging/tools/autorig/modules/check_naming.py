import maya.cmds as cmds

def selIssues(*args):
    cmds.select(toSel)
    return

def doubleNames(*args):
    allNames = cmds.ls(type='transform')
    doubleNames = []
    for name in allNames:
        if name.count('|') and 'nosmooth' not in name:
            if doubleNames.count(name)==0:
                doubleNames.append(name)
    if len(doubleNames)>0:
        print("-------------------------------------------------------------------------------------------------------------------------" + "\n")
        print ("\nRILEVATI I SEGUENTI NOMI DUPLICATI IN SCENA:\n")
        for i in doubleNames:
            print (i)
    else:
        print("\n-------------------------------------------------------------------------------------------------------------------------" + "\n")
        print ("\nNON SONO STATI RILEVATI NOMI DUPLICATI.\n")
    return doubleNames

def setsInScene():
    sestsInScene= cmds.ls(sets=True)
    setsControls = []
    for elem in sestsInScene:
        a = str(elem)
        if "CONTROLS" in a:
            setsControls.append(elem)
    return setsControls

def checkPlus(controls,listSet,optionalList):
    l=[]
    for cnt in controls:
        if (cnt not in listSet) and (cnt not in optionalList):
            l.append(cnt)
    return l

def checkMinus(listControls,listSet):
    l=[]
    for controlInList in listControls:
        if controlInList not in listSet:
            l.append(controlInList)
    return l

def checkSets(*args):
    bendyQuadList = [ "L_front_hip1_bendy_Ctrl", "L_front_hip2_A_bendy_Ctrl", "L_front_hip2_B_bendy_Ctrl", "L_rear_hip1_bendy_Ctrl", "L_rear_hip2_A_bendy_Ctrl", 
    "L_rear_hip2_B_bendy_Ctrl", "L_rear_knee_A_bendy_Ctrl", "L_rear_knee_B_bendy_Ctrl", "R_front_hip1_bendy_Ctrl", "R_front_hip2_A_bendy_Ctrl", "R_front_hip2_B_bendy_Ctrl", 
    "R_rear_hip1_bendy_Ctrl", "R_rear_hip2_A_bendy_Ctrl", "R_rear_hip2_B_bendy_Ctrl", "R_rear_knee_A_bendy_Ctrl", "R_rear_knee_B_bendy_Ctrl"]
    
    bodyQuadList = ["head_Ctrl", "neck_Ctrl", "spineHigh_Ctrl", "spineMid_Ctrl", "spineLow_Ctrl", "cog_Ctrl", "hip_Ctrl", "R_rear_legoffset_ik_Ctrl", "R_rear_upleg_Ctrl", 
    "L_rear_legoffset_ik_Ctrl", "L_rear_upleg_Ctrl", "R_front_upleg_Ctrl", "R_front_legoffset_ik_Ctrl", "L_front_legoffset_ik_Ctrl", "L_front_upleg_Ctrl", "L_front_flex_ik_Ctrl", 
    "L_front_foot_ik_Ctrl", "L_front_foot_constraint_ik_Ctrl", "L_rear_flex_ik_Ctrl", "L_rear_foot_ik_Ctrl", "L_rear_foot_constraint_ik_Ctrl", "TransformPivot_Ctrl", "R_front_foot_ik_Ctrl", 
    "R_front_foot_constraint_ik_Ctrl", "R_front_flex_ik_Ctrl", "R_rear_flex_ik_Ctrl", "R_rear_foot_ik_Ctrl", "R_rear_foot_constraint_ik_Ctrl", "Transform_Ctrl", "Root_Ctrl", "Main_Ctrl", 
    "R_ear_03_Ctrl", "R_ear_02_Ctrl", "R_ear_01_Ctrl", "R_ear_root_Ctrl", "L_ear_03_Ctrl", "L_ear_02_Ctrl", "L_ear_01_Ctrl", "L_ear_root_Ctrl", "L_eye_Ctrl", "R_eye_Ctrl", "eyeMaster_Ctrl", 
    "tail_07_Ctrl", "tail_06_Ctrl", "tail_05_Ctrl", "tail_04_Ctrl", "tail_03_Ctrl", "tail_02_Ctrl", "tail_01_Ctrl", "tail_root_Ctrl", "R_rear_toe_D_Ctrl", "R_rear_toe_C_Ctrl", "R_rear_toe_B_Ctrl", 
    "R_rear_toe_A_Ctrl", "L_rear_toe_D_Ctrl", "L_rear_toe_C_Ctrl", "L_rear_toe_B_Ctrl", "L_rear_toe_A_Ctrl", "R_front_toe_D_Ctrl", "R_front_toe_C_Ctrl", "R_front_toe_B_Ctrl", "R_front_toe_A_Ctrl", 
    "L_front_toe_D_Ctrl", "L_front_toe_C_Ctrl", "L_front_toe_B_Ctrl", "L_front_toe_A_Ctrl", "L_front_claw_A_Ctrl", "L_front_claw_B_Ctrl", "L_front_claw_C_Ctrl", "L_front_claw_D_Ctrl", "R_front_claw_A_Ctrl", 
    "R_front_claw_B_Ctrl", "R_front_claw_C_Ctrl", "R_front_claw_D_Ctrl", "L_rear_claw_C_Ctrl", "L_rear_claw_D_Ctrl", "L_rear_claw_A_Ctrl", "L_rear_claw_B_Ctrl", "R_rear_claw_C_Ctrl", "R_rear_claw_A_Ctrl", 
    "R_rear_claw_B_Ctrl", "R_rear_claw_D_Ctrl", "spineShaper_Ctrl", "L_front_knee_pv_Ctrl", "R_rear_knee_pv_Ctrl", "L_rear_knee_pv_Ctrl", "R_front_knee_pv_Ctrl", "R_propAnchor_LOC_Ctrl", 
    "R_propOffset_Ctrl", "R_propNULL_Ctrl", "L_propAnchor_LOC_Ctrl", "L_propOffset_Ctrl", "L_propNULL_Ctrl", "R_rear_ball_fk_Ctrl", "R_rear_ankle_fk_Ctrl", "R_rear_knee_fk_Ctrl", "R_rear_hip2_fk_Ctrl", 
    "R_rear_hip1_fk_Ctrl", "R_front_hip1_fk_Ctrl", "R_front_hip2_fk_Ctrl", "R_front_knee_fk_Ctrl", "R_front_ankle_fk_Ctrl", "R_front_ball_fk_Ctrl", "L_front_hip1_fk_Ctrl", "L_front_hip2_fk_Ctrl", 
    "L_front_knee_fk_Ctrl", "L_front_ankle_fk_Ctrl", "L_front_ball_fk_Ctrl", "L_rear_hip1_fk_Ctrl", "L_rear_hip2_fk_Ctrl", "L_rear_knee_fk_Ctrl", "L_rear_ankle_fk_Ctrl", "L_rear_ball_fk_Ctrl"]
    
    faceList = ["visibility_Ctrl", "R_eyeLidLow_Ctrl", "R_eyeLidCornerOut_Ctrl", "R_eyeLidUp_Ctrl", "R_eyeLidCornerInn_Ctrl", "L_eyeLidLow_Ctrl", "L_eyeLidCornerOut_Ctrl", "L_eyeLidUp_Ctrl", 
    "L_eyeLidCornerInn_Ctrl", "brows_master_Ctrl", "nose_Ctrl", "R_upperMouth_Ctrl", "L_upperMouth_Ctrl", "R_cornerMouth_Ctrl", "L_cornerMouth_Ctrl", "lower_lip_Ctrl", "upper_lip_Ctrl", 
    "R_brow_03_Ctrl", "R_brow_02_Ctrl", "R_brow_01_Ctrl", "L_brow_03_Ctrl", "L_brow_02_Ctrl", "L_brow_01_Ctrl", "R_eyeLidLowInn_Ctrl", "R_eyeLidLowOut_Ctrl", "R_eyeLidUpOut_Ctrl", "R_eyeLidUpInn_Ctrl", 
    "L_eyeLidLowInn_Ctrl", "L_eyeLidLowOut_Ctrl", "L_eyeLidUpOut_Ctrl", "L_eyeLidUpInn_Ctrl", "L_low_lip_01_Ctrl", "L_low_lip_Ctrl", "low_lip_Ctrl", "R_low_lip_Ctrl", "R_low_lip_01_Ctrl", 
    "L_up_lip_01_Ctrl", "L_up_lip_Ctrl", "up_lip_Ctrl", "R_up_lip_Ctrl", "R_up_lip_01_Ctrl",  "headSqs_Ctrl", "headSqs_move_Ctrl", "R_lower_teeth_Ctrl", "L_lower_teeth_Ctrl", "C_lower_teeth_Ctrl", "low_teeth_Ctrl", "tongue_05_Ctrl", 
    "tongue_04_Ctrl", "tongue_03_Ctrl", "tongue_02_Ctrl", "tongue_01_Ctrl", "jaw_Ctrl", "R_upper_teeth_Ctrl", "L_upper_teeth_Ctrl", "C_upper_teeth_Ctrl", "up_teeth_Ctrl", "mouth_master_Ctrl", 
    "R_eye_fk_Ctrl", "L_eye_fk_Ctrl", "R_eyeMover_Ctrl", "L_eyeMover_Ctrl", "L_brow_up_down_sad_Ctrl", "R_brow_up_down_sad_Ctrl", "L_brow_angry_Ctrl", "R_brow_angry_Ctrl", "global_blend_Ctrl", 
    "mouth_blend_Ctrl", "brow_blend_Ctrl", "eye_blend_Ctrl", "extras_blend_Ctrl", "phoneme_blend_Ctrl", "jaw_rotation_Ctrl", "jaw_translation_Ctrl", "jaw_forward_backward_Ctrl", "upper_teeth_Ctrl", 
    "lower_teeth_Ctrl", "tongue_rotation_Ctrl", "tongue_up_down_Ctrl"]
    
    optionalList = ["R_low_whisker_06_Ctrl", "R_low_whisker_05_Ctrl", "R_low_whisker_04_Ctrl", "R_low_whisker_03_Ctrl", "R_low_whisker_02_Ctrl", "R_low_whisker_01_Ctrl", "R_up_whisker_05_Ctrl", "R_up_whisker_04_Ctrl", 
    "R_up_whisker_03_Ctrl", "R_up_whisker_02_Ctrl", "R_up_whisker_01_Ctrl", "L_low_whisker_06_Ctrl", "L_low_whisker_05_Ctrl", "L_low_whisker_04_Ctrl", "L_low_whisker_03_Ctrl", "L_low_whisker_02_Ctrl", "L_low_whisker_01_Ctrl", 
    "L_up_whisker_05_Ctrl", "L_up_whisker_04_Ctrl", "L_up_whisker_03_Ctrl", "L_up_whisker_02_Ctrl", "L_up_whisker_01_Ctrl", "L_mid_whisker_01_Ctrl", "L_mid_whisker_02_Ctrl", "L_mid_whisker_03_Ctrl", "L_mid_whisker_04_Ctrl", "L_mid_whisker_05_Ctrl",
    "L_up_whisker_05_Ctrl", "L_mid_whisker_06_Ctrl", "R_mid_whisker_01_Ctrl", "R_mid_whisker_02_Ctrl", "R_mid_whisker_03_Ctrl", "R_mid_whisker_04_Ctrl","R_mid_whisker_05_Ctrl","R_mid_whisker_06_Ctrl", "tail_06_Ctrl","tail_07_Ctrl","tail_08_Ctrl",
    "tail_01_Ctrl","tail_02_Ctrl","tail_03_Ctrl","tail_04_Ctrl","tail_05_Ctrl"]
    
    bodyBipedQuadList = ["L_pinkyTip_fk_Ctrl", "R_indexTop_fk_Ctrl", "R_indexMid_fk_Ctrl", "L_indexTop_fk_Ctrl", "L_indexMid_fk_Ctrl", "R_middleTip_fk_Ctrl", "R_pinkyTop_fk_Ctrl", "hip_Ctrl", 
    "cog_Ctrl", "spineLow_Ctrl", "spineHigh_Ctrl", "head_Ctrl", "neck_Ctrl", "spineMid_Ctrl", "R_eye_Ctrl", "L_eye_Ctrl", "L_wrist_fk_Ctrl", "L_clavicleTrans_Ctrl", "eyeMaster_Ctrl", "L_upArm_fk_Ctrl", 
    "L_elbow_fk_Ctrl", "L_hand_Ctrl", "R_wrist_fk_Ctrl", "R_clavicleTrans_Ctrl", "R_hand_Ctrl", "R_upArm_fk_Ctrl", "R_elbow_fk_Ctrl", "L_pinkyBase_fk_Ctrl", "L_pinkyMid_fk_Ctrl", "L_pinkyTop_fk_Ctrl", 
    "L_indexBase_fk_Ctrl", "L_thumbBase_fk_Ctrl", "L_thumbTip_fk_Ctrl", "L_thumbMid_fk_Ctrl", "L_middleTip_fk_Ctrl", "L_indexTip_fk_Ctrl", "L_middleMid_fk_Ctrl", "R_pinkyBase_fk_Ctrl", 
    "L_middleBase_fk_Ctrl", "L_middleTop_fk_Ctrl", "R_pinkyTip_fk_Ctrl", "R_thumbMid_fk_Ctrl", "R_thumbTip_fk_Ctrl", "R_indexBase_fk_Ctrl", "R_thumbBase_fk_Ctrl", "R_pinkyMid_fk_Ctrl", 
    "R_middleTop_fk_Ctrl", "R_middleMid_fk_Ctrl", "R_indexTip_fk_Ctrl", "R_middleBase_fk_Ctrl", "L_hip1_fk_Ctrl", "L_hip2_fk_Ctrl", "L_ball_fk_Ctrl", "L_ankle_fk_Ctrl", "L_knee_fk_Ctrl", 
    "R_knee_fk_Ctrl", "R_hip2_fk_Ctrl", "R_hip1_fk_Ctrl", "R_ankle_fk_Ctrl", "R_ball_fk_Ctrl", "spineShaper_Ctrl", "Transform_Ctrl", "Main_Ctrl", 
    "L_shoulder_ik_Ctrl", "L_arm_ik_Ctrl", "R_shoulder_ik_Ctrl", "R_arm_ik_Ctrl", "L_foot_ik_Ctrl", "L_legoffset_ik_Ctrl", "R_legoffset_ik_Ctrl", "R_foot_ik_Ctrl", "L_elbow_pv_Ctrl", 
    "L_arm_constraint_ik_Ctrl", "R_elbow_pv_Ctrl", "R_arm_constraint_ik_Ctrl", "TransformPivot_Ctrl", "Root_Ctrl", "L_flex_ik_Ctrl", "L_knee_pv_Ctrl", "R_flex_ik_Ctrl", "R_knee_pv_Ctrl", 
    "L_wrist_sec_fk_Ctrl", "L_upArm_sec_fk_Ctrl", "L_elbow_sec_fk_Ctrl", "R_wrist_sec_fk_Ctrl", "R_upArm_sec_fk_Ctrl", "R_elbow_sec_fk_Ctrl", "L_foot_constraint_ik_Ctrl", "R_foot_constraint_ik_Ctrl", 
    "R_ear_root_Ctrl", "L_ear_root_Ctrl", "tail_06_Ctrl", "tail_05_Ctrl", "tail_01_Ctrl", "tail_root_Ctrl", "tail_04_Ctrl", "tail_03_Ctrl", "tail_02_Ctrl", "L_propNULL_Ctrl", "L_propOffset_Ctrl", 
    "R_propNULL_Ctrl", "R_propOffset_Ctrl", "L_ear_01_Ctrl", "L_ear_02_Ctrl", "L_ear_03_Ctrl", "R_ear_01_Ctrl", "R_ear_02_Ctrl", "R_ear_03_Ctrl"]
    
    bendyBipedQuadList = ["R_forearm_bendy_Ctrl", "R_elbow_bendy_Ctrl", "R_upArm_bendy_Ctrl", "L_forearm_bendy_Ctrl", "L_elbow_bendy_Ctrl", "L_upArm_bendy_Ctrl", "R_knee_A_bendy_Ctrl", 
    "R_knee_B_bendy_Ctrl", "R_hip2_A_bendy_Ctrl", "R_hip2_B_bendy_Ctrl", "R_hip1_bendy_Ctrl", "L_knee_A_bendy_Ctrl", "L_knee_B_bendy_Ctrl", "L_hip2_A_bendy_Ctrl", 
    "L_hip2_B_bendy_Ctrl", "L_hip1_bendy_Ctrl"]
    
    bodyBipedList = ["R_heel_ik_Ctrl", "R_hip_Ctrl", "neck_Ctrl", "cog_Ctrl", "hip_Ctrl", "spineShaper_Ctrl", "spineMid_Ctrl", "Transform_Ctrl", "TransformPivot_Ctrl", "Main_Ctrl", 
    "R_pinkyTip_fk_Ctrl", "L_pinkyTip_fk_Ctrl", "L_pinkyMid_fk_Ctrl", "R_thumbMid_fk_Ctrl", "R_thumbBase_fk_Ctrl", "L_pinkyBase_fk_Ctrl", "R_indexBase_fk_Ctrl", "R_indexMid_fk_Ctrl", 
    "R_middleBase_fk_Ctrl", "R_indexTip_fk_Ctrl", "R_thumbTip_fk_Ctrl", "R_middleTip_fk_Ctrl", "R_pinkyTop_fk_Ctrl", "R_pinkyMid_fk_Ctrl", "R_pinkyBase_fk_Ctrl", "R_middleMid_fk_Ctrl", 
    "L_thumbBase_fk_Ctrl", "L_middleMid_fk_Ctrl", "L_middleBase_fk_Ctrl", "L_pinkyTop_fk_Ctrl", "L_indexTip_fk_Ctrl", "L_middleTip_fk_Ctrl", "spineHigh_Ctrl", "spineLow_Ctrl", "head_Ctrl", 
    "L_clavicleTrans_Ctrl", "R_knee_pv_Ctrl", "R_hand_Ctrl", "R_arm_ik_Ctrl", "R_shoulder_ik_Ctrl", "L_hand_Ctrl", "L_arm_ik_Ctrl", "L_shoulder_ik_Ctrl", "R_eye_Ctrl", "eyeMaster_Ctrl", 
    "L_eye_Ctrl", "Root_Ctrl", "R_clavicleTrans_Ctrl", "L_elbow_pv_Ctrl", "L_knee_pv_Ctrl", "L_heel_ik_Ctrl", "L_hip_Ctrl", "R_heel_constraint_ik_Ctrl", "L_indexMid_fk_Ctrl", 
    "L_indexBase_fk_Ctrl", "L_thumbMid_fk_Ctrl", "L_thumbTip_fk_Ctrl", "L_arm_constraint_ik_Ctrl", "R_arm_constraint_ik_Ctrl", "L_heel_constraint_ik_Ctrl", "L_eyeMover_Ctrl", 
    "R_eyeMover_Ctrl", "R_elbow_pv_Ctrl", "R_elbow_sec_fk_Ctrl", "L_knee_sec_fk_Ctrl", "L_ball_fk_Ctrl", "R_knee_sec_fk_Ctrl", "L_wrist_sec_fk_Ctrl", "L_wrist_fk_Ctrl", 
    "R_upArm_sec_fk_Ctrl", "R_elbow_fk_Ctrl", "R_upArm_fk_Ctrl", "L_upLeg_sec_fk_Ctrl", "L_ankle_fk_Ctrl", "L_ankle_sec_fk_Ctrl", "L_knee_fk_Ctrl", "L_upLeg_fk_Ctrl", 
    "L_upArm_sec_fk_Ctrl", "L_upArm_fk_Ctrl", "L_elbow_fk_Ctrl", "R_wrist_sec_fk_Ctrl", "R_wrist_fk_Ctrl", "R_upLeg_fk_Ctrl", "R_upLeg_sec_fk_Ctrl", "R_ball_fk_Ctrl", "R_ankle_fk_Ctrl", 
    "R_ankle_sec_fk_Ctrl", "R_knee_fk_Ctrl", "L_elbow_sec_fk_Ctrl"]
    
    bendyBipedList = ["R_lowLeg_bendy_Ctrl", "R_knee_bendy_Ctrl", "R_upLeg_bendy_Ctrl", "L_lowLeg_bendy_Ctrl", "L_upLeg_bendy_Ctrl", "L_knee_bendy_Ctrl", "R_forearm_bendy_Ctrl", 
    "R_upArm_bendy_Ctrl", "R_elbow_bendy_Ctrl", "L_forearm_bendy_Ctrl", "L_elbow_bendy_Ctrl", "L_upArm_bendy_Ctrl"]
    
    
    radioCol = cmds.radioCollection(charCheck, query=True, sl=True)
    labelCheck = cmds.radioButton(radioCol, query=True, label=True)
    
    setsControls = setsInScene()
    checkBodyControlsPlus = []
    checkBendyControlsPlus = []
    checkFaceControlsPlus = []
    checkBodyControlsMin = []
    checkBendyControlsMin = []
    checkFaceControlsMin = []
     
    if len(setsControls)==0:
        print("-------------------------------------------------------------------------------------------------------------------------" + "\n")
        print ("\nSET DEI CONTROLLI MANCANTI IN SCENA!\n")
        return
        
    if "BODY_CONTROLS" in setsControls:
        #listBodyControls = cmds.sets("BODY_CONTROLS", query=True)
        cmds.select("BODY_CONTROLS", r=True)
        listBodyControls=cmds.ls(sl=True)
        cmds.select(cl=True)
        
        if labelCheck == "Biped":
            checkBodyControlsPlus=checkPlus(listBodyControls,bodyBipedList,optionalList)
            switchBody = bodyBipedList
        
        elif labelCheck == "Quadruped":
            checkBodyControlsPlus=checkPlus(listBodyControls,bodyQuadList,optionalList)
            switchBody = bodyQuadList
        
        elif labelCheck == "Biped Quad":
            checkBodyControlsPlus=checkPlus(listBodyControls,bodyBipedQuadList,optionalList)
            switchBody = bodyBipedQuadList
        
        checkBodyControlsMin=checkMinus(switchBody,listBodyControls)
    
    if "BENDY_CONTROLS" in setsControls:
        #listBendyControls = cmds.sets("BENDY_CONTROLS", query=True)
        cmds.select("BENDY_CONTROLS", r=True)
        listBendyControls=cmds.ls(sl=True)
        cmds.select(cl=True)
        
        
        if labelCheck == "Biped":
            checkBendyControlsPlus=checkPlus(listBendyControls,bendyBipedList,optionalList)
            switchBendy = bendyBipedList
        
        elif labelCheck == "Quadruped":
            checkBendyControlsPlus=checkPlus(listBendyControls,bendyQuadList,optionalList)
            switchBendy = bendyQuadList
        
        elif labelCheck == "Biped Quad":
            checkBendyControlsPlus=checkPlus(listBendyControls,bendyBipedQuadList,optionalList)
            switchBendy = bendyBipedQuadList
        
        checkBendyControlsMin=checkMinus(switchBendy,listBendyControls)
                
    if "FACE_CONTROLS" in setsControls:
        #listFaceControls = cmds.sets("FACE_CONTROLS", query=True)
        cmds.select("FACE_CONTROLS", r=True)
        listFaceControls=cmds.ls(sl=True)
        cmds.select(cl=True)
        
        checkFaceControlsPlus=checkPlus(listFaceControls,faceList,optionalList)
        checkFaceControlsMin=checkMinus(faceList,listFaceControls)
                
    print("-------------------------------------------------------------------------------------------------------------------------" + "\n")
    
    if (len(checkBodyControlsPlus) == 0) and (len(checkBodyControlsMin) == 0):
        print("I BODY CONTROLS SONO CORRETTI \n")
    else:
        if len(checkBodyControlsPlus)>0:
            print("\nNEL BODY CONTROLS HAI I SEGUENTI CONTROLLI DI TROPPO:")
            for elem in checkBodyControlsPlus:
                print(elem)
        if len(checkBodyControlsMin)>0:
            print("\nNEL BODY CONTROLS HAI I SEGUENTI CONTROLLI MANCANTI:")
            for elem in checkBodyControlsMin:
                print(elem)
        print ("\n")    
    print("-------------------------------------------------------------------------------------------------------------------------" + "\n")
       
    if (len(checkBendyControlsPlus) == 0) and (len(checkBendyControlsMin) == 0):
        print("I BENDY CONTROLS SONO CORRETTI \n")
    else:
        if len(checkBendyControlsPlus)>0:
            print("\nNEL BENDY CONTROLS HAI I SEGUENTI CONTROLLI DI TROPPO:")
            for elem in checkBendyControlsPlus:
                print(elem)
        if len(checkBendyControlsMin)>0:
            print("\nNEL BENDY CONTROLS HAI I SEGUENTI CONTROLLI MANCANTI:")
            for elem in checkBendyControlsMin:
                print(elem)
        print ("\n") 
    print("-------------------------------------------------------------------------------------------------------------------------" + "\n")
       
    if (len(checkFaceControlsPlus) == 0) and (len(checkFaceControlsMin) == 0):
        print("\nI FACE CONTROLS SONO CORRETTI \n")
    else:
        if len(checkFaceControlsPlus)>0:
            print("\nNEL FACE CONTROLS HAI I SEGUENTI CONTROLLI DI TROPPO:")
            for elem in checkFaceControlsPlus:
                print(elem)
        if len(checkFaceControlsMin)>0:
            print("\nNEL FACE CONTROLS HAI I SEGUENTI CONTROLLI MANCANTI:")
            for elem in checkFaceControlsMin:
                print(elem)
        print ("\n") 
    issues=checkFaceControlsPlus+checkBendyControlsPlus+checkBodyControlsPlus
    return issues

def getIndexInString(a,parsed_str):
    for i in range(len(parsed_str)):
        if parsed_str[i]==a:
            return i

def checkNaming():
    issues=[]
    issues_ctrl=[] #issue: _ctrl alla fine del nome
    issues_ikfk=[] #issue: ik o fk maiuscolo nel nome
    issues_side=[] #issue: lato (L_,R_,C_) scritto male (minuscolo o non all'inizio o "lf","lt" o "rt")
    issues_case=[] #issue: nome contiene stringhe maiuscole (escluse eccezioni -> vedi lista exception)
    setsControls = setsInScene()
    exception=["A","B","C","D","L","R","C","FT","BT","BK","UP","Ctrl","Transform","Root","Main","TransformPivot"]
    side=["L","R","C","FT","BT","BK","UP"]
    if len(setsControls)==0:
        print("-------------------------------------------------------------------------------------------------------------------------" + "\n")
        print ("\nSET DEI CONTROLLI MANCANTI IN SCENA!\n")
    else:
        cmds.select("ALL_CONTROLS", r=True)
        controls=cmds.ls(sl=True)
        cmds.select(cl=True)
        for c in controls:
            parsed=c.split("_")
            if parsed[-1] != "Ctrl":
                issues_ctrl.append(c)
            if ("CNT" in parsed):
                issues_ctrl.append(c)
            if ("IK" in parsed) or ("FK" in parsed) or ("Ik" in parsed) or ("Fk" in parsed):
                issues_ikfk.append(c)
            if ("lf" in parsed) or ("rt" in parsed) or ("lt" in parsed):
                issues_side.append(c)
            if ("l" in parsed) or ("r" in parsed) or ("c" in parsed):
                issues_side.append(c)
            for s in side:
                if (s in parsed):
                     index=getIndexInString(s,parsed)
                     if index!=0:
                         issues_side.append(c)
            for p in parsed:
                if p not in exception:
                    changed_p=p[0].lower()
                    if p[0]!=changed_p:
                        issues_case.append(c)
        issues = issues_ctrl+issues_ikfk+issues_side+issues_case
        if len(issues)==0:
            print("-------------------------------------------------------------------------------------------------------------------------" + "\n")
            print ("\nNON SONO STATI RILEVATI ERRORI DI NAMING.\n")
        if len(issues_ctrl)>0:
            print("-------------------------------------------------------------------------------------------------------------------------" + "\n")
            print ("\nTROVATO Ctrl ERRATO SUI SEGUENTI CONTROLLI:\n")
            for cnt in issues_ctrl:
                print (cnt)
        if len(issues_ikfk)>0:
            print("-------------------------------------------------------------------------------------------------------------------------" + "\n")
            print ("\nTROVATO ik/fk ERRATO SUI SEGUENTI CONTROLLI:\n")
            for cnt in issues_ikfk:
                print (cnt)
        if len(issues_side)>0:
            print("-------------------------------------------------------------------------------------------------------------------------" + "\n")
            print ("\nTROVATO LATO (L,R,C) ERRATO SUI SEGUENTI CONTROLLI:\n")
            for cnt in issues_side:
                print (cnt)
        if len(issues_case)>0:
            print ("------------------------------------------------------------------------------------")
            print ("\nTROVATE MAIUSCOLE ERRATE SUI SEGUENTI CONTROLLI:\n")
            for cnt in issues_case:
                print (cnt)
    return issues

def doubleNamesCmd(*args):
    global toSel
    toSel=doubleNames()
    return 

def checkSetsCmd(*args):
    global toSel
    toSel=checkSets()
    return 

def checkNamingCmd(*args):
    global toSel
    toSel=checkNaming()
    return 

def createWindow():
    global charCheck, toSel
    toSel=[]
    if (cmds.window("Check_naming_ui", q=True,exists=True)):
        cmds.deleteUI("Check_naming_ui", window=True)
        
    W=cmds.window("Check_naming_ui",title="CHECK NAMING UI", ip=True, w=200, h=200)    
    
    C0=cmds.columnLayout( adjustableColumn=True, rowSpacing=10, p=W,cat=("both",10),w=200,h=120)
    cmds.text("Select char type:",al="center")
    R1=cmds.rowLayout(numberOfColumns=3,p=C0)
    C1=cmds.columnLayout( adjustableColumn=True, rowSpacing=10, p=R1) 
    charCheck= cmds.radioCollection(p=C1) 
    biped= cmds.radioButton( label='Biped',p=C1)    
    C2=cmds.columnLayout( adjustableColumn=True, rowSpacing=10, p=R1)
    quad= cmds.radioButton( label='Quadruped',p=C2)
    C3=cmds.columnLayout( adjustableColumn=True, rowSpacing=10, p=R1)
    bipedQuad= cmds.radioButton( label='Biped Quad',p=C3)  
    charCheck = cmds.radioCollection(charCheck, edit=True, select=biped)
    
    R2=cmds.rowLayout(numberOfColumns=1,p=W, adj=True)
    C3=cmds.columnLayout( adjustableColumn=True, rowSpacing=10, p=R2)
    cmds.button(l="CHECK DOUBLE NAMES", c=doubleNamesCmd, w=150,h=30, bgc=[0.3, 0.3, 0.3],p=C3)
    cmds.button(l="CHECK NAMING", c=checkNamingCmd,w=150,h=30, bgc=[0.3, 0.3, 0.3],p=C3)
    cmds.button(l="CHECK SETS", c=checkSetsCmd, w=150,h=30, bgc=[0.3, 0.3, 0.3],p=C3)
    cmds.button(l="SELECT ISSUES", c=selIssues, w=150,h=30, bgc=[0.3, 0.3, 0.3],p=C3)
    
    cmds.showWindow( W )
    return

createWindow()