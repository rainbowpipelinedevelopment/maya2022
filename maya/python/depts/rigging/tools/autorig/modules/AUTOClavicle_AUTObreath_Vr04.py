import maya.cmds as mc
import maya.api.OpenMaya as om2

def AutoClavicleAutoBreath():

    def autoClavicleAndBreath(clav, clavCtrl, hand, handCtrl, ctrl):
    
        mc.select(clear = True)
        handleJoint = []
    
        clavPos = mc.xform(clav, rp = True, q = True, ws = True)
        handPos = mc.xform(hand, rp = True, q = True, ws = True)
    
        handleJoint.append(mc.joint(p = clavPos, name = "{}_autoClavicle".format(clav)))
        handleJoint.append(mc.joint(p = handPos, name = "{}_autoClavicle".format(hand)))
    
        autoHandle = mc.ikHandle(sj = handleJoint[0], ee = handleJoint[1], solver = 'ikRPsolver')[0]
    
        mc.hide(autoHandle)
    
        mc.parent(handleJoint[0], 'spine_end_jnt')
        mc.parent(autoHandle, ctrl)
    
        clavCtrlOffset = mc.group(empty = True, name = '{}_offset'.format(clavCtrl))
        mc.matchTransform(clavCtrlOffset, clavCtrl)
        clavCtrlParent = mc.listRelatives(clavCtrl, parent = True)
        mc.parent(clavCtrl, clavCtrlOffset)
        mc.parent(clavCtrlOffset, clavCtrlParent)
    
        mc.addAttr(ctrl, ln = 'clavicleFollow', min = 0, max = 1, keyable = True)
        conditionClav = mc.createNode('condition', name = "CON_{}_clavicleFollow".format(ctrl))
    
        multiplyClavFollow = mc.createNode('multiplyDivide', name = "MDL_{}_clavicleFollow".format(ctrl))
        multiplyClavWeight = mc.createNode('multiplyDivide', name = "MDL_{}_clavicleWeight".format(ctrl))
        multiplyClavAttr = mc.createNode('multiplyDivide', name = "MDL_{}_clavicleAttr".format(ctrl))
    
    
        if 'L' in clav:
            mc.setAttr("{}.input2Y".format(multiplyClavWeight), 0.6)
            mc.setAttr("{}.input2Z".format(multiplyClavWeight), 0.6)
            mc.setAttr("{}.operation".format(conditionClav), 2)
    
        else:
            mc.setAttr("{}.operation".format(conditionClav), 4)
            mc.setAttr("{}.input2Y".format(multiplyClavWeight), -0.6)
            mc.setAttr("{}.input2Z".format(multiplyClavWeight), -0.6)
    
        mc.connectAttr("{}.rotateZ".format(handleJoint[0]), "{}.firstTerm".format(conditionClav))
        mc.connectAttr("{}.rotateZ".format(handleJoint[0]), "{}.colorIfTrueR".format(conditionClav))
        mc.connectAttr("{}.outColorR".format(conditionClav), "{}.input1Z".format(multiplyClavWeight))
        mc.connectAttr("{}.rotateY".format(handleJoint[0]), "{}.input1Y".format(multiplyClavWeight))
    
        mc.connectAttr("{}.outputY".format(multiplyClavWeight), "{}.input1Y".format(multiplyClavAttr))
        mc.connectAttr("{}.outputZ".format(multiplyClavWeight), "{}.input1Z".format(multiplyClavAttr))
        mc.connectAttr("{}.Switch_IKFK".format(handCtrl), "{}.input2Y".format(multiplyClavAttr))
        mc.connectAttr("{}.Switch_IKFK".format(handCtrl), "{}.input2Z".format(multiplyClavAttr))
    
    
        mc.connectAttr("{}.outputY".format(multiplyClavAttr), "{}.input1Y".format(multiplyClavFollow))
        mc.connectAttr("{}.outputZ".format(multiplyClavAttr), "{}.input1Z".format(multiplyClavFollow))
        mc.connectAttr("{}.clavicleFollow".format(ctrl), "{}.input2Y".format(multiplyClavFollow))
        mc.connectAttr("{}.clavicleFollow".format(ctrl), "{}.input2Z".format(multiplyClavFollow))
    
        mc.connectAttr("{}.outputY".format(multiplyClavFollow), "{}.rotateY".format(clavCtrlOffset))
        mc.connectAttr("{}.outputZ".format(multiplyClavFollow), "{}.rotateZ".format(clavCtrlOffset))
    
        mc.setAttr("CON_L_arm_ik_Ctrl_clavicleFollow.colorIfFalseR", 0)
    
    
    def autoBreath():
    
    
        mc.group( em = True, n="GRP_L_clavicleTrans_Ctrl_Breath_offset")
        mc.matchTransform ('GRP_L_clavicleTrans_Ctrl_Breath_offset' , 'L_clavicleTrans_Ctrl')
        mc.parent('GRP_L_clavicleTrans_Ctrl_Breath_offset', 'L_clavicleTrans_Ctrl_offset')
        mc.parent('L_clavicleTrans_Ctrl','GRP_L_clavicleTrans_Ctrl_Breath_offset')
        mc.setAttr("CON_R_arm_ik_Ctrl_clavicleFollow.colorIfFalseR", 0)
    
    
        mc.group( em = True, n="GRP_R_clavicleTrans_Ctrl_Breath_offset")
        mc.matchTransform ('GRP_R_clavicleTrans_Ctrl_Breath_offset' , 'R_clavicleTrans_Ctrl')
        mc.parent('GRP_R_clavicleTrans_Ctrl_Breath_offset', 'R_clavicleTrans_Ctrl_offset')
        mc.parent('R_clavicleTrans_Ctrl','GRP_R_clavicleTrans_Ctrl_Breath_offset')
    
        mc.select ('spineHigh_Ctrl')
        mc.addAttr ( shortName='Breath', longName='Breath', defaultValue=0, minValue=0, maxValue=10,)
        mc.setAttr('spineHigh_Ctrl.Breath', k=True)
        mc.select ('spineHigh_Ctrl')
        mc.addAttr ( shortName='Follow_Breath', longName='Follow_Breath', defaultValue=0, minValue=0, maxValue=1,)
        mc.setAttr('spineHigh_Ctrl.Follow_Breath', k=True)
    
    
        mc.select ('spine_end_jnt')
        mc.group( em = True, n="GRP_Spine_End_Ctrl_Breath_Follow_offset")
        mc.matchTransform ('GRP_Spine_End_Ctrl_Breath_Follow_offset' , 'spine_end_jnt')
        mc.parent('GRP_Spine_End_Ctrl_Breath_Follow_offset', 'spine_hi_jnt')
        mc.parent('spine_end_jnt','GRP_Spine_End_Ctrl_Breath_Follow_offset')
    
        mc.delete('RIG_weightSpine_e_jnt_parentConstraint1', 'RIG_weightSpine_f_jnt_parentConstraint1',
                  'RIG_weightSpine_g_jnt_parentConstraint1')
        mc.delete('RIG_spineSquashStretchFinalVal_1_plsMns1')
    
        mc.select ('weightSpine_e_jnt')
        mc.group( em = True, n="GRP_weightSpine_e_jnt_Ctrl_Breath_offset")
        mc.matchTransform ('GRP_weightSpine_e_jnt_Ctrl_Breath_offset' , 'weightSpine_e_jnt')
        mc.parent('GRP_weightSpine_e_jnt_Ctrl_Breath_offset', 'weightSpine_grp')
        mc.parent('weightSpine_e_jnt','GRP_weightSpine_e_jnt_Ctrl_Breath_offset')
    
        mc.select ('weightSpine_f_jnt')
        mc.group( em = True, n="GRP_weightSpine_f_jnt_Ctrl_Breath_offset")
        mc.matchTransform ('GRP_weightSpine_f_jnt_Ctrl_Breath_offset' , 'weightSpine_f_jnt')
        mc.parent('GRP_weightSpine_f_jnt_Ctrl_Breath_offset', 'weightSpine_grp')
        mc.parent('weightSpine_f_jnt','GRP_weightSpine_f_jnt_Ctrl_Breath_offset')
    
        mc.select ('weightSpine_g_jnt')
        mc.group( em = True, n="GRP_weightSpine_g_jnt_Ctrl_Breath_offset")
        mc.matchTransform ('GRP_weightSpine_g_jnt_Ctrl_Breath_offset' , 'weightSpine_g_jnt')
        mc.parent('GRP_weightSpine_g_jnt_Ctrl_Breath_offset', 'weightSpine_grp')
        mc.parent('weightSpine_g_jnt','GRP_weightSpine_g_jnt_Ctrl_Breath_offset')
    
        mc.parentConstraint('RIG_ctrlSpine_e_jnt','GRP_weightSpine_e_jnt_Ctrl_Breath_offset', mo=True)
        mc.parentConstraint('RIG_ctrlSpine_f_jnt','GRP_weightSpine_f_jnt_Ctrl_Breath_offset', mo=True)
        mc.parentConstraint('RIG_ctrlSpine_g_jnt','GRP_weightSpine_g_jnt_Ctrl_Breath_offset', mo=True)
    
        mc.createNode('setRange', n='Set_range_SpineEnd_Breath')
        mc.setAttr("Set_range_SpineEnd_Breath.minY", 1)
        mc.setAttr("Set_range_SpineEnd_Breath.maxY", 1)
        mc.setAttr("Set_range_SpineEnd_Breath.oldMaxY", 1)
        mc.connectAttr('spineHigh_Ctrl.Follow_Breath', 'Set_range_SpineEnd_Breath.valueY')
        mc.connectAttr('Set_range_SpineEnd_Breath.outValueY', 'spine_end_jnt.scaleY')
    
        mc.createNode('multiplyDivide', n='MLD_L_Clavicle_Breath')
        mc.connectAttr('spineHigh_Ctrl.Follow_Breath', 'MLD_L_Clavicle_Breath.input2X')
        mc.connectAttr('spineHigh_Ctrl.Follow_Breath', 'MLD_L_Clavicle_Breath.input2Y')
        mc.connectAttr('MLD_L_Clavicle_Breath.outputX', 'GRP_L_clavicleTrans_Ctrl_Breath_offset.translateY')
        mc.connectAttr('MLD_L_Clavicle_Breath.outputY', 'GRP_L_clavicleTrans_Ctrl_Breath_offset.rotateZ')
    
        mc.createNode('multiplyDivide', n='MLD_R_Clavicle_Breath')
        mc.connectAttr('spineHigh_Ctrl.Follow_Breath', 'MLD_R_Clavicle_Breath.input2X')
        mc.connectAttr('spineHigh_Ctrl.Follow_Breath', 'MLD_R_Clavicle_Breath.input2Y')
        mc.connectAttr('MLD_R_Clavicle_Breath.outputX', 'GRP_R_clavicleTrans_Ctrl_Breath_offset.translateY')
        mc.connectAttr('MLD_R_Clavicle_Breath.outputY', 'GRP_R_clavicleTrans_Ctrl_Breath_offset.rotateZ')
    
        mc.setAttr("spineHigh_Ctrl.Breath", 0)
        mc.setDrivenKeyframe('MLD_L_Clavicle_Breath.input1X', cd='spineHigh_Ctrl.Breath')
        mc.setDrivenKeyframe('MLD_L_Clavicle_Breath.input1Y', cd='spineHigh_Ctrl.Breath')
        mc.setAttr("spineHigh_Ctrl.Breath", 10)
        mc.setAttr("MLD_L_Clavicle_Breath.input1X", 0.5)
        mc.setAttr("MLD_L_Clavicle_Breath.input1Y", -2)
        mc.setDrivenKeyframe('MLD_L_Clavicle_Breath.input1X', cd='spineHigh_Ctrl.Breath')
        mc.setDrivenKeyframe('MLD_L_Clavicle_Breath.input1Y', cd='spineHigh_Ctrl.Breath')
    
        mc.setAttr("spineHigh_Ctrl.Breath", 0)
        mc.setDrivenKeyframe('MLD_R_Clavicle_Breath.input1X', cd='spineHigh_Ctrl.Breath')
        mc.setDrivenKeyframe('MLD_R_Clavicle_Breath.input1Y', cd='spineHigh_Ctrl.Breath')
        mc.setAttr("spineHigh_Ctrl.Breath", 10)
        mc.setAttr("MLD_R_Clavicle_Breath.input1X", -0.5)
        mc.setAttr("MLD_R_Clavicle_Breath.input1Y", -2)
        mc.setDrivenKeyframe('MLD_R_Clavicle_Breath.input1X', cd='spineHigh_Ctrl.Breath')
        mc.setDrivenKeyframe('MLD_R_Clavicle_Breath.input1Y', cd='spineHigh_Ctrl.Breath')
    
        mc.setAttr("spineHigh_Ctrl.Breath", 0)
        mc.setDrivenKeyframe('Set_range_SpineEnd_Breath.maxY', cd='spineHigh_Ctrl.Breath')
        mc.setDrivenKeyframe('Set_range_SpineEnd_Breath.oldMaxY', cd='spineHigh_Ctrl.Breath')
        mc.setAttr("spineHigh_Ctrl.Breath", 10)
        mc.setAttr("Set_range_SpineEnd_Breath.maxY", 1.05)
        mc.setAttr("Set_range_SpineEnd_Breath.oldMaxY", 1.05)
        mc.setDrivenKeyframe('Set_range_SpineEnd_Breath.maxY', cd='spineHigh_Ctrl.Breath')
        mc.setDrivenKeyframe('Set_range_SpineEnd_Breath.oldMaxY', cd='spineHigh_Ctrl.Breath')
    
        mc.setAttr("spineHigh_Ctrl.Breath", 0)
        mc.setDrivenKeyframe('weightSpine_e_jnt.translateY', cd='spineHigh_Ctrl.Breath')
        mc.setDrivenKeyframe('weightSpine_e_jnt.scaleX', cd='spineHigh_Ctrl.Breath')
        mc.setAttr('spineHigh_Ctrl.Breath', 10)
        mc.setAttr('weightSpine_e_jnt.translateY',0)
        mc.setAttr('weightSpine_e_jnt.scaleX', 1.14)
        mc.setDrivenKeyframe('weightSpine_e_jnt.translateY', cd='spineHigh_Ctrl.Breath')
        mc.setDrivenKeyframe('weightSpine_e_jnt.scaleX', cd='spineHigh_Ctrl.Breath')
    
        mc.setAttr("spineHigh_Ctrl.Breath", 0)
        mc.setDrivenKeyframe('weightSpine_f_jnt.translateY', cd='spineHigh_Ctrl.Breath')
        mc.setDrivenKeyframe('weightSpine_f_jnt.scaleX', cd='spineHigh_Ctrl.Breath')
        mc.setAttr('spineHigh_Ctrl.Breath', 10)
        mc.setAttr('weightSpine_f_jnt.translateY',-1)
        mc.setAttr('weightSpine_f_jnt.scaleX', 1.059)
        mc.setDrivenKeyframe('weightSpine_f_jnt.translateY', cd='spineHigh_Ctrl.Breath')
        mc.setDrivenKeyframe('weightSpine_f_jnt.scaleX', cd='spineHigh_Ctrl.Breath')
    
        mc.setAttr("spineHigh_Ctrl.Breath", 0)
        mc.setDrivenKeyframe('weightSpine_g_jnt.translateY', cd='spineHigh_Ctrl.Breath')
        mc.setDrivenKeyframe('weightSpine_g_jnt.scaleX', cd='spineHigh_Ctrl.Breath')
        mc.setAttr('spineHigh_Ctrl.Breath', 10)
        mc.setAttr('weightSpine_g_jnt.translateY',-1.3)
        mc.setAttr('weightSpine_g_jnt.scaleX', 1.059)
        mc.setDrivenKeyframe('weightSpine_g_jnt.translateY', cd='spineHigh_Ctrl.Breath')
        mc.setDrivenKeyframe('weightSpine_g_jnt.scaleX', cd='spineHigh_Ctrl.Breath')
    
    
        mc.setAttr("spineHigh_Ctrl.Breath", 0)
        mc.setAttr("spineHigh_Ctrl.Follow_Breath", 1)
    
        mc.undoInfo(closeChunk=True)
    
    
    clavicleJnt = ['L_clavicle_jnt', 'R_clavicle_jnt']
    clavicleCtrl = ['L_clavicleTrans_Ctrl', 'R_clavicleTrans_Ctrl']
    handJnt = ['L_hand_jnt', 'R_hand_jnt']
    handControls = ['L_hand_Ctrl', 'R_hand_Ctrl']
    ikCtrl = ['L_arm_ik_Ctrl', 'R_arm_ik_Ctrl']
    
    for clav, clavCtrl, hand, handCtrl, ctrl in zip(clavicleJnt,clavicleCtrl, handJnt, handControls, ikCtrl):
    
        if not ((mc.objExists(clav)) or (mc.objExists(clavCtrl)) or (mc.objExists(hand))
            or (mc.objExists(handCtrl)) or (mc.objExists(ctrl))):
    
            om2.MGlobal.displayError("Uno dei seguenti elementi risulta assente o con un nome differente: {}, {}, {}, {}, {}"
                                     .format(clav, clavCtrl, hand, handCtrl, ctrl))
    
        else:
    
            autoClavicleAndBreath(clav, clavCtrl, hand, handCtrl, ctrl)
    
    try:
    
        mc.undoInfo(openChunk = True)
        autoBreath()
    
    except:
        mc.undoInfo(closeChunk=True)
        om2.MGlobal.displayError("lo script dell'autoBreath non ha funzionato, controllare file di "
                                 "riferimento per nomi dei seguenti gruppi:"
                                 "spineHigh_Ctrl, weightSpine_e_jnt, weightSpine_f_jnt, weightSpine_g_jnt,"
                                 "L/R_clavicleTrans_Ctrl")
                                 
                                 
                                     
AutoClavicleAutoBreath()