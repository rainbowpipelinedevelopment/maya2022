"""
- importare i controlli dei prop null
- importare i locator dei prop null e posizionarli
- controllare i nomi dei controlli se corrispondono
- lanciare tutto lo script
"""

from maya import cmds

def align(x, y):
    
    T = cmds.xform(x, ws=True, q=True, rp=True)
    R = cmds.xform(x, ws=True, q=True, ro=True)
    cmds.xform(y, ws=True, t=T)
    cmds.xform(y, ws=True, ro=R)

def prop_null(tail):

    #faccio la parte sinistra
    L_Ctrl = "L_propOffset_Ctrl"
    L_Ctrl_Grp = "L_propNULL_GRP"
    if (tail=="Present"):
        names = ["hand_L", "hand_R", "hip_L", "cog_L", "tail_L", "transform_L", "chest_L", "head_L"]
        ctrls = ["L_hand_Ctrl", "R_hand_Ctrl", "hip_Ctrl", "cog_Ctrl", "tail_06_Ctrl", "Transform_Null", "L_clavicleTrans_Ctrl", "head_Ctrl"]
    else:
        names = ["hand_L", "hand_R", "hip_L", "cog_L", "transform_L", "chest_L", "head_L"]
        ctrls = ["L_hand_Ctrl", "R_hand_Ctrl", "hip_Ctrl", "cog_Ctrl", "Transform_Null", "L_clavicleTrans_Ctrl", "head_Ctrl"]
    
    i = 0
    while i < len(ctrls):

        #allineo il gruppo del controllo a ogni locator e faccio il constrain azzerando il peso
        align("LOC_"+ names[i], L_Ctrl_Grp)
        constr = cmds.parentConstraint(ctrls[i], L_Ctrl_Grp, mo=True)
        cmds.setAttr(constr[0] + "." + ctrls[i] + "W" + str(i), 0)

        #creo il condition e gli connetto il follow del prop null, dopo connetto il condition al constrain
        condition = cmds.createNode("condition", n="CND_Prop_Null_L_" + names[i])
        cmds.setAttr(condition + ".colorIfTrueR", 1)
        cmds.setAttr(condition + ".colorIfFalseR", 0)
        cmds.setAttr(condition + ".secondTerm", i)
        cmds.connectAttr(L_Ctrl + ".follow", condition + ".firstTerm")
        cmds.connectAttr(condition + ".outColorR", constr[0] + "." + ctrls[i] + "W" + str(i))

        i += 1

    cmds.setAttr(constr[0] + ".interpType", 0)
    cmds.select(cl=True)

    #faccio la parte destra
    R_Ctrl = "R_propOffset_Ctrl"
    R_Ctrl_Grp = "R_propNULL_GRP"
    
    if (tail=="Present"):
        names = ["hand_L", "hand_R", "hip_R", "cog_R", "tail_R", "transform_R", "chest_R", "head_R"]
        ctrls = ["L_hand_Ctrl", "R_hand_Ctrl", "hip_Ctrl", "cog_Ctrl", "tail_06_Ctrl", "Transform_Ctrl", "R_clavicleTrans_Ctrl", "head_Ctrl"]
    else:
        names = ["hand_L", "hand_R", "hip_R", "cog_R", "transform_R", "chest_R", "head_R"]
        ctrls = ["L_hand_Ctrl", "R_hand_Ctrl", "hip_Ctrl", "cog_Ctrl", "Transform_Ctrl", "R_clavicleTrans_Ctrl", "head_Ctrl"]
    
    i = 0
    while i < len(ctrls):

        #allineo il gruppo del controllo a ogni locator e faccio il constrain azzerando il peso
        align("LOC_"+ names[i], R_Ctrl_Grp)
        constr = cmds.parentConstraint(ctrls[i], R_Ctrl_Grp, mo=True)
        cmds.setAttr(constr[0] + "." + ctrls[i] + "W" + str(i), 0)

        #creo il condition e gli connetto il follow del prop null, dopo connetto il condition al constrain
        condition = cmds.createNode("condition", n="CND_Prop_Null_R_" + names[i])
        cmds.setAttr(condition + ".colorIfTrueR", 1)
        cmds.setAttr(condition + ".colorIfFalseR", 0)
        cmds.setAttr(condition + ".secondTerm", i)
        cmds.connectAttr(R_Ctrl + ".follow", condition + ".firstTerm")
        cmds.connectAttr(condition + ".outColorR", constr[0] + "." + ctrls[i] + "W" + str(i))

        i += 1

    cmds.setAttr(constr[0] + ".interpType", 0)
    cmds.select(cl=True)