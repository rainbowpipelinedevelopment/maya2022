'''
    Rename Deformers UI
    
    INPUT PARAMETERS:
        
        Mesh Selection        viewport selection of meshes
        
    DESCRIPTION:
        
        The function renames the deformer driver node and set of the deformers affecting each item in Mesh Selection, according to the item name
'''

import maya.cmds as cmds

def renameDeformers_UI():
    #building UI
    winName = 'RD'
    if cmds.window(winName,exists=True):
        cmds.deleteUI(winName)
        cmds.windowPref(winName,remove=True) 
    cmds.window(winName,title='Rename Deformers UI',resizeToFitChildren=True,sizeable=False)
    mainLayout = cmds.formLayout(numberOfDivisions=100)    
    SK_button = cmds.button('Rename skinCluster',parent=mainLayout,height=40,width=200,command=renameSkinClusterCommand,statusBarMessage='rename skinCluster affecting selection')
    BS_button = cmds.button('Rename blendShape',parent=mainLayout,height=40,width=200,command=renameBlendShapeCommand,statusBarMessage='rename blendShape affecting selection')    
    LTC_button = cmds.button('Rename ffd',parent=mainLayout,height=40,width=200,command=renameFfdCommand,statusBarMessage='rename ffd affecting selection')    
    WIR_button = cmds.button('Rename wire',parent=mainLayout,height=40,width=200,command=renameWireCommand,statusBarMessage='rename wire affecting selection')   
    WRP_button = cmds.button('Rename wrap',parent=mainLayout,height=40,width=200,command=renameWrapCommand,statusBarMessage='rename wrap affecting selection')    
    SWR_button = cmds.button('Rename shrinkWrap',parent=mainLayout,height=40,width=200,command=renameShrinkWrapCommand,statusBarMessage='rename shrinkWrap affecting selection')        
    cmds.formLayout(mainLayout,edit=True,attachForm=([SK_button,'left',10],[SK_button,'top',10],[BS_button,'top',10],[LTC_button,'top',10],[LTC_button,'right',10],[WIR_button,'left',10],[WIR_button,'bottom',10],[WRP_button,'bottom',10],[SWR_button,'bottom',10],[SWR_button,'right',10]),
                                         attachControl=([BS_button,'left',10,SK_button],[BS_button,'right',10,LTC_button],[WIR_button,'top',10,SK_button],[WRP_button,'left',10,WIR_button],[WRP_button,'right',10,SWR_button],[WRP_button,'top',10,BS_button],[SWR_button,'top',10,LTC_button]))
    cmds.showWindow(winName)

def getDeformers(target):
    deformerList = cmds.findDeformers(target)
    print(deformerList)

    for item in deformerList:
        print(item)

    return deformerList
    
# def getDeformers(target):
#     deformerSets = cmds.listSets(object=target,type=2,extendToShape=True)
#     deformer_map = {}
#     for item in deformerSets:
#         deformerName = cmds.listConnections(item+'.usedBy[0]',source=True)
#         deformer_map.setdefault(deformerName[0],item)
#     return deformer_map

def cleanseSelection(selectionList):
    cleansedList = []    
    for item in selectionList:
        selectionType = cmds.objectType(item)
        if selectionType == 'transform':
            selectionShapes = cmds.listRelatives(item,shapes=True)
            if selectionShapes:
                selectionShapeType = cmds.objectType(selectionShapes[0])
                if selectionShapeType == 'mesh':
                    cleansedList.append(item)
                else:
                    cmds.warning('Skipping "'+item+'": not a valid mesh')
            else:
                cmds.warning('Skipping "'+item+'": not a valid mesh')                            
        elif selectionType == 'mesh':
            selectionParent = cmds.listRelatives(item,parent=True)
            cleansedList.append(selectionParent)
        else:
            cmds.warning('Skipping "'+item+'": not a valid mesh')
    return cleansedList
            
def cleanseName(name):
    cleansed_name = name.replace('MSH_','')
    cleansed_name = cleansed_name.replace('Msh_','')    
    cleansed_name = cleansed_name.replace('msh_','')    
    cleansed_name = cleansed_name.replace('_MSH','')    
    cleansed_name = cleansed_name.replace('_Msh','')        
    cleansed_name = cleansed_name.replace('_msh','')  
    return cleansed_name      
                    
def renameSkinClusterCommand(*args):
    selectionList = cmds.ls(selection=True)
    if selectionList:
        cleansedList = cleanseSelection(selectionList)
        for item in cleansedList:
            affectingDeformers = getDeformers(item)
            for token in affectingDeformers:
                deformerType = cmds.objectType(token)
                if deformerType == 'skinCluster':
                    newDeformerName = 'SK_' + cleanseName(item)
                    cmds.rename(token,newDeformerName)     
    else:
        cmds.warning('No deformer renamed: select at least a valid mesh')

def renameBlendShapeCommand(*args):
    selectionList = cmds.ls(selection=True)
    if selectionList:
        cleansedList = cleanseSelection(selectionList)
        for item in cleansedList:
            affectingDeformers = getDeformers(item)
            for token in affectingDeformers:
                deformerType = cmds.objectType(token)
                if deformerType == 'blendShape':
                    newDeformerName = 'BS_' + cleanseName(item)
                    cmds.rename(token,newDeformerName)     
    else:
        cmds.warning('No deformer renamed: select at least a valid mesh')

def renameFfdCommand(*args):
    selectionList = cmds.ls(selection=True)
    if selectionList:
        cleansedList = cleanseSelection(selectionList)
        for item in cleansedList:
            affectingDeformers = getDeformers(item)
            for token in affectingDeformers:
                deformerType = cmds.objectType(token)
                if deformerType == 'ffd':
                    newDeformerName = 'LTC_' + cleanseName(item)
                    cmds.rename(token,newDeformerName)     
    else:
        cmds.warning('No deformer renamed: select at least a valid mesh')

def renameWireCommand(*args):
    selectionList = cmds.ls(selection=True)
    if selectionList:
        cleansedList = cleanseSelection(selectionList)
        for item in cleansedList:
            affectingDeformers = getDeformers(item)
            for token in affectingDeformers:
                deformerType = cmds.objectType(token)
                if deformerType == 'wire':
                    newDeformerName = 'WIR_' + cleanseName(item)
                    cmds.rename(token,newDeformerName)     
    else:
        cmds.warning('No deformer renamed: select at least a valid mesh')

def renameWrapCommand(*args):
    selectionList = cmds.ls(selection=True)
    if selectionList:
        cleansedList = cleanseSelection(selectionList)
        for item in cleansedList:
            affectingDeformers = getDeformers(item)
            for token in affectingDeformers:
                deformerType = cmds.objectType(token)
                if deformerType == 'wrap':
                    newDeformerName = 'WRP_' + cleanseName(item)
                    cmds.rename(token,newDeformerName)     
    else:
        cmds.warning('No deformer renamed: select at least a valid mesh')

def renameShrinkWrapCommand(*args):
    selectionList = cmds.ls(selection=True)
    if selectionList:
        cleansedList = cleanseSelection(selectionList)
        for item in cleansedList:
            affectingDeformers = getDeformers(item)
            for token in affectingDeformers:
                deformerType = cmds.objectType(token)
                if deformerType == 'shrinkWrap':
                    newDeformerName = 'SWR_' + cleanseName(item)
                    cmds.rename(token,newDeformerName)     
    else:
        cmds.warning('No deformer renamed: select at least a valid mesh')
        