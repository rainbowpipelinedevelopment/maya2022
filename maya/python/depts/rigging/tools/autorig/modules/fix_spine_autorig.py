#//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#RBW AUTORIG DEF
#//////////////////////////////////////////////////////////////////////////////////////////////////////////////

#//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#PLACER SCRIPT AB FIXES
#//////////////////////////////////////////////////////////////////////////////////////////////////////////////
import maya.cmds as cmds
import maya.mel as mel

def rbwFixedSpine():

    #create GRP meshes RM
    cmds.group(em=True, name='GRP_Meshes_grp')
    cmds.group(em=True, name='GRP_Meshes_RM')
    cmds.group(em=True, name='GRP_Meshes_RND')
    cmds.parent('GRP_Meshes_RM', 'GRP_Meshes_grp')
    cmds.parent('GRP_Meshes_RND', 'GRP_Meshes_grp')

    #collect placer controls
    root_control = "RIG_root_ctrl";
    master_control = "RIG_master_ctrl";
    submaster_control = "RIG_subMaster_ctrl";

    #reset root group and save notes 
    cmds.select(root_control)
    cmds.pickWalk(direction = "up");
    root_control_frz_group = cmds.ls(sl=True)[0];
    print (root_control_frz_group);
    root_control_offset_z = cmds.getAttr(root_control_frz_group + ".tz");
    cmds.select(root_control);
    root_control_offset_group = cmds.group(n=root_control + "_offset_group");
    cmds.setAttr(root_control_offset_group + ".tz", -root_control_offset_z);

    #duplicate root_control
    new_root_control = cmds.duplicate(root_control);
    root_control_hidden = cmds.rename(root_control, root_control + "_HIDDEN");
    new_root_control = cmds.rename(new_root_control,root_control);
    cmds.parent(root_control_hidden, new_root_control);

    #adding attribute to "RIG_Transform_ctrl"
    transformCtrl = root_control
    cmds.select(transformCtrl)
    cmds.addAttr(ln="L_Arm_01_IKFK", at="float", k=True, hxv=True, hnv=True, max=1, min=0, dv=1)
    cmds.addAttr(ln="R_Arm_01_IKFK", at="float", k=True, hxv=True, hnv=True, max=1, min=0, dv=1)
    cmds.addAttr(ln="L_Leg_01_IKFK", at="float", k=True, hxv=True, hnv=True, max=1, min=0, dv=1)
    cmds.addAttr(ln="R_Leg_01_IKFK", at="float", k=True, hxv=True, hnv=True, max=1, min=0, dv=1)
    #cmds.addAttr(ln="Tail_01_IKFK", at="float", k=True, hxv=True, hnv=True, max=1, min=0, dv=1)
    cmds.addAttr(ln="Geo_Vis", at="enum", en="off:on", dv=1)
    cmds.setAttr(transformCtrl + ".Geo_Vis", k=False, cb=True)
    cmds.addAttr(ln="Head_Geo_Vis", at="enum", en="off:on", dv=1)
    cmds.setAttr(transformCtrl + ".Head_Geo_Vis", k=False, cb=True)
    cmds.addAttr(ln="Mouth_Geo_Vis", at="enum", en="off:on", dv=1)
    cmds.setAttr(transformCtrl + ".Mouth_Geo_Vis", k=False, cb=True)
    cmds.addAttr(ln="Arms_Geo_Vis", at="enum", en="off:on", dv=1)
    cmds.setAttr(transformCtrl + ".Arms_Geo_Vis", k=False, cb=True)
    cmds.addAttr(ln="Legs_Geo_Vis", at="enum", en="off:on", dv=1)
    cmds.setAttr(transformCtrl + ".Legs_Geo_Vis", k=False, cb=True)
    cmds.addAttr(ln="Claws_Geo_Vis", at="enum", en="off:on", dv=1)
    cmds.setAttr(transformCtrl + ".Claws_Geo_Vis", k=True, cb=False)
    cmds.addAttr(ln="Clothes_Geo_Vis", at="enum", en="off:on", dv=1)
    cmds.setAttr(transformCtrl + ".Clothes_Geo_Vis", k=False, cb=True)
    cmds.addAttr(ln="Geo_Res", at="enum", en="HiRes:Proxy")
    cmds.setAttr(transformCtrl + ".Geo_Res", k=False, cb=True)
    cmds.addAttr(ln="Body_Cage_Fur", at="enum", en="off:on", dv=0)
    cmds.setAttr(transformCtrl + ".Body_Cage_Fur", k=False, cb=True)
    #cmds.addAttr(ln="Transparency_Body_Cage_Fur", at="float", k=True, hxv=True, hnv=True, max=10, min=0, dv=10)
    cmds.addAttr(ln="Contact_Helper_Foot", at="enum", en="off:on", dv=1)
    cmds.setAttr(transformCtrl + ".Contact_Helper_Foot", k=False, cb=True)
    cmds.addAttr(ln="Main_Ctrl_Vis", at="enum", en="off:on", dv=1)
    cmds.setAttr(transformCtrl + ".Main_Ctrl_Vis", k=False, cb=True)
    cmds.addAttr(ln="Root_Ctrl_Vis", at="enum", en="off:on", dv=1)
    cmds.setAttr(transformCtrl + ".Root_Ctrl_Vis", k=False, cb=True)

    #connecting new attributes to old and hide it
    cmds.connectAttr(transformCtrl+".Geo_Vis", "GRP_Meshes_grp.visibility", f=True)

    #add Pivot Vis Attr
    cmds.addAttr(root_control, ln= "Pivot_Vis", at="enum", en = "off:on");
    cmds.setAttr(root_control + ".Pivot_Vis", e=True, keyable=False, cb=True);
    cmds.setAttr(root_control + ".Pivot_Vis", 1);

    cmds.hide(root_control_hidden + "Shape");

    pivot_control = cmds.curve( d = 1, p=[(2, 0, 5), (2, 0, 2), (5, 0, 2), (5, 0, -2), (2, 0, -2), (2, 0, -5), (-2, 0, -5), (-2, 0, -2), (-5, 0, -2), (-5, 0, 2), (-2, 0, 2), (-2, 0, 5), (2, 0, 5)] );
    cmds.setAttr(pivot_control + ".ry", 45);
    cmds.setAttr(pivot_control + ".scaleX", 2);
    cmds.setAttr(pivot_control + ".scaleY", 2);
    cmds.setAttr(pivot_control + ".scaleZ", 2);
    cmds.makeIdentity(pivot_control, apply=True);
    pivot_control = cmds.rename(pivot_control,"RIG_TransformPivot_Ctrl");
    cmds.select(pivot_control);
    pivot_control_group = cmds.group(n="Transform_Pivot_Ctrl_Grp");
    cmds.connectAttr(root_control + ".Pivot_Vis", pivot_control_group + ".v");
    cmds.setAttr("RIG_TransformPivot_Ctrl.v", lock=True, keyable=False, channelBox=False)

    #parent and connect 
    cmds.parent(pivot_control_group, root_control_hidden);
    cmds.connectAttr(pivot_control + ".translate", root_control + ".rotatePivot");
    cmds.connectAttr(pivot_control + ".translate", root_control + ".scalePivot");

    #change ctrl colors
    cmds.setAttr(root_control + "Shape.overrideEnabled", 1);
    cmds.setAttr(root_control + "Shape.overrideColor", 17);
    cmds.setAttr(master_control + "Shape.overrideEnabled", 1);
    cmds.setAttr(master_control + "Shape.overrideColor", 20);
    cmds.setAttr(submaster_control + "Shape.overrideEnabled", 1);
    cmds.setAttr(submaster_control + "Shape.overrideColor", 18);
    cmds.setAttr(pivot_control + ".overrideEnabled",1);
    cmds.setAttr(pivot_control + ".overrideColor",20);

    #rename placers
    cmds.rename(master_control, "Root_Ctrl");
    cmds.rename(submaster_control, "Main_Ctrl");
    cmds.rename(root_control, "Transform_Ctrl");
    cmds.select(cl=True)

    #connection between Transform and HIDDEN
    cmds.connectAttr ('Transform_Ctrl.masterScale', 'RIG_root_ctrl_HIDDEN.masterScale')
    cmds.connectAttr ('Transform_Ctrl.skeletonVis', 'RIG_root_ctrl_HIDDEN.skeletonVis')
    cmds.connectAttr ('Transform_Ctrl.masterCtrlVis', 'RIG_root_ctrl_HIDDEN.masterCtrlVis')
    cmds.connectAttr ('Transform_Ctrl.subMasterCtrlVis', 'RIG_root_ctrl_HIDDEN.subMasterCtrlVis')
    cmds.connectAttr ('Transform_Ctrl.rigSettingsCtrlVis', 'RIG_root_ctrl_HIDDEN.rigSettingsCtrlVis')
    cmds.connectAttr ('Transform_Ctrl.masterSpineCtrlVis', 'RIG_root_ctrl_HIDDEN.masterSpineCtrlVis')
    cmds.connectAttr ('Transform_Ctrl.worldSpaceCtrlVis', 'RIG_root_ctrl_HIDDEN.worldSpaceCtrlVis')
    cmds.setAttr ('Transform_Ctrl.v', k=False, cb=False)
    cmds.setAttr ('Transform_Ctrl.skeletonVis', k=False, cb=False)
    cmds.connectAttr ('Transform_Ctrl.Main_Ctrl_Vis', 'Transform_Ctrl.subMasterCtrlVis')
    cmds.setAttr ('Transform_Ctrl.subMasterCtrlVis', k=False, cb=False)
    cmds.setAttr ('Transform_Ctrl.Main_Ctrl_Vis', 1)
    cmds.connectAttr ('Transform_Ctrl.Root_Ctrl_Vis', 'Transform_Ctrl.masterCtrlVis')
    cmds.setAttr ('Transform_Ctrl.masterCtrlVis', k=False, cb=False)
    cmds.setAttr ('Transform_Ctrl.Root_Ctrl_Vis', 1)
    cmds.setAttr ('Transform_Ctrl.rigSettingsCtrlVis', k=False, cb=False)
    cmds.setAttr ('Transform_Ctrl.rigSettingsCtrlVis', 0)
    cmds.setAttr ('Transform_Ctrl.masterSpineCtrlVis', k=False, cb=False)
    cmds.setAttr ('Transform_Ctrl.masterSpineCtrlVis', 0)
    cmds.setAttr ('Transform_Ctrl.worldSpaceCtrlVis', k=False, cb=False)
    cmds.setAttr ('Transform_Ctrl.worldSpaceCtrlVis', 0)
    cmds.disconnectAttr ('RIG_root_ctrl_HIDDEN.subMasterCtrlVis', 'Main_Ctrl.v')
    cmds.connectAttr ('RIG_root_ctrl_HIDDEN.subMasterCtrlVis', 'Main_CtrlShape.v')
    cmds.disconnectAttr ('RIG_root_ctrl_HIDDEN.masterCtrlVis', 'Root_Ctrl.v')
    cmds.connectAttr ('RIG_root_ctrl_HIDDEN.masterCtrlVis', 'Root_CtrlShape.v')

    #connection for MS
    cmds.group ('RIG_root_ctrl_HIDDEN', n='group1')
    cmds.parent ('group1', 'RIG_RIG_root_ctrl_frz_grp')
    cmds.delete ('RIG_RIG_root_ctrl_frz_grp_parentConstraint1')
    cmds.delete ('RIG_RIG_subMaster_ctrl_frz_grp_parentConstraint1')
    cmds.parent ('RIG_RIG_subMaster_ctrl_frz_grp', 'Root_Ctrl')
    cmds.parent ('RIG_root_ctrl_offset_group', 'Main_Ctrl')
    cmds.setAttr ('group1.translateZ', 0)
    cmds.parent ('RIG_root_ctrl_HIDDEN', 'RIG_RIG_root_ctrl_frz_grp')
    cmds.delete ('group1')
    cmds.group(em=True, name='Transform_Null')
    cmds.parent ('Transform_Null', 'Main_Ctrl')
    cmds.rename ('RIG_root_ctrl_offset_group', 'Transform_Ctrl_Grp')
    cmds.rename ('RIG_RIG_subMaster_ctrl_frz_grp', 'Main_Ctrl_Grp')
    cmds.rename ('RIG_RIG_master_ctrl_frz_grp', 'Root_Ctrl_Grp')
    cmds.parent("Transform_Pivot_Ctrl_Grp", "Main_Ctrl")
    cmds.setAttr("Transform_Pivot_Ctrl_Grp.tz", 0)
    cmds.parentConstraint ('Transform_Null', 'RIG_RIG_root_ctrl_frz_grp', mo=True)
    cmds.setAttr ('Transform_Ctrl.Main_Ctrl_Vis', 0)
    cmds.setAttr ('Transform_Ctrl.Root_Ctrl_Vis', 0)

    cmds.setAttr("Transform_Ctrl.sx", k=True, l=False)
    cmds.setAttr("Transform_Ctrl.sy", k=True, l=False)
    cmds.setAttr("Transform_Ctrl.sz", k=True, l=False)
    cmds.connectAttr ('Transform_Ctrl.masterScale', 'Transform_Ctrl.sx')
    cmds.connectAttr ('Transform_Ctrl.masterScale', 'Transform_Ctrl.sy')
    cmds.connectAttr ('Transform_Ctrl.masterScale', 'Transform_Ctrl.sz')
    cmds.setAttr("Transform_Ctrl.sx", k=False, l=True)
    cmds.setAttr("Transform_Ctrl.sy", k=False, l=True)
    cmds.setAttr("Transform_Ctrl.sz", k=False, l=True)
    cmds.disconnectAttr ('RIG_head_a_jnt.scale', 'RIG_head_rig_grp.scale')

    cmds.parent("Transform_Ctrl_Grp", "RIG_rig_grp")
    cmds.parent("Root_Ctrl_Grp", "Transform_Ctrl")

    cmds.select ( cl=True )
    #mettere se necessario il valore inverso di Tz sul 'Transform Null' che leggi dal 'RIG_RIG_root_ctrl_frz_grp'

    #remove unuseful things
    cmds.delete ('RIG_RIG_head_b_ctrl_frz_grp')
    cmds.delete ('RIG_RIG_head_c_ctrl_frz_grp')
    cmds.delete ('RIG_jawCtrl_grp')
    cmds.parent ('RIG_head_d_jnt', 'RIG_head_a_jnt')
    cmds.parent ('RIG_lf_eyeMover_jnt', 'RIG_head_a_jnt')
    cmds.parent ('RIG_rt_eyeMover_jnt', 'RIG_head_a_jnt')
    cmds.delete ('RIG_head_b_jnt')
    cmds.delete ('RIG_jaw_a_jnt')
    cmds.setAttr ("RIG_eyeMaster_ctrl.head_bCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_rt_eye_ctrl.eyeMoverCtrlVis", 1)
    cmds.setAttr ("RIG_lf_eye_ctrl.eyeMoverCtrlVis", 1)
    cmds.parent ('RIG_RIG_lf_eyeMover_ctrl_frz_grp', 'RIG_head_ctrl')
    cmds.setAttr("RIG_lf_eyeMover_ctrlShape.overrideColor", 17)
    cmds.parent ('RIG_RIG_rt_eyeMover_ctrl_frz_grp', 'RIG_head_ctrl')
    cmds.setAttr("RIG_rt_eyeMover_ctrlShape.overrideColor", 17)
    cmds.setAttr ("RIG_eyeMaster_ctrl.rootCtrl", 1)
    cmds.select ( cl=True )
    
    #add scale attribute to hip and mid spine controls
    cmds.setAttr("RIG_hip_ctrl.sx",k=True,lock=False);
    cmds.setAttr("RIG_hip_ctrl.sy",k=True,lock=False);
    cmds.setAttr("RIG_hip_ctrl.sz",k=True,lock=False);

    cmds.setAttr("RIG_spineMid_1_ctrl.sx",k=True,lock=False);
    cmds.setAttr("RIG_spineMid_1_ctrl.sy",k=True,lock=False);
    cmds.setAttr("RIG_spineMid_1_ctrl.sz",k=True,lock=False);

    cmds.scaleConstraint("RIG_hip_ctrl", "RIG_hip_jnt", mo=True, w=1.0);
    cmds.setAttr("RIG_hip_jnt.segmentScaleCompensate",0);
    cmds.parentConstraint("RIG_spineMid_1_ctrl", "weightSpine_grp", mo=True, w=1.0);
    cmds.scaleConstraint("RIG_spineMid_1_ctrl", "weightSpine_grp", mo=True, w=1.0);

    #fix spine follicles
    try:
        cmds.select("RIG_root_ctrl_HIDDEN");
    except:
        cmds.warning("RIG_root_ctrl_HIDDEN not found, using RIG_root_ctrl instead");
        cmds.select("RIG_root_ctrl");
        
    root_control = cmds.ls(sl=True)[0];
    spine_follicles = cmds.listRelatives("RIG_spineRhFollicles_grp",c=True);
    for follicle in spine_follicles:
        cmds.scaleConstraint(root_control, follicle, mo=True, w=1.0)

    #name follicles
    cmds.rename ("follicle1", "FLC_spine_01")
    cmds.rename ("follicle2", "FLC_spine_02")
    cmds.rename ("follicle3", "FLC_spine_03")
    cmds.rename ("follicle4", "FLC_spine_04")
    cmds.rename ("follicle5", "FLC_spine_05")
    cmds.rename ("follicle6", "FLC_spine_06")

    cmds.select ( cl=True )
    
    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #ALIGN FOLLOW SWITCH
    #Sostituisce l'attributo align con un attributo follow con funzionamento inverso
    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////

    cmds.select ( "RIG_head_ctrl", "RIG_neck_ctrl" )

    controls = cmds.ls(sl=True);
    for control in controls:
        cmds.addAttr(control, ln= "follow", at="float", dv = 1, min = 0, max = 1);
        cmds.setAttr(control + ".follow", e=True, keyable=True);
        reverse_node = cmds.shadingNode("reverse", asUtility = True, n="REV_align_" + control);
        cmds.connectAttr(control + ".follow", reverse_node + ".input.inputX");
        cmds.connectAttr(reverse_node + ".output.outputX", control + ".align");
        cmds.setAttr(control + ".align", e=True, keyable = False, lock=True, channelBox = False);
        cmds.select ( cl=True )
            
    cmds.select ( cl=True )
    
    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #SPACE SWITCH FIXED
    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    eye = ["RIG_eyeMaster_ctrl"]
    cmds.select(eye)
    cmds.addAttr(ln="Follow", at="enum", en="World:Transform:Cog:Head", dv=1)
    cmds.setAttr("RIG_eyeMaster_ctrl.Follow", k=True)
    cmds.select(cl=True)
    
    # Creo e collego i condition per gli occhi
    eye_L_World = cmds.createNode("condition", n="CND_eyeMaster_Follow_World")
    eye_L_Transform = cmds.createNode("condition", n="CND_eyeMaster_Follow_Transform")
    eye_L_Cog = cmds.createNode("condition", n="CND_eyeMaster_Follow_Cog")
    eye_L_Head = cmds.createNode("condition", n="CND_eyeMaster_Follow_Head")

    eyeCND = [eye_L_World, eye_L_Transform, eye_L_Cog, eye_L_Head]

    for eyes in eyeCND:
        cmds.setAttr(eyes + ".colorIfTrueR", 1)
        cmds.setAttr(eyes + ".colorIfTrueG", 1)
        cmds.setAttr(eyes + ".colorIfTrueB", 1)
        cmds.setAttr(eyes + ".colorIfFalseR", 0)
        cmds.setAttr(eyes + ".colorIfFalseG", 0)
        cmds.setAttr(eyes + ".colorIfFalseB", 0)
        cmds.connectAttr("RIG_eyeMaster_ctrl.Follow", eyes + ".firstTerm")

    cmds.setAttr(eye_L_Transform + ".secondTerm", 1)
    cmds.setAttr(eye_L_Cog + ".secondTerm", 2)
    cmds.setAttr(eye_L_Head + ".secondTerm", 3)

    cmds.connectAttr(eye_L_World + ".outColorR", "RIG_eyeMaster_ctrl.worldSpaceCtrl")
    cmds.connectAttr(eye_L_Transform + ".outColorR", "RIG_eyeMaster_ctrl.rootCtrl")
    cmds.connectAttr(eye_L_Cog + ".outColorR", "RIG_eyeMaster_ctrl.cogCtrl")
    cmds.connectAttr(eye_L_Head + ".outColorR", "RIG_eyeMaster_ctrl.headCtrl")

    cmds.setAttr("RIG_eyeMaster_ctrl.Follow", 3)

    #lockandhide connected attrs
    cmds.setAttr ("RIG_eyeMaster_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_eyeMaster_ctrl.rootCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_eyeMaster_ctrl.worldSpaceCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_eyeMaster_ctrl.cogCtrl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_eyeMaster_ctrl.headCtrl", lock=True, keyable=False, channelBox=False)
    
    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #CHIUSURA
    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    cmds.setAttr("RIG_head_ctrl.useRootSpace", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_neck_ctrl.useRootSpace", lock=True, keyable=False, channelBox=False )
    
    cmds.setAttr ("RIG_spineHigh_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineHigh_ctrl.rootCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineHigh_ctrl.worldSpaceCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineHigh_ctrl.cogCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineHigh_ctrl.midSpineCtrl_1", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineMid_1_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineMid_1_ctrl.rootCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineMid_1_ctrl.worldSpaceCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineMid_1_ctrl.cogCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineMid_1_ctrl.lowSpineCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineLow_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineLow_ctrl.rootCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineLow_ctrl.worldSpaceCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_spineLow_ctrl.cogCtrl", lock=True, keyable=False, channelBox=False )
    
    cmds.setAttr ("RIG_lf_eye_ctrl.eyeMoverCtrlVis", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_rt_eye_ctrl.eyeMoverCtrlVis", lock=True, keyable=False, channelBox=False )

    cmds.setAttr ("RIG_hip_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_hip_ctrl.rootCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_hip_ctrl.worldSpaceCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_hip_ctrl.cogCtrl", lock=True, keyable=False, channelBox=False )

    cmds.setAttr ("RIG_cog_ctrl.autoSquashStretch", 0)
    cmds.setAttr ("RIG_cog_ctrl.spineShaper", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_cog_ctrl.stretchValue", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_cog_ctrl.autoSquashStretch", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_cog_ctrl.squashStretch", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_cog_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_cog_ctrl.rootCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_cog_ctrl.worldSpaceCtrl", lock=True, keyable=False, channelBox=False )
    
    cmds.select ('RIG_Skeleton_grp', 'RIG_rig_grp', 'GRP_Meshes_grp')
    cmds.group(n='CHAR')
    cmds.select ( cl=True )
    
    try:
        cmds.delete('abRTDeleteMeWhenDone')
    except:
        print("--- skipped " + "abRTDeleteMeWhenDone" + "\n");

    try:
        cmds.rename('RIG_rig_grp', 'RIG_controls_grp')
    except:
        print("--- skipped " + "RIG_rig_grp" + "\n");
    
    cmds.select(cl=True)
        
    print("--- END SCRIPT ---" + "\n");
    
    cmds.delete("lf_legWeightJnt_grp", "rt_legWeightJnt_grp", "RIG_lf_upLeg_jnt", "RIG_rt_upLeg_jnt", "RIG_lf_clavicle_jnt", "RIG_rt_clavicle_jnt", "lf_armWeightJnt_grp", "rt_armWeightJnt_grp")
    cmds.rename("RIG_spineMid_1_ctrl", "RIG_spineMid_ctrl")
    
    #######################################
    #####FIX SEGMENT SCALE COMPENSATE######
    #######################################
    cmds.setAttr("RIG_lf_eye_jnt.segmentScaleCompensate", 0)
    cmds.setAttr("RIG_rt_eye_jnt.segmentScaleCompensate", 0)
    cmds.setAttr("RIG_lf_eyeMover_jnt.segmentScaleCompensate", 0)
    cmds.setAttr("RIG_rt_eyeMover_jnt.segmentScaleCompensate", 0)
    
    #######################################
    #####ADD SPINE FOLLOW ATTR######
    #######################################
    
    cmds.addAttr("RIG_spineMid_ctrl", ln="follow_translate", at="float", k=True, dv=1, min=0, max=1)
    cmds.addAttr("RIG_spineMid_ctrl", ln="follow_rotate", at="float", k=True, dv=1, min=0, max=1)
    cmds.addAttr("RIG_spineHigh_ctrl", ln="follow_translate", at="float", k=True, dv=1, min=0, max=1)
    cmds.addAttr("RIG_spineHigh_ctrl", ln="follow_rotate", at="float", k=True, dv=1, min=0, max=1)
    rev_mid = cmds.createNode("reverse", n="REV_Follow_spineMid")
    rev_high = cmds.createNode("reverse", n="REV_Follow_spineHigh")
    constr_trans_mid = cmds.parentConstraint("RIG_cog_ctrl","RIG_spineLow_ctrl", "RIG_RIG_spineMid_1_ctrl_frz_grp", mo=True, sr=("x","y","z"))[0]
    constr_rot_mid = cmds.parentConstraint("RIG_cog_ctrl","RIG_spineLow_ctrl", "RIG_RIG_spineMid_1_ctrl_frz_grp", mo=True, st=("x","y","z"))[0]
    constr_trans_high = cmds.parentConstraint("RIG_cog_ctrl","RIG_spineMid_ctrl", "RIG_RIG_spineHigh_ctrl_frz_grp1", mo=True, sr=("x","y","z"))[0]
    constr_rot_high = cmds.parentConstraint("RIG_cog_ctrl","RIG_spineMid_ctrl", "RIG_RIG_spineHigh_ctrl_frz_grp1", mo=True, st=("x","y","z"))[0]

    cmds.connectAttr("RIG_spineMid_ctrl.follow_translate", "REV_Follow_spineMid.inputX")
    cmds.connectAttr("RIG_spineMid_ctrl.follow_rotate", "REV_Follow_spineMid.inputY")
    cmds.connectAttr("REV_Follow_spineMid.outputX", constr_trans_mid + ".RIG_cog_ctrlW0")
    cmds.connectAttr("RIG_spineMid_ctrl.follow_translate", constr_trans_mid + ".RIG_spineLow_ctrlW1")
    cmds.connectAttr("REV_Follow_spineMid.outputY", constr_rot_mid + ".RIG_cog_ctrlW0")
    cmds.connectAttr("RIG_spineMid_ctrl.follow_rotate", constr_rot_mid + ".RIG_spineLow_ctrlW1")

    cmds.connectAttr("RIG_spineHigh_ctrl.follow_translate", "REV_Follow_spineHigh.inputX")
    cmds.connectAttr("RIG_spineHigh_ctrl.follow_rotate", "REV_Follow_spineHigh.inputY")
    cmds.connectAttr("REV_Follow_spineHigh.outputX", constr_trans_high + ".RIG_cog_ctrlW0")
    cmds.connectAttr("RIG_spineHigh_ctrl.follow_translate", constr_trans_high + ".RIG_spineMid_ctrlW1")
    cmds.connectAttr("REV_Follow_spineHigh.outputY", constr_rot_high + ".RIG_cog_ctrlW0")
    cmds.connectAttr("RIG_spineHigh_ctrl.follow_rotate", constr_rot_high + ".RIG_spineMid_ctrlW1")

    cmds.setAttr(constr_trans_mid + ".interpType", 0)
    cmds.setAttr(constr_rot_mid + ".interpType", 0)
    cmds.setAttr(constr_trans_high + ".interpType", 0)
    cmds.setAttr(constr_rot_high + ".interpType", 0)
    cmds.select(cl=True)
    
#//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#END
#//////////////////////////////////////////////////////////////////////////////////////////////////////////////