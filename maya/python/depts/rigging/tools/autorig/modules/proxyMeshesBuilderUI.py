'''
    Proxy Meshes Builder UI 
    
    INPUT PARAMETERS:
        
        Root Mesh                a valid mesh influenced by a skinCluster comprising Proxy Joints
        
        Proxy Joints             the joints to constrain the proxy meshes
        
        Junction Joints          the joints to constrain the junction meshes
        
        Proxy/Junction Meshes    the meshes created by this function, which are to be constrained Proxy/Junction Joints 
        
    DESCRIPTION:
        
        The function builds Proxy Meshes according to Proxy Joints weight assigned to Root Mesh by means of a skinCluster. 
        Junction Meshes size is regulated by the distance between Junction Joints and the corresponding Proxy Mesh.
'''

import maya.cmds as cmds

def proxyMeshesBuilder_UI():    
    #building UI
    winName = 'PMB'
    if cmds.window(winName,exists=True):
        cmds.deleteUI(winName)
        cmds.windowPref(winName,remove=True) 
    cmds.window(winName,title='Proxy Meshes Builder UI',resizeToFitChildren=True)
    mainLayout = cmds.columnLayout(adjustableColumn=True)
    cmds.text('Select Root Mesh',parent=mainLayout,height=30,width=380,statusBarMessage='the selected mesh must be skinned with all the parent joints')
    cmds.textField('meshField',parent=mainLayout,height=30,width=380,changeCommand=meshFieldCommand,statusBarMessage='the selected mesh must be skinned with all the parent joints')
    cmds.button('^^^',parent=mainLayout,height=30,width=380,command=meshButtonCommand,statusBarMessage='the selected mesh must be skinned with all the parent joints')
    cmds.text('Select Proxy Joints',parent=mainLayout,height=30,width=380,statusBarMessage='select the joints to constrain the proxy meshes')
    cmds.button('Build Proxy Meshes',parent=mainLayout,height=30,width=380,command=buildProxyMeshesCommand,statusBarMessage='select the joints to constrain the proxy meshes')       
    cmds.text('Select Junction Joints',parent=mainLayout,height=30,width=380,statusBarMessage='select the joints to constrain the junction meshes')
    cmds.button('Build Junction Meshes',parent=mainLayout,height=30,width=380,command=buildJunctionMeshesCommand,statusBarMessage='select the joints to constrain the junction meshes')       
    cmds.text('Select Proxy/Junction Meshes',parent=mainLayout,height=30,width=380,statusBarMessage='select the meshes to be constrained')
    cmds.button('Constrain Proxy Meshes',parent=mainLayout,height=30,width=380,command=constrainProxyMeshesCommand,statusBarMessage='select the meshes to be constrained')           
    cmds.showWindow(winName)
    
def meshButtonCommand(*args):
    #reading input parameters
    selectionList = cmds.ls(selection=True)
    if selectionList:
        selection = selectionList[-1]
        if cmds.objectType(selection,isType='transform'):
            target_shapeList = cmds.listRelatives(selection,shapes=True)
            target_shape = target_shapeList[0]
        else:
            target_shape = selection
        if cmds.objectType(target_shape,isType='mesh'):
            cmds.textField('meshField',edit=True,text=selection)
        else:
            cmds.warning('Select a valid mesh')   
        
def meshFieldCommand(*args):
    #reading input parameters
    inputMesh = cmds.textField('meshField',query=True,text=True)            
    if inputMesh:
        if cmds.objExists(inputMesh):
            if cmds.objectType(inputMesh,isType='transform'):
                target_shapeList = cmds.listRelatives(inputMesh,shapes=True)
                target_shape = target_shapeList[0]
            else:
                target_shape = inputMesh
            if not cmds.objectType(target_shape,isType='mesh'):
                cmds.warning('Select a valid mesh')      
        else:
            cmds.warning('"'+inputMesh+'" does not exist')

def buildProxyMeshesCommand(*args):
    #reading input parameters
    inputMesh = cmds.textField('meshField',query=True,text=True)
    if not inputMesh:
        cmds.error('Select a valid mesh')
    if not cmds.objExists(inputMesh):
        cmds.error('"'+inputMesh+'" does not exist')
    if cmds.objectType(inputMesh,isType='transform'):
        target = inputMesh
        target_shapeList = cmds.listRelatives(inputMesh,shapes=True)
        target_shape = target_shapeList[0]
    else:
        target = cmds.listRelatives(inputMesh,parent=True)
        target_shape = inputMesh
    if not cmds.objectType(target_shape,isType='mesh'):
        cmds.error('Select a valid mesh')    
    jointList = cmds.ls(selection=True)
    if not jointList:
        cmds.error('Invalid selection: select the joints to constrain the proxy meshes')
    weightsCount = len(jointList)
    vtx_map = {}    
    for token in jointList:
        if not cmds.objectType(token,isType='joint'):
            cmds.error('Invalid selection: select the joints to constrain the proxy meshes')
        vtx_map.setdefault(token,[])
    
    #reading skin weights
    cmds.duplicate(target,name='root_mesh_base')
    base_mesh_children = cmds.listRelatives('root_mesh_base',shapes=True)
    if base_mesh_children:
        base_mesh_redundantChildren = base_mesh_children[1:]
        for item in base_mesh_redundantChildren:
            cmds.delete(item)
    cmds.duplicate('root_mesh_base',name='root_mesh_copy')
    cmds.skinCluster(jointList,'root_mesh_copy',bindMethod=0,skinMethod=0,normalizeWeights=1,weightDistribution=0,maximumInfluences=5,obeyMaxInfluences=True,dropoffRate=4,toSelectedBones=True,name='SK_root_mesh_copy')
    target_history = cmds.listHistory(target)
    target_SKlist = cmds.ls(target_history,type='skinCluster')
    cmds.copySkinWeights(sourceSkin=target_SKlist[0],destinationSkin='SK_root_mesh_copy',noMirror=True,surfaceAssociation='closestPoint',influenceAssociation=['closestJoint','oneToOne','name'],normalize=True)
    vtx_number = cmds.polyEvaluate('root_mesh_copy',vertex=True)
    for index in range(0,vtx_number):
        vtx_name = 'root_mesh_copy.vtx['+str(index)+']'
        vtx_weights = cmds.skinPercent('SK_root_mesh_copy',vtx_name,query=True,value=True) 
        max_weight = max(vtx_weights)
        for counter in range(0,weightsCount):
            if (vtx_weights[counter]/max_weight>0.8):            # if (normalized_weight > threshold):
                influence = jointList[counter]
                vtx_map[influence].append(vtx_name)
    
    #building proxy meshes
    cmds.skinCluster('root_mesh_copy',edit=True,unbind=True)
    faces_inOtherProxyMeshes = [] 
    face_number = cmds.polyEvaluate('root_mesh_copy',face=True)
    proxyGroup = cmds.group(empty=True,world=True)
    pbWinName = 'PB'      
    if cmds.window(pbWinName,exists=True):
        cmds.deleteUI(pbWinName)
        cmds.windowPref(pbWinName,remove=True) 
    cmds.window(pbWinName,title='...Building Proxy Meshes...',sizeable=False,widthHeight=[300,50])
    pbLayout = cmds.formLayout(numberOfDivisions=100)
    pbControl = cmds.progressBar(parent=pbLayout,width=280,height=30,minValue=0,maxValue=weightsCount,progress=0)
    cmds.formLayout(pbLayout,edit=True,attachForm=([pbControl,'left',10],[pbControl,'top',10]))  
    cmds.showWindow(pbWinName)  
    for token in jointList:
        vtx_list = vtx_map[token]
        if vtx_list:
            proxyName = token
            proxyName = proxyName.replace('_jnt','_msh')
            proxyName = proxyName.replace('_Jnt','_Msh')
            proxyName = proxyName.replace('_JNT','_MSH')
            if proxyName == token:
                cmds.warning('Skipped building "'+token+'" proxy: joint name must end with _jnt, _Jnt or _JNT')
                continue
            currentName = 'WIP_' + proxyName                                                
            face_list = cmds.polyListComponentConversion(vtx_list,fromVertex=True,toFace=True)
            face_flatList = cmds.ls(face_list,flatten=True)
            proxyFaces = [item.replace('root_mesh_copy',currentName) for item in face_flatList if not (item in faces_inOtherProxyMeshes)]
            if proxyFaces:                
                complementaryProxyFaces = [currentName+'.f['+str(index)+']' for index in range(0,face_number) if not ( (currentName+'.f['+str(index)+']') in proxyFaces)]
                cmds.duplicate('root_mesh_base',name=currentName)  
                cmds.delete(complementaryProxyFaces)                
                cmds.parent(currentName,proxyGroup)
                cmds.rename(currentName,proxyName)
                faces_inOtherProxyMeshes = faces_inOtherProxyMeshes + [item.replace(currentName,'root_mesh_copy') for item in proxyFaces]  
        cmds.progressBar(pbControl,edit=True,step=1)
    cmds.deleteUI(pbWinName)   
    cmds.delete('root_mesh_base','root_mesh_copy')            
    cmds.rename(proxyGroup,'GRP_Proxy_Meshes')
    cmds.select(clear=True)
    
def buildJunctionMeshesCommand(*args):
    #reading input parameters
    jointList = cmds.ls(selection=True)
    if not jointList:
        cmds.error('Invalid selection: select the joints to constrain the junction meshes')
    
    #building junction meshes
    CPOM_node = cmds.createNode('closestPointOnMesh')
    DB_node = cmds.createNode('distanceBetween')
    LOC_jointPlaceholder = cmds.spaceLocator()
    cmds.connectAttr(CPOM_node+'.result.position',DB_node+'.point2',force=True)
    cmds.connectAttr(LOC_jointPlaceholder[0]+'.translate',CPOM_node+'.inPosition',force=True)
    cmds.connectAttr(LOC_jointPlaceholder[0]+'.translate',DB_node+'.point1',force=True)     
    junctionsCount = len(jointList)
    pbWinName = 'PB'      
    if cmds.window(pbWinName,exists=True):
        cmds.deleteUI(pbWinName)
        cmds.windowPref(pbWinName,remove=True) 
    cmds.window(pbWinName,title='...Building Junction Meshes...',sizeable=False,widthHeight=[300,50])
    pbLayout = cmds.formLayout(numberOfDivisions=100)
    pbControl = cmds.progressBar(parent=pbLayout,width=280,height=30,minValue=0,maxValue=junctionsCount,progress=0)
    cmds.formLayout(pbLayout,edit=True,attachForm=([pbControl,'left',10],[pbControl,'top',10]))  
    cmds.showWindow(pbWinName)      
    for token in jointList:
        if not cmds.objectType(token,isType='joint'):
            cmds.error('Invalid selection: select the joints to constrain the junction meshes')
        proxyName = token
        proxyName = proxyName.replace('_jnt','_msh')
        proxyName = proxyName.replace('_Jnt','_Msh')
        proxyName = proxyName.replace('_JNT','_MSH')
        if proxyName == token:
            cmds.warning('Skipped building "'+token+'" proxy: joint name must end with _jnt, _Jnt or _JNT')
            continue     
        junctionName = proxyName + '_junction'
        try:
            proxy_shapeList = cmds.listRelatives(proxyName,shapes=True)            
            proxy_shape = proxy_shapeList[0]
            cmds.connectAttr(proxy_shape+'.worldMesh[0]',CPOM_node+'.inMesh',force=True)
        except:
            cmds.warning('Skipped building "'+junctionName+'": unable to find "'+proxyName+'"')
            continue
        joint_position = cmds.xform(token,query=True,absolute=True,worldSpace=True,translation=True)
        cmds.xform(LOC_jointPlaceholder[0],absolute=True,worldSpace=True,translation=joint_position)               
        junction_radius = cmds.getAttr(DB_node+'.distance')
        cmds.polySphere(radius=junction_radius,name=junctionName,subdivisionsX=8,subdivisionsY=8,constructionHistory=False)        
        cmds.xform(junctionName,absolute=True,worldSpace=True,translation=joint_position) 
        proxyGroup = cmds.listRelatives(proxyName,parent=True)  
        if proxyGroup:
            cmds.parent(junctionName,proxyGroup)
        cmds.progressBar(pbControl,edit=True,step=1)
    cmds.deleteUI(pbWinName)         
    cmds.delete([CPOM_node,DB_node,LOC_jointPlaceholder[0]]) 
    cmds.select(clear=True)       

def constrainProxyMeshesCommand(*args):
    meshList = cmds.ls(selection=True)
    if not meshList:
        cmds.warning('Select the meshes to be constrained')
    for mesh in meshList:
        parentJoint = mesh
        parentJoint = parentJoint.replace('_msh','_jnt')
        parentJoint = parentJoint.replace('_Msh','_Jnt')        
        parentJoint = parentJoint.replace('_MSH','_JNT') 
        parentJoint = parentJoint.replace('_junction','')                          
        try:
            cmds.parentConstraint(parentJoint,mesh,maintainOffset=True,weight=1)
            cmds.scaleConstraint(parentJoint,mesh,maintainOffset=True,weight=1)
        except:
            cmds.warning('Skipped to constrain mesh "'+mesh+'": unable to find joint "'+parentJoint+'". Mesh name must end with _msh, Msh or _MSH. In addition junction name must have the suffix _junction')
                                           