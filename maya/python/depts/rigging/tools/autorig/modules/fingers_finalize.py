#//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#FINGERS_FINALIZE
#seleziona tutti i control base 
#//////////////////////////////////////////////////////////////////////////////////////////////////////////////

import maya.cmds as cmds
import maya.mel as mel
from maya import cmds , OpenMaya

def fingersFinalize():
    cntBase = cmds.ls(sl=1)
    #print cntBase
    
    if len(cntBase) == 0 :
        print ("----------------------------------------------------------------------")
        print ("WARNING: You must select at least one control base to run this script!")
        print ("----------------------------------------------------------------------")
        cmds.confirmDialog(message = "You must select at least one control base to run this script!", button=['OK'], title='Warning', icn = 'warning', bgc = (1, 0.7, 0) )
    
    else:
        
        for i in cntBase:
            cmds.addAttr( i , ln="curl", nn="Curl", at="float", dv=0 , k=True )
            cmds.addAttr( i , ln="control_vis", nn="Control Vis", at="enum", en="off:on" , k=True , h=False )
            cmds.setAttr( i + ".control_vis", cb=True, k=False )            

            listGrp = cmds.listRelatives( i, c=True )
            print (i)
            print (listGrp)

            cmds.connectAttr( i + ".control_vis", listGrp[1] + ".visibility" )
            listOff = cmds.listRelatives( listGrp, ad=True, typ='transform' )
            
            print (cntBase[0])
            checkName = cntBase[0].find( "thumb")
            print (checkName)  
            
            if checkName > 0 :
                
                thumbBaseGrp = cmds.listRelatives( cntBase[0], p=True )
                print (thumbBaseGrp)
                cmds.connectAttr( i + ".curl", thumbBaseGrp[0] + ".rotateZ" )
                
                cmds.connectAttr( i + ".curl", listOff[1] + ".rotateZ" )
                cmds.connectAttr( i + ".curl", listOff[5] + ".rotateZ" )
                try:
                    cmds.connectAttr( i + ".curl", listOff[9] + ".rotateZ" )
                except:
                    pass
            
            else:
                cmds.connectAttr( i + ".curl", listOff[1] + ".rotateY" )
                cmds.connectAttr( i + ".curl", listOff[5] + ".rotateY" )
                try:
                    cmds.connectAttr( i + ".curl", listOff[9] + ".rotateY" )
                except:
                    pass
        cmds.select(cl=True)
        print ("----------------------------")
        print ("INFO: Fingers are Finalized!")
        print ("----------------------------")
        #cmd.confirmDialog(message = "Fingers are Finalized!", button=['OK'], title='Info', icn = 'information', bgc = (1, 0.7, 0))
