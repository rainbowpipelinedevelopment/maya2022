from PySide2 import QtCore
from PySide2 import QtWidgets
from shiboken2 import wrapInstance

import maya.OpenMayaUI as omui
import maya.api.OpenMaya as om
import maya.cmds as mc 
import math as m

"""
To use this script copy this .py file in your script directory and use this code inside of maya to call it:

from bendySystemUI_vr01_user import CreateBendy_dialog

CreateBendy_dialog.show_dialog()

"""

def create_bendy(segNum, frontTwist):

	def getLegJntList(name, side): #The bendy system will be built on this list of joints

		if 'Front' in name:

			legJntList = "{}_{}_hip1_jnt".format(name, side), "{}_{}_hip2_jnt".format(name, side), "{}_{}_knee1_jnt".format(name, side), "{}_{}_ankle_jnt".format(name, side)

		if 'Rear' in name:

			legJntList = "{}_{}_hip1_jnt".format(name, side), "{}_{}_hip2_jnt".format(name, side), "{}_{}_knee1_jnt".format(name, side), "{}_{}_ankle_jnt".format(name, side)

		return legJntList 

	def getWorldPosition(obj): #Get the world position of the original joints

		pos = mc.xform(obj, q = True, ws = True, rp = True)

		return pos 

	def createRemoveSet(*args):

		for item in args:

			if mc.objExists("REMOVEBENDYSET"):

				mc.sets(item, e = True, add = 'REMOVEBENDYSET')

			else:

				bendySet = mc.sets(item, name = 'REMOVEBENDYSET')
				mc.setAttr("{}.hiddenInOutliner".format(bendySet), True)

	def createFollicleSystem(ribbonSurface, segJntList, segNum): #Create a segNum of follicles on the leg ribbon

		folNum = 0
		follicleShape = []
		follicleTrans = []

		for bone in segJntList:

			folNum += len(bone)

		folStep = 1.0/(folNum)

		step = 0

		for x in range(0, folNum):

			follicleShape.append(mc.createNode('follicle',  name= "{}".format(segJntList[0][x].replace("jnt", "flcShape"))))
			follicleTrans.append(mc.listRelatives(follicleShape[x], parent = 1)[0])
			follicleTrans[x] = mc.rename(follicleTrans[x], follicleTrans[x].replace("flcShape", "flc"))
			mc.setAttr("{}.v".format(follicleShape[x]), 0)	

			mc.connectAttr('{}.local'.format(ribbonSurface),  '{}.inputSurface'.format(follicleShape[x]))
			mc.connectAttr('{}.worldMatrix[0]'.format(ribbonSurface), '{}.inputWorldMatrix'.format(follicleShape[x]))
			mc.connectAttr('{}.outTranslate'.format(follicleShape[x]), '{}.translate'.format(follicleTrans[x]))
			mc.connectAttr('{}.outRotate'.format(follicleShape[x]),'{}.rotate'.format(follicleTrans[x]))
			mc.setAttr("{}.parameterV".format(follicleShape[x]), 0.5)
			mc.setAttr("{}.parameterU".format(follicleShape[x]), step)

			step += folStep

			mc.scaleConstraint('Transform_Null', follicleTrans[x], mo = True)
			mc.parent(segJntList[0][x], follicleTrans[x])

		flcGroup = mc.group(follicleTrans, name = "GRP_{}_Bendyfollicles".format(ribbonSurface.replace("SRF_", "").replace("_leg_ribbon", "")))

		createRemoveSet(follicleShape, follicleTrans)

		return flcGroup, follicleTrans 

	def createControls(follicleTrans, driverJnt, jointList): #Create controls to move the bendy system

		def createAlingLocators(pos, hierarchyOrder):

			loc = mc.spaceLocator(name = pos.replace('Ctrl_Grp', 'Alignloc'))
			mc.delete(mc.listRelatives(loc, children = True, shapes = True)[0])

			mc.matchTransform(loc, pos)

			mc.parent(loc, hierarchyOrder)

			pacRotation = mc.parentConstraint(loc, pos, mo = True, st = ('x', 'y', 'z'))
			pacTranslate = mc.parentConstraint(loc, pos, mo = True, sr = ('x', 'y', 'z'))

			createRemoveSet(loc)

		def bindControlShape(name):

			curve = mc.curve(p = ([-4.7315590960577346e-17, -2.07478373504157e-15, 4.171067894342816],
								[-8.713228564928879e-17, -0.21431739633599511, 4.200039444073622],
								[-2.225090475909078e-16, -0.41411487557344406, 4.28279849574677],
								[-4.731559096057731e-17, -0.5861458250732493, 4.413857617841019],
								[-3.3399579271929985e-16, -0.7172049471675009, 4.585888567340824],
								[-1.4287565821348484e-16, -0.7999639988406472, 4.785686046578274],
								[-4.731559096057731e-17, -0.828935548571454, 5.00000344291427],
								[-1.4287565821348484e-16, -0.7999639988406474, 5.214320839250261],
								[-2.384357254663926e-16, -0.7172049471675004, 5.414117744668664],
								[-1.1102230246251565e-16, -0.5861458250732496, 5.586149267987517],
								[-6.324226883606189e-17, -0.41411487557344573, 5.717207816262725],
								[-8.713228564928879e-17, -0.21431739633599511, 5.7999680155739615],
								[-4.731559096057711e-17, -9.280629280066789e-16, 5.828932105657184],
								[-7.12056077738042e-17, 0.21431739633598995, 5.799968015573956],
								[-3.138891308509274e-17, 0.4141148755734424, 5.717207816262722],
								[-4.731559096057731e-17, 0.5861458250732459, 5.5861492679875155],
								[-1.4287565821348484e-16, 0.7172049471674973, 5.414117744668667],
								[-7.916894671154645e-17, 0.7999639988406445, 5.214320839250263],
								[1.1195118779426845e-16, 0.8289355485714505, 5.000003442914268],
								[-4.731559096057731e-17, 0.7999639988406458, 4.785686046578273],
								[1.6391120541361015e-17, 0.7172049471674987, 4.585888567340827],
								[-7.916894671154645e-17, 0.5861458250732468, 4.413857617841019],
								[-1.5462235209608146e-17, 0.41411487557344134, 4.282798495746767],
								[-5.5278929898319617e-17, 0.2143173963359913, 4.200039444073622],
								[-4.7315590960577346e-17, -2.07478373504157e-15, 4.171067894342816],
								[0.0, 0.0, 0.0]),
								knot = ([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25]),
								degree =1, name = name)

			return curve

		driverCtrl = []
		driverCtrlGrp = []
		bindCtrl = []
		bindCtrlGrp = []

		for x, item in enumerate(driverJnt):

			driverCtrl.append(mc.circle(ch = False, nr = [1,0,0], name = "{}_Ctrl".format(item.replace("JNT_", "")), radius = 3)[0])

			mc.setAttr("{}.v".format(driverCtrl[x]), keyable = False, lock = True)
			shape = mc.listRelatives(driverCtrl[x], children = True, shapes = True)[0]
			mc.setAttr("{}.overrideEnabled".format(shape), 1)
			mc.setAttr("{}.overrideColor".format(shape), 18)

			driverCtrlGrp.append(mc.group(empty = True, name = "{}_Grp".format(driverCtrl[x])))

			mc.parent(driverCtrl[x], driverCtrlGrp[x])
			mc.matchTransform(driverCtrlGrp[x], item)
			mc.parentConstraint(driverCtrl[x], item, mo = True)
			mc.scaleConstraint(driverCtrl[x], item, mo = True)

			driverJntParent = mc.listRelatives(item, parent = True)[0]

			mc.parent(item, world = True)

			createAlingLocators(driverCtrlGrp[x], driverJntParent)

		for x, item in enumerate(follicleTrans):

			#bindCtrl.append(mc.circle(ch = False, nr = [1,0,0], name = "{}".format(item.replace("flc", "Ctrl")), radius = 1.5)[0])
			
			bindCtrl.append(bindControlShape("{}".format(item.replace("flc", "Ctrl"))))
			
			bindJnt = mc.listRelatives(item, children = True, type = 'joint')[0]

			mc.setAttr("{}.v".format(bindCtrl[x]), keyable = False, lock = True)
			mc.setAttr("{}.v".format(bindJnt), 0)
			shape = mc.listRelatives(bindCtrl[x], children = True, shapes = True)[0]
			mc.setAttr("{}.overrideEnabled".format(shape), 1)
			mc.setAttr("{}.overrideColor".format(shape), 20)

			bindCtrlGrp.append(mc.group(empty = True, name = "{}_Grp".format(bindCtrl[x])))

			mc.parent(bindCtrl[x], bindCtrlGrp[x])
			mc.matchTransform(bindCtrlGrp[x], bindJnt)
			
			mc.parent(bindJnt, bindCtrl[x])
			mc.parent(bindCtrlGrp[x], item)

		createRemoveSet(bindCtrl, bindJnt, driverCtrl, driverCtrlGrp, bindCtrlGrp)

		return driverCtrl, driverCtrlGrp, bindCtrl, bindCtrlGrp

	def scaleSystem(driverCtrl, bindCtrlGrp, segNum): #Scale the system with a falloff from driven control to the bind controls

		nCtrl = []

		for x in range(len(bindCtrlGrp)):

			nCtrl.append(x)

		step = int(segNum/2)

		weight = 1.0
		wStep = 1.0/segNum

		y = 0
		u = 0

		for x in range(len(driverCtrl)):

			if x == 0:

				weight = 1

				for z in nCtrl[0:segNum-step]:

					mc.scaleConstraint(driverCtrl[x],bindCtrlGrp[z], mo = True, w = weight)

					weight -= wStep

				y += (segNum-step)

			elif (x > 0) and (x < len(driverCtrl)):

				weight = 1

				for z in nCtrl[(y-step):(y+(step+1))]:

					if z < y:

						weight = 1.0 - (wStep * (y - z)) 

					elif z == y:

						weight = 1

					elif z > y:

						weight = 1.0 - (wStep * (z - y))

					mc.scaleConstraint(driverCtrl[x],bindCtrlGrp[z], mo = True, w = weight)

				y += (segNum-step)

	def addFrontTwist(jointList, side): #Add the front twist joint to help with front ankle rotation

		pos1 = om.MVector(mc.xform(jointList[0], q = True, ws = True, rp = True))
		pos2 = om.MVector(mc.xform(jointList[1], q = True, ws = True, rp = True))
		midPosition = (pos1.x + pos2.x)/2, (pos1.y + pos2.y)/2, (pos1.z + pos2.z)/2

		twistJnt = mc.joint(p = midPosition, name = "{}_twist_jnt".format(jointList[0].replace("_knee1_jnt","")))
		twistCtrl = mc.circle(ch = False, nr = [1,0,0], name = "{}".format(twistJnt.replace("twist_jnt", "twist_Ctrl")), radius = 1.5)[0]
		twistRotGrp = mc.group(empty = True, name = "{}_Rotation_Grp".format(twistCtrl))
		twistGrp = mc.group(twistRotGrp, name = "{}_Grp".format(twistCtrl))
		alingGrp = mc.group(empty = True, name = twistGrp.replace('Ctrl_Grp', 'driver_Alignloc'))

		mdlTwist = mc.createNode('multiplyDivide', name = "MDL_{}".format(twistJnt.replace("twist_jnt", "")))
		mc.setAttr("{}.input2".format(mdlTwist), 0.5, 0.5, 0.5, type = "double3")
		mc.connectAttr("{}.rotate".format(jointList[1]), "{}.input1".format(mdlTwist))
		mc.connectAttr("{}.output".format(mdlTwist), "{}.rotate".format(twistRotGrp))
		
		mc.parent(twistJnt, jointList[0])
		mc.setAttr("{}.jointOrient".format(twistJnt), 0, 0, 0, type = "double3")
		mc.setAttr("{}.v".format(twistCtrl), keyable = False, lock = True)
		shape = mc.listRelatives(twistCtrl, children = True, shapes = True)[0]
		mc.setAttr("{}.overrideEnabled".format(shape), 1)
		mc.setAttr("{}.overrideColor".format(shape), 20)

		mc.parent(twistCtrl, twistRotGrp)
		mc.matchTransform(twistGrp, twistJnt)
		mc.matchTransform(alingGrp, twistJnt)
		mc.parent(alingGrp, jointList[0])
		mc.parent(twistJnt, twistCtrl)

		mc.parentConstraint(alingGrp, twistGrp, mo = True)

		mc.parent(twistGrp, "GRP_Front_{}_BendyControls".format(side))


		if mc.objExists('Bendy_bindJnt'):

			mc.sets(twistJnt, e = True, add = 'Bendy_bindJnt')

		else:

			mc.sets(twistJnt, name = 'FrontTwist_bindJnt')

		createRemoveSet(twistJnt, mdlTwist, twistRotGrp, twistCtrl, alingGrp, twistGrp)

	def createBendySystem(jointList, segNum):

		def createBendJntPos(jointList, jointPosition, segNum):

			segJnt = []
			segJntList = []
			step = 1.0/(segNum + 1)

			for x, jnt in enumerate(jointList[:-1]):
				
				stepValue = step

				mc.select(clear = True)

				pos1 = om.MVector(jntPosition[x])
				pos2 = om.MVector(jntPosition[x+1])

				tmpVect = (pos2 - pos1)

				magTmp = m.sqrt((pos1.x*pos1.x) + (pos1.y*pos1.y) + (pos1.z*pos1.z))  

				normVect = (tmpVect/magTmp)

				moveVect = normVect*(magTmp*0) + pos1

				segJnt.append(mc.joint(jnt, radius = 0.5, p = [moveVect.x, moveVect.y, moveVect.z] , name = jnt.replace("jnt", "seg00_jnt")))

				for y in range(0, segNum):

					moveVect = (normVect*(magTmp*stepValue)) + pos1

					segJnt.append(mc.joint(jnt, radius = 0.5, p = [moveVect.x, moveVect.y, moveVect.z] , name = jnt.replace("jnt", "seg{:02}_jnt".format(y+1))))

					stepValue += step

				if x == (len(jointList[:-1]) - 1):

					moveVect = (normVect*(magTmp*stepValue)) + pos1

					segJnt.append(mc.joint(jnt, radius = 0.5, p = [moveVect.x, moveVect.y, moveVect.z] , name = jnt.replace("jnt", "seg{:02}_jnt".format(y+2))))

					stepValue += step

			segJntList.append(segJnt)

			createRemoveSet(segJntList[0])

			return segJntList

		def createDriverJnt(surface):

			driverJnt = []
			nCv = (mc.getAttr("{}.cp".format(surface), s = True)/2)

			x = 0

			for cv in range(nCv):

				clusterTmp = mc.cluster("{}.cv[{}][0:1]".format(surface, cv))
				mc.select(clear = True)
				clusterTmpPos = mc.xform(clusterTmp, ws = True, rp = True, q = True)
				mc.delete(clusterTmp)
				driverJnt.append(mc.joint(name = "{}_{:02}_driver".format(surface.replace("SRF_","").replace("_leg_ribbon", ""), cv),  p = clusterTmpPos))

				if (cv%2 == 0) and (cv>1):

					x += 1

				mc.parent(driverJnt[cv], jointList[x])

				mc.setAttr("{}.jointOrient".format(driverJnt[cv]), 0,0,0)

			sk = mc.skinCluster(driverJnt, surface, tsb = True)[0]

			for cv in range(nCv):

				mc.skinPercent(sk, driverJnt[x], "{}.cv[{}][0:1]".format(surface, cv), v = 1)

			createRemoveSet(driverJnt)

			return driverJnt

		jntPosition = []
		legCurve = []

		for jnt in jointList:

			jntPosition.append(getWorldPosition(jnt))

		for x, cv in enumerate(jntPosition[:-1]):

			pos1 = om.MVector(jntPosition[x])
			pos2 = om.MVector(jntPosition[x+1])
			midPosition = (pos1.x + pos2.x)/2, (pos1.y + pos2.y)/2, (pos1.z + pos2.z)/2

			legCurve.append(mc.curve(d = 2, k = [0,0,1,1],  p = [pos1, midPosition, pos2], name = "CRV_{}".format(jointList[x])))

		attachedCurve = mc.attachCurve(legCurve, ch = False, rpo = False, kmk = True, name = "CRV_{}leg".format(jointList[0].split("hip1_jnt")[0]))[0]
		
		ribbonSurface = mc.extrude(attachedCurve, ch = False, rn = 0, upn = 1, l = 0.1, po = 0, et = 0,  name = "{}_ribbon".format(attachedCurve.replace('CRV','SRF')))[0]

		clusterTmp = mc.cluster("{}.cv[0][0:1]".format(ribbonSurface))
		mc.matchTransform(ribbonSurface, clusterTmp, piv = True)
		mc.matchTransform(ribbonSurface, jointList[0], pos = True)
		mc.matchTransform(clusterTmp, jointList[0], rot = True)

		mc.delete(clusterTmp)
		mc.delete(legCurve)
		mc.delete(attachedCurve)

		segJntList = createBendJntPos(jointList, jntPosition, segNum)

		flcGroup, follicles = createFollicleSystem(ribbonSurface, segJntList, segNum)

		driverJnt = createDriverJnt(ribbonSurface)

		driverCtrl, driverCtrlGrp, bindCtrl, bindCtrlGrp = createControls(follicles, driverJnt, jointList)

		if segNum%2 != 0:

			scaleSystem(driverCtrl, bindCtrlGrp, segNum)

		jntGroup = mc.group(driverJnt, name = "GRP_{}_BendyJoints".format(ribbonSurface.replace("SRF_", "").replace("_leg_ribbon", "")))
		ctrlGrpGroup = mc.group(driverCtrlGrp, name = "GRP_{}_BendyControls".format(ribbonSurface.replace("SRF_", "").replace("_leg_ribbon", "")))
		utilitiesGrp = mc.group(ribbonSurface, name = "GRP_{}_BendyUtilities".format(ribbonSurface.replace("SRF_", "").replace("_leg_ribbon", "")))

		mc.setAttr("{}.v".format(jntGroup), 0)
		mc.setAttr("{}.inheritsTransform".format(jntGroup), 0)
		mc.setAttr("{}.inheritsTransform".format(flcGroup), 0)
		mc.setAttr("{}.v".format(utilitiesGrp), 0)
		mc.setAttr("{}.inheritsTransform".format(utilitiesGrp), 0)

		legBendyRig = mc.group(jntGroup, ctrlGrpGroup, flcGroup, utilitiesGrp, name = "GRP_{}_Bendy".format(ribbonSurface.replace("SRF_", "").replace("_leg_ribbon", "")))

		if mc.objExists('Bendy_bindJnt'):

			mc.sets(segJntList[0], e = True, add = 'Bendy_bindJnt')

		else:

			mc.sets(segJntList[0], name = 'Bendy_bindJnt')

		createRemoveSet(legBendyRig, ribbonSurface)

		return legBendyRig

	mc.undoInfo(openChunk = True)

	bendyGrp = []

	if mc.objExists("Rear_rig"):

		rearLJntList = getLegJntList('Rear', 'lt')
		rearRJntList = getLegJntList('Rear', 'rt')

		bendyGrp.append(createBendySystem(rearLJntList,segNum))
		bendyGrp.append(createBendySystem(rearRJntList,segNum))

	if mc.objExists("Front_rig"):

		frontLJntList = getLegJntList('Front', 'lt')
		frontRJntList = getLegJntList('Front', 'rt')

		bendyGrp.append(createBendySystem(frontLJntList[:-1],segNum))
		bendyGrp.append(createBendySystem(frontRJntList[:-1],segNum))

		if frontTwist == True:

			addFrontTwist(frontLJntList[-2:], "lt")
			addFrontTwist(frontRJntList[-2:], "rt")

	hierBendyGrp = mc.group(empty = True, name = 'GRP_Bendy_RIG')

	mc.parentConstraint("Transform_Null", hierBendyGrp, mo = True)
	mc.scaleConstraint("Transform_Null", hierBendyGrp, mo = True)

	for item in bendyGrp:

		mc.parent(item, hierBendyGrp)

	createRemoveSet(hierBendyGrp)

	mc.undoInfo(closeChunk = True)	

def maya_main_window():

	main_window_ptr = omui.MQtUtil.mainWindow()
	return wrapInstance(long(main_window_ptr), QtWidgets.QWidget)

class CreateBendy_dialog(QtWidgets.QDialog):

	dlg_instance = None

	@classmethod
	def show_dialog(cls):
		if not cls.dlg_instance:
			cls.dlg_instance = CreateBendy_dialog()

		if cls.dlg_instance.isHidden():
			cls.dlg_instance.show()

		else:
			cls.dlg_instance.raise_()
			cls.dlg_instance.activateWindow()

	def __init__(self, parent = maya_main_window()):
		super(CreateBendy_dialog, self).__init__(parent)

		self.setWindowTitle("Create Bendy on Quad")
		self.setFixedSize(275,100)
		self.setWindowFlags(self.windowFlags() ^ QtCore.Qt.WindowContextHelpButtonHint)

		self.geometry = None

		self.create_widgets()
		self.create_layout()
		self.create_connections()

	def create_widgets(self):
		self.segNum_spinBox = QtWidgets.QSpinBox()
		self.segNum_spinBox.setFixedWidth(60)
		self.segNum_spinBox.setValue(3)
		self.segNum_spinBox.setRange(3, 9)
		self.segNum_spinBox.setSingleStep(2)
		self.segNum_spinBox.setButtonSymbols(QtWidgets.QAbstractSpinBox.NoButtons)

		self.frontTwist_checkBox = QtWidgets.QCheckBox()
		self.frontTwist_checkBox.setChecked(True)

		self.remove_button = QtWidgets.QPushButton("Remove Bendy")
		self.bendy_button = QtWidgets.QPushButton("Create Bendy")
		self.close_button = QtWidgets.QPushButton("Close")

	def create_layout(self):
		spinBox_layout = QtWidgets.QFormLayout()
		spinBox_layout.addRow("Number of segment:", self.segNum_spinBox)

		checkBox_layout = QtWidgets.QFormLayout()
		checkBox_layout.addRow("Front Twist:", self.frontTwist_checkBox)

		button_layout = QtWidgets.QHBoxLayout()
		button_layout.addWidget(self.remove_button)
		button_layout.addStretch()
		button_layout.addWidget(self.bendy_button)
		button_layout.addWidget(self.close_button)

		main_layout = QtWidgets.QVBoxLayout(self)
		main_layout.addLayout(spinBox_layout)
		main_layout.addLayout(checkBox_layout)
		main_layout.addStretch()
		main_layout.addLayout(button_layout)


	def create_connections(self):
		self.remove_button.clicked.connect(self.removeBendy)

		self.close_button.clicked.connect(self.close)

		self.bendy_button.clicked.connect(self.bendySystem_connection)

	def removeBendy(self):

		if mc.objExists('REMOVEBENDYSET'):

			question = QtWidgets.QMessageBox.question(self,'Want to remove the Bendy System?', 'Are you sure you want to remove the bendy system?\n\n'
			'(NOTE: If the BodyRename is done the bendy module can\'t be created again)\n\n'
			'(NOTE: Removing the bendySystem while having a skinCluster on the Quad can cause the mesh to collapse)')

			if question == QtWidgets.QMessageBox.StandardButton.Yes:

				mc.select('REMOVEBENDYSET')
				bendySetElement = mc.ls(sl = True)
				mc.delete(bendySetElement)

			else:
				
				om.MGlobal.displayInfo('The Bendy System won\'t be removed.')

		else: 

			om.MGlobal.displayWarning('There is no bendy system to remove.')


	def bendySystem_connection(self):

		segNum = self.segNum_spinBox.value()

		frontTwist = self.frontTwist_checkBox.isChecked()

		if (segNum%2 != 0):

			if not mc.objExists("GRP_Bendy_RIG"):

				create_bendy(segNum, frontTwist)

			else:

				om.MGlobal.displayError('The Bendy System seems to exist already, try to undo the command or return to a previous file version.')
				return

		else:

			om.MGlobal.displayError('Set an odd number of segments.')
			return


	def showEvent(self, e):
		super(CreateBendy_dialog, self).showEvent(e)

		if self.geometry:
			self.restoreGeometry(self.geometry)

	def closeEvent(self, e):
		if isinstance(self, CreateBendy_dialog):
			super(CreateBendy_dialog, self).closeEvent(e)

			self.geometry = self.saveGeometry()

if __name__ == "__main__":

	try:
		createBendy_dialog.close()
		createBendy_dialog.deleteLater()
	except:
		pass

	createBendy_dialog = CreateBendy_dialog()
	createBendy_dialog.show()