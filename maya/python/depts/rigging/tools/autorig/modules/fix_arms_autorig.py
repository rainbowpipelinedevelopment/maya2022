#//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#RBW AUTORIG DEF
#//////////////////////////////////////////////////////////////////////////////////////////////////////////////

#//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#PLACER SCRIPT AB FIXES
#//////////////////////////////////////////////////////////////////////////////////////////////////////////////
import maya.cmds as cmds
import maya.mel as mel

def rbwFixedArms():

    #fix IK handL attrs
    cmds.setAttr ("RIG_lf_hand_ctrl.rubberHoseCtrlVis", 1)
    cmds.setAttr ("RIG_lf_hand_ctrl.autoSquashStretch", 0)
    cmds.select ( "RIG_lf_arm_ik_ctrl" )
    cmds.addAttr( shortName='asl', longName='armStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='esl', longName='elbowStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='wsl', longName='wristStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='esls', longName='elbowSoftness', at="float", h=False, k=True)
    cmds.connectAttr ('RIG_lf_arm_ik_ctrl.armStretch', 'RIG_lf_hand_ctrl.armStretch')
    cmds.connectAttr ('RIG_lf_arm_ik_ctrl.elbowStretch', 'RIG_lf_hand_ctrl.elbowStretch')
    cmds.connectAttr ('RIG_lf_arm_ik_ctrl.wristStretch', 'RIG_lf_hand_ctrl.wristStretch')
    cmds.connectAttr ('RIG_lf_arm_ik_ctrl.elbowSoftness', 'RIG_lf_hand_ctrl.elbowSoftness')
    cmds.setAttr ("RIG_lf_hand_ctrl.armStretch", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_lf_hand_ctrl.elbowStretch", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_lf_hand_ctrl.wristStretch", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_lf_hand_ctrl.elbowSoftness", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_lf_hand_ctrl.tx", lock=False, keyable=True)
    cmds.setAttr ("RIG_lf_hand_ctrl.ty", lock=False, keyable=True)
    cmds.setAttr ("RIG_lf_hand_ctrl.tz", lock=False, keyable=True)
    cmds.setAttr ("RIG_lf_hand_ctrl.sx", lock=False, keyable=True)
    cmds.setAttr ("RIG_lf_hand_ctrl.sy", lock=False, keyable=True)
    cmds.setAttr ("RIG_lf_hand_ctrl.sz", lock=False, keyable=True)
    cmds.setAttr ("RIG_lf_arm_ik_ctrl.armTwist", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_lf_arm_ik_ctrl.pvControl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_lf_arm_ik_ctrl.maxStretch", 10)
    cmds.select ( cl=True )

    #fix IK handR attrs
    cmds.setAttr ("RIG_rt_hand_ctrl.rubberHoseCtrlVis", 1)
    cmds.setAttr ("RIG_rt_hand_ctrl.autoSquashStretch", 0)
    cmds.select ( "RIG_rt_arm_ik_ctrl" )
    cmds.addAttr( shortName='asr', longName='armStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='esr', longName='elbowStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='wsr', longName='wristStretch', at="float", h=False, k=True)
    cmds.addAttr( shortName='esrs', longName='elbowSoftness', at="float", h=False, k=True)
    cmds.connectAttr ('RIG_rt_arm_ik_ctrl.armStretch', 'RIG_rt_hand_ctrl.armStretch')
    cmds.connectAttr ('RIG_rt_arm_ik_ctrl.elbowStretch', 'RIG_rt_hand_ctrl.elbowStretch')
    cmds.connectAttr ('RIG_rt_arm_ik_ctrl.wristStretch', 'RIG_rt_hand_ctrl.wristStretch')
    cmds.connectAttr ('RIG_rt_arm_ik_ctrl.elbowSoftness', 'RIG_rt_hand_ctrl.elbowSoftness')
    cmds.setAttr ("RIG_rt_hand_ctrl.armStretch", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_rt_hand_ctrl.elbowStretch", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_rt_hand_ctrl.wristStretch", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_rt_hand_ctrl.elbowSoftness", lock=True, keyable=False, channelBox=False )
    cmds.setAttr ("RIG_rt_hand_ctrl.tx", lock=False, keyable=True)
    cmds.setAttr ("RIG_rt_hand_ctrl.ty", lock=False, keyable=True)
    cmds.setAttr ("RIG_rt_hand_ctrl.tz", lock=False, keyable=True)
    cmds.setAttr ("RIG_rt_hand_ctrl.sx", lock=False, keyable=True)
    cmds.setAttr ("RIG_rt_hand_ctrl.sy", lock=False, keyable=True)
    cmds.setAttr ("RIG_rt_hand_ctrl.sz", lock=False, keyable=True)
    cmds.setAttr ("RIG_rt_arm_ik_ctrl.armTwist", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_rt_arm_ik_ctrl.pvControl", lock=True, keyable=False, channelBox=False)
    cmds.setAttr ("RIG_rt_arm_ik_ctrl.maxStretch", 10)
    cmds.select ( cl=True )

    #clavicleL fixed
    cmds.setAttr ( "RIG_lf_clavicle_ctrl.clavicleTransCtrlVis", 1)
    cmds.setAttr ("RIG_lf_clavicleTrans_ctrl.rx", lock=False, keyable=True )
    cmds.setAttr ("RIG_lf_clavicleTrans_ctrl.ry", lock=False, keyable=True )
    cmds.setAttr ("RIG_lf_clavicleTrans_ctrl.rz", lock=False, keyable=True )
    cmds.delete ('RIG_RIG_lf_clavicle_ctrl_frz_grp1_pointConstraint1')
    cmds.parentConstraint ('RIG_lf_clavicleTrans_ctrl', 'RIG_RIG_lf_clavicle_ctrl_frz_grp1', mo=True)
    cmds.setAttr ("RIG_RIG_lf_clavicle_ctrl_frz_grp1.v", 0)
    cmds.setAttr ("RIG_RIG_lf_clavicle_ctrl_frz_grp1.v", lock=True)
    cmds.select ( cl=True )

    #clavicleR fixed
    cmds.setAttr ( "RIG_rt_clavicle_ctrl.clavicleTransCtrlVis", 1)
    cmds.setAttr ("RIG_rt_clavicleTrans_ctrl.rx", lock=False, keyable=True )
    cmds.setAttr ("RIG_rt_clavicleTrans_ctrl.ry", lock=False, keyable=True )
    cmds.setAttr ("RIG_rt_clavicleTrans_ctrl.rz", lock=False, keyable=True )
    cmds.delete ('RIG_RIG_rt_clavicle_ctrl_frz_grp1_pointConstraint1')
    cmds.setAttr("RIG_RIG_rt_clavicleTrans_ctrl_frz_grp.rx", 0)
    cmds.parentConstraint ('RIG_rt_clavicleTrans_ctrl', 'RIG_RIG_rt_clavicle_ctrl_frz_grp1', mo=True)
    cmds.setAttr ("RIG_RIG_rt_clavicle_ctrl_frz_grp1.v", 0)
    cmds.setAttr ("RIG_RIG_rt_clavicle_ctrl_frz_grp1.v", lock=True)
    cmds.select ( cl=True )

    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #FIX_Translate_FK
    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////

    #delete all constraints on groups of each clavicle
    cmds.delete("RIG_lf_armFK_Grp_parentConstraint1")
    cmds.delete("RIG_lf_armIK_Grp_parentConstraint1")
    cmds.delete("RIG_lf_upArm_upJntFkCtrlAlign_grp_parentConstraint1")
    cmds.delete("RIG_RIG_lf_shoulder_ctrl_frz_grp_parentConstraint1")

    cmds.delete("RIG_rt_armFK_Grp_parentConstraint1")
    cmds.delete("RIG_rt_armIK_Grp_parentConstraint1")
    cmds.delete("RIG_rt_upArm_upJntFkCtrlAlign_grp_parentConstraint1")
    cmds.delete("RIG_RIG_rt_shoulder_ctrl_frz_grp_parentConstraint1")

    #delete all the constraints attached on groups and joints of legs and arms (joints FK e CTRL)
    cmds.delete ('RIG_RIG_lf_wrist_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_lf_wrist_fk_ctrl_frz_grp_pointConstraint1')
    cmds.delete ('RIG_RIG_lf_elbow_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_lf_elbow_fk_ctrl_frz_grp_pointConstraint1')
    cmds.delete ('RIG_RIG_lf_upArm_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_lf_upArm_fk_ctrl_frz_grp_pointConstraint1')

    cmds.delete ('RIG_RIG_rt_wrist_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_rt_wrist_fk_ctrl_frz_grp_pointConstraint1')
    cmds.delete ('RIG_RIG_rt_elbow_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_rt_elbow_fk_ctrl_frz_grp_pointConstraint1')
    cmds.delete ('RIG_RIG_rt_upArm_fk_ctrl_frz_grp1_orientConstraint1')
    cmds.delete ('RIG_RIG_rt_upArm_fk_ctrl_frz_grp_pointConstraint1')

    cmds.delete ('RIG_lf_wrist_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_lf_elbow_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_lf_upArm_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_lf_upArm_fk_jnt_pointConstraint1')
    cmds.delete ('RIG_lf_upArm_jnt_pointConstraint1')

    cmds.delete ('RIG_rt_wrist_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_rt_elbow_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_rt_upArm_fk_jnt_orientConstraint1')
    cmds.delete ('RIG_rt_upArm_fk_jnt_pointConstraint1')
    cmds.delete ('RIG_rt_upArm_jnt_pointConstraint1')

    #do a "breakConnection" on the FK joint's "TranslateX"
    cmds.disconnectAttr("RIG_lf_armTrans_blend.outputR", "RIG_lf_elbow_jnt.tx")
    cmds.disconnectAttr("RIG_lf_armTrans_blend.outputG", "RIG_lf_wrist_jnt.tx")
    cmds.disconnectAttr("RIG_rt_armTrans_blend.outputR", "RIG_rt_elbow_jnt.tx")
    cmds.disconnectAttr("RIG_rt_armTrans_blend.outputG", "RIG_rt_wrist_jnt.tx")

    #attiva t e r su tutti controlli fk
    cmds.setAttr ("RIG_lf_wrist_fk_ctrl.secondaryFkCtrlVis", 1)
    cmds.setAttr ("RIG_lf_upArm_fk_ctrl.secondaryFkCtrlVis", 1)
    cmds.setAttr ("RIG_rt_wrist_fk_ctrl.secondaryFkCtrlVis", 1)
    cmds.setAttr ("RIG_rt_upArm_fk_ctrl.secondaryFkCtrlVis", 1)

    cmds.setAttr("RIG_lf_elbow_fk_ctrl.rx",k=True,lock=False);
    cmds.setAttr("RIG_rt_elbow_fk_ctrl.rx",k=True,lock=False);

    cmds.setAttr("RIG_lf_upArm_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_lf_upArm_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_lf_upArm_fk_ctrl.tz",k=True,lock=False);
    cmds.setAttr("RIG_rt_upArm_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_rt_upArm_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_rt_upArm_fk_ctrl.tz",k=True,lock=False);

    cmds.setAttr("RIG_lf_upArm_sec_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_lf_upArm_sec_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_lf_upArm_sec_fk_ctrl.tz",k=True,lock=False);
    cmds.setAttr("RIG_rt_upArm_sec_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_rt_upArm_sec_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_rt_upArm_sec_fk_ctrl.tz",k=True,lock=False);

    cmds.setAttr("RIG_lf_elbow_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_lf_elbow_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_lf_elbow_fk_ctrl.tz",k=True,lock=False);
    cmds.setAttr("RIG_rt_elbow_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_rt_elbow_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_rt_elbow_fk_ctrl.tz",k=True,lock=False);

    cmds.setAttr("RIG_lf_wrist_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_lf_wrist_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_lf_wrist_fk_ctrl.tz",k=True,lock=False);
    cmds.setAttr("RIG_rt_wrist_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_rt_wrist_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_rt_wrist_fk_ctrl.tz",k=True,lock=False);

    cmds.setAttr("RIG_lf_wrist_sec_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_lf_wrist_sec_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_lf_wrist_sec_fk_ctrl.tz",k=True,lock=False);
    cmds.setAttr("RIG_rt_wrist_sec_fk_ctrl.tx",k=True,lock=False);
    cmds.setAttr("RIG_rt_wrist_sec_fk_ctrl.ty",k=True,lock=False);
    cmds.setAttr("RIG_rt_wrist_sec_fk_ctrl.tz",k=True,lock=False);

    #crea sec elbow e knee
    cmds.duplicate ("RIG_RIG_lf_elbow_fk_ctrl_frz_grp1")
    cmds.rename ("RIG_RIG_lf_elbow_fk_ctrl_frz_grp2", "RIG_lf_elbow_sec_fk_ctrl_grp")
    cmds.rename ("RIG_lf_elbow_sec_fk_ctrl_grp|RIG_lf_elbow_fk_ctrl", "RIG_lf_elbow_sec_fk_ctrl")
    cmds.parent ("RIG_lf_elbow_sec_fk_ctrl_grp", "RIG_lf_elbow_fk_ctrl")
    cmds.setAttr("RIG_lf_elbow_sec_fk_ctrl.v",1);
    cmds.select ( "RIG_lf_elbow_fk_ctrl" )
    cmds.addAttr (shortName='secfkel', longName='secondaryFkCtrlVis', at="bool", h=False, k=True)
    cmds.connectAttr ("RIG_lf_elbow_fk_ctrl.secondaryFkCtrlVis", "RIG_lf_elbow_sec_fk_ctrl_grp.v")

    cmds.duplicate ("RIG_RIG_rt_elbow_fk_ctrl_frz_grp1")
    cmds.rename ("RIG_RIG_rt_elbow_fk_ctrl_frz_grp2", "RIG_rt_elbow_sec_fk_ctrl_grp")
    cmds.rename ("RIG_rt_elbow_sec_fk_ctrl_grp|RIG_rt_elbow_fk_ctrl", "RIG_rt_elbow_sec_fk_ctrl")
    cmds.parent ("RIG_rt_elbow_sec_fk_ctrl_grp", "RIG_rt_elbow_fk_ctrl")
    cmds.setAttr("RIG_rt_elbow_sec_fk_ctrl.v",1);
    cmds.select ( "RIG_rt_elbow_fk_ctrl" )
    cmds.addAttr (shortName='secfker', longName='secondaryFkCtrlVis', at="bool", h=False, k=True)
    cmds.connectAttr ("RIG_rt_elbow_fk_ctrl.secondaryFkCtrlVis", "RIG_rt_elbow_sec_fk_ctrl_grp.v")

    cmds.setAttr ("RIG_lf_elbow_fk_ctrl.secondaryFkCtrlVis", 0)
    cmds.setAttr ("RIG_rt_elbow_fk_ctrl.secondaryFkCtrlVis", 0)
    cmds.setAttr ("RIG_lf_upArm_fk_ctrl.secondaryFkCtrlVis", 0)
    cmds.setAttr ("RIG_rt_upArm_fk_ctrl.secondaryFkCtrlVis", 0)
    cmds.setAttr ("RIG_lf_wrist_fk_ctrl.secondaryFkCtrlVis", 0)
    cmds.setAttr ("RIG_rt_wrist_fk_ctrl.secondaryFkCtrlVis", 0)
    ################## - Fix rotateX Left cavicle - ##################
    #cmds.disconnectAttr("RIG_lf_clavicle_jnt.rotateX")
    cmds.parentConstraint("RIG_lf_clavicle_ctrl", "RIG_RIG_lf_shoulder_ctrl_frz_grp", mo=True)
    cmds.parentConstraint("RIG_lf_clavicle_ctrl", "RIG_lf_armFK_Grp", mo=True)
    cmds.parentConstraint("RIG_lf_clavicle_ctrl", "RIG_lf_armIK_Grp", mo=True)
    cmds.parentConstraint("RIG_lf_clavicle_ctrl", "RIG_lf_upArm_upJntFkCtrlAlign_grp", mo=True)

    ################## - Fix rotateX Right cavicle - ##################
    #cmds.disconnectAttr("RIG_rt_clavicle_jnt.rotateX")
    cmds.parentConstraint("RIG_rt_clavicle_ctrl", "RIG_RIG_rt_shoulder_ctrl_frz_grp", mo=True)
    cmds.parentConstraint("RIG_rt_clavicle_ctrl", "RIG_rt_armFK_Grp", mo=True)
    cmds.parentConstraint("RIG_rt_clavicle_ctrl", "RIG_rt_armIK_Grp", mo=True)
    cmds.parentConstraint("RIG_rt_clavicle_ctrl", "RIG_rt_upArm_upJntFkCtrlAlign_grp", mo=True)

    ################## - Fix Translate Left Arm - ##################
    cmds.parentConstraint("RIG_lf_upArm_sec_fk_ctrl", "RIG_lf_upArm_fk_jnt",mo=True)
    cmds.parentConstraint("RIG_lf_elbow_sec_fk_ctrl", "RIG_lf_elbow_fk_jnt",mo=True)
    cmds.parentConstraint("RIG_lf_wrist_sec_fk_ctrl", "RIG_lf_wrist_fk_jnt",mo=True)

    # - Left upArm
    # - PAC just translate
    cmds.parentConstraint("RIG_lf_clavicle_ctrl", "RIG_RIG_lf_upArm_fk_ctrl_frz_grp", mo=True)

    # - Left Elbow
    cmds.parentConstraint("RIG_lf_upArm_sec_fk_ctrl", "RIG_RIG_lf_elbow_fk_ctrl_frz_grp", mo=True)

    # - Left Wrist
         # - PAC just translate
    cmds.parentConstraint("RIG_lf_elbow_sec_fk_ctrl", "RIG_RIG_lf_wrist_fk_ctrl_frz_grp", mo=True)

    # - Connection to DEF joints chain
    bndLfUpArm = cmds.createNode("blendColors", n="BND_translate_lf_upArm")
    bndLfElbow = cmds.createNode("blendColors", n="BND_translate_lf_elbow")
    bndLfWrist = cmds.createNode("blendColors", n="BND_translate_lf_wrist")

    cmds.connectAttr("RIG_lf_hand_ctrl.ikFkBlend", bndLfUpArm+".blender", f=True)
    cmds.connectAttr("RIG_lf_hand_ctrl.ikFkBlend", bndLfElbow+".blender", f=True)
    cmds.connectAttr("RIG_lf_hand_ctrl.ikFkBlend", bndLfWrist+".blender", f=True)

    cmds.connectAttr("RIG_lf_upArm_ik_jnt.translate", bndLfUpArm+".color1", f=True)
    cmds.connectAttr("RIG_lf_upArm_fk_jnt.translate", bndLfUpArm+".color2", f=True)
    cmds.connectAttr(bndLfUpArm+".output", "RIG_lf_upArm_jnt.translate", f=True)

    cmds.connectAttr("RIG_lf_elbow_ik_jnt.translate", bndLfElbow+".color1", f=True)
    cmds.connectAttr("RIG_lf_elbow_fk_jnt.translate", bndLfElbow+".color2", f=True)
    cmds.connectAttr(bndLfElbow+".output", "RIG_lf_elbow_jnt.translate", f=True)

    cmds.connectAttr("RIG_lf_wrist_ik_jnt.translate", bndLfWrist+".color1", f=True)
    cmds.connectAttr("RIG_lf_wrist_fk_jnt.translate", bndLfWrist+".color2", f=True)
    cmds.connectAttr(bndLfWrist+".output", "RIG_lf_wrist_jnt.translate", f=True)


    ################## - Fix Translate Right Arm - ##################
    cmds.parentConstraint("RIG_rt_upArm_sec_fk_ctrl", "RIG_rt_upArm_fk_jnt",mo=True)
    cmds.parentConstraint("RIG_rt_elbow_sec_fk_ctrl", "RIG_rt_elbow_fk_jnt",mo=True)
    cmds.parentConstraint("RIG_rt_wrist_sec_fk_ctrl", "RIG_rt_wrist_fk_jnt",mo=True)

    # - Right upArm
         # - PAC
    cmds.parentConstraint("RIG_rt_clavicle_ctrl", "RIG_RIG_rt_upArm_fk_ctrl_frz_grp", mo=True)
         # - Follow rt upArm


    # - Right Elbow
    cmds.parentConstraint("RIG_rt_upArm_sec_fk_ctrl", "RIG_RIG_rt_elbow_fk_ctrl_frz_grp", mo=True)

    # - Right Wrist
         # - PAC just translate
    cmds.parentConstraint("RIG_rt_elbow_sec_fk_ctrl", "RIG_RIG_rt_wrist_fk_ctrl_frz_grp", mo=True)

    # - Connection to DEF joints chain
    bndRtUpArm = cmds.createNode("blendColors", n="BND_translate_rt_upArm")
    bndRtElbow = cmds.createNode("blendColors", n="BND_translate_rt_elbow")
    bndRtWrist = cmds.createNode("blendColors", n="BND_translate_rt_wrist")

    cmds.connectAttr("RIG_rt_hand_ctrl.ikFkBlend", bndRtUpArm+".blender", f=True)
    cmds.connectAttr("RIG_rt_hand_ctrl.ikFkBlend", bndRtElbow+".blender", f=True)
    cmds.connectAttr("RIG_rt_hand_ctrl.ikFkBlend", bndRtWrist+".blender", f=True)

    cmds.connectAttr("RIG_rt_upArm_ik_jnt.translate", bndRtUpArm+".color1", f=True)
    cmds.connectAttr("RIG_rt_upArm_fk_jnt.translate", bndRtUpArm+".color2", f=True)
    cmds.connectAttr(bndRtUpArm+".output", "RIG_rt_upArm_jnt.translate", f=True)

    cmds.connectAttr("RIG_rt_elbow_ik_jnt.translate", bndRtElbow+".color1", f=True)
    cmds.connectAttr("RIG_rt_elbow_fk_jnt.translate", bndRtElbow+".color2", f=True)
    cmds.connectAttr(bndRtElbow+".output", "RIG_rt_elbow_jnt.translate", f=True)

    cmds.connectAttr("RIG_rt_wrist_ik_jnt.translate", bndRtWrist+".color1", f=True)
    cmds.connectAttr("RIG_rt_wrist_fk_jnt.translate", bndRtWrist+".color2", f=True)
    cmds.connectAttr(bndRtWrist+".output", "RIG_rt_wrist_jnt.translate", f=True)

    cmds.select ( cl=True )

    #delete 4 local align
    cmds.deleteAttr("RIG_lf_upArm_fk_ctrl.localAlign")
    cmds.deleteAttr("RIG_rt_upArm_fk_ctrl.localAlign")

    #rotateOrder added

    cmds.setAttr ("RIG_lf_clavicleTrans_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_shoulder_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_arm_ik_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_hand_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_elbow_pv_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_upArm_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_elbow_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_wrist_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_upArm_sec_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_elbow_sec_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_lf_wrist_sec_fk_ctrl.rotateOrder", k=False, cb=True )

    cmds.setAttr ("RIG_rt_clavicleTrans_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_shoulder_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_arm_ik_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_hand_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_elbow_pv_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_upArm_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_elbow_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_wrist_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_upArm_sec_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_elbow_sec_fk_ctrl.rotateOrder", k=False, cb=True )
    cmds.setAttr ("RIG_rt_wrist_sec_fk_ctrl.rotateOrder", k=False, cb=True )

    cmds.select ( cl=True )

    #autoTwist added
    cmds.select("RIG_lf_wrist_fk_ctrl")
    cmds.addAttr( shortName='atwl', longName='AutoTwist', at="float", h=False, k=True, minValue=0, maxValue=1)
    cmds.setAttr ("RIG_lf_wrist_fk_ctrl.AutoTwist", 1 )

    cmds.select("RIG_rt_wrist_fk_ctrl")
    cmds.addAttr( shortName='atwr', longName='AutoTwist', at="float", h=False, k=True, minValue=0, maxValue=1)
    cmds.setAttr ("RIG_rt_wrist_fk_ctrl.AutoTwist", 1 )

    cmds.shadingNode('blendColors', asShader=True, n="BLD_autoTwistWrist_L")
    cmds.shadingNode('blendColors', asShader=True, n="BLD_autoTwistWrist_R")

    cmds.connectAttr("RIG_lf_wrist_fk_ctrl.AutoTwist", "BLD_autoTwistWrist_L.blender")
    cmds.connectAttr("RIG_lf_wrist_fk_ctrl.rx", "BLD_autoTwistWrist_L.color1R")
    cmds.connectAttr("BLD_autoTwistWrist_L.outputR", "RIG_lf_elbow_sec_fk_ctrl_grp.rx")

    cmds.connectAttr("RIG_rt_wrist_fk_ctrl.AutoTwist", "BLD_autoTwistWrist_R.blender")
    cmds.connectAttr("RIG_rt_wrist_fk_ctrl.rx", "BLD_autoTwistWrist_R.color1R")
    cmds.connectAttr("BLD_autoTwistWrist_R.outputR", "RIG_rt_elbow_sec_fk_ctrl_grp.rx")

    cmds.select ( cl=True )

    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #FOREARM FIX SCRIPT
    #Lanciare lo script senza selezionare nulla
    #Dopo aver lanciato lo script andare sul controllo "RIG_rigSettings_ctrl" e settare gli attributi
    #'Right Forearm 1...4' e 'Left Forearm 1...4' in modo tale da avere una buona deformazione durante la 'roateX' del wrist/hand
    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////

    pls_lf = cmds.createNode("plusMinusAverage", n="PLS_lf_wrist_rotation")

    cmds.connectAttr("RIG_lf_wrist_jnt.rotateX", pls_lf+".input3D[0].input3Dx", force=True)
    cmds.connectAttr("RIG_lf_hand_jnt.rotateX", pls_lf+".input3D[1].input3Dx", force=True)

    cmds.connectAttr(pls_lf+".output3Dx", "RIG_lf_forearmManualRotCtrl_blend.color1.color1R", force=True)

    pls_rt = cmds.createNode("plusMinusAverage", n="PLS_rt_wrist_rotation")

    cmds.connectAttr("RIG_rt_wrist_jnt.rotateX", pls_rt+".input3D[0].input3Dx", force=True)
    cmds.connectAttr("RIG_rt_hand_jnt.rotateX", pls_rt+".input3D[1].input3Dx", force=True)

    cmds.connectAttr(pls_rt+".output3Dx", "RIG_lf_forearmManualRotCtrl_blend.color1.color1R", force=True)

    cmds.setAttr("RIG_lf_hand_ctrl.forearmTwistCtrl", 0)
    cmds.setAttr("RIG_lf_hand_ctrl.forearmTwistCtrl", k=False, l=True, cb=False)

    cmds.setAttr("RIG_rt_hand_ctrl.forearmTwistCtrl", 0)
    cmds.setAttr("RIG_rt_hand_ctrl.forearmTwistCtrl", k=False, l=True, cb=False)

    cmds.select(cl=True)

    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #ARMS LEGS TOONY SCALE FIXED
    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    # HOW TO USE THE SCRIPT: 
    #    1) pick all Toony Controls and Show/Unlock the Scale Attributes
    #    2) RUN the script

    ############################################
    #               Arm Toony Fix              #
    ############################################

    upArmTcntLF = "RIG_lf_upArmRubberHose_ctrl"
    elbowTcntLF = "RIG_lf_elbowRubberHose_ctrl"
    forearmTcntLF = "RIG_lf_forearmRubberHose_ctrl"

    upArmTcntRT = "RIG_rt_upArmRubberHose_ctrl"
    elbowTcntRT = "RIG_rt_elbowRubberHose_ctrl"
    forearmTcntRT = "RIG_rt_forearmRubberHose_ctrl"

    #Slocca i canali dello scale a tutti rubberHose controls
    cmds.setAttr("RIG_lf_upArmRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_lf_upArmRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_lf_upArmRubberHose_ctrl.sz", k=True, l=False)
    cmds.setAttr("RIG_lf_elbowRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_lf_elbowRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_lf_elbowRubberHose_ctrl.sz", k=True, l=False)
    cmds.setAttr("RIG_lf_forearmRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_lf_forearmRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_lf_forearmRubberHose_ctrl.sz", k=True, l=False)

    cmds.setAttr("RIG_rt_upArmRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_rt_upArmRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_rt_upArmRubberHose_ctrl.sz", k=True, l=False)
    cmds.setAttr("RIG_rt_elbowRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_rt_elbowRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_rt_elbowRubberHose_ctrl.sz", k=True, l=False)
    cmds.setAttr("RIG_rt_forearmRubberHose_ctrl.sx", k=True, l=False)
    cmds.setAttr("RIG_rt_forearmRubberHose_ctrl.sy", k=True, l=False)
    cmds.setAttr("RIG_rt_forearmRubberHose_ctrl.sz", k=True, l=False)

    ############################################
    #               Arm Toony Left             #
    ############################################

    # mlp and str first armToony ---> RIG_lf_upArmRubberHose_ctrl
    mlpUpArmLF = cmds.createNode("multiplyDivide", n="MLP_RIG_lf_upArmRubberHose_ctrl")
    strUpArmLF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_upArmWeight_jnt")
    strUpArmLFSplit1LF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_upArmWeightSplit_1_jnt")
    strUpArmLFSpilt2LF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_upArmWeightSplit_2_jnt")

    cmds.setAttr(strUpArmLF+".minX", -5)
    cmds.setAttr(strUpArmLF+".minY", -5)
    cmds.setAttr(strUpArmLF+".minZ", -5)
    cmds.setAttr(strUpArmLF+".maxX", 5)
    cmds.setAttr(strUpArmLF+".maxY", 5)
    cmds.setAttr(strUpArmLF+".maxZ", 5)
    cmds.setAttr(strUpArmLF+".oldMinX", -12.5)
    cmds.setAttr(strUpArmLF+".oldMinY", -12.5)
    cmds.setAttr(strUpArmLF+".oldMinZ", -12.5)
    cmds.setAttr(strUpArmLF+".oldMaxX", 10)
    cmds.setAttr(strUpArmLF+".oldMaxY", 10)
    cmds.setAttr(strUpArmLF+".oldMaxZ", 10)

    cmds.setAttr(strUpArmLFSplit1LF+".minX", -7)
    cmds.setAttr(strUpArmLFSplit1LF+".minY", -7)
    cmds.setAttr(strUpArmLFSplit1LF+".minZ", -7)
    cmds.setAttr(strUpArmLFSplit1LF+".maxX", 7)
    cmds.setAttr(strUpArmLFSplit1LF+".maxY", 7)
    cmds.setAttr(strUpArmLFSplit1LF+".maxZ", 7)
    cmds.setAttr(strUpArmLFSplit1LF+".oldMinX", -11)
    cmds.setAttr(strUpArmLFSplit1LF+".oldMinY", -11)
    cmds.setAttr(strUpArmLFSplit1LF+".oldMinZ", -11)
    cmds.setAttr(strUpArmLFSplit1LF+".oldMaxX", 10)
    cmds.setAttr(strUpArmLFSplit1LF+".oldMaxY", 10)
    cmds.setAttr(strUpArmLFSplit1LF+".oldMaxZ", 10)

    cmds.setAttr(strUpArmLFSpilt2LF+".minX", -9)
    cmds.setAttr(strUpArmLFSpilt2LF+".minY", -9)
    cmds.setAttr(strUpArmLFSpilt2LF+".minZ", -9)
    cmds.setAttr(strUpArmLFSpilt2LF+".maxX", 9)
    cmds.setAttr(strUpArmLFSpilt2LF+".maxY", 9)
    cmds.setAttr(strUpArmLFSpilt2LF+".maxZ", 9)
    cmds.setAttr(strUpArmLFSpilt2LF+".oldMinX", -10.25)
    cmds.setAttr(strUpArmLFSpilt2LF+".oldMinY", -10.25)
    cmds.setAttr(strUpArmLFSpilt2LF+".oldMinZ", -10.25)
    cmds.setAttr(strUpArmLFSpilt2LF+".oldMaxX", 10)
    cmds.setAttr(strUpArmLFSpilt2LF+".oldMaxY", 10)
    cmds.setAttr(strUpArmLFSpilt2LF+".oldMaxZ", 10)

    # mlp second armToony ---> RIG_lf_elbowRubberHose_ctrl
    mlpElbowLF = cmds.createNode("multiplyDivide", n="MLP_RIG_lf_elbowRubberHose_ctrl")

    # mlp and str first armToony ---> RIG_lf_forearmRubberHose_ctrl
    mlpForearmLF = cmds.createNode("multiplyDivide", n="MLP_RIG_lf_forearmRubberHose_ctrl")
    strForearmSplit3LF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_elbowWeightSplit_3_jnt")
    strForearmSplit4LF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_elbowWeightSplit_4_jnt")
    strWristLF = cmds.createNode("setRange", n="STR_Scale_RIG_lf_elbowWeightSplit_4_jnt")

    cmds.setAttr(strWristLF+".minX", -5)
    cmds.setAttr(strWristLF+".minY", -5)
    cmds.setAttr(strWristLF+".minZ", -5)
    cmds.setAttr(strWristLF+".maxX", 5)
    cmds.setAttr(strWristLF+".maxY", 5)
    cmds.setAttr(strWristLF+".maxZ", 5)
    cmds.setAttr(strWristLF+".oldMinX", -12.5)
    cmds.setAttr(strWristLF+".oldMinY", -12.5)
    cmds.setAttr(strWristLF+".oldMinZ", -12.5)
    cmds.setAttr(strWristLF+".oldMaxX", 10)
    cmds.setAttr(strWristLF+".oldMaxY", 10)
    cmds.setAttr(strWristLF+".oldMaxZ", 10)

    cmds.setAttr(strForearmSplit4LF+".minX", -7)
    cmds.setAttr(strForearmSplit4LF+".minY", -7)
    cmds.setAttr(strForearmSplit4LF+".minZ", -7)
    cmds.setAttr(strForearmSplit4LF+".maxX", 7)
    cmds.setAttr(strForearmSplit4LF+".maxY", 7)
    cmds.setAttr(strForearmSplit4LF+".maxZ", 7)
    cmds.setAttr(strForearmSplit4LF+".oldMinX", -11)
    cmds.setAttr(strForearmSplit4LF+".oldMinY", -11)
    cmds.setAttr(strForearmSplit4LF+".oldMinZ", -11)
    cmds.setAttr(strForearmSplit4LF+".oldMaxX", 10)
    cmds.setAttr(strForearmSplit4LF+".oldMaxY", 10)
    cmds.setAttr(strForearmSplit4LF+".oldMaxZ", 10)

    cmds.setAttr(strForearmSplit3LF+".minX", -9)
    cmds.setAttr(strForearmSplit3LF+".minY", -9)
    cmds.setAttr(strForearmSplit3LF+".minZ", -9)
    cmds.setAttr(strForearmSplit3LF+".maxX", 9)
    cmds.setAttr(strForearmSplit3LF+".maxY", 9)
    cmds.setAttr(strForearmSplit3LF+".maxZ", 9)
    cmds.setAttr(strForearmSplit3LF+".oldMinX", -10.25)
    cmds.setAttr(strForearmSplit3LF+".oldMinY", -10.25)
    cmds.setAttr(strForearmSplit3LF+".oldMinZ", -10.25)
    cmds.setAttr(strForearmSplit3LF+".oldMaxX", 10)
    cmds.setAttr(strForearmSplit3LF+".oldMaxY", 10)
    cmds.setAttr(strForearmSplit3LF+".oldMaxZ", 10)

    # connection controls to multiplies
    cmds.connectAttr(upArmTcntLF+".scaleX", mlpUpArmLF+".input1X", force=True)
    cmds.connectAttr(upArmTcntLF+".scaleY", mlpUpArmLF+".input1Y", force=True)
    cmds.connectAttr(upArmTcntLF+".scaleZ", mlpUpArmLF+".input1Z", force=True)

    cmds.connectAttr(elbowTcntLF+".scaleX", mlpElbowLF+".input1X", force=True)
    cmds.connectAttr(elbowTcntLF+".scaleY", mlpElbowLF+".input1Y", force=True)
    cmds.connectAttr(elbowTcntLF+".scaleZ", mlpElbowLF+".input1Z", force=True)

    cmds.connectAttr(forearmTcntLF+".scaleX", mlpForearmLF+".input1X", force=True)
    cmds.connectAttr(forearmTcntLF+".scaleY", mlpForearmLF+".input1Y", force=True)
    cmds.connectAttr(forearmTcntLF+".scaleZ", mlpForearmLF+".input1Z", force=True)

    # connection mlps to strs
    cmds.connectAttr(mlpUpArmLF+".outputX", strUpArmLF+".valueX", force=True)
    cmds.connectAttr(mlpUpArmLF+".outputY", strUpArmLF+".valueY", force=True)
    cmds.connectAttr(mlpUpArmLF+".outputZ", strUpArmLF+".valueZ", force=True)

    cmds.connectAttr(mlpUpArmLF+".outputX", strUpArmLFSplit1LF+".valueX", force=True)
    cmds.connectAttr(mlpUpArmLF+".outputY", strUpArmLFSplit1LF+".valueY", force=True)
    cmds.connectAttr(mlpUpArmLF+".outputZ", strUpArmLFSplit1LF+".valueZ", force=True)

    cmds.connectAttr(mlpUpArmLF+".outputX", strUpArmLFSpilt2LF+".valueX", force=True)
    cmds.connectAttr(mlpUpArmLF+".outputY", strUpArmLFSpilt2LF+".valueY", force=True)
    cmds.connectAttr(mlpUpArmLF+".outputZ", strUpArmLFSpilt2LF+".valueZ", force=True)

    cmds.connectAttr(mlpForearmLF+".outputX", strForearmSplit3LF+".valueX", force=True)
    cmds.connectAttr(mlpForearmLF+".outputY", strForearmSplit3LF+".valueY", force=True)
    cmds.connectAttr(mlpForearmLF+".outputZ", strForearmSplit3LF+".valueZ", force=True)

    cmds.connectAttr(mlpForearmLF+".outputX", strForearmSplit4LF+".valueX", force=True)
    cmds.connectAttr(mlpForearmLF+".outputY", strForearmSplit4LF+".valueY", force=True)
    cmds.connectAttr(mlpForearmLF+".outputZ", strForearmSplit4LF+".valueZ", force=True)

    cmds.connectAttr(mlpForearmLF+".outputX", strWristLF+".valueX", force=True)
    cmds.connectAttr(mlpForearmLF+".outputY", strWristLF+".valueY", force=True)
    cmds.connectAttr(mlpForearmLF+".outputZ", strWristLF+".valueZ", force=True)

    # connection scale directly where doesn't need the scale averaged
    cmds.connectAttr(mlpUpArmLF+".outputX", "RIG_lf_upArmWeightSplit_3_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpUpArmLF+".outputY", "RIG_lf_upArmWeightSplit_3_jnt.scaleX", force=True)
    cmds.connectAttr(mlpUpArmLF+".outputZ", "RIG_lf_upArmWeightSplit_3_jnt.scaleY", force=True)

    cmds.connectAttr(mlpElbowLF+".outputX", "RIG_lf_upArmWeightSplit_4_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpElbowLF+".outputY", "RIG_lf_upArmWeightSplit_4_jnt.scaleX", force=True)
    cmds.connectAttr(mlpElbowLF+".outputZ", "RIG_lf_upArmWeightSplit_4_jnt.scaleY", force=True)

    cmds.connectAttr(mlpElbowLF+".outputX", "RIG_lf_elbowWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpElbowLF+".outputY", "RIG_lf_elbowWeight_jnt.scaleX", force=True)
    cmds.connectAttr(mlpElbowLF+".outputZ", "RIG_lf_elbowWeight_jnt.scaleY", force=True)

    cmds.connectAttr(mlpElbowLF+".outputX", "RIG_lf_elbowWeightSplit_1_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpElbowLF+".outputY", "RIG_lf_elbowWeightSplit_1_jnt.scaleX", force=True)
    cmds.connectAttr(mlpElbowLF+".outputZ", "RIG_lf_elbowWeightSplit_1_jnt.scaleY", force=True)

    cmds.connectAttr(mlpForearmLF+".outputX", "RIG_lf_elbowWeightSplit_2_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpForearmLF+".outputY", "RIG_lf_elbowWeightSplit_2_jnt.scaleX", force=True)
    cmds.connectAttr(mlpForearmLF+".outputZ", "RIG_lf_elbowWeightSplit_2_jnt.scaleY", force=True)

    # connection
    cmds.connectAttr(strUpArmLF+".outValueX", "RIG_lf_upArmWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpArmLF+".outValueY", "RIG_lf_upArmWeight_jnt.scaleX", force=True)
    cmds.connectAttr(strUpArmLF+".outValueZ", "RIG_lf_upArmWeight_jnt.scaleY", force=True)

    cmds.connectAttr(strUpArmLFSplit1LF+".outValueX", "RIG_lf_upArmWeightSplit_1_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpArmLFSplit1LF+".outValueY", "RIG_lf_upArmWeightSplit_1_jnt.scaleX", force=True)
    cmds.connectAttr(strUpArmLFSplit1LF+".outValueZ", "RIG_lf_upArmWeightSplit_1_jnt.scaleY", force=True)

    cmds.connectAttr(strUpArmLFSpilt2LF+".outValueX", "RIG_lf_upArmWeightSplit_2_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpArmLFSpilt2LF+".outValueY", "RIG_lf_upArmWeightSplit_2_jnt.scaleX", force=True)
    cmds.connectAttr(strUpArmLFSpilt2LF+".outValueZ", "RIG_lf_upArmWeightSplit_2_jnt.scaleY", force=True)


    cmds.connectAttr(strForearmSplit3LF+".outValueX", "RIG_lf_elbowWeightSplit_3_jnt.scaleZ", force=True)
    cmds.connectAttr(strForearmSplit3LF+".outValueY", "RIG_lf_elbowWeightSplit_3_jnt.scaleX", force=True)
    cmds.connectAttr(strForearmSplit3LF+".outValueZ", "RIG_lf_elbowWeightSplit_3_jnt.scaleY", force=True)

    cmds.connectAttr(strForearmSplit4LF+".outValueX", "RIG_lf_elbowWeightSplit_4_jnt.scaleZ", force=True)
    cmds.connectAttr(strForearmSplit4LF+".outValueY", "RIG_lf_elbowWeightSplit_4_jnt.scaleX", force=True)
    cmds.connectAttr(strForearmSplit4LF+".outValueZ", "RIG_lf_elbowWeightSplit_4_jnt.scaleY", force=True)

    cmds.connectAttr(strWristLF+".outValueX", "RIG_lf_wristWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(strWristLF+".outValueY", "RIG_lf_wristWeight_jnt.scaleX", force=True)
    cmds.connectAttr(strWristLF+".outValueZ", "RIG_lf_wristWeight_jnt.scaleY", force=True)


    ############################################
    #               Arm Toony Right            #
    ############################################

    # mlp and str first armToony ---> RIG_rt_upArmRubberHose_ctrl
    mlpUpArmRT = cmds.createNode("multiplyDivide", n="MLP_RIG_rt_upArmRubberHose_ctrl")
    strUpArmRT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_upArmWeight_jnt")
    strUpArmRTSplit1RT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_upArmWeightSplit_1_jnt")
    strUpArmRTSpilt2RT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_upArmWeightSplit_2_jnt")

    cmds.setAttr(strUpArmRT+".minX", -5)
    cmds.setAttr(strUpArmRT+".minY", -5)
    cmds.setAttr(strUpArmRT+".minZ", -5)
    cmds.setAttr(strUpArmRT+".maxX", 5)
    cmds.setAttr(strUpArmRT+".maxY", 5)
    cmds.setAttr(strUpArmRT+".maxZ", 5)
    cmds.setAttr(strUpArmRT+".oldMinX", -12.5)
    cmds.setAttr(strUpArmRT+".oldMinY", -12.5)
    cmds.setAttr(strUpArmRT+".oldMinZ", -12.5)
    cmds.setAttr(strUpArmRT+".oldMaxX", 10)
    cmds.setAttr(strUpArmRT+".oldMaxY", 10)
    cmds.setAttr(strUpArmRT+".oldMaxZ", 10)

    cmds.setAttr(strUpArmRTSplit1RT+".minX", -7)
    cmds.setAttr(strUpArmRTSplit1RT+".minY", -7)
    cmds.setAttr(strUpArmRTSplit1RT+".minZ", -7)
    cmds.setAttr(strUpArmRTSplit1RT+".maxX", 7)
    cmds.setAttr(strUpArmRTSplit1RT+".maxY", 7)
    cmds.setAttr(strUpArmRTSplit1RT+".maxZ", 7)
    cmds.setAttr(strUpArmRTSplit1RT+".oldMinX", -11)
    cmds.setAttr(strUpArmRTSplit1RT+".oldMinY", -11)
    cmds.setAttr(strUpArmRTSplit1RT+".oldMinZ", -11)
    cmds.setAttr(strUpArmRTSplit1RT+".oldMaxX", 10)
    cmds.setAttr(strUpArmRTSplit1RT+".oldMaxY", 10)
    cmds.setAttr(strUpArmRTSplit1RT+".oldMaxZ", 10)

    cmds.setAttr(strUpArmRTSpilt2RT+".minX", -9)
    cmds.setAttr(strUpArmRTSpilt2RT+".minY", -9)
    cmds.setAttr(strUpArmRTSpilt2RT+".minZ", -9)
    cmds.setAttr(strUpArmRTSpilt2RT+".maxX", 9)
    cmds.setAttr(strUpArmRTSpilt2RT+".maxY", 9)
    cmds.setAttr(strUpArmRTSpilt2RT+".maxZ", 9)
    cmds.setAttr(strUpArmRTSpilt2RT+".oldMinX", -10.25)
    cmds.setAttr(strUpArmRTSpilt2RT+".oldMinY", -10.25)
    cmds.setAttr(strUpArmRTSpilt2RT+".oldMinZ", -10.25)
    cmds.setAttr(strUpArmRTSpilt2RT+".oldMaxX", 10)
    cmds.setAttr(strUpArmRTSpilt2RT+".oldMaxY", 10)
    cmds.setAttr(strUpArmRTSpilt2RT+".oldMaxZ", 10)

    # mlp second armToony ---> RIG_rt_elbowRubberHose_ctrl
    mlpElbowRT = cmds.createNode("multiplyDivide", n="MLP_RIG_rt_elbowRubberHose_ctrl")

    # mlp and str first armToony ---> RIG_rt_forearmRubberHose_ctrl
    mlpForearmRT = cmds.createNode("multiplyDivide", n="MLP_RIG_rt_forearmRubberHose_ctrl")
    strForearmSplit3RT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_elbowWeightSplit_3_jnt")
    strForearmSplit4RT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_elbowWeightSplit_4_jnt")
    strWristRT = cmds.createNode("setRange", n="STR_Scale_RIG_rt_elbowWeightSplit_4_jnt")

    cmds.setAttr(strWristRT+".minX", -5)
    cmds.setAttr(strWristRT+".minY", -5)
    cmds.setAttr(strWristRT+".minZ", -5)
    cmds.setAttr(strWristRT+".maxX", 5)
    cmds.setAttr(strWristRT+".maxY", 5)
    cmds.setAttr(strWristRT+".maxZ", 5)
    cmds.setAttr(strWristRT+".oldMinX", -12.5)
    cmds.setAttr(strWristRT+".oldMinY", -12.5)
    cmds.setAttr(strWristRT+".oldMinZ", -12.5)
    cmds.setAttr(strWristRT+".oldMaxX", 10)
    cmds.setAttr(strWristRT+".oldMaxY", 10)
    cmds.setAttr(strWristRT+".oldMaxZ", 10)

    cmds.setAttr(strForearmSplit4RT+".minX", -7)
    cmds.setAttr(strForearmSplit4RT+".minY", -7)
    cmds.setAttr(strForearmSplit4RT+".minZ", -7)
    cmds.setAttr(strForearmSplit4RT+".maxX", 7)
    cmds.setAttr(strForearmSplit4RT+".maxY", 7)
    cmds.setAttr(strForearmSplit4RT+".maxZ", 7)
    cmds.setAttr(strForearmSplit4RT+".oldMinX", -11)
    cmds.setAttr(strForearmSplit4RT+".oldMinY", -11)
    cmds.setAttr(strForearmSplit4RT+".oldMinZ", -11)
    cmds.setAttr(strForearmSplit4RT+".oldMaxX", 10)
    cmds.setAttr(strForearmSplit4RT+".oldMaxY", 10)
    cmds.setAttr(strForearmSplit4RT+".oldMaxZ", 10)

    cmds.setAttr(strForearmSplit3RT+".minX", -9)
    cmds.setAttr(strForearmSplit3RT+".minY", -9)
    cmds.setAttr(strForearmSplit3RT+".minZ", -9)
    cmds.setAttr(strForearmSplit3RT+".maxX", 9)
    cmds.setAttr(strForearmSplit3RT+".maxY", 9)
    cmds.setAttr(strForearmSplit3RT+".maxZ", 9)
    cmds.setAttr(strForearmSplit3RT+".oldMinX", -10.25)
    cmds.setAttr(strForearmSplit3RT+".oldMinY", -10.25)
    cmds.setAttr(strForearmSplit3RT+".oldMinZ", -10.25)
    cmds.setAttr(strForearmSplit3RT+".oldMaxX", 10)
    cmds.setAttr(strForearmSplit3RT+".oldMaxY", 10)
    cmds.setAttr(strForearmSplit3RT+".oldMaxZ", 10)

    # connection controls to multiplies
    cmds.connectAttr(upArmTcntRT+".scaleX", mlpUpArmRT+".input1X", force=True)
    cmds.connectAttr(upArmTcntRT+".scaleY", mlpUpArmRT+".input1Y", force=True)
    cmds.connectAttr(upArmTcntRT+".scaleZ", mlpUpArmRT+".input1Z", force=True)

    cmds.connectAttr(elbowTcntRT+".scaleX", mlpElbowRT+".input1X", force=True)
    cmds.connectAttr(elbowTcntRT+".scaleY", mlpElbowRT+".input1Y", force=True)
    cmds.connectAttr(elbowTcntRT+".scaleZ", mlpElbowRT+".input1Z", force=True)

    cmds.connectAttr(forearmTcntRT+".scaleX", mlpForearmRT+".input1X", force=True)
    cmds.connectAttr(forearmTcntRT+".scaleY", mlpForearmRT+".input1Y", force=True)
    cmds.connectAttr(forearmTcntRT+".scaleZ", mlpForearmRT+".input1Z", force=True)

    # connection mlps to strs
    cmds.connectAttr(mlpUpArmRT+".outputX", strUpArmRT+".valueX", force=True)
    cmds.connectAttr(mlpUpArmRT+".outputY", strUpArmRT+".valueY", force=True)
    cmds.connectAttr(mlpUpArmRT+".outputZ", strUpArmRT+".valueZ", force=True)

    cmds.connectAttr(mlpUpArmRT+".outputX", strUpArmRTSplit1RT+".valueX", force=True)
    cmds.connectAttr(mlpUpArmRT+".outputY", strUpArmRTSplit1RT+".valueY", force=True)
    cmds.connectAttr(mlpUpArmRT+".outputZ", strUpArmRTSplit1RT+".valueZ", force=True)

    cmds.connectAttr(mlpUpArmRT+".outputX", strUpArmRTSpilt2RT+".valueX", force=True)
    cmds.connectAttr(mlpUpArmRT+".outputY", strUpArmRTSpilt2RT+".valueY", force=True)
    cmds.connectAttr(mlpUpArmRT+".outputZ", strUpArmRTSpilt2RT+".valueZ", force=True)

    cmds.connectAttr(mlpForearmRT+".outputX", strForearmSplit3RT+".valueX", force=True)
    cmds.connectAttr(mlpForearmRT+".outputY", strForearmSplit3RT+".valueY", force=True)
    cmds.connectAttr(mlpForearmRT+".outputZ", strForearmSplit3RT+".valueZ", force=True)

    cmds.connectAttr(mlpForearmRT+".outputX", strForearmSplit4RT+".valueX", force=True)
    cmds.connectAttr(mlpForearmRT+".outputY", strForearmSplit4RT+".valueY", force=True)
    cmds.connectAttr(mlpForearmRT+".outputZ", strForearmSplit4RT+".valueZ", force=True)

    cmds.connectAttr(mlpForearmRT+".outputX", strWristRT+".valueX", force=True)
    cmds.connectAttr(mlpForearmRT+".outputY", strWristRT+".valueY", force=True)
    cmds.connectAttr(mlpForearmRT+".outputZ", strWristRT+".valueZ", force=True)

    # connection scale directly where doesn't need the scale averaged
    cmds.connectAttr(mlpUpArmRT+".outputX", "RIG_rt_upArmWeightSplit_3_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpUpArmRT+".outputY", "RIG_rt_upArmWeightSplit_3_jnt.scaleX", force=True)
    cmds.connectAttr(mlpUpArmRT+".outputZ", "RIG_rt_upArmWeightSplit_3_jnt.scaleY", force=True)

    cmds.connectAttr(mlpElbowRT+".outputX", "RIG_rt_upArmWeightSplit_4_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpElbowRT+".outputY", "RIG_rt_upArmWeightSplit_4_jnt.scaleX", force=True)
    cmds.connectAttr(mlpElbowRT+".outputZ", "RIG_rt_upArmWeightSplit_4_jnt.scaleY", force=True)

    cmds.connectAttr(mlpElbowRT+".outputX", "RIG_rt_elbowWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpElbowRT+".outputY", "RIG_rt_elbowWeight_jnt.scaleX", force=True)
    cmds.connectAttr(mlpElbowRT+".outputZ", "RIG_rt_elbowWeight_jnt.scaleY", force=True)

    cmds.connectAttr(mlpElbowRT+".outputX", "RIG_rt_elbowWeightSplit_1_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpElbowRT+".outputY", "RIG_rt_elbowWeightSplit_1_jnt.scaleX", force=True)
    cmds.connectAttr(mlpElbowRT+".outputZ", "RIG_rt_elbowWeightSplit_1_jnt.scaleY", force=True)


    cmds.connectAttr(mlpForearmRT+".outputX", "RIG_rt_elbowWeightSplit_2_jnt.scaleZ", force=True)
    cmds.connectAttr(mlpForearmRT+".outputY", "RIG_rt_elbowWeightSplit_2_jnt.scaleX", force=True)
    cmds.connectAttr(mlpForearmRT+".outputZ", "RIG_rt_elbowWeightSplit_2_jnt.scaleY", force=True)

    # connection strs output to jnts scale
    cmds.connectAttr(strUpArmRT+".outValueX", "RIG_rt_upArmWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpArmRT+".outValueY", "RIG_rt_upArmWeight_jnt.scaleX", force=True)
    cmds.connectAttr(strUpArmRT+".outValueZ", "RIG_rt_upArmWeight_jnt.scaleY", force=True)

    cmds.connectAttr(strUpArmRTSplit1RT+".outValueX", "RIG_rt_upArmWeightSplit_1_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpArmRTSplit1RT+".outValueY", "RIG_rt_upArmWeightSplit_1_jnt.scaleX", force=True)
    cmds.connectAttr(strUpArmRTSplit1RT+".outValueZ", "RIG_rt_upArmWeightSplit_1_jnt.scaleY", force=True)

    cmds.connectAttr(strUpArmRTSpilt2RT+".outValueX", "RIG_rt_upArmWeightSplit_2_jnt.scaleZ", force=True)
    cmds.connectAttr(strUpArmRTSpilt2RT+".outValueY", "RIG_rt_upArmWeightSplit_2_jnt.scaleX", force=True)
    cmds.connectAttr(strUpArmRTSpilt2RT+".outValueZ", "RIG_rt_upArmWeightSplit_2_jnt.scaleY", force=True)

    cmds.connectAttr(strForearmSplit3RT+".outValueX", "RIG_rt_elbowWeightSplit_3_jnt.scaleZ", force=True)
    cmds.connectAttr(strForearmSplit3RT+".outValueY", "RIG_rt_elbowWeightSplit_3_jnt.scaleX", force=True)
    cmds.connectAttr(strForearmSplit3RT+".outValueZ", "RIG_rt_elbowWeightSplit_3_jnt.scaleY", force=True)

    cmds.connectAttr(strForearmSplit4RT+".outValueX", "RIG_rt_elbowWeightSplit_4_jnt.scaleZ", force=True)
    cmds.connectAttr(strForearmSplit4RT+".outValueY", "RIG_rt_elbowWeightSplit_4_jnt.scaleX", force=True)
    cmds.connectAttr(strForearmSplit4RT+".outValueZ", "RIG_rt_elbowWeightSplit_4_jnt.scaleY", force=True)

    cmds.connectAttr(strWristRT+".outValueX", "RIG_rt_wristWeight_jnt.scaleZ", force=True)
    cmds.connectAttr(strWristRT+".outValueY", "RIG_rt_wristWeight_jnt.scaleX", force=True)
    cmds.connectAttr(strWristRT+".outValueZ", "RIG_rt_wristWeight_jnt.scaleY", force=True)

    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #CHIUSURA
    #//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    cmds.setAttr("RIG_lf_hand_ctrl.ikVis", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.fkVis", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.shoulderCtrlVis", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.upLimbAutoTwist", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.upLimbTwist", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.upLimbTwistScalar", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.midJntUpLimbTwistScalar", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.useAimTwist", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.rubberHoseCtrlVis", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.autoSquashStretch", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.squashStretch", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.LeftArm_Squash_And_Stretch___", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.leftArmMaxStretchAmt", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.leftArmStretchSpeed", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.leftArmMaxSquashAmt", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.leftArmSquashSpeed", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.lfUpArm_SsScale", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.lfUpArmSplit_1_SsScale", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.lfUpArmSplit_2_SsScale", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.lfUpArmSplit_3_SsScale", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.lfUpArmSplit_4_SsScale", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.lfElbow_SsScale", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.lfForeArmSplit_1_SsScale", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.lfForeArmSplit_2_SsScale", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.lfForeArmSplit_3_SsScale", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.lfForeArmSplit_4_SsScale", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_hand_ctrl.lfWrist_SsScale", lock=True, keyable=False, channelBox=False )

    cmds.setAttr("RIG_rt_hand_ctrl.ikVis", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.fkVis", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.shoulderCtrlVis", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.upLimbAutoTwist", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.upLimbTwist", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.upLimbTwistScalar", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.midJntUpLimbTwistScalar", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.useAimTwist", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.rubberHoseCtrlVis", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.autoSquashStretch", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.squashStretch", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.RightArm_Squash_And_Stretch___", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.rightArmMaxStretchAmt", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.rightArmStretchSpeed", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.rightArmMaxSquashAmt", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.rightArmSquashSpeed", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.rtUpArm_SsScale", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.rtUpArmSplit_1_SsScale", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.rtUpArmSplit_2_SsScale", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.rtUpArmSplit_3_SsScale", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.rtUpArmSplit_4_SsScale", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.rtElbow_SsScale", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.rtForeArmSplit_1_SsScale", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.rtForeArmSplit_2_SsScale", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.rtForeArmSplit_3_SsScale", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.rtForeArmSplit_4_SsScale", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_hand_ctrl.rtWrist_SsScale", lock=True, keyable=False, channelBox=False )

    cmds.setAttr("RIG_lf_arm_ik_ctrl.stretchyArm", 1)
    cmds.setAttr("RIG_lf_arm_ik_ctrl.stretchValue", lock=True, keyable=False, channelBox=False )

    cmds.setAttr("RIG_rt_arm_ik_ctrl.stretchyArm", 1)
    cmds.setAttr("RIG_rt_arm_ik_ctrl.stretchValue", lock=True, keyable=False, channelBox=False )

    cmds.select("RIG_lf_hand_jnt")
    lfHandJnt = cmds.listRelatives( "RIG_lf_hand_jnt", ad=True, typ='joint' )
    print (lfHandJnt)
    for i in lfHandJnt:
        cmds.setAttr( i + ".segmentScaleCompensate", 0 )
        print (i)

    cmds.select("RIG_rt_hand_jnt")
    rtHandJnt = cmds.listRelatives( "RIG_rt_hand_jnt", ad=True, typ='joint' )
    print (rtHandJnt)
    for i in rtHandJnt:
        cmds.setAttr( i + ".segmentScaleCompensate", 0 )
        print (i)
            
    try:
        cmds.scaleConstraint( "RIG_lf_hand_ctrl", "RIG_lf_hand_jnt", mo=True )
        cmds.pointConstraint( "RIG_lf_hand_ctrl", "RIG_lf_hand_jnt", mo=True )
             
    except:
        pass

    try:
        cmds.scaleConstraint( "RIG_rt_hand_ctrl", "RIG_rt_hand_jnt", mo=True )
        cmds.pointConstraint( "RIG_rt_hand_ctrl", "RIG_rt_hand_jnt", mo=True )   
    except:
        pass
        
    cmds.select ( cl=True )

    cmds.parent("RIG_lf_clavicle_jnt", "RIG_Skeleton_grp")
    cmds.parent("RIG_rt_clavicle_jnt", "RIG_Skeleton_grp")
    cmds.delete("RIG_spine_low_jnt")
    cmds.delete("weightSpine_grp")
    cmds.delete("lf_legWeightJnt_grp")
    cmds.delete("rt_legWeightJnt_grp")
    cmds.setAttr("RIG_lf_elbow_pv_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_lf_elbow_pv_ctrl.lfArmIkCtrl", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_elbow_pv_ctrl.________SPACES___", lock=True, keyable=False, channelBox=False )
    cmds.setAttr("RIG_rt_elbow_pv_ctrl.rtArmIkCtrl", lock=True, keyable=False, channelBox=False )

    cmds.select ( cl=True )