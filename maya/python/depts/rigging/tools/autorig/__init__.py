import os
import importlib

iconpath = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'rigging')


def run_rau(*args):
    import depts.rigging.tools.autorig.rbw_autorig_ui as rau
    importlib.reload(rau)
    rau.main()

rauTool = {
    "name": "RBW Autorig UI",
    "launch": run_rau,
    "statustip": "RBW Autorig UI",
    "icon": os.path.join(iconpath, "rbwAuto.png")
}


def run_crg(*args):
    import maya.mel as mel
    mel.eval("source cr_moduleTemplateBuilder; cr_moduleTemplateBuilder;")

crgTool = {
    "name": 'Creature Rig',
    "launch": run_crg,
    "statustip": "Creature Rig",
    "icon": os.path.join(iconpath, "zampa.png")
}


def run_ipd(*args):
    import maya.cmds as cmds
    fileToImport = os.path.join(os.getenv('LOCALPIPE'), 'scenes', 'rigging', 'prop', 'RBW_Prop_Default.ma')
    cmds.file(fileToImport, i=True, type="mayaAscii", namespace=":")

ipdTool = {
    "name": "Import Prop Default",
    "launch": run_ipd,
    "statustip": "Import Prop Default",
    "icon": os.path.join(iconpath, "prop_def.png")
}

def run_ipfxd(*args):
    import maya.cmds as cmds
    fileToImport = os.path.join(os.getenv('LOCALPIPE'), 'scenes', 'rigging', 'prop', 'RBW_Prop_FX_Default.ma')
    cmds.file(fileToImport, i=True, type="mayaAscii", namespace=":")

ipfxdTool = {
    "name": "Import Prop FX Default",
    "launch": run_ipfxd,
    "statustip": "Import Prop FX Default",
    "icon": os.path.join(iconpath, "prop_fx_def.png")
}

def run_iwr(*args):
    import maya.cmds as cmd
    localpipeScenesDir = os.path.dirname(os.getenv('LOCALPIPE'))
    fileToImport = os.path.join(localpipeScenesDir, 'maya', 'scenes', 'rigging', 'vehicle', 'wheels_base_rig.ma').replace('\\', '/')
    cmd.file(fileToImport, i=True, type="mayaAscii", namespace=":")

iwrTool = {
    "name": "Vehicles Base Rig",
    "launch": run_iwr,
    "statustip": "Vehicles Base Rig",
    "icon": os.path.join(iconpath, "wheels_base_rig.png")
}


def run_sqs(*args):
    import depts.rigging.tools.autorig.sqs_UI as sqs
    importlib.reload(sqs)
    sqs.createWindow()

sqsTool = {
    "name": "Auto SQS",
    "launch": run_sqs,
    "statustip": "Auto SQS",
    "icon": os.path.join(iconpath, "auto_sqs.png")
}

def run_WingsAutorig(*args):
    import depts.rigging.tools.autorig.Wings_Rig as Wings
    importlib.reload(Wings)
    Wings.createWindow()

WingsTool = {
    "name": "Wings Rig",
    "launch": run_WingsAutorig,
    "statustip": "Wings Rig",
    "icon": os.path.join(iconpath, "Wings.png")
}


tools = [
    rauTool,
    crgTool,
    ipdTool,
    ipfxdTool,
    iwrTool,
    sqsTool,
    WingsTool
]


PackageTool = {
    "name": "autorig",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
