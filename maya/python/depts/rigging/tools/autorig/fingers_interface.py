from imp import reload
import maya.cmds as cmd
from maya import OpenMayaUI as omui

def fingersSKTCmd(dummy):
    import depts.rigging.tools.autorig.modules.fingers_autorig as fga
    reload(fga)
    fga.fingersAutorig()
    return

def fingersSKTFinalizeCmd(dummy):
    import depts.rigging.tools.autorig.modules.fingers_finalize as fgf
    reload(fgf)
    fgf.fingersFinalize()

def fingersSKTLastCmd(dummy):
    import depts.rigging.tools.autorig.modules.fingers_last as fgl
    reload(fgl)
    fgl.fingersLast()

def fingersWindow():
    if (cmd.window("fingers_ui", q=True,exists=True)):
        cmd.deleteUI("fingers_ui", window=True)
    W=cmd.window("fingers_ui",title="Fingers Module", s=True, ip=True, rtf=True)
    RC1=cmd.rowColumnLayout( numberOfColumns=2, p=W, columnAttach=[(1,'left',10),(2,'both',5),(3,'both',5)])
    cmd.text("Select all joints of a single finger:", al="right",w=230)
    cmd.button(l="Fingers Autorig", c= fingersSKTCmd, w=150,h=30, bgc=[0.3, 0.3, 0.3])
    cmd.text("Select the first control of a single finger:", al="right",w=230)
    cmd.button(l="Fingers Finalize", c= fingersSKTFinalizeCmd, w=150,h=30, bgc=[0.3, 0.3, 0.3])
    cmd.text("Select all 'Lf' or 'Rt' groups:", al="right",w=230)  
    cmd.button(l="Fingers Last", c= fingersSKTLastCmd, w=150,h=30, bgc=[0.3, 0.3, 0.3])
    cmd.showWindow( W )
    return