import os

import maya.cmds as cmds


localpipe = os.getenv("LOCALPY")
iconpath = os.path.join(localpipe, "depts", "rigging", "tools", "icons")


def getVtxPos( shapeNode ):
	vtxWorldPosition = []    
	vtxIndexList = cmds.getAttr( shapeNode+".vrts", multiIndices=True )
	for i in vtxIndexList :
		curPointPosition = cmds.xform( str(shapeNode)+".pnts["+str(i)+"]", query=True, translation=True, worldSpace=True )
		vtxWorldPosition.append( curPointPosition )
	return vtxWorldPosition

def getDeformerPos(msh):
    pos=getVtxPos(msh)
    valueY=(pos[0][1]+pos[1][1]+pos[6][1]+pos[7][1])/4
    valueX=(pos[0][0]+pos[1][0]+pos[6][0]+pos[7][0])/4
    valueZ=(pos[0][2]+pos[1][2]+pos[6][2]+pos[7][2])/4
    return valueX,valueY,valueZ

def setGhost(cube,ghost):
    pos=getVtxPos(cube)
    cmds.xform(ghost+".cv[0]",t=pos[0],ws=True,a=True)
    cmds.xform(ghost+".cv[4]",t=pos[0],ws=True,a=True)
    cmds.xform(ghost+".cv[8]",t=pos[0],ws=True,a=True)
    cmds.xform(ghost+".cv[1]",t=pos[1],ws=True,a=True)
    cmds.xform(ghost+".cv[7]",t=pos[1],ws=True,a=True)
    cmds.xform(ghost+".cv[2]",t=pos[3],ws=True,a=True)
    cmds.xform(ghost+".cv[12]",t=pos[3],ws=True,a=True)
    cmds.xform(ghost+".cv[3]",t=pos[2],ws=True,a=True)
    cmds.xform(ghost+".cv[11]",t=pos[2],ws=True,a=True)
    cmds.xform(ghost+".cv[13]",t=pos[5],ws=True,a=True)
    cmds.xform(ghost+".cv[15]",t=pos[5],ws=True,a=True)
    cmds.xform(ghost+".cv[6]",t=pos[7],ws=True,a=True)
    cmds.xform(ghost+".cv[16]",t=pos[7],ws=True,a=True)
    cmds.xform(ghost+".cv[5]",t=pos[6],ws=True,a=True)
    cmds.xform(ghost+".cv[9]",t=pos[6],ws=True,a=True)
    cmds.xform(ghost+".cv[10]",t=pos[4],ws=True,a=True)
    cmds.xform(ghost+".cv[14]",t=pos[4],ws=True,a=True)
    cmds.xform(ghost,cp=1)
    cmds.delete(ghost,constructionHistory = True)
    cmds.setAttr(ghost+".overrideEnabled",1)
    cmds.setAttr(ghost+".overrideDisplayType",2)
    return

def create_head_squash(s,t,u,n):
    LTC_divisions = (s,t,u)
    sel = cmds.ls(sl=True)
    cube=sel[0]
    cube=cmds.rename(cube, "MSH_Cube_Cage_"+n )
    localpipeBackDir = os.path.dirname(localpipe)
    fileToImport = os.path.join(localpipeBackDir, 'scenes', 'rigging', 'rbw_autorig', 'SQS_Ctrls.ma')
    cmds.file(fileToImport, i=True, type="mayaAscii", namespace=":")
    sqs_ctrl = "headSqs_Ctrl"
    sqs_ctrl = cmds.rename(sqs_ctrl,n+"Sqs_Ctrl")
    ghost=cmds.rename("lattice_SQS_ghost_Ctrl", "lattice_"+n+"Sqs_ghost")
    obj=sqs_ctrl
    obj2=ghost
    for i in range(3):
        obj=cmds.pickWalk(obj,d="up")[0]
        obj2=cmds.pickWalk(obj2,d="up")[0]
        obj=cmds.rename(obj,str(obj).replace("head",n))
        obj2=cmds.rename(obj2,str(obj2).replace("SQS",n+"Sqs"))
    alignCtrl(cube,obj)
    cmds.select(obj)
    cmds.move(15,y=True,r=True, os=True)
    cmds.select(cl=True)
    setGhost(cube,ghost)

    #creo il lattice e i deformatori

    LTC = cmds.lattice(cube, n="LTC_"+n, divisions=LTC_divisions, objectCentered=True, outsideLattice=2, ldv=(12,12,12))
    cmds.connectAttr(sqs_ctrl+".Falloff", LTC[0] + ".outsideFalloffDist" ) 
    SQS = cmds.nonLinear(LTC[1], type="squash", n="SQS_"+n, lowBound=0, highBound=2)
    BND_01 = cmds.nonLinear(LTC[1], type="bend", n="BND_"+n+"_01", lowBound=0, highBound=2)
    BND_02 = cmds.nonLinear(LTC[1], type="bend", n="BND_"+n+"_02", lowBound=0, highBound=2)
    TWS = cmds.nonLinear(LTC[1], type="twist", n="TWS_"+n, lowBound=0, highBound=2)
    cmds.xform(BND_02[1], ro=(0,-90,0),r=True, os=True)
    cmds.group(cube, LTC, SQS, BND_01, BND_02, TWS, n="GRP_Deformer_"+n)

    #creo i multiply

    mlp_01 = cmds.createNode("multiplyDivide", n="MLP_Sqs_Tws_"+n, ss=True)
    cmds.setAttr(mlp_01 + ".input2X", 0.1)
    cmds.setAttr(mlp_01 + ".input2Y", -1)
    mlp_02 = cmds.createNode("multiplyDivide", n="MLP_Bnd_"+n, ss=True)
    cmds.setAttr(mlp_02 + ".input2X", 5)
    cmds.setAttr(mlp_02 + ".input2Y", 5)

    #connetto translate e rotate ai multiply e i multiply ai deformatori

    cmds.connectAttr(sqs_ctrl+".ty", mlp_01 + ".input1X")
    cmds.connectAttr(mlp_01 + ".outputX", SQS[0] + ".factor")

    cmds.connectAttr(sqs_ctrl+".ry", mlp_01 + ".input1Y")
    cmds.connectAttr(mlp_01 + ".outputY", TWS[0] + ".endAngle")

    cmds.connectAttr(sqs_ctrl+".tx", mlp_02 + ".input1X")
    cmds.connectAttr(mlp_02 + ".outputX", BND_01[0] + ".curvature")

    cmds.connectAttr(sqs_ctrl+".tz", mlp_02 + ".input1Y")
    cmds.connectAttr(mlp_02 + ".outputY", BND_02[0] + ".curvature")

    # rinomino i deformatori e connetto la visibility della cage

    d1=cmds.rename(SQS[1], "SQS_"+n)
    d2=cmds.rename(BND_01[1], "BND_"+n+"_01")
    d3=cmds.rename(BND_02[1], "BND_"+n+"_02")
    d4=cmds.rename(TWS[1], "TWS_"+n)

    cmds.connectAttr(sqs_ctrl+".Effect_Cage", obj2+".v")
    
    v=getDeformerPos(cube)
    cmds.select(cl=True)
    cmds.select(d1,d2,d3,d4)
    cmds.move(v[1],y=True,a=True,ws=True)
    cmds.move(v[0],x=True,a=True,ws=True)
    cmds.move(v[2],z=True,a=True,ws=True)
    return

def alignCtrl(msh,grp):
    pc=cmds.pointConstraint(msh,grp,mo=False)
    oc=cmds.orientConstraint(msh,grp,mo=False)
    cmds.delete(pc,oc)
    return

def getDivisions(*args):
    divS=cmds.intSliderGrp(slid1,q=True,v=True)
    divT=cmds.intSliderGrp(slid2,q=True,v=True)
    divU=cmds.intSliderGrp(slid3,q=True,v=True)
    NAME=cmds.textField("name",q=True,tx=True)
    create_head_squash(divS,divT,divU,NAME)
    return

def createWindow(*args):
    global slid1,slid2,slid3,NAME
    if cmds.window("sqs_UI", ex= True):
        cmds.deleteUI("sqs_UI")
    
    cmds.window("sqs_UI", t="Auto Squash & Stretch", sizeable=True, visible=True)
    c = cmds.columnLayout(adj=True)
    
    rc=cmds.rowColumnLayout( numberOfColumns=2, p=c, columnAttach=[(1,'right',0),(2,'both',5)], columnWidth=[(1, 160), (2, 300)])
    cmds.text( label='Name: ', ann="Input ik ribbon name" )
    cmds.textField("name",w=250,tx="Head")   
    cmds.text(label="\nScegli il numerro di suddivisione del lattice\n", p=c)
    slid1=cmds.intSliderGrp("sDivisions", field=True, label='S Divisions', minValue=1, maxValue=20, fieldMinValue=1, fieldMaxValue=20, value=10, p=c )
    slid2=cmds.intSliderGrp("tDivisions", field=True, label='T Divisions', minValue=1, maxValue=20, fieldMinValue=1, fieldMaxValue=20, value=10, p=c )
    slid3=cmds.intSliderGrp("uDivisions", field=True, label='U Divisions', minValue=1, maxValue=20, fieldMinValue=1, fieldMaxValue=20, value=10, p=c )
    cmds.text(label="\nSeleziona il cubo dell'ingombro e clicca sul bottone\n", align="center", p=c)
    cmds.button(label="Auto SQS", command=getDivisions, p=c, align="left", w=150)
    cmds.showWindow("sqs_UI")
    return