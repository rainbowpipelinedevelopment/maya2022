import maya.cmds as cmd
import maya.mel as mel

# SELECT SOME JOINT CHAIN AND RUN executeProcedures()

# TO MAKE DYNAMIC CHAIN SCALABLE, 
# CONNECT THE RIG GLOBAL SCALE ATTRIBUTE TO THE DYNAMIC CHAIN CONTROL GLOBAL SCALE ATTRIBUTE.
# IF THERE ISN'T A RIG TO CONNECT TO, CONNECT THE CONTROL CHAIN GLOBAL SCALE ATTRIBUTE
# TO THE GROUP *_STC SCALE X, Y AND Z.

def executeProcedures():

    name=''

    result = cmd.promptDialog(
            title='DM_dynamic_chain',
            message='Enter Name:',
            button=['OK', 'Cancel'],
            defaultButton='OK',
            cancelButton='Cancel',
            dismissString='Cancel')

    if result == 'OK':
        name = cmd.promptDialog(query=True, text=True)
        print(name)
    else:
        return
        
    check = name

    res=cmd.objExists(check)

    print(res)
    if res==1:
        cmd.inViewMessage( amg='<hl>'+name+'</hl> already exists. Choose a different name.', pos='midCenter', fade=True )
        return
    
    howMany=0

    result = cmd.promptDialog(
            title='DM_dynamic_chain',
            message='How many joints for each chain?:',
            button=['OK', 'Cancel'],
            defaultButton='OK',
            cancelButton='Cancel',
            dismissString='Cancel')

    if result == 'OK':
        howMany = cmd.promptDialog(query=True, text=True)
    else:
        return

    sel = cmd.ls(sl=1)
    ctrl = createTextControl(name)
    cmd.select(ctrl)
    expose = exposeAttrs()
    cmd.connectAttr(ctrl+'.StartCurveAttract', ctrl+'.Stiffness')
    curves = []
    iksvar = []
    efcvar = []
    
    for s in sel:
        cmd.select(s)
        crv = curveByJointChain(int(howMany))
        curves.append(crv)     
        
    cmd.select(curves) 
    
    mhs = makeHairSystem(ctrl)
    cmd.select(mhs[4])
    
    opc = cmd.ls(sl=1, l=1)
    
    
    for i in range(0,len(opc),1):
        cmd.select(opc[i])
        tempiks = ikSplineByCurve(num=int(howMany))
        iksvar.append(tempiks[1])
        efcvar.append(tempiks[2])
        cmd.skinCluster(sel[i], mhs[2][i], dr=4.5, bm=1, sm=1, nw=2, wd=1, mi=5, );
        for sj in range(0,len(tempiks[0]),1):
            cmd.rename(tempiks[0][sj], 'temporary_temporaryoraryName_splineJoint_'+PAD(sj, 3))
            
        
    md1 = cmd.createNode('multiplyDivide')
    md2 = cmd.createNode('multiplyDivide')
    cmd.connectAttr(ctrl+'.Stiffness', md1+'.input1X')
    cmd.connectAttr(ctrl+'.StiffnessOverride', md1+'.input2X')
    cmd.connectAttr(md1+'.outputX', mhs[0][0]+'.stiffness')
    
    cmd.connectAttr(ctrl+'.StartCurveAttract', md2+'.input1X')
    cmd.connectAttr(ctrl+'.CurveAttOverride', md2+'.input2X')
    cmd.connectAttr(md2+'.outputX', mhs[0][0]+'.startCurveAttract')
    temporaryattrslock='''
        setAttr -lock true "temporaryoraryName.DISP_OBJ";
        setAttr -lock true "temporaryoraryName.DYN2FK";
        setAttr -lock true "temporaryoraryName.ATTR";
        setAttr -lock true "temporaryoraryName.Stiffness";
        setAttr -lock true "temporaryoraryName.StartCurveAttract";
    '''
    attrslock=temporaryattrslock.replace('temporaryoraryName', name)
    mel.eval(attrslock)
    
    cmd.setAttr(mhs[0][0]+".stiffnessScale[0].stiffnessScale_Position", 0.5)
    cmd.setAttr(mhs[0][0]+".stiffnessScale[0].stiffnessScale_FloatValue", 1.0)

    cmd.setAttr(mhs[0][0]+".stiffnessScale[1].stiffnessScale_Position", 1)
    cmd.setAttr(mhs[0][0]+".stiffnessScale[1].stiffnessScale_FloatValue", 0.5)

    cmd.setAttr(mhs[0][0]+".attractionScale[0].attractionScale_Position", 0.5)
    cmd.setAttr(mhs[0][0]+".attractionScale[0].attractionScale_FloatValue", 1.0)

    cmd.setAttr(mhs[0][0]+".attractionScale[1].attractionScale_Position", 1)
    cmd.setAttr(mhs[0][0]+".attractionScale[1].attractionScale_FloatValue", 0.5)

    cmd.connectAttr(ctrl+'.Mass', mhs[0][0]+'.mass')
    cmd.connectAttr(ctrl+'.Damping', mhs[0][0]+'.damp')
    cmd.connectAttr(ctrl+'.Gravity', mhs[6][0]+'.gravity')
    cmd.connectAttr(ctrl+'.Drag', mhs[0][0]+'.drag')
    
    cmd.connectAttr(ctrl+'.Friction', mhs[0][0]+'.friction')
    cmd.connectAttr(ctrl+'.Iterations', mhs[0][0]+'.iterations')
    cmd.connectAttr(ctrl+'.CollisionOffset', mhs[0][0]+'.collideWidthOffset')
    cmd.connectAttr(ctrl+'.BendResistance', mhs[0][0]+'.bendResistance')
    
    '''
    for c in curves:
        print c
    for m in mhs:
        for x in m:
            print x
    for i in iksvar:
        print i
    for e in efcvar:
        print e
    '''

    cmd.select(cl=1)            
    stcGrp = cmd.group(em=1, name='temporary_GRP_temporaryoraryName_STC')
    stc = []
    for c in range(0,len(curves),1):
        nameType = 'temporary_temporaryoraryName_STC_'+PAD(c, 3)
        print(nameType)
        cmd.rename(curves[c], nameType)
        stc.append(nameType)
    
    cmd.parent(stc, stcGrp)
        
    cmd.select(cl=1)            
    flcGrp = cmd.group(em=1, name='temporary_GRP_temporaryoraryName_FLC')
    flc = []
    for f in range(0,len(mhs[1]),1):
        nameType = 'temporary_temporaryoraryName_FLC_'+PAD(f, 3)
        cmd.rename(mhs[1][f], nameType)
        flc.append(nameType)

    newFlc = 'temporary_temporaryoraryName_GRP_FLC'
    flcGrp = []
    #print mhs[3]
    cmd.rename(mhs[3], newFlc)
    flcGrp.append(newFlc)
    
    cmd.parent(newFlc, flcGrp)
    
    cmd.select(cl=1)            
    opcGrp = cmd.group(em=1, name='temporary_GRP_temporaryoraryName_OPC')
    opc = []
    for o in range(0,len(mhs[4]),1):
        nameType = 'temporary_temporaryoraryName_OPC_'+PAD(o, 3)
        cmd.rename(mhs[4][o], nameType)
        opc.append(nameType)

    newOpc = 'temporary_temporaryoraryName_GRP_OPC'
    opcGrp = []
    #print mhs[5]
    cmd.rename(mhs[5], newOpc)
    opcGrp.append(newOpc)
    
    cmd.parent(newOpc, opcGrp)
    
    cmd.select(cl=1)            
    iksGrp = cmd.group(em=1, name='temporary_GRP_temporaryoraryName_IKS')
    iks = []
    for i in range(0,len(iksvar),1):
        nameType = 'temporary_temporaryoraryName_IKS_'+PAD(i, 3)
        cmd.rename(iksvar[i], nameType)
        iks.append(nameType)

    cmd.parent(iks, iksGrp)

    grpSplJoints = cmd.group(em=1, name='temporary_GRP_temporaryoraryName_splineJoints')
    cmd.select('temporary_temporaryoraryName_splineJoint_*')
    jroots = cmd.ls(sl=1, assemblies=1)
    #print jroots
    cmd.parent(jroots, grpSplJoints)

    nucName='temporary_temporaryoraryName_nucleus'
    cmd.rename(mhs[6], nucName)
    hsName='temporary_temporaryoraryName_hairSystem'
    cmd.rename(cmd.listRelatives(mhs[0], p=1, f=1), hsName)
    ng=cmd.group(em=1, name='temporary_GRP_temporaryoraryName_nucleus')
    hsg=cmd.group(em=1, name='temporary_GRP_temporaryoraryName_hairSystem')
    cmd.parent(nucName, ng)
    cmd.parent(hsName, hsg)
    ctrlGrp=cmd.group(em=1, name='temporary_GRP_temporaryoraryName')
    cmd.parent(ctrl,ctrlGrp)
        
    tempattrs='''
    setAttr -keyable false -channelBox false "aaaaa.tx";
    setAttr -keyable false -channelBox false "aaaaa.ty";
    setAttr -keyable false -channelBox false "aaaaa.tz";
    setAttr -keyable false -channelBox false "aaaaa.rx";
    setAttr -keyable false -channelBox false "aaaaa.ry";
    setAttr -keyable false -channelBox false "aaaaa.rz";
    setAttr -keyable false -channelBox false "aaaaa.sx";
    setAttr -keyable false -channelBox false "aaaaa.sy";
    setAttr -keyable false -channelBox false "aaaaa.sz";
    '''
    attrs=tempattrs.replace('aaaaa', name)
    mel.eval(attrs) 

    cmd.setAttr('temporary_GRP_temporaryoraryName_STC.v', 0)
    cmd.setAttr('temporary_GRP_temporaryoraryName_FLC.v', 0)
    cmd.setAttr('temporary_GRP_temporaryoraryName_OPC.v', 0)
    cmd.setAttr('temporary_GRP_temporaryoraryName_IKS.v', 0)
    cmd.setAttr('temporary_GRP_temporaryoraryName_nucleus.v', 0)
    cmd.setAttr('temporary_GRP_temporaryoraryName_hairSystem.v', 0)
    cmd.connectAttr(ctrl+'.CollisionCurves', 'temporary_GRP_temporaryoraryName_OPC.v')
    cmd.connectAttr(ctrl+'.Skeleton', 'temporary_GRP_temporaryoraryName_splineJoints.v')
    cmd.setAttr(ctrl+'.overrideEnabled', 1)
    cmd.setAttr(ctrl+'.overrideColor', 17)
    #cmd.connectAttr(ctrl+'.globalScale', 'temporary_GRP_temporaryoraryName_STC.sx')
    #cmd.connectAttr(ctrl+'.globalScale', 'temporary_GRP_temporaryoraryName_STC.sy')
    #cmd.connectAttr(ctrl+'.globalScale', 'temporary_GRP_temporaryoraryName_STC.sz')
    cmd.connectAttr(ctrl+'.globalScale', 'temporary_GRP_temporaryoraryName_splineJoints.sx')
    cmd.connectAttr(ctrl+'.globalScale', 'temporary_GRP_temporaryoraryName_splineJoints.sy')
    cmd.connectAttr(ctrl+'.globalScale', 'temporary_GRP_temporaryoraryName_splineJoints.sz')        
    mel.eval('searchReplaceNames "temporaryoraryName" "'+name+'" "all";')
    grpMain = cmd.group(em=1, name='GRP_'+name+'_dynamicChain')
    fsel = cmd.ls('*temporary_GRP*')
    cmd.parent(fsel, grpMain)   
    mel.eval('searchReplaceNames "temporary_" " " "all";')
    cmd.select(cl=1)
    
    #################rbwfixes#################
    #attacco attributo Automation al Simulation Method dei follicoli
    cmd.select(name + "_FLC_*")
    flcs = cmd.ls(selection=True, transforms=True)
    print(flcs)
    cndSM = cmd.shadingNode("condition", name="CND_Simulation_Method", asUtility=True)
    cmd.connectAttr(name + ".Automation", cndSM + ".firstTerm")
    cmd.setAttr(cndSM + ".colorIfFalseR", 2)
    cmd.setAttr(cndSM + ".colorIfFalseG", 2)
    cmd.setAttr(cndSM + ".colorIfFalseB", 2)
    
    for fc in flcs:
        fcShape = cmd.pickWalk(fc, direction='down')[0]
        v = cmd.connectAttr(cndSM +'.outColorR', fcShape +'.simulationMethod')
    
    cmd.select(cl=True)
    
    #connesione per fixare la scala
    if cmd.objExists('Transform_Ctrl'):
        cmd.connectAttr('Transform_Ctrl.masterScale', name + ".globalScale")
        cmd.setAttr(name + '.globalScale', lock=True, keyable=False, channelBox=False)
    else:
        print('Non esiste il Transform_Ctrl, fai attenzione e ricordati di collegare il global scale')
    
    #sistemiamo gli attributi del controllo
    cmd.setAttr(name + '.visibility', lock=True, keyable=False, channelBox=False)
    cmd.setAttr(name + '.DISP_OBJ', lock=True, keyable=False, channelBox=False)
    cmd.setAttr(name + '.CollisionObjects', lock=True, keyable=False, channelBox=False)
    cmd.setAttr(name + '.CollisionCurves', lock=True, keyable=False, channelBox=False)
    cmd.setAttr(name + '.Skeleton', lock=True, keyable=False, channelBox=False)
    cmd.setAttr(name + '.Display', lock=True, keyable=False, channelBox=False)
    cmd.setAttr(name + '.DisplayQuality', lock=True, keyable=False, channelBox=False)
    cmd.addAttr(name + '.Iterations', edit=True, hasMinValue=False, hasMaxValue=False)
    cmd.setAttr(name + '.Iterations', lock=True, keyable=False, channelBox=False)
    cmd.setAttr(name + '.Friction', lock=True, keyable=False, channelBox=False)
    cmd.setAttr(name + '.Gravity', lock=True, keyable=False, channelBox=False)
    cmd.setAttr(name + '.DYN2FK', lock=True, keyable=False, channelBox=False)
    cmd.setAttr(name + '.ATTR', lock=True, keyable=False, channelBox=False)
    cmd.setAttr(name + '.Stiffness', lock=True, keyable=False, channelBox=False)
    cmd.setAttr(name + '.StartCurveAttract', lock=True, keyable=False, channelBox=False)
    cmd.setAttr(name + '.Mass', lock=True, keyable=False, channelBox=False)
    
    #adesso facciamo il parentConstraint dalla testa se esiste il controllo col nome DH
    if cmd.objExists('Head_a_jnt') and name == ('DH'):
        cmd.parentConstraint('Head_a_jnt', 'GRP_' + name + '_splineJoints')
    elif cmd.objExists('RIG_head_a_jnt') and name == ('DH'):
        cmd.parentConstraint('RIG_head_a_jnt', 'GRP_' + name + '_splineJoints')
    else:
        print('Ricordati di fare il parent constraint al gruppo _splineJoints')
    
    #adesso facciamo il parentConstraint dalla testa se esiste il controllo col nome DH
    if cmd.objExists('Head_a_jnt') and name == ('DE'):
        cmd.parentConstraint('Head_a_jnt', 'GRP_' + name + '_splineJoints')
    elif cmd.objExists('RIG_head_a_jnt') and name == ('DE'):
        cmd.parentConstraint('RIG_head_a_jnt', 'GRP_' + name + '_splineJoints')
    else:
        print('Ricordati di fare il parent constraint al gruppo _splineJoints')
    
    
def ikSplineByCurve(num=10):
    num=num
    sel = cmd.ls(sl=1)
    sels = cmd.ls(sl=1, dag=1, shapes=1, ni=1, l=1)
    mmv = cmd.getAttr(sels[0]+'.minMaxValue')[0]
    par = mmv[1] / num
    joints=[]

    for i in range(0,num,1):
        if i==0:
            pr=0
        jp = cmd.pointOnCurve(sel, pr=pr, p=1)
        j=cmd.joint(p=(jp[0],jp[1],jp[2]))
        pr+=par
        joints.append(j)

    iks = cmd.ikHandle(sj=joints[0], ee=joints[-1], solver='ikSplineSolver', curve=sel[0], ccv=0, scv=0, pcv=0)
    #cmd.joint(joints, e=1, oj='xyz', secondaryAxisOrient='zdown', ch=1, zso=1)
    cmd.parent(joints[0], w=1)
    cmd.select(joints)
    cmd.select(sel[0])

    return [joints, iks[0], iks[1]]


    
def exposeAttrs():

    sel = cmd.ls(sl=1)
    temporaryattrs = '''
    addAttr -ln "DISP_OBJ"  -at bool  |aaaaa;
    setAttr -e-keyable true |aaaaa.DISP_OBJ;
    addAttr -ln "CollisionObjects"  -at double  -dv 0 -min 0 -max 1 |aaaaa;
    setAttr -e-keyable true |aaaaa.CollisionObjects;
    addAttr -ln "CollisionCurves"  -at double  -dv 0 -min 0 -max 1 |aaaaa;
    setAttr -e-keyable true |aaaaa.CollisionCurves;
    addAttr -ln "Skeleton"  -at double  -dv 1 -min 0 -max 1 |aaaaa;
    setAttr -e-keyable true |aaaaa.Skeleton;
    addAttr -ln "Display"  -at "enum" -en "Mesh:Pfx:"  |aaaaa;
    setAttr -e-keyable true |aaaaa.Display;
    addAttr -ln "DisplayQuality"  -at "enum" -en "Low:Medium:High:"  |aaaaa;
    setAttr -e-keyable true |aaaaa.DisplayQuality;
    addAttr -ln "DYN2FK" -nn "DYN 2 FK" -at bool  |aaaaa;
    setAttr -e-keyable true |aaaaa.DYN2FK;
    addAttr -ln "TopHair"  -at double  -dv 0.7 -min 0 -max 1 |aaaaa;
    setAttr -e-keyable true |aaaaa.TopHair;
    addAttr -ln "ATTR"  -at bool  |aaaaa;
    setAttr -e-keyable true |aaaaa.ATTR;
    addAttr -ln "Automation"  -at bool -dv 0.0  |aaaaa;
    setAttr -e-keyable true |aaaaa.Automation;
    addAttr -ln "Damping"  -at double  -dv 0.2  -min 0 -max 10 |aaaaa;
    setAttr -e-keyable true |aaaaa.Damping;
    addAttr -ln "Drag"  -at double  -dv 0.2 -min 0 -max 10 |aaaaa;
    setAttr -e-keyable true |aaaaa.Drag;
    addAttr -ln "Iterations"  -at double  -dv 0.2 -min 0 -max 1 |aaaaa;
    setAttr -e-keyable true |aaaaa.Iterations;
    addAttr -ln "Friction"  -at double  -dv 4 -min 0 -max 10 |aaaaa;
    setAttr -e-keyable true |aaaaa.Friction;
    addAttr -ln "Mass"  -at double  -dv 1.0 -min 0 -max 10 |aaaaa;
    setAttr -e-keyable true |aaaaa.Mass;
    addAttr -ln "CollisionOffset"  -at double  -dv 1 -min 0 -max 10 |aaaaa;
    setAttr -e-keyable true |aaaaa.CollisionOffset;
    addAttr -ln "StartFrame"  -at double  -dv 1 -min 0 |aaaaa;
    setAttr -e-keyable true |aaaaa.StartFrame;
    addAttr -ln "BendResistance"  -at double  -dv 1 -min 0 -max 10 |aaaaa;
    setAttr -e-keyable true |aaaaa.BendResistance;
    addAttr -ln "Gravity"  -at double  -dv 9.8 -min 0 |aaaaa;
    setAttr -e-keyable true |aaaaa.Gravity;
    addAttr -ln "Stiffness"  -at double  -dv 0.8 -min 0 |aaaaa;
    setAttr -e-keyable true |aaaaa.Stiffness;
    addAttr -ln "StartCurveAttract"  -at double  -dv 0.8 -min 0 -max 1 |aaaaa;
    setAttr -e-keyable true |aaaaa.StartCurveAttract;
    addAttr -ln "CharacterAction"  -at "enum" -en "Normal:Medium:High:"  |aaaaa;
    setAttr -e-keyable true |aaaaa.CharacterAction;
    addAttr -ln "CurveAttOverride"  -at double  -dv 1 -min 0 -max 2 |aaaaa;
    setAttr -e-keyable true |aaaaa.CurveAttOverride;
    addAttr -ln "StiffnessOverride"  -at double  -dv 1 -min 0 -max 2 |aaaaa;
    setAttr -e-keyable true |aaaaa.StiffnessOverride;   
    addAttr -ln "globalScale"  -at double  -dv 1 -min 0 |aaaaa;
    setAttr -e-keyable true |aaaaa.globalScale;     
    setDrivenKeyframe -currentDriver aaaaa.CharacterAction aaaaa.StartCurveAttract;
    setAttr "aaaaa.CharacterAction" 1;
    setAttr "aaaaa.StartCurveAttract" 0.9;
    setDrivenKeyframe -currentDriver aaaaa.CharacterAction aaaaa.StartCurveAttract;
    setAttr "aaaaa.CharacterAction" 2;
    setAttr "aaaaa.StartCurveAttract" 1;
    setDrivenKeyframe -currentDriver aaaaa.CharacterAction aaaaa.StartCurveAttract;
    setAttr "aaaaa.CharacterAction" 0;
    '''
    attrs=temporaryattrs.replace('aaaaa',sel[0])
    mel.eval(attrs)

    
    
def makeHairSystem(ctrl):
    mel.eval('makeCurvesDynamic 2 { "1", "0", "0", "1", "0"};')
    hs = cmd.ls(sl=1, l=1)
    nucleus = []
    cons=cmd.listConnections(hs)
    for c in cons:
        nt = cmd.nodeType(c)
        if (nt == 'nucleus'):
            nucleus.append(c)
            break
    cmd.select(hs)
    mel.eval('convertHairSelection "follicles"')
    flc = cmd.ls(sl=1, l=1)
    mel.eval('pickWalk -d up;')
    flcgrp = cmd.ls(sl=1, l=1)
    mel.eval('convertHairSelection "startCurves"')
    stc = cmd.ls(sl=1, l=1)
    mel.eval('convertHairSelection "currentCurves"')
    opc = cmd.ls(sl=1, l=1)
    mel.eval('pickWalk -d up;')
    opcgrp = cmd.ls(sl=1, l=1)
    for f in flc:
        cmd.setAttr(f+'.pointLock', 1)
    mel.eval('displayHairCurves "currentAndStart" 1;')
    
    for s in range(0,len(stc),1):
        #print stc[s]
        #print opc[s]
        #print ctrl
        
        bs = cmd.blendShape(stc[s],opc[s], o='world', bf=1, ib=0, tc=1)
        w = cmd.aliasAttr(bs[0], q=1)
        cmd.connectAttr(ctrl+'.TopHair', bs[0]+'.'+w[0])

    cmd.connectAttr(ctrl+'.Automation', nucleus[0]+'.enable')
    cmd.connectAttr(ctrl+'.StartFrame', nucleus[0]+'.startFrame')

    tempAttrs1='''
    setAttr "hhhhh.simulationMethod" 1;
    setDrivenKeyframe -currentDriver ccccc.Automation hhhhh.simulationMethod;
    setAttr "ccccc.Automation" 1;
    setAttr "hhhhh.simulationMethod" 3;
    setDrivenKeyframe -currentDriver ccccc.Automation hhhhh.simulationMethod;
    setAttr "ccccc.Automation" 0;
    '''
    
    tempAttrs2=tempAttrs1.replace('hhhhh', hs[0])
    attrs=tempAttrs2.replace('ccccc', ctrl)
    
    mel.eval(attrs)

    return [hs,flc,stc,flcgrp,opc,opcgrp,nucleus]

    
    
def curveByJointChain(reb):
    # SELECT A JOINTS ROOT
    cmd.select(hierarchy = 1)
    joints = cmd.ls(sl=1, l=1, type='joint')
    coords = []
    for j in joints:
        pos = cmd.xform(j, q=1, t=1, ws=1)
        coords.append(pos)
    curve = cmd.curve(p=coords)
    cmd.rebuildCurve(curve, ch=1, rpo=1, rt=0, end=1, kr=0, kcp=0, kep=1, kt=0, s=reb, d=3, tol=0.01)

    return curve
    


def createTextControl(myText):
    txt = cmd.textCurves(f="Arial", t=myText, n="first")
    curvest = cmd.listRelatives((cmd.listRelatives(ad=1, type='nurbsCurve')), p=1)
    cmd.parent(curvest, w=1)
    cmd.delete(ch=1)
    sel = cmd.ls(sl=1, l=1)
    cmd.makeIdentity(sel, apply=1, t=1, r=1, s=1, n=0, pn=1)
    curves = cmd.ls(sl=1, dag=1, shapes=1, ni=1, l=1)
    ctrl = cmd.createNode('transform', name=myText )
    cmd.parent(curves, ctrl, r=1, s=1)
    cmd.delete(curvest, txt)
    cmd.select(ctrl)
    mel.eval('CenterPivot')
    mel.eval('move -rpr 0 0 0 ;')
    cmd.makeIdentity(apply=1, t=1, r=1, s=1, n=0, pn=1)
    cmd.select(ctrl)
    mel.eval('SelectCurveCVsAll')
    mel.eval('scale -r -p 0cm 0cm 0cm 5 5 5 ;') 
    
    return ctrl


def PAD(i, padding):
    counter=str(i)
    while len(counter)<padding:
        counter='0'+counter
    return counter
