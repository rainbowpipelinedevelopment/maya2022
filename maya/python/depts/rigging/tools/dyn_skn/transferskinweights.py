"""
Comandi per trasferire le pesature skinCluster fra oggetti
@author e.pili
"""
import maya.cmds as cmd


def transferSkinWeights():
    """First select source, then the other meshes"""
    copySkinList = cmd.ls(sl=1)
    sourceSkinCluster = ""
    destSkinCluster = ""
    historyList = cmd.listHistory(copySkinList[0])
    
    for o in historyList:
        if cmd.objectType(o) == "skinCluster":
            sourceSkinCluster = o
    sourceInfluenceList = cmd.skinCluster(sourceSkinCluster, q=1, influence=1)
        
    for i in range(1,len(copySkinList)):
        try:
            cmd.skinCluster(copySkinList[i],e=1, ub=1)
        except:
            pass
        
        cmd.skinCluster(sourceInfluenceList, copySkinList[i])
        
        historyList = cmd.listHistory(copySkinList[i])
        for o in historyList:
            if cmd.objectType(o) == "skinCluster":
                destSkinCluster = o
                
        cmd.copySkinWeights(ss=sourceSkinCluster, ds=destSkinCluster, \
        noMirror=1, \
        surfaceAssociation="closestPoint", \
        influenceAssociation="closestJoint")


def transferMultiToOneExplicit (objects):
    """
    Transfer skinCluster from an object list to a single target object
    """
    targetObj = objects[-1]
    objects.pop(-1)
    
    #detach skin
    historyList = cmd.listHistory(targetObj)
    for o in historyList:
        if cmd.objectType(o) == "skinCluster":
            cmd.delete(o)
            break
    
    #skinning target
    influencesAll=getInfluenceList(objects)
    targetSkinCluster = cmd.skinCluster(targetObj,influencesAll)[0]
    
    cmd.select(objects, r=1)
    cmd.select(targetObj, add=1)
    cmd.copySkinWeights(noMirror=1,
        surfaceAssociation="closestPoint", \
        influenceAssociation="closestJoint")


def transferMultiToOne():
    sel = cmd.ls(sl=1)
    if not sel:
        print('You have to select at least two mesh, with the first one skinned already...')
        return
    transferMultiToOneExplicit(sel)
        

def getInfluenceList(objList):
    """ 
    Return list of influence affecting the skinClusters of the give objects
    """
    influencesAll=[]
    for obj in objList :
        influence= cmd.skinCluster (obj ,q=True,influence=True)
        influenceSize = len(influence)
        for i in range (influenceSize) :
            influencesAll.append(influence[i])

    influencesAllSize = len(influencesAll)
    uniqueList = []
    for i in range (influencesAllSize):
        if uniqueList.count(influencesAll[i])== 0:
            uniqueList.append(influencesAll[i])
            
    return uniqueList
