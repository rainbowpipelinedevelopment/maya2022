import os
import importlib

iconpath = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'rigging')


def run_jcbc(*args):
    import depts.rigging.tools.dyn_skn.jointsChainByCurve as jcbc
    importlib.reload(jcbc)
    jcbc.executeProcedures()

jcbcTool = {
    "name": "Dynamic Chains RBW",
    "launch": run_jcbc,
    "statustip": "Dynamic Chains RBW",
    "icon": os.path.join(iconpath, "dyn_rbw.png")
}


def run_dwe(*args):
    import depts.rigging.tools.dyn_skn.fari_deformersWeightsEditor as dwe
    importlib.reload(dwe)
    dwe.DeformersWeightsEditor.showUI()

dweTool = {
    "name": "Weights Editor",
    "launch": run_dwe,
    "statustip": "Deformers Weights Editor",
    "icon": os.path.join(iconpath, "copyVertex.png")
}


def run_bpmc(*args):
    import depts.rigging.tools.dyn_skn.bindPreMatrixConnector as bpmc
    importlib.reload(bpmc)

bpmcTool = {
    "name": "Bind PreMatrix",
    "launch": run_bpmc,
    "statustip": "Bind PreMatrix Connector",
    "icon": os.path.join(iconpath, "bindprematrix.png")
}


def run_xswmto(*args):
    import depts.rigging.tools.dyn_skn.transferskinweights as xswmto
    importlib.reload(xswmto)
    xswmto.transferMultiToOne()

xswmtoTool = {
    "name": "Xfer Multi to One",
    "launch": run_xswmto,
    "statustip": "xferSkinWeightsMultiToOne",
    "icon": os.path.join(iconpath, "xferSkinWeightsMultiToOne.png")
}




tools = [
    jcbcTool,
    dweTool,
    bpmcTool,
    xswmtoTool,
]


PackageTool = {
    "name": "dyn_skn",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
