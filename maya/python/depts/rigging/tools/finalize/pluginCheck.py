__author__ = 'a.rastelli'

import importlib

from PySide2 import QtGui, QtCore, QtWidgets
from shiboken2 import wrapInstance

import maya.cmds as cmd
import maya.OpenMayaUI as omui

import api.scene
importlib.reload(api.scene)



class MainWindow (QtWidgets.QWidget):
    def __init__(self):
        """
        Costruttore della classe.
        Definisce l'interfaccia (molto semplice, solamente una lista di
        checkbox con due pulsanti).
        Per costruire l'interfaccia, e' inserito il codice di maya per ottenere
        la lista dei plugin sconosciuti legati al file aperto.
        Tale lista va a popolare una sequenza di checkbox, utile per poter
        selezionare specificamente il plugin sconosciuto che si vuole togliere
        tra i requirements della scena di Maya.
        @return
        """
        ptr = omui.MQtUtil.mainWindow()
        widget = wrapInstance(int(ptr), QtWidgets.QWidget)
        super(MainWindow, self).__init__(widget)

        self.listPlugins = api.scene.listUnknowPlugin()
        self.listCheckboxPlugins = []

        vlayout = QtWidgets.QVBoxLayout()

        for plugin in self.listPlugins:
            hlayout = QtWidgets.QHBoxLayout()
            checkbox = QtWidgets.QCheckBox(plugin)
            self.listCheckboxPlugins.append(checkbox)
            hlayout.addWidget(checkbox)
            vlayout.addLayout(hlayout)

        hlayout = QtWidgets.QHBoxLayout()
        self.removeplugin = QtWidgets.QPushButton()
        self.removeplugin.setText("Remove Selected")
        self.removeplugin.clicked.connect(self.removeselected)
        hlayout.addWidget(self.removeplugin)

        self.removeallplugin = QtWidgets.QPushButton()
        self.removeallplugin.setText("Remove All")
        self.removeallplugin.clicked.connect(self.removeAll)
        hlayout.addWidget(self.removeallplugin)

        hlayout.addWidget(QtWidgets.QSplitter())
        vlayout.addLayout(hlayout)

        self.setLayout(vlayout)
        self.resize(200, 400)
        self.adjustSize()
        self.setWindowFlags(QtCore.Qt.Window)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)

    def removeselected(self):
        """
        Rimozione dei plugin selezionati con controllo nel caso in cui
        uno dei plugin non possa essere rimosso e manda lo script in errore.
        @return
        """
        for ckbox in self.listCheckboxPlugins:
            if not ckbox.isChecked():
                continue
            try:
                cmd.unknownPlugin(ckbox.text(), remove=True)
                ckbox.setCheckable(False)
                ckbox.setDisabled(True)
            except:
                QtWidgets.QMessageBox.critical(
                    None, "Unknown Plugin Error",
                    "Impossible to remove %s." % ckbox.text())

    def removeAll(self):
        """
        Rimuove tutti i plugin nella lista indicata
        In caso di errori su uno o piu' tra i plugin da rimuovere
        viene mostrata una finestra con la lista degli elementi che hanno
        dato errore.
        @return
        """
        listNotRemoved = []
        for ckbox in self.listCheckboxPlugins:
            try:
                cmd.unknownPlugin(ckbox.text(), remove=True)
                ckbox.setDisabled(True)
                ckbox.setCheckable(False)
            except:
                listNotRemoved.append(ckbox.text())

        # Visualizzazione dei messaggi di fallimento o successo
        if len(listNotRemoved):
            listJoinedNotRemoved = ", ".join(listNotRemoved)
            QtWidgets.QMessageBox.critical(
                None, "Unknown Plugin Error",
                "Impossible to remove %s", listJoinedNotRemoved)
        else:
            QtWidgets.QMessageBox.information(
                None, "Unknown Plugin",
                "All Unknown plugins successfuly removed")
