# usage:
#
# import depts.rigging.cats.deleteSelectedTweak
# reload(depts.rigging.cats.deleteSelectedTweak)

import maya.cmds as cmds


def deleteSelectedTweak():
    sel = cmds.ls(sl=True, l=True)
    for s in sel:
        shp = cmds.listRelatives(sel, s=True, f=True)
        nodo = cmds.listConnections(shp, type='tweak')
        cmds.delete(nodo)
