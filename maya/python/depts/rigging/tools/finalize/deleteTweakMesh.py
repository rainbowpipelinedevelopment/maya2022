# usage:
#
# import depts.rigging.cats.deleteTweakMesh
# reload(depts.rigging.cats.deleteTweakMesh)

import maya.cmds as cmds


def deleteTweakMesh():
    sel = cmds.ls(type='mesh')
    nodo = cmds.listConnections(sel, type='tweak')
    print(nodo)
    cmds.delete(nodo)

