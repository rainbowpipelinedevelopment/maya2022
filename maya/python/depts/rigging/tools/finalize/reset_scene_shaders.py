'''
@brief Script per resettare tutti gli shaders complessi (anche con textures) e portarli a shaders semplici con un colore che e' la media di 100 punti random dello shader originale...
@author a.muraca
@date 15/04/2013

@details questo script assegna a tutti i nodi <i>shape</i> presenti in scena,
  un nuovo materiale con un colore che e' la media di n punti del precedente shader complesso.

#INSTRUCTION Run the procedure, select a mesh(s) with a shader and DO setAverageColor()

'''

import random

import maya.cmds as cmd
import maya.mel as mel


#questa era la vecchia procedura che rimuove tutti gli shaders e lascia tutto a lambert1 initial shader...
def resetSceneShaders():
	objects=cmd.ls(s=1)
	cmd.select(objects)
	cmd.sets(fe='initialShadingGroup')
	for ma in cmd.ls(mat=1):
		if ma!='lambert1' and ma!='particleCloud1':
			cmd.delete(ma)
	mel.eval('hyperShadePanelMenuCommand("hyperShadePanel1", "deleteUnusedNodes")')
	allItems=cmd.ls(assemblies=1)
	cmd.select(allItems,r=1)
	cmd.select('lambert1',add=1,d=1)
	mel.eval('hyperShade -assign "lambert1";')
	cmd.select(clear=1)
	print('Scene cleaned of all the shader networks.')


def setAverageColorSingle(target):
	sample = 100		#RANDOM SAMPLE POINTS
	colorArray = []
	
	count = cmd.polyEvaluate(v = True)
	
	conn = cmd.listConnections(target, type = "shadingEngine")[0]	  #lamber2SG
	inputShader = cmd.listConnections("%s.surfaceShader"%conn, d = True)[0]			#lambert2
	
	if (cmd.listConnections("%s.color"%inputShader, s=True ) != None):
		
		texture = cmd.convertSolidTx("%s.color"%inputShader,"%s"%target, bm = 3, rx = 256, ry = 256)[0]
		if (texture):
			for i in range(sample):
				vertex = random.randint(0,count)				  
				uv = cmd.polyListComponentConversion("%s.vtx[%s]"%(target, vertex), fv = 1, tuv = 1)[0]   
				posUV = cmd.polyEditUV(uv, query = True)											   
				colorPixel = cmd.colorAtPoint(texture, o='RGB', u=posUV[0], v=posUV[1])				   
				
				colorArray.append(colorPixel)
				
			midColor = average(colorArray)
			path = cmd.getAttr("%s.fileTextureName"%texture)
			cmd.sysFile( path , delete=True )
			cmd.delete(texture)
			return midColor


def disconnectToColor(target, midColor):
	if (midColor != None):
		conn = cmd.listConnections(target, type = "shadingEngine")[0]
		inputShader = cmd.listConnections("%s.surfaceShader"%conn, d = True)[0] 
		newShader = cmd.shadingNode("lambert", asShader=True, n = "%s_ac"%inputShader)
		
		shadingG = cmd.sets( r=1, nss=1, em=1, n= newShader + "SG" )
		cmd.connectAttr (newShader + ".outColor", shadingG+".surfaceShader",f=1)
		cmd.sets (target, e=1, forceElement = shadingG)
		
		cmd.setAttr("%s.color"%newShader, midColor[0], midColor[1], midColor[2], type = "double3")


def setAverageColor():
	targets = cmd.ls(selection = True, type = "mesh", dag = True)
	print(targets)
	#filtrare le eventuali shape originali che non hanno connessione con shaders
	#if len(targets) > 1:
	#	print 'this shape has more than one shape: '+str(targets)
	#	targets=targets[0]
	
	midTotColorArray = []
	
	if (targets != []):
		for i in range(len(targets)):
			try:
				midColorArray = []
				midColorArray.append(targets[i])
				midColorArray.append(setAverageColorSingle(targets[i]))
				midTotColorArray.append(midColorArray)
			except:
				print('there was some problems in here: '+str(targets[i]))
				pass
	else:
		print("You must select a geometry")
		
	for i in range(len(targets)):
		try:
			disconnectToColor(midTotColorArray[i][0], midTotColorArray[i][1])
		except:
			print('there was some problems in here: '+str(targets[i]))
			pass
	print("DONE for these targets: "+str(targets))


def average(array):
	sumR = 0
	sumG = 0
	sumB = 0
	for r in range(len(array)):
		sumR += array[r][0]
	for g in range(len(array)):
		sumG += array[g][1]
	for b in range(len(array)):
		sumB += array[b][2]
	return [(sumR/len(array)),(sumG/len(array)),(sumB/len(array))]


def setAverageColorAll():
	allMeshShapes=cmd.ls(type='mesh')
	meshesClean=[]
	for shape in allMeshShapes:
		cmd.select(shape, r=1)
		uppernode=(cmd.pickWalk(direction='up'))
		cmd.select(uppernode, r=1)
		transform=cmd.ls(sl=1)
		if not transform[0] in meshesClean:
			meshesClean.append(transform[0])
	print(meshesClean)
	print(len(meshesClean))
	for mesh in meshesClean:
		cmd.select(mesh, r=1)
		setAverageColor()
