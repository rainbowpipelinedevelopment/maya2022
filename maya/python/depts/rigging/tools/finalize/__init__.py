import os
import importlib

iconpath = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'rigging')

def run_dst(*args):
    import depts.rigging.tools.finalize.deleteSelectedTweak as dst
    importlib.reload(dst)
    dst.deleteSelectedTweak()

dstTool = {
    "name": "Delete Tweak SelObj",
    "launch": run_dst,
    "statustip": "Delete Tweak SelObj",
    "icon": os.path.join(iconpath, "delTweakSelObj.png")
}


def run_dtm(*args):
    import depts.rigging.tools.finalize.deleteTweakMesh as dtm
    importlib.reload(dtm)
    dtm.deleteTweakMesh()

dtmTool = {
    "name": "Delete Tweak Mesh",
    "launch": run_dtm,
    "statustip": "Delete Tweak Mesh",
    "icon": os.path.join(iconpath, "delTweakAllMesh.png")
}


def run_plck(*args):
    import depts.rigging.tools.finalize.pluginCheck as plck
    importlib.reload(plck)
    import api.scene
    importlib.reload(api.scene)

    import maya.cmds as cmd

    if api.scene.listUnknowPlugin() is None:
        import PySide2.QtWidgets as QtWidgets
        QtWidgets.QMessageBox.information(None, "No bad plugin Found", "No unknown plugin found in this file")
        return

    window = pc.MainWindow()
    window.show()

plckTool = {
    "name": "Unknown Plugin Check",
    "launch": run_plck,
    "statustip": "Unknown Plugin Check",
    "icon": os.path.join(iconpath, "pluginCheck.png")
}


def run_lts(*args):
    import depts.rigging.tools.finalize.make_loc_tracker as lts
    importlib.reload(lts)
    lts.main()

ltsTool = {
    "name": "Loc Tracker SET",
    "launch": run_lts,
    "statustip": "Loc Tracker SET",
    "icon": os.path.join(iconpath, "makeLocTracker.png")
}


def run_rss(*args):
    import depts.rigging.tools.finalize.reset_scene_shaders as rss
    importlib.reload(rss)
    rss.resetSceneShaders()

rssTool = {
    "name": 'Remove All Shader',
    "launch": run_rss,
    "statustip": "Remove All Shader",
    "icon": os.path.join(iconpath, "resetShader.png")
}




tools = [
    dstTool,
    dtmTool,
    plckTool,
    ltsTool,
    rssTool,
]


PackageTool = {
    "name": "finalize",
    "tools": tools,
    "icon_size": "16",
    "icon_only": False
}
