"""
@brief proc to build the loc tracker for compositing purpose
@author d.viezzoli

"""

import maya.cmds as cmd 
import maya.mel as mel 


#selezionare un controllo di Ab
def main():
	#determino se e' un rig nuovo o vecchio (winxII)
	sel=cmd.ls(sl=1)
	oldRig=0
	if len(sel):
		place=sel[0].find('_')
		if place == 3:
			#case abAutorig NUOVO
			prefix=(sel[0][:3]+'_')
		elif place==4:
			if sel[0]=='CTRL_upHip':
				#case old rig (selected by automatic line in the batch tool..)
				prefix=''
				oldRig=1
	else:
		print('You should select a body rig control, please check')
		return 0
	locs=['null_'+prefix+'hip_jnt','null_'+prefix+'lf_ankle_jnt','null_'+prefix+'rt_ankle_jnt','null_'+prefix+'lf_wrist_jnt','null_'+prefix+'rt_wrist_jnt','null_'+prefix+'head_a_jnt']
	jnts=[]
	#butto i vecchi se ho ri-lanciato la procedura..
	if cmd.objExists('null_grp'):
		cmd.delete('null_grp')
	for loc in locs:
	    j=loc.replace('null_','')
	    if not cmd.objExists(j):
	        if "ankle" in j:
	            j=j.replace('lf_','lt_').replace('RIG_','Rear_')
	        if "wrist" in j:
	            j=j.replace('lf_','lt_').replace('wrist','ankle').replace('RIG_','Front_')
	    jnts.append(j)
	index=0    
	for loc in locs:
		if not oldRig:
			if not cmd.objExists(loc):
				cmd.spaceLocator(name=loc)
				if cmd.objExists(jnts[index]):
					cmd.parentConstraint(jnts[index],loc,w=1)
					cmd.scaleConstraint(jnts[index],loc,w=1)
				else:
					cmd.parentConstraint(jnts[0],loc,w=1)
					cmd.scaleConstraint(jnts[0],loc,w=1)
		index=index+1
		if oldRig:
			if not cmd.objExists(loc):
				cmd.spaceLocator(name=loc)
				#jnt=loc.replace('null_','')
				if loc.count("hip"):
					jnt='JA_back_01'
				if loc.count("lf_ankle"):
					jnt='JA_foot_L'
				if loc.count("rt_ankle"):
					jnt='JA_foot_R'
				if loc.count("lf_wrist"):
					jnt='JA_wristFix_01_L'
				if loc.count("rt_wrist"):
					jnt='JA_wristFix_01_R'
				if loc.count("head"):
					jnt='headJoint'
				if cmd.objExists(jnt):
					cmd.parentConstraint(jnt,loc,w=1)
					#cmd.scaleConstraint(jnt,loc,w=1)
				else:
					cmd.parentConstraint(locs[0],loc,w=1)
					#cmd.scaleConstraint(locs[0],loc,w=1)
	cmd.select(locs,r=1)
	grp=cmd.group(name='null_grp')
	if not oldRig:
		cmd.parent(grp,prefix+'controls_grp')
	if oldRig:
		cmd.parent(grp, 'CHR')
	if not cmd.objExists('LOC_TRACKER'):
		cmd.sets(locs,name='LOC_TRACKER')
	else:
		cmd.sets(locs, include='LOC_TRACKER')
	cmd.setAttr(grp+'.visibility',0)
	cmd.select(clear=1)
	return 1
