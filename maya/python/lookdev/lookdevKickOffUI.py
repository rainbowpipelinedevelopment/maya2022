import importlib
from PySide2 import QtWidgets, QtGui, QtCore
import os
import functools
import shutil

import maya.cmds as cmd

import api.log
import api.asset
import api.savedAsset
import api.widgets.rbw_UI as rbwUI
import lookdev.lookdevTools as lookdevTools

importlib.reload(api.log)
# importlib.reload(api.asset)
# importlib.reload(api.savedAsset)
# importlib.reload(rbwUI)
importlib.reload(lookdevTools)


################################################################################
# @brief      This class describes a lookdev kick off ui.
#
class LookdevKickOffUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super().__init__()

        if cmd.window('LookdevKickOffUI', exists=True):
            cmd.deleteUI('LookdevKickOffUI')

        self.mcFolder = os.getenv('MC_FOLDER').replace('\\', '/')
        self.id_mv = os.getenv('ID_MV')

        self.deptTypes = {
            'modeling': 'hig',
            'surfacing': 'rnd',
            'grooming': 'rnd',
            'set': 'hig'
        }

        self.assets = lookdevTools.getAllAssets()

        self.setObjectName('LookdevKickOffUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)

        self.mainLayout.addWidget(self.splitter)

        ########################################################################
        # LEFT PANEL
        #
        self.leftWidget = rbwUI.RBWFrame(layout='V', radius=5)

        # ------------------------ asset features ---------------------------- #
        self.assetFeaturesGroup = rbwUI.RBWGroupBox(title='Asset:', fontSize=13, layout='V')

        self.deptComboBox = rbwUI.RBWComboBox(text='Dept:', bgColor='transparent')
        self.deptComboBox.activated.connect(self.setDept)

        self.assetFeaturesGroup.addWidget(self.deptComboBox)

        self.categoryComboBox = rbwUI.RBWComboBox(text='Category:', bgColor='transparent')
        self.categoryComboBox.activated.connect(self.fillGroupComboBox)

        self.assetFeaturesGroup.addWidget(self.categoryComboBox)

        self.groupComboBox = rbwUI.RBWComboBox(text='Group:', bgColor='transparent')
        self.groupComboBox.activated.connect(self.fillNameComboBox)

        self.assetFeaturesGroup.addWidget(self.groupComboBox)

        self.nameComboBox = rbwUI.RBWComboBox(text='Name:', bgColor='transparent')
        self.nameComboBox.activated.connect(self.fillVariantComboBox)

        self.assetFeaturesGroup.addWidget(self.nameComboBox)

        self.variantComboBox = rbwUI.RBWComboBox(text='Variant:', bgColor='transparent')
        self.variantComboBox.activated.connect(self.setCurrentAsset)

        self.assetFeaturesGroup.addWidget(self.variantComboBox)

        self.leftWidget.addWidget(self.assetFeaturesGroup)

        self.leftWidget.addStretch()

        # --------------------------- add button ---------------------------- #
        self.addLayerButton = rbwUI.RBWButton(text='add Layer', icon=['add.png'], size=[182, 35])
        self.addLayerButton.clicked.connect(self.addLayer)

        self.leftWidget.addWidget(self.addLayerButton)

        self.splitter.addWidget(self.leftWidget)

        ########################################################################
        # RIGHT PANEL
        #
        self.rightPanel = rbwUI.RBWFrame(layout='V', radius=5)

        # --------------------------- layers tree ---------------------------- #
        self.layersTree = rbwUI.RBWTreeWidget(['Tod', 'Camera', 'Resolution', 'Quality', 'Frames', 'Delete'])
        self.layersTree.setColumnWidth(0, 100)
        self.layersTree.setColumnWidth(1, 100)
        self.layersTree.setColumnWidth(2, 100)
        self.layersTree.setColumnWidth(2, 100)
        self.layersTree.setColumnWidth(4, 140)
        self.layersTree.setColumnWidth(5, 15)

        self.rightPanel.addWidget(self.layersTree)

        # --------------------------- kickOff button ------------------------- #
        self.kickOffButton = rbwUI.RBWButton(text='KickOff', icon=['kickOff.png'], size=[150, 35])
        self.kickOffButton.clicked.connect(self.kickOff)

        self.rightPanel.addWidget(self.kickOffButton)

        self.splitter.addWidget(self.rightPanel)

        self.fillDeptComboBox()

        self.splitter.setStretchFactor(0, 1)
        self.splitter.setStretchFactor(1, 3)

        self.setMinimumSize(840, 250)
        self.setTitle('Lookdev KickOff UI')
        self.setIcon('kickOff.png')
        self.setFocus()

    ############################################################################
    # @brief      fill the depts combobox.
    #
    def fillDeptComboBox(self):
        self.depts = self.deptTypes.keys()
        self.deptComboBox.clear()
        self.deptComboBox.addItems(self.depts)
        self.deptComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

    ############################################################################
    # @brief      Sets the dept.
    #
    def setDept(self):
        self.dept = self.deptComboBox.currentText()
        self.deptType = self.deptTypes[self.dept]
        self.autoFillAssetCombos()

    ############################################################################
    # @brief      fill the category combobox.
    #
    def fillCategoryComboBox(self):
        self.categoryComboBox.clear()

        for category in sorted(self.assets.keys()):
            self.categoryComboBox.addItem(category)
        self.categoryComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

    ############################################################################
    # @brief      fill the group combobox.
    #
    def fillGroupComboBox(self):
        self.groupComboBox.clear()
        self.currentCategory = self.categoryComboBox.currentText()

        for group in sorted(self.assets[self.currentCategory].keys()):
            self.groupComboBox.addItem(group)
        self.groupComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

    ############################################################################
    # @brief      fill the name combobox.
    #
    def fillNameComboBox(self):
        self.nameComboBox.clear()
        self.currentGroup = self.groupComboBox.currentText()

        for name in sorted(self.assets[self.currentCategory][self.currentGroup].keys()):
            self.nameComboBox.addItem(name)
        self.nameComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

    ############################################################################
    # @brief      fill the varinat combobox.
    #
    def fillVariantComboBox(self):
        self.variantComboBox.clear()
        self.currentName = self.nameComboBox.currentText()

        for variant in sorted(self.assets[self.currentCategory][self.currentGroup][self.currentName]):
            self.variantComboBox.addItem(variant)
        self.variantComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

    ############################################################################
    # @brief      Sets the current asset.
    #
    def setCurrentAsset(self):
        self.currentVariant = self.variantComboBox.currentText()
        self.currentAsset = api.asset.Asset(params={'category': self.currentCategory, 'group': self.currentGroup, 'name': self.currentName, 'variant': self.currentVariant})
        self.autofillLayersGroup()

    ############################################################################
    # @brief      Auto fill the asset combo if it can.
    #
    def autoFillAssetCombos(self):
        wipSaveNode = '{}_saveNode'.format(self.dept)
        if cmd.objExists(wipSaveNode):
            wipSaveNodes = cmd.ls(wipSaveNode)
            if len(wipSaveNodes) == 1:
                if cmd.getAttr('{}.approvation'.format(wipSaveNode)) == 'wip':
                    category = cmd.getAttr('{}.genre'.format(wipSaveNode))
                    group = cmd.getAttr('{}.group'.format(wipSaveNode))
                    name = cmd.getAttr('{}.name'.format(wipSaveNode))
                    variant = cmd.getAttr('{}.variant'.format(wipSaveNode))

                    self.fillCategoryComboBox()
                    categoryIndex = self.categoryComboBox.findText(category, QtCore.Qt.MatchFixedString)
                    self.categoryComboBox.setCurrentIndex(categoryIndex)

                    self.fillGroupComboBox()
                    groupIndex = self.groupComboBox.findText(group, QtCore.Qt.MatchFixedString)
                    self.groupComboBox.setCurrentIndex(groupIndex)

                    self.fillNameComboBox()
                    nameIndex = self.nameComboBox.findText(name, QtCore.Qt.MatchFixedString)
                    self.nameComboBox.setCurrentIndex(nameIndex)

                    self.fillVariantComboBox()
                    variantIndex = self.variantComboBox.findText(variant, QtCore.Qt.MatchFixedString)
                    self.variantComboBox.setCurrentIndex(variantIndex)
                    self.setCurrentAsset()
                else:
                    self.fillCategoryComboBox()
            else:
                self.fillCategoryComboBox()
        else:
            self.fillCategoryComboBox()

    ############################################################################
    # @brief      Auto fill layers group.
    #
    def autofillLayersGroup(self):
        self.layersTree.clear()

        self.lookdevDeptFolder = os.path.join(
            self.mcFolder,
            'scenes',
            self.currentAsset.getAssetFolder(),
            'lookdev',
            self.dept
        ).replace('\\', '/')

        if os.path.exists(self.lookdevDeptFolder):
            vrsceneFolder = os.path.join(self.lookdevDeptFolder, 'VRSCENE').replace('\\', '/')
            if os.path.exists(vrsceneFolder):
                self.layers = self.getLayers()
                for layer in self.layers:
                    if self.layers[layer]['version']:
                        vrsceneInfoJson = os.path.join(vrsceneFolder, layer, self.layers[layer]['version'], 'settings.json').replace('\\', '/')
                        if os.path.exists(vrsceneInfoJson):
                            vrsceneInfo = api.json.json_read(vrsceneInfoJson)
                            self.addLayer(vrsceneInfo['todIndex'], vrsceneInfo['camera'], vrsceneInfo['resolution'], vrsceneInfo['frames'], vrsceneInfo['quality'])
                        else:
                            self.addLayer()
                    else:
                        self.addLayer()
        else:
            api.log.logger().debug('No lookdev save file for the given asset under {}.'.format(self.dept.capitalize()))

    ############################################################################
    # @brief      Gets the layers.
    #
    # @return     The layers.
    #
    def getLayers(self):
        layers = {}
        renderLayerQuery = "SELECT `name`, `id` FROM `renderLayer` WHERE `type` = 'asset' AND `linkId` = {}".format(self.currentAsset.variantId)
        renderLayerResult = api.database.selectQuery(renderLayerQuery)

        for tupla in renderLayerResult:
            versionQuery = "SELECT `version` FROM `savedRenderLayer` WHERE `renderLayerId`={} ORDER BY `date` DESC".format(tupla[1])
            version = api.database.selectSingleQuery(versionQuery)
            if version:
                layers[tupla[0]] = {'version': version[0]}
            else:
                layers[tupla[0]] = {'version': None}

        return layers

    ############################################################################
    # @brief      Adds a layer widget.
    #
    # @param      tod         The tod
    # @param      camera      The camera
    # @param      resolution  The resolution
    # @param      frames      The frames
    #
    def addLayer(self, tod=None, camera='', resolution='2K', frames='', quality='Medium'):
        LayerTreeWidgetItem(tod, camera, resolution, frames, quality, self.layersTree)

    ############################################################################
    # @brief      Initializes the reference folder.
    #
    def initializeReferenceFolder(self):
        self.referenceFolder = os.path.join(self.lookdevDeptFolder, 'REFERENCES').replace('\\', '/')
        if not os.path.exists(self.referenceFolder):
            os.makedirs(self.referenceFolder)
            commonReferenceFolder = '//speed.nas/library/02_production/01_content/_config/lookdev/nuke_env_imgs'

            bottomReference = os.path.join(commonReferenceFolder, 'bottomReference.psd')
            sideReference = os.path.join(commonReferenceFolder, 'sideReference.psd')

            newBottomReference = os.path.join(self.referenceFolder, 'bottomReference.psd')
            newSideReference = os.path.join(self.referenceFolder, 'sideReference.psd')

            shutil.copy2(bottomReference, newBottomReference)
            shutil.copy2(sideReference, newSideReference)

    ############################################################################
    # @brief      kickOff function.
    #
    def kickOff(self):
        savedAssetParams = {
            'asset': self.currentAsset,
            'dept': self.dept,
            'deptType': self.deptType,
            'approvation': 'wip',
            'note': 'Autosave from lookdev kickOff'
        }

        self.currentSavedAsset = api.savedAsset.SavedAsset(params=savedAssetParams)

        layersList = []
        root = self.layersTree.invisibleRootItem()
        for i in range(0, root.childCount()):
            item = root.child(i)
            layersList.append({'todIndex': item.getTodIndex(), 'camera': item.getCameraName(), 'frames': item.getFrames(), 'resolution': item.getResolution(), 'quality': item.getQuality()})

        for i in range(0, len(layersList)):
            currentLayer = layersList[i]
            for j in range(i + 1, len(layersList)):
                compareLayer = layersList[j]
                if currentLayer['todIndex'] == compareLayer['todIndex'] and currentLayer['camera'] == compareLayer['camera']:
                    rbwUI.RBWError(title='ATTENTION', text='Two or more layers have the same tod and camera.\nControl and try again.')
                    return

        if lookdevTools.checkScene(self.currentSavedAsset):
            success = self.currentSavedAsset.save()
            if not success:
                rbwUI.RBWError(title='ATTENTION', text='Some error during autosave.\nThe kickOff is abort.')
                return

            sceneParams = {
                'savedAsset': self.currentSavedAsset,
                'layers': layersList
            }

            lookdevTools.buildLookdevScene(sceneParams)

            self.initializeReferenceFolder()

            cmd.file(new=True, force=True)
        else:
            pass


################################################################################
# @brief      This class describes a layer tree widget item.
#
class LayerTreeWidgetItem(QtWidgets.QTreeWidgetItem):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self, tod=None, camera='', resolution='2K', frames='', quality='Medium', parent=None):
        super(LayerTreeWidgetItem, self).__init__(parent)

        self.tod = tod
        self.camera = camera
        self.resolution = resolution
        self.frames = frames
        self.quality = quality

        self.cameraTopGroup = lookdevTools.getCamera()

        if lookdevTools.getLightrig() in ['{}_shader_default_:CTRL_set'.format(os.getenv('PROJECT')), 'generic_shader_default_:CTRL_set']:
            self.tods = {
                0: 'studio',
                1: 'midday',
                2: 'night',
                3: 'morning',
                4: 'reflection',
                5: 'hybrid'
            }
        else:
            self.tods = {0: 'default', 1: 'snap'}

        self.qualities = ['Low', 'Medium']

        self.resolutions = ['4K', '2K', '1K']

        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        # tod combo
        self.todComboBox = rbwUI.RBWComboBox(bgColor='transparent', fieldColor='rgba(40, 40, 40, 150)', stretch=False)
        self.todComboBox.combo.setMinimumHeight(25)
        self.treeWidget().setItemWidget(self, 0, self.todComboBox)

        # camera combo
        self.cameraComboBox = rbwUI.RBWComboBox(bgColor='transparent', fieldColor='rgba(40, 40, 40, 150)', stretch=False)
        self.cameraComboBox.combo.setMinimumHeight(25)
        self.treeWidget().setItemWidget(self, 1, self.cameraComboBox)

        # resolution combo
        self.resolutionComboBox = rbwUI.RBWComboBox(bgColor='transparent', fieldColor='rgba(40, 40, 40, 150)', stretch=False)
        self.resolutionComboBox.combo.setMinimumHeight(25)
        self.treeWidget().setItemWidget(self, 2, self.resolutionComboBox)

        self.qualityComboBox = rbwUI.RBWComboBox(bgColor='transparent', fieldColor='rgba(40, 40, 40, 150)', stretch=False)
        self.qualityComboBox.combo.setMinimumHeight(25)
        self.treeWidget().setItemWidget(self, 3, self.qualityComboBox)

        # frames line edit
        self.framesLine = rbwUI.RBWLineEdit(bgColor='transparent', fieldColor='rgba(40, 40, 40, 150)', stretch=False)
        self.framesLine.edit.setMinimumHeight(25)
        self.treeWidget().setItemWidget(self, 4, self.framesLine)

        # delete button
        self.deleteButton = rbwUI.RBWButton(icon=['delete.png'], size=[80, 25], color='rgba(40, 40, 40, 150)')
        self.deleteButton.clicked.connect(functools.partial(self.delete))

        self.treeWidget().setItemWidget(self, 5, self.deleteButton)

        self.fillWidget()

    ############################################################################
    # @brief      delete a layer widget.
    #
    #
    def delete(self):
        root = self.treeWidget().invisibleRootItem()
        root.removeChild(self)

    ############################################################################
    # @brief      fill the widget fields.
    #
    def fillWidget(self):
        todsIndex = list(self.tods.keys())
        todsIndex.sort()
        for todIndex in todsIndex:
            self.todComboBox.addItem(self.tods[todIndex])
        if self.tod:
            self.todComboBox.setCurrentIndex(self.tod)
        else:
            self.todComboBox.setCurrentIndex(0)

        self.qualityComboBox.addItems(self.qualities)
        qualityIndex = self.qualityComboBox.findText(self.quality, QtCore.Qt.MatchFixedString)
        self.qualityComboBox.setCurrentIndex(qualityIndex)

        cameras = cmd.listRelatives(self.cameraTopGroup)
        for cameraGroup in cameras:
            cameraName = cameraGroup.split(':')[1].split('_')[0].split('CAMERA')[1]
            self.cameraComboBox.addItem(cameraName)
        if self.camera != '':
            cameraIndex = self.cameraComboBox.findText(self.camera, QtCore.Qt.MatchFixedString)
            self.cameraComboBox.setCurrentIndex(cameraIndex)

        self.resolutionComboBox.addItems(self.resolutions)
        resolutionIndex = self.resolutionComboBox.findText(self.resolution, QtCore.Qt.MatchFixedString)
        self.resolutionComboBox.setCurrentIndex(resolutionIndex)

        if self.frames != '':
            self.framesLine.setText(self.frames)
        else:
            self.framesLine.setText('1;24;46;69;91;114;136;158')

    ############################################################################
    # @brief      Gets the tod index.
    #
    # @return     The tod index.
    #
    def getTodIndex(self):
        return self.todComboBox.currentIndex()

    ############################################################################
    # @brief      Gets the camera name.
    #
    # @return     The camera name.
    #
    def getCameraName(self):
        return self.cameraComboBox.currentText()

    ############################################################################
    # @brief      Gets the resolution.
    #
    # @return     The resolution.
    #
    def getResolution(self):
        return self.resolutionComboBox.currentText()

    ############################################################################
    # @brief      Gets the frames.
    #
    # @return     The frames.
    #
    def getFrames(self):
        return self.framesLine.text()

    ############################################################################
    # @brief      Gets the quality.
    #
    # @return     The quality.
    #
    def getQuality(self):
        return self.qualityComboBox.currentText()
