import importlib
import os
import re
import shutil
from datetime import datetime
import time
from PySide2 import QtGui, QtWidgets

import maya.cmds as cmd
import maya.mel as mel
import maya.app.renderSetup.model.collection as collection
import vray.vray_maya_utils

import api.log
import api.vray
import api.scene
import api.asset
import api.savedAsset
import api.database
import api.exception
import api.renderlayer
import api.xgen
import api.shotgrid
import api.widgets.rbw_UI as rbwUI
import common.switchAssembly

importlib.reload(api.log)
importlib.reload(api.vray)
importlib.reload(api.scene)
# importlib.reload(api.asset)
# importlib.reload(api.savedAsset)
importlib.reload(api.database)
importlib.reload(api.exception)
importlib.reload(api.renderlayer)
importlib.reload(api.xgen)
importlib.reload(api.shotgrid)
# importlib.reload(rbwUI)
importlib.reload(common.switchAssembly)

masterLayerOverrides = {
    "width": 2048,
    "height": 2048,
    "pixelAspect": 1,
    "samplerType": 4,
    "aaFilterType": 6,
    "dmcMinSubdivs": 2,
    "dmcMaxSubdivs": 8,
    "dmcThreshold": 0.01,
    "giOn": 1,
    "secondaryEngine": 2
}


################################################################################
# @brief      Gets the project fps.
#
# @return     The project fps.
#
def getProjectFps():
    queryFPS = "SELECT fps FROM `p_prj_framerate` AS pf JOIN `p_framerate` AS f ON pf.framerate_id = f.id WHERE pf.id_mv = {}".format(os.getenv("ID_MV"))
    fps = api.database.selectQuery(queryFPS)[0][0]

    return int(fps)


################################################################################
# @brief      Gets all assets.
#
# @return     All assets.
#
def getAllAssets():
    query = "SELECT `category`, `group`, `name`, `variant` FROM `V_assetList` WHERE `projectID` = '{}'".format(os.getenv('ID_MV'))

    results = api.database.selectQuery(query)

    assets = {}

    for assetTupla in results:
        category = assetTupla[0]
        if category not in assets.keys():
            assets[category] = {}

    for assetTupla in results:
        category = assetTupla[0]
        group = assetTupla[1]
        if group not in assets[category].keys():
            assets[category][group] = {}

    for assetTupla in results:
        category = assetTupla[0]
        group = assetTupla[1]
        name = assetTupla[2]
        if name not in assets[category][group].keys():
            assets[category][group][name] = []

    for assetTupla in results:
        category = assetTupla[0]
        group = assetTupla[1]
        name = assetTupla[2]
        variant = assetTupla[3]
        if variant not in assets[category][group][name]:
            assets[category][group][name].append(variant)

    return assets


################################################################################
# @brief      Determines whether the specified parameters is this asset.
#
# @param      params  The parameters
#
# @return     True if the specified parameters is this asset, False otherwise.
#
def isThisAsset(params):
    thisAssetSaveNode = None

    for saveNode in cmd.ls(type='saveNode'):
        if ':' not in saveNode:
            thisAssetSaveNode = saveNode

    if thisAssetSaveNode:
        thisAssetGenre = cmd.getAttr('{}.genre'.format(thisAssetSaveNode))
        thisAssetGroup = cmd.getAttr('{}.group'.format(thisAssetSaveNode))
        thisAssetName = cmd.getAttr('{}.name'.format(thisAssetSaveNode))
        thisAssetVariant = cmd.getAttr('{}.variant'.format(thisAssetSaveNode))

        if thisAssetGenre == params['category'] and thisAssetGroup == params['group'] and thisAssetName == params['name'] and thisAssetVariant == params['variant']:
            return True
        else:
            return False
    else:
        return False


################################################################################
# @brief      Gets the component path.
#
# @param      componentParams  The component parameters
#
# @return     The component path.
#
def getComponentPath(componentParams):
    params = componentParams.copy()

    assetParams = params.pop('asset')
    try:
        asset = api.asset.Asset(params=assetParams)
    except api.exception.InvalidAsset:
        return None

    params['asset'] = asset

    savedAsset = api.savedAsset.SavedAsset(params=params)
    if savedAsset.getLatest():
        return os.path.join(asset.content, savedAsset.getLatest()[2]).replace('\\', '/')
    else:
        return None


################################################################################
# @brief      import the want component reference in scene.
#
# @param      component  The component
#
def importComponents(component):
    componentsDict = {
        'lightrig': {'asset': {'id_mv': 1, 'category': 'lightrig', 'group': os.getenv('PROJECT'), 'name': 'shader', 'variant': 'default'}, 'dept': 'surfacing', 'deptType': 'rnd', 'approvation': 'def'},
        'camera': {'asset': {'id_mv': 1, 'category': 'library', 'group': 'viewtool', 'name': 'camera', 'variant': 'lookdev'}, 'dept': 'surfacing', 'deptType': 'rnd', 'approvation': 'def'},
        'geos': {'asset': {'id_mv': 1, 'category': 'library', 'group': 'viewtool', 'name': 'geos', 'variant': 'lookdev'}, 'dept': 'surfacing', 'deptType': 'rnd', 'approvation': 'def'},
        'sphereBall': {'asset': {'id_mv': 1, 'category': 'library', 'group': 'viewtool', 'name': 'sphereball', 'variant': 'lookdev'}, 'dept': 'surfacing', 'deptType': 'rnd', 'approvation': 'def'}
    }

    if getProjectFps() == 25:
        componentsDict['camera']['asset']['variant'] = '25'

    if component != 'all':
        componentsDictToImport = {component: componentsDict[component]}
    else:
        importRenderElememts()
        componentsDictToImport = componentsDict

    for component in componentsDictToImport:
        componentParams = componentsDictToImport[component]
        componentPath = getComponentPath(componentParams)

        if not componentPath:
            componentParams['asset']['group'] = 'generic'
            componentPath = getComponentPath(componentParams)

        componentNamespace = '{}_{}_{}_'.format(componentParams['asset']['group'], componentParams['asset']['name'], componentParams['asset']['variant'])
        if not cmd.namespace(exists=componentNamespace):
            api.scene.addToLibraryGroup(componentPath, componentNamespace)
        else:
            rbwUI.RBWError(title='ATTENTION', text='The namespace {} already exists. Please clean the scene and try again.'.format(componentNamespace))


################################################################################
# @brief      delete the want component reference in scene.
#
# @param      component  The component
#
def deleteComponents(component):
    componentsDict = {
        'lightrig': '{}_shader_default_RN'.format(os.getenv('PROJECT')),
        'camera': 'viewtool_camera_lookdev_RN',
        'geos': 'viewtool_geos_lookdev_RN',
        'sphereBall': 'viewtool_sphereball_lookdev_RN'
    }

    if getProjectFps() == 25:
        componentsDict['camera'] = 'viewtool_camera_25_RN'

    if not cmd.objExists(componentsDict['lightrig']):
        componentsDict['lightrig'] = 'generic_shader_default_RN'

    if component != 'all':
        componentsToDelete = {component: componentsDict[component]}
    else:
        deleteRenderElements()
        componentsToDelete = componentsDict

    for component in componentsToDelete:
        componentNode = componentsToDelete[component]
        if cmd.objExists(componentNode):
            cmd.select('{}:*'.format(componentNode.split('RN')[0]))
            animCurves = cmd.listConnections(t='animCurve')
            cmd.delete(animCurves)
            cmd.cutKey(componentNode, hi='below')
            if cmd.lockNode(componentNode, query=True, lock=True):
                cmd.lockNode(componentNode, lock=False)
            try:
                referencePath = cmd.referenceQuery(componentNode, filename=True, withoutCopyNumber=True)
                cmd.file(referencePath, removeReference=True)
            except:
                cmd.delete(componentNode)

    libraryElements = cmd.listRelatives('LIBRARY')

    if not libraryElements:
        cmd.delete('LIBRARY')


################################################################################
# @brief      import the lookdev render elements in scene.
#
def importRenderElememts():
    renderElememtsFile = os.path.join(
        '//speed.nas',
        'library',
        '02_production',
        '01_content',
        '_config',
        'lookdevRenderElements.mb'
    ).replace('\\', '/')

    renderElementsFilter = cmd.itemFilter(byType='VRayRenderElement')
    renderElementsSetFilter = cmd.itemFilter(byType='VRayRenderElementSet')
    allRenderElementsFilter = cmd.itemFilter(union=(renderElementsFilter, renderElementsSetFilter))

    renderElementsLookdevFilter = cmd.itemFilter(byName="LOOKDEV_*")
    allRenderElementsFilter = cmd.itemFilter(intersect=(allRenderElementsFilter, renderElementsLookdevFilter))

    elements = cmd.lsThroughFilter(allRenderElementsFilter)
    if elements is None:
        api.vray.importRenderElements(renderElememtsFile)
    else:
        rbwUI.RBWError(title='ATTENTION', text='Some render element named LOOKDEV are already in scene.\nPlease clean and try again.')


################################################################################
# @brief      delete the lookdev render elements.
#
def deleteRenderElements():
    renderElementsFilter = cmd.itemFilter(byType='VRayRenderElement')
    renderElementsSetFilter = cmd.itemFilter(byType='VRayRenderElementSet')
    allRenderElementsFilter = cmd.itemFilter(union=(renderElementsFilter, renderElementsSetFilter))
    elements = cmd.lsThroughFilter(allRenderElementsFilter)

    if elements is not None:
        for element in elements:
            if element.startswith('LOOKDEV_'):
                cmd.delete(element)


############################################################################
# @brief      order cause for camera file list.
#
# @param      input  The input
#
# @return     the version number
#
def myOrder(input):
    return int(input.split('_')[-1].split('vr')[1].split('.')[0])


################################################################################
# @brief      import the latest lookdev camera for the given asset.
#
# @param      assetParams  The asset parameters
#
#
def importLatestLookdevCamera(assetParams):
    cameraFolder = os.path.join(
        os.getenv('CONTENT'),
        'mc_{}'.format(os.getenv('PROJECT')),
        'scenes',
        assetParams['genre'],
        assetParams['group'],
        assetParams['name'],
        assetParams['variant'],
        'lookdev',
        'export',
        'camera'
    ).replace('\\', '/')

    if os.path.exists(cameraFolder):
        files = [f for f in os.listdir(cameraFolder) if f.endswith('.ma')]
        if len(files) > 0:
            files.sort(key=myOrder)
            lastCameraVersion = os.path.join(cameraFolder, files[-1]).replace('\\', '/')

            topGroups = api.scene.getTopGroups()
            cmd.file(lastCameraVersion, i=True, type="mayaAscii", ignoreVersion=True, mergeNamespacesOnClash=False, pr=True)
            newTopGroups = api.scene.getTopGroups()

            for node in newTopGroups:
                if node not in topGroups:
                    if not cmd.objExists('LIBRARY'):
                        cmd.group(empty=True, n='LIBRARY')
                    cmd.parent(node, 'LIBRARY')

            if cmd.objExists('viewtool_camera_lookdev_:surfacing_saveNode'):
                referenceNode = cmd.referenceQuery('viewtool_camera_lookdev_:surfacing_saveNode', referenceNode=True)
            else:
                referenceNode = cmd.referenceQuery('viewtool_camera_25_:surfacing_saveNode', referenceNode=True)
            referencePath = cmd.referenceQuery(referenceNode, filename=True, withoutCopyNumber=True, unresolvedName=True)
            api.log.logger().debug('referencePath: {}'.format(referencePath))
            if referencePath.startswith('${SPEED_SHARE}/'):
                newReferencePath = referencePath.replace('${SPEED_SHARE}/', '//speed.nas/')
                cmd.file(newReferencePath, loadReference=referenceNode, type='mayaBinary')
        else:
            return
    else:
        return


################################################################################
# @brief      import the latest lookdev extra for the given asset.
#
# @param      assetParams  The asset parameters
#
#
def importLatestLookdevExtra(assetParams):
    extraFolder = os.path.join(
        os.getenv('CONTENT'),
        'mc_{}'.format(os.getenv('PROJECT')),
        'scenes',
        assetParams['genre'],
        assetParams['group'],
        assetParams['name'],
        assetParams['variant'],
        'lookdev',
        'export',
        'extra'
    ).replace('\\', '/')

    if os.path.exists(extraFolder):
        files = [f for f in os.listdir(extraFolder) if f.endswith('.ma')]
        if len(files) > 0:
            files.sort(key=myOrder)
            lastExtraVersion = os.path.join(extraFolder, files[-1]).replace('\\', '/')

            cmd.file(lastExtraVersion, i=True, type="mayaAscii", ignoreVersion=True, mergeNamespacesOnClash=False, pr=True)
        else:
            return
    else:
        return


################################################################################
# @brief      import add component.
#
# @param      params  The parameters
#
def importAddComponent(params):
    lastAddComponent = getComponentPath(params)

    if lastAddComponent:
        addComponentNamespace = '{}_{}_{}_{}'.format(params['asset']['group'], params['asset']['name'], params['asset']['variant'], params['deptType'].upper())
        if isThisAsset(params['asset']):
            api.scene.addToImportGroup(lastAddComponent, addComponentNamespace)
        elif params['asset']['category'] != 'lightrig':
            api.scene.addToExtraGroup(lastAddComponent, addComponentNamespace)
        else:
            api.scene.addToLibraryGroup(lastAddComponent, addComponentNamespace)


################################################################################
# @brief      update add component.
#
# @param      params  The parameters
#
def updateAddComponent(params):
    lastAddComponent = getComponentPath(params)

    if lastAddComponent:
        addComponentNamespace = '{}_{}_{}_{}'.format(params['asset']['group'], params['asset']['name'], params['asset']['variant'], params['deptType'].upper())
        referenceList = cmd.ls('{}*'.format(addComponentNamespace), type='reference')

        if len(referenceList) > 0:
            referenceNode = referenceList[0]
            referencePath = cmd.referenceQuery(referenceNode, filename=True, withoutCopyNumber=True).replace('\\', '/')

            if lastAddComponent != referencePath:
                cmd.file(lastAddComponent, loadReference=referenceNode, type='mayaBinary')


################################################################################
# @brief      delete add component.
#
# @param      params  The parameters
#
def deleteAddComponent(params):
    addComponentNamespace = '{}_{}_{}_{}'.format(params['asset']['group'], params['asset']['name'], params['asset']['variant'], params['deptType'].upper())
    referenceList = cmd.ls('{}*'.format(addComponentNamespace), type='reference')

    if len(referenceList) > 0:

        # azzero le bs esisenti con questa reference
        if params['deptType'] == 'gbs':
            for bs in cmd.ls(type='blendShape'):
                if ':' not in bs:
                    inputConnections = cmd.listConnections('{}.inputTarget'.format(bs), source=True, destination=False)
                    for inputConnection in inputConnections:
                        if addComponentNamespace in inputConnection:
                            inputMesh = inputConnection.split(':')[1] if ':' in inputConnection else inputConnection
                            # restore bs value at 0
                            cmd.setAttr('{}.{}'.format(bs, inputMesh), 0)
                            # delete blendshape node
                            cmd.delete(bs)

        referenceNode = referenceList[0]
        if cmd.lockNode(referenceNode, query=True, lock=True):
            cmd.lockNode(referenceNode, lock=False)
        try:
            referencePath = cmd.referenceQuery(referenceNode, filename=True, withoutCopyNumber=True)
            cmd.file(referencePath, removeReference=True)
        except:
            cmd.delete(referenceNode)

        for set in ['IMPORT', 'LIBRARY']:
            if cmd.objExists(set):
                setElements = cmd.listRelatives(set)

                if not setElements:
                    cmd.delete(set)


################################################################################
# @brief      rename the shading engine connect with the given material.
#
# @param      material     The material
# @param      assetParams  The asset parameters
# @param      meshName     The mesh name
#
def renameShadingEngine(material, assetParams, meshName):
    shadingEngine = cmd.listConnections(material, source=False, type='shadingEngine')[0]
    shadingEngineList = shadingEngine.split('_')

    shadingEngineList[0] = os.getenv('PROJECT')
    if assetParams['variant'] != 'df' and assetParams['variant'] != 'default':
        shadingEngineList[1] = '{}{}'.format(assetParams['name'], assetParams['variant'].capitalize())
    else:
        shadingEngineList[1] = assetParams['name']

    if meshName != '':
        shadingEngineList[3] = meshName

    newShadingEgine = '_'.join(shadingEngineList)
    cmd.rename(shadingEngine, newShadingEgine)


################################################################################
# @brief      rename the give material.
#
# @param      material     The material
# @param      assetParams  The asset parameters
# @param      meshName     The mesh name
#
# @return     the new material name.
#
def renameMaterial(material, assetParams, meshName):
    materialTokens = material.split('_')

    materialTokens[0] = os.getenv('PROJECT')
    if assetParams['variant'] != 'df' and assetParams['variant'] != 'default':
        materialTokens[1] = '{}{}'.format(assetParams['name'], assetParams['variant'].capitalize())
    else:
        materialTokens[1] = assetParams['name']

    if meshName != '':
        materialTokens[3] = meshName

    newMaterialName = '_'.join(materialTokens)
    cmd.rename(material, newMaterialName)

    return newMaterialName


################################################################################
# @brief      rename all the subnode
#
# @param      material     The material
# @param      assetParams  The asset parameters
# @param      meshName     The mesh name
#
def renameMaterialSubnode(material, assetParams, meshName):
    allConnections = cmd.listConnections(material, destination=False)
    if allConnections:
        connections = set(allConnections)
        for connection in connections:
            if connection != material:
                if len(connection.split('_')) > 1:
                    connectionList = connection.split('_')
                    connectionList[0] = os.getenv('PROJECT')
                    if assetParams['variant'] != 'df' and assetParams['variant'] != 'default':
                        connectionList[1] = '{}{}'.format(assetParams['name'], assetParams['variant'].capitalize())
                    else:
                        connectionList[1] = assetParams['name']
                    if meshName != '':
                        connectionList[3] = meshName
                    if cmd.objExists(connection):
                        cmd.rename(connection, '_'.join(connectionList))
                    renameMaterialSubnode('_'.join(connectionList), assetParams, meshName)


################################################################################
# @brief      Gets the material texture nodes.
#
# @param      material  The material
#
# @return     The material texture nodes.
#
def getMaterialTextureNodes(material):
    textures = []
    connections = cmd.listConnections(material, destination=False)
    if connections:
        for connection in connections:
            if cmd.nodeType(connection) == 'file':
                textures.append(connection)
            else:
                pass
            subTextures = getMaterialTextureNodes(connection)
            textures = textures + subTextures
    return textures


################################################################################
# @brief      copy the shader texture in content.
#
# @param      assetParams  The asset parameters
#
def copyTextureInContent(assetParams, material=None):
    if not material:
        material = cmd.ls(selection=True)[0]
        dialog = True
        meshName = rbwUI.RBWInput(title='Insert Mesh Name', text='Please Insert a meshName for the shader if you want', mode={'Mesh Name': 'string'}, resizable=True).result()['Mesh Name']
    else:
        meshName = ''
        dialog = False

    materialTextures = getMaterialTextureNodes(material)
    api.log.logger().debug(materialTextures)
    copyDict = {}

    copyMessage = 'I will copy these textures:\n'

    mcFolder = os.getenv('MC_FOLDER').replace('\\', '/')

    for texture in materialTextures:
        texturePath = cmd.getAttr('{}.fileTextureName'.format(texture))
        if mcFolder not in texturePath or not dialog:
            newTextureFolder = os.path.join(
                mcFolder,
                'sourceimages',
                assetParams['category'],
                assetParams['group'],
                assetParams['name'],
                assetParams['variant'],
                'tiff'
            ).replace('\\', '/')

            textureFileName = os.path.basename(texturePath)
            textureInfoList = textureFileName.split('_')
            textureInfoList[0] = os.getenv('PROJECT')
            if assetParams['variant'] != 'df' and assetParams['variant'] != 'default':
                textureInfoList[1] = '{}{}{}'.format(assetParams['name'], assetParams['variant'][0].upper(), assetParams['variant'][1:])
            else:
                textureInfoList[1] = assetParams['name']

            if meshName != '':
                textureInfoList[3] = meshName

            newTexturePath = os.path.join(newTextureFolder, '_'.join(textureInfoList)).replace('\\', '/')

            copyMessage = '{}=============================================================\nfrom: {}\n\nto: {}\n'.format(copyMessage, texturePath, newTexturePath)

            copyDict[texture] = {'oldPath': texturePath, 'newPath': newTexturePath}
            api.log.logger().debug(copyDict)

    if dialog:
        answer = rbwUI.RBWConfirm(title='Copy Settings', text=copyMessage, resizable=True).result()
    else:
        answer = True

    if answer:
        for texture in copyDict:
            if not os.path.exists(copyDict[texture]['newPath']):
                if not os.path.exists(copyDict[texture]['newPath']):
                    if not os.path.exists(os.path.dirname(copyDict[texture]['newPath'])):
                        os.makedirs(os.path.dirname(copyDict[texture]['newPath']))
                    copyTexture(copyDict[texture]['oldPath'], copyDict[texture]['newPath'])
            cmd.setAttr('{}.fileTextureName'.format(texture), copyDict[texture]['newPath'], type='string')

        renameShadingEngine(material, assetParams, meshName)
        newMaterialName = renameMaterial(material, assetParams, meshName)
        renameMaterialSubnode(newMaterialName, assetParams, meshName)


################################################################################
# @brief      copy the source path into the dest path.
#
# @param      source  The source
# @param      dest    The destination
#
def copyTexture(source, dest):
    sourceInfoList = source.split('.')
    if len(sourceInfoList) == 4:
        padding = sourceInfoList[2]
        paddingLength = len(padding)
        textureBaseNameList = os.path.basename(source).split('.')
        textureBaseNameList[1] = r'\d{{{}}}'.format(paddingLength)
        testString = r'\.'.join(textureBaseNameList)

        for file in os.listdir(os.path.dirname(source)):
            if re.match(testString, file):
                filePadding = file.split('.')[1]
                sourceFile = source.replace('.{}.'.format(padding), '.{}.'.format(filePadding))
                destFile = dest.replace('.{}.'.format(padding), '.{}.'.format(filePadding))

                shutil.copyfile(sourceFile, destFile)
    else:
        shutil.copyfile(source, dest)


################################################################################
# @brief      change the reference path to go in farm.
#
def changeReferencePath():
    componentsSaveNode = [
        'viewtool_camera_lookdev_:surfacing_saveNode',
        'generic_shader_default_:surfacing_saveNode',
        'viewtool_geos_lookdev_:surfacing_saveNode',
        'viewtool_sphereball_lookdev_:surfacing_saveNode',
        '{}_shader_default_:surfacing_saveNode'.format(os.getenv('PROJECT'))
    ]

    if getProjectFps() == 25:
        componentsSaveNode[0] = 'viewtool_camera_25_:surfacing_saveNode'

    for saveNode in componentsSaveNode:
        if cmd.objExists(saveNode):
            if cmd.listConnections(saveNode) is not None:
                if cmd.getAttr('{}.approvation'.format(saveNode)) == 'def':
                    if cmd.referenceQuery(saveNode, isNodeReferenced=True):
                        referenceNode = cmd.referenceQuery(saveNode, referenceNode=True)
                        referencePath = cmd.referenceQuery(referenceNode, filename=True, withoutCopyNumber=True)
                        if referencePath.startswith('//speed.nas/'):
                            newReferencePath = referencePath.replace('//speed.nas/', '${SPEED_SHARE}/')
                            cmd.file(newReferencePath, loadReference=referenceNode, type='mayaBinary')


################################################################################
# @brief      Gets the lightrig.
#
# @return     The lightrig.
#
def getLightrig():
    surfacingSaveNode = '*:surfacing_saveNode'
    lightrig = None
    for node in cmd.ls(surfacingSaveNode):
        if cmd.getAttr('{}.genre'.format(node)) == 'lightrig':
            lightrig = cmd.listConnections(node)[0]
            api.log.logger().debug('lightrig: {}'.format(lightrig))

    return lightrig


################################################################################
# @brief      Gets the camera.
#
# @return     The camera.
#
def getCamera():
    surfacingSaveNode = '*:surfacing_saveNode'
    camera = None
    for node in cmd.ls(surfacingSaveNode):
        if cmd.getAttr('{}.genre'.format(node)) == 'library' and cmd.getAttr('{}.name'.format(node)) == 'camera':
            camera = cmd.listConnections(node)[0]
            api.log.logger().debug('camera: {}'.format(camera))

    return camera


################################################################################
# @brief      Gets the render top groups.
#
# @param      savedAsset  The saved asset
#
# @return     The render top groups.
#
def getRenderTopGroups(savedAsset):
    returnString = ''

    wipSaveNode = '{}_saveNode'.format(savedAsset.dept)
    topGroups = []
    if cmd.objExists(wipSaveNode):
        topGroup = cmd.listConnections(wipSaveNode)[0]
        topGroups.append(topGroup)
        if savedAsset.dept == 'grooming':
            surfacingSaveNode = '{}_{}_{}_RND:surfacing_saveNode'.format(savedAsset.getGroup(), savedAsset.getName(), savedAsset.getVariant())
            if cmd.objExists(surfacingSaveNode):
                rndTopGroup = cmd.listConnections(surfacingSaveNode)[0]
                topGroups.append(rndTopGroup)
            else:
                modelingSaveNode = '{}_{}_{}_HIG:modeling_saveNode'.format(savedAsset.getGroup(), savedAsset.getName(), savedAsset.getVariant())
                if cmd.objExists(modelingSaveNode):
                    higTopGroup = cmd.listConnections(modelingSaveNode)[0]
                    topGroups.append(higTopGroup)
        elif savedAsset.dept == 'surfacing' and savedAsset.getCategory() == 'set':
            if cmd.objExists('IMPORT'):
                importedSurfs = cmd.listRelatives('IMPORT')
                for importedSurf in importedSurfs:
                    topGroups.append(importedSurf)

    if len(topGroups) > 0:
        for group in topGroups:
            returnString = '{}{}\n'.format(returnString, group)

    if cmd.objExists('EXTRA'):
        returnString = '{}EXTRA\n'.format(returnString)

    return returnString


################################################################################
# @brief      Gets the filename.
#
# @param      layerName   The layer name
# @param      savedAsset  The saved asset
#
# @return     The filename.
#
def getFilename(layerName, savedAsset):
    existingQuery = "SELECT * FROM `renderLayer` WHERE `type`='asset' AND `linkId`={} AND `name`='{}'".format(savedAsset.getAssetId(), layerName)
    results = api.database.selectSingleQuery(existingQuery)

    # get render layer
    if results:
        layerId = results[0]
    else:
        fields = 'name, type, linkId, enabled'
        args = (layerName, 'asset', savedAsset.getAssetId(), 1)
        pers = ", ".join(["\'{}\'".format(o) for o in args])

        insertQuery = 'INSERT INTO `renderLayer` (%s) VALUES (%s) ' % (fields, pers)

        api.log.logger().debug(insertQuery)

        layerId = api.database.insertQuery(insertQuery)

    # get render layer version
    versionQuery = "SELECT sre.version FROM `savedRenderLayer` AS sre JOIN `savedAsset_wip` AS sa ON sre.linkId=sa.id WHERE sa.dept='{}' AND `renderLayerId`={} ORDER BY sre.date DESC".format(savedAsset.dept, layerId)

    api.log.logger().debug(versionQuery)

    versionResult = api.database.selectQuery(versionQuery)

    if versionResult:
        version = int(versionResult[0][0].split('v')[1])
        lastVersion = 'v{}'.format(str(version + 1).zfill(3))
    else:
        lastVersion = 'v001'

    dateVar = datetime.fromtimestamp(time.time())
    date = dateVar.strftime("%Y.%m.%d-%H.%M.%S")

    fields = 'renderLayerId, linkId, version, approvation, date'
    args = (layerId, savedAsset.db_id, lastVersion, 0, date)
    pers = ", ".join(["\'{}\'".format(o) for o in args])
    insertQuery = 'INSERT INTO `savedRenderLayer` (%s) VALUES (%s) ' % (fields, pers)

    api.log.logger().debug(insertQuery)

    api.database.insertQuery(insertQuery)

    return os.path.join(
        os.getenv('MC_FOLDER'),
        'scenes',
        savedAsset.getAsset().getAssetFolder(),
        'lookdev',
        savedAsset.dept,
        'RENDER',
        layerName,
        lastVersion,
        '<Layer>.exr').replace('\\', '/')


################################################################################
# @brief      Saves a lookdev scene.
#
# @param      sceneParams  The scene parameters
#
# @return     lookdev file name.
#
def saveLookdevScene(sceneParams):
    if len(api.xgen.listCollection()) > 0:
        useXgen = True
    else:
        useXgen = False

    savedAsset = sceneParams['savedAsset']

    pipeMayaProject = os.path.join(os.getenv('MC_FOLDER')).replace('\\', '/')

    saveFolder = os.path.join(
        pipeMayaProject,
        'scenes',
        savedAsset.getAsset().getAssetFolder(),
        'lookdev',
        savedAsset.dept
    ).replace('\\', '/')

    if not os.path.exists(saveFolder):
        version = 'vr001'
    else:
        versions = []
        for file in os.listdir(saveFolder):
            if file.startswith('vr'):
                versions.append(file)

        if len(versions) > 0:
            versions.sort()
            lastVersion = int(versions[-1].split('vr')[1])
            version = 'vr{}'.format(str(lastVersion + 1).zfill(3))
        else:
            version = 'vr001'

    saveFolder = os.path.join(saveFolder, version).replace('\\', '/')
    os.makedirs(saveFolder)

    newFileName = 'lookdev_{}_{}_{}_{}_{}'.format(savedAsset.getGroup(), savedAsset.getName(), savedAsset.getVariant(), savedAsset.deptType, version)

    cmd.workspace(pipeMayaProject, openWorkspace=True)

    if useXgen:
        # set xgen project
        api.xgen.setProjectPath(str(savedAsset.savedAssetFolder))

        # set camera culling
        if not cmd.objExists('NO_CULL'):
            api.xgen.enableCullingForEveryDescription()
        else:
            xg_palette = api.xgen.listCollection()[0]
            xgen_descriptions = sorted(api.xgen.listDescription(xg_palette))

            for description in xgen_descriptions:
                if not cmd.sets(description, im='NO_CULL'):
                    api.xgen.enableCulling(str(xg_palette), str(description))

    newFullFileName = os.path.join(saveFolder, newFileName).replace('\\', '/')

    vray.vray_maya_utils.vraySetVFBDisplayCorrection('scene-linear Rec 709/sRGB', 'legacy', 'sRGB gamma')

    try:
        cmd.file(rename='{}.mb'.format(newFullFileName))
        cmd.file(save=True, type='mayaBinary')
    except:
        rbwUI.RBWError(title='Save Problems', text='Problem in save .mb\nPlease contact the pipeline department')
        return None

    try:
        cmd.file(rename='{}.ma'.format(newFullFileName))
        cmd.file(save=True, type='mayaAscii')
    except:
        rbwUI.RBWError(title='Save Problems', text='Problem in save .ma\nPlease contact the pipeline department')
        return None

    # export camera
    exportCamera(saveFolder, savedAsset)

    # export extra
    exportExtra(saveFolder, savedAsset)

    # grooing
    if useXgen:
        finalizeGromming(newFullFileName)

    # first time seen from SG
    assetSGid = savedAsset.getShotgunId()
    shotgridObj = api.shotgrid.ShotgridObj()
    firstTimeSeen = shotgridObj.assetInfoByAsset(assetSGid)['sg_first_time_seen']['name']
    data = {'firstTimeSeen': firstTimeSeen}

    jsonFile = os.path.join('/'.join(saveFolder.split('/')[:-2]), 'assetInfo.json').replace('\\', '/')
    if os.path.exists(jsonFile):
        existingData = api.json.json_read(jsonFile)
        try:
            if data['firstTimeSeen'] != existingData['firstTimeSeen']:
                api.json.json_write(data, jsonFile)
        except KeyError:
            api.json.json_write(data, jsonFile)
    else:
        api.json.json_write(data, jsonFile)

    return '{}.ma'.format(newFullFileName)


################################################################################
# @brief      { function_description }
#
# @param      saveFolder  The save folder
# @param      savedAsset  The saved asset
#
def exportCamera(saveFolder, savedAsset):
    cameraSaveFolder = os.path.join(
        '/'.join(saveFolder.split('/')[:-2]),
        'export',
        'camera'
    ).replace('\\', '/')

    if not os.path.exists(cameraSaveFolder):
        os.makedirs(cameraSaveFolder)
        lastCameraVersion = 0
    else:
        cameras = []
        for file in os.listdir(cameraSaveFolder):
            if file.endswith('.ma'):
                cameras.append(file)
        if len(cameras) > 0:
            cameras.sort(key=myOrder)
            lastCameraVersionString = cameras[-1].split('_')[-1]
            lastCameraVersion = int(lastCameraVersionString.split('r')[1].split('.')[0])
        else:
            lastCameraVersion = 0

    cameraFullFileName = os.path.join(
        cameraSaveFolder,
        'camera_{}_vr{}.ma'.format(savedAsset.dept, str(lastCameraVersion + 1).zfill(3))
    ).replace('\\', '/')

    if cmd.objExists('viewtool_camera_lookdev_:surfacing_saveNode'):
        cameraTopGroup = cmd.listConnections('viewtool_camera_lookdev_:surfacing_saveNode')[0]
    else:
        cameraTopGroup = cmd.listConnections('viewtool_camera_25_:surfacing_saveNode')[0]
    cmd.parent(cameraTopGroup, world=True)
    cmd.select(cameraTopGroup, replace=True)
    cmd.file(cameraFullFileName, force=True, options='v=0;', type='mayaAscii', preserveReferences=True, exportSelected=True, constraints=True)
    cmd.parent(cameraTopGroup, 'LIBRARY')


################################################################################
# @brief      { function_description }
#
# @param      saveFolder  The save folder
# @param      savedAsset  The saved asset
#
def exportExtra(saveFolder, savedAsset):
    if cmd.objExists('EXTRA'):
        extraSaveFolder = os.path.join(
            '/'.join(saveFolder.split('/')[:-2]),
            'export',
            'extra'
        ).replace('\\', '/')

        if not os.path.exists(extraSaveFolder):
            os.makedirs(extraSaveFolder)
            lastExtraVersion = 0
        else:
            extras = []
            for file in os.listdir(extraSaveFolder):
                if file.endswith('.ma'):
                    extras.append(file)
            if len(extras) > 0:
                extras.sort(key=myOrder)
                lastExtraVersionString = extras[-1].split('_')[-1]
                lastExtraVersion = int(lastExtraVersionString.split('r')[1].split('.')[0])
            else:
                lastExtraVersion = 0

        extraFullFileName = os.path.join(
            extraSaveFolder,
            'extra_{}_vr{}.ma'.format(savedAsset.dept, str(lastExtraVersion + 1).zfill(3))
        ).replace('\\', '/')

        cmd.select('EXTRA', replace=True)
        cmd.file(extraFullFileName, force=True, options='v=0;', type='mayaAscii', preserveReferences=True, exportSelected=True, constraints=True)
        cmd.select(clear=True)


################################################################################
# @brief      finalize grooming file.
#
# @param      newFullFileName  The new full file name
#
def finalizeGromming(newFullFileName):
    # qui inserire procedure che nasconde le gpu cache e spegne altri valori di viewport
    modelPanels = cmd.getPanel(type="modelPanel")
    visiblePanels = cmd.getPanel(visiblePanels=True)
    modelPanel = list(set(modelPanels) & set(visiblePanels))[0]
    # spengo gpu cache
    cmd.modelEditor(modelPanel, edit=True, pluginObjects=['gpuCacheDisplayFilter', False])
    # spengo le shadow
    cmd.modelEditor(modelPanel, edit=True, shadows=False)
    # bounding box view
    cmd.modelEditor(modelPanel, edit=True, displayAppearance='boundingBox')
    # spengo motion blur
    cmd.setAttr('hardwareRenderingGlobals.motionBlurEnable', 0)
    # spengo AO
    cmd.setAttr('hardwareRenderingGlobals.ssaoEnable', 0)
    # spengo anti-aliasing
    cmd.setAttr('hardwareRenderingGlobals.multiSampleEnable', 0)

    # list scalps and export .abc
    scalps = api.xgen.getScalps()

    currentCollection = api.xgen.currentCollection()
    # delete xgen descrption for crash
    cmd.delete(currentCollection)

    # export alembic
    alembicFullFileName = '{}__{}.abc'.format(newFullFileName, currentCollection)

    rootsArgs = ["-root {}".format(scalp) for scalp in scalps]
    rootsArgs = " ".join(rootsArgs)

    command = '''
                -frameRange 1.0 390.0 -uvwrite -step 0.2 -attrPrefix xgen -worldSpace {} -stripNamespaces -file '{}'"
                '''.format(rootsArgs, alembicFullFileName)
    cmd.AbcExport(j=command)

    # cambiare il path dentro il .xgen con la variabile ${SPEED_SHARE}
    xgenFullFileName = alembicFullFileName.replace('.abc', '.xgen')

    with open(xgenFullFileName, 'r') as xgenFile:
        oldXgenData = xgenFile.read()

    newXgenData = oldXgenData.replace('//speed.nas/', '${SPEED_SHARE}/')

    with open(xgenFullFileName, 'w') as xgenFile:
        xgenFile.write(newXgenData)


################################################################################
# @brief      check the scene for lookdev kickOff.
#
# @param      savedAssetParams  The saved asset
#
# @return     if scene is ready to be submitt.
#
def checkScene(savedAsset):
    # check render elements
    masterLayer = api.renderlayer.getDefaultRenderLayer()
    api.renderlayer.switchByName(masterLayer)
    renderElementsFilter = cmd.itemFilter(byType='VRayRenderElement')
    renderElementsSetFilter = cmd.itemFilter(byType='VRayRenderElementSet')
    allRenderElementsFilter = cmd.itemFilter(union=(renderElementsFilter, renderElementsSetFilter))
    elements = cmd.lsThroughFilter(allRenderElementsFilter)

    if elements:
        for element in elements:
            if not element.startswith('LOOKDEV_'):
                rbwUI.RBWError(title='ATTENTION', text='One or more render elements are not the official ones.\nPlease control and try again.')
                return False
    else:
        rbwUI.RBWError(title='ATTENTION', text='No render elements in scene.\nPlease import and try again.')
        return False

    # check render layer
    renderLayers = api.renderlayer.list()
    if renderLayers:
        rbwUI.RBWError(title='ATTENTION', text='There are some render layer in scene.\nPlease delete and retry.')
        return False

    # check saveNode integrity
    wipSaveNode = '{}_saveNode'.format(savedAsset.dept)
    exists = False
    for saveNode in cmd.ls(wipSaveNode):
        saveNodeCategory = cmd.getAttr('{}.genre'.format(saveNode))
        saveNodeGroup = cmd.getAttr('{}.group'.format(saveNode))
        saveNodeName = cmd.getAttr('{}.name'.format(saveNode))
        saveNodeVariant = cmd.getAttr('{}.variant'.format(saveNode))
        saveNodeApprovation = cmd.getAttr('{}.approvation'.format(saveNode))

        if saveNodeCategory == savedAsset.getCategory() and saveNodeGroup == savedAsset.getGroup() and saveNodeName == savedAsset.getName() and saveNodeVariant == savedAsset.getVariant() and saveNodeApprovation == savedAsset.approvation:
            exists = True

    if exists:
        try:
            topGroup = cmd.listConnections(wipSaveNode)[0]
            api.log.logger().debug('topGroup: {}'.format(topGroup))
        except:
            rbwUI.RBWError(title='ATTENTION', text='The save node has no connection with the top group.\nPlease make a wip save and then try again.')
            return False
    else:
        rbwUI.RBWError(title='ATTENTION', text='The wip save node not exists.\nPlease make a wip save and then try again.')
        return False

    # check outliner
    if cmd.objExists('|LIBRARY'):
        topGroups = api.scene.getTopGroups()
        topGroups.remove(topGroup)

        if len(topGroups) > 0:
            rbwUI.RBWError(title='ATTENTION', text='There are more than one top gropus in scene.\nPlease clear the scene and then try again.')
            return False
    else:
        rbwUI.RBWError(title='ATTENTION', text='No group named LIBRARY.\nControl the scene and try again.')
        return False

    # check camera namespace
    if getProjectFps() == 25:
        cameraSaveNode = 'viewtool_camera_25_:surfacing_saveNode'
    else:
        cameraSaveNode = 'viewtool_camera_lookdev_:surfacing_saveNode'

    if not cmd.objExists(cameraSaveNode):
        rbwUI.RBWError(title='ATTENTION', text='The camera reference has a wrong namespace.\nControl and try again.')
        return False

    if not cmd.referenceQuery(cameraSaveNode, isNodeReferenced=True):
        rbwUI.RBWError(title='ATTENTION', text='Sorry but the camera is imported and not referenced.\nFix the scene and try again.')
        return False

    # check lightrig
    lightrig = getLightrig()
    if lightrig:
        if not cmd.referenceQuery(lightrig, isNodeReferenced=True):
            rbwUI.RBWError(title='ATTENTION', text='Sorry but the lightrig is imported and not referenced.\nFix the scene and try again.')
            return False

    # check reference path
    references = cmd.ls(type='reference')
    for reference in references:
        try:
            referencePath = cmd.referenceQuery(reference, filename=True, withoutCopyNumber=True, unresolvedName=True)
            if referencePath.startswith('${SPEED_SHARE}'):
                rbwUI.RBWError(title='ATTENTION', text='One or more references has a bad path, it starts with ${SPEED_SHARE}.\nReplace with //speed.nas and try again.')
                return False
        except RuntimeError:
            pass

    # check vrscene in assembly for surfacing set dressing tables
    if savedAsset.dept == 'surfacing' and savedAsset.getCategory() == 'set':
        missingSurfSave = []
        for assemblyNode in cmd.ls(type='assemblyReference'):
            assembly = api.assembly.Assembly(node=assemblyNode)
            if 'vrscene' not in assembly.getRepresentationList():
                missingSurfSave.append(assemblyNode)

        if len(missingSurfSave) > 0:
            proced = rbwUI.RBWConfirm(title='ATTENTION', text='One or more assemblies in scene have not a surfacing representation inside.\nThose items will be not rendered by our farm.\nDo you still want to go on?', defOk=['Yes, go on', 'ok.png'], resizable=True).result()
            if not proced:
                return False
            else:
                return True

    # check first time sceene
    try:
        shotgridObj = api.shotgrid.ShotgridObj()
        shotgridObj.assetInfoByAsset(savedAsset.getShotgunId())['sg_first_time_seen']['name']
    except TypeError:
        rbwUI.RBWError(text='The asset has no \'First Time Seen \' field set on SG.\nPlease contact production and try again.')
        return False

    return True


################################################################################
# @brief      Builds a lookdev scene.
#
# @param      sceneParams  The scene parameters
#
def buildLookdevScene(sceneParams):
    if sceneParams['savedAsset'].dept == 'grooming':

        sceneParams['savedAsset'].load()

    pipeLightrig = False

    if getLightrig() in ['{}_shader_default_:CTRL_set'.format(os.getenv('PROJECT')), 'generic_shader_default_:CTRL_set']:
        pipeLightrig = True
        tods = {
            0: 'studio',
            1: 'midday',
            2: 'night',
            3: 'morning',
            4: 'reflection',
            5: 'hybrid'
        }

    else:
        tods = {0: 'default', 1: 'snap'}

    if sceneParams['savedAsset'].asset.category == 'set':
        qualities = {
            'Low': {
                'dmcThreshold': 0.06,
                'dmcMaxSubdivs': 4,
                'dmcMinSubdivs': 1
            },

            'Medium': {
                'dmcThreshold': 0.03,
                'dmcMaxSubdivs': 4,
                'dmcMinSubdivs': 1
            }
        }

    else:
        qualities = {
            'Low': {
                'dmcThreshold': 0.05,
                'dmcMaxSubdivs': 4,
                'dmcMinSubdivs': 2
            },

            'Medium': {
                'dmcThreshold': 0.02,
                'dmcMaxSubdivs': 16,
                'dmcMinSubdivs': 4
            }
        }

    if sceneParams['savedAsset'].dept == 'surfacing' and sceneParams['savedAsset'].asset.category == 'set':
        cmd.select(clear=True)
        common.switchAssembly.switchTo(representation='vrscene')

    changeReferencePath()

    renderGroups = getRenderTopGroups(sceneParams['savedAsset'])

    snap = False

    for layerDict in sceneParams['layers']:
        todIndex = layerDict['todIndex']
        camera = layerDict['camera']
        qualityValue = layerDict['quality']

        if not pipeLightrig and todIndex == 1:
            snap = True
        else:
            cmd.setAttr("defaultRenderGlobals.currentRenderer", "vray", type="string")
            cmd.setAttr("vraySettings.imageFormatStr", "exr (multichannel)", type="string")

            api.renderlayer.createFromTemplate('LOOKDEV', '{}_{}'.format(tods[todIndex], camera), todIndex, renderGroups)
            renderLayerName = 'LOOKDEV_{}_{}_RL'.format(tods[todIndex], camera)
            layerDict['name'] = renderLayerName
            if getLightrig() not in ['{}_shader_default_:CTRL_set'.format(os.getenv('PROJECT')), 'generic_shader_default_:CTRL_set']:
                api.renderlayer.switch(renderLayerName)

                todOverrideCollection = api.renderlayer.getRenderLayerCollectionByName(renderLayerName, '_tod_override')
                collection.delete(todOverrideCollection)

                lightinvisibleOverrideCollection = api.renderlayer.getRenderLayerCollectionByName(renderLayerName, '_lightinvisible_override')
                collection.delete(lightinvisibleOverrideCollection)

                lightCollection = api.renderlayer.getRenderLayerCollectionByName(renderLayerName, '_light_col')
                lightCollection.getSelector().setPattern('')
                lightCollection.getSelector().setStaticSelection('|LIBRARY|{}'.format(getLightrig()))
            else:
                api.renderlayer.switch(renderLayerName)

                todOverrideCollection = api.renderlayer.getRenderLayerCollectionByName(renderLayerName, '_tod_override')
                todOverrideCollection.getSelector().setStaticSelection('{}:hdr_VRayMultiSubTex'.format(getLightrig().split(':')[0]))

            if qualityValue != 'Custom':
                overrides = qualities[qualityValue]
                for override in overrides:
                    api.renderlayer.createRenderSettingsOverride(renderLayerName, override, overrides[override])

            if getProjectFps() == 25:
                cameraFullName = 'viewtool_camera_25_:CAMERA{}Shape'.format(layerDict['camera'])
            else:
                cameraFullName = 'viewtool_camera_lookdev_:CAMERA{}Shape'.format(layerDict['camera'])

            cmd.lookThru(cameraFullName)
            cmd.refresh(force=True)

    newSceneName = saveLookdevScene(sceneParams)

    sceneParams['sceneName'] = newSceneName

    if snap:
        makeBlast(sceneParams)
    else:
        sendLayersToFarm(sceneParams)
        pass


################################################################################
# @brief      Makes a blast.
#
# @param      sceneParams  The scene parameters
#
def makeBlast(sceneParams):
    cmd.setAttr("defaultRenderGlobals.imageFormat", 8)
    format = "image"
    compression = "jpg"
    savedAsset = sceneParams['savedAsset']

    for layerDict in sceneParams['layers']:
        layerName = 'LOOKDEV_snap_{}_RL'.format(layerDict['camera'])

        imagePath = getFilename(layerName, savedAsset).replace('<Layer>.exr', layerName).replace('\\', '/')

        width = 2048
        height = 2048

        frameRange = layerDict['frames']
        ranges = []

        if ';' in frameRange:
            frameRanges = frameRange.split(';')
            for range in frameRanges:
                if '-' in range:
                    ranges.append([range.split('-')[0], range.split('-')[1]])
                else:
                    ranges.append([range, range])
        else:
            if '-' in frameRange:
                ranges.append([frameRange.split('-')[0], frameRange.split('-')[1]])
            else:
                ranges.append([frameRange, frameRange])

        if getProjectFps() == 25:
            camera = 'viewtool_camera_25_:CAMERA{}Shape'.format(layerDict['camera'])
        else:
            camera = 'viewtool_camera_lookdev_:CAMERA{}Shape'.format(layerDict['camera'])
        cmd.setAttr('{}.displayResolution'.format(camera), 0)
        cmd.lookThru(camera)

        focalLength = int(cmd.getAttr('{}.focalLength'.format(camera)))
        focalLengthData = {'focalLength': focalLength}
        layerInfoJson = os.path.join(os.path.dirname(imagePath), 'layerInfo.json').replace('\\', '/')
        api.json.json_write(focalLengthData, layerInfoJson)

        ###########################################
        # per prevenire scazzi della viewport
        currentFrame = cmd.currentTime(query=True)
        cmd.currentTime(1.0)
        cmd.currentTime(currentFrame)
        cmd.select(clear=True)
        ###########################################

        for range in ranges:

            start = range[0]
            end = range[1]

            cmd.playblast(
                filename=imagePath,
                offScreen=True,
                startTime=start,
                endTime=end,
                viewer=False,
                compression=compression,
                format=format,
                percent=100,
                showOrnaments=True,
                framePadding=4,
                widthHeight=[width, height],
                forceOverwrite=True
            )

        kickOffVrsceneFolder = '/'.join(imagePath.split('/')[:-1]).replace('RENDER', 'VRSCENE')
        layerJsonDetails = os.path.join(kickOffVrsceneFolder, 'settings.json')

        api.json.json_write(layerDict, layerJsonDetails)


################################################################################
# @brief      Sends a layers to farm.
#
# @param      sceneParams  The scene parameters
#
def sendLayersToFarm(sceneParams):
    savedAsset = sceneParams['savedAsset']
    sceneFileName = sceneParams['sceneName']
    frame = int(cmd.currentTime(query=True))
    for layerDict in sceneParams['layers']:
        fileName = getFilename(layerDict['name'], savedAsset)
        newImagePath = fileName.replace('<Layer>', layerDict['name'])
        version = newImagePath.split('/')[-2]

        kickOffVrsceneFilename = fileName.replace('RENDER', 'VRSCENE').replace('.exr', '.vrscene')
        vrsceneFileName = kickOffVrsceneFilename.replace('<Layer>.vrscene', '{}_0001.vrscene'.format(layerDict['name']))

        if layerDict['resolution'] == '4K':
            resolutionString = '4096x4096'
        elif layerDict['resolution'] == '2K':
            resolutionString = '2048x2048'
        elif layerDict['resolution'] == '1K':
            resolutionString = '1024x1024'

        kickOffJobSettings = {
            'jobName': '{}_lookdev_{}_{}_{}_{}_{}_{}'.format(
                os.getenv('PROJECT').upper(),
                savedAsset.getCategory(),
                savedAsset.getGroup(),
                savedAsset.getName(),
                savedAsset.getVariant(),
                layerDict['name'],
                version
            ),
            'batchName': '{}_lookdev_{}_{}_{}_{}_KICKOFF'.format(
                os.getenv('PROJECT').upper(),
                savedAsset.getCategory(),
                savedAsset.getGroup(),
                savedAsset.getName(),
                savedAsset.getVariant()
            ),
            'inputFileName': sceneFileName,
            'outputFileName': kickOffVrsceneFilename,
            'goal': 'lookdev vrscene caching',
            'layer': layerDict['name'],
            'dept': savedAsset.dept,
            'current_frame': frame,
            'frames': layerDict['frames'],
            'camera': 'viewtool_camera_lookdev_:CAMERA{}Shape'.format(layerDict['camera']),
            'enableAnimatedGIJobs': False,
            'enableGIJobs': False,
            'resolution_value': resolutionString,
            'renderDeep': False,
            'renderBeauty': True,
            'notes': 'From lookdev kickoff',
            'mode': 'fml',
            'isLightJob': False,
            'isHighJob': False,
            'extraAttr': '',
            'extraTime': False,
            'priority': None,
            'chunk': False,
            'suspended': False,
            'overscan': False,
            'vrscenes': False,
            'obj': savedAsset.getAsset()
        }

        if getProjectFps() == 25:
            kickOffJobSettings['camera'] = 'viewtool_camera_25_:CAMERA{}Shape'.format(layerDict['camera'])

        kickOffJob = api.farm.createAndSubmitJob(kickOffJobSettings)

        jobSettings = {
            'jobName': '{}_lookdev_{}_{}_{}_{}_{}_{}'.format(
                os.getenv('PROJECT').upper(),
                savedAsset.getCategory(),
                savedAsset.getGroup(),
                savedAsset.getName(),
                savedAsset.getVariant(),
                layerDict['name'],
                version
            ),
            'batchName': '{}_lookdev_{}_{}_{}_{}_RENDERING'.format(
                os.getenv('PROJECT').upper(),
                savedAsset.getCategory(),
                savedAsset.getGroup(),
                savedAsset.getName(),
                savedAsset.getVariant()
            ),
            'inputFileName': vrsceneFileName,
            'outputFileName': newImagePath,
            'goal': 'rendering',
            'layer': layerDict['name'],
            'dept': savedAsset.dept,
            'dependency': [kickOffJob],
            'frames': layerDict['frames'],
            'current_frame': frame,
            'enableAnimatedGIJobs': False,
            'enableGIJobs': False,
            'resolution_value': resolutionString,
            'renderDeep': False,
            'renderBeauty': True,
            'notes': 'From lookdev kickoff',
            'mode': 'fml',
            'isLightJob': False,
            'isHighJob': False,
            'extraAttr': '',
            'extraTime': False,
            'priority': None,
            'chunk': False,
            'suspended': False,
            'overscan': False,
            'obj': savedAsset.getAsset()
        }

        api.farm.createAndSubmitJob(jobSettings)

        focalLength = int(cmd.getAttr('{}.focalLength'.format(kickOffJobSettings['camera'])))
        focalLengthData = {'focalLength': focalLength}

        layerInfoJson = os.path.join(os.path.dirname(newImagePath), 'layerInfo.json').replace('\\', '/')
        api.json.json_write(focalLengthData, layerInfoJson)

        kickOffVrsceneFolder = '/'.join(vrsceneFileName.split('/')[:-1]).replace('RENDER', 'VRSCENE')
        layerJsonDetails = os.path.join(kickOffVrsceneFolder, 'settings.json')

        api.json.json_write(layerDict, layerJsonDetails)


################################################################################
# @brief      Sets the surfacing render layers.
#
def surfacingSceneSetup():
    project = os.getenv('PROJECT')
    renderUsing = cmd.getAttr("defaultRenderGlobals.currentRenderer")

    if renderUsing == 'vray':
        cmd.setAttr("vraySettings.imageFormatStr", "exr (multichannel)", type='string')
        cmd.setAttr("hardwareRenderingGlobals.textureMaxResolution", 512)
        cmd.setAttr("hardwareRenderingGlobals.enableTextureMaxRes", 1)

        for override in masterLayerOverrides:
            cmd.setAttr("vraySettings.{}".format(override), masterLayerOverrides[override])

        mel.eval("vrayUpdateResolution()")
        cmd.vray("vfbControl", "-bkgr", True)
        if os.path.exists("//speed.nas/{}/02_production/01_content/_config/lightRig/SURFACING/Background.png".format(project.lower())):
            cmd.vray("vfbControl", "-loadbkgrimage", "//speed.nas/{}/02_production/01_content/_config/lightRig/SURFACING/Background.png".format(project.lower()))

        if cmd.objExists('CTRL_set'):
            cmd.setAttr("CTRL_set.Rattr", 0)

    else:
        cmd.confirmDialog(title='ERROR', message='First select Render V-Ray', icon='critical', button=['Ok'])

    return
