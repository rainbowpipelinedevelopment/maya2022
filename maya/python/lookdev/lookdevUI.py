import importlib
import functools
import os
from PySide2 import QtWidgets, QtGui, QtCore


import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI
import api.log
import lookdev.lookdevTools as lookdevTools
import common.checkUpdate.checkUpdateUI as checkUpdateUI
import depts.surfacing.tools.main.shaderLibraryUI as shaderLibraryUI

# importlib.reload(rbwUI)
importlib.reload(api.log)
importlib.reload(lookdevTools)
importlib.reload(checkUpdateUI)
importlib.reload(shaderLibraryUI)

iconPath = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'common').replace('\\', '/')


class LookdevUI(rbwUI.RBWDockableWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super().__init__()

        if cmd.window('LookdevUI', exists=True):
            cmd.deleteUI('LookdevUI')

        if cmd.window('LookdevUIWorkspaceControl', exists=True):
            cmd.deleteUI('LookdevUIWorkspaceControl')

        self.projectName = os.getenv('PROJECT')
        api.log.logger().debug(self.projectName)
        self.mcFolder = os.getenv('MC_FOLDER')
        api.log.logger().debug(self.mcFolder)
        self.id_mv = os.getenv('ID_MV')
        api.log.logger().debug(self.id_mv)

        self.tods = {
            0: 'studio',
            1: 'midday',
            2: 'night',
            3: 'morning',
            4: 'reflection',
            5: 'hybrid'
        }

        self.geos = 'viewtool_geos_lookdev_:CTRL_set'
        self.lightrig = lookdevTools.getLightrig()
        self.camera = lookdevTools.getCamera()
        self.assets = lookdevTools.getAllAssets()

        self.setObjectName('LookdevUI')
        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setStyle()
        ########################################################################
        # MAIN SCROLL AREA
        #
        self.scrollWidget = QtWidgets.QScrollArea()
        self.scrollWidget.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.scrollWidget.setStyleSheet('background-color: transparent; border-color: transparent')
        self.scrollWidget.setWidgetResizable(True)

        self.topWidget = rbwUI.RBWFrame(layout='V', radius=5)
        self.scrollWidget.setWidget(self.topWidget)

        self.mainLayout.addWidget(self.scrollWidget)

        # ------------------------ ASSET FEATURES ---------------------------- #
        self.assetFeaturesGroup = rbwUI.RBWGroupBox(title='Asset:', layout='V', spacing=3, fontSize=13)

        self.categoryComboBox = rbwUI.RBWComboBox(text='Category:', bgColor='transparent')
        self.categoryComboBox.activated.connect(self.fillGroupComboBox)

        self.assetFeaturesGroup.addWidget(self.categoryComboBox)

        self.groupComboBox = rbwUI.RBWComboBox(text='Group:', bgColor='transparent')
        self.groupComboBox.activated.connect(self.fillNameComboBox)

        self.assetFeaturesGroup.addWidget(self.groupComboBox)

        self.nameComboBox = rbwUI.RBWComboBox(text='Name:', bgColor='transparent')
        self.nameComboBox.activated.connect(self.fillVariantComboBox)

        self.assetFeaturesGroup.addWidget(self.nameComboBox)

        self.variantComboBox = rbwUI.RBWComboBox(text='Variant:', bgColor='transparent')
        self.variantComboBox.activated.connect(self.setCurrentVariant)

        self.assetFeaturesGroup.addWidget(self.variantComboBox)

        self.topWidget.addWidget(self.assetFeaturesGroup)

        # --------------------- GENERAL IMPORT AREA ------------------------- #
        self.componentsGroup = rbwUI.RBWGroupBox(title='Components:', layout='V', spacing=1, fontSize=13)

        self.importAllButton = self.addButton('All', ['../lookdev/all.png'], [functools.partial(self.importComponent, 'all'), functools.partial(self.deleteComponent, 'all')], 35, 'left')
        self.importLightrigButton = self.addButton('Lightrig', ['../lookdev/lightrig.png'], [functools.partial(self.importComponent, 'lightrig'), functools.partial(self.deleteComponent, 'lightrig')], 35, 'left')
        self.importCameraButton = self.addButton('Camera', ['../lookdev/camera.png'], [functools.partial(self.importComponent, 'camera'), functools.partial(self.deleteComponent, 'camera')], 35, 'left')
        self.importGeosButton = self.addButton('Geos', ['../lookdev/geos.png'], [functools.partial(self.importComponent, 'geos'), functools.partial(self.deleteComponent, 'geos')], 35, 'left')
        self.importSphereBallButton = self.addButton('SphereBall', ['../lookdev/sphereBall.png'], [functools.partial(self.importComponent, 'sphereBall'), functools.partial(self.deleteComponent, 'sphereBall')], 35, 'left')
        self.importRenderElementsButton = self.addButton('Render Elements', ['../lookdev/renderElements.png'], [functools.partial(lookdevTools.importRenderElememts), functools.partial(lookdevTools.deleteRenderElements)], 35, 'left')

        self.componentsGroup.addWidget(self.importAllButton)
        self.componentsGroup.addWidget(self.importLightrigButton)
        self.componentsGroup.addWidget(self.importCameraButton)
        self.componentsGroup.addWidget(self.importGeosButton)
        self.componentsGroup.addWidget(self.importSphereBallButton)
        self.componentsGroup.addWidget(self.importRenderElementsButton)

        self.topWidget.addWidget(self.componentsGroup)

        # --------------------- ASSET IMPORT AREA -------------------------- #
        self.assetsGroup = rbwUI.RBWGroupBox(title='Assets:', layout='V', spacing=1, fontSize=13)

        self.importBlendshapeButton = self.addButton('BlendShape', ['../lookdev/blendshape.png'], [functools.partial(self.manageAddComponent, 'gbs', 'import'), functools.partial(self.manageAddComponent, 'gbs', 'update'), functools.partial(self.manageAddComponent, 'gbs', 'delete')], 35, 'left')
        self.importRndButton = self.addButton('Rnd', ['../lookdev/rnd.png'], [functools.partial(self.manageAddComponent, 'rnd', 'import'), functools.partial(self.manageAddComponent, 'rnd', 'update'), functools.partial(self.manageAddComponent, 'rnd', 'delete')], 35, 'left')
        self.importHigButton = self.addButton('Hig', ['../lookdev/hig.png'], [functools.partial(self.manageAddComponent, 'hig', 'import'), functools.partial(self.manageAddComponent, 'hig', 'update'), functools.partial(self.manageAddComponent, 'hig', 'delete')], 35, 'left')
        self.importLatestLookdevCameraButton = self.addButton('Latest Cam', ['../lookdev/camera.png'], [functools.partial(self.importLatestLookdevCamera)], 35, 'left')
        self.importLatestLookdevExtraButton = self.addButton('Latest Extra', ['../lookdev/extra.png'], [functools.partial(self.importLatestLookdevExtra)], 35, 'left')
        self.importCustomLightrigButton = self.addButton('Custom Lightrig', ['../lookdev/lightrig.png'], [functools.partial(self.manageAddComponent, 'lightrig', 'import')], 35, 'left')

        self.assetsGroup.addWidget(self.importBlendshapeButton)
        self.assetsGroup.addWidget(self.importHigButton)
        self.assetsGroup.addWidget(self.importRndButton)
        self.assetsGroup.addWidget(self.importLatestLookdevCameraButton)
        self.assetsGroup.addWidget(self.importLatestLookdevExtraButton)
        self.assetsGroup.addWidget(self.importCustomLightrigButton)

        self.topWidget.addWidget(self.assetsGroup)

        # ------------------------ SHADING AND TEXTURING --------------------#
        self.shadingAndTextGroup = rbwUI.RBWGroupBox(title='Shading & Texturing:', layout='V', spacing=1, fontSize=13)

        self.sceneSetupButton = self.addButton('Scene Setup', ['../lookdev/setup.png'], [functools.partial(lookdevTools.surfacingSceneSetup)], 35, 'left')
        self.shaderLibraryButton = self.addButton('Shader Library', ['../lookdev/shader_library.png'], [self.openShaderLibrary], 35, 'left')
        self.copyTextInContentButton = self.addButton('Copy Text in MC', ['../lookdev/copy.png'], [functools.partial(self.copyTextureInContent)], 35, 'left')

        self.shadingAndTextGroup.addWidget(self.sceneSetupButton)
        self.shadingAndTextGroup.addWidget(self.shaderLibraryButton)
        self.shadingAndTextGroup.addWidget(self.copyTextInContentButton)

        self.topWidget.addWidget(self.shadingAndTextGroup)

        # --------------------------- GENERIC TOOLS -----------------------#
        self.genericToolsGroup = rbwUI.RBWGroupBox(title='Generic Tools:', layout='V', spacing=1, fontSize=13)

        self.checkUpdateButton = self.addButton('Check Update', ['refresh.png'], [functools.partial(self.openCheckUpdate)], 35, 'left')
        self.genericToolsGroup.addWidget(self.checkUpdateButton)

        self.topWidget.addWidget(self.genericToolsGroup)

        # ---------------------------- HDRI -------------------------------- #
        self.hdriOptionGroup = rbwUI.RBWGroupBox(title='HDRI:', layout='V', fontSize=13)

        self.todComboBox = rbwUI.RBWComboBox(text='TOD:', bgColor='transparent')
        self.todComboBox.activated.connect(self.setLightrigTod)

        self.groundPrimaryVisibilityCheckBox = rbwUI.RBWCheckBox(text='Ground Primary Visibility:')
        self.groundPrimaryVisibilityCheckBox.setChecked(False)
        self.groundPrimaryVisibilityCheckBox.stateChanged.connect(self.setGroundPrimaryVisibility)

        self.lightInvisibilityCheckBox = rbwUI.RBWCheckBox(text='Light Invisibility:')
        self.lightInvisibilityCheckBox.setChecked(False)
        self.lightInvisibilityCheckBox.stateChanged.connect(self.setLightInvisibility)

        self.hdriOptionGroup.addWidget(self.todComboBox)
        self.hdriOptionGroup.addWidget(self.groundPrimaryVisibilityCheckBox)
        self.hdriOptionGroup.addWidget(self.lightInvisibilityCheckBox)

        self.topWidget.addWidget(self.hdriOptionGroup)

        self.autoFillUI()

        self.setWindowTitle('Lookdev UI')
        self.setFocus()

    ############################################################################
    # @brief      Adds a button.
    #
    # @param      label         The label
    # @param      iconName      The icon name
    # @param      functionList  The function list
    # @param      iconSize      The icon size
    # @param      align         The align
    #
    # @return     { description_of_the_return_value }
    #
    def addButton(self, label, icon, functionList, iconSize, align='center'):
        # create button
        tempButton = rbwUI.RBWButton(text=label, icon=icon, size=[185, iconSize], align=align)

        # set function
        tempButton.clicked.connect(functionList[0])
        if len(functionList) > 1:
            if len(functionList) == 2:
                popMenu = QtWidgets.QMenu()

                importAction = QtWidgets.QAction("Import", self)
                deleteAction = QtWidgets.QAction("Delete", self)

                popMenu.addAction(importAction)
                popMenu.addAction(deleteAction)

                importAction.triggered.connect(functionList[0])
                deleteAction.triggered.connect(functionList[1])

            elif len(functionList):
                popMenu = QtWidgets.QMenu()
                importAction = QtWidgets.QAction("Import", self)
                deleteAction = QtWidgets.QAction("Delete", self)

                updateAction = QtWidgets.QAction("Update", self)
                updateIcon = QtGui.QIcon()
                updateIcon.addFile(os.path.join(iconPath, 'refresh.png'))
                updateAction.setIcon(updateIcon)

                popMenu.addAction(importAction)
                popMenu.addAction(updateAction)
                popMenu.addAction(deleteAction)

                importAction.triggered.connect(functionList[0])
                updateAction.triggered.connect(functionList[1])
                deleteAction.triggered.connect(functionList[2])

            importIcon = QtGui.QIcon()
            importIcon.addFile(os.path.join(iconPath, 'merge.png'))
            importAction.setIcon(importIcon)

            deleteIcon = QtGui.QIcon()
            deleteIcon.addFile(os.path.join(iconPath, 'delete.png'))
            deleteAction.setIcon(deleteIcon)

            tempButton.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
            tempButton.customContextMenuRequested.connect(functools.partial(self.openRightClickMenu, tempButton, popMenu))

        return tempButton

    ############################################################################
    # @brief      Opens a right click menu.
    #
    # @param      button    The button
    # @param      popMenu   The pop menu
    # @param      position  The position
    #
    def openRightClickMenu(self, button, popMenu, position):
        popMenu.exec_(button.mapToGlobal(position))

    ############################################################################
    # @brief      fill the category combobox.
    #
    def fillCategoryComboBox(self):
        self.categoryComboBox.clear()

        for genre in sorted(self.assets.keys()):
            self.categoryComboBox.addItem(genre)
        self.categoryComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

    ############################################################################
    # @brief      fill the group combobox.
    #
    def fillGroupComboBox(self):
        self.groupComboBox.clear()
        self.currentCategory = self.categoryComboBox.currentText()

        for group in sorted(self.assets[self.currentCategory].keys()):
            self.groupComboBox.addItem(group)
        self.groupComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

    ############################################################################
    # @brief      fill the name combobox.
    #
    def fillNameComboBox(self):
        self.nameComboBox.clear()
        self.currentGroup = self.groupComboBox.currentText()

        for name in sorted(self.assets[self.currentCategory][self.currentGroup].keys()):
            self.nameComboBox.addItem(name)
        self.nameComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

    ############################################################################
    # @brief      fill the varinat combobox.
    #
    def fillVariantComboBox(self):
        self.variantComboBox.clear()
        self.currentName = self.nameComboBox.currentText()

        for variant in sorted(self.assets[self.currentCategory][self.currentGroup][self.currentName]):
            self.variantComboBox.addItem(variant)
        self.variantComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

    ############################################################################
    # @brief      Sets the current variant.
    #
    def setCurrentVariant(self):
        self.currentVariant = self.variantComboBox.currentText()

    ############################################################################
    # @brief      fill hdri fields.
    #
    def fillHdriFields(self):
        if self.lightrig in ['generic_shader_default_:CTRL_set', '{}_shader_default_:CTRL_set'.format(os.getenv('PROJECT'))]:
            if cmd.objExists(self.lightrig):
                self.hdriOptionGroup.show()
                self.todComboBox.clear()

                # combobox
                self.currentTodIndex = int(cmd.getAttr('{}:hdr_VRayMultiSubTex.idGenTex'.format(self.lightrig.split(':')[0])))
                todIndexes = list(self.tods)
                todIndexes.sort()
                for todIndex in todIndexes:
                    self.todComboBox.addItem(self.tods[todIndex])
                self.todComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)
                self.todComboBox.setCurrentIndex(self.currentTodIndex)

                # checkboxs
                groundPrimaryVis = cmd.getAttr('{}:ground_geoShape.primaryVisibility'.format(self.lightrig.split(':')[0]))
                self.groundPrimaryVisibilityCheckBox.setChecked(groundPrimaryVis)

                hdriInvis = cmd.getAttr('{}:hdri_ML0_Shape1.invisible'.format(self.lightrig.split(':')[0]))
                self.lightInvisibilityCheckBox.setChecked(hdriInvis)
            else:
                self.hdriOptionGroup.hide()
        else:
            self.hdriOptionGroup.hide()

    ############################################################################
    # @brief      auto fill all the ui parts.
    #
    def autoFillUI(self):
        self.fillCategoryComboBox()
        self.fillHdriFields()

    ############################################################################
    # @brief      import the given component.
    #
    # @param      component  The component
    #
    def importComponent(self, component):
        lookdevTools.importComponents(component)

        if component == 'all' or component == 'lightrig':
            if cmd.objExists('{}_shader_default_:CTRL_set'.format(self.projectName)):
                self.lightrig = '{}_shader_default_:CTRL_set'.format(self.projectName)
            else:
                self.lightrig = 'generic_shader_default_:CTRL_set'

            self.fillHdriFields()

    ############################################################################
    # @brief      clean the scene from the current references.
    #
    # @param      component  The component
    #
    def deleteComponent(self, component):
        lookdevTools.deleteComponents(component)
        if component == 'all' or component == 'lightrig':
            self.lightrig = None
            self.hdriOptionGroup.hide()

    ############################################################################
    # @brief      Gets the add component parameters.
    #
    # @param      addComponent  The add component
    #
    # @return     The add component parameters.
    #
    def getAddComponentParams(self, addComponent):
        if addComponent == 'rnd':
            dept = 'surfacing'
            deptType = 'rnd'
            approvation = 'wip'

        elif addComponent == 'hig':
            dept = 'modeling'
            deptType = 'hig'
            approvation = 'wip'

        elif addComponent == 'gbs':
            dept = 'modeling'
            deptType = 'gbs'
            approvation = 'def'

        elif addComponent == 'lightrig':
            dept = 'surfacing'
            deptType = 'rnd'
            approvation = 'def'

        assetParam = {
            'asset': {
                'id_mv': self.id_mv,
                'category': self.currentCategory,
                'group': self.currentGroup,
                'name': self.currentName,
                'variant': self.currentVariant
            },
            'dept': dept,
            'deptType': deptType,
            'approvation': approvation
        }

        return assetParam

    ############################################################################
    # @brief      manage the latest wip for the given dept.
    #
    # @param      dept  The dept
    #
    def manageAddComponent(self, addComponent, mode='import'):
        try:
            assetParam = self.getAddComponentParams(addComponent)

            if mode == 'import':
                lookdevTools.importAddComponent(assetParam)
            elif mode == 'update':
                lookdevTools.updateAddComponent(assetParam)
            else:
                lookdevTools.deleteAddComponent(assetParam)

            if assetParam['asset']['category'] == 'lightrig':
                self.fillHdriFields()

        except AttributeError:
            rbwUI.RBWError(title='ATTENTION', text='Please insert asset features.')

    ############################################################################
    # @brief      import the latest lookdev camera.
    #
    def importLatestLookdevCamera(self):
        try:
            assetParams = {
                'genre': self.currentCategory,
                'group': self.currentGroup,
                'name': self.currentName,
                'variant': self.currentVariant
            }

            lookdevTools.importLatestLookdevCamera(assetParams)

        except AttributeError:
            cmd.confirmDialog(title='ATTENTION', icon='critical', message='Please insert asset features.', button=['OK'])

    ############################################################################
    # @brief      import the latest lookdev extra.
    #
    def importLatestLookdevExtra(self):
        try:
            assetParams = {
                'genre': self.currentCategory,
                'group': self.currentGroup,
                'name': self.currentName,
                'variant': self.currentVariant
            }

            lookdevTools.importLatestLookdevExtra(assetParams)

        except AttributeError:
            cmd.confirmDialog(title='ATTENTION', icon='critical', message='Please insert asset features.', button=['OK'])

    ############################################################################
    # @brief      Sets the ground primary visibility.
    #
    def setGroundPrimaryVisibility(self):
        cmd.setAttr('{}:ground_geoShape.primaryVisibility'.format(self.lightrig.split(':')[0]), self.groundPrimaryVisibilityCheckBox.isChecked())

    ############################################################################
    # @brief      Sets the light invisibility.
    #
    def setLightInvisibility(self):
        cmd.setAttr('{}:hdri_ML0_Shape1.invisible'.format(self.lightrig.split(':')[0]), self.lightInvisibilityCheckBox.isChecked())

    ############################################################################
    # @brief      Sets the lightrig tod.
    #
    def setLightrigTod(self):
        self.currentTodIndex = int(self.todComboBox.currentIndex())
        cmd.setAttr('{}:hdr_VRayMultiSubTex.idGenTex'.format(self.lightrig.split(':')[0]), self.currentTodIndex)
        cmd.getAttr('{}:{}_VRayPlaceEnvTex.horRotation'.format(self.lightrig.split(':')[0], self.tods[self.currentTodIndex]))

    ############################################################################
    # @brief      launch the copy textures in content tool from lookdev tools.
    #
    def copyTextureInContent(self):
        try:
            params = {
                'category': self.currentCategory,
                'group': self.currentGroup,
                'name': self.currentName,
                'variant': self.currentVariant
            }

            lookdevTools.copyTextureInContent(params)
        except AttributeError:
            rbwUI.RBWError(title='ATTENTION', text='Please insert asset features.')

    ############################################################################
    # @brief      Opens a check update.
    #
    def openCheckUpdate(self):
        win = checkUpdateUI.CheckUpdateUI('lighting')
        win.show()

    ############################################################################
    # @brief      Opens a shader library.
    #
    def openShaderLibrary(self):
        win = shaderLibraryUI.ShaderLibraryUI()
        win.show()

    def testFunction(self):
        print(self.todComboBox.currentText())
