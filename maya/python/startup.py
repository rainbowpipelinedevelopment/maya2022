import importlib
import os
import sys
import datetime

import maya.cmds as cmd
import maya.mel as mel
import maya.api.OpenMaya as om

import api.log
import api.json
import utilz.shelfsInspector as shelfsInspector
import common.cleanupMayaScriptJobs as cleanupMayaScriptJobs

importlib.reload(api.log)
importlib.reload(api.json)
importlib.reload(shelfsInspector)

version = "1.0 - ARAKNO"
year = "since ??/??/2022"
type = "[ ARAKNO ]"
system = os.name

envDivisor = ";"
if system == "posix":
    envDivisor = ":"


################################################################################
# @brief      Sets the pipeline.
#
# @param      pipelinePath  The pipeline path
# @param      contentPath   The content path
#
def setPipeline(pipelinePath, contentPath):
    startTxt = """
    -------------------------------------------------------

    \tRAINBOW CGI ANIMATION STUDIO

    -------------------------------------------------------
    \n
    -------------------------------------------------------
      PIPELINE SETTINGS v %s  |  relase: %s
    -------------------------------------------------------
    """ % (version, year)

    errorTxt = """
    -------------------------------------------------------
    \tSOME ERRORS HAS OCCURED CONTACT SUPPORT:
    \t\tpipeline@rbw-cgi.it
    -------------------------------------------------------
    """

    api.log.logger().debug(startTxt)

    # controllo le cartelle dentro il path definito
    if pipelinePath and os.path.exists(pipelinePath):
        pluginPath = os.path.join(pipelinePath, "plugins")
        pythonPath = os.path.join(pipelinePath, "python")
        melPath = os.path.join(pipelinePath, "mel")
        iconsPath = os.path.join(pipelinePath, "img", "icons")
        libPath = os.path.join(pythonPath, "lib")

        # settaggio moduli pipeline
        sys.path.append(pipelinePath.replace('\\', '/'))

        # aggiungo SPEED_SHARE alle variabili
        os.environ["SPEED_SHARE"] = '\\\\speed.nas'

        # MEL
        setMels(melPath)

        # LIBRARIES
        setLibraries(pythonPath, libPath)

        # PLUGINS
        setPlugins(pluginPath)

        # ICONS
        setIcons(iconsPath)

        printUserInfoes()

        setProject()

        if om.MGlobal.mayaState() != om.MGlobal.kBatch:
            cmd.evalDeferred('import startup; startup.setupMayaPreferences("{}")'.format(pipelinePath))
        else:
            api.log.logger().debug("Maya running in batch mode - no shelf needed")

        settingsData = api.json.json_read(os.path.join(os.getenv('LOCALPIPE'), 'json_file', 'settings.json'))
        if not os.path.exists(settingsData['settingsPath'].replace('USERNAME', os.getenv('USERNAME'))):
            api.json.json_write(settingsData['default'], settingsData['settingsPath'].replace('USERNAME', os.getenv('USERNAME')))
    else:
        api.log.logger().debug(errorTxt)


################################################################################
# @brief      Sets the plugins.
#
# @param      pluginPath  The plugin path
#
def setPlugins(pluginPath):
    import api.database
    importlib.reload(api.database)

    mayaVersion = cmd.about(version=1)
    api.log.logger().debug("mayaVersion {}".format(mayaVersion))

    mayaRelease = 'maya{}'.format(mayaVersion)

    if cmd.about(win=1):
        opsys = 'windows'
    elif cmd.about(linux=1):
        opsys = 'linux'

    plugin_Paths = []
    pluginENV = "MAYA_PLUG_IN_PATH"

    finalPath = os.path.join(pluginPath, opsys, mayaRelease)
    plugin_Paths.append(finalPath + envDivisor)

    oldPluginENV = os.getenv(pluginENV)
    if oldPluginENV:
        plugin_Paths.append(oldPluginENV + envDivisor)

    allPlugin = "".join(plugin_Paths)
    os.environ[pluginENV] = allPlugin

    # ADD PLUGIN
    addPlugsQuery = "SELECT `mode`, `path`, `env_variable`, `env_variable_value` FROM `p_prj_plugins` as prjplug JOIN `p_add_plugins` as paddplug ON prjplug.id_plugin = paddplug.id WHERE prjplug.id_mv = {} AND paddplug.software='{}'".format(os.getenv('ID_MV'), mayaVersion)
    addPlugs = api.database.selectQuery(addPlugsQuery)

    if len(addPlugs) > 0:
        for plug in addPlugs:
            if plug[0] == 'pythonpath':
                api.log.logger().debug('adding: {} to pythonpath'.format(plug[1]))
                sys.path.append(os.path.normpath(plug[1]))
            os.environ[plug[2]] = plug[3]


################################################################################
# @brief      Sets the mels.
#
# @param      melsPath  The mels path
#
def setMels(melsPath):
    mel_Paths = []
    melENV = "MAYA_SCRIPT_PATH"

    modulesFldr = os.listdir(melsPath)

    # per ogni cartella trovata
    for module in modulesFldr:
        path = os.path.join(melsPath, module)
        nPath = os.path.normpath(path)
        mel_Paths.append(nPath + envDivisor)

    # traverse mel subdirectories
    numMelPaths = len(mel_Paths)

    for i in range(0, numMelPaths):
        for dirname, dirnames, filenames in os.walk(mel_Paths[i][:-1]):
            for subdirname in dirnames:
                mel_Paths.append(os.path.join(dirname, subdirname) + envDivisor)

    api.log.logger().debug("MEL:")
    for script in mel_Paths:
        api.log.logger().debug("mel Dir script: {}".format(script))

    oldMelEnv = os.getenv(melENV)

    if oldMelEnv is not None:
        mel_Paths.append(oldMelEnv + envDivisor)

    allpaths = "".join(mel_Paths)
    os.environ[melENV] = allpaths


################################################################################
# @brief      Sets the icons.
#
# @param      iconsPath  The icons path
#
def setIcons(iconsPath):
    icons_Paths = []
    iconsENV = "XBMLANGPATH"

    icons_Paths.append(iconsPath + envDivisor)

    oldIconsENV = os.getenv(iconsENV)
    if oldIconsENV:
        icons_Paths.append(oldIconsENV + envDivisor)

    allIcons = "".join(icons_Paths)
    os.environ[iconsENV] = allIcons


################################################################################
# @brief      Sets the libraries.
#
# @param      pythonPath  The python path
# @param      libPath     The library path
#
def setLibraries(pythonPath, libPath):
    for lib in os.listdir(pythonPath):
        if os.path.isdir(os.path.join(pythonPath, lib)):
            sys.path.append(os.path.join(pythonPath, lib))
    sys.path.append(os.path.normpath(os.path.join(pythonPath, "python-api-master", "sgtk")))

    # aggiungo al path le librerie
    for lib in os.listdir(libPath):
        libFullPath = os.path.normpath(os.path.join(libPath, lib))
        if os.path.isdir(libFullPath):
            sys.path.append(libFullPath)


################################################################################
# @brief      Prints user infoes.
#
def printUserInfoes():
    api.log.logger().debug("-" * 55)
    nw = datetime.datetime.now()
    clock = nw.time()
    api.log.logger().debug('User informations   |   {} - {}'.format(cmd.about(cd=1), (str(clock)[:5])))
    hostnm = mel.eval('system("hostname")')
    usr = mel.eval('getenv "USER"')
    api.log.logger().debug("Machine: {}".format(hostnm)),
    api.log.logger().debug("User: {}".format(usr))
    api.log.logger().debug("Operating System: {}".format(cmd.about(os=1)))
    api.log.logger().debug(cmd.about(osv=1))
    api.log.logger().debug("Maya Version: {}".format(cmd.about(v=1)))
    api.log.logger().debug("Maya Product: {}".format(cmd.about(p=1)))
    if cmd.about(x64=1):
        bit = 64
    else:
        bit = 32
    api.log.logger().debug("Bit version: {}".format(bit))
    api.log.logger().debug("Api Version: {}".format(cmd.about(api=1)))
    api.log.logger().debug("-" * 55)


################################################################################
# @brief      Sets the project.
#
def setProject():
    projectDir = os.getenv('MC_FOLDER')
    cmd.workspace(projectDir, openWorkspace=True)

    api.log.logger().debug('#' * 80)
    api.log.logger().debug('WORKSPACE: {}'.format(cmd.workspace(query=True, openWorkspace=True)))
    api.log.logger().debug('#' * 80)


################################################################################
# @brief      setup maya preferences.
#
def setupMayaPreferences(pipelinePath):
    import api.widgets.rbw_UI as rbwUI
    import api.database
    importlib.reload(api.database)

    # fps
    setMayaFramerate()

    # resolution
    setMayaResolution()

    # Disable default light
    cmd.setAttr('defaultRenderGlobals.enableDefaultLight', False)

    cmd.currentTime(1)

    # Print all preferences
    api.log.logger().debug("WIDTH: {}".format(cmd.getAttr('defaultResolution.width')))
    api.log.logger().debug("HEIGHT: {}".format(cmd.getAttr('defaultResolution.height')))
    api.log.logger().debug("PIXEL ASPECT: {}".format(cmd.getAttr('defaultResolution.pixelAspect')))
    api.log.logger().debug("DEVICE ASPECT RATIO: {}".format(cmd.getAttr('defaultResolution.deviceAspectRatio')))
    api.log.logger().debug("DEFAULT LIGHT: {}".format(cmd.getAttr('defaultRenderGlobals.enableDefaultLight')))
    api.log.logger().debug("FPS: {} - {}".format(cmd.currentUnit(query=True, time=True), mel.eval('currentTimeUnitToFPS')))

    # current unit
    cmd.currentUnit(linear='cm')
    api.log.logger().debug('LINEAR SCALE SET ON CENTIMETER')

    # Set the default value for UP Axis Y (to prevent un-wanted user changes)
    mel.eval('setUpAxis "y";')
    api.log.logger().debug("UP-AXIS: set to Y as the Maya default is")

    # Maya Catmul Clark for new poly preference
    cmd.polyOptions(newPolymesh=True, smoothDrawType=0)
    api.log.logger().debug("Subdivision Method: Maya Catmull-Clark (for new meshes)")

    # MayaCatmul Clark for global subdivision method
    cmd.polyOptions(smoothDrawType=0)
    api.log.logger().debug("Global Subdivision Method: Maya Catmull-Clark")

    # new font size bigger for full-hd playblasts
    mel.eval("optionVar -iv fontSetOpt 2;")
    mel.eval("displayPref -fm 2;")
    mel.eval("displayPref -sfs 12;")
    mel.eval("displayPref -dfs 16;")
    api.log.logger().debug("FONT SIZE CHANGED FOR PLAYBLASTS")

    # set evaluation to 'parallel' and gpuOverride to 0 to avoid xgen craches
    cmd.evaluationManager(mode='parallel')
    cmd.optionVar(iv=('gpuOverride', 0))

    # check on coloraspace
    userDeptQuery = "SELECT `dept` FROM `V_userInfo` WHERE `username` = '{}'".format(os.getenv('USERNAME'))
    userDept = api.database.selectSingleQuery(userDeptQuery)[0].lower()

    colorConfigPath = cmd.colorManagementPrefs(query=True, configFilePath=True)
    if 'Maya-legacy' not in colorConfigPath:
        mel.eval("changeColorMgtPrefsConfigFilePath(\"legacy\");")

    policyFileName = cmd.colorManagementPrefs(query=True, policyFileName=True)
    if policyFileName != 'C:/RBW_files/colorManagement_rules_03.xml' and userDept == 'surfacing':
        rbwUI.RBWWarning(text='Rules file not loaded.\nPlease load from "C:/RBW_files/colorManagement_rules_03.xml"', defCancel=None)

    import maya.OpenMaya
    if maya.OpenMaya.MGlobal.mayaState() != maya.OpenMaya.MGlobal.kBatch:
        loadShelf()
        loadMenu(pipelinePath)
    else:
        api.log.logger().debug("Maya running in batch mode - no shelf needed")

    # set the plug-in env variable
    addPlugs = getPlugins()
    for plug in addPlugs:
        if plug[0] == 'pythonpath':
            api.log.logger().debug('adding: {} to pythonpath'.format(plug[1]))
            sys.path.append(os.path.normpath(plug[1]))
        os.environ[plug[2]] = plug[3]

    cleanApp = cleanupMayaScriptJobs.cleanupMayaScriptJobs()
    cleanApp.delete_worm_nodes()
    cleanApp.cleanup_maya_scripts()
    api.log.logger().debug("..eventual cleanup for worms is done...")


################################################################################
# @brief      Gets the framerate.
#
# @return     The framerate.
#
def getFramerate():
    try:
        queryFPS = "SELECT name FROM `p_prj_framerate` AS pf JOIN `p_framerate` AS f ON pf.framerate_id = f.id WHERE pf.id_mv = {}".format(os.getenv("ID_MV"))
        fps = api.database.selectQuery(queryFPS)[0][0].lower()
        if fps.count('frame'):
            fps = fps.split('frame')[0]
        return fps
    except:
        return


################################################################################
# @brief      Gets the plugins.
#
# @return     The plugins.
#
def getPlugins():
    mayaVersion = 'maya{}'.format(cmd.about(version=True))
    addPlugsQuery = "SELECT `mode`, `path`, `env_variable`, `env_variable_value` FROM `p_prj_plugins` as prjplug JOIN `p_add_plugins` as paddplug ON prjplug.id_plugin = paddplug.id WHERE prjplug.id_mv = {} AND paddplug.software='{}'".format(os.getenv('ID_MV'), mayaVersion)
    addPlugs = api.database.selectQuery(addPlugsQuery)

    if addPlugs:
        return addPlugs
    else:
        return []


################################################################################
# @brief      Sets the maya framerate.
#
def setMayaFramerate():
    fps = getFramerate()
    if fps:
        cmd.optionVar(sv=('workingUnitTimeDefault', fps))
        cmd.currentUnit(time=fps)
        fpsDict = {
            'game': om.MTime.kGames,
            'film': om.MTime.kFilm,
            'pal': om.MTime.kPALFrame,
            'ntsc': om.MTime.kNTSCFrame,
            'show': om.MTime.kShowScan,
            'palf': om.MTime.kPALField,
            'ntscf': om.MTime.kNTSCField
        }
        om.MTime.setUIUnit(fpsDict[fps])
    else:
        cmd.optionVar(sv=('workingUnitTimeDefault', 'pal'))
        cmd.currentUnit(time='pal')
        om.MTime.setUIUnit(om.MTime.kPALFrame)
        # rmail.sendRbwLogMail("FPS SETTINGS FROM DB NOT AVAILABLE")
        cmd.confirmDialog(icn='critical', b='Dismiss', m='Could not retrieve fps settings from DB.\nContact pipeline@rbw-cgi.it as soon as possible.')


################################################################################
# @brief      Gets the resolution.
#
# @return     The resolution.
#
def getResolution():
    try:
        resolutionQuery = "SELECT width, height FROM `p_prj_resolution` AS pr JOIN `p_resolution` AS r ON pr.resolutionId = r.id WHERE pr.id_mv = {}".format(
            os.getenv("ID_MV")
        )

        resolutionTuple = api.database.selectQuery(resolutionQuery)[0]

        return resolutionTuple
    except:
        return


################################################################################
# @brief      Sets the maya resolution.
#
def setMayaResolution():
    resolutionTuple = getResolution()
    if resolutionTuple:
        cmd.setAttr('defaultResolution.width', resolutionTuple[0])
        cmd.setAttr('defaultResolution.height', resolutionTuple[1])
    else:
        # rmail.sendRbwLogMail("RESOLUTION FROM DB NOT AVAILABLE")
        cmd.confirmDialog(icn='critical', b='Dismiss', m='Could not retrieve resolution settings from DB.\nContact pipeline@rbw-cgi.it as soon as possible.')


################################################################################
# @brief      Loads a shelf.
#
def loadShelf():
    import api.shelf
    importlib.reload(api.shelf)

    shelf_inspector = shelfsInspector.shelfsInspector('shelfs', 'python')

    for shelfDict in shelf_inspector.buttonPackages:
        shelfName = shelfDict['name']
        shelf = api.shelf.createShelf(shelfName)
        for shelfButton in shelfDict['buttons']:
            api.shelf.addButton(
                label=shelfButton['label'],
                command=shelfButton['launch'],
                image=shelfButton['icon'],
                shelfLayout=shelf
            )


################################################################################
# @brief      Loads a menu.
#
def loadMenu(pipelinePath):
    import menus as RBWm
    importlib.reload(RBWm)

    RBWm.Create(pipelinePath)
