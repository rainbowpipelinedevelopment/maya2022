import importlib
import os

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'arakno')


################################################################################
# @brief      run modeling tab.
#
# @param      args  The arguments
#
def run_modelingTab(*args):
    import common.deptInterface as deptInt
    import utilz.utilities as utilities

    importlib.reload(deptInt)
    importlib.reload(utilities)
    deptInt.DeptWindow.showInterface('modeling', 'modeling', None)


################################################################################
# @brief      run surfacing tab.
#
# @param      args  The arguments
#
def run_surfacingTab(*args):
    import common.deptInterface as deptInt
    import utilz.utilities as utilities

    importlib.reload(deptInt)
    importlib.reload(utilities)

    deptInt.DeptWindow.showInterface('surfacing', 'surfacing', None)


################################################################################
# @brief      run grooming tab.
#
# @param      args  The arguments
#
def run_groomingTab(*args):
    import common.deptInterface as deptInt
    import utilz.utilities as utilities

    importlib.reload(deptInt)
    importlib.reload(utilities)

    deptInt.DeptWindow.showInterface('grooming', 'grooming', None)


################################################################################
# @brief      run rigging tab.
#
# @param      args  The arguments
#
def run_riggingTab(*args):
    import common.deptInterface as deptInt
    import utilz.utilities as utilities

    importlib.reload(deptInt)
    importlib.reload(utilities)
    deptInt.DeptWindow.showInterface('rigging', 'rigging', None)


################################################################################
# @brief      run set dressing tab.
#
# @param      args  The arguments
#
def run_setTab(*args):
    import common.deptInterface as deptInt
    import utilz.utilities as utilities

    importlib.reload(deptInt)
    importlib.reload(utilities)
    deptInt.DeptWindow.showInterface('setdressing', 'setdressing', None)


################################################################################
# @brief      run fx tab.
#
# @param      args  The arguments
#
def run_fxTab(*args):
    import common.deptInterface as deptInt
    import utilz.utilities as utilities

    importlib.reload(deptInt)
    importlib.reload(utilities)
    deptInt.DeptWindow.showInterface('fx', 'fx', None)


################################################################################
# @brief      tun animation tab.
#
# @param      args  The arguments
#
def run_animTab(*args):
    import common.deptInterface as deptInt
    import utilz.utilities as utilities

    importlib.reload(deptInt)
    importlib.reload(utilities)
    deptInt.DeptWindow.showInterface('animation', 'animation', None)


################################################################################
# @brief      run finishing tab.
#
# @param      args  The arguments
#
def run_finshingTab(*args):
    import common.deptInterface as deptInt
    import utilz.utilities as utilities

    importlib.reload(deptInt)
    importlib.reload(utilities)
    deptInt.DeptWindow.showInterface('finishing', 'finishing', None)


################################################################################
# @brief      run the lighting tab.
#
# @param      args  The arguments
#
def run_lightingTab(*args):
    import common.deptInterface as deptInt
    import utilz.utilities as utilities

    importlib.reload(deptInt)
    importlib.reload(utilities)
    deptInt.DeptWindow.showInterface('lighting', 'lighting', None)


################################################################################
# @brief      run shotUI
#
# @param      args  The arguments
#
def run_shotUI(*args):
    import common.loadAndSaveShotUI as lss
    importlib.reload(lss)

    win = lss.ShotUI()
    win.show()


################################################################################
# @brief      opens the /mc_prj/scenes folder
#
# @param      args  The arguments
#
def run_openSceneFolder(*args):
    import common.openSceneFolder as openSceneFolder
    importlib.reload(openSceneFolder)

    openSceneFolder.openSceneFolder()


################################################################################
# @brief      open the sg page.
#
# @param      args  The arguments
#
def run_openSgPage(*args):
    import common.openSgPage as openSgPage
    importlib.reload(openSgPage)

    openSgPage.openSgPage()

################################################################################
# @brief      take a viewport screenshot
#
# @param      args  The arguments
#
def run_viewportScreenshot(*args):
    import utilities as ut
    from datetime import datetime

    now = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
    fileName = os.path.join(os.getenv('USERPROFILE'), 'Documents', 'mayaScreenshots', 'mayaViewport_{}'.format(now))
    ut.grabViewport(fileName)


modelingButton = {
    "label": "MDL",
    "launch": run_modelingTab,
    "icon": os.path.join(iconDir, 'modeling.png')
}

surfacingButton = {
    "label": "SURF",
    "launch": run_surfacingTab,
    "icon": os.path.join(iconDir, 'surfacing.png')
}

groomingButton = {
    "label": "GROOM",
    "launch": run_groomingTab,
    "icon": os.path.join(iconDir, 'grooming.png')
}

riggingButton = {
    "label": "RIG",
    "launch": run_riggingTab,
    "icon": os.path.join(iconDir, 'rigging.png')
}

setButton = {
    "label": "SET",
    "launch": run_setTab,
    "icon": os.path.join(iconDir, 'setdressing.png')
}

fxButton = {
    "label": "FX",
    "launch": run_fxTab,
    "icon": os.path.join(iconDir, 'fx.png')
}

animationButton = {
    "label": "ANIM",
    "launch": run_animTab,
    "icon": os.path.join(iconDir, 'animation.png')
}

finishingButton = {
    "label": "FIN",
    "launch": run_finshingTab,
    "icon": os.path.join(iconDir, 'finishing.png')
}


lightingButton = {
    "label": "LGT",
    "launch": run_lightingTab,
    "icon": os.path.join(iconDir, 'lighting.png')
}

shotUI = {
    "label": "L/S Shots",
    "launch": run_shotUI,
    "icon": os.path.join(iconDir, 'loadAndSaveShot.png')
}


openSceneFolder = {
    "label": "Sc Fold",
    "launch": run_openSceneFolder,
    "icon": os.path.join(iconDir, 'openFolder.png')
}


openSgPage = {
    "label": "SG Page",
    "launch": run_openSgPage,
    "icon": os.path.join(iconDir, 'sg.png')
}


viewportScreenshot = {
    "label": "Screenshot",
    "launch": run_viewportScreenshot,
    "icon": os.path.join(iconDir, 'screenshot.png')
}


buttons = [
    modelingButton,
    surfacingButton,
    groomingButton,
    riggingButton,
    setButton,
    fxButton,
    animationButton,
    finishingButton,
    lightingButton,
    shotUI,
    openSceneFolder,
    openSgPage,
    viewportScreenshot
]

PackageButtons = {
    'name': 'ARAKNO',
    "buttons": buttons
}
