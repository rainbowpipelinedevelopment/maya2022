import importlib
import os

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'lookdev')


def run_lookdevToolUI(*args):
    import lookdev.lookdevUI as lookdevUI
    importlib.reload(lookdevUI)

    win = lookdevUI.LookdevUI()
    win.show(dockable=True, area='left')


def run_kickOffUI(*args):
    import lookdev.lookdevKickOffUI as lookdevKickOffUI
    importlib.reload(lookdevKickOffUI)

    win = lookdevKickOffUI.LookdevKickOffUI()
    win.show()


lookdevUIButton = {
    "label": "LKD UI",
    "launch": run_lookdevToolUI,
    "icon": os.path.join(iconDir, 'lookdevUI.png')
}

kickOffUIButton = {
    "label": 'K.O.',
    "launch": run_kickOffUI,
    "icon": os.path.join(iconDir, 'kickOffUI.png')
}

buttons = [
    lookdevUIButton,
    kickOffUIButton
]

PackageButtons = {
    'name': 'LOOKDEV',
    "buttons": buttons
}
