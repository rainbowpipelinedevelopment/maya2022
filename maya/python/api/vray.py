import importlib
import os
import vray.vray_maya_utils as vrayUtils

import maya.cmds as cmd
import maya.mel as mel
import maya.app.renderSetup.model.renderSettings as renderSettings

import api.scene
import api.log

importlib.reload(api.scene)
importlib.reload(api.log)


renderElementsRoot = os.path.join(os.getenv('CONTENT'), '_config', 'lightRig').replace('\\', '/')
renderElementsDenoiseSettingsFolder = os.path.join(renderElementsRoot, 'renderElementsDenoiseSettings').replace('\\', '/')

defaultDenoise = [
    'diff_ML0',
    'diff_ML1',
    'diff_ML2',
    'diff_ML3',
    'diff_ML4',
    'diff_ML5',
    'diff_ML6',
    'diff_ML7',
    'spec_ML0',
    'spec_ML1',
    'spec_ML2',
    'spec_ML3',
    'spec_ML4',
    'spec_ML5',
    'spec_ML6',
    'spec_ML7',
    'gidiff_ML0',
    'gidiff_ML1',
    'gidiff_ML2',
    'gidiff_ML3',
    'gidiff_ML4',
    'gidiff_ML5',
    'gidiff_ML6',
    'gidiff_ML7',
    'gispec_ML0',
    'gispec_ML1',
    'gispec_ML2',
    'gispec_ML3',
    'gispec_ML4',
    'gispec_ML5',
    'gispec_ML6',
    'gispec_ML7',
    'vrayRE_Diffuse',
    'vrayRE_Specular',
    'vrayRE_Reflection',
    'vrayRE_Refraction',
    'vrayRE_GI',
    'vrayRE_SSS',
    'vrayRE_Shadow',
    'vrayRE_Self_Illumination',
    'vrayRE_Atmospheric_Effects',
    'AO'
]


################################################################################
# @brief      get render elements
#
# @return     the render layer list
#
def render_elements():
    vrayRenderElements = cmd.ls(type='VRayRenderElement')
    vrayRenderElements = vrayRenderElements + cmd.ls(type='VRayRenderElementSet')

    return vrayRenderElements


################################################################################
# @brief      import the render elements from path.
#
# @param      path  The path
#
def importRenderElements(path):
    vrayUtils.vrayImportAOV(path)


################################################################################
# @brief      export the render elements to path.
#
# @param      path  The path
#
def exportRenderElements(path):
    vrayUtils.VRAY_AOV_RENDER_SETUP_HACK = True
    try:
        cmd.file(path, force=True, type='mayaBinary', preserveReferences=False, exportSelected=True, constructionHistory=True, prompt=False)
    finally:
        vrayUtils.VRAY_AOV_RENDER_SETUP_HACK = False


################################################################################
# @brief      Adds a render element.
#
# @param      renderElementName  The render element name
#
def addRenderElement(renderElementName=''):
    if not renderElementName:
        return False

    return mel.eval('vrayAddRenderElement {}'.format(renderElementName))


################################################################################
# @brief      Lists all the available image formats
#
# @return     The list of all the available image formats
#
def listImageFormat():
    formats = {}

    for idx, imgFormat in enumerate(mel.eval('string $images[]; $images=$g_vrayImgExt')):
        formats.update({idx + 1: imgFormat})

    return formats


################################################################################
# @brief      Sets the image format.
#
# @param      format  The format
#
# @return     { description_of_the_return_value }
#
def setImageFormat(format=5):
    imageFormat = ''
    formats = listImageFormat()
    for index in formats:
        if format == index:
            imageFormat = formats[index]

    cmd.setAttr('vraySettings.imageFormatStr', imageFormat, type='string')

    return getImageFormat()


################################################################################
# @brief      Sets the image format string.
#
# @param      format  The format
#
# @return     { description_of_the_return_value }
#
def setImageFormatString(formatStr='exr'):
    for formatId, formatName in listImageFormat():
        if formatName == formatStr:
            return setImageFormat(formatId)


################################################################################
# @brief      Gets the image format.
#
# @return     The image format.
#
def getImageFormat():
    return cmd.getAttr('vraySettings.imageFormatStr')


################################################################################
# @brief      Creates a vray file.
#
# @return     the file path.
#
def createVRayFile():
    sceneDirname = api.scene.dirname()
    sceneFilename, sceneExtensione = os.path.splitext(api.scene.filename())
    vrsceneFilename = '{}.vrscene'.format(sceneFilename)
    vrayScene = os.path.join(sceneDirname, vrsceneFilename).replace('\\', '/')

    enableExportVrscene()
    disableRender()
    setVrsceneFilename(vrayScene)
    render()
    disableExportVrscene()
    enableRender()

    return vrayScene


################################################################################
# @brief      Enables the export vrscene.
#
def enableExportVrscene():
    cmd.setAttr('vraySettings.vrscene_on', True)


################################################################################
# @brief      Disables the render.
#
def disableRender():
    cmd.setAttr('vraySettings.vrscene_render_on', False)


################################################################################
# @brief      Sets the vrscene filename.
#
# @param      filename  The filename
#
def setVrsceneFilename(filename=''):
    cmd.setAttr('vraySettings.vrscene_filename', filename, type='string')


################################################################################
# @brief      Renders the object.
#
def render():
    cmd.vrend()


################################################################################
# @brief      Gets the vray version.
#
# @return     The vray version.
#
def getVrayVersion():
    version = mel.eval('vray version')
    return ''.join(version.split('.')[0:3])


################################################################################
# @brief      Disables the export vrscene.
#
def disableExportVrscene():
    cmd.setAttr('vraySettings.vrscene_on', False)


################################################################################
# @brief      Enables the render.
#
def enableRender():
    cmd.setAttr('vraySettings.vrscene_render_on', True)


################################################################################
# @brief      Gets the resolution.
#
# @return     The resolution.
#
def getResolution():
    width = cmd.getAttr('vraySettings.width')
    height = cmd.getAttr('vraySettings.height')

    return width, height


################################################################################
# @brief      Sets the resolution.
#
# @param      width   The width
# @param      height  The height
#
# @return     The resolution.
#
def setResolution(width=1920, height=1080):
    cmd.setAttr('vraySettings.width', width)
    cmd.setAttr('vraySettings.height', height)

    return getResolution()


################################################################################
# @brief      Adds a vrscene.
#
# @param      path  The path
#
# @return     the vrscene node.
#
def addVrscene(path=None):
    mel.eval('vrayVRaySceneCreateNode("{}", 0);'.format(path.replace('\\', '/')))
    return cmd.ls(selection=True)


################################################################################
# @brief      import multiple vrscene sequence
#
# @param      inputFolder  The input folder
# @param      inputFile    The input file
# @param      end          The end
#
# @return     path to new vrscene node.
#
def importMultipleVrScene(inputFolder=None, inputFile=None, end=None):

    api.log.logger().debug('inputFolder: {}\ninputFile: {}'.format(inputFolder, inputFile))

    folder = inputFolder
    correctFile = ((inputFile.split('.')[0].split('_')))
    # nodeName = os.path.basename(inputFolder)
    nodeName = inputFolder.split('/')[-2]
    padding = correctFile
    correctFile = ('_').join(correctFile[:-1])
    pad = len(padding[-1])

    vrsceneShape = cmd.createNode('VRayScene', name='{}_Shape'.format(nodeName))
    vrsceneShape = cmd.ls(selection=True, dag=True, shapes=True, long=True)
    vrsceneTrash = cmd.listRelatives(vrsceneShape, parent=True, fullPath=True)
    cmd.connectAttr('time1.outTime', '{}.inputTime'.format(vrsceneShape[0].split('|')[-1]), force=True)
    vrsceneMesh = cmd.createNode('mesh', name='{}_MeshShape'.format(nodeName))
    cmd.connectAttr('{}.outMesh'.format(vrsceneShape[0].split('|')[-1]), '{}.inMesh'.format(vrsceneMesh))
    vrsceneMeshTransform = cmd.listRelatives(vrsceneMesh, parent=True, fullPath=True)
    cmd.select(vrsceneMesh, replace=True)
    mel.eval('sets -e -forceElement initialShadingGroup;')
    cmd.parent(vrsceneShape[0], vrsceneMeshTransform[0], relative=True, shape=True)

    cmd.setAttr('{}.FilePath'.format(vrsceneShape[0].split('|')[-1]), '{}/{}'.format(folder, correctFile), type='string')

    cmd.delete(vrsceneTrash)
    cmd.setAttr('{}.previewFaces'.format(vrsceneShape[0].split('|')[-1]), 1000)
    vrsceneShape = vrsceneShape[0].split('|')[-1]
    if vrsceneShape != '{}_Shape'.format(nodeName):
        cmd.rename(vrsceneShape, '{}_Shape'.format(nodeName))
        vrsceneShape = '{}_Shape'.format(nodeName)

    frame = cmd.currentTime(query=True)
    frameInt = str(int(frame))
    padding = frameInt.zfill(pad)

    if end is not None:
        exp1 = '''
        int $offset = 0;
        int $endAnim = end;
        int $frameNumber;
        int $i = `currentTime -q`;
        int $offsetedI = $i - $offset + $endAnim - 2;
        int $period = ($endAnim - 1) * 2;
        int $semipiriod = $endAnim - 1;
        int $moduledI;

        if ($offsetedI >= 0){$moduledI = $offsetedI % $period;}
        else{$moduledI = $period - abs($offsetedI);}

        $frameNumber = int(abs($moduledI - $semipiriod) + 1);

        string $pad = `python ("'%0pppppd' % " + $frameNumber)`;
        string $frame = ( "xxxxx_" + $pad + ".vrscene" );
        setAttr -type "string" sssss.FilePath $frame;
    '''
        exp1 = exp1.replace('end;', '{};'.format(end))
    else:
        exp1 = '''
        int $num = `currentTime -q`;
        string $pad = `python ("'%0pppppd' % " + $num)`;
        string $frame = ( "xxxxx_" + $pad + ".vrscene" );
        setAttr -type "string" sssss.FilePath $frame;
    '''
    exp2 = exp1.replace('xxxxx', '{}/{}'.format(folder, correctFile))

    exp3 = exp2.replace('ppppp', str(pad))

    exp4 = exp3.replace('sssss', vrsceneShape)

    cmd.expression(string=exp4, object='', alwaysEvaluate=True, unitConversion='all', name='{}_expression'.format(nodeName))

    currentVrscenePath = cmd.getAttr('{}.FilePath'.format(vrsceneShape))
    setVrscenePreviewFaceCount(currentVrscenePath, 5)

    if not cmd.objExists('VRSCENE'):
        cmd.group(empty=True, n='VRSCENE')

    cmd.parent(vrsceneMeshTransform[0], 'VRSCENE')

    return '|VRSCENE|{}'.format(vrsceneMeshTransform[0])


################################################################################
# @brief      Sets the vrscene preview face count.
#
# @param      path   The path
# @param      nFace  The face
#
def setVrscenePreviewFaceCount(path=None, nFace=0):
    api.log.logger().debug('path: {}, nFace: {}'.format(path, nFace))
    mel.eval('vrayVRayScenePreviewSettings -p "{}"-previewHair 0;'.format(path))


################################################################################
# @brief      Creates a vray proxy.
#
# @param      folder  The folder
# @param      name    The name
#
# @return     the vrayProxy node.
#
def createVrayProxy(folder, name, exportType=1, createInScene=True, frameRange=[]):
    params = {
        'dir': folder,
        'exportType': exportType,
        'fname': '{}.vrmesh'.format(name),
        'exportHierarchy': True,
        'overwrite': True,
        'previewFaces': 1,
        'oneVoxelPerMesh': True,
        'node': name
    }

    if createInScene:
        params['createProxyNode'] = True
        params['newProxyNode'] = True
    else:
        params['createProxyNode'] = False

    if exportType == 3:
        params['animOn'] = True
        params['animType'] = 3
        params['startFrame'] = frameRange[0]
        params['endFrame'] = frameRange[1]

    return cmd.vrayCreateProxy(**params)


def loadAbcInVrayProxy(path, nodeName):
    params = {
        'node': nodeName,
        'existing': True,
        'dir': path,
        'createProxyNode': True,
        'newProxyNode': True
    }
    return cmd.vrayCreateProxy(**params)[0]


################################################################################
# @brief      get vray exr converter path
#
# @return     the path if exists or none.
#
def vrayExrConverterPath():
    vrayExrConverterPath = os.path.join(
        '//speed.nas',
        'arakno',
        'pipeSoftware',
        'vray',
        'maya{}'.format(cmd.about(version=True)),
        getVrayVersion(),
        'vray',
        'bin'
    ).replace('\\', '/')

    api.log.logger().debug('vrayExrConverterPath: {}'.format(vrayExrConverterPath))

    if os.path.exists(vrayExrConverterPath):
        return os.path.join(vrayExrConverterPath, 'img2tiledexr.exe')
    return False


################################################################################
# @brief      Adds a vray texture input gamma.
#
# @param      node  The node
#
def addTextureInputGamma(node):
    cmd.vray('addAttributesFromGroup', node, 'vray_file_gamma', 1)


################################################################################
# @brief      Removes a texture input gamma.
#
# @param      node  The node
#
def removeTextureInputGamma(node):
    cmd.vray('addAttributesFromGroup', node, 'vray_file_gamma', 0)


################################################################################
# @brief      Sets the texture input gamma srgb.
#
# @param      node  The node
#
def setTextureInputGammaSRGB(node):
    cmd.setAttr('{}.vrayFileColorSpace'.format(node), 2)


################################################################################
# @brief      Creates a wrapper.
#
# @return     { description_of_the_return_value }
#
def createWrapper():
    cmd.shadingNode("VRayMtlWrapper", asShader=True, name="arakno_wrapper")
    cmd.sets(renderable=True, noSurfaceShader=True, empty=True, name="arakno_wrapperSG")
    cmd.connectAttr("arakno_wrapper.outColor", "arakno_wrapperSG.surfaceShader", force=True)
    cmd.defaultNavigation(connectToExisting=True, destination="arakno_wrapper.baseMaterial", source="sl_assetName_trace_meshName_shader")
    cmd.connectAttr("sl_assetName_trace_meshName_shader.outColor", "arakno_wrapper.baseMaterial", force=True)
    cmd.setAttr("arakno_wrapper.useIrradianceMap", 0)
    cmd.setAttr("arakno_wrapper.generateGI", 0)
    cmd.setAttr("arakno_wrapper.receiveGI", 0)
    cmd.setAttr("arakno_wrapper.generateCaustics", 0)
    cmd.setAttr("arakno_wrapper.receiveCaustics", 0)
    cmd.setAttr("arakno_wrapper.matteSurface", 1)
    cmd.setAttr("arakno_wrapper.alphaContribution", -1)
    cmd.setAttr("arakno_wrapper.shadows", 1)
    cmd.setAttr("arakno_wrapper.affectAlpha", 1)
    cmd.setAttr("arakno_wrapper.reflectionAmount", 0)
    cmd.setAttr("arakno_wrapper.refractionAmount", 0)


################################################################################
# @brief       Creates a black hole wrapper.
#
# @return      { description_of_the_return_value }
#
def createBlackHoleWrapper():
    cmd.shadingNode("VRayMtlWrapper", asShader=True, name="arakno_blackHole_wrapper")
    cmd.sets(renderable=True, noSurfaceShader=True, empty=True, name="arakno_blackHole_wrapperSG")
    cmd.connectAttr("arakno_blackHole_wrapper.outColor", "arakno_blackHole_wrapperSG.surfaceShader", force=True)
    cmd.defaultNavigation(connectToExisting=True, destination="arakno_blackHole_wrapper.baseMaterial", source="sl_assetName_trace_meshName_shader")
    cmd.connectAttr("sl_assetName_trace_meshName_shader.outColor", "arakno_blackHole_wrapper.baseMaterial", force=True)
    cmd.setAttr("arakno_blackHole_wrapper.useIrradianceMap", 0)
    cmd.setAttr("arakno_blackHole_wrapper.generateGI", 1)
    cmd.setAttr("arakno_blackHole_wrapper.receiveGI", 0)
    cmd.setAttr("arakno_blackHole_wrapper.generateCaustics", 1)
    cmd.setAttr("arakno_blackHole_wrapper.receiveCaustics", 0)
    cmd.setAttr("arakno_blackHole_wrapper.matteSurface", 1)
    cmd.setAttr("arakno_blackHole_wrapper.alphaContribution", -1)
    cmd.setAttr("arakno_blackHole_wrapper.generateRenderElements", 0)
    cmd.setAttr("arakno_blackHole_wrapper.shadows", 0)
    cmd.setAttr("arakno_blackHole_wrapper.reflectionAmount", 0)
    cmd.setAttr("arakno_blackHole_wrapper.refractionAmount", 0)
    cmd.setAttr("arakno_blackHole_wrapper.affectAlpha", 1)


################################################################################
# @brief      Determines if vray right loaded.
#
# @return     True if vray right loaded, False otherwise.
#
def isVrayRightLoaded():
    if 'vraySettings' in renderSettings.getDefaultNodes():
        return True
    else:
        return False


############################################################################
# @brief      check if vray is set as render engine.
#
def checkVray():
    # per prima cosa attivo il plug-in dal plug-in manager
    plug = "vrayformaya"
    if not cmd.pluginInfo(plug, q=True, l=True):
        return False

    # ora lo imposto come motore di rendering corrente
    if cmd.getAttr("defaultRenderGlobals.currentRenderer") != "vray":
        return False

    return True


################################################################################
# @brief      Creates a vray dislpacement.
#
# @param      elements  The elements
#
def createVrayDislpacement(elements):
    displacement_node = cmd.createNode('VRayDisplacement', name='smoothNode')

    for element in elements:
        cmd.sets(element, addElement=displacement_node)

    cmd.vray('addAttributesFromGroup', displacement_node, 'vray_opensubdiv', True)
    cmd.setAttr('{}.vrayOsdPreserveMapBorders'.format(displacement_node), 0)
    mel.eval('vrayUpdateAE;')


################################################################################
# @brief      assign materialId.
#
# @param      obj    The object
# @param      matId  The matrix identifier
#
def assignMaterialId(obj, matId):
    mel.eval('vray addAttributesFromGroup {} vray_material_id 1;'.format(obj))
    cmd.setAttr('{}.vrayMaterialId'.format(obj), matId)
    cmd.setAttr('{}.vrayColorId'.format(obj), 0, 0, 0)


################################################################################
# @brief      Gets the render element real name.
#
# @param      element  The element
#
# @return     The render element real name.
#
def getRenderElementRealName(element):
    realName = None
    attrs = cmd.listAttr(element)
    for attr in attrs:
        if 'vray_name_' in attr:
            realName = cmd.getAttr('{}.{}'.format(element, attr))

    return realName


################################################################################
# @brief      Gets the rbw denoised elements.
#
def getRBWDefaultDenoisedElements():
    elementsList = []
    lastVersion = getRenderElementsDenoiseSettingsVersion()
    lastVersionConfigFile = os.path.join(renderElementsDenoiseSettingsFolder, 'renderElements_denoiseSettings_v{}.json'.format(str(lastVersion).zfill(3))).replace('\\', '/')

    if os.path.exists(lastVersionConfigFile):
        renderElements = api.json.json_read(lastVersionConfigFile)
        for element in renderElements:
            if renderElements[element] is True:
                elementsList.append(element)
    else:
        elementsList = defaultDenoise

    return elementsList


################################################################################
# @brief      Gets the render elements denoise settings version.
#
# @return     The render elements denoise settings version.
#
def getRenderElementsDenoiseSettingsVersion():
    if os.path.exists(renderElementsDenoiseSettingsFolder):
        files = [file for file in os.listdir(renderElementsDenoiseSettingsFolder) if file.endswith('.json')]

        if files:
            files.sort()
            lastFile = files[-1]
            lastVersion = int(lastFile[:-5].split('_')[-1].split('v')[1])
        else:
            lastVersion = 0
    else:
        lastVersion = 0

    return lastVersion
