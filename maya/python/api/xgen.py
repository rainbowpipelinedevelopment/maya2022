import xgenm as xg
import xgenm.xgGlobal as xgg
import xgenm.XgExternalAPI as xgExAPI
import xgenm.xmaya.xgmExternalAPI as xgmExAPI
import xgenm.xmaya.xgmCurvesToGuidesTool as xgmC2G

import maya.mel as mel
import maya.cmds as cmd

import os
import importlib

import api.log

importlib.reload(api.log)


################################################################################
# @brief      This function helps to know if some code can be executed in an
#             environment where the xgen GUI is available, and a viewport can
#             be updated (for example for refreshing the viewport after some
#             changes in a description).
#
# @return     true if the script is executed inside Maya, false otherwise
#
def inMaya():
    return xgg.Maya


################################################################################
# @brief      Sets the project path.
#
# @param      projectpath  The projectpath
#
def setProjectPath(projectpath):
    projectpath = str(projectpath)
    # Check for the input variable
    if projectpath is None:
        raise ValueError('The project path must be defined.')

    if not os.path.exists(projectpath):
        raise OSError('The path specified does not exists.')

    # Set xgen with the new project
    xgExAPI.setProjectPath(projectpath)
    path = xgExAPI.getProjectPath()

    collections = listCollection()
    for collection in collections:
        xgExAPI.setAttr('xgProjectPath', path, collection)

    if len(collections):
        cmd.file(modified=True)

    os.environ['XGEN_ROOT'] = path

    xgg.FolderTracker.desc().addRoot(path + 'collections')


################################################################################
# @brief      Gets the current project path
#
# @return     the current project path
#
def projectPath():
    return xgExAPI.getProjectPath()


################################################################################
# @brief      Get all the available modules in xgen without distinctions
#             between generators, primitive, renderer etc..
#
# @return     A list of all available modules in xgen
#
def availableModules():
    return xgExAPI.availableModules()


################################################################################
# @brief      Gets the geometry.
#
# @return     The geometry.
#
def getGeom():
    return xgg.GeomTypes()


################################################################################
# @brief      Gets the primitive.
#
# @return     The primitive.
#
def getPrimitive():
    return xgg.PrimitiveTypes()


################################################################################
# @brief      Gets the generator.
#
# @return     The generator.
#
def getGenerator():
    return xgg.GeneratorTypes()


################################################################################
# @brief      Gets the method.
#
# @return     The method.
#
def getMethod():
    return ['Guides', 'Attribute']


################################################################################
# @brief      Gets the fx module.
#
# @return     The fx module.
#
def getFXModule():
    return xgg.FXModuleTypes()


################################################################################
# @brief      Gets the renderer.
#
# @return     The renderer.
#
def getRenderer():
    return xgg.RendererTypes()


################################################################################
# @brief      Returns the collections root path for all the collection in the
#             current scene, relative to the current project
#
# @return     The collection root path
#
def collectionsRootPath():
    return xgmExAPI.paletteRootPath()


################################################################################
# @brief      Returns the path to the current collection if no argument is
#             specified, otherwise returns the path to the specified collection
#
# @param      collection  The collection
#
# @return     The path of the specified collection
#
def collectionPath(collection=None):
    if collection is None:
        collection = currentCollection()

    return xgmExAPI.palettePath(collection)


################################################################################
# @brief      Returns the path of all the available collection in the current
#             project
#
# @return     List of collection folder names
#
def allAvailableCollectionPath():
    return xgmExAPI.paletteFolderNames()


################################################################################
# @brief      Returns the path of the requested description or collection.
#
# @param      description  The description
# @param      collection   The collection
#
# @return     The path in witch the requested description is located
#
def descriptionPath(description=None, collection=None):
    if collection is None:
        collection = currentCollection()

    if description is None:
        description = currentDescription()

    return xgmExAPI.descriptionPath(collection, description)


################################################################################
# @brief      Returns the path for all the description in all the collection in
#             the current project.
#
# @return     The list of all the available description paths
#
def allAvailableDescriptionPath():
    return xgmExAPI.descriptionFolderNames()


################################################################################
# @brief      Returns a list of all the paths in the current xgen configuration
#             for all the descriptions and collections.
#
# @return     The list of all the path used by xgen for collection and
#             description
#
def folderList():
    return xgg.FolderTracker.desc().paths


################################################################################
# @brief      Returns the xgen root folder.
#
# @return     The xgen root folder
#
def folderRoot():
    return xgg.FolderTracker.desc().root


################################################################################
# @brief      Creates a collection.
#
# @param      collection  The collection
#
# @return     The newly created collection
#
def createCollection(collection):
    if collection is None:
        raise ValueError('You must specify a non empty collection name')

    if not isinstance(collection, basestring):
        raise TypeError('The collection name must be a string')

    return xg.createPalette(collection)


################################################################################
# @brief      Creates a description.
#
# @param      description  The description
# @param      collection   The collection
# @param      renderer     The renderer
# @param      primitive    The primitive
# @param      method       The method
# @param      generator    The generator
#
# @return     The newly created description
#
def createDescription(description, collection='collection', renderer='Renderman', primitive='Spline', method='Guides', generator='Random'):
    if description is None:
        raise ValueError('The description name must be defined')

    if not isinstance(description, basestring):
        raise TypeError('The description name must be a string')

    description = xg.createDescription(collection, description, primitive, generator, renderer, method)

    exportCount = xgmExAPI.igExportMaps(description)
    api.log.logger().debug('Count: {}'.format(exportCount))

    xgmExAPI.igSyncMaps(description)

    return description


################################################################################
# @brief      Binds the selected geometry faces to the specified description.
#
# @param      description  The description
# @param      collection   The collection
# @param      mode         The mode
#
# @return     The selected faces that are bound to the description
#
def bindFaces(description=None, collection=None, mode='Append'):
    selection = cmd.ls(selection=True)

    if selection is None:
        raise ValueError('No object selected')

    selectedFaces = cmd.filterExpand(selectionMask=34)  # Filter the faces for the selected object

    if selectedFaces is None:
        raise TypeError('No faces selected')

    cmd.select(clear=True)

    for face in selectedFaces:
        cmd.select(face, add=True)

    xgmExAPI.modifyFaceBinding(collection, description, mode)

    return selectedFaces


################################################################################
# @brief      list the collection
#
# @return     the collection list.
#
def listCollection():
    return xg.palettes()


################################################################################
# @brief      current collection.
#
# @return     the current collection name.
#
def currentCollection():
    return xgg.DescriptionEditor.currentPalette()


################################################################################
# @brief      list all the collection.
#
# @return     the collection list
#
def listAllDescription():
    return xg.descriptions()


################################################################################
# @brief      Return the description that are linked to the specified
#             collection.
#
# @param      collection  The collection
#
# @return     descriptions that are linked to the collection specified
#
def listDescription(collection=None):
    if collection is None:
        collection = currentCollection()

    return xg.descriptions(collection)


################################################################################
# @brief      Returns the guides that are in a description.
#
# @param      description  The description
#
# @return     List of all the guides that are in the description
#
def listDescriptionGuides(description):
    if description is None:
        description = currentDescription()

    return xgmExAPI.descriptionGuides(description)


################################################################################
# @brief      Returns the current description as in the xgen description editor.
#
# @return     The current active description
#
def currentDescription():
    return xgg.DescriptionEditor.currentDescription()


################################################################################
# @brief      Returns the list of the patched binded to a description.
#
# @param      description  The description
#
# @return     A list of all the patches that are binded to the description
#
def listDescriptionPatches(description=None):
    if description is None:
        description = currentDescription()

    return xgmExAPI.descriptionPatches(description)


################################################################################
# @brief      Returns the list of objectso of the specified palette and description
#
# @param      palette      The palette
# @param      description  The description
# @param      activeOnly   The active only
#
# @return     A list of all the objects
#
def listDescriptionObjects(palette, description, activeOnly=True):
    if description is None:
        description = currentDescription()

    return xg.objects(palette, description, activeOnly)


################################################################################
# @brief      list expression under collection.
#
# @param      palette  The palette
#
# @return     the expression list
#
def listExpressions(palette):
    expressions = []
    collectionAttrs = xg.allAttrs(palette)
    for attr in collectionAttrs:
        if attr.startswith('custom'):
            attrNameList = attr.split('_')
            expressions.append('_'.join(attrNameList[2:]))

    return expressions


################################################################################
# @brief      Converts the curves selected in xGen guides.
#
# @param      description  The description
#
# @return     the guides generated
#
def curvesToGuides(description):
    if description is None:
        description = currentDescription()

    xgmC2G.xgmCurvesToGuidesToolCDC(description)

    return listDescriptionGuides(description)


################################################################################
# @brief      Returns the decription that contains the given guide
#
# @param      guide  The guide
#
# @return     the description name
#
def guideToDescription(guide):
    if guide is None:
        raise ValueError('The guide name must be specified')

    if not isinstance(guide, basestring):
        raise TypeError('The guide name must be a string')

    return xgmExAPI.guideDescription(guide)


################################################################################
# @brief      Converts the guides into nurbs curves
#
# @param      description  The description
#
# @return     the converted curves
#
def guidesToCurves(description=None):
    guidestat = 0
    locklength = 1

    group = ''
    if description is not None:
        group = "converted_from_{}".format(description)

    melCommand = "xgmCreateCurvesFromGuidesOption({}, {}, \"{}\")".format(guidestat, locklength, str(group))
    curves = mel.eval(melCommand)

    return curves


################################################################################
# @brief      Sets the alembic cache.
#
# @param      palette      The palette
# @param      description  The description
# @param      cachePath    The cache path
#
def setAlembicCache(palette, description, cachePath):
    de = xgg.DescriptionEditor

    palette = str(palette)
    description = str(description)
    cachePath = str(cachePath)
    xg.setAttr('useCache', 'True', palette, description, 'SplinePrimitive')
    xg.setAttr('liveMode', 'False', palette, description, 'SplinePrimitive')
    xg.setAttr('cacheFileName', cachePath, palette, description, 'SplinePrimitive')

    xg.initInterpolation(palette, description)
    de.refresh("Full")


################################################################################
# @brief      Sets the create mel.
#
# @param      palette      The palette
# @param      description  The description
#
def setCreateMel(palette, description):
    de = xgg.DescriptionEditor
    xgmExAPI.setActive(palette, description, "MelRenderer")
    de.refresh("Full")


################################################################################
# @brief      Sets the mel path.
#
# @param      palette      The palette
# @param      descritpion  The descritpion
# @param      melPath      The mel path
#
def setMelPath(palette, descritpion, melPath):
    de = xgg.DescriptionEditor
    xg.setAttr("outputDir", str(melPath), str(palette), str(descritpion), "MelRenderer")
    de.refresh("Full")


################################################################################
# @brief      Enables the culling.
#
# @param      collection   The collection
# @param      description  The description
#
def enableCulling(collection, description):
    de = xgg.DescriptionEditor
    xg.setAttr('cullFlag', 'true', collection, description, 'RandomGenerator')
    xg.setAttr('cullBackface', 'true', collection, description, 'RandomGenerator')
    xg.setAttr('cullFrustrum', 'true', collection, description, 'RandomGenerator')
    xg.setAttr('cullAngleBF', '5.0', collection, description, 'RandomGenerator')
    xg.setAttr('cullAngleF', '5.0', collection, description, 'RandomGenerator')
    de.refresh("Full")


################################################################################
# @brief      Enables the culling for every description.
#
def enableCullingForEveryDescription():
    xg_palette = listCollection()[0]
    xgen_descriptions = sorted(listDescription(xg_palette))

    for description in xgen_descriptions:
        enableCulling(str(xg_palette), str(description))


################################################################################
# @brief      set half density for the given collection and description
#
# @param      collection   The collection
# @param      description  The description
#
def halveDensity(collection, description):
    de = xgg.DescriptionEditor
    attrs = xg.allAttrs(collection, description, 'RandomGenerator')
    if 'density' in attrs:
        currentValue = float(xg.getAttr('density', collection, description, 'RandomGenerator'))
        xg.setAttr('density', str(currentValue * 0.5), collection, description, 'RandomGenerator')
    de.refresh("Full")


################################################################################
# @brief      set density to half in all description
#
def halveDensityToAllDescriptions():
    xg_palette = listCollection()[0]
    xgen_descriptions = sorted(listDescription(xg_palette))

    for description in xgen_descriptions:
        halveDensity(str(xg_palette), str(description))


################################################################################
# @brief      Sets the preview value.
#
# @param      collection   The collection
# @param      description  The description
# @param      value        The value in float
#
def setPreviewValue(collection, description, value):
    de = xgg.DescriptionEditor
    xg.setAttr('percent', str(value), str(collection), str(description), 'GLRenderer')
    de.refresh("Full")


################################################################################
# @brief      Creates an expression.
#
# @param      name         The name
# @param      value        The value
# @param      collection   The collection
# @param      description  The description
# @param      object       The object
#
def createExpression(name, value, collection, description=None, object=None):
    api.log.logger().debug('name: {}\nvalue: {}\ncollection: {}\ndescription: {}\nobject: {}'.format(name, value, collection, description, object))
    de = xgg.DescriptionEditor
    if description is None and object is None:
        if not xg.attrExists(name, collection):
            xg.addCustomAttr(name, collection)
        else:
            api.log.logger().debug('attribute: {} already exist.\nOverride its value'.format(name))
        xg.setAttr(name, value, collection)
    else:
        if not xg.attrExists(name, collection, description, object):
            xg.addCustomAttr(name, collection, description, object)
        else:
            api.log.logger().debug('attribute: {} already exist.\nOverride its value'.format(name))
        xg.setAttr(name, value, collection, description, object)
    de.refresh("Full")


################################################################################
# @brief      import the collection from the given filename.
#
# @param      filename  The filename
#
def importCollection(filename):
    xg.importPalette(filename, [], "")


################################################################################
# @brief      Gets the scalps.
#
# @return     The scalps.
#
def getScalps():
    scalpMeshes = []

    palettes = api.xgen.listCollection()

    if len(palettes) > 1:
        api.log.logger().error("More than one xgen Palette found, exiting..")
        return scalpMeshes

    elif len(palettes) == 1:
        groomingGroup = cmd.listRelatives(palettes[0], parent=True)[0]

    descriptions = sorted(listDescription(palettes[0]))

    if len(descriptions) > 0:
        for description in descriptions:
            scalp = getScalp(description, groomingGroup)
            if scalp:
                scalpMeshes.append(scalp)

    return scalpMeshes


################################################################################
# @brief      Gets the scalp.
#
# @param      description    The description
# @param      groomingGroup  The grooming group
#
# @return     The scalp.
#
def getScalp(description, groomingGroup):
    patches = []
    for testPatch in cmd.listRelatives(description, children=True, type="transform"):
        if description in testPatch:
            patches.append(testPatch)

    if len(patches) > 0:

        for patch in patches:
            # the name of the grooming msh is in the first children of the xgen description
            groomingMesh = patch.split("_{}".format(description))[0]
            cmd.select(clear=True)
            cmd.select(groomingMesh)
            groomingShape = cmd.pickWalk(direction="down")

            if cmd.listConnections(groomingShape, source=True, destination=False, type="ffd") is None:
                return"|{}|{}".format(groomingGroup, groomingMesh)
