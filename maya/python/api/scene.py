import importlib
import os

import maya.cmds as cmd
import maya.mel as mel

import api.log
import api.matrix
import api.shot

importlib.reload(api.log)
importlib.reload(api.matrix)
importlib.reload(api.shot)


################################################################################
# @brief      get the scene dirname
#
# @return     the dir.
#
def dirname():
    sceneFullName = cmd.file(query=True, sceneName=True)

    return os.path.dirname(sceneFullName)


################################################################################
# @brief      get file name.
#
# @return     filename.
#
def filename():
    return cmd.file(query=True, sceneName=True, shortName=True)


################################################################################
# @brief      get scene name
#
# @return     scenename.
#
def scenename(relative=False):
    path = cmd.file(query=True, sceneName=True)
    return path.split('{}/'.format(os.getenv('MC_FOLDER')))[1] if relative else path


################################################################################
# @brief      get the workspace folder
#
# @return     the workspace.
#
def workspace():
    return cmd.workspace(query=True, fullName=True)


################################################################################
# @brief      get the timeline
#
# @return     the timeline.
#
def getTimeline():
    return mel.eval("$tmpVar=$gPlayBackSlider")


################################################################################
# @brief      clean the bad plugins.
#
def badPluginCleanUp():
    killingList = [
        'TurtleDefaultBakeLayer',
        'TurtleBakeLayerManager',
        'TurtleRenderOptions',
        'TurtleUIOptions',
        'miDefaultFramebuffer',
        'mentalrayGlobals',
        'mentalrayItemsList',
        'miDefaultOptions'
    ]

    for badNode in killingList:
        if cmd.objExists(badNode):
            api.log.logger().debug('node: {} exists'.format(badNode))
            cmd.lockNode(badNode, lock=False)
            try:
                cmd.delete(badNode)
            except:
                pass


################################################################################
# @brief      list all the used materials in scene.
#
# @return     the used material list.
#
def listUsedSceneMaterials():
    sceneMaterials = sorted(set(cmd.ls([mat for item in cmd.ls(type='shadingEngine') for mat in cmd.listConnections(item) if cmd.sets(item, q=True)], materials=True)))

    return sceneMaterials


################################################################################
# @brief      list unknow plugin in scene.
#
# @return     { description_of_the_return_value }
#
def listUnknowPlugin():
    whiteList = ['stereoCamera', 'ngSkinTools']
    list = cmd.unknownPlugin(query=True, list=True)
    if list is None or list == whiteList:
        return list


################################################################################
# @brief      this function delete all unused plugin and unused nodes
#
# @return     None
#
def pluginCleanUp():
    if cmd.unknownPlugin(query=True, list=True) is not None:
        for plugin in cmd.unknownPlugin(query=True, list=True):
            api.log.logger().debug("removing plugin: {}".format(plugin))
            try:
                cmd.unknownPlugin(plugin, remove=True)
            except:
                pass

    for plugin in ['Mayatomr', 'Turtle', 'stereoCamera']:
        try:
            pluginInfo = cmd.pluginInfo(plugin, query=True, dependNode=True)
            api.log.logger().debug('\nplugin {} node are: '.format(plugin))
            api.log.logger().debug(pluginInfo)
            if pluginInfo is not None:
                for nodeTypes in pluginInfo:
                    if cmd.ls(type=nodeTypes) is not None:
                        for nodes in cmd.ls(type=nodeTypes):
                            api.log.logger().debug("deleting node: {}".format(nodes))
                            cmd.lockNode(nodes, lock=False)
                            cmd.delete(nodes)
        except Exception as e:
            api.log.logger().error("Node {} not found in scene\n".format(plugin))
            api.log.logger().error(e)
            continue

    try:
        cmd.select('ngSkinToolsData_*')
        ngSkinNodes = cmd.ls(sl=True)
        cmd.delete(ngSkinNodes)
        api.log.logger().debug("Deleted unwanted ngSkin Nodes: {}".format(ngSkinNodes))
    except:
        pass

    if cmd.ls(type='unknown') is not None:
        for unknown in cmd.ls(type='unknown'):
            api.log.logger().debug("deleting: {}".format(unknown))
            cmd.lockNode(unknown, lock=False)
            cmd.delete(unknown)

    # Get all model editors in Maya and reset the editorChanged event
    for item in cmd.lsUI(editors=True):
        if cmd.modelEditor(item, exists=True):
            cmd.modelEditor(item, edit=True, editorChanged="")

    cmd.flushUndo()


############################################################################
# @brief      plugin check.
#
# @return     { description_of_the_return_value }
#
def pluginCheck():
    import common.pluginCheckUI as pc
    importlib.reload(pc)

    if listUnknowPlugin() is None:
        return

    window = pc.MainWindow()
    window.show()


############################################################################
# @brief      special clean.
#
def specialCleanup():
    # clear the new "selCom" error...
    for _editor in cmd.lsUI(editors=True):
        if not cmd.outlinerEditor(_editor, query=True, exists=True):
            continue
        _sel_cmd = cmd.outlinerEditor(_editor, query=True, selectCommand=True)
        if not _sel_cmd or not _sel_cmd.startswith('<function selCom at '):
            continue
        cmd.outlinerEditor(_editor, edit=True, selectCommand='print("")')


################################################################################
# @brief      Gets the top groups.
#
# @return     The top groups.
#
def getTopGroups():
    top_nodes = cmd.ls(assemblies=True)
    defaultNodes = ["front", "side", "persp", "top", "left", "bottom", "back", 'blendshape', 'IMPORT', 'LIBRARY', 'EXTRA', 'PRX']
    top_groups = [grp for grp in top_nodes if grp not in defaultNodes]
    return top_groups


################################################################################
# @brief      Gets the group name.
#
# @return     The group name.
#
def getGroupName():
    api.log.logger().debug(" === Get group name ===")

    # default top nodes
    default = ["front", "side", "persp", "top", "left", "bottom", "back", 'blendshape', 'IMPORT', 'LIBRARY', 'EXTRA', 'PRX', 'UTILITY']

    # only 1 top group (except from default one)
    all = cmd.ls(assemblies=True)
    # ciclo su un clone della lista
    for it in all[:]:
        if it in default:
            # rimuovo gli elementi di default
            all.remove(it)
    # piu' di un gruppo
    if not all:
        return []
    if len(all) > 1:
        return all
    if all:
        return all[0]


################################################################################
# @brief      Gets all trasform.
#
# @return     All trasform.
#
def getAllTrasform():
    # default top nodes
    default = ["front", "side", "persp", "top", "left", "bottom", "back", 'blendshape', 'IMPORT', 'LIBRARY', 'EXTRA', 'PRX', 'UTILITY']

    all = cmd.ls(assemblies=True)
    # ciclo su un clone della lista
    for it in all[:]:
        if it in default:
            # rimuovo gli elementi di default
            all.remove(it)

    allItems = []
    for i in all:
        items = cmd.listRelatives(i, allDescendents=True, fullPath=True)
        if items:
            for p in items:
                stop = False
                if p.count('blendshape') or p.count('GRP_place3d'):
                    continue
                for lock in ['LOC_eyePivot_L', 'LOC_eyePivot_R']:
                    if p.count(lock):
                        stop = True
                if stop:
                    continue

                if cmd.objectType(p) == 'transform':
                    # escludo dalla lista i locator degli occhi che non sono
                    # riuscito a riprendere i nomi dal dictionaries chiedere a dora..!
                    allItems.append(p)

    api.log.logger().debug('Items: {}'.format(allItems))

    return allItems


################################################################################
# @brief      Gets the vis rm.
#
# @return     The vis rm.
#
def getVisRM():
    allRM = cmd.ls(sl=True)
    api.log.logger().debug("allRM: {}".format(len(allRM)))

    # preparing another list of invisible meshes ONLY (not considering groups) for a final double check
    # to avoid an inconsistency when both meshes and groups are invisible
    meshVisibilityOff = []

    for mesh in allRM:
        if not cmd.getAttr("{}.visibility".format(mesh)):
            meshVisibilityOff.append(mesh)
        else:
            continue

    # check the visibility for the all the hierarchy's groups for every mesh
    visRM = []
    notVisRM = []
    for rm in allRM:
        vis = cmd.getAttr(rm + ".visibility")
        if not vis:
            continue
        else:
            shapeNode = cmd.listRelatives(rm, children=1)
            vis = cmd.getAttr(shapeNode[0] + ".visibility")
            if not vis:
                continue
            else:
                allHierarchy = cmd.listRelatives(rm, p=True, f=True)
                fullHierarchy = allHierarchy[0]
                parentGroupsCount = fullHierarchy.count('|')
                testString = fullHierarchy

                for x in range(parentGroupsCount - 1):
                    if testString:
                        vis = cmd.getAttr(testString + ".visibility")
                        stringList = testString.split('|')
                        testString = '|'.join(stringList[:-1])

                        if not vis:
                            break

                if not vis:
                    if rm not in notVisRM:
                        notVisRM.append(rm)
                    continue
                else:
                    if rm not in visRM:
                        visRM.append(rm)

    # make the double check mentioned above
    notVisFixedlist = list(set(meshVisibilityOff + notVisRM))

    api.log.logger().debug("visRM: {}".format(len(visRM)))
    api.log.logger().debug("notVisRM: {}".format(len(notVisFixedlist)))
    return (visRM, notVisFixedlist)


################################################################################
# @brief      get all the used matirial in scene.
#
# @return     the matirials.
#
def getMaterialsInScene():
    materials = []
    for shadingEngine in cmd.ls(type='shadingEngine'):
        if cmd.sets(shadingEngine, q=True):
            for material in cmd.ls(cmd.listConnections(shadingEngine), materials=True):
                materials.append(material)
    return materials


################################################################################
# @brief      merge function.
#
# @param      scenePath  The scene path
#
# @return     new top group.
#
def mergeAsset(scenePath):
    oldNodes = getTopGroups()
    cmd.file(scenePath, i=True, options="v=0;p=17", ignoreVersion=True, mergeNamespacesOnClash=False, pr=True, loadReferenceDepth="all")
    newNodes = getTopGroups()

    if isinstance(newNodes, list):
        for node in newNodes:
            if node not in oldNodes:
                return node
    else:
        return newNodes


################################################################################
# @brief      Creates a reference.
#
# @param      scenePath  The scene path
# @param      sceneType  The scene type
# @param      namespace  The namespace
#
def createReference(scenePath, sceneType, namespace):
    cmd.namespace(set=':')
    if namespace[0].isdigit():
        namespace = '{}{}'.format(os.getenv('PROJECT'), namespace)
    cmd.file(scenePath, r=True, type=sceneType, namespace=namespace)
    cmd.namespace(set=':')


################################################################################
# @brief      Adds to character group.
#
# @param      path         The path
# @param      namespace    The namespace
# @param      translation  The translation
# @param      rotation     The rotation
# @param      scale        The scale
#
# @return     the new node insert in scene
#
def addToCharacterGroup(path, namespace, translation=[0.0, 0.0, 0.0], rotation=[0.0, 0.0, 0.0], scale=[1.0, 1.0, 1.0]):
    api.log.logger().debug('path: {}\nnamespace: {}'.format(path, namespace))

    if cmd.objExists('CHARACTER'):
        pass
    else:
        cmd.group(empty=True, n='CHARACTER')

    oldNodes = getTopGroups()
    createReference(path, 'mayaBinary', namespace)
    newNodes = getTopGroups()
    for node in newNodes:
        if node not in oldNodes:
            cmd.xform(node, translation=translation)
            cmd.xform(node, rotation=rotation)
            cmd.xform(node, scale=scale)
            cmd.parent(node, 'CHARACTER')
            api.log.logger().debug('Add {} to CHARACTER'.format(node))
            return node


################################################################################
# @brief      Adds to property group.
#
# @param      path         The path
# @param      namespace    The namespace
# @param      translation  The translation
# @param      rotation     The rotation
# @param      scale        The scale
#
# @return     the new node insert in scene
#
def addToPropGroup(path, namespace, translation=[0.0, 0.0, 0.0], rotation=[0.0, 0.0, 0.0], scale=[1.0, 1.0, 1.0]):
    api.log.logger().debug('path: {}\nnamespace: {}'.format(path, namespace))

    if cmd.objExists('PROP'):
        pass
    else:
        cmd.group(empty=True, n='PROP')

    oldNodes = getTopGroups()
    if path.endswith('.mb'):
        createReference(path, 'mayaBinary', namespace)
    else:
        createReference(path, 'mayaAscii', namespace)
    newNodes = getTopGroups()
    for node in newNodes:
        if node not in oldNodes:
            cmd.xform(node, translation=translation)
            cmd.xform(node, rotation=rotation)
            cmd.xform(node, scale=scale)
            cmd.parent(node, 'PROP')
            api.log.logger().debug('Add {} to PROP'.format(node))
            return node


################################################################################
# @brief      Adds to library group.
#
# @param      path         The path
# @param      namespace    The namespace
# @param      translation  The translation
# @param      rotation     The rotation
# @param      scale        The scale
#
# @return     the new node insert in scene
#
def addToLibraryGroup(path, namespace, translation=[0.0, 0.0, 0.0], rotation=[0.0, 0.0, 0.0], scale=[1.0, 1.0, 1.0]):
    api.log.logger().debug('path: {}\nnamespace: {}'.format(path, namespace))

    if not cmd.objExists('LIBRARY'):
        cmd.group(empty=True, n='LIBRARY')

    oldNodes = getTopGroups()
    createReference(path, 'mayaBinary', namespace)
    newNodes = getTopGroups()
    for node in newNodes:
        if node not in oldNodes:
            cmd.xform(node, translation=translation)
            cmd.xform(node, rotation=rotation)
            cmd.xform(node, scale=scale)
            cmd.parent(node, 'LIBRARY')
            api.log.logger().debug('Add {} to LIBRARY'.format(node))
            return node


################################################################################
# @brief      Adds to set group.
#
# @param      path         The path
# @param      translation  The translation
# @param      rotation     The rotation
# @param      scale        The scale
#
# @return     the new node insert in scene
#
def addToSetGroup(path, translation=[0.0, 0.0, 0.0], rotation=[0.0, 0.0, 0.0], scale=[1.0, 1.0, 1.0]):
    api.log.logger().debug('path: {}'.format(path))

    if cmd.objExists(path.split("\\")[-1].replace(".mb", "_SET")):
        api.log.logger().error("There is already a set with this name in this scene, please check.")
        return

    if cmd.objExists('SET'):
        pass
    else:
        cmd.group(empty=True, n='SET')

    oldNodes = getTopGroups()
    cmd.file(path, i=True, type="mayaBinary", ignoreVersion=True, mergeNamespacesOnClash=False, pr=True)
    newNodes = getTopGroups()
    for node in newNodes:
        if node not in oldNodes:
            setItems = cmd.listRelatives(node, allDescendents=True, type='assembly', fullPath=True)
            for setItem in setItems:
                currentMatrix = cmd.xform(setItem, query=True, matrix=True, worldSpace=True)
                transformMatrix = api.matrix.getMatrixFromVectors(translation, rotation, scale)
                newMatrix = api.matrix.multiplyMatrix(currentMatrix, transformMatrix)
                cmd.xform(setItem, matrix=newMatrix)
            cmd.parent(node, 'SET')
            api.log.logger().debug('Add {} to SET'.format(node))
            returnNode = node
    for assem in cmd.ls(type='assembly'):
        if cmd.assembly(assem, query=True, active=True) == 'surfacing':
            cmd.assembly(assem, edit=True, active='cache base')

    return returnNode


################################################################################
# @brief      Adds to effects group.
#
# @param      path         The path
# @param      namespace    The namespace
# @param      translation  The translation
# @param      rotation     The rotation
# @param      scale        The scale
#
# @return     the new node insert in scene
#
def addToFxGroup(path, namespace, translation=[0.0, 0.0, 0.0], rotation=[0.0, 0.0, 0.0], scale=[1.0, 1.0, 1.0]):
    api.log.logger().debug('path: {}\nnamespace: {}'.format(path, namespace))

    if cmd.objExists('FX'):
        pass
    else:
        cmd.group(empty=True, n='FX')

    oldNodes = getTopGroups()
    if path.endswith('.mb'):
        createReference(path, 'mayaBinary', namespace)
    else:
        createReference(path, 'mayaAscii', namespace)
    newNodes = getTopGroups()
    for node in newNodes:
        if node not in oldNodes:
            cmd.xform(node, translation=translation)
            cmd.xform(node, rotation=rotation)
            cmd.xform(node, scale=scale)
            cmd.parent(node, 'FX')
            api.log.logger().debug('Add {} to FX'.format(node))
            return node


################################################################################
# @brief      Adds to set edit group.
#
# @param      path         The path
# @param      translation  The translation
# @param      rotation     The rotation
# @param      scale        The scale
#
# @return     the new node insert in scene
#
def addToSetEditGroup(path, translation=[0.0, 0.0, 0.0], rotation=[0.0, 0.0, 0.0], scale=[1.0, 1.0, 1.0]):
    api.log.logger().debug('path: {}'.format(path))

    if cmd.objExists('SET_EDIT'):
        pass
    else:
        cmd.group(empty=True, n='SET_EDIT')

    oldNodes = getTopGroups()

    filenameWithExt = os.path.basename(path)
    filename = filenameWithExt.split('.')[0]
    filename = filename.split('setEdit_assembly')[0]

    # If the node's name starts with a number put the project's abbreviation in front of it
    if filename[0].isdigit():
        filename = '{}{}'.format(os.getenv('PROJECT'), filename)

    assemblyNode = cmd.assembly(name=filename, type='assemblyReference')
    cmd.setAttr('{}.definition'.format(assemblyNode), path, type='string')

    newNodes = getTopGroups()
    for node in newNodes:
        if node not in oldNodes:
            cmd.xform(node, translation=translation)
            cmd.xform(node, rotation=rotation)
            cmd.xform(node, scale=scale)
            cmd.parent(node, 'SET_EDIT')
            api.log.logger().debug('Add {} to SET_EDIT'.format(node))
            return node


################################################################################
# @brief      Adds to set edit group.
#
# @param      path         The path
# @param      translation  The translation
# @param      rotation     The rotation
# @param      scale        The scale
#
# @return     the new node insert in scene
#
def addToAddItemsGroup(path, translation=[0.0, 0.0, 0.0], rotation=[0.0, 0.0, 0.0], scale=[1.0, 1.0, 1.0]):
    api.log.logger().debug('path: {}'.format(path))

    if cmd.objExists('ADD_ITEMS'):
        pass
    else:
        cmd.group(empty=True, n='ADD_ITEMS')

    oldNodes = getTopGroups()

    filenameWithExt = os.path.basename(path)
    filename = filenameWithExt.split('.')[0]
    filename = filename.split('setEdit_assembly')[0]

    # If the node's name starts with a number put the project's abbreviation in front of it
    if filename[0].isdigit():
        filename = '{}{}'.format(os.getenv('PROJECT'), filename)

    assemblyNode = cmd.assembly(name=filename, type='assemblyReference')
    cmd.setAttr('{}.definition'.format(assemblyNode), path, type='string')

    newNodes = getTopGroups()
    for node in newNodes:
        if node not in oldNodes:
            cmd.xform(node, translation=translation)
            cmd.xform(node, rotation=rotation)
            cmd.xform(node, scale=scale)
            cmd.parent(node, 'ADD_ITEMS')
            api.log.logger().debug('Add {} to ADD_ITEMS'.format(node))
            return node


################################################################################
# @brief      Adds to import group.
#
# @param      path         The path
# @param      namespace    The namespace
# @param      translation  The translation
# @param      rotation     The rotation
# @param      scale        The scale
#
# @return     the new node insert in scene
#
def addToImportGroup(path, namespace, translation=[0.0, 0.0, 0.0], rotation=[0.0, 0.0, 0.0], scale=[1.0, 1.0, 1.0]):
    api.log.logger().debug('path: {}\nnamespace: {}'.format(path, namespace))

    if not cmd.objExists('IMPORT'):
        cmd.group(empty=True, n='IMPORT')

    oldNodes = getTopGroups()
    createReference(path, 'mayaBinary', namespace)
    newNodes = getTopGroups()
    for node in newNodes:
        if node not in oldNodes:
            cmd.xform(node, translation=translation)
            cmd.xform(node, rotation=rotation)
            cmd.xform(node, scale=scale)
            cmd.parent(node, 'IMPORT')
            api.log.logger().debug('Add {} to IMPORT'.format(node))
            return node


################################################################################
# @brief      Adds to extra group.
#
# @param      path         The path
# @param      namespace    The namespace
# @param      translation  The translation
# @param      rotation     The rotation
# @param      scale        The scale
#
# @return     the new node insert in scene
#
def addToExtraGroup(path, namespace, translation=[0.0, 0.0, 0.0], rotation=[0.0, 0.0, 0.0], scale=[1.0, 1.0, 1.0]):
    api.log.logger().debug('path: {}\nnamespace: {}'.format(path, namespace))

    if not cmd.objExists('EXTRA'):
        cmd.group(empty=True, n='EXTRA')

    oldNodes = getTopGroups()
    createReference(path, 'mayaBinary', namespace)
    newNodes = getTopGroups()
    for node in newNodes:
        if node not in oldNodes:
            cmd.xform(node, translation=translation)
            cmd.xform(node, rotation=rotation)
            cmd.xform(node, scale=scale)
            cmd.parent(node, 'EXTRA')
            api.log.logger().debug('Add {} to EXTRA'.format(node))
            return node


################################################################################
# @brief      Adds to lights group.
#
# @param      path         The path
# @param      namespace    The namespace
# @param      translation  The translation
# @param      rotation     The rotation
# @param      scale        The scale
#
# @return     the node insert in scene
#
def addToLightsGroup(path, namespace, translation=[0.0, 0.0, 0.0], rotation=[0.0, 0.0, 0.0], scale=[1.0, 1.0, 1.0]):
    api.log.logger().debug('path: {}\nnamespace: {}'.format(path, namespace))

    if not cmd.objExists('LIGHTS'):
        cmd.group(empty=True, n='LIGHTS')

    oldNodes = getTopGroups()
    createReference(path, 'mayaBinary', namespace)
    newNodes = getTopGroups()
    for node in newNodes:
        if node not in oldNodes:
            cmd.xform(node, translation=translation)
            cmd.xform(node, rotation=rotation)
            cmd.xform(node, scale=scale)
            cmd.parent(node, 'LIGHTS')
            api.log.logger().debug('Add {} to LIGHTS'.format(node))
            return node


################################################################################
# @brief      import in extra.
#
# @param      path         The path
# @param      translation  The translation
# @param      rotation     The rotation
# @param      scale        The scale
#
# @return     the imported top group
#
def importInExtra(path, translation=[0.0, 0.0, 0.0], rotation=[0.0, 0.0, 0.0], scale=[1.0, 1.0, 1.0]):
    api.log.logger().debug('path: {}'.format(path))

    if not cmd.objExists('EXTRA'):
        cmd.group(empty=True, n='EXTRA')

    oldNodes = getTopGroups()
    cmd.file(path, i=True, type="mayaAscii", ignoreVersion=True, mergeNamespacesOnClash=False, pr=True)
    newNodes = getTopGroups()
    for node in newNodes:
        if node not in oldNodes:
            cmd.xform(node, translation=translation)
            cmd.xform(node, rotation=rotation)
            cmd.xform(node, scale=scale)
            cmd.parent(node, 'EXTRA')
            api.log.logger().debug('Add {} to EXTRA'.format(node))
            return node


################################################################################
# @brief      deleted the given reference.
#
# @param      referenceNode  The reference node
#
def deleteReference(referenceNode):
    referencePath = cmd.referenceQuery(referenceNode, filename=True)
    cmd.file(referencePath, removeReference=True, force=True)


################################################################################
# @brief      Gets the save nodes.
#
# @return     The save nodes.
#
def getSaveNodes(dept=None):
    saveNodes = []
    topGroups = []
    topGroups = topGroups + (cmd.listRelatives('CHARACTER') if cmd.objExists('CHARACTER') and cmd.listRelatives('CHARACTER') else [])
    topGroups = topGroups + (cmd.listRelatives('PROP') if cmd.objExists('PROP') and cmd.listRelatives('PROP') else [])
    topGroups = topGroups + (cmd.listRelatives('LIBRARY') if cmd.objExists('LIBRARY') and cmd.listRelatives('LIBRARY') else [])
    topGroups = topGroups + (cmd.listRelatives('SET') if cmd.objExists('SET') and cmd.listRelatives('SET') else [])
    topGroups = topGroups + (cmd.listRelatives('FX') if cmd.objExists('FX') and cmd.listRelatives('FX') else [])
    topGroups = topGroups + (cmd.listRelatives('LIGHTS') if cmd.objExists('LIGHTS') and cmd.listRelatives('LIGHTS') else [])

    for topGroup in topGroups:
        api.log.logger().debug(topGroup)

        try:
            saveNode = cmd.listConnections(topGroup, destination=False, source=True, type='saveNode')[0]
            if not dept:
                saveNodes.append(saveNode)
            else:
                saveNode
                if cmd.getAttr('{}.dept'.format(saveNode)) == dept:
                    saveNodes.append(saveNode)
        except TypeError:
            pass

    return saveNodes


################################################################################
# @brief      Gets the shot by scene.
#
# @return     The shot by scene.
#
def getShotByScene():
    shotInfo = api.scene.scenename(relative=True).split('/')

    try:
        openShot = api.shot.Shot({
            'season': shotInfo[2],
            'episode': shotInfo[3],
            'sequence': shotInfo[4],
            'shot': shotInfo[5]
        })

        return openShot
    except api.exception.InvalidShot:
        return None
