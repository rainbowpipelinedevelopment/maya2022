import importlib
from datetime import datetime
import time
import os

import maya.cmds as cmd

from config.dictionaries import finishingFolders
import api.system
import api.shot
import api.saveNode
import api.json
import api.exception
import common.cleanupMayaScriptJobs as cleanupMayaScriptJobs

importlib.reload(api.system)
# importlib.reload(api.shot)
importlib.reload(api.saveNode)
importlib.reload(api.json)
importlib.reload(api.exception)
importlib.reload(cleanupMayaScriptJobs)


################################################################################
# @brief      This class describes a saved shot.
#
class SavedShot(object):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      params          Params
    #
    def __init__(self, params):

        super(SavedShot, self).__init__()

        if 'path' in params:
            savedShotParams = self.createFromParam(path=params['path'])

        elif 'db_id' in params:
            savedShotParams = self.createFromParam(id=params['db_id'])

        else:
            savedShotParams = params.copy()
            self.generateDbId()

            if 'deptType' not in savedShotParams:
                savedShotParams['deptType'] = ''

            if 'itemName' not in savedShotParams:
                savedShotParams['itemName'] = ''

            if 'promoted' not in savedShotParams:
                savedShotParams['promoted'] = 0

        if savedShotParams:
            for label in savedShotParams:

                name = label
                value = savedShotParams[label]
                setattr(self, name, value)
                self.softwareVersion = 'maya{}'.format(cmd.about(v=True))
        else:
            raise api.exception.InvalidSavedShot('The saved shot params are wrong or maybe the save doen\'t exists')

    ############################################################################
    # @brief      Returns a dictionary representation of the object.
    #
    # @return     Dictionary representation of the object.
    #
    def toDict(self):
        return self.__dict__

    ############################################################################
    # @brief      Creates a from parameter.
    #
    # @param      path  The path
    # @param      id    The identifier
    #
    # @return     the creation params
    #
    def createFromParam(self, path=None, id=None):
        savedShotParams = {}

        if path:
            savedShot = self.getDataFromPath(path)
        else:
            savedShot = self.getDataFromId(id)

        if savedShot:

            savedShotParams['db_id'] = savedShot[0]
            savedShotParams['dept'] = savedShot[1]
            savedShotParams['path'] = savedShot[2]
            savedShotParams['version'] = savedShot[3]
            savedShotParams['deptType'] = savedShot[4]
            savedShotParams['date'] = savedShot[8].strftime("%Y.%m.%d-%H.%M.%S")
            savedShotParams['note'] = savedShot[9]
            savedShotParams['itemName'] = savedShot[10]
            savedShotParams['promoted'] = savedShot[11]

            try:
                savedShotParams['shot'] = api.shot.Shot({'id': savedShot[5]})
            except api.exception.InvalidAsset():
                savedShotParams['shot'] = None

        return savedShotParams

    ############################################################################
    # @brief      Gets the data from identifier.
    #
    # @param      id    The identifier
    #
    # @return     The data from identifier.
    #
    def getDataFromId(self, id):
        savedShotQuery = "SELECT * FROM `savedShot` WHERE `id` = '{}'".format(id)
        savedShot = api.database.selectSingleQuery(savedShotQuery)

        return savedShot

    ############################################################################
    # @brief      Gets the data from path.
    #
    # @param      path  The path
    #
    # @return     The data from path.
    #
    def getDataFromPath(self, path):
        savedShotQuery = "SELECT * FROM `savedShot` WHERE `path` LIKE '%{}%'".format(path)
        savedShot = api.database.selectSingleQuery(savedShotQuery)

        return savedShot

    ############################################################################
    # @brief      Gets the available versions.
    #
    # @return     The available versions.
    #
    def getAvailableVersions(self):
        versionsQuery = "SELECT * FROM `savedShot` WHERE `shotId` = '{}' AND `dept` = '{}' AND `deptType` = '{}' AND `itemName` = '{}' AND `softwareVersion` = '{}' AND `system`='RBW' ORDER BY `date` DESC".format(
            self.shot.shotId,
            self.dept,
            self.deptType,
            self.itemName,
            self.softwareVersion)

        versions = api.database.selectQuery(versionsQuery)

        return versions

    ############################################################################
    # @brief      Gets the latest.
    #
    # @return     The latest.
    #
    def getLatest(self):

        versions = self.getAvailableVersions()

        if versions:
            return versions[0]
        else:
            return None

    ############################################################################
    # @brief      increment the saved asset version.
    #
    def incrementVersion(self):
        latest = self.getLatest()
        if latest:
            versionString = latest[3]

            versionNumber = int(versionString.split('r')[1])

            self.version = 'vr{}'.format(str(versionNumber + 1).zfill(3))
        else:
            self.version = 'vr001'

    ############################################################################
    # @brief      generate a new temporal id for DB.
    #
    def generateDbId(self):
        dateVar = datetime.fromtimestamp(time.time())
        self.date = dateVar.strftime("%Y.%m.%d-%H.%M.%S")
        utc_time = time.mktime(dateVar.timetuple())
        self.db_id = '{}{}r'.format(int(utc_time), str(api.system.getUserId()).zfill(3))

    ############################################################################
    # @brief      Gets the dept object.
    #
    # @return     The dept object.
    #
    def getDeptObject(self):
        import depts.animation.sc_animation as sc_animation
        import depts.camera.sc_camera as sc_camera
        import depts.fx.sc_fx as sc_fx
        import depts.finishing.sc_finishing as sc_finishing
        import depts.environment.sc_environment as sc_environment
        import depts.lighting.sc_lighting as sc_lighting

        importlib.reload(sc_animation)
        importlib.reload(sc_camera)
        importlib.reload(sc_fx)
        importlib.reload(sc_finishing)
        importlib.reload(sc_environment)
        importlib.reload(sc_lighting)

        initDict = {
            'Lay': sc_animation.Layout(savedShot=self),
            'Pri': sc_animation.Primary(savedShot=self),
            'Sec': sc_animation.Secondary(savedShot=self),
            'Cam': sc_camera.Camera(savedShot=self),
            'Fx': sc_fx.Fx(savedShot=self),
            'Fin': sc_finishing.Finishing(savedShot=self),
            'Env': sc_environment.Environment(savedShot=self),
            'Lgt': sc_lighting.Lighting(savedShot=self),
        }

        return initDict[self.dept]

    ############################################################################
    # @brief      Saves on db.
    #
    def saveOnDB(self):
        self.relPath = os.path.join(self.savedShotFolder.split('mc_{}/'.format(os.getenv('PROJECT')))[1], '{}.ma'.format(self.sceneName)).replace('\\', '/')
        args = (
            self.db_id,
            self.dept,
            self.relPath,
            self.version,
            self.deptType,
            self.shot.shotId,
            'RBW',
            os.getenv('USERNAME'),
            self.date,
            self.note,
            self.itemName,
            self.promoted,
            1,
            self.shot.id_mv,
            self.softwareVersion
        )

        fields = 'id, dept, path, version, deptType, shotId, system, user, date, note, itemName, promoted, visible, id_mv, softwareVersion'

        pers = ", ".join(["\'{}\'".format(o) for o in args])

        dbName = 'savedShot'

        query = 'INSERT INTO %s (%s) VALUES (%s) ' % (dbName, fields, pers)

        api.log.logger().debug(query)

        api.database.insertQuery(query)

    ############################################################################
    # @brief      save function.
    #
    # @param      params  The parameters
    #
    # @return     { description_of_the_return_value }
    #
    def save(self):
        self.generateDbId()

        # get all infoes
        if self.dept != 'Cam':
            self.incrementVersion()

        self.savedShotDeptFolder = os.path.join(
            self.shot.getShotFolder(),
            self.dept
        ).replace('\\', '/')

        if self.deptType and self.itemName:
            self.savedShotFolder = os.path.join(self.savedShotDeptFolder, finishingFolders[self.deptType], self.itemName, self.version)
            self.sceneName = '{}_{}_{}_{}_{}_Anim_{}_{}_{}_RBW'.format(self.shot.projectName.upper(), self.shot.season, self.shot.episode, self.shot.sequence, self.shot.shot, self.dept, self.itemName, self.version)

        elif self.itemName:
            if self.dept == 'Env' and self.itemName == 'source':
                self.savedShotFolder = os.path.join(self.savedShotDeptFolder, self.version)
                self.sceneName = '{}_{}_{}_{}_{}_Anim_{}_{}_RBW'.format(self.shot.projectName.upper(), self.shot.season, self.shot.episode, self.shot.sequence, self.shot.shot, self.dept, self.version)
            else:
                self.savedShotFolder = os.path.join(self.savedShotDeptFolder, self.itemName, self.version)
                self.sceneName = '{}_{}_{}_{}_{}_Anim_{}_{}_{}_RBW'.format(self.shot.projectName.upper(), self.shot.season, self.shot.episode, self.shot.sequence, self.shot.shot, self.dept, self.itemName, self.version)

        elif self.deptType:
            self.savedShotFolder = os.path.join(self.savedShotDeptFolder, finishingFolders[self.deptType], self.version)
            self.sceneName = '{}_{}_{}_{}_{}_Anim_{}_{}_RBW'.format(self.shot.projectName.upper(), self.shot.season, self.shot.episode, self.shot.sequence, self.shot.shot, self.dept, self.version)

        else:
            self.savedShotFolder = os.path.join(self.savedShotDeptFolder, self.version)
            self.sceneName = '{}_{}_{}_{}_{}_Anim_{}_{}_RBW'.format(self.shot.projectName.upper(), self.shot.season, self.shot.episode, self.shot.sequence, self.shot.shot, self.dept, self.version)

        if not os.path.exists(self.savedShotFolder):
            os.makedirs(self.savedShotFolder)

        # get saver
        self.deptObject = self.getDeptObject()

        # save
        if self.deptObject.save():
            self.saveOnDB()
            return True
        else:
            return False

    ############################################################################
    # @brief      load function
    #
    # @param      mode  The mode (load, merge, reference)
    #
    def load(self, mode='load', confirm=True):
        self.deptObject = self.getDeptObject()
        self.deptObject.load(mode, confirm=confirm)

        cleanApp = cleanupMayaScriptJobs.cleanupMayaScriptJobs()
        cleanApp.delete_worm_nodes()
        api.log.logger().debug("..eventual cleanup for worms is done...")

    def getShotObj(self):
        return self.shot

    ############################################################################
    # @brief      Gets the season.
    #
    # @return     The season.
    #
    def getSeason(self):
        return self.shot.season

    ############################################################################
    # @brief      Gets the episode.
    #
    # @return     The episode.
    #
    def getEpisode(self):
        return self.shot.episode

    ############################################################################
    # @brief      Gets the sequence.
    #
    # @return     The sequence.
    #
    def getSequence(self):
        return self.shot.sequence

    ############################################################################
    # @brief      Gets the shot.
    #
    # @return     The shot.
    #
    def getShot(self):
        return self.shot.shot

    ############################################################################
    # @brief      Gets the season identifier sg.
    #
    # @return     The season identifier sg.
    #
    def getSeasonIdSg(self):
        return self.shot.seasonIdSg

    ############################################################################
    # @brief      Gets the episode identifier sg.
    #
    # @return     The episode identifier sg.
    #
    def getEpisodeIdSg(self):
        return self.shot.episodeIdSg

    ############################################################################
    # @brief      Gets the sequence identifier sg.
    #
    # @return     The sequence identifier sg.
    #
    def getSequenceIdSg(self):
        return self.shot.sequenceIdSg

    ############################################################################
    # @brief      Gets the shot identifier sg.
    #
    # @return     The shot identifier sg.
    #
    def getShotIdSg(self):
        return self.shot.shotIdSg

    ############################################################################
    # @brief      Gets the season identifier.
    #
    # @return     The season identifier.
    #
    def getSeasonId(self):
        return self.shot.seasonId

    ############################################################################
    # @brief      Gets the episode identifier.
    #
    # @return     The episode identifier .
    #
    def getEpisodeId(self):
        return self.shot.episodeId

    ############################################################################
    # @brief      Gets the sequence identifier.
    #
    # @return     The sequence identifier.
    #
    def getSequenceId(self):
        return self.shot.sequenceId

    ############################################################################
    # @brief      Gets the shot identifier.
    #
    # @return     The shot identifier.
    #
    def getShotId(self):
        return self.shot.shotId
