import importlib
import os
from datetime import datetime
import time

import maya.cmds as cmd

import api.database
import api.system
import api.log
import api.exception
import api.asset
import api.scene

# importlib.reload(api.database)
importlib.reload(api.system)
importlib.reload(api.log)
importlib.reload(api.exception)
# importlib.reload(api.asset)
importlib.reload(api.scene)

################################################################################
# @brief      This class describes an assembly.
#
class Assembly(object):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      params  The parameters
    # @param      node    The node
    #
    def __init__(self, params=None, node=None):
        super(Assembly, self).__init__()

        self.id_mv = os.getenv('ID_MV')
        self.mcFolder = os.getenv('MC_FOLDER').replace('\\', '/')
        self.node = self.definitionPath = ''

        if node:
            self.node = node
            definitionPath = cmd.getAttr('{}.definition'.format(self.node))

            if self.mcFolder in definitionPath:
                self.definitionPath = definitionPath.split(self.mcFolder)[1][1:]
            else:
                self.definitionPath = definitionPath

            assemblyParams = self.createFromParam(path=self.definitionPath)

        elif 'path' in params.keys():
            assemblyParams = self.createFromParam(path=params['path'])
        elif 'db_id' in params.keys():
            assemblyParams = self.createFromParam(id=params['db_id'])
        else:
            assemblyParams = params.copy()
            dateVar = datetime.fromtimestamp(time.time())
            assemblyParams['date'] = dateVar.strftime("%Y.%m.%d-%H.%M.%S")
            utc_time = time.mktime(dateVar.timetuple())
            assemblyParams['db_id'] = '{}{}r'.format(int(utc_time), str(api.system.getUserId()).zfill(3))

            if 'surfacingId' not in assemblyParams.keys():
                assemblyParams['surfacingId'] = ''

            if 'modHigId' not in assemblyParams.keys():
                assemblyParams['modHigId'] = ''

            if 'modPrxId' not in assemblyParams.keys():
                assemblyParams['modPrxId'] = ''

            if 'surfTxtId' not in assemblyParams.keys():
                assemblyParams['surfTxtId'] = ''

        if assemblyParams:
            for label in assemblyParams:

                name = label
                value = assemblyParams[label]
                setattr(self, name, value)
        else:
            raise api.exception.InvalidAssembly('The assembly is wrong or maybe it has been deleted')

    ############################################################################
    # @brief      Creates from parameters.
    #
    # @param      path  The path
    # @param      id    The identifier
    #
    # @return     { description_of_the_return_value }
    #
    def createFromParam(self, path=None, id=None):
        assemblyParams = {}

        if path:
            assembly = self.getDataFromPath(path)
        else:
            assembly = self.getDataFromId(id)

        if assembly:

            assemblyParams['db_id'] = assembly[0]
            assemblyParams['definitionPath'] = assembly[1]
            assemblyParams['version'] = assembly[2]
            assemblyParams['date'] = assembly[3]
            assemblyParams['surfacingId'] = assembly[5]
            assemblyParams['modHigId'] = assembly[6]
            assemblyParams['modPrxId'] = assembly[7]
            assemblyParams['surfTxtId'] = assembly[8]

        try:
            assemblyParams['asset'] = api.asset.Asset({'id': assembly[4]})
        except Exception as e:
            api.log.logger().debug(e)
            assemblyParams['asset'] = None

        return assemblyParams

    ############################################################################
    # @brief      Gets the data from identifier.
    #
    # @param      id    The identifier
    #
    # @return     The data from identifier.
    #
    def getDataFromId(self, id):
        assemblyQuery = "SELECT * FROM `assembly` WHERE `id` = '{}'".format(id)
        assembly = api.database.selectSingleQuery(assemblyQuery)

        return assembly

    ############################################################################
    # @brief      Gets the data from path.
    #
    # @param      path  The path
    #
    # @return     The data from path.
    #
    def getDataFromPath(self, path):
        assemblyQuery = "SELECT * FROM `assembly` WHERE `path` LIKE '%{}%'".format(path)
        assembly = api.database.selectSingleQuery(assemblyQuery)

        return assembly

    ############################################################################
    # @brief      Creates a definition node.
    #
    # @return     the definition node.
    #
    def createDefinitionNode(self):
        definitionName = '{}_{}_{}_{}_setEdit_'.format(self.asset.category, self.asset.group, self.asset.name, self.asset.variant)
        api.log.logger().debug('definitionName: {}'.format(definitionName))
        self.node = cmd.assembly(name=definitionName)

    ################################################################################
    # @brief      Gets the definition.
    #
    # @return     The definition.
    #
    def getDefinition(self):
        return self.definitionPath

    ############################################################################
    # @brief      Gets the representation list.
    #
    # @return     The representation list.
    #
    def getRepresentationList(self):
        resultDict = {}
        representationCount = (cmd.attributeQuery('representations', node=self.node, numberOfChildren=True)[0] + 2)
        for index in range(0, representationCount):
            reprName = cmd.getAttr('{}.representations[{}].repName'.format(self.node, index))
            reprData = cmd.getAttr('{}.representations[{}].repData'.format(self.node, index))
            reprType = cmd.getAttr('{}.representations[{}].repType'.format(self.node, index))
            reprLabel = cmd.getAttr('{}.representations[{}].repLabel'.format(self.node, index))

            resultDict[reprName] = {
                'label': reprLabel,
                'data': reprData,
                'type': reprType
            }

        return resultDict

    ############################################################################
    # @brief      Gets the representation path from identifier.
    #
    # @param      attr  The attribute
    #
    # @return     The representation path from identifier.
    #
    def getRepresentationPathFromId(self, attr):
        savedAssetId = getattr(self, attr)
        pathQuery = "SELECT `path` FROM `savedAsset` WHERE `id` = '{}' AND `visible`=1".format(savedAssetId)
        return api.database.selectSingleQuery(pathQuery)[0]

    ############################################################################
    # @brief      Creates a cache representation.
    #
    # @param      name  The name
    # @param      path  The path
    #
    def createCacheRepresentation(self, name='cache base', path=None):
        cmd.assembly(self.node, edit=True, createRepresentation='Cache', input=path, repName=name)

    ############################################################################
    # @brief      Creates a scene representation.
    #
    # @param      name  The name
    # @param      path  The path
    #
    def createSceneRepresentation(self, name='surfacing', path=None):
        cmd.assembly(self.node, edit=True, createRepresentation='Scene', input=path, repName=name)

    ############################################################################
    # @brief      populate the assembly node.
    #
    def populateAssemby(self):
        import api.savedAsset
        # importlib.reload(api.savedAsset)

        try:
            if not self.modHigId and not self.definitionPath:
                higSavedAssetParam = {
                    'asset': self.asset,
                    'dept': 'modeling',
                    'deptType': 'hig',
                    'approvation': 'def'
                }

                modHig = api.savedAsset.SavedAsset(params=higSavedAssetParam)
                self.modHigId = modHig.getLatest()[0]
                modHig = api.savedAsset.SavedAsset(params={'db_id': self.modHigId, 'approvation': 'def'})
            else:
                if self.modHigId:
                    modHig = api.savedAsset.SavedAsset(params={'db_id': self.modHigId, 'approvation': 'def'})
                else:
                    raise api.exception.InvalidSavedAsset

            self.createCacheRepresentation(name='cache base', path=modHig.path.replace('.mb', '_base.abc'))
            if self.asset.category != 'set':
                self.createCacheRepresentation(name='cache smooth', path=modHig.path.replace('.mb', '_smooth.abc'))
        except (api.exception.InvalidSavedAsset, TypeError):
            self.modHigId = ''

        try:
            if not self.modPrxId and not self.definitionPath:
                prxSavedAssetParam = {
                    'asset': self.asset,
                    'dept': 'modeling',
                    'deptType': 'prx',
                    'approvation': 'def'
                }

                modPrx = api.savedAsset.SavedAsset(params=prxSavedAssetParam)
                self.modPrxId = modPrx.getLatest()[0]
                modPrx = api.savedAsset.SavedAsset(params={'db_id': self.modPrxId, 'approvation': 'def'})
            else:
                if self.modPrxId:
                    modPrx = api.savedAsset.SavedAsset(params={'db_id': self.modPrxId, 'approvation': 'def'})
                else:
                    raise api.exception.InvalidSavedAsset

            self.createCacheRepresentation(name='cache proxy', path=modPrx.path.replace('.mb', '_base.abc'))
        except (api.exception.InvalidSavedAsset, TypeError):
            self.modPrxId = ''

        try:
            if not self.surfacingId and not self.definitionPath:
                surfSavedAssetParam = {
                    'asset': self.asset,
                    'dept': 'surfacing',
                    'deptType': 'rnd',
                    'approvation': 'def'
                }

                surfacing = api.savedAsset.SavedAsset(params=surfSavedAssetParam)
                self.surfacingId = surfacing.getLatest()[0]
                surfacing = api.savedAsset.SavedAsset(params={'db_id': self.surfacingId, 'approvation': 'def'})
            else:
                if self.surfacingId:
                    surfacing = api.savedAsset.SavedAsset(params={'db_id': self.surfacingId, 'approvation': 'def'})
                else:
                    raise api.exception.InvalidSavedAsset

            self.createSceneRepresentation(name='surfacing', path=surfacing.path)
            self.createSceneRepresentation(name='vrscene', path=surfacing.path.replace('.mb', '_vrscene.ma'))
            self.createSceneRepresentation(name='vrmesh', path=surfacing.path.replace('.mb', '_vrmesh.ma'))
        except (api.exception.InvalidSavedAsset, TypeError):
            self.surfacingId = ''

        try:
            if not self.surfTxtId and not self.definitionPath:
                rigSavedAssetParam = {
                    'asset': self.asset,
                    'dept': 'surfacing',
                    'deptType': 'txt',
                    'approvation': 'def'
                }

                surf = api.savedAsset.SavedAsset(params=rigSavedAssetParam)
                self.surfTxtId = surf.getLatest()[0]
                surfTxt = api.savedAsset.SavedAsset(params={'db_id': self.surfTxtId, 'approvation': 'def'})
            else:
                if self.surfTxtId:
                    surfTxt = api.savedAsset.SavedAsset(params={'db_id': self.surfTxtId, 'approvation': 'def'})
                else:
                    raise api.exception.InvalidSavedAsset

            self.createSceneRepresentation(name='animText', path=surfTxt.path)
        except (api.exception.InvalidSavedAsset, TypeError):
            self.surfTxtId = ''

    ################################################################################
    # @brief      Gets the assembly version.
    #
    # @return     The render version.
    #
    def getAssemblyVersion(self):
        return self.version

    ################################################################################
    # @brief      Switch the current active representation to the new specified one.
    #
    # @return     The new representation name
    #
    def switchRepresentation(self, representationName='cache base'):
        if representationName in self.getRepresentationList():
            cmd.assembly(self.node, edit=True, active=representationName)

    ################################################################################
    # @brief      get the current active representation name.
    #
    def getActiveRepresentationName(self):
        return cmd.assembly(self.node, query=True, active=True)

    ################################################################################
    # @brief      get the current active representation label.
    #
    def getActiveRepresentationLabel(self):
        return cmd.assembly(self.node, query=True, activeLabel=True)

    ############################################################################
    # @brief      create a new scene assembly
    #
    def create(self):
        cmd.file(newFile=True, force=True)
        self.createDefinitionNode()
        self.populateAssemby()

    ############################################################################
    # @brief      Gets the available versions.
    #
    # @return     The available versions.
    #
    def getAvailableVersions(self):
        versionsQuery = "SELECT * FROM `assembly` WHERE `assetId` = '{}' ORDER BY `date` DESC".format(
            self.asset.variantId
        )

        versions = api.database.selectQuery(versionsQuery)

        return versions

    ############################################################################
    # @brief      Gets the latest.
    #
    # @return     The latest.
    #
    def getLatest(self):
        versions = self.getAvailableVersions()

        if versions:
            return versions[0]
        else:
            return None

    ############################################################################
    # @brief      increment the saved asset version.
    #
    def incrementVersion(self):
        latest = self.getLatest()
        if latest:
            versionString = latest[2]

            versionNumber = int(versionString.split('r')[1])

            self.version = 'vr{}'.format(str(versionNumber + 1).zfill(3))
        else:
            self.version = 'vr001'

    ############################################################################
    # @brief      Saves on db.
    #
    def saveOnDB(self):
        args = (
            self.db_id,
            self.definitionPath,
            self.version,
            self.date,
            self.asset.variantId,
            self.surfacingId,
            self.modHigId,
            self.modPrxId,
            self.surfTxtId,
            self.id_mv
        )

        fields = 'id, path, version, date, assetId, surfacingId, modHigId, modPrxId, surfTxtId, id_mv'

        pers = ", ".join(["\'{}\'".format(o) for o in args])

        dbName = 'assembly'

        query = 'INSERT INTO %s (%s) VALUES (%s) ' % (dbName, fields, pers)

        api.log.logger().debug(query)

        api.database.insertQuery(query)

        self.saveDict = {
            'id': self.db_id,
            'path': self.definitionPath,
            'version': self.version,
            'date': self.date,
            'assetId': self.asset.variantId,
            'surfacingId': self.surfacingId,
            'modHigId': self.modHigId,
            'modPrxId': self.modPrxId,
            'surfTxtId': self.surfTxtId,
            'id_mv': self.id_mv
        }

    ############################################################################
    # @brief      rename the scene and then save.
    #
    # @param      path    The path
    # @param      format  The format
    #
    def renameAndSave(self, path, format):
        try:
            showFramerateQuery = "SELECT name FROM `p_prj_framerate` AS pf JOIN `p_framerate` AS f ON pf.framerate_id = f.id WHERE pf.id_mv = {}".format(self.id_mv)
            showFps = api.database.selectSingleQuery(showFramerateQuery)[0]
            if showFps.count('Frame'):
                showFps = showFps.split('Frame')[0]
            cmd.currentUnit(time=showFps.lower())
        except TypeError:
            cmd.currentUnit(time='pal')
            api.log.logger().error('Can\'t get fps from DB, set to (pal) by default')

        remotePath = path
        cmd.file(rename=remotePath)
        api.log.logger().debug("Saving to {}".format(remotePath))
        cmd.file(save=True, force=True, type=format)

    ############################################################################
    # @brief      real save function
    #
    def realSave(self):
        api.log.logger().debug('realSave maya ascii (default)')
        api.log.logger().debug('server save')

        api.scene.badPluginCleanUp()

        if cmd.unknownPlugin(query=True, list=True) is not None:
            api.scene.pluginCheck()

        # special clean up
        api.scene.specialCleanup()

        # real save maya ascii
        try:
            maPath = os.path.join(self.savedAssemblyFolder, '{}.ma'.format(self.sceneName)).replace('\\', '/')
            self.renameAndSave(maPath, 'mayaAscii')
            api.log.logger().debug('ma saved in: {}'.format(maPath))
        except RuntimeError:
            api.log.logger().debug('Error: Cannot save in Maya Ascii!')

    ############################################################################
    # @brief      save function.
    #
    def save(self):
        self.incrementVersion()

        self.savedAssemblyFolder = os.path.join(
            self.mcFolder,
            'scenes',
            self.asset.category,
            self.asset.group,
            self.asset.name,
            self.asset.variant,
            'assembly',
            self.version
        ).replace('\\', '/')

        if not os.path.exists(self.savedAssemblyFolder):
            os.makedirs(self.savedAssemblyFolder)

        self.sceneName = '{}assembly_{}'.format(self.node, self.version)

        self.definitionPath = os.path.join(self.savedAssemblyFolder.split(self.mcFolder)[1][1:], '{}.ma'.format(self.sceneName)).replace('\\', '/')

        self.realSave()
        self.saveOnDB()

        # write json file
        saveJson = os.path.join(
            self.savedAssemblyFolder,
            '{}.json'.format(self.sceneName)
        ).replace('\\', '/')

        api.json.json_write(self.saveDict, saveJson)

    ############################################################################
    # @brief      load function
    #
    # @param      mode  The mode
    #
    def load(self, mode='merge'):
        if mode == 'merge':
            self.node = cmd.assembly(name=os.path.basename(self.definitionPath).split('setEdit')[0], type='assemblyReference')
            cmd.setAttr('{}.definition'.format(self.node), self.definitionPath, type='string')

    ############################################################################
    # @brief      Gets the asset.
    #
    # @return     The asset.
    #
    def getAsset(self):
        return self.asset

    ############################################################################
    # @brief      Gets the asset identifier.
    #
    # @return     The asset identifier.
    #
    def getAssetId(self):
        return self.asset.variantId

    ############################################################################
    # @brief      Gets the category.
    #
    # @return     The category.
    #
    def getCategory(self):
        return self.asset.category

    ############################################################################
    # @brief      Gets the group.
    #
    # @return     The group.
    #
    def getGroup(self):
        return self.asset.group

    ############################################################################
    # @brief      Gets the name.
    #
    # @return     The name.
    #
    def getName(self):
        return self.asset.name

    ############################################################################
    # @brief      Gets the variant.
    #
    # @return     The variant.
    #
    def getVariant(self):
        return self.asset.variant

    ############################################################################
    # @brief      Gets the shotgun identifier.
    #
    # @return     The shotgun identifier.
    #
    def getShotgunId(self):
        return self.asset.shotgunId
