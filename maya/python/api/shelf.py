import maya.cmds as cmd
import os
import functools
import importlib

import api.database as db
import api.log

importlib.reload(db)
importlib.reload(api.log)


################################################################################
# @brief      Gets the main shelf layout.
#
# @return     The main shelf layout.
#
def _getMainShelfLayout():
    if cmd.layout('ShelfLayout', query=True, exists=True):
        api.log.logger().debug('ShelfLayout found')
        return 'ShelfLayout'

    return False


################################################################################
# @brief      Delete the specified shelf from the main shelf layout.
#
# @param      name  The name of the shelf
#
# @return     True if deleted, false otherwise
#
def deleteShelf(name=None):
    if cmd.shelfLayout(name, query=True, exists=True):
        cmd.deleteUI(name)
        api.log.logger().debug('Shelf {} deleted.'.format(name))

        return True

    return False


################################################################################
# @brief      Creates a shelf, and if already exists, delete the older one
#             before creating it as a new object.
#
# @param      name  The name
#
# @return     The new shelf object or False if maya runs from command line app
#
def createShelf(name=None):
    main_shelf = _getMainShelfLayout()

    deleteShelf(name)

    if main_shelf:
        api.log.logger().debug('Shelf {} created'.format(name))
        return cmd.shelfLayout(name, parent=main_shelf, height=50)
        # return cmd.shelfLayout(name, parent=main_shelf, backgroundColor=[0.0, 0.0, 0.0])

    return False


################################################################################
# @brief      Adds a button into the specified shelf layout object.
#
# @param      shelfLayout  The shelf layout
# @param      label        The label to show on the button hover event
# @param      image        The image icon for the button
# @param      command      The command to execute when clicking the button
#
# @return     The new shelf button object
#
def addButton(shelfLayout=None, label=None, image=None, command=None, enableOverlayLabel=True):
    buttonName = label.replace('-', '_')
    buttonName = buttonName.replace(' ', '')

    def extendedFunction(label, command):
        userIdQuery = "SELECT `id_user` FROM `tk_dim_user` WHERE username='{}'".format(os.getenv('USERNAME'))
        userId = db.selectQuery(userIdQuery)[0][0]

        insertQuery = "INSERT INTO `toolStats` (`toolName`, `userId`, `idMv`) VALUES ('{}', '{}', '{}')".format(label, userId, os.getenv('ID_MV'))
        db.insertQuery(insertQuery)

        command()

    return cmd.shelfButton(
        buttonName,
        enableCommandRepeat=True,
        enable=True,
        width=50,
        height=50,
        manage=True,
        visible=True,
        preventOverride=False,
        align='center',
        label=label,
        imageOverlayLabel=label if enableOverlayLabel else '',
        overlayLabelColor=[0.0, 0.0, 0.0],
        overlayLabelBackColor=[1.0, 1.0, 1.0, 0.0],
        labelOffset=0,
        font='tinyBoldLabelFont',
        image=image,
        image1=image,
        style='iconOnly',
        marginWidth=0,
        marginHeight=0,
        command=command,
        # command=functools.partial(extendedFunction, label, command),
        sourceType='python',
        noDefaultPopup=True,
        parent=shelfLayout
    )


################################################################################
# @brief      Adds a popup menu linked to the specified parent button.
#
# @param      parentButton  The parent button
# @param      name          The name of the popup menu object
#
# @return     The new popup menu object
#
def addPopupMenu(parentButton=None, name=None):
    popupMenuName = '{}PopupMenu'.format(name)

    return cmd.popupMenu(popupMenuName, parent=parentButton)


################################################################################
# @brief      Adds a menu item into the specified popup menu.
#
# @param      parentPopupMenu  The parent popup menu
# @param      label            The label to show in the menu item
# @param      command          The command to execute
#
# @return     The new menu item object
#
def addMenuItem(parentPopupMenu=None, label=None, command=None):
    menuItemName = '{}MenuItem'.format(label)

    return cmd.menuItem(
        menuItemName,
        label=label,
        command=command,
        parent=parentPopupMenu
    )


################################################################################
# @brief      Adds a shelf separator into the specified shelf layout object
#
# @param      shelfLayout  The parent shelf layout
#
# @return     The new separator object
#
def addSeparator(shelfLayout=None):

    return cmd.separator(
        width=12,
        height=35,
        preventOverride=False,
        enableBackground=False,
        style='shelf',
        horizontal=False,
        parent=shelfLayout
    )
