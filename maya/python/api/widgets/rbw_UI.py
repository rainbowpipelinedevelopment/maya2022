import os
import time
import importlib
import functools
from PySide2 import QtWidgets, QtGui, QtCore

import maya.cmds as cmd
from maya.app.general.mayaMixin import MayaQWidgetDockableMixin

import api.log
import api.system
import api.widgets.flowLayout as flowLayout

importlib.reload(api.log)
importlib.reload(api.system)
importlib.reload(flowLayout)

iconPath = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'common').replace('\\', '/')
jsonPath = os.path.join(os.getenv('LOCALPIPE'), 'json_file').replace('\\', '/')
settingsPath = os.path.join('C:/', 'Users', os.getenv('USERNAME'), 'Documents', 'AraknoSettings', 'arakno_ui.json').replace('\\', '/')

################################################################################
# @brief      This class describes a rbw window.
#
class RBWWindow(QtWidgets.QMainWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super().__init__(parent=api.system.getMayaWin())

        self.resizable = True
        self.margin = 10
        self.cursor = QtGui.QCursor()
        self.top = False
        self.bottom = False
        self.left = False
        self.right = False

        self.inRect = False
        self.moveFlag = False

        self.setMouseTracking(True)
        self.installEventFilter(self)

        self.initFrame()

    ############################################################################
    # @brief      Initializes the frame.
    #
    def initFrame(self):
        self.setWindowFlags(self.windowFlags() | QtCore.Qt.FramelessWindowHint)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        self.setSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)

        # ---------------------------- frame ------------------------------ #
        self.frame = QtWidgets.QWidget()
        self.frame.setObjectName('MainFrame')
        self.frame.setStyleSheet('QWidget#MainFrame {border-image: url("' + os.path.join(iconPath, '..', 'backgrounds', 'rainbow.jfif').replace('\\', '/') + '"); border-radius: 11px;}')
        self.setCentralWidget(self.frame)

        self.frameBox = QtWidgets.QVBoxLayout(self.frame)
        self.frameBox.setContentsMargins(3, 3, 3, 3)
        self.frameBox.setSpacing(3)

        self.mainWidget = QtWidgets.QLabel()
        self.mainWidget.setSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Ignored)
        self.mainWidget.setObjectName('Background')
        self.mainWidget.setPixmap(QtGui.QPixmap(os.path.join(iconPath, '..', 'backgrounds', 'arakno_background.png').replace('\\', '/')).scaled(480, 480))
        self.mainWidget.setStyleSheet('QLabel#Background {background-color: #404040; border-radius: 8px;}')
        self.mainWidget.setAlignment(QtCore.Qt.AlignCenter)

        self.frameBox.addWidget(self.mainWidget)

        self.mainLayout = QtWidgets.QVBoxLayout(self.mainWidget)
        self.mainLayout.setContentsMargins(3, 3, 3, 3)
        self.mainLayout.setSpacing(3)

        # -------------------- title and manage button -------------------- #
        self.titleBar = RBWTitleBar(self)
        self.mainLayout.addWidget(self.titleBar)

        self.setFocus()
        self.setTheme()

    ############################################################################
    # @brief      Sets the style.
    #
    def setStyle(self):
        css_path = os.path.join(os.getenv('LOCALPY'), 'api', 'widgets', 'rbwUI_style.css').replace('\\', '/')
        with open(css_path) as f:
            self.setStyleSheet(f.read())

    ############################################################################
    # @brief      Sets the main.
    #
    # @param      value  The value
    #
    def setMain(self, value):
        if value:
            self.titleBar.show()
        else:
            self.titleBar.hide()

    ############################################################################
    # @brief      Sets the icon.
    #
    # @param      image  The image
    #
    def setIcon(self, image):
        self.titleBar.setIcon(image)

    ############################################################################
    # @brief      Sets the title.
    #
    # @param      title  The title
    #
    def setTitle(self, title):
        self.setWindowTitle(title)
        self.titleBar.setTitle(title)

    ############################################################################
    # @brief      Sets the theme.
    #
    def setTheme(self):
        if os.path.exists(settingsPath):
            settings = api.json.json_read(settingsPath)
            if 'theme' in settings:
                theme = api.json.json_read(os.path.join(jsonPath, 'themes.json').replace('\\', '/'))[settings['theme']]
                self.frame.setStyleSheet(theme['frame'].replace('ICONPATH', iconPath))
                self.mainWidget.setPixmap(QtGui.QPixmap(theme['background'].replace('ICONPATH', iconPath)).scaled(480, 480))

    ############################################################################
    # @brief      Adds a status bar.
    #
    def addStatusBar(self):
        self.statusBar = StatusBar(self)
        self.mainLayout.addWidget(self.statusBar)

    # ========================= WINDOW FUNCTIONING =============================

    ############################################################################
    # @brief      { function_description }
    #
    def updateMargins(self):
        mainRect = self.rect()
        mainRectTL = QtCore.QPoint(mainRect.left(), mainRect.top())
        mainRectTR = QtCore.QPoint((mainRect.right() - self.margin + 1), mainRect.top())
        mainRectBL = QtCore.QPoint(mainRect.left(), (mainRect.bottom() - self.margin + 1))
        mainRectBR = QtCore.QPoint((mainRect.right() - self.margin + 1), (mainRect.bottom() - self.margin + 1))
        mainRectTC = QtCore.QPoint((mainRect.left() + self.margin), mainRect.top())
        mainRectBC = QtCore.QPoint((mainRect.left() + self.margin), (mainRect.bottom() - self.margin + 1))
        mainRectCL = QtCore.QPoint(mainRect.left(), (mainRect.top() + self.margin))
        mainRectCR = QtCore.QPoint((mainRect.right() - self.margin + 1), (mainRect.top() + self.margin))

        self.TL = QtCore.QRect(mainRectTL, QtCore.QSize(self.margin, self.margin))
        self.TR = QtCore.QRect(mainRectTR, QtCore.QSize(self.margin, self.margin))
        self.BL = QtCore.QRect(mainRectBL, QtCore.QSize(self.margin, self.margin))
        self.BR = QtCore.QRect(mainRectBR, QtCore.QSize(self.margin, self.margin))
        self.TC = QtCore.QRect(mainRectTC, QtCore.QSize(mainRect.right(), self.margin))
        self.BC = QtCore.QRect(mainRectBC, QtCore.QSize(mainRect.right(), self.margin))
        self.CL = QtCore.QRect(mainRectCL, QtCore.QSize(self.margin, mainRect.bottom()))
        self.CR = QtCore.QRect(mainRectCR, QtCore.QSize(self.margin, mainRect.bottom()))

    ############################################################################
    # @brief      Sets the margins.
    #
    # @param      value  The value
    #
    def setMargins(self, value):
        self.margin = value
        self.updateMargins()

    ############################################################################
    # @brief      Determines if resizable.
    #
    # @return     True if resizable, False otherwise.
    #
    def isResizable(self):
        return self.resizable

    ############################################################################
    # @brief      Sets the resizable.
    #
    # @param      value  The value
    #
    def setResizable(self, value):
        self.resizable = value

    ############################################################################
    # @brief      minimize the window.
    #
    def minimize(self):
        self.showMinimized()

    ############################################################################
    # @brief      { function_description }
    #
    def toggleSize(self):
        if self.isMaximized():
            self.showNormal()
            self.titleBar.resizeButton.setIcon(QtGui.QPixmap(os.path.join(iconPath, 'maximize.png')))
        else:
            self.showMaximized()
            self.titleBar.resizeButton.setIcon(QtGui.QPixmap(os.path.join(iconPath, 'normal.png')))

    ############################################################################
    # @brief      Closes a window.
    #
    def closeWindow(self):
        self.deleteLater()

    ############################################################################
    # @brief      { function_description }
    #
    # @param      source  The source
    # @param      event   The event
    #
    # @return     { description_of_the_return_value }
    #
    def eventFilter(self, source, event):
        if event.type() == QtGui.QHoverEvent.HoverMove:
            if self.isResizable():
                if not (self.window().isMaximized() or self.window().isFullScreen()):
                    mousePos = event.pos()
                    self.inRect = True
                    if self.TL.contains(mousePos) or self.BR.contains(mousePos):
                        self.cursor.setShape(QtCore.Qt.SizeFDiagCursor)
                        self.top = True if self.TL.contains(mousePos) else False
                        self.bottom = True if self.BR.contains(mousePos) else False
                        self.left = True if self.TL.contains(mousePos) else False
                        self.right = True if self.BR.contains(mousePos) else False

                    elif self.TR.contains(mousePos) or self.BL.contains(mousePos):
                        self.cursor.setShape(QtCore.Qt.SizeBDiagCursor)
                        self.top = True if self.TR.contains(mousePos) else False
                        self.bottom = True if self.BL.contains(mousePos) else False
                        self.left = True if self.BL.contains(mousePos) else False
                        self.right = True if self.TR.contains(mousePos) else False

                    elif self.TC.contains(mousePos) or self.BC.contains(mousePos):
                        self.cursor.setShape(QtCore.Qt.SizeVerCursor)
                        self.top = True if self.TC.contains(mousePos) else False
                        self.bottom = True if self.BC.contains(mousePos) else False
                        self.left = False
                        self.right = False

                    elif self.CL.contains(mousePos) or self.CR.contains(mousePos):
                        self.cursor.setShape(QtCore.Qt.SizeHorCursor)
                        self.top = False
                        self.bottom = False
                        self.left = True if self.CL.contains(mousePos) else False
                        self.right = True if self.CR.contains(mousePos) else False

                    else:
                        self.inRect = False
                        self.cursor.setShape(QtCore.Qt.ArrowCursor)
                        self.top = False
                        self.bottom = False
                        self.left = False
                        self.right = False

                    self.setCursor(self.cursor)

        return super().eventFilter(source, event)

    ############################################################################
    # @brief      mouse press event function.
    #
    # @param      event  The event
    #
    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            if self.inRect:
                self.resizeWindow()
                self.moveFlag = False
            else:
                self.moveFlag = True
                self.movePosition = event.globalPos() - self.pos()
                event.accept()

    ############################################################################
    # @brief      mouse move event function.
    #
    # @param      event  The event
    #
    def mouseMoveEvent(self, event):
        if QtCore.Qt.LeftButton and self.moveFlag:
            self.move(event.globalPos() - self.movePosition)
            event.accept()

    ############################################################################
    # @brief      mouse reloase override function.
    #
    # @param      QMouseEvent  The quarter mouse event
    #
    def mouseReleaseEvent(self, QMouseEvent):
        self.moveFlag = False
        if self.isResizable():
            self.updateMargins()
            self.inRect = False

    ############################################################################
    # @brief      { function_description }
    #
    def resizeWindow(self):
        window = self.window().windowHandle()
        if self.cursor.shape() == QtCore.Qt.SizeHorCursor:
            if self.left:
                window.startSystemResize(QtCore.Qt.LeftEdge)
            elif self.right:
                window.startSystemResize(QtCore.Qt.RightEdge)
        elif self.cursor.shape() == QtCore.Qt.SizeVerCursor:
            if self.top:
                window.startSystemResize(QtCore.Qt.TopEdge)
            elif self.bottom:
                window.startSystemResize(QtCore.Qt.BottomEdge)
        elif self.cursor.shape() == QtCore.Qt.SizeBDiagCursor:
            if self.top and self.right:
                window.startSystemResize(QtCore.Qt.TopEdge | QtCore.Qt.RightEdge)
            elif self.bottom and self.left:
                window.startSystemResize(QtCore.Qt.BottomEdge | QtCore.Qt.LeftEdge)
        elif self.cursor.shape() == QtCore.Qt.SizeFDiagCursor:
            if self.top and self.left:
                window.startSystemResize(QtCore.Qt.TopEdge | QtCore.Qt.LeftEdge)
            elif self.bottom and self.right:
                window.startSystemResize(QtCore.Qt.BottomEdge | QtCore.Qt.RightEdge)

        self.updateMargins()


################################################################################
# @brief      This class describes a rbw dockable window.
#
class RBWDockableWindow(MayaQWidgetDockableMixin, QtWidgets.QMainWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(RBWDockableWindow, self).__init__()

        self.initFrame()

    ############################################################################
    # @brief      Initializes the frame.
    #
    def initFrame(self):
        self.setWindowFlags(self.windowFlags() | QtCore.Qt.FramelessWindowHint)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        self.setSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)

        # ---------------------------- frame ------------------------------ #
        self.frame = QtWidgets.QWidget()
        self.frame.setObjectName('MainFrame')
        self.frame.setStyleSheet('QWidget#MainFrame {border-image: url("' + os.path.join(iconPath, '..', 'backgrounds', 'rainbow.jfif').replace('\\', '/') + '"); border-radius: 11px;}')
        self.setCentralWidget(self.frame)

        self.frameBox = QtWidgets.QVBoxLayout(self.frame)
        self.frameBox.setContentsMargins(3, 3, 3, 3)
        self.frameBox.setSpacing(3)

        self.mainWidget = QtWidgets.QLabel()
        self.mainWidget.setSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Ignored)
        self.mainWidget.setObjectName('Background')
        self.mainWidget.setPixmap(QtGui.QPixmap(os.path.join(iconPath, '..', 'backgrounds', 'arakno_background.png').replace('\\', '/')).scaled(480, 480))
        self.mainWidget.setStyleSheet('QLabel#Background {background-color: #404040; border-radius: 8px;}')
        self.mainWidget.setAlignment(QtCore.Qt.AlignCenter)

        self.frameBox.addWidget(self.mainWidget)

        self.mainLayout = QtWidgets.QVBoxLayout(self.mainWidget)
        self.mainLayout.setContentsMargins(3, 3, 3, 3)
        self.mainLayout.setSpacing(3)

        self.setWindowTitle('RBWDockableWindow')
        self.setMinimumWidth(200)
        self.setFocus()
        self.setTheme()

    ############################################################################
    # @brief      Sets the style.
    #
    def setStyle(self):
        css_path = os.path.join(os.getenv('LOCALPY'), 'api', 'widgets', 'rbwUI_style.css').replace('\\', '/')
        with open(css_path) as f:
            self.setStyleSheet(f.read())

    ############################################################################
    # @brief      Sets the theme.
    #
    def setTheme(self):
        if os.path.exists(settingsPath):
            settings = api.json.json_read(settingsPath)
            if 'theme' in settings:
                theme = api.json.json_read(os.path.join(jsonPath, 'themes.json').replace('\\', '/'))[settings['theme']]
                self.frame.setStyleSheet(theme['frame'].replace('ICONPATH', iconPath))
                self.mainWidget.setPixmap(QtGui.QPixmap(theme['background'].replace('ICONPATH', iconPath)).scaled(480, 480))


# ============================================== CUSTOM RAINBOW WIDGETS =========================================================


################################################################################
# @brief      This class describes a rbw title bar.
#
class RBWTitleBar(QtWidgets.QWidget):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self, parent):
        super().__init__(parent)
        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setAttribute(QtCore.Qt.WA_StyledBackground, True)
        self.setObjectName('RBWTitleBar')
        self.setStyleSheet('background-color: rgba(100, 100, 100, 150); border-radius: 7px')
        self.layout = QtWidgets.QHBoxLayout(self)

        self.titleLabel = RBWLabel('', 20)
        self.setTitle('RBW Template UI')
        self.windowIcon = QtWidgets.QLabel()
        self.setIcon('RBWCGI.png')
        self.windowIcon.setStyleSheet('background-color: transparent; border: 2px solid transparent')

        self.closeButton = RBWButton(icon=['close.png'], size=[30, 30])
        self.closeButton.clicked.connect(self.parentWidget().closeWindow)
        self.minimizeButton = RBWButton(icon=['minimize.png'], size=[30, 30])
        self.minimizeButton.clicked.connect(self.parentWidget().minimize)
        self.resizeButton = RBWButton(icon=['maximize.png'], size=[30, 30])
        self.resizeButton.clicked.connect(self.parentWidget().toggleSize)

        self.layout.addWidget(self.windowIcon)
        self.layout.addWidget(self.titleLabel)
        self.layout.addStretch()
        self.layout.addWidget(self.minimizeButton)
        self.layout.addWidget(self.resizeButton)
        self.layout.addWidget(self.closeButton)

        self.setFixedHeight(50)

    ############################################################################
    # @brief      Sets the title.
    #
    # @param      title  The title
    #
    def setTitle(self, title):
        self.setWindowTitle(title)
        self.titleLabel.setText(title)

    ############################################################################
    # @brief      Sets the icon.
    #
    # @param      icon  The icon
    #
    def setIcon(self, icon):
        self.windowIcon.setPixmap(QtGui.QIcon(os.path.join(iconPath, icon)).pixmap(30, 30))


################################################################################
# @brief      This class describes a status bar.
#
class StatusBar(QtWidgets.QWidget):

    def __init__(self, parent):
        super().__init__(parent)
        self.initUI()
        self.showMessage('Ready')

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.label = QtWidgets.QLabel("Status bar...")
        self.label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.label.setStyleSheet("""
            background-color: rgba(100, 100, 100, 150);
            font-size: 12px;
            padding-left: 5px;
            color: white;
            border-radius: 5px;
        """)
        self.layout = QtWidgets.QHBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.addWidget(self.label)
        self.setLayout(self.layout)

        self.setFixedHeight(24)

    ############################################################################
    # @brief      Shows the message.
    #
    # @param      text  The text
    #
    def showMessage(self, text):
        self.label.setText(text)


################################################################################
# @brief      This class describes a rbw frame.
#
class RBWFrame(QtWidgets.QWidget):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      layout   The layout
    # @param      margins  The margins
    # @param      spacing  The spacing
    # @param      bgColor  The background color
    # @param      radius   The radius
    #
    def __init__(self, layout, margins=[5, 5, 5, 5], spacing=5, bgColor='rgba(100, 100, 100, 150)', radius=5):
        super(RBWFrame, self).__init__()

        box = QtWidgets.QVBoxLayout(self)
        box.setContentsMargins(0, 0, 0, 0)
        self.widget = QtWidgets.QWidget()
        self.widget.setObjectName('RBWFrame')
        box.addWidget(self.widget)

        if layout == 'V':
            self.layout = QtWidgets.QVBoxLayout(self.widget)
        elif layout == 'H':
            self.layout = QtWidgets.QHBoxLayout(self.widget)
        elif layout == 'G':
            self.layout = QtWidgets.QGridLayout(self.widget)
        elif layout == 'F':
            self.layout = QtWidgets.QFormLayout(self.widget)
        elif layout == 'FL':
            self.layout = flowLayout.FlowLayout(self.widget)

        self.setStyleSheet('QWidget#RBWFrame{{background-color: {}; font: bold; font-size: 15px; border-radius: {}px;}}'.format(bgColor, radius))
        self.setContentsMargins(margins)
        self.setSpacing(spacing)

    ############################################################################
    # @brief      Adds a widget.
    #
    # @param      widget     The widget
    # @param      alignment  The alignment
    #
    def addWidget(self, widget, alignment=None):
        if alignment:
            self.layout.addWidget(widget, alignment=alignment)
        else:
            self.layout.addWidget(widget)

    ############################################################################
    # @brief      Adds a row.
    #
    # @param      widget1  The widget 1
    # @param      widget2  The widget 2
    #
    def addRow(self, widget1, widget2):
        self.layout.addRow(widget1, widget2)

    ############################################################################
    # @brief      Adds a layout.
    #
    # @param      layout  The layout
    #
    def addLayout(self, layout):
        self.layout.addLayout(layout)

    ############################################################################
    # @brief      Adds a stretch.
    #
    def addStretch(self):
        self.layout.addStretch()

    ############################################################################
    # @brief      Sets the contents margins.
    #
    # @param      margins  The margins
    #
    def setContentsMargins(self, margins):
        self.layout.setContentsMargins(margins[0], margins[1], margins[2], margins[3])

    ############################################################################
    # @brief      Sets the spacing.
    #
    # @param      spacing  The spacing
    #
    def setSpacing(self, spacing):
        self.layout.setSpacing(spacing)


class RBWSpacerLine(QtWidgets.QFrame):
    def __init__(self, orient='V', color='white'):
        super(RBWSpacerLine, self).__init__()
        self.setFrameStyle(QtWidgets.QFrame.VLine if orient == 'V' else QtWidgets.QFrame.HLine)
        self.setStyleSheet('background-color: {}'.format(color))


################################################################################
# @brief      This class describes a rbw label.
#
class RBWLabel(QtWidgets.QLabel):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      text    The text
    # @param      size    The size
    # @param      bold    The bold
    # @param      italic  The italic
    # @param      color   The color
    #
    def __init__(self, text, size=15, bold=True, italic=True, color='white', alignment='AlignLeft'):
        super(RBWLabel, self).__init__(text)
        self.setStyleSheet('background-color: transparent; color: {}; qproperty-alignment: {}'.format(color, alignment))
        f1 = self.font()
        f1.setPixelSize(size)
        f1.setBold(bold)
        f1.setItalic(italic)
        self.setFont(f1)


################################################################################
# @brief      This class describes a rbw button.
#
class RBWButton(QtWidgets.QPushButton):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      text      The text
    # @param      icon      The icon
    # @param      size      The size
    # @param      tooltip   The tooltip
    # @param      widgets   The widgets
    # @param      radius    The radius of the corners
    # @param      align     The alignment of the content
    # @param      color     The color
    # @param      hover     The hover
    # @param      pressed   The pressed
    # @param      checkable If it's checkable or not
    # @param      name      The object name
    #
    def __init__(self, text=None, icon=[], size=[], tooltip=[], widgets=[], radius=[5, 5, 5, 5], align='center', color='rgba(30, 30, 30, 150)', hover='rgba(130, 130, 130, 150)', pressed='white', checkable=False, name=None):
        super(RBWButton, self).__init__(text)
        self.setCursor(QtCore.Qt.PointingHandCursor)
        self.setStyleSheet('''
            QPushButton{{background-color: {}; border-top-left-radius: {}px; border-top-right-radius: {}px; border-bottom-right-radius: {}px; border-bottom-left-radius: {}px; text-align: {}}}
            QPushButton:hover{{background-color: {}}}
            QPushButton:pressed{{background-color: {}}}
            QPushButton:checked{{background-color: {}; color: rgb(30, 30, 30)}}
        '''.format(color, radius[0], radius[1], radius[2], radius[3], align, hover, pressed, pressed))
        self.setCheckable(checkable)

        if icon:
            self.icons = icon
            self.setIcon(QtGui.QPixmap(os.path.join(iconPath, icon[0])))
            if size:
                iSize = size[1] if size[1] else size[0]
                self.setIconSize(QtCore.QSize(iSize, iSize))

        if size:
            if not size[0]:
                self.setFixedHeight(size[1])
            elif not size[1]:
                self.setFixedWidth(size[0])
            else:
                self.setFixedSize(size[0], size[1])

        if tooltip:
            self.setToolTip(tooltip[0])

        if widgets:
            self.icons = icon
            self.tooltips = tooltip
            self.widgets = widgets

            self.setCheckable(True)
            self.clicked.connect(self.updateState)

    ############################################################################
    # @brief      update state.
    #
    def updateState(self):
        if self.isChecked():
            self.setIcon(QtGui.QPixmap(os.path.join(iconPath, self.icons[1])))
            self.setToolTip(self.tooltips[1])
            self.widgets[0].hide()
            self.widgets[1].show()
        else:
            self.setIcon(QtGui.QPixmap(os.path.join(iconPath, self.icons[0])))
            self.setToolTip(self.tooltips[0])

            self.widgets[1].hide()
            self.widgets[0].show()


################################################################################
# @brief      This class describes a rbw switch.
#
class RBWSwitch(QtWidgets.QCheckBox):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      width      The width
    # @param      off        Off
    # @param      offCircle  Off circle
    # @param      on         { parameter_description }
    # @param      onCircle   On circle
    # @param      anim       The animation
    #
    def __init__(self, width=40, off='#ba0001', offCircle='#2d2d2d', on='#378805', onCircle='#dddddd', anim=QtCore.QEasingCurve.OutBounce):
        QtWidgets.QCheckBox.__init__(self)

        self.setFixedSize(width, 18)
        self.setCursor(QtCore.Qt.PointingHandCursor)

        self.off = off
        self.offCircle = offCircle
        self.on = on
        self.onCircle = onCircle

        self._position = 2
        self.anim = QtCore.QPropertyAnimation(self, b'position')
        self.anim.setEasingCurve(anim)
        self.anim.setDuration(500)

        self.stateChanged.connect(self.startTransition)

    ############################################################################
    # @brief      { function_description }
    #
    # @return     { description_of_the_return_value }
    #
    @QtCore.Property(float)
    def position(self):
        return self._position

    ############################################################################
    # @brief      { function_description }
    #
    # @param      pos   The position
    #
    @position.setter
    def position(self, pos):
        self._position = pos
        self.update()

    ############################################################################
    # @brief      Gets the position.
    #
    # @return     The position.
    #
    def getPosition(self):
        return self._position

    ############################################################################
    # @brief      Sets the position.
    #
    # @param      pos   The new value
    #
    def setPosition(self, pos):
        self._position = pos
        self.update()

    ############################################################################
    # @brief      Starts a transition.
    #
    # @param      value  The value
    #
    def startTransition(self, value):
        self.anim.stop()
        if value:
            self.anim.setEndValue(self.width() - 16)
        else:
            self.anim.setEndValue(2)

        self.anim.start()

    ############################################################################
    # @brief      { function_description }
    #
    # @param      pos   The position
    #
    # @return     { description_of_the_return_value }
    #
    def hitButton(self, pos: QtCore.QPoint):
        return self.contentsRect().contains(pos)

    ############################################################################
    # @brief      { function_description }
    #
    # @param      event  The event
    #
    def paintEvent(self, event):
        p = QtGui.QPainter(self)
        p.setRenderHint(QtGui.QPainter.Antialiasing)
        p.setPen(QtCore.Qt.NoPen)

        rect = QtCore.QRect(0, 0, self.width(), self.height())

        if not self.isChecked():
            p.setBrush(QtGui.QColor(self.off))
            p.drawRoundedRect(0, 0, rect.width(), self.height(), self.height() / 2, self.height() / 2)
            p.setBrush(QtGui.QColor(self.offCircle))
            p.drawEllipse(self._position, 2, 14, 14)
        else:
            p.setBrush(QtGui.QColor(self.on))
            p.drawRoundedRect(0, 0, rect.width(), self.height(), self.height() / 2, self.height() / 2)
            p.setBrush(QtGui.QColor(self.onCircle))
            p.drawEllipse(self._position, 2, 14, 14)

        p.end()


################################################################################
# @brief      This class describes a rbw check box.
#
class RBWCheckBox(QtWidgets.QWidget):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      text      The text
    # @param      toolTip   The tool tip
    # @param      defValue  The definition value
    # @param      stretch   The stretch
    #
    def __init__(self, text, toolTip=None, defValue=False, stretch=True, margins=[0, 0, 0, 0]):
        super(RBWCheckBox, self).__init__()

        layout = QtWidgets.QHBoxLayout(self)
        layout.setContentsMargins(*margins)
        self.label = QtWidgets.QLabel(text)
        self.label.setStyleSheet('background-color: transparent; color: white')
        self.switch = RBWSwitch()
        layout.addWidget(self.label)
        if stretch:
            layout.addStretch()
        layout.addWidget(self.switch)

        f1 = self.label.font()
        f1.setPixelSize(12)
        f1.setBold(True)
        self.label.setFont(f1)

        if toolTip:
            self.setToolTip(toolTip)

        self.setChecked(defValue)

        self.stateChanged = self.switch.stateChanged
        self.toggled = self.switch.toggled

    ############################################################################
    # @brief      Determines if checked.
    #
    # @return     True if checked, False otherwise.
    #
    def isChecked(self):
        return self.switch.isChecked()

    ############################################################################
    # @brief      Sets the checked.
    #
    # @param      value  The value
    #
    def setChecked(self, value):
        self.switch.setChecked(value)

    ############################################################################
    # @brief      Gets the text.
    #
    # @return     The text.
    #
    def getText(self):
        return self.label.text()

    ############################################################################
    # @brief      Sets the text.
    #
    # @param      text  The text
    #
    def setText(self, text):
        self.label.setText(text)

    ############################################################################
    # @brief      toggle function
    #
    def toggle(self):
        self.switch.toggle()


################################################################################
# @brief      This class describes a rbw combo box.
#
class RBWComboBox(QtWidgets.QWidget):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      text        The text
    # @param      toolTip     The tool tip
    # @param      items       The items
    # @param      bgColor     The background color
    # @param      fieldColor  The field color
    # @param      stretch     The stretch
    #
    def __init__(self, text='', toolTip=None, items=[], bgColor='rgba(100, 100, 100, 150)', fieldColor='rgba(30, 30, 30, 150)', stretch=True, size=100):

        super(RBWComboBox, self).__init__()

        box = QtWidgets.QVBoxLayout(self)
        box.setContentsMargins(0, 0, 0, 0)
        widget = QtWidgets.QWidget()
        widget.setObjectName('RBWFrame')
        box.addWidget(widget)

        self.layout = QtWidgets.QHBoxLayout(widget)
        if bgColor == 'transparent':
            self.layout.setContentsMargins(0, 0, 0, 0)
        else:
            self.layout.setContentsMargins(5, 2, 2, 2)

        if text:
            self.label = QtWidgets.QLabel(text)
            self.label.setStyleSheet('background-color: transparent; color: white')
            f1 = self.label.font()
            f1.setPixelSize(12)
            f1.setBold(True)
            self.label.setFont(f1)

            self.layout.addWidget(self.label)
            if stretch:
                self.layout.addStretch()

        self.combo = QtWidgets.QComboBox()
        self.combo.setMinimumWidth(size)
        self.combo.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

        self.layout.addWidget(self.combo)

        self.setStyleSheet('QWidget{{background-color: {}; border-radius: 7px}}'.format(bgColor))

        if items:
            self.addItems(items)

        if toolTip:
            self.setToolTip(toolTip)

        self.combo.setStyleSheet(
            '''
            QComboBox
            {{
                background-color: {};
                color:white;
                border-radius: 5px;
            }}

            QComboBox::drop-down
            {{
                border: 0px;
                padding-right: 3px;
            }}

            QComboBox::down-arrow
            {{
                border-top-right-radius: 5px;
                border-bottom-right-radius: 5px;
                image: url("{}/img/icons/common/down.png");
                width: 15px;
                height: 15px;
            }}
            '''.format(fieldColor, os.getenv('LOCALPIPE').replace('\\', '/'))
        )

        self.activated = self.combo.activated
        self.currentIndexChanged = self.combo.currentIndexChanged
        self.currentTextChanged = self.combo.currentTextChanged
        self.editTextChanged = self.combo.editTextChanged
        self.highlighted = self.combo.highlighted
        self.textActivated = self.combo.textActivated
        self.textHighlighted = self.combo.textHighlighted

    ############################################################################
    # @brief      Sets the text.
    #
    # @param      text  The text
    #
    def setText(self, text):
        if hasattr(self, 'label'):
            self.label.setText(text)

    ############################################################################
    # @brief      Adds an item.
    #
    # @param      item  The item
    #
    def addItem(self, item):
        self.combo.addItem(item)

    ############################################################################
    # @brief      Adds items.
    #
    # @param      items  The items
    #
    def addItems(self, items):
        self.combo.addItems(items)

    ############################################################################
    # @brief      Get current INdex.
    #
    # @return     the index.
    #
    def currentIndex(self):
        return self.combo.currentIndex()

    ############################################################################
    # @brief      Get current Text.
    #
    # @return     the text
    #
    def currentText(self):
        return self.combo.currentText()

    ############################################################################
    # @brief      Clears the object.
    #
    def clear(self):
        self.combo.clear()

    ############################################################################
    # @brief      Sets the current index.
    #
    # @param      index  The index
    #
    def setCurrentIndex(self, index):
        self.combo.setCurrentIndex(index)

    ############################################################################
    # @brief      Sets the current text.
    #
    # @param      text  The text
    #
    def setCurrentText(self, text):
        self.combo.setCurrentText(text)

    ############################################################################
    # @brief      Sets the size adjust policy.
    #
    # @param      policy  The policy
    #
    def setSizeAdjustPolicy(self, policy):
        self.combo.setSizeAdjustPolicy(policy)

    ############################################################################
    # @brief      Finds a text.
    #
    # @param      string     The string
    # @param      matchFlag  The match flag
    #
    # @return     the index.
    #
    def findText(self, string, matchFlag):
        return self.combo.findText(string, matchFlag)


################################################################################
# @brief      This class describes a rbw group box.
#
class RBWGroupBox(QtWidgets.QGroupBox):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      title     The title
    # @param      layout    The layout
    # @param      fontSize  The font size
    # @param      margins   The margins
    # @param      spacing   The spacing
    # @param      bgColor   The background color
    # @param      radius    The radius
    # @param      fixed     The fixed
    #
    def __init__(self, title, layout, fontSize=15, margins=[12, 20, 12, 6], spacing=5, bgColor='rgba(30, 30, 30, 150)', radius=6):
        super(RBWGroupBox, self).__init__(title)
        self.setObjectName('RBWGroupBox')

        if layout == 'V':
            self.layout = QtWidgets.QVBoxLayout()
        elif layout == 'H':
            self.layout = QtWidgets.QHBoxLayout()
        elif layout == 'G':
            self.layout = QtWidgets.QGridLayout()
        elif layout == 'F':
            self.layout = QtWidgets.QFormLayout()

        self.setStyleSheet('QGroupBox#RBWGroupBox{{background-color: {}; font: bold; font-size: {}px; border-radius: {}px;}}'.format(bgColor, fontSize, radius))
        self.setContentsMargins(margins)
        self.setSpacing(spacing)
        self.setLayout(self.layout)

    ############################################################################
    # @brief      Adds a widget.
    #
    # @param      widget     The widget
    # @param      alignment  The alignment
    #
    def addWidget(self, widget, alignment=None):
        if alignment:
            self.layout.addWidget(widget, alignment=alignment)
        else:
            self.layout.addWidget(widget)

    ############################################################################
    # @brief      Adds a row.
    #
    # @param      widget1  The widget 1
    # @param      widget2  The widget 2
    #
    def addRow(self, widget1, widget2):
        self.layout.addRow(widget1, widget2)

    ############################################################################
    # @brief      Adds a layout.
    #
    # @param      layout  The layout
    #
    def addLayout(self, layout):
        self.layout.addLayout(layout)

    ############################################################################
    # @brief      Adds a stretch.
    #
    def addStretch(self):
        self.layout.addStretch()

    ############################################################################
    # @brief      Sets the contents margins.
    #
    # @param      margins  The margins
    #
    def setContentsMargins(self, margins):
        self.layout.setContentsMargins(margins[0], margins[1], margins[2], margins[3])

    ############################################################################
    # @brief      Sets the spacing.
    #
    # @param      spacing  The spacing
    #
    def setSpacing(self, spacing):
        self.layout.setSpacing(spacing)


################################################################################
# @brief      This class describes a rbw scroll area.
#
class RBWScrollArea(QtWidgets.QScrollArea):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      layout    The layout
    # @param      spacing   The spacing
    # @param      margins   The margins
    # @param      scrollV   Activate vertical Scroll
    # @param      scrollH   Activate horizontal Scroll
    #
    def __init__(self, layout='V', spacing=5, margins=[5, 5, 5, 5], scrollV=True, scrollH=True):
        super(RBWScrollArea, self).__init__()
        widget = QtWidgets.QWidget()
        widget.setStyleSheet('QWidget{background-color: transparent; border-radius: 5px}')
        self.setWidget(widget)

        if layout == 'V':
            self.layout = QtWidgets.QVBoxLayout(widget)
        elif layout == 'H':
            self.layout = QtWidgets.QHBoxLayout(widget)
        elif layout == 'G':
            self.layout = QtWidgets.QGridLayout(widget)
        elif layout == 'F':
            self.layout = QtWidgets.QFormLayout(widget)

        self.layout.setContentsMargins(margins[0], margins[1], margins[2], margins[3])
        self.layout.setSpacing(spacing)

        self.setWidgetResizable(True)
        if not scrollV:
            self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        if not scrollH:
            self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)

    ############################################################################
    # @brief      Adds a widget.
    #
    # @param      widget     The widget
    # @param      alignment  The alignment
    #
    def addWidget(self, widget, alignment=None):
        if alignment:
            self.layout.addWidget(widget, alignment=alignment)
        else:
            self.layout.addWidget(widget)

    ############################################################################
    # @brief      Adds a layout.
    #
    # @param      layout  The layout
    #
    def addLayout(self, layout):
        self.layout.addLayout(layout)

    ############################################################################
    # @brief      Adds a stretch.
    #
    def addStretch(self):
        self.layout.addStretch()

    ############################################################################
    # @brief      Sets the contents margins.
    #
    # @param      margins  The margins
    #
    def setContentsMargins(self, margins):
        self.layout.setContentsMargins(margins[0], margins[1], margins[2], margins[3])

    ############################################################################
    # @brief      Sets the spacing.
    #
    # @param      spacing  The spacing
    #
    def setSpacing(self, spacing):
        self.layout.setSpacing(spacing)


################################################################################
# @brief      This class describes a rbw tab widget.
#
class RBWTabWidget(QtWidgets.QTabWidget):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      size  The size
    #
    def __init__(self, size, tabSize=85, bgColor='rgba(30, 30, 30, 150)'):
        super(RBWTabWidget, self).__init__()

        self.setObjectName('RBWTabWidget')
        self.setStyleSheet('''
            QTabWidget::pane
            {{
                background-color: {};
                border-radius: 5px;
            }}

            QTabBar #RBWTabWidget
            {{
                background-color: transparent;
            }}

            QTabWidget::tab-bar #RBWTabWidget
            {{
                alignment: left;
                left: 10px;
                background-color: transparent;
                border-radius: 0px
            }}

            QTabBar::tab
            {{
                font: bold;
                font-size: {}px;
                background-color: rgba(100, 100, 100, 150);
                border-top-left-radius: 4px;
                border-top-right-radius: 4px;
                min-height: 25px;
                min-width: {}px;
                padding-left: 8px;
                padding-right: 8px;
                color: white;
            }}

            QTabBar::tab:hover
            {{
                background-color: rgba(200, 200, 200, 150);
                color: black;
            }}

            QTabBar::tab:selected
            {{
                background-color: {};
                color: white;
            }}

            QTabBar::tab:!selected
            {{
                margin-top: 6px;
            }}'''.format(bgColor, size, tabSize, bgColor))


################################################################################
# @brief      This class describes a rbw line edit.
#
class RBWLineEdit(QtWidgets.QWidget):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      title       The title
    # @param      toolTip     The tool tip
    # @param      text        The text
    # @param      bgColor     The background color
    # @param      fieldColor  The field color
    # @param      completer   The completer
    #
    def __init__(self, title='', toolTip=None, text='', placeHolder=None, bgColor='rgba(100, 100, 100, 150)', fieldColor='rgba(30, 30, 30, 150)', completer=None, toolButton=None, stretch=True, size=150):
        super(RBWLineEdit, self).__init__()

        box = QtWidgets.QVBoxLayout(self)
        box.setContentsMargins(0, 0, 0, 0)
        widget = QtWidgets.QWidget()
        widget.setObjectName('RBWFrame')
        box.addWidget(widget)

        layout = QtWidgets.QHBoxLayout(widget)
        if bgColor == 'transparent':
            layout.setContentsMargins(0, 0, 0, 0)
        else:
            layout.setContentsMargins(5, 2, 2, 2)

        if title:
            self.label = RBWLabel(title, 12, bold=True, italic=False)
            layout.addWidget(self.label)
            if stretch:
                layout.addStretch()

        self.edit = QtWidgets.QLineEdit()
        self.edit.setMinimumWidth(size)
        layout.addWidget(self.edit)

        self.setStyleSheet('QWidget{{background-color: {}; border-radius: 7px}}'.format(bgColor))
        self.edit.setStyleSheet('border-radius: 5px; background-color: {}'.format(fieldColor))

        if completer:
            completerWidget = QtWidgets.QCompleter(sorted(completer))
            completerWidget.popup().setStyleSheet("background-color: rgba(50, 50, 50, 150); color: white;")
            self.edit.setCompleter(completerWidget)

        if toolButton:
            self.toolButton = QtWidgets.QToolButton()
            self.toolButton.setText("...")
            self.toolButton.clicked.connect(functools.partial(self.setChoice, toolButton))
            layout.addWidget(self.toolButton)

        if toolTip:
            self.setToolTip(toolTip)

        if placeHolder:
            self.edit.setPlaceholderText(placeHolder)

        if text:
            self.setText(text)

        self.returnPressed = self.edit.returnPressed
        self.textChanged = self.edit.textChanged
        self.editingFinished = self.edit.editingFinished

    ############################################################################
    # @brief      Sets the title.
    #
    # @param      text  The text
    #
    def setTitle(self, text):
        self.label.setText(text)

    ############################################################################
    # @brief      Sets the text.
    #
    # @param      text  The text
    #
    def setText(self, text):
        self.edit.setText(text)

    ############################################################################
    # @brief      get line text
    #
    # @return     the line text.
    #
    def text(self):
        return self.edit.text()

    ############################################################################
    # @brief      set the given file/path to the lineedit
    #
    # @param      QFileDialog mode.
    #
    def setChoice(self, mode):
        if mode == 'file':
            text = QtWidgets.QFileDialog.getOpenFileName()[0]
        elif mode == 'path':
            text = QtWidgets.QFileDialog.getExistingDirectory()

        self.edit.setText(text.replace('\\', '/'))

    ############################################################################
    # @brief      Sets the read only.
    #
    # @param      value  The value
    #
    def setReadOnly(self, value):
        self.edit.setReadOnly(value)


################################################################################
# @brief      This class describes a rbw text edit.
#
class RBWTextEdit(QtWidgets.QWidget):
    def __init__(self, title='', toolTip=None, text='', bgColor='rgba(100, 100, 100, 150)', fieldColor='rgba(30, 30, 30, 150)', enabled=True):
        super(RBWTextEdit, self).__init__()
        self.setToolTip(toolTip)

        box = QtWidgets.QVBoxLayout(self)
        box.setContentsMargins(0, 0, 0, 0)
        widget = QtWidgets.QWidget()
        widget.setObjectName('RBWFrame')
        box.addWidget(widget)

        layout = QtWidgets.QHBoxLayout(widget)
        if bgColor == 'transparent':
            layout.setContentsMargins(0, 0, 0, 0)
        else:
            layout.setContentsMargins(5, 2, 2, 2)

        if title:
            self.label = RBWLabel(title, 12, bold=True, italic=False)
            layout.addWidget(self.label)

        self.edit = QtWidgets.QTextEdit()
        self.edit.setMinimumWidth(200)
        layout.addWidget(self.edit)

        self.setStyleSheet('QWidget{{background-color: {}; border-radius: 7px}}'.format(bgColor))
        self.edit.setStyleSheet('border-radius: 5px; background-color: {}'.format(fieldColor))

        self.edit.setEnabled(enabled)

        if text:
            self.setText(text)

        self.copyAvailable = self.edit.copyAvailable
        self.currentCharFormatChanged = self.edit.currentCharFormatChanged
        self.cursorPositionChanged = self.edit.cursorPositionChanged
        self.redoAvailable = self.edit.redoAvailable
        self.selectionChanged = self.edit.selectionChanged
        self.textChanged = self.edit.textChanged
        self.undoAvailable = self.edit.undoAvailable

    ############################################################################
    # @brief      Sets the title.
    #
    # @param      text  The text
    #
    def setTitle(self, text):
        self.label.setText(text)

    ############################################################################
    # @brief      Sets the text.
    #
    # @param      text  The text
    #
    def setText(self, text):
        self.edit.setText(text)

    ############################################################################
    # @brief      { function_description }
    #
    # @return     { description_of_the_return_value }
    #
    def text(self):
        return self.edit.toPlainText()


################################################################################
# @brief      This class describes a rbw tree widget.
#
class RBWTreeWidget(QtWidgets.QTreeWidget):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      fields   The fields
    # @param      size     The size
    # @param      scrollV  The scroll v
    # @param      scrollH  The scroll h
    #
    def __init__(self, fields=[], size=[], scrollV=True, scrollH=True, decorate=False, darkMode=False, headerAlign=QtCore.Qt.AlignLeft):
        super(RBWTreeWidget, self).__init__()

        self.setColumnCount(len(fields))
        self.headerItem = QtWidgets.QTreeWidgetItem(fields)
        self.setHeaderItem(self.headerItem)
        self.header().setDefaultAlignment(headerAlign)
        self.setRootIsDecorated(decorate)
        if size:
            for i in range(0, len(size)):
                if size[i]:
                    self.setColumnWidth(i, size[i])

        if not scrollV:
            self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        if not scrollH:
            self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)

        if darkMode:
            self.setStyleSheet(
                '''
                QAbstractItemView
                {
                    background-color: rgba(50, 50, 50, 150);
                    color: white;
                }

                QTreeView
                {
                    border: 2px solid rgba(100, 100, 100, 150);
                    border-radius: 6px;
                }

                QHeaderView
                {
                    border-radius: 6px;
                }

                QHeaderView::section {
                    background-color: rgba(100, 100, 100, 150);
                    color: white;
                    font: bold;
                    padding-left: 4px;
                    border-top-left-radius: 1px;
                    border-top-right-radius: 1px;
                    border-right: 2px solid rgba(30, 30, 30, 150);
                }

                QHeaderView::section:last {
                    border-right: none;
                }
                '''
            )

    def updateTree(self, fields=[], size=[], headerAlign=QtCore.Qt.AlignLeft):
        self.setColumnCount(len(fields))
        self.headerItem = QtWidgets.QTreeWidgetItem(fields)
        self.setHeaderItem(self.headerItem)
        self.header().setDefaultAlignment(headerAlign)
        if size:
            for i in range(0, len(size)):
                if size[i]:
                    self.setColumnWidth(i, size[i])


################################################################################
# @brief      This class describes a rbw progress bar.
#
class RBWProgressBar(RBWWindow):
    def __init__(self, mainLenght=100):
        super(RBWProgressBar, self).__init__()

        self.cycles = {}
        self.errors = {}

        self.setMain(False)
        self.mainWidget.setPixmap(QtGui.QPixmap(''))
        self.setTitle('Progress')
        self.setIcon('waiting.png')
        self.setStyle()
        self.show()

        self.addCycle('Main', mainLenght, True)

    def addCycle(self, name, lenght, main=False):
        newCycle = RBWCycle(name, lenght, self, main)
        self.cycles[name] = newCycle
        self.resizeHeight()
        QtWidgets.QApplication.processEvents()

    def resizeHeight(self):
        if len(self.cycles.keys()):
            self.resize(500, (12 + (60 * len(self.cycles.keys())) + (3 * (len(self.cycles.keys()) - 1))))
            self.updateMargins()
        else:
            self.deleteLater()


################################################################################
# @brief      This class describes a cycle for the RBW's progress bar.
#
class RBWCycle(RBWFrame):
    def __init__(self, name, lenght, progressWidget, main=False):
        super(RBWCycle, self).__init__('V')

        self.name = name
        self.progressWidget = progressWidget
        self.currentProgress = 0
        self.errors = []

        self.bar = QtWidgets.QProgressBar()
        self.bar.setMaximum(lenght)
        self.subLayout = RBWFrame('H', bgColor='transparent', margins=[0, 0, 0, 0])
        self.label = RBWLabel('', size=10, bold=False)
        self.errorIcon = QtWidgets.QLabel()
        self.errorIcon.setPixmap(QtGui.QPixmap(os.path.join(iconPath, 'error.png').replace('\\', '/')).scaled(20, 20))
        self.errorLabel = RBWLabel('0')
        self.subLayout.addWidget(self.label, alignment=QtCore.Qt.AlignLeft)
        self.subLayout.addStretch()
        self.subLayout.addWidget(self.errorIcon, alignment=QtCore.Qt.AlignRight)
        self.subLayout.addWidget(self.errorLabel, alignment=QtCore.Qt.AlignRight)
        self.addWidget(self.bar)
        self.addWidget(self.subLayout)

        self.errorIcon.hide()
        self.errorLabel.hide()

        self.setFixedHeight(60)

        progressWidget.mainLayout.addWidget(self)

    def updateProgress(self, newValue=None):
        if not newValue:
            self.currentProgress += 1
        else:
            self.currentProgress = newValue

        self.progressWidget.setFocus()
        self.bar.setValue(self.currentProgress)
        QtWidgets.QApplication.processEvents()

        if self.currentProgress == self.bar.maximum():
            time.sleep(1)
            self.progressWidget.errors[self.name] = self.errors
            self.progressWidget.cycles.pop(self.name)
            self.progressWidget.resizeHeight()
            self.setParent(None)
            self.deleteLater()

    def setText(self, text):
        self.label.setText(text)

    def addError(self, text):
        self.errorIcon.show()
        self.errorLabel.show()

        self.errors.append(text)
        self.errorLabel.setText(str(len(self.errors)))
        self.errorIcon.setToolTip('\n'.join(self.errors))


# ================================================= CUSTOM RAINBOW DIALOGS ======================================================
class RBWDialog(QtWidgets.QDialog):
    def __init__(self, icon='info.png', title='Info', text='', widget=None, frame=None, frameScale=[100, 120], isMain=True, resizable=False, parent=api.system.getMayaWin()):
        super().__init__(parent)
        self.setAttribute(QtCore.Qt.WidgetAttribute.WA_Hover)

        if not frame:
            if os.path.exists(settingsPath):
                settings = api.json.json_read(settingsPath)
                if 'theme' in settings:
                    theme = api.json.json_read(os.path.join(jsonPath, 'themes.json').replace('\\', '/'))[settings['theme']]
                    try:
                        frame = [theme['frame'].split('ICONPATH/')[1].split(');')[0], '../backgrounds/info_background.png', 3]
                    except IndexError:
                        frame = ['', '../backgrounds/info_background.png', 3]
            else:
                frame = ['../backgrounds/rainbow.jfif', '../backgrounds/info_background.png', 3]

        self.moveFlag = False
        self.inRect = False
        self.resizable = True if resizable else False
        if resizable:
            self.margin = 10
            self.cursor = QtGui.QCursor()
            self.top = False
            self.bottom = False
            self.left = False
            self.right = False
            self.setMouseTracking(True)
            self.installEventFilter(self)

        self.text = text
        self.widget = widget

        # ========================== UI BUILDING ============================
        self.setWindowFlags(self.windowFlags() | QtCore.Qt.FramelessWindowHint)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        self.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)

        baseLayout = QtWidgets.QVBoxLayout(self)
        baseLayout.setContentsMargins(0, 0, 0, 0)

        frameBorder = QtWidgets.QWidget()
        frameBorder.setObjectName('MainFrame')
        frameBorder.setStyleSheet('QWidget#MainFrame {border-image: url("' + os.path.join(iconPath, frame[0]).replace('\\', '/') + '"); border-radius: 10px;}')
        baseLayout.addWidget(frameBorder)

        frameBox = QtWidgets.QVBoxLayout(frameBorder)
        frameBox.setContentsMargins(frame[2], frame[2], frame[2], frame[2])
        self.mainWidget = QtWidgets.QLabel()
        self.mainWidget.setSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Ignored)
        self.mainWidget.setObjectName('Background')
        self.mainWidget.setStyleSheet('QLabel#Background {background-color: #404040; border-radius: 8px;}')
        self.mainWidget.setPixmap(QtGui.QPixmap(os.path.join(iconPath, frame[1]).replace('\\', '/')).scaled(frameScale[0], frameScale[1]))
        self.mainWidget.setAlignment(QtCore.Qt.AlignCenter)
        frameBox.addWidget(self.mainWidget)

        mainLayout = QtWidgets.QVBoxLayout(self.mainWidget)
        mainLayout.setContentsMargins(3, 3, 3, 3)
        mainLayout.setSpacing(3)

        self.managementWidget = QtWidgets.QWidget()
        self.managementWidget.setStyleSheet('background-color: rgba(100, 100, 100, 150); border-radius: 5px')
        self.managementWidget.setFixedHeight(40)
        self.managementLayout = QtWidgets.QHBoxLayout(self.managementWidget)
        self.windowIcon = QtWidgets.QLabel()
        self.windowIcon.setStyleSheet('background-color: transparent')
        self.windowIcon.setPixmap(QtGui.QPixmap(os.path.join(iconPath, icon).replace('\\', '/')).scaled(20, 20))
        self.titleLabel = RBWLabel(title, 15)
        self.closeButton = RBWButton(icon=['close.png'], size=[20, 20], tooltip=['Close dialog'])
        self.closeButton.clicked.connect(self.closeDialog)
        self.managementLayout.addWidget(self.windowIcon)
        self.managementLayout.addWidget(self.titleLabel)
        self.managementLayout.addStretch()
        self.managementLayout.addWidget(self.closeButton)

        self.frameLayout = RBWFrame('V')
        self.contentWidget = RBWFrame('V', margins=[0, 0, 0, 0], bgColor='transparent')
        self.frameLayout.addWidget(self.contentWidget)

        mainLayout.addWidget(self.managementWidget)
        mainLayout.addWidget(self.frameLayout)

        image = QtGui.QIcon()
        image.addFile(os.path.join(iconPath, icon))
        self.setWindowIcon(image)
        self.setWindowTitle(title)

        css_path = os.path.join(os.getenv('LOCALPY'), 'api', 'widgets', 'rbwUI_style.css').replace('\\', '/')
        with open(css_path) as f:
            self.setStyleSheet(f.read())
        # ======================================================================

        self.populate()

        self.setFocus()

        if isMain:
            if text:
                api.log.logger().debug(text)
            self.exec_()

    def populate(self):
        self.label = None
        if self.text:
            label = RBWLabel(self.text, italic=False)

            scrollArea = QtWidgets.QScrollArea()
            scrollArea.setFrameShape(QtWidgets.QFrame.NoFrame)
            scrollArea.setWidgetResizable(True)
            scrollArea.setStyleSheet('background-color: transparent')

            scrollWidget = RBWFrame('V', margins=[0, 0, 0, 0], bgColor='transparent')
            scrollWidget.addWidget(label)

            scrollArea.setWidget(scrollWidget)

            self.contentWidget.addWidget(scrollArea)
            self.setSize(label)

        if self.widget:
            for widget in self.widget:
                self.contentWidget.addWidget(widget)
                self.setSize(widget)

            self.setMinimumSize(400, 300)

            if self.resizable:
                self.updateMargins()

    def closeDialog(self):
        self.reject()

    # ========================= DIALOG FUNCTIONING =============================
    def updateMargins(self):
        mainRect = self.rect()
        mainRectTL = QtCore.QPoint(mainRect.left(), mainRect.top())
        mainRectTR = QtCore.QPoint((mainRect.right() - self.margin + 1), mainRect.top())
        mainRectBL = QtCore.QPoint(mainRect.left(), (mainRect.bottom() - self.margin + 1))
        mainRectBR = QtCore.QPoint((mainRect.right() - self.margin + 1), (mainRect.bottom() - self.margin + 1))
        mainRectTC = QtCore.QPoint((mainRect.left() + self.margin), mainRect.top())
        mainRectBC = QtCore.QPoint((mainRect.left() + self.margin), (mainRect.bottom() - self.margin + 1))
        mainRectCL = QtCore.QPoint(mainRect.left(), (mainRect.top() + self.margin))
        mainRectCR = QtCore.QPoint((mainRect.right() - self.margin + 1), (mainRect.top() + self.margin))

        self.TL = QtCore.QRect(mainRectTL, QtCore.QSize(self.margin, self.margin))
        self.TR = QtCore.QRect(mainRectTR, QtCore.QSize(self.margin, self.margin))
        self.BL = QtCore.QRect(mainRectBL, QtCore.QSize(self.margin, self.margin))
        self.BR = QtCore.QRect(mainRectBR, QtCore.QSize(self.margin, self.margin))
        self.TC = QtCore.QRect(mainRectTC, QtCore.QSize(mainRect.right(), self.margin))
        self.BC = QtCore.QRect(mainRectBC, QtCore.QSize(mainRect.right(), self.margin))
        self.CL = QtCore.QRect(mainRectCL, QtCore.QSize(self.margin, mainRect.bottom()))
        self.CR = QtCore.QRect(mainRectCR, QtCore.QSize(self.margin, mainRect.bottom()))

    def setMargins(self, value):
        self.margin = value
        self.updateMargins()

    def isResizable(self):
        return self.resizable

    def setResizable(self, value):
        self.resizable = value

    def eventFilter(self, source, event):
        if event.type() == QtGui.QHoverEvent.HoverMove:
            if self.isResizable():
                mousePos = event.pos()
                self.inRect = True
                if self.TL.contains(mousePos) or self.BR.contains(mousePos):
                    self.cursor.setShape(QtCore.Qt.SizeFDiagCursor)
                    self.top = True if self.TL.contains(mousePos) else False
                    self.bottom = True if self.BR.contains(mousePos) else False
                    self.left = True if self.TL.contains(mousePos) else False
                    self.right = True if self.BR.contains(mousePos) else False

                elif self.TR.contains(mousePos) or self.BL.contains(mousePos):
                    self.cursor.setShape(QtCore.Qt.SizeBDiagCursor)
                    self.top = True if self.TR.contains(mousePos) else False
                    self.bottom = True if self.BL.contains(mousePos) else False
                    self.left = True if self.BL.contains(mousePos) else False
                    self.right = True if self.TR.contains(mousePos) else False

                elif self.TC.contains(mousePos) or self.BC.contains(mousePos):
                    self.cursor.setShape(QtCore.Qt.SizeVerCursor)
                    self.top = True if self.TC.contains(mousePos) else False
                    self.bottom = True if self.BC.contains(mousePos) else False
                    self.left = False
                    self.right = False

                elif self.CL.contains(mousePos) or self.CR.contains(mousePos):
                    self.cursor.setShape(QtCore.Qt.SizeHorCursor)
                    self.top = False
                    self.bottom = False
                    self.left = True if self.CL.contains(mousePos) else False
                    self.right = True if self.CR.contains(mousePos) else False

                else:
                    self.inRect = False
                    self.cursor.setShape(QtCore.Qt.ArrowCursor)
                    self.top = False
                    self.bottom = False
                    self.left = False
                    self.right = False

                self.setCursor(self.cursor)

        return super().eventFilter(source, event)

    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            if self.inRect:
                self.resizeWindow()
                self.moveFlag = False
            else:
                self.moveFlag = True
                self.movePosition = event.globalPos() - self.pos()
                event.accept()

    def mouseMoveEvent(self, event):
        if QtCore.Qt.LeftButton and self.moveFlag:
            self.move(event.globalPos() - self.movePosition)
            event.accept()

    def mouseReleaseEvent(self, QMouseEvent):
        self.moveFlag = False
        if self.isResizable():
            self.updateMargins()
            self.inRect = False

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Enter:
            return

    def resizeWindow(self):
        window = self.window().windowHandle()
        if self.cursor.shape() == QtCore.Qt.SizeHorCursor:
            if self.left:
                window.startSystemResize(QtCore.Qt.LeftEdge)
            elif self.right:
                window.startSystemResize(QtCore.Qt.RightEdge)
        elif self.cursor.shape() == QtCore.Qt.SizeVerCursor:
            if self.top:
                window.startSystemResize(QtCore.Qt.TopEdge)
            elif self.bottom:
                window.startSystemResize(QtCore.Qt.BottomEdge)
        elif self.cursor.shape() == QtCore.Qt.SizeBDiagCursor:
            if self.top and self.right:
                window.startSystemResize(QtCore.Qt.TopEdge | QtCore.Qt.RightEdge)
            elif self.bottom and self.left:
                window.startSystemResize(QtCore.Qt.BottomEdge | QtCore.Qt.LeftEdge)
        elif self.cursor.shape() == QtCore.Qt.SizeFDiagCursor:
            if self.top and self.left:
                window.startSystemResize(QtCore.Qt.TopEdge | QtCore.Qt.LeftEdge)
            elif self.bottom and self.right:
                window.startSystemResize(QtCore.Qt.BottomEdge | QtCore.Qt.RightEdge)

        self.updateMargins()

    def setSize(self, widget):
        widget.resize(widget.minimumSizeHint())
        width = 200 if widget.width() < 200 else (widget.width() + 45)
        height = 150 if widget.height() < 150 else widget.height() + 96
        self.resize(width, height)

        if self.resizable:
            self.updateMargins()
    # ==========================================================================


class RBWConfirm(RBWDialog):
    def __init__(self, defOk=['Ok', 'ok.png'], defCancel=['Cancel', 'cancel.png'], buttons=[], **params):
        isMain = False if 'isMain' in params.keys() else True
        params['isMain'] = False

        self.buttons = {}

        super().__init__(**params)

        layout = QtWidgets.QDialogButtonBox()
        layout.setCenterButtons(True)

        if defOk:
            okButton = RBWButton(text=defOk[0], icon=[defOk[1]], size=[None, 30])
            okButton.clicked.connect(self.accept)
            self.buttons[defOk[0]] = okButton
            layout.addButton(okButton, QtWidgets.QDialogButtonBox.AcceptRole)

        for button in buttons:
            widget = RBWButton(text=button[0], icon=[button[1]], size=[None, 30])
            widget.clicked.connect(button[2])
            self.buttons[button[0]] = widget
            layout.addButton(widget, QtWidgets.QDialogButtonBox.ActionRole)

        if defCancel:
            cancelButton = RBWButton(text=defCancel[0], icon=[defCancel[1]], size=[None, 30])
            cancelButton.clicked.connect(self.reject)
            self.buttons[defCancel[0]] = cancelButton
            layout.addButton(cancelButton, QtWidgets.QDialogButtonBox.RejectRole)

        self.frameLayout.addWidget(layout)

        if isMain:
            self.exec_()


class RBWWarning(RBWConfirm):
    def __init__(self, **params):
        params['isMain'] = False
        params['icon'] = 'warning.png'
        params['title'] = 'Warning'
        params['frame'] = ['../backgrounds/yellow.png', '../backgrounds/warning_background.png', 5]

        super(RBWWarning, self).__init__(**params)
        api.log.logger().warning(params['text'])
        self.exec_()


class RBWError(RBWConfirm):
    jokeButtons = [
        ['OH NO!', 'cancel.png'],
        ['Oh Dear...', '../jokes/deer.png'],
        ['F**k!', '../jokes/fuck.png'],
        ['Crap!', '../jokes/crap.png'],
        ['No way!', '../jokes/noway.png'],
        ['Time for a coffee then...', '../jokes/coffee.png'],
        ['God help me...', '../jokes/god.png'],
        ['DOH!', '../jokes/homer.png'],
    ]

    def __init__(self, **params):
        from random import randint

        params['isMain'] = False
        params['icon'] = 'error.png'
        params['title'] = 'Error'
        params['frame'] = ['../backgrounds/red.png', '../backgrounds/error_background.png', 5]
        params['defOk'] = None
        params['defCancel'] = self.jokeButtons[randint(0, (len(self.jokeButtons) - 1))]

        super(RBWError, self).__init__(**params)
        api.log.logger().error(params['text'])
        self.exec_()


class RBWInput(RBWConfirm):
    def __init__(self, mode, **params):
        self.mode = mode

        params['isMain'] = False
        params['resizable'] = True
        params['defCancel'] = None
        params['widget'] = self.initUI()

        super(RBWInput, self).__init__(**params)
        self.exec_()

    def initUI(self):
        self.modes = {}
        widgets = []
        for value in self.mode.keys():
            mode = self.mode[value]
            if mode in ['string', 'int', 'float']:
                widget = RBWLineEdit(title=value, toolTip='', bgColor='transparent')
                widgets.append(widget)
                self.modes[value] = widget
            elif mode == 'text':
                widget = RBWTextEdit(title=value, toolTip='', bgColor='transparent')
                widgets.append(widget)
                self.modes[value] = widget

        return widgets

    def result(self):
        results = {}
        for value in self.mode.keys():
            mode = self.mode[value]
            if mode in ['string', 'text']:
                results[value] = str(self.modes[value].text())
            elif mode == 'int':
                results[value] = int(self.modes[value].text())
            elif mode == 'float':
                results[value] = float(int(self.modes[value].text()))

        return results


class RBWChoise(RBWConfirm):
    def __init__(self, mode, **params):
        self.mode = mode

        params['isMain'] = False
        params['resizable'] = True
        params['defCancel'] = None
        params['widget'] = self.initUI()

        super(RBWChoise, self).__init__(**params)
        self.exec_()

    def initUI(self):
        self.modes = {}
        widgets = []
        for value in self.mode.keys():
            modeDict = self.mode[value]
            mode = modeDict['type']
            items = modeDict['items']

            if mode == 'combo':
                widget = RBWComboBox(text=value, items=items, bgColor='transparent')
                widgets.append(widget)
                self.modes[value] = widget

        return widgets

    def result(self):
        results = {}
        for value in self.mode.keys():
            mode = self.mode[value]['type']
            if mode == 'combo':
                results[value] = self.modes[value].currentText()

        return results


################################################################################
# @brief      This class describes a rbw preview panel.
#
class RBWPreviewPanel(RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      parent     The parent
    # @param      imagePath  The image path
    #
    def __init__(self, parent, imagePath):

        super().__init__()

        if cmd.window('PreviewPanel', exists=True):
            cmd.deleteUI('PreviewPanel')

        self.imagePath = imagePath

        self.setObjectName('PreviewPanel')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topWidget = RBWFrame(layout='V', radius=5)

        self.photoViewer = RBWPhotoViewer(self, self.imagePath)
        self.imageLabel = RBWLabel(os.path.basename(self.imagePath).split('.')[0])
        self.topWidget.addWidget(self.photoViewer, alignment=QtCore.Qt.AlignHCenter)
        self.topWidget.addWidget(self.imageLabel, alignment=QtCore.Qt.AlignHCenter)

        self.mainLayout.addWidget(self.topWidget)

        self.setFixedSize(502, 582)

        self.setTitle('Preview Snapshot UI')
        self.setIcon('../arakno/loadAndSave.png')
        self.setFocus()


################################################################################
# @brief      This class describes a rbw photo viewer.
#
class RBWPhotoViewer(QtWidgets.QGraphicsView):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      parent     The parent
    # @param      imagePath  The image path
    #
    def __init__(self, parent, imagePath):
        super().__init__(parent)
        self.zoom = 0
        self.imagePath = imagePath
        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.scene = QtWidgets.QGraphicsScene(self)
        self.photo = QtWidgets.QGraphicsPixmapItem()
        self.scene.addItem(self.photo)
        self.setScene(self.scene)
        self.setTransformationAnchor(QtWidgets.QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QtWidgets.QGraphicsView.AnchorUnderMouse)
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setBackgroundBrush(QtGui.QBrush(QtGui.QColor(30, 30, 30)))
        self.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.setFixedSize(480, 480)
        self.setPhoto(QtGui.QPixmap(self.imagePath))

    ############################################################################
    # @brief      { function_description }
    #
    #
    def fitInView(self):
        # sostituito con la dimenzione data ini initUI
        # viewrect = self.viewport().rect()
        rect = QtCore.QRectF(self.photo.pixmap().rect())
        if not rect.isNull():
            self.setSceneRect(rect)
            unity = self.transform().mapRect(QtCore.QRectF(0, 0, 1, 1))
            self.scale(1 / unity.width(), 1 / unity.height())
            scenerect = self.transform().mapRect(rect)
            factor = min(480 / scenerect.width(), 480 / scenerect.height())
            self.scale(factor, factor)
            self.zoom = 0

    ############################################################################
    # @brief      Sets the photo.
    #
    # @param      pixmap  The pixmap
    #
    def setPhoto(self, pixmap=None):
        self.zoom = 0
        self.setDragMode(QtWidgets.QGraphicsView.ScrollHandDrag)
        self.photo.setPixmap(pixmap)
        self.fitInView()

    ############################################################################
    # @brief      { function_description }
    #
    # @param      event  The event
    #
    def wheelEvent(self, event):
        if event.angleDelta().y() > 0:
            factor = 1.25
            self.zoom += 1
        else:
            factor = 0.8
            self.zoom -= 1
        if self.zoom > 0:
            self.scale(factor, factor)
        elif self.zoom == 0:
            self.fitInView()
        else:
            self.zoom = 0
