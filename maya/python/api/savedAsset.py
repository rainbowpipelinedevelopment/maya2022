import importlib
from datetime import datetime
import time
import os

import maya.cmds as cmd

import api.system
import api.asset
import api.saveNode
import api.assembly
import api.json
import api.exception
import api.widgets.rbw_UI as rbwUI

import common.cleanupMayaScriptJobs as cleanupMayaScriptJobs

importlib.reload(api.system)
# importlib.reload(api.asset)
importlib.reload(api.saveNode)
# importlib.reload(api.assembly)
importlib.reload(api.json)
importlib.reload(api.exception)
# importlib.reload(rbwUI)
importlib.reload(cleanupMayaScriptJobs)


################################################################################
# @brief      This class describes a saved asset.
#
class SavedAsset(object):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      params          Params
    # @param      saveNode        The save node
    #
    def __init__(self, params=None, saveNode=None):

        super(SavedAsset, self).__init__()

        if saveNode:
            savedAssetParams = self.createFromSaveNode(saveNode)

        elif 'path' in params:
            savedAssetParams = self.createFromParam(path=params['path'])

        elif 'db_id' in params and 'approvation' in params:
            savedAssetParams = self.createFromParam(id=params['db_id'], approvation=params['approvation'])

        else:
            savedAssetParams = params.copy()
            self.generateDbId()

            if 'parent' not in savedAssetParams.keys():
                savedAssetParams['parent'] = ''

        if savedAssetParams:
            for label in savedAssetParams:

                name = label
                value = savedAssetParams[label]
                setattr(self, name, value)
        else:
            raise api.exception.InvalidSavedAsset('The saved asset params are wrong or maybe the save doen\'t exists')

    ############################################################################
    # @brief      Returns a dictionary representation of the object.
    #
    # @return     Dictionary representation of the object.
    #
    def toDict(self):
        toDict = self.__dict__
        toDict['asset'] = self.asset.toDict()
        return toDict

    ############################################################################
    # @brief      Creates a from save node.
    #
    # @param      saveNode  The save node
    #
    def createFromSaveNode(self, saveNode):
        savedAssetParams = {}

        savedAssetParams['db_id'] = cmd.getAttr('{}.db_id'.format(saveNode))
        savedAssetParams['dept'] = cmd.getAttr('{}.dept'.format(saveNode))
        savedAssetParams['note'] = cmd.getAttr('{}.note'.format(saveNode))
        savedAssetParams['parent'] = cmd.getAttr('{}.parent'.format(saveNode))
        savedAssetParams['approvation'] = cmd.getAttr('{}.approvation'.format(saveNode))
        savedAssetParams['date'] = cmd.getAttr('{}.date'.format(saveNode))
        savedAssetParams['version'] = cmd.getAttr('{}.version'.format(saveNode))
        savedAssetParams['deptType'] = cmd.getAttr('{}.type'.format(saveNode))

        try:
            savedAssetParams['asset'] = api.asset.Asset({
                'category': cmd.getAttr('{}.genre'.format(saveNode)),
                'group': cmd.getAttr('{}.group'.format(saveNode)),
                'name': cmd.getAttr('{}.name'.format(saveNode)),
                'variant': cmd.getAttr('{}.variant'.format(saveNode)),
                'id_mv': cmd.getAttr('{}.id_mv'.format(saveNode))
            })
        except api.exception.InvalidAsset:
            savedAssetParams['asset'] = None

        pathQuery = "SELECT `path` FROM `savedAsset` WHERE `id`='{}' AND `visible`= 1".format(savedAssetParams['db_id'])
        if savedAssetParams['approvation'] == 'wip':
            pathQuery = pathQuery.replace("`savedAsset`", "`savedAsset_wip`")
        try:
            savedAssetParams['path'] = api.database.selectSingleQuery(pathQuery)[0]
        except TypeError:
            raise api.exception.InvalidSavedAsset('The saved asset params are wrong or maybe the save doen\'t exists')

        # temporany hack, removed in 2 month
        if savedAssetParams['dept'] == 'vfx':
            savedAssetParams['dept'] = 'fx'

        return savedAssetParams

    ############################################################################
    # @brief      Creates a from param.
    #
    # @param      param  The param
    #
    def createFromParam(self, path=None, id=None, approvation=None):
        savedAssetParams = {}

        if path:
            savedAsset, approvation = self.getDataFromPath(path)
        else:
            savedAsset, approvation = self.getDataFromId(id, approvation)

        if savedAsset:

            savedAssetParams['db_id'] = savedAsset[0]
            savedAssetParams['dept'] = savedAsset[1]
            savedAssetParams['path'] = savedAsset[2]
            savedAssetParams['version'] = savedAsset[3]
            savedAssetParams['deptType'] = savedAsset[4]
            savedAssetParams['approvation'] = approvation
            savedAssetParams['date'] = savedAsset[8].strftime("%Y.%m.%d-%H.%M.%S")
            savedAssetParams['note'] = savedAsset[9]
            savedAssetParams['parent'] = savedAsset[10]

            try:
                savedAssetParams['asset'] = api.asset.Asset({'id': savedAsset[5], 'id_mv': savedAsset[12]})
            except api.exception.InvalidAsset:
                savedAssetParams['asset'] = None

        return savedAssetParams

    ############################################################################
    # @brief      Gets the data from identifier.
    #
    # @param      id           The identifier
    # @param      approvation  The approvation
    #
    # @return     The data from identifier.
    #
    def getDataFromId(self, id, approvation):
        savedAssetQuery = "SELECT * FROM `savedAsset` WHERE `id` = '{}' AND `visible`=1".format(id)
        if approvation == 'wip':
            savedAssetQuery = savedAssetQuery.replace('savedAsset', 'savedAsset_wip')

        savedAsset = api.database.selectSingleQuery(savedAssetQuery)

        return savedAsset, approvation

    ############################################################################
    # @brief      Gets the data from path.
    #
    # @param      path  The path
    #
    # @return     The data from path.
    #
    def getDataFromPath(self, path):
        savedAssetQuery = "SELECT * FROM `savedAsset` WHERE `path` LIKE '%{}%' AND `id_mv`={} AND `visible`=1".format(path, os.getenv('ID_MV'))
        savedAsset = api.database.selectSingleQuery(savedAssetQuery)
        approvation = 'def'

        if not savedAsset:
            savedAssetQuery = savedAssetQuery.replace('savedAsset', 'savedAsset_wip')
            savedAsset = api.database.selectSingleQuery(savedAssetQuery)
            approvation = 'wip'

        return savedAsset, approvation

    ############################################################################
    # @brief      Gets the available versions.
    #
    # @return     The available versions.
    #
    def getAvailableVersions(self):
        versionsQuery = "SELECT * FROM `savedAsset` WHERE `assetId` = '{}' AND `dept` = '{}' AND `deptType` = '{}' AND `visible`=1 ORDER BY `date` DESC".format(
            self.asset.variantId, self.dept, self.deptType
        )
        if self.approvation == 'wip':
            versionsQuery = versionsQuery.replace('`savedAsset`', '`savedAsset_wip`')

        versions = api.database.selectQuery(versionsQuery)

        return versions

    ############################################################################
    # @brief      Gets the latest.
    #
    # @return     The latest.
    #
    def getLatest(self):
        versions = self.getAvailableVersions()

        if versions:
            return versions[0]
        else:
            return None

    ############################################################################
    # @brief      increment the saved asset version.
    #
    def incrementVersion(self):
        latest = self.getLatest()
        if latest:
            system = latest[6]
            versionString = latest[3]

            versionNumber = int(versionString.split(system)[1])

            self.version = 'v{}{}'.format(system, str(versionNumber + 1).zfill(3))
        else:
            self.version = 'vr001'

    ############################################################################
    # @brief      generate a new temporal id for DB.
    #
    def generateDbId(self):
        dateVar = datetime.fromtimestamp(time.time())
        self.date = dateVar.strftime("%Y.%m.%d-%H.%M.%S")
        utc_time = time.mktime(dateVar.timetuple())
        self.db_id = '{}{}r'.format(int(utc_time), str(api.system.getUserId()).zfill(3))

    ############################################################################
    # @brief      Gets the dept object.
    #
    # @return     The dept object.
    #
    def getDeptObject(self):
        import depts.modeling.mc_modeling as mc_modeling
        import depts.rigging.mc_rigging as mc_rigging
        import depts.surfacing.mc_surfacing as mc_surfacing
        import depts.setdressing.mc_setdressing as mc_setdressing
        import depts.grooming.mc_grooming as mc_grooming
        import depts.fx.mc_fx as mc_fx

        importlib.reload(mc_modeling)
        importlib.reload(mc_rigging)
        importlib.reload(mc_surfacing)
        importlib.reload(mc_setdressing)
        importlib.reload(mc_grooming)
        importlib.reload(mc_fx)

        initDict = {
            'modeling': mc_modeling.Modeling(self),
            'grooming': mc_grooming.Grooming(self),
            'surfacing': mc_surfacing.Surfacing(self),
            'rigging': mc_rigging.Rigging(self),
            'fx': mc_fx.Fx(self),
            'set': mc_setdressing.Set(self)
        }

        return initDict[self.dept]

    ############################################################################
    # @brief      populate the save node.
    #
    def populateSaveNode(self):
        # costruisco il saveNode
        self.saveNode = api.saveNode.SaveNode()
        self.saveNode.create(self.dept)

        self.saveDict = {
            'db_id': self.db_id,
            'group': self.asset.group,
            'name': self.asset.name,
            'dept': self.dept,
            'share': 'speed.nas',
            'note': self.note,
            'variant': self.asset.variant,
            'parent': '',
            'approvation': self.approvation,
            'project': self.asset.projectName,
            'date': self.date,
            'id_mv': self.asset.id_mv,
            'version': self.version,
            'user': os.getenv('USERNAME'),
            'genre': self.asset.category,
            'type': self.deptType,
            'asset_id': self.asset.variantId,
            'shotgun_id': self.asset.shotgunId
        }

        for label in self.saveDict:
            self.saveNode.setAttr(label, self.saveDict[label])

    ############################################################################
    # @brief      Saves on db.
    #
    def saveOnDB(self):
        self.relPath = os.path.join(self.savedAssetFolder.split('mc_{}/'.format(os.getenv('PROJECT')))[1], '{}.mb'.format(self.sceneName)).replace('\\', '/')
        args = (
            self.db_id,
            self.dept,
            self.relPath,
            self.version,
            self.deptType,
            self.asset.variantId,
            'r',
            os.getenv('USERNAME'),
            self.date,
            self.note,
            1,
            self.parent,
            self.asset.id_mv,
            'maya{}'.format(cmd.about(v=True))
        )

        fields = 'id, dept, path, version, deptType, assetId, system, user, date, note, visible, parent, id_mv, softwareVersion'

        pers = ", ".join(["\'{}\'".format(o) for o in args])

        if self.approvation == 'def':
            dbName = 'savedAsset'
        else:
            dbName = 'savedAsset_wip'

        query = 'INSERT INTO %s (%s) VALUES (%s) ' % (dbName, fields, pers)

        api.log.logger().debug(query)

        api.database.insertQuery(query)

    ############################################################################
    # @brief      save function.
    #
    def save(self):
        # get all infoes
        self.generateDbId()
        self.incrementVersion()

        self.savedAssetFolder = os.path.join(
            self.asset.getAssetFolder(),
            self.dept,
            self.deptType,
            self.approvation,
            self.version
        ).replace('\\', '/')

        if not os.path.exists(self.savedAssetFolder):
            os.makedirs(self.savedAssetFolder)

        self.sceneName = '{}_{}_{}_{}_{}_{}_{}'.format(self.dept, self.asset.category, self.asset.group, self.asset.name, self.asset.variant, self.deptType, self.version)

        # populate savenode
        self.populateSaveNode()

        # write json file
        saveJson = os.path.join(
            self.savedAssetFolder,
            '{}.json'.format(self.sceneName)
        ).replace('\\', '/')

        api.json.json_write(self.saveDict, saveJson)

        # get saver
        self.deptObject = self.getDeptObject()

        # get top group
        topGroup = self.deptObject.getGroupName()
        if topGroup or self.getCategory() == 'shader':

            if self.getCategory() != 'shader':
                connection = '{}.creator'.format(topGroup)
                self.saveNode.connect(connection)

            # save
            if self.deptObject.save():
                self.saveOnDB()

                if self.approvation == 'def':
                    if self.dept in ['modeling', 'surfacing']:
                        if self.asset.category in ["prop", "set"]:
                            if self.deptType in ["hig", "prx", "rnd", "txt"]:
                                assembly = api.assembly.Assembly(params={'asset': self.asset})
                                assembly.create()
                                assembly.save()

                                cmd.file(saveJson.replace('.json', '.mb'), open=True, force=True)

                return True
        else:
            # comunicazione errore
            rbwUI.RBWError(title='ATTENTION', text='More than one top group in scene.')
            return

    ############################################################################
    # @brief      load function
    #
    # @param      mode    The mode (load, merge, reference)
    # @param      params  The parameters
    #
    def load(self, mode='load', params=None):
        self.deptObject = self.getDeptObject()
        self.deptObject.load(mode=mode, params=params)

        cleanApp = cleanupMayaScriptJobs.cleanupMayaScriptJobs()
        cleanApp.delete_worm_nodes()
        api.log.logger().debug("..eventual cleanup for worms is done...")

    ############################################################################
    # @brief      Gets the asset.
    #
    # @return     The asset.
    #
    def getAsset(self):
        return self.asset

    ############################################################################
    # @brief      Gets the category.
    #
    # @return     The category.
    #
    def getCategory(self):
        return self.asset.category

    ############################################################################
    # @brief      Gets the group.
    #
    # @return     The group.
    #
    def getGroup(self):
        return self.asset.group

    ############################################################################
    # @brief      Gets the name.
    #
    # @return     The name.
    #
    def getName(self):
        return self.asset.name

    ############################################################################
    # @brief      Gets the variant.
    #
    # @return     The variant.
    #
    def getVariant(self):
        return self.asset.variant

    ############################################################################
    # @brief      Gets the category identifier.
    #
    # @return     The category identifier.
    #
    def getCategoryId(self):
        return self.asset.categoryId

    ############################################################################
    # @brief      Gets the group identifier.
    #
    # @return     The group identifier.
    #
    def getGroupId(self):
        return self.asset.groupId

    ############################################################################
    # @brief      Gets the name identifier.
    #
    # @return     The name identifier.
    #
    def getNameId(self):
        return self.asset.nameId

    ############################################################################
    # @brief      Gets the asset identifier.
    #
    # @return     The asset identifier.
    #
    def getAssetId(self):
        return self.asset.variantId

    ############################################################################
    # @brief      Gets the shotgun identifier.
    #
    # @return     The shotgun identifier.
    #
    def getShotgunId(self):
        return self.asset.shotgunId

    ############################################################################
    # @brief      Gets the name space.
    #
    # @return     The namespace.
    #
    def getNamespace(self):
        return '{}_{}_{}_'.format(self.getGroup(), self.getName(), self.getVariant())
