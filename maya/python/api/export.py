import importlib
import os

import maya.cmds as cmd

import api.scene
importlib.reload(api.scene)


################################################################################
# @brief      export ma file.
#
# @param      filename  The filename
# @param      selected  The selected
#
# @return     the result fileName.
#
def export_ma(filename, selected=True, preserveReferences=False):
    scene_dirname = api.scene.dirname()

    if not filename:
        filename = os.path.join(scene_dirname, 'exported.ma')

    cmd.file(rename=filename)
    cmd.file(force=True, exportSelected=selected, preserveReferences=preserveReferences, type='mayaAscii')

    return filename
