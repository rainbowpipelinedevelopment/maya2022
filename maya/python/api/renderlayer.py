import importlib
import os
import json

import maya.app.renderSetup.model.override as override
import maya.app.renderSetup.model.selector as selector
import maya.app.renderSetup.model.collection as collection
import maya.app.renderSetup.model.renderLayer as renderLayer
import maya.app.renderSetup.model.renderSetup as renderSetup
import maya.app.renderSetup.model.utils as utils
import maya.app.renderSetup.model.typeIDs as typeIDs
import maya.app.renderSetup.views.overrideUtils as overrideUtils

import maya.cmds as cmd
import maya.mel as mel

import api.log
import api.vray
import api.json

importlib.reload(api.log)
importlib.reload(api.vray)
importlib.reload(api.json)


# C:\Program Files\Autodesk\Maya2022\Python37\Lib\site-packages\maya\app\renderSetup\model
################################################################################
# @brief      Returns the name of the default render layer or False if is not
#             present in the scene
#
# @return     The name of the default render layer, False otherwise
#
def getDefaultRenderLayer():
    return cmd.ls('defaultRenderLayer')[0] or False


################################################################################
# @brief      Returns the list of all the render layers that are in the current
#             scene and are not referenced.
#
# @return     The list of the render layers in the current scene
#
def list():
    renderLayers = cmd.renderSetup(q=True, renderLayers=True)
    return renderLayers if renderLayers else []


################################################################################
# @brief      Switch to the render layer specified
#
# @param      layer  The layer
#
# @return     The current render layer
#
def switch(layer=None):
    if not layer.startswith('rs_') and 'defaultRenderLayer' not in layer:
        layer = 'rs_{}'.format(layer)
    rs = renderSetup.instance()
    rs.switchToLayerUsingLegacyName(layer)
    return getCurrent()


###############################################################################
# @brief      Switch to the render layer specified
#
# @param      layer_name  The layer name
#
# @return     The current render layer
#
def switchByName(layer_name=''):
    if not layer_name.startswith('rs_') and 'defaultRenderLayer' not in layer_name:
        layer_name = 'rs_{}'.format(layer_name)
    return switch(layer_name)


################################################################################
# @brief      Returns the current selected render layer.
#
# @return     The name of the current render layer
#
def getCurrent():
    return cmd.editRenderLayerGlobals(query=True, currentRenderLayer=True)[3:]


################################################################################
# @brief      Sets the current layer as renderable or not.
#
# @param      layer       The layer
# @param      renderable  The renderable
#
# @return     True if the layer is renderable, False otherwise
#
def setRenderable(layer=None, renderable=True):
    if not layer.startswith('rs_') and 'defaultRenderLayer' not in layer:
        layer = 'rs_{}'.format(layer)
    cmd.setAttr('{}.renderable'.format(layer), renderable)
    return isRenderable(layer)


################################################################################
# @brief      Returns if the layer specified is renderable or not.
#
# @param      layer  The layer
#
# @return     True if the layer is renderable, False otherwise
#
def isRenderable(layer=None):
    if not layer.startswith('rs_') and 'defaultRenderLayer' not in layer:
        layer = 'rs_{}'.format(layer)
    return cmd.getAttr('{}.renderable'.format(layer))


################################################################################
# @brief      Creates an override.
#
# @param      layer      The layer
# @param      node       The node
# @param      attribute  The attribute
#
# @return     { description_of_the_return_value }
#
def createOverride(layer=None, node=None, attribute=None):
    if not layer.startswith('rs_') and 'defaultRenderLayer' not in layer:
        layer = 'rs_{}'.format(layer)
    cmd.editRenderLayerAdjustment('{}.{}'.format(node, attribute), layer='{}'.format(layer))


################################################################################
# @brief      Gets the render layer member.
#
# @param      layer  The layer
#
# @return     The render layer member.
#
def getRenderLayerMember(layer=None):
    if not layer.startswith('rs_') and 'defaultRenderLayer' not in layer:
        layer = 'rs_{}'.format(layer)
    firstMember = cmd.editRenderLayerMembers('{}'.format(layer), query=True, fullNames=True)
    api.log.logger().debug(firstMember)
    allMember = []
    if firstMember is not None:
        for member in firstMember:
            children = cmd.listRelatives(member, fullPath=True, allDescendents=True)
            if children is not None:
                allMember = allMember + children

    return allMember


################################################################################
# @brief      Determines if a vrscene.
#
# @param      layerName  The layer name
#
# @return     True if a vrscene, False otherwise.
#
def hasAVrscene(layerName):
    layerMembers = getRenderLayerMember(layerName)

    for member in layerMembers:
        if member.startswith('|VRSCENE'):
            return True

    return False


################################################################################
# @brief      Gets the render layer collections.
#
# @param      layerName  The layer name
#
# @return     The render layer collections.
#
def getRenderLayerCollections(layerName):
    rs = renderSetup.instance()
    layer = rs.getRenderLayer(layerName)

    children = layer.getChildren()

    return children


################################################################################
# @brief      Gets the render layer collection by name.
#
# @param      layerName       The layer name
# @param      collectionType  The collection type
#
# @return     The render layer collection by name.
#
def getRenderLayerCollectionByName(layerName, collectionType='geo'):
    children = getRenderLayerCollections(layerName)
    for child in children:
        if collectionType in child.name():
            return child


################################################################################
# @brief      Gets the sub collection by collection name.
#
# @param      collectionNode  The collection node
#
# @return     The sub collection by collection name.
#
def getSubCollectionByCollectionName(collectionNode):
    return collectionNode.getCollections()


################################################################################
# @brief      Gets the AOVs overrides by layer name.
#
# @param      layer  The layer
#
# @return     The AOVs overrides by layer name.
#
def getAOVsOverridesByLayerName(layer):
    returnAovsOverrides = {}
    aovsCollection = getRenderLayerCollectionByName(layer, 'AOV')
    subCollections = getSubCollectionByCollectionName(aovsCollection)

    for subCollection in subCollections:
        aovOverrides = subCollection.getOverrides()
        for aovOverride in aovOverrides:
            returnAovsOverrides[aovOverride.name()] = aovOverride

    return returnAovsOverrides


################################################################################
# @brief      recursive renamer.
#
# @param      rootNode  The root node
# @param      oldName   The old name
# @param      newName   The new name
#
def recursiveRename(rootNode, oldName, newName):
    rootNode.setName(rootNode.name().replace(oldName[:-2], newName[:-2]))
    try:
        children = rootNode.getChildren()
        if len(children) > 0:
            for child in children:
                recursiveRename(child, oldName, newName)
    except AttributeError:
        pass


################################################################################
# @brief      Creates a layer from template.
#
# @param      layerType        The layer type
# @param      layerName        The layer name
# @param      tobIndex         The tod index
# @param      renderTopGroups  The render tops groups
#
def createFromTemplate(layerType, layerName, todIndex=0, renderTopGroups=''):
    jsonFileFolder = os.path.join(os.getenv('LOCALPIPE'), 'json_file').replace('\\', '/')
    templatePath = os.path.join(jsonFileFolder, 'template_{}.json'.format(layerType)).replace('\\', '/')
    realLayerName = '{}_{}_RL'.format(layerType, layerName)

    if layerType == 'LOOKDEV':
        templateName = 'LOOKDEV_tod_camera_RL'
    else:
        templateName = '{}_template_RL'.format(layerType)

    importFromTemplate(templatePath)

    rs = renderSetup.instance()
    renderLayer = rs.getRenderLayer(templateName)
    renderLayer.setName(realLayerName)
    renderLayerChildren = renderLayer.getChildren()
    for child in renderLayerChildren:
        if templateName != realLayerName:
            recursiveRename(child, templateName, realLayerName)

    if layerType not in ['slapcomp', 'custom_slapcomp', 'SHADER', 'FUR', 'LOOKDEV']:
        renderElements = api.vray.render_elements()
        renderElementsSettingsPath = '//speed.nas/pun/02_production/01_content/_config/lightRig/renderElementsSettings/renderElements_settings_v001.json'
        # renderElementsSettingsPath = os.path.join(manageRenderElements.renderElementsSettingsFolder, 'renderElements_settings_v{}.json'.format(str(manageRenderElements.getRenderElementsSettingsVersion()).zfill(3)))
        renderElementsSettingsData = api.json.json_read(renderElementsSettingsPath)

        layerData = renderElementsSettingsData[layerType]
        for renderElement in renderElements:
            if renderElement in layerData and not layerData[renderElement]:
                createAoOverride(realLayerName, renderElement, 0)

    if layerType == 'LOOKDEV':
        if renderTopGroups != '':
            geoCol = getRenderLayerCollectionByName(realLayerName, 'geo_col')
            geoCol.getSelector().setStaticSelection(renderTopGroups)

        cameraName = layerName.split('_')[1]
        renderCameraCollection = getRenderLayerCollectionByName(realLayerName, 'renderCamera_col')
        renderCameraCollection.getSelector().setPattern('*:CAMERA{}_GRP'.format(cameraName))
        todOverrideCollection = getRenderLayerCollectionByName(realLayerName, 'tod_override')
        for child in todOverrideCollection.getChildren():
            if child.attributeName() == 'idGenTex':
                child.setAttrValue(todIndex)

    elif layerType == 'DEEP':
        deepOverrides = {
            "relements_deepMergeMode": 1,
            "relements_deepMergeCoeff": 0.1,
            "relements_deepExcludeRGB": 1
        }

        for dOverride in deepOverrides:
            createRenderSettingsOverride(realLayerName, dOverride, deepOverrides[dOverride])

    elif layerType == 'DOME':
        createFarClipPlaneOverride(realLayerName)


################################################################################
# @brief      Creates a deep from visible layer.
#
def createDeepFromVisibleLayer():
    rs = renderSetup.instance()
    visibleLayer = rs.getVisibleRenderLayer()
    layerName = visibleLayer.name()[:-3]

    collectionsToCopy = ['geo_col', 'extrageo_col', 'vrscene_col', 'geo_prune_col']

    deepCollections = {}
    for coll in collectionsToCopy:
        beautyCollection = getRenderLayerCollectionByName(visibleLayer.name(), coll)
        if beautyCollection:
            collectionDict = {}
            collectionDict['selection'] = beautyCollection.getSelector().getStaticSelection()
            collectionDict['pattern'] = beautyCollection.getSelector().getPattern()
            deepCollections[coll] = collectionDict

    createFromTemplate('DEEP', layerName)

    deepLayer = rs.getVisibleRenderLayer()
    for coll in deepCollections:
        deepCollection = getRenderLayerCollectionByName(deepLayer.name(), coll)
        deepCollection.getSelector().setStaticSelection(deepCollections[coll]['selection'])
        deepCollection.getSelector().setPattern(deepCollections[coll]['pattern'])


################################################################################
# @brief      import a render layer from a template file
#
# @param      filename  The filename
#
def importFromTemplate(filename):
    with open(filename, "r") as file:
        json_data = json.load(file)
        renderSetup.instance().decode(json_data, renderSetup.DECODE_AND_MERGE, None)


################################################################################
# @brief      export the select render layer to a template file
#
# @param      filename        The filename
# @param      note            The note
# @param      exportSettings  The bool for export settings
#
def exportToTemplate(filename, exportSettings=False):
    with open(filename, "w+") as file:
        json.dump(renderSetup.instance().encode(includeSceneSettings=exportSettings), fp=file, indent=2, sort_keys=True)


################################################################################
# @brief      export the given render layer to a json file
#
# @param      layerName:  The layer name
# @param      filename:   The filename
#
# @return     { description_of_the_return_value }
#
def exportSelectedRenderLayer(layerName, filename):
    currentRenderLayer = renderSetup.instance().getRenderLayer(layerName)
    jsonData = {"renderSetup": {"name": "renderSetup", "renderLayers": [currentRenderLayer.encode()]}}
    with open(filename, "w+") as file:
        json.dump(jsonData, fp=file, indent=2, sort_keys=True)


################################################################################
# @brief      Duplicates and rename render layer.
#
# @param      layerName     The layer name
# @param      newLayerName  The new layer name
#
def duplicateAndRenameRenderLayer(layerName, newLayerName):
    rs = renderSetup.instance()
    currentRenderLayer = rs.getRenderLayer(layerName)

    data = {
        "renderSetup": {
            "name": "renderSetup",
            "renderLayers": [currentRenderLayer.copyForClipboard()]
        }
    }

    newData = api.json.replaceString(data, layerName, newLayerName)

    rs.decode(newData, renderSetup.DECODE_AND_MERGE, None)


################################################################################
# @brief      delete the render element not create by pipeline
#
# @param      exportPath  The export path
#
# @return     { description_of_the_return_value }
#
def cleanUpRenderElement(exportPath):
    allRenderElement = cmd.ls(type="VRayRenderElement")
    badRenderElement = []

    for renderElement in allRenderElement:
        if ':' in renderElement:
            badRenderElement.append(renderElement.replace(':', '_'))

    jsonData = api.json.json_read(exportPath)

    for renderLayerDict in jsonData['renderSetup']['renderLayers']:
        for layerCollection in renderLayerDict['renderSetupLayer']['collections']:
            if 'aovCollection' in layerCollection.keys():
                newList = layerCollection['aovCollection']['children'][:]
                for child in layerCollection['aovCollection']['children']:
                    for badElement in badRenderElement:
                        if badElement == child['aovChildCollection']['name'].split('_col')[0]:
                            newList.remove(child)
                layerCollection['aovCollection']['children'] = newList

    api.json.json_write(jsonData, exportPath)


################################################################################
# @brief      clean the export file from other shots.
#
# @param      exportPath  The export path
#
def cleanupExportSettings(exportPath):
    exportData = api.json.json_read(exportPath)
    if 'sceneAOVs' in exportData:
        exportData.pop('sceneAOVs')
    if 'sceneSettings' in exportData:
        exportData.pop('sceneSettings')

    api.json.json_write(exportData, exportPath)


########################################################################################
# @brief      change the value of a render settings override for the given render layer
#
# @param      layerName:  The layer name
# @param      attrName:   The attribute name
# @param      attrValue:  The attribute value
#
def changeRenderSettingsOverrideValue(layerName, attrName, attrValue):
    switchByName(layerName)
    rs = renderSetup.instance()
    currentRenderLayer = rs.getRenderLayer(layerName)
    if renderSetup.availableOverrides('vraySettings', attrName):
        if renderSetup.hasOverrideApplied('vraySettings', attrName):
            for layerOverride in utils.getOverridesRecursive(currentRenderLayer):
                if layerOverride.typeName() == 'absUniqueOverride' and layerOverride.attributeName() == attrName:
                    layerOverride.setAttrValue(attrValue)


################################################################################
# @brief      Creates a render settings override for the given render layer.
#
# @param      layerName:  The layer name
# @param      attrName:   The attribute name
# @param      attrValue:  The attribute value
#
def createRenderSettingsOverride(layerName, attrName, attrValue):
    switchByName(layerName)
    rs = renderSetup.instance()
    currentRenderLayer = rs.getRenderLayer(layerName)
    if renderSetup.availableOverrides('vraySettings', attrName):
        if not renderSetup.hasOverrideApplied('vraySettings', attrName):
            newOverride = currentRenderLayer.createAbsoluteOverride('vraySettings', attrName)
            newOverride.setAttrValue(attrValue)
        else:
            changeRenderSettingsOverrideValue(layerName, attrName, attrValue)
    else:
        api.log.logger().debug('Can\'t create override for this render setting: {}'.format(attrName))


################################################################################
# @brief      Removes a render settings override.
#
# @param      layerName  The layer name
# @param      attrName   The attribute name
#
def removeRenderSettingsOverride(layerName, attrName):
    switchByName(layerName)
    rs = renderSetup.instance()
    currentRenderLayer = rs.getRenderLayer(layerName)
    if renderSetup.hasOverrideApplied('vraySettings', attrName):
        for layerOverride in utils.getOverridesRecursive(currentRenderLayer):
            if layerOverride.typeName() == 'absUniqueOverride' and layerOverride.attributeName() == attrName:
                override.delete(layerOverride)


################################################################################
# @brief      change tyhe aov override value.
#
# @param      layerName  The layer name
# @param      attrName   The attribute name
# @param      attrValue  The attribute value
#
def changeAoOverrideValue(layerName, attrName, attrValue):
    switchByName(layerName)
    rs = renderSetup.instance()
    currentRenderLayer = rs.getRenderLayer(layerName)
    if renderSetup.availableOverrides(attrName, 'enabled'):
        if renderSetup.hasOverrideApplied(attrName, 'enabled'):
            for layerOverride in utils.getOverridesRecursive(currentRenderLayer):
                if layerOverride.typeName() == 'absOverride':
                    try:
                        if attrName in str(layerOverride.getApplyOverrides()[0].getEnabledPlug()):
                            layerOverride.setAttrValue(attrValue)
                    except:
                        continue


################################################################################
# @brief      Creates an ao override for the given render layer.
#
# @param      layerName:  The layer name
# @param      attrName:   The attribute name
# @param      attrValue:  The attribute value
#
def createAoOverride(layerName, attrName, attrValue):
    switchByName(layerName)
    rs = renderSetup.instance()
    currentRenderLayer = rs.getRenderLayer(layerName)
    if renderSetup.availableOverrides(attrName, 'enabled'):
        if not renderSetup.hasOverrideApplied(attrName, 'enabled'):
            newOverride = currentRenderLayer.createAbsoluteOverride(attrName, 'enabled')
            newOverride.setAttrValue(attrValue)
        else:
            changeAoOverrideValue(layerName, attrName, attrValue)
    else:
        api.log.logger().debug('Can\'t create override for this render element: {}'.format(attrName))


################################################################################
# @brief      Creates a black hole collection for render layer.
#
# @param      layerName:  The layer name
#
def createBlackHoleCollectionForRenderLayer(layerName):
    if not cmd.objExists('arakno_blackHole_wrapper'):
        api.vray.createBlackHoleWrapper()

    rs = renderSetup.instance()
    # take render layer
    currentRenderLayer = rs.getRenderLayer(layerName)

    # create blackhole collection and set filter to 'All'
    blackHoleCollection = currentRenderLayer.createCollection('{}_BH_col'.format(currentRenderLayer.name()))
    blackHoleCollectionSelector = blackHoleCollection.getSelector()
    blackHoleCollectionSelector.setFilterType(0)

    # create some abs override for this collection
    matteSurfaceOverride = blackHoleCollection.createAbsoluteOverride(cmd.ls(type='VRayMtlWrapper')[0], 'matteSurface')
    matteSurfaceOverride.setAttrValue(1)

    alphaContributionOverride = blackHoleCollection.createAbsoluteOverride(cmd.ls(type='VRayMtlWrapper')[0], 'alphaContribution')
    alphaContributionOverride.setAttrValue(-1)

    shadowsOverride = blackHoleCollection.createAbsoluteOverride(cmd.ls(type='VRayMtlWrapper')[0], 'shadows')
    shadowsOverride.setAttrValue(0)

    # create shading engine sub collection

    shadingOverrideCollection = blackHoleCollection.createCollection('{}_shadingEngines'.format(blackHoleCollection.name()))
    shadingOverrideCollectionSelector = shadingOverrideCollection.getSelector()
    shadingOverrideCollectionSelector.setFilterType(11)
    shadingOverrideCollectionSelector.setPattern('*')

    # create the shader override
    realShaderOverride = shadingOverrideCollection.createOverride('Shader Override', typeIDs.shaderOverride)
    realShaderOverride.setShader('arakno_blackHole_wrapper')

    # create shapes collection
    shapesOverrideCollection = blackHoleCollection.createCollection('{}_col_shapes'.format(blackHoleCollection.name()))
    shapesOverrideCollectionSelector = shapesOverrideCollection.getSelector()
    shapesOverrideCollectionSelector.setFilterType(2)
    shapesOverrideCollectionSelector.setPattern('*')

    # create all shapes absOverrides
    useWrapperOverride = shapesOverrideCollection.createAbsoluteOverride(cmd.ls(type='VRayScene')[0], 'useWrapper')
    useWrapperOverride.setAttrValue(1)

    useIrradianceMapOverride = shapesOverrideCollection.createAbsoluteOverride(cmd.ls(type='VRayScene')[0], 'useIrradianceMap')
    useIrradianceMapOverride.setAttrValue(0)

    receiveGIOverride = shapesOverrideCollection.createAbsoluteOverride(cmd.ls(type='VRayScene')[0], 'receiveGI')
    receiveGIOverride.setAttrValue(0)

    receiveCausticsOverride = shapesOverrideCollection.createAbsoluteOverride(cmd.ls(type='VRayScene')[0], 'receiveCaustics')
    receiveCausticsOverride.setAttrValue(0)

    matteSurfaceOverride = shapesOverrideCollection.createAbsoluteOverride(cmd.ls(type='VRayScene')[0], 'matteSurface')
    matteSurfaceOverride.setAttrValue(1)

    alphaContributionOverride = shapesOverrideCollection.createAbsoluteOverride(cmd.ls(type='VRayScene')[0], 'alphaContribution')
    alphaContributionOverride.setAttrValue(-1)

    generateRenderElementsOverride = shapesOverrideCollection.createAbsoluteOverride(cmd.ls(type='VRayScene')[0], 'generateRenderElements')
    generateRenderElementsOverride.setAttrValue(0)

    reflectionAmountOverride = shapesOverrideCollection.createAbsoluteOverride(cmd.ls(type='VRayScene')[0], 'reflectionAmount')
    reflectionAmountOverride.setAttrValue(0)

    refractionAmountOverride = shapesOverrideCollection.createAbsoluteOverride(cmd.ls(type='VRayScene')[0], 'refractionAmount')
    refractionAmountOverride.setAttrValue(0)

    mrs_useOverride = shapesOverrideCollection.createAbsoluteOverride(cmd.ls(type='VRayScene')[0], 'mrs_use')
    mrs_useOverride.setAttrValue(1)


################################################################################
# @brief      Determines if bh collection.
#
# @param      layerName  The layer name
#
# @return     True if bh collection, False otherwise.
#
def hasBHCollection(layerName):
    rs = renderSetup.instance()
    currentRenderLayer = rs.getRenderLayer(layerName)
    layerCollections = currentRenderLayer.getCollections()
    for layerCollection in layerCollections:
        if '{}_BH_col'.format(currentRenderLayer.name()) == layerCollection.name():
            return True

    return False


################################################################################
# @brief      Determines if collection.
#
# @param      collectionName  The collection name
# @param      layerName       The layer name
#
# @return     True if collection, False otherwise.
#
def hasCollection(collectionName, layerName):
    rs = renderSetup.instance()
    currentRenderLayer = rs.getRenderLayer(layerName)
    layerCollections = currentRenderLayer.getCollections()
    for layerCollection in layerCollections:
        if layerCollection.name().endswith(collectionName):
            return True

    return False


################################################################################
# @brief      Determines if untitled collections.
#
# @param      layerName  The layer name
#
# @return     True if untitled collections, False otherwise.
#
def hasUntitledCollections(layerName):
    children = getRenderLayerCollections(layerName)
    for child in children:
        if child.name() == '_untitled_':
            return True

    return False


################################################################################
# @brief      Creates a far clip plane override.
#
# @param      layerName  The layer name
#
def createFarClipPlaneOverride(layerName):
    rs = renderSetup.instance()
    if cmd.objExists('BAKED_CAM'):
        currentRenderLayer = rs.getRenderLayer(layerName)

        # create the collection
        cameraCollection = currentRenderLayer.createCollection('{}_camera_col'.format(currentRenderLayer.name()))
        cameraCollectionSelector = cameraCollection.getSelector()
        cameraCollectionSelector.setFilterType(2)
        cameraCollectionSelector.setStaticSelection('BAKED_CAMShape')

        # create override
        farClipPlaneOverride = cameraCollection.createAbsoluteOverride('BAKED_CAMShape', 'farClipPlane')
        currentValue = cmd.getAttr('BAKED_CAMShape.farClipPlane')
        farClipPlaneOverride.setAttrValue(100 * currentValue)

# copyForClipboard funzione in renderLayer.py file class RenderLayer
