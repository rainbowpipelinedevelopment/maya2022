import importlib
import requests
import os
import socket
import re
from datetime import datetime

import maya.cmds as cmd

import api.database
import api.json
import api.log
import api.vray
import api.widgets.rbw_UI as rbwUI
# import API.mail

importlib.reload(api.database)
importlib.reload(api.json)
importlib.reload(api.log)
importlib.reload(api.vray)
# importlib.reload(rbwUI)
# reload(API.mail)


################################################################################
# @brief      Gets the priority.
#
# @param      obj           The object
# @param      priorityName  The priority name
#
# @return     The priority.
#
def getPriority(obj, priorityName):
    if hasattr(obj, 'shotId') and priorityName in ['cfxAlembicPrio', 'cfxMayaBatchPrio', 'finVrmeshPrio', 'finVrscenePrio', 'kickoffPrio', 'fmlPrio', 'ffrPrio', 'denoisePrio']:
        shotPrioQuery = "SELECT fr.{} FROM `farm_rules` as fr JOIN `farm_shots2rules` AS fsr ON fr.id=fsr.ruleId WHERE fsr.shotId = '{}'".format(priorityName, obj.shotId)
        shotPrio = api.database.selectSingleQuery(shotPrioQuery)

        if shotPrio:
            return shotPrio[0]

    priorityQuery = "SELECT `{}` FROM `farm_showPriorities` WHERE `id_mv` = '{}'".format(priorityName, os.getenv('ID_MV'))
    priorityValue = api.database.selectSingleQuery(priorityQuery)[0]

    return priorityValue


################################################################################
# @brief      Creates a kick off post job.
#
# @param      params  The parameters
#
# @return     the path for the job post job script
#
def createKickOffPostJob(params):
    vrscenePath = params['outputFileName'].replace('<Layer>', params['layer'])

    vrsceneFolder = os.path.dirname(vrscenePath)
    if not os.path.exists(vrsceneFolder):
        os.makedirs(vrsceneFolder)

    postScriptTemplateFile = '//speed.nas/deadline/custom/scripts/Jobs/Rainbow/Rainbow_postJob_vray_maya_command_kickOff.py'

    postScriptFile = os.path.join(vrsceneFolder, 'farm_kickOff_post_script.py').replace('\\', '/')

    oldFile = 'vrsceneFile = "toChange"'
    newFile = 'vrsceneFile = "{}"'.format(vrscenePath)

    oldFrame = 'currentFrame = "toChange"'
    newFrame = 'currentFrame = {}'.format(params['current_frame'])

    with open(postScriptTemplateFile, "r") as postScript_r:
        scriptData = postScript_r.read()

    scriptData = scriptData.replace(oldFile, newFile)
    scriptData = scriptData.replace(oldFrame, newFrame)

    if params['renderDeep']:
        scriptData = scriptData.replace('deepExport = False', 'deepExport = True')
        scriptData = scriptData.replace('deepVersion = "toChange"', 'deepVersion = "{}"'.format(str(params['deepVersion']).zfill(3)))

    if params['vrscenes']:
        oldHasVrscene = 'includeVrscene = False'
        newHasVrscene = 'includeVrscene = True'

        scriptData = scriptData.replace(oldHasVrscene, newHasVrscene)

    with open(postScriptFile, "w") as postScript_w:
        postScript_w.write(scriptData)

    return postScriptFile


def createKickOffMultiFilePostJob(params):
    vrscenePath = params['outputFileName'].replace('<Layer>', params['layer'])

    vrsceneFolder = os.path.dirname(vrscenePath)
    if not os.path.exists(vrsceneFolder):
        os.makedirs(vrsceneFolder)

    postScriptTemplateFile = '//speed.nas/deadline/custom/scripts/Jobs/Rainbow/Rainbow_postJob_vray_maya_command_kickOff_multiFile.py'

    postScriptFile = os.path.join(vrsceneFolder, 'farm_kickOff_post_script.py').replace('\\', '/')

    oldPath = 'vrscenePath = "toChange"'
    newPath = 'vrscenePath = "{}"'.format(vrscenePath)

    oldFrame = 'currentFrame = "toChange"'
    newFrame = 'currentFrame = {}'.format(params['current_frame'])

    if ',' in params['frames'] or ';' in params['frames']:
        deadlineFrameRange = params['frames'].replace(',', 'x')
        deadlineFrameRange = deadlineFrameRange.replace(';', ',')
    else:
        deadlineFrameRange = params['frames']

    heads = []
    rangeRegex = r"\d{1,3}-\d{1,3}"
    if ',' in deadlineFrameRange:
        ranges = deadlineFrameRange.split(',')
    else:
        ranges = [deadlineFrameRange]

    for timeRange in ranges:
        if re.match(rangeRegex, timeRange) and 'x' not in timeRange:
            start = int(timeRange.split('-')[0])
            end = int(timeRange.split('-')[1])
            for i in range(start, end + 1):
                if (i - start + 1) % 40 == 1:
                    heads.append(i)

    oldHeadsLine = "chunkHeads = None"
    newHeadsLine = "chunkHeads = {}".format(str(heads))

    with open(postScriptTemplateFile, "r") as postScript_r:
        scriptData = postScript_r.read()

    scriptData = scriptData.replace(oldPath, newPath)
    scriptData = scriptData.replace(oldFrame, newFrame)

    if params['renderDeep']:
        scriptData = scriptData.replace('deepExport = False', 'deepExport = True')
        scriptData = scriptData.replace('deepVersion = "toChange"', 'deepVersion = "{}"'.format(str(params['deepVersion']).zfill(3)))

    if len(heads) > 0:
        scriptData = scriptData.replace(oldHeadsLine, newHeadsLine)

    if params['vrscenes']:
        oldHasVrscene = 'includeVrscene = False'
        newHasVrscene = 'includeVrscene = True'

        scriptData = scriptData.replace(oldHasVrscene, newHasVrscene)

    with open(postScriptFile, "w") as postScript_w:
        postScript_w.write(scriptData)

    return postScriptFile


#############################################################################################################
# @brief      Gets the compose script job path.
#
# @param      params  The parameters
#
# @return     The script job path.
#
def getComposeScriptJobPath(params):
    scriptJobTemplateFile = "//speed.nas/deadline/custom/scripts/Jobs/Rainbow/Rainbow_maya_compose_script_job.py"

    scriptPath = params['melFile'].replace('.mel', '.py')

    oldPath = 'fullMelPath = "toChange"'
    newPath = 'fullMelPath = "{}"'.format(params['melFile'].replace('//speed.nas', '${SPEED_SHARE}'))

    with open(scriptJobTemplateFile, "r") as scriptJob_r:
        scriptData = scriptJob_r.read()

    scriptData = scriptData.replace(oldPath, newPath)

    with open(scriptPath, "w") as scriptJob_w:
        scriptJob_w.write(scriptData)

    return scriptPath


################################################################################
# @brief      Gets the export script job path.
#
# @param      params  The parameters
#
# @return     The export script job path.
#
def getExportScriptJobPath(params):
    scriptJobTemplateFile = "//speed.nas/deadline/custom/scripts/Jobs/Rainbow/Rainbow_maya_export_script_job.py"

    scriptPath = os.path.join(params['outputFileName'], 'export_script_job.py').replace('\\', '/')

    oldDuration = 'duration = "toChange"'
    oldDir = 'cacheDir = "toChange"'
    oldSet = 'mayaSet = "toChange"'

    newDuration = 'duration = {}'.format(params['frames'].split('-')[1])
    newDir = 'cacheDir = "{}"'.format(params['outputFileName'])
    newSet = 'mayaSet = "{}"'.format(params['mayaSet'])

    with open(scriptJobTemplateFile, "r") as scriptJob_r:
        scriptData = scriptJob_r.read()

    scriptData = scriptData.replace(oldDuration, newDuration)
    scriptData = scriptData.replace(oldDir, newDir)
    scriptData = scriptData.replace(oldSet, newSet)

    if not os.path.exists(params['outputFileName']):
        os.makedirs(params['outputFileName'])

    with open(scriptPath, "w") as scriptJob_w:
        scriptJob_w.write(scriptData)

    return scriptPath


################################################################################
# @brief      Creates an alembic export job parameter.
#
# @param      params  The parameters
#
# @return     job properties
#
def createAlembicExportJobParam(params):
    projectPath = os.getenv('MC_FOLDER').replace('\\', '/')

    jobProperties = {
        "JobInfo": {
            "BatchName": params['batchName'],
            "Name": params['jobName'],
            "UserName": os.getenv('USERNAME'),
            "Region": "",
            "Department": "Grooming",
            "Frames": 1,
            "ChunkSize": 100000,
            "Pool": "alembic",
            "Priority": "{}".format(getPriority(params['obj'], 'cfxAlembicPrio')),
            "Denylist": "",
            "OverrideTaskExtraInfoNames": False,
            "Plugin": "MayaBatch",
            "EventOptIns": ""
        },

        "PluginInfo": {
            "Animation": 0,
            "RenderSetupIncludeLights": 1,
            "AlembicJob": True,
            "OutputFile": os.path.basename(params['outputFileName']),
            "Verbose": True,
            "StripNamespaces": True,
            "UVWrite": True,
            "WorldSpace": True,
            "AlembicAttributes": "",
            "AlembicAttributePrefix": "",
            "AlembicFormatOption": "Ogawa",
            "OutputFilePath": os.path.dirname(params['outputFileName']),
            "AlembicSelection": "All",
            "Version": cmd.about(version=True),
            "UseLegacyRenderLayers": 0,
            "Build": None,
            "ProjectPath": projectPath,
            "StartupScript": "",
            "SkipExistingFrames": 0,
            "Camera": "persp",
            "Camera0": "",
            "Camera1": "front",
            "Camera2": "persp",
            "Camera3": "side",
            "Camera4": "top",
            "CountRenderableCameras": 1,
            "IgnoreError211": 0,
            "UseLocalAssetCaching": 0,
            "EnableOpenColorIO": 0,
            "OCIOConfigFile": "",
            "OCIOPolicyFile": "",
            "StrictErrorChecking": True,
            "SceneFile": params['inputFileName']
        },

        "AuxFiles": [],

        "IdOnly": "true"
    }

    if params['dependency'] is not None:
        jobProperties["JobInfo"]["Dep"] = []
        api.log.logger().debug(params['dependency'])
        for i in range(0, len(params['dependency'])):
            jobProperties["JobInfo"]["JobDependency{}".format(i)] = params['dependency'][i]

    return jobProperties


################################################################################
# @brief      Creates a compose ma scene job parameter.
#
# @param      params  The parameters
#
# @return     job properties.
#
def createComposeMaSceneJobParam(params):
    projectPath = os.getenv('MC_FOLDER').replace('\\', '/')

    jobProperties = {
        "JobInfo": {
            "BatchName": params['batchName'],
            "Name": params['jobName'],
            "UserName": os.getenv('USERNAME'),
            "Region": "",
            "Department": "Grooming",
            "Frames": 1,
            "ChunkSize": 100000,
            "Pool": "high",
            "Priority": "{}".format(getPriority(params['obj'], 'cfxMayaBatchPrio')),
            "Denylist": "",
            "OverrideTaskExtraInfoNames": False,
            "Plugin": "MayaBatch",
            "EventOptIns": ""
        },

        "PluginInfo": {
            "Animation": 0,
            "ScriptJob": True,
            "ScriptFilename": getComposeScriptJobPath(params),
            "Version": cmd.about(version=True),
            "UseLegacyRenderLayers": 0,
            "Build": None,
            "ProjectPath": projectPath,
            "StartupScript": "",
            "SkipExistingFrames": 0,
            "Camera": "",
            "Camera0": "",
            "Camera1": "front",
            "Camera2": "persp",
            "Camera3": "side",
            "Camera4": "top",
            "CountRenderableCameras": 1,
            "IgnoreError211": 0,
            "UseLocalAssetCaching": 0,
            "EnableOpenColorIO": 0,
            "OCIOConfigFile": "",
            "OCIOPolicyFile": "",
            "StrictErrorChecking": True,
            "SceneFile": os.path.join(os.getenv('CONTENT'), '_pipeline', 'farm', 'empty.mb')
        },

        "AuxFiles": [],

        "IdOnly": "true"
    }

    return jobProperties


def createVrmeshExportJobParams(params):
    projectPath = os.getenv('MC_FOLDER').replace('\\', '/')

    mayaVersion = cmd.about(version=True)
    vrayVersion = api.vray.getVrayVersion()

    jobProperties = {
        "JobInfo": {
            "BatchName": params['batchName'],
            "Name": params['jobName'],
            "UserName": os.getenv('USERNAME'),
            "Region": "",
            "Department": "Finishing",
            "Frames": params['frames'],
            "ChunkSize": 100000,
            "Pool": "vrmesh",
            "Priority": "{}".format(getPriority(params['obj'], 'finVrmeshPrio')),
            "Denylist": "",
            "OverrideTaskExtraInfoNames": False,
            "Plugin": "MayaBatch",
            "EventOptIns": "",
            "OutputDirectory0": params['outputFileName'],
            "EnvironmentKeyValue0": "MAYA_PLUG_IN_PATH=//speed.nas/arakno/pipeSoftware/vray/maya{}/{}/maya_vray/plug-ins;/mnt/rainbow/speed/arakno/pipeSoftware/vray/maya{}/{}_lnx/maya_vray/plug-ins".format(mayaVersion, vrayVersion, mayaVersion, vrayVersion),
            "EnvironmentKeyValue1": "MAYA_RENDER_DESC_PATH=//speed.nas/arakno/pipeSoftware/vray/maya{}/{}/maya_vray/rendererDesc;/mnt/rainbow/speed/arakno/pipeSoftware/vray/maya{}/{}_lnx/maya_vray/rendererDesc".format(mayaVersion, vrayVersion, mayaVersion, vrayVersion),
            "EnvironmentKeyValue2": "MAYA_MODULE_PATH=//speed.nas/arakno/pipeSoftware/modules/{}/vray{};/mnt/rainbow/speed/arakno/pipeSoftware/modules/{}/vray{}_lnx".format(mayaVersion, vrayVersion, mayaVersion, vrayVersion),
            "EnvironmentKeyValue3": "VRAY_FOR_MAYA2022_PLUGINS=//speed.nas/arakno/pipeSoftware/vray/maya{}/{}/maya_vray/vrayplugins;/mnt/rainbow/speed/arakno/pipeSoftware/vray/maya{}/{}_lnx/maya_vray/vrayplugins".format(mayaVersion, vrayVersion, mayaVersion, vrayVersion),
            "EnvironmentKeyValue4": "MAYA_VP2_DEVICE_OVERRIDE=VirtualDeviceDx11",
            "EnvironmentKeyValue5": "VRAY_AUTH_CLIENT_FILE_PATH=//speed.nas/arakno/pipeSoftware/vray;/mnt/rainbow/speed/arakno/pipeSoftware/vray",
            "EnvironmentKeyValue6": "VRAY_FOR_MAYA2022_MAIN=//speed.nas/arakno/pipeSoftware/vray/maya{}/{}/maya_vray;/mnt/rainbow/speed/arakno/pipeSoftware/vray/maya{}/{}_lnx/maya_vray".format(mayaVersion, vrayVersion, mayaVersion, vrayVersion),
            "EnvironmentKeyValue7": "SPEED_SHARE=//speed.nas"
        },

        "PluginInfo": {
            "Animation": 1,
            "ScriptJob": True,
            "ScriptFilename": getExportScriptJobPath(params),
            "Version": cmd.about(version=True),
            "UseLegacyRenderLayers": 0,
            "Build": None,
            "ProjectPath": projectPath,
            "StartupScript": "",
            "SkipExistingFrames": 0,
            "Camera": "",
            "Camera0": "",
            "Camera1": "front",
            "Camera2": "persp",
            "Camera3": "side",
            "Camera4": "top",
            "CountRenderableCameras": 1,
            "IgnoreError211": 0,
            "UseLocalAssetCaching": 0,
            "EnableOpenColorIO": 0,
            "OCIOConfigFile": "",
            "OCIOPolicyFile": "",
            "StrictErrorChecking": True,
            "SceneFile": params['inputFileName']
        },

        "AuxFiles": [],

        "IdOnly": "true"
    }

    return jobProperties


################################################################################
# @brief      Creates a denoise job parameter.
#
# @param      params  The parameters
#
# @return     { description_of_the_return_value }
#
def createDenoiseJobParam(params):
    denoiseModes = {
        0: 'Mild',
        1: 'Default',
        2: 'Strong'
    }

    if ',' in params['frames'] or ';' in params['frames']:
        deadlineFrameRange = params['frames'].replace(',', 'x')
        deadlineFrameRange = deadlineFrameRange.replace(';', ',')
    else:
        deadlineFrameRange = params['frames']

    jobProperties = {
        "PluginInfo":
        {
            "InputPath": "{}".format(params['inputFileName']),
            "DenoiseMode": denoiseModes[params['denoiseMode']],
            "Boost": "0",
            "SkipExisting": "False",
            "DenoiseElements": "False",
            "UseGPU": "False",
            "OverrideThreshold": "False",
            "Threshold": "0.001",
            "OverrideStrength": "False",
            "Strength": "1.0",
            "OverrideRadius": "False",
            "PixelRadius": "10.0",
            "AdjustRadius": "False",
            "FrameBlend": "1",
            "RenderStrips": "-1",
            "UsingFrames": "True",
            "ElementsList": params['elementsList'],
            "KeepBeauty": params['keepBeauty']
        },

        "JobInfo":
        {
            "ChunkSize": "1",
            "Priority": "{}".format(getPriority(params['obj'], 'denoisePrio')),
            "EventOptIns": "",
            "OverrideTaskFailureDetection": "True",
            "FailureDetectionTaskErrors": "150",
            "Frames": "{}".format(deadlineFrameRange),
            "Name": "{}".format(params['jobName']),
            "OverrideTaskExtraInfoNames": "False",
            "Plugin": "RBWVDenoise62003",
            "Region": "",
            "UserName": "{}".format(os.getenv('USERNAME')),
            "Whitelist": "",
            "Pool": "denoise",
            "Group": "",
            "Comment": "{}".format(params['notes']),
            "Department": params['dept'],
            "BatchName": "{}".format(params['batchName']),
            "PostJobScript": "//speed.nas/deadline/custom/scripts/Jobs/Rainbow/Rainbow_postJob_vray_denoise.py",
            "OutputDirectory0": "{}".format(os.path.split(params['outputFileName'])[0])
        },

        "AuxFiles": [],

        "IdOnly": "true"
    }

    if params['dependency'] is not None:
        for i in range(0, len(params['dependency'])):
            jobProperties["JobInfo"]["JobDependency{}".format(i)] = params['dependency'][i]

    return jobProperties


################################################################################
# @brief      Creates a rendering job parameter.
#
# @param      params  The parameters
#
def createRenderingJobParam(params):
    fileExt = params['outputFileName'].split('.')[-1]
    OutputFileName0 = params['outputFileName'].replace('.{}'.format(fileExt), '.####.{}'.format(fileExt))

    if params['isLightJob']:
        pool = 'light'

    elif params['isHighJob']:
        pool = 'high'

    else:

        if 'ENV' in params['jobName']:
            pool = 'low'

        else:
            pool = 'mid'

    if ',' in params['frames'] or ';' in params['frames']:
        deadlineFrameRange = params['frames'].replace(',', 'x')
        deadlineFrameRange = deadlineFrameRange.replace(';', ',')
    else:
        deadlineFrameRange = params['frames']

    width = int(params['resolution_value'].split('x')[0])
    height = int(params['resolution_value'].split('x')[1])

    mayaVersion = cmd.about(version=True)
    vrayPluginVersion = api.vray.getVrayVersion()

    jobProperties = {
        "PluginInfo":
        {
            "CommandLineOptions": "-remapPath=\"/speed=/mnt/rainbow/speed;/speed.nas=/mnt/rainbow/speed;\\speed=/mnt/rainbow/speed;\\speed.nas=/mnt/rainbow/speed\" -region=none -verboseLevel=4",
            "AutocloseVFB": "False",
            "DisplaySRGB": "On",
            "InputFilename": "{}".format(params['inputFileName']),
            "OutputFilename": "{}".format(params['outputFileName']),
            "RTNoise": "0.001",
            "RTSamples": "0",
            "RTTimeout": "0.0",
            "SeparateFilesPerFrame": "False",
            "Threads": "0",
            "VRayEngine": "V-Ray",
            "Width": width,
            "Height": height
        },

        "JobInfo":
        {
            "ChunkSize": "1",
            "Priority": "{}".format(getPriority(params['obj'], 'ffrPrio')),
            "EventOptIns": "",
            "OverrideTaskFailureDetection": "True",
            "FailureDetectionTaskErrors": "150",
            "Frames": "{}".format(deadlineFrameRange),
            "Name": "{}".format(params['jobName']),
            "OverrideTaskExtraInfoNames": "False",
            "OnTaskTimeout": "FailAndNotify",
            "TaskTimeoutSeconds": "10800",
            "Plugin": "Vray{}".format(vrayPluginVersion),
            "Region": "",
            "UserName": "{}".format(os.getenv('USERNAME')),
            "Whitelist": "",
            "Pool": "{}".format(pool),
            "Group": "",
            "Comment": "{}".format(params['notes']),
            "Department": params['dept'],
            "BatchName": "{}".format(params['batchName']),
            "PreTaskScript": "//speed.nas/deadline/custom/scripts/Jobs/Rainbow/Rainbow_preTask_vray_standalone_{}_{}.py".format(mayaVersion, vrayPluginVersion),
            "PostTaskScript": "//speed.nas/deadline/custom/scripts/Jobs/Rainbow/Rainbow_postTask_vray_standalone.py",
            "PostJobScript": "//speed.nas/deadline/custom/scripts/Jobs/Rainbow/Rainbow_postJob_FFR_vray_standalone.py",
            "LimitGroups": "vray_local",
            "OutputDirectory0": "{}".format(os.path.split(params['outputFileName'])[0].replace('\\', '/')),
            "OutputFilename0": "{}".format(os.path.split(OutputFileName0)[1]),
            "ExtraInfo1": params['extraAttr']
        },

        "AuxFiles": [],

        "IdOnly": "true"
    }

    if params['extraTime']:
        jobProperties["JobInfo"]["TaskTimeoutSeconds"] = "21600"

        # emailSubject = 'Extra Time for job {}'.format(params['jobName'])

        # emailMessage = 'L\'utente <b>{}</b> ha inviato in farm il job <b>{}</b> richiedendo un timeout maggiore.\nEcco il suo commento: <b>{}</b>'.format(
        #     os.getenv('USERNAME'),
        #     params['jobName'],
        #     params['notes']
        # )

        # API.mail.sendWranglerAlert(emailSubject, emailMessage)

    if params['priority'] is not None:
        jobProperties["JobInfo"]["ExtraInfo0"] = params['priority']

    if params['dependency'] is not None:
        jobProperties["JobInfo"]["Dep"] = []
        if "InitialStatus" in jobProperties["JobInfo"]:
            jobProperties["JobInfo"].pop("InitialStatus")
        api.log.logger().debug(params['dependency'])
        for i in range(0, len(params['dependency'])):
            jobProperties["JobInfo"]["JobDependency{}".format(i)] = params['dependency'][i]

    if params['mode'] == 'fml':
        jobProperties["JobInfo"]["Priority"] = "{}".format(getPriority(params['obj'], 'fmlPrio'))
        jobProperties["JobInfo"]["PostJobScript"] = "//speed.nas/deadline/custom/scripts/Jobs/Rainbow/Rainbow_postJob_FML_vray_standalone.py"
        jobProperties["JobInfo"]["Pool"] = "fml"

    if params['chunk']:
        jobProperties["JobInfo"]["ChunkSize"] = "5"
        jobProperties["JobInfo"]["TaskTimeoutSeconds"] = "10800"

    # inserisco qui l'istruzione che se l'overscan e' attivo togliamo la region e le dimensioni allimmagine
    if params['overscan']:
        del jobProperties['PluginInfo']['Width']
        del jobProperties['PluginInfo']['Height']
        jobProperties['PluginInfo']['CommandLineOptions'] = "-remapPath=\"/speed=/mnt/rainbow/speed;/speed.nas=/mnt/rainbow/speed;\\speed=/mnt/rainbow/speed;\\speed.nas=/mnt/rainbow/speed\" -verboseLevel=4"

    if params['inputFileName'].endswith('_0001.vrscene'):
        jobProperties["PluginInfo"]["SeparateFilesPerFrame"] = True

    return jobProperties


################################################################################
# @brief      Creates a lighting vrscene caching job parameter.
#
# @param      params  The parameters
#
# @return     { description_of_the_return_value }
#
def createLightingVrsceneCachingJobParam(params):
    projectPath = os.getenv('MC_FOLDER').replace('\\', '/')

    if ',' in params['frames'] or ';' in params['frames']:
        deadlineFrameRange = params['frames'].replace(',', 'x')
        deadlineFrameRange = deadlineFrameRange.replace(';', ',')
    else:
        deadlineFrameRange = params['frames']

    OutputFileName = params['outputFileName'].split('mc_{}/'.format(os.getenv('PROJECT')))[1]

    if '/sys/VRSCENE/' in params['outputFileName']:
        imageOutputFileName = os.path.dirname(params['outputFileName'].replace('/sys/VRSCENE/', '/Lgt/RENDER/'))
    elif '/VRSCENE/' in params['outputFileName']:
        imageOutputFileName = os.path.dirname(params['outputFileName'].replace('/VRSCENE/', '/RENDER/'))
    else:
        imageOutputFileName = os.path.dirname(params['outputFileName'])

    imageOutputFileName = os.path.join(imageOutputFileName, params['layer']).replace('\\', '/')

    if params['goal'] == 'lookdev vrscene caching':
        PostJobScript = createKickOffMultiFilePostJob(params)
    else:
        PostJobScript = createKickOffPostJob(params)

    mayaVersion = cmd.about(version=True)
    vrayVersion = api.vray.getVrayVersion()

    jobProperties = {
        "PluginInfo":
        {
            "Animation": 1,
            "Build": "64bit",
            "IgnoreError211": 0,
            "OutputFilePath": "",
            "OutputFilePrefix": "",
            "SceneFile": "{}".format(params['inputFileName']),
            "ProjectPath": "{}".format(projectPath),
            "Renderer": "vrayExport",
            "SkipExistingFrames": 0,
            "VRayExportFile": "{}".format(OutputFileName),
            "Version": "{}".format(mayaVersion),
            "StrictErrorChecking": False,
            "CommandLineOptions": "-rl \"{}\" -cam \"{}\" -im \"{}\"".format(params['layer'], params['camera'], imageOutputFileName)
        },

        "JobInfo":
        {
            "Priority": "{}".format(getPriority(params['obj'], 'kickoffPrio')),
            "Frames": "{}".format(deadlineFrameRange),
            "Name": "{}".format(params['jobName']),
            "UserName": "{}".format(os.getenv('USERNAME')),
            "Denylist": "",
            "ChunkSize": 1000,
            "Comment": "{}".format(params['notes']),
            "Department": params['dept'],
            "BatchName": "{}".format(params['batchName']),
            "EventOptIns": "",
            "Group": "",
            "OverrideTaskExtraInfoNames": "False",
            "Plugin": "MayaCmdPy3",
            "Pool": "kickoff",
            "PreTaskScript": "//speed.nas/deadline/custom/scripts/Jobs/Rainbow/Rainbow_preTask_vray_maya_command_{}_{}.py".format(mayaVersion, vrayVersion),
            'PostJobScript': "{}".format(PostJobScript),
            "OnTaskTimeout": "FailAndNotify",
            "TaskTimeoutSeconds": 5400,
            "JobCleanupDays": 7,
            "OverrideJobCleanup": True,
            "OverrideAutoJobCleanup": True
        },

        "AuxFiles": [],

        "IdOnly": "true"
    }

    if params['goal'] == 'lookdev vrscene caching':
        jobProperties['JobInfo']['ChunkSize'] = 40
        jobProperties['PluginInfo']['CommandLineOptions'] = "-exportFramesSeparate -rl \"{}\" -cam \"{}\" -im \"{}\"".format(params['layer'], params['camera'], imageOutputFileName)

    return jobProperties


################################################################################
# @brief      Creates a finishing vrscene caching job parameter.
#
# @param      params  The parameters
#
# @return     { description_of_the_return_value }
#
def createFinishingVrsceneCachingJobParam(params):
    projectPath = os.getenv('MC_FOLDER').replace('\\', '/')

    mayaVersion = cmd.about(version=True)
    vrayVersion = api.vray.getVrayVersion()

    jobProperties = {
        "PluginInfo":
        {
            "Animation": 1,
            "Build": "64bit",
            "CommandLineOptions": "-exportFramesSeparate -cam \"BAKED_CAM\"",
            "IgnoreError211": 0,
            "OutputFilePath": "",
            "OutputFilePrefix": "",
            "SceneFile": "{}".format(params['inputFileName']),
            "ProjectPath": "{}".format(projectPath),
            "Renderer": "vrayExport",
            "SkipExistingFrames": 0,
            "VRayExportFile": "{}".format(params['outputFileName']),
            "StrictErrorChecking": False,
            "Version": "{}".format(mayaVersion)
        },

        "JobInfo":
        {
            "Priority": "{}".format(getPriority(params['obj'], 'finVrscenePrio')),
            "Frames": "{}".format(params['frames']),
            "Name": "{}".format(params['jobName']),
            "UserName": "{}".format(os.getenv('USERNAME')),
            "Denylist": "",
            "ChunkSize": 1,
            "Comment": "",
            "Department": params['dept'],
            "BatchName": "{}".format(params['batchName']),
            "EventOptIns": "",
            "Group": "",
            "OverrideTaskExtraInfoNames": "False",
            "Plugin": "MayaCmdPy3",
            "Pool": "vrscene",
            "PreTaskScript": "//speed.nas/deadline/custom/scripts/Jobs/Rainbow/Rainbow_preTask_vray_maya_command_{}_{}.py".format(mayaVersion, vrayVersion),
            "PostJobScript": "//speed.nas/deadline/custom/scripts/Jobs/Rainbow/Rainbow_postJob_vray_maya_command.py",
            "OnTaskTimeout": "FailAndNotify",
            "TaskTimeoutSeconds": 1200
        },

        "AuxFiles": [],

        "IdOnly": "true"
    }

    return jobProperties


################################################################################
# @brief      Creates and submit job.
#
# @param      jobParams  The job parameters
#
# @return     { description_of_the_return_value }
#
def createAndSubmitJob(jobParams):
    if jobParams['goal'] == 'rendering':
        api.log.logger().debug('sto\' creando un job di rendering')
        jsonParam = createRenderingJobParam(jobParams)

    elif jobParams['goal'] == 'denoise':
        api.log.logger().debug('sto\' creando un job di denoise')
        jsonParam = createDenoiseJobParam(jobParams)

    elif jobParams['goal'] == 'lighting vrscene caching':
        api.log.logger().debug('sto\' creando un job di vrscene per lighting')
        jsonParam = createLightingVrsceneCachingJobParam(jobParams)

    elif jobParams['goal'] == 'finishing vrscene caching':
        api.log.logger().debug('sto\' creando un job di vrscene per finishing')
        jsonParam = createFinishingVrsceneCachingJobParam(jobParams)

    elif jobParams['goal'] == 'export alembic':
        api.log.logger().debug('sto\' creando un job di alembic')
        jsonParam = createAlembicExportJobParam(jobParams)

    elif jobParams['goal'] == 'compose ma scene':
        api.log.logger().debug('sto\' creando un job di compose ma scene')
        jsonParam = createComposeMaSceneJobParam(jobParams)

    elif jobParams['goal'] == 'lookdev vrscene caching':
        api.log.logger().debug('sto\' creando un job di vrscene per lookdev')
        jsonParam = createLightingVrsceneCachingJobParam(jobParams)

    elif jobParams['goal'] == 'export vrmesh':
        api.log.logger().debug('sto\' creando un job di vrmesh per finishing')
        jsonParam = createVrmeshExportJobParams(jobParams)

    jobID = submitUrlRequest(jsonParam)

    if jobID:
        api.log.logger().debug(40 * '<<')
        api.log.logger().debug('{} SUBMITTED'.format(jobParams['jobName']))
        api.log.logger().debug(40 * '>>')
        return jobID

    else:
        rbwUI.RBWError(title='Job Not Created', text='Attenzione il job {} non e\' stato creato\nAttendi qualche minuto e riprova.'.format(jobParams['jobName']))


################################################################################
# @brief      submit url request from given params.
#
# @param      jsonParam  The json parameter
#
# @return     jobId or None.
#
def submitUrlRequest(jsonParam):
    tryCounter = 0
    while(tryCounter < 20):

        try:
            r = requests.post('http://badger:8081/api/jobs', json=jsonParam)
            jobID = r.json()["_id"]
            return jobID
        except ValueError:
            tryCounter += 1

    return None
