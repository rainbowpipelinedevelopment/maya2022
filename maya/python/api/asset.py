import importlib
import os

import api.database
import api.log
import api.exception
import api.system

importlib.reload(api.database)
importlib.reload(api.log)
importlib.reload(api.exception)
importlib.reload(api.system)


################################################################################
# @brief      This class describes an asset.
#
class Asset(object):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      params  The params
    #
    def __init__(self, params):
        super(Asset, self).__init__()

        if 'id_mv' not in params:
            params['id_mv'] = os.getenv('ID_MV')

        assetParams = self.checkParams(params)
        if assetParams:
            for label in assetParams:
                name = label
                value = assetParams[label]
                setattr(self, name, value)

            if self.id_mv == os.getenv('ID_MV'):
                self.projectName = os.getenv('PROJECT')
                self.content = os.getenv('MC_FOLDER')
            else:
                self.projectName, self.content = api.system.getProjectContent(self.id_mv)
        else:
            raise api.exception.InvalidAsset('The asset is wrong or maybe it has been deleted')

    ############################################################################
    # @brief      Gets the identifiers.
    #
    # @param      params  The parameters
    #
    # @return     the asset parameters
    #
    def checkParams(self, params):
        assetParams = {}
        if 'id' in params:
            idsQuery = "SELECT `category`, `group`, `name`, `variant`, `categoryID`, `groupID`, `nameID`, `shotgunID` FROM `V_assetList`WHERE `variantID` = {} AND `projectID` = '{}'".format(params['id'], params['id_mv'])
            try:
                assetParams['category'], assetParams['group'], assetParams['name'], assetParams['variant'], assetParams['categoryId'], assetParams['groupId'], assetParams['nameId'], assetParams['shotgunId'] = api.database.selectSingleQuery(idsQuery)
                assetParams['variantId'] = params['id']
                assetParams['id_mv'] = params['id_mv']
            except TypeError:
                pass
        else:
            idsQuery = "SELECT `categoryID`, `groupID`, `nameID`, `variantID`, `shotgunID` FROM `V_assetList` WHERE `category` = '{}' AND `group` = '{}' AND `name` = '{}' AND `variant` = '{}' AND `projectID` = '{}'".format(
                params['category'],
                params['group'],
                params['name'],
                params['variant'],
                params['id_mv']
            )

            try:
                assetParams['categoryId'], assetParams['groupId'], assetParams['nameId'], assetParams['variantId'], assetParams['shotgunId'] = api.database.selectSingleQuery(idsQuery)
                assetParams['category'] = params['category']
                assetParams['group'] = params['group']
                assetParams['name'] = params['name']
                assetParams['variant'] = params['variant']
                assetParams['id_mv'] = params['id_mv']
            except TypeError:
                pass

        return assetParams

    ############################################################################
    # @brief      Returns a dictionary representation of the object.
    #
    # @return     Dictionary representation of the object.
    #
    def toDict(self):
        toDict = self.__dict__
        return toDict

    ############################################################################
    # @brief      Gets the asset code.
    #
    # @return     The asset code.
    #
    def getAssetCode(self):
        return '{}_{}_{}_{}'.format(self.category, self.group, self.name, self.variant)

    ############################################################################
    # @brief      Gets the asset folder.
    #
    # @return     The asset folder.
    #
    def getAssetFolder(self):
        return os.path.join(self.content, 'scenes', self.category, self.group, self.name, self.variant).replace('\\', '/')

    ############################################################################
    # @brief      Tells if it's a UE project.
    #
    # @return     True or False based on DB results.
    #
    def isUeProject(self):
        ueQuery = "SELECT * FROM `p_prj_software` WHERE `id_mv` = {} AND `id_software` = 6".format(self.id_mv)
        return True if api.database.selectQuery(ueQuery) else False
