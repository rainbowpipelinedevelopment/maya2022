import importlib
import json
import os

import api.log
importlib.reload(api.log)


################################################################################
# @brief      Read json file and returns it's content as dictionary
#
# @param      input_file  The input file
#
# @return     Dictionary rapresentation of JSON file
#
def json_read(input_file=None):
    if not input_file:
        api.log.logger().error('No input file defined')

    with open(input_file) as json_data_file:
        json_data = json.load(json_data_file)

    return json_data


################################################################################
# @brief      Writes the json_data dictionary into the output json file
#
# @param      json_data    The json data
# @param      output_file  The output file
#
# @return     The json data dictionary written into the file
#
def json_write(json_data=None, output_file=None):
    if not json_data:
        api.log.logger().error('No JSON data to write')

    if not output_file:
        api.log.logger().error('No output file defined')

    # Create the file directory to handle the IOError if that dir doesn't exists
    dirname = os.path.dirname(output_file)

    if not os.path.exists(dirname):
        api.log.logger().debug('Folder created: {}'.format(dirname))
        os.makedirs(dirname)

    with open(output_file, 'w') as json_data_file:
        json.dump(json_data, json_data_file, indent=4, separators=(',', ': '), sort_keys=True)

    return json_data


################################################################################
# @brief      replace oldString with newString in the jsonData
#
# @param      jsonData   The json data
# @param      oldString  The old string
# @param      newString  The new string
#
# @return     the replace dict
#
def replaceString(jsonData, oldString, newString):
    dictStr = json.dumps(jsonData)
    dictStr = dictStr.replace(oldString, newString)
    newJsonData = json.loads(dictStr)

    return newJsonData
