import math


################################################################################
# @brief      cross product by two given vector
#
# @param      v1    The v 1
# @param      v2    The v 2
#
# @return     the result vector
#
def crossProduct(v1, v2):
    resultVector = [(v1[1] * v2[2]) - (v1[2] * v2[1]), (v1[2] * v2[0]) - (v1[0] * v2[2]), (v1[0] * v2[1]) - (v1[1] * v2[0])]

    for i in range(0, 3):
        if abs(resultVector[i]) == 0.0:
            resultVector[i] = 0.0

    return resultVector


################################################################################
# @brief      Gets the norm.
#
# @param      v     given vector
#
# @return     The norm.
#
def getNorm(v):
    return math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2])


################################################################################
# @brief      normalize the givern vector
#
# @param      v     the given vector
#
# @return     the normalize vector.
#
def normalize(v):
    norm = getNorm(v)

    if norm != 0.0:
        return [v[0] / norm, v[1] / norm, v[2] / norm]
    else:
        return v
