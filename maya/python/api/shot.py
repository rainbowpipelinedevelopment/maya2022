import importlib
import os

import api.database
import api.log
import api.exception
import api.camera
import api.shotgrid

importlib.reload(api.database)
importlib.reload(api.log)
importlib.reload(api.exception)
importlib.reload(api.camera)
importlib.reload(api.shotgrid)


################################################################################
# @brief      This class describes a shot.
#
class Shot(object):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      params  The parameters
    #
    def __init__(self, params):
        super(Shot, self).__init__()
        shotParams = self.checkParams(params)
        if shotParams:
            for label in shotParams:
                sequence = label
                value = shotParams[label]
                setattr(self, sequence, value)

            self.id_mv = os.getenv('ID_MV')
            self.projectName = os.getenv('PROJECT')
            self.content = os.getenv('MC_FOLDER')
            self.shotgridObj = api.shotgrid.ShotgridObj()
        else:
            raise api.exception.InvalidShot('The shot is wrong or maybe it has been deleted')

    ############################################################################
    # @brief      Gets the identifiers.
    #
    # @param      params  The parameters
    #
    # @return     the asset parameters
    #
    def checkParams(self, params):
        shotParams = {}
        if 'id' in params:
            idsQuery = "SELECT `season`, `episode`, `sequence`, `shot`, `seasonID`, `episodeID`, `sequenceID`, `seasonIdSg`, `episodeIdSg`, `sequenceIdSg`, `shotIdSg` FROM `V_shotList`WHERE `shotID` = {} AND `projectID` = '{}'".format(params['id'], os.getenv('ID_MV'))
            try:
                shotParams['season'], shotParams['episode'], shotParams['sequence'], shotParams['shot'], shotParams['seasonId'], shotParams['episodeId'], shotParams['sequenceId'], shotParams['seasonIdSg'], shotParams['episodeIdSg'], shotParams['sequenceIdSg'], shotParams['shotIdSg'] = api.database.selectSingleQuery(idsQuery)
                shotParams['shotId'] = params['id']
            except TypeError:
                pass
        else:
            idsQuery = "SELECT `seasonID`, `episodeID`, `sequenceID`, `shotID`, `seasonIdSg`, `episodeIdSg`, `sequenceIdSg`, `shotIdSg` FROM `V_shotList` WHERE `season` = '{}' AND `episode` = '{}' AND `sequence` = '{}' AND `shot` = '{}' AND `projectID` = '{}'".format(
                params['season'],
                params['episode'],
                params['sequence'],
                params['shot'],
                os.getenv('ID_MV')
            )

            try:
                shotParams['seasonId'], shotParams['episodeId'], shotParams['sequenceId'], shotParams['shotId'], shotParams['seasonIdSg'], shotParams['episodeIdSg'], shotParams['sequenceIdSg'], shotParams['shotIdSg'] = api.database.selectSingleQuery(idsQuery)
                shotParams['season'] = params['season']
                shotParams['episode'] = params['episode']
                shotParams['sequence'] = params['sequence']
                shotParams['shot'] = params['shot']
            except TypeError:
                pass

        return shotParams

    ############################################################################
    # @brief      Gets the shot folder.
    #
    # @return     The shot folder.
    #
    def getShotFolder(self):
        return os.path.join(self.content, 'scenes', 'animation', self.season, self.episode, self.sequence, self.shot).replace('\\', '/')

    ############################################################################
    # @brief      Gets the shot duration.
    #
    # @return     The shot duration.
    #
    def getShotDuration(self):
        durationQuery = "SELECT `duration` FROM `shot` WHERE `id` = {}".format(self.shotId)
        duration = api.database.selectSingleQuery(durationQuery)[0]

        return int(duration)

    ################################################################################
    # @brief      Gets the frames with camera movement.
    #
    # @return     The frames with camera movement.
    #
    def getFramesWithCameraMovement(self):
        try:
            startFrame = 1
            endFrame = self.getShotDuration()

            totalFrames = range(startFrame, endFrame + 1)

            stoppedFrameRanges = self.shotgridObj.get_frames_with_no_cam_movement(self.shotIdSg)['sg_frames_without_cam_movement'].split(',')

            freezeFrames = []

            for frameRange in stoppedFrameRanges:
                if '-' in frameRange:
                    start = int(frameRange.split('-')[0])
                    end = int(frameRange.split('-')[1])
                    for frame in totalFrames:
                        if frame > start and frame <= end:
                            freezeFrames.append(frame)
                else:
                    freezeFrames.append(int(frameRange))

            moveFrames = [item for item in totalFrames if item not in freezeFrames]

            return api.camera.detect_continous_numbers(moveFrames)[0].replace(',', ';')
        except:
            return

    def getShotCode(self):
        return '{}_{}_{}_{}_{}'.format(self.projectName.upper(), self.season, self.episode, self.sequence, self.shot)

    ############################################################################
    # @brief      Tells if it's a UE project.
    #
    # @return     True or False based on DB results.
    #
    def isUeProject(self):
        ueQuery = "SELECT * FROM `p_prj_software` WHERE `id_mv` = {} AND `id_software` = 6".format(self.id_mv)
        return True if api.database.selectQuery(ueQuery) else False
