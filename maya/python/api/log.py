import os
import logging
import logging.handlers

loggers = {}


################################################################################
# @brief      Sets the logger as singleton-like object
# @details    This procedure check the value of a global defined variable that,
#             by default, is set as None. If the value retrieved is None, then
#             the logger objects are constructed and organized to generate all
#             the logging data required to properly show the level of
#             information requested. If this variable is NOT None, then the
#             variable is returned because contains the main logger object,
#             assigned when the variable was None.
#
# @return     The logger object
#
def logger():

    global loggers

    if 'api.log' in loggers:
        return loggers.get('api.log')

    log_filename = os.path.join('%USERPROFILE%', 'Documents', 'maya', '2022', 'log')
    log_filename = os.path.normpath(log_filename)
    log_filename = os.path.expandvars(log_filename)

    if not os.path.exists(log_filename):
        os.makedirs(log_filename)

    log_filename = os.path.join(log_filename, 'maya.log')
    log_filename = os.path.normpath(log_filename)

    localLogger = logging.getLogger('api.log')

    while localLogger.handlers:
        localLogger.handlers.pop()

    localLogger.propagate = False
    localLogger.setLevel(logging.DEBUG)

    stream_formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(filename)s.%(funcName)s@%(lineno)d - %(message)s')

    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(stream_formatter)
    stream_handler.setLevel(logging.DEBUG)

    file_formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(filename)s.%(funcName)s@%(lineno)d - %(message)s')

    file_handler = logging.handlers.TimedRotatingFileHandler(log_filename, when='d', interval=1, backupCount=10)
    file_handler.setFormatter(file_formatter)
    file_handler.setLevel(logging.DEBUG)
    file_handler.suffix = "%Y%m%d"

    localLogger.addHandler(stream_handler)
    localLogger.addHandler(file_handler)

    loggers.update({'api.log': localLogger})

    return loggers.get('api.log')
