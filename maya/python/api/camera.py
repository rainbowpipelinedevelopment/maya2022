import importlib
import os
import csv

import maya.cmds as cmd
import maya.mel as mel
import xml.dom.minidom as xml

import api.log
import api.widgets.rbw_UI as rbwUI
import api.database

importlib.reload(api.log)
# importlib.reload(rbwUI)
importlib.reload(api.database)


################################################################################
# @brief      Gets the scene camera.
#
# @return     The scene camera.
#
def getSceneCamera():
    cameraQuery = "SELECT `group`, `name`, `variant` FROM `V_assetList` WHERE `name` = 'camera' AND `projectID` = '{}'".format(os.getenv('ID_MV'))
    results = api.database.selectQuery(cameraQuery)

    camerasList = []
    if results:
        for result in results:
            camerasList.append('{}_{}_{}_:rigging_saveNode'.format(result[0], result[1], result[2]))

    try:
        saveNode = cmd.ls(camerasList)
        connSaveNode = cmd.listConnections(saveNode)
        camShape = cmd.listRelatives(connSaveNode[0], allDescendents=True, type='camera')
        camera = cmd.listRelatives(camShape, parent=True)[0]
    except TypeError:
        camera = None
        api.log.logger().error("Error! No camera was found following pipeline naming convention.")

    return camera


############################################################################
# @brief      Locks the unlock camera.
#
# @param      cam   The camera
# @param      lock  The lock
#
def lockUnlockCamera(cam=None, lock=None):
    if not cam:
        cam = api.camera.getSceneCamera()
        api.log.logger().debug("cam: {}".format(cam))
        if not cam:
            return
    refName = cmd.referenceQuery(cam, rfn=1)
    actualLock = cmd.getAttr("{}.locked".format(refName))
    if lock is None:
        lck = not actualLock
    else:
        lck = lock
    if actualLock != lck:
        cmd.file(unloadReference=refName)
        cmd.setAttr("{}.locked".forma(refName), lck)
        cmd.file(loadReference=refName)
    cmd.select(cam)


def exportXml(cam, xmlPath):
    # create xml document
    doc = xml.Document()
    camera = doc.createElement("camera")
    far = doc.createElement("far")
    fcp = cmd.getAttr("{}.farClipPlane".format(cam))
    far_value = doc.createTextNode(str(fcp))
    far.appendChild(far_value)
    near = doc.createElement("near")
    near_value = doc.createTextNode(str(cmd.getAttr("{}.nearClipPlane".format(cam))))
    near.appendChild(near_value)
    focalLength = doc.createElement("focalLength")
    focalLength_value = doc.createTextNode(str(cmd.getAttr("{}.focalLength".format(cam))))
    focalLength.appendChild(focalLength_value)
    angleOfView = doc.createElement("angleOfView")
    angleOfView_value = doc.createTextNode(str(cmd.camera(cam, query=1, hfv=1)))
    angleOfView.appendChild(angleOfView_value)
    hfa = doc.createElement("horizontalFilmAperture")
    hfa_value = doc.createTextNode(str(cmd.getAttr("{}.horizontalFilmAperture".format(cam)) * 25.4))
    hfa.appendChild(hfa_value)
    vfa = doc.createElement("verticalFilmAperture")
    vfa_value = doc.createTextNode(str(cmd.getAttr("{}.verticalFilmAperture".format(cam)) * 25.4))
    vfa.appendChild(vfa_value)
    hfo = doc.createElement("horizontalFilmOffset")
    hfo_value = doc.createTextNode(str(cmd.getAttr("{}.horizontalFilmOffset".format(cam)) * 25.4))
    hfo.appendChild(hfo_value)
    camera.appendChild(far)
    camera.appendChild(near)
    camera.appendChild(focalLength)
    camera.appendChild(angleOfView)
    camera.appendChild(hfa)
    camera.appendChild(vfa)
    camera.appendChild(hfo)
    doc.appendChild(camera)
    cameraInfoXml = open(xmlPath, "w")
    cameraInfoXml.write(doc.toprettyxml())
    cameraInfoXml.close()


def exportFbx(fbxPath):
    plugin = "fbxmaya.mll"
    cmd.loadPlugin(plugin, qt=True)

    startFrame = -30.0
    endFrame = cmd.playbackOptions(q=True, aet=True) + 1

    mel.eval("FBXExportBakeComplexAnimation -v true;")  # bake animation
    mel.eval("FBXExportBakeComplexStart -v {};".format(startFrame))
    mel.eval("FBXExportBakeComplexEnd -v {};".format(endFrame))
    mel.eval("FBXExportInAscii -v true; ")  # ASCII type
    mel.eval("FBXExportFileVersion FBX200611;")  # version of FBX export
    mel.eval('FBXExport -f "{}" -s'.format(fbxPath))
    api.log.logger().debug("Exported FBX here: {}".format(fbxPath))
    return True


def exportAlembic(alembicPath, camera):
    alembicCommand = "-dataFormat ogawa -root {} -file {}".format(camera, alembicPath)
    cmd.AbcExport(j=alembicCommand, verbose=True)


def exportLocators(locatorsPath):
    locTrackerList = cmd.ls('*:LOC_TRACKER')
    locs = []

    for locTracker in locTrackerList:
        locMembers = cmd.sets(locTracker, query=True)

        if locMembers:
            for member in locMembers:
                locs.append(member)

    if len(locs) > 0:
        roots = ' '.join(['-root ' + loc for loc in locs])

        startFrame = -30.0
        endFrame = cmd.playbackOptions(q=True, aet=True) + 1

        alembicCommand = '-frameRange {} {} {} -file {}'.format(startFrame, endFrame, roots, locatorsPath)
        api.log.logger().debug(alembicCommand)
        cmd.AbcExport(j=alembicCommand, verbose=True)
    else:
        return


def newChannelExporter(filename, obj=None):
    if obj:
        cmd.select(obj)
    obj = cmd.ls(selection=True)
    objShapes = cmd.ls(selection=True, dag=True, shapes=True)
    minTime = -30.0
    maxTime = cmd.playbackOptions(query=True, maxTime=True)

    for o in range(0, len(obj)):
        if cmd.nodeType(objShapes[o]) == 'camera':
            attributes = ['translateX', 'translateY', 'translateZ', 'rotateX', 'rotateY', 'rotateZ', 'fl']
        else:
            attributes = ['translateX', 'translateY', 'translateZ', 'rotateX', 'rotateY', 'rotateZ', 'scaleX', 'scaleY', 'scaleZ']

        lines = []

        myFileObject = open(filename, 'w')
        for time in range(int(minTime), int(maxTime) + 1):
            if cmd.nodeType(objShapes[o]) == 'camera':
                for i in range(0, len(attributes), 7):
                    line = (
                        time,
                        round(cmd.getAttr('{}.{}'.format(obj[o], attributes[i]), t=time), 9),
                        round(cmd.getAttr('{}.{}'.format(obj[o], attributes[i + 1]), t=time), 9),
                        round(cmd.getAttr('{}.{}'.format(obj[o], attributes[i + 2]), t=time), 9),
                        round(cmd.getAttr('{}.{}'.format(obj[o], attributes[i + 3]), t=time), 9),
                        round(cmd.getAttr('{}.{}'.format(obj[o], attributes[i + 4]), t=time), 9),
                        round(cmd.getAttr('{}.{}'.format(obj[o], attributes[i + 5]), t=time), 9),
                        round(cmd.getAttr('{}.{}'.format(obj[o], attributes[i + 6]), t=time), 9)
                    )

                    line = (str(el) for el in line)
                    lines.append(line)
            else:
                for i in range(0, len(attributes), 9):
                    line = (
                        time,
                        round(cmd.getAttr(obj[o] + '.' + attributes[i], t=time), 9),
                        round(cmd.getAttr(obj[o] + '.' + attributes[i + 1], t=time), 9),
                        round(cmd.getAttr(obj[o] + '.' + attributes[i + 2], t=time), 9),
                        round(cmd.getAttr(obj[o] + '.' + attributes[i + 3], t=time), 9),
                        round(cmd.getAttr(obj[o] + '.' + attributes[i + 4], t=time), 9),
                        round(cmd.getAttr(obj[o] + '.' + attributes[i + 5], t=time), 9),
                        round(cmd.getAttr(obj[o] + '.' + attributes[i + 6], t=time), 9),
                        round(cmd.getAttr(obj[o] + '.' + attributes[i + 7], t=time), 9),
                        round(cmd.getAttr(obj[o] + '.' + attributes[i + 8], t=time), 9))
                    line = (str(el) for el in line)
                    lines.append(line)

        for line in lines:
            writeThis = '{}\n'.format(" ".join(line))
            myFileObject.writelines(writeThis)

        myFileObject.close()


def bake_camera(selCam=None, inTime=None, outTime=None, isolateCheck=1):
    if not inTime and not outTime:
        inTime = cmd.playbackOptions(query=True, minTime=True)
        outTime = cmd.playbackOptions(query=True, maxTime=True)

    if selCam:
        cmd.select(selCam)

    camNode = cmd.ls(selection=True, dagObjects=True, type='camera')[0]
    api.log.logger().debug('STARTING NODE: {}'.format(camNode))
    selCam = cmd.listRelatives(camNode, parent=True)
    api.log.logger().debug('SELECTED CAM: {}'.format(selCam))
    selCamShape = cmd.listRelatives(selCam[0], shapes=True, type='camera')
    api.log.logger().debug('SELECTED CAM SHAPE: {}'.format(selCamShape))

    api.log.logger().debug("selected camera: {}".format(selCam[0]))
    api.log.logger().debug("selected camera shape: {}".format(selCamShape[0]))

    # unlock camera for the menu bar
    if cmd.camera(selCamShape[0], query=True, lockTransform=True):
        cmd.camera(selCamShape[0], edit=True, lockTransform=False)

    # unlock dei channels (se per caso lockati da animazione)
    mel.eval("source channelBoxCommand;")
    mel.eval('CBunlockAttr "{}.tx"'.format(selCam[0]))
    mel.eval('CBunlockAttr "{}.ty"'.format(selCam[0]))
    mel.eval('CBunlockAttr "{}.tz";'.format(selCam[0]))
    mel.eval('CBunlockAttr "{}.rx";'.format(selCam[0]))
    mel.eval('CBunlockAttr "{}.ry";'.format(selCam[0]))
    mel.eval('CBunlockAttr "{}.rz";'.format(selCam[0]))
    mel.eval('CBunlockAttr "{}.coi";'.format(selCamShape[0]))

    new_baked_camera_name = "BAKED_CAM"
    bakedCam = cmd.duplicate(selCam[0], name=new_baked_camera_name)[0]

    cmd.select(bakedCam)
    bakedCam = cmd.rename(new_baked_camera_name)
    api.log.logger().debug("baked camera after renaming: {}".format(bakedCam))

    bakedCamShape = cmd.listRelatives(bakedCam, shapes=True, type='camera')

    api.log.logger().debug("Camera in scene: {}".format(selCam[0]))
    api.log.logger().debug("Baked cam: {}".format(bakedCam))

    cmd.setAttr('{}.rotateX'.format(bakedCam), keyable=True)
    cmd.setAttr('{}.rotateY'.format(bakedCam), keyable=True)
    cmd.setAttr('{}.rotateZ'.format(bakedCam), keyable=True)

    # Uncheck Auto Render Clip Plane for the baked cam
    cmd.setAttr('{}.bestFitClippingPlanes'.format(bakedCam), False)

    hasParent = cmd.listRelatives(selCam[0], parent=True)
    if hasParent:
        cmd.parent(bakedCam, world=True)
    else:
        pass

    cmd.parentConstraint(selCam[0], bakedCam, weight=True, maintainOffset=True)

    try:
        cmd.setAttr('{}.horizontalFilmOffset'.format(bakedCamShape[0]), keyable=True)
    except:
        pass

    cmd.connectAttr('{}.horizontalFilmAperture'.format(selCamShape[0]), '{}.horizontalFilmAperture'.format(bakedCamShape[0]))
    cmd.connectAttr('{}.verticalFilmAperture'.format(selCamShape[0]), '{}.verticalFilmAperture'.format(bakedCamShape[0]))
    cmd.connectAttr('{}.focalLength'.format(selCamShape[0]), '{}.focalLength'.format(bakedCamShape[0]))
    cmd.connectAttr('{}.fStop'.format(selCamShape[0]), '{}.fStop'.format(bakedCamShape[0]))
    cmd.connectAttr('{}.focusDistance'.format(selCamShape[0]), '{}.focusDistance'.format(bakedCamShape[0]))
    cmd.connectAttr('{}.shutterAngle'.format(selCamShape[0]), '{}.shutterAngle'.format(bakedCamShape[0]))
    cmd.connectAttr('{}.centerOfInterest'.format(selCamShape[0]), '{}.centerOfInterest'.format(bakedCamShape[0]))
    cmd.connectAttr('{}.horizontalFilmOffset'.format(selCamShape[0]), '{}.horizontalFilmOffset'.format(bakedCamShape[0]))

    # Bake near/far clip planes
    cmd.connectAttr('{}.nearClipPlane'.format(selCamShape[0]), '{}.nearClipPlane'.format(bakedCamShape[0]))
    cmd.connectAttr('{}.farClipPlane'.format(selCamShape[0]), '{}.farClipPlane'.format(bakedCamShape[0]))

    # new channels requested from BD for SKT
    cmd.connectAttr('{}.psc'.format(selCamShape[0]), '{}.psc'.format(bakedCamShape[0]))
    cmd.connectAttr('{}.filmTranslateH'.format(selCamShape[0]), '{}.filmTranslateH'.format(bakedCamShape[0]))
    cmd.connectAttr('{}.filmTranslateV'.format(selCamShape[0]), '{}.filmTranslateV'.format(bakedCamShape[0]))
    cmd.connectAttr('{}.horizontalRollPivot'.format(selCamShape[0]), '{}.horizontalRollPivot'.format(bakedCamShape[0]))
    cmd.connectAttr('{}.verticalRollPivot'.format(selCamShape[0]), '{}.verticalRollPivot'.format(bakedCamShape[0]))
    cmd.connectAttr('{}.frv'.format(selCamShape[0]), '{}.frv'.format(bakedCamShape[0]))
    cmd.connectAttr('{}.ptsc'.format(selCamShape[0]), '{}.ptsc'.format(bakedCamShape[0]))

    cmd.setAttr('{}.psc'.format(bakedCamShape[0]), keyable=True)
    cmd.setAttr('{}.filmTranslateH'.format(bakedCamShape[0]), keyable=True)
    cmd.setAttr('{}.filmTranslateV'.format(bakedCamShape[0]), keyable=True)
    cmd.setAttr('{}.horizontalRollPivot'.format(bakedCamShape[0]), keyable=True)
    cmd.setAttr('{}.verticalRollPivot'.format(bakedCamShape[0]), keyable=True)
    cmd.setAttr('{}.frv'.format(bakedCamShape[0]), keyable=True)
    cmd.setAttr('{}.ptsc'.format(bakedCamShape[0]), keyable=True)

    cmd.select(bakedCam)

    if isolateCheck == 1:
        isolateSelected(1)

    cmd.bakeResults(bakedCam, simulation=True, t=(inTime, outTime), sampleBy=True, disableImplicitControl=True, preserveOutsideKeys=True, sparseAnimCurveBake=False, controlPoints=False, shape=True)

    # One more bake for near/far Clip Planes (otherwise they are not baked!)
    nearClipPlane = '{}.nearClipPlane'.format(bakedCam)
    farClipPlane = '{}.farClipPlane'.format(bakedCam)
    cmd.bakeResults(nearClipPlane, farClipPlane, simulation=True, t=(inTime, outTime), sampleBy=True, disableImplicitControl=True, preserveOutsideKeys=True, sparseAnimCurveBake=False, controlPoints=False, shape=True)

    # imposto le tangenti a linear, per esportarle correttamente in AE
    cmd.selectKey(bakedCam)
    cmd.keyTangent(itt="linear", ott="linear")

    if isolateCheck == 1:
        isolateSelected(0)

    hasChild = cmd.listRelatives(bakedCam, children=True, type='transform', fullPath=True)
    if hasChild:
        for c in hasChild:
            cmd.delete(c)

    return cmd.ls(selection=True)


def isolateSelected(flag=1):
    try:
        flag = str(flag)
        panel = mel.eval("getPanel -wf")
        melCmd = ("getPanel -to {}".format(panel))
        if mel.eval(melCmd) == "modelPanel":
            activeModelPanel = panel
            api.log.logger().debug("Your active model panel is: {}".format(activeModelPanel))
        else:
            activeModelPanel = "modelPanel4"

        melCmd = ('enableIsolateSelect {} {}'.format(str(activeModelPanel), flag))
        mel.eval(melCmd)
    except TypeError:
        pass


################################################################################
# @brief      return start and end of continuos intervals of a list of numbers
#
# @param      chanPath    the path of the .chan file
#
def detect_cam_movements_from_chan(chanPath=None):
    if chanPath and os.path.exists(chanPath):

        frames_with_movement = []
        frames_static = []

        with open(chanPath) as chan_file:  # read the .chan file
            chan_csv = list(csv.reader(chan_file, delimiter=" "))
            for c, row in enumerate(chan_csv[:]):
                frameNum = int(row[0])
                if frameNum > 0:  # skip first frame
                    previous = chan_csv[c - 1]
                    current = chan_csv[c]

                    differences = [(previous[i], current[i]) for i in range(1, 7) if previous[i] != current[i]]
                    if differences:
                        frames_with_movement.append(frameNum)
                    else:
                        frames_static.append(frameNum)

        # group continuos numbers
        ranges_str, ranges_lst = detect_continous_numbers(frames_static)

        api.log.logger().debug("Frames with no camera movement: {}".format(ranges_str))

        return ranges_str, ranges_lst

    else:
        rbwUI.RBWError(text="This chan path was not valid or the shot name\ndidn't follow our naming convention: {}".format(chanPath))
        return None


################################################################################
# @brief      return start and end of continuos intervals of a list of numbers
#
# @param      items
#
def detect_continous_numbers(items):
    ranges_list = []
    is_start = False

    # group continuos numbers
    for i in range(len(items)):
        current_frame_num = items[i]
        if i < len(items) - 1:
            next_frame_num = items[i + 1]
            delta = (current_frame_num + 1) - next_frame_num

            if not delta and not is_start:
                if not i:
                    ranges_list.append(current_frame_num)
                    ranges_list.append("-")
                if i + 1 == len(items) - 1:
                    ranges_list.append(current_frame_num + 1)

            elif not delta and is_start:
                ranges_list.append("-")

                if i + 1 == (len(items) - 1):
                    ranges_list.append(next_frame_num)

                is_start = False

            elif delta:
                if current_frame_num not in ranges_list:
                    ranges_list.append(current_frame_num)
                ranges_list.append(",")
                ranges_list.append(next_frame_num)
                is_start = True
        else:
            if current_frame_num not in ranges_list:
                ranges_list.append(current_frame_num)

    # string version
    a = "".join([str(i) for i in ranges_list])
    # list of lists version
    b = [i.split("-") for i in a.split(",")]

    return a, b
