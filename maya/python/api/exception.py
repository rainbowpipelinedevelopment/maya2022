class InvalidAsset(Exception):
    "Raised when the input value are wrong or the asset is turn off or deleted"
    pass


class InvalidSavedAsset(Exception):
    "Raised when the input value are wrong ot the saved asset not exists"
    pass


class InvalidShot(Exception):
    "Raised when the input value are wrong or the shot is turn off or deleted"
    pass


class InvalidSavedShot(Exception):
    "Raised when the input value are wrong ot the saved shot not exists"
    pass


class InvalidAssembly(Exception):
    "Raised when the input value are wrong or the assembly is turn off or deleted"
    pass
