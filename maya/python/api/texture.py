import re
import os

import maya.cmds as cmd
import maya.mel as mel


################################################################################
# @brief      Determines if vectorial map.
#
# @param      filename  The filename
#
# @return     True if vectorial map, False otherwise.
#
def isVectorialMap(filename=None):
    regex = r"\_(C|SI|RC|RFC|FC|N|T|MC|SC|DC|PRC|SRC|OC|Sub|SCC|SSS1C|SSS2C|SSS3C)\d?\."
    matches = re.finditer(regex, filename)
    return bool(len(list(matches)))


################################################################################
# @brief      Determines whether the specified filename is normal map.
#
# @param      filename  The filename
#
# @return     True if the specified filename is normal map, False otherwise.
#
def isNormalMap(filename=None):
    regex = r"\_N\d?\."
    matches = re.finditer(regex, filename)
    return bool(len(list(matches)))


################################################################################
# @brief      List the file nodes in the scene
#
# @param      filter  The filter
#
# @return     A list of file nodes
#
def listTextureNodes(filter=None):
    textures = cmd.ls(type='file')

    if filter is not None:
        pass

    return textures


################################################################################
# @brief      Gets the image from texture node.
#
# @param      node  The node
#
# @return     The image from texture node.
#
def getImageFromTextureNode(node=None):
    return cmd.getAttr('{}.fileTextureName'.format(node))


################################################################################
# @brief      Determines if exr.
#
# @param      filename  The filename
#
# @return     True if exr, False otherwise.
#
def isExr(filename=None):
    if os.path.splitext(filename)[1].replace('.', '').lower() == 'exr':
        return True
    return False


################################################################################
# @brief      Determines if tiff.
#
# @param      filename  The filename
#
# @return     True if tiff, False otherwise.
#
def isTiff(filename=None):
    if 'tif' in os.path.splitext(filename)[1].replace('.', '').lower():
        return True
    return False


################################################################################
# @brief      Determines if udim.
#
# @param      filename  The filename
#
# @return     True if udim, False otherwise.
#
def isUDIM(filename=None, ext='tif'):
    regex = r'\.([\d]{{4}}){}$'.format(ext)
    matches = re.finditer(regex, filename)
    if len(list(enumerate(matches))):
        return True
    return False


################################################################################
# @brief      { function_description }
#
# @param      node      The node
# @param      filename  The filename
#
# @return     { description_of_the_return_value }
#
def remapTextureNode(node=None, filename=None):
    cmd.setAttr('{}.fileTextureName'.format(node), filename, type='string')
    mel.eval("source AEfileTemplate.mel;")
    mel.eval("AEfileTextureReloadCmd {}.fileTextureName;".format(node))

    if isNormalMap(node):
        setColorspaceRaw(node)


################################################################################
# @brief      Sets to mipmap.
#
# @param      node  The node
#
# @return     { description_of_the_return_value }
#
def setFilterTypeMipmap(node):
    cmd.setAttr('{}.filterType'.format(node), 1)


################################################################################
# @brief      Sets the filter type quadratic.
#
# @param      node  The node
#
# @return     { description_of_the_return_value }
#
def setFilterTypeQuadratic(node):
    cmd.setAttr('{}.filterType'.format(node), 3)


################################################################################
# @brief      Sets the color space raw.
#
# @param      node  The node
#
def setColorspaceRaw(node):
    cmd.setAttr('{}.colorSpace'.format(node), 'Raw', type='string')
