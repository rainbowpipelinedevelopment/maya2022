import importlib
import shotgun_api3
import os

import api.log
import api.database

importlib.reload(api.log)
importlib.reload(api.database)


class ShotgridObj(object):

    def __init__(self):

        self.id_mv = os.getenv('ID_MV')
        self.projectName = os.getenv('PROJECT')
        self.content = os.getenv('CONTENT')
        self.mcFolder = os.getenv('MC_FOLDER')

        resolutionQuery = "SELECT width, height FROM `p_prj_resolution` AS pr JOIN `p_resolution` AS r ON pr.resolutionId = r.id WHERE pr.id_mv = {}".format(self.id_mv)
        resolutionTupla = api.database.selectSingleQuery(resolutionQuery)

        fpsQuery = "SELECT fps FROM `p_prj_framerate` AS pf JOIN `p_framerate` AS f ON pf.framerate_id = f.id WHERE pf.id_mv = {}".format(self.id_mv)
        fps = api.database.selectSingleQuery(fpsQuery)[0]

        shotgunIdQuery = "SELECT `shotgunId` FROM `tk_cfg_prj` WHERE `id_mv` = '{}'".format(self.id_mv)
        shotgunId = api.database.selectSingleQuery(shotgunIdQuery)[0]

        self.projectResolution = {'width': int(resolutionTupla[0]), 'height': int(resolutionTupla[1])}
        self.framerate = int(fps)
        self.shotgridProject = {'code': str(self.projectName.upper()), 'type': 'Project', 'id': int(shotgunId)}

    ################################################################################
    # @brief      create the object to connect with shotgrid
    #
    # @param      scriptName  The script name
    #
    # @return     the object.
    #
    def sg(self, scriptName="rbw maya asset"):
        certsPath = os.path.join(
            os.getenv('LOCALPY'),
            'lib',
            'python-api-master',
            'shotgun_api3',
            'lib',
            'httplib2',
            'python3',
            'cacerts.txt'
        ).replace('\\', '/')

        os.environ['SHOTGUN_API_CACERTS'] = certsPath

        scripts_key = {
            "sandboxScript": "2e0a4f1bdd81f6d0d5aacff938ffb1658fc8f35130cac0cdf2131001324a2013",
            "rbw_versioning": "e6cd7e6c9ed3e48deb58896b1e54c96d3be2b5e1205752ccacbee92b56644a34",
            "rbw maya asset": "8e6e286eb433e223f63ab683a7d77cfcf50e6a142f53468a66f70756d6602447"
        }

        param = {
            'base_url': 'https://rainbowcgi.shotgunstudio.com',
            'script_name': scriptName,
            'api_key': scripts_key[scriptName],
            'convert_datetimes_to_utc': True
        }

        return shotgun_api3.Shotgun(**param)

    ################################################################################
    # @brief      get the user shotgrid infoes  by his username
    #
    # @param      username  The username
    #
    # @return     the infoes
    #
    def userIdByName(self, username):
        param = {
            'entity_type': 'HumanUser',
            'filters': [
                ['login', 'contains', username]
            ],

            'fields': ['name']
        }

        return self.sg().find_one(**param)

    ################################################################################
    # @brief      get the pipeline step entity by name.
    #
    # @param      pipelineStepName  The pipeline step name
    #
    # @return     the entity find.
    #
    def pipelineStepIdByName(self, pipelineStepName):
        param = {
            'entity_type': 'Step',
            'filters': [
                ['code', 'contains', pipelineStepName]
            ],
            'fields': []
        }

        return self.sg().find_one(**param)

    ################################################################################
    # @brief      find asset from the asset features.
    #
    # @param      assetCategory  The asset category
    # @param      assetGroup     The asset group
    # @param      assetName      The asset name
    # @param      assetVariant   The asset variant
    #
    # @return     the search results.
    #
    def assetInfoByAsset(self, assetId):
        param = {
            'entity_type': 'Asset',
            'filters': [
                ['project', 'is', self.shotgridProject],
                ['id', 'is', int(assetId)],
                ['sg_status_list', 'is_not', 'omt']
            ],
            'fields': ["id", "delivery_sg_delivered_asset_deliveries", "sg_variant", "sg_3d_asset_type", "sg_group", "description", "code", "cached_display_name", "sg_published_files", 'sg_possible_rnd_variants', 'sg_latest_surfacing', 'sg_first_time_seen']
        }

        return self.sg().find_one(**param)

    ################################################################################
    # @brief      get task entity from pipeline step and asset.
    #
    # @param      pipelineStep   The pipeline step
    # @param      assetId        The asset identifier
    # @param      taskName       The task name
    #
    # @return     the find entity.
    #
    def taskByPipelineStepByAsset(self, pipelineStep, assetId=None, taskName=None):
        param = {
            'entity_type': 'Task',
            'filters': [
                ['step', 'is', self.pipelineStepIdByName(pipelineStep)],
                ['project', 'is', self.shotgridProject]
            ],
            'fields': ['notes', 'task_assignees', 'sg_status_list', 'cached_display_name', 'entity']
        }

        param['filters'].append(['entity', 'is', {'type': 'Asset', 'id': assetId}])

        if taskName:
            param['filters'].append(['cached_display_name', 'is', taskName])

        return self.sg().find(**param)

    ################################################################################
    # @brief      Creates an asset version.
    #
    # @param      versionName  The version name
    # @param      description  The description
    # @param      assetInfo    The asset information
    # @param      taskID       The task id
    # @param      imagePath    The image path
    # @param      status       The status
    #
    def createAssetVersion(self, versionName, description, assetInfo, taskID, imagePath, status='rev'):
        data = {
            'project': self.shotgridProject,
            'code': versionName,
            'description': description,
            'sg_status_list': status,
            'user': self.userIdByName(os.getenv('USERNAME')),
            'entity': assetInfo,
            'sg_task': {
                'type': 'Task',
                'id': taskID
            }
        }
        result = self.sg().create('Version', data)
        self.sg().upload('Version', result['id'], imagePath, field_name='sg_uploaded_movie')

    ################################################################################
    # @brief      Finds project publishes.
    #
    # @param      code       The code
    # @param      dept       The dept
    # @param      projectId  The project identifier
    #
    # @return     the publish list.
    #
    def findProjectPublishes(self, code, dept, projectId):
        param = {
            'entity_type': 'PublishedFile',
            'filters': [
                ["project", "is", self.shotgridProject],
                ["code", "is", code],
                ["sg_dept", "is", dept]
            ],

            'fields': ["id", "sg_delivery", "name", "path", "version", "version_number", "sg_dept", "entity", "project", "delivery_sg_published_files_deliveries"]
        }

        return self.sg(scriptName="rbw_versioning").find(**param)

    ################################################################################
    # @brief      Gets the publish by name.
    #
    # @param      publishName  The publish name
    #
    # @return     The publish by name.
    #
    def getPublishByName(self, publishName):
        publishParam = {
            "entity_type": "PublishedFile",
            "filters": [
                ['code', 'is', publishName]
            ]
        }

        return self.sg().find_one(**publishParam)

    ################################################################################
    # @brief      Creates a scene publish.
    #
    # @param      params       The parameters
    # @param      isMayaScene  Indicates if maya scene
    # @param      uploadThumb  The upload thumb
    # @param      log          The log
    # @param      force        The force
    #
    # @return     new publish if create or None.
    #
    def createScenePublish(self, params, isMayaScene=True, uploadThumb=True, log=True, force=False):
        # check if publish already exists
        if len(self.findProjectPublishes(params["filename"], params["dept"], self.shotgridProject["id"])) > 0 and not force:
            if log:
                api.log.logger().debug("Publish already exists, skipping creation. Use the 'force' to override")
            return None
        else:
            # data structure required by shotgun
            # link to the file path
            fileLink = {
                "local_path": params["path"]
            }

            if isMayaScene:
                # id 4 is the Maya Scene type
                publishedFileType = {
                    "type": "PublishedFileType",
                    "id": 4
                }

            try:
                publishParams = {
                    "project": self.shotgridProject,
                    "description": params["description"],
                    "sg_status_list": params["status"],
                    "path": fileLink,
                    "code": params["filename"],
                    "sg_dept": params["dept"],
                    "version_number": params["versionNumber"],
                    "published_file_type": publishedFileType,
                    "entity": {"type": "Asset", "id": int(params['assetId'])}
                }

                # create the publish
                publish = self.sg(scriptName="rbw_versioning").create("PublishedFile", publishParams)

            except IndexError:
                if log:
                    api.log.logger().debug("No match found in Shotgun for asset {}".format(params["rbw_name"]))
                return None

            # upload the thumbnail of the published file
            if "thumbnailPath" in params.keys() and uploadThumb:
                try:
                    if log:
                        api.log.logger().debug("Publish id: {}".format(publish["id"]))
                        api.log.logger().debug("Thumbnail path: {}".format(params["thumbnailPath"]))
                    self.sg().upload_thumbnail("PublishedFile", publish["id"], params["thumbnailPath"])
                except Exception as err:
                    if log:
                        api.log.logger().error("error on uploading thumbnail: {}".format(err))

            return publish

    ################################################################################
    # @brief      update latest dept field under the given asset.
    #
    # @param      publishFile  The publish file
    #
    # @return     the succef o the update.
    #
    def updateLastDeptPublish(self, publishFile):
        try:
            dept = publishFile['sg_dept']
            updated_data = {
                "sg_latest_{}".format(dept): publishFile
            }
            api.log.logger().debug(60 * '===')
            api.log.logger().debug('updating asset: {}'.format(publishFile['entity']["id"]))
            api.log.logger().debug(60 * '===')
            return self.sg(scriptName="rbw_versioning").update("Asset", publishFile['entity']["id"], updated_data)
        except TypeError:
            api.log.logger().error(60 * '!!!')
            api.log.logger().error('Il publish gia\' esiste, ci sonon problemi con il versionamento')
            api.log.logger().error(60 * '!!!')

    ################################################################################
    # @brief      check asset delivery status
    #
    # @param      assetFullName  The asset full name
    # @param      assetId        The asset identifier
    #
    # @return     the check result.
    #
    def checkAssetDelivered(self, assetFullName, assetId):
        shotgunAsset = self.assetInfoByAsset(assetId)
        if len(shotgunAsset) == 0:
            api.log.logger().error("No related shotgun asset was found for {}".format(assetFullName))
            return

        if len(shotgunAsset[0]["delivery_sg_delivered_asset_deliveries"]) > 0:
            return True
        else:
            return False

    ################################################################################
    # @brief      update the asset delivery status.
    #
    # @param      assetFullName  The asset full name
    # @param      newStatus      The new status
    # @param      assetId        The asset identifier
    #
    # @return     the success or not of the function.
    #
    def updateAssetDeliveryStatus(self, assetFullName, newStatus, assetId):
        shotgunAsset = self.assetInfoByAsset(assetId)
        if len(shotgunAsset) == 0:
            api.log.logger().error("No related shotgun asset was found for {}".format(assetFullName))
            return

        updatedData = {
            "sg_delivery_status": newStatus
        }

        return self.sg().update("Asset", shotgunAsset[0]["id"], updatedData)

    ################################################################################
    # @brief      update sub asset under asset.
    #
    # @param      assetId      The asset identifier
    # @param      assetIdList  The asset identifier list
    #
    # @return     { description_of_the_return_value }
    #
    def updateAssetSubAssets(self, assetId, assetIdList):
        subAssetDicts = []
        for subAssetId in assetIdList:
            subAssetDicts.append({'type': 'Asset', 'id': subAssetId})

        param = {
            'entity_type': 'Asset',
            'entity_id': assetId,
            'data': {
                'assets': subAssetDicts
            }
        }

        api.log.logger().debug(param)

        return self.sg().update(**param)

    ################################################################################
    # @brief      update publish link in asset.
    #
    # @param      assetId      The asset identifier
    # @param      publishList  The publish list
    #
    # @return     { description_of_the_return_value }
    #
    def updatePublishInSet(self, assetId, publishList):
        param = {
            'entity_type': 'Asset',
            'entity_id': assetId,
            'data': {
                'sg_publishes_in_set': publishList
            }
        }

        return self.sg().update(**param)

    ################################################################################
    # @brief      Gets the shot duration.
    #
    # @param      shotId  The shot identifier
    #
    # @return     The shot duration.
    #
    def getShotDuration(self, shotId):
        param = {
            'entity_type': 'Shot',
            'filters': [
                ['id', 'is', shotId]
            ],
            'fields': ['sg_cut_duration', 'sg_frame_count']
        }

        return self.sg().find_one(**param)

    ################################################################################
    # @brief      { function_description }
    #
    # @param      shotId    The shot identifier
    # @param      duration  The duration
    #
    def updateShotDuration(self, shotId, duration):
        newParam = {
            'sg_cut_duration': duration,
            'sg_frame_count': duration
        }

        self.sg().update('Shot', shotId, newParam)

    ################################################################################
    # @brief      update shot sub assets.
    #
    # @param      shotId  The shot identifier
    # @param      assets  The assets
    #
    def updateShotSubAssets(self, shotId, assets):
        self.sg().update('Shot', shotId, {'assets': assets}, {'assets': 'set'})

    ################################################################################
    # @brief      update the frames w/o cam movements
    #
    # @param      self  The obj
    # @param      shotCode  The shot code
    # @param      ranges  The ranges
    #
    def update_frames_with_no_cam_movement(self, shotId, ranges):
        updated_data = {
            "sg_frames_without_cam_movement": ranges
        }

        return self.sg().update("Shot", shotId, updated_data)

    ############################################################################
    # @brief      Gets the frames with no camera movement.
    #
    # @param      shotId  The shot identifier
    #
    # @return     The frames with no camera movement.
    #
    def get_frames_with_no_cam_movement(self, shotId):
        param = {
            'entity_type': 'Shot',
            'filters': [
                ['id', 'is', shotId]
            ],
            'fields': ['sg_frames_without_cam_movement']
        }
        return self.sg().find_one(**param)
