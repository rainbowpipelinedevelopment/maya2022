import importlib
import pymysql

import api.log
importlib.reload(api.log)


################################################################################
# @brief      This class describes a database offline.
#
class DbOffline(Exception):
    pass


################################################################################
# @brief      connect to db.
#
# @return     connection object.
#
def dbConnect(db):
    i = 0
    while i < 10:
        try:
            conn = pymysql.connect(
                host='fickle.rbw.cgi',
                port=3306,
                user='Pipedbusr',
                passwd='5DWQM)D',
                db=db
            )
            return conn
        except pymysql.OperationalError:
            i += 1
            api.log.logger().error("---> Error 2003: Can not connect to MySQL server on fickle.rbw.cgi (timed out) <---")


################################################################################
# @brief      select query function.
#
# @param      query  The query
# @param      args   The arguments
#
# @return     the selected objects.
#
def selectQuery(query, db='rbw_arakno', args=None):
    try:
        conn = dbConnect(db)
        cur = conn.cursor()
        cur.execute(query, args)
        selList = [r for r in cur.fetchall()]
        cur.close()
        return selList
    except pymysql.OperationalError:
        api.log.logger().error(">>> WARNING: DB is offline!!!")
        raise DbOffline(query)


################################################################################
# @brief      select single query function.
#
# @param      query  The query
# @param      args   The arguments
#
# @return     the first object selected.
#
def selectSingleQuery(query, db='rbw_arakno', args=None):
    try:
        conn = dbConnect(db)
        cur = conn.cursor()
        cur.execute(query, args)
        sel_list = cur.fetchone()
        cur.close()
        return sel_list
    except pymysql.OperationalError:
        api.log.logger().error(">>> WARNING: DB is offline!!!")
        raise DbOffline(query)


################################################################################
# @brief      insert query function.
#
# @param      query  The query
# @param      args   The arguments
#
# @return     the id of the insert object.
#
def insertQuery(query, db='rbw_arakno', args=None):
    try:
        conn = dbConnect(db)
        cur = conn.cursor()
        cur.execute(query, args)
        cur._executed
        db_id = cur.lastrowid
        conn.commit()
        cur.close()

        return db_id
    except pymysql.OperationalError:
        api.log.logger().error(">>> WARNING: DB is offline!!!")
        raise DbOffline(query)


################################################################################
# @brief      delete query function.
#
# @param      query  The query
# @param      args   The arguments
#
def deleteQuery(query, db='rbw_arakno', args=None):
    try:
        conn = dbConnect(db)
        cur = conn.cursor()
        cur.execute(query, args)
        conn.commit()
        conn.close()
    except pymysql.OperationalError:
        api.log.logger().error(">>> WARNING: DB is offline!!!")
        raise DbOffline(query)
