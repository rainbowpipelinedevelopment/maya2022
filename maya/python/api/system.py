import importlib
import os
from PySide2 import QtWidgets

import maya.cmds as cmd
from maya import OpenMayaUI
from shiboken2 import wrapInstance

import api.database
import config.dictionaries

importlib.reload(api.database)
importlib.reload(config.dictionaries)


################################################################################
# @brief      Gets the user identifier.
#
# @return     The user identifier.
#
def getUserId():
    username = os.getenv('USERNAME')
    userIdQuery = "SELECT `id_user` FROM `tk_dim_user` WHERE `username`='{}'".format(username)

    userId = api.database.selectSingleQuery(userIdQuery)[0]

    return userId


################################################################################
# @brief      { function_description }
#
# @param      dirname       The dirname
# @param      filewildcard  The filewildcard
#
# @return     { description_of_the_return_value }
#
def list_file(dirname, filewildcard='*'):
    return cmd.getFileList(folder=dirname, filespec=filewildcard)


################################################################################
# @brief      Gets the maya window.
#
# @return     The maya window.
#
def getMayaWin():
    win_ptr = OpenMayaUI.MQtUtil.mainWindow()
    return wrapInstance(int(win_ptr), QtWidgets.QMainWindow)


################################################################################
# @brief      Gets the project content.
#
# @param      id_mv  The identifier mv
#
# @return     The project content.
#
def getProjectContent(id_mv):
    projectNameQUery = "SELECT `mc_name` FROM `tk_cfg_prj` WHERE `id_mv` = '{}'".format(id_mv)
    projectName = api.database.selectSingleQuery(projectNameQUery)[0]

    projectContent = '//speed.nas/{}/02_production/01_content/mc_{}'.format(projectName, projectName)

    return projectName, projectContent


################################################################################
# @brief      Gets the assets from db.
#
# @param      adds  The adds
#
# @return     The assets from db.
#
def getAssetsFromDB(adds=[]):
    assetQuery = "SELECT `variantID`, `category`, `group`, `name`, `variant` FROM `V_assetList` WHERE `projectID`={}".format(os.getenv('ID_MV'))

    for add in adds:
        assetQuery += ' AND {} {} {}'.format(add[0], add[1], add[2])

    assets = config.dictionaries.Ordered_dict()
    try:
        result = api.database.selectQuery(assetQuery)
        if result and len(result):
            for id, cat, group, nam, var in result:
                assets.setdefault(cat, config.dictionaries.Ordered_dict())
                assets[cat].setdefault(group, config.dictionaries.Ordered_dict())
                assets[cat][group].setdefault(nam, config.dictionaries.Ordered_dict())
                assets[cat][group][nam][var] = id
    except:
        assets = {}

    return assets


################################################################################
# @brief      Gets the shots from db.
#
# @param      adds  The adds
#
# @return     The shots from db.
#
def getShotsFromDB(adds=[]):
    shotQuery = "SELECT `shotID`, `season`, `episode`, `sequence`, `shot` FROM `V_shotList` WHERE `projectID`={}".format(os.getenv('ID_MV'))

    for add in adds:
        shotQuery += ' AND {} {} {}'.format(add[0], add[1], add[2])

    shots = config.dictionaries.Ordered_dict()
    try:
        result = api.database.selectQuery(shotQuery)
        if result and len(result):
            for id, season, episode, sequence, shot in result:
                shots.setdefault(season, config.dictionaries.Ordered_dict())
                shots[season].setdefault(episode, config.dictionaries.Ordered_dict())
                shots[season][episode].setdefault(sequence, config.dictionaries.Ordered_dict())
                shots[season][episode][sequence][shot] = id
    except:
        shots = {}

    return shots
