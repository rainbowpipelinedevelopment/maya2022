import importlib

import maya.cmds as cmd

import api.log
importlib.reload(api.log)


################################################################################
# @brief      This class describes a save node.
#
class SaveNode(object):

    ############################################################################
    # @brief      Constructs the object.
    #
    # @param      self  The object
    #
    def __init__(self):
        self.node = "saveNode"
        if not self.isLoaded():
            api.log.logger().error('saveNode plugin automatcally loaded')
            cmd.loadPlugin('{}.py'.format(self.node))

    ############################################################################
    # @brief      Creates the SaveNode for the given dept, deleting all other
    #             nodes for the same dept
    #
    # @param      self  The object
    # @param      dept  The dept
    #
    def create(self, dept):
        nodes = cmd.ls(type=self.node)
        api.log.logger().debug('nodes: {}'.format(nodes))
        if nodes:
            for node in nodes:
                api.log.logger().debug('node is: {}'.format(node))
                api.log.logger().debug('self.isConnected(node): {}'.format(self.isConnected(node)))
                if node.count(dept):
                    api.log.logger().debug('the node for this dept already exists, delete the previous one: {}'.format(node))
                    cmd.delete(node)

        nName = "_".join([dept, self.node])
        self.savenode = cmd.createNode(self.node, n=nName)

    ############################################################################
    # @brief      connect the save node to the given node.
    #
    # @param      self  The object
    # @param      conn  The connection
    #
    def connect(self, conn):

        api.log.logger().debug('=' * 60)
        api.log.logger().debug('Conn: {}'.format(conn))
        api.log.logger().debug('=' * 60)

        try:
            listConnections = cmd.listConnections(conn)
            api.log.logger().debug('listConnections: {}'.format(listConnections))
            if listConnections:
                source, destination = cmd.listConnections(conn, connections=True, plugs=True)
                api.log.logger().warning("source: {}".format(str(source)))
                api.log.logger().warning("dest: {}".format(str(destination)))
                cmd.disconnectAttr(destination, source)
        except RuntimeError as re:
            api.log.logger().error(re)
        except Exception as e:
            api.log.logger().error(e)

        cmd.connectAttr('{}.db_id'.format(self.savenode), conn)

    ############################################################################
    # @brief      Determines if connected.
    #
    # @param      self  The object
    # @param      node  The node
    #
    # @return     True if connected, False otherwise.
    #
    def isConnected(self, node):
        try:
            if cmd.listConnections('{}.db_id'.format(node), connections=True, plugs=True):
                return True
        except Exception as e:
            api.log.logger().error(e)
            return False

    ############################################################################
    # @brief      Load the savenode for the given department
    #
    # @param      self           The object
    # @param      dept           The dept
    # @param      noReferences   No references
    # @param      noConnections  No connections
    #
    def load(self, dept, noReferences=None, noConnections=True):
        if noReferences:
            noReferences = ':'
        nodes = cmd.ls(type=self.node)

        if not nodes:
            api.log.logger().error('No saveNodes on scene...')

        for node in nodes:
            if noReferences and node.count(noReferences):
                continue

            if node.count(dept):
                if self.isConnected(node) or noConnections:
                    self.savenode = node
                    break
            elif node.count(dept.split(':')[0]) and node.count(dept.split(':')[-1]):
                if self.isConnected(node) or noConnections:
                    self.savenode = node
                    break
        try:
            type(self.savenode)
        except AttributeError:
            api.log.logger().error('No saveNodes with references on scene for this dept: {}'.format(dept))

    ############################################################################
    # @brief      Gets all.
    #
    # @param      self        The object
    # @param      references  The references
    #
    # @return     All.
    #
    def getAll(self, references=None):
        if references:
            return [node for node in cmd.ls(type=self.node) if node.count(":")]
        return cmd.ls(type=self.node)

    ############################################################################
    # @brief      Determines if loaded.
    #
    # @param      self  The object
    #
    # @return     True if loaded, False otherwise.
    #
    def isLoaded(self):
        return cmd.pluginInfo('{}.py'.format(self.node), query=True, loaded=True, name=True)

    ############################################################################
    # @brief      delete the node.
    #
    # @param      self  The object
    # @param      node  The node
    #
    def deleteNode(node):
        cmd.delete(node)

    ############################################################################
    # @brief      return the SN value for the given attribute
    #
    # @param      self  The object
    # @param      attr  The attribute
    #
    # @return     The attribute.
    #
    def getAttr(self, attr):
        attr = attr.replace(' ', '_')
        attr = cmd.getAttr("{}.{}".format(self.savenode, attr))
        if attr:
            return attr
        else:
            return

    ############################################################################
    # @brief      creates the attribute attr and set the value to the given val
    #
    # @param      self  The object
    # @param      attr  The attribute
    # @param      val   The value
    #
    # @return     { description_of_the_return_value }
    #
    def setAttr(self, attr, val):
        api.log.logger().debug('SAVENODE setAttr')
        api.log.logger().debug('attr: {} = {}'.format(attr, val))

        attr = str(attr.replace(' ', '_'))
        cmd.addAttr(self.savenode, shortName=attr, dt='string')
        cmd.setAttr('{}.{}'.format(self.savenode, attr), val, lock=True, type='string')
