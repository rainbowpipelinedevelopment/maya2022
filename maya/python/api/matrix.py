import importlib
import math

import api.log

importlib.reload(api.log)


################################################################################
# @brief      Gets the matrix from vectors.
#
# @param      translation  The translation
# @param      rotation     The rotation
# @param      scale        The scale
#
# @return     The matrix from vectors.
#
def getMatrixFromVectors(translation=[0.0, 0.0, 0.0], rotation=[0.0, 0.0, 0.0], scale=[1.0, 1.0, 1.0]):

    tx = translation[0]
    ty = translation[1]
    tz = translation[2]

    sx = scale[0]
    sy = scale[1]
    sz = scale[2]

    cosa = math.cos(math.radians(rotation[0]))
    sina = math.sin(math.radians(rotation[0]))
    cosb = math.cos(math.radians(rotation[1]))
    sinb = math.sin(math.radians(rotation[1]))
    cosc = math.cos(math.radians(rotation[2]))
    sinc = math.sin(math.radians(rotation[2]))

    matrix = [
        sx * cosb * cosc,
        sx * cosb * sinc,
        -sx * sinb,
        0.0,
        sy * ((-cosa * sinc) + (sina * sinb * cosc)),
        sy * ((cosa * cosc) + (sina * sinb * sinc)),
        sy * sina * cosb,
        0.0,
        sz * ((sina * sinc) + (cosa * sinb * cosc)),
        sz * ((-sina * cosc) + (cosa * sinb * sinc)),
        sz * cosa * cosb,
        0.0,
        tx,
        ty,
        tz,
        1.0
    ]

    for index in range(0, len(matrix)):
        if abs(matrix[index]) == 0.0:
            matrix[index] = 0.0

    return matrix


################################################################################
# @brief      Gets the matrix index.
#
# @param      i     { parameter_description }
# @param      j     { parameter_description }
#
# @return     The matrix index.
#
def getMatrixIndex(i, j):
    return i * 4 + j


################################################################################
# @brief      multiply the given matrix.
#
# @param      matrix1  The matrix 1
# @param      matrix2  The matrix 2
#
# @return     { description_of_the_return_value }
#
def multiplyMatrix(matrix1, matrix2):
    resultMatrix = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

    for i in range(0, 4):
        for j in range(0, 4):
            n = 0

            for k in range(0, 4):
                n += (matrix1[getMatrixIndex(i, k)] * matrix2[getMatrixIndex(k, j)])

            resultMatrix[getMatrixIndex(i, j)] = n

    for index in range(0, len(resultMatrix)):
        if abs(resultMatrix[index]) == 0.0:
            resultMatrix[index] = 0.0

    return resultMatrix


################################################################################
# @brief      multiply the given vector for the given matrix
#
# @param      matrix  The matrix
# @param      vector  The vector
#
# @return     the result vector
#
def multiplyVector(matrix, vector):
    realVector = [vector[0], vector[1], vector[2], 1.0]
    resultVector = [0.0, 0.0, 0.0, 0.0]

    for i in range(0, 4):
        for j in range(0, 4):
            n = 0

            for k in range(0, 4):
                n += (realVector[k] * matrix[getMatrixIndex(k, j)])

            resultVector[j] = n

    for i in range(0, 4):
        if abs(resultVector[i]) == 0.0:
            resultVector[i] = 0.0

    if resultVector[3] != 1:
        return [resultVector[0] / resultVector[3], resultVector[1] / resultVector[3], resultVector[2] / resultVector[3]]
    else:
        return resultVector[:-1]


################################################################################
# @brief      Prints a matrix.
#
# @param      mat   The matrix
#
def printMatrix(mat):
    output = ''
    for i in range(0, 4):
        output = '{}{}\t'.format(output, mat[i])
    output = '{}\n'.format(output)

    for i in range(0, 4):
        output = '{}{}\t'.format(output, mat[i + 4])
    output = '{}\n'.format(output)

    for i in range(0, 4):
        output = '{}{}\t'.format(output, mat[i + 8])
    output = '{}\n'.format(output)

    for i in range(0, 4):
        output = '{}{}\t'.format(output, mat[i + 12])
    output = '{}\n'.format(output)

    print(output)
