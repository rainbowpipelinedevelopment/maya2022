import importlib
import os
import time

import maya.cmds as cmd
import maya.OpenMaya as mayaApi
import maya.OpenMayaUI as mayaApiUI

import api.scene
importlib.reload(api.scene)


################################################################################
# @brief      get the 4 pipeline snapshot.
#
# @param      filename  The filename
# @param      ext       The extent
#
def snapshots(filename=None, ext="jpg", rigging=False):
    if not filename:
        filepath = api.scene.scenename()
    else:
        filepath = filename

    dir = os.path.dirname(filepath)
    dir = os.path.join(dir, "snapshots")

    if not os.path.isdir(dir):
        os.makedirs(dir)

    filename = api.scene.filename()
    filename = filename[:filename.rfind(".")]

    # toggle grid visibility - TURN OFF GRID
    currState = cmd.grid(toggle=True, query=True)
    cmd.grid(toggle=(currState == 0))

    # clear selection for better visibility
    cmd.select(clear=True)

    # view the smooth shade model version
    modelPanel = getVisibleModelPanels()[0]
    if rigging:
        cmd.modelEditor(modelPanel, edit=True, displayAppearance='smoothShaded', displayTextures=True)
    else:
        cmd.modelEditor(modelPanel, edit=True, displayAppearance='smoothShaded')

    cmd.viewSet(rightSide=True)
    cmd.viewFit()
    imageName = "{}_side".format(filename)
    grabViewport(dir, imageName, ext)

    cmd.viewSet(front=True)
    cmd.viewFit()
    imageName = "{}_front".format(filename)
    grabViewport(dir, imageName, ext)

    cmd.viewSet(top=True)
    cmd.viewFit()
    imageName = "{}_top".format(filename)
    grabViewport(dir, imageName, ext)

    cmd.viewSet(persp=True)
    cmd.viewFit()
    imageName = "{}_persp".format(filename)
    grabViewport(dir, imageName, ext)

    # toggle grid visibility - TURN ON GRID
    currState = cmd.grid(toggle=True, query=True)
    cmd.grid(toggle=(currState == 0))


################################################################################
# @brief      take a snapshot from the active viewport.
#
# @param      directory  The directory
# @param      imageName  The image name
# @param      ext        The extent
#
def grabViewport(directory="", imageName="", ext="jpg"):
    if not directory:
        user = os.getenv("USERNAME")
        directory = os.path.join("C:/Users", user, "Documents")

    if not imageName:
        sceneName = api.scene.filename()
        if sceneName:
            imageName = os.path.basename(sceneName).split(".")[0]
        else:
            imageName = "snapshots_{}".format(time.strftime("%d%m%y_%H%M%S", time.localtime()))

    viewport = mayaApiUI.M3dView.active3dView()
    viewport.refresh()

    img = mayaApi.MImage()
    img.create(1280, 720)
    viewport.readColorBuffer(img, True)

    filePath = os.path.join(directory, '{}.{}'.format(imageName, ext))
    api.log.logger().debug(filePath)
    img.writeToFile(filePath, ext)


################################################################################
# @brief      Gets the visible model panels.
#
# @return     The visible model panels.
#
def getVisibleModelPanels():
    modelPanels = cmd.getPanel(type="modelPanel")
    visiblePanels = cmd.getPanel(visiblePanels=True)
    return list(set(modelPanels) & set(visiblePanels))
