import os
import maya.OpenMaya as OpenMaya
import maya.OpenMayaUI as mui
import shiboken2
from PySide2 import QtWidgets


def getMayaWindow():
    """!
    Get the maya main window as a `QMainWindow` instance
    """

    ptr = mui.MQtUtil.mainWindow()
    if ptr:
        return shiboken2.wrapInstance(int(ptr), QtWidgets.QWidget)


def grabViewport(fileName="", ext="jpg"):
    """!
    Take a snapshot of the active viewport

    @param fileName: name of the snapshot
    @param ext: image extension
    """
    if not os.path.exists(os.path.dirname(fileName)):
        os.makedirs(os.path.dirname(fileName))

    viewport = mui.M3dView.active3dView()
    viewport.refresh()

    img = OpenMaya.MImage()
    img.create(1280, 720)
    viewport.readColorBuffer(img, True)

    img.writeToFile('{}.{}'.format(fileName, ext), ext)
    os.startfile('{}.{}'.format(fileName, ext))
