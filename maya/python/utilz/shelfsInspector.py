import imp
import importlib
import pkgutil

import api.log

importlib.reload(api.log)


################################################################################
# @brief      This class describes a shelfs inspector.
#
class shelfsInspector(object):

    packageDictVar = "PackageButtons"

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      package   The package
    # @param      subpaths  The subpaths
    #
    def __init__(self, package, subpaths=None):

        path = []
        if subpaths is not None:
            if not isinstance(subpaths, list):
                if isinstance(subpaths, str):
                    subpaths = [subpaths]
                else:
                    api.log.logger().debug('{} is not a \'list\' and not a \'string\''.format(subpaths))
            for subpath in subpaths:
                module_info = imp.find_module(subpath)
                if module_info is not None:
                    path.append(module_info[1])

        api.log.logger().debug("imp.find_module({},{}) INSPECTOR SHELF".format(package, str(path)))

        infos = imp.find_module(package, path)

        if infos is None:
            raise Exception()

        self.start_package_path = infos[1]
        self.start_package_name = package
        self.module = imp.load_module(package, *infos)
        self.buttonPackages = []

        if hasattr(self.module, "__path__"):
            self.walkPackages()

    ############################################################################
    # @brief      walk into module to find buttons.
    #
    def walkPackages(self):
        for loader, modname, ispkg in pkgutil.walk_packages(self.module.__path__, self.module.__name__ + "."):
            if ispkg:
                module = loader.find_module(modname).load_module(modname)
                if hasattr(module, self.packageDictVar):
                    self.buttonPackages.append({})
                    for key in getattr(module, self.packageDictVar):
                        self.buttonPackages[-1][key] = getattr(module, self.packageDictVar)[key]
