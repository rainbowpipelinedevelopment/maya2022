import importlib

import maya.cmds as cmd

import api.widgets.rbw_UI as rbwUI

# importlib.reload(rbwUI)


################################################################################
# @brief      Delete base namespace attached to the selected objects
#
def delNameSpaceSelected():
    selectionList = cmd.ls(sl=1)
    if selectionList:
        if len(selectionList):
            for object in selectionList:
                if(cmd.objExists(object)):
                    namespace = getNamespace(object)
                    deleteNamespace(namespace)


################################################################################
# @brief      Get the base namespace from the given obj, or the full namespace
#             path of it.
#
# @param      inputObj  The input object
# @param      fullPath  The full path
#
# @return     The namespace.
#
def getNamespace(inputObj, fullPath=0):
    if len(inputObj) > 0:
        if not fullPath:
            nameSpace = inputObj.split(":")[0].split("|")[-1]
            return ":{}".format(nameSpace)
        else:
            namespaceParts = inputObj.split(":")
            namespaceParts.pop(len(namespaceParts) - 1)
            fullPathNamespace = ":".join(namespaceParts)
            return ":{}".format(fullPathNamespace)
    else:
        rbwUI.RBWError(text="select something")
        return None


################################################################################
# @brief      Empty and remove the given namespace.
#
# @param      namespace  The namespace
#
def deleteNamespace(namespace):
    if len(namespace):
        cmd.namespace(force=True, mv=(namespace, ":"))
        cmd.namespace(removeNamespace=namespace)
