import importlib
import os
from PySide2 import QtWidgets, QtGui, QtCore
import functools

import maya.cmds as cmd

import api.shelf
import api.log
import api.json
import api.database
import api.widgets.rbw_UI as rbwUI
import utilz.toolsInspector as tins
import config.dictionaries

importlib.reload(api.shelf)
importlib.reload(api.log)
importlib.reload(api.json)
importlib.reload(api.database)
# importlib.reload(rbwUI)
importlib.reload(tins)
importlib.reload(config.dictionaries)

settingsPath = api.json.json_read(os.path.join(os.getenv('LOCALPIPE'), 'json_file', 'settings.json'))['settingsPath'].replace('USERNAME', os.getenv('USERNAME'))


################################################################################
# @brief      This class describes a dept window.
#
class DeptWindow(rbwUI.RBWWindow):
    windows = {}

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      dept          The dept
    # @param      package       The package
    # @param      path_package  The path package
    # @param      parent        The parent
    #
    def __init__(self, dept, package=None, path_package=None, parent=None):
        super().__init__()

        if cmd.window('NewDeptWindow', exists=True):
            cmd.deleteUI('NewDeptWindow')

        self.setObjectName("NewDeptWindow")
        self.dept = dept
        self.settings = api.json.json_read(settingsPath)

        if package is None:
            package = dept
        if path_package is None:
            path_package = "depts"

        self.toolInspector = tins.ToolsInspector(package, path_package)

        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.updateLayout()

        self.setTitle('{} interface'.format(self.dept))
        self.setFocus()

    ############################################################################
    # @brief      updates the layout according to the user prefs.
    #
    def updateLayout(self):
        if 'deptTabIconSize' not in self.settings['common']:
            self.settings['common']['deptTabIconSize'] = 0

        if 'deptTabLayout' not in self.settings['common']:
            self.settings['common']['deptTabLayout'] = 0

        self.toolsPkg = self.toolInspector.toolPackages
        self.iconSize = 50 - (self.settings['common']['deptTabIconSize'] * 10)
        self.toolSize = [170 - (self.settings['common']['deptTabIconSize'] * 10), 35 - (self.settings['common']['deptTabIconSize'] * 5)]

        if not self.settings['common']['deptTabLayout']:
            self.layoutWidget = rbwUI.RBWFrame('V', bgColor='transparent', margins=[0, 0, 0, 0])
            mainToolWidget = QtWidgets.QToolBar("Main toolbar")
            mainToolWidget.setIconSize(QtCore.QSize(self.iconSize, self.iconSize))
            mainToolWidget.setObjectName('MainToolBar')
            mainToolWidget.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
            mainToolWidget.setStyleSheet('background-color: rgba(60, 60, 60, 150)')

            mainTools = [x for pkg in self.toolsPkg for x in pkg['tools'] if pkg['name'] == 'main']
            for tool in mainTools:
                toolName = tool["name"]

                if "icon" in tool.keys():
                    iconPath = tool["icon"]
                    icon = QtGui.QIcon(iconPath)
                    toolAction = QtWidgets.QAction(icon, toolName, self)
                else:
                    toolAction = QtWidgets.QAction(toolName, self)

                if "shortcut" in tool.keys():
                    toolAction.setShortcut(tool["shortcut"])

                if "statustip" in tool.keys():
                    toolAction.setStatusTip(tool["statustip"])

                toolAction.triggered.connect(tool["launch"])
                separator = QtWidgets.QAction(self)
                separator.setSeparator(True)

                mainToolWidget.addAction(toolAction)
                mainToolWidget.addAction(separator)
            self.layoutWidget.addWidget(mainToolWidget)

            cWidget = rbwUI.RBWFrame(layout='V', radius=5)

            if any(pkg["name"] == "generic" for pkg in self.toolsPkg):
                genericTools = [x for pkg in self.toolsPkg for x in pkg['tools'] if pkg['name'] == 'generic']
                genericToolWidget = rbwUI.RBWFrame(layout='FL', margins=[7, 7, 7, 7], bgColor='transparent')

                for tool in genericTools:
                    toolParams = {'text': tool['name'], 'size': self.toolSize}

                    if "icon" in tool.keys():
                        toolParams['icon'] = [tool["icon"]]

                    if "statustip" in tool.keys():
                        toolParams['tooltip'] = [tool["statustip"]]

                    button = rbwUI.RBWButton(**toolParams)
                    button.clicked.connect(tool['launch'])
                    genericToolWidget.addWidget(button)
                genericToolWidget.setMinimumHeight(70 - (self.settings['common']['deptTabIconSize'] * 5))
                genericToolWidget.resize(genericToolWidget.sizeHint())
                cWidget.addWidget(genericToolWidget)

            customToolsPkg = sorted(list(filter(lambda x: x["name"] not in ["main", "generic"], self.toolsPkg)), key=lambda x: self.toolInspector.customPackageList.index(x["name"]))
            if customToolsPkg:
                tabToolWidget = rbwUI.RBWTabWidget(size=15, tabSize=120)

                for pkg in customToolsPkg:
                    singleTabWidget = rbwUI.RBWFrame(layout='FL', margins=[5, 5, 5, 5], bgColor='transparent')
                    singleTabWidget.widget.setContentsMargins(5, 5, 5, 5)

                    for tool in pkg['tools']:
                        toolParams = {'text': tool['name'], 'size': self.toolSize}
                        toolParams['text'] = tool["name"]

                        if "icon" in tool.keys():
                            toolParams['icon'] = [tool["icon"]]

                        if "statustip" in tool.keys():
                            toolParams['tooltip'] = [tool["statustip"]]

                        button = rbwUI.RBWButton(**toolParams)
                        button.clicked.connect(tool['launch'])
                        singleTabWidget.addWidget(button)

                    singleTabWidget.setMinimumSize(singleTabWidget.layout.sizeHint().height(), singleTabWidget.layout.sizeHint().width())
                    singleTabWidget.setLayout(singleTabWidget.layout)

                    tabToolWidget.addTab(singleTabWidget, pkg["name"])
                tabToolWidget.setMinimumHeight(100)
                tabToolWidget.resize(tabToolWidget.sizeHint())
                cWidget.addWidget(tabToolWidget)

            self.layoutWidget.addWidget(cWidget)

            self.wWidth = 901 - (self.settings['common']['deptTabIconSize'] * 45)
            self.wHeight = 400 - (self.settings['common']['deptTabIconSize'] * 30)

            self.setMinimumWidth(901 - (self.settings['common']['deptTabIconSize'] * 45))
            self.setMinimumHeight(340 - (self.settings['common']['deptTabIconSize'] * 30))
            self.setMaximumHeight(450 - (self.settings['common']['deptTabIconSize'] * 30))

        else:
            self.layoutWidget = rbwUI.RBWFrame('H', bgColor='transparent', margins=[0, 0, 0, 0], spacing=0)
            self.categoryLayout = rbwUI.RBWFrame('V', bgColor='transparent', margins=[0, 10, 0, 10], spacing=2)
            self.categoryLayout.setMaximumWidth(100)
            self.previousCategory = None
            self.categoryButtons = {}
            self.setPackage([x for x in self.toolsPkg if x['name'] == 'main'])
            self.setPackage([x for x in self.toolsPkg if x['name'] == 'generic'])
            self.setPackage([x for x in self.toolsPkg if x['name'] not in ['main', 'generic']])
            self.categoryLayout.addStretch()
            toolScroll = rbwUI.RBWScrollArea(layout='V', margins=[0, 0, 0, 0])
            self.toolArea = rbwUI.RBWFrame('V')
            toolScroll.addWidget(self.toolArea)
            self.toolBox = rbwUI.RBWFrame(layout='FL', margins=[5, 5, 5, 5], spacing=3)
            self.layoutWidget.addWidget(self.categoryLayout)
            self.layoutWidget.addWidget(toolScroll)
            self.categoryButtons['main'].click()

            self.wWidth = 690 - (self.settings['common']['deptTabIconSize'] * 45)
            self.wHeight = 300 - (self.settings['common']['deptTabIconSize'] * 30)

            self.setMinimumWidth(690 - (self.settings['common']['deptTabIconSize'] * 45))
            self.setMinimumHeight(300 - (self.settings['common']['deptTabIconSize'] * 30))
            self.setMaximumHeight(450 - (self.settings['common']['deptTabIconSize'] * 30))

        self.mainLayout.addWidget(self.layoutWidget)

        self.layoutBox = rbwUI.RBWFrame('H', bgColor='rgb(80, 80, 80)')
        layoutLabel = rbwUI.RBWLabel('Tab layout:', size=12, alignment='AlignCentre')
        self.layoutButton = rbwUI.RBWButton(icon=['tabLayout.png'], tooltip=['Change tab layout'], size=[24, 24])
        self.layoutButton.clicked.connect(functools.partial(self.updatePreference, 'deptTabLayout'))
        self.iconSizeButton = rbwUI.RBWButton(icon=['iconSize_{}.png'.format(self.settings['common']['deptTabIconSize'])], tooltip=['Change tools icons size'], size=[24, 24])
        self.iconSizeButton.clicked.connect(functools.partial(self.updatePreference, 'deptTabIconSize'))
        self.shelfButton = rbwUI.RBWButton(icon=['shelf.png'], tooltip=['Create tools shelf'], size=[24, 24])
        self.shelfButton.clicked.connect(functools.partial(self.updatePreference, 'shelf'))
        self.layoutBox.addWidget(layoutLabel)
        self.layoutBox.addWidget(self.layoutButton)
        self.layoutBox.addWidget(self.iconSizeButton)
        self.layoutBox.addWidget(self.shelfButton)

        self.titleBar.layout.addWidget(self.layoutBox)
        self.titleBar.layout.addWidget(self.titleBar.minimizeButton)
        self.titleBar.layout.addWidget(self.titleBar.resizeButton)
        self.titleBar.layout.addWidget(self.titleBar.closeButton)
        self.titleBar.setStyleSheet('background-color: {}; border-radius: 7px;'.format(config.dictionaries.deptsColors[self.dept]))

        self.resize(self.wWidth, self.wHeight)

    ############################################################################
    # @brief      update the user preferences.
    #
    # @param      pref  The pref to be updated
    #
    def updatePreference(self, pref):
        if pref == 'shelf':
            shelf = api.shelf.createShelf('RBW {}'.format(self.dept.upper()))
            for tool in [x for pkg in self.toolsPkg for x in pkg['tools']]:
                api.shelf.addButton(
                    label=tool['name'],
                    command=tool['launch'],
                    image=tool['icon'],
                    shelfLayout=shelf,
                    enableOverlayLabel=False
                )
        elif pref == 'deptTabLayout':
            self.settings['common'][pref] = 0 if self.settings['common'][pref] else 1
        else:
            self.settings['common'][pref] += 1
            if self.settings['common'][pref] == 3:
                self.settings['common'][pref] = 0

        api.json.json_write(self.settings, settingsPath)

        self.layoutBox.deleteLater()
        self.layoutWidget.deleteLater()
        self.updateLayout()

    ############################################################################
    # @brief      builds category buttons for layout 1.
    #
    # @param      packages  The tool package to be built
    #
    def setPackage(self, packages):
        for pkg in packages:
            button = rbwUI.RBWButton(pkg['name'].capitalize(), size=[150, 20], radius=[5, 0, 0, 5], align='left', checkable=True)
            button.clicked.connect(functools.partial(self.changeCategory, button, pkg))
            self.categoryLayout.addWidget(button)
            self.categoryButtons[pkg['name']] = button

    ############################################################################
    # @brief      update the selected category for layout 1.
    #
    # @param      button   the clicked button
    # @param      button   the related tools of the selected category
    #
    def changeCategory(self, button, pkg):
        self.toolBox.deleteLater()
        self.toolBox = rbwUI.RBWFrame(layout='FL', bgColor='transparent', margins=[5, 5, 5, 5], spacing=3)

        if self.previousCategory and self.previousCategory != button:
            self.previousCategory.setChecked(False)

        self.previousCategory = button
        for tool in pkg['tools']:
            toolParams = {'text': tool['name'], 'size': self.toolSize}

            if "icon" in tool.keys():
                toolParams['icon'] = [tool["icon"]]

            if "statustip" in tool.keys():
                toolParams['tooltip'] = [tool["statustip"]]

            button = rbwUI.RBWButton(**toolParams)
            button.clicked.connect(tool['launch'])
            self.toolBox.addWidget(button)
        self.toolBox.setMinimumHeight(70 - (self.settings['common']['deptTabIconSize'] * 5))
        self.toolBox.resize(self.toolBox.sizeHint())
        self.toolArea.addWidget(self.toolBox)

    ############################################################################
    # @brief      Connects a function.
    #
    # @param      object  The object
    # @param      signal  The signal
    # @param      tool    The tool
    #
    def connectFunction(self, object, signal, tool):

        def extendedFunction(tool):
            userIdQuery = "SELECT `id_user` FROM `tk_dim_user` WHERE username='{}'".format(os.getenv('USERNAME'))
            api.log.logger().debug(userIdQuery)
            userId = api.database.selectQuery(userIdQuery)[0][0]

            insertQuery = "INSERT INTO `toolStats` (`toolName`, `userId`, `idMv`) VALUES ('{}', '{}', '{}')".format(tool["name"], userId, os.getenv('ID_MV'))
            api.log.logger().debug(insertQuery)
            api.database.insertQuery(insertQuery)

            tool["launch"]()

        self.connect(object, QtCore.SIGNAL('{}()'.format(signal)), functools.partial(extendedFunction, tool))

    @classmethod
    def showInterface(cls, *args):

        if args[0] in cls.windows:
            try:
                cls.windows[args[0]].close()
            except:
                api.log.logger().error("Window already deleted. It can not be closed.")

        cls.windows[args[0]] = cls(*args)
        cls.windows[args[0]].show()
