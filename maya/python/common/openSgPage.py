import importlib
import os
import webbrowser

import api.scene
import api.savedAsset
import api.savedShot
import api.exception
import api.database

importlib.reload(api.scene)
importlib.reload(api.savedAsset)
importlib.reload(api.savedShot)
importlib.reload(api.exception)
importlib.reload(api.database)


################################################################################
# @brief      Opens a sg page.
#
def openSgPage():
    sceneName = api.scene.scenename(relative=True)

    if '/animation/' in sceneName:
        try:
            savedShot = api.savedShot.SavedShot(params={'path': sceneName})
            sgId = savedShot.shot.shotIdSg
            sgPageQuery = "SELECT `shots_page` FROM `sg_pages` WHERE `id_mv` = '{}'".format(os.getenv('ID_MV'))
            sgPageNumber = api.database.selectSingleQuery(sgPageQuery)[0]

            url = "https://rainbowcgi.shotgunstudio.com/page/{}#Shot_{}".format(sgPageNumber, sgId)

        except api.exception.InvalidSavedShot:
            return
    else:
        try:
            savedAsset = api.savedAsset.SavedAsset(params={'path': sceneName})
            sgId = savedAsset.asset.shotgunId
            sgPageQuery = "SELECT `assets_page` FROM `sg_pages` WHERE `id_mv` = '{}'".format(os.getenv('ID_MV'))
            sgPageNumber = api.database.selectSingleQuery(sgPageQuery)[0]

            url = "https://rainbowcgi.shotgunstudio.com/page/{}#Asset_{}".format(sgPageNumber, sgId)

        except api.exception.InvalidSavedAsset:
            return

    if os.path.exists('C:/Program Files (x86)/Google/Chrome/Application/chrome.exe'):
        chrome_path = 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s'

    elif os.path.exists('C:/Program Files/Google/Chrome/Application/chrome.exe'):
        chrome_path = 'C:/Program Files/Google/Chrome/Application/chrome.exe %s'

    try:
        webbrowser.get(chrome_path).open(url)
    except:
        webbrowser.open(url)
