import importlib

import maya.cmds as cmd


def isArray(a):
    try:
        b = a[:]
        b.append(None)
    except:
        return 0
    return 1


class editStr:
    stringContent = ""
    lineBuffer = []

    def __init__(self, strContent=""):
        self.setString(strContent)

    def setString(self, strContent):
        self.stringContent = strContent

    def getString(self):
        return self.stringContent

    def putLinesInBuffer(self):
        self.lineBuffer = self.stringContent.split('\n')

    def setFromLineBuffer(self):
        self.stringContent = '\n'.join(self.lineBuffer)

    def insertLineInBuffer(self, index, newString):
        self.lineBuffer.insert(index, newString)

    def removeLineInBuffer(self, fromLine, toLine=-99999):
        if (toLine == -99999):
            del self.lineBuffer[fromLine]
        else:
            del self.lineBuffer[fromLine:toLine]


class editExp:
    defaultExpressionName = "EXP_rigDescription_001"
    expressionName = ""
    updateCmd = ""

    def __init__(self, expName="", expCmd="", newIfExists=0):
        if (newIfExists):   # in case we don't want to overwrite an existing expression
            if(cmd.objExists(expName)):
                expName = cmd.createNode("expression", name=expName)
        self.expressionName = expName       # attach the instance to the expression named expName
        if cmd.objExists(self.expressionName):
            if (expCmd != ""):            # update the expression command only if argument is defined
                self.setExpCmd(expCmd)
        else:
            self.setExpCmd(expCmd)

    def setExpName(self, expName):
        if (self.expressionName != ""):
            if cmd.objExists(self.expressionName):
                cmd.rename(self.expressionName, expName)
                self.expressionName = expName
            else:
                self.expressionName = expName
        else:
            self.expressionName = self.defaultExpressionName

    def setExpCmd(self, expCmd):
        self.updateCmd = expCmd
        self.update()

    # create the expression if it doesn't exist, otherwise update the expression command
    def update(self):
        if cmd.objExists(self.expressionName):
            cmd.expression(self.expressionName, s=self.updateCmd, edit=True)
        else:
            cmd.expression(s=self.updateCmd, name=self.expressionName)

    def getExistingExpCmd(self):
        if cmd.objExists(self.expressionName):
            return(cmd.expression(self.expressionName, s=True, query=True))

class rigDescriptionManager(editStr):

    mainMarker = '//-'
    connectingVariable = '$obj'
    dummyAttribute = 'message'
    commentSeparator = '///'
    descriptionSign = '/:/'
    infoEntryChar = "// "
    splitArrayChar = " , "

    # store an expression editor object
    mainExp = None

    def __init__(self, expName="", expCmd="", newIfExists=1):
        self.mainExp = editExp(expName, expCmd, newIfExists)  # attach the instance to the expression named expName
        if (expCmd != ""):                        # update the expression command only if argument is defined
            self.mainExp.setExpCmd(expCmd)
        self.copyExpCommandInEditString()

    def updateExp(self):
        self.mainExp.setExpCmd(self.getString())
        self.mainExp.update()

    def buildMarkerLine(self, markerName):
        return self.mainMarker + ' ' + markerName

    def buildMarkerEntryString(self, objectName, comments):
        return u'' + self.connectingVariable + ' = ' + objectName + '.' + self.dummyAttribute + '; // ' + comments

    def buildInfoEntryString(self, tag, value):
        return u'' + self.infoEntryChar + tag + ' = ' + str(value)

    def findMarker(self, markerName):
        returnArray = []
        i = 0
        for line in self.lineBuffer:
            if (line.strip() == self.buildMarkerLine(markerName)):
                returnArray.append(i)
            i += 1
        return returnArray

    def findMarkerEnd(self, blockStartingLine):
        i = blockStartingLine
        for line in self.lineBuffer[i:]:
            line = line.strip()
            if (line[:len(self.mainMarker)] == self.mainMarker):
                break
            i += 1
        return i

    def markerExists(self, markerName):
        markerName = markerName.strip()
        markerExists = False
        foundMarker = self.findMarker(markerName)
        if((len(foundMarker))):
            markerExists = True
        return markerExists

    def findMarkerRange(self, markerName, occurrence=1):
        if(occurrence < 1):
            return [-1, -1]

        markerName = markerName.strip()
        markerOccurences = self.findMarker(markerName)

        if(occurrence > len(markerOccurences)):
            return [-1, -1]

        firstLine = self.findMarker(markerName)[occurrence - 1]
        endLine = self.findMarkerEnd(firstLine + 1)
        return [firstLine + 1, endLine]

    def findObjectInMarker(self, markerName, objectName, occurrence=1):
        lineList = []
        markerName = markerName.strip()
        markerRange = self.findMarkerRange(markerName, occurrence)
        for i in range(markerRange[0], markerRange[1]):
            if(self.isObjectInLine(objectName, self.lineBuffer[i])):
                lineList.append(i)
        return lineList

    def findTagInMarker(self, markerName, tag, occurrence=1):
        lineList = []
        markerRange = self.findMarkerRange(markerName, occurrence)
        for i in range(markerRange[0], markerRange[1]):
            if(self.isTagInLine(tag, self.lineBuffer[i])):
                lineList.append(i)
        return lineList

    def copyExpCommandInEditString(self):
        self.setString(self.mainExp.getExistingExpCmd())

    def copyExpCommandInLineBuffer(self):
        self.copyExpCommandInEditString()
        self.putLinesInBuffer()

    def copyLineBufferInExpCommand(self):
        self.setFromLineBuffer()
        self.mainExp.setExpCmd(self.stringContent)

    def addMarker(self, markerName, ignoreExisting=False, insertAtLine=-1):
        self.copyExpCommandInLineBuffer()
        if(not(self.markerExists(markerName)) or ignoreExisting):
            if(insertAtLine >= 0):
                self.insertLineInBuffer(insertAtLine, self.buildMarkerLine(markerName))
            else:
                lastLine = len(self.lineBuffer)
                self.insertLineInBuffer(lastLine, self.buildMarkerLine(markerName))
        self.copyLineBufferInExpCommand()

    def removeMarker(self, markerName, occurrence=1):
        self.copyExpCommandInLineBuffer()
        mrkRange = self.findMarkerRange(markerName, occurrence)
        if (mrkRange[0] >= 0):
            self.removeLineInBuffer(mrkRange[0] - 1, mrkRange[1])
        self.copyLineBufferInExpCommand()

    def addEntryInMarker(self, markerName, objectName, comments="", occurrence=1):
        self.copyExpCommandInLineBuffer()
        if not(self.markerExists(markerName)):
            self.addMarker(markerName)
        existingIndex = self.findObjectInMarker(markerName, objectName, occurrence)
        if (len(existingIndex)):
            self.removeEntryFromMarker(markerName, objectName, occurrence)
            self.insertLineInBuffer(existingIndex[0], self.buildMarkerEntryString(objectName, comments))
        else:
            markerRange = self.findMarkerRange(markerName, occurrence)
            if(markerRange[0] >= 0):
                self.insertLineInBuffer(markerRange[1], self.buildMarkerEntryString(objectName, comments))
        self.copyLineBufferInExpCommand()

    def addEntryInInfo(self, markerName, tag, value, occurrence=1):
        self.copyExpCommandInLineBuffer()

        # create a valuestring to save in place of array values (easier to retrive)
        valueStr = str(value)
        if (isArray(value)):
            if(len(value)):
                valueStr = str(value[0])
                for i in range(1, len(value)):
                    valueStr = valueStr + self.splitArrayChar + str(value[i])

        if not(self.markerExists(markerName)):
            self.addMarker(markerName)

        existingIndex = self.findTagInMarker(markerName, tag, occurrence)
        if (len(existingIndex)):
            self.removeEntryFromInfo(markerName, tag, occurrence)
            self.insertLineInBuffer(existingIndex[0], self.buildInfoEntryString(tag, valueStr))
        else:
            markerRange = self.findMarkerRange(markerName, occurrence)
            if(markerRange[0] >= 0):
                self.insertLineInBuffer(markerRange[1], self.buildInfoEntryString(tag, valueStr))
        self.copyLineBufferInExpCommand()

    def geTagValueInLine(self, line):
        startPosition = line.find('=') + 2
        valueString = line[startPosition:len(line)]
        found = valueString.find(self.splitArrayChar)
        if (found >- 1):
            return valueString.split(self.splitArrayChar)
        return valueString

    def getValueTagFromInfo(self, markerName, tag, occurence=1):
        self.copyExpCommandInLineBuffer()
        lineId = self.findTagInMarker(markerName, tag, occurence)
        if(len(lineId)):
            return self.geTagValueInLine(self.lineBuffer[lineId[0]])
        return ""

    def getMarkerList(self):
        self.copyExpCommandInLineBuffer()
        markerList = []
        for line in self.lineBuffer:
            if (line[:len(self.mainMarker)] == self.mainMarker): markerList.append(line[len(self.mainMarker) + 1:])
        return markerList

    def getObjectListInMarker(self, markerName, occurrence=1):
        self.copyExpCommandInLineBuffer()
        objList = []
        markerRange = self.findMarkerRange(markerName, occurrence)
        if (markerRange[0] >= 0):
            for i in range(markerRange[0], markerRange[1]):
                obj_i = self.getObjectInLine(self.lineBuffer[i])
                if(obj_i != ""):
                    objList.append(obj_i)
        return objList

    def isObjectInLine(self, objectName, line):
        startPosition = line.find('=') + 2
        dotPosition = line.find('.')
        objectSlice = line[startPosition:dotPosition]
        if (objectSlice == objectName):
            return True
        return False

    def isTagInLine(self, tag, line):
        startPosition = len(self.infoEntryChar)
        dotPosition = line.find('=') - 1
        objectSlice = line[startPosition:dotPosition]
        if (objectSlice == tag):
            return True
        return False

    def getObjectInLine(self, line):
        objName = ""
        startPosition = line.find('=') + 2
        dotPosition = line.find('.')
        if (startPosition < 2 or dotPosition < 2):
            return objName
        objectSlice = line[startPosition:dotPosition]
        return objectSlice

    def getMarkersFromObject(self, objectName):
        self.copyExpCommandInLineBuffer()
        markerList = self.getMarkerList()
        connectedTo = []
        lastViewedLine = -1
        for marker in markerList:
            occurrence = 1
            markerRange = [-2, -2]
            while (markerRange[0] < lastViewedLine):
                markerRange = self.findMarkerRange(marker, occurrence)
                occurrence += 1
            if(markerRange[0] >= 0):
                for line in range(markerRange[0], markerRange[1]):
                    if (self.isObjectInLine(objectName, self.lineBuffer[line])):
                        connectedTo.append(marker)
                        break
            lastViewedLine = markerRange[1]
        return connectedTo

    def removeEntryFromMarker(self, markerName, objectName, occurrence=1):
        self.copyExpCommandInLineBuffer()
        deleteList = self.findObjectInMarker(markerName, objectName, occurrence)
        while(len(deleteList)):
            del self.lineBuffer[deleteList[0]]
            deleteList = self.findObjectInMarker(markerName, objectName, occurrence)
        self.copyLineBufferInExpCommand()

    def removeEntryFromInfo(self, markerName, tag, occurrence=1):
        self.copyExpCommandInLineBuffer()
        deleteList = self.findTagInMarker(markerName, tag, occurrence)
        while(len(deleteList)):
            del self.lineBuffer[deleteList[0]]
            deleteList = self.findTagInMarker(markerName, tag, occurrence)
        self.copyLineBufferInExpCommand()

    def isOnjectInMarker(self, markerName, objectName, occurrence=1):
        self.copyExpCommandInLineBuffer()
        indexList = self.findObjectInMarker(markerName, objectName, occurrence)
        if len(indexList):
            return True
        return False

    def getCommentInLine(self, line):
        slashIndex = line.find('//')
        if (slashIndex > 0):
            return line[slashIndex + 3:]
        else:
            return ''

    def getEntryComment(self, markerName, objectName, occurrence=1):
        self.copyExpCommandInLineBuffer()
        objectIndex = self.findObjectInMarker(markerName, objectName, occurrence)
        if(len(objectIndex)):
            return self.getCommentInLine(self.lineBuffer[objectIndex[0]])
        else:
            return ''

    def setEntryComment(self, markerName, objectName, newComment, occurrence=1):
        self.copyExpCommandInLineBuffer()
        objectIndex = self.findObjectInMarker(markerName, objectName, occurrence)
        if(len(objectIndex)):
            self.addEntryInMarker(markerName, objectName, newComment, occurrence)

    def clearDescription(self):
        self.lineBuffer = []
        self.copyLineBufferInExpCommand()

    def deleteExpNode(self):
        try:
            cmd.delete(self.mainExp.expressionName)
        except:
            print("RIG DESCRIPTION couldn't delete expression...")

    def getExpNodeName(self):
        return self.mainExp.expressionName

    def getExpressionCommand(self):
        return self.mainExp.getExistingExpCmd()
