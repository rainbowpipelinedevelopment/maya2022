import importlib

import maya.cmds as cmd

import common.makeButton.rigDescriptionManager as rdm

importlib.reload(rdm)


def DM_buttonMaker(name="rivetJoint"):
    '''
    Choose TWO edges and run DM_buttonMaker()
    It makes a joint constrained to selected edges.
    Useful to constraint a piece of geometry to a skinned or tweaked mesh.

    @code
import depts.rigging.tool as rTool
reload(rTool)
rTool.DM_buttonMaker("rivetJoint")
    @endcode
    '''
    sel = cmd.ls(sl=True, fl=True)
    print(len(sel))
    '''
    if len(sel)<2:
        print("Select Two Edges!!!")
    '''
    nameBD1 = sel[0].split(".")[0]
    nameBD2 = sel[1].split(".")[0]
    edgeBD1 = sel[0].split("[")[1].split("]")[0]
    edgeBD2 = sel[1].split("[")[1].split("]")[0]

    if nameBD1 is not nameBD2:
        print("Select Two Edges!!!")

    object = nameBD1
    edge = []
    edge.append(edgeBD1)
    edge.append(edgeBD2)

    curves = []

    for i in range(0, 2):
        edgeCurve = cmd.createNode("curveFromMeshEdge", n="rivetEdge" + str(i))
        cmd.setAttr(edgeCurve + ".ihi", 1)
        cmd.setAttr(edgeCurve + ".ei[0]", int(edge[i]))
        curves.append(edgeCurve)

    loc = cmd.joint(n=name)
    loc = zerpParentGroup(loc, "RVT_" + loc)

    cmd.addAttr(loc, ln="PositionU", at="double", min=0, max=1, dv=0.5)
    cmd.addAttr(loc, ln="PositionV", at="double", min=0, max=1, dv=0.5)
    cmd.setAttr(loc + ".PositionU", e=True, k=True)
    cmd.setAttr(loc + ".PositionV", e=True, k=True)

    curveLoft = cmd.createNode("loft", n="rivetLoft#")
    cmd.setAttr(curveLoft + ".u", 1)
    cmd.setAttr(curveLoft + ".rsn", 1)

    POS = cmd.createNode("pointOnSurfaceInfo", n="rivetPointOnSurfaceInfo#")
    cmd.setAttr(POS + ".top", 1)

    cmd.connectAttr(curveLoft + ".os", POS + ".is", f=True)
    cmd.connectAttr(POS + ".p", loc + ".t", f=True)

    cmd.connectAttr(loc + ".PositionU", POS + ".u", f=True)
    cmd.connectAttr(loc + ".PositionV", POS + ".v", f=True)

    cmd.connectAttr(curves[0] + ".oc", curveLoft + ".ic[0]", f=True)
    cmd.connectAttr(curves[1] + ".oc", curveLoft + ".ic[1]", f=True)

    cmd.connectAttr(object + ".w", curves[0] + " .im", f=True)
    cmd.connectAttr(object + ".w", curves[1] + ".im", f=True)

    aim = cmd.createNode("aimConstraint", p=loc)
    cmd.connectAttr(POS + ".n", aim + ".tg[0].tt", f=True)
    cmd.connectAttr(POS + ".tv", aim + ".wu", f=True)

    cmd.connectAttr(aim + ".cr", loc + ".r")

    aimAttr = cmd.listAttr(aim, k=True)

    for each in aimAttr:
        cmd.setAttr(aim + "." + each, k=True, l=1)

    return loc


def zerpParentGroup(obj, grpName=""):
    """Azzera le trasformazioni dell'oggetto inserendo un gruppo

    Azzera le trasformazioni dell'oggetto inserendo un gruppo in gerarchia, ed assegnandogli il nome dato dall'argomento "grpName"
    @code
import depts.rigging.core.basictools as bt
reload(bt)
bt.zerpParentGroup("null1", "newGroup")
    @endcode
    """
    sel = cmd.ls(sl=True)

    objName = obj
    if (objName.find('|') >= 0):
        objName = obj.split('|')[-1]
    if(grpName == ""):
        grpName = ('GRP_offset_' + objName)

    rd = rdm.rigDescriptionManager("RD___TEMP____")
    rd.clearDescription()
    rd.addEntryInMarker("OBJ", obj)

    grpZero = cmd.group(em=True, name=grpName)
    rd.addEntryInMarker("ZEROGRP", ("|" + grpZero))

    cmd.delete(cmd.parentConstraint(rd.getObjectListInMarker("OBJ")[0], rd.getObjectListInMarker("ZEROGRP")[0]))
    cmd.delete(cmd.scaleConstraint(rd.getObjectListInMarker("OBJ")[0], rd.getObjectListInMarker("ZEROGRP")[0]))

    try:
        parentObjName = cmd.listRelatives(rd.getObjectListInMarker("OBJ")[0], parent=1, f=1)[0]
        cmd.parent(rd.getObjectListInMarker("ZEROGRP")[0], parentObjName)
    except:
        pass
    cmd.parent(rd.getObjectListInMarker("OBJ")[0], rd.getObjectListInMarker("ZEROGRP")[0])

    if(cmd.objectType(rd.getObjectListInMarker("OBJ")[0]) == "joint"):
        cmd.makeIdentity(rd.getObjectListInMarker("OBJ")[0], apply=1, t=1, r=1, s=1, n=0)

    returnObj = rd.getObjectListInMarker("ZEROGRP")[0]
    rd.deleteExpNode()
    del rd

    try:
        if (len(sel)):
            cmd.select(sel)
    except:
        pass

    return returnObj
