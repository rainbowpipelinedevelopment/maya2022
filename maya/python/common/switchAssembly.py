import importlib
import os

import maya.cmds as cmd

import api.log
import api.assembly
import api.scene
import api.asset
import api.savedAsset
import api.widgets.rbw_UI as rbwUI
import api.database

importlib.reload(api.log)
# importlib.reload(api.assembly)
importlib.reload(api.scene)
# importlib.reload(api.asset)
# importlib.reload(api.savedAsset)
# importlib.reload(rbwUI)
importlib.reload(api.database)


referenceInfoes = {
    'cache base': 'modHigId',
    'cache proxy': 'modPrxId',
    'animText': 'surfTxtId',
    'surfacing': 'surfacingId',
    'vrscene': 'surfacingId',
    'vrmesh': 'surfacingId'
}


################################################################################
# @brief      switch to representation.
#
# @param      representation  The representation
#
def switchTo(representation='cache base'):
    api.log.logger().debug('Switching all assemblies to {}...'.format(representation))

    assemblySelectedList = cmd.ls(selection=True)

    if not assemblySelectedList:
        assemblySelectedList = cmd.ls(type='assembly')

    for assemblyNode in assemblySelectedList:
        assembly = api.assembly.Assembly(node=assemblyNode)
        if representation in assembly.getRepresentationList():
            assembly.switchRepresentation(representation)
            api.log.logger().debug('Switched {} into {}'.format(assemblyNode, representation))

        else:
            api.log.logger().warning('Cannot switch {} into {} because this representation does not exists'.format(assemblyNode, representation))

    api.log.logger().debug('Switching procedure completed!')


def recursiveSwitchAssemblies(representation, rootNode='SET'):
    children = cmd.listRelatives(rootNode, fullPath=True)
    if children:
        for node in children:
            if cmd.getAttr('{}.visibility'.format(node)):
                if cmd.nodeType(node) == 'transform':
                    recursiveSwitchAssemblies(representation, node)
                elif cmd.nodeType(node) == 'assemblyReference':
                    cmd.select(node, replace=True)
                    switchTo(representation)


################################################################################
# @brief      smart assembly switch to representation.
#
# @param      representation  The representation
#
def switchAssembliesTo(representation='vrscene'):
    selection = cmd.ls(selection=True)

    if len(selection) == 0:
        selection = ['SET']

    cmd.select(clear=True)

    for selectObj in selection:
        if cmd.nodeType(selectObj) == 'assemblyReference':
            cmd.select(selectObj, replace=True)
            switchTo(representation)
        else:
            recursiveSwitchAssemblies(representation, selectObj)


################################################################################
# @brief      Gets the representation in reference.
#
# @param      representationName  The representation name
# @param      deleteSource        The delete source
# @param      parentNode          The parent node
# @param      reference           The reference
# @param      approvation         The approvation
#
# @return     The asset in reference.
#
def getRepresentationInScene(representationName=None, onlyImport=False, deleteSource=False, world=False, reference=True, approvation='def'):
    if not representationName:
        api.log.logger().debug('Get reference from assembly...')
    else:
        api.log.logger().debug('Get {} in reference from assembly...'.format(representationName))

    selectionList = cmd.ls(selection=True)

    if not selectionList:
        selectionList = cmd.ls(type='assembly')

    api.log.logger().debug('selectionList: {}'.format(selectionList))

    newItems = []
    for item in selectionList:
        if world:
            translation = cmd.xform(item, query=True, translation=True, worldSpace=True)
            rotation = cmd.xform(item, query=True, rotation=True, worldSpace=True)
            scale = cmd.xform(item, query=True, scale=True, worldSpace=True)
        else:
            translation = cmd.xform(item, query=True, translation=True)
            rotation = cmd.xform(item, query=True, rotation=True)
            scale = cmd.xform(item, query=True, scale=True)

        assembly = api.assembly.Assembly(node=item)

        if not representationName:
            currentRepresentationName = assembly.getActiveRepresentationName() if assembly.getActiveRepresentationName() != 'cache smooth' else 'cache base'
            savedAssetId = getattr(assembly, referenceInfoes[currentRepresentationName])
        else:
            savedAssetId = getattr(assembly, referenceInfoes[representationName])

        savedAssetTemp = api.savedAsset.SavedAsset(params={'db_id': savedAssetId, 'approvation': 'def'})

        if approvation == 'def':
            savedAsset = savedAssetTemp
        else:
            savedAsset = getWipSavedAssetFromDef(savedAssetTemp)

        assemblyNameSpaceList = cmd.assembly(item, repNamespace=True, query=True).split('_')
        assemblyNameSpaceList[-1] = ''
        newNamespace = '_'.join(assemblyNameSpaceList)
        if '__' in newNamespace:
            newNamespace = newNamespace.replace('__', '_')

        latestRelSavedAssetPath = savedAsset.getLatest()[2]
        latestSavedAssetPath = os.path.join(os.getenv('MC_FOLDER'), latestRelSavedAssetPath).replace('\\', '/')

        if representationName == 'vrscene':
            latestSavedAssetPath = latestSavedAssetPath.replace('.mb', '_vrscene.ma')

        if representationName == 'vrmesh':
            latestSavedAssetPath = latestSavedAssetPath.replace('.mb', '_vrmesh.ma')

            # check multiUV and get the vrscene instead
            latestInfoSavedAssetPath = latestSavedAssetPath.replace('_vrmesh.ma', '_info.json')
            if os.path.exists(latestInfoSavedAssetPath):
                if api.json.json_read(latestInfoSavedAssetPath)['multiUV']:
                    latestSavedAssetPath = latestSavedAssetPath.replace('_vrmesh.ma', '.mb')

        if not os.path.exists(latestSavedAssetPath):
            continue

        if reference:
            newItem = api.scene.addToPropGroup(latestSavedAssetPath, newNamespace)
        else:
            newItem = api.scene.mergeAsset(latestSavedAssetPath)

        cmd.xform(newItem, translation=translation)
        cmd.xform(newItem, rotation=rotation)
        cmd.xform(newItem, scale=scale)

        if not onlyImport:
            if cmd.objExists('SET_EDIT'):
                pass
            else:
                cmd.group(empty=True, n='SET_EDIT')
            cmd.parent(item, 'SET_EDIT')
            cmd.setAttr('{}.visibility'.format(item), 0)

            if deleteSource:
                cmd.delete(item)
                if not cmd.sets('SET_EDIT', q=True):
                    cmd.delete('SET_EDIT')

        newItems.append(newItem)

    return newItems


################################################################################
# @brief      Gets the saved asset from assembly.
#
# @param      dept          The dept
# @param      deptType      The dept type
# @param      deleteSource  The delete source
# @param      parentNode    The parent node
# @param      reference     The reference
# @param      approvation   The approvation
#
# @return     The saved asset from assembly.
#
def getSavedAssetFromAssembly(dept='rigging', deptType='hig', deleteSource=False, world=False, reference=True, approvation='def'):
    selectionList = cmd.ls(selection=True)

    if not selectionList:
        selectionList = cmd.ls(type='assembly')

    api.log.logger().debug('selectionList: {}'.format(selectionList))

    newItems = []
    for item in selectionList:
        translation = cmd.xform(item, query=True, translation=True)
        rotation = cmd.xform(item, query=True, rotation=True)
        scale = cmd.xform(item, query=True, scale=True)

        assembly = api.assembly.Assembly(node=item)

        savedAsset = api.savedAsset.SavedAsset(params={'asset': assembly.getAsset(), 'dept': dept, 'deptType': deptType, 'approvation': approvation})

        assemblyNameSpaceList = cmd.assembly(item, repNamespace=True, query=True).split('_')
        assemblyNameSpaceList[-1] = ''
        newNamespace = '_'.join(assemblyNameSpaceList)
        if '__' in newNamespace:
            newNamespace = newNamespace.replace('__', '_')

        latestRelSavedAssetPath = savedAsset.getLatest()[2]
        latestSavedAssetPath = os.path.join(os.getenv('MC_FOLDER'), latestRelSavedAssetPath).replace('\\', '/')

        if not os.path.exists(latestSavedAssetPath):
            continue

        if reference:
            newItem = api.scene.addToPropGroup(latestSavedAssetPath, newNamespace)
        else:
            newItem = api.scene.mergeAsset(latestSavedAssetPath)

        cmd.xform(newItem, translation=translation)
        cmd.xform(newItem, rotation=rotation)
        cmd.xform(newItem, scale=scale)

        if cmd.objExists('SET_EDIT'):
            pass
        else:
            cmd.group(empty=True, n='SET_EDIT')
        cmd.parent(item, 'SET_EDIT')
        cmd.setAttr('{}.visibility'.format(item), 0)

        if deleteSource:
            cmd.delete(item)
            if not cmd.sets('SET_EDIT', q=True):
                cmd.delete('SET_EDIT')

        newItems.append(newItem)

    return newItems


################################################################################
# @brief      Gets the assembly from reference.
#
# @return     The assembly from reference.
#
def getAssemblyFromReference(deleteSource=False, parentAssembly=False):
    selectionList = cmd.ls(selection=True)
    api.log.logger().debug(selectionList)

    if selectionList:
        for item in selectionList:
            parent = cmd.listRelatives(item, parent=True, fullPath=True)
            saveNode = cmd.listConnections(item, type='saveNode')[0]
            if cmd.referenceQuery(saveNode, inr=True):

                translation = cmd.xform(item, query=True, translation=True)
                rotation = cmd.xform(item, query=True, rotation=True)
                scale = cmd.xform(item, query=True, scale=True)

                assetId = cmd.getAttr('{}.asset_id'.format(saveNode))
                asset = api.asset.Asset(params={'id': assetId})

                assembly = api.assembly.Assembly(params={'asset': asset})

                try:
                    assemblyFile = assembly.getLatest()[1]

                    api.scene.addToSetEditGroup(assemblyFile)
                    newItem = cmd.ls(selection=True)[0]

                    cmd.xform(newItem, translation=translation)
                    cmd.xform(newItem, rotation=rotation)
                    cmd.xform(newItem, scale=scale)

                    if deleteSource:
                        referenceNode = cmd.referenceQuery(saveNode, rfn=True)
                        api.scene.deleteReference(referenceNode)

                    if parentAssembly and parent:
                        cmd.parent(newItem, parent[0])

                except TypeError:
                    rbwUI.RBWWarning(title='No Assembly for this Asset', text='Sorry but {} has not an assembly'.format(item))
    else:
        rbwUI.RBWError(title='ATTENTION', text='Select some reference and try again.')


################################################################################
# @brief      Gets the wip saved asset from definition version.
#
# @param      savedAsset  The saved asset
#
# @return     The wip saved asset from definition.
#
def getWipSavedAssetFromDef(savedAsset):
    savedAssetWipParams = {
        'asset': savedAsset.asset,
        'approvation': 'wip',
        'dept': savedAsset.dept,
        'deptType': savedAsset.deptType
    }

    wipSavedAsset = api.savedAsset.SavedAsset(params=savedAssetWipParams)
    latestWipId = wipSavedAsset.getLatest()[0]

    savedAssetWip = api.savedAsset.SavedAsset(params={'db_id': latestWipId, 'approvation': 'wip'})

    return savedAssetWip
