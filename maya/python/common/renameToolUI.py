import importlib
from PySide2 import QtCore

import maya.cmds as cmd

import api.log
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.log)
# importlib.reload(rbwUI)


################################################################################
# @brief      This class describes a rename tool ui.
#
class RenameToolUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(RenameToolUI, self).__init__()

        if cmd.window('RenameToolUI', exists=True):
            cmd.deleteUI('RenameToolUI')

        self.setObjectName('RenameToolUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topFrame = rbwUI.RBWFrame(layout='V', spacing=5)
        self.mainLayout.addWidget(self.topFrame)

        # ----------------------- search and replace ------------------------- #
        self.searchAndReplaceGroup = rbwUI.RBWGroupBox(title='Search and Replace', layout='V', fontSize=12)

        self.searchLine = rbwUI.RBWLineEdit(title='Search', stretch=False)
        self.replaceLine = rbwUI.RBWLineEdit(title='Replace', stretch=False)
        self.searchAndReplaceButton = rbwUI.RBWButton(text='Search and Replace', size=[200, 25])
        self.searchAndReplaceButton.clicked.connect(self.searchAndReplace)

        self.searchAndReplaceGroup.addWidget(self.searchLine)
        self.searchAndReplaceGroup.addWidget(self.replaceLine)
        self.searchAndReplaceGroup.addWidget(self.searchAndReplaceButton, alignment=QtCore.Qt.AlignHCenter)

        self.topFrame.addWidget(self.searchAndReplaceGroup)

        # --------------------------- add prefix ----------------------------- #
        self.addPrefixGroup = rbwUI.RBWGroupBox(title='Add Prefix', layout='V', fontSize=12)

        self.prefixLine = rbwUI.RBWLineEdit(title='Prefix', stretch=False)
        self.addPrefixButton = rbwUI.RBWButton(text='Add Prefix', size=[200, 25])
        self.addPrefixButton.clicked.connect(self.addPrefix)

        self.addPrefixGroup.addWidget(self.prefixLine)
        self.addPrefixGroup.addWidget(self.addPrefixButton, alignment=QtCore.Qt.AlignHCenter)

        self.topFrame.addWidget(self.addPrefixGroup)

        # --------------------------- add suffix ----------------------------- #
        self.addSuffixGroup = rbwUI.RBWGroupBox(title='Add Suffix', layout='V', fontSize=12)

        self.suffixLine = rbwUI.RBWLineEdit(title='Suffix', stretch=False)
        self.addSuffixButton = rbwUI.RBWButton(text='Add Suffix', size=[200, 25])
        self.addSuffixButton.clicked.connect(self.addSuffix)

        self.addSuffixGroup.addWidget(self.suffixLine)
        self.addSuffixGroup.addWidget(self.addSuffixButton, alignment=QtCore.Qt.AlignHCenter)

        self.topFrame.addWidget(self.addSuffixGroup)

        # ------------------------ rename and number ------------------------- #
        self.renameAndNumberGroup = rbwUI.RBWGroupBox(title='Rename and Number', layout='V', fontSize=12)

        self.renameLine = rbwUI.RBWLineEdit(title='Rename', stretch=False)
        self.startLine = rbwUI.RBWLineEdit(title='Start', stretch=False)
        self.paddingLine = rbwUI.RBWLineEdit(title='Padding', stretch=False)
        self.renameAndNumberButton = rbwUI.RBWButton(text='Rename and Number', size=[200, 25])
        self.renameAndNumberButton.clicked.connect(self.renameAndNumber)

        self.renameAndNumberGroup.addWidget(self.renameLine)
        self.renameAndNumberGroup.addWidget(self.startLine)
        self.renameAndNumberGroup.addWidget(self.paddingLine)
        self.renameAndNumberGroup.addWidget(self.renameAndNumberButton, alignment=QtCore.Qt.AlignHCenter)

        self.topFrame.addWidget(self.renameAndNumberGroup)

        self.setMinimumWidth(400)
        self.setFixedHeight(468)
        self.setTitle('Rename Tool')
        self.setIcon('rename.png')
        self.setFocus()

    ############################################################################
    # @brief      search and replace.
    #
    def searchAndReplace(self):
        searchWord = self.searchLine.text()
        replaceWord = self.replaceLine.text()

        if searchWord and replaceWord:
            selectedObjects = cmd.ls(selection=True, long=True)
            for selectedObject in selectedObjects:
                objectName = selectedObject.split('|')[-1]
                if searchWord in objectName:
                    newName = objectName.replace(searchWord, replaceWord)
                    cmd.rename(selectedObject, newName)
        else:
            rbwUI.RBWError(text='\'Search\' or \'Replace\' or both lines are empty')

    ############################################################################
    # @brief      Adds a prefix.
    #
    def addPrefix(self):
        prefixWord = self.prefixLine.text()

        if prefixWord:
            selectedObjects = cmd.ls(selection=True, long=True)
            for selectedObject in selectedObjects:
                objectName = selectedObject.split('|')[-1]
                newObjectName = '{}{}'.format(prefixWord, objectName)
                cmd.rename(selectedObject, newObjectName)
        else:
            rbwUI.RBWError(text='The prefix line is empty')

    ############################################################################
    # @brief      Adds a suffix.
    #
    def addSuffix(self):
        suffixWord = self.suffixLine.text()

        if suffixWord:
            selectedObjects = cmd.ls(selection=True, long=True)
            for selectedObject in selectedObjects:
                objectName = selectedObject.split('|')[-1]
                newObjectName = '{}{}'.format(objectName, suffixWord)
                cmd.rename(selectedObject, newObjectName)
        else:
            rbwUI.RBWError(text='The suffix line is empty')

    ############################################################################
    # @brief      rename and numberthe selected object
    #
    def renameAndNumber(self):
        try:
            renameWord = self.renameLine.text()
            startNumber = int(self.startLine.text())
            paddingNumber = int(self.paddingLine.text())

            if renameWord and startNumber and paddingNumber:
                selectedObjects = cmd.ls(selection=True, long=True)
                for i in range(0, len(selectedObjects)):
                    newName = '{}{}'.format(renameWord, str(i + startNumber).zfill(paddingNumber))
                    cmd.rename(selectedObjects[i], newName)
            else:
                rbwUI.RBWError(text='\'Rename\' or \'Start\' or \'Padding\' or all lines are empty')
        except ValueError:
            rbwUI.RBWError(text='\'Start\' or \'Padding\' are not number')
