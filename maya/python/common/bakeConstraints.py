import maya.cmds as cmd


def bakeConstraints():
    parentC = [c for c in cmd.ls(type="parentConstraint") if ':' not in c]
    pointC = [c for c in cmd.ls(type="pointConstraint") if ':' not in c]
    orientC = [c for c in cmd.ls(type="orientConstraint") if ':' not in c]
    scaleC = [c for c in cmd.ls(type="scaleConstraint") if ':' not in c]
    aimC = [c for c in cmd.ls(type="aimConstraint") if ':' not in c]
    # geoC = [c for c in cmd.ls(type="geometryConstraint") if ':' not in c]
    geoC = []
    normalC = [c for c in cmd.ls(type="normalConstraint") if ':' not in c]
    tangentC = [c for c in cmd.ls(type="tangentConstraint") if ':' not in c]

    allC = parentC + pointC + orientC + scaleC + aimC + geoC + normalC + tangentC
    allParents = []

    startFrame = int(cmd.playbackOptions(query=True, ast=True))
    endFrame = int(cmd.playbackOptions(query=True, aet=True))

    timeTuple = (startFrame, endFrame)

    for c in allC:
        allParents.append(cmd.listRelatives(c, fullPath=True, parent=True)[0])

    for p in allParents:
        cmd.bakeResults(p, t=timeTuple)
        cmd.delete(p, cn=True)
