import importlib
from PySide2 import QtGui, QtCore, QtWidgets

from maya.app.general.mayaMixin import MayaQWidgetDockableMixin
import maya.cmds as cmd

import api.scene
importlib.reload(api.scene)


################################################################################
# @brief      This class describes a puglin check ui.
#
class PluginCheckUI(MayaQWidgetDockableMixin, QtWidgets.QMainWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(PluginCheckUI, self).__init__()

        if cmd.window('PluginCheckUI', exists=True):
            cmd.deleteUI('PluginCheckUI')

        self.listPlugins = api.scene.listUnknowPlugin()
        self.listCheckboxPlugins = []

        self.setObjectName('PluginCheckUI')
        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.centralWidget = QtWidgets.QWidget()
        self.topBox = QtWidgets.QVBoxLayout()

        self.checkBoxGroup = QtWidgets.QGroupBox('Unknown Plugins')
        self.checkBoxLayout = QtWidgets.QVBoxLayout()

        for plugin in self.listPlugins:
            checkbox = QtWidgets.QCheckBox(plugin)
            self.listCheckboxPlugins.append(checkbox)
            self.checkBoxLayout.addWidget(checkbox)
        self.checkBoxGroup.setLayout(self.checkBoxLayout)

        self.topBox.addWidget(self.checkBoxGroup)

        self.buttonLayout = QtWidgets.QHBoxLayout()
        self.removePluginButton = QtWidgets.QPushButton('Remove Selected')
        self.removePluginButton.clicked.connect(self.removeSelectedPlugin)
        self.buttonLayout.addWidget(self.removePluginButton)

        self.removeAllPluginPluginButton = QtWidgets.QPushButton('Remove All')
        self.removeAllPluginPluginButton.clicked.connect(self.removeAllPlugin)
        self.buttonLayout.addWidget(self.removeAllPluginPluginButton)

        self.topBox.addWidget(self.buttonLayout)

        self.centralWidget.setLayout(self.topBox)
        self.setCentralWidget(self.centralWidget)

        self.centralWidget.setMinimumSize(200, 400)

        self.setWindowTitle('Plugin Check UI')
        self.setFocus()

    ############################################################################
    # @brief      Removes a selected plugin.
    #
    def removeSelectedPlugin(self):
        checkBoxes = [x for x in self.checkBoxLayout.children() if isinstance(x, QtWidgets.QCheckBox)]
        for checkBox in checkBoxes:
            if checkBox.isChecked():
                try:
                    cmd.unknownPlugin(checkBox.text(), remove=True)
                    checkBox.setCheckable(False)
                    checkBox.setDisabled(True)
                except:
                    QtWidgets.QMessageBox.critical(None, "Unknown Plugin Error", "Impossible to remove {}.".format(checkBox.text()))

    ############################################################################
    # @brief      Removes all plugin.
    #
    def removeAllPlugin(self):
        listNotRemoved = []
        checkBoxes = [x for x in self.checkBoxLayout.children() if isinstance(x, QtWidgets.QCheckBox)]
        for checkBox in checkBoxes:
            try:
                cmd.unknownPlugin(checkBox.text(), remove=True)
                checkBox.setDisabled(True)
                checkBox.setCheckable(False)
            except:
                listNotRemoved.append(checkBox.text())

        # Visualizzazione dei messaggi di fallimento o successo
        if len(listNotRemoved):
            listJoinedNotRemoved = ", ".join(listNotRemoved)
            QtWidgets.QMessageBox.critical(None, "Unknown Plugin Error", "Impossible to remove {}".format(listJoinedNotRemoved))
        else:
            QtWidgets.QMessageBox.information(None, "Unknown Plugin", "All Unknown plugins successfuly removed")
