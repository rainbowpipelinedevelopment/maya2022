import importlib
import os
import functools
import xml.etree.cElementTree as ET
from PySide2 import QtGui, QtWidgets, QtCore

import maya.cmds as cmd

import api.log
import api.json
import config.dictionaries
import api.asset
import api.savedAsset
import api.scene
import api.database
import api.widgets.rbw_UI as rbwUI
import api.texture

importlib.reload(api.log)
importlib.reload(api.json)
importlib.reload(config.dictionaries)
# importlib.reload(api.asset)
# importlib.reload(api.savedAsset)
importlib.reload(api.scene)
importlib.reload(api.database)
# importlib.reload(rbwUI)
importlib.reload(api.texture)

settingsPath = api.json.json_read(os.path.join(os.getenv('LOCALPIPE'), 'json_file', 'settings.json'))['settingsPath'].replace('USERNAME', os.getenv('USERNAME'))


################################################################################
# @brief      This class describes an asset ui.
#
class AssetUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs the object
    #
    # @param      self  The object
    #
    def __init__(self, deptName='Dept'):
        super().__init__()

        if cmd.window('AssetUI', exists=True):
            cmd.deleteUI('AssetUI')

        self.deptName = deptName
        self.settings = api.json.json_read(settingsPath)

        self.mcFolder = os.getenv('MC_FOLDER')
        api.log.logger().debug(self.mcFolder)
        self.id_mv = os.getenv('ID_MV')
        api.log.logger().debug(self.id_mv)

        self.assets = api.system.getAssetsFromDB()

        self.setObjectName('AssetUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.horizontalSplitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)
        self.horizontalSplitter.setObjectName('splitterH')
        self.horizontalSplitter.setStyleSheet('QSplitter#splitterH{background-color: transparent; border-radius: 6px;}')
        self.mainLayout.addWidget(self.horizontalSplitter)

        ########################################################################
        # LEFT PANEL
        #

        # vertical splitter
        self.verticalSplitter = QtWidgets.QSplitter(QtCore.Qt.Vertical)
        self.verticalSplitter.setObjectName('splitterV')
        self.verticalSplitter.setStyleSheet('QSplitter#splitterV{background-color: transparent; border-radius: 6px;}')

        # --------------------------- TOP WIDGET -------------------------- #
        self.topWidget = rbwUI.RBWFrame(layout='V', radius=5, spacing=5)

        # ------------------------- search widget ------------------------- #
        self.searchGroup = rbwUI.RBWGroupBox(title='Search asset name or a part of this:', layout='V', fontSize=12)

        self.searchWidget = rbwUI.RBWFrame(layout='H', margins=[0, 0, 0, 0], bgColor='transparent')

        self.searchLineEdit = rbwUI.RBWLineEdit(title='Type asset name:', bgColor='transparent', stretch=False)
        self.searchLineEdit.returnPressed.connect(self.searchEntry)
        self.searchButton = rbwUI.RBWButton(icon=['search.png'])
        self.searchButton.clicked.connect(self.searchEntry)

        self.searchWidget.addWidget(self.searchLineEdit)
        self.searchWidget.addWidget(self.searchButton)

        self.searchGroup.addWidget(self.searchWidget)

        self.searchResultList = QtWidgets.QListWidget()
        self.searchResultList.hide()
        self.searchResultList.itemClicked.connect(self.matchAsset)

        self.searchGroup.addWidget(self.searchResultList)

        self.topWidget.addWidget(self.searchGroup, alignment=QtCore.Qt.AlignTop)

        # --------------------------- info widget ------------------------- #
        self.infoLayout = QtWidgets.QGridLayout()

        # asset info
        self.assetInfoGroup = rbwUI.RBWGroupBox(title='Asset Info:', layout='V', fontSize=12)

        self.assetCategoryComboBox = rbwUI.RBWComboBox('Category', bgColor='transparent', size=200)
        self.assetGroupComboBox = rbwUI.RBWComboBox('Group', bgColor='transparent', size=200)
        self.assetNameComboBox = rbwUI.RBWComboBox('Name', bgColor='transparent', size=200)
        self.assetVariantComboBox = rbwUI.RBWComboBox('Variant', bgColor='transparent', size=200)

        self.assetCategoryComboBox.activated.connect(self.fillAssetGroupComboBox)
        self.assetGroupComboBox.activated.connect(self.fillAssetNameComboBox)
        self.assetNameComboBox.activated.connect(self.fillAssetVariantComboBox)
        self.assetVariantComboBox.activated.connect(self.setCurrentAsset)

        self.assetInfoGroup.addWidget(self.assetCategoryComboBox)
        self.assetInfoGroup.addWidget(self.assetGroupComboBox)
        self.assetInfoGroup.addWidget(self.assetNameComboBox)
        self.assetInfoGroup.addWidget(self.assetVariantComboBox)

        self.infoLayout.addWidget(self.assetInfoGroup, 0, 0)

        # dept asset info
        self.deptInfoGroup = rbwUI.RBWGroupBox(title='Dept Info:', layout='V', fontSize=12)

        self.deptTypeComboBox = rbwUI.RBWComboBox('Type', bgColor='transparent', size=150)
        self.deptApprovationComboBox = rbwUI.RBWComboBox('Approvation', bgColor='transparent', size=150)
        self.deptApprovationComboBox.activated.connect(self.fillSavedAssetsTree)
        self.wipWarningToggle = rbwUI.RBWCheckBox('Toggle Wip warning', 'Turn ON/OFF the dialog pop up that warn you about opening a Wip instead of a Def file', self.settings['common']['wipWarning'])
        self.wipWarningToggle.stateChanged.connect(self.toggleWipWarning)

        self.deptInfoGroup.addWidget(self.deptTypeComboBox)
        self.deptInfoGroup.addWidget(self.deptApprovationComboBox)
        self.deptInfoGroup.addWidget(self.wipWarningToggle)

        self.infoLayout.addWidget(self.deptInfoGroup, 1, 0)

        # saved asset info
        self.saveInfoGroup = rbwUI.RBWGroupBox(title='Save Info:', layout='F', fontSize=12)
        self.saveInfoGroup.layout.setRowWrapPolicy(QtWidgets.QFormLayout.DontWrapRows)
        self.saveInfoGroup.layout.setLabelAlignment(QtCore.Qt.AlignLeft)

        self.saveNotesLabel = rbwUI.RBWLabel(text='Notes', italic=False, size=12)
        self.saveNotesTextEdit = QtWidgets.QTextEdit()
        self.saveNotesTextEdit.setObjectName('saveNotesTextEdit')
        self.saveExportSelectionLabel = rbwUI.RBWLabel(text='Export Selection', italic=False, size=12)
        self.saveExportSelectionSwitch = rbwUI.RBWSwitch()

        self.saveInfoGroup.addRow(self.saveNotesLabel, self.saveNotesTextEdit)
        self.saveInfoGroup.addRow(self.saveExportSelectionLabel, self.saveExportSelectionSwitch)

        self.infoLayout.addWidget(self.saveInfoGroup, 0, 1, 2, 1)

        self.topWidget.addLayout(self.infoLayout)

        self.verticalSplitter.addWidget(self.topWidget)

        # ------------------------- BOTTOM WIDGET ------------------------- #
        self.bottomWidget = rbwUI.RBWFrame(layout='V', radius=5, spacing=5)

        # ---------------------------  results ---------------------------- #
        self.savedAssetsTree = rbwUI.RBWTreeWidget(fields=['Version', 'User', 'Date', 'Note', 'Path'])
        self.connect(self.savedAssetsTree, QtCore.SIGNAL("clicked(QModelIndex)"), self.updatePreview)

        self.bottomWidget.addWidget(self.savedAssetsTree)

        # ---------------------------- buttons ---------------------------- #
        self.buttonsWidget = rbwUI.RBWFrame(layout='H', margins=[0, 0, 0, 0], bgColor='transparent')
        self.loadButton = rbwUI.RBWButton(icon=['load.png'], size=[200, 35], text='Load')
        self.mergeButton = rbwUI.RBWButton(icon=['merge.png'], size=[200, 35], text='Merge')
        self.ReferenceButton = rbwUI.RBWButton(icon=['reference.png'], size=[200, 35], text='Merge As Reference')
        self.saveButton = rbwUI.RBWButton(icon=['save.png'], size=[200, 35], text='Save')

        self.loadButton.clicked.connect(functools.partial(self.load, 'load'))
        self.mergeButton.clicked.connect(functools.partial(self.load, 'merge'))
        self.ReferenceButton.clicked.connect(functools.partial(self.load, 'reference'))
        self.saveButton.clicked.connect(self.save)

        self.buttonsWidget.addWidget(self.loadButton)
        self.buttonsWidget.addWidget(self.mergeButton)
        self.buttonsWidget.addWidget(self.ReferenceButton)
        self.buttonsWidget.addWidget(self.saveButton)

        self.bottomWidget.addWidget(self.buttonsWidget, alignment=QtCore.Qt.AlignBottom)

        self.verticalSplitter.addWidget(self.bottomWidget)

        self.horizontalSplitter.addWidget(self.verticalSplitter)

        ########################################################################
        # RIGHT AREA
        #
        self.rightWidget = rbwUI.RBWFrame(layout='V', radius=5)

        # preview panel
        self.imageListWidget = QtWidgets.QListWidget()
        self.imageListWidget.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.imageListWidget.setViewMode(QtWidgets.QListView.IconMode)
        self.imageListWidget.setResizeMode(QtWidgets.QListView.Adjust)
        self.imageListWidget.setMovement(QtWidgets.QListView.Static)
        self.imageListWidget.setIconSize(QtCore.QSize(150, 150))
        self.imageListWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.imageListWidget.itemClicked.connect(self.previewImage)

        self.rightWidget.addWidget(self.imageListWidget)

        self.horizontalSplitter.addWidget(self.rightWidget)

        self.autoFillCombo()

        self.verticalSplitter.setStretchFactor(0, 1)
        self.verticalSplitter.setStretchFactor(1, 2)
        self.horizontalSplitter.setStretchFactor(0, 0)

        self.setMinimumSize(1010, 700)

        self.setTitle('Load & Save {} UI'.format(self.deptName))
        self.setIcon('../arakno/loadAndSave{}.png'.format(self.deptName.capitalize()))
        self.addStatusBar()
        self.setFocus()

    ############################################################################
    # @brief      Toggles the wipWarning settings for the user.
    #
    def toggleWipWarning(self, value):
        self.settings['common']['wipWarning'] = True if value else False
        api.json.json_write(self.settings, settingsPath)

    ############################################################################
    # @brief      fill the asset category combo box.
    #
    def fillAssetCategoryComboBox(self):
        self.assetCategoryComboBox.clear()
        categories = sorted(self.assets.keys())

        self.assetCategoryComboBox.addItems(categories)

        if len(categories) == 1:
            self.assetCategoryComboBox.setCurrentIndex(0)
            self.fillAssetGroupComboBox()

    ############################################################################
    # @brief      fill the group combo box.
    #
    def fillAssetGroupComboBox(self):
        self.currentCategory = self.assetCategoryComboBox.currentText()

        self.assetGroupComboBox.clear()
        groups = sorted(self.assets[self.currentCategory].keys())

        self.assetGroupComboBox.addItems(groups)

        if len(groups) == 1:
            self.assetGroupComboBox.setCurrentIndex(0)
            self.fillAssetNameComboBox()

    ############################################################################
    # @brief      fill th name combo box.
    #
    def fillAssetNameComboBox(self):
        self.currentGroup = self.assetGroupComboBox.currentText()

        self.assetNameComboBox.clear()
        names = sorted(self.assets[self.currentCategory][self.currentGroup].keys())

        self.assetNameComboBox.addItems(names)

        if len(names) == 1:
            self.assetNameComboBox.setCurrentIndex(0)
            self.fillAssetVariantComboBox()

    ############################################################################
    # @brief      fill the variant combo box.
    #
    def fillAssetVariantComboBox(self):
        self.currentName = self.assetNameComboBox.currentText()

        self.assetVariantComboBox.clear()
        variants = sorted(self.assets[self.currentCategory][self.currentGroup][self.currentName].keys())

        self.assetVariantComboBox.addItems(variants)

        if len(variants) == 1:
            self.assetVariantComboBox.setCurrentIndex(0)
            self.setCurrentAsset()

    def fillSubTypeComboBox(self):
        self.deptTypeComboBox.clear()

        try:
            subTYpes = [''] + config.dictionaries.mc_depts[self.deptName]
        except KeyError:
            subTYpes = ['']
        self.deptTypeComboBox.addItems(subTYpes)

    ############################################################################
    # @brief      fill the saved asset approvation combo box.
    #
    def fillApprovationComboBox(self):
        self.deptApprovationComboBox.clear()

        self.deptApprovationComboBox.addItems(['', 'wip', 'def'])

    ############################################################################
    # @brief      auto fill of all the combos.
    #
    def autoFillCombo(self):
        deptSaveNodes = cmd.ls('{}_saveNode'.format(self.deptName.lower()))
        if deptSaveNodes:
            try:
                saveNode = deptSaveNodes[0]

                self.fillAssetCategoryComboBox()
                categoryIndex = self.assetCategoryComboBox.findText(cmd.getAttr('{}.genre'.format(saveNode)), QtCore.Qt.MatchFixedString)
                self.assetCategoryComboBox.setCurrentIndex(categoryIndex)

                self.fillAssetGroupComboBox()
                groupIndex = self.assetGroupComboBox.findText(cmd.getAttr('{}.group'.format(saveNode)), QtCore.Qt.MatchFixedString)
                self.assetGroupComboBox.setCurrentIndex(groupIndex)

                self.fillAssetNameComboBox()
                nameIndex = self.assetNameComboBox.findText(cmd.getAttr('{}.name'.format(saveNode)), QtCore.Qt.MatchFixedString)
                self.assetNameComboBox.setCurrentIndex(nameIndex)

                self.fillAssetVariantComboBox()
                variantIndex = self.assetVariantComboBox.findText(cmd.getAttr('{}.variant'.format(saveNode)), QtCore.Qt.MatchFixedString)
                self.assetVariantComboBox.setCurrentIndex(variantIndex)
                self.setCurrentAsset()
            except:
                self.fillAssetCategoryComboBox()
        else:
            self.fillAssetCategoryComboBox()

        self.fillSubTypeComboBox()
        self.fillApprovationComboBox()

    ############################################################################
    # @brief      search the give asset.
    #
    def searchEntry(self):
        wantedEntryName = self.searchLineEdit.text()
        if wantedEntryName == '':
            self.updateResultList()
        else:
            searchAssetFromNameQuery = "SELECT DISTINCT `category`, `group`, `name` FROM `V_assetList` WHERE `projectId`={} AND `name` LIKE '%{}%'".format(
                self.id_mv,
                wantedEntryName
            )

            results = api.database.selectQuery(searchAssetFromNameQuery)
            if results:
                self.updateResultList(results)

    ############################################################################
    # @brief      update the list result.
    #
    # @param      queryResults  The query results
    #
    def updateResultList(self, queryResults=None):
        self.searchResultList.clear()
        if queryResults:
            self.searchResultList.show()
            for infoTupla in queryResults:
                item = QtWidgets.QListWidgetItem('{} - {} - {}'.format(infoTupla[0], infoTupla[1], infoTupla[2]))
                self.searchResultList.addItem(item)
        else:
            self.searchResultList.hide()

    ############################################################################
    # @brief      update the combo to match the selected asset.
    #
    def matchAsset(self):
        wantedCategory, wantedGroup, wantedName = self.searchResultList.currentItem().text().split(' - ')

        self.fillAssetCategoryComboBox()
        categoryIndex = self.assetCategoryComboBox.findText(wantedCategory, QtCore.Qt.MatchFixedString)
        self.assetCategoryComboBox.setCurrentIndex(categoryIndex)

        self.fillAssetGroupComboBox()
        groupIndex = self.assetGroupComboBox.findText(wantedGroup, QtCore.Qt.MatchFixedString)
        self.assetGroupComboBox.setCurrentIndex(groupIndex)

        self.fillAssetNameComboBox()
        nameIndex = self.assetNameComboBox.findText(wantedName, QtCore.Qt.MatchFixedString)
        self.assetNameComboBox.setCurrentIndex(nameIndex)

        self.fillAssetVariantComboBox()

        self.fillSubTypeComboBox()
        self.fillApprovationComboBox()
        self.savedAssetsTree.clear()

    ############################################################################
    # @brief      Sets the current asset.
    #
    def setCurrentAsset(self):
        self.currentVariant = self.assetVariantComboBox.currentText()

        self.currentAsset = api.asset.Asset({
            'category': self.currentCategory,
            'group': self.currentGroup,
            'name': self.currentName,
            'variant': self.currentVariant
        })

        self.fillSubTypeComboBox()
        self.fillApprovationComboBox()
        self.savedAssetsTree.clear()

    ############################################################################
    # @brief      Gets the saved assets.
    #
    # @return     The saved assets.
    #
    def getSavedAssets(self):
        # 'Version', 'User', 'Date', 'Note', 'Path'
        versionsQuery = "SELECT `version`, `user`, `date`, `note`, `path`, `parent` FROM `savedAsset` WHERE `assetId` = '{}' AND `dept` = '{}' AND `deptType` = '{}' AND `visible`= 1 ORDER BY `date` DESC".format(
            self.currentAsset.variantId, self.deptName.lower(), self.currentDeptType
        )
        if self.currentApprovation == 'wip':
            versionsQuery = versionsQuery.replace('`savedAsset`', '`savedAsset_wip`')

        return api.database.selectQuery(versionsQuery)

    ############################################################################
    # @brief      fill the assets tree.
    #
    def fillSavedAssetsTree(self):
        self.savedAssetsTree.clear()
        self.currentDeptType = self.deptTypeComboBox.currentText()
        self.currentApprovation = self.deptApprovationComboBox.currentText()

        saves = self.getSavedAssets()

        for save in saves:
            child = QtWidgets.QTreeWidgetItem(self.savedAssetsTree)
            style = False
            if save[5] != '':
                style = True
            for i in range(0, 5):
                child.setText(i, str(save[i]))
                if style and self.currentApprovation == 'wip':
                    child.setForeground(i, QtGui.QBrush(QtGui.QColor("yellow")))

    ############################################################################
    # @brief      update the previe in the right panel
    #
    # @param      index  The index
    #
    def updatePreview(self, index):
        self.imageListWidget.clear()

        rowIndex = index.row()
        root = self.savedAssetsTree.invisibleRootItem()
        item = root.child(rowIndex)
        relPath = item.text(4)

        fullPath = os.path.join(self.mcFolder, relPath).replace('\\', '/')

        if os.path.exists(fullPath):
            snapshotFolder = os.path.join(os.path.dirname(fullPath), "snapshots").replace('\\', '/')
            baseName = os.path.basename(fullPath).split('.')[0]
            imgList = ["persp", "side", "front", "top"]
            if os.path.exists(snapshotFolder):
                for image in imgList:
                    imagePath = os.path.join(snapshotFolder, "{}_{}.jpg".format(baseName, image)).replace('\\', '/')

                    pixmap = QtGui.QPixmap(imagePath)
                    icon = QtGui.QIcon()
                    icon.addPixmap(pixmap)

                    imageItem = QtWidgets.QListWidgetItem(icon, image)
                    self.imageListWidget.addItem(imageItem)

    ############################################################################
    # @brief      { function_description }
    #
    def previewImage(self):
        selectedItem = self.savedAssetsTree.selectedItems()[0]
        selectedPath = selectedItem.text(4)
        api.log.logger().debug('selectedPath: {}'.format(selectedPath))

        curruntRow = self.imageListWidget.currentRow()
        item = self.imageListWidget.item(curruntRow)
        selectedImage = item.text()
        api.log.logger().debug('selectedImage: {}'.format(selectedImage))

        imageName = os.path.basename(selectedPath).replace('.mb', '_{}.jpg'.format(selectedImage))
        imagePath = os.path.join(self.mcFolder, os.path.dirname(selectedPath), 'snapshots', imageName).replace('\\', '/')

        previewPanel = rbwUI.RBWPreviewPanel(self, imagePath)
        previewPanel.show()
        api.log.logger().debug(previewPanel.sizeHint())

    ############################################################################
    # @brief      load function.
    #
    # @param      mode  The mode
    #
    def load(self, mode='load'):
        # check if there are unsaved changes
        fileCheckState = cmd.file(q=True, modified=True)
        if fileCheckState:
            sceneName = api.scene.scenename()
            if sceneName:
                answer = rbwUI.RBWConfirm(title='Unsaved Scene', text='There are unsaved changes.', defOk=['Load', 'ok.png'], defCancel=['Skip', 'cancel.png']).result()
                if not answer:
                    api.log.logger().debug("Load action skipped... Please procede to save your opened scene!")
                    return
        currentSavedAssetItem = self.savedAssetsTree.currentItem()
        relativePath = currentSavedAssetItem.text(4)

        currentSavedAsset = api.savedAsset.SavedAsset(params={'path': relativePath})

        if self.currentApprovation == 'def' and mode == 'load' and self.deptName.lower() != 'grooming':
            if self.deptName.lower() == 'surfacing' and self.currentName.endswith('block'):
                currentSavedAsset.load(mode)
            else:
                parentWipId = currentSavedAsset.parent
                wipSavedAsset = api.savedAsset.SavedAsset(params={'db_id': parentWipId, 'approvation': 'wip'})
                if self.settings['common']['wipWarning']:
                    rbwUI.RBWDialog(text='You cannot open the Def file directly.\nI\'ll load the Wip {} which is autogenerated from the Def save.'.format(wipSavedAsset.version), parent=self)
                wipSavedAsset.load(mode)
        else:
            currentSavedAsset.load(mode)

    ############################################################################
    # @brief      save function.
    #
    def save(self):
        try:
            savedAssetParam = {
                'asset': self.currentAsset,
                'dept': self.deptName.lower(),
                'deptType': self.currentDeptType,
                'approvation': self.currentApprovation,
                'note': self.saveNotesTextEdit.toPlainText()
            }

            if self.saveExportSelectionSwitch.isChecked():
                savedAssetParam['selection'] = cmd.ls(selection=True)[0]
            else:
                savedAssetParam['selection'] = None

            if self.currentApprovation == 'def' and self.deptName.lower() != 'grooming':
                if not self.checkToSave():
                    return

                wipSavedAssetParam = {
                    'asset': self.currentAsset,
                    'dept': self.deptName.lower(),
                    'deptType': self.currentDeptType,
                    'approvation': 'wip',
                    'note': 'Pipeline autosave before def save',
                    'selection': savedAssetParam['selection']
                }

                wipSavedAsset = api.savedAsset.SavedAsset(params=wipSavedAssetParam)
                wipSuccess = wipSavedAsset.save()

                if wipSuccess:
                    savedAssetParam['parent'] = wipSavedAsset.db_id
                    savedAsset = api.savedAsset.SavedAsset(params=savedAssetParam)
                    success = savedAsset.save()
                    if success:
                        self.updateWipParentOnDB(wipSavedAsset, savedAsset)
                else:
                    rbwUI.RBWError(title='ATTENTION', text='Problems during autosave wip.\nPleas control the scene and make a new save.')
                    return
            else:
                savedAsset = api.savedAsset.SavedAsset(params=savedAssetParam)
                success = savedAsset.save()

            if success:
                rbwUI.RBWConfirm(title='Save {} Complete'.format(self.deptName), text='Asset Save Complete', defCancel=None)
                self.fillSavedAssetsTree()
            else:
                rbwUI.RBWError(title='ATTENTION', text='Some Error During Save')

        except AttributeError:
            rbwUI.RBWError(title='ATTENTION', text='One between deptType or approvation are not properly selected.\nSelect those and try again.')

    ############################################################################
    # @brief      update the parent field for wip saved asset.
    #
    # @param      wipSavedAsset  The wip saved asset
    # @param      defSaveAsset   The definition save asset
    #
    def updateWipParentOnDB(self, wipSavedAsset, defSaveAsset):
        defDbId = defSaveAsset.db_id
        wipDbId = wipSavedAsset.db_id

        defVersion = defSaveAsset.version
        wipNote = wipSavedAsset.note

        newNote = '{} {}'.format(wipNote, defVersion)

        updateQuery = "UPDATE `savedAsset_wip` SET `parent`='{}', `note`='{}' WHERE `id`='{}'".format(defDbId, newNote, wipDbId)
        api.log.logger().debug(updateQuery)
        api.database.deleteQuery(updateQuery)

    ############################################################################
    # @brief      check save node approvation
    #
    # @return     the check result.
    #
    def checkSaveNodes(self):
        saveNodeList = [node for node in cmd.ls(typ="saveNode") if node.count(self.deptName.lower()) and ':' not in node]
        if saveNodeList and len(saveNodeList) > 1:
            rbwUI.RBWError(text="More than a save node for {}".format(self.deptName.lower()), title='ATTENTION')
            return False
        else:
            return True

    ############################################################################
    # @brief      che texture extenction.
    #
    # @return     check result
    #
    def checkTextureExt(self):
        textures = []
        for texture in cmd.ls(type="file"):
            texturePath = cmd.getAttr("{}.fileTextureName".format(texture))
            if texturePath not in textures:
                textures.append(texturePath)

        for texture in textures:
            fileName = os.path.basename(texture)
            ext = fileName.split('.')[-1]
            if ext in ['exr', 'tif', 'tiff']:
                rbwUI.RBWError(text="The file could not be saved.\nThe file contains one or more images node loading the following unsupported file format extensions (exr, tiff or tif).\nTo procede and save please get rid of those files.", title='ATTENTION')
                return False

        return True

    ############################################################################
    # @brief      check to save function
    #
    def checkToSave(self):
        if not self.checkSaveNodes():
            return

        return True

    ################################################################################
    # @brief      check surfacing texture paths.
    #
    #
    def checkTexturesPath(self):
        api.log.logger().debug('checking tex')
        commonShaderFolder = os.path.join(
            '//speed.nas',
            'library',
            '02_production',
            '01_content',
            'mc_library',
            'sourceimages',
            'shader'
        ).replace('\\', '/')

        prjSourceImagesFolder = os.path.join(
            self.mcFolder,
            'sourceimages'
        ).replace('\\', '/')

        assetSourceImagesFolder = os.path.join(
            prjSourceImagesFolder,
            self.currentAsset.category,
            self.currentAsset.group,
            self.currentAsset.name,
            self.currentAsset.variant
        ).replace('\\', '/')

        textureNodes = api.texture.listTextureNodes()

        otherAssetTextures = {}
        shaderLibraryTextures = {}
        exrsTextures = {}

        for node in textureNodes:
            texturePath = api.texture.getImageFromTextureNode(node)
            if assetSourceImagesFolder not in texturePath:
                if prjSourceImagesFolder in texturePath:
                    otherAssetTextures[node] = texturePath
                elif commonShaderFolder in texturePath:
                    shaderLibraryTextures[node] = texturePath
                elif texturePath.endswith('.exr'):
                    exrsTextures[node] = texturePath

        if len(otherAssetTextures) > 0 or len(shaderLibraryTextures) > 0 or len(exrsTextures) > 0:
            errorMessage = 'SOME TEXTURES DONT RESPECT THE NOMENCLATURE FOR THE DEF SAVE.\n\n'

            if len(otherAssetTextures) > 0:
                errorMessage = '{}THOSE TEXTURES CAME FROM ANOTHER ASSET.\n'.format(errorMessage)
                for node in otherAssetTextures:
                    errorMessage = '{}- {}: {}\n'.format(errorMessage, node, otherAssetTextures[node])

                errorMessage = '{}\n'.format(errorMessage)

            if len(shaderLibraryTextures) > 0:
                errorMessage = '{}THOSE TEXTURES CAME FROM THE SHADER LIBRARY.\n'.format(errorMessage)
                for node in shaderLibraryTextures:
                    errorMessage = '{}- {}: {}\n'.format(errorMessage, node, shaderLibraryTextures[node])

                errorMessage = '{}\n'.format(errorMessage)

            if len(exrsTextures) > 0:
                errorMessage = '{}THOSE TEXTURES ARE EXRS.\n'.format(errorMessage)
                for node in exrsTextures:
                    errorMessage = '{}- {}: {}\n'.format(errorMessage, node, exrsTextures[node])

            rbwUI.RBWError(title='ATTENTION', text=errorMessage)
            return False
        else:
            return True

    ############################################################################
    # @brief      check MSH set integrity
    #
    # @return     check result.
    #
    def checkMSHSet(self):
        modelingSaveParams = {
            'asset': self.currentAsset,
            'dept': 'modeling',
            'deptType': 'hig',
            'approvation': 'def'
        }

        modelingSavedAsset = api.savedAsset.SavedAsset(params=modelingSaveParams)

        if modelingSavedAsset.getLatest():
            modelingSavedAssetMshJson = os.path.join(self.mcFolder, modelingSavedAsset.getLatest()[2].replace('.mb', '_MSH.json'))
            modelingSavedAssetXml = os.path.join(self.mcFolder, modelingSavedAsset.getLatest()[2].replace('.mb', '.xml'))

            mshData = {}
            data = api.json.json_read(modelingSavedAssetMshJson)['MSH']

            root = ET.parse(modelingSavedAssetXml).getroot()
            for shape in root.iter('shape'):
                try:
                    shapeDict = shape.attrib
                    meshName = shapeDict['fullpath'].split('|')[-2]
                    vertices = int(shapeDict['vertices'])

                    if meshName in data:
                        mshData[meshName] = {'vertices': vertices}
                except ValueError:
                    pass

            if cmd.objExists('MSH'):
                mshMembers = cmd.sets('MSH', query=True)

                if mshMembers:
                    mshMembers.sort()
                    data.sort()
                else:
                    rbwUI.RBWError(title='ATTENTION', text='The MSH set is different from modeling one (is empty).\nMake they equal and try again.')
                    return

                if mshMembers != data:
                    differenceInScene = list(set(mshMembers) - set(data))
                    differenceFromData = list(set(data) - set(mshMembers))

                    message = 'The MSH set is different from modeling one.\n'

                    if differenceInScene:
                        message = '{}Mesh in scene MSH set and not in modeling MSH set:\n'.format(message)
                        for node in differenceInScene:
                            message = '{}- {}\n'.format(message, node)

                    if differenceFromData:
                        message = '{}Mesh in modeling MSH and not in scene MSH set:\n'.format(message)
                        for node in differenceFromData:
                            message = '{}- {}\n'.format(message, node)

                    message = '{}Make they equal and try again.'.format(message)
                    rbwUI.RBWError(title='ATTENTION', text=message)
                    return
                else:
                    differentVerticesMeshes = []
                    for mesh in data:
                        shape = cmd.listRelatives(mesh, shapes=True)[0]
                        if cmd.polyEvaluate(shape, v=1) != mshData[mesh]['vertices']:
                            differentVerticesMeshes.append(mesh)

                    if len(differentVerticesMeshes) > 0:
                        message = 'The following meshes have a different vertices number from the modeling one.\n'

                        for node in differentVerticesMeshes:
                            message = '{}- {}\n'.format(message, node)

                        message = '{}Make they equal and try again.'.format(message)

                        rbwUI.RBWError(title='ATTENTION', text=message)
                        return
                    else:
                        return True
            else:
                differenceInScene = []
                differentVerticesMeshes = []
                for mesh in data:
                    if cmd.objExists(mesh):
                        shape = cmd.listRelatives(mesh, shapes=True)[0]
                        if cmd.polyEvaluate(shape, v=1) != mshData[mesh]['vertices']:
                            differentVerticesMeshes.append(mesh)
                    else:
                        differenceInScene.append(mesh)

                if len(differenceInScene) > 0:
                    message = 'The MSH set is different from modeling one.\n'

                    if differenceInScene:
                        message = '{}Mesh in scene MSH set and not in modeling MSH set:\n'.format(message)
                        for node in differenceInScene:
                            message = '{}- {}\n'.format(message, node)

                        message = '{}Make they equal and try again.'.format(message)

                        rbwUI.RBWError(title='ATTENTION', text=message)
                        return

                    if differentVerticesMeshes:
                        message = 'The following meshes have a different vertices number from the modeling one.\n'

                        for node in differentVerticesMeshes:
                            message = '{}- {}\n'.format(message, node)

                        message = '{}Make they equal and try again.'.format(message)

                        rbwUI.RBWError(title='ATTENTION', text=message)
                        return

                cmd.sets(name='MSH', empty=True)
                cmd.sets(data, add='MSH')
                return True

        else:
            rbwUI.RBWError(title='ATTENTION', text='No modeling save found for this asset.\nPlease save a modeling and try again.')
            return
