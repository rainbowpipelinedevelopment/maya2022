import importlib
import os
from PySide2 import QtCore, QtWidgets

import maya.cmds as cmd

import api.log
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.log)
# importlib.reload(rbwUI)


################################################################################
# @brief      This class describes a file renamer ui.
#
class FileRenamerUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    def __init__(self):
        super(FileRenamerUI, self).__init__()

        if cmd.window('FileRenamerUI', exists=True):
            cmd.deleteUI('FileRenamerUI')

        self.setObjectName('FileRenamerUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.topFrame = rbwUI.RBWFrame(layout='V')
        self.mainLayout.addWidget(self.topFrame)

        self.instructionLabel = rbwUI.RBWLabel(text='Replace \'oldWord\' with \'newWord\' in all \'Root Folder\' files', size=12)

        self.oldWordLine = rbwUI.RBWLineEdit(title='Old Word', stretch=False, bgColor='rgba(30, 30, 30, 150)')
        self.newWordLine = rbwUI.RBWLineEdit(title='New Word', stretch=False, bgColor='rgba(30, 30, 30, 150)')

        self.folderWidget = rbwUI.RBWFrame(layout='H', bgColor='transparent', margins=[0, 0, 0, 0])
        self.folderLine = rbwUI.RBWLineEdit(title='Root Folder', stretch=False, bgColor='rgba(30, 30, 30, 150)')
        self.folderToolButton = rbwUI.RBWButton(text='. . .', size=[40, 20])
        self.folderToolButton.clicked.connect(self.selectRootFolder)

        self.folderWidget.addWidget(self.folderLine)
        self.folderWidget.addWidget(self.folderToolButton)

        self.renameButton = rbwUI.RBWButton(text='Rename', icon=['rename.png'], size=[200, 35])
        self.renameButton.clicked.connect(self.runRenamer)

        self.topFrame.addWidget(self.instructionLabel)
        self.topFrame.addWidget(self.oldWordLine)
        self.topFrame.addWidget(self.newWordLine)
        self.topFrame.addWidget(self.folderWidget)
        self.topFrame.addWidget(self.renameButton, alignment=QtCore.Qt.AlignHCenter)

        # self.setMinimumSize(400, 200)
        self.setMinimumWidth(400)
        self.setFixedHeight(205)
        self.setTitle('File Renamer')
        self.setIcon('rename.png')
        self.setFocus()

    ############################################################################
    # @brief      select theroot folder from dialog.
    #
    def selectRootFolder(self):
        rootFolder = QtWidgets.QFileDialog.getExistingDirectory(self)
        self.folderLine.setText(rootFolder)

    ############################################################################
    # @brief      run renamer.
    #
    def runRenamer(self):
        rootFolder = self.folderLine.text()
        oldWord = self.oldWordLine.text()
        newWord = self.newWordLine.text()

        if oldWord and newWord and rootFolder:
            if os.path.exists(rootFolder):
                files = os.listdir(rootFolder)
                if files:
                    for file in files:
                        if oldWord in file:
                            newFile = file.replace(oldWord, newWord)
                            oldFileFullPath = os.path.join(rootFolder, file).replace('\\', '/')
                            newFileFullPath = os.path.join(rootFolder, newFile).replace('\\', '/')

                            os.rename(oldFileFullPath, newFileFullPath)
                            api.log.logger().debug('Rename {} to {}'.format(file, newFile))
                else:
                    rbwUI.RBWError(text='There are not files in this folder: {} please check'.format(rootFolder))
            else:
                rbwUI.RBWError(text='The given folder doesn\'t exists')
        else:
            rbwUI.RBWError(text='One or more fields are empty.\nPlease fill them all and try again')
