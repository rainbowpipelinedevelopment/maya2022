import os
import sys
import importlib
import datetime
import subprocess
import shutil

import maya.cmds as cmd
import maya.mel as mel
import maya.OpenMaya as om

from PySide2 import QtGui, QtWidgets

import api.widgets.rbw_UI as rbwUI
import api.savedShot
import api.scene
import depts.animation.sc_animation as scAnim
import depts.finishing.sc_finishing as scFin
import depts.environment.sc_environment as scEnv
import depts.fx.sc_fx as scFx
import depts.finishing.tools.generic.makeColored as mkcol
import api.database
import api.log
import api.vray

# importlib.reload(rbwUI)
# importlib.reload(api.savedShot)
importlib.reload(api.scene)
importlib.reload(scAnim)
importlib.reload(scFin)
importlib.reload(scEnv)
importlib.reload(scFx)
importlib.reload(mkcol)
importlib.reload(api.database)
importlib.reload(api.log)
importlib.reload(api.vray)

displayDict = {
    'textures': 1,
    'lights': 0,
    'strokes': 0,
    'nurbs': 0,
    'meshes': 1,
    'nParticles': 1,
    'particle instancers': 1,
    'dynamics': 0,
    'fluids': 1,
    'colors': 0,
    'cameras': 0,
    'image_plane': 0,
    'horizontal_flip': 0,
    'smooth-char': 0
}

settings = {
    'controllers': True,
    'nurbsCurves': False,
    'nurbsSurfaces': True,
    'controlVertices': True,
    'hulls': True,
    'polymeshes': True,
    'subdivSurfaces': True,
    'planes': True,
    'lights': True,
    'cameras': True,
    'imagePlane': True,
    'joints': True,
    'ikHandles': True,
    'deformers': True,
    'dynamics': True,
    'particleInstancers': True,
    'fluids': True,
    'hairSystems': True,
    'follicles': True,
    'nCloths': True,
    'nParticles': True,
    'nRigids': True,
    'dynamicConstraints': True,
    'locators': True,
    'dimensions': True,
    'pivots': True,
    'handles': True,
    'textures': True,
    'strokes': True,
    'motionTrails': True,
    'pluginShapes': True,
    'clipGhosts': True,
    'greasePencils': True,
    'manipulators': True,
    'grid': True,
    'hud': True,
    'hos': True,
    'selectionHiliteDisplay': True,
}


def main(dept='Anim'):
    panel = PlayblastTool(dept)
    if panel.startFrame:
        panel.show()


class PlayblastTool(rbwUI.RBWWindow):

    def __init__(self, dept):
        super().__init__()

        if cmd.window('PlayblastTool', exists=True):
            cmd.deleteUI('PlayblastTool')
        self.setObjectName('PlayblastTool')

        self.dept = dept
        self.mcFolder = os.getenv('MC_FOLDER')
        self.id_mv = os.getenv('ID_MV')
        self.localDir = os.path.join(os.environ.get("HOME"), "local_playblast").replace('\\', '/')

        db_id = api.database.selectSingleQuery("SELECT `id` FROM `savedShot` WHERE `id_mv` = {} AND `path` LIKE '%{}%'".format(self.id_mv, api.scene.filename()))[0]
        self.savedShot = api.savedShot.SavedShot(params={'db_id': db_id})
        self.deptObj = self.savedShot.getDeptObject()
        self.seasonDir = os.path.join(self.mcFolder, 'scenes', 'animation', self.savedShot.shot.season).replace('\\', '/')

        self.framerate = self.deptObj.framerate
        self.marketingAttr = '{}:Cam_Main_ctrl.marketing'.format(cmd.ls(type="camera")[-1].split(':')[0])
        if not cmd.objExists(self.marketingAttr) or (cmd.objExists(self.marketingAttr) and not cmd.getAttr(self.marketingAttr)):
            self.sceneWidth, self.sceneHeight = str(self.deptObj.prjWidth), str(self.deptObj.prjHeight)
            api.log.logger().debug('-> Scene resolution setted as default show values: {}-{}'.format(self.sceneWidth, self.sceneHeight))
        elif cmd.objExists(self.marketingAttr) and cmd.getAttr(self.marketingAttr):
            self.sceneWidth, self.sceneHeight = str(cmd.getAttr('defaultResolution.width')), str(cmd.getAttr('defaultResolution.height'))
            api.log.logger().debug('-> Resolution value override skipped due to "Marketing" camera attribute turned ON')

        self.startFrame, self.endFrame = self.getStartEndFrame()
        if not self.startFrame and not self.endFrame:
            return
        if int(self.startFrame) == 0:
            self.startFrame = '1'

        if os.name != 'posix':
            self.ffmpeg = os.path.join(os.getenv('LOCALPIPE'), "lib", "ffmpeg2.5", "ffmpeg.exe").replace('\\', '/')
        else:
            self.ffmpeg = os.path.join(os.getenv('LOCALPIPE'), "lib", "ffmpegMac", "ffmpeg").replace('\\', '/')

        self.display = {
            "currentTime": [3, 0, self.getCurrentTime],
            "textScene": [2, 0, self.getOutputName],
            "userInfo": [5, 1, self.getCurrentUser],
            "focalLength": [6, 0, self.getFocalLength],
            "SceneName": [8, 0, self.getSceneName],
            "Sets": [1, 0, self.getSetNames]
        }

        self.smoothDict = {}
        self.checkList = []

        self.initUI()
        self.updateMargins()

    def initUI(self):
        self.mainBox = rbwUI.RBWFrame('H')
        subLay_1 = rbwUI.RBWFrame('V', bgColor='transparent', margins=[0, 0, 0, 0])
        subLay_2 = rbwUI.RBWFrame('V', bgColor='transparent', margins=[0, 0, 0, 0])

        self.pbTypeCombo = rbwUI.RBWComboBox('Playblast type:', items=['PRODUCTION', 'LOCAL'])

        self.cameraGroup = rbwUI.RBWGroupBox('Camera options', 'H', margins=[8, 25, 8, 8])
        subCameraLayout_1 = rbwUI.RBWFrame('V', bgColor='transparent', margins=[0, 0, 0, 0])
        subCameraLayout_2 = rbwUI.RBWFrame('V', bgColor='transparent', margins=[0, 0, 0, 0])
        self.widthEdit = rbwUI.RBWLineEdit('Width:')
        self.heightEdit = rbwUI.RBWLineEdit('Height:')
        self.halfCheck = rbwUI.RBWCheckBox('Half size')
        subCameraLayout_1.addWidget(self.widthEdit)
        subCameraLayout_1.addWidget(self.heightEdit)
        subCameraLayout_1.addWidget(self.halfCheck)
        self.startEdit = rbwUI.RBWLineEdit('Start frame:')
        self.endEdit = rbwUI.RBWLineEdit('End frame:')
        self.cameraCombo = rbwUI.RBWComboBox('Camera:', items=[x.replace('Shape', '') for x in cmd.ls(type="camera")])
        subCameraLayout_2.addWidget(self.startEdit)
        subCameraLayout_2.addWidget(self.endEdit)
        subCameraLayout_2.addWidget(self.cameraCombo)
        self.cameraGroup.addWidget(subCameraLayout_1)
        self.cameraGroup.addWidget(subCameraLayout_2)

        self.codecGroup = rbwUI.RBWGroupBox('Codec options', 'V', margins=[8, 25, 8, 8])
        self.formatCombo = rbwUI.RBWComboBox('Format:', items=['mov', 'avi'])
        self.audioCheck = rbwUI.RBWCheckBox('Export audio')
        self.codecGroup.addWidget(self.formatCombo)
        self.codecGroup.addWidget(self.audioCheck)

        self.finGroup = rbwUI.RBWGroupBox('Finishing options', 'V', margins=[8, 25, 8, 8])
        self.smoothCharCheck = rbwUI.RBWCheckBox('Smooth-CHAR')
        self.smoothPropCheck = rbwUI.RBWCheckBox('Smooth-PROP')
        self.colorCharCheck = rbwUI.RBWCheckBox('Colored-CHAR')
        self.colorPropCheck = rbwUI.RBWCheckBox('Colored-PROP')
        self.finGroup.addWidget(self.smoothCharCheck)
        self.finGroup.addWidget(self.smoothPropCheck)
        self.finGroup.addWidget(self.colorCharCheck)
        self.finGroup.addWidget(self.colorPropCheck)

        self.envGroup = rbwUI.RBWGroupBox('Environment options', 'V', margins=[8, 25, 8, 8])

        self.displayGroup = rbwUI.RBWGroupBox('Display options', 'V', margins=[8, 25, 8, 8])
        self.displayScroll = rbwUI.RBWScrollArea('V', scrollH=False)
        self.displayGroup.addWidget(self.displayScroll)
        for key, value in sorted(displayDict.items()):
            check = rbwUI.RBWCheckBox(key)
            check.setChecked(value)
            self.displayScroll.addWidget(check)
            self.checkList.append(check)

        self.runButton = rbwUI.RBWButton('BLAST!', icon=['../common/playblast.png'], size=[None, 30])
        self.runButton.clicked.connect(self.getOptions)

        subLay_1.addWidget(self.pbTypeCombo)
        subLay_1.addWidget(self.cameraGroup)
        subLay_1.addWidget(self.codecGroup)
        subLay_1.addWidget(self.finGroup)
        subLay_1.addWidget(self.envGroup)
        subLay_2.addWidget(self.displayGroup)
        subLay_2.addWidget(self.runButton)
        self.mainBox.addWidget(subLay_1)
        self.mainBox.addWidget(subLay_2)

        self.mainLayout.addWidget(self.mainBox)

        self.widthEdit.setText(self.sceneWidth)
        self.heightEdit.setText(self.sceneHeight)
        self.startEdit.setText(self.startFrame)
        self.endEdit.setText(self.endFrame)
        self.cameraCombo.setCurrentText(self.getCamera())
        self.audioCheck.setChecked(True)

        self.setMinimumSize(700, 280)
        self.setTitle('{} Playblast Tool'.format(self.dept.capitalize()))
        self.setIcon('../common/playblast.png')
        self.setFocus()

        if self.dept != 'Fin':
            self.finGroup.hide()

        if self.dept != 'Env':
            self.envGroup.hide()
        else:
            self.getEnvInfo()

        if self.savedShot.dept != 'Fin':
            self.smoothCharCheck.setEnabled(False)
            self.smoothPropCheck.setEnabled(False)
            self.colorCharCheck.setEnabled(False)
            self.colorPropCheck.setEnabled(False)

        self.changePath()

    def getEnvInfo(self):
        self.envChecks = {}
        seGroups = cmd.ls('SE_*', type='transform')
        for group in seGroups:
            element = group.split('_')[1].lower()
            elementFolder = os.path.join(self.savedShot.shot.getShotFolder(), 'Env', element).replace('\\', '/')
            checkBox = QtWidgets.QCheckBox('Export "{}"'.format(group))
            checkBox.setChecked(False if os.path.exists(elementFolder) else True)
            self.envChecks[group.replace('SE_', '')] = checkBox
            self.envGroup.addWidget(checkBox)

    def getStartEndFrame(self):
        if cmd.objExists('*:*uides_des*') and not int(cmd.playbackOptions(q=True, animationStartTime=True)) == -30:
            cmd.playbackOptions(e=True, animationStartTime=-30)
            cmd.playbackOptions(e=True, minTime=-30)

            msg = [
                "This shot contains guides!\n",
                "When working with hair please remember to:",
                " - simply animate the hairs when needed (acting or storytelling reasons)",
                " - animate them EVEN if they are not actually visible entirely or partially in the shot",
                "   (or it will impact on the rest of the remaining frames or visible area)",
                " - NOT intersect the hairs against other objects",
                " - have handles in pre roll and post roll for animation\n",
                "I added -30 frames to startframe and +1 on endframe, check everything and retry"
            ]
            rbwUI.RBWDialog(text='\n'.join(msg), parent=self)
            return None, None
        else:
            start = str(int(cmd.playbackOptions(q=True, animationStartTime=True)))
            end = str(int(cmd.playbackOptions(q=True, animationEndTime=True)))
            return start, end

    def getCamera(self):
        try:
            child = cmd.listRelatives('LIBRARY', children=True)
            if len(child) == 1:
                cameraShape = cmd.listRelatives(child[0], allDescendents=True, type='camera')
            else:
                cameraShape = cmd.listRelatives(child[-1], allDescendents=True, type='camera')
                api.log.logger().debug("-> There is more than 1 camera in LIBRARY")

            self.camera = cmd.listRelatives(cameraShape, parent=True)[0]

        except:
            self.camera = cmd.ls(type="camera")[-1]
            api.log.logger().debug("-> I can't select the camera!")

        return self.camera

    def getSound(self):
        aPlayBackSliderPython = mel.eval('$tmpVar=$gPlayBackSlider')
        current = cmd.timeControl(aPlayBackSliderPython, q=True, sound=True)

        if not current and cmd.ls(type='audio'):
            try:
                mel.eval("setSoundDisplay {} 1;".format(cmd.ls(type='audio')[0]))
            except:
                rbwUI.RBWWarning(text="There is an audio but is not active in the timeline!\nThe blast will be without a sound", parent=self)
            current = cmd.timeControl(aPlayBackSliderPython, query=True, sound=True)

        if current:
            soundPath = cmd.getAttr('{}.filename'.format(current)).replace('\\', '/')
            soundOffset = cmd.getAttr('{}.offset'.format(current))
            soundDir = '//speed.nas/{}/01_pre_production/08_animatic/{}/Individual_Shots/AIFF'.format(os.getenv('PROJECT'), self.savedShot.shot.episode)
            if not soundPath.count(soundDir):
                cmd.setAttr('{}.filename'.format(current), os.path.join(soundDir, os.path.basename(soundPath)).replace('\\', '/'), type='string')
                soundPath = cmd.getAttr('{}.filename'.format(current)).replace('\\', '/')

            if not os.path.exists(soundPath):
                infoMex = 'The path of the current Audio file is not correct:\n\n{}\n\nPlease fix it in order to make a good playblast with audio.'.format(soundPath)
                rbwUI.RBWWarning(text=infoMex, parent=self)
                cmd.select('{}.filename'.format(current), r=True)
                return None, None

            else:
                return cmd.getAttr('{}.filename'.format(current)), soundOffset

        else:
            return None, None

    def getVisibleModelPanels(self):
        modelPanels = cmd.getPanel(type="modelPanel")
        visiblePanels = cmd.getPanel(visiblePanels=True)
        return list(set(modelPanels) & set(visiblePanels))

    def changePath(self, default=False):
        self.videoFormat = self.formatCombo.currentText()
        self.pbType = 1 if self.pbTypeCombo.currentText() == 'PRODUCTION' and not default else 0
        if self.pbType:
            self.halfCheck.setChecked(True if int(self.sceneWidth) > 1920 else False)

        editingPath = os.path.join(self.seasonDir, self.savedShot.shot.episode, '_editing', self.savedShot.dept).replace('\\', '/')
        if not os.path.isdir(editingPath):
            os.makedirs(editingPath)

        versionedNameList = [os.getenv('PROJECT').upper(), self.savedShot.shot.season, self.savedShot.shot.episode, self.savedShot.shot.sequence, self.savedShot.shot.shot]
        noVersionVideoName = "{}_RBW.{}".format("_".join(versionedNameList), self.videoFormat)
        versionedNameList.append('Anim_{}'.format(self.savedShot.dept))

        shotPath = os.path.join(self.savedShot.shot.getShotFolder(), self.savedShot.dept).replace('\\', '/')
        if not os.path.exists(shotPath):
            shotPath = shotPath.replace(os.path.basename(self.seasonDir), 'S01')
            editingPath = editingPath.replace(os.path.basename(self.seasonDir), 'S01')
        if self.savedShot.dept in ['Fin']:
            self.savedShot.deptType = 'All'
            self.savedShot.itemName = ''
        lastVersionSave = self.savedShot.getLatest()
        if lastVersionSave:
            latestVersion = int(lastVersionSave[3].split('r')[1])
        else:
            latestVersion = 0
        # versions = sorted([x.split('vr')[1] for x in os.listdir(shotPath) if x.startswith('vr')])
        # latestVersion = int(versions[-1]) if versions else 0
        if self.savedShot.dept in ['Fx']:
            fxName = self.savedShot.itemName
            # fxName = os.path.basename(os.path.dirname(cmd.file(query=True, sn=True)))
            # latestVersion = int(cmd.file(q=1, sceneName=1).split('/')[-1].split('_')[7][1:])
            versionedNameList.append(fxName)
        self.version = 'v{}'.format(str(latestVersion + (1 if not default else 0)).zfill(3))

        versionedNameList.append(self.version)
        versionedNameList.append('RBW')
        versionVideoName = "{}.{}".format("_".join(versionedNameList), self.videoFormat)

        # make the temporary folder with specific name to run multiple blast from different maya istances
        now = datetime.datetime.now()
        tmpFolderName = now.strftime("%Y%m%d_%H%M%S")

        imgDir = os.path.join(self.localDir, tmpFolderName)
        self.imageName = noVersionVideoName.split(".")[0]

        self.negativeFrames = True if self.pbType else False
        folder = editingPath if (self.pbType or default) else self.localDir
        file = versionVideoName if (self.pbType or default) else noVersionVideoName

        self.imagepath = os.path.join(imgDir, self.imageName).replace('\\', '/')
        self.filename = os.path.join(folder, file).replace('\\', '/')

    def getOptions(self, default=False):
        if cmd.getAttr("{}.panZoomEnabled".format(self.camera)):
            cmd.setAttr("{}.panZoomEnabled".format(self.camera), 0)

        self.sizeConf = 0.5 if self.halfCheck.isChecked() else 1

        # display options from gui
        for check in self.checkList:
            if check.isChecked():
                displayDict[str(check.getText())] = 1
            else:
                displayDict[str(check.getText())] = 0

        # get audio if required
        self.sound = False
        if self.audioCheck.isChecked():
            self.sound = True
            self.sound, self.soundOffset = self.getSound()
            list_audio = cmd.ls(type="audio")
            if list_audio:
                playBackSliderPython = mel.eval('$tmpVar=$gPlayBackSlider')
                cmd.timeControl(playBackSliderPython, s=list_audio[0], e=True, ds=True)
            # api.log.logger().debug('-> sound: {}'.format(self.sound))

        self.changePath(default)
        api.log.logger().debug('-> Version: {}'.format(self.version))

        self.makeBlast(default)

        if cmd.window("PlayblastTool", exists=True):
            cmd.deleteUI("PlayblastTool")

    def makeBlast(self, default):
        geoRes = {}
        if self.pbType:
            api.log.logger().debug("-> Saving a new version of the maya-scene...")  # env e fx e fin (se msh o vrscene) passa itemname
            if not self.filename.count('_Fin_'):
                if self.filename.count('_Env_'):
                    import depts.environment.tools.generic.checkEnvScene as checkEnv
                    importlib.reload(checkEnv)
                    if checkEnv.checkVrMeshes(self.mcFolder):
                        goOn = checkEnv.checkScene()
                        if goOn[0]:
                            for element in self.envChecks:
                                if self.envChecks[element].isChecked():
                                    savedElementFile = self.deptObj.exportEnvGroups(element)
                                    api.vray.exportBatchVrscene(savedElementFile)
                        else:
                            msg = "These assets can't be switched for the export:\n\n"
                            if goOn[1]:
                                msg += "Bad input assets:\n - {}\nPlease load 'hig' or 'prx' modeling for PROPS and 'source' or 'ani' fx for FX\n\n".format('\n - '.join(['{}  <{}-{}-{}>'.format(x, goOn[1][x]['category'], goOn[1][x]['dept'], goOn[1][x]['type']) for x in goOn[1].keys()]))
                            if goOn[2]:
                                msg += "Bad output assets:\n - {}\nPlease ask to save surfacing rnd for PROPS or fx rnd for FX".format('\n - '.join(['{}  <{}-{}>'.format(x, goOn[2][x]['category'], goOn[2][x]['dept']) for x in goOn[2].keys()]))
                            rbwUI.RBWError(text=msg, parent=self)
                            return
                    else:
                        rbwUI.RBWError(text="Not all the props have a vrmesh or a def save yet,\nask surfacing dept for a check", parent=self)
                        return

                self.savedShot.note = 'Autosave by Playblast Tool'
                self.savedShot.promoted = 0
                self.savedShot.save()
            else:
                import depts.animation.tools.generic.getAudio as getAudio
                importlib.reload(getAudio)
                getAudio.main(auto=True)
                self.savedShot.note = 'Autosave by Playblast Tool NOT FOR LGT'
                self.savedShot.deptType = 'all'
                self.savedShot.save()
                self.finishingSceneSetup()

                # get audio if required
                self.sound = False
                if self.audioCheck.isChecked():
                    self.sound = True
                    self.sound, self.soundOffset = self.getSound()
                    list_audio = cmd.ls(type="audio")
                    if list_audio:
                        playBackSliderPython = mel.eval('$tmpVar=$gPlayBackSlider')
                        cmd.timeControl(playBackSliderPython, s=list_audio[0], e=True, ds=True)

            # store the Geo_Res value for each rig and switch to Proxy if it's possible
            for ctrl in cmd.ls('*:Transform_Ctrl'):
                controlAttrName = '{}.Geo_Res'.format(ctrl)
                try:
                    currentValue = cmd.getAttr('{}.Geo_Res'.format(ctrl))
                    # force to HiRes for the production playblast
                    cmd.setAttr(controlAttrName, 0)
                    geoRes[controlAttrName] = currentValue
                except:
                    pass

        # switching all the assemblies to txt representation
        self.assemblyReps = {}
        for assembly in cmd.ls(type='assembly'):
            assemblyObj = api.assembly.Assembly(node=assembly)
            self.assemblyReps[assemblyObj] = assemblyObj.getActiveRepresentationName()
            assemblyObj.switchRepresentation('txt')

        imageName = os.path.split(self.imagepath)[-1]
        imgDir = os.path.split(self.imagepath)[0]
        folder = os.path.dirname(self.filename)
        self.localFile = os.path.join(self.localDir, os.path.basename(self.filename)).replace('\\', '/')

        if not os.path.isdir(folder):
            os.makedirs(folder)

        if os.path.isdir(imgDir):
            for item in os.listdir(imgDir):
                if item.count(imageName):
                    os.remove(os.path.join(imgDir, item))

        if os.path.isfile(self.localFile):
            try:
                os.remove(self.localFile)
            except Exception:
                msg = "ERROR: I can't delete {}, maybe it's already opened by someone!".format(self.localFile)
                rbwUI.RBWError(text=msg, parent=self)
                raise Exception(msg)

        self.outputName = os.path.basename(self.filename)

        # set black stripes height, user playblast size e arrangiamento per FFMPEG che vuole size power of 2
        stripes_height = 0
        if not cmd.objExists(self.marketingAttr) or (cmd.objExists(self.marketingAttr) and not cmd.getAttr(self.marketingAttr)):
            stripes_height = (int(self.heightEdit.text()) * .2) * self.sizeConf
        self.pbWidth = ((int(self.widthEdit.text()) * self.sizeConf) / 2) * 2
        self.pbHeight = (int(self.heightEdit.text()) * self.sizeConf) + stripes_height
        self.videoRatio = float(self.pbWidth) / float(self.pbHeight)

        api.log.logger().debug("===== BLAST OPTIONS =====")
        api.log.logger().debug("Blast size: {}, {}".format(self.pbWidth, self.pbHeight))
        api.log.logger().debug("Stripe Height: {}".format(int(stripes_height / 2) if stripes_height else 0))
        api.log.logger().debug("Video Ratio: {}".format(self.videoRatio))
        api.log.logger().debug("=========================")

        cmd.setAttr("defaultRenderGlobals.imageFormat", 8)
        self.vcodec = "mjpeg" if self.videoFormat == 'avi' else 'copy'

        pbStart = str(int(cmd.playbackOptions(q=True, ast=True)))
        pbEnd = str(int(cmd.playbackOptions(q=True, aet=True)))

        self.adjustCamera()
        self.setEditorNotes()

        cmd.playblast(
            filename=self.imagepath,
            offScreen=True,
            startTime=pbStart,
            endTime=pbEnd,
            viewer=False,
            compression="jpg",
            format="image",
            percent=100,
            showOrnaments=True,
            framePadding=4,
            widthHeight=[self.pbWidth, self.pbHeight],
            forceOverwrite=True
        )

        self.deleteNotes()

        # set visibility again
        for hud in self.visibleHudList:
            cmd.headsUpDisplay(hud, e=True, visible=True)

        # set camera visibility again
        for panel in cmd.getPanel(type="modelPanel"):
            cmd.modelEditor(panel, e=True, cameras=True)

        # rinomino le img in sequenza a partire da 1 prendendo in considerazione solo i frame positivi
        if self.vcodec:
            self.renameJpgs(imgDir, imageName)

        if displayDict['horizontal_flip'] == 1:
            nameToRemove = self.imagepath.split("\\")[-1:]
            imagesPath = self.imagepath[:-len(nameToRemove[0])]
            if os.path.isdir(os.path.normpath(imagesPath)):
                for imageFile in os.listdir(imagesPath):
                    fullFilePath = os.path.normpath(os.path.join(imagesPath, imageFile))
                    pixmap1 = QtGui.QPixmap(fullFilePath)
                    pixmap1 = pixmap1.transformed(QtWidgets.QTransform().rotate(90))
                    pixmap1 = pixmap1.save(fullFilePath)
                    self.imagepath = om.MImage()
                    self.imagepath.readFromFile(fullFilePath)
                    self.imagepath.verticalFlip()
                    self.imagepath.writeToFile(fullFilePath, "jpg")
                    pixmap2 = QtGui.QPixmap(fullFilePath)
                    pixmap2 = pixmap2.transformed(QtWidgets.QTransform().rotate(-90))
                    pixmap2 = pixmap2.save(fullFilePath)

        # set sound ffmpeg command
        self.assembleMovFile()

        # copio il mov appena fatto in locale nella content se ho scelto production
        if self.localFile != self.filename:
            if os.path.isfile(self.filename):
                try:
                    os.remove(self.filename)
                except:
                    msg = "ERROR: I can't delete {}, maybe it's already opened by someone!".format(self.filename)
                    rbwUI.RBWError(text=msg, parent=self)
                    return

            shutil.copy2(self.localFile, folder)

        # copy jpgs files in the maya-content for eventual use in screen
        if self.filename.count('_Anim_'):
            self.copyJpgs(imgDir)

        # delete images
        self.deleteJpgs(imgDir)

        api.log.logger().debug("-> {}: {}".format('File', self.filename if self.pbType else 'LocalFile', self.localFile))

        # run video of playblast
        if os.path.isfile(self.localFile):
            if os.name != 'posix':
                os.startfile(self.localFile)
            else:
                opener = "open" if sys.platform == "darwin" else "xdg-open"
                subprocess.call([opener, self.localFile])

        # restore the geo res value (from original dictionary data)
        if len(geoRes):
            [cmd.setAttr(k, geoRes[k]) for k in geoRes]

        # resize .mov file generating the _Small version
        if self.pbType or default:
            self.makeSmall()

        for sett in settings:
            if sett == 'pluginObjects' and not settings[sett]:
                cmd.modelEditor(self.modelPanel, e=True, pluginObjects=('', 0))
            else:
                cmd.modelEditor(self.modelPanel, e=True, **{sett: settings[sett]})

        if displayDict['smooth-char']:
            self.restoreSmooth()

        if self.filename.count('_Fin_'):
            self.restoreFinishingSetup()

        # restore assemblies representations
        [key.switchRepresentation(value) for key, value in self.assemblyReps.items()]

    def finishingSceneSetup(self):
        displayDict['smooth-char'] = 1 if self.smoothCharCheck.isChecked() else 0

        coloredAssets = []
        if self.colorCharCheck.isChecked() and cmd.objExists('CHARACTER'):
            coloredAssets += [x for x in cmd.listRelatives('CHARACTER', children=True)]
        if self.colorPropCheck.isChecked() and cmd.objExists('PROP'):
            coloredAssets += [x for x in cmd.listRelatives('PROP', children=True)]

        if coloredAssets:
            if cmd.objExists('CHARACTER'):
                cmd.setAttr('CHARACTER.v', 1)
            if cmd.objExists('PROP'):
                cmd.setAttr('PROP.v', 1)

            if cmd.objExists('MSH_colored_'):
                cmd.delete('MSH_colored_')

            for shd in cmd.ls(type='lambert'):
                if shd.startswith('SHD_'):
                    cmd.delete(shd)

            cmd.select(clear=True)
            for x in coloredAssets:
                cmd.select(x, add=True)
            mkcol.main()

            if self.colorCharCheck.isChecked() and cmd.objExists('CHARACTER'):
                cmd.setAttr('CHARACTER.v', 0)
            if self.colorPropCheck.isChecked() and cmd.objExists('PROP'):
                cmd.setAttr('PROP.v', 0)

    def restoreFinishingSetup(self):
        if self.colorCharCheck.isChecked() and cmd.objExists('CHARACTER'):
            cmd.setAttr('CHARACTER.v', 1)
        if self.colorPropCheck.isChecked() and cmd.objExists('PROP'):
            cmd.setAttr('PROP.v', 1)

        if cmd.objExists('MSH_colored_'):
            cmd.setAttr('MSH_colored_.v', 0)

    def adjustCamera(self):
        cmd.lookThru(self.camera)
        if not cmd.objExists(self.marketingAttr) or (cmd.objExists(self.marketingAttr) and not cmd.getAttr(self.marketingAttr)):
            cmd.camera(self.camera, e=True, filmFit='horizontal')
            cmd.setAttr('defaultResolution.width', int(self.sceneWidth))
            cmd.setAttr('defaultResolution.height', int(self.sceneHeight))

        cmd.camera(self.camera, e=True, displayResolution=True)
        cmd.camera(self.camera, e=True, displayGateMask=True)

        cmd.setAttr("{}.displayGateMaskOpacity".format(self.camera), 1.0)
        cmd.setAttr("{}.displayGateMaskColor".format(self.camera), 0, 0, 0, type="double3")

        cmd.camera(self.camera, e=True, overscan=1.0)

        cmd.setAttr("defaultResolution.pixelAspect", 1.0)
        cmd.setAttr("defaultResolution.deviceAspectRatio", 1.77777779102)

    def setEditorNotes(self):
        # turn off visibility for all huds that are not in noteList
        hudList = []
        listPresets = cmd.headsUpDisplay(listHeadsUpDisplays=True)

        for hud in listPresets:
            if cmd.headsUpDisplay(hud, visible=True, q=True) and hud not in self.display:
                hudList.append(hud)

        self.visibleHudList = hudList
        for hud in hudList:
            cmd.headsUpDisplay(hud, e=True, visible=False)

        self.modelPanel = self.getVisibleModelPanels()[0]

        for sett in settings:
            try:
                value = cmd.modelEditor(self.modelPanel, q=True, **{sett: settings[sett]})
                settings[sett] = value
            except:
                api.log.logger().warning('Error setting {} to {} with modelEditor command!'.format(sett, settings[sett]))

        cmd.modelEditor(self.modelPanel, e=True, allObjects=False)
        cmd.modelEditor(self.modelPanel, e=True, nurbsSurfaces=True)
        cmd.modelEditor(self.modelPanel, e=True, dynamics=True)
        cmd.modelEditor(self.modelPanel, e=True, polymeshes=True)
        cmd.modelEditor(self.modelPanel, e=True, fluids=True)
        cmd.modelEditor(self.modelPanel, e=True, nParticles=True)
        cmd.modelEditor(self.modelPanel, e=True, particleInstancers=True)
        cmd.modelEditor(self.modelPanel, e=True, cameras=True)
        cmd.modelEditor(self.modelPanel, e=True, pluginShapes=True)
        cmd.modelEditor(self.modelPanel, e=True, pluginObjects=['gpuCacheDisplayFilter', True])

        # hack mtp per image plane sempre visibile
        if os.getenv('PROJECT') == 'mtp':
            cmd.modelEditor(self.modelPanel, e=True, imagePlane=True)

        if displayDict['cameras'] == 0:
            cmd.modelEditor(self.modelPanel, e=True, cameras=False)
        if displayDict['strokes'] == 1:
            cmd.modelEditor(self.modelPanel, e=True, strokes=True)
        if displayDict['nurbs'] == 0:
            cmd.modelEditor(self.modelPanel, e=True, nurbsSurfaces=False)
        if displayDict['meshes'] == 0:
            cmd.modelEditor(self.modelPanel, e=True, polymeshes=False)
        if displayDict['nParticles'] == 0:
            cmd.modelEditor(self.modelPanel, e=True, nParticles=False)
        if displayDict['particle instancers'] == 0:
            cmd.modelEditor(self.modelPanel, e=True, particleInstancers=False)
        if displayDict['dynamics'] == 1:
            cmd.modelEditor(self.modelPanel, e=True, dynamics=True)
        if displayDict['fluids'] == 0:
            cmd.modelEditor(self.modelPanel, e=True, fluids=False)

        if displayDict['textures'] == 1:
            cmd.DisplayShadedAndTextured()
        else:
            cmd.DisplayShaded()

        if displayDict['lights'] == 1:
            cmd.DisplayLight()

        if displayDict['colors'] == 1:
            mkcol.mainForBlast()

        if displayDict['image_plane'] == 1:
            cmd.modelEditor(self.modelPanel, e=True, imagePlane=True)

        if displayDict['smooth-char'] == 1 or self.smoothPropCheck.isChecked():
            if (cmd.objExists('*:All_Rig_Driver_Meshes_RM') and self.savedShot.dept != 'Fin') or (cmd.objExists('*:MSH') and self.savedShot.dept == 'Fin'):
                assets = []
                if self.dept == 'Fin':
                    if self.smoothCharCheck.isChecked():
                        chars = [x.split(':')[0] for x in cmd.listRelatives('CHARACTER', children=True)] if cmd.objExists('CHARACTER') and cmd.listRelatives('CHARACTER', children=True) else []
                        if self.savedShot.dept == 'Fin':
                            if self.colorCharCheck.isChecked():
                                assets += [x for x in cmd.ls('MSH_colored_', dag=1, shapes=1, ni=1, type='mesh', l=1) if x.split('|')[-2].split(':')[0] in chars and x.split('|')[-1].startswith('MSH_')]
                            else:
                                assets += [x for x in cmd.sets('*:MSH', q=True) if x.split(':')[0] in chars]
                        else:
                            assets += [x for x in cmd.sets('*:All_Rig_Driver_Meshes_RM', q=True) if x.split(':')[0] in chars]
                    if self.smoothPropCheck.isChecked():
                        props = [x.split(':')[0] for x in cmd.listRelatives('PROP', children=True)] if cmd.objExists('PROP') and cmd.listRelatives('PROP', children=True) else []
                        if self.savedShot.dept == 'Fin':
                            if self.colorPropCheck.isChecked():
                                assets += [x for x in cmd.ls('MSH_colored_', dag=1, shapes=1, ni=1, type='mesh', l=1) if x.split('|')[-2].split(':')[0] in props and x.split('|')[-1].startswith('MSH_')]
                            else:
                                assets += [x for x in cmd.sets('*:MSH', q=True) if x.split(':')[0] in props]
                        else:
                            assets += [x for x in cmd.sets('*:All_Rig_Driver_Meshes_RM', q=True) if x.split(':')[0] in props]
                else:
                    chars = [x.split(':')[0] for x in cmd.listRelatives('CHARACTER', children=True)] if cmd.objExists('CHARACTER') and cmd.listRelatives('CHARACTER', children=True) else []
                    assets += [x for x in cmd.sets('*:All_Rig_Driver_Meshes_RM', q=True) if x.split(':')[0] in chars and x.count(':MSH_')]

                for asset in assets:
                    self.smoothDict[asset] = cmd.displaySmoothness(asset, q=True, polygonObject=True)[0]
                    cmd.displaySmoothness(asset, polygonObject=3)

        cmd.select(cl=True)
        self.displayBlastNotes()
        cmd.refresh()

        cmd.modelEditor(self.modelPanel, e=True, displayAppearance='smoothShaded')
        cmd.modelEditor(self.modelPanel, e=True, pluginObjects=('gpuCacheDisplayFilter', 1))

    def copyJpgs(self, imgDir):
        jpgsFolder = ''
        cutFilePath = self.filename.split('_Anim_')[0].replace('\\', '/')
        if cutFilePath.count('/Lay/'):
            jpgsFolder = cutFilePath.replace('/Lay/', '/Lay_jpgs/')
        elif cutFilePath.count('/Pri/'):
            jpgsFolder = cutFilePath.replace('/Pri/', '/Pri_jpgs/')
        elif cutFilePath.count('/Sec/'):
            jpgsFolder = cutFilePath.replace('/Sec/', '/Sec_jpgs/')
        elif cutFilePath.count('/Fin/'):
            jpgsFolder = cutFilePath.replace('/Fin/', '/Fin_jpgs/')
        elif cutFilePath.count('/Fx/'):
            jpgsFolder = cutFilePath.replace('/Fx/', '/Fx_jpgs/')
        elif cutFilePath.count('/Env/'):
            jpgsFolder = cutFilePath.replace('/Env/', '/Env_jpgs/')

        if os.path.exists(jpgsFolder):
            self.deleteJpgs(jpgsFolder)
            shutil.copytree(imgDir, jpgsFolder)

    def renameJpgs(self, imgDir, imageName):
        imgList = []
        for img in os.listdir(imgDir):
            if img.count(imageName):
                frame = img.split(".")[-2]
                if int(frame) > 0:
                    imgList.append(os.path.join(imgDir, img))

        newNameList = []

        for counter in range(1, len(imgList) + 1):
            newName = "{}{}.{}.jpeg".format(imageName, "_rbw", str(counter).zfill(4))
            newNameList.append(os.path.join(imgDir, newName))

        for imgCounter in range(len(imgList)):
            os.rename(imgList[imgCounter], newNameList[imgCounter])

    def deleteJpgs(self, imgDir):
        for item in os.listdir(imgDir):
            os.remove(os.path.join(imgDir, item))
        try:
            os.rmdir(imgDir)
        except OSError:
            api.log.logger().debug("-> I'm not able to remove images's temp folder")

    def assembleMovFile(self):
        self.soundCommand = ""
        if self.sound:
            if self.soundOffset == 0.0:
                self.soundCommand = ' -i "{}" '.format(self.sound)
            else:
                offsetTimecode = self.frames_to_timecode(frames=int(self.soundOffset), separator='.')
                self.soundCommand = '-itsoffset {} -i "{}" '.format(offsetTimecode, self.sound)

        ffCommand = ' '.join([
            '"{}"'.format(self.ffmpeg),
            '-framerate {}'.format(self.framerate),
            '-i "{0[image]}".%04d.jpeg'.format({'image': "{}_rbw".format(self.imagepath)}),
            self.soundCommand,
            '-r {}'.format(self.framerate),
            '-s {}x{}'.format(self.pbWidth, self.pbHeight),
            '-aspect {}'.format(self.videoRatio),
            '-c:v {}'.format(self.vcodec),
            '-b:v 100000k',
            '-pix_fmt rgb24',
            '"{}"'.format(self.localFile)
        ])

        api.log.logger().debug('ffCommand: {}'.format(ffCommand))
        process = subprocess.Popen(ffCommand, shell=True)
        mkcol.eventualCleanup()
        if not process.wait():
            pass

    def makeSmall(self):
        endFrame = int(cmd.playbackOptions(q=1, aet=1))
        self.framerate = int(self.framerate)
        newMovPath = self.filename.replace('.mov', '_Small.mov')
        seconds = self.from_timecode_to_seconds(endFrame)

        resizeCommand = '{} -y -t {} -r {} -i {} -vf scale={}:-2 -pix_fmt yuv420p {}'.format(self.ffmpeg, seconds, self.framerate, self.filename, self.pbWidth, newMovPath)
        resizeProcess = subprocess.Popen(resizeCommand, shell=True, stdout=subprocess.PIPE)
        while resizeProcess.poll() is None:
            pass

        if not resizeProcess.wait() and os.path.exists(newMovPath):
            os.remove(self.filename)
            api.log.logger().debug('-> Small.mov file exported and fullHD playblast deleted')

    def frames_to_timecode(self, frames, separator=':'):
        return '{0:02d}:{1:02d}:{2:02d}{3}{4:02d}'.format(
            frames // (3600 * self.framerate),
            frames // (60 * self.framerate) % 60,
            frames // self.framerate % 60,
            separator,
            frames % self.framerate
        )

    def from_timecode_to_seconds(self, frames):
        timeString = self.frames_to_timecode(frames)

        timeList = timeString.split(":")
        hours = int(timeList[0]) * 60 * 60
        minutes = int(timeList[1]) * 60
        seconds = int(timeList[2])
        frames = int(timeList[3]) * (1 / float(self.framerate))

        totalTime = hours + minutes + seconds + frames

        return totalTime

    def restoreSmooth(self):
        for x in self.smoothDict:
            cmd.displaySmoothness(x, polygonObject=self.smoothDict[x])

    # ========================== Head Up Display functions ============================
    def displayBlastNotes(self):
        for disp, values in self.display.items():
            cmd.headsUpDisplay(disp, section=values[0], block=values[1], attachToRefresh=True, blockSize="large", dataFontSize="large", allowOverlap=True, blockAlignment="center", label="", labelFontSize="large", command=values[2])

    def getCurrentTime(self):
        frame = str(int(cmd.currentTime(query=True))).zfill(3)
        tot = str(int(cmd.playbackOptions(q=1, aet=1))).zfill(3)
        return "frame: %(frame)s | tot: %(tot)s" % {'frame': frame, 'tot': tot}

    def getOutputName(self):
        return self.outputName

    def getCurrentUser(self):
        return os.getenv("USERNAME")

    def getFocalLength(self):
        focalLength = cmd.camera(self.camera, q=True, fl=True)
        return str(round(focalLength, 2))

    def getSceneName(self):
        return cmd.file(q=True, sceneName=True, shortName=True)

    def getSetNames(self):
        setNamesList = []
        domeNamesList = []

        if cmd.objExists('SET'):
            setList = cmd.listRelatives('SET', children=True)
            if setList:
                for setListElement in setList:
                    setNameTokens = setListElement.split('_')
                    setName = '_'.join(setNameTokens[1:-1])
                    setNamesList.append(setName)

        if cmd.objExists('PROP'):
            propList = cmd.listRelatives('PROP', children=True)
            if propList:
                for prop in propList:
                    if 'dome' in prop:
                        namespace = prop.split(':')[0]
                        saveNode = '{}:rigging_saveNode'.format(namespace) if not namespace.count('_RND') else '{}:surfacing_saveNode'.format(namespace)
                        version = cmd.getAttr('{}.version'.format(saveNode))
                        if namespace.startswith(os.getenv('PROJECT')):
                            namespaceWithoutProjectCode = namespace.split(os.getenv('PROJECT'))[1]
                        else:
                            namespaceWithoutProjectCode = namespace
                        domeName = '{}{}'.format(namespaceWithoutProjectCode, version)
                        domeNamesList.append(domeName)

        setsAndDomesList = setNamesList + domeNamesList

        startFrame = int(cmd.playbackOptions(q=1, ast=1))
        if startFrame == 0:
            startFrame = 1
        endFrame = int(cmd.playbackOptions(q=1, aet=1))
        totalTime = (endFrame - startFrame + 1)

        indexList = 0
        try:
            currentFrame = int(cmd.currentTime(query=True))
            displayTime = int(totalTime / len(setsAndDomesList))
            indexList = int(currentFrame / displayTime)
        except ZeroDivisionError:
            api.log.logger().warning('-> No Sets or Domes detected in this scene, the HUD will display an empty field')
            pass

        try:
            return setsAndDomesList[indexList]
        except IndexError:
            return

    def deleteNotes(self):
        for disp in self.display:
            try:
                cmd.headsUpDisplay(disp, remove=True)
            except:
                pass
    # =================================================================================
