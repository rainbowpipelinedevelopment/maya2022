import importlib
import os

import maya.cmds as cmd

import api.log
import api.scene
import api.savedAsset

importlib.reload(api.log)
importlib.reload(api.scene)
# importlib.reload(api.savedAsset)


################################################################################
# @brief      Opens a source images folder.
#
# @param      dept  The dept
#
def openSourceImagesFolder(dept):
    saveNode = cmd.ls('{}_saveNode'.format(dept))[0]
    currentSavedAsset = api.savedAsset.SavedAsset(saveNode=saveNode)

    category = currentSavedAsset.getCategory()
    group = currentSavedAsset.getGroup()
    name = currentSavedAsset.getName()
    variant = currentSavedAsset.getVariant()

    sourceImagesFolder = os.path.join(
        os.getenv('MC_FOLDER'),
        'sourceimages',
        category,
        group,
        name,
        variant
    ).replace('/', '\\')

    api.log.logger().debug('explorer {}'.format(sourceImagesFolder))

    if os.path.exists(sourceImagesFolder):
        os.system('explorer {}'.format(sourceImagesFolder))
    else:
        api.log.logger().error('Can\'t not open path "{}"'.format(sourceImagesFolder))


################################################################################
# @brief      Opens a scene folder.
#
def openSceneFolder():
    sceneName = api.scene.scenename()
    mcFolder = os.getenv('MC_FOLDER')

    if mcFolder in sceneName:
        pathToOpen = os.path.dirname(sceneName).replace('/', '\\')
        os.system('explorer {}'.format(pathToOpen))
    else:
        os.system('explorer {}'.format(mcFolder.replace('/', '\\')))
