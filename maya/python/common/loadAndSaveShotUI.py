import importlib
import os
import functools
from PySide2 import QtGui, QtWidgets, QtCore

import maya.cmds as cmd

import api.log
import api.system
import api.scene
import api.vray
import config.dictionaries
import api.shot
import api.savedShot
import api.json
import api.camera
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.log)
importlib.reload(api.system)
importlib.reload(api.scene)
importlib.reload(api.vray)
importlib.reload(config.dictionaries)
# importlib.reload(api.shot)
# importlib.reload(api.savedShot)
importlib.reload(api.json)
importlib.reload(api.camera)
# importlib.reload(rbwUI)

iconPath = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'common').replace('\\', '/')


################################################################################
# @brief      This class describes a shot ui.
#
class ShotUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs the object
    #
    # @param      self  The object
    #
    def __init__(self):
        super().__init__()

        if cmd.window('ShotUI', exists=True):
            cmd.deleteUI('ShotUI')

        self.mcFolder = os.getenv('MC_FOLDER')
        api.log.logger().debug(self.mcFolder)
        self.id_mv = os.getenv('ID_MV')
        api.log.logger().debug(self.id_mv)
        self.animationFolder = os.path.join(self.mcFolder, 'scenes', 'animation').replace('\\', '/')
        api.log.logger().debug(self.animationFolder)

        self.shots = api.system.getShotsFromDB()

        self.setObjectName('ShotUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        # vertical splitter
        self.verticalSplitter = QtWidgets.QSplitter(QtCore.Qt.Vertical)
        self.verticalSplitter.setObjectName('splitterV')
        self.verticalSplitter.setStyleSheet('QSplitter#splitterV{background-color: transparent; border-radius: 5px;}')
        self.mainLayout.addWidget(self.verticalSplitter)

        ########################################################################
        # TOP WIDGET
        self.topWidget = rbwUI.RBWFrame(layout='V', radius=5, spacing=10)

        # ------------------------- search widget ------------------------- #
        self.searchGroup = rbwUI.RBWGroupBox(title='Search shot(name):', layout='V', fontSize=12)

        self.searchWidget = rbwUI.RBWFrame(layout='H', margins=[0, 0, 0, 0], bgColor='transparent')

        self.searchLineEdit = rbwUI.RBWLineEdit(title='Type shot name:', bgColor='transparent', stretch=False)
        self.searchLineEdit.returnPressed.connect(self.searchEntry)
        self.searchButton = rbwUI.RBWButton(icon=['search.png'])
        self.searchButton.clicked.connect(self.searchEntry)

        self.searchWidget.addWidget(self.searchLineEdit)
        self.searchWidget.addWidget(self.searchButton)

        self.searchGroup.addWidget(self.searchWidget)

        self.searchResultList = QtWidgets.QListWidget()
        self.searchResultList.hide()
        self.searchResultList.itemClicked.connect(self.matchShot)

        self.searchGroup.addWidget(self.searchResultList)

        self.topWidget.addWidget(self.searchGroup, alignment=QtCore.Qt.AlignTop)

        # ----------------------------- info widget --------------------------- #
        self.infoLayout = QtWidgets.QGridLayout()

        # shot info
        self.shotInfoGroup = rbwUI.RBWGroupBox(title='Shot Info:', layout='V', fontSize=12)

        self.shotSeasonComboBox = rbwUI.RBWComboBox('Season', bgColor='transparent')
        self.shotEpisodeComboBox = rbwUI.RBWComboBox('Episode', bgColor='transparent')
        self.shotSequenceComboBox = rbwUI.RBWComboBox('Seuquence', bgColor='transparent')
        self.shotShotComboBox = rbwUI.RBWComboBox('Shot', bgColor='transparent')

        self.shotSeasonComboBox.activated.connect(self.fillShotEpisodeComboBox)
        self.shotEpisodeComboBox.activated.connect(self.fillShotSequenceComboBox)
        self.shotSequenceComboBox.activated.connect(self.fillShotShotComboBox)
        self.shotShotComboBox.activated.connect(self.setCurrentShot)

        self.shotInfoGroup.addWidget(self.shotSeasonComboBox)
        self.shotInfoGroup.addWidget(self.shotEpisodeComboBox)
        self.shotInfoGroup.addWidget(self.shotSequenceComboBox)
        self.shotInfoGroup.addWidget(self.shotShotComboBox)

        self.infoLayout.addWidget(self.shotInfoGroup, 0, 0)

        # dept shot info
        self.deptInfoGroup = rbwUI.RBWGroupBox(title='Dept Info:', layout='V', fontSize=12)

        self.deptComboBox = rbwUI.RBWComboBox('Dept', bgColor='transparent')
        self.deptTypeComboBox = rbwUI.RBWComboBox('Type', bgColor='transparent')
        self.deptItemNameComboBox = rbwUI.RBWComboBox('Item Name', bgColor='transparent')

        self.deptTypeComboBox.hide()
        self.deptItemNameComboBox.hide()

        self.deptComboBox.activated.connect(self.setDept)
        self.deptTypeComboBox.activated.connect(self.fillDeptItemNameComboBox)
        self.deptItemNameComboBox.activated.connect(self.fillSavedShotTree)

        self.deptInfoGroup.addWidget(self.deptComboBox)
        self.deptInfoGroup.addWidget(self.deptTypeComboBox)
        self.deptInfoGroup.addWidget(self.deptItemNameComboBox)

        self.infoLayout.addWidget(self.deptInfoGroup, 1, 0)

        # save info
        self.saveInfoGroup = rbwUI.RBWGroupBox(title='Save Info:', layout='F', fontSize=12)
        self.saveInfoGroup.layout.setRowWrapPolicy(QtWidgets.QFormLayout.DontWrapRows)
        self.saveInfoGroup.layout.setLabelAlignment(QtCore.Qt.AlignLeft)

        self.saveNotesLabel = rbwUI.RBWLabel(text='Notes', italic=False, size=12)
        self.saveNotesTextEdit = QtWidgets.QTextEdit()
        self.saveNotesTextEdit.setObjectName('saveNotesTextEdit')
        self.savePromotedLabel = rbwUI.RBWLabel(text='Save Promoted', italic=False, size=12)
        self.savePromotedSwitch = rbwUI.RBWSwitch()

        self.savePromotedLabel.hide()
        self.savePromotedSwitch.hide()

        self.saveInfoGroup.addRow(self.saveNotesLabel, self.saveNotesTextEdit)
        self.saveInfoGroup.addRow(self.savePromotedLabel, self.savePromotedSwitch)

        self.infoLayout.addWidget(self.saveInfoGroup, 0, 1, 2, 1)

        self.topWidget.addLayout(self.infoLayout)

        self.verticalSplitter.addWidget(self.topWidget)

        ########################################################################
        # BOTTOM WIDGET
        self.bottomWidget = rbwUI.RBWFrame(layout='V', radius=5, spacing=5)

        # ------------------------------ shots tree ----------------------------- #
        self.savedShotsTree = rbwUI.RBWTreeWidget(fields=['System', 'Version', 'User', 'Date', 'Note', 'Path'])

        self.bottomWidget.addWidget(self.savedShotsTree)

        # -------------------------------- buttons ------------------------------- #
        self.buttonsWidget = rbwUI.RBWFrame(layout='H', margins=[0, 0, 0, 0], bgColor='transparent')
        self.loadButton = rbwUI.RBWButton(icon=['load.png'], color='rgba(30, 30, 30, 150)', size=[200, 35], text='Load')
        self.mergeButton = rbwUI.RBWButton(icon=['merge.png'], size=[200, 35], text='Merge')
        self.saveButton = rbwUI.RBWButton(icon=['save.png'], size=[200, 35], text='Save')

        self.loadButton.clicked.connect(functools.partial(self.load, 'load'))
        self.mergeButton.clicked.connect(functools.partial(self.load, 'merge'))
        self.saveButton.clicked.connect(self.save)

        self.buttonsWidget.addWidget(self.loadButton)
        self.buttonsWidget.addWidget(self.mergeButton)
        self.buttonsWidget.addWidget(self.saveButton)

        self.bottomWidget.addWidget(self.buttonsWidget, alignment=QtCore.Qt.AlignBottom)

        self.verticalSplitter.addWidget(self.bottomWidget)

        self.verticalSplitter.setStretchFactor(0, 1)
        self.verticalSplitter.setStretchFactor(1, 2)

        self.autoFillCombo()
        self.setMinimumSize(632, 630)

        self.setTitle('Load and Save Shot UI')
        self.setIcon('../arakno/loadAndSave_small.png')
        self.addStatusBar()
        self.setFocus()

    ############################################################################
    # @brief      fill the shot season combo box.
    #
    def fillShotSeasonComboBox(self):
        self.shotSeasonComboBox.clear()
        seasons = sorted(self.shots.keys())

        self.shotSeasonComboBox.addItems(seasons)

        if len(seasons) == 1:
            self.shotSeasonComboBox.setCurrentIndex(0)
            self.fillShotEpisodeComboBox()

    ############################################################################
    # @brief      fill the episode combo box.
    #
    def fillShotEpisodeComboBox(self):
        self.currentSeason = self.shotSeasonComboBox.currentText()

        self.shotEpisodeComboBox.clear()
        episodes = sorted(self.shots[self.currentSeason].keys())

        self.shotEpisodeComboBox.addItems(episodes)

        if len(episodes) == 1:
            self.shotEpisodeComboBox.setCurrentIndex(0)
            self.fillShotSequenceComboBox()

    ############################################################################
    # @brief      fill the sequence combo box.
    #
    def fillShotSequenceComboBox(self):
        self.currentEpisode = self.shotEpisodeComboBox.currentText()

        self.shotSequenceComboBox.clear()
        sequences = sorted(self.shots[self.currentSeason][self.currentEpisode].keys())

        self.shotSequenceComboBox.addItems(sequences)

        if len(sequences) == 1:
            self.shotSequenceComboBox.setCurrentIndex(0)
            self.fillShotShotComboBox()

    ############################################################################
    # @brief      fill the shot combo box.
    #
    def fillShotShotComboBox(self):
        self.currentSequence = self.shotSequenceComboBox.currentText()

        self.shotShotComboBox.clear()
        shots = sorted(self.shots[self.currentSeason][self.currentEpisode][self.currentSequence].keys())

        self.shotShotComboBox.addItems(shots)

        if len(shots) == 1:
            self.shotShotComboBox.setCurrentIndex(0)
            self.setCurrentShot()

    ############################################################################
    # @brief      fill the dept combo box.
    #
    def fillDeptComboBox(self):
        self.deptComboBox.clear()

        scDepts = [dept for dept in config.dictionaries.sc_depts.keys()]

        self.deptComboBox.addItems(scDepts)

    ############################################################################
    # @brief      fill the dept type combo box.
    #
    def fillDeptTypeComboBox(self):
        self.deptTypeComboBox.clear()

        subTypes = config.dictionaries.sc_depts[self.currentDept]

        self.deptTypeComboBox.addItems(subTypes)

        if len(subTypes) == 1:
            self.deptTypeComboBox.setCurrentIndex(0)

    ############################################################################
    # @brief      fill the item name combo box.
    #
    def fillDeptItemNameComboBox(self):
        self.deptItemNameComboBox.clear()
        shotDeptFolder = os.path.join(self.currentShotObj.getShotFolder(), self.currentDept).replace('\\', '/')

        if self.currentDept == 'Fin':
            self.currentDeptType = self.deptTypeComboBox.currentText()
            if os.path.exists(shotDeptFolder):
                if self.currentDeptType not in ['msh', 'vrscene']:
                    self.deptItemNameComboBox.hide()
                    itemNames = []
                else:
                    self.deptItemNameComboBox.show()
                    infoJson = os.path.join(shotDeptFolder, 'finishingInitInfos.json').replace('\\', '/')
                    if os.path.exists(infoJson):
                        data = api.json.json_read(infoJson)
                        itemNames = list(data.keys())
            else:
                itemNames = []

        elif self.currentDept == 'Fx':
            if os.path.exists(shotDeptFolder):
                itemNames = []
                finInfoJson = os.path.join(shotDeptFolder, '..', 'Fin', 'fxInitInfos.json').replace('\\', '/')
                if os.path.exists(finInfoJson):
                    fxNames = api.json.json_read(finInfoJson)
                    itemNames = itemNames + [fxName for fxName in fxNames if fxNames[fxName]['surfSavedAsset']]
                itemFromSgJson = os.path.join(shotDeptFolder, 'taskNames.json').replace('\\', '/')
                if os.path.exists(itemFromSgJson):
                    itemNames = itemNames + list(api.json.json_read(itemFromSgJson)['taskNames'])
            else:
                itemNames = []

        else:
            if os.path.exists(shotDeptFolder):
                infoJson = os.path.join(shotDeptFolder, 'envInfos.json').replace('\\', '/')
                if os.path.exists(infoJson):
                    data = api.json.json_read(infoJson)
                    itemNames = list(data.keys())
            else:
                itemNames = []

        if not itemNames:
            self.fillSavedShotTree()

        self.deptItemNameComboBox.addItems(itemNames)

        if len(itemNames) == 1:
            self.deptItemNameComboBox.setCurrentIndex(0)

    ############################################################################
    # @brief      auto fill of all the combos.
    #
    def autoFillCombo(self):
        fileName = api.scene.scenename()
        if self.animationFolder in fileName:
            try:
                relPath = fileName.split(self.animationFolder)[1]
                season = relPath.split('/')[1]
                episode = relPath.split('/')[2]
                sequence = relPath.split('/')[3]
                shot = relPath.split('/')[4]

                self.fillShotSeasonComboBox()
                seasonIndex = self.shotSeasonComboBox.findText(season, QtCore.Qt.MatchFixedString)
                self.shotSeasonComboBox.setCurrentIndex(seasonIndex)

                self.fillShotEpisodeComboBox()
                episodeIndex = self.shotEpisodeComboBox.findText(episode, QtCore.Qt.MatchFixedString)
                self.shotEpisodeComboBox.setCurrentIndex(episodeIndex)

                self.fillShotSequenceComboBox()
                sequenceIndex = self.shotSequenceComboBox.findText(sequence, QtCore.Qt.MatchFixedString)
                self.shotSequenceComboBox.setCurrentIndex(sequenceIndex)

                self.fillShotShotComboBox()
                shotIndex = self.shotShotComboBox.findText(shot, QtCore.Qt.MatchFixedString)
                self.shotShotComboBox.setCurrentIndex(shotIndex)
                self.setCurrentShot()
                self.fillDeptComboBox()

            except:
                self.fillShotSeasonComboBox()
                self.fillDeptComboBox()
        else:
            self.fillShotSeasonComboBox()
            self.fillDeptComboBox()

    ############################################################################
    # @brief      search the give shot.
    #
    def searchEntry(self):
        wantedEntryName = self.searchLineEdit.text()
        if wantedEntryName == '':
            self.updateResultList()
        else:
            searchAssetFromNameQuery = "SELECT DISTINCT `season`, `episode`, `sequence`, `shot` FROM `V_shotList` WHERE `projectId`={} AND `shot` LIKE '%{}%'".format(
                self.id_mv,
                wantedEntryName
            )

            results = api.database.selectQuery(searchAssetFromNameQuery)
            if results:
                self.updateResultList(results)

    ############################################################################
    # @brief      update the list result.
    #
    # @param      queryResults  The query results
    #
    def updateResultList(self, queryResults=None):
        self.searchResultList.clear()
        if queryResults:
            self.searchResultList.show()
            for infoTupla in queryResults:
                item = QtWidgets.QListWidgetItem('{} - {} - {} - {}'.format(infoTupla[0], infoTupla[1], infoTupla[2], infoTupla[3]))
                self.searchResultList.addItem(item)
        else:
            self.searchResultList.hide()

    ############################################################################
    # @brief      update the combo to match the selected shot.
    #
    def matchShot(self):
        wantedSeason, wantedEpisode, wantedSequence, wantedShot = self.searchResultList.currentItem().text().split(' - ')

        self.fillShotSeasonComboBox()
        seasonIndex = self.shotSeasonComboBox.findText(wantedSeason, QtCore.Qt.MatchFixedString)
        self.shotSeasonComboBox.setCurrentIndex(seasonIndex)

        self.fillShotEpisodeComboBox()
        epipsodeIndex = self.shotEpisodeComboBox.findText(wantedEpisode, QtCore.Qt.MatchFixedString)
        self.shotEpisodeComboBox.setCurrentIndex(epipsodeIndex)

        self.fillShotSequenceComboBox()
        sequenceIndex = self.shotSequenceComboBox.findText(wantedSequence, QtCore.Qt.MatchFixedString)
        self.shotSequenceComboBox.setCurrentIndex(sequenceIndex)

        self.fillShotShotComboBox()
        shotIndex = self.shotShotComboBox.findText(wantedShot, QtCore.Qt.MatchFixedString)
        self.shotShotComboBox.setCurrentIndex(shotIndex)

        self.setCurrentShot()

    ############################################################################
    # @brief      Sets the current shot.
    #
    def setCurrentShot(self):
        self.currentShot = self.shotShotComboBox.currentText()

        self.currentShotObj = api.shot.Shot({
            'season': self.currentSeason,
            'episode': self.currentEpisode,
            'sequence': self.currentSequence,
            'shot': self.currentShot
        })

    ############################################################################
    # @brief      Sets the dept.
    #
    def setDept(self):
        self.currentDept = self.deptComboBox.currentText()

        if self.currentDept not in ["Fin", "Fx", "Env"]:
            self.deptTypeComboBox.hide()
            self.deptItemNameComboBox.hide()

            if hasattr(self, 'currentDeptType'):
                delattr(self, 'currentDeptType')

            if hasattr(self, 'currentItemName'):
                delattr(self, 'currentItemName')

            if self.currentDept in ["Lay", "Pri", "Sec", "Lgt"]:
                self.savePromotedLabel.show()
                self.savePromotedSwitch.show()
            else:
                self.savePromotedLabel.hide()
                self.savePromotedSwitch.hide()

            self.fillSavedShotTree()
        else:
            if self.currentDept == "Fin":

                self.savePromotedLabel.hide()
                self.savePromotedSwitch.hide()
                self.deptTypeComboBox.show()
                self.deptItemNameComboBox.hide()

                self.fillDeptTypeComboBox()
            else:
                self.savePromotedLabel.hide()
                self.savePromotedSwitch.hide()
                self.deptTypeComboBox.hide()
                self.deptItemNameComboBox.show()

                self.fillDeptItemNameComboBox()

    ############################################################################
    # @brief      Gets the saved shots.
    #
    # @return     The saved shots.
    #
    def getSavedShots(self):
        # 'System', 'Version', 'User', 'Date', 'Note', 'Path'
        if self.currentDept in ['Fin', 'Fx', 'Env']:
            self.currentItemName = self.deptItemNameComboBox.currentText()

        if hasattr(self, 'currentDeptType') and hasattr(self, 'currentItemName'):
            versionsQuery = "SELECT `system`, `version`, `user`, `date`, `note`, `path` FROM `savedShot` WHERE `shotId`='{}' AND `dept`='{}' AND `deptType`='{}' AND `itemName`='{}' AND `softwareVersion` = 'maya{}' ORDER BY `date` DESC".format(
                self.currentShotObj.shotId,
                self.currentDept,
                self.currentDeptType,
                self.currentItemName,
                cmd.about(v=True)
            )

        elif hasattr(self, 'currentItemName'):
            versionsQuery = "SELECT `system`, `version`, `user`, `date`, `note`, `path` FROM `savedShot` WHERE `shotId`='{}' AND `dept`='{}' AND `itemName`='{}' AND `softwareVersion` = 'maya{}' ORDER BY `date` DESC".format(
                self.currentShotObj.shotId,
                self.currentDept,
                self.currentItemName,
                cmd.about(v=True)
            )

        elif hasattr(self, 'currentDeptType'):
            versionsQuery = "SELECT `system`, `version`, `user`, `date`, `note`, `path` FROM `savedShot` WHERE `shotId`='{}' AND `dept`='{}' AND `deptType`='{}' AND `softwareVersion` = 'maya{}' ORDER BY `date` DESC".format(
                self.currentShotObj.shotId,
                self.currentDept,
                self.currentDeptType,
                cmd.about(v=True)
            )

        else:
            versionsQuery = "SELECT `system`, `version`, `user`, `date`, `note`, `path` FROM `savedShot` WHERE `shotId`='{}' AND `dept`='{}' AND `softwareVersion` = 'maya{}' ORDER BY `date` DESC".format(
                self.currentShotObj.shotId,
                self.currentDept,
                cmd.about(v=True)
            )

        api.log.logger().debug('versionsQuery: {}'.format(versionsQuery))

        return api.database.selectQuery(versionsQuery)

    ############################################################################
    # @brief      fill the shot tree.
    #
    def fillSavedShotTree(self):
        api.log.logger().debug('fillSavedShotTree')
        self.savedShotsTree.clear()

        saves = self.getSavedShots()

        for save in saves:
            child = QtWidgets.QTreeWidgetItem(self.savedShotsTree)
            for i in range(0, 6):
                child.setText(i, str(save[i]))
                if save[0] == 'SPR':
                    child.setForeground(i, QtGui.QBrush(QtGui.QColor("yellow")))

    ############################################################################
    # @brief      load function.
    #
    # @param      mode  The mode
    #
    def load(self, mode='load'):
        if not self.checkToLoad():
            return
        currentSavedShotItem = self.savedShotsTree.currentItem()
        relativePath = currentSavedShotItem.text(5)

        currentSavedShot = api.savedShot.SavedShot(params={'path': relativePath})
        if hasattr(self, 'currentItemName'):
            if self.currentDept == 'Fx' and '_' not in self.currentItemName and mode == 'merge':
                mode = 'reference'
        currentSavedShot.load(mode)

    ############################################################################
    # @brief      save function.
    #
    def save(self):
        if self.checkToSave():
            savedShotParam = {
                'shot': self.currentShotObj,
                'dept': self.currentDept,
                'note': self.saveNotesTextEdit.toPlainText(),
                'promoted': 1 if self.savePromotedSwitch.isChecked() else 0
            }

            savedShotParam['deptType'] = self.currentDeptType if hasattr(self, 'currentDeptType') else ''
            savedShotParam['itemName'] = self.currentItemName if hasattr(self, 'currentItemName') else ''

            savedShot = api.savedShot.SavedShot(params=savedShotParam)
            success = savedShot.save()

            if success:
                rbwUI.RBWDialog(title='Save {} Complete'.format(self.currentDept), text='Shot Save Complete', parent=self)
                self.fillSavedShotTree()

    ############################################################################
    # @brief      { function_description }
    #
    # @return     { description_of_the_return_value }
    #
    def checkToLoad(self):
        if self.currentDept == 'Lgt':
            if not api.vray.checkVray():
                message = 'Sorry but vray is not loaded as plug-in or is not set as active rendering engine.\nPlease check it and try again.'
                rbwUI.RBWError(text=message)
                return False
        return True

    ############################################################################
    # @brief      { function_description }
    #
    # @return     { description_of_the_return_value }
    #
    def checkToSave(self):
        if self.currentDept in ['Lay', 'Pri', 'Sec']:
            camera = api.camera.getSceneCamera()
            if not camera:
                if self.savePromotedSwitch.isChecked():
                    rbwUI.RBWError(text='No pipeline camera in scene.\nSave is abort')
                    return False
                else:
                    rbwUI.RBWWarning(text='No pipeline camera in scene.\nThe save it\'s possible except for promoted ones for pipeline reasons..', defCancel=None)
                    return True
        elif self.currentDept == 'Fx':
            if not hasattr(self, 'currentItemName'):
                rbwUI.RBWError(text='You have to select an Fx name.\nSave is abort')
                return False
        return True
