import importlib
import os
import shutil

import maya.cmds as cmd

# import api.mail as rmail
import api.log

importlib.reload(api.log)


class cleanupMayaScriptJobs():
    ''' Fix for the vaccine.py worm infecting maya files'''

    def delete_worm_nodes(self):
        # type: () -> None
        nodes = ['*breed_gene*', '*vaccine_gene*', '*sysytenasdasdfsadfsdaf_dsfsdfaasd*', '*PuTianTongQing*', '*daxunhuan*']
        deleted = []
        for node in nodes:
            try:
                cmd.delete(node)
                deleted.append(node)
            except:
                pass

        if len(deleted):
            api.log.logger().debug('Some nodes deleted: {}'.format(len(deleted)))
            # rmail.sendRbwLogMail("SOSPICIOUS NODES FOUND AND REMOVED!\n\n\n" + str(cmd.file(q=1, sceneName=1)))
            self.cleanup_maya_scripts()

    def cleanup_maya_scripts(self):
        # type: () -> None
        usd = cmd.internalVar(usd=True)
        usd = '{}/maya/scripts'.format(usd.split('maya')[0])
        maya_path = '{}scripts'.format(cmd.internalVar(userAppDir=True))
        if os.path.exists(maya_path):
            if "__pycache__" in os.listdir(usd):
                self.delete_worm_files(usd)

    def delete_worm_files(self, path):
        files = ["__pycache__"]
        deleted = []
        for file in files:
            file_path = os.path.join(path, file).replace('\\', '/')
            try:
                shutil.rmtree(file_path)
                deleted.append(file_path)
                api.log.logger().debug('deleted file {}'.format(file_path))
            except:
                pass

        if deleted:
            # rmail.sendRbwLogMail("SOSPICIOUS PYTHON FOUND AND REMOVED!\n\n\n" + str(deleted))
            pass
