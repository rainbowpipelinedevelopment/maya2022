import importlib
import os

import maya.cmds as cmd
import maya.mel as mel

import api.database
import api.savedAsset
import api.savedShot
import api.exception
import api.scene
import api.widgets.rbw_UI as rbwUI
import depts.setdressing.tools.finalize.check as checkSet

importlib.reload(api.database)
importlib.reload(api.scene)
# importlib.reload(api.savedAsset)
# importlib.reload(api.savedShot)
importlib.reload(api.exception)
# importlib.reload(rbwUI)
importlib.reload(checkSet)


depts = {
    'lookdev': 'surfacing',
    'animation': 'rigging',
    'finishing': 'rigging',
    'lighting': 'surfacing'
}


################################################################################
# @brief      list references
#
# @param      dept  The dept
#
# @return     a dict with reference infos.
#
def listReferences(dept):
    nodeType = depts[dept]
    referencesSaveNodes = api.scene.getSaveNodes(dept=nodeType)
    referencesSaveNodes = referencesSaveNodes + api.scene.getSaveNodes(dept='fx')

    updated = {}
    outOfDate = {}

    turnOff = []

    for saveNode in referencesSaveNodes:
        if cmd.listConnections(saveNode) is not None:
            if cmd.getAttr('{}.approvation'.format(saveNode)) == 'def':
                if cmd.referenceQuery(saveNode, isNodeReferenced=True):
                    try:
                        node = cmd.referenceQuery(saveNode, referenceNode=True)
                        topGroup = cmd.listConnections(saveNode, source=False, destination=True)[0]
                        savedAsset = api.savedAsset.SavedAsset(saveNode=saveNode)
                        namespace = cmd.referenceQuery(node, namespace=True)
                        dic = {'node': node, 'savedAsset': savedAsset, 'namespace': namespace, 'topGroup': topGroup}
                        if dic['savedAsset'].version < dic['savedAsset'].getAvailableVersions()[0][3]:
                            outOfDate[saveNode] = dic
                        else:
                            updated[saveNode] = dic
                    except api.exception.InvalidSavedAsset:
                        turnOff.append(cmd.referenceQuery(saveNode, referenceNode=True))
    if len(turnOff) > 0:
        message = 'Those references file load a suppressed save file, please inform your lead to understand how to procede:'
        for item in turnOff:
            message = '{}\n- {}'.format(message, item)
        rbwUI.RBWWarning(text=message, defCancel=None, resizable=True)

    return {**outOfDate, **updated}


################################################################################
# @brief      update reference node.
#
# @param      node       The node
# @param      path       The path
# @param      sceneType  The scene type
#
def updateReference(node, path, sceneType):
    cmd.file(path, loadReference=node, type=sceneType)
    mel.eval('generateAllUvTilePreviews')
    mel.eval('setAttr "hardwareRenderingGlobals.alphaCutPrepass" 1;')


################################################################################
# @brief      update cache node.
#
# @param      topGroup  The top grpup
# @param      cacheDir  The cache dir
#
def updateCache(topGroup, cacheDir):
    namespace = topGroup.split(':')[0]
    proxies = cmd.ls('{}:MSH_*'.format(namespace), type='VRayProxy')

    for proxy in proxies:
        fileName = os.path.basename(cmd.getAttr('{}.fileName'.format(proxy)))
        newFileName = os.path.join(cacheDir, fileName).replace('\\', '/')
        if os.path.exists(newFileName):
            cmd.setAttr('{}.fileName'.format(proxy), newFileName, type='string')

            shaderOverrides = cmd.vrayUpdateProxy(proxy, getShaderSets=True)
            for i in range(0, len(shaderOverrides)):
                shaderOverrideString = cmd.getAttr('{}.shaders[{}].shadersNames'.format(proxy, i))
                if shaderOverrideString != shaderOverrides[i]:
                    cmd.setAttr('{}.shaders[{}].shadersNames'.format(proxy, i), shaderOverrides[i], type='string')

    rivetsGroup = '{}|RIVETS'.format(topGroup)
    if cmd.objExists(rivetsGroup):
        cmd.delete(rivetsGroup)

    itemName = namespace[:-3]
    newRivetFile = os.path.join(cacheDir, '{}_rivets.ma'.format(itemName)).replace('\\', '/')
    rivets = api.scene.mergeAsset(newRivetFile)

    cmd.parent(rivets, topGroup)


################################################################################
# @brief      list scene sets.
#
# @return     the sets
#
def listSets():
    setSaveNodes = api.scene.getSaveNodes(dept='set')

    updated = {}
    outOfDate = {}

    turnOff = []
    for setSaveNode in setSaveNodes:
        try:
            node = cmd.listConnections(setSaveNode, destination=True, source=False)[0]
            savedAsset = api.savedAsset.SavedAsset(saveNode=setSaveNode)
            dic = {'node': node, 'savedAsset': savedAsset}
            if dic['savedAsset'].version < dic['savedAsset'].getAvailableVersions()[0][3]:
                outOfDate[setSaveNode] = dic
            else:
                updated[setSaveNode] = dic
        except api.exception.InvalidSavedAsset:
            turnOff.append(cmd.listConnections(setSaveNode, destination=True, source=False)[0])

    if len(turnOff) > 0:
        message = 'Those sets in scene refers to suppressed save file, please inform your lead to understand how to procede:'
        for item in turnOff:
            message = '{}\n- {}'.format(message, item)
        rbwUI.RBWWarning(text=message, defCancel=None, resizable=True)

    return {**outOfDate, **updated}


################################################################################
# @brief      update the given set in scene.
#
# @param      setNode  The name of the set in the scene outliner
#
def updateSet(setNode):
    saveNode = cmd.listConnections(setNode)[0]
    savedAsset = api.savedAsset.SavedAsset(saveNode=saveNode)
    latest = savedAsset.getLatest()

    if int(latest[3].split('vr')[1]) != int(savedAsset.version.split('vr')[1]):

        cmd.delete(setNode)
        api.scene.addToSetGroup(latest[2])

    checkSet.checkDuplicate()


################################################################################
# @brief      list assemblies
#
# @return     a dict with assemblies infos.
#
def listAssemblies():
    assemblies = {}

    updated = {}
    outOfDate = {}
    turnOffAssets = {}

    for assembly in cmd.ls(type='assemblyReference'):
        assemblyPath = cmd.getAttr('{}.definition'.format(assembly))
        assetFullName = '_'.join(assemblyPath.split('/')[8:12])
        if assetFullName not in assemblies:
            assemblies[assetFullName] = [assembly]
        else:
            assemblies[assetFullName].append(assembly)

    for name in assemblies:
        currentVersion = getAssemblyVersion(assemblies[name])
        availableVersions = getAssemblyAvailableVersions(assemblies[name])
        if availableVersions:
            if currentVersion < availableVersions[0]:
                outOfDate[name] = {'nodes': assemblies[name], 'current': currentVersion, 'available': availableVersions}
            else:
                updated[name] = {'nodes': assemblies[name], 'current': currentVersion, 'available': availableVersions}
        else:
            turnOffAssets[name] = {'nodes': assemblies[name], 'current': currentVersion}

    return {**outOfDate, **updated, **turnOffAssets}


############################################################################
# @brief      Gets the assembly version.
#
# @return     The assembly version.
#
def getAssemblyVersion(nodes):
    versions = []
    for node in nodes:
        definitionPath = cmd.getAttr('{}.definition'.format(node))
        version = definitionPath.split('/')[13]
        if version not in versions:
            versions.append(version)
    versions.sort()
    return versions[0]


############################################################################
# @brief      Gets the assembly available versions.
#
# @return     The assembly available versions.
#
def getAssemblyAvailableVersions(nodes):
    availableVersions = []

    availableVersionsQuery = "SELECT a.version FROM `assembly` AS a JOIN `V_assetList` AS va ON a.assetId=va.variantID WHERE a.path LIKE '%{}%' AND va.projectID={} ORDER BY a.date DESC".format('/'.join(cmd.getAttr('{}.definition'.format(nodes[0])).split('/')[7:-2]), os.getenv('ID_MV'))
    results = api.database.selectQuery(availableVersionsQuery)

    for result in results:
        availableVersions.append(result[0])

    availableVersions.sort(reverse=True)
    return availableVersions


################################################################################
# @brief      update the given assembly with given path.
#
# @param      node  The node
# @param      path  The path
#
def updateAssembly(node, path):
    cmd.setAttr('{}.definition'.format(node), path, type='string')


################################################################################
# @brief      update all assemblis without the UI.
#
def updateAllAssembliesBatch():
    assemblies = cmd.ls(type='assemblyReference')
    for assembly in assemblies:
        definition = '/'.join(cmd.getAttr('{}.definition'.format(assembly)).split('/')[7:])
        definitionForQuery = '/'.join(cmd.getAttr('{}.definition'.format(assembly)).split('/')[7:-2])
        latestAssemblyPathQuery = "SELECT a.path FROM `assembly` AS a JOIN `V_assetList` AS va ON a.assetId=va.variantID WHERE a.path LIKE '%{}%' AND va.projectID={} ORDER BY a.date DESC".format(definitionForQuery, os.getenv('ID_MV'))
        latestAssemblyPath = api.database.selectSingleQuery(latestAssemblyPathQuery)[0]
        if latestAssemblyPath != definition:
            updateAssembly(assembly, latestAssemblyPath)


################################################################################
# @brief      list the scene vrscenes
#
# @return     the vrscenes
#
def listVrscenes():
    updated = {}
    outOfDate = {}

    if cmd.objExists('VRSCENE'):
        for vraySceneTransform in cmd.listRelatives('VRSCENE'):
            vrayScene = vraySceneTransform.replace('_Mesh', '_Shape')
            vrscenePath = cmd.getAttr('{}.FilePath'.format(vrayScene)).split('/mc_{}/'.format(os.getenv('PROJECT')))[1]

            dic = {'node': vraySceneTransform, 'savedShot': api.savedShot.SavedShot(params={'path': '{}.ma'.format(vrscenePath[:-13])})}
            if dic['savedShot'].version < dic['savedShot'].getAvailableVersions()[0][3]:
                outOfDate[vraySceneTransform] = dic
            else:
                updated[vraySceneTransform] = dic

    return {**outOfDate, **updated}


################################################################################
# @brief      update vrscene.
#
# @param      node  The vrscene node
# @param      newPath      The new path
#
def updateVrscene(node, newPath):
    nodevrsceneShape = node.replace('_Mesh', '_Shape')
    cmd.setAttr('{}.FilePath'.format(nodevrsceneShape), newPath, type='string')

    vrsceneExpression = node.replace('_Mesh', '_expression')

    oldExpressionText = cmd.expression(vrsceneExpression, query=True, string=True)
    oldPathString = oldExpressionText.split('( "')[1].split('" +')[0]

    newPathString = '_'.join(newPath.split('_')[:-1])
    newExpressionText = oldExpressionText.replace(oldPathString, '{}_'.format(newPathString))

    cmd.expression(vrsceneExpression, edit=True, string=newExpressionText)


################################################################################
# @brief      list the scene shot fxs.
#
# @return     the shot fxs
#
def listShotFxs():
    updated = {}
    outOfDate = {}

    for shotFxReference in cmd.ls('*_hou_FX*', type='reference'):
        path = cmd.referenceQuery(shotFxReference, filename=True, withoutCopyNumber=True).split(os.getenv('MC_FOLDER'))[1][1:]
        api.log.logger().debug(path)

        dic = {'node': shotFxReference, 'savedShot': api.savedShot.SavedShot(params={'path': path})}
        if dic['savedShot'].version < dic['savedShot'].getAvailableVersions()[0][3]:
            outOfDate[shotFxReference] = dic
        else:
            updated[shotFxReference] = dic

    return {**outOfDate, **updated}
