import importlib
from PySide2 import QtWidgets
import os

import maya.cmds as cmd

import api.log
import api.widgets.rbw_UI as rbwUI
import common.checkUpdate.checkUpdate as checkUpdate

importlib.reload(api.log)
# importlib.reload(rbwUI)
importlib.reload(checkUpdate)


################################################################################
# @brief      This class describes a msh tree widget item.
#
class MshTreeWidgetItem(QtWidgets.QTreeWidgetItem):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      savedAsset  The saved asset
    #
    def __init__(self, savedAsset, parent):
        super().__init__(parent)

        self.node = savedAsset['node']
        self.currentRefVersion = savedAsset['savedAsset'].version
        self.availableRefVersions = savedAsset['savedAsset'].getAvailableVersions()
        self.dept = savedAsset['savedAsset'].dept
        self.namespace = savedAsset['namespace'][1:]
        self.topGroup = savedAsset['topGroup']

        proxies = cmd.ls('{}:MSH*'.format(self.namespace), type='VRayProxy')
        path = cmd.getAttr('{}.fileName'.format(proxies[0]))
        self.cacheDir = '/'.join(path.split('/')[:-2])
        self.availableCacheVersions = [dir for dir in os.listdir(self.cacheDir) if dir.startswith('vr')]
        self.availableCacheVersions.sort(reverse=True)

        self.currentCacheVersion = path.split('/')[-2]

        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setText(0, self.node)
        self.setText(1, self.currentRefVersion)
        self.setText(5, self.availableRefVersions[0][9])
        self.setText(3, self.currentCacheVersion)

        self.availableRefVersionsCombobox = rbwUI.RBWComboBox(bgColor='transparent', fieldColor='rgba(20, 20, 20, 150)')
        self.availableRefVersionsCombobox.activated.connect(self.changeNote)

        self.treeWidget().setItemWidget(self, 2, self.availableRefVersionsCombobox)

        self.fillAvailableRefCombobox()

        self.availableCacheVersionsCombobox = rbwUI.RBWComboBox(bgColor='transparent', fieldColor='rgba(20, 20, 20, 150)')

        self.treeWidget().setItemWidget(self, 4, self.availableCacheVersionsCombobox)

        self.fillAvailableCacheCombobox()

        if self.currentRefVersion < self.availableRefVersions[0][3]:
            referenceStatus = False
        else:
            referenceStatus = True

        if self.currentCacheVersion < self.availableCacheVersions[0]:
            cacheStatus = False
        else:
            cacheStatus = True

        if not referenceStatus and cacheStatus:
            self.status = 'outOfDate'
            self.mode = 1
        elif referenceStatus and not cacheStatus:
            self.status = 'outOfDate'
            self.mode = 2
        elif not referenceStatus and not cacheStatus:
            self.status = 'outOfDate'
            self.mode = 3
        else:
            self.status = 'update'
            self.mode = None

        if not self.mode:
            self.updateButton = rbwUI.RBWButton(text='UPDATED', color='rgba(0, 255, 0, 150)', hover='rgba(105, 138, 105, 150)', icon=['refresh.png'], size=[130, 25])
        else:
            if self.mode == 1:
                text = 'REFERENCE'
            elif self.mode == 2:
                text = 'CACHE'
            else:
                text = 'OUT OF DATE'

            self.updateButton = rbwUI.RBWButton(text=text, color='rgba(255, 0, 0, 150)', hover='rgba(250, 104, 104, 150)', icon=['refresh.png'], size=[130, 25])
        self.updateButton.clicked.connect(self.update)
        self.treeWidget().setItemWidget(self, 6, self.updateButton)

    ############################################################################
    # @brief      fill available ref versions combobox.
    #
    def fillAvailableRefCombobox(self):
        self.availableRefVersionsCombobox.clear()

        for version in self.availableRefVersions:
            self.availableRefVersionsCombobox.addItem(version[3])

    ############################################################################
    # @brief      fill available cache versions combobox.
    #
    def fillAvailableCacheCombobox(self):
        self.availableCacheVersionsCombobox.clear()

        for version in self.availableCacheVersions:
            self.availableCacheVersionsCombobox.addItem(version)

    ############################################################################
    # @brief      change note function.
    #
    def changeNote(self):
        self.setText(5, self.availableRefVersions[self.availableRefVersionsCombobox.currentIndex()][9])

    ############################################################################
    # @brief      update reference and cache function.
    #
    def update(self):
        update = False

        if self.currentRefVersion != self.availableRefVersionsCombobox.currentText():
            newPath = self.availableRefVersions[self.availableRefVersionsCombobox.currentIndex()][2].replace('.mb', '_vrmesh.ma')
            checkUpdate.updateReference(self.node, newPath, 'mayaAscii')
            self.currentRefVersion = self.availableRefVersionsCombobox.currentText()
            update = True

        if self.currentCacheVersion != self.availableCacheVersionsCombobox.currentText():
            newCacheDir = os.path.join(self.cacheDir, self.availableCacheVersionsCombobox.currentText()).replace('\\', '/')
            checkUpdate.updateCache(self.topGroup, newCacheDir)
            self.currentCacheVersion = self.availableCacheVersionsCombobox.currentText()
            update = True

        if update:
            self.initUI()


################################################################################
# @brief      This class describes a vrscene tree widget item.
#
class VrsceneTreeWidgetItem(QtWidgets.QTreeWidgetItem):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      savedAsset  The saved shot
    #
    def __init__(self, savedShot, parent):
        super().__init__(parent)

        self.node = savedShot['node']
        self.currentVersion = savedShot['savedShot'].version
        self.availableVersions = savedShot['savedShot'].getAvailableVersions()
        self.dept = savedShot['savedShot'].dept
        self.deptType = savedShot['savedShot'].deptType

        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setText(0, self.node)
        self.setText(1, self.currentVersion)

        self.availableVersionsCombobox = rbwUI.RBWComboBox(bgColor='transparent', fieldColor='rgba(20, 20, 20, 150)')

        self.treeWidget().setItemWidget(self, 2, self.availableVersionsCombobox)

        self.fillAvailableCombobox()

        if self.currentVersion < self.availableVersions[0][3]:
            self.status = 'outOfDate'
            self.updateButton = rbwUI.RBWButton(text='OUT OF DATE', color='rgba(255, 0, 0, 150)', hover='rgba(250, 104, 104, 150)', icon=['refresh.png'], size=[130, 25])
        else:
            self.status = 'updated'
            self.updateButton = rbwUI.RBWButton(text='UPDATED', color='rgba(0, 255, 0, 150)', hover='rgba(105, 138, 105, 150)', icon=['refresh.png'], size=[130, 25])
        self.updateButton.clicked.connect(self.update)
        self.treeWidget().setItemWidget(self, 3, self.updateButton)

    ############################################################################
    # @brief      fill available versions combobox.
    #
    def fillAvailableCombobox(self):
        self.availableVersionsCombobox.clear()

        for version in self.availableVersions:
            self.availableVersionsCombobox.addItem(version[3])

    ############################################################################
    # @brief      update reference function.
    #
    def update(self):
        if self.currentVersion != self.availableVersionsCombobox.currentText():
            newPath = self.availableVersions[self.availableVersionsCombobox.currentIndex()][2].replace('.ma', '_0001.vrscene')

            checkUpdate.updateVrscene(self.node, newPath)

            self.currentVersion = self.availableVersionsCombobox.currentText()
            self.status = 'updated'
            self.initUI()
        else:
            pass


################################################################################
# @brief      This class describes a shot effects tree widget item.
#
class ShotFxTreeWidgetItem(VrsceneTreeWidgetItem):

    def update(self):
        if self.currentVersion != self.availableVersionsCombobox.currentText():
            newPath = self.availableVersions[self.availableVersionsCombobox.currentIndex()][2]

            checkUpdate.updateReference(self.node, newPath, 'mayaAscii')

            self.currentVersion = self.availableVersionsCombobox.currentText()
            self.status = 'updated'
            self.initUI()
        else:
            pass
