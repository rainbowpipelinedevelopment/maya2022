import importlib
from PySide2 import QtWidgets

import maya.cmds as cmd

import api.log
import api.widgets.rbw_UI as rbwUI
import common.checkUpdate.checkUpdate as checkUpdate

importlib.reload(api.log)
# importlib.reload(rbwUI)
importlib.reload(checkUpdate)


################################################################################
# @brief      This class describes a mc tree widget item.
#
class McTreeWidgetItem(QtWidgets.QTreeWidgetItem):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      savedAsset  The saved asset
    #
    def __init__(self, savedAsset, parent):
        super().__init__(parent)

        self.node = savedAsset['node']
        self.currentVersion = savedAsset['savedAsset'].version
        self.availableVersions = savedAsset['savedAsset'].getAvailableVersions()
        self.dept = savedAsset['savedAsset'].dept

        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setText(0, self.node)
        self.setText(1, self.currentVersion)
        self.setText(3, self.availableVersions[0][9])

        self.availableVersionsCombobox = rbwUI.RBWComboBox(bgColor='transparent', fieldColor='rgba(20, 20, 20, 150)')
        self.availableVersionsCombobox.activated.connect(self.changeNote)

        self.treeWidget().setItemWidget(self, 2, self.availableVersionsCombobox)

        self.fillAvailableCombobox()

        if self.currentVersion < self.availableVersions[0][3]:
            self.status = 'outOfDate'
            self.updateButton = rbwUI.RBWButton(text='OUT OF DATE', color='rgba(255, 0, 0, 150)', hover='rgba(250, 104, 104, 150)', icon=['refresh.png'], size=[130, 25])
        else:
            self.status = 'updated'
            self.updateButton = rbwUI.RBWButton(text='UPDATED', color='rgba(0, 255, 0, 150)', hover='rgba(105, 138, 105, 150)', icon=['refresh.png'], size=[130, 25])
        self.updateButton.clicked.connect(self.update)
        self.treeWidget().setItemWidget(self, 4, self.updateButton)

    ############################################################################
    # @brief      fill available versions combobox.
    #
    def fillAvailableCombobox(self):
        self.availableVersionsCombobox.clear()

        for version in self.availableVersions:
            self.availableVersionsCombobox.addItem(version[3])

    ############################################################################
    # @brief      change note function.
    #
    def changeNote(self):
        self.setText(3, self.availableVersions[self.availableVersionsCombobox.currentIndex()][9])

    ############################################################################
    # @brief      update reference function.
    #
    def update(self):
        if self.currentVersion != self.availableVersionsCombobox.currentText():
            newPath = self.availableVersions[self.availableVersionsCombobox.currentIndex()][2]

            if self.dept == 'set':
                checkUpdate.updateSet(self.node)
            else:
                checkUpdate.updateReference(self.node, newPath, 'mayaBinary')

            self.currentVersion = self.availableVersionsCombobox.currentText()
            self.status = 'updated'
            self.initUI()
        else:
            pass
