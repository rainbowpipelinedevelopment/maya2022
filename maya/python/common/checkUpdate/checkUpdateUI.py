import importlib
import os
from PySide2 import QtWidgets, QtCore, QtGui

import maya.cmds as cmd


import api.log
import common.checkUpdate.mcTreeWidgetItem as mcTreeWidgetItem
import common.checkUpdate.scTreeWidgetItem as scTreeWidgetItem
import common.checkUpdate.assemblyTreeWidgetItem as assemblyTreeWidgetItem
import common.checkUpdate.checkUpdate as checkUpdate
import api.widgets.rbw_UI as rbwUI

importlib.reload(api.log)
importlib.reload(mcTreeWidgetItem)
importlib.reload(scTreeWidgetItem)
importlib.reload(assemblyTreeWidgetItem)
importlib.reload(checkUpdate)
# importlib.reload(rbwUI)


availableItems = {
    'animation': ['References', 'Assemblies', 'Sets'],
    'finishing': ['References', 'Assemblies', 'Sets'],
    'lighting': ['ReferenceAndCaches', 'References', 'Assemblies', 'Vrscenes', 'ShotFxs'],
    'setdressing': ['Assemblies']
}

iconDir = os.path.join(os.getenv('LOCALPIPE'), 'img', 'icons', 'arakno')


################################################################################
# @brief      Class for check update ui
#
class CheckUpdateUI(rbwUI.RBWWindow):

    ############################################################################
    # @brief      Constructs the object
    #
    # @param      self  The object
    #
    def __init__(self, department):
        self.department = department
        super().__init__()
        if cmd.window('CheckUpdateUI', exists=True):
            cmd.deleteUI('CheckUpdateUI')

        self.setObjectName('CheckUpdateUI')
        self.initUI()
        self.updateMargins()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setMain(True)
        self.setStyle()

        self.splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)

        self.mainLayout.addWidget(self.splitter)

        ########################################################################
        # LEFT PANEL
        #
        self.availableItemPanel = rbwUI.RBWFrame(layout='V', margins=[5, 5, 5, 5], radius=5)

        self.availableItemsTree = rbwUI.RBWTreeWidget(['Available Items'])
        self.availableItemsTree.setIconSize(QtCore.QSize(20, 20))
        self.availableItemsTree.itemSelectionChanged.connect(self.activeTree)

        self.fillAvailableItemWidget()

        self.availableItemPanel.addWidget(self.availableItemsTree)

        self.splitter.addWidget(self.availableItemPanel)

        ########################################################################
        # RIGHT PANEL
        #
        self.mainPanel = rbwUI.RBWFrame(layout='V', margins=[5, 5, 5, 5], radius=5)

        self.splitter.addWidget(self.mainPanel)

        if 'ReferenceAndCaches' in availableItems[self.department]:
            # reference and caches tree
            self.referenceAndCachesTree = rbwUI.RBWTreeWidget(['Reference', 'Current Ref Version', 'Available Ref Versions', 'Current Cache Version', 'Available Cache Versions', 'Note', 'Status'])
            self.referenceAndCachesTree.setObjectName('ReferenceAndCachesTree')
            self.referenceAndCachesTree.setColumnWidth(0, 180)
            self.referenceAndCachesTree.setColumnWidth(1, 90)

            self.mainPanel.addWidget(self.referenceAndCachesTree)

            self.updateAllReferenceAndCachesButtun = rbwUI.RBWButton(text='Update All Reference and Caches', icon=['refresh.png'], size=[600, 35])
            self.updateAllReferenceAndCachesButtun.setObjectName('ReferenceAndCachesAllButton')
            self.updateAllReferenceAndCachesButtun.clicked.connect(self.updateAllReferences)
            self.mainPanel.addWidget(self.updateAllReferenceAndCachesButtun, alignment=QtCore.Qt.AlignCenter)

            self.fillReferenceAndCachesTree()

        if 'References' in availableItems[self.department]:
            # references tree
            self.referencesTree = rbwUI.RBWTreeWidget(['Reference', 'Current Version', 'Available Versions', 'Note', 'Status'])
            self.referencesTree.setObjectName('ReferencesTree')
            self.referencesTree.setColumnWidth(0, 180)
            self.referencesTree.setColumnWidth(1, 90)

            self.mainPanel.addWidget(self.referencesTree)

            self.updateAllReferencesButtun = rbwUI.RBWButton(text='Update All References', icon=['refresh.png'], size=[600, 35])
            self.updateAllReferencesButtun.setObjectName('ReferencesAllButton')
            self.updateAllReferencesButtun.clicked.connect(self.updateAllReferences)
            self.mainPanel.addWidget(self.updateAllReferencesButtun, alignment=QtCore.Qt.AlignCenter)

            self.fillReferencesTree()

        if 'Sets' in availableItems[self.department]:
            # sets tree
            self.setsTree = rbwUI.RBWTreeWidget(['Set', 'Current Version', 'Available Versions', 'Note', 'Status'])
            self.setsTree.setObjectName('SetsTree')
            self.setsTree.setColumnWidth(0, 180)
            self.setsTree.setColumnWidth(1, 90)

            self.mainPanel.addWidget(self.setsTree)

            self.fillSetsTree()

        if 'Assemblies' in availableItems[self.department]:
            # assemblies tree
            self.assembliesTree = rbwUI.RBWTreeWidget(['Assembly', 'Current Version', 'Available Versions', 'Mod Note', 'Status'])
            self.assembliesTree.setObjectName('AssembliesTree')
            self.assembliesTree.setColumnWidth(0, 180)
            self.assembliesTree.setColumnWidth(1, 90)

            self.mainPanel.addWidget(self.assembliesTree)

            self.updateAllAssembliesButtun = rbwUI.RBWButton(text='Update All Assemblies', icon=['refresh.png'], size=[600, 35])
            self.updateAllAssembliesButtun.setObjectName('AssembliesAllButton')
            self.updateAllAssembliesButtun.clicked.connect(self.updateAllAssemblies)
            self.mainPanel.addWidget(self.updateAllAssembliesButtun, alignment=QtCore.Qt.AlignCenter)

            self.fillAssembliesTree()

        if 'Vrscenes' in availableItems[self.department]:
            # vrscenes tree
            self.vrscenesTree = rbwUI.RBWTreeWidget(['VrScene', 'Current Version', 'Available Versions', 'Status'])
            self.vrscenesTree.setObjectName('VrscenesTree')
            self.vrscenesTree.setColumnWidth(0, 180)
            self.vrscenesTree.setColumnWidth(1, 90)

            self.mainPanel.addWidget(self.vrscenesTree)

            self.updateAllVrscenesButtun = rbwUI.RBWButton(text='Update All Vrscenes', icon=['refresh.png'], size=[600, 35])
            self.updateAllVrscenesButtun.setObjectName('VrscenesAllButton')
            self.updateAllVrscenesButtun.clicked.connect(self.updateAllVrscenes)
            self.mainPanel.addWidget(self.updateAllVrscenesButtun, alignment=QtCore.Qt.AlignCenter)

            self.fillVrscenesTree()

        if 'ShotFxs' in availableItems[self.department]:
            # shot fxs tree
            self.shotFxsTree = rbwUI.RBWTreeWidget(['Shot Fx', 'Current Version', 'Available Versions', 'Status'])
            self.shotFxsTree.setObjectName('ShotFxsTree')
            self.shotFxsTree.setColumnWidth(0, 180)
            self.shotFxsTree.setColumnWidth(1, 90)

            self.mainPanel.addWidget(self.shotFxsTree)

            self.updateAllShotFxsButtun = rbwUI.RBWButton(text='Update All Shot Fxs', icon=['refresh.png'], size=[600, 35])
            self.updateAllShotFxsButtun.setObjectName('ShotFxsAllButton')
            self.updateAllShotFxsButtun.clicked.connect(self.updateAllReferences)
            self.mainPanel.addWidget(self.updateAllShotFxsButtun, alignment=QtCore.Qt.AlignCenter)

            self.fillShotFxsTree()

        self.turnOffTrees()
        self.setItemStatus()
        self.availableItemsTree.invisibleRootItem().child(0).setSelected(True)

        self.splitter.setStretchFactor(0, 1)
        self.splitter.setStretchFactor(1, 3)

        self.setMinimumSize(800, 300)
        self.setTitle('Check Update')
        self.setIcon('refresh.png')
        self.setFocus()

    ############################################################################
    # @brief      fill available items tree.
    #
    def fillAvailableItemWidget(self):
        self.availableItemsTree.clear()

        for item in availableItems[self.department]:
            child = QtWidgets.QTreeWidgetItem(self.availableItemsTree)
            child.setText(0, item)

    ############################################################################
    # @brief      active the selected tree.
    #
    def activeTree(self):
        self.turnOffTrees()
        objectName = self.availableItemsTree.selectedItems()[0].text(0)
        tree = self.mainPanel.findChild(QtWidgets.QTreeWidget, '{}Tree'.format(objectName))
        allButton = self.mainPanel.findChild(QtWidgets.QPushButton, '{}AllButton'.format(objectName))
        tree.show()
        if allButton:
            allButton.show()

    ############################################################################
    # @brief      turn off all trees.
    #
    def turnOffTrees(self):
        for item in availableItems[self.department]:
            tree = self.mainPanel.findChild(QtWidgets.QTreeWidget, '{}Tree'.format(item))
            allButton = self.mainPanel.findChild(QtWidgets.QPushButton, '{}AllButton'.format(item))
            tree.hide()
            if allButton:
                allButton.hide()

    ############################################################################
    # @brief      Sets the item status.
    #
    def setItemStatus(self):
        root = self.availableItemsTree.invisibleRootItem()
        for i in range(0, root.childCount()):
            item = root.child(i)
            itemName = item.text(0)

            icon = QtGui.QIcon()
            if getattr(self, '{}Updated'.format(itemName.lower())):
                icon.addFile(os.path.join(iconDir, 'updated.png'))
            else:
                icon.addFile(os.path.join(iconDir, 'outOfDate.png'))

            item.setIcon(0, icon)

    ############################################################################
    # @brief      fille reference and caches tree.
    #
    def fillReferenceAndCachesTree(self):
        self.referenceAndCachesTree.clear()
        self.references = checkUpdate.listReferences(self.department)

        self.referenceandcachesUpdated = True

        for referenceSaveNode in self.references:
            if cmd.referenceQuery(self.references[referenceSaveNode]['node'], filename=True, withoutCopyNumber=True).endswith('.ma'):
                referenceAndCacheItem = scTreeWidgetItem.MshTreeWidgetItem(self.references[referenceSaveNode], self.referenceAndCachesTree)
                if referenceAndCacheItem.status == 'outOfDate':
                    self.referenceandcachesUpdated = False

    ############################################################################
    # @brief      update all reference and caches.
    #
    def updateAllReferenceAndCaches(self):
        root = self.referenceAndCachesTree.invisibleRootItem()
        for i in range(0, root.childCount()):
            item = root.child(i)
            item.update()

        self.resetReferenceAndCachesTree()

    ############################################################################
    # @brief      reset the reference and caches tree and item
    #
    def resetReferenceAndCachesTree(self):
        self.fillReferenceAndCachesTree()
        self.setItemStatus()

    ############################################################################
    # @brief      fill references tree.
    #
    def fillReferencesTree(self):
        self.referencesTree.clear()
        self.references = checkUpdate.listReferences(self.department)

        self.referencesUpdated = True

        for referenceSaveNode in self.references:
            if cmd.referenceQuery(self.references[referenceSaveNode]['node'], filename=True, withoutCopyNumber=True).endswith('.mb'):
                referenceItem = mcTreeWidgetItem.McTreeWidgetItem(self.references[referenceSaveNode], self.referencesTree)
                if referenceItem.status == 'outOfDate':
                    self.referencesUpdated = False

    ############################################################################
    # @brief      update all references.
    #
    def updateAllReferences(self):
        root = self.referencesTree.invisibleRootItem()
        for i in range(0, root.childCount()):
            item = root.child(i)
            item.update()

        self.resetReferencesTree()

    ############################################################################
    # @brief      reset the reference tree and item
    #
    def resetReferencesTree(self):
        self.fillReferencesTree()
        self.setItemStatus()

    ############################################################################
    # @brief      fill the sets tree.
    #
    def fillSetsTree(self):
        self.setsTree.clear()
        self.sets = checkUpdate.listSets()

        self.setsUpdated = True

        for setsSaveNode in self.sets:
            setItem = mcTreeWidgetItem.McTreeWidgetItem(self.sets[setsSaveNode], self.setsTree)
            if setItem.status == 'outOfDate':
                self.setsUpdated = False

    ############################################################################
    # @brief      fill assemblies tree.
    #
    def fillAssembliesTree(self):
        self.assembliesTree.clear()
        self.assemblies = checkUpdate.listAssemblies()

        self.assembliesUpdated = True

        errors = []

        for assembly in self.assemblies:
            if 'available' in self.assemblies[assembly].keys():
                assemblyItem = assemblyTreeWidgetItem.AssemblyTreeWidgetItem(assembly, self.assemblies[assembly], self.assembliesTree)
                if assemblyItem.status == 'outOfDate':
                    self.assembliesUpdated = False
                assemblyItem.updateButton.clicked.connect(self.resetAssembliesTree)
            else:
                errors.append(assembly)

        if len(errors):
            message = 'One or more assembly come from turn off assets.'
            for assembly in errors:
                message = '{}\n- {}'.format(message, assembly)
            message = '{}\nPlease delete those from the scene.'.format(message)

            rbwUI.RBWError(text=message, resizable=True)

    ############################################################################
    # @brief      update all assemblies.
    #
    def updateAllAssemblies(self):
        root = self.assembliesTree.invisibleRootItem()
        for i in range(0, root.childCount()):
            item = root.child(i)
            item.updateAssembly()

        self.resetAssembliesTree()

    ############################################################################
    # @brief      reset the assemblies tree and item
    #
    def resetAssembliesTree(self):
        self.fillAssembliesTree()
        self.setItemStatus()

    ############################################################################
    # @brief      fill the vrscenes tree.
    #
    def fillVrscenesTree(self):
        self.vrscenesTree.clear()
        self.vrscenes = checkUpdate.listVrscenes()

        self.vrscenesUpdated = True

        for vrscene in self.vrscenes:
            vrsceneItem = scTreeWidgetItem.VrsceneTreeWidgetItem(self.vrscenes[vrscene], self.vrscenesTree)
            if vrsceneItem.status == 'outOfDate':
                self.vrscenesUpdated = False
            vrsceneItem.updateButton.clicked.connect(self.resetVrscenesTree)

    ############################################################################
    # @brief      update all vrscenes.
    #
    def updateAllVrscenes(self):
        root = self.vrscenesTree.invisibleRootItem()
        for i in range(0, root.childCount()):
            item = root.child(i)
            item.update()

        self.resetVrscenesTree()

    ############################################################################
    # @brief      reset the vrscenes tree and item
    #
    def resetVrscenesTree(self):
        self.fillVrscenesTree()
        self.setItemStatus()

    ############################################################################
    # @brief      fill the shot fxs tree.
    #
    def fillShotFxsTree(self):
        self.shotFxsTree.clear()
        self.shotFxs = checkUpdate.listShotFxs()

        self.shotfxsUpdated = True

        for shotFx in self.shotFxs:
            shotFxItem = scTreeWidgetItem.ShotFxTreeWidgetItem(self.shotFxs[shotFx], self.shotFxsTree)
            if shotFxItem.status == 'outOfDate':
                self.shotfxsUpdated = False
            shotFxItem.updateButton.clicked.connect(self.resetShotFxsTree)

    ############################################################################
    # @brief      update all shot fxs.
    #
    def updateAllShotFxs(self):
        root = self.shotFxsTree.invisibleRootItem()
        for i in range(0, root.childCount()):
            item = root.child(i)
            item.update()

        self.resetShotFxsTree()

    ############################################################################
    # @brief      reset the shot fxs tree and item
    #
    def resetShotFxsTree(self):
        self.fillShotFxsTree()
        self.setItemStatus()
