import importlib
import os
from PySide2 import QtWidgets

import maya.cmds as cmd

import api.log
import api.database
import api.savedAsset
import api.widgets.rbw_UI as rbwUI
import common.checkUpdate.checkUpdate as checkUpdate

importlib.reload(api.log)
importlib.reload(api.database)
# importlib.reload(api.savedAsset)
# importlib.reload(rbwUI)
importlib.reload(checkUpdate)


################################################################################
# @brief      This class describes an assembly tree widget item.
#
class AssemblyTreeWidgetItem(QtWidgets.QTreeWidgetItem):

    ############################################################################
    # @brief      Constructs a new instance.
    #
    # @param      name      The name
    # @param      nodes     The nodes
    # @param      parent    The parent
    #
    def __init__(self, name, dic, parent):
        super().__init__(parent)

        self.name = name
        self.nodes = dic['nodes']
        self.definitionPath = '/'.join(cmd.getAttr('{}.definition'.format(self.nodes[0])).split('/')[7:-2])
        self.currentVersion = dic['current']
        self.availableVersions = dic['available']

        self.initUI()

    ############################################################################
    # @brief      Initializes the ui.
    #
    def initUI(self):
        self.setText(0, self.name)
        self.setText(1, self.currentVersion)

        self.availableVersionsCombobox = rbwUI.RBWComboBox(bgColor='transparent', fieldColor='rgba(20, 20, 20, 150)')
        self.availableVersionsCombobox.activated.connect(self.changeNote)
        self.availableVersionsCombobox.addItems(self.availableVersions)

        self.treeWidget().setItemWidget(self, 2, self.availableVersionsCombobox)

        self.changeNote()

        if self.currentVersion < self.availableVersions[0]:
            self.status = 'outOfDate'
            self.updateButton = rbwUI.RBWButton(text='OUT OF DATE', color='rgba(255, 0, 0, 150)', hover='rgba(250, 104, 104, 150)', icon=['refresh.png'], size=[130, 25])
        else:
            self.status = 'updated'
            self.updateButton = rbwUI.RBWButton(text='UPDATED', color='rgba(0, 255, 0, 150)', hover='rgba(105, 138, 105, 150)', icon=['refresh.png'], size=[130, 25])
        self.updateButton.clicked.connect(self.updateAssembly)
        self.treeWidget().setItemWidget(self, 4, self.updateButton)

    ############################################################################
    # @brief      change note function.
    #
    def changeNote(self):
        currentDefinitionPath = os.path.join(self.definitionPath, self.availableVersionsCombobox.currentText()).replace('\\', '/')
        noteQuery = "SELECT sa.note FROM `assembly` AS a JOIN `savedAsset` AS sa ON a.modHigId = sa.id WHERE a.path LIKE '%{}%' AND sa.id_mv={} AND sa.visible= 1".format(currentDefinitionPath, os.getenv('ID_MV'))
        note = api.database.selectSingleQuery(noteQuery)

        if not note:
            noteQuery = noteQuery.replace('modHigId', 'modPrxId')
            note = api.database.selectSingleQuery(noteQuery)
            if not note:
                note = ['']

        self.setText(3, note[0])

    ############################################################################
    # @brief      update function.
    #
    def updateAssembly(self):
        if self.currentVersion != self.availableVersionsCombobox.currentText():

            newPathQuery = "SELECT `path` FROM `assembly` WHERE `path` LIKE '%{}%'".format(
                os.path.join(self.definitionPath, self.availableVersionsCombobox.currentText()).replace('\\', '/')
            )

            newPath = api.database.selectSingleQuery(newPathQuery)[0]
            for node in self.nodes:
                checkUpdate.updateAssembly(node, newPath)

            self.currentVersion = self.availableVersionsCombobox.currentText()
            if self.currentVersion == self.availableVersions[0]:
                self.status = 'updated'
            else:
                self.status = 'outOfDate'
            self.initUI()
        else:
            pass
