import maya.cmds as cmd

excludeGroup = 'blendshape'


################################################################################
# @brief      Looks for all the mesh and gpuCache and fix the related shape
#             name, prepending the underscore to the Shape word.
#
# @return     the number of renamed shapes.
#
def renameShape():
    renamed = 0
    # ASSIGN TRANSFORM NAME TO SHAPE
    meshShape = [item for item in cmd.ls(dag=1, type=['mesh', 'gpuCache'], ni=1, l=1) if not item.count(excludeGroup)]
    if meshShape:
        meshParent = cmd.listRelatives(meshShape, parent=1, fullPath=1)
        meshRenamedShape = []
        for i in range(0, len(meshShape), 1):
            if cmd.referenceQuery(meshShape[i], isNodeReferenced=True):
                continue

            lookForLastChar = meshParent[i].split('|')[-1][-1]
            if lookForLastChar == '_':
                meshRenamedShape.append(cmd.rename(meshShape[i], meshParent[i].split('|')[-1] + 'Shape'))
                renamed += 1
            else:
                meshRenamedShape.append(cmd.rename(meshShape[i], meshParent[i].split('|')[-1] + '_Shape'))
                renamed += 1

        for mesh in meshParent:
            if cmd.referenceQuery(mesh, isNodeReferenced=True):
                continue

            cmd.setAttr('{}.tx'.format(mesh), lock=0)
            cmd.setAttr('{}.ty'.format(mesh), lock=0)
            cmd.setAttr('{}.tz'.format(mesh), lock=0)
            cmd.setAttr('{}.rx'.format(mesh), lock=0)
            cmd.setAttr('{}.ry'.format(mesh), lock=0)
            cmd.setAttr('{}.rz'.format(mesh), lock=0)
            cmd.setAttr('{}.sx'.format(mesh), lock=0)
            cmd.setAttr('{}.sy'.format(mesh), lock=0)
            cmd.setAttr('{}.sz'.format(mesh), lock=0)

            cmd.setAttr('{}.translate'.format(mesh), lock=0)
            cmd.setAttr('{}.rotate'.format(mesh), lock=0)
            cmd.setAttr('{}.scale'.format(mesh), lock=0)

    return renamed
