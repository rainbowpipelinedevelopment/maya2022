import importlib
import csv
import os
from datetime import datetime
from xml.dom import minidom

import maya.cmds as cmd

import api.log
import api.json
import api.asset
import api.savedAsset

importlib.reload(api.log)
importlib.reload(api.json)
importlib.reload(api.asset)
importlib.reload(api.savedAsset)

# use this code to start
"""
import importlib
import maya.cmds as cmd

import common.checkMayaSets as checkMayaSets
importlib.reload(checkMayaSets)

csvFilePath = '//speed.nas/pun/01_pre_production/08_animatic/302A/CSV/PUN_302A_CSV_RBW_v002_FINAL.csv'

checkMayaSets.runBatch(csvFilePath)
"""


################################################################################
# @brief      { function_description }
#
# @param      csvFilePath  The csv file path
#
def runBatch(csvFilePath):
    data = fromCsvToDict(csvFilePath)

    episodeString = csvFilePath.split('/')[6]

    potentialAssetList, missingAssets = getAssetList(data)

    assetList = cleanAssetList(potentialAssetList, episodeString)

    assetPaths = getFilePath(assetList)
    # api.json.json_write(assetPaths, 'C:/Users/a.olivieri/Desktop/assetPaths.json')

    checkResult = {'highPrio': {'rig': [], 'rnd': []}, 'lowPrio': {'rig': [], 'rnd': []}}
    destFolder = os.path.join(os.getenv('USERPROFILE'), 'Desktop', '{}-checkMayaSets'.format(datetime.today().strftime('%Y-%m-%d'))).replace('\\', '/')
    if not os.path.exists(destFolder):
        os.makedirs(destFolder)

    api.log.logger().debug('check maya sets ----> starts')
    for prio in assetPaths:
        for asset in assetPaths[prio]:
            api.log.logger().debug('current Asset: {}'.format(asset))
            if 'rig' in assetPaths[prio][asset]:
                if not checkMayaSets(assetPaths[prio][asset]['rig'], cnt=False):
                    checkResult[prio]['rig'].append(asset)
            if 'rnd' in assetPaths[prio][asset]:
                if not checkMayaSets(assetPaths[prio][asset]['rnd'], cnt=True):
                    checkResult[prio]['rnd'].append(asset)

    if missingAssets:
        missingAssetsPath = os.path.join(destFolder, 'missingAssets.txt').replace('\\', '/')
        writeFile(missingAssets, missingAssetsPath)

    if len(checkResult['highPrio']['rig']) > 0 or len(checkResult['lowPrio']['rig']) > 0:
        wrongRigstPath = os.path.join(destFolder, 'wrongRigs.json').replace('\\', '/')
        api.json_write({'highPrio': checkResult['highPrio']['rig'], 'lowPrio': checkResult['lowPrio']['rig']}, wrongRigstPath)

    if len(checkResult['highPrio']['rnd']) > 0 or len(checkResult['lowPrio']['rnd']) > 0:
        wrongRndsPath = os.path.join(destFolder, 'wrongRnds.json').replace('\\', '/')
        api.json_write({'highPrio': checkResult['highPrio']['rnd'], 'lowPrio': checkResult['lowPrio']['rnd']}, wrongRndsPath)

    api.log.logger().debug('check maya sets ----> ends')


################################################################################
# @brief      Writes a file.
#
# @param      inputList  The input list
# @param      filePath   The file path
#
def writeFile(inputList, filePath):
    with open(filePath, 'w') as f:
        for obj in inputList:
            f.write(f"{obj}\n")


################################################################################
# @brief      read the csv file into a python dict.
#
# @param      csvFilePath  The csv file path
#
# @return     the dict.
#
def fromCsvToDict(csvFilePath):
    data = {}
    with open(csvFilePath) as csvf:
        csvReader = csv.DictReader(csvf)

        for rows in csvReader:
            key = rows['Shot Code']
            data[key] = rows

    return data


################################################################################
# @brief      Gets the asset list.
#
# @param      data  The data
#
# @return     The asset list.
#
def getAssetList(data):
    assetStringList = []
    assetList = []
    setStringList = []
    setList = []
    missingAssets = []
    for shot in data:
        assets = data[shot]['Assets'].split(', ')
        for asset in assets:
            if not asset.startswith('set'):
                if asset not in assetStringList:
                    assetStringList.append(asset)
            else:
                if asset not in setStringList:
                    setStringList.append(asset)

    for assetCode in assetStringList + setStringList:
        try:
            category, group, name, variant = assetCode.split('_')[0:4]
        except:
            api.log.logger().debug('wrong asset data code: {}'.format(assetCode))
            missingAssets.append(assetCode)
            continue

        if assetCode in assetStringList:
            try:
                assetList.append(api.asset.Asset(params={'category': category, 'group': group, 'name': name, 'variant': variant}))
            except api.exception.InvalidAsset:
                api.log.logger().debug('wrong asset data entry: {}'.format(assetCode))
        else:
            try:
                setList.append(api.asset.Asset(params={'category': category, 'group': group, 'name': name, 'variant': variant}))
            except api.exception.InvalidAsset:
                api.log.logger().debug('wrong asset data entry: {}'.format(assetCode))

    for setAsset in setList:
        savedAssetParams = {
            'asset': setAsset,
            'approvation': 'def',
            'dept': 'set',
            'deptType': 'hig'
        }

        savedAsset = api.savedAsset.SavedAsset(params=savedAssetParams)
        latestSetSave = savedAsset.getLatest()

        if latestSetSave:
            xmlFile = os.path.join(os.getenv('MC_FOLDER'), latestSetSave[2]).replace('\\', '/').replace('.mb', '.xml')
            setSubAssets, setSubBlocks = getSetAssets(xmlFile)
            assetList = assetList + setSubAssets

            if setSubBlocks:
                for setSubBlock in setSubBlocks:
                    blockSavedAssetParams = {
                        'asset': setSubBlock,
                        'approvation': 'def',
                        'dept': 'set',
                        'deptType': 'hig'
                    }

                    blockSavedAsset = api.savedAsset.SavedAsset(params=blockSavedAssetParams)
                    latestBlockSetSave = blockSavedAsset.getLatest()

                    if latestBlockSetSave:
                        xmlFile = os.path.join(os.getenv('MC_FOLDER'), latestBlockSetSave[2]).replace('\\', '/').replace('.mb', '.xml')
                        setSubSubAssets, setSubSubBlocks = getSetAssets(xmlFile)
                        assetList = assetList + setSubSubAssets

    return assetList, missingAssets


################################################################################
# @brief      Gets the set assets.
#
# @param      xmlFile  The xml file
#
# @return     The set assets.
#
def getSetAssets(xmlFile):
    assets = []
    returnAssets = []
    returnBlocks = []
    xmlData = minidom.parse(xmlFile)
    assemblies = xmlData.getElementsByTagName('assembly')
    for assembly in assemblies:
        assemblyPath = assembly.getAttribute('path')
        assets.append('_'.join(assemblyPath.split('/')[1:5]))

    assets = list(set(assets))

    for assetCode in assets:
        category, group, name, variant = assetCode.split('_')[0:4]

        if category != 'set':
            try:
                returnAssets.append(api.asset.Asset(params={'category': category, 'group': group, 'name': name, 'variant': variant}))
            except api.exception.InvalidAsset:
                api.log.logger().debug(xmlFile)
                api.log.logger().debug('wrong asset data entry: {}'.format(assetCode))
        else:
            try:
                returnBlocks.append(api.asset.Asset(params={'category': category, 'group': group, 'name': name, 'variant': variant}))
            except api.exception.InvalidAsset:
                api.log.logger().debug('wrong asset data entry: {}'.format(assetCode))

    return returnAssets, returnBlocks


################################################################################
# @brief      Gets the shots xml.
#
# @param      episodeString  The episode string
#
# @return     The shots xml.
#
def getShotsXml(episodeString):
    shotPaths = []
    seasonString = 'S{}'.format(str(episodeString[0]).zfill(2))

    shotQuery = "SELECT * FROM `V_shotList` WHERE `projectID` = '{}' AND `season` = '{}' AND `episode` = '{}'".format(os.getenv('ID_MV'), seasonString, episodeString)
    shotResult = api.database.selectQuery(shotQuery)

    for shot in shotResult:
        shotId = shot[10]

        shotPathQuery = "SELECT `path` FROM `savedShot` WHERE `shotId` = '{}' AND (`dept` = 'Lay' OR `dept` = 'Sec') ORDER BY `date` DESC".format(shotId)
        shotPathsResult = api.database.selectQuery(shotPathQuery)

        if shotPathsResult:
            shotPaths.append(os.path.join(os.getenv('MC_FOLDER'), shotPathsResult[0][0]).replace('.ma', '.xml').replace('\\', '/'))

    return shotPaths


################################################################################
# @brief      Gets the caching assets.
#
# @param      episodeString  The episode string
#
# @return     The caching assets.
#
def getCachingAssets(episodeString):
    assetsIds = []

    shotXmlToCheck = getShotsXml(episodeString)
    for shotXml in shotXmlToCheck:
        xmlData = minidom.parse(shotXml)
        assets = xmlData.getElementsByTagName('asset')
        for asset in assets:
            assetDept = asset.getAttribute('dept')
            if assetDept == 'rigging':
                assetsIds.append(int(asset.getAttribute('id')))

    assetsIds = list(set(assetsIds))

    return assetsIds


################################################################################
# @brief      Clear the given asset list.
#
# @param      assetList      The asset list
# @param      episodeString  The episode string
#
# @return     the clear list
#
def cleanAssetList(assetList, episodeString):
    assetDict = {}

    for asset in assetList:
        assetDict[asset.variantId] = asset

    episodeCachingAsset = getCachingAssets(episodeString)

    extraAssetIds = [k for k in episodeCachingAsset if k not in assetDict]
    for extraAssetId in extraAssetIds:
        assetDict[extraAssetId] = api.asset.Asset(params={'id': extraAssetId})

    assetPaths = {'highPrio': [], 'lowPrio': []}

    assetPaths['highPrio'] = [assetDict[k] for k in assetDict if k in episodeCachingAsset]
    assetPaths['lowPrio'] = [assetDict[k] for k in assetDict if k not in episodeCachingAsset]

    return assetPaths


################################################################################
# @brief      Gets the file path.
#
# @param      assetDict  The asset dict
#
# @return     The file path.
#
def getFilePath(assetDict):
    filePaths = {'highPrio': {}, 'lowPrio': {}}

    assetList = assetDict['highPrio'] + assetDict['lowPrio']

    for asset in assetList:
        savedAssetParams = {
            'asset': asset,
            'approvation': 'def',
            'dept': 'rigging',
            'deptType': 'hig'
        }

        savedAsset = api.savedAsset.SavedAsset(params=savedAssetParams)
        latestRigSave = savedAsset.getLatest()

        prio = 'highPrio' if asset in assetDict['highPrio'] else 'lowPrio'

        if latestRigSave:
            if asset.getAssetCode() not in filePaths[prio]:
                filePaths[prio][asset.getAssetCode()] = {}
            filePaths[prio][asset.getAssetCode()]['rig'] = latestRigSave[2]

            savedAsset.dept = 'surfacing'
            savedAsset.deptType = 'rnd'

            latestRndSave = savedAsset.getLatest()

            if latestRndSave:
                if asset.getAssetCode() not in filePaths[prio]:
                    filePaths[prio][asset.getAssetCode()] = {}
                filePaths[prio][asset.getAssetCode()]['rnd'] = latestRndSave[2]

    return filePaths


################################################################################
# @brief      check maya if maya sets exist.
#
# @param      filePath  The file path
# @param      cnt       The cnt boolean check
#
# @return     check result.
#
def checkMayaSets(filePath, cnt=False):
    cmd.file(new=True, force=True)
    cmd.file(filePath, open=True, force=True)
    api.log.logger().debug('open: {}'.format(filePath))

    mshCheck = False

    if cmd.objExists('MSH'):
        mshCheck = True

    if cnt:
        if cmd.objExists('CNT'):
            cntCheck = True
        else:
            cntCheck = False
    else:
        return mshCheck

    if mshCheck and cntCheck:
        return True
    else:
        return False
